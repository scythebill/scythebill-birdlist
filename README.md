# Scythebill #

Scythebill is a desktop application for birders to keep track of their life lists and birding records. There's plenty of great birdlist software and birding software available today, but:

* Scythebill is **free** (and will remain so).
* Scythebill is **easy-to-use**. It takes a few minutes to get started.
* Scythebill is **cross-platform** - you don't need to lose your list just because you switch from Mac to Windows to Linux.
* Scythebill is **open-source**, so anyone can contribute to its development.
 
Of course, for many birders, the commercial software is absolutely ideal, and Scythebill can export your records out, so you can load them into another piece of software. It's your data - you should be able to use it anywhere you want.

Scythebill binaries can be [downloaded here](http://downloads.scythebill.com);  this website is for the open-source codebase, and not particularly intended to be user-friendly.

## Scythebill license ##

Scythebill is licensed under the [Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0) license.

## How to build ##

Required downloads:

1. Java 10. (Later versions *should* work for everything but assembling the Mac version, but have not been tested.)
2. Git (for access to the source)
3. Maven (I'm using 3.6.1, anything later will be fine too) 

### Build instructions ###

1. Download the code
2. Change to the directory with the code, and then "mvn install"
3. .. and done. The first build will be slow - lots for maven to download - but subsequent clean builds take about 30 seconds. The generic app will be in app/target/app-<version>-SNAPSHOT.jar. If you're on a Mac, the application will be in mac-app/target.

### Build instructions for Windows and Linux ###

These are both currently built using [BitRock InstallBuilder](http://installbuilder.bitrock.com/).
BitRock graciously allows use of their product for open-source projects.

### MacOS notes ###

MacOS is built via JavaPackager, right in Maven, without any extra build steps.  It's signed using a key that only Adam Winer happens to have, but can be built without that key.

(JavaPackager was removed in Java 11, so updating to later versions of Java won't be possible
for now.  JEP 343 will hopefully restore this tool.)

## How to release ##

1. Download a clean copy of the code.
2. Run mvn release:clean release:prepare

## Contributing code ##

Contributions would be very, very welcome. Please let me know if you're interested.  If you've got questions about contributing, please email support@scythebill.com.

Code style is Google-ish:

1. No tabs allowed.
2. Indent of two spaces.
3. Variable/class access should be as restrictive as possible (e.g. private instance variables by default)
4. No prefixes for variable names, which should be in "camel-case" (thisIsMyVariableName).

A few notes about the code:

1. There are far too few unit tests. Mea culpa.
2. Guice is used (perhaps abused) in a lot of the code. Most of the oddest instances are hidden in startup code and the frame registry code, so most hacking on the code should not require any deep understanding of Guice.