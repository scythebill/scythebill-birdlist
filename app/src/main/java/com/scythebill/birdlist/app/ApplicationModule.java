/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.app;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.imageio.ImageIO;

import com.google.common.base.Charsets;
import com.google.common.base.Supplier;
import com.google.common.io.Resources;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.multibindings.Multibinder;
import com.scythebill.birdlist.model.taxa.IocUpgradeLoader;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.TaxonomyMappingLoader;
import com.scythebill.birdlist.model.taxa.names.NamesLoader;
import com.scythebill.birdlist.model.taxa.names.NamesLoaders;
import com.scythebill.birdlist.ui.app.Startup;
import com.scythebill.birdlist.ui.app.TaxonomyReference;
import com.scythebill.birdlist.ui.guice.Clements;
import com.scythebill.birdlist.ui.guice.IOC;
import com.scythebill.birdlist.ui.guice.Scythebill;
import com.scythebill.birdlist.ui.guice.ScythebillSplash;
import com.scythebill.birdlist.ui.guice.SidebarImage;

/**
 * Module for application-level objects.
 */
public class ApplicationModule extends AbstractModule {
  @Override
  protected void configure() {
    TaxonomyMappingLoader map65 = new TaxonomyMappingLoader("clements6_5",
        Resources.asCharSource(Resources.getResource("65to2024.csv"), Charsets.UTF_8),
        null);
    TaxonomyMappingLoader map651 = new TaxonomyMappingLoader("clements6_5_1",
        Resources.asCharSource(Resources.getResource("651to2024.csv"), Charsets.UTF_8),
        null);
    TaxonomyMappingLoader map66 = new TaxonomyMappingLoader("clements6_6",
        Resources.asCharSource(Resources.getResource("66to2024.csv"), Charsets.UTF_8),
        null);
    TaxonomyMappingLoader map67 = new TaxonomyMappingLoader("clements6_7",
        Resources.asCharSource(Resources.getResource("67to2024.csv"), Charsets.UTF_8),
        null);
    TaxonomyMappingLoader map68 = new TaxonomyMappingLoader("clements6_8",
        Resources.asCharSource(Resources.getResource("68to2024.csv"), Charsets.UTF_8),
        Resources.asCharSource(Resources.getResource("68to2024iocssp.csv"), Charsets.UTF_8));
    Multibinder<TaxonomyMappingLoader> mappingBinder = Multibinder.newSetBinder(
        binder(), TaxonomyMappingLoader.class);
    mappingBinder.addBinding().toInstance(map65);
    mappingBinder.addBinding().toInstance(map651);
    mappingBinder.addBinding().toInstance(map66);
    mappingBinder.addBinding().toInstance(map67);
    mappingBinder.addBinding().toInstance(map68);
    TaxonomyMappingLoader map69 = new TaxonomyMappingLoader("clements6_9",
        Resources.asCharSource(Resources.getResource("69to2024.csv"), Charsets.UTF_8),
        Resources.asCharSource(Resources.getResource("69to2024iocssp.csv"), Charsets.UTF_8));
    mappingBinder.addBinding().toInstance(map69);
    TaxonomyMappingLoader map2015 = new TaxonomyMappingLoader("clements2015",
        Resources.asCharSource(Resources.getResource("2015to2024.csv"), Charsets.UTF_8),
        Resources.asCharSource(Resources.getResource("2015to2024iocssp.csv"), Charsets.UTF_8));
    mappingBinder.addBinding().toInstance(map2015);
    TaxonomyMappingLoader map2016 = new TaxonomyMappingLoader("clements2016",
        Resources.asCharSource(Resources.getResource("2016to2024.csv"), Charsets.UTF_8),
        Resources.asCharSource(Resources.getResource("2016to2024iocssp.csv"), Charsets.UTF_8));
    mappingBinder.addBinding().toInstance(map2016);
    TaxonomyMappingLoader map2017 = new TaxonomyMappingLoader("clements2017",
        Resources.asCharSource(Resources.getResource("2017to2024.csv"), Charsets.UTF_8),
        Resources.asCharSource(Resources.getResource("2017to2024iocssp.csv"), Charsets.UTF_8));
    mappingBinder.addBinding().toInstance(map2017);
    TaxonomyMappingLoader map2018 = new TaxonomyMappingLoader("clements2018",
        Resources.asCharSource(Resources.getResource("2018to2024.csv"), Charsets.UTF_8),
        Resources.asCharSource(Resources.getResource("2018to2024iocssp.csv"), Charsets.UTF_8));
    mappingBinder.addBinding().toInstance(map2018);
    TaxonomyMappingLoader map2019 = new TaxonomyMappingLoader("clements2019",
        Resources.asCharSource(Resources.getResource("2019to2024.csv"), Charsets.UTF_8),
        Resources.asCharSource(Resources.getResource("2019to2024iocssp.csv"), Charsets.UTF_8));
    mappingBinder.addBinding().toInstance(map2019);
    TaxonomyMappingLoader map2021 = new TaxonomyMappingLoader("clements2021",
        Resources.asCharSource(Resources.getResource("2021to2024.csv"), Charsets.UTF_8),
        Resources.asCharSource(Resources.getResource("2021to2024iocssp.csv"), Charsets.UTF_8));
    mappingBinder.addBinding().toInstance(map2021);
    TaxonomyMappingLoader map2022 = new TaxonomyMappingLoader("clements2022",
        Resources.asCharSource(Resources.getResource("2022to2024.csv"), Charsets.UTF_8),
        Resources.asCharSource(Resources.getResource("2022to2024iocssp.csv"), Charsets.UTF_8));
    mappingBinder.addBinding().toInstance(map2022);
    TaxonomyMappingLoader map2023 = new TaxonomyMappingLoader("clements2023",
        Resources.asCharSource(Resources.getResource("2023to2024.csv"), Charsets.UTF_8),
        Resources.asCharSource(Resources.getResource("2023to2024iocssp.csv"), Charsets.UTF_8));
    mappingBinder.addBinding().toInstance(map2023);
    
    Multibinder<IocUpgradeLoader> iocMappingBinder = Multibinder.newSetBinder(
        binder(), IocUpgradeLoader.class);
    IocUpgradeLoader upgradeIoc63 = new IocUpgradeLoader(
        "ioc_63",
        Resources.asCharSource(Resources.getResource("ioc-6.3-upgrade.csv"), Charsets.UTF_8));
    iocMappingBinder.addBinding().toInstance(upgradeIoc63);
    IocUpgradeLoader upgradeIoc64 = new IocUpgradeLoader(
        "ioc_64",
        Resources.asCharSource(Resources.getResource("ioc-6.4-upgrade.csv"), Charsets.UTF_8));
    iocMappingBinder.addBinding().toInstance(upgradeIoc64);
    IocUpgradeLoader upgradeIoc71 = new IocUpgradeLoader(
        "ioc_71",
        Resources.asCharSource(Resources.getResource("ioc-7.1-upgrade.csv"), Charsets.UTF_8));
    iocMappingBinder.addBinding().toInstance(upgradeIoc71);
    IocUpgradeLoader upgradeIoc72 = new IocUpgradeLoader(
        "ioc_72",
        Resources.asCharSource(Resources.getResource("ioc-7.2-upgrade.csv"), Charsets.UTF_8));
    iocMappingBinder.addBinding().toInstance(upgradeIoc72);
    IocUpgradeLoader upgradeIoc73 = new IocUpgradeLoader(
        "ioc_73",
        Resources.asCharSource(Resources.getResource("ioc-7.3-upgrade.csv"), Charsets.UTF_8));
    iocMappingBinder.addBinding().toInstance(upgradeIoc73);
    IocUpgradeLoader upgradeIoc81 = new IocUpgradeLoader(
        "ioc_81",
        Resources.asCharSource(Resources.getResource("ioc-8.1-upgrade.csv"), Charsets.UTF_8));
    iocMappingBinder.addBinding().toInstance(upgradeIoc81);
    IocUpgradeLoader upgradeIoc82 = new IocUpgradeLoader(
        "ioc_82",
        Resources.asCharSource(Resources.getResource("ioc-8.2-upgrade.csv"), Charsets.UTF_8));
    iocMappingBinder.addBinding().toInstance(upgradeIoc82);
    IocUpgradeLoader upgradeIoc91 = new IocUpgradeLoader(
        "ioc_91",
        Resources.asCharSource(Resources.getResource("ioc-9.1-upgrade.csv"), Charsets.UTF_8));
    iocMappingBinder.addBinding().toInstance(upgradeIoc91);
    IocUpgradeLoader upgradeIoc92 = new IocUpgradeLoader(
        "ioc_92",
        Resources.asCharSource(Resources.getResource("ioc-9.2-upgrade.csv"), Charsets.UTF_8));
    iocMappingBinder.addBinding().toInstance(upgradeIoc92);
    IocUpgradeLoader upgradeIoc101 = new IocUpgradeLoader(
        "ioc_101",
        Resources.asCharSource(Resources.getResource("ioc-10.1-upgrade.csv"), Charsets.UTF_8));
    iocMappingBinder.addBinding().toInstance(upgradeIoc101);
    IocUpgradeLoader upgradeIoc102 = new IocUpgradeLoader(
        "ioc_102",
        Resources.asCharSource(Resources.getResource("ioc-10.2-upgrade.csv"), Charsets.UTF_8));
    iocMappingBinder.addBinding().toInstance(upgradeIoc102);
    IocUpgradeLoader upgradeIoc111 = new IocUpgradeLoader(
        "ioc_111",
        Resources.asCharSource(Resources.getResource("ioc-11.1-upgrade.csv"), Charsets.UTF_8));
    iocMappingBinder.addBinding().toInstance(upgradeIoc111);
    IocUpgradeLoader upgradeIoc112 = new IocUpgradeLoader(
        "ioc_112",
        Resources.asCharSource(Resources.getResource("ioc-11.2-upgrade.csv"), Charsets.UTF_8));
    iocMappingBinder.addBinding().toInstance(upgradeIoc112);
    IocUpgradeLoader upgradeIoc121 = new IocUpgradeLoader(
        "ioc_121",
        Resources.asCharSource(Resources.getResource("ioc-12.1-upgrade.csv"), Charsets.UTF_8));
    iocMappingBinder.addBinding().toInstance(upgradeIoc121);
    IocUpgradeLoader upgradeIoc122 = new IocUpgradeLoader(
        "ioc_122",
        Resources.asCharSource(Resources.getResource("ioc-12.2-upgrade.csv"), Charsets.UTF_8));
    iocMappingBinder.addBinding().toInstance(upgradeIoc122);
    IocUpgradeLoader upgradeIoc131 = new IocUpgradeLoader(
        "ioc_131",
        Resources.asCharSource(Resources.getResource("ioc-13.1-upgrade.csv"), Charsets.UTF_8));
    iocMappingBinder.addBinding().toInstance(upgradeIoc131);
    IocUpgradeLoader upgradeIoc132 = new IocUpgradeLoader(
        "ioc_132",
        Resources.asCharSource(Resources.getResource("ioc-13.2-upgrade.csv"), Charsets.UTF_8));
    iocMappingBinder.addBinding().toInstance(upgradeIoc132);
    IocUpgradeLoader upgradeIoc141 = new IocUpgradeLoader(
        "ioc_141",
        Resources.asCharSource(Resources.getResource("ioc-14.1-upgrade.csv"), Charsets.UTF_8));
    iocMappingBinder.addBinding().toInstance(upgradeIoc141);
    IocUpgradeLoader upgradeIoc142 = new IocUpgradeLoader(
        "ioc_142",
        Resources.asCharSource(Resources.getResource("ioc-14.2-upgrade.csv"), Charsets.UTF_8));
    iocMappingBinder.addBinding().toInstance(upgradeIoc142);
  }
  
  @Provides @Clements
  TaxonomyReference providesClementsReference() {
    URL taxonUrl = Resources.getResource("taxon.xml");
    return new TaxonomyReference(taxonUrl);
  }

  @Provides @IOC
  TaxonomyReference providesIocReference() {
    URL taxonUrl = Resources.getResource("ioc-taxon.xml");
    return new TaxonomyReference(taxonUrl);
  }

  @Provides @Singleton @Scythebill
  Image providesApplicationImage() {
    URL resource = Resources.getResource("scythebill128.png");
    return Toolkit.getDefaultToolkit().createImage(resource);
  }

  @Provides @Singleton @ScythebillSplash
  Image providesSplashImage() {
    try {
      URL resource = Resources.getResource("scythebill400.jpg");
      return Toolkit.getDefaultToolkit().createImage(resource);
    } catch (IllegalArgumentException e) {
      return null;
    }
  }

  @Provides @Singleton @SidebarImage
  BufferedImage providesSidebarImage() {
    URL resource = Resources.getResource("scythebill400.jpg");
    try {
      BufferedImage image = ImageIO.read(resource);
      return image.getSubimage(0, 0, 200, 400);
    } catch (IOException e) {
      return null;
    }
  }

  /**
   * Returns a Supplier that will return the global taxonomy, or null if the
   * Taxonomy is not yet available, or loading hasn't even started.
   * <p>
   * Intentionally not a Provider - as this isn't a real Guice provider in intent.
   */
  @Provides @Clements
  Supplier<Taxonomy> provideTaxonomySupplier(final Startup startup) {
    return new Supplier<Taxonomy>() {
      @Override public Taxonomy get() {
        Future<Taxonomy> taxonomyFuture = startup.getTaxonomyFuture();
        if (taxonomyFuture == null || !taxonomyFuture.isDone()) {
          return null;
        }
        try {
          return taxonomyFuture.get();
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        } catch (ExecutionException e) {
          throw new RuntimeException(e.getCause());
        }
      }
    };
  }
  
  @Provides @IOC
  NamesLoader provideIocNamesLoader() {
    return NamesLoaders.fromUrls("multilingual/names-%s.csv");
  }

  @Provides @Clements
  NamesLoader provideClementsNamesLoader() {
    return NamesLoaders.fromUrls("multilingual/clements-names-%s.csv");
  }

  @Provides @IOC
  Supplier<Taxonomy> provideIocTaxonomySupplier(final Startup startup) {
    return new Supplier<Taxonomy>() {
      @Override public Taxonomy get() {
        Future<? extends Taxonomy> taxonomyFuture = startup.getIocTaxonomyFuture();
        if (taxonomyFuture == null || !taxonomyFuture.isDone()) {
          return null;
        }
        try {
          return taxonomyFuture.get();
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        } catch (ExecutionException e) {
          throw new RuntimeException(e.getCause());
        }
      }
    };
  }
}
