package com.scythebill.birdlist.app;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Collection;
import java.util.List;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.io.Resources;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Type;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.xml.XmlTaxonImport;
import com.scythebill.birdlist.ui.app.TaxonomyLoader;
import com.scythebill.birdlist.ui.app.TaxonomyReference;

import junit.framework.TestCase;

/**
 * Tests the IOC taxonomy mappings for potential problems.
 */
public class IocMappedTaxonomyTest extends TestCase {
  public void testTaxonomy() throws Exception {
    Taxonomy clements = loadClementsTaxonomy();
    MappedTaxonomy ioc = loadIocTaxonomy(clements);

    List<String> errors = Lists.newArrayList();
    List<String> warnings = Lists.newArrayList();
    verifySymmetricMappingsIoc(ioc, ioc.getRoot(), errors);
    verifySymmetricMappingsClements(ioc, clements.getRoot(), warnings);
    verifyMappingsExistToClements(ioc, ioc.getRoot(), errors, warnings);
    verifyMappingsExistToIoc(ioc, clements.getRoot(), errors, warnings);
    verifySubspeciesNotMappedOut(ioc, clements, ioc.getRoot(), warnings);
    verifyClementsGroupsNotMappedOut(ioc, clements, clements.getRoot(), errors);
    noticeSplits(ioc, clements, clements.getRoot(), warnings);
    noticeLumps(ioc, clements, ioc.getRoot(), warnings);
    noticeUnmappedSubspecies(ioc, clements, ioc.getRoot(), warnings);
    noticeUnmappedMonotypicClementsGroups(ioc, clements, clements.getRoot(), warnings);
        
    if (!warnings.isEmpty()) {
      System.err.println(Joiner.on('\n').join(warnings));
    }

    if (!errors.isEmpty()) {
      throw new AssertionError(Joiner.on('\n').join(errors));
    }
  }

  /**
   * For any Clements group, verify that if it got mapped to an IOC species,
   * it's the same IOC species that its parent is mapped to.  
   * 
   * Example:
   * - Osprey (Australasian) was being mapped to Eastern Osprey, but the species
   * is directly mapped to Western Osprey (not right).
   */
  private void verifyClementsGroupsNotMappedOut(
      MappedTaxonomy ioc,
      Taxonomy clements,
      Taxon clementsTaxon,
      List<String> errors) {
    if (clementsTaxon.getType() == Taxon.Type.group) {
      Resolved groupResolved = ioc.resolveInto(
              SightingTaxons.newSightingTaxon(clementsTaxon.getId()));
      if (groupResolved == null) {
        errors.add("Could not create an IOC mapping for " + clementsTaxon.getCommonName());
        ioc.resolveInto(
            SightingTaxons.newSightingTaxon(clementsTaxon.getId()));
        return;
      }
      Resolved speciesResolved = ioc.resolveInto(
              SightingTaxons.newSightingTaxon(clementsTaxon.getParent().getId()));
      if (groupResolved.getType() == Type.SINGLE
          && groupResolved.getSmallestTaxonType() == Taxon.Type.species
          && speciesResolved.getType() == Type.SINGLE
          && speciesResolved.getSmallestTaxonType() == Taxon.Type.species
          && !groupResolved.equals(speciesResolved)) {
        errors.add(String.format("Bad group mapping: %s to %s, but %s to %s",
            TaxonUtils.getCommonName(clementsTaxon),
            groupResolved.getCommonName(),
            TaxonUtils.getCommonName(clementsTaxon.getParent()),
            speciesResolved.getCommonName()));
      }
    }
    
    for (Taxon child : clementsTaxon.getContents()) {
      verifyClementsGroupsNotMappedOut(ioc, clements, child, errors);
    }
  }

  /**
   * For every species, verify that when it is mapped to Clements and then back to IOC
   * that the output is the original species.  Any failures here make it impossible to
   * allow proper input from IOC.
   */
  private void verifySymmetricMappingsIoc(
          MappedTaxonomy ioc, Taxon taxon, List<String> errors) {
    if (taxon instanceof Species) {
      SightingTaxon mapping = ioc.getMapping(taxon);
      // Ignore non-existent mappings in this step
      if (mapping != null) {
        Resolved resolved = ioc.resolveInto(mapping);
        if (resolved.getType() == SightingTaxon.Type.SINGLE) {
          if (resolved.getTaxon() != taxon) {
            errors.add(String.format("IOC %s maps back to %s!",
                TaxonUtils.getCommonName(taxon),
                resolved.getCommonName()));
          }
        } else {
          errors.add(String.format("IOC %s non-unique back to %s!",
              TaxonUtils.getCommonName(taxon),
              resolved.getCommonName()));
          ioc.getMapping(taxon);
          ioc.resolveInto(mapping);
        }
      }
    }
    
    if (taxon.getType() == Taxon.Type.subspecies
        && taxon.getName().equals(taxon.getParent().getName())
        && ioc.getExactMapping(taxon) == null) {
      SightingTaxon baseMappingOfParent = ioc.getMapping(taxon.getParent());
      if (baseMappingOfParent == null) {
        errors.add(String.format("No mapping for parent taxon %s",
            TaxonUtils.getCommonName(taxon.getParent())));
      } else if (baseMappingOfParent.getType() == SightingTaxon.Type.SP) {
        errors.add(String.format("No mapping for nominate ssp %s",
            TaxonUtils.getCommonName(taxon)));
      }
    }
    for (Taxon child : taxon.getContents()) {
      verifySymmetricMappingsIoc(ioc, child, errors);
    }
  }

  
  /**
   * For every species, verify that when it is mapped from Clements to IOC and back
   * that the output is the original species.  Failures here aren't as bad as IOC 
   * errors - but they do mean major annoyance when flipping between taxonomies
   * while entering species.
   */
  private void verifySymmetricMappingsClements(
          MappedTaxonomy ioc, Taxon taxon, List<String> errors) {
    // TODO: check this for ssp. and groups as well
    if (taxon.getType() == Taxon.Type.species) {
      Resolved mapped = ioc.resolveInto(SightingTaxons.newSightingTaxon(taxon.getId()));
      // Ignore non-existent mappings in this step
      if (mapped != null) {
        SightingTaxon backToClements = ioc.getMapping(mapped.getSightingTaxon());
        Resolved resolved = backToClements.resolve(ioc.getBaseTaxonomy());
        Collection<String> ids = TaxonUtils.simplifyTaxa(ioc.getBaseTaxonomy(), resolved.getSightingTaxon().getIds());
        resolved = SightingTaxons.newPossiblySpTaxon(ids).resolve(ioc.getBaseTaxonomy());
        
        if (resolved.getType() == SightingTaxon.Type.SINGLE) {
          if (resolved.getTaxon() != taxon) {
            errors.add(String.format("Clements %s maps back to %s!",
                TaxonUtils.getCommonName(taxon),
                resolved.getCommonName()));
          }
        } else {
          errors.add(String.format("Clements %s non-unique back to %s!",
              TaxonUtils.getCommonName(taxon),
              resolved.getCommonName()));          
        }
      }
    }
    for (Taxon child : taxon.getContents()) {
      verifySymmetricMappingsClements(ioc, child, errors);
    }
  }

  /**
   * For every IOC subspecies, verify that if it is mapped to a Clements subspecies,
   * each of them has a shared parent.
   */
  private void verifySubspeciesNotMappedOut(
          MappedTaxonomy ioc, Taxonomy clements, Taxon taxon, List<String> errors) {
    if (taxon.getType() == Taxon.Type.subspecies) {
      SightingTaxon mapping = ioc.getMapping(taxon);
      // Ignore non-existent mappings in this step
      if (mapping != null) {
        Resolved resolvedInBase = mapping.resolve(clements);
        // And ignore any IOC subspecies that get mapped to species (they're lumps, it happens)
        if (resolvedInBase.getSmallestTaxonType() != Taxon.Type.species) {
          for (Taxon subspeciesInBase : resolvedInBase.getTaxa()) {
            SightingTaxon parentMapping = ioc.getExactMapping(taxon.getParent());
            if (parentMapping != null) {
              Resolved resolvedParent = parentMapping.resolve(clements);
              boolean foundAnAncestor = false;
              for (Taxon parentTaxonInBase : resolvedParent.getTaxa()) {
                if (parentTaxonInBase == subspeciesInBase 
                    || TaxonUtils.isChildOf(parentTaxonInBase, subspeciesInBase)) {
                  foundAnAncestor = true;
                  break;
                }
              }
              
              if (!foundAnAncestor) {
                errors.add(String.format("IOC %s maps to %s, but parent maps to %s",
                    TaxonUtils.getCommonName(taxon),
                    resolvedInBase.getCommonName(),
                    resolvedParent.getCommonName()));
              }
            }
          }
        }
      }
    }
    for (Taxon child : taxon.getContents()) {
      verifySubspeciesNotMappedOut(ioc, clements, child, errors);
    }
  }

  /**
   * For every species, verify that when it is mapped from IOC, a Clements mapping exists.
   */
  private void verifyMappingsExistToClements(
      MappedTaxonomy ioc, Taxon taxon, List<String> errors, List<String> warnings) {
    if (taxon.getType() == Taxon.Type.species) {
      SightingTaxon mapping = ioc.getMapping(taxon);
      if (mapping == null) {
        Species species = (Species) taxon;
        // Don't sweat failed mappings for extinct species, as IOC includes some
        // long-extinct species that Clements does not.  Ditto data-deficient, because
        // of Deignan's Babbler
        List<String> listToAdd = (species.getStatus() == Status.EX || species.getStatus() == Status.DD)
            ? warnings
            : errors;
        listToAdd.add(String.format("No Clements mapping for %s", TaxonUtils.getCommonName(taxon)));
      }
    }
    for (Taxon child : taxon.getContents()) {
      verifyMappingsExistToClements(ioc, child, errors, warnings);
    }
  }

  /**
   * For every species, verify that when it is mapped from Clements, an IOC mapping exists.
   */
  private void verifyMappingsExistToIoc(
      MappedTaxonomy ioc, Taxon taxon, List<String> errors, List<String> warnings) {
    if (taxon.getType() == Taxon.Type.species) {
      Resolved mapping = ioc.resolveInto(SightingTaxons.newSightingTaxon(taxon.getId()));
      if (mapping == null) {
        Species species = (Species) taxon;
        // Don't sweat failed mappings for extinct species, as IOC includes some
        // long-extinct species that Clements does not.  Ditto data-deficient, because
        // of Deignan's Babbler
        List<String> listToAdd = (species.getStatus() == Status.EX || species.getStatus() == Status.DD)
            ? warnings
            : errors;
        listToAdd.add(String.format("No IOC mapping for %s",
            TaxonUtils.getCommonName(taxon)));
      }
    }
    for (Taxon child : taxon.getContents()) {
      verifyMappingsExistToIoc(ioc, child, errors, warnings);
    }
  }
  
  private void noticeSplits(
      MappedTaxonomy ioc, Taxonomy clements, Taxon taxon, List<String> warnings) {
    if (taxon.getType() == Taxon.Type.species) {
      Resolved resolved = ioc.resolveInto(SightingTaxons.newSightingTaxon(taxon.getId()));
      if (resolved == null) {
        warnings.add(String.format("No mapping for: %s", TaxonUtils.getCommonName(taxon)));
      } else if (resolved.getType() == SightingTaxon.Type.SP) {
        if (resolved.getSmallestTaxonType() == Taxon.Type.species) {
          warnings.add(String.format("Split: %s to %s",
                   TaxonUtils.getCommonName(taxon), resolved.getCommonName()));
        } else {
          warnings.add(String.format("Lump: %s", TaxonUtils.getCommonName(taxon)));
        }
      }
    } else {
      for (Taxon child : taxon.getContents()) {
        noticeSplits(ioc, clements, child, warnings);
      }
    }
  }

  private void noticeLumps(
      MappedTaxonomy ioc, Taxonomy clements, Taxon taxon, List<String> warnings) {
    if (taxon.getType() == Taxon.Type.species) {
      SightingTaxon mapping = ioc.getMapping(taxon);
      if (mapping != null) {
        Resolved resolved = mapping.resolve(clements);
        if (resolved != null && resolved.getType() == SightingTaxon.Type.SP) {
          if (resolved.getLargestTaxonType() == Taxon.Type.species) {
            warnings.add(String.format("Lump: %s to %s",
                resolved.getCommonName(), TaxonUtils.getCommonName(taxon)));
          }
        }
      }
    } else {
      for (Taxon child : taxon.getContents()) {
        noticeLumps(ioc, clements, child, warnings);
      }
    }
  }

  private String taxonAndRange(Taxon taxon) {
    return String.format("%s: %s",
        TaxonUtils.getFullName(taxon),
        ((Species) taxon).getRange());
  }
    
  private void noticeUnmappedSubspecies(
      final MappedTaxonomy ioc, Taxonomy clements, final Taxon iocTaxon, List<String> warnings) {
    if (iocTaxon.getType() == Taxon.Type.subspecies) {
      SightingTaxon mapping = ioc.getExactMapping(iocTaxon);
      // No exact mapping.  Map the parent, and search for unmapped subspecies on the other side.
      if (mapping == null) {
        mapping = ioc.getMapping(iocTaxon.getParent());
        if (mapping != null) {
          Resolved resolved = mapping.resolve(clements);
          final List<String> unmappedClementsSsps = Lists.newArrayList(); 
          for (Taxon resolvedTaxa : resolved.getTaxa()) {
            TaxonUtils.visitTaxa(resolvedTaxa, new TaxonVisitor() {
              @Override public boolean visitTaxon(Taxon taxon) {
                if (taxon.getType() == Taxon.Type.subspecies) {
                  SightingTaxon iocMapping = ioc.getExactMappingFromBase(taxon.getId());
                  if (iocMapping == null) {
                    unmappedClementsSsps.add(taxonAndRange(taxon));
                    unmappedClementsSsps.add(String.format("\"%s\",\"%s\"",
                        taxon.getId(), iocTaxon.getId()));
                  }
                }
                return true;
              }
            });
          }
          if (!unmappedClementsSsps.isEmpty()) {
            warnings.add(String.format("Unmapped %s could be any of:\n  %s",
                taxonAndRange(iocTaxon),
                Joiner.on("\n  ").join(unmappedClementsSsps)));
          }
        }
      }
    } else {
      for (Taxon child : iocTaxon.getContents()) {
        noticeUnmappedSubspecies(ioc, clements, child, warnings);
      }
    }
  }
  
  private void noticeUnmappedMonotypicClementsGroups(
      MappedTaxonomy ioc,
      Taxonomy clements,
      Taxon clementsTaxon,
      List<String> warnings) {
    if (clementsTaxon.getType() == Taxon.Type.group) {
      if (clementsTaxon.getContents().isEmpty()) {
        Resolved iocResolved = ioc.resolveInto(SightingTaxons.newSightingTaxon(clementsTaxon.getId()));
        if (iocResolved.getSmallestTaxonType() == Taxon.Type.species) {
          for (Taxon otherGroup : clementsTaxon.getParent().getContents()) {
            if (otherGroup == clementsTaxon || otherGroup.getType() != Taxon.Type.group) {
              continue;
            }
            
            Resolved otherGroupResolved = ioc.resolveInto(SightingTaxons.newSightingTaxon(otherGroup.getId()));
            if (otherGroupResolved == null) {
              warnings.add("No IOC mapping for " + otherGroup.getCommonName());
            } else if (otherGroupResolved.getType() == SightingTaxon.Type.SINGLE) {
              if (otherGroupResolved.getTaxon().getParent() == iocResolved.getTaxon()) {
                warnings.add(String.format(
                    "Clements group %s maps to %s;  looks naked",
                    clementsTaxon.getCommonName(), iocResolved.getCommonName()));
                break;
              }
            }
          }
        }
      }
    } else {
      for (Taxon child : clementsTaxon.getContents()) {
        noticeUnmappedMonotypicClementsGroups(ioc, clements, child, warnings);
      }
    }
  }


  static Taxonomy loadClementsTaxonomy() throws Exception {
    URL taxonUrl = Resources.getResource("taxon.xml");
    return new TaxonomyLoader(new TaxonomyReference(taxonUrl)).call();
  }
  
  static MappedTaxonomy loadIocTaxonomy(Taxonomy clements) throws Exception {
    URL iocTaxonUrl = Resources.getResource("ioc-taxon.xml");
    BufferedReader reader = new BufferedReader(new InputStreamReader(
        iocTaxonUrl.openStream(), Charsets.UTF_8));
    try {
      return new XmlTaxonImport().importMappedTaxa(reader, clements);
    } finally {
      reader.close();
    }
    
  }
}
