/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.app;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Verify that mapping files contain legit content, at least for the target.
 */
public class MappingFilesTest {
  private static List<String> mappingFiles;
  private static Taxonomy clements;

  @BeforeClass
  public static void loadClements() throws Exception {
    clements = IocMappedTaxonomyTest.loadClementsTaxonomy();    
  }
  
  @BeforeClass
  public static void findMappingFiles() {
    mappingFiles = Lists.newArrayList();
    URL location = ApplicationModule.class.getProtectionDomain().getCodeSource().getLocation();
    String path = location.getPath();
      
    if (path.endsWith(".jar")) {
      // JAR file (running in the real app)
      try {
        String decodedPath = URLDecoder.decode(path, "UTF-8");
        JarFile jarFile = new JarFile(decodedPath);
        try {
          Enumeration<JarEntry> entries = jarFile.entries();
          while (entries.hasMoreElements()) {
            JarEntry entry = entries.nextElement();
            System.err.println(entry.getName());
            if (entry.getName().endsWith(".csv")
                && entry.getName().contains("/")) {
              mappingFiles.add(entry.getName());
            }
          } 
        } finally {
          jarFile.close();
        }
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    } else {
      // Files - developing
      File root = new File(path);
      mappingFiles.addAll(Arrays.asList(root.list((file, name) -> name.endsWith(".csv"))));
    }
    
    // Ignore IOC mapping files for now
    mappingFiles.removeIf(s -> s.startsWith("ioc"));
    
    // Remove all the IOC "reports" files
    mappingFiles.removeIf(s -> s.startsWith("report-"));
  }
  
  @Test
  public void mappingFiles() throws Exception {
    Splitter commaSplitter = Splitter.on(',');
    for (String mappingFile : mappingFiles) {
      if (!mappingFile.contains("iocssp")) {
        try (ImportLines lines = newImportLines(mappingFile)) {
          String[] line;
          while ((line = lines.nextLine()) != null) {
            if (line.length > 3) {
              for (String id : commaSplitter.split(line[2])) {
                if (clements.getTaxon(id) == null) {
                  throw new IllegalArgumentException("Invalid taxon ID " + id + " in " + mappingFile);
                }
              }
            }
          }
        }
      }
    }
  }

  @Test
  public void iocSspFiles() throws Exception {
    Splitter commaSplitter = Splitter.on(',');
    for (String mappingFile : mappingFiles) {
      if (mappingFile.contains("iocssp")) {
        try (ImportLines lines = newImportLines(mappingFile)) {
          String[] line;
          while ((line = lines.nextLine()) != null) {
            if (line.length > 3) {
              for (String id : commaSplitter.split(line[2])) {
                if (clements.getTaxon(id) == null) {
                  throw new IllegalArgumentException("Invalid taxon ID " + id + " in " + mappingFile);
                }
              }
            }
          }
        }
      }
    }
  }

  private ImportLines newImportLines(String mappingFile) throws IOException {
    return CsvImportLines.fromUrl(ApplicationModule.class.getResource("/" + mappingFile), StandardCharsets.UTF_8);
  }
}   
