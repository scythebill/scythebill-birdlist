package com.scythebill.birdlist.app;

import java.io.PrintStream;
import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.base.Joiner;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.checklist.TransposedChecklist;
import com.scythebill.birdlist.model.checklist.TransposedChecklists;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;

public class PbcChecklistContentsTest {
  private static Checklists checklists;
  private static MappedTaxonomy ioc;
  private static Taxonomy clements;
  private static ReportSet reportSet;

  @BeforeClass
  public static void findChecklists() throws Exception {
    checklists = new Checklists(null);
    clements = IocMappedTaxonomyTest.loadClementsTaxonomy();
    ioc = IocMappedTaxonomyTest.loadIocTaxonomy(clements);
    reportSet = ReportSets.newReportSet(clements);
  }
  
  @Test
  public void testUsa_ioc() {
    inspectIocCountry("US");
  }

  @Test
  public void testCanada_ioc() {
    inspectIocCountry("CA");
  }

  @Test
  public void testAustralia_ioc() {
    inspectIocCountry("AU");
  }

  @Test
  public void testAustralia_clements() {
    inspectClementsCountry("AU");
  }
  
  //  @Test
//  public void testEngland_ioc() {
//    inspectCountry("GB-ENG");
//  }
//
//  @Test
//  public void testScotland_ioc() {
//    inspectCountry("GB-SCT");
//  }
//
//  @Test
//  public void testWales_ioc() {
//    inspectCountry("GB-WLS");
//  }
//
//  @Test
//  public void testIreland_ioc() {
//    inspectCountry("IE");
//  }

  @Test
  public void testIndonesiaAsia_ioc() {
    inspectCountry("ID-Asia", ImmutableSet.of("ID-JW", "ID-KA", "ID-NU", "ID-SL", "ID-SM"), ioc);
  }

  @Test
  public void testIndonesiaAustralasia_ioc() {
    inspectCountry("ID-Australasia", ImmutableSet.of("ID-IJ", "ID-MA"), ioc);
  }

  @Test
  public void testIndonesiaAsia_clements() {
    inspectCountry("ID-Asia", ImmutableSet.of("ID-JW", "ID-KA", "ID-NU", "ID-SL", "ID-SM"), clements);
  }
  
  @Test
  public void testIndonesiaAustralasia_clements() {
    inspectCountry("ID-Australasia", ImmutableSet.of("ID-IJ", "ID-MA"), clements);
  }
  
  @Test
  public void testAllIntroducedAndRare() {
    // Remove this "test" once rarity-from-introduced gathering is complete
    Set<SightingTaxon> species = gatherSpecies(ioc);
    TransposedChecklists transposedChecklists = TransposedChecklists.instance();
    TransposedChecklist transposed = transposedChecklists.getTransposedChecklist(reportSet, ioc);
    
    ImmutableSet<Status> rarity = ImmutableSet.of(Status.RARITY);
    ImmutableSet<Status> introduced = ImmutableSet.of(Status.INTRODUCED);
    for (SightingTaxon taxon : species) {
      if (!Iterables.isEmpty(transposed.locationsWithStatuses(taxon.getId(), introduced)) &&
          !Iterables.isEmpty(transposed.locationsWithStatuses(taxon.getId(), rarity))) {
        System.err.println("Rare and introduced: " + taxon.resolveInternal(ioc).getCommonName());
        System.err.println("    .... in: " + Joiner.on(',').join(transposed.locationsWithStatuses(taxon.getId(), rarity)));
      }
    }
  }
    
  
  @Test
  public void testAllSpeciesMapped_ioc() {
    Set<SightingTaxon> species = gatherSpecies(ioc);
    removeExtantTaxa(ioc, species);
    
    for (SightingTaxon remainingTaxon : species) {
      Resolved resolved = remainingTaxon.resolveInternal(ioc);
      // It's good to map extinct species... but critical to map non-extinct species
      @SuppressWarnings("resource")
      PrintStream stream = resolved.getTaxonStatus() == Species.Status.EX ? System.out : System.err;
      stream.println("Unmapped in IOC: " + resolved.getCommonName());
    }
  }

  @Test
  public void testAllSpeciesMapped_clements() {
    Set<SightingTaxon> species = gatherSpecies(clements);
    removeExtantTaxa(clements, species);
    
    for (SightingTaxon remainingTaxon : species) {
      Resolved resolved = remainingTaxon.resolveInternal(clements);
      // It's good to map extinct species... but critical to map non-extinct species
      @SuppressWarnings("resource")
      PrintStream stream = resolved.getTaxonStatus() == Species.Status.EX ? System.out : System.err;
      stream.println("Unmapped in Clements: " + resolved.getCommonName());
    }
  }

  void removeExtantTaxa(Taxonomy taxonomy, Set<SightingTaxon> species) {
    for (String code : checklists.checklistCodes()) {
      Checklist checklist = checklists.getChecklist(code);
      species.removeAll(checklist.getTaxa(taxonomy));
    }
  }
  
  private Set<SightingTaxon> gatherSpecies(Taxonomy taxonomy) {
    Set<SightingTaxon> taxa = Sets.newLinkedHashSetWithExpectedSize(10000);
    TaxonUtils.visitTaxa(taxonomy, taxon -> {
      if (taxon.getType() == Taxon.Type.species) {
        taxa.add(SightingTaxons.newSightingTaxon(taxon.getId()));
        return false;
      }
      return true;
    });
    
    return taxa;
  }

  private void inspectClementsCountry(String country) {
    inspectCountry(country, clements);
  }

  private void inspectIocCountry(String country) {
    inspectCountry(country, ioc);
  }
  
  private void inspectCountry(String country, Taxonomy taxonomy) {
    Set<String> stateCodes = new LinkedHashSet<>(); 
    String countryPrefix = country + "-";
    for (String state : checklists.checklistCodes()) {
      if (state.startsWith(countryPrefix) && !state.equals("US-HI")) {
        stateCodes.add(state);
      }
    }
    
    inspectCountry(country, stateCodes, taxonomy);
  }
  
  private void inspectCountry(String country, Set<String> states, Taxonomy taxonomy) {
    Checklist checklist = checklists.getChecklist(country);
    Set<SightingTaxon> countryTaxa = 
        stripEscapees(checklist, taxonomy, checklist.getTaxa(taxonomy));
    Set<SightingTaxon> countryRarities =
        checklist.getTaxa(taxonomy, Status.RARITY);
    
    Set<SightingTaxon> stateTaxa = Sets.newLinkedHashSet();
    Set<SightingTaxon> stateNonRarities = Sets.newLinkedHashSet();
    for (String state : states) {
      Checklist stateChecklist = checklists.getChecklist(state);
      stateTaxa.addAll(stripEscapees(stateChecklist, taxonomy, stateChecklist.getTaxa(taxonomy)));
      stateNonRarities.addAll(stateChecklist.getTaxa(taxonomy, Status.NATIVE));
      stateNonRarities.addAll(stateChecklist.getTaxa(taxonomy, Status.INTRODUCED));
    }
    
    SetView<SightingTaxon> countryButNotState = Sets.difference(countryTaxa, stateTaxa);
    SetView<SightingTaxon> stateButNotCountry = Sets.difference(stateTaxa, countryTaxa);
    SetView<SightingTaxon> rareInCountryButNotState = Sets.intersection(stateNonRarities, countryRarities);
    
    if (!countryButNotState.isEmpty()) {
      System.err.println();
      System.err.printf("%s has more than states in %s:\n", country, taxonomy.getName());
      for (SightingTaxon taxon : countryButNotState) {
        try {
          System.err.println(taxon.resolveInternal(taxonomy).getCommonName());
        } catch (NullPointerException e) {
          // Handle null explicitly - this test runs after IOC gets updated but before
          // checklists get updated
          System.err.println(taxon);
        }
      }
    }
    
    if (!stateButNotCountry.isEmpty()) {
      System.err.println();
      System.err.println("States have more than " + country + " in " + taxonomy.getName() + ":");
      for (SightingTaxon taxon : stateButNotCountry) {
        try {
          System.err.println(taxon.resolveInternal(taxonomy).getCommonName());
        } catch (NullPointerException e) {
          // Handle null explicitly - this test runs after IOC gets updated but before
          // checklists get updated
          System.err.println(taxon);
        }
      }
    }
    
    if (!rareInCountryButNotState.isEmpty()) {
      System.err.println();
      System.err.println("States have non-rarities that are rare in " + country + " in " + taxonomy.getName() + ":");
      for (SightingTaxon taxon : rareInCountryButNotState) {
        try {
          System.err.println(taxon.resolveInternal(taxonomy).getCommonName());
        } catch (NullPointerException e) {
          // Handle null explicitly - this test runs after IOC gets updated but before
          // checklists get updated
          System.err.println(taxon);
        }
      }
    }
  }

  private Set<SightingTaxon> stripEscapees(
      final Checklist checklist, final Taxonomy taxonomy, ImmutableSet<SightingTaxon> taxa) {
    return FluentIterable.from(taxa)
        .filter(taxon -> checklist.getStatus(taxonomy, taxon) != Status.ESCAPED).toSet();
  }
}
