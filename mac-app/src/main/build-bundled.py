


#! /usr/bin/env python
# Builds the Scythebill.app file
import os
import shutil
import sys

appDir = "target/bundled/Scythebill.app/"
signingKey = "Developer ID Application: Adam Winer (KEU5368JH7)"

def MakeDirsIfNeeded(path):
  if not os.path.exists(path):
    os.makedirs(path)

def DeleteIfPresent(path):
  if os.path.exists(path):
    os.remove(path)

def CodeSign(path, extraArgs=""):
    command = "codesign --verbose " + extraArgs + " --force -s \"" + signingKey + "\" -f \"" + path + "\""
    if os.system(command):
      print "FAILED: " + command
    else:
      print "SUCCESS: " + command

def build():
  print "Building..."

  MakeDirsIfNeeded("target/bundled")

  version = sys.argv[1]
  os.system("ant bundle-scythebill -Dproject.version=" + version)
  shutil.copy("target/classes/Info-bundled.plist", appDir + "Contents/Info.plist")

  for filename in os.listdir("src/main/macresources"):
    # Skip all . files and directories (e.g., .svn)
    if filename.startswith(".") or filename.endswith("~"):
      continue

    # .plists are handled specially
    if (filename.endswith(".plist")):
      continue

    shutil.copy("src/main/macresources/" + filename, appDir + "Contents/Resources/" + filename)

  # Manually copy Quaqua in, as it is not available in a
  # Maven repository, and we want the jnilibs as well
  #quaquaDir = os.environ['QUAQUA_HOME'] + "/dist/"
  #shutil.copy(quaquaDir + "libquaqua.jnilib", \
  #    appDir + "Contents/Java/libquaqua.jnilib")
  #shutil.copy(quaquaDir + "libquaqua64.jnilib", \
  #    appDir + "Contents/Java/libquaqua64.jnilib")
  
  shutil.copy("../app/src/main/resources/taxon.xml", \
      appDir + "Contents/Resources/taxon.xml")

  shutil.copy("../app/src/main/resources/ioc-taxon.xml", \
      appDir + "Contents/Resources/ioc-taxon.xml")

  # Remove large Java files that are unused by Scythebill
  javadir = appDir + "Contents/PlugIns/" + os.environ['JAVA_VERSION'] + \
      "/Contents/Home/jre/"
  # Not using JavaFX
  DeleteIfPresent(javadir + "lib/libjfxwebkit.dylib")
  DeleteIfPresent(javadir + "lib/jfxrt.jar")
  # Not using gstreamer
  DeleteIfPresent(javadir + "lib/libgstreamer-lite.dylib")

  #shutil.rmtree(javadir + "lib/fonts")

  if os.system("/usr/bin/SetFile -a B " + appDir):
    print "Could not set app file status"
  else:
    print "Set file status"

  os.system("xattr -cr " + appDir)

  jdkPlugin = appDir + "Contents/PlugIns/" + os.environ['JAVA_VERSION']  
  CodeSign(jdkPlugin)

  CodeSign(appDir)

if __name__ == "__main__":
  build()
