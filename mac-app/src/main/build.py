#! /usr/bin/env python
# Builds the Scythebill.app file
import os
import shutil
import sys

appDir = "target/binary/Scythebill.app/"

def MakeDirsIfNeeded(path):
  if not os.path.exists(path):
    os.makedirs(path)

def build():
  print "Building..."
  MakeDirsIfNeeded(appDir + "Contents/MacOS")
  MakeDirsIfNeeded(appDir + "Contents/Resources/Java/lib")
  shutil.copy("/System/Library/Frameworks/JavaVM.framework/Versions/Current/Resources/MacOS/JavaApplicationStub", appDir + "Contents/MacOS/JavaApplicationStub")
  os.chmod(appDir + "Contents/MacOS/JavaApplicationStub", 0755)

  shutil.copy("target/classes/Info.plist", appDir + "Contents/Info.plist")

  for filename in os.listdir("src/main/macresources"):
    # Skip all . files and directories (e.g., .svn)
    if filename.startswith(".") or filename.endswith("~"):
      continue

    # .plists are handled specially
    if (filename.endswith(".plist")):
      continue

    # Treat Info.plist specially - read from target/classes, where the version
    # number has been inserted
    shutil.copy("src/main/macresources/" + filename, appDir + "Contents/Resources/" + filename)

  # Manually copy Quaqua in, as it is not available in a
  # Maven repository, and we want the jnilibs as well
  quaquaDir = os.environ['QUAQUA_HOME'] + "/dist/"
  shutil.copy(quaquaDir + "quaqua-patched.jar", \
      appDir + "Contents/Resources/Java/quaqua.jar")
  shutil.copy(quaquaDir + "libquaqua.jnilib", \
      appDir + "Contents/Resources/Java/libquaqua.jnilib")
  shutil.copy(quaquaDir + "libquaqua64.jnilib", \
      appDir + "Contents/Resources/Java/libquaqua64.jnilib")

  shutil.copy("../app/src/main/resources/taxon.xml", \
      appDir + "Contents/Resources/taxon.xml")

  shutil.copy("../app/src/main/resources/ioc-taxon.xml", \
      appDir + "Contents/Resources/ioc-taxon.xml")
 
  version = sys.argv[1]
  shutil.copy("target/mac-app-" + version + ".jar", \
      appDir + "Contents/Resources/Java/app.jar")

  if os.system("/usr/bin/SetFile -a B " + appDir):
    print "Could not set app file status"
  else:
    print "Set file status"

  signingKey = "Developer ID Application: Adam Winer (KEU5368JH7)"
  if os.system("codesign -s \"" + signingKey + "\" -f \"" + appDir + "\""):
    print "Could not sign file"
  else:
    print "Signed file"

if __name__ == "__main__":
  build()
