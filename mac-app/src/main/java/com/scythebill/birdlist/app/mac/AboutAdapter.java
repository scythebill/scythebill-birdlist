/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.app.mac;

import java.awt.desktop.AboutEvent;
import java.awt.desktop.AboutHandler;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.scythebill.birdlist.ui.panels.AboutFrame;

@Singleton
public class AboutAdapter implements AboutHandler {
  private final AboutFrame aboutFrame;

  @Inject
  public AboutAdapter(AboutFrame aboutFrame) {
    this.aboutFrame = aboutFrame;
  }

  @Override
  public void handleAbout(AboutEvent event) {
    aboutFrame.show();
  }
}
