/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.app.mac;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.desktop.OpenFilesEvent;
import java.awt.desktop.OpenFilesHandler;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.swing.LookAndFeel;
import javax.swing.UIManager;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.escape.Escaper;
import com.google.common.html.HtmlEscapers;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.scythebill.birdlist.app.ApplicationModule;
import com.scythebill.birdlist.model.xml.XmlReportSetImport;
import com.scythebill.birdlist.ui.app.FrameRegistry;
import com.scythebill.birdlist.ui.app.OtherFileLoaderRegistry;
import com.scythebill.birdlist.ui.app.Startup;
import com.scythebill.birdlist.ui.guice.CommonModule;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.FilePreferences;
import com.scythebill.birdlist.ui.panels.MainFrame;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * MacOS-specific app launcher.
 */
@Singleton
public class App {
  private final static Logger logger = Logger.getLogger(App.class.getName());
  private final Startup startup;
  private final FrameRegistry frameRegistry;
  private final FilePreferences filePreferences;
  private final QuitAdapter quit;
  private final AboutAdapter about;
  private final PreferencesAdapter preferences;
  private final Alerts alerts;
  private OtherFileLoaderRegistry otherFilesLoader;
  
  @Inject
  public App(
      FrameRegistry frameRegistry,
      Startup startup,
      QuitAdapter quit,
      AboutAdapter about,
      PreferencesAdapter preferences,
      FilePreferences filePreferences,
      Alerts alerts,
      OtherFileLoaderRegistry otherFilesLoader) {
    this.frameRegistry = frameRegistry;
    this.startup = startup;
    this.quit = quit;
    this.about = about;
    this.preferences = preferences;
    this.filePreferences = filePreferences;
    this.alerts = alerts;
    this.otherFilesLoader = otherFilesLoader;
  }
  
  public void start() {
    startup.init();
    
    Desktop desktop = Desktop.getDesktop();
    desktop.setOpenFileHandler(new OpenFilesHandler() {
      @Override
      public void openFiles(OpenFilesEvent event) {
        
        List<File> checklistFiles = Lists.newArrayList();
        List<File> otherFiles = Lists.newArrayList();
        // Divide files into checklist files and "others"
        for (File file : event.getFiles()) {
          if (file.getName().endsWith(XmlReportSetImport.REPORT_SET_SUFFIX)) {
            checklistFiles.add(file);
          } else {
            otherFiles.add(file);
          }
        }
        
        // If loading all the files fails, then show the "no other window" frame
        final AtomicInteger counter = new AtomicInteger(checklistFiles.size());
        Runnable onFailure = () -> {
          if (counter.decrementAndGet() == 0) {
            frameRegistry.showNoOtherWindowFrame();
            startup.finish();              
          }
        };
        // Notify "otherFilesLoader" of the other files to process
        otherFilesLoader.addFiles(otherFiles);
        for (File file : checklistFiles) {
          try {
            frameRegistry.startLoadingReportSet(file.getAbsolutePath(), onFailure);
          } catch (Exception e) {
            alerts.reportError(e);
          }
        }        
      }
    });
    desktop.setQuitHandler(quit);
    desktop.setAboutHandler(about);
    
    desktop.setPreferencesHandler(preferences);
    
    EventQueue.invokeLater(() -> {
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
              @Override
              public void uncaughtException(Thread thread, Throwable t) {
                logger.log(Level.SEVERE, "Unhandled exception", t);
                alerts.reportError(t);
              }
            });
        startup.start(this::onGlobalsLoaded);
    });    
  }

  /**
   * Called once the global state has been properly initialized.
   * Shows an open-file dialog if no file has been opened yet.
   */
  private void onGlobalsLoaded() {
    // If we've gotten this far, and no open-file message has been received,
    // then try to open the last file opened.  If that doesn't exist,
    // then show the "Open/New" window as a clearer call to action.
    if (frameRegistry.isEmpty()) {
      boolean successfullyOpened = false;
      List<String> failedLoads = new ArrayList<>();
      for (String lastLoadedReportSet : filePreferences.getLastLoadedReportSets()) {
        File file = new File(lastLoadedReportSet);
        if (file.exists()) {
          // Start loading the file, and return.  But if loading fails, then show
          // the "no other window" frame
          Runnable onFailure = () -> {
            frameRegistry.showNoOtherWindowFrame();
            startup.finish();              
          };
          frameRegistry.startLoadingReportSet(lastLoadedReportSet, onFailure);
          successfullyOpened = true;
        } else {
          String fileName = file.getName();
          if (file.getParent() == null) {
            failedLoads.add(fileName);
          } else {
            failedLoads.add(
                String.format(Messages.getMessage(Name.FILE_IN_DIRECTORY), fileName, file.getParent()));
          }
        }
      }
      
      if (!successfullyOpened) {
        if (!failedLoads.isEmpty()) {
          Escaper htmlEscaper = HtmlEscapers.htmlEscaper();
          boolean multipleFiles = failedLoads.size() != 1;
          alerts.showError(null,
              Messages.getMessage(multipleFiles ? Name.CANT_FIND_BSXM_FILE_TITLE : Name.CANT_FIND_BSXM_FILES_TITLE),
              Messages.getMessage(multipleFiles ? Name.CANT_FIND_BSXM_FILE_MESSAGE : Name.CANT_FIND_BSXM_FILES_MESSAGE)
              + "<br>"
              + failedLoads.stream() 
                  .map(htmlEscaper::escape)
                  .map(s -> "<br>&nbsp;&nbsp;&nbsp;" + s)
                  .collect(Collectors.joining())
              + "<br><br>"
              + Messages.getMessage(multipleFiles ? Name.PLEASE_SEARCH_FOR_BSXM_FILE : Name.PLEASE_SEARCH_FOR_BSXM_FILES));
        }
        frameRegistry.showNoOtherWindowFrame();
        startup.finish();
      }
    }
  }

  /**
   * Kick off the application.
   */
  public static void main(String[] args) throws Exception {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
      System.setProperty("apple.laf.useScreenMenuBar", "true");
      
      // Pull in a variety of useful MacOS defaults
      UIManager.put("Browser.selectionBackground", new Color(208, 208, 208));
      UIManager.put("Browser.selectionForeground", new Color(0, 0, 0));
      UIManager.put("Browser.inactiveSelectionBackground", new Color(208, 208, 208));
      UIManager.put("Browser.inactiveSelectionForeground", new Color(0, 0, 0));
      UIManager.put("Table.alternateBackground.0", new Color(237, 243, 254));
      UIManager.put("Table.alternateBackground.1", new Color(255, 255, 255));
      
      // And MacOS-specific icons for the browser
      for (String name : ImmutableList.of(
          "Browser.expandedIcon",
          "Browser.expandingIcon",
          "Browser.focusedSelectedExpandedIcon",
          "Browser.focusedSelectedExpandingIcon",
          "Browser.sizeHandleIcon",
          "Browser.selectedExpandedIcon",
          "Browser.selectedExpandingIcon")) {
        UIManager.put(name, LookAndFeel.makeIcon(App.class, String.format("/%s.png", name)));
      }
      // The selectionForeground color (white) looks poor;  use the standard foreground
      UIManager.put("Table.selectionForeground", UIManager.get("Table.foreground"));
    } catch (Exception e) {
      logger.log(Level.SEVERE, "Could not set system L&F", e);
    }
    initUI();

    Injector injector = Guice.createInjector(
        new AbstractModule() {
          @Override
          protected void configure() {
            binder().requireAtInjectOnConstructors();
          }
        },
        new ApplicationModule(),
        new MacApplicationModule(),
        new CommonModule());
    try {
      injector.getInstance(App.class).start();
    } catch (Exception e) {
      injector.getInstance(Alerts.class).reportError(e);
      System.exit(1);
    }
  }
  
  private static void initUI() {
    MainFrame.initCommonUI();
  }
}
