/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.app.mac;

import com.google.common.collect.ImmutableList;
import com.google.inject.AbstractModule;
import com.google.inject.Module;
import com.scythebill.birdlist.ui.app.FrameFactory;
import com.scythebill.birdlist.ui.util.FileDialogs;

/**
 * Module for application-level bindings. 
 */
public class MacApplicationModule extends AbstractModule {
  @Override
  protected void configure() {
    bind(FrameFactory.class).toInstance(new FrameFactory() {
      @Override protected Iterable<? extends Module> getCustomPerWindowModules() {
        return ImmutableList.of(new PerWindowModule());
      }
    });
    
    bind(FileDialogs.Flags.class).toInstance(new FileDialogs.Flags() {
      @Override public boolean useNativeOpenDialog() {
        // Native open dialogs behave generally better
        return true;
      }

      @Override public boolean useNativeSaveDialog() {
        // https://bugs.openjdk.java.net/browse/JDK-8013553 prevented this
        // in Java 7, but we're on Java 8 now 
        return true;
      }
      
    });
  }

  /*
  @Provides @Singleton @Scythebill
  Image providesApplicationImage() {
    return Taskbar.getTaskbar().getIconImage();
  }
  */
}
