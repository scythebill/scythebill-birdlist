


#! /usr/bin/env python
# Builds the Scythebill.app file
import os
import shutil
import sys

appDir = "deploy/Scythebill.app/"
appDirAarch = "deploy_aarch/Scythebill.app/"
signingKey = "Developer ID Application: Adam Winer (KEU5368JH7)"

def MakeDirsIfNeeded(path):
  if not os.path.exists(path):
    os.makedirs(path)

def DeleteIfPresent(path):
  if os.path.exists(path):
    os.remove(path)

def Command(command):
  if os.system(command):
    print("FAILED: " + command)
  else:
    print("SUCCESS: " + command)

def CodeSign(path, extraArgs=""):
    command = "codesign --verbose " + extraArgs + " --force -s \"" + signingKey + "\" -f \"" + path + "\""
    Command(command)

def build():
  print("Building...")

  if os.path.exists(appDir):
    shutil.rmtree(appDir)
 
  if os.path.exists(appDirAarch):
    shutil.rmtree(appDirAarch)

  version = sys.argv[1]
  if version.endswith('-SNAPSHOT'):
    versionNoSnapshot = version[:-len('-SNAPSHOT')]
  else:
    versionNoSnapshot = version
  

  # jdk.unsupported: used by gson
  # java.naming: used by httpclient
  # jdk.crypto.cryptoki: without that, SSL handshakes break
  # java.sql: needed by com.opencsv.CSVWriter
  # jd.charsets: needed by org.apache.poi.util.StringUtil, annoyingly
  
  Command("$JAVA_HOME_X86/bin/jpackage --type app-image --app-version " + versionNoSnapshot + " --icon src/main/macresources/scythebill.icns " +
          "--add-modules java.base,java.desktop,java.prefs,java.xml,java.logging,java.naming,jdk.crypto.cryptoki,jdk.unsupported,java.sql,jdk.charsets,jdk.localedata,jdk.zipfs " +
          "--name Scythebill --dest deploy --resource-dir target/classes --input target --main-jar mac-app-" + version + ".jar " +
          "--main-class com.scythebill.birdlist.app.mac.App --mac-sign --mac-signing-key-user-name 'Developer ID Application: Adam Winer (KEU5368JH7)' " +
          "--java-options -Xdock:name=Scythebill")
  Command("$JAVA_HOME_AARCH/bin/jpackage --type app-image --app-version " + versionNoSnapshot + " --icon src/main/macresources/scythebill.icns " +
          "--add-modules java.base,java.desktop,java.prefs,java.xml,java.logging,java.naming,jdk.crypto.cryptoki,jdk.unsupported,java.sql,jdk.charsets,jdk.localedata,jdk.zipfs " +
          "--name Scythebill --dest deploy_aarch --resource-dir target/classes --input target --main-jar mac-app-" + version + ".jar " +
          "--main-class com.scythebill.birdlist.app.mac.App --mac-sign --mac-signing-key-user-name 'Developer ID Application: Adam Winer (KEU5368JH7)' " +
          "--java-options -Xdock:name=Scythebill")
#  shutil.copy("target/classes/Info-bundled.plist", appDir + "Contents/Info.plist")

#  for filename in os.listdir("src/main/macresources"):
    # Skip all . files and directories (e.g., .svn)
#    if filename.startswith(".") or filename.endswith("~"):
#      continue

    # .plists are handled specially
#    if (filename.endswith(".plist")):
#      continue

#    sourceFile = "src/main/macresources/" + filename
#    if os.path.isdir(sourceFile):
#      continue
    
#    shutil.copy(sourceFile, appDir + "Contents/Resources/" + filename)

#  shutil.copy("../app/src/main/resources/taxon.xml", \
#    appDir + "Contents/Resources/taxon.xml")

#  shutil.copy("../app/src/main/resources/ioc-taxon.xml", \
#    appDir + "Contents/Resources/ioc-taxon.xml")

  # Remove large Java files that are unused by Scythebill
#  javadir = appDir + "Contents/PlugIns/Java.runtime/"

  # Not using JavaFX;  this lib is unused 60MB!
#  DeleteIfPresent(javadir + "Contents/Home/lib/libjfxwebkit.dylib")

  # Not obviously used, and is making notarization unhappy
#  DeleteIfPresent(javadir + "Contents/Home/lib/security/public_suffix_list.dat")

  # Should only be used for java Process calls (which Scythebill doesn't use)
  # and would require additional signing
#  DeleteIfPresent(javadir + "Contents/Home/lib/jspawnhelper")
  
#  os.system("xattr -cr " + appDir)

#  jdkPlugin = appDir + "Contents/PlugIns/Java.runtime"
#  CodeSign(jdkPlugin)

#  CodeSign(appDir + "Contents/MacOS/libpackager.dylib")
#  CodeSign(appDir)

#  Command("find " + javadir + "Contents " +
#          "-name \*.dylib -exec codesign --timestamp -vvv -f --sign "+
#          "\"Developer ID Application: Adam Winer (KEU5368JH7)\" -f {} \;")
#  Command("codesign --entitlements src/main/macresources/entitlements.plist " +
#          "--options runtime --timestamp --deep -vvv -f --sign " +
#          "\"Developer ID Application: Adam Winer (KEU5368JH7)\" " +
#          "-f \"" + appDir + "\"")
  Command("ditto -c -k --sequesterRsrc --keepParent deploy/Scythebill.app/ deploy/Scythebill.zip")
  Command("ditto -c -k --sequesterRsrc --keepParent deploy_aarch/Scythebill.app/ deploy_aarch/Scythebill.zip")

if __name__ == "__main__":
  build()
