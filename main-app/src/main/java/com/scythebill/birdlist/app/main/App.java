/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.app.main;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.swing.ImageIcon;
import javax.swing.UIManager;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.escape.Escaper;
import com.google.common.html.HtmlEscapers;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.scythebill.birdlist.app.ApplicationModule;
import com.scythebill.birdlist.model.xml.XmlReportSetImport;
import com.scythebill.birdlist.ui.app.FrameRegistry;
import com.scythebill.birdlist.ui.app.OtherFileLoaderRegistry;
import com.scythebill.birdlist.ui.app.Startup;
import com.scythebill.birdlist.ui.guice.CommonModule;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.FilePreferences;
import com.scythebill.birdlist.ui.panels.MainFrame;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * App launcher.
 */
@Singleton
public class App {
  private final static Logger logger = Logger.getLogger(App.class.getName());
  private final Startup startup;
  private final FrameRegistry frameRegistry;
  private final FilePreferences filePreferences;
  private final Alerts alerts;
  private final OtherFileLoaderRegistry otherFileLoaderRegistry;
  
  @Inject
  public App(
      FrameRegistry frameRegistry,
      Startup startup,
      FilePreferences filePreferences,
      Alerts alerts,
      OtherFileLoaderRegistry otherFileLoaderRegistry) {
    this.frameRegistry = frameRegistry;
    this.startup = startup;
    this.filePreferences = filePreferences;
    this.alerts = alerts;
    this.otherFileLoaderRegistry = otherFileLoaderRegistry;
  }

  public void start(final ImmutableList<String> args) {
    startup.init();
    
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
          @Override
          public void uncaughtException(Thread thread, Throwable t) {
            logger.log(Level.SEVERE, "Unhandled exception", t);
            alerts.reportError(t);
          }
        });
        startup.start(() -> onGlobalsLoaded(args));
      }
    });    
  }

  /**
   * Called once the global state has been properly initialized.
   * Shows an open-file dialog if no file has been opened yet.
   */
  private void onGlobalsLoaded(ImmutableList<String> args) {
    // If we've gotten this far, and no open-file message has been received,
    // then try to open the last file opened.  If that doesn't exist,
    // then show the "Open/New" window as a clearer call to action.
    final Runnable whenNoFiles = () -> {
      frameRegistry.showNoOtherWindowFrame();
      startup.finish();              
    };

    if (frameRegistry.isEmpty()) {
      if (!args.isEmpty()) {
        List<File> files = Lists.newArrayList();
        List<File> otherFiles = Lists.newArrayList();
        for (String arg : args) {
          File file = new File(arg);
          if (file.exists()) {
            // TODO: do something more intelligent with .btxm files
            if (arg.endsWith(XmlReportSetImport.REPORT_SET_SUFFIX)) {
              files.add(file);
            } else {
              otherFiles.add(file);
            }
          }
        }

        otherFileLoaderRegistry.addFiles(otherFiles);

        if (!files.isEmpty()) {
          // Try to load all of the files (more-or-less in parallel).  If all the files fail
          // to load, then show the "no files" window
          final AtomicInteger fileCount = new AtomicInteger(files.size());
          Runnable onFileFailure = () -> {
            if (fileCount.decrementAndGet() == 0) {
              whenNoFiles.run();
            }
          };
          
          for (File file : files) {
            frameRegistry.startLoadingReportSet(file.getAbsolutePath(), onFileFailure);
          }
          return;
        }
      }
      
      boolean successfullyOpened = false;
      List<String> failedLoads = new ArrayList<>();
      for (String lastLoadedReportSet : filePreferences.getLastLoadedReportSets()) {
        File file = new File(lastLoadedReportSet);
        if (file.exists()) {
          // Start loading the file, and return.  But if loading fails, then show
          // the "no other window" frame
          // Start loading the file, and return
          frameRegistry.startLoadingReportSet(lastLoadedReportSet, whenNoFiles);
          successfullyOpened = true;
        } else {
          String fileName = file.getName();
          if (file.getParent() == null) {
            failedLoads.add(fileName);
          } else {
            failedLoads.add(
                String.format(Messages.getMessage(Name.FILE_IN_DIRECTORY), fileName, file.getParent()));
          }
        }
      }      
      
      if (!successfullyOpened) {
        if (!failedLoads.isEmpty()) {
          Escaper htmlEscaper = HtmlEscapers.htmlEscaper();
          boolean multipleFiles = failedLoads.size() != 1;
          alerts.showError(null,
              Messages.getMessage(multipleFiles ? Name.CANT_FIND_BSXM_FILE_TITLE : Name.CANT_FIND_BSXM_FILES_TITLE),
              Messages.getMessage(multipleFiles ? Name.CANT_FIND_BSXM_FILE_MESSAGE : Name.CANT_FIND_BSXM_FILES_MESSAGE)
              + "<br>"
              + failedLoads.stream() 
                  .map(htmlEscaper::escape)
                  .map(s -> "<br>&nbsp;&nbsp;&nbsp;" + s)
                  .collect(Collectors.joining())
              + "<br><br>"
              + Messages.getMessage(multipleFiles ? Name.PLEASE_SEARCH_FOR_BSXM_FILE : Name.PLEASE_SEARCH_FOR_BSXM_FILES));
        }
        whenNoFiles.run();
      }
    }
  }

  /**
   * Kick off the application.
   */
  public static void main(String[] args) throws Exception {
    initUI();
    
    Injector injector = Guice.createInjector(
        new AbstractModule() {
          @Override
          protected void configure() {
            binder().requireAtInjectOnConstructors();
          }
        },
        new MainApplicationModule(),
        new ApplicationModule(),
        new CommonModule());

    try {
      injector.getInstance(App.class).start(ImmutableList.copyOf(args));
    } catch (Exception e) {
      injector.getInstance(Alerts.class).reportError(e);
      System.exit(1);
    }
  }

  private static void initUI() throws Exception {
    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    MainFrame.initCommonUI();
    Image arrowImage = Toolkit.getDefaultToolkit().createImage(
        App.class.getResource("/grayarrow.png"));
    UIManager.put("Browser.expandedIcon", new ImageIcon(arrowImage));
    UIManager.put("Browser.Icon", new ImageIcon(arrowImage));
    UIManager.put("Browser.selectedExpandedIcon", new ImageIcon(arrowImage));
    UIManager.put("Browser.focusedSelectedExpandedIcon", new ImageIcon(arrowImage));
    // Add a size handle
    Image sizeHandleImage = Toolkit.getDefaultToolkit().createImage(
        App.class.getResource("/sizeHandleIcon.png"));
    UIManager.put("Browser.sizeHandleIcon", new ImageIcon(sizeHandleImage));
    // Populate list-selection-inactive colors, which are important for the browser component,
    // but typically only set on MacOS
    if (UIManager.getColor("List.selectionInactiveBackground") == null) {
      UIManager.put("List.selectionInactiveBackground", new Color(202, 202, 202));
    }
    if (UIManager.getColor("Tree.selectionInactiveBackground") == null) {
      UIManager.put("Tree.selectionInactiveBackground", new Color(202, 202, 202));
    }
  }
}
