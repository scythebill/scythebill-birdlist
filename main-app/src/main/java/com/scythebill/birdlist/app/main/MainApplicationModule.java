package com.scythebill.birdlist.app.main;

import com.google.common.collect.ImmutableList;
import com.google.inject.AbstractModule;
import com.google.inject.Module;
import com.scythebill.birdlist.ui.app.FrameFactory;

public class MainApplicationModule extends AbstractModule {

  @Override
  protected void configure() {
    bind(FrameFactory.class).toInstance(new FrameFactory() {
      @Override protected Iterable<? extends Module> getCustomPerWindowModules() {
        return ImmutableList.of(new PerWindowModule());
      }
    });    
  }
  

}
