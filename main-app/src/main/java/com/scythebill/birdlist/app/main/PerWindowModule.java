/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.app.main;

import com.google.inject.AbstractModule;
import com.scythebill.birdlist.ui.actions.DefaultMenuConfiguration;
import com.scythebill.birdlist.ui.actions.MenuConfiguration;

/**
 * Module for per-window objects.
 */
public class PerWindowModule extends AbstractModule {
  @Override
  protected void configure() {
    bind(MenuConfiguration.class).to(DefaultMenuConfiguration.class);
  }
}
