/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.checklist;

import java.util.Set;

import com.google.common.collect.ImmutableSet;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;

public interface Checklist {
  public enum Status {
    NATIVE(null),
    ENDEMIC("endemic"),
    EXTINCT("extinct"),
    INTRODUCED("introduced"),
    RARITY("rarity"),
    RARITY_FROM_INTRODUCED("rarity-from-introduced"),
    ESCAPED("escaped");
    

    private final String text;
    private Status(String text) {
      this.text = text;
    }
    
    public String text() {
      return text; 
    }    
  }
  
  ImmutableSet<SightingTaxon> getTaxa(Taxonomy taxonomy);

  ImmutableSet<SightingTaxon> getTaxa(Taxonomy taxonomy, Status status);

  Status getStatus(Taxonomy taxonomy, SightingTaxon taxon);
  
  boolean includesTaxon(Taxon taxon);

  boolean includesTaxon(Taxon taxon, Set<Status> excludingStatus);

  boolean isBuiltIn();
  
  boolean isSynthetic();

}
