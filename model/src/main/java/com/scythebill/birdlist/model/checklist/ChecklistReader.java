/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.checklist;

import java.io.Closeable;
import java.io.IOException;
import java.io.Reader;
import java.util.Map;
import java.util.logging.Logger;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxons;

/**
 * Reads a (split) hecklist from disk.
 */
class ChecklistReader implements Closeable {
  private final Reader reader;
  private final static Splitter SP_SPLITTER = Splitter.on('/');
  private final static Logger logger = Logger.getLogger(ChecklistReader.class.getName());
  final static ImmutableMap<String, Status> STATUS_TEXT;
  static {
    ImmutableMap.Builder<String, Status> builder = ImmutableMap.builder();
    for (Status status : Status.values()) {
      if (status.text() != null) {
        builder.put(status.text(), status);
      }
    }
    STATUS_TEXT = builder.build();
  }

  public ChecklistReader(Reader reader) {
    this.reader = reader;
  }

  public SplitChecklist read() throws IOException {
    ImportLines importLines = CsvImportLines.fromReader(reader);
    // Skip the header.  TODO: sanity check this line
    importLines.nextLine();
    
    Map<SightingTaxon, Status> taxa = Maps.newHashMap();
    
    while (true) {
      String[] nextLine = importLines.nextLine();
      if (nextLine == null) {
        break;
      }
      
      Status status = toStatus(nextLine[2]);
      String id = nextLine[1];
      if (id.indexOf('/') < 0) {
        taxa.put(SightingTaxons.newSightingTaxon(id), status);
      } else {
        taxa.put(SightingTaxons.newSpTaxon(SP_SPLITTER.split(id)), status);
      }
    }
    
    return new SplitChecklist(taxa);
  }
    
  private Status toStatus(String status) {
    if (Strings.isNullOrEmpty(status)) {
      return Status.NATIVE;
    }
    Status statusEnum = STATUS_TEXT.get(status);
    if (statusEnum == null) {
      logger.warning("Unexpected checklist status " + status);
      return Status.NATIVE;
    }
    return statusEnum;
  }

  @Override
  public void close() throws IOException {
    reader.close();
  }
}
