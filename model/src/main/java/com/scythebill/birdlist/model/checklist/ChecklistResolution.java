/*   
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.checklist;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Type;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Runs taxonomic resolution using checklist data.
 */
public class ChecklistResolution {
  private final Checklists checklists;
  private final ImmutableSet<Status> excludingStatuses;
  private List<Sighting> sightingsToRemove;
  private List<Sighting> sightingsToAdd;
  private Multimap<SightingTaxon, SightingTaxon> taxaMappings;
  private int simplifiedButStillSp;
  private int notSimplified;

  public ChecklistResolution(
      Checklists checklists,
      Set<Checklist.Status> excludingStatuses) {
    this.checklists = checklists;
    this.excludingStatuses = ImmutableSet.copyOf(excludingStatuses);
  }

  public List<Sighting> sightingsToRemove() {
    return Collections.unmodifiableList(sightingsToRemove);
  }

  public List<Sighting> sightingsToAdd() {
    return Collections.unmodifiableList(sightingsToAdd);
  }
  
  public Multimap<SightingTaxon, SightingTaxon> taxaMappings() {
    return taxaMappings;
  }

  public int notSimplifiedCount() {
    return notSimplified;
  }
  
  public int simplifiedButNotFullyResolved() {
    return simplifiedButStillSp;
  }
  
  public ImmutableSet<SightingTaxon> attemptChecklistResolution(
      ReportSet reportSet,
      Set<SightingTaxon> spsToResolve,
      Taxonomy taxonomy) {
    if (!taxonomy.isBuiltIn()) {
      throw new IllegalArgumentException("Only built-in taxonomies");
    }
    
    sightingsToRemove = Lists.newArrayList();
    sightingsToAdd = Lists.newArrayList();
    simplifiedButStillSp = 0;
    notSimplified = 0;
    
    boolean iocResolution = taxonomy instanceof MappedTaxonomy;
    
    ImmutableMultimap.Builder<SightingTaxon, SightingTaxon> taxaMappingsBuilder =
        ImmutableMultimap.builder();
    
    // Keep track of the sighting taxa that still need to be resolved.
    Set<SightingTaxon> remainingSightingTaxons = Sets.newHashSet();
    for (Sighting sighting : reportSet.getSightings()) {
      SightingTaxon oldSightingTaxon = sighting.getTaxon();
      if (spsToResolve.contains(oldSightingTaxon)) {
        Location location = reportSet.getLocations().getLocation(sighting.getLocationId());
        Checklist checklist = checklists.getNearestBuiltInChecklist(
            taxonomy, reportSet, location);
        
        // No checklist.  Nothing we can do!
        if (checklist == null) {
          remainingSightingTaxons.add(oldSightingTaxon);
          notSimplified++;
          continue;
        }
        
        SightingTaxon newSightingTaxon =
            resolveAgainstChecklist(taxonomy, iocResolution, oldSightingTaxon, checklist);

        if (newSightingTaxon != null) {
          // If the preferred taxonomy is IOC, this *might* not be a simplification.  Compare
          // the ID size when mapped to IOC, and don't bother if it didn't make things better
          boolean isStillSp;
          if (iocResolution) {
            Resolved originalMapped = oldSightingTaxon.resolve(taxonomy);
            Resolved newMapped = newSightingTaxon.resolve(taxonomy);
            if (originalMapped.getSightingTaxon().getIds().size() <
                newMapped.getSightingTaxon().getIds().size()) {
              notSimplified++;
              remainingSightingTaxons.add(oldSightingTaxon);
              continue;
            }
            isStillSp = newMapped.getType() == Type.SP;
          } else {
            isStillSp = newSightingTaxon.getType() == Type.SP;
          }
          
          taxaMappingsBuilder.put(oldSightingTaxon, newSightingTaxon);
          sightingsToRemove.add(sighting);
          sightingsToAdd.add(sighting.asBuilder().setTaxon(newSightingTaxon).build());
          if (isStillSp) {
            simplifiedButStillSp++;
            remainingSightingTaxons.add(newSightingTaxon);
          }
        } else {
          taxaMappingsBuilder.put(oldSightingTaxon, oldSightingTaxon);
          remainingSightingTaxons.add(oldSightingTaxon);
          notSimplified++;
        }
      }
    }

    taxaMappings = taxaMappingsBuilder.build();

    // Nothing found that is simpler, bail
    if (sightingsToAdd.isEmpty()) {
      return null;
    }
    
    return ImmutableSet.copyOf(remainingSightingTaxons);
  }

  public SightingTaxon resolveAgainstChecklist(Taxonomy taxonomy, boolean iocResolution,
      SightingTaxon oldSightingTaxon, Checklist checklist) {
    SightingTaxon newSightingTaxon = null;
    
    if (iocResolution) {
      // An IOC upgrade: map to each IOC taxon, then check the status in the IOC checklist
      Resolved originalMapped = oldSightingTaxon.resolve(taxonomy);
      if (originalMapped.getType() == Type.SP) {
        Set<String> plausibleIds = Sets.newHashSet();
        for (Taxon taxon : originalMapped.getTaxa()) {
          if (iocTaxonIsInIocChecklist(checklist, taxon)) {
            plausibleIds.add(taxon.getId());
          }
        }
        
        // If anything mapped, and not *everything* mapped, then this looks like a simplification.
        if (!plausibleIds.isEmpty()
            && plausibleIds.size() != originalMapped.getSightingTaxon().getIds().size()) {
          // Map the sighting taxon back to Clements
          newSightingTaxon = ((MappedTaxonomy) taxonomy).getMapping(
              SightingTaxons.newPossiblySpTaxon(plausibleIds));
        }
      }
    } else {
      // A Clements upgrade: check the status of the Clements taxon in just the Clements checklists.
      // This upgrade *used* to use the IOC checklists, but now that the IOC checklists are entirely
      // separate this might lead to some significant problems.
      Set<String> plausibleIds = Sets.newHashSet();
      for (String id : oldSightingTaxon.getIds()) {
        if (clementsTaxonIsInClementsChecklist(taxonomy, checklist, id)) {
          plausibleIds.add(id);
        }
      }
      
      if (plausibleIds.size() != oldSightingTaxon.getIds().size()
          && !plausibleIds.isEmpty()) {
        newSightingTaxon = SightingTaxons.newPossiblySpTaxon(plausibleIds);
      }
    }
    return newSightingTaxon;
  }
            
  private boolean clementsTaxonIsInClementsChecklist(
      Taxonomy clements, Checklist checklist, String id) {
    // Raise the taxon to a species, and then see if it matches
    Taxon taxon = TaxonUtils.getParentOfType(clements.getTaxon(id), Taxon.Type.species);
    return checklist.includesTaxon(taxon, excludingStatuses);
  }

  private boolean iocTaxonIsInIocChecklist(Checklist checklist, Taxon taxon) {
    // Raise the taxon to a species, and then see if it matches
    Taxon species = TaxonUtils.getParentOfType(taxon, Taxon.Type.species);
    return checklist.includesTaxon(species, excludingStatuses);
  }
}
