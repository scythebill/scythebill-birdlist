/*   
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.checklist;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.google.common.base.Objects;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Streams;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * A slower checklist synthesizer which directly merges checklists
 * without using the transposed checklists, and does not support
 * real computation of endemic status.
 */
public class ChecklistSynthesizer {
  private final static Logger logger = Logger.getLogger(ChecklistSynthesizer.class.getName());
  private final ImmutableMap<Status, Integer> STATUS_PRIORITY =
      ImmutableMap.<Status, Integer>builder()
          .put(Status.EXTINCT, 0)
          .put(Status.ESCAPED, 1)
          .put(Status.RARITY_FROM_INTRODUCED, 2)
          .put(Status.INTRODUCED, 3)
          .put(Status.RARITY, 4)
          .put(Status.NATIVE, 5)
          .put(Status.ENDEMIC, 6)
          .build();
      
  static final class CacheKey {
    final ImmutableSet<String> locationCodes;
    final Taxonomy taxonomy;
    private final int hash;
    
    CacheKey(Iterable<String> locationCodes, Taxonomy taxonomy) {
      this.locationCodes = ImmutableSet.copyOf(locationCodes);
      this.taxonomy = taxonomy;
      this.hash = Objects.hashCode(this.locationCodes, taxonomy.getId());
    }
    
    @Override
    public int hashCode() {
      return hash;
    }
    
    @Override
    public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      
      if (!(o instanceof CacheKey)) {
        return false;
      }
      
      CacheKey that = (CacheKey) o;
      return that.taxonomy == taxonomy
          && that.locationCodes.equals(locationCodes);
    }
  }
  
  private final LoadingCache<CacheKey, Checklist> cache = CacheBuilder.newBuilder()
      .maximumSize(10)
      .build(new CacheLoader<CacheKey, Checklist>() {
        @Override
        public Checklist load(CacheKey key) throws Exception {
          return createChecklist(key.locationCodes, key.taxonomy);
        }
      });
  
  private final Checklists checklists;
  private final ReportSet reportSet;

  @Inject
  public ChecklistSynthesizer(
      Checklists checklists,
      ReportSet reportSet) {
    this.checklists = checklists;
    this.reportSet = reportSet;
  }
  
  
  public Checklist synthesizeChecklist(Taxonomy taxonomy, Iterable<Location> locations) {
    ImmutableSet<String> locationIds =
        Streams.stream(locations).map(Location::getId).collect(ImmutableSet.toImmutableSet());
    return cache.getUnchecked(new CacheKey(locationIds, taxonomy));
  }

  private Checklist createChecklist(ImmutableSet<String> locationCodes, Taxonomy taxonomy) {
    Map<SightingTaxon, Status> taxonToStatus = new LinkedHashMap<>(); 
    for (String locationCode : locationCodes) {
      Location location = reportSet.getLocations().getLocation(locationCode);
      if (location == null) {
        logger.warning("Could not find location " + locationCode);
        return null;
      }
      
      Checklist checklist = checklists.getChecklist(reportSet, taxonomy, location);
      if (checklist == null) {
        return null;
      }

      ImmutableSet<SightingTaxon> taxa = checklist.getTaxa(taxonomy);
      for (SightingTaxon taxon : taxa) {
        Status newStatus = checklist.getStatus(taxonomy, taxon);
        Status oldStatus = taxonToStatus.get(taxon);
        if (oldStatus == null
            || STATUS_PRIORITY.get(newStatus) > STATUS_PRIORITY.get(oldStatus)) {
          taxonToStatus.put(taxon, newStatus);
        }
      }
    }
    
    return new ClementsChecklist(taxonToStatus);
  }
}
