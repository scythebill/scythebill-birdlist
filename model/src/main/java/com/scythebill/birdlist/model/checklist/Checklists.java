/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.checklist;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Pattern;

import javax.annotation.Nullable;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.io.CharSource;
import com.google.common.io.Resources;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Location.Type;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Class for loading checklists.
 */
public class Checklists {
  private static final String CSV_SUFFIX = ".csv";
  private static final String CHECKLISTS_PATH = "com/scythebill/birdlist/model/checklist/";
  private static final String CLEMENTS_CHECKLISTS_PATH = CHECKLISTS_PATH + "clements/";
  private final ImmutableSet<String> checklistCodes;
  private final LoadingCache<String, Checklist> checklistCache;
  private final TransposedChecklistSynthesizer transposedSynthesizer;
  // TODO: move these entirely out of Checklists, as a result of cleaning up the final
  // functions that use them from this file.
  private final static ImmutableMultimap<String, String> SYNTHETIC_CHECKLISTS = ImmutableMultimap.<String, String>builder()
      .putAll("GB", "GB-ENG", "GB-NIR", "GB-SCT", "GB-WLS")
      .putAll("SH", "SH-AC", "SH-SH", "SH-TA")
      .build();
  private final static ImmutableMultimap<String, String> SYNTHETIC_US_CHECKLISTS = ImmutableMultimap.<String, String>builder()
      .putAll("West Indies", "PR", "VI")
      .putAll("Pacific Ocean", "US-HI", "GU", "MP", "AS")
      .build();
  private final static ImmutableMap<String, ImmutableSet<String>> SUPPORTED_REGIONS_WITH_EXCLUSIONS = ImmutableMap.<String, ImmutableSet<String>>builder()
      // Exclude Turkey from Europe - it still has a unified list for Europe/Asia, and we don't want the Asian species
      .put("Europe", ImmutableSet.of("TR"))
      .put("Asia", ImmutableSet.of())
      .put("Eurasia", ImmutableSet.of())
      .put("Africa", ImmutableSet.of())
      .put("Australasia", ImmutableSet.of())
      .put("South Polar Region", ImmutableSet.of())
      .put("Central America", ImmutableSet.of())
      .put("North America", ImmutableSet.of())
      .put("South America", ImmutableSet.of())
      .put("West Indies", ImmutableSet.of())
      .put("Indian Ocean", ImmutableSet.of())
      .put("Pacific Ocean", ImmutableSet.of())
      .put("Atlantic Ocean", ImmutableSet.of())
      .put("Arctic Ocean", ImmutableSet.of())
      .build();

  @Inject
  public Checklists(TransposedChecklistSynthesizer transposedSynthesizer) {
    this.transposedSynthesizer = transposedSynthesizer;
    List<String> checklistFiles = enumerateChecklistFiles();
    ImmutableSet.Builder<String> checklistCodes = ImmutableSet.builder();
    for (String checklistFile : checklistFiles) {
      String code = checklistFile.substring(0, checklistFile.length() - CSV_SUFFIX.length());
      checklistCodes.add(code);
    }
    // Add all the automatic synthetic checklists.
    if (transposedSynthesizer != null) {
      checklistCodes.addAll(SYNTHETIC_CHECKLISTS.keySet());
    }
    
    this.checklistCodes = checklistCodes.build();
    checklistCache = CacheBuilder.newBuilder()
        .maximumSize(100)
        .build(new CacheLoader<String, Checklist>() {
          @Override
          public Checklist load(String code) throws Exception {
            return loadChecklist(code);
          }
        });
  }
  
  public boolean hasChecklist(Taxonomy taxonomy, ReportSet reportSet, Location location) {
    if (!taxonomy.isBuiltIn()) {
      ExtendedTaxonomyChecklists checklists = reportSet.getExtendedTaxonomyChecklist(taxonomy.getId());
      if (checklists == null || checklists.getChecklists().isEmpty()) {
        return false;
      }

      String code = getChecklistLocationCode(location);
      if (code != null) {
        return checklists.getChecklists().containsKey(code);
      } else {
        Collection<String> transposedChecklistLocations = transposedSynthesizer.getTransposedChecklistLocations(location);
        return transposedChecklistLocations != null;
      }
    } else {
      if (reportSet.getChecklist(location) != null) {
        return true;
      }
      
      if ("US".equals(location.getEbirdCode())) {
        return true;
      }
  
      if (location.isBuiltInLocation()
          && location.getType() == Location.Type.region
          && SUPPORTED_REGIONS_WITH_EXCLUSIONS.containsKey(location.getModelName())) {
        return true;
      }
      
      String code = getChecklistLocationCode(location);
      if (code != null) {
        return checklistCodes.contains(code);
      } else {
        Collection<String> transposedChecklistLocations = transposedSynthesizer.getTransposedChecklistLocations(location);
        return transposedChecklistLocations != null;
      }
    }
  }

  @Nullable
  public Checklist getChecklist(ReportSet reportSet, Taxonomy taxonomy, Location location) {
    if (!taxonomy.isBuiltIn()) {
      ExtendedTaxonomyChecklists checklists = reportSet.getExtendedTaxonomyChecklist(taxonomy.getId());
      if (checklists == null || checklists.getChecklists().isEmpty()) {
        return null;
      }
      if (location.isBuiltInLocation()) {
        String code = getChecklistLocationCode(location);
        if (code != null) {
          return checklists.getChecklists().get(code);
        }
        Collection<String> transposedChecklistLocations = transposedSynthesizer.getTransposedChecklistLocations(location);
        if (transposedChecklistLocations != null) {
          return transposedSynthesizer.synthesizeChecklistInternal(reportSet, transposedChecklistLocations);
        }
      }
      
      return null;
    }

    // First check - is this something that's always handled by transposed checklists?
    Collection<String> transposedChecklistLocations = transposedSynthesizer.getTransposedChecklistLocations(location);
    if (transposedChecklistLocations != null) {
      return transposedSynthesizer.synthesizeChecklistInternal(reportSet, transposedChecklistLocations);
    }

    String locationCode = getChecklistLocationCode(location);    
    // Second check - nothing built-in?  See if there's something custom.
    if (locationCode == null || !checklistCodes.contains(locationCode)) {
      return reportSet.getChecklist(location);
    }
    
    // Finally, return a built-in checklist (if one exists)
    return checklistCache.getUnchecked(locationCode);
  }
  
  /** Returns the best built-in checklist for a given location. */
  @Nullable
  public Checklist getNearestBuiltInChecklist(Taxonomy taxonomy,
      @Nullable ReportSet reportSet, Location location) {
    if (!taxonomy.isBuiltIn()) {
      if (reportSet == null) {
        return null;
      }
      while (location != null) {
        Checklist checklist = getChecklist(reportSet, taxonomy, location);
        if (checklist != null) {
          return checklist;
        }
        location = location.getParent();
      }
      return null;
    } else {
      while (location != null) {
        String locationCode = getChecklistLocationCode(location);
        if (locationCode != null && checklistCodes.contains(locationCode)) {
          // Synthesize if needed...
          if (SYNTHETIC_CHECKLISTS.containsKey(locationCode)) {
            return transposedSynthesizer.synthesizeChecklistInternal(reportSet,
                SYNTHETIC_CHECKLISTS.get(locationCode));
          }
          
          // ... or return the raw checklist
          return checklistCache.getUnchecked(locationCode);
        }
        location = location.getParent();
      }
      return null;
    }
  }
  
  /** Returns the best built-in checklist for a given location. */
  @Nullable
  public String getLocationWithNearestBuiltInChecklist(Location location) {
    while (location != null) {
      String locationCode = getChecklistLocationCode(location);
      if (locationCode != null && checklistCodes.contains(locationCode)) {
        return locationCode;
      }
      location = location.getParent();
    }
    return null;
  }

  /** Returns all checklist codes.  Used for testing. */
  public ImmutableSet<String> checklistCodes() {
    return checklistCodes;
  }
  
  /** Returns a checklist by code.  Used for testing. */
  public Checklist getChecklist(String code) {
    return checklistCache.getUnchecked(code);
  }

  public static Collection<String> getChecklistLocationCodes(Location location) {
    String locationCode = getChecklistLocationCode(location);
    if (locationCode == null) {
      if ("US".equals(location.getEbirdCode())) {
        if (SYNTHETIC_US_CHECKLISTS.containsKey(location.getParent().getModelName())) {
          return SYNTHETIC_US_CHECKLISTS.get(location.getParent().getModelName());
        }
      }
      
      if ("BQ".equals(location.getEbirdCode())
          && "South America".equals(location.getParent().getModelName())) {
        return ImmutableSet.of("BQ-BO", "AW", "CW");
      }
      
      if ("UM".equals(location.getEbirdCode())) {
        return ImmutableSet.of("UM-71");
      }
      
      return ImmutableSet.of();
    } else {
      if (SYNTHETIC_CHECKLISTS.containsKey(locationCode)) {
        return SYNTHETIC_CHECKLISTS.get(locationCode);
      }
            
      return ImmutableSet.of(locationCode);
    }
  }

  public static String getChecklistLocationCode(Location location) {
    String code = Locations.getLocationCode(location);
    if ("US".equals(code) && !"North America".equals(location.getParent().getModelName())) {
      // The "US" checklist is really the North American US only - don't return it for Hawaii/West Indies, etc.
      return null;
    }
    
    if ("BQ".equals(code) && !"West Indies".equals(location.getParent().getModelName())) {
      // The Caribbean Netherlands checklist is for the West Indies only - each of Aruba, Bonaire,
      // and Curaçao has its own checklist.
      return null;
    }
    
    // No checklist for the US Minor Outlying Islands as a whole
    if ("UM".equals(code)) {
      return null;
    }
    
    // When modifying this code, see also ExtendedTaxonomyChecklists
    if ("RU".equals(code) || "ID".equals(code) || "XX".equals(code)) {
      Location builtInParentRegion = null;
      Location parent = location.getParent();
      while (builtInParentRegion == null && parent != null) {
        if (parent.isBuiltInLocation() && parent.getType() == Location.Type.region) {
          builtInParentRegion = parent;
          break;
        }
        parent = parent.getParent();
      }
      
      if (builtInParentRegion != null) {
        String parentName = builtInParentRegion.getModelName();
        return code + "-" + parentName;
      }
      
      return null;
    }
    
    return code;
  }
  
  private Checklist loadChecklist(String code) {
    String clementsFile = String.format("clements/%s.csv", code);
    URL clementsUrl = Resources.getResource(getClass(), clementsFile);
    CharSource clementsCharSource = Resources.asCharSource(clementsUrl, StandardCharsets.UTF_8);

    String iocFile = String.format("ioc/%s.csv", code);
    URL iocUrl = Resources.getResource(getClass(), iocFile);
    CharSource iocCharSource = Resources.asCharSource(iocUrl, StandardCharsets.UTF_8);

    return new ClementsAndIocChecklist(code, clementsCharSource, iocCharSource);
  }
  
  private final static Pattern IRELAND_AND_UK_COUNTIES = Pattern.compile(
      "^(?:GB-\\w\\w\\w-)|(?:IE-)");
  
  private List<String> enumerateChecklistFiles() {
    List<String> files = Lists.newArrayList();
    URL location = getClass().getProtectionDomain().getCodeSource().getLocation();
    String path = location.getPath();
    
    if (path.endsWith(".jar")) {
      // JAR file (running in the real app)
      try {
        String decodedPath = URLDecoder.decode(path, "UTF-8");
        JarFile jarFile = new JarFile(decodedPath);
        try {
          Enumeration<JarEntry> entries = jarFile.entries();
          while (entries.hasMoreElements()) {
            JarEntry entry = entries.nextElement();
            // Could check both Clements and IOC... but the code assumes that
            // both are the same.
            if (entry.getName().startsWith(CLEMENTS_CHECKLISTS_PATH)
                && entry.getName().endsWith(CSV_SUFFIX)) {
              String name = entry.getName().substring(CLEMENTS_CHECKLISTS_PATH.length());
              if (!IRELAND_AND_UK_COUNTIES.matcher(name).find()) {
                files.add(name);
              }
            }
          }
        } finally {
          jarFile.close();
        }
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    } else {
      // Files - developing
      File root = new File(path);
      File checklistsDir = new File(root, CLEMENTS_CHECKLISTS_PATH);
      files.addAll(Arrays.asList(checklistsDir.list(new FilenameFilter() {
        @Override
        public boolean accept(File file, String name) {
          return name.endsWith(CSV_SUFFIX) 
              && !IRELAND_AND_UK_COUNTIES.matcher(name).find();
        }
      })));
    }
    return files;
  }

  public static ImmutableSet<String> gatherCountryChecklistCodes(Location parent) {
    ImmutableSet.Builder<String> codes = ImmutableSet.builder();
    gatherCountryChecklistCodes(parent, codes);
    return codes.build();
  }

  /**
   * Gathers country codes.  Does not collect location codes
   * for anything within a country (including country-in-country).
   */
  private static void gatherCountryChecklistCodes(
      Location parent, ImmutableSet.Builder<String> codes) {
    for (Location child : parent.contents()) {
      if (child.isBuiltInLocation() && child.getType() != Type.region) {
        codes.addAll(Checklists.getChecklistLocationCodes(child));
      } else {
        String code = child.getEbirdCode();
        // Include High Seas entries
        if (code != null && code.startsWith("XX-")) {
          codes.add(code);
        } else {
          gatherCountryChecklistCodes(child, codes);
        }
      }
    }
  }
}
