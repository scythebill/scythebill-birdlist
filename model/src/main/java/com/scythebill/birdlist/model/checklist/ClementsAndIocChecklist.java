/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.checklist;

import java.io.IOException;
import java.util.Set;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableSet;
import com.google.common.io.CharSource;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Checklist class that maintains both a Clements and an IOC copy.
 */
class ClementsAndIocChecklist implements Checklist {
  private Supplier<SplitChecklist> clements;
  private Supplier<SplitChecklist> ioc;
  @SuppressWarnings("unused")
  private final String file;

  ClementsAndIocChecklist(
      String file,
      CharSource clementsSource,
      CharSource iocSource) {
    this.file = file;
    this.clements = Suppliers.memoize(() -> loadChecklist(clementsSource));
    this.ioc = Suppliers.memoize(() -> loadChecklist(iocSource));
  }

  @Override
  public ImmutableSet<SightingTaxon> getTaxa(Taxonomy taxonomy) {
    return getSplitChecklist(taxonomy).getTaxa();
  }

  @Override
  public boolean includesTaxon(Taxon taxon) {
    // TODO: how to handle Clements groups, or other subspecies?
    // This is currently used by QueryResults to include results up front in location lists, 
    // where this is irrelevant.
    return getTaxa(taxon.getTaxonomy())
        .contains(SightingTaxons.newSightingTaxon(taxon.getId()));
  }

  @Override
  public boolean includesTaxon(Taxon taxon, Set<Status> excludingStatuses) {
    Status status = getStatus(taxon.getTaxonomy(), SightingTaxons.newSightingTaxon(taxon.getId()));
    if (status == null) {
      return false;
    }
    
    return !excludingStatuses.contains(status);
  }

  @Override
  public Status getStatus(Taxonomy taxonomy, SightingTaxon taxon) {
    return getSplitChecklist(taxonomy).getStatus(taxon);
  }

  @Override
  public ImmutableSet<SightingTaxon> getTaxa(Taxonomy taxonomy, Status status) {
    return getSplitChecklist(taxonomy).getTaxa(status);
  }

  @Override
  public boolean isBuiltIn() {
    return true;
  }
  
  @Override
  public boolean isSynthetic() {
    return false;
  }

  private SplitChecklist loadChecklist(CharSource source) {
    try (ChecklistReader reader = new ChecklistReader(source.openStream())) {
      return reader.read();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private SplitChecklist getSplitChecklist(Taxonomy taxonomy) {
    if (taxonomy instanceof MappedTaxonomy) {
      return ioc.get();
    } else {
      return clements.get();
    }
  }
}
