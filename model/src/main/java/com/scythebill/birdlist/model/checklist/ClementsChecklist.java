/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.checklist;

import java.util.Map;
import java.util.Set;

import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Checklist class that maintains groups/ssps/sp's in the base
 * Clements taxonomy, and produces Clements/IOC species lists on demand.
 */
public class ClementsChecklist implements Checklist {
  private final ImmutableMap<SightingTaxon, Status> clementsBase;
  private ImmutableMap<SightingTaxon, Status> cachedIoc;
  private ImmutableMap<SightingTaxon, Status> cachedClements;

  public ClementsChecklist(
      Map<SightingTaxon, Status> clements) {
    this.clementsBase = ImmutableMap.copyOf(clements);
  }
  
  public ImmutableMap<SightingTaxon, Status> baseChecklistMap() {
    return clementsBase;
  }

  @Override
  public ImmutableSet<SightingTaxon> getTaxa(Taxonomy taxonomy) {
    ImmutableMap<SightingTaxon, Status> map = getTaxaMap(taxonomy);    
    return map.keySet();
  }

  @Override
  public boolean includesTaxon(Taxon taxon) {
    // TODO: how to handle Clements groups, or other subspecies?
    // This is currently used by QueryResults to include results up front in location lists, 
    // where this is irrelevant.
    return getTaxa(taxon.getTaxonomy())
        .contains(SightingTaxons.newSightingTaxon(taxon.getId()));
  }

  @Override
  public boolean includesTaxon(Taxon taxon, Set<Status> excludingStatuses) {
    Status status = getStatus(taxon.getTaxonomy(), SightingTaxons.newSightingTaxon(taxon.getId()));
    if (status == null) {
      return false;
    }
    
    return !excludingStatuses.contains(status);
  }

  @Override
  public Status getStatus(Taxonomy taxonomy, SightingTaxon taxon) {
    // For single-with-secondary-subspecies, check the species itself
    if (taxon.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
      taxon = SightingTaxons.newSightingTaxon(taxon.getId());
    }
    ImmutableMap<SightingTaxon, Status> map = getTaxaMap(taxonomy);
    return map.get(taxon);
  }

  @Override
  public ImmutableSet<SightingTaxon> getTaxa(Taxonomy taxonomy, Status status) {
    Map<SightingTaxon, Status> map = getTaxaMap(taxonomy);    
    return ImmutableSet.copyOf(
        Maps.filterValues(map, Predicates.equalTo(status)).keySet());
  }

  @Override
  public boolean isBuiltIn() {
    return false;
  }

  @Override
  public boolean isSynthetic() {
    return false;
  }

  private ImmutableMap<SightingTaxon, Status> getTaxaMap(Taxonomy taxonomy) {
    if (taxonomy instanceof MappedTaxonomy) {
      if (cachedIoc == null) {
        cachedIoc = createTaxaMap(taxonomy);
      }
      return cachedIoc;
    } else {
      if (cachedClements == null) {
        cachedClements = createTaxaMap(taxonomy);
      }
      return cachedClements;
    }
  }
  
  private ImmutableMap<SightingTaxon, Status> createTaxaMap(Taxonomy taxonomy) {
    Map<SightingTaxon, Status> builder = Maps.newHashMap();
    for (Map.Entry<SightingTaxon, Status> baseEntry : clementsBase.entrySet()) {
      Resolved resolve = baseEntry.getKey().resolve(taxonomy);
      SightingTaxon species = resolve.getParentOfAtLeastType(Taxon.Type.species);
      // Split out "sp's"
      if (species.getType() == SightingTaxon.Type.SP) {
        for (String id : species.getIds()) {
          builder.put(SightingTaxons.newSightingTaxon(id), baseEntry.getValue());
        }
      } else {
        builder.put(species, baseEntry.getValue());
      }
    }
    return ImmutableMap.copyOf(builder);
  }
}
