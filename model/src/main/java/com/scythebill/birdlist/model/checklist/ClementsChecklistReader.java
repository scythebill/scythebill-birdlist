/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.checklist;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.TaxonomyMappings;
import com.scythebill.birdlist.model.taxa.UpgradeTracker;

/** Reads a ClementsChecklist as a CSV file. */
public class ClementsChecklistReader {
  private final Taxonomy clements;
  private final PredefinedLocations predefinedLocations;
  private final TaxonomyMappings taxonomyMappings;

  /** Return type encapsulating a checklist and a location. */
  static public class ChecklistAndLocation {
    public final Checklist checklist;
    public final Location location;
    ChecklistAndLocation(Checklist checklist, Location location) {
      this.checklist = checklist;
      this.location = location;
    }
  }
  
  /** Exception thrown indicating the taxonomy is not valid. */
  public class InvalidTaxonomyException extends Exception {
  }
  
  public ClementsChecklistReader(
      Taxonomy clements,
      PredefinedLocations predefinedLocations,
      TaxonomyMappings taxonomyMappings) {
    this.clements = clements;
    this.predefinedLocations = predefinedLocations;
    this.taxonomyMappings = taxonomyMappings;
  }
  
  /** Reads a checklist and location from a CSV import file. */
  public ChecklistAndLocation read(
      ReportSet reportSet,
      File file) throws IOException, InvalidTaxonomyException  {
    ImportLines importLines = null;
    try {
      importLines = CsvImportLines.fromFile(file, Charsets.UTF_8);
      String[] locationLine = importLines.nextLine();
      if (locationLine == null || locationLine.length == 0) {
        throw new IllegalArgumentException("No location");
      }
      
      String[] headerLine = importLines.nextLine();
      if (headerLine == null || headerLine.length == 0) {
        throw new IllegalArgumentException("No header");
      }
      
      if (headerLine.length != 4
          || !"Status".equals(headerLine[3])) {
        throw new IllegalArgumentException("Invalid checklist file");
      }
      
      UpgradeTracker tracker = null;
      String taxonomy = headerLine[0];
      if (!clements.getId().equals(taxonomy)) {
        tracker = taxonomyMappings.getTracker(taxonomy, null /* no mapped taxonomy here */);
      }
      
      // Get the location from the location line
      Location location = getLocation(reportSet, locationLine);
      // And now the checklist from each line
      Map<SightingTaxon, Status> checklistMap = Maps.newHashMap();
      while (true) {
        String[] nextLine = importLines.nextLine();
        if (nextLine == null) {
          break;
        }
        
        String ids = nextLine[0];
        SightingTaxon taxon;
        if (ids.contains("/")) {
          Iterable<String> split = Splitter.on('/').split(ids);
          // Need to upgrade:  send the sp. through the tracker 
          // TODO: this really should be checklist-based 
          if (tracker != null) {
            taxon = SightingTaxons.newPossiblySpTaxon(
                Lists.newArrayList(tracker.getTaxonId(clements, split, false)));
          } else {
            taxon = SightingTaxons.newSpTaxon(split);
          }
        } else {
          // Need to upgrade:  send the taxon through the upgrader, and verify against a checklist
          // (not for the current location, but the parent)
          if (tracker != null) {
            taxon = tracker.getChecklistResolvedTaxonId(clements, ids, location.getParent());
          } else {
            taxon = SightingTaxons.newSightingTaxon(ids);
          }
        }
        
        // Verify the taxon is legit and can be resolved
        taxon.resolve(clements);
        
        Status status = ChecklistReader.STATUS_TEXT.get(nextLine[3]);
        if (status == null) {
          status = Status.NATIVE;
        }
        checklistMap.put(taxon, status);
      }
      
      return new ChecklistAndLocation(new ClementsChecklist(checklistMap), location);
    } finally {
      if (importLines != null) {
        importLines.close();
      }
    }
  }

  /**
   * Extract the Location from the location line, potentially creating it.
   * Note that this behaves... funkily... if the targets hierarchy doesn't match
   * this hierarchy.  Not sure if that's going to be a big problem in practice;
   * if people mostly create checklists for built-in locations or immediate
   * children of built-in locations, it should be OK.
   */
  public Location getLocation(ReportSet reportSet, String[] locationLine) {
    List<String> locationList = Lists.newArrayList(locationLine);
    Collections.reverse(locationList);
    String first = locationList.remove(0);
    Location location;
    if (first.startsWith(ClementsChecklistWriter.CODE_PREFIX)) {
      String code = minusPrefix(first, ClementsChecklistWriter.CODE_PREFIX);
      location = Locations.getLocationByCodePossiblyCreating(
          reportSet.getLocations(), predefinedLocations, code);
    } else if (first.startsWith(ClementsChecklistWriter.ID_PREFIX)) {
      String id = minusPrefix(first, ClementsChecklistWriter.ID_PREFIX);
      location = reportSet.getLocations().getLocation(id);
    } else {
      throw new IllegalArgumentException("Unexpected identifier in checklist import: " + first);
    }
    if (location == null) {
      throw new IllegalArgumentException("Could not find: " + first);
    }
    
    for (String locationElement : locationList) {
      List<String> list = Splitter.on(':').limit(2).splitToList(locationElement);
      if (list.size() != 2) {
        throw new IllegalArgumentException("Unexpected identifier in checklist import: " + locationElement);
      }
      
      String name = list.get(1);
      Location content = location.getContent(name);
      if (content != null) {
        location = content;
      } else {
        Location.Type type = ClementsChecklistWriter.LOCATION_TYPE_TO_PREFIX.inverse().get(list.get(0));
        location = Location.builder()
            .setParent(location)
            .setName(name)
            .setType(type)
            .build();
      }
    }
    return location;
  }

  private String minusPrefix(String text, String prefix) {
    return text.substring(prefix.length() + 1);
  }

  /** See if this file is a checklist file. */
  public static boolean isClementsChecklist(File file) {
    ImportLines importLines = null;
    try {
      importLines = CsvImportLines.fromFile(file, Charsets.UTF_8);
      String[] locationLine = importLines.nextLine();
      if (locationLine == null || locationLine.length == 0) {
        return false;
      }
      
      String[] headerLine = importLines.nextLine();
      if (headerLine == null || headerLine.length == 0) {
        return false;
      }
      
      if (headerLine.length != 4
          || !"Status".equals(headerLine[3])) {
        return false;
      }
      
      return true;
    } catch (IOException e) {
      return false;
    } finally {
      if (importLines != null) {
        try {
          importLines.close();
        } catch (IOException e) {
        }
      }
    }
  }
}
