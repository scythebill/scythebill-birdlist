/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.checklist;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;
import java.util.Map.Entry;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/** Writes a ClementsChecklist as a CSV file. */
public class ClementsChecklistWriter {
  static String CODE_PREFIX = "code";  
  static String ID_PREFIX = "id";
  static String PARK_PREFIX = "park";
  static String TOWN_PREFIX = "town";
  static String CITY_PREFIX = "city";
  static String COUNTY_PREFIX = "county";
  static String STATE_PREFIX = "state";
  static String COUNTRY_PREFIX = "country";
  static String REGION_PREFIX = "region";
  static String OTHER_PREFIX = "loc";
  static final ImmutableBiMap<Location.Type, String> LOCATION_TYPE_TO_PREFIX;
  static {
    ImmutableBiMap.Builder<Location.Type, String> builder = ImmutableBiMap.builder();
    builder.put(Location.Type.city, CITY_PREFIX);
    builder.put(Location.Type.country, COUNTRY_PREFIX);
    builder.put(Location.Type.county, COUNTY_PREFIX);
    builder.put(Location.Type.park, PARK_PREFIX);
    builder.put(Location.Type.region, REGION_PREFIX);
    builder.put(Location.Type.state, STATE_PREFIX);
    builder.put(Location.Type.town, TOWN_PREFIX);
    LOCATION_TYPE_TO_PREFIX = builder.build();
  }
  
  private Taxonomy clements;

  public ClementsChecklistWriter(
      Taxonomy clements) {
    this.clements = clements;
  }
  
  public void write(
      File file, ClementsChecklist checklist, Location location) throws IOException  {
    ExportLines export = null;
    try {
      Writer writer = new OutputStreamWriter(
          new FileOutputStream(file), Charsets.UTF_8);
      export = CsvExportLines.fromWriter(writer);
      
      List<String> locationDescription = Lists.newArrayList();
      Location current = Preconditions.checkNotNull(location);
      while (current != null) {
        // ebird code - terminate.
        if (current.getEbirdCode() != null) {
          locationDescription.add(CODE_PREFIX + ":" + Locations.getLocationCode(current));
          break;
        }
        
        // Other built-in locations - use the ID (second-best) and terminate
        if (current.isBuiltInLocation()) {
          locationDescription.add(ID_PREFIX + ":" + current.getId());
          break;
        }
        
        // Anything else - output the type and name
        String prefix = LOCATION_TYPE_TO_PREFIX.get(current.getType());
        if (prefix == null) {
          prefix = OTHER_PREFIX;
        }
        locationDescription.add(prefix + ":" + current.getModelName());
        
        current = current.getParent();
      }
      
      export.nextLine(locationDescription.toArray(new String[0]));
      
      export.nextLine(new String[]{clements.getId(), "Common", "Scientific", "Status"});
      for (Entry<SightingTaxon, Status> entry : checklist.baseChecklistMap().entrySet()) {
        SightingTaxon taxon = entry.getKey();
        Resolved resolved = taxon.resolve(clements);
        String[] nextLine = new String[] {
          Joiner.on('/').join(taxon.getIds()),
          resolved.getCommonName(),
          resolved.getFullName(),
          entry.getValue().text()
        };
        export.nextLine(nextLine);
      }
    } finally {
      if (export != null) {
        export.close();
      }
    }
  }
}
