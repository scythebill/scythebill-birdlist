/*   
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.checklist;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Table;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.xml.ExtendedTaxonomyParsing;

/**
 * Utilities to assist with handling checklist of extended taxonomies.
 */
public class ExtendedTaxonomyChecklists {
  private final ImmutableMap<String, Checklist> checklists;
  private final TransposedChecklist transposedChecklist;
  
  private ExtendedTaxonomyChecklists(
      Map<String, Checklist> checklists,
      TransposedChecklist transposedChecklist) {
    this.checklists = ImmutableMap.copyOf(checklists);
    this.transposedChecklist = transposedChecklist;
    
  }
  
  public ImmutableMap<String, Checklist> getChecklists() {
    return checklists;
  }
  
  public TransposedChecklist getTransposedChecklist() {
    return transposedChecklist;
  }

  /**
   * Builds checklists off of per-species lists of locations.
   */
  public static class Builder {
    /** Table of location code, taxon, and checklist status. */
    private final Table<String, SightingTaxon, Checklist.Status> table = HashBasedTable.create();

    /** Table of taxon, location code, and checklist status. */ 
    private final Table<SightingTaxon, String, Checklist.Status> transposed = HashBasedTable.create();
    
    public void addEntries(SightingTaxon taxon, Species.Status status, Collection<String> checklistIdsWithStatus) {
      boolean extinct = status == Species.Status.EX || status == Species.Status.EW;
      for (String checklistIdWithStatus : checklistIdsWithStatus) {
        var locationAndStatus = ExtendedTaxonomyParsing.locationAndStatus(checklistIdWithStatus);
        var checklistStatus = locationAndStatus.status();

        if (extinct) {
          checklistStatus = Checklist.Status.EXTINCT;
        } else if (checklistStatus == Checklist.Status.NATIVE && checklistIdsWithStatus.size() == 1) {
          // TODO: this is wrong, given checklists with states.  Endemic needs to take into account
          // the presence of parent and child checklists.
          checklistStatus = Checklist.Status.ENDEMIC;
        }
        
        // For Indonesia and Russia, automatically expand to
        // separate Asia/Australasia and Europe/Asia lists.
        switch (locationAndStatus.locationCode()) {
          case "ID":
            table.put("ID-Asia", taxon, checklistStatus);
            table.put("ID-Australasia", taxon, checklistStatus);
            transposed.put(taxon, "ID-Asia", checklistStatus);
            transposed.put(taxon, "ID-Australasia", checklistStatus);
            break;
          case "RU":
            transposed.put(taxon, "RU-Europe", checklistStatus);
            transposed.put(taxon, "RU-Asia", checklistStatus);
            table.put("RU-Europe", taxon, checklistStatus);
            table.put("RU-Asia", taxon, checklistStatus);
            break;
          default:
            table.put(locationAndStatus.locationCode(), taxon, checklistStatus);
            transposed.put(taxon, locationAndStatus.locationCode(), checklistStatus);
            break;
        }
      }
    }
    
    public ExtendedTaxonomyChecklists build() {
      ImmutableMap.Builder<String, Checklist> mapBuilder = ImmutableMap.builder();
      for (String checklistId : table.rowKeySet()) {
        mapBuilder.put(checklistId, new ExtendedTaxonomyChecklist(table.row(checklistId)));
      }
      
      return new ExtendedTaxonomyChecklists(mapBuilder.build(),
          new ExtendedTaxonomyTransposedChecklist(transposed));
    }
  }
  
  /** The TransposedChecklist for an extended taxonomy. */
  static class ExtendedTaxonomyTransposedChecklist implements TransposedChecklist {
    private final ImmutableTable<SightingTaxon, String, Checklist.Status> transposed;
    
    ExtendedTaxonomyTransposedChecklist(Table<SightingTaxon, String, Checklist.Status> transposed) {
      this.transposed = ImmutableTable.copyOf(transposed);
    }

    @Override
    public Iterable<String> locationsWithStatuses(Taxon species, ImmutableSet<Status> statusSet) {
      SightingTaxon taxon = SightingTaxons.newSightingTaxon(species.getId());
      if (!transposed.containsRow(taxon)) {
        return ImmutableSet.of();
      }
      return transposed.row(taxon)
          .keySet()
          .stream()
          .filter(
              locationId -> statusSet.contains(transposed.get(taxon, locationId)))
          .collect(ImmutableSet.toImmutableSet());
    }

    @Override
    public Iterable<String> locationsWithStatuses(String speciesId,
        ImmutableSet<Status> statusSet) {
      SightingTaxon taxon = SightingTaxons.newSightingTaxon(speciesId);
      if (!transposed.containsRow(taxon)) {
        return ImmutableSet.of();
      }
      return transposed.row(taxon)
          .keySet()
          .stream()
          .filter(
              locationId -> statusSet.contains(transposed.get(taxon, locationId)))
          .collect(ImmutableSet.toImmutableSet());
    }
  }
  
  /**
   * A checklist for an extended taxonomy.
   */
  static class ExtendedTaxonomyChecklist implements Checklist {
    private final ImmutableMap<SightingTaxon, Status> taxa;

    public ExtendedTaxonomyChecklist(Map<SightingTaxon, Status> taxa) {
      this.taxa = ImmutableMap.copyOf(taxa);
    }

    @Override
    public ImmutableSet<SightingTaxon> getTaxa(Taxonomy taxonomy) {
      return taxa.keySet();
    }

    @Override
    public ImmutableSet<SightingTaxon> getTaxa(Taxonomy taxonomy, Status status) {
      return taxa.entrySet().stream()
          .filter(e -> (e.getValue() == status))
          .map(Map.Entry::getKey).collect(ImmutableSet.toImmutableSet());
    }

    @Override
    public Status getStatus(Taxonomy taxonomy, SightingTaxon taxon) {
      return taxa.get(taxon);
    }

    @Override
    public boolean includesTaxon(Taxon taxon) {
      return taxa.containsKey(SightingTaxons.newSightingTaxon(taxon.getId()));
    }

    @Override
    public boolean includesTaxon(Taxon taxon, Set<Status> excludingStatus) {
      Status status = taxa.get(SightingTaxons.newSightingTaxon(taxon.getId()));
      if (status == null) {
        return false;
      }
      
      return !excludingStatus.contains(status);
    }

    @Override
    public boolean isBuiltIn() {
      return true;
    }

    @Override
    public boolean isSynthetic() {
      return false;
    }    
  }
}
