/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.checklist;

import java.util.Map;

import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxons;

/**
 * Class that maintains a checklist for just one taxonomy.
 */
final class SplitChecklist {
  private final ImmutableMap<SightingTaxon, Status> taxa;

  SplitChecklist(Map<SightingTaxon, Status> taxa) {
    this.taxa = ImmutableMap.copyOf(taxa);
  }

  ImmutableSet<SightingTaxon> getTaxa() {
    return taxa.keySet();
  }

  Status getStatus(SightingTaxon taxon) {
    // For single-with-secondary-subspecies, check the species itself
    if (taxon.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
      taxon = SightingTaxons.newSightingTaxon(taxon.getId());
    }
    return taxa.get(taxon);
  }

  public ImmutableSet<SightingTaxon> getTaxa(Status status) {
    return ImmutableSet.copyOf(
        Maps.filterValues(taxa, Predicates.equalTo(status)).keySet());
  }
}
