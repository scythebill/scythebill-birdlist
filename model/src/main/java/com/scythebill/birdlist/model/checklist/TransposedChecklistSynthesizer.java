/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.checklist;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.query.SyntheticLocation;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * A class that synthesizes checklists out of the transposed lists, especially
 * combining multiple locations into one checklist.
 */
@Singleton
public class TransposedChecklistSynthesizer {
  private static ImmutableMultimap<String, String> EXCLUSIONS = buildExclusions();
  
  static final class CacheKey {
    final ImmutableSet<String> locationCodes;
    final Taxonomy taxonomy;
    private final int hash;
    
    CacheKey(Iterable<String> locationCodes, Taxonomy taxonomy) {
      Preconditions.checkArgument(taxonomy.isBuiltIn(), "Only built-in taxonomies can be cached");
      this.locationCodes = ImmutableSet.copyOf(locationCodes);
      this.taxonomy = taxonomy;
      this.hash = Objects.hashCode(this.locationCodes, taxonomy.getId());
    }
    
    @Override
    public int hashCode() {
      return hash;
    }
    
    @Override
    public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      
      if (!(o instanceof CacheKey)) {
        return false;
      }
      
      CacheKey that = (CacheKey) o;
      return that.taxonomy == taxonomy
          && that.locationCodes.equals(locationCodes);
    }
  }
  
  private final LoadingCache<CacheKey, Checklist> cache = CacheBuilder.newBuilder()
      .maximumSize(10)
      .build(new CacheLoader<CacheKey, Checklist>() {
        @Override
        public Checklist load(CacheKey key) throws Exception {
          return createChecklist(null, key.locationCodes, key.taxonomy);
        }
      });
  
  @Inject
  TransposedChecklistSynthesizer() {
  }
  
  public Checklist synthesizeChecklist(ReportSet reportSet, Taxonomy taxonomy, Iterable<Location> locations) {
    if (!taxonomy.isBuiltIn()) {
      return null;
    }
    
    ImmutableSet.Builder<String> allChecklistCodes = ImmutableSet.builder();
    for (Location location : locations) {
      Collection<String> checklistCodes = getTransposedChecklistLocations(location);
      if (checklistCodes != null) {
        allChecklistCodes.addAll(checklistCodes);
      } else if (("US".equals(location.getEbirdCode())
          && !"North America".equals(location.getParent().getModelName()))
          || ("BQ".equals(location.getEbirdCode())
              && !"West Indies".equals(location.getParent().getModelName()))) {
        // US, but not North America;  or Netherland Antilles, but not West Indies.
        // Add all the children.
        for (Location child : location.contents()) {
          String checklistCode = Checklists.getChecklistLocationCode(child);
          if (checklistCode != null) {
            allChecklistCodes.add(checklistCode);
          }
        }
      } else {
        String checklistLocationCode = Checklists.getChecklistLocationCode(location);
        if (checklistLocationCode != null) {
          allChecklistCodes.add(checklistLocationCode);
        } else {
          return null;
        }
      }
    }
    
    return synthesizeChecklistInternal(reportSet, taxonomy, allChecklistCodes.build());
  }
  
  private final static ImmutableMap<String, ImmutableSet<String>> SUPPORTED_REGIONS_WITH_EXCLUSIONS = ImmutableMap.<String, ImmutableSet<String>>builder()
      // Exclude Turkey from Europe - it still has a unified list for Europe/Asia, and we don't want the Asian species
      .put("Europe", ImmutableSet.of("TR"))
      .put("Asia", ImmutableSet.of())
      .put("Eurasia", ImmutableSet.of())
      .put("Africa", ImmutableSet.of())
      .put("Australasia", ImmutableSet.of())
      .put("South Polar Region", ImmutableSet.of())
      .put("Central America", ImmutableSet.of())
      .put("North America", ImmutableSet.of())
      .put("South America", ImmutableSet.of())
      .put("West Indies", ImmutableSet.of())
      .put("Indian Ocean", ImmutableSet.of())
      .put("Pacific Ocean", ImmutableSet.of())
      .put("Atlantic Ocean", ImmutableSet.of())
      .put("Arctic Ocean", ImmutableSet.of())
      .build();

  private final static ImmutableMultimap<String, String> SYNTHETIC_US_CHECKLISTS = ImmutableMultimap.<String, String>builder()
      .putAll("West Indies", "PR", "VI")
      .putAll("Pacific Ocean", "US-HI", "GU", "MP", "AS")
      .build();

  private final static ImmutableMultimap<String, String> SYNTHETIC_CHECKLISTS = ImmutableMultimap.<String, String>builder()
      .putAll("GB", "GB-ENG", "GB-NIR", "GB-SCT", "GB-WLS")
      .putAll("SH", "SH-AC", "SH-SH", "SH-TA")
      .build();

  /**
   * Return a collection of checklist location IDs for a location
   * that is always handled with transposed checklists.
   */
  public Collection<String> getTransposedChecklistLocations(Location location) {
    if (location instanceof SyntheticLocation) {
      Collection<String> syntheticLocationUnion = ((SyntheticLocation) location).syntheticChecklistUnion();
      if (syntheticLocationUnion != null) {
        return syntheticLocationUnion;
      }
    }
    
    if (location.isBuiltInLocation()
        && location.getType() == Location.Type.region
        && SUPPORTED_REGIONS_WITH_EXCLUSIONS.containsKey(location.getModelName())) {
      Set<String> checklistCodes = Checklists.gatherCountryChecklistCodes(location);
      ImmutableSet<String> exclusions = SUPPORTED_REGIONS_WITH_EXCLUSIONS.get(location.getModelName());
      if (!exclusions.isEmpty()) {
        checklistCodes = Sets.difference(checklistCodes, exclusions);
      }
      
      return checklistCodes;
    }
    
    if ("US".equals(location.getEbirdCode())) {
      if (SYNTHETIC_US_CHECKLISTS.containsKey(location.getParent().getModelName())) {
        return SYNTHETIC_US_CHECKLISTS.get(location.getParent().getModelName());
      }
    }
    
    if ("BQ".equals(location.getEbirdCode())) {
      if ("South America".equals(location.getParent().getModelName())) {
        return ImmutableSet.of("AW", "BQ-BO", "CW");
      } else {
        return ImmutableSet.of("BQ");
      }
    }
    
    // Second check:  anything that is always synthesized?
    String locationCode = Checklists.getChecklistLocationCode(location);
    if (SYNTHETIC_CHECKLISTS.containsKey(locationCode)) {
      return SYNTHETIC_CHECKLISTS.get(locationCode);
    }
    
    return null;
  }
  
  /**
   * Synthesize a checklist without any reference to a taxonomy.  The checklist
   * will get lazily synthesized as needed on individual calls (each of which does have a taxonomy). 
   */
  public Checklist synthesizeChecklistInternal(ReportSet reportSet, Iterable<String> locationCodes) {
    final ImmutableSet<String> copiedLocationCodes = ImmutableSet.copyOf(locationCodes);
    return new Checklist() {
      private Map<Taxonomy, Checklist> synthesizedChecklists = new HashMap<>();
      
      private Checklist getChecklist(Taxonomy taxonomy) {
        return synthesizedChecklists.computeIfAbsent(taxonomy,
            t -> synthesizeChecklistInternal(reportSet, t, copiedLocationCodes));
      }
      
      @Override
      public boolean isBuiltIn() {
        return true;
      }
      
      @Override
      public boolean includesTaxon(Taxon taxon, Set<Status> excludingStatus) {
        Checklist checklist = getChecklist(taxon.getTaxonomy());
        return checklist.includesTaxon(taxon, excludingStatus);
      }
      
      @Override
      public boolean includesTaxon(Taxon taxon) {
        Checklist checklist = getChecklist(taxon.getTaxonomy());
        return checklist.includesTaxon(taxon);
      }
      
      @Override
      public ImmutableSet<SightingTaxon> getTaxa(Taxonomy taxonomy, Status status) {
        Checklist checklist = getChecklist(taxonomy);
        return checklist.getTaxa(taxonomy, status);
      }
      
      @Override
      public ImmutableSet<SightingTaxon> getTaxa(Taxonomy taxonomy) {
        Checklist checklist = getChecklist(taxonomy);
        return checklist.getTaxa(taxonomy);
      }
      
      @Override
      public Status getStatus(Taxonomy taxonomy, SightingTaxon taxon) {
        Checklist checklist = getChecklist(taxonomy);
        return checklist.getStatus(taxonomy, taxon);
      }

      @Override
      public boolean isSynthetic() {
        return true;
      }
    };
  }
  
  Checklist synthesizeChecklistInternal(ReportSet reportSet, Taxonomy taxonomy, Iterable<String> locationCodes) {
    if (!taxonomy.isBuiltIn()) {
      // TODO: add a cache on the reportSet
      return createChecklist(reportSet, ImmutableSet.copyOf(locationCodes), taxonomy);
    }
    return cache.getUnchecked(new CacheKey(locationCodes, taxonomy));
  }
  
  private Checklist createChecklist(
      ReportSet reportSet,
      ImmutableSet<String> locationCodes,
      Taxonomy taxonomy) {
    // Unless it's a built-in taxonomy, there must be a reportSet
    Preconditions.checkArgument(reportSet != null || taxonomy.isBuiltIn());
    final TransposedChecklist transposed =
        TransposedChecklists.instance().getTransposedChecklist(reportSet, taxonomy);
    final Map<SightingTaxon, Status> builder = Maps.newHashMap();
    
    // Find the set of built-in checklists to exclude from endemic calculation
    ImmutableSet.Builder<String> exclusionBuilder = ImmutableSet.builder();
    for (String locationCode : locationCodes) {
      // TODO: this is wrong when the built exclusions *includes* one of the location codes,
      // but that seems unlikely for a user to intentionally build? 
      exclusionBuilder.addAll(EXCLUSIONS.get(locationCode));
    }
    final ImmutableSet<String> currentExclusions = exclusionBuilder.build();
    
    TaxonUtils.visitTaxa(taxonomy.getRoot(), new TaxonVisitor() {
      @Override
      public boolean visitTaxon(Taxon taxon) {
        if (taxon.getType() == Taxon.Type.species) {
          SightingTaxon sightingTaxon = SightingTaxons.newSightingTaxon(taxon.getId());
          for (String locationId : transposed.locationsWithStatuses(taxon, ImmutableSet.of(Status.EXTINCT))) {
            if (locationCodes.contains(locationId)) {
              builder.put(SightingTaxons.newSightingTaxon(taxon.getId()), Status.EXTINCT);
              break;
            }
          }

          for (String locationId : transposed.locationsWithStatuses(taxon, ImmutableSet.of(Status.RARITY_FROM_INTRODUCED))) {
            if (locationCodes.contains(locationId)) {
              builder.put(sightingTaxon, Status.RARITY_FROM_INTRODUCED);
              break;
            }
          }
          for (String locationId : transposed.locationsWithStatuses(taxon, ImmutableSet.of(Status.RARITY))) {
            if (locationCodes.contains(locationId)) {
              builder.put(sightingTaxon, Status.RARITY);
              break;
            }
          }
          
          for (String locationId : transposed.locationsWithStatuses(taxon, ImmutableSet.of(Status.INTRODUCED))) {
            if (locationCodes.contains(locationId)) {
              builder.put(sightingTaxon, Status.INTRODUCED);
              break;
            }
          }
          
          // Shortcut to ensure that "endemic" flags are honored.  The main logic for computing endemic status
          // is *mostly* correct, but has some occasional failures.  Specifically, Bermuda Petrel is considered
          // endemic to Bermuda, since it's rare in the US, but it's not on the North Carolina review list.
          boolean alreadyEndemic = false;
          Iterable<String> endemicLocations = transposed.locationsWithStatuses(taxon, ImmutableSet.of(Status.ENDEMIC));
          if (!Iterables.isEmpty(endemicLocations)) {
            boolean allEndemicLocationsIncluded = true;
            for (String endemicLocation : endemicLocations) {
              if (!locationCodes.contains(endemicLocation)) {
                allEndemicLocationsIncluded = false;
                break;
              }
            }
            
            if (allEndemicLocationsIncluded) {
              alreadyEndemic = true;
              builder.put(sightingTaxon, Status.ENDEMIC);              
            }
          }
            
          if (!alreadyEndemic) {
            boolean oneNativeInLocations = false;
            boolean oneNativeNotInLocations = false;
            for (String locationId : transposed.locationsWithStatuses(taxon, ImmutableSet.of(Status.NATIVE, Status.ENDEMIC))) {
              if (locationCodes.contains(locationId)) {
                builder.put(sightingTaxon, Status.NATIVE);
                oneNativeInLocations = true;
              } else if (!currentExclusions.contains(locationId)) {
                oneNativeNotInLocations = true;
              }
              
              if (oneNativeInLocations && oneNativeNotInLocations) {
                break;
              }
            }
            
            if (oneNativeInLocations && !oneNativeNotInLocations) {
              builder.put(sightingTaxon, Status.ENDEMIC);
            }
          }
                              
          return false;
        }
        
        return true;
      }
    });
    
    final ImmutableMap<SightingTaxon, Status> map = ImmutableMap.copyOf(builder);
    
    return new Checklist() {
      @Override
      public ImmutableSet<SightingTaxon> getTaxa(Taxonomy checklistTaxonomy) {
        Preconditions.checkArgument(taxonomy == checklistTaxonomy);
        return map.keySet();
      }

      @Override
      public ImmutableSet<SightingTaxon> getTaxa(Taxonomy checklistTaxonomy,
          final Status status) {
        Preconditions.checkArgument(taxonomy == checklistTaxonomy);
        
        return FluentIterable.from(getTaxa(checklistTaxonomy))
            .filter(new Predicate<SightingTaxon>() {
              @Override
              public boolean apply(SightingTaxon taxon) {
                return map.get(taxon) == status;
              }
            }).toSet();
      }

      @Override
      public Status getStatus(Taxonomy checklistTaxonomy, SightingTaxon taxon) {
        Preconditions.checkArgument(taxonomy == checklistTaxonomy);
        // For single-with-secondary-subspecies, check the species itself
        if (taxon.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
          taxon = SightingTaxons.newSightingTaxon(taxon.getId());
        }
        return map.get(taxon);
      }

      @Override
      public boolean includesTaxon(Taxon taxon) {
        Preconditions.checkArgument(taxonomy == taxon.getTaxonomy());
        return map.containsKey(SightingTaxons.newSightingTaxon(taxon.getId()));
      }

      @Override
      public boolean includesTaxon(Taxon taxon, Set<Status> excludingStatus) {
        Preconditions.checkArgument(taxonomy == taxon.getTaxonomy());
        Status status = map.get(SightingTaxons.newSightingTaxon(taxon.getId()));
        if (status == null) {
          return false;
        }
        return !excludingStatus.contains(status);
      }

      @Override
      public boolean isBuiltIn() {
        return true;
      }
      
      @Override
      public boolean isSynthetic() {
        return true;
      }
    };
  }

  private static ImmutableMultimap<String, String> buildExclusions() {
    // A set of states which do not contribute to endemic vs. non-endemic.
    ImmutableSet<String> states = ImmutableSet.of(
        "CA-AB",
        "CA-BC",
        "CA-MB",
        "CA-NB",
        "CA-NL",
        "CA-NT",
        "CA-NS",
        "CA-NU",
        "CA-ON",
        "CA-PE",
        "CA-QC",
        "CA-SK",
        "CA-YT",
        "US-AL",
        "US-AK",
        "US-AZ",
        "US-AR",
        "US-CA",
        "US-CO",
        "US-CT",
        "US-DE",
        "US-DC",
        "US-FL",
        "US-GA",
        // Note that Hawaii is *not* here.  This is intentional, as it already
        // has special treatment for endemic status (and is basically considered a "country")
        "US-ID",
        "US-IL",
        "US-IN",
        "US-IA",
        "US-KS",
        "US-KY",
        "US-LA",
        "US-ME",
        "US-MD",
        "US-MA",
        "US-MI",
        "US-MN",
        "US-MS",
        "US-MO",
        "US-MT",
        "US-NE",
        "US-NV",
        "US-NH",
        "US-NJ",
        "US-NM",
        "US-NY",
        "US-NC",
        "US-ND",
        "US-OH",
        "US-OK",
        "US-OR",
        "US-PA",
        "US-RI",
        "US-SC",
        "US-SD",
        "US-TN",
        "US-TX",
        "US-UT",
        "US-VT",
        "US-VA",
        "US-WA",
        "US-WV",
        "US-WI",
        "US-WY",
        "AU-ACT",
        "AU-NSW",
        "AU-NT",
        "AU-QLD",
        "AU-SA",
        "AU-TAS",
        "AU-VIC",
        "AU-WA",
        "ID-JW",
        "ID-KA",
        "ID-MA",
        "ID-NU",
        "ID-IJ",
        "ID-SL",
        "ID-SM",
        "FI-01",
        "PG-NSA");
    
    ImmutableMap<String, String> INDONESIA_AUSTRALASIA_STATE_TO_COUNTRY = ImmutableMap.of(
        "ID-IJ", "ID-Australasia",
        "ID-MA", "ID-Australasia");
    
    // For calculation of endemic state during checklist synthesis, we need to come up with:
    // (1) A list of location IDs that should be included, mostly ones that are handed,
    //    but occasionally additional ones where the children of a location have checklists
    //    but not that parent.
    // (2) A list of location IDs that should be *ex*cluded, typically children or parents
    //     of a requested location.  For example, if we have California and Oregon, then
    //     the presence of that species in Washington means it's not an endemic (so include
    //     Washington) but the presence of that species in the United States is irrelevant.
    //     Likewise, if we have the N. American US, we need to *ex*clude all the states of the US.
    // This builds up the exclusion multimap
    ImmutableMultimap.Builder<String, String> builder = ImmutableMultimap.builder();
    for (String state : states) {
      String country = state.substring(0, 2);
      
      // Indonesia has two checklists, -Asia, and -Australasia.  Map to the correct one.
      if (country.equals("ID")) {
        country = INDONESIA_AUSTRALASIA_STATE_TO_COUNTRY.getOrDefault(state, "ID-Asia");
      }
      
      // For each state: exclude the country if the state is present, exclude the state if the country is present
      builder.put(state, country);
      builder.put(country, state);
    }
    
    return builder.build();
  }
}
