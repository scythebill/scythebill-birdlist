/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.checklist;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;

import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;
import com.google.common.io.Resources;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;

public class TransposedChecklists {
  private static final String NATIVE = "native";
  private final static TransposedChecklists instance = new TransposedChecklists();
  private final static Splitter COMMA_SPLITTER = Splitter.on(',');
  private final LoadingCache<String, TransposedChecklist> transposedChecklistCache =
      CacheBuilder.newBuilder().build(new CacheLoader<String, TransposedChecklist>() {
        @Override
        public TransposedChecklist load(String filename) throws Exception {
          return getTransposedChecklist(filename);
        }
      });
  
  private TransposedChecklists() {}
  
  public static TransposedChecklists instance() {
    return instance;
  }
  
  public TransposedChecklist getTransposedChecklist(ReportSet reportSet, Taxonomy taxonomy) {
    if (taxonomy.isBuiltIn()) {
      String transposeFileName = (taxonomy instanceof MappedTaxonomy)
          ? "ioc.transpose"
          : "clements.transpose";
      return transposedChecklistCache.getUnchecked(transposeFileName);
    } else {
      ExtendedTaxonomyChecklists checklists = reportSet.getExtendedTaxonomyChecklist(taxonomy.getId());
      if (checklists == null || checklists.getChecklists().isEmpty()) {
        return null;
      }
      return checklists.getTransposedChecklist();
    }
  }
    
  private final ImmutableSet<Checklist.Status> ALL_STATUSES = ImmutableSet.copyOf(Checklist.Status.values());
  public boolean hasTransposedChecklistEntry(ReportSet reportSet, Taxon taxon) {
    if (taxon.getType() != Taxon.Type.species) {
      return false;
    }
    
    TransposedChecklist transposedChecklist =
        TransposedChecklists.instance().getTransposedChecklist(reportSet, taxon.getTaxonomy());
    if (transposedChecklist == null) {
      return false;
    }
    
    return !Iterables.isEmpty(transposedChecklist.locationsWithStatuses(taxon, ALL_STATUSES));    
  }
  
  private TransposedChecklist getTransposedChecklist(String filename) {
    try {
      final Table<String, String, List<String>> loaded = loadTranspose(filename);
      return new TransposedChecklist() {
        @Override public Iterable<String> locationsWithStatuses(
            Taxon taxon,
            ImmutableSet<Status> statusSet) {
          return locationsWithStatuses(taxon.getId(), statusSet);
        }

        @Override public Iterable<String> locationsWithStatuses(
            String speciesId,
            ImmutableSet<Status> statusSet) {
          List<List<String>> locationsListList = Lists.newArrayList();
          for (Status status : statusSet) {
            String statusString = status == Status.NATIVE ? NATIVE : status.text();
            List<String> list = loaded.get(speciesId, statusString);
            if (list != null) {
              locationsListList.add(list);
            }
          }
          return Iterables.concat(locationsListList);
        }
        
      };
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }
  
  private Table<String, String, List<String>> loadTranspose(String transposeFileName) throws IOException {
    Table<String, String, List<String>> speciesStatusCountriesTable =
        HashBasedTable.create(11000, 3);
    URL url = Resources.getResource(getClass(), transposeFileName);
    InputStream stream = url.openStream();
    BufferedReader reader = new BufferedReader(new InputStreamReader(stream, Charsets.UTF_8));
    try {
      while (true) {
        String line = reader.readLine();
        if (line == null) {
          break;
        }
        
        int equalsIndex = line.indexOf('=');
        if (equalsIndex < 0) {
          continue;
        }
        
        String start = line.substring(0, equalsIndex);
        String species;
        String status;
        int slashIndex = start.indexOf('/');
        if (slashIndex < 0) {
          species = start;
          status = NATIVE;
        } else {
          species = start.substring(0, slashIndex);
          status = start.substring(slashIndex + 1);
        }
        String end = line.substring(equalsIndex + 1);
        List<String> countries = COMMA_SPLITTER.splitToList(end);
        speciesStatusCountriesTable.put(species, status, countries);
      }
      
      return speciesStatusCountriesTable;
    } finally {
      reader.close();
    }
  }  
  
  /** 
   * For a given Resolved taxon, return a location that can be assumed given the associated transposed checklist.
   */
  public static Location getAssumedLocation(Resolved resolved, LocationSet locations,
      PredefinedLocations predefinedLocations, TransposedChecklist transposedChecklist) {
    // Do a very quick-and-dirty attempt to pick a location at least for some species where they're endemic to one site.
    // This code could be better!  It could find the parent of all locations where the species is found (e.g.
    // a region) when it's not endemic to just one location.
    String endemicLocation = null;
    for (Taxon taxon : resolved.getTaxa()) {
      Taxon species = TaxonUtils.getParentOfAtLeastType(taxon, Taxon.Type.species);
      Iterable<String> endemicLocations = transposedChecklist.locationsWithStatuses(species, ImmutableSet.of(Checklist.Status.ENDEMIC));
      if (Iterables.isEmpty(endemicLocations)) {
        endemicLocation = null;
        break;
      }
      
      // TODO: it is actually possible to be endemic twice, e.g. "California" and "US", and we want the most specific,
      // not an arbitrary one
      String first = endemicLocations.iterator().next();
      if (endemicLocation != null && !first.equals(endemicLocation)) {
        endemicLocation = null;
        break;
      }
      
      endemicLocation = first;
    }
    
    if (endemicLocation == null) {
      return null;
    }
    
    if ("ID-Asia".equals(endemicLocation)) {
      return getLocationWithParent(locations, "ID", "Asia");
    } else if ("ID-Australasia".equals(endemicLocation)) {
      return getLocationWithParent(locations, "ID", "Australasia");
    } else if ("RU-Asia".equals(endemicLocation)) {
      return getLocationWithParent(locations, "RU", "Asia");
    }
    return Locations.getLocationByCodePossiblyCreating(locations, predefinedLocations, endemicLocation);
  }

  private static Location getLocationWithParent(LocationSet locations,
      String countryCode,
      String parent) {
    for (Location location : locations.getLocationsByCode("ID")) {
      if (location.getParent().getModelName().equals(parent)) {
        return location;
      }
    }
    
    return null;
  }
}
