/*   
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.export;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Iterator;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTimeFieldType;
import org.joda.time.LocalTime;
import org.joda.time.ReadablePartial;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.io.ByteSink;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.sighting.ApproximateNumber;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences;

/**
 * Output sightings to a CSV file in the BirdTrack CSV format
 * See https://www.bto.org/sites/default/files/guide_to_the_birdtrack_upload_records_tool_v2_aug2017.pdf
 */
public class BirdTrackCsvExport {
  /** Use a constant pattern, irrespective of locale. */
  private static final DateTimeFormatter DATE_FORMATTER =
      new DateTimeFormatterBuilder()
          .appendDayOfMonth(2)
          .appendLiteral('-')
          .appendMonthOfYear(2)
          .appendLiteral('-')
          .appendYear(4, 4)
          .toFormatter();
  /** Use a constant pattern, irrespective of locale. */
  private static final DateTimeFormatter TIME_FORMATTER =
      new DateTimeFormatterBuilder()
          .appendHourOfDay(2)
          .appendLiteral(':')
          .appendMinuteOfHour(2)
          .toFormatter();
  
  /** Maximum number of rows */
  private static final long MAX_ROWS = 15000;

  private static final String[] COLUMN_HEADERS = new String[] {
    "Species",
    "Date",
    "Start",
    "End",
    "Comments",
    "Where",
    "Latitude",
    "Longitude",
    "Count",
    "Breeding",
    "Activity",
    "Complete_List",
    "Visit_Comments",
  };
  
  private static final int TOTAL_COLUMN_COUNT = COLUMN_HEADERS.length;
  private static final int COLUMN_TAXON_NAME = 0;
  private static final int COLUMN_DATE = 1;
  private static final int COLUMN_START = 2;
  private static final int COLUMN_END = 3;
  private static final int COLUMN_DESCRIPTION = 4;
  private static final int COLUMN_PLACE_NAME = 5;
  private static final int COLUMN_LATITUDE = 6;
  private static final int COLUMN_LONGITUDE = 7;
  private static final int COLUMN_COUNT = 8;
  private static final int COLUMN_BREEDING = 9;
  private static final int COLUMN_ACTIVITY = 10;
  private static final int COLUMN_COMPLETE_LIST = 11;
  private static final int COLUMN_VISIT_COMMENTS = 12;
  private final NamesPreferences namesPreferences;

  public BirdTrackCsvExport(NamesPreferences namesPreferences) {
    this.namesPreferences = namesPreferences;
  }

  /**
   * Returns true if the current sighting is acceptable for BirdTrack imports.
   */
  public static boolean isAcceptableSighting(ReportSet reportSet, Sighting sighting) {
    if (VisitInfoKey.forSighting(sighting) == null) {
      return false;
    }
    
    if (!isValidDate(sighting.getDateAsPartial())) {
      return false;
    }
    
    if (sighting.getLocationId() == null) {
      return false;
    }
    
    Location location = reportSet.getLocations().getLocation(sighting.getLocationId());
    if (!location.getLatLong().isPresent()) {
      return false;
    }
    
    if (location.isPrivate()) {
      return false;
    }

    return true;
  }

  public void writeSpeciesList(ByteSink outSupplier,
      QueryResults queryResults,
      Iterator<Sighting> sightingIterator,
      Taxonomy taxonomy,
      ReportSet reportSet) throws IOException {
    Writer out = new BufferedWriter(
        new OutputStreamWriter(outSupplier.openStream(), Charsets.UTF_8), 10240);
    try (ExportLines exportLines = CsvExportLines.fromWriter(out)) {
      String savedLocale = namesPreferences.locale;
      try {
        // Force the locale to use BRITISH translations
        namesPreferences.locale = NamesPreferences.AvailableIocLocale.BRITISH.code();
        writeSpeciesList(exportLines, sightingIterator, taxonomy, reportSet);
      } finally {
        namesPreferences.locale = savedLocale;
      }
    }
  }

  private void writeSpeciesList(ExportLines exportLines,
      Iterator<Sighting> sightingIterator, Taxonomy taxonomy, ReportSet reportSet)
      throws IOException {
    exportLines.nextLine(COLUMN_HEADERS);
    
    int rowCount = 0;
    while (sightingIterator.hasNext()) {
      Sighting sighting = sightingIterator.next();
      String[] line = new String[TOTAL_COLUMN_COUNT];

      // Convert to a BirdTrack-acceptable name - "Common Name" or "Common Name (subspecies)"
      Resolved taxon = sighting.getTaxon().resolve(taxonomy); 
      if (taxon.getSmallestTaxonType() == Taxon.Type.species) {
        line[COLUMN_TAXON_NAME] = taxon.getCommonName();      
      } else {
        Resolved species = taxon.getParentOfAtLeastType(Taxon.Type.species).resolveInternal(taxon.getTaxonomy());
        // If it was a "spuh" of multiple subspecies, then just raise to species
        if (taxon.getTaxa().size() > 1) {
          line[COLUMN_TAXON_NAME] = species.getCommonName();
        } else {
          // Domestic and introduced forms don't appear to be in the BirdTrack taxonomy - treat them
          // as the parent species.  
          if (taxon.getTaxon().getStatus() == Status.DO
              || taxon.getTaxon().getStatus() == Status.IN) {
            line[COLUMN_TAXON_NAME] = species.getCommonName();
          } else {
            // Otherwise include the species *and* the subspecies
            line[COLUMN_TAXON_NAME] =
                String.format("%s (%s)", species.getCommonName(), taxon.getTaxon().getName());
          }
        }
      }
      
      if (sighting.getLocationId() != null) {
        Location location = reportSet.getLocations().getLocation(sighting.getLocationId());        
        line[COLUMN_PLACE_NAME] = location.getDisplayName();
        if (location.getLatLong().isPresent()) {
          LatLongCoordinates latLong = location.getLatLong().get();
          line[COLUMN_LATITUDE] = String.format(Locale.US, "%2.4f", latLong.latitudeAsDouble());
          line[COLUMN_LONGITUDE] = String.format(Locale.US, "%2.4f", latLong.longitudeAsDouble());
        }
      }
      
      if (isValidDate(sighting.getDateAsPartial())) {
        line[COLUMN_DATE] = DATE_FORMATTER.print(sighting.getDateAsPartial());
        if (sighting.getTimeAsPartial() != null) {
          LocalTime timeAsPartial = sighting.getTimeAsPartial();
          // Round down to the nearest 5 minutes, per BIrdTrack rules
          timeAsPartial = timeAsPartial.minusMinutes(timeAsPartial.getMinuteOfHour() % 5);
          line[COLUMN_START] = TIME_FORMATTER.print(timeAsPartial);
        }
      }

      if (sighting.hasSightingInfo()) {
        SightingInfo sightingInfo = sighting.getSightingInfo();
        if (sightingInfo.getDescription() != null) {
          line[COLUMN_DESCRIPTION] = StringUtils.abbreviate(trimLatLong(sightingInfo.getDescription()), 1000);
        }
        
        if (sightingInfo.getNumber() != null) {
          ApproximateNumber number = sightingInfo.getNumber();
          String numberString = number.toString();
          if (number.isExact()) {
            line[COLUMN_COUNT] = numberString;
          } else {
            if (numberString.startsWith("+") || numberString.startsWith(">")) {
              line[COLUMN_COUNT] = "+" + number.getNumber();
            } else {
              line[COLUMN_COUNT] = "c" + number.getNumber();
            }
          }
        }
        
        if (sightingInfo.getBreedingBirdCode() != null ) {
          line[COLUMN_BREEDING] = sightingInfo.getBreedingBirdCode().getBirdTrackId();
        }
        
        switch (sightingInfo.getSightingStatus()) {
          case DEAD:
            line[COLUMN_ACTIVITY] = "9,,";
            break;
          case INTRODUCED_NOT_ESTABLISHED:
            line[COLUMN_ACTIVITY] = "10,,";
            break;
          case ID_UNCERTAIN:
            line[COLUMN_ACTIVITY] = "11,,";
            break;
          default:
            break;
            
        }
      }
      
      VisitInfo visitInfo = reportSet.getVisitInfo(VisitInfoKey.forSighting(sighting));
      if (visitInfo != null) {
        if (visitInfo.duration().isPresent() && sighting.getTimeAsPartial() != null) {
          LocalTime endTime = sighting.getTimeAsPartial().plus(
              visitInfo.duration().get().toPeriod(sighting.getTimeAsPartial().getChronology()));
          // Round down to the nearest 5 minutes, per BIrdTrack rules
          endTime = endTime.minusMinutes(endTime.getMinuteOfHour() % 5);
          line[COLUMN_END] = TIME_FORMATTER.print(endTime);
        }
        
        if (visitInfo.completeChecklist()) {
          line[COLUMN_COMPLETE_LIST] = "Y";
        }
        
        if (visitInfo.comments().isPresent()) {
          line[COLUMN_VISIT_COMMENTS] = StringUtils.abbreviate(visitInfo.comments().get(), 1000);
        }
      }
      exportLines.nextLine(line);

      // Stop iterating when the row count is excessive
      if (rowCount++ >= MAX_ROWS) {
        return;
      }
    }
  }

  private final static CharMatcher LAT_LONG_CHARS = CharMatcher.anyOf("-.0123456789,");
  private final static CharMatcher NOT_LAT_LONG_CHARS = LAT_LONG_CHARS.negate().or(CharMatcher.whitespace());
  
  /**
   * BirdLasser and Observado imports add "LL:" with a lat-long.  This is inappropriate to send
   * to eBird;  trim it (and trim multiple instances if multiple sightings get appended to one).
   */
  private String trimLatLong(String in) {
    while (true) {
      int lastLongStart = in.lastIndexOf("LL:");
      if (lastLongStart < 0) {
        break;
      }
      
      int endOfLatLong = NOT_LAT_LONG_CHARS.indexIn(in, lastLongStart + 3);
      if (endOfLatLong == lastLongStart + 3) {
        break;
      }
      
      if (endOfLatLong < 0) {
        in = in.substring(0, lastLongStart);
      } else {
        in = in.substring(0, lastLongStart) + in.substring(endOfLatLong);
      }
      in = CharMatcher.whitespace().trimFrom(in);
    }
    
    return in;
  }  

  /** Records must have at least day/month/year. */
  private static boolean isValidDate(ReadablePartial partial) {
    return partial != null
         && partial.isSupported(DateTimeFieldType.year())
         && partial.isSupported(DateTimeFieldType.monthOfYear())
         && partial.isSupported(DateTimeFieldType.dayOfMonth());
  }

}
