/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.export;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

import org.joda.time.DateTimeFieldType;
import org.joda.time.ReadablePartial;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.ByteSink;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.io.ProgressOutputStream;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfo.ObservationType;
import com.scythebill.birdlist.model.sighting.VisitInfo.VisitField;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Output sightings to a CSV file in the EBird record format form.
 * See https://support.ebird.org/support/solutions/articles/48000907878-upload-spreadsheet-data-to-ebird
 */
public class EBirdCsvExport {
  /** Use a constant pattern, irrespective of locale. */
  private static final DateTimeFormatter EBIRD_DATE_FORMATTER =
      new DateTimeFormatterBuilder()
          .appendMonthOfYear(1)
          .appendLiteral('/')
          .appendDayOfMonth(1)
          .appendLiteral('/')
          .appendYear(4, 4)
          .toFormatter();
  /** Use a constant pattern, irrespective of locale. */
  private static final DateTimeFormatter EBIRD_TIME_FORMATTER =
      new DateTimeFormatterBuilder()
          .appendHourOfDay(2)
          .appendLiteral(':')
          .appendMinuteOfHour(2)
          .toFormatter();
  /** Maximum file size.  Here, using 900K to fit well under the 1MB limit */
  private static final long MAX_FILE_SIZE = 900 * 1024;
  // Column indices.  Most columns are not used yet;  see the above website
  // and linked-to docs for more information
  
  public static final int COLUMN_COMMON_NAME = 0;
  public static final int COLUMN_GENUS = 1;
  public static final int COLUMN_SPECIES = 2;
  public static final int COLUMN_SPECIES_COUNT = 3;
  public static final int COLUMN_SPECIES_COMMENTS = 4;
  public static final int COLUMN_LOCATION_NAME = 5;
  public static final int COLUMN_LATITUDE = 6;
  public static final int COLUMN_LONGITUDE = 7;
  public static final int COLUMN_OBSERVATION_DATE = 8;
  public static final int COLUMN_START_TIME = 9;
  public static final int COLUMN_STATE = 10;
  public static final int COLUMN_COUNTRY = 11;
  public static final int COLUMN_PROTOCOL = 12;
  public static final int COLUMN_NUMBER_OF_OBSERVERS = 13;
  public static final int COLUMN_DURATION = 14;
  public static final int COLUMN_ALL_OBSERVATIONS_REPORTED = 15;
  public static final int COLUMN_DISTANCE_COVERED = 16;
  public static final int COLUMN_AREA_COVERED = 17;
  public static final int COLUMN_CHECKLIST_COMMENTS = 18;
  private static final int TOTAL_COLUMN_COUNT = 19;

  /**
   * These observation types - the keys, that is - are not supported for eBird imports.
   * Because... no idea, really.  They're just not.  Until they are, lie about the
   * observation type when imported.  In a couple of cases, the fields that are required
   * match another type exactly, so it seems the best we can do is enter those exactly.
   */
  private static final ImmutableMap<ObservationType, ObservationType> UNSUPPORTED_OBSERVATION_TYPES = ImmutableMap.of(
      ObservationType.PELAGIC_PROTOCOL, ObservationType.TRAVELING,
      ObservationType.BANDING, ObservationType.HISTORICAL,
      ObservationType.NOCTURNAL_FLIGHT_CALL_COUNT, ObservationType.STATIONARY,
      ObservationType.NOCTURNAL_BIRDING, ObservationType.HISTORICAL);
  private final Taxonomy clements;
  private final ImmutableMap<Taxon, Taxon> nonCanonicalTaxa;

  public EBirdCsvExport(Taxonomy clements) {
    this.clements = clements;
    // Taxa not in the "true" Clements that need additional mappings
    nonCanonicalTaxa = ImmutableMap.of();
  }

  public void writeSpeciesList(ByteSink outSupplier,
      Iterator<Sighting> sightingsIterator,
      QueryResults queryResults,
      ReportSet reportSet) throws IOException {
    ProgressOutputStream progress = new ProgressOutputStream(outSupplier.openStream());
    Writer out = new BufferedWriter(
        new OutputStreamWriter(progress, Charsets.UTF_8), 10240);
    try (ExportLines exportLines = CsvExportLines.fromWriter(out)) {
      writeSpeciesList(exportLines, sightingsIterator, queryResults, reportSet, progress);
    }
  }
  
  /**
   * Returns true if the record can acceptably be exported to eBird.
   */
  public static boolean isAcceptableEBirdRecord(Sighting sighting, ReportSet reportSet) {
    if (sighting.getLocationId() == null) {
      // No location - no EBird.  Move along.
      return false;
    }

    Location location = reportSet.getLocations().getLocation(sighting.getLocationId());
    // Locations need to be at least a little precise for them to be useful
    if (!isAcceptableLocation(location)) {
      return false;
    }
    
    if (location.isPrivate()) {
      return false;
    }

    // And drop records that are "dubious"
    if (!isAcceptableRecord(sighting)) {
      return false;
    }
    
    return true;
  }

  /**
   * Returns true if the record has to be described as a "list-building" sighting.
   */
  public static boolean isListBuildingSighting(Sighting sighting, ReportSet reportSet) {
    return isListBuildingDate(sighting.getDateAsPartial());
  }
  

  /** EBird records without a day/month/year must be "list-building". */
  private static boolean isListBuildingDate(ReadablePartial partial) {
    return partial == null
         || !partial.isSupported(DateTimeFieldType.year())
         || !partial.isSupported(DateTimeFieldType.monthOfYear())
         || !partial.isSupported(DateTimeFieldType.dayOfMonth());
  }

  private static boolean isAcceptableLocation(Location location) {
    // Records only located to region are not sufficient.
    // State records are undesirable, but people can deal with that
    // inside eBird (no reason to pre-emptively reject).
  if ((location.isBuiltInLocation()
          // Allow non-built-in regions, which users sometimes
          // mistakenly choose as a location type.
          && location.getType() == Location.Type.region)) {
      return false;
    }
    
    return true;
  }

  private static boolean isAcceptableRecord(Sighting sighting) {
    if (sighting.getSightingInfo() != null) {
      SightingInfo sightingInfo = sighting.getSightingInfo();
      // Do not return any sighting that was "rejected" or uncertain.
      // TODO: make this configurable
      if (sightingInfo.getSightingStatus() == SightingInfo.SightingStatus.RECORD_NOT_ACCEPTED
          || sightingInfo.getSightingStatus() == SightingInfo.SightingStatus.ID_UNCERTAIN) {
        return false;
      }
    }
    
    return true;
  }
  
  private void writeSpeciesList(ExportLines exportLines, Iterator<Sighting> sightingsIterator,
      QueryResults queryResults, ReportSet reportSet, ProgressOutputStream progress)
      throws IOException {
    while (sightingsIterator.hasNext()) {
      Sighting sighting = sightingsIterator.next();
      Resolved nonCanonicalTaxon =
          sighting.getTaxon().resolve(clements);
      if (nonCanonicalTaxon.getSmallestTaxonType() == Taxon.Type.subspecies) {
        nonCanonicalTaxon = nonCanonicalTaxon.getParentOfAtLeastType(Taxon.Type.group).resolve(clements);
      }
      Resolved canonicalTaxon = canonicalizeForEBird(nonCanonicalTaxon);
      
      boolean needsLocationReconciliation = needsLocationReconciliation(canonicalTaxon);
      
      if (!isAcceptableEBirdRecord(sighting, reportSet)) {
        continue;
      }
      
      Location location = reportSet.getLocations().getLocation(sighting.getLocationId());        
      Location state = getAncestorOfTypeWithEbirdCode(location, Location.Type.state);
      Location country = getAncestorOfTypeWithEbirdCode(location, Location.Type.country);

      Resolved taxonForLine = needsLocationReconciliation
          ? canonicalizeForLocation(canonicalTaxon, state)
          : canonicalTaxon;
          
      String[] line = new String[TOTAL_COLUMN_COUNT];
      // TODO: this should work for species and groups;  but not subspecies
      line[COLUMN_COMMON_NAME] = taxonForLine.getCommonName();
      // Append (hybrid) to match EBird naming conventions
      if (sighting.getTaxon().getType() == SightingTaxon.Type.HYBRID) {
        line[COLUMN_COMMON_NAME] += " (hybrid)";
      }
      
      if (taxonForLine.getType() == SightingTaxon.Type.SINGLE
          || taxonForLine.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
        Taxon singleTaxon = taxonForLine.getTaxon();
        Taxon species = TaxonUtils.getParentOfType(singleTaxon, Taxon.Type.species);
        if (singleTaxon.getType() == Taxon.Type.group) {
          line[COLUMN_SPECIES] = species.getName() + " " + singleTaxon.getName();
        } else {
          line[COLUMN_SPECIES] = species.getName();
        }
        Taxon genus = TaxonUtils.getParentOfType(singleTaxon, Taxon.Type.genus);
        line[COLUMN_GENUS] = genus.getName();
      }
      line[COLUMN_LOCATION_NAME] = location.getModelName();
      if (location.getLatLong().isPresent()) {
        LatLongCoordinates latLong = location.getLatLong().get();
        line[COLUMN_LATITUDE] = latLong.latitudeAsCanonicalString();
        line[COLUMN_LONGITUDE] = latLong.longitudeAsCanonicalString();
      }
      
      if (state != null && state.getEbirdCode() != null) {
        String ebirdCode = state.getEbirdCode();
        // Hyphen in the state;  ignore whatever the country is, and simply use the
        // code from the state to define both the country and state.  This comes up
        // for Northern Ireland, which is (per eBird) a "state" in United Kingdom.
        if (ebirdCode.contains("-")) {
          int indexOfHyphen = ebirdCode.indexOf('-');
          line[COLUMN_COUNTRY] = ebirdCode.substring(0, indexOfHyphen);
          line[COLUMN_STATE] = ebirdCode.substring(indexOfHyphen + 1);
        } else {
          line[COLUMN_STATE] = ebirdCode;
        }
      }
      if (country != null && country.getEbirdCode() != null) {
        String ebirdCode = country.getEbirdCode();
        if (ebirdCode.contains("-")) {
          // Hyphen in the country:  it defines both a country and a state.  This up for England, which is
          // (per eBird) a "state" in the United Kingdom, though in British terms it is considered a country. 
          int indexOfHyphen = ebirdCode.indexOf('-');
          line[COLUMN_COUNTRY] = MoreObjects.firstNonNull(line[COLUMN_COUNTRY], ebirdCode.substring(0, indexOfHyphen));
          String stateHalfOfCountry = ebirdCode.substring(indexOfHyphen + 1);
          // Ignore long "states".  This is a pure hack to support getting Ceuta and Melilla out of Europe
          if (stateHalfOfCountry.length() <= 3) {
            line[COLUMN_STATE] = MoreObjects.firstNonNull(line[COLUMN_STATE], stateHalfOfCountry);
          }
        } else {
          // Apply the country, unless it was already set above
          line[COLUMN_COUNTRY] = MoreObjects.firstNonNull(line[COLUMN_COUNTRY], ebirdCode);
        }
      }
      
      if (isListBuildingDate(sighting.getDateAsPartial())) {
        line[COLUMN_OBSERVATION_DATE] = "1/1/1900";
        line[COLUMN_START_TIME] = "";
      } else {
        line[COLUMN_OBSERVATION_DATE] = EBIRD_DATE_FORMATTER.print(sighting.getDateAsPartial());
        if (sighting.getTimeAsPartial() != null) {
          line[COLUMN_START_TIME] = EBIRD_TIME_FORMATTER.print(sighting.getTimeAsPartial());
        }
      }
      SightingInfo sightingInfo = sighting.getSightingInfo();
      if (sightingInfo != null) {
        if (sightingInfo.getNumber() != null) {
          line[COLUMN_SPECIES_COUNT] = Integer.toString(sightingInfo.getNumber().getNumber());
        } else {
          line[COLUMN_SPECIES_COUNT] = "X";
        }
        
        if (sightingInfo.getDescription() != null) {
          line[COLUMN_SPECIES_COMMENTS] = trimLatLong(replaceDisallowedCharacters(sightingInfo.getDescription()));
        }
      }
      
      VisitInfoKey visitInfoKey = VisitInfoKey.forSighting(sighting);
      VisitInfo visitInfo = visitInfoKey == null ? null : reportSet.getVisitInfo(visitInfoKey);
      
      if (visitInfo != null) {
        ObservationType observationType = visitInfo.observationType();
        // If the observation type isn't supported by eBird imports, then swap it for one that is
        if (UNSUPPORTED_OBSERVATION_TYPES.containsKey(observationType)) {
          observationType = UNSUPPORTED_OBSERVATION_TYPES.get(observationType);
        }
        // Not exactly sure how data gets into this state - there's clearly a bug! - but snap
        // back to HISTORICAL if the observation type needs start time and doesn't have it.
        if (sighting.getTimeAsPartial() == null
            && observationType.getRequiredFields().contains(VisitField.START_TIME)) {
          observationType = ObservationType.HISTORICAL;
        }

        line[COLUMN_PROTOCOL] = observationType.eBirdId();
        
        line[COLUMN_ALL_OBSERVATIONS_REPORTED] = visitInfo.completeChecklist() ? "Y" : "N";
        if (visitInfo.area().isPresent()) {
          line[COLUMN_AREA_COVERED] = Float.toString(visitInfo.area().get().acres());
        }
        if (visitInfo.comments().isPresent()) {
          line[COLUMN_CHECKLIST_COMMENTS] = replaceDisallowedCharacters(visitInfo.comments().get());
        }
        if (visitInfo.distance().isPresent()) {
          line[COLUMN_DISTANCE_COVERED] = Float.toString(visitInfo.distance().get().miles());
        }
        if (visitInfo.duration().isPresent()) {
          long minutes = visitInfo.duration().get().getStandardMinutes();
          // There've been bugs in duration parsing that produced negative durations; drop them, rather than letting eBird error out.
          if (minutes > 0) {
            line[COLUMN_DURATION] = Long.toString(minutes);
          }
        }
        if (visitInfo.partySize().isPresent()) {
          line[COLUMN_NUMBER_OF_OBSERVERS] = Integer.toString(visitInfo.partySize().get());
        }
      } else {
        line[COLUMN_ALL_OBSERVATIONS_REPORTED] = "N";
        line[COLUMN_PROTOCOL] = "Incidental";
      }
      
      // If it's a list-building sighting, then force to Jan. 1 1900 and "Incidental". 
      if (isListBuildingSighting(sighting, reportSet)) {
        line[COLUMN_PROTOCOL] = "Incidental";
        if (Strings.isNullOrEmpty(line[COLUMN_CHECKLIST_COMMENTS])) {
          line[COLUMN_CHECKLIST_COMMENTS] = "life-list building checklist";
        } else {
          line[COLUMN_CHECKLIST_COMMENTS] = "life-list building checklist. " + line[COLUMN_CHECKLIST_COMMENTS];
        }
      }
      exportLines.nextLine(line);

      // Stop iterating when the file length is excessive
      if (progress.getBytesWritten() > MAX_FILE_SIZE) {
        return;
      }
    }
  }

  /** Returns true if the location will need re-resolution based on the location. */
  private boolean needsLocationReconciliation(Resolved resolved) {
    // Was once needed for African Barn Owls (affinis vs poensis), but now obsolete.
    return false;
  }

  private Resolved canonicalizeForLocation(Resolved resolved, Location state) {
    throw new IllegalStateException("Not expecting location resolution for " + resolved);
  }

  /** Canonicalize an eBird taxon for any liberties Scythebill takes. */
  private Resolved canonicalizeForEBird(Resolved resolved) {
    boolean containsNonCanonicalTaxon = false;
    for (Taxon taxon : resolved.getTaxa()) {
      if (nonCanonicalTaxa.containsKey(taxon)) {
        containsNonCanonicalTaxon = true;
        break;
      }
    }
    
    if (!containsNonCanonicalTaxon) {
      return resolved;
    }
    
    LinkedHashSet<Taxon> canonicalTaxa = resolved.getTaxa().stream().map(
        taxon -> nonCanonicalTaxa.getOrDefault(taxon, taxon))
        .collect(Collectors.toCollection(LinkedHashSet::new));
    return SightingTaxons.newPossiblySpResolved(canonicalTaxa);
  }

  /**
   * Returns the Location ancestor with a given type that has an ebird code, or null.
   * Will return the location itself if it is of the correct type.
   */
  private static Location getAncestorOfTypeWithEbirdCode(
      Location location, Location.Type type) {
    while (location.getType() != type || location.getEbirdCode() == null) {
      location = location.getParent();
      if (location == null) {
        break;
      }
    }
    
    return location;
  }
  
  /**
   * eBird disallows double-quotes and hard-returns.  Replace double-quotes
   * with single-quotes, and hard returns with single spaces.
   */
  private String replaceDisallowedCharacters(String in) {
    return in.replace('\n', ' ').replace('\r', ' ').replace('"', '\'');
  }

  private final static CharMatcher LAT_LONG_CHARS = CharMatcher.anyOf("-.0123456789,");
  private final static CharMatcher NOT_LAT_LONG_CHARS = LAT_LONG_CHARS.negate().or(CharMatcher.whitespace());
  
  /**
   * BirdLasser and Observado imports add "LL:" with a lat-long.  This is inappropriate to send
   * to eBird;  trim it (and trim multiple instances if multiple sightings get appended to one).
   */
  private String trimLatLong(String in) {
    while (true) {
      int lastLongStart = in.lastIndexOf("LL:");
      if (lastLongStart < 0) {
        break;
      }
      
      int endOfLatLong = NOT_LAT_LONG_CHARS.indexIn(in, lastLongStart + 3);
      if (endOfLatLong == lastLongStart + 3) {
        break;
      }
      
      if (endOfLatLong < 0) {
        in = in.substring(0, lastLongStart);
      } else {
        in = in.substring(0, lastLongStart) + in.substring(endOfLatLong);
      }
      in = CharMatcher.whitespace().trimFrom(in);
    }
    
    return in;
  }

}
