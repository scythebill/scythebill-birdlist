/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.export;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Predicates;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.io.ByteSink;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.io.TimeIO;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Photo;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.user.User;

/**
 * Output all sightings to a CSV file in a custom Scythebill record format form.
 */
@SuppressWarnings("unused")
public class FullReportExport {

  private static final int COLUMN_COMMON_NAME = 0;
  private static final int COLUMN_SCIENTIFIC_NAME = 1;
  private static final int COLUMN_SUBSPECIES = 2;
  private static final int COLUMN_TAXONOMIC_INDEX = 3;
  private static final int COLUMN_DATE = 4;
  private static final int COLUMN_TIME = 5;  
  private static final int COLUMN_NUMBER = 6;
  private static final int COLUMN_FEMALE = 7;
  private static final int COLUMN_MALE = 8;
  private static final int COLUMN_IMMATURE = 9;
  private static final int COLUMN_ADULT = 10;
  private static final int COLUMN_HEARD_ONLY = 11;
  private static final int COLUMN_STATUS = 12;
  private static final int COLUMN_BREEDING = 13;
  private static final int COLUMN_PHOTOGRAPHED = 14;
  private static final int COLUMN_PHOTOS = 15;
  private static final int COLUMN_DESCRIPTION = 16;
  private static final int COLUMN_LATITUDE = 17;
  private static final int COLUMN_LONGITUDE = 18;
  private static final int COLUMN_LOCATION_DESCRIPTION = 19;
  private static final int COLUMN_REGION = 20;
  private static final int COLUMN_COUNTRY = 21;
  private static final int COLUMN_STATE = 22;
  private static final int COLUMN_COUNTY = 23;
  private static final int COLUMN_CITY = 24;
  private static final int COLUMN_FIRST_LOCATION = 25;
  
  // VisitInfo column indices;  these are relative to the end
  private static final int COLUMN_VISIT_OBSERVATION_TYPE = 0;
  
  private static final ImmutableList<String> HEADER = ImmutableList.of(
          "Common",
          "Scientific",
          "Subspecies",
          "Index",
          "Date",
          "Start time",
          "Number",
          "Female",
          "Male",
          "Immature",
          "Adult",
          "Heard only",
          "Status",
          "Breeding",
          "Photographed",
          "Photos",
          "Description",
          "Latitude",
          "Longitude",
          "Location Description",
          "Region",
          "Country",
          "State",
          "County",
          "City");

  private static final ImmutableList<String> VISIT_INFO_HEADER = ImmutableList.of(
      "Observation type",
      "Duration",
      "Distance",
      "Area",
      "Party size",
      "Complete list",
      "Visit comments");
  private static final ImmutableList<String> USER_HEADER = ImmutableList.of(
      "Observers",
      "Observer Names");
  private static final Joiner LINE_JOINER = Joiner.on(System.lineSeparator());
  
  /** An empty var for simplifying logic.  Sharing a mutable var like this is poor, but at least it's private and on the instance. */
  private final SightingInfo emptySightingInfo = new SightingInfo();
  
  private final LoadingCache<ImmutableSet<User>, String> abbreviationCache = CacheBuilder.newBuilder()
      .build(new CacheLoader<ImmutableSet<User>, String>() {
        @Override
        public String load(ImmutableSet<User> users) throws Exception {
          return toAbbreviations(users);
        }
      });
  private final LoadingCache<ImmutableSet<User>, String> nameCache = CacheBuilder.newBuilder()
      .build(new CacheLoader<ImmutableSet<User>, String>() {
        @Override
        public String load(ImmutableSet<User> users) throws Exception {
          return toNames(users);
        }
      });

  public void exportReportSet(ByteSink outSupplier,
          ReportSet reportSet, Taxonomy taxonomy, QueryResults queryResults) throws IOException {
    int maximumLocationDepth = findMaximumLocationDepth(reportSet.getLocations());     
    try (ExportLines csvWriter = startCsvExport(outSupplier, reportSet, maximumLocationDepth)) {
      List<Resolved> taxa = queryResults.getTaxaAsList();
      for (Resolved taxon : taxa) {
        for (Sighting sighting : queryResults.getAllSightings(taxon)) {
          String locationId = sighting.getLocationId();
          Location location = locationId == null ? null : reportSet.getLocations().getLocation(locationId);
          VisitInfoKey visitInfoKey = VisitInfoKey.forSighting(sighting);
          VisitInfo visitInfo = visitInfoKey == null ? null : reportSet.getVisitInfo(visitInfoKey); 
          writeSighting(csvWriter, taxon, sighting, taxonomy, location, maximumLocationDepth, visitInfo);
        }
      }
    }
  }

  /** Export an explicit list of sightings. */
  public void exportExplicitSightings(ByteSink outSupplier,
      ReportSet reportSet, Taxonomy taxonomy, Iterable<Sighting> sightings) throws IOException {
    int maximumLocationDepth = findMaximumLocationDepth(reportSet.getLocations());     
    try (ExportLines csvWriter = startCsvExport(outSupplier, reportSet, maximumLocationDepth)) {
      for (Sighting sighting : sightings) {
        Resolved taxon = sighting.getTaxon().resolve(taxonomy);
        String locationId = sighting.getLocationId();
        Location location = locationId == null ? null : reportSet.getLocations().getLocation(locationId);
        VisitInfoKey visitInfoKey = VisitInfoKey.forSighting(sighting);
        VisitInfo visitInfo = visitInfoKey == null ? null : reportSet.getVisitInfo(visitInfoKey); 
        writeSighting(csvWriter, taxon, sighting, taxonomy, location, maximumLocationDepth, visitInfo);
      }
    }
  }

  private ExportLines startCsvExport(ByteSink outSupplier, ReportSet reportSet,
      int maximumLocationDepth) throws IOException {
    Writer out = new BufferedWriter(
            new OutputStreamWriter(outSupplier.openStream(), Charsets.UTF_8), 10240);
    ExportLines exportLines = CsvExportLines.fromWriter(out);
    List<String> header = Lists.newArrayList(HEADER);
    for (int i = 0; i < maximumLocationDepth; i++) {
      header.add(String.format("Location %s", i + 1));
    }
    header.addAll(VISIT_INFO_HEADER);
    if (reportSet.getUserSet() != null) {
      header.addAll(USER_HEADER);
    }
    
    exportLines.nextLine(header.toArray(new String[0]));
    return exportLines;
  }

  private void writeSighting(
      ExportLines csvWriter,
      Resolved resolved,
      Sighting sighting,
      Taxonomy taxonomy,
      @Nullable Location location,
      int maximumLocationDepth,
      VisitInfo visitInfo) throws IOException {
    List<String> output = Lists.newArrayList();
    
    if (resolved.getSmallestTaxonType() == Taxon.Type.species) {
      output.add(resolved.hasCommonName() ? resolved.getCommonName() : "");
      output.add(resolved.getFullName());
      output.add("");
    } else {
      Resolved group = resolved.getParentOfAtLeastType(Taxon.Type.group).resolveInternal(taxonomy);
      output.add(group.hasCommonName() ? group.getCommonName() : "");
      Resolved species = resolved.getParentOfAtLeastType(Taxon.Type.species).resolveInternal(taxonomy);
      output.add(species.getFullName());
      output.add(resolved.getName());
    }

    // Add the taxonomic index
    if (resolved.getType() == SightingTaxon.Type.SINGLE) {
      output.add("" + resolved.getTaxon().getTaxonomyIndex());
    } else {
      // And for hybrids/sp's, sequence immediately after the last entry with ".5".
      Taxon last = Iterables.getLast(resolved.getTaxa());
      output.add("" + last.getTaxonomyIndex() + ".5");
    }
    
    if (sighting.getDateAsPartial() == null) {
      output.add("");
    } else {
      output.add(PartialIO.toString(sighting.getDateAsPartial()));
    }
    
    if (sighting.getTimeAsPartial() == null) {
      output.add("");
    } else {
      output.add(TimeIO.toString(sighting.getTimeAsPartial()));
    }
    
    SightingInfo sightingInfo = sighting.hasSightingInfo() ? sighting.getSightingInfo() : emptySightingInfo;
    if (sightingInfo.getNumber() != null) {
      output.add(sightingInfo.getNumber().toString());
    } else {
      output.add("");
    }
    
    output.add(sightingInfo.isFemale() ? "Y" : "");
    output.add(sightingInfo.isMale() ? "Y" : "");
    output.add(sightingInfo.isImmature() ? "Y" : "");
    output.add(sightingInfo.isAdult() ? "Y" : "");
    output.add(sightingInfo.isHeardOnly() ? "Y" : "");
    output.add(sightingInfo.getSightingStatus().getExportText());
    output.add(sightingInfo.getBreedingBirdCode().getId());
    output.add(sightingInfo.isPhotographed() ? "Y" : "");
    
    List<String> photos = Lists.newArrayList();
    for (Photo photo : sightingInfo.getPhotos()) {
      photos.add(photo.getUri().toString());
    }
    output.add(LINE_JOINER.join(photos));
    
    output.add(Strings.nullToEmpty(sightingInfo.getDescription()));
    
    if (location != null && location.getLatLong().isPresent()) {
      output.add(location.getLatLong().get().latitude());
      output.add(location.getLatLong().get().longitude());
    } else {
      output.add("");
      output.add("");
    }
    
    if (location != null && location.getDescription() != null) {
      output.add(location.getDescription());
    } else {
      output.add("");
    }
    
    // Add empty strings for region/country/state/county/city.  They'll be filled in later.
    output.add("");
    output.add("");
    output.add("");
    output.add("");
    output.add("");
    
    if (location != null) {
      appendLocations(output, location);
    }
    
    // Add additional entries for any output
    while (output.size() < HEADER.size() + maximumLocationDepth) {
      output.add("");
    }
    
    ImmutableSet<User> users = sightingInfo.getUsers();
    if (visitInfo != null) {
      output.add(visitInfo.observationType().id());
      if (visitInfo.duration().isPresent()) {
        output.add("" + visitInfo.duration().get().getStandardMinutes());
      } else {
        output.add("");
      }
      
      if (visitInfo.distance().isPresent()) {
        output.add("" + visitInfo.distance().get().kilometers());
      } else {
        output.add("");
      }
      
      if (visitInfo.area().isPresent()) {
        output.add("" + visitInfo.area().get().hectares());
      } else {
        output.add("");
      }
      
      if (visitInfo.partySize().isPresent()) {
        output.add("" + visitInfo.partySize().get());
      } else {
        output.add("");        
      }
      
      output.add(visitInfo.completeChecklist() ? "Y" : "");
      // TODO: sort by visit info key, and therefore only need to output comments once?
      if (visitInfo.comments().isPresent()) {
        output.add(visitInfo.comments().get());
      } else {
        output.add("");        
      }
    } else if (!users.isEmpty()) {
      for (String header : VISIT_INFO_HEADER) {
        output.add("");        
      }
    }
    
    if (!users.isEmpty()) {
      output.add(abbreviationCache.getUnchecked(users));
      output.add(nameCache.getUnchecked(users));
    }

    csvWriter.nextLine(output.toArray(new String[0]));
  }
  
  private boolean maybeSetInFixedColumn(
      List<String> output,
      Location location,
      int columnIndex) {
    if ("".equals(output.get(columnIndex))) {
      output.set(columnIndex, location.getDisplayName());
      return true;
    }
    
    if (location.getParent() != null
        && location.getParent().getType() == location.getType()
        && output.get(columnIndex).equals(location.getParent().getDisplayName())) {
      output.set(columnIndex, location.getDisplayName());
      return true;
    }
    
    return false;
  }

  private void appendLocations(List<String> output, Location location) {
    if (location.getParent() != null) {
      appendLocations(output, location.getParent());
    }
    
    if (location.getType() != null) {
      switch (location.getType()) {
        case region:
          if (maybeSetInFixedColumn(output, location, COLUMN_REGION)) {
            return;
          }
          break;
        case country:
          if ("".equals(output.get(COLUMN_COUNTRY))) {
            output.set(COLUMN_COUNTRY, location.getDisplayName());
            return;
          } else if (location.isBuiltInLocation()
              && location.getParent().isBuiltInLocation()
              && location.getParent().getType() == Location.Type.country
              && "".equals(output.get(COLUMN_STATE))) {
            // Special hack for the UK:  when one built-in country is
            // inside another built-in country, try to treat the inner
            // one as a "state".
            output.set(COLUMN_STATE, location.getDisplayName());
            return;
          }
          break;
        case state:
          if ("".equals(output.get(COLUMN_STATE))) {
            output.set(COLUMN_STATE, location.getDisplayName());
            return;
          }
          break;
        case county:
          if ("".equals(output.get(COLUMN_COUNTY))) {
            output.set(COLUMN_COUNTY, location.getDisplayName());
            return;
          }
          break;
        case city:
        case town:
          if ("".equals(output.get(COLUMN_CITY))) {
            output.set(COLUMN_CITY, location.getDisplayName());
            return;
          }
          break;
        default:
          break;
      }
    }
    output.add(location.getDisplayName());
  }
  
  private int findMaximumLocationDepth(LocationSet locations) {
    int maxDepth = 0;
    for (Location location : locations.rootLocations()) {
      maxDepth = Math.max(maxDepth, findMaximumLocationDepth(
          location, 0, false, false, false, false));
    }
    return maxDepth;
  }

  private int findMaximumLocationDepth(
      Location location,
      int currentDepth,
      boolean foundRegion,
      boolean foundCountry,
      boolean foundState,
      boolean foundCounty) {
    if (location.getType() != null) {
      switch (location.getType()) {
        case region:
          if (!foundRegion) {
            foundRegion = true;
          } else {
            currentDepth++;
          }
          break;
        case country:
          if (!foundCountry) {
            foundCountry = true;
          } else {
            currentDepth++;
          }
          break;
        case state:
          if (!foundState) {
            foundState = true;
          } else {
            currentDepth++;
          }
          break;
        case county:
          if (!foundCounty) {
            foundCounty = true;
          } else {
            currentDepth++;
          }
          break;
        default:
          currentDepth++;
      }
    } else {
      currentDepth++;
    }
    
    int maxDepth = currentDepth;
    for (Location child : location.contents()) {
      maxDepth = Math.max(maxDepth, findMaximumLocationDepth(
          child, currentDepth, foundRegion, foundCountry, foundState, foundCounty));
    }
    return maxDepth;
  }

  private static String toAbbreviations(ImmutableSet<User> users) {
    return users.stream()
        .map(User::abbreviation)
        .filter(Predicates.notNull())
        .collect(Collectors.joining(","));
  }

  private static String toNames(ImmutableSet<User> users) {
    return users.stream()
        .map(User::name)
        .filter(Predicates.notNull())
        .collect(Collectors.joining("\n"));
  }

}
