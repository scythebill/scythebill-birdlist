/*   
 * Copyright 2020 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.export;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.joda.time.DateTimeFieldType;
import org.joda.time.ReadablePartial;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.io.ByteSink;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Taxon;

/**
 * Output sightings to a CSV file in the iNaturalist CSV format
 * See https://www.inaturalist.org/observations/import#csv_import
 */
public class INaturalistCsvExport {
  /** Use a constant pattern, irrespective of locale. */
  private static final DateTimeFormatter DATE_FORMATTER =
      new DateTimeFormatterBuilder()
          .appendYear(4, 4)
          .appendLiteral('-')
          .appendMonthOfYear(2)
          .appendLiteral('-')
          .appendDayOfMonth(2)
          .toFormatter();
  /** Use a constant pattern, irrespective of locale. */
  private static final DateTimeFormatter TIME_FORMATTER =
      new DateTimeFormatterBuilder()
          .appendHourOfDay(2)
          .appendLiteral(':')
          .appendMinuteOfHour(2)
          .toFormatter();
  
  /** Maximum number of rows */
  private static final long MAX_ROWS = 9999;

  private static final String[] COLUMN_HEADERS = new String[] {
    "Taxon name",
    "Date observed",
    "Description",
    "Place name",
    "Latitude",
    "Longitude",
    "Tags",
    "Geoprivacy"  
  };
  
  private static final int TOTAL_COLUMN_COUNT = COLUMN_HEADERS.length;
  private static final int COLUMN_TAXON_NAME = 0;
  private static final int COLUMN_DATE = 1;
  private static final int COLUMN_DESCRIPTION = 2;
  private static final int COLUMN_PLACE_NAME = 3;
  private static final int COLUMN_LATITUDE = 4;
  private static final int COLUMN_LONGITUDE = 5;
  private static final int COLUMN_TAGS = 6;
  private static final int COLUMN_GEOPRIVACY = 7;

  public INaturalistCsvExport() {
  }

  public void writeSpeciesList(ByteSink outSupplier,
      Iterator<Resolved> taxonIterator,
      QueryResults queryResults,
      ReportSet reportSet) throws IOException {
    Writer out = new BufferedWriter(
        new OutputStreamWriter(outSupplier.openStream(), Charsets.UTF_8), 10240);
    try (ExportLines exportLines = CsvExportLines.fromWriter(out)) {
      writeSpeciesList(exportLines, taxonIterator, queryResults, reportSet);
    }
  }

  /** Records must have at least day/month/year. */
  private static boolean isValidDate(ReadablePartial partial) {
    return partial != null
         && partial.isSupported(DateTimeFieldType.year())
         && partial.isSupported(DateTimeFieldType.monthOfYear())
         && partial.isSupported(DateTimeFieldType.dayOfMonth());
  }

  private void writeSpeciesList(ExportLines exportLines, Iterator<Resolved> taxonIterator,
      QueryResults queryResults, ReportSet reportSet)
      throws IOException {
    exportLines.nextLine(COLUMN_HEADERS);
    
    int rowCount = 0;
    while (taxonIterator.hasNext()) {
      Resolved taxon = taxonIterator.next();
      Collection<Sighting> taxonSightings = queryResults.getAllSightings(taxon);
            
      for (Sighting sighting : taxonSightings) {
        String[] line = new String[TOTAL_COLUMN_COUNT];
        if (taxon.getLargestTaxonType() == Taxon.Type.group
            && !taxon.getTaxa().iterator().next().getContents().isEmpty()) {
          taxon = taxon.getParentOfAtLeastType(Taxon.Type.species).resolveInternal(taxon.getTaxonomy());
        }
        
        line[COLUMN_TAXON_NAME] = taxon.getFullName();
        
        List<String> names = new ArrayList<>();
        if (sighting.getLocationId() != null) {
          Location location = reportSet.getLocations().getLocation(sighting.getLocationId());
          // Skip all private locations
          if (location.isPrivate()) {
            continue;
          }

          Location state = Locations.getAncestorOfType(location, Location.Type.state);
          Location country = Locations.getAncestorOfType(location, Location.Type.country);
          Location city = Locations.getAncestorOfType(location, Location.Type.city);
          Location town = Locations.getAncestorOfType(location, Location.Type.town);

          names.add(location.getDisplayName());
          if (town != null && town != location) {
            names.add(town.getDisplayName());
          }
          if (city != null && city != location) {
            names.add(city.getDisplayName());
          }
          if (state != null && state != location) {
            names.add(state.getDisplayName());
          }
          if (country != null && country != location) {
            names.add(country.getDisplayName());
          }
          line[COLUMN_PLACE_NAME] = Joiner.on(", ").join(names);
          if (location.getLatLong().isPresent()) {
            LatLongCoordinates latLong = location.getLatLong().get();
            line[COLUMN_LATITUDE] = String.format(Locale.US, "%2.4f", latLong.latitudeAsDouble());
            line[COLUMN_LONGITUDE] = String.format(Locale.US, "%2.4f", latLong.longitudeAsDouble());
          }
        }
        
        if (isValidDate(sighting.getDateAsPartial())) {
          line[COLUMN_DATE] = DATE_FORMATTER.print(sighting.getDateAsPartial());
          if (sighting.getTimeAsPartial() != null) {
            line[COLUMN_DATE] += " " + TIME_FORMATTER.print(sighting.getTimeAsPartial());
          }
        }

        if (sighting.hasSightingInfo()) {
          SightingInfo sightingInfo = sighting.getSightingInfo();
          if (sightingInfo.getDescription() != null) {
            line[COLUMN_DESCRIPTION] = trimLatLong(replaceDisallowedCharacters(sightingInfo.getDescription()));

            // If the description has a trailing lat/long, then replace the general location lat/long
            LatLongCoordinates latLong = returnLatLong(sightingInfo.getDescription());
            if (latLong != null) {
              line[COLUMN_LATITUDE] = String.format(Locale.US, "%2.4f", latLong.latitudeAsDouble());
              line[COLUMN_LONGITUDE] = String.format(Locale.US, "%2.4f", latLong.longitudeAsDouble());
            }
          }
          
          List<String> tags = new ArrayList<>();
          if (sightingInfo.isHeardOnly()) {
            tags.add("heardonly");
          }
          if (sightingInfo.isFemale()) {
            tags.add("female");
          }
          if (sightingInfo.isMale()) {
            tags.add("male");
          }
          if (sightingInfo.isImmature()) {
            tags.add("immature");
          }
          if (sightingInfo.isPhotographed()) {
            tags.add("photographed");
          }
          line[COLUMN_TAGS] = Joiner.on(',').join(tags);
        }
        
        // TODO: support choosing a geoprivacy?
        line[COLUMN_GEOPRIVACY] = "";
        
        exportLines.nextLine(line);
      }

      // Stop iterating when the row count is excessive
      if (rowCount++ >= MAX_ROWS) {
        return;
      }
    }
  }

  /**
   * eBird disallows double-quotes and hard-returns.  Replace double-quotes
   * with single-quotes, and hard returns with single spaces.
   */
  private String replaceDisallowedCharacters(String in) {
    return in.replace('\n', ' ').replace('\r', ' ').replace('"', '\'');
  }

  private final static CharMatcher LAT_LONG_CHARS = CharMatcher.anyOf("-.0123456789,");
  private final static CharMatcher NOT_LAT_LONG_CHARS = LAT_LONG_CHARS.negate().or(CharMatcher.whitespace());
  
  /**
   * BirdLasser and Observado imports add "LL:" with a lat-long.  This is inappropriate to send
   * to eBird;  trim it (and trim multiple instances if multiple sightings get appended to one).
   */
  private String trimLatLong(String in) {
    while (true) {
      int lastLongStart = in.lastIndexOf("LL:");
      if (lastLongStart < 0) {
        break;
      }
      
      int endOfLatLong = NOT_LAT_LONG_CHARS.indexIn(in, lastLongStart + 3);
      if (endOfLatLong == lastLongStart + 3) {
        break;
      }
      
      if (endOfLatLong < 0) {
        in = in.substring(0, lastLongStart);
      } else {
        in = in.substring(0, lastLongStart) + in.substring(endOfLatLong);
      }
      in = CharMatcher.whitespace().trimFrom(in);
    }
    
    return in;
  }
  
  private LatLongCoordinates returnLatLong(String in) {
    int lastLongStart = in.lastIndexOf("LL:");
    if (lastLongStart < 0) {
      return null;
    }
    
    String latLong = in.substring(lastLongStart + 3);
    if (!LAT_LONG_CHARS.matchesAllOf(latLong)) {
      return null;
    }
    
    List<String> split = Splitter.on(',').splitToList(latLong);
    if (split.size() != 2) {
      return null;
    }
    
    try {
      return LatLongCoordinates.withLatAndLong(split.get(0), split.get(1));
    } catch (IllegalArgumentException e) {
      return null;
    }
  }
}
