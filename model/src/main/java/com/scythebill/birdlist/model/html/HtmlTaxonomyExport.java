/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.html;

import java.io.IOException;
import java.io.Writer;

import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Rudimentary HTML export of a taxonomy.
 */
public class HtmlTaxonomyExport
{
  static public void export(Writer out, Taxonomy taxonomy) throws IOException
  {
    out.write("<html>\n");
    out.write("<head>\n");
    out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
    out.write("</head>\n");
    out.write("<body>\n");
    _export(out, taxonomy.getRoot());
    out.write("</body>\n");
    out.write("</html>\n");
  }
  
  static public void _export(Writer out, Taxon taxon) throws IOException
  {
     out.write("<span class='type'>");
     out.write(taxon.getType().getName());
     out.write("</span>\n");

     out.write("<span class='sci'>");
     out.write(taxon.getName());
     out.write("</span>\n");
     
     if (taxon.getCommonName() != null)
     {
       out.write("<span class='com'>");
       out.write(taxon.getCommonName());
       out.write("</span>\n");
     }
     
     if (taxon instanceof Species)
     {
       Species species = (Species) taxon;
       if (species.getRange() != null)
       {
         out.write("<span class='range'>(");
         out.write(species.getRange());
         out.write(")</span>");
       }
     }
     
     if (!taxon.getContents().isEmpty())
     {
       out.write("<ol>");
       for (Taxon child : taxon.getContents())
       {
         out.write("<li>");
         _export(out, child);
         out.write("</li>");
       }
       
       out.write("</ol>");
     }
  }
}
