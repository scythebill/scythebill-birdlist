/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.io;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Arrays;

import com.opencsv.CSVWriter;

/**
 * Base implementation of ExportLines for CSV sources.  Eliminates a couple
 * of oddities of CSVWriter.
 */
public class CsvExportLines implements ExportLines {
  private final CSVWriter csvWriter;

  public static ExportLines fromWriter(Writer writer) throws IOException {
    return new CsvExportLines(writer);
  }

  public static ExportLines toFile(File out, Charset charset) throws IOException {
    return fromWriter(new OutputStreamWriter(
        new BufferedOutputStream(new FileOutputStream(out)),
        charset));
  }

  private CsvExportLines(Writer writer) throws IOException {
    // Use CSVWriter.DEFAULT_ESCAPE_CHARACTER for escapes - which is a double-quote.
    // This means that quotes are escaped as "" not \", which is Excel-compatible.
    csvWriter = new CSVWriter(writer, CSVWriter.DEFAULT_SEPARATOR,
        CSVWriter.DEFAULT_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER);
  }

  @Override
  public void close() throws IOException {
    csvWriter.close();
  }

  @Override
  public void nextLine(String[] line) throws IOException {
    // Optimize a bit by changing "" to null, which saves on storage
    // and should make reading less expensive
    if (Arrays.asList(line).contains("")) {
      line = line.clone();
      for (int i = 0; i < line.length; i++) {
        if ("".equals(line[i])) {
          line[i] = null;
        }
      }
    }
    csvWriter.writeNext(line);
  }

}
