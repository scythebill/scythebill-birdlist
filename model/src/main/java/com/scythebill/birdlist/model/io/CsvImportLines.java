/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.io;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.io.input.BOMInputStream;

import com.google.common.base.CharMatcher;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.opencsv.CSVParser;
import com.opencsv.CSVReader;

/**
 * Base implementation of ImportLines for CSV sources.  Eliminates a couple
 * of oddities of CSVReader.
 */
public class CsvImportLines {
  public static ImportLines fromFile(File file, Charset charset) throws IOException {
    if (file.getName().endsWith(".zip")) {
      Set<Path> paths = new LinkedHashSet<>();
      FileSystems.newFileSystem(file.toPath(), ImmutableMap.of()).getRootDirectories()
          .forEach(root -> {
            try {
              Files.walk(root)
                   .filter(path -> path.toString().endsWith(".csv"))
                  .forEach(path -> paths.add(path));
            } catch (IOException e) {
            }
          });
      if (paths.isEmpty()) {
        throw new IOException(
            ".zip file does not contain any .csv files.");
      }
      if (paths.size() > 1) {
        throw new IOException(".zip file contains multiple .csv files.");
      }
      
      return new OpenCsvImportLines(Files.newInputStream(Iterables.getOnlyElement(paths)), charset);
    }
    return new OpenCsvImportLines(new FileInputStream(file), charset);
  }
  
  public static ImportLines fromUrl(URL url, Charset charset) throws IOException {
    return new OpenCsvImportLines(url.openStream(), charset);
  }
  
  public static ImportLines fromReader(Reader reader) throws IOException {
    return new OpenCsvImportLines(reader);
  }

  public static ImportLines fromReader(CSVReader csvReader) {
    return new OpenCsvImportLines(csvReader);
  }

  /** 
   * Generates an ImportLines reader that will never try to concatenate multiple lines of
   * text, no matter how badly the escaping is mangled.
   */
  public static ImportLines fromFileNoMulti(File file, Charset charset) throws IOException {
    ClonedCsvReader reader = new ClonedCsvReader(
        new InputStreamReader(new BufferedInputStream(new FileInputStream(file)), charset),
        ClonedCsvParser.DEFAULT_SEPARATOR,
        ClonedCsvParser.DEFAULT_QUOTE_CHARACTER,
        (char) 0); // Disable any notion of "escape" characters for "no multi"
    reader.disableMultiLineReads();
    return new ClonedCsvImportLines(reader);
  }
  
  /** 
   * Generates an ImportLines reader that will never try to concatenate multiple lines of
   * text, no matter how badly the escaping is mangled.
   * <p>
   * Also ignores quotes not followed by separators, for parsers that completely
   * fail to escape quotes properly.
   */
  public static ImportLines fromFileNoMultiIgnoringQuotesNotFollowedBySeparators(File file, Charset charset) throws IOException {
    ClonedCsvReader reader = new ClonedCsvReader(
        new InputStreamReader(new BufferedInputStream(new FileInputStream(file)), charset),
        ClonedCsvParser.DEFAULT_SEPARATOR,
        ClonedCsvParser.DEFAULT_QUOTE_CHARACTER,
        (char) 0); // Disable any notion of "escape" characters for "no multi"
    reader.disableMultiLineReads();
    reader.ignoreQuotesNotFollowedBySeparator();
    return new ClonedCsvImportLines(reader);
  }
  

  static class ClonedCsvImportLines extends AbstractImportLines {
    private final ClonedCsvReader reader;
    private int lineNumber = 0;

    ClonedCsvImportLines(ClonedCsvReader reader) {
      this.reader = reader;
    }

    @Override
    public void close() throws IOException {
      reader.close();
    }

    @Override
    protected String[] readNext() throws IOException {
      lineNumber++;
      return reader.readNext();
    }

    @Override
    public int lineNumber() {
      return lineNumber;
    }
  }
  
  static class OpenCsvImportLines extends AbstractImportLines {
    private final CSVReader csvReader;
    private int lineNumber = 0;

    private OpenCsvImportLines(InputStream stream, Charset charset) throws IOException {
      csvReader = fromBufferedReader(new InputStreamReader(
          new BOMInputStream(new BufferedInputStream(stream)), charset));
    }

    private OpenCsvImportLines(Reader reader) throws IOException {
      csvReader = fromBufferedReader(new BufferedReader(reader));
    }

    protected OpenCsvImportLines(CSVReader csvReader) {
      this.csvReader = csvReader;
    }

    @Override
    public void close() throws IOException {
      csvReader.close();
    }

    @Override
    protected String[] readNext() throws IOException {
      lineNumber++;
      return csvReader.readNext();
    }

    @Override
    public int lineNumber() {
      return lineNumber;
    }
    
    /**
     * Create a CSVReader for a reader which is already buffered (either at the reader level or InputStream)
     */
    private static CSVReader fromBufferedReader(Reader reader) {
      // Use a default separator (comma) and quote character (double-quote) but do not use any escape character.
      // We write using " as an escape - but OpenCSV supports double-double-quotes as an escape pattern automatically
      // (per RFC 4180).  If we use the default OpenCSV escape character (backslash), then backslashes will not survive
      // the export-import roundtrip.  By nulling out, only the "" escape works, and backslashes survive.  The downside
      // is that any export (including earlier Scythebill) that uses backslash means that escaped quotes get imported
      // as \" instead of just ", and escaped backslashes as \\ instead of \.
      return new CSVReader(reader, CSVParser.DEFAULT_SEPARATOR, CSVParser.DEFAULT_QUOTE_CHARACTER, CSVParser.NULL_CHARACTER);
    }
  }

  static abstract class AbstractImportLines implements ImportLines {
    private boolean withoutTrimming;

    @Override
    public String[] nextLine() throws IOException {
      while (true) {
        String[] next = readNext();
        if (next == null) {
          return null;
        }

        // Skip over "empty" lines returned when CSVReader encounters unexpected LFs,
        // or for entirely empty lines produced by CSV-ing empty content
        if (lineIsEmpty(next)) {
          continue;
        }
        
        // Work around for http://sourceforge.net/p/opencsv/bugs/85/
        if ("\"".equals(next[0])) {
          next[0] = "";
        }
        
        // Strip a BOM if present;  some eeevil software will add this randomly to files.
        if (next[0].length() > 0 && next[0].charAt(0) == '\uFEFF') {
          next[0] = next[0].substring(1);
        }
        return trimWhitespace(next);
      }
    }

    
    @Override
    public ImportLines withoutTrimming() {
      this.withoutTrimming = true;
      return this;
    }
    
    protected abstract String[] readNext() throws IOException;
    
    private boolean lineIsEmpty(String[] next) {
      for (String value : next) {
        if (!value.isEmpty()) {
          return false;
        }
      }
      return true;
    }

    private String[] trimWhitespace(String[] next) {
      if (!withoutTrimming) {
        for (int i = 0; i < next.length; i++) {
          next[i] = CharMatcher.whitespace().trimFrom(next[i]);
        }
      }
      return next;
    }
    
  }
}
