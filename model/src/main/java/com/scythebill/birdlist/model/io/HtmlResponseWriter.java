/* The following class is modified from an original in the Apache MyFaces project. */
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package com.scythebill.birdlist.model.io;

import java.io.IOException;
import java.io.Writer;

import com.google.common.html.HtmlEscapers;

/**
 * ResponseWriter capable of outputting HTML-formatted content.
 */
public class HtmlResponseWriter extends ResponseWriter {
  static private final String[][] emptyElementArr = new String[256][];

  static private final String[] aNames = new String[] { "area", };

  static private final String[] bNames = new String[] { "br", "base", "basefont", };

  static private final String[] cNames = new String[] { "col", };

  static private final String[] fNames = new String[] { "frame", };

  static private final String[] hNames = new String[] { "hr", };

  static private final String[] iNames = new String[] { "img", "input", "isindex", };

  static private final String[] lNames = new String[] { "link", };

  static private final String[] mNames = new String[] { "meta", };

  static private final String[] pNames = new String[] { "param", };

  static {
    emptyElementArr['a'] = aNames;
    emptyElementArr['A'] = aNames;
    emptyElementArr['b'] = bNames;
    emptyElementArr['B'] = bNames;
    emptyElementArr['c'] = cNames;
    emptyElementArr['C'] = cNames;
    emptyElementArr['f'] = fNames;
    emptyElementArr['F'] = fNames;
    emptyElementArr['h'] = hNames;
    emptyElementArr['H'] = hNames;
    emptyElementArr['i'] = iNames;
    emptyElementArr['I'] = iNames;
    emptyElementArr['l'] = lNames;
    emptyElementArr['L'] = lNames;
    emptyElementArr['m'] = mNames;
    emptyElementArr['M'] = mNames;
    emptyElementArr['p'] = pNames;
    emptyElementArr['P'] = pNames;
  }

  private final Writer _out;
  private final String _encoding;
  private boolean _closeStart;

  public HtmlResponseWriter(Writer writer, String encoding) {
    _out = writer;
    _encoding = encoding;
  }

  public String getCharacterEncoding() {
    return _encoding;
  }

  public String getContentType() {
    return "text/html";
  }

  @Override public void startDocument() throws IOException {
    _out.write("<!DOCTYPE html>");
    _out.write(System.getProperty("line.separator"));
  }

  @Override public void endDocument() throws IOException {
  }

  @Override public void startElement(String name, Object objectFor)
      throws IOException {
    closeStartIfNecessary();

    Writer out = _out;
    out.write('<');
    out.write(name);
    _closeStart = true;
  }

  @Override public void writeAttribute(String name, Object value)
      throws IOException {
    if (value == null)
      return;

    Writer out = _out;
    // write the attribute value
    out.write(' ');
    out.write(name);
    out.write("=\"");
    writeAttributeText(value);
    out.write("\"");
  }

  @Override public void writeComment(Object comment) throws IOException {
    if (comment != null) {
      closeStartIfNecessary();

      Writer out = _out;
      out.write("<!-- ");
      // TODO: this does not actually escape the text at all.
      out.write(comment.toString());
      out.write(" -->");
    }
  }

  @Override public void writeText(char[] buffer, int offset, int length)
      throws IOException {
    if (buffer != null) {
      closeStartIfNecessary();
      _writeBodyText(buffer, offset, length);
    }
  }

  @Override public void writeText(Object text) throws IOException {
    if (text != null) {
      closeStartIfNecessary();
      // _openCDATAIfNecessary();
      writeBodyText(text);
    }
  }

  @Override public void writeRawText(String text) throws IOException {
    if (text != null) {
      closeStartIfNecessary();
      _out.write(text);
    }    
  }

  @Override public void endElement(String name) throws IOException {
    Writer out = _out;
    if (_closeStart) {
      out.write('>');
      _closeStart = false;

      boolean isEmptyElement = _isEmptyElement(name);
      if (isEmptyElement) {
        return;
      }
    }

    // _closeCDATAIfNecessary();
    out.write("</");
    out.write(name);
    out.write(">");
  }

  @Override public ResponseWriter cloneWithWriter(Writer writer) {
    return new HtmlResponseWriter(writer, getCharacterEncoding());
  }

  @Override public void write(char[] cbuf, int off, int len) throws IOException {
    closeStartIfNecessary();
    _out.write(cbuf, off, len);
  }

  @Override public void write(char[] cbuf) throws IOException {
    closeStartIfNecessary();
    _out.write(cbuf);
  }

  @Override public void write(int c) throws IOException {
    closeStartIfNecessary();
    _out.write(c);
  }

  @Override public void write(String str) throws IOException {
    closeStartIfNecessary();
    _out.write(str);
  }

  @Override public void write(String str, int off, int len) throws IOException {
    closeStartIfNecessary();
    _out.write(str, off, len);
  }

  @Override public void close() throws IOException {
    _out.close();
  }

  @Override public void flush() throws IOException {
    _out.flush();
  }

  protected void writeAttributeText(Object text) throws IOException {
    char[] buffer = text.toString().toCharArray();
    _writeAttributeText(buffer, 0, buffer.length);
  }

  protected void writeBodyText(Object text) throws IOException {
    char[] buffer = text.toString().toCharArray();
    _writeBodyText(buffer, 0, buffer.length);
  }

  protected void closeStartIfNecessary() throws IOException {
    if (_closeStart) {
      Writer out = _out;
      out.write('>');
      _closeStart = false;
    }
  }

  /*
   * private void _openCDATAIfNecessary() throws IOException { if (!_closeCDATA) {
   * Writer out = _out; out.write("<![CDATA["); _closeCDATA = true; } }
   * 
   * private void _closeCDATAIfNecessary() throws IOException { if (_closeCDATA) {
   * Writer out = _out; out.write("]]>"); _closeCDATA = false; } }
   */

  private void _writeAttributeText(char[] text, int start, int length)
      throws IOException {
    Writer out = _out;
    int end = start + length;
    for (int i = start; i < end; i++) {
      char ch = text[i];

      if (ch <= 0x7f) {
        switch (ch) {
        case '>':
          out.write("&gt;");
          break;
        case '<':
          out.write("&lt;");
          break;
        case '"':
          out.write("&quot;");
          break;
        case '&':
          out.write("&amp;");
          break;
        default:
          out.write(ch);
          break;
        }
      } else {
        _writeHexRef(ch);
      }
    }
  }

  public static String htmlEscape(String text) {
    return HtmlEscapers.htmlEscaper().escape(text);
  }
  
  private void _writeBodyText(char[] text, int start, int length)
      throws IOException {
    Writer out = _out;
    int end = start + length;
    for (int i = start; i < end; i++) {
      char ch = text[i];

      if (ch <= 0x7f) {
        switch (ch) {
        case '>':
          out.write("&gt;");
          break;
        case '<':
          out.write("&lt;");
          break;
        case '&':
          out.write("&amp;");
          break;
        default:
          out.write(ch);
          break;
        }
      } else {
        _writeHexRef(ch);
      }
    }
  }

  private void _writeHexRef(char ch) throws IOException {
    Writer out = _out;
    out.write("&#x");
    // =-=AEW Could easily be more efficient.
    out.write(Integer.toHexString(ch));
    out.write(';');
  }

  static private boolean _isEmptyElement(String name) {
    // =-=AEW Performance? Certainly slower to use a hashtable,
    // at least if we can't assume the input name is lowercased.
    String[] array = emptyElementArr[name.charAt(0)];
    if (array != null) {
      for (int i = array.length - 1; i >= 0; i--) {
        if (name.equalsIgnoreCase(array[i]))
          return true;
      }
    }

    return false;
  }
}
