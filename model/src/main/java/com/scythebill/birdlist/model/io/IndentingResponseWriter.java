/* The following class is modified from an original in the Apache MyFaces project. */
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package com.scythebill.birdlist.model.io;

import java.io.IOException;
import java.io.Writer;

public class IndentingResponseWriter extends ResponseWriter {
  private ResponseWriter decorated;
  private boolean justEndedElement;
  private int elementDepth;
  private int spacesPer;

  static private final int MAX_INDENT = 50;
  static private final int DEFAULT_SPACES_PER_LEVEL = 2;
  static private final String LINE_SEPARATOR = System.getProperty("line.separator");

  static private final char[] spaces;
  static {
    spaces = new char[MAX_INDENT];

    for (int i = 0; i < MAX_INDENT; i++) {
      spaces[i] = ' ';
    }
  }

  public IndentingResponseWriter(ResponseWriter decorated) {
    this(decorated, DEFAULT_SPACES_PER_LEVEL);
  }

  public IndentingResponseWriter(ResponseWriter decorated, int spacesPerLevel) {
    this.decorated = decorated;
    this.spacesPer = spacesPerLevel;
  }

  protected ResponseWriter getResponseWriter() {
    return decorated;
  }

  /**
   * Creates a new instance of this DebugResponseWriter, using a different
   * Writer.
   */
  @Override public ResponseWriter cloneWithWriter(Writer writer) {
    return new IndentingResponseWriter(getResponseWriter().cloneWithWriter(
        writer));
  }

  @Override public void startElement(String name, Object objectFor)
      throws IOException {
    int depth = this.elementDepth++;
    if (depth > 0) {
      _writeIndent(depth);
    }

    justEndedElement = false;
    getResponseWriter().startElement(name, objectFor);
  }

  @Override public void endElement(String name) throws IOException {
    elementDepth--;
    _seeIfJustEndedElement();
    getResponseWriter().endElement(name);

    justEndedElement = true;
  }

  /**
   * Writes a comment.
   */
  @Override public void writeComment(Object comment) throws IOException {
    // start a new line only if an element just ended and
    // it wasn't a special case element
    _seeIfJustEndedElement();

    getResponseWriter().writeComment(comment);
    getResponseWriter().writeText(LINE_SEPARATOR);
  }

  /**
   * Writes a String, escaped properly for this method.
   */
  @Override public void writeText(Object text) throws IOException {
    _seeIfJustEndedElement();
    getResponseWriter().writeText(text);
  }

  /**
   * Writes a character array, escaped properly for this method.
   */
  @Override public void writeText(char[] text, int start, int length)
      throws IOException {
    _seeIfJustEndedElement();
    getResponseWriter().writeText(text, start, length);
  }

  /**
   * Writes a string, without performing any escaping.
   */
  @Override public void write(String text) throws IOException {
    _seeIfJustEndedElement();
    getResponseWriter().write(text);
  }

  /**
   * Writes a character array, without performing any escaping.
   */
  @Override public void write(char[] text, int start, int length)
      throws IOException {
    _seeIfJustEndedElement();
    getResponseWriter().write(text, start, length);
  }

  /**
   * Writes a character, without performing any escaping.
   */
  @Override public void write(int c) throws IOException {
    _seeIfJustEndedElement();
    getResponseWriter().write(c);
  }


  @Override public void writeRawText(String text) throws IOException {
    getResponseWriter().writeRawText(text);
  }
  
  @Override public void endDocument() throws IOException {
    getResponseWriter().endDocument();
  }

  @Override public void startDocument() throws IOException {
    getResponseWriter().startDocument();
  }

  @Override public void writeAttribute(String name, Object value)
      throws IOException {
    getResponseWriter().writeAttribute(name, value);
  }

  @Override public void close() throws IOException {
    getResponseWriter().close();
  }

  @Override public void flush() throws IOException {
    getResponseWriter().flush();
  }

  private void _seeIfJustEndedElement() throws IOException {
    if (justEndedElement) {
      justEndedElement = false;
      _writeIndent(elementDepth);
    }
  }

  private void _writeIndent(int depth) throws IOException {
    depth = depth * spacesPer;
    if (depth > MAX_INDENT)
      depth = MAX_INDENT;

    getResponseWriter().write(LINE_SEPARATOR);

    // If depth goes negative, this isn't just a no-op, it's
    // death.
    if (depth > 0)
      getResponseWriter().write(spaces, 0, depth);

  }
}
