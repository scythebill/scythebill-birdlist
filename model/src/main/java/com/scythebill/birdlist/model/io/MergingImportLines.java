/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.io;

import java.io.IOException;

import com.google.common.base.Preconditions;

/**
 * Supports merging adjacent lines as needed to support line-by-line parsing.
 */
public abstract class MergingImportLines implements ImportLines {

  private final ImportLines importLines;
  private String[] readAhead = null;
  private boolean needToSkipOneMerge;

  protected MergingImportLines(ImportLines importLines) {
    this.importLines = importLines;
    needToSkipOneMerge = neverMergeFirstLine();
  }

  @Override
  public void close() throws IOException {
    importLines.close();
  }

  @Override
  public ImportLines withoutTrimming() {
    importLines.withoutTrimming();
    return this;
  }
  
  @Override
  public int lineNumber() {
    // Not exactly right;  should account for read-ahead.
    return importLines.lineNumber();
  }

  @Override
  public String[] nextLine() throws IOException {
    // If we need to skip a merge, just do a read-internal and directly return.
    if (needToSkipOneMerge) {
      needToSkipOneMerge = false;
      return nextLineInternal();
    }
    
    String[] mergedLine = null;
    while (true) {
      String[] line = readAhead();
      if (line == null) {
        return mergedLine;
      } else if (mergedLine == null) {
        mergedLine = line;
      } else {
        if (!merge(mergedLine, line)) {
          // Found a new line that should not be merged - push it back
          pushBack(line);
          return mergedLine;
        }
      }
    }
  }
  
  /**
   * Checks if {@code nextLine} should be merged into {@code mergedLine}.
   * If yes, then modify mergedLine to handle, and return true;
   * If no, then do nothing to mergedLine, and return false.
   * @param mergedLine
   * @param nextLine
   * @return
   */
  protected abstract boolean merge(String[] mergedLine, String[] nextLine);

  /**
   * Read the next line;  custom importers can override if any sanitization is necessary.
   */
  protected String[] nextLineInternal() throws IOException {
    return importLines.nextLine();
  }
  
  private String[] readAhead() throws IOException {
    if (readAhead != null) {
      String[] temp = readAhead;
      readAhead = null;
      return temp;
    }
    
    return nextLineInternal();
  }
  
  /** Merging the first line is problematic, if it might contain the header, and this forces a read-ahead. */
  protected boolean neverMergeFirstLine() {
    return true;
  }
  
  private void pushBack(String[] nextLine) {
    Preconditions.checkState(readAhead == null, "Already have line in readAhead");
    readAhead = nextLine;
  }
}
