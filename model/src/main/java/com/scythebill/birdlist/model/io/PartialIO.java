/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.io;

import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import org.joda.time.DateTimeFieldType;
import org.joda.time.LocalDate;
import org.joda.time.Partial;
import org.joda.time.ReadablePartial;
import org.joda.time.chrono.GJChronology;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatterBuilder;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

/**
 * IO utilities for handling Joda-Time Partial objects.
 */
public class PartialIO {
  private static final GJChronology GJ_CHRONOLOGY = GJChronology.getInstance();
  private static final char SEPARATOR = '-';
  /** Map of abbrev. character to type */
  private static Map<Character, DateTimeFieldType> ABBREVIATIONS_TO_FIELD =
    createAbbreviationsToFieldMap();
  /** Map of field name to abbrev. */
  private static Map<String, Character> FIELD_ABBREVIATIONS = createFieldAbbreviationsMap();
  /** Map of assumed type to next assumed type */
  private static Map<DateTimeFieldType, DateTimeFieldType> NEXT_FIELD_MAP = 
    createNextFieldMap();

  /**
   * Shows a user-formatted version of a Partial.  
   */
  public static String toUserString(ReadablePartial p, Locale locale) {
    DateTimeFormatterBuilder builder = new DateTimeFormatterBuilder();
    if (p.isSupported(DateTimeFieldType.dayOfMonth())) {
      builder.appendDayOfMonth(1);
      builder.appendLiteral(' ');
    }

    if (p.isSupported(DateTimeFieldType.monthOfYear())) {
      builder.appendShortText(DateTimeFieldType.monthOfYear());
      builder.appendLiteral(' ');
    }
    
    if (p.isSupported(DateTimeFieldType.year())) {
      builder.appendYear(2, 2);
    }
    
    StringBuffer buffer = new StringBuffer();
    builder.toPrinter().printTo(buffer, p, locale);
    return buffer.toString();
  }
  
  /**
   * To a user string, in a short form.
   * TODO: aside from a full day-month-year, this is not
   * internationalized properly.
   */
  public static String toShortUserString(ReadablePartial p, Locale locale) {
    DateTimeFormatterBuilder builder;
    if (p.isSupported(DateTimeFieldType.dayOfMonth())) {
      if (p.isSupported(DateTimeFieldType.monthOfYear())
          && p.isSupported(DateTimeFieldType.year())) {
        return DateTimeFormat.shortDate().withLocale(locale).print(p);
      } else {
        builder = new DateTimeFormatterBuilder();
        builder.appendDayOfMonth(1);
        builder.appendLiteral('/');
        if (p.isSupported(DateTimeFieldType.monthOfYear())) {
          builder.appendMonthOfYear(1);
        }
        builder.appendLiteral('/');
        if (p.isSupported(DateTimeFieldType.year())) {
          builder.appendYear(2, 4);
        }
      }
    } else {
      builder = new DateTimeFormatterBuilder();
      boolean hasField = false;
      if (p.isSupported(DateTimeFieldType.monthOfYear())) {
        builder.appendMonthOfYear(1);
        builder.appendLiteral('/');
        hasField = true;
      }
      
      if (p.isSupported(DateTimeFieldType.year())) {
        builder.appendYear(2, 4);
        hasField = true;
      }
      
      if (!hasField) {
        return "";
      }
    }
    
    StringBuffer buffer = new StringBuffer();
    builder.toPrinter().printTo(buffer, p, locale);
    return buffer.toString();
  }
  
  /**
   * Converts a Partial to a String, roughly emulating ISO formatting.
   */
  public static String toString(ReadablePartial p) {
    StringBuilder builder = new StringBuilder(p.size() * 4);
    DateTimeFieldType lastType = null;
    for (int i = 0; i < p.size(); i++) {
      if (i != 0) {
        builder.append(SEPARATOR);
      }
      
      DateTimeFieldType type = p.getFieldType(i);
      int value = p.getValue(i);

      // If a type needs to be appended, do so
      if (type != NEXT_FIELD_MAP.get(lastType)) {
        Character abbrev = FIELD_ABBREVIATIONS.get(type.getName());
        if (abbrev == null) {
          throw new IllegalArgumentException("Field type " + type + " not supported.");
        }

        builder.append(abbrev);
      }
      
      Preconditions.checkArgument(value >= 0);
      
      // Pad all out to at least two characters
      if (value < 10) {
        builder.append('0');
      }
        
      builder.append(value);
      lastType = type;
    }

    return builder.toString();
  }

  private final static Pattern YEAR_MONTH_DAY = Pattern.compile(
		  "\\d{4}-\\d{2}-\\d{2}");
  private static ReadablePartial fromYearMonthDayString(String string) {
  	int year = Integer.parseUnsignedInt(string.substring(0, 4));
  	int month = Integer.parseUnsignedInt(string.substring(5, 7));
  	int day = Integer.parseUnsignedInt(string.substring(8));
  	return new LocalDate(year, month, day, GJ_CHRONOLOGY);
  }

  /**
   * Parse a Partial from a string of the form YYYY-MM-DD.  Portions may be missing.
   * 
   * @throws IllegalArgumentException if the string is not in a valid form
   */
  public static ReadablePartial fromString(String string) {
    // Optimize the common case;  parse this directly into a more efficient structure.
    // This improved reportset loading performance by 20%. 
    if (YEAR_MONTH_DAY.matcher(string).matches()) {
      return fromYearMonthDayString(string);
    }
    Partial p = new Partial(GJ_CHRONOLOGY);
    DateTimeFieldType lastType = null;
    DateTimeFieldType currentType = null;
    int length = string.length();
    int i = 0;
    while (i < length) {
      char ch = string.charAt(i);
      if (ch >= '0' && ch <= '9') {
        // Advance to the next assumed type if one is not
        // explicit
        if (currentType == null) {
          currentType = NEXT_FIELD_MAP.get(lastType);
          if (currentType == null) {
            throw new IllegalArgumentException("Couldn't guess type of field in " + string);
          }
        }
        
        Preconditions.checkArgument(p.indexOf(currentType) < 0);

        int separatorIndex = string.indexOf(SEPARATOR, i);
        if (separatorIndex < 0) {
          separatorIndex = length;
        }
        String numString = string.substring(i, separatorIndex);
        try {
          int value = Integer.parseInt(numString);
          p = p.with(currentType, value);
        } catch (NumberFormatException nfe) {
          throw new IllegalArgumentException("Couldn't parse field in " + string, nfe);
        }
        
        // Update state for the next field
        lastType = currentType;
        currentType = null;
        i = separatorIndex + 1;
      } else {
        // We should be looking at a field identifier
        if (currentType != null) {
          throw new IllegalArgumentException("Invalid syntax in " + string);
        }
        
        currentType = ABBREVIATIONS_TO_FIELD.get(ch);
        if (currentType == null) {
          throw new IllegalArgumentException("Abbreviation '" + ch + "' not understood "
                  + " in " + string);          
        }
        
        // Move ahead one character
        i++;
        if (i == length) {
          throw new IllegalArgumentException("Invalid syntax in " + string);
        }
      }
    }
    
    return p;
  }

  private static Map<Character, DateTimeFieldType> createAbbreviationsToFieldMap() {
    ImmutableMap.Builder<Character, DateTimeFieldType> builder =
      ImmutableMap.builder();
    builder.put('y', DateTimeFieldType.year())
      .put('M', DateTimeFieldType.monthOfYear())
      .put('d', DateTimeFieldType.dayOfMonth())
      .put('h', DateTimeFieldType.hourOfDay())
      .put('m', DateTimeFieldType.minuteOfHour())
      .put('s', DateTimeFieldType.secondOfMinute());
    return builder.build();
  }

  private static Map<String, Character> createFieldAbbreviationsMap() {
    ImmutableMap.Builder<String, Character> builder = ImmutableMap.builder(); 
    
    for (Map.Entry<Character, DateTimeFieldType> entry : ABBREVIATIONS_TO_FIELD.entrySet()) {
      builder.put(entry.getValue().getName(), entry.getKey());
    }

    return builder.build();
  }
  
  private static Map<DateTimeFieldType, DateTimeFieldType> createNextFieldMap() {
    // Can't use ImmutableMap, as we need null keys and values
    Map<DateTimeFieldType, DateTimeFieldType> map = Maps.newHashMap();
    map.put(null, DateTimeFieldType.year());
    map.put(DateTimeFieldType.year(), DateTimeFieldType.monthOfYear());
    map.put(DateTimeFieldType.monthOfYear(), DateTimeFieldType.dayOfMonth());
    map.put(DateTimeFieldType.dayOfMonth(), DateTimeFieldType.hourOfDay());
    map.put(DateTimeFieldType.hourOfDay(), DateTimeFieldType.minuteOfHour());
    map.put(DateTimeFieldType.minuteOfHour(), DateTimeFieldType.secondOfMinute());
    map.put(DateTimeFieldType.secondOfMinute(), null);
    
    return Collections.unmodifiableMap(map);
  }
}
