/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.io;

import java.io.IOException;
import java.io.InputStream;

/**
 * InputStream delegate that keeps track of input progress.
 */
public class ProgressInputStream extends InputStream {
  private final InputStream delegate;
  private long count = 0;

  public ProgressInputStream(InputStream delegate) {
    this.delegate = delegate;
  }

  public synchronized long getCurrentPosition() {
    return count;
  }
  
  @Override public void close() throws IOException {
    delegate.close();
  }
  
  @Override public synchronized void mark(int readlimit) {
    throw new UnsupportedOperationException();
  }

  @Override public boolean markSupported() {
    return false;
  }

  @Override public int read(byte[] b, int off, int len) throws IOException {
    return incrementCount(delegate.read(b, off, len));
  }

  @Override public int read(byte[] b) throws IOException {
    return incrementCount(delegate.read(b));
  }

  @Override public long skip(long n) throws IOException {
    return incrementCount(delegate.skip(n));
  }

  @Override public int read() throws IOException {
    int readByte = delegate.read();
    incrementCount(1);
    return readByte;
  }

  private synchronized int incrementCount(int bytesRead) {
    count += bytesRead;   
    return bytesRead;
  }

  private synchronized long incrementCount(long bytesRead) {
    count += bytesRead;   
    return bytesRead;
  }
}
