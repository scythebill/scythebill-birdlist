/* The following class is modified from an original in the Apache MyFaces project. */
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package com.scythebill.birdlist.model.io;

import java.io.Closeable;
import java.io.IOException;
import java.io.Writer;

/** Base class for outputting formatted content off a stream of commands. */
abstract public class ResponseWriter extends Writer implements Closeable {
  abstract public void startDocument() throws IOException;

  abstract public void endDocument() throws IOException;

  public void startElement(String name) throws IOException {
    startElement(name, null);
  }

  abstract public void startElement(String name, Object objectFor)
      throws IOException;

  abstract public void writeAttribute(String name, Object value)
      throws IOException;

  abstract public void writeComment(Object comment) throws IOException;

  abstract public void writeText(char[] buffer, int offset, int length)
      throws IOException;

  abstract public void writeText(Object text) throws IOException;

  abstract public void endElement(String name) throws IOException;

  abstract public ResponseWriter cloneWithWriter(Writer writer);

  abstract public void writeRawText(String text) throws IOException;
}
