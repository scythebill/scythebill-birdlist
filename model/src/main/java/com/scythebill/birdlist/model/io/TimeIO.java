/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.io;

import java.time.chrono.IsoChronology;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.util.Locale;

import org.joda.time.DateTimeFieldType;
import org.joda.time.LocalTime;
import org.joda.time.ReadablePartial;
import org.joda.time.chrono.GJChronology;
import org.joda.time.format.DateTimeFormat;

import com.google.common.base.CharMatcher;

/**
 * IO utilities for handling Time-based Partial objects.
 */
public class TimeIO {
  private static final GJChronology GJ_CHRONOLOGY = GJChronology.getInstance();
  private static DateTimeFormatter TIME_FORMATTER = new DateTimeFormatterBuilder()
      .appendPattern("H")
      .appendLiteral(':')
      .appendPattern("mm")
      .toFormatter()
      .withChronology(IsoChronology.INSTANCE);
  private static DateTimeFormatter TIME_FORMATTER_WITH_SECONDS = new DateTimeFormatterBuilder()
      .appendPattern("H")
      .appendLiteral(':')
      .appendPattern("mm")
      .appendLiteral(':')
      .appendPattern("ss")
      .toFormatter()
      .withChronology(IsoChronology.INSTANCE);
  private static DateTimeFormatter TWELVE_HOUR = new DateTimeFormatterBuilder()
      .appendPattern("h")
      .appendLiteral(':')
      .appendPattern("mm")
      .optionalStart()
      .appendLiteral(' ')
      .optionalEnd()
      .appendPattern("a")
      .toFormatter()
      // HalfdayOfDayText is locale-specific, but we want AM/PM only 
      .withLocale(Locale.US)
      .withChronology(IsoChronology.INSTANCE);
  private static DateTimeFormatter TWELVE_HOUR_WITH_SECONDS = new DateTimeFormatterBuilder()
      .appendPattern("h")
      .appendLiteral(':')
      .appendPattern("mm")
      .appendLiteral(':')
      .appendPattern("ss")
      .optionalStart()
      .appendLiteral(' ')
      .optionalEnd()
      // HalfdayOfDayText is locale-specific, but we want AM/PM only 
      .appendPattern("a")
      .toFormatter()
      .withLocale(Locale.US)
      .withChronology(IsoChronology.INSTANCE);
  private static final CharMatcher COLON = CharMatcher.is(':');

  public static void main(String[] args) {
    System.err.println(TimeIO.fromString("11:59"));
    System.err.println(TimeIO.fromExternalString("12:59 PM"));
    System.err.println(TimeIO.toString(TimeIO.fromString("11:59")));
  }
  
  /**
   * Converts a time Partial to a String, roughly emulating ISO formatting.
   */
  public static String toString(ReadablePartial p) {
    if (!p.isSupported(DateTimeFieldType.hourOfDay())) {
      return "";
    }
    
    int hour = p.get(DateTimeFieldType.hourOfDay());
    int minute = p.isSupported(DateTimeFieldType.minuteOfHour())
        ? p.get(DateTimeFieldType.minuteOfHour()) : 0;
    
    return String.format("%02d:%02d", hour, minute);
  }

  /**
   * Converts a time Partial to a String suitable for a filename.
   */
  public static String toFileString(ReadablePartial p) {
    if (!p.isSupported(DateTimeFieldType.hourOfDay())) {
      return "";
    }
    
    int hour = p.get(DateTimeFieldType.hourOfDay());
    int minute = p.isSupported(DateTimeFieldType.minuteOfHour())
        ? p.get(DateTimeFieldType.minuteOfHour()) : 0;
    
    return String.format("%02d%02d", hour, minute);
  }

  /** Parse a Partial from a string, returning null if parsing fails. */
  public static LocalTime fromString(String string) {
    try {
      return parse(string, TIME_FORMATTER);
    } catch (IllegalArgumentException e) {
      return null;
    }
  }

  /**
   * Parse a Partial from an external string, which might be 12 hour or 24 hour.
   * 
   *  @throws IllegalArgumentException if the string is not a valid type (unlike
   *  most of the other methods in this class)
   */
  public static LocalTime fromExternalString(String string) {
    // Trim off any milliseconds/etc. if present.
    int decimal = string.indexOf('.');
    if (decimal >= 0) {
      string = string.substring(0, decimal);
    }
    LocalTime time;
    if (string.endsWith("AM") || string.endsWith("PM")) {
      if (COLON.countIn(string) > 1) {
        time = parse(string, TWELVE_HOUR_WITH_SECONDS);
      } else {
        time = parse(string, TWELVE_HOUR);
      }
    } else {
      if (COLON.countIn(string) > 1) {
        time = parse(string, TIME_FORMATTER_WITH_SECONDS);
      } else {
        time = parse(string, TIME_FORMATTER);
      }
    }
    return time;
  }

  public static String toShortUserString(ReadablePartial startTime, Locale locale) {
    if (!startTime.isSupported(DateTimeFieldType.hourOfDay())) {
      return null;
    }
    int hour = startTime.get(DateTimeFieldType.hourOfDay());
    int minute = startTime.isSupported(DateTimeFieldType.minuteOfHour())
        ? startTime.get(DateTimeFieldType.minuteOfHour())
        : 0;
    
    LocalTime localTime = new LocalTime(hour, minute, 0, 0, GJ_CHRONOLOGY);    
    return DateTimeFormat.shortTime().print(localTime);
  }

  /**
   * Normalize a time, stripping off anything but minutes and seconds.
   */
  public static LocalTime normalize(LocalTime time) {
    return new LocalTime(time.getHourOfDay(), time.getMinuteOfHour(), 0, 0, GJ_CHRONOLOGY);
  }

  /** Parse a Joda LocalTime using the java.time formatter API. */
  private static LocalTime parse(String string, DateTimeFormatter formatter) {
    try {
      return fromJavaTime(java.time.LocalTime.parse(string, formatter));
    } catch (DateTimeParseException e) {
      // Make it resemble the Joda exception API as well.
      throw new IllegalArgumentException(e);
    }
  }

  private static LocalTime fromJavaTime(java.time.LocalTime javaTime) {
    return new LocalTime(javaTime.getHour(), javaTime.getMinute(), 0, 0, GJ_CHRONOLOGY);
  }
  
}
