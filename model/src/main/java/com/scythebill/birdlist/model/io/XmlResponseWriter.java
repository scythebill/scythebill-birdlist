/* The following class is modified from an original in the Apache MyFaces project. */
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package com.scythebill.birdlist.model.io;

import java.io.IOException;
import java.io.Writer;

import com.google.common.base.CharMatcher;

/** ResponseWriter for outputting XML content. */
public class XmlResponseWriter extends ResponseWriter {
  private final Writer out;
  
  /**
   * Matcher for characters that must not appear in XML, per
   * http://www.w3.org/TR/2000/REC-xml-20001006#NT-Char , only
   * for characters < 0x7f
   */
  private static final CharMatcher INVALID_LOW_XML_CHARACTERS =
      CharMatcher.inRange('\u0000', '\u0008')
      .or(CharMatcher.inRange('\u000b', '\u000c'))
      .or(CharMatcher.inRange('\u000e', '\u001f'));
  
  private final String encoding;
  private boolean closeStart;

  public XmlResponseWriter(Writer writer, String encoding) {
    this.out = writer;
    this.encoding = encoding;
  }

  public String getCharacterEncoding() {
    return encoding;
  }

  public String getContentType() {
    return "text/xml";
  }

  @Override public void startDocument() throws IOException {
    out.write("<?xml version=\"1.0\"");
    if (encoding != null)
      out.write(" encoding=\"" + encoding + "\"");
    out.write("?>");
    out.write(System.getProperty("line.separator"));
  }

  @Override public void endDocument() throws IOException {
  }

  @Override public void startElement(String name, Object objectFor)
      throws IOException {
    closeStartIfNecessary();

    out.write('<');
    out.write(name);
    closeStart = true;
  }

  @Override public void writeAttribute(String name, Object value)
      throws IOException {
    if (value == null)
      return;

    // write the attribute value
    out.write(' ');
    out.write(name);
    out.write("=\"");
    writeAttributeText(value);
    out.write("\"");
  }

  @Override public void writeComment(Object comment) throws IOException {
    if (comment != null) {
      closeStartIfNecessary();

      out.write("<!-- ");
      out.write(comment.toString());
      out.write(" -->");
    }
  }

  @Override public void writeText(char[] buffer, int offset, int length)
      throws IOException {
    if (buffer != null && buffer.length > 0) {
      closeStartIfNecessary();
      _writeBodyText(buffer, offset, length);
    }
  }

  @Override public void writeText(Object text) throws IOException {
    if (text != null) {
      String str = text.toString();
      if (!str.isEmpty()) {
        closeStartIfNecessary();
        // _openCDATAIfNecessary();
        writeBodyText(str);
      }
    }
  }

  @Override public void endElement(String name) throws IOException {
    if (closeStart) {
      out.write("/>");
      closeStart = false;
    } else {
      // _closeCDATAIfNecessary();
      out.write("</");
      out.write(name);
      out.write(">");
    }
  }

  @Override public ResponseWriter cloneWithWriter(Writer writer) {
    return new XmlResponseWriter(writer, getCharacterEncoding());
  }

  @Override public void write(char[] cbuf, int off, int len) throws IOException {
    if (len > 0) {
      closeStartIfNecessary();
      out.write(cbuf, off, len);
    }
  }

  @Override public void write(char[] cbuf) throws IOException {
    if (cbuf.length > 0) {
      closeStartIfNecessary();
      out.write(cbuf);
    }
  }

  @Override public void write(int c) throws IOException {
    closeStartIfNecessary();
    out.write(c);
  }

  @Override public void write(String str) throws IOException {
    if (!str.isEmpty()) {
      closeStartIfNecessary();
      out.write(str);
    }
  }

  @Override public void writeRawText(String text) throws IOException {
    if (text != null) {
      closeStartIfNecessary();
      out.write(text);
    }    
  }

  @Override public void write(String str, int off, int len) throws IOException {
    if (len > 0) {
      closeStartIfNecessary();
      out.write(str, off, len);
    }
  }

  @Override public void close() throws IOException {
    out.close();
  }

  @Override public void flush() throws IOException {
    out.flush();
  }

  protected void writeAttributeText(Object text) throws IOException {
    char[] buffer = text.toString().toCharArray();
    _writeAttributeText(buffer, 0, buffer.length);
  }

  protected void writeBodyText(Object text) throws IOException {
    char[] buffer = text.toString().toCharArray();
    _writeBodyText(buffer, 0, buffer.length);
  }

  protected void closeStartIfNecessary() throws IOException {
    if (closeStart) {
      out.write('>');
      closeStart = false;
    }
  }

  /*
   * private void _openCDATAIfNecessary() throws IOException { if (!_closeCDATA) {
   * Writer out = out; out.write("<![CDATA["); _closeCDATA = true; } }
   * 
   * private void _closeCDATAIfNecessary() throws IOException { if (_closeCDATA) {
   * Writer out = out; out.write("]]>"); _closeCDATA = false; } }
   */

  private void _writeAttributeText(char[] text, int start, int length)
      throws IOException {
    int end = start + length;
    for (int i = start; i < end; i++) {
      char ch = text[i];

      if (ch < 0x7f) {
        switch (ch) {
          case '>':
            out.write("&gt;");
            break;
          case '<':
            out.write("&lt;");
            break;
          case '"':
            out.write("&quot;");
            break;
          case '&':
            out.write("&amp;");
            break;
          case '\t':
            out.write("&#x9;");
            break;
          default:
            if (!INVALID_LOW_XML_CHARACTERS.matches(ch)) {
              out.write(ch);
            }
            break;
          }
      } else {
        // Make sure that high-surrogates are written as proper codepoints, not as raw hexs
        if (Character.isHighSurrogate(ch)) {
          int codePoint = Character.codePointAt(text, i);
          _writeHexRef(codePoint);
          // and skip past the low-surrogate
          i++;
        } else { 
          _writeHexRef(ch);
        }
      }
    }
  }

  private void _writeBodyText(char[] text, int start, int length)
      throws IOException {
    int end = start + length;
    for (int i = start; i < end; i++) {
      char ch = text[i];

      if (ch < 0x7f) {
        switch (ch) {
          case '>':
            out.write("&gt;");
            break;
          case '<':
            out.write("&lt;");
            break;
          case '&':
            out.write("&amp;");
            break;
          case '\t':
            out.write("&#x9;");
            break;
          default:
            if (!INVALID_LOW_XML_CHARACTERS.matches(ch)) {
              out.write(ch);
            }
            break;
        }
      } else {
        // Make sure that high-surrogates are written as proper codepoints, not as raw hexs
        if (Character.isHighSurrogate(ch)) {
          int codePoint = Character.codePointAt(text, i);
          _writeHexRef(codePoint);
          // and skip past the low-surrogate
          i++;
        } else { 
          _writeHexRef(ch);
        }
      }
    }
  }

  private void _writeHexRef(int ch) throws IOException {
    out.write("&#x");
    // TODO: Could easily be more efficient.
    out.write(Integer.toHexString(ch));
    out.write(';');
  }
}
