/*
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.scythebill.birdlist.model.mapdata;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.geotools.data.FileDataStore;
import org.geotools.data.collection.SpatialIndexFeatureCollection;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.util.factory.GeoTools;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.linearref.LinearLocation;
import org.locationtech.jts.linearref.LocationIndexedLine;
import org.opengis.feature.Property;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.expression.Expression;
import org.opengis.filter.spatial.BBOX;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.io.Resources;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;

/**
 * API for extracting lat/long to locations without any HTTP traffic.
 */
public abstract class MapData {
  private static final Logger logger = Logger.getLogger(MapData.class.getName());
  private static final MapData instance;

  /**
   * Gets a shared instance of the MapData. If MapData initialization failed, the returned instance
   * will always throw an error.
   */
  public static MapData instance() {
    return instance;
  }

  /**
   * Gets an ISO code - [country] or [country]-[state] - for a latitude and longitude. Returns null
   * if no code could be found.
   */
  public abstract String getISOCode(LatLongCoordinates latLong);

  private static MapData createMapData() {
    try {
      ShapefileDataStoreFactory dataStoreFactory = new ShapefileDataStoreFactory();
      FileDataStore mapUnits = dataStoreFactory
          .createDataStore(Resources.getResource(MapData.class, "ne_50m_admin_0_map_units.shp"));
      SimpleFeatureSource mapUnitsFeatureSource = mapUnits.getFeatureSource();

      FileDataStore states = dataStoreFactory.createDataStore(
          Resources.getResource(MapData.class, "ne_50m_admin_1_states_provinces.shp"));
      SimpleFeatureSource statesFeatureSource = states.getFeatureSource();

      return new MapDataImpl(mapUnitsFeatureSource.getFeatures(), mapUnitsFeatureSource.getBounds(),
          statesFeatureSource.getFeatures());
    } catch (Throwable t) {
      logger.log(Level.SEVERE, "Failed to parse map data", t);
      return new ThrowingMapData(new IllegalStateException("Could not parse map data", t));
    }
  }

  static {
    instance = createMapData();
  }

  static class MapDataImpl extends MapData {
    private final SimpleFeatureCollection mapUnitFeatures;
    private final ReferencedEnvelope mapUnitEnvelope;
    private final SpatialIndexFeatureCollection mapUnitSpatialIndex;
    private final SimpleFeatureCollection statesFeatures;
    private final FilterFactory2 filterFactory;
    private final GeometryFactory geometryFactory;
    private final AtomicLong totalTime = new AtomicLong();
    private final AtomicInteger resolutionCount = new AtomicInteger();
    private final SpatialIndexFeatureCollection statesSpatialIndex;

    private final static ImmutableMap<String, String> CODES_TO_EBIRD_CODE =
        ImmutableMap.<String, String>builder().put("ATC,ATC", "AC").put("ATG,ACA", "AG")
            .put("ATG,ACB", "AG-10").put("PRT,PAZ", "PT-20").put("PNG,PNB", "PG-NSA")
            .put("BEL,BCR", "BE-BRU").put("GBR,ENG", "GB-ENG").put("BIH,BHF", "BA-BIH")
            .put("BEL,BFR", "BE-VLG").put("PSX,GAZ", "PS").put("GEO,GEG", "GE").put("NOR,NJM", "SJ")
            .put("PRT,PMD", "PT-30").put("CYN,CYN", "CY").put("GBR,NIR", "GB-NIR")
            .put("NOR,NOR", "NO").put("PNG,PNX", "PG").put("PRT,PRX", "PT").put("BIH,BIS", "BA-SRP")
            .put("GBR,SCT", "GB-SCT").put("SRB,SRS", "RS").put("KAS,KAS", "IN-JK")
            .put("SOL,SOL", "SO").put("SRB,SRV", "RS-VO").put("GBR,WLS", "GB-WLS")
            .put("BEL,BWR", "BE-WAL").put("PSX,WEB", "PS").put("TZA,TZZ", "TZ").build();

    private final static ImmutableSet<String> COUNTRIES_WITH_STATES =
        ImmutableSet.of("CA", "US", "AU", "BR");

    public MapDataImpl(SimpleFeatureCollection mapUnitFeatures, ReferencedEnvelope mapUnitsEnvelope,
        SimpleFeatureCollection statesFeatures) {
      this.mapUnitFeatures = mapUnitFeatures;
      this.mapUnitEnvelope = mapUnitsEnvelope;
      this.mapUnitSpatialIndex = new SpatialIndexFeatureCollection(mapUnitFeatures.getSchema());
      mapUnitSpatialIndex.addAll(mapUnitFeatures);
      this.statesFeatures = statesFeatures;
      this.statesSpatialIndex = new SpatialIndexFeatureCollection(statesFeatures.getSchema());
      statesSpatialIndex.addAll(statesFeatures);
      this.filterFactory = CommonFactoryFinder.getFilterFactory2(GeoTools.getDefaultHints());
      this.geometryFactory = new GeometryFactory();
    }

    @Override
    public String getISOCode(LatLongCoordinates latLong) {
      long startTime = System.currentTimeMillis();
      String code = getEBirdCodeInternal(latLong);
      long endTime = System.currentTimeMillis();
      
      long currentTotalTime = totalTime.addAndGet(endTime - startTime);
      int currentCount = resolutionCount.incrementAndGet();
      
      if ((currentCount % 100) == 0) {
        logger.info(String.format(
            "MapData: %d calls, total time %d ms, avg. %f ms",
            currentCount, currentTotalTime, ((double) currentTotalTime) / currentCount)); 
      }
      
      // The 50m data doesn't include Ecuadorian states - but, the longitude of the Galapagos islands
      // is quite obvious!
      if ("EC".equals(code) && latLong.longitudeAsDouble() < -85.0) {
        code = "EC-W";
      }
      
      return code;
    }

    private String getEBirdCodeInternal(LatLongCoordinates latLong) {
      Point p = geometryFactory
          .createPoint(new Coordinate(latLong.longitudeAsDouble(), latLong.latitudeAsDouble()));
      if (!mapUnitEnvelope.contains(p.getCoordinate())) {
        return null;
      }

      SimpleFeature feature = findFeature(mapUnitFeatures, p);
      if (feature == null) {
        feature = findNearestFeatureWithinDistance(mapUnitSpatialIndex, p);
        if (feature == null) {
          return null;
        }
      }

      Property isoA2Property = feature.getProperty("ISO_A2");
      if (isoA2Property == null) {
        throw new IllegalStateException("Missing ISO_A2 property");
      }

      String isoA2 = (String) isoA2Property.getValue();
      if (COUNTRIES_WITH_STATES.contains(isoA2)) {
        SimpleFeature stateFeature = findFeature(statesFeatures, p);
        if (stateFeature == null) {
          stateFeature = findNearestFeatureWithinDistance(statesSpatialIndex, p);
        }

        if (stateFeature != null) {
          Property iso3166_2Property = stateFeature.getProperty("iso_3166_2");
          if (iso3166_2Property == null) {
            throw new IllegalStateException("Missing iso_3166_2 property");
          }

          String iso3166_2 = (String) iso3166_2Property.getValue();
          // Jarvis Bay has this odd code. Treat it as NSW, which seems kinda wrong
          // (it's more ACT) but eBird treats it as NSW.
          if ("AU-X02~".equals(iso3166_2)) {
            return "AU-NSW";
          }
          return iso3166_2;
        }
      }

      if ("-99".equals(isoA2)) {
        Property adm0a3Property = feature.getProperty("ADM0_A3");
        String adm0a3 = adm0a3Property == null ? "" : (String) adm0a3Property.getValue();
        Property guA3Property = feature.getProperty("GU_A3");
        String guA3 = guA3Property == null ? "" : (String) guA3Property.getValue();

        return CODES_TO_EBIRD_CODE.get(String.format("%s,%s", adm0a3, guA3));
      }

      return isoA2;
    }

    private SimpleFeature findFeature(SimpleFeatureCollection features, Point p) {
      Expression propertyName =
          filterFactory.property(features.getSchema().getGeometryDescriptor().getName());
      Filter filter = filterFactory.contains(propertyName, filterFactory.literal(p));
      SimpleFeatureCollection sub = features.subCollection(filter);
      if (sub.size() > 0) {
        SimpleFeatureIterator featureIterator = sub.features();
        try {
          if (featureIterator.hasNext()) {
            return featureIterator.next();
          }
        } finally {
          featureIterator.close();
        }
      }

      return null;
    }

    /**
     * It's darn approximate, but "3.3" is somewhere near 200 nautical miles, which is the general
     * listing rule for countries. It's a pretty close estimate for latitude. For longitude, it's
     * great at the equator, and nonsense at the poles.
     */
    private static final double MAX_SEARCH_DISTANCE = 3.3;

    private SimpleFeature findNearestFeatureWithinDistance(
        SpatialIndexFeatureCollection spatialIndex, Point p) {
      Coordinate coordinate = p.getCoordinate();
      ReferencedEnvelope search = new ReferencedEnvelope(new Envelope(coordinate),
          spatialIndex.getSchema().getCoordinateReferenceSystem());
      search.expandBy(MAX_SEARCH_DISTANCE);
      BBOX bbox = filterFactory.bbox(
          filterFactory.property(spatialIndex.getSchema().getGeometryDescriptor().getName()),
          search);
      SimpleFeatureCollection candidates = spatialIndex.subCollection(bbox);

      SimpleFeature lastMatched = null;
      double minDist = MAX_SEARCH_DISTANCE + 1.0e-6;
      try (SimpleFeatureIterator itr = candidates.features()) {
        while (itr.hasNext()) {
          SimpleFeature feature = itr.next();
          LocationIndexedLine line =
              new LocationIndexedLine(((MultiPolygon) feature.getDefaultGeometry()).getBoundary());
          LinearLocation here = line.project(coordinate);
          Coordinate point = line.extractPoint(here);
          double dist = point.distance(coordinate);
          if (dist < minDist) {
            minDist = dist;
            lastMatched = feature;
          }
        }
      }

      return lastMatched;
    }
  }

  static class ThrowingMapData extends MapData {
    private final RuntimeException e;

    ThrowingMapData(RuntimeException e) {
      this.e = e;
    }

    @Override
    public String getISOCode(LatLongCoordinates latLong) {
      throw e;
    }

  }
}
