/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Sighting;

/**
 * Synthetic location for the ABA region.  See
 * <a href="http://www.aba.org/bigday/listingareas.pdf>this file</a>
 * for the definition of this region.
 */
class AbaRegionWithoutHawaii extends SyntheticLocation {
  private static final Logger logger = Logger.getLogger(AbaRegionWithoutHawaii.class.getName());
  private Predicate<Sighting> predicate;

  static public AbaRegionWithoutHawaii regionIfAvailable(LocationSet locationSet) {
    Location northAmericanUsa = SyntheticLocations.getNorthAmericanUsa(locationSet);
    if (northAmericanUsa == null) {
      logger.warning("No ABA list available: No United States in North America");
      return null;
    }

    Location canada = getCanada(locationSet);    
    if (canada == null) {
      logger.warning("No ABA list available: No country Canada");
      return null;
    }
    
    return new AbaRegionWithoutHawaii(
        locationSet,
        northAmericanUsa,
        canada,
        // Optionally include St. Pierre and Miquelon, if present
        getStPierreAndMiquelon(locationSet));
  }

  private AbaRegionWithoutHawaii(LocationSet locationSet, Location... locations) {
    super("ABA region (without Hawaii)", Name.ABA_REGION_WITHOUT_HAWAII, "abawh");
    List<Predicate<Sighting>> list = Lists.newArrayList();
    for (Location location : locations) {
      if (location == null) {
        continue;
      }
      list.add(SightingPredicates.in(location, locationSet));
    }
    
    predicate = Predicates.or(list);
  }

  @Override
  public Collection<String> syntheticChecklistUnion() {
    return ImmutableSet.of("US", "CA", "PM");
  }
  
  @Override
  public Predicate<Sighting> isInPredicate() {
    return predicate;
  }

  /** Return the location for Canada. */
  private static Location getCanada(LocationSet locationSet) {
    return locationSet.getLocationByCode("CA");
  }
  
  private static Location getStPierreAndMiquelon(LocationSet locationSet) {
    return locationSet.getLocationByCode("PM");
  }
}
