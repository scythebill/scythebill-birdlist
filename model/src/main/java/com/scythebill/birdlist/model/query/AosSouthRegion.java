/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import static java.util.stream.Collectors.toCollection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Sighting;

/**
 * Synthetic location for the AOS South region.
 */
class AosSouth extends SyntheticLocation {
  /** List of all codes in South America, plus Falklands and Galapagos. */ 
  private static final ImmutableSet<String> SYNTHETIC_CHECKLIST_UNION = ImmutableSet.of(
      "AR", "AW", "BO", "BR", "BQ-BO", "CL", "CO", "EC",
      "GF", "GY", "PY", "PE", "SR", "TT", "UY", "VE",
      "EC-W", "FK");
  private static final Logger logger = Logger.getLogger(AouRegion.class.getName());
  private Predicate<Sighting> predicate;

  static public AosSouth regionIfAvailable(LocationSet locationSet) {
    Location southAmerica = getSouthAmerica(locationSet);
    if (southAmerica == null) {
      return null;
    }
    
    List<Location> locations = new ArrayList<Location>();
    locations.add(southAmerica);
    Location galapagos = locationSet.getLocationByCode("EC-W");
    if (galapagos != null) {
      locations.add(galapagos);
    }
    Location falklands = locationSet.getLocationByCode("FK");
    if (falklands != null) {
      locations.add(falklands);      
    }
    return new AosSouth(
        locationSet,
        locations);
  }

  @Override
  public Collection<String> syntheticChecklistUnion() {
    return SYNTHETIC_CHECKLIST_UNION;
  }
  
  private AosSouth(LocationSet locationSet,
      List<Location> locations) {
    super("AOS Area - South", Name.AOU_SOUTH, "aossouth");
    List<Predicate<Sighting>> inPredicates =
        locations.stream().map(
            l -> SightingPredicates.in(l, locationSet))
        .collect(toCollection(ArrayList::new));
    predicate = Predicates.or(inPredicates);
  }

  @Override
  public Predicate<Sighting> isInPredicate() {
    return predicate;
  }

  /** Return the location for "NorthAmerica. */
  private static Location getSouthAmerica(LocationSet locationSet) {
    Collection<Location> locationsByName = locationSet.getLocationsByModelName("South America");
    if (locationsByName.size() > 1) {
      logger.warning("AOS South list not available: >1 South America");
      return null;
    }
    
    if (locationsByName.isEmpty()) {
      logger.warning("AOS South list not available: no South America");
      return null;
    }
    
    return Iterables.getOnlyElement(locationsByName);
  }
}
