/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import static java.util.stream.Collectors.toCollection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Sighting;

/**
 * Synthetic location for the Aos North region, once called the AOU region.
 */
class AouRegion extends SyntheticLocation {
  /** List of all codes in North America, plus Hawaii, Midway, Clipperton, and Johnston. */ 
  private static final ImmutableSet<String> SYNTHETIC_CHECKLIST_UNION = ImmutableSet.of(
      "US", "CA", "PM", "US-HI", "UM-71", "UM-67", "GL",
      "CP", "MX", "BM", "BZ", "CR", "SV", "GT", "HN",
      "NI", "PA", "AI", "AG", "BB", "VG", "BQ", "KY",
      "DM", "DO", "GD", "GP", "HT", "JM", "MQ", "MS",
      "BL", "KN", "LC", "MF", "VC", "CO-SAP", "TC", "PR", "VI");
  private static final Logger logger = Logger.getLogger(AouRegion.class.getName());
  private Predicate<Sighting> predicate;

  static public AouRegion regionIfAvailable(LocationSet locationSet) {
    Location northAmerica = getNorthAmerica(locationSet);
    if (northAmerica == null) {
      return null;
    }
    
    List<Location> locations = new ArrayList<Location>();
    locations.add(northAmerica);
    Location hawaii = locationSet.getLocationByCode("US-HI");
    if (hawaii != null) {
      locations.add(hawaii);
    }
    Location johnston = locationSet.getLocationByCode("UM-67");
    if (johnston != null) {
      locations.add(johnston);      
    }
    Location midway = locationSet.getLocationByCode("UM-71");
    if (midway != null) {
      locations.add(midway);      
    }
    Location clipperton = locationSet.getLocationByCode("CP");
    if (clipperton != null) {
      locations.add(clipperton);
    }
    return new AouRegion(
        locationSet,
        locations);
  }

  @Override
  public Collection<String> syntheticChecklistUnion() {
    return SYNTHETIC_CHECKLIST_UNION;
  }
  
  private AouRegion(LocationSet locationSet,
      List<Location> locations) {
    super("AOS Area - North", Name.AOU_NORTH, "aou");
    List<Predicate<Sighting>> inPredicates =
        locations.stream().map(
            l -> SightingPredicates.in(l, locationSet))
        .collect(toCollection(ArrayList::new));
    predicate = Predicates.or(inPredicates);
  }

  @Override
  public Predicate<Sighting> isInPredicate() {
    return predicate;
  }

  /** Return the location for "NorthAmerica. */
  private static Location getNorthAmerica(LocationSet locationSet) {
    Collection<Location> locationsByName = locationSet.getLocationsByModelName("North America");
    if (locationsByName.size() > 1) {
      logger.warning("AOU list not available: >1 North America");
      return null;
    }
    
    if (locationsByName.isEmpty()) {
      logger.warning("AOU list not available: no North America");
      return null;
    }
    
    return Iterables.getOnlyElement(locationsByName);
  }
}
