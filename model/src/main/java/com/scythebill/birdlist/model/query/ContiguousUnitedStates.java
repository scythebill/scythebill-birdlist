/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import java.util.Collection;
import java.util.logging.Logger;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Sighting;

/**
 * Synthetic location for all 48 contiguous US states.
 */
class ContiguousUnitedStates extends SyntheticLocation {
  private static final Logger logger = Logger.getLogger(ContiguousUnitedStates.class.getName());
  private Predicate<Sighting> predicate;

  static public ContiguousUnitedStates regionIfAvailable(LocationSet locationSet) {
    Location northAmericanUsa = SyntheticLocations.getNorthAmericanUsa(locationSet);
    if (northAmericanUsa == null) {
      logger.warning("No United States in North America - no contiguous US list available");
      return null;
    }
    Location alaska = locationSet.getLocationByCode("US-AK");
    if (alaska == null) {
      logger.warning("No Alaska state - no contiguous US list available");
      return null;
    }
    
    return new ContiguousUnitedStates(
        locationSet,
        northAmericanUsa,
        alaska);
  }

  private ContiguousUnitedStates(LocationSet locationSet,
      Location northAmericaUsa, Location alaska) {
    super("United States - contiguous", Name.CONTIGUOUS_UNITED_STATES, "us48");
    predicate = Predicates.and(
        SightingPredicates.in(northAmericaUsa, locationSet),
        Predicates.not(SightingPredicates.in(alaska, locationSet)));
  }

  private static final ImmutableSet<String> SYNTHETIC_CHECKLIST_UNION = ImmutableSet.of(
      "US-AL",
      "US-AZ",
      "US-AR",
      "US-CA",
      "US-CO",
      "US-CT",
      "US-DE",
      "US-DC",
      "US-FL",
      "US-GA",
      "US-ID",
      "US-IL",
      "US-IN",
      "US-IA",
      "US-KS",
      "US-KY",
      "US-LA",
      "US-ME",
      "US-MD",
      "US-MA",
      "US-MI",
      "US-MN",
      "US-MS",
      "US-MO",
      "US-MT",
      "US-NE",
      "US-NV",
      "US-NH",
      "US-NJ",
      "US-NM",
      "US-NY",
      "US-NC",
      "US-ND",
      "US-OH",
      "US-OK",
      "US-OR",
      "US-PA",
      "US-RI",
      "US-SC",
      "US-SD",
      "US-TN",
      "US-TX",
      "US-UT",
      "US-VT",
      "US-VA",
      "US-WA",
      "US-WV",
      "US-WI",
      "US-WY");
      
  
  @Override
  public Collection<String> syntheticChecklistUnion() {
    return SYNTHETIC_CHECKLIST_UNION;
  }

  @Override
  public Predicate<Sighting> isInPredicate() {
    return predicate;
  }
}
