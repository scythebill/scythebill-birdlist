/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.Sighting;

/**
 * Generic implementation of a synthetic location.
 */
class GenericSyntheticLocation extends SyntheticLocation {
  private static final Logger logger = Logger.getLogger(GenericSyntheticLocation.class.getName());
      
  private final Predicate<Sighting> predicate;

  private ImmutableSet<String> syntheticChecklistUnion;
  
  public static Builder named(LocationSet locationSet, String name, Name bundleName) {
    return new Builder(locationSet, name, bundleName);
  }
  
  public static class Builder {
    private String name;
    private Name bundleName;
    private LocationSet locationSet;
    private String abbreviation;
    private List<Location> locations = new ArrayList<>();
    private List<Location> removedLocations = new ArrayList<>();
    private boolean dontCreate;
    private ImmutableSet.Builder<String> checklistCodes = ImmutableSet.builder();
    private ImmutableSet<String> syntheticChecklistUnion;

    Builder(LocationSet locationSet, String name, Name bundleName) {
      this.locationSet = locationSet;
      this.bundleName = bundleName;
      this.name = name;
    }
    
    public Builder withAbbreviation(String abbreviation) {
      this.abbreviation = abbreviation;
      return this;
    }
    
    public Builder withRequiredLocations(String... codes) {
      for (String code : codes) {
        if (!addLocationByCode(code)) {
          logger.warning("Could not find required country code " + code + ", needed for " + name );
          dontCreate = true;
        }
      }
      
      return this;
    }
    
    public Builder withLocations(String... codes) {
      for (String code : codes) {
        if (!addLocationByCode(code)) {
          logger.warning("Could not find country code " + code + ", needed for " + name );
        }
      }
      
      return this;
    }

    public Builder withOptionalLocations(String... codes) {
      for (String code : codes) {
        addLocationByCode(code);
      }
      
      return this;
    }
    
    private boolean addLocationByCode(String code) {
      boolean found = false;
      if (code.equals("ID-Asia")) {
        for (Location indonesia : locationSet.getLocationsByCode("ID")) {
          if (Locations.getBuiltInAncestor(indonesia.getParent()).getModelName().equals("Asia")) {
            locations.add(indonesia);
            found = true;
          }            
        }
      } else {
        // ID-Australasia not supported here yet.
        Preconditions.checkArgument(!code.startsWith("ID-A"), "Indonesia regional lists not supported");
        for (Location location : locationSet.getLocationsByCode(code)) {
          // Don't even warn
          locations.add(location);
          found = true;
        }
      }      
      checklistCodes.add(code);
      return found;
    }
    
    public Builder withRemovedLocations(String... codes) {
      for (String code : codes) {
        Location location = locationSet.getLocationByCode(code);
        if (location != null) {
          // Don't warn if absent, because obviously nothing is present
          removedLocations.add(location);
        }
      }
      
      return this;
    }
    
    public Builder withSyntheticChecklistUnionFromAllLocations() {
      // Use the checklist codes, *including* optional ones that didn't actually have a present location.
      // Otherwise, endemic status is busted.
      syntheticChecklistUnion = checklistCodes.build();
      return this;
    }

    public Builder withSyntheticChecklistUnion(String... codes) {
      syntheticChecklistUnion = ImmutableSet.copyOf(codes);
      return this;
    }
    
    public GenericSyntheticLocation build() {
      if (dontCreate) {
        return null;
      }
      if (locations.isEmpty()) {
        return null;
      }
      
      Preconditions.checkState(abbreviation != null, "No abbreviation set");
      return new GenericSyntheticLocation(
          locationSet,
          name,
          bundleName,
          abbreviation,
          locations,
          removedLocations,
          syntheticChecklistUnion);
    }
  }

  public static GenericSyntheticLocation regionIfAvailable(
      LocationSet locationSet,
      String name,
      Name bundleName,
      String abbreviation,
      String... codes) {
    List<Location> list = Lists.newArrayList();
    ImmutableSet.Builder<String> syntheticChecklistCodes =
        ImmutableSet.builder();
    for (String code : codes) {
      Location location = locationSet.getLocationByCode(code);
      if (location == null) {
        logger.warning("Could not find country code " + code + ", needed for " + name );
      } else {
        list.add(location);
        syntheticChecklistCodes.add(code);
      }
    }
    return new GenericSyntheticLocation(
        locationSet,
        name,
        bundleName,
        abbreviation,
        list,
        ImmutableList.of(),
        syntheticChecklistCodes.build());
  }

  private GenericSyntheticLocation(
      LocationSet locationSet,
      String name,
      Name bundleName,
      String abbreviation,
      List<Location> locations,
      List<Location> removedLocations,
      ImmutableSet<String> syntheticChecklistUnion) {
    super(name, bundleName, abbreviation);
    List<Predicate<Sighting>> inList = locationsToPredicates(locationSet, locations);
    if (removedLocations.isEmpty()) {
      predicate = Predicates.or(inList);
    } else {
      List<Predicate<Sighting>> notInList = locationsToPredicates(locationSet, removedLocations);
      predicate = Predicates.and(
          Predicates.not(Predicates.or(notInList)),
          Predicates.or(inList));
    }
    this.syntheticChecklistUnion = syntheticChecklistUnion;
  }

  @Override
  public Collection<String> syntheticChecklistUnion() {
    return syntheticChecklistUnion;
  }
  
  List<Predicate<Sighting>> locationsToPredicates(LocationSet locationSet,
      List<Location> locations) {
    List<Predicate<Sighting>> list = Lists.newArrayList();
    for (Location location : locations) {
      if (location == null) {
        continue;
      }
      list.add(SightingPredicates.in(location, locationSet));
    }
    return list;
  }

  @Override
  public Predicate<Sighting> isInPredicate() {
    return predicate;
  }
}
