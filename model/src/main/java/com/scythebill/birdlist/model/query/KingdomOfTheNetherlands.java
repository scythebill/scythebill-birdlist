/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import java.util.Collection;
import java.util.List;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Sighting;

/**
 * Synthetic location for the Kingdom of the Netherlands.
 */
class KingdomOfTheNetherlands extends SyntheticLocation {
  private final Predicate<Sighting> predicate;

  static public KingdomOfTheNetherlands regionIfAvailable(
      LocationSet locationSet) {
    List<Location> locations = Lists.newArrayList();
    
    Location netherlands = locationSet.getLocationByCode("NL");
    if (netherlands == null) {
      return null;
    }

    locations.addAll(locationSet.getLocationsByCode("BQ"));    
    Location aruba = locationSet.getLocationByCode("AW");
    if (aruba != null) {
      locations.add(aruba);
    }
        
    Location curacao = locationSet.getLocationByCode("CW");
    if (curacao != null) {
      locations.add(curacao);
    }

    Location sintmaarten = locationSet.getLocationByCode("SX");
    if (sintmaarten != null) {
      locations.add(sintmaarten);
    }

    return new KingdomOfTheNetherlands(
        locationSet,
        locations);
  }

  @Override
  public Collection<String> syntheticChecklistUnion() {
    return ImmutableSet.of("NL", "BQ", "AW", "CW", "SX");
  }
  
  private KingdomOfTheNetherlands(LocationSet locationSet, List<Location> locations) {
    super("Kingdom of the Netherlands", Name.KINGDOM_OF_THE_NETHERLANDS, "nlwidepterr");
    List<Predicate<Sighting>> list = Lists.newArrayList();
    for (Location location : locations) {
      list.add(SightingPredicates.in(location, locationSet));
    }
    
    predicate = Predicates.or(list);
  }

  @Override
  public Predicate<Sighting> isInPredicate() {
    return predicate;
  }
}
