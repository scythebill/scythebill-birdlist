/*   
 * Copyright 2020 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Sighting;

/**
 * Synthetic location for the Pacific Ocean (ABA).  See
 * <a href="https://www.aba.org/listing-areas-and-regions/">this file</a>
 * for the definition of this region.
 */
class PacificOceanAbaRegion extends SyntheticLocation {
  private static final Logger logger = Logger.getLogger(PacificOceanAbaRegion.class.getName());
  private static final ImmutableSet<String> PACIFIC_OCEAN_ABA_BUT_NOT_PACIFIC_OCEAN = ImmutableSet.of(
      "AU-TAS-Macquarie", // Macquarie Island
      "NC", // New Caledonia
      "NZ", // New Zealand
      "NF", // Norfolk Island
      "SB", // Solomon Islands
      "VU"  // Vanuatu
      );
  /** A second set of locations that isn't necessarily present, but only added on demand. */
  private static final ImmutableSet<String> PACIFIC_OCEAN_ABA_BUT_NOT_PACIFIC_OCEAN_DONT_WARN = ImmutableSet.of(
      "PG-NSA" // Bougainville
      );
      
  private Predicate<Sighting> predicate;
  private ImmutableSet<String> syntheticUnionCodes;

  static public PacificOceanAbaRegion regionIfAvailable(LocationSet locationSet) {
    Location pacificOcean = getBuiltInLocation(locationSet, "Pacific Ocean");
    if (pacificOcean == null) {
      logger.warning("No Pacific Ocean (ABA) list available: No Pacific Ocean!");
      return null;
    }

    Set<String> syntheticUnionCodes = new LinkedHashSet<>();
    syntheticUnionCodes.addAll(Checklists.gatherCountryChecklistCodes(pacificOcean));
    syntheticUnionCodes.addAll(PACIFIC_OCEAN_ABA_BUT_NOT_PACIFIC_OCEAN);
    
    // All of europe...    
    List<Location> list = Lists.newArrayList(pacificOcean);
    for (String code : PACIFIC_OCEAN_ABA_BUT_NOT_PACIFIC_OCEAN) {
      Location location = locationSet.getLocationByCode(code);
      if (location == null) {
        logger.warning("Could not find country code " + code);
      } else {
        list.add(location);
      }      
    }
    for (String code : PACIFIC_OCEAN_ABA_BUT_NOT_PACIFIC_OCEAN_DONT_WARN) {
      Location location = locationSet.getLocationByCode(code);
      if (location != null) {
        list.add(location);
      }      
    }
    return new PacificOceanAbaRegion(
        locationSet,
        list,
        ImmutableSet.copyOf(syntheticUnionCodes));
  }

  private PacificOceanAbaRegion(LocationSet locationSet, Iterable<Location> locations,
      ImmutableSet<String> syntheticUnionCodes) {
    super("Pacific Ocean (ABA)", Name.PACIFIC_OCEAN_ABA, "pacoceaba");
    List<Predicate<Sighting>> list = Lists.newArrayList();
    for (Location location : locations) {
      if (location == null) {
        continue;
      }
      list.add(SightingPredicates.in(location, locationSet));
    }
    
    predicate = Predicates.or(list);
    this.syntheticUnionCodes = syntheticUnionCodes;
  }

  @Override
  public Predicate<Sighting> isInPredicate() {
    return predicate;
  }

  @Override
  public Collection<String> syntheticChecklistUnion() {
    return syntheticUnionCodes;
  }

  /** Return a built-in location. */
  private static Location getBuiltInLocation(LocationSet locationSet, String name) {
    Collection<Location> locationsByName = locationSet.getLocationsByModelName(name);
    for (Location location : locationsByName) {
      if (location.isBuiltInLocation()) {
        return location;
      }
    }
    
    return null;
  }
}
