/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import java.util.Collection;

import javax.annotation.Nullable;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;

/**
 * Definition of a single query.
 */
public interface QueryDefinition {
  public enum QueryAnnotation {
    /** Entry annotation indicating that a sighting is a lifer. */
    LIFER
  }
  
  /** Preprocessor, responsible for an initial phase of analysis. */
  public interface Preprocessor {
    void reset();
    void preprocess(
        Sighting sighting, @Nullable Predicate<Sighting> countablePredicate);
  }
  
  public interface PostProcessor {
    boolean acceptTaxon(SightingTaxon sightingTaxon, Collection<Sighting> sightings,
        @Nullable Predicate<Sighting> countablePredicate);
  }

  /** Returns the preprocessor for this definition. */
  Optional<Preprocessor> preprocessor();
  
  /** Returns the sighting predicate;  only valid after the preprocessor has run on all sightings. */
  Predicate<Sighting> predicate();
  
  /** Returns a postprocessor for this definition. */
  default Optional<PostProcessor> postprocessor() {
    return Optional.absent();
  }
  
  // TODO: optimize by avoiding the need to find annotations unless it's strictly necessary
  /**
   * Annotates a sighting, relative to a mapping of its taxon.  This may
   * include a mapping to a different taxonomy, *or* a mapping into a
   * different taxonomic level (or both!).
   */
  Optional<QueryAnnotation> annotate(Sighting sighting, SightingTaxon taxon);
}
