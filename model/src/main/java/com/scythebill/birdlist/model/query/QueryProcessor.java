/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import java.util.Map;

import javax.annotation.Nullable;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.query.QueryDefinition.PostProcessor;
import com.scythebill.birdlist.model.query.QueryDefinition.Preprocessor;
import com.scythebill.birdlist.model.query.QueryDefinition.QueryAnnotation;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Runs queries over a ReportSet.
 * TODO: use background threads to issue queries.  But ReportSet isn't thread-safe yet.
 */
public class QueryProcessor {
  private final ReportSet reportSet;
  private final Taxonomy taxonomy;
  private final Checklist checklist;
  private boolean keepInvalidSightingsAfterUpdate;
  private boolean dontIncludeFamilyNames;
  private boolean includeAllChecklistSpecies;
  private Predicate<Resolved> taxonFilter = Predicates.alwaysTrue();
  private ImmutableSet<Status> ignoredStatus;
  private boolean includeIncompatibleSightings;
  private boolean onlyIncludeCountableSightings;
  private Predicate<Sighting> additionalPredicate;
  private ImmutableMap<Taxonomy, Checklist> incompatibleTaxonomyChecklists;

  public QueryProcessor(
      ReportSet reportSet,
      Taxonomy taxonomy) {
    this(reportSet, taxonomy, null, null);
  }

  public QueryProcessor(
      ReportSet reportSet,
      Taxonomy taxonomy,
      @Nullable Checklist checklist,
      @Nullable Map<Taxonomy, Checklist> incompatibleTaxonomyChecklists) {
    this.reportSet = Preconditions.checkNotNull(reportSet);
    this.taxonomy = Preconditions.checkNotNull(taxonomy);
    this.checklist = checklist;
    this.incompatibleTaxonomyChecklists = incompatibleTaxonomyChecklists == null
        ? ImmutableMap.of() : ImmutableMap.copyOf(incompatibleTaxonomyChecklists);
  }

  /**
   * Run a query returning all sightings that match a predicate.
   * 
   * @param sightingPredicate a predicate identifying which sightings should be considered.
   * @param smallestType colllapse all sightings up to this taxonomic level
   * @param countablePredicate 
   */
  private QueryResults.Builder runQueryInternal(
      QueryDefinition queryDefinition,
      Taxon.Type smallestType,
      Predicate<Sighting> countablePredicate) {
    Preconditions.checkNotNull(queryDefinition);

    // If sightings have to be mapped up from subspecies or displayed in
    // a MappedTaxonomy, then resolve any sighting before displaying.
    boolean resolveAll = smallestType != Taxon.Type.subspecies
        || taxonomy instanceof MappedTaxonomy;
    QueryResults.Builder resultsBuilder = new QueryResults.Builder();
    if (keepInvalidSightingsAfterUpdate) {
      resultsBuilder.keepInvalidSightingsAfterUpdate();
    }
    if (dontIncludeFamilyNames) {
      resultsBuilder.dontIncludeFamilyNames();
    }
    if (includeAllChecklistSpecies) {
      resultsBuilder.includeAllChecklistSpecies(ignoredStatus);
    }
    resultsBuilder.withTaxonFilter(taxonFilter);

    // First, run the preprocessors (if present)
    if (queryDefinition.preprocessor().isPresent()) {
      Preprocessor preprocessor = queryDefinition.preprocessor().get();
      // Notify the preprocessors that execution is going to begin
      preprocessor.reset();
      for (Sighting sighting : reportSet.getSightings(taxonomy)) {
        preprocessor.preprocess(sighting, countablePredicate);
      }
    }
    
    Predicate<Sighting> sightingPredicate = Preconditions.checkNotNull(
        queryDefinition.predicate());
    // Drop any non-countable sighting if requested.
    if (onlyIncludeCountableSightings && countablePredicate != null) {
      sightingPredicate = Predicates.and(sightingPredicate, countablePredicate);
    }
    if (additionalPredicate != null) {
      sightingPredicate = Predicates.and(sightingPredicate, additionalPredicate);
    }
    
    for (Sighting sighting : reportSet.getSightings()) {
      // See if the sighting meets the criteria
      if (sightingPredicate.apply(sighting)) {
        // If it does, and its a compatible taxon, register it.
        if (TaxonUtils.areCompatible(sighting.getTaxonomy(), taxonomy)) {
          SightingTaxon taxon = sighting.getTaxon();
          if (resolveAll) {
            Resolved resolved = taxon.resolve(taxonomy);
            taxon = resolved.getParentOfAtLeastType(smallestType);
          }
          Optional<QueryAnnotation> annotation = queryDefinition.annotate(sighting, taxon);
          resultsBuilder.addSighting(sighting, taxon, annotation);
        } else {
          // Otherwise, store an incompatible sighting if requested.
          if (includeIncompatibleSightings) {
            SightingTaxon taxon = sighting.getTaxon();
            if (resolveAll) {
              Resolved resolved = taxon.resolve(sighting.getTaxonomy());
              taxon = resolved.getParentOfAtLeastType(smallestType);
            }
            Optional<QueryAnnotation> annotation = queryDefinition.annotate(sighting, taxon);
            resultsBuilder.addIncompatibleSighting(sighting, taxon, annotation);
          }
        }
      }
    }

    Optional<PostProcessor> postProcessor = queryDefinition.postprocessor();
    if (postProcessor.isPresent()) {
      resultsBuilder.postProcess(postProcessor.get(), countablePredicate);
    }
    return resultsBuilder;
  }

  /**
   * Run a query returning all sightings that match a predicate.
   * 
   * @param sightingPredicate a predicate identifying which sightings should be considered.
   * @param smallestType colllapse all sightings up to this taxonomic level
   */
  public QueryResults runQuery(
      QueryDefinition queryDefinition,
      @Nullable Predicate<Sighting> countablePredicate,
      Taxon.Type smallestType) {
    return new QueryResults(
        taxonomy,
        checklist,
        incompatibleTaxonomyChecklists,
        queryDefinition,
        runQueryInternal(queryDefinition, smallestType, countablePredicate),
        smallestType,
        countablePredicate);
  }

  /**
   * Run a query returning all sightings that match a predicate.
   * 
   * @param sightingPredicate a predicate identifying which sightings should be considered.
   * @param smallestType colllapse all sightings up to this taxonomic level
   */
  public QueryResults runQuery(
      Predicate<Sighting> sightingPredicate,
      @Nullable Predicate<Sighting> countablePredicate,
      Taxon.Type smallestType) {
    return runQuery(
        new PredicateQueryDefinition(sightingPredicate),
        countablePredicate,
        smallestType);
  }

  public void keepInvalidSightingsAfterUpdate() {
    this.keepInvalidSightingsAfterUpdate = true;
  }

  public void dontIncludeFamilyNames() {
    this.dontIncludeFamilyNames = true;
  }

  /** Include all checklist species in lists of taxa returned, even if there are no sightings. */
  public void includeAllChecklistSpecies(Iterable<Checklist.Status> ignoredStatus) {
    this.includeAllChecklistSpecies = true;
    this.ignoredStatus = ImmutableSet.copyOf(ignoredStatus);
  }
  
  public void withTaxonFilter(Predicate<Resolved> taxonFilter) {
    this.taxonFilter = taxonFilter;
  }

  public QueryProcessor includingIncompatibleSightings() {
    this.includeIncompatibleSightings = true;
    return this;
  }

  public QueryProcessor onlyIncludeCountableSightings() {
    this.onlyIncludeCountableSightings = true;
    return this;
  }

  public QueryProcessor withAdditionalPredicate(Predicate<Sighting> additionalPredicate) {
    if (this.additionalPredicate == null) {
      this.additionalPredicate = additionalPredicate;
    } else {
      this.additionalPredicate = Predicates.and(this.additionalPredicate, additionalPredicate);
    }
    return this;
  }
}
