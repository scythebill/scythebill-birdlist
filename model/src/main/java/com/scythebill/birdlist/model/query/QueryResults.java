/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;

import javax.annotation.Nullable;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.query.QueryDefinition.PostProcessor;
import com.scythebill.birdlist.model.query.QueryDefinition.QueryAnnotation;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.sighting.VisitInfoKeyOrdering;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Encapsulation of results from a query.
 * <p>
 * MUST be passed SightingTaxon instances that are already resolved to the taxonomy
 * passed here.
 */
public class QueryResults {
  public interface Listener {
    void resultsChanged();
  }

  private final List<Listener> listeners = Lists.newCopyOnWriteArrayList();

  static class Builder {
    private final Multimap<String, Sighting> sightings =
        ArrayListMultimap.create();
    private final Multimap<SightingTaxon, Sighting> nonSingleSightings =
        ArrayListMultimap.create();
    private final Map<Taxonomy, Multimap<String, Sighting>> incompatibleSightings =
        new LinkedHashMap<>();
    private final Map<Taxonomy, Multimap<SightingTaxon, Sighting>> incompatibleNonSingleSightings =
        new LinkedHashMap<>();
    private final Map<Sighting, QueryAnnotation> annotations = Maps.newHashMap();
    private final Set<VisitInfoKey> incompatibleVisitInfos = new LinkedHashSet<>();
    
    private boolean keepInvalidSightingsAfterUpdate;
    private boolean dontIncludeFamilyNames;
    private boolean includeAllChecklistSpecies;
    private Predicate<Resolved> taxonFilter;
    private ImmutableSet<Status> ignoredStatus;
    
    /**
     * Add a sighting to query results.  Note that taxon != sighting.getTaxon(),
     * because it may be a resolved taxon.
     */
    void addSighting(Sighting sighting, SightingTaxon taxon, Optional<QueryAnnotation> annotation) {
      if (taxon.getType() == SightingTaxon.Type.SINGLE
          || taxon.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
        sightings.put(taxon.getId(), sighting);
      } else {
        nonSingleSightings.put(taxon, sighting);
      }
      if (annotation.isPresent()) {
        annotations.put(sighting, annotation.get());
      }
    }

    void addSighting(Sighting sighting, SightingTaxon taxon) {
      addSighting(sighting, taxon, Optional.<QueryAnnotation>absent());
    }

    /** Track an incompatible sighting. 
     * @param annotation */
    void addIncompatibleSighting(Sighting sighting, SightingTaxon taxon, Optional<QueryAnnotation> annotation) {
      VisitInfoKey visitInfoKey = VisitInfoKey.forSighting(sighting);
      // Remember visit infos for incompatible sightings
      if (visitInfoKey != null) {
        incompatibleVisitInfos.add(visitInfoKey);
      }

      if (taxon.getType() == SightingTaxon.Type.SINGLE
          || taxon.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
        incompatibleSightings.computeIfAbsent(
            sighting.getTaxonomy(),
            (__) -> ArrayListMultimap.<String, Sighting>create())
            .put(taxon.getId(), sighting);
      } else {
        incompatibleNonSingleSightings.computeIfAbsent(
            sighting.getTaxonomy(),
            (__) -> ArrayListMultimap.<SightingTaxon, Sighting>create())
            .put(taxon, sighting);
      }
      
      if (annotation.isPresent()) {
        annotations.put(sighting, annotation.get());
      }
    }

    void dontIncludeFamilyNames() {
      this.dontIncludeFamilyNames = true;
    }
    
    void keepInvalidSightingsAfterUpdate() {
      this.keepInvalidSightingsAfterUpdate = true;
    }

    /** Include all checklist species in lists of taxa returned, even if there are no sightings. 
     * @param ignoredStatus */
    public void includeAllChecklistSpecies(Iterable<Status> ignoredStatus) {
      this.includeAllChecklistSpecies = true;
      this.ignoredStatus = ImmutableSet.copyOf(ignoredStatus);
    }

    public void withTaxonFilter(Predicate<Resolved> taxonFilter) {
      this.taxonFilter = taxonFilter;
    }

    public void postProcess(PostProcessor postProcessor, @Nullable Predicate<Sighting> countablePredicate) {
      // Remove anything that fails post-processing
      
      // First for all single taxa
      Iterator<String> sightingsIterator = sightings.keySet().iterator();
      while (sightingsIterator.hasNext()) {
        String taxonId = sightingsIterator.next();
        if (!postProcessor.acceptTaxon(
            SightingTaxons.newSightingTaxon(taxonId), sightings.get(taxonId), countablePredicate)) {
          sightingsIterator.remove();
        }
      }
      
      // Then for all non-single taxa
      Iterator<SightingTaxon> nonSingleSightingsIterator =
          nonSingleSightings.keySet().iterator();
      while (nonSingleSightingsIterator.hasNext()) {
        SightingTaxon taxon = nonSingleSightingsIterator.next();
        if (!postProcessor.acceptTaxon(taxon, nonSingleSightings.get(taxon), countablePredicate)) {
          nonSingleSightingsIterator.remove();
        }
      }
    }
  }

  private final Taxonomy taxonomy;
  private final Checklist checklist;
  private final ImmutableMap<Taxonomy, Checklist> incompatibleTaxonomyChecklists;
  // Data result sets in Scythebill are small enough that crudely inserting into
 // a simple Multimap is sufficient.
  private final Multimap<String, Sighting> sightings;
  private final boolean keepInvalidSightingsAfterUpdate;
  private final boolean includeFamilyNames;
  private final boolean includeAllChecklistSpecies;
  private final Taxon.Type smallestType;
  private final Predicate<Sighting> countable;
  private final QueryDefinition queryDefinition;
  private final Predicate<Resolved> taxonFilter;
  private final Map<Sighting, QueryAnnotation> annotations;
  private final Map<QueryAnnotation, Set<Sighting>> sightingsWithAnnotation = Maps.newHashMap();
  private final Map<QueryAnnotation, Set<Resolved>> taxaWithAnnotation = Maps.newHashMap();
  private Set<String> countableSightingTaxa;
  private Set<SightingTaxon> countableNonSingleSightingTaxa;
  private List<Resolved> taxaAsList;
  private Set<String> speciesIds;
  private List<Resolved> countableTaxaAsList;
  private Multimap<SightingTaxon, Sighting> nonSingleSightings;
  private boolean queryInvalid;
  private ImmutableSet<Status> ignoredStatus;
  
  // Information about "incompatible" data - data from other taxonomies,
  // in case there is a desired to produce output for other taxonomies with
  // the main report.
  private final Set<VisitInfoKey> incompatibleVisitInfos;
  private final Map<Taxonomy, Multimap<String, Sighting>> incompatibleSightings;
  private final Map<Taxonomy, Multimap<SightingTaxon, Sighting>> incompatibleNonSingleSightings;

  /** Full results from a query. */
  QueryResults(
      Taxonomy taxonomy,
      @Nullable Checklist checklist,
      @Nullable Map<Taxonomy, Checklist> incompatibleTaxonomyChecklists,
      QueryDefinition queryDefinition,
      QueryResults.Builder builder,
      Taxon.Type smallestType,
      @Nullable Predicate<Sighting> countable) {
    this.taxonomy = taxonomy;
    this.checklist = checklist;
    this.incompatibleTaxonomyChecklists =
        incompatibleTaxonomyChecklists == null ? ImmutableMap.of() : ImmutableMap.copyOf(incompatibleTaxonomyChecklists);
    this.queryDefinition = Preconditions.checkNotNull(queryDefinition);
    this.taxonFilter = Preconditions.checkNotNull(builder.taxonFilter);
    this.countable = countable;
    this.sightings = builder.sightings;
    this.nonSingleSightings = builder.nonSingleSightings;
    this.smallestType = smallestType;
    this.annotations = builder.annotations;
    this.keepInvalidSightingsAfterUpdate = builder.keepInvalidSightingsAfterUpdate;
    this.includeFamilyNames = !builder.dontIncludeFamilyNames;
    this.includeAllChecklistSpecies = builder.includeAllChecklistSpecies;
    this.ignoredStatus = builder.ignoredStatus;
    this.incompatibleVisitInfos = Collections.unmodifiableSet(builder.incompatibleVisitInfos);
    this.incompatibleSightings = builder.incompatibleSightings;
    this.incompatibleNonSingleSightings = builder.incompatibleNonSingleSightings;
    precomputeResults(true);
  }

  /**
   * Precompute (or recompute) query results, and discard any cached data.
   */
  private void precomputeResults(boolean updateTaxa) {
    this.countableSightingTaxa = getCountablePredicate() == null
        ? sightings.keySet()
        : ImmutableSet.copyOf(Multimaps.filterValues(sightings, getCountablePredicate()).keySet());
    this.countableNonSingleSightingTaxa = getCountablePredicate() == null
        ? nonSingleSightings.keySet()
        : ImmutableSet.copyOf(
            Multimaps.filterValues(nonSingleSightings, getCountablePredicate()).keySet());
        
    if (updateTaxa) {
      // TODO(awiner): "true" means include families.  Most don't need, some
      // do, so always getting it this way is wacky.  Also a bit odd that we force
      // this to be computed up-front, but it ends up getting used in a bunch of places
      this.taxaAsList = getResolvedTaxa(
              taxonomy, sightings.keySet(), nonSingleSightings.keySet(), includeFamilyNames,
              includeAllChecklistSpecies);
      this.countableTaxaAsList = getCountablePredicate() == null
          ? taxaAsList
          : getResolvedTaxa(taxonomy, countableSightingTaxa, countableNonSingleSightingTaxa,
              includeFamilyNames, includeAllChecklistSpecies);
    }
    
    // Discard cached species IDs.
    speciesIds = null;
  }
  
  public Taxonomy getTaxonomy() {
    return taxonomy;
  }
  
  /**
   * Return the smallest type of taxon requested.
   */
  public Taxon.Type getSmallestType() {
    return smallestType;
  }
  
  /**
   * Returns the total number of countable species, optionally mixing in
   * non-single taxa when none of their constituents are present.
   */
  public int getCountableSpeciesSize(Taxonomy taxonomy, boolean includeNonSingle) {
    Set<String> countableSpeciesIds = getCountableSpeciesIds(taxonomy);
    if (!includeNonSingle) {
      return countableSpeciesIds.size();
    }
    
    int nonSingleCount = countNonSingleTaxa(taxonomy, countableSpeciesIds, Taxon.Type.species);
    return countableSpeciesIds.size() + nonSingleCount;
  }

  /**
   * Return a Set containing IDs of all species from the query results.
   */
  Set<String> getCountableSpeciesIds(Taxonomy taxonomy) {
    if (taxonomy == getTaxonomy()) {
      if (speciesIds == null) {
        if (smallestType == Taxon.Type.species) {
          speciesIds = countableSightingTaxa;
      
          // Iterate through all non-single taxa.  Resolve, and raise to species level.
          // Mix *those* in to the species totals as needed.
          Set<String> nonSingleButSpecies = Sets.newHashSet(); 
          for (SightingTaxon taxon : countableNonSingleSightingTaxa) {
            Resolved resolved = taxon.resolveInternal(taxonomy);
            if (resolved.getSmallestTaxonType() != Taxon.Type.species) {
              // If they're not already at species level, raise to species level
              // and see if that makes them a single species.
              SightingTaxon species = resolved.getParentOfAtLeastType(Taxon.Type.species);
              if (species.getType() == SightingTaxon.Type.SINGLE
                  && !speciesIds.contains(species.getId())) {
                nonSingleButSpecies.add(species.getId());
              }
            }
          }
          
          if (!nonSingleButSpecies.isEmpty()) {
            speciesIds = Sets.union(nonSingleButSpecies, speciesIds);
          }
        } else {
          List<Resolved> taxa = countableTaxaAsList;
          speciesIds = TaxonUtils.getTaxaAtLevel(taxa, Type.species);
        }
      }
      return Collections.unmodifiableSet(speciesIds);
    } else {
      Set<String> incompatibleSpeciesIds = new LinkedHashSet<>();
      for (Resolved resolved : getCountableTaxaAsList(taxonomy, /*includeChecklist=*/false)) {
        if (resolved.getLargestTaxonType() == Taxon.Type.family) {
          continue;
        }
        
        SightingTaxon species = resolved.getParentOfAtLeastType(Taxon.Type.species);
        if (species.getType() == SightingTaxon.Type.SINGLE) {
          incompatibleSpeciesIds.add(species.getId());
        }
      }
      return incompatibleSpeciesIds;
    }
  }

  /**
   * Returns the total number of countable groups and species, optionally mixing in
   * non-single taxa when none of their constituents are present.
   */
  public int getCountableGroupsAndSpeciesSize(Taxonomy taxonomy, boolean includeNonSingle) {
    Set<String> countableGroupsIds = getCountableGroupsAndSpeciesIds(taxonomy);
    if (!includeNonSingle) {
      return countableGroupsIds.size();
    }
    
    int nonSingleCount = countNonSingleTaxa(taxonomy, countableGroupsIds, Taxon.Type.group);
    return countableGroupsIds.size() + nonSingleCount;
  }

  /** Return a Set of sightings with a particular annotation. */
  public Set<Sighting> getSightingsWithAnnotation(QueryAnnotation annotation) {
    if (!sightingsWithAnnotation.containsKey(annotation)) {
      Set<Sighting> filtered = Maps.filterValues(
          annotations, Predicates.equalTo(annotation)).keySet();
      sightingsWithAnnotation.put(annotation, filtered);
    }
    
    return sightingsWithAnnotation.get(annotation);
  }
  
  /** Return a Set of sightings with a particular annotation. */
  public Set<Resolved> getTaxaWithAnnotation(Taxonomy taxonomy, QueryAnnotation annotation) {
    if (taxonomy == getTaxonomy()
        && taxaWithAnnotation.containsKey(annotation)) {
      return taxaWithAnnotation.get(annotation);      
    }

    Set<Resolved> taxa = Sets.newHashSet();
    for (Sighting sighting : getSightingsWithAnnotation(annotation)) {
      if (TaxonUtils.areCompatible(taxonomy, sighting.getTaxonomy())) {
        SightingTaxon taxon = sighting.getTaxon();
        Resolved resolved = taxon.resolve(taxonomy);
        taxa.add(resolved);
        
        while (resolved.getLargestTaxonType() != Taxon.Type.species) {
          resolved = resolved.getParent().resolveInternal(taxonomy);
          taxa.add(resolved);
        }
      }
    }
    
    // Cache for the primary taxonomy
    if (taxonomy == getTaxonomy()) {
      taxaWithAnnotation.put(annotation, taxa);
    }
    
    return taxa;
  }
    
  /** Return a Set of sightings with a particular annotation, raised to a particular level. */
  public Set<Resolved> getTaxaWithAnnotation(Taxonomy taxonomy, QueryAnnotation annotation, Taxon.Type type) {
    Set<Resolved> taxa = Sets.newHashSet();
    for (Sighting sighting : getSightingsWithAnnotation(annotation)) {
      if (TaxonUtils.areCompatible(taxonomy, sighting.getTaxonomy())) {
        Resolved resolved = sighting.getTaxon().resolve(taxonomy);
        SightingTaxon parentOfAtLeastType = resolved.getParentOfAtLeastType(type);
        Optional<QueryAnnotation> annotatedAtType = queryDefinition.annotate(sighting, parentOfAtLeastType);
        if (annotatedAtType.isPresent() && annotation == annotatedAtType.get()) {
          resolved = parentOfAtLeastType.resolveInternal(taxonomy);
          taxa.add(resolved);
        }
      }
    }
    return taxa;
  }
    
  /**
   * Return a Set containing IDs of all groups and species from the query results,
   * but omitting species when a contained group is present.
   * <p>
   * In particular, this means that when a subspecies is present that does not
   * belong in a group, it will be counted (as a species) only if there are no
   * groups.
   */
  Set<String> getCountableGroupsAndSpeciesIds(Taxonomy taxonomy) {
    List<Resolved> taxa = getCountableTaxaAsList(taxonomy, includeAllChecklistSpecies);
    // Fetch everything up to the group level (including the group that subspecies are in)
    Set<String> groupSet = TaxonUtils.getTaxaAtLevel(taxa, Type.group);
    
    // Now find anything that is a subspecies or species, and raise to a species (since
    // we already found the groups).  And add those if no child is present.
    for (Resolved taxon : taxa) {
      if (taxon.getType() == SightingTaxon.Type.SINGLE) {
        // Single - skip over groups, since those were counted above
        if (taxon.getSmallestTaxonType() == Taxon.Type.species
            || taxon.getSmallestTaxonType() == Taxon.Type.subspecies) {
          Taxon speciesTaxon = TaxonUtils.getParentOfType(taxon.getTaxon(), Taxon.Type.species);
          if (!TaxonUtils.isItselfOrAChildIn(speciesTaxon, groupSet)) {
            groupSet.add(speciesTaxon.getId());
          }
        }
      } else {
        // Multiple.  Skip over species, since those can't resolve to a single species
        if (taxon.getSmallestTaxonType() != Taxon.Type.species) {
          SightingTaxon species = taxon.getParentOfAtLeastType(Taxon.Type.species);
          if (species.getType() == SightingTaxon.Type.SINGLE) {
            Taxon speciesTaxon = taxonomy.getTaxon(species.getId());
            if (!TaxonUtils.isItselfOrAChildIn(speciesTaxon, groupSet)) {
              groupSet.add(species.getId());
            }
          }
        }
      }
    }
    
    return Collections.unmodifiableSet(groupSet);
  }
  
  /**
   * Return a Set containing IDs of all subspecies, groups, and species from the
   * query results, but omitting species when a contained group is present,
   * and groups when a contained subspecies is present.
   */
  Set<String> getCountableSubspeciesGroupsAndSpeciesIds(Taxonomy taxonomy) {
    List<Resolved> taxa = getCountableTaxaAsList(taxonomy, includeAllChecklistSpecies);
    Set<String> subspeciesSet = TaxonUtils.getTaxaAtLevel(taxa, Type.subspecies);
    // First pass:  add groups when no subspecies of that group is present
    for (Resolved taxon : taxa) {
      if (taxon.getType() == SightingTaxon.Type.SINGLE) {
        if (taxon.getSmallestTaxonType() == Taxon.Type.group) {
          if (!TaxonUtils.isItselfOrAChildIn(taxon.getTaxon(), subspeciesSet)) {
            subspeciesSet.add(taxon.getTaxon().getId());
          }
        }
      }
    }
    // Second pass: add species when no group or subspecies of that species is present
    for (Resolved taxon : taxa) {
      if (taxon.getType() == SightingTaxon.Type.SINGLE) {
        if (taxon.getSmallestTaxonType() == Taxon.Type.species) {
          if (!TaxonUtils.isItselfOrAChildIn(taxon.getTaxon(), subspeciesSet)) {
            subspeciesSet.add(taxon.getTaxon().getId());
          }
        }
      }
    }
    // Third pass: non-single species
    for (Resolved taxon : taxa) {
      if (taxon.getType() != SightingTaxon.Type.SINGLE
          && taxon.getSmallestTaxonType() != Taxon.Type.species) {
        // Raise to group (or species, if not in a group)
        SightingTaxon sightingTaxon = taxon.getParentOfAtLeastType(Taxon.Type.group);
        Taxon taxonToTest = null;
        if (sightingTaxon.getType() == SightingTaxon.Type.SINGLE) {
          taxonToTest = taxonomy.getTaxon(sightingTaxon.getId());
        } else {
          sightingTaxon = taxon.getParentOfAtLeastType(Taxon.Type.species);
          if (sightingTaxon.getType() == SightingTaxon.Type.SINGLE) {
            taxonToTest = taxonomy.getTaxon(sightingTaxon.getId());
          }
        }
        if (taxonToTest != null) {
          if (!TaxonUtils.isItselfOrAChildIn(taxonToTest, subspeciesSet)) {
            subspeciesSet.add(taxonToTest.getId());
          }
        }
      }
    }
    
    return Collections.unmodifiableSet(subspeciesSet);
  }
  
  /**
   * Returns the total number of countable groups and species, optionally mixing in
   * non-single taxa when none of their constituents are present.
   */
  public int getCountableSubspeciesGroupsAndSpeciesSize(Taxonomy taxonomy, boolean includeNonSingle) {
    Set<String> countableSubspeciesIds = getCountableSubspeciesGroupsAndSpeciesIds(taxonomy);
    if (!includeNonSingle) {
      return countableSubspeciesIds.size();
    }
    
    int nonSingleCount = countNonSingleTaxa(taxonomy, countableSubspeciesIds, Taxon.Type.subspecies);
    return countableSubspeciesIds.size() + nonSingleCount;
  }

  /**
   * Count the number of (countable) non-single taxa at a given level.
   */
  private int countNonSingleTaxa(
      Taxonomy taxonomy, Set<String> countableTaxaIds, Taxon.Type taxonType) {
    int nonSingleCount = 0;
    Set<String> nonSingleIds = Sets.newHashSet();
    
    // For every non-single, species-level sp.:
    //   - See if any of its constituent parts has already been tallied
    //   - If none have, add it to the list
    //   - Unless it overlaps with a different sp.
    // Note that this *could* in theory cause an undercount:
    //   A/B, B/C, and A/D should go down as two, but would in fact
    // get recorded as just one (since A/B would come first, and preclude
    // counting either B/C or A/D).  In practice, this is outstandingly
    // unlikely to ever occur, and a full solution is going to be slower.
    for (Resolved resolved : getCountableTaxaAsList(taxonomy, /*includeChecklist=*/false)) {
      if (resolved.getType() == SightingTaxon.Type.SP
          // Not obvious whether getLargest... or getSmallest... is ideal.  They're both weird for here,
          // but it's rare enough to have them  different *and* for people to care about the exact value
          // here that I'll punt on this...
          && resolved.getLargestTaxonType() == taxonType) {
        boolean alreadyCounted = false;
        for (String id : resolved.getSightingTaxon().getIds()) {
          if (countableTaxaIds.contains(id) || nonSingleIds.contains(id)) {
            alreadyCounted = true;
            break;
          }
        }
        
        if (!alreadyCounted) {
          nonSingleCount++;
          for (String id : resolved.getSightingTaxon().getIds()) {
            nonSingleIds.add(id);
          }
        }
      }
    }
    return nonSingleCount;
  }

  public List<Resolved> getTaxaAsList() {
    return Collections.unmodifiableList(taxaAsList);
  }

  public List<VisitInfoKey> getAllVisitInfoKeys() {
    SortedSet<VisitInfoKey> visitInfoKeys = Sets.newTreeSet(new VisitInfoKeyOrdering());
    // Add any incompatible visit infos
    visitInfoKeys.addAll(incompatibleVisitInfos);
    for (Sighting sighting : sightings.values()) {
      VisitInfoKey visitInfoKey = VisitInfoKey.forSighting(sighting);
      if (visitInfoKey != null) {
        visitInfoKeys.add(visitInfoKey);
      }
    }
    
    return ImmutableList.copyOf(visitInfoKeys);
  }
  
  public List<VisitInfoKey> getVisitInfoKeys(String locationId) {
    SortedSet<VisitInfoKey> visitInfoKeys = Sets.newTreeSet(new VisitInfoKeyOrdering());
    for (Sighting sighting : sightings.values()) {
      if (locationId.equals(sighting.getLocationId())) {
        VisitInfoKey visitInfoKey = VisitInfoKey.forSighting(sighting);
        if (visitInfoKey != null) {
          visitInfoKeys.add(visitInfoKey);
        }
      }
    }
    
    return ImmutableList.copyOf(visitInfoKeys);
  }
  

  public Collection<Sighting> getAllSightings(Taxon taxon) {
    return getAllSightings(SightingTaxons.newResolved(taxon));
  }

  public Collection<Sighting> getAllSightings(Resolved taxon) {
    return Collections.unmodifiableCollection(getTaxonSightings(taxon));
  }

  public Sighting getBestSighting(Taxon taxon, Ordering<Sighting> ordering) {
    return getBestSighting(SightingTaxons.newResolved(taxon), ordering);
  }

  /**
   * Returns true if any one of the sightings of that taxon is countable.
   */
  public boolean isCountable(Resolved taxon) {
    if (getCountablePredicate() == null) {
      return true;
    }

    Collection<Sighting> taxonSightings = getTaxonSightings(taxon);
    return taxonSightings.stream().anyMatch(getCountablePredicate());
  }
  
  public Sighting getBestSighting(Resolved taxon, Ordering<Sighting> ordering) {
    Collection<Sighting> taxonSightings = getTaxonSightings(taxon);
    
    return taxonSightings.isEmpty()
            ? null
            : ordering.max(taxonSightings);
  }

  public List<Sighting> getBestSightings(
      Resolved taxon, Ordering<Sighting> ordering, int count) {
    Collection<Sighting> taxonSightings = getTaxonSightings(taxon);
    
    return taxonSightings.isEmpty()
            ? ImmutableList.<Sighting>of()
            : ordering.greatestOf(taxonSightings, count);
  }
  
  private Collection<Sighting> getTaxonSightings(Resolved taxon) {
    if (taxon.getTaxonomy() == getTaxonomy()) {
      switch (taxon.getType()) {
        case SINGLE:
          return sightings.get(taxon.getTaxon().getId());
        default:
          return nonSingleSightings.get(taxon.getSightingTaxon());
      }
    } else {
      return getAllSightingsIncompatible(taxon);
    }
  }

  /** Return true if the taxon is present. */
  public boolean containsTaxon(Taxon taxon) {
    // TODO: broken, this fails to report properly on group/subspecies sp.
    // within a single species!
    // TODO: broken if taxon is in another taxonomy
    return sightings.containsKey(taxon.getId());
  }

  /** Return true if the taxon is present *and* countable. */
  public boolean containsTaxonCountably(Taxon taxon) {
    // TODO: broken, this fails to report properly on group/subspecies sp.
    // within a single species!
    // TODO: broken if taxon is in another taxonomy
    return countableSightingTaxa.contains(taxon.getId());
  }

  public int getSightingsCount(Taxonomy taxonomy) {
    if (taxonomy == getTaxonomy()) {
      return sightings.size() + nonSingleSightings.size();
    } else {
      return incompatibleSightings.getOrDefault(taxonomy, ImmutableMultimap.<String, Sighting>of())
          .size()
          + incompatibleNonSingleSightings
              .getOrDefault(taxonomy, ImmutableMultimap.<SightingTaxon, Sighting>of()).size();

    }
  }
  
  /**
   * Given a collection of IDs, returns a List of all taxa in
   * that, optionally adding in the families in that collection,
   * in taxonomic order.
   * @param taxonomy the taxonomy all must match
   * @param ids the collection of String IDs;  should have a fast
   *     contains() implementation so this is not O(N).
   * @param nonSingleTaxa 
   * @param includeFamilies if true, the returned list will include
   *     the family ancestors of each contained taxon
   * @param includeChecklist 
   */
  private List<Resolved> getResolvedTaxa(
      Taxonomy taxonomy,
      Set<String> ids,
      Set<SightingTaxon> nonSingleTaxa,
      boolean includeFamilies,
      boolean includeChecklist) {
    List<Resolved> list = Lists.newArrayListWithExpectedSize(ids.size());
    Multimap<String, SightingTaxon> idsWithNonSingleTaxa = ArrayListMultimap.create();
    for (SightingTaxon taxon : nonSingleTaxa) {
      for (String id : taxon.getIds()) {
        idsWithNonSingleTaxa.put(id, taxon); 
      }      
    }
    
    addTaxa(list, taxonomy, ids, idsWithNonSingleTaxa, taxonomy.getRoot(), includeFamilies, includeChecklist, null);
    return list;
  }

  /** Recursive function for building up the list of Resolved taxa. */
  private boolean addTaxa(
      List<Resolved> list,
      Taxonomy taxonomy,
      Set<String> ids,
      Multimap<String, SightingTaxon> idsWithNonSingleTaxa,
      Taxon taxon,
      boolean includeFamilies,
      boolean includeChecklist,
      Taxon currentFamily) {
    
    boolean added = false;
    if (taxon.getType().compareTo(getSmallestType()) < 0) {
      return false;
    }
    
    if (ids.contains(taxon.getId()) ||
        includeChecklist && shouldBeAddedFromChecklist(taxon)) {
      
      // TODO: this creates two objects just to wrap a Taxon.  Not sure it matters.
      Resolved resolved = SightingTaxons.newResolved(taxon);
      if (taxonFilter.apply(resolved)) {
        if (includeFamilies && (currentFamily != null)) {
          list.add(SightingTaxons.newResolved(currentFamily));
        }
        
        list.add(resolved);
        
        added = true;
        currentFamily = null;
      }
    }

    if (taxon.getType() == Taxon.Type.family) {
      currentFamily = taxon;
    }

    for (Taxon child : taxon.getContents()) {
      if (addTaxa(list, taxonomy, ids, idsWithNonSingleTaxa, child, includeFamilies,
          includeChecklist, currentFamily)) {
        added = true;
        currentFamily = null;
      }
    }
    
    // After all children have been parsed, see if there are any relevant non-single taxa
    // to handle.  For each non-single sighting taxa that is present and includes this
    // taxon:
    //  - See if we've already handled all the other taxa in it
    //  - If we have, then output it
    //  - Otherwise, mark that we've encountered this taxa (by removing it from
    //    "idsWithNonSingleTaxa")
    if (idsWithNonSingleTaxa.containsKey(taxon.getId())) {
      Collection<SightingTaxon> sightingTaxa = idsWithNonSingleTaxa.get(taxon.getId());
      for (SightingTaxon sightingTaxon : sightingTaxa) {
        boolean stillPending = false;
        for (String id : sightingTaxon.getIds()) {
          if (!id.equals(taxon.getId())
             && idsWithNonSingleTaxa.containsKey(id)) {
            stillPending = true;
            break;
          }
        }
        if (!stillPending) {
          Resolved resolved = sightingTaxon.resolveInternal(taxonomy);
          if (taxonFilter.apply(resolved)) {
            if (includeFamilies && (currentFamily != null)) {
              list.add(SightingTaxons.newResolved(currentFamily));
            }
            added = true;
            currentFamily = null;
            list.add(resolved);
          }
        }
      }
      
      idsWithNonSingleTaxa.removeAll(taxon.getId());
    }
    
    return added;
  }

  /**
   * Returns true if the taxon should be added to the query results based purely on
   * the checklist contents.
   */
  private boolean shouldBeAddedFromChecklist(Taxon taxon) {
    if (!includeAllChecklistSpecies) {
      return false;
    }
    
    Checklist checklist = getChecklist(taxon.getTaxonomy());
    if (checklist == null) {
      return false;
    }
    
    // Checklist doesn't contain the taxon - false.
    if (!checklist.includesTaxon(taxon)) {
      return false;
    }
    
    // No statuses are ignored -  true.
    if (ignoredStatus.isEmpty()) {
      return true;
    }
    
    Status status = checklist.getStatus(
        taxonomy, SightingTaxons.newSightingTaxon(taxon.getId()));
    // Return true only if the status should not be ignored.
    return !ignoredStatus.contains(status);
  }

  /**
   * Remove all sightings from the query results.
   */
  public void removeSightings(Collection<Sighting> removedSightings) {
    updateSightings(removedSightings, ImmutableList.<Sighting>of(), true);
  }

  /**
   * Updates QueryResults for removed and added sightings.
   */
  public void updateSightings(
      Collection<Sighting> removedSightings,
      Collection<Sighting> addedSightings,
      boolean updateTaxa) {
    // FIXME: update/clear annotations?
    
    sightings.values().removeAll(removedSightings);
    nonSingleSightings.values().removeAll(removedSightings);
    for (Sighting addedSighting : addedSightings) {
      // See if the added sighting is valid or not
      // TODO: this doesn't apply to annotations, which would need to be reprocessed
      boolean isValidSighting = queryDefinition.predicate().apply(addedSighting);
      if (!isValidSighting) {
        // If it isn't, then there's two options:
        //   - Keep the sighting around, but mark the query results as invalid, so
        //     a UI can reissue the query
        //   - Immediately drop the invalid results from the query.
        // Which to use depends on the relevant UI. 
        if (keepInvalidSightingsAfterUpdate) {
          queryInvalid = true;
        } else {
          continue;
        }
      }
      
      SightingTaxon taxon = addedSighting.getTaxon();
      Resolved resolved = taxon.resolve(taxonomy);
      taxon = resolved.getParentOfAtLeastType(smallestType);
      if (taxon.getType() == SightingTaxon.Type.SINGLE
          || taxon.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
        sightings.put(taxon.getId(), addedSighting);
      } else {
        nonSingleSightings.put(taxon, addedSighting);
      }
    }
    
    precomputeResults(updateTaxa);
    notifyListeners();
  }
  
  /**
   * Gathers a sorted multimap from Sighting to Resolved.
   */
  public TreeMap<Sighting, Resolved> gatherSortedSightings(
      List<Resolved> taxa,
      Ordering<Sighting> sightingOrdering,
      int sightingsCount) {
    // Gather  all the sightings, sorted as needed
    TreeMap<Sighting, Resolved> allSightings = Maps.newTreeMap(
        // Compound with an "arbitrary" (object-identity) ordering, because
        // we don't want to merge sightings from the same day.
        sightingOrdering.reverse().compound(Ordering.arbitrary()));
    for (Resolved taxon : taxa) {
      if (taxon.getSmallestTaxonType() == Taxon.Type.family) {
        continue;
      }
      
      // Grab sightings for this taxon;  always grab at least one sighting,
      // so it's possible to sort, even if no sighting output is needed.
      for (Sighting sighting : getBestSightings(
              taxon, sightingOrdering, Math.max(1, sightingsCount))) {
        allSightings.put(sighting, taxon);
      }        
    }
    
    return allSightings;
  }
  /**
   * Resolve a SightingTaxon into a Resolved for these QueryResults, which
   * may require boosting that sighting to a higher taxon.
   */
  public Resolved resolveToType(SightingTaxon taxon) {
    Resolved resolved = taxon.resolve(taxonomy);
    taxon = resolved.getParentOfAtLeastType(smallestType);
    // This process needs to align with the way the taxon list is built for getTaxaAsList(),
    // and QueryResults always treats "single-with-secondary-subspecies" as a single taxon
    // keyed based on just the primary ID. 
    if (taxon.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
      taxon = SightingTaxons.newSightingTaxon(taxon.getId()); 
    }
    return taxon.resolveInternal(taxonomy);
  }
  
  public void addListener(Listener listener) {
    listeners.add(Preconditions.checkNotNull(listener));
  }
  
  private void notifyListeners() {
    for (Listener listener : listeners) {
      listener.resultsChanged();
    }
  }

  /**
   * If true, the query is now invalid - it contains results that do not pass the Predicate.
   */
  public boolean isQueryInvalid() {
    return queryInvalid;
  }

  public Iterable<Sighting> getAllSightings() {
    return Iterables.concat(
        sightings.values(),
        nonSingleSightings.values());
  }
  
  @Nullable
  public Checklist getChecklist(Taxonomy taxonomy) {
    if (taxonomy == getTaxonomy()) {
      return checklist;
    } else {
      return incompatibleTaxonomyChecklists.get(taxonomy);
    }
  }

  public Optional<QueryAnnotation> getAnnotation(Sighting sighting, SightingTaxon taxon) {
    return queryDefinition.annotate(sighting, taxon);
  }

  public boolean containsAnnotation(QueryAnnotation annotation, Resolved resolved) {
    return getTaxaWithAnnotation(resolved.getTaxonomy(), annotation).contains(resolved);
  }

  public int getFamilyCount(Taxonomy taxonomy) {
    int familyCount = 0;
    for (Resolved resolved : getCountableTaxaAsList(taxonomy, includeAllChecklistSpecies)) {
      if (resolved.getSmallestTaxonType() == Taxon.Type.family) {
        familyCount++;
      }
    }
    
    return familyCount;
  }
  
  public int getSpuhCount() {
    // TODO: does this include ssp. "spuhs"?
    return (int) countableTaxaAsList
        .stream()
        .filter(s -> s.getType() == SightingTaxon.Type.SP)
        .count();
  }
  
  public Collection<Taxonomy> getIncompatibleTaxonomies() {
    return ImmutableSet.<Taxonomy>builder()
        .addAll(incompatibleSightings.keySet())
        .addAll(incompatibleNonSingleSightings.keySet())
        .build();
  }
  
  public List<Resolved> getIncompatibleTaxaAsList(
      Taxonomy taxonomy,
      boolean includeFamilies) {
    if (!incompatibleSightings.containsKey(taxonomy)
        && !incompatibleNonSingleSightings.containsKey(taxonomy)
        && !includeAllChecklistSpecies) {
      return ImmutableList.of();
    }
    
    return getResolvedTaxa(taxonomy, 
        incompatibleSightings.getOrDefault(
            taxonomy, ImmutableMultimap.<String, Sighting>of()).keySet(),
        incompatibleNonSingleSightings.getOrDefault(
            taxonomy, ImmutableMultimap.<SightingTaxon, Sighting>of())
        .keySet(), includeFamilies, includeAllChecklistSpecies);
  }

  private Collection<Sighting> getAllSightingsIncompatible(Resolved taxon) {
    switch (taxon.getType()) {
      case SINGLE:
        return Collections.unmodifiableCollection(
            incompatibleSightings.getOrDefault(
                taxon.getTaxonomy(), ImmutableMultimap.<String, Sighting>of())
            .get(taxon.getTaxon().getId()));
      default:
        SightingTaxon sightingTaxon = taxon.getSightingTaxon();
        return Collections.unmodifiableCollection(
            incompatibleNonSingleSightings.getOrDefault(
                taxon.getTaxonomy(), ImmutableMultimap.<SightingTaxon, Sighting>of())
            .get(sightingTaxon));
    }
  }
  
  private List<Resolved> getCountableTaxaAsList(Taxonomy taxonomy, boolean includeChecklist) {
    if (taxonomy == getTaxonomy()) {
      return countableTaxaAsList;
    } else {
      // TODO: this is potentially getting called *a lot*.  Consider caching.
      Multimap<String, Sighting> countableSingleSightings =
          incompatibleSightings.getOrDefault(taxonomy, ImmutableMultimap.<String, Sighting>of());
      if (getCountablePredicate() != null) {
        countableSingleSightings = Multimaps.filterValues(countableSingleSightings, getCountablePredicate()); 
      }

      Multimap<SightingTaxon, Sighting> countableNonSingleSightings = incompatibleNonSingleSightings.getOrDefault(taxonomy,
          ImmutableMultimap.<SightingTaxon, Sighting>of());
      if (getCountablePredicate() != null) {
        countableNonSingleSightings =
            Multimaps.filterValues(countableNonSingleSightings, getCountablePredicate());
      }
      
      return getResolvedTaxa(taxonomy, 
          countableSingleSightings.keySet(),
          countableNonSingleSightings.keySet(),
          includeFamilyNames,
          includeChecklist);
    }
  }

  public Predicate<Sighting> getCountablePredicate() {
    return countable;
  }
}
