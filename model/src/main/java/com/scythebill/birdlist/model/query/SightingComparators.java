/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import org.joda.time.Chronology;
import org.joda.time.DateTimeFieldType;
import org.joda.time.Partial;
import org.joda.time.ReadablePartial;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Ordering;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.ResolvedComparator;

/**
 * Comparator instances pertaining to Sighting objects.
 */
public class SightingComparators {
  private SightingComparators() { }
  
  /** An ordering that puts heard-only species last. */
  public static Ordering<Sighting> preferNotHeardOnly() {
    return new HeardOnlyComparator();
  }

  /** An ordering that puts more recent sightings towards the front of a list. */
  public static Ordering<Sighting> preferMoreRecent() {
    return new MoreRecentComparator();
  }

  /** An ordering that puts older sightings towards the front of a list. */
  public static Ordering<Sighting> preferEarlier() {
    return new MoreRecentComparator().reverse();
  }

  /** An ordering that puts earlier-in-the-year sightings towards the front of a list. */
  public static Ordering<Sighting> preferEarlierInTheYear() {
    return new LaterInTheYearComparator().reverse();
  }

  /** An ordering that prefers non-introduced populations. */
  public static Ordering<Sighting> preferNotIntroduced() {
    return new PreferNotIntroducedComparator();
  }

  /** An ordering that prioritizes higher counts. */
  public static Ordering<Sighting> preferHigherCounts() {
    return new HigherCountComparator();
  }

  public static Ordering<Sighting> inTaxonomicOrder(Taxonomy taxonomy) {
    return new TaxonomicOrdering(taxonomy);
  }
  
  private static final class HeardOnlyComparator extends Ordering<Sighting> {
    @Override
    public int compare(Sighting a, Sighting b) {
      int aValue = a.getSightingInfo().isHeardOnly() ? 1 : 0;
      int bValue = b.getSightingInfo().isHeardOnly() ? 1 : 0;
      return bValue - aValue;
    }
  }
  
  // Higher is "worse"
  private static final ImmutableMap<SightingStatus, Integer> SIGHTING_STATUS_VALUES;
  static {
    ImmutableMap.Builder<SightingStatus, Integer> builder = ImmutableMap.builder();
    builder.put(SightingStatus.NONE, 0);
    builder.put(SightingStatus.RECORD_NOT_ACCEPTED, 1);
    builder.put(SightingStatus.INTRODUCED, 2);
    builder.put(SightingStatus.BETTER_VIEW_DESIRED, 3);
    builder.put(SightingStatus.INTRODUCED_NOT_ESTABLISHED, 4);
    builder.put(SightingStatus.RESTRAINED, 5);
    builder.put(SightingStatus.UNSATISFACTORY_VIEWS, 6);
    builder.put(SightingStatus.ID_UNCERTAIN, 7);
    builder.put(SightingStatus.SIGNS, 8);
    builder.put(SightingStatus.DEAD, 9);
    builder.put(SightingStatus.NOT_BY_ME, 10);
    builder.put(SightingStatus.DOMESTIC, 11);
    SIGHTING_STATUS_VALUES = builder.build();
  }
  
  private static final class PreferNotIntroducedComparator extends Ordering<Sighting> {
    @Override public int compare(Sighting a, Sighting b) {
      SightingStatus aStatus = a.getSightingInfo().getSightingStatus();
      SightingStatus bStatus = b.getSightingInfo().getSightingStatus();
      
      return SIGHTING_STATUS_VALUES.get(bStatus) - SIGHTING_STATUS_VALUES.get(aStatus);
    }
    
  }
  
  private static final class MoreRecentComparator extends Ordering<Sighting> {
    @Override
    public int compare(Sighting a, Sighting b) {
      ReadablePartial aDate = a.getDateAsPartial();
      ReadablePartial bDate = b.getDateAsPartial();
      if (aDate == null) {
        return bDate == null ? 0 : - 1;
      }
      
      if (bDate == null) {
        return 1;
      }
      
      int compared = comparePartials(aDate, bDate);
      if (compared != 0) {
        return compared;
      }
      
      ReadablePartial aTime = a.getTimeAsPartial();
      ReadablePartial bTime = b.getTimeAsPartial();
      if (aTime == null) {
        return bTime == null ? 0 : - 1;
      }
      
      if (bTime == null) {
        return 1;
      }
      
      return comparePartials(aTime, bTime);
    } 
  }
  
  private static final class LaterInTheYearComparator extends Ordering<Sighting> {
    @Override
    public int compare(Sighting a, Sighting b) {
      ReadablePartial aDate = a.getDateAsPartial();
      ReadablePartial bDate = b.getDateAsPartial();

      if (aDate == null) {
        return bDate == null ? 0 : -1;
      }
      
      if (bDate == null) {
        return 1;
      }
      
      // TODO: might be slow since it'll create new partials *a lot* 
      if (aDate.isSupported(DateTimeFieldType.year())) {
        // Change the year to 1988.  Simply clearing the year doesn't work, as
        // Feb. 29 gets rejected then!  So, use a leap year.
        aDate = new Partial(aDate).with(DateTimeFieldType.year(), 1988);
      }
      if (bDate.isSupported(DateTimeFieldType.year())) {
        bDate = new Partial(bDate).with(DateTimeFieldType.year(), 1988);
      }
      
      int compared = comparePartials(aDate, bDate);
      if (compared != 0) {
        return compared;
      }
      
      ReadablePartial aTime = a.getTimeAsPartial();
      ReadablePartial bTime = b.getTimeAsPartial();
      if (aTime == null) {
        return bTime == null ? 0 : - 1;
      }
      
      if (bTime == null) {
        return 1;
      }
      
      return comparePartials(aTime, bTime);
    } 
  }

  /**
   * Compares two partials that may have different fields to create
   * a total ordering.  The partials are considered from largest
   * to smallest field.  If the partials are equal in all higher
   * fields, then one lacks a field that the other has, the partial
   * with that field is considered larger.
   */
  public static int comparePartials(ReadablePartial a, ReadablePartial b) {
    Chronology chrono = a.getChronology();
    int aSize = a.size();
    int bSize = b.size();
    
    for (int i = 0; i < aSize && i < bSize; i++) {
      DateTimeFieldType aFieldType = a.getFieldType(i);
      DateTimeFieldType bFieldType = b.getFieldType(i);
      if (aFieldType != bFieldType) {
        return aFieldType.getDurationType().getField(chrono).compareTo(
                bFieldType.getDurationType().getField(chrono));
      }
      
      int aValue = a.getValue(i);
      int bValue = b.getValue(i);
      if (aValue == bValue)
        continue;
      
      return aValue - bValue;
    }
    
    return aSize - bSize;
  }

  private static final class HigherCountComparator extends Ordering<Sighting> {
    @Override
    public int compare(Sighting a, Sighting b) {
      int aCount;
      if (a.hasSightingInfo() && a.getSightingInfo().getNumber() != null) {
        aCount = a.getSightingInfo().getNumber().getNumber();
      } else {
        aCount = 0; 
      }
      
      int bCount;
      if (b.hasSightingInfo() && b.getSightingInfo().getNumber() != null) {
        bCount = b.getSightingInfo().getNumber().getNumber();
      } else {
        bCount = 0; 
      }

      return aCount - bCount;
    }
  }
  
  private static class TaxonomicOrdering extends Ordering<Sighting> {
    private final Taxonomy taxonomy;
    private final ResolvedComparator resolvedComparator = new ResolvedComparator();

    TaxonomicOrdering(Taxonomy taxonomy) {
      this.taxonomy = taxonomy;
    }
    
    @Override
    public int compare(Sighting left, Sighting right) {
      Resolved leftResolved = left.getTaxon().resolve(taxonomy);
      Resolved rightResolved = right.getTaxon().resolve(taxonomy);
      // Flip the order - always sort ascending.
      return resolvedComparator.compare(rightResolved, leftResolved);
    }
  }

}
