/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import java.util.Collection;

import org.joda.time.DateTimeFieldType;
import org.joda.time.Partial;
import org.joda.time.ReadablePartial;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.user.User;

/**
 * Predicate instances that apply to sighting instances.
 */
public class SightingPredicates {
  private SightingPredicates() {
  }

  /**
   * Any sightings are OK.
   */
  public static Predicate<Sighting> all() {
    return Predicates.alwaysTrue();
  }

  /**
   * Only heard-only sightings are OK.  Usually not'ed.
   */
  public static Predicate<Sighting> isHeardOnly() {
    return new Predicate<Sighting>() {
      @Override public boolean apply(Sighting input) {
        return input.hasSightingInfo() && input.getSightingInfo().isHeardOnly();
      }

      @Override public String toString() {
        return "IsHeardOnly[]";
      }
    };
  }

  /**
   * Only photographed sightings are OK.
   */
  public static Predicate<Sighting> isPhotographed() {
    return new Predicate<Sighting>() {
      @Override public boolean apply(Sighting input) {
        return input.hasSightingInfo() && input.getSightingInfo().isPhotographed();
      }

      @Override public String toString() {
        return "isPhotographed[]";
      }
    };
  }

  /**
   * Is the sighting in a specific location?
   * @param queryLoc the parent location
   * @param locations all locations
   */
  public static Predicate<Sighting> in(final Location queryLoc,
      final LocationSet locations) {
    Preconditions.checkNotNull(queryLoc);
    return new Predicate<Sighting>() {
      @Override public boolean apply(Sighting sighting) {
        Location loc = locations.getLocation(sighting.getLocationId());
        if (loc == null) {
          return false;
        }
        return loc.isIn(queryLoc);
      }

      @Override public String toString() {
        return "LocationIsIn[" + queryLoc.getModelName() + "]";
      }
    };
  }

  public static Predicate<Sighting> afterOrEquals(final ReadablePartial after) {
    return new Predicate<Sighting>() {
      @Override public boolean apply(Sighting sighting) {
        ReadablePartial date = sighting.getDateAsPartial();
        if (date == null) {
          return false;
        }

        if (!isPartialSubset(after, date)) {
          return false;
        }

        return comparePartialsSubset(after, date) >= 0;
      }

      @Override public String toString() {
        return "AfterOrEquals[" + after + "]";
      }
    };
  }

  public static Predicate<Sighting> after(final ReadablePartial after) {
    return new Predicate<Sighting>() {
      @Override public boolean apply(Sighting sighting) {
        ReadablePartial date = sighting.getDateAsPartial();
        if (date == null) {
          return false;
        }

        if (!isPartialSubset(after, date)) {
          return false;
        }

        return comparePartialsSubset(after, date) > 0;
      }

      @Override public String toString() {
        return "After[" + after + "]";
      }
    };
  }

  public static Predicate<Sighting> dateIsExactly(final ReadablePartial exactly) {
    return new Predicate<Sighting>() {
      @Override public boolean apply(Sighting sighting) {
        ReadablePartial date = sighting.getDateAsPartial();
        if (date == null) {
          return false;
        }

        return date.equals(exactly);
      }

      @Override public String toString() {
        return "DateIsExactly[" + exactly + "]";
      }
    };
  }

  public static Predicate<Sighting> timeIsExactly(final ReadablePartial exactly) {
    return new Predicate<Sighting>() {
      @Override public boolean apply(Sighting sighting) {
        ReadablePartial time = sighting.getTimeAsPartial();
        if (time == null) {
          return false;
        }

        return time.equals(exactly);
      }

      @Override public String toString() {
        return "TimeIsExactly[" + exactly + "]";
      }
    };
  }

  /**
   * This is not the same as {@link Predicates#not} of {@link #afterOrEquals(Partial)},
   * because in for both these methods, a null date is false.
   * @param partial
   * @return
   */
  public static Predicate<Sighting> beforeOrEquals(final ReadablePartial partial) {
    return new Predicate<Sighting>() {
      @Override public boolean apply(Sighting sighting) {
        ReadablePartial date = sighting.getDateAsPartial();
        if (date == null) {
          return false;
        }

        if (!isPartialSubset(partial, date)) {
          return false;
        }

        return comparePartialsSubset(partial, date) <= 0;
      }

      @Override public String toString() {
        return "BeforeOrEquals[" + partial + "]";
      }
    };
  }

  /**
   * This is not the same as {@link Predicates#not} of {@link #afterOrEquals(Partial)},
   * because in for both these methods, a null date is false.
   * @param partial
   * @return
   */
  public static Predicate<Sighting> before(final ReadablePartial partial) {
    return new Predicate<Sighting>() {
      @Override public boolean apply(Sighting sighting) {
        ReadablePartial date = sighting.getDateAsPartial();
        if (date == null) {
          return false;
        }

        if (!isPartialSubset(partial, date)) {
          return false;
        }

        return comparePartialsSubset(partial, date) < 0;
      }

      @Override public String toString() {
        return "Before[" + partial + "]";
      }
    };
  }

  /**
   * Checks that "partial" contains all the fields in "base".
   */
  public static boolean isPartialSubset(ReadablePartial base,
      ReadablePartial partial) {
    for (int i = 0; i < base.size(); i++) {
      DateTimeFieldType type = base.getFieldType(i);
      if (!partial.isSupported(type)) {
        return false;
      }
    }

    return true;
  }

  /**
   * Compares two partials that may have different fields.  Instead
   * of considering all fields, only fields that are in "base" are
   * considered relevant.  "partial" must be a superset of "base".
   * 
   * @return 0 if they match, > 0 if partial is greater than base, < 0
   *     if less than 
   */
  public static int comparePartialsSubset(ReadablePartial base,
      ReadablePartial partial) {
    for (int i = 0; i < base.size(); i++) {
      DateTimeFieldType type = base.getFieldType(i);
      int baseValue = base.getValue(i);
      int partialValue = partial.get(type);
      if (baseValue == partialValue) {
        continue;
      }

      return partialValue - baseValue;
    }

    return 0;
  }

  /**
   * Return a predicate that requires that a sighting's population status be one
   * of a collection.
   */
  public static Predicate<Sighting> sightingStatusIsIn(
      final Collection<SightingStatus> allowedStatuses) {
    return new Predicate<Sighting>() {
      @Override
      public boolean apply(Sighting input) {
        return allowedStatuses.contains(input.getSightingInfo()
            .getSightingStatus());
      }

      @Override public String toString() {
        return "StatusIsIn[" + allowedStatuses + "]";
      }
    };
  }

  public static Predicate<Sighting> is(final Location location,
      LocationSet locationSet) {
    Preconditions.checkNotNull(location);
    return new Predicate<Sighting>() {
      @Override public boolean apply(Sighting input) {
        return location.getId().equals(input.getLocationId());
      }
      
      @Override public String toString() {
        return "LocationIs[" + location.getModelName() + "]";
      }
    };
  }
  
  public static Predicate<Sighting> compatibleWith(Taxonomy compatibleTaxonomy) {
    final Taxonomy baseTaxonomy = TaxonUtils.getBaseTaxonomy(compatibleTaxonomy);
    return new Predicate<Sighting>() {
      @Override public boolean apply(Sighting input) {
        return input.getTaxonomy() == baseTaxonomy;
      }
      
      @Override public String toString() {
        return "CompatibleWith[" + baseTaxonomy.getName() + "]";
      }
    };
  }

  public static Predicate<Sighting> includesUser(User user) {
    return new Predicate<Sighting>() {
      @Override public boolean apply(Sighting input) {
        return input.hasSightingInfo() && input.getSightingInfo().getUsers().contains(user);
      }
      
      @Override public String toString() {
        return "IncludesUser[" + user + "]";
      }
    };
  }

  public static Predicate<Sighting> includesAnyOfUsers(ImmutableSet<User> users) {
    return new Predicate<Sighting>() {
      @Override public boolean apply(Sighting input) {
        return input.hasSightingInfo() &&
            input.getSightingInfo().getUsers().stream().anyMatch(users::contains);
      }
      
      @Override public String toString() {
        return "IncludesAnyOfUsers[" + users + "]";
      }
    };
  }

  public static Predicate<Sighting> usersAreExactly(ImmutableSet<User> userSet) {
    return new Predicate<Sighting>() {
      @Override public boolean apply(Sighting input) {
        ImmutableSet<User> sightingUsers;
        if (input.hasSightingInfo()) {
          sightingUsers = input.getSightingInfo().getUsers();
        } else {
          sightingUsers = ImmutableSet.of();
        }
        return sightingUsers.equals(userSet);
      }
      
      @Override public String toString() {
        return "UsersAreExactly[" + userSet + "]";
      }
    };
  }
}
