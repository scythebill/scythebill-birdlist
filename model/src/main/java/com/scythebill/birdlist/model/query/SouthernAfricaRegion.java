/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import java.util.List;
import java.util.logging.Logger;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Sighting;

/**
 * Synthetic location for the Southern Africa region.
 */
class SouthernAfricaRegion extends SyntheticLocation {
  private static final Logger logger = Logger.getLogger(SouthernAfricaRegion.class.getName());
  /**
   * Predefined IDs for Southern Africa.  Mozambique is roughly defined - it includes
   * all of Tete province, even though part of it is north of the Zambezi.
   */
  private static final ImmutableSet<String> SOUTHERN_AFRICAN_IDS = ImmutableSet.of(
      "ZA", // South Africa
      "ZW", // Zimbabwe
      "BW", // Botswana
      "NA", // Namibia
      "SZ", // Swaziland
      "LS", // Lesotho
      "MZ-L", // Mozambique - Maputo
      "MZ-G",   // Mozambique - Gaza
      "MZ-I",   // Mozambique - Inhambane
      "MZ-B",   // Mozambique - Manica
      "MZ-S",   // Mozambique - Sofala
      "MZ-T"    // Mozambique - Tete
      );
      
  private Predicate<Sighting> predicate;

  static public SouthernAfricaRegion regionIfAvailable(LocationSet locationSet) {
    List<Location> list = Lists.newArrayList();
    for (String code : SOUTHERN_AFRICAN_IDS) {
      Location location = locationSet.getLocationByCode(code);
      if (location == null && !code.contains("-")) {
        logger.warning("Could not find country code " + code);
      } else {
        list.add(location);
      }
    }
    return new SouthernAfricaRegion(
        locationSet,
        list);
  }

  private SouthernAfricaRegion(LocationSet locationSet, Iterable<Location> locations) {
    super("Southern Africa", Name.SOUTHERN_AFRICA, "sounafr");
    List<Predicate<Sighting>> list = Lists.newArrayList();
    for (Location location : locations) {
      if (location == null) {
        continue;
      }
      list.add(SightingPredicates.in(location, locationSet));
    }
    
    predicate = Predicates.or(list);
  }

  @Override
  public Predicate<Sighting> isInPredicate() {
    return predicate;
  }
}
