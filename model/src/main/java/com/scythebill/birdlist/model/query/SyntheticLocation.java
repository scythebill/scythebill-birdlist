/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import java.util.Collection;
import java.util.ResourceBundle;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Sighting;

/**
 * A synthetic location, corresponding to a 
 */
public abstract class SyntheticLocation extends Location {
  private final String id;

  private final static ResourceBundle BUNDLE = ResourceBundle.getBundle("com.scythebill.birdlist.model.sighting.locations");

  private Name bundleName;

  enum Name {
    ABA_REGION,
    ABA_REGION_WITHOUT_HAWAII,
    UNITED_STATES_FIFTY,
    CONTIGUOUS_UNITED_STATES,
    AOU_NORTH,
    AOU_SOUTH,
    WESTERN_PALEARCTIC,
    SOUTHERN_AFRICA,
    UNITED_STATES_AND_DEPENDENT_TERRITORIES,
    AUSTRALIA_WITH_DEPENDENT_TERRITORIES,
    AUSTRALASIA_ABA,
    PACIFIC_OCEAN_ABA,
    ORIENTAL_REGION,
    CHILE_WITH_OFFSHORE_ISLANDS,
    COLOMBIA_WITH_CARIBBEAN_ISLANDS,
    ECUADOR_WITH_GALAPAGOS,
    PORTUGAL_WITH_AZORES_AND_MADEIRA,
    SPAIN_WTH_CANARY_ISLANDS_ETC,
    NEW_ZEALAND_WITH_TOKELAU,
    NORWAY_WITH_BOUVET_AND_SVALBARD,
    UNITED_KINGDOM_WITH_DEPENDENT_TERRITORIES,
    CHINA_WITH_ADMINISTRATIVE_REGIONS,
    KINGDOM_OF_DENMARK,
    SOUTH_AFRICA_WITH_ISLANDS,
    FRANCE_WITH_OVERSEA_TERRITORIES,
    IRELAND_ISLAND_OF,
    HAWAII_WITH_NORTHERWESTERN_ISLANDS,
    KINGDOM_OF_THE_NETHERLANDS;
    
    @Override
    public String toString() {
      return BUNDLE.getString(name());
    }
  }
  
  protected SyntheticLocation(
      String name,
      Name bundleName,
      String id) {
    super(Type.region, name);
    this.bundleName = bundleName;
    this.id = String.format("***%s***", id);
  }

  public abstract Predicate<Sighting> isInPredicate();
  
  @Override
  public String getDisplayName() {
    return bundleName.toString();
  }

  /**
   * Returns, if possible, a set of checklist codes (NOT location codes) whose union
   * would define a checklist for this synthetic location.
   */
  public Collection<String> syntheticChecklistUnion() {
    return null;
  }
  
  @Override
  public Collection<Location> contents() {
    throw new UnsupportedOperationException();
  }

  @Override
  public Location getContent(String name) {
    throw new UnsupportedOperationException();
  }

  @Override
  public String getEbirdCode() {
    return null;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public Location getParent() {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean isIn(Location ancestor) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void reparent(Location newIn) {
    throw new UnsupportedOperationException();
  }
  
  @Override
  public boolean isBuiltInLocation() {
    return true;
  }
  
  @Override
  public Optional<LatLongCoordinates> getLatLong() {
    return Optional.absent();
  }

  @Override
  public String getDescription() {
    return null;
  }
  
  @Override
  public boolean isPrivate() {
    return false;
  }
}
