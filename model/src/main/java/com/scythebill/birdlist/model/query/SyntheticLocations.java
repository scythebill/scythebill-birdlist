/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import java.util.Collection;
import java.util.Map;

import com.google.common.collect.Maps;
import com.scythebill.birdlist.model.query.SyntheticLocation.Name;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;

/** Class for collecting together all synthetic locations. */
public class SyntheticLocations {
  private final Map<String, SyntheticLocation> locations = Maps.newHashMap();
  
  public SyntheticLocations(LocationSet locationSet) {
    addIfAvailable(AbaRegion.regionIfAvailable(locationSet));
    addIfAvailable(AbaRegionWithoutHawaii.regionIfAvailable(locationSet));
    addIfAvailable(UnitedStatesFiftyLocation.regionIfAvailable(locationSet));
    addIfAvailable(ContiguousUnitedStates.regionIfAvailable(locationSet));
    addIfAvailable(AouRegion.regionIfAvailable(locationSet));
    addIfAvailable(AosSouth.regionIfAvailable(locationSet));
    addIfAvailable(WesternPalearcticRegion.regionIfAvailable(locationSet));
    addIfAvailable(SouthernAfricaRegion.regionIfAvailable(locationSet));
    addIfAvailable(UnitedStatesAndDependentTerritoriesLocation.regionIfAvailable(locationSet));
    addIfAvailable(GenericSyntheticLocation.regionIfAvailable(locationSet,
        "Australia (with dependent territories)",
        Name.AUSTRALIA_WITH_DEPENDENT_TERRITORIES,
        "auwidepterr",
        "AU",
        "AC",
        "CS",
        "CC",
        "CX",
        "NF",
        "HM",
        "AU-TAS-Macquarie"));
    addIfAvailable(GenericSyntheticLocation.named(
        locationSet, "Australasia (ABA)", Name.AUSTRALASIA_ABA) 
        .withAbbreviation("austaba")
        .withRequiredLocations("AU", "PG")
        .withOptionalLocations("AC", "CS")
        .withRemovedLocations("PG-NSA")
        .build());
    addIfAvailable(PacificOceanAbaRegion.regionIfAvailable(locationSet));
    addIfAvailable(GenericSyntheticLocation.named(
        locationSet, "Oriental Region", Name.ORIENTAL_REGION) 
        .withAbbreviation("orireg")
        .withLocations(
            "ID-Asia",
            "MV",
            "IN",
            "LA",
            // If just Pakistan east of the Indus, then should only be three provinces;
            // the OBC website is a bit unclear but I was told that it's all of Pakistan,
            "PK",
            "HK",
            "BD",
            "BT",
            "BN",
            "KH",
            "CN",
            "CX",
            "TL",
            "JP",
            "LA",
            "MO",
            "MY",
            "MN",
            "MM",
            "NP",
            "KP",
            "SG",
            "KR",
            "LK",
            "TW",
            "TH",
            "VN",
            "PH")
        .withOptionalLocations(
            // Not exactly right, as the Aru Islands are part of Maluku, not Papua, but are not
            // part of the OBC region it seems
            "ID-MA",
            // Only supposed to be to 90-degrees longitude.
            "RU-KYA",
            "RU-TY",
            "RU-IRK",
            "RU-BU",
            "RU-CHI",
            "RU-AGB",
            "RU-ORB",
            "RU-KAM",
            "RU-AMU",
            "RU-YEV",
            "RU-PRI",
            "RU-SAK",
            "RU-KHA",
            "RU-MAG",
            "RU-SA",
            "RU-CHU")
        // Not exactly?  Really should incorporate Asian Russia to be more accurate
        .withSyntheticChecklistUnionFromAllLocations()
        .build());
    addIfAvailable(GenericSyntheticLocation.named(locationSet,
        "Chile (with offshore islands)", Name.CHILE_WITH_OFFSHORE_ISLANDS)
        .withAbbreviation("clwidepterr")
        .withRequiredLocations("CL")
        .withOptionalLocations(
        "CL-VS-Easter",
        "CL-VS-JuanFernandez")
        .withSyntheticChecklistUnionFromAllLocations()
        .build());
    addIfAvailable(GenericSyntheticLocation.regionIfAvailable(locationSet,
        "Colombia (with Caribbean islands)",
        Name.COLOMBIA_WITH_CARIBBEAN_ISLANDS,
        "cowidepterr",
        "CO",
        "CO-SAP"));
    addIfAvailable(GenericSyntheticLocation.regionIfAvailable(locationSet,
        "Ecuador (with Galapagos)",
        Name.ECUADOR_WITH_GALAPAGOS,
        "ecwidepterr",
        "EC",
        "EC-W"));
    addIfAvailable(GenericSyntheticLocation.regionIfAvailable(locationSet,
        "Portugal (with Azores and Madeira)",
        Name.PORTUGAL_WITH_AZORES_AND_MADEIRA,
        "ptwidepterr",
        "PT",
        "PT-20",
        "PT-30"));
    addIfAvailable(GenericSyntheticLocation.regionIfAvailable(locationSet,
        "Spain (with Canary Islands, Ceuta, and Melilla)",
        Name.SPAIN_WTH_CANARY_ISLANDS_ETC,
        "eswidepterr",
        "ES",
        "ES-CEUMEL",
        "ES-CN"));
    addIfAvailable(GenericSyntheticLocation.regionIfAvailable(locationSet,
        "New Zealand (with Tokelau)",
        Name.NEW_ZEALAND_WITH_TOKELAU,
        "nzwidepterr",
        "NZ",
        "TK"));
    addIfAvailable(GenericSyntheticLocation.regionIfAvailable(locationSet,
        "Norway (with Bouvet Island and Svalbard)",
        Name.NORWAY_WITH_BOUVET_AND_SVALBARD,
        "nowidepterr",
        "NO",
        "BV",
        "SJ"));
    addIfAvailable(GenericSyntheticLocation.regionIfAvailable(locationSet,
        "United Kingdom (with dependent territories)",
        Name.UNITED_KINGDOM_WITH_DEPENDENT_TERRITORIES,
        "gbwidepterr",
        "GB",
        "AI",
        "BM",
        "IO",
        "VG",
        "KY",
        "FK",
        "GI",
        "MS",
        "PN",
        "SH",
        "TC",
        "GS"));
    addIfAvailable(GenericSyntheticLocation.regionIfAvailable(locationSet,
        "China (with administrative regions)",
        Name.CHINA_WITH_ADMINISTRATIVE_REGIONS,
        "cnwidepterr",
        "CN",
        "HK",
        "MO"));
    addIfAvailable(GenericSyntheticLocation.regionIfAvailable(locationSet,
        "Kingdom of Denmark",
        Name.KINGDOM_OF_DENMARK,
        "dkwidepterr",
        "DK",
        "FO",
        "GL"));
    addIfAvailable(GenericSyntheticLocation.regionIfAvailable(locationSet,
        "South Africa (with Prince Edward and Marion Islands)",
        Name.SOUTH_AFRICA_WITH_ISLANDS,
        "zawidepterr",
        "ZA",
        "ZA-WC-EdwardMarion"));
    addIfAvailable(GenericSyntheticLocation.regionIfAvailable(locationSet,
        "France (with Overseas Territories)",
        Name.FRANCE_WITH_OVERSEA_TERRITORIES,
        "frwidepterr",
        "FR",
        "BL",
        "MF",
        "PM",
        "WF",
        "PF",
        "NC",
        "CP",
        "TF",
        "RE",
        "GP",
        "GF",
        "YT",
        "MQ"));
    addIfAvailable(GenericSyntheticLocation.regionIfAvailable(locationSet,
        "Ireland (Island of)",
        Name.IRELAND_ISLAND_OF,
        "ireislof",
        "IE",
        "GB-NIR"));
    addIfAvailable(GenericSyntheticLocation.named(locationSet,
        "Hawaii (with Northwestern Islands)",
        Name.HAWAII_WITH_NORTHERWESTERN_ISLANDS)
        .withAbbreviation("ushiwinowiis")
        .withRequiredLocations("US-HI")
        .withOptionalLocations("UM-71")
        .withSyntheticChecklistUnionFromAllLocations()
        .build());
    addIfAvailable(KingdomOfTheNetherlands.regionIfAvailable(locationSet));
  }

  private void addIfAvailable(SyntheticLocation location) {
    if (location != null) {
      locations.put(location.getId(), location);
    }
  }

  public Iterable<SyntheticLocation> locations() {
    return locations.values();
  }

  public SyntheticLocation byId(String id) {
    return locations.get(id);
  }

  /** Return the location for "UnitedStates-in-NorthAmerica. */
  static Location getNorthAmericanUsa(LocationSet locationSet) {
    Location northAmerica = Locations.getBuiltInLocationByModelName(locationSet, "North America");
    if (northAmerica == null) {
      return null;
    }

    Location westIndies = Locations.getBuiltInLocationByModelName(locationSet, "West Indies");

    Collection<Location> locationsByName = locationSet.getLocationsByModelName("United States");
    for (Location location : locationsByName) {
      // "West Indies" version of the US is a descendant of North America, but not what we're
      // looking for here.
      if (westIndies != null && Locations.isDescendentOfLocation(westIndies, location)) {
        continue;
      }
      
      if (Locations.isDescendentOfLocation(northAmerica, location)) {
        return location;
      }
    }
    
    return null;
  }

}
