/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import java.util.Collection;
import java.util.List;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Sighting;

/**
 * Synthetic location for all US states and territories.
 */
class UnitedStatesAndDependentTerritoriesLocation extends SyntheticLocation {
  private final Predicate<Sighting> predicate;

  static public UnitedStatesAndDependentTerritoriesLocation regionIfAvailable(
      LocationSet locationSet) {
    List<Location> locations = Lists.newArrayList();
    Collection<Location> unitedStatesLocations = locationSet.getLocationsByModelName("United States");
    for (Location unitedStates : unitedStatesLocations) {
      if ("US".equals(unitedStates.getEbirdCode())) {
        locations.add(unitedStates);
      }
    }
    
    Location minorIslands = locationSet.getLocationByCode("UM");
    if (minorIslands != null) {
      locations.add(minorIslands);
    }

    Location puertoRico = locationSet.getLocationByCode("PR");
    if (puertoRico != null) {
      locations.add(puertoRico);
    }
    
    Location guam = locationSet.getLocationByCode("GU");
    if (guam != null) {
      locations.add(guam);
    }
    
    Location americanSamoa = locationSet.getLocationByCode("AS");
    if (americanSamoa != null) {
      locations.add(americanSamoa);
    }
    
    Location usVirginIslands = locationSet.getLocationByCode("VI");
    if (usVirginIslands != null) {
      locations.add(usVirginIslands);
    }

    Location northernMarianaIslands = locationSet.getLocationByCode("MP");
    if (northernMarianaIslands != null) {
      locations.add(northernMarianaIslands);
    }

    return new UnitedStatesAndDependentTerritoriesLocation(
        locationSet,
        locations);
  }

  private UnitedStatesAndDependentTerritoriesLocation(LocationSet locationSet, List<Location> locations) {
    super("United States (with dependent territories)",
        Name.UNITED_STATES_AND_DEPENDENT_TERRITORIES, "uswidepterr");
    List<Predicate<Sighting>> list = Lists.newArrayList();
    for (Location location : locations) {
      list.add(SightingPredicates.in(location, locationSet));
    }
    
    predicate = Predicates.or(list);
  }

  private static final ImmutableSet<String> SYNTHETIC_CHECKLIST_UNION =
      ImmutableSet.of("US", "US-HI", "UM-71", "PR", "GU", "AS", "VI", "MP");
  
  @Override
  public Collection<String> syntheticChecklistUnion() {
    return SYNTHETIC_CHECKLIST_UNION;
  }

  @Override
  public Predicate<Sighting> isInPredicate() {
    return predicate;
  }
}
