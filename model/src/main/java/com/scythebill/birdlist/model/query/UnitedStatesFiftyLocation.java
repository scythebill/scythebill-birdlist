/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Sighting;

/**
 * Synthetic location for all 50 US states.
 */
class UnitedStatesFiftyLocation extends SyntheticLocation {
  private static final Logger logger = Logger.getLogger(UnitedStatesFiftyLocation.class.getName());
  private Predicate<Sighting> predicate;

  static public UnitedStatesFiftyLocation regionIfAvailable(LocationSet locationSet) {
    Location northAmericanUsa = SyntheticLocations.getNorthAmericanUsa(locationSet);
    if (northAmericanUsa == null) {
      logger.warning("No United States in North America - no US 50 list available");
      return null;
    }
    
    Location hawaii = locationSet.getLocationByCode("US-HI");
    if (hawaii == null) {
      logger.warning("No Hawaii states - no US 50 list available");
      return null;
    }
    
    return new UnitedStatesFiftyLocation(
        locationSet,
        northAmericanUsa,
        hawaii);
  }

  private UnitedStatesFiftyLocation(LocationSet locationSet, Location... locations) {
    super("United States - 50 states", Name.UNITED_STATES_FIFTY, "us50");
    List<Predicate<Sighting>> list = Lists.newArrayList();
    for (Location location : locations) {
      list.add(SightingPredicates.in(location, locationSet));
    }
    
    predicate = Predicates.or(list);
  }

  @Override
  public Collection<String> syntheticChecklistUnion() {
    return ImmutableSet.of("US", "US-HI", "UM-71");
  }
  
  @Override
  public Predicate<Sighting> isInPredicate() {
    return predicate;
  }
}
