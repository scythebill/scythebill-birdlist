/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Sighting;

/**
 * Synthetic location for the Western Palearctic region.  See
 * <a href="http://www.aba.org/bigday/listingareas.pdf>this file</a>
 * for the definition of this region.
 */
class WesternPalearcticRegion extends SyntheticLocation {
  private static final Logger logger = Logger.getLogger(WesternPalearcticRegion.class.getName());
  private static final ImmutableSet<String> WESTERN_PALEARCTIC_BUT_NOT_EUROPE = ImmutableSet.of(
      "TR", // Turkey
      "AZ", // Azerbaijan
      "AM", // Armenia
      "GE", // Georgia
      "CY", // Cyprus
      "IL", // Israel
      "PS", // Palestinian Territory
      "JO", // Jordan
      "LB", // Lebanon
      "KW", // Kuwait
      "SY", // Syria
      "IQ", // Iraq
      "MA", // Morocco
      "EH", // Western Sahara
      "DZ", // Algeria
      "TN", // Tunisia
      "LY", // Libya
      "EG", // Egypt
      "ES-CN", // Canary Islands
      "CV",  // Cape Verde
      "SJ", // Svalbard
      "FO",  // Faroe Islands 
      "PT-20", // Azores
      "PT-30",  // Madeira
      "ES-CEUMEL" // Ceuta and Melilla
      );
      
  private Predicate<Sighting> predicate;
  private ImmutableSet<String> syntheticUnionCodes;

  static public WesternPalearcticRegion regionIfAvailable(LocationSet locationSet) {
    Location europe = getBuiltInLocation(locationSet, "Europe");
    if (europe == null) {
      logger.warning("No Western Palearctic list available: No Europe!");
      return null;
    }

    Set<String> syntheticUnionCodes = new LinkedHashSet<>();
    syntheticUnionCodes.addAll(Checklists.gatherCountryChecklistCodes(europe));
    syntheticUnionCodes.addAll(WESTERN_PALEARCTIC_BUT_NOT_EUROPE);
    
    // All of europe...    
    List<Location> list = Lists.newArrayList(europe);
    for (String code : WESTERN_PALEARCTIC_BUT_NOT_EUROPE) {
      Location location = locationSet.getLocationByCode(code);
      if (location == null) {
        logger.warning("Could not find country code " + code);
      } else {
        list.add(location);
      }      
    }
    return new WesternPalearcticRegion(
        locationSet,
        list,
        ImmutableSet.copyOf(syntheticUnionCodes));
  }

  private WesternPalearcticRegion(LocationSet locationSet, Iterable<Location> locations,
      ImmutableSet<String> syntheticUnionCodes) {
    super("Western Palearctic", Name.WESTERN_PALEARCTIC, "wepale");
    List<Predicate<Sighting>> list = Lists.newArrayList();
    for (Location location : locations) {
      if (location == null) {
        continue;
      }
      list.add(SightingPredicates.in(location, locationSet));
    }
    
    predicate = Predicates.or(list);
    this.syntheticUnionCodes = syntheticUnionCodes;
  }

  @Override
  public Predicate<Sighting> isInPredicate() {
    return predicate;
  }

  @Override
  public Collection<String> syntheticChecklistUnion() {
    return syntheticUnionCodes;
  }

  /** Return a built-in location. */
  private static Location getBuiltInLocation(LocationSet locationSet, String name) {
    Collection<Location> locationsByName = locationSet.getLocationsByModelName(name);
    for (Location location : locationsByName) {
      if (location.isBuiltInLocation()) {
        return location;
      }
    }
    
    return null;
  }
}
