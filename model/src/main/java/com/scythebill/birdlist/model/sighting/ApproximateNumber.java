/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import java.text.NumberFormat;

import com.google.common.base.CharMatcher;
import com.google.common.base.Objects;
import com.google.common.primitives.Ints;

/**
 * Storage for an approximate number (a number with an optional additional character).
 */
public final class ApproximateNumber {  
  private final int number;
  private final char approximateChar;

  private ApproximateNumber(int number, char approximateChar) {
    this.number = number;
    this.approximateChar = approximateChar;
  }
  
  public static ApproximateNumber tryParse(String string) {
    string = CharMatcher.whitespace().trimFrom(string);
    if (string.isEmpty()) {
      return null;
    }
    
    if (CharMatcher.inRange('0', '9').matches(string.charAt(0))) {
      Integer integer = Ints.tryParse(string);
      if (integer == null) {
        return null;
      }
      if (integer.intValue() <= 0) {
        return null;
        
      }
      return new ApproximateNumber(integer, (char) 0);
    }
    
    Integer integer = Ints.tryParse(string.substring(1));
    if (integer == null) {
      return null;
    }
    if (integer.intValue() <= 0) {
      return null;
      
    }
    
    // Don't allow negative numbers
    if (string.charAt(0) == '-') {
      return null;
    }
    return new ApproximateNumber(integer, string.charAt(0));
  }
  
  public static ApproximateNumber exact(int value) {
    return new ApproximateNumber(value, (char) 0);
  }
  
  public int getNumber() {
    return number;
  }
  
  public boolean isExact() {
    return approximateChar == 0;
  }
  
  @Override
  public String toString() {
    if (approximateChar == 0) {
      return "" + number;
    } else {
      return "" + approximateChar + number;
    }
  }

  public String toFormattedString() {
    if (approximateChar == 0) {
      return NumberFormat.getIntegerInstance().format(number);
    } else {
      return approximateChar + NumberFormat.getIntegerInstance().format(number);
    }
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(approximateChar, number);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof ApproximateNumber)) {
      return false;
    }
    ApproximateNumber that = (ApproximateNumber) obj;
    return approximateChar == that.approximateChar
        && number == that.number;
  }

  public ApproximateNumber plus(ApproximateNumber that) {
    if (isExact()) {
      if (that.isExact()) {
        return ApproximateNumber.exact(getNumber() + that.getNumber());
      } else {
        return new ApproximateNumber(getNumber() + that.getNumber(), that.approximateChar);
      }
    } else {
      // Not really legit (they might *both* be approximate, in which case adding them up
      // is super-dubious)
      return new ApproximateNumber(getNumber() + that.getNumber(), approximateChar);
    }
  }
}
