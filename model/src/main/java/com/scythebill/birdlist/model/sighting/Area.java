/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * Storage for an Area;  can convert between hectares and acres.
 */
public final class Area {
  private static final float HECTARES_IN_AN_ACRE = 0.404686f;
  private static final float ACRES_IN_A_HECTARE = 2.47105f;
  private final float areaInHectares;
  
  private Area(float areaInHectares) {
    this.areaInHectares = areaInHectares;
  }
  
  public static final Area inAcres(float areaInAcres) {
    return new Area(areaInAcres * HECTARES_IN_AN_ACRE);
  }
  
  public static final Area inHectares(float areaInHectares) {
    return new Area(areaInHectares);
  }
  
  public float acres() {
    return roundAtThreeDigits(areaInHectares * ACRES_IN_A_HECTARE);
  }

  public float hectares() {
    return roundAtThreeDigits(areaInHectares);
  }

  private static float roundAtThreeDigits(float f) {
    return Math.round(f * 1000f) / 1000f;
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .addValue(hectares())
        .toString();
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(hectares());
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof Area)) {
      return false;
    }
    Area that = (Area) obj;
    return hectares() == that.hectares();
  }
}
