/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

/**
 * Computes the closest locations from a given center point.
 */
// TODO: reimplement with a non-brute-force solution, in particular after optimizing
// LatLongCoordinates.  It may be sufficient to simply prune the search space (ignore
// anything > 1 degree of lat. or long. away?)
public class ClosestLocations {
  private final TreeMap<Double, String> locations = Maps.newTreeMap();
  private final LatLongCoordinates center;
  private final static Random random = new SecureRandom();
  
  public ClosestLocations(LatLongCoordinates center) {
    this.center = Preconditions.checkNotNull(center);
  }

  public ClosestLocations(LatLongCoordinates center, LocationSet locations) {
    this(center);
    for (Location root : locations.rootLocations()) {
      addLocation(root);
    }
  }

  /**
   * Finds the nearest location to a given lat-long.
   */
  public static Location nearestLocation(LatLongCoordinates center, LocationSet locations) {
    ClosestLocation bestLocation = new ClosestLocation();
    for (Location root : locations.rootLocations()) {
      bestLocation.testLocation(center, root);
    }
    return bestLocation.minimumLocaiton;
  }
  
  private static class ClosestLocation {
    double minimumDistance = Double.MAX_VALUE;
    Location minimumLocaiton = null;
    void testLocation(LatLongCoordinates center, Location location) {
      if (location.getLatLong().isPresent()) {
        double distance = center.kmDistance(location.getLatLong().get());
        if (distance < minimumDistance) {
          minimumLocaiton = location;
        }
      }
      for (Location child : location.contents()) {
        testLocation(center, child);
      }
    }
  }
  
  /**
   * Return the {@code maxCloseLocations} nearest locations, with none further
   * than {@code maxCloseDistanceKm}.
   */
  public Collection<Object> getNearestLocations(
      int maxCloseLocations,
      double maxCloseDistanceKm) {
    Multimap<Double, String> sortedEntries = Multimaps.newListMultimap(
        new TreeMap<Double, Collection<String>>(), ArrayList::new);
    for (Map.Entry<Double, String> entry : locations.entrySet()) {
      if (entry.getKey() > maxCloseDistanceKm) {
        break;
      }
      // Add locations, sorted by the distance.
      sortedEntries.put(entry.getKey(), entry.getValue());
    }
    
    return sortedEntries.values().stream().limit(maxCloseLocations)
        .collect(ImmutableList.toImmutableList());
  }

  public void addLocation(Location location) {
    if (location.getLatLong().isPresent()) {
      double distance = center.kmDistance(location.getLatLong().get());
      String alreadyPresent = locations.put(distance, location.getId());
      // If there's already a location there...
      if (alreadyPresent != null) {
        distance += random.nextDouble() / 1000.0;
        // ... then reinsert it at an arbitrary distance away.
        locations.put(distance, alreadyPresent);
      }
    }
    
    // Add all children
    for (Location child : location.contents()) {
      addLocation(child);
    }
  }
}
