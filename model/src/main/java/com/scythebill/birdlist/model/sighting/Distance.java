/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * Storage for a distance measurement;  can convert between
 * miles and kilometers.
 */
public final class Distance {
  private static final float KILOMETERS_IN_A_MILE = 1.60934f;
  private static final float MILES_IN_A_KILOMETER = 0.621371f;
  private final float distanceInKm;
  
  private Distance(float distanceInKm) {
    this.distanceInKm = distanceInKm;
  }
  
  public static final Distance inMiles(float distanceInMiles) {
    return new Distance(distanceInMiles * KILOMETERS_IN_A_MILE);
  }
  
  public static final Distance inKilometers(float distanceInKm) {
    return new Distance(distanceInKm);
  }
  
  public float miles() {
    return roundAtThreeDigits(distanceInKm * MILES_IN_A_KILOMETER);
  }

  public float kilometers() {
    return roundAtThreeDigits(distanceInKm);
  }

  private static float roundAtThreeDigits(float f) {
    return Math.round(f * 1000f) / 1000f;
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .addValue(kilometers())
        .toString();
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(kilometers());
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof Distance)) {
      return false;
    }
    Distance that = (Distance) obj;
    return kilometers() == that.kilometers();
  }
}
