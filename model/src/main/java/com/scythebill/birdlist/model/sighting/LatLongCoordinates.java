/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import java.util.Locale;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.primitives.Ints;

/**
 * Encapsulates a latitude and longitude, stored in string form.  
 * <p>
 * Latitudes and longitudes must obey the following international rules:
 * - Incoming strings must be parsable from either period-decimal or comma-decimal formats.
 * - Outgoing strings should be expressed in a localized format
 * - Exports for eBird must be in en-US format (.)
 * - Exports for Scythebill should be in a localized format
 * 
 */
public class LatLongCoordinates {
  private final String latitude;
  private final String longitude;
  private final double latitudeDouble;
  private final double longitudeDouble;
  private final static double RADIUS_KM = 6371;
  
  private LatLongCoordinates(double latitude, double longitude) {
    Preconditions.checkArgument(latitude >= -90 && latitude <= 90, "Latitude not in [-90,90]", latitude);
    Preconditions.checkArgument(longitude >= -180 && longitude <= 180, "Longitude not in [-180,180]", longitude);
    this.latitude = String.format("%3.7f", latitude);
    this.longitude = String.format("%3.7f", longitude);
    // Cache doubles for the above, to make distance calculations faster.
    // Parse the strings *back* to lat/long, for consistency with whatever
    // truncation happened above.
    this.latitudeDouble = flexibleParseDouble(this.latitude);
    this.longitudeDouble = flexibleParseDouble(this.longitude);
  }

  private LatLongCoordinates(String latitude, String longitude) {
    // Parse *back* to doubles, even though we're going to store as strings.
    // This will ensure that later equality checks will be ~accurate, as we won't
    // have consistent string formatting
    this(flexibleParseDouble(latitude), flexibleParseDouble(longitude));
  }

  /**
   * Creates a latitude and longitude.
   * @throws IllegalArgumentException if either field is invalid
   */
  public static LatLongCoordinates withLatAndLong(double latitude, double longitude) {
    return new LatLongCoordinates(latitude, longitude);
  }

  /**
   * Creates a latitude and longitude.
   * @throws IllegalArgumentException if either field is invalid
   */
  public static LatLongCoordinates withLatAndLong(String latitude, String longitude) {
    return new LatLongCoordinates(latitude, longitude);
  }
  
  public String latitude() {
    return latitude;
  }
  
  public String longitude() {
    return longitude;
  }
  
  public double longitudeAsDouble() {
    return longitudeDouble;
  }
  
  public double latitudeAsDouble() {
    return latitudeDouble;
  }

  /**
   * Return the latitude as a "canonical" string, with a "." as the decimal.
   * This is necessary for most strings used in URLs, or for eBird exports.
   */
  public String latitudeAsCanonicalString() {
    return String.format(Locale.US, "%3.7f", latitudeDouble);
  }

  /**
   * Return the longitude as a "canonical" string, with a "." as the decimal.
   * This is necessary for most strings used in URLs, or for eBird exports.
   */
  public String longitudeAsCanonicalString() {
    return String.format(Locale.US, "%3.7f", longitudeDouble);
  }

  public double kmDistance(LatLongCoordinates that) {
    double thisLatRadians = Math.toRadians(latitudeAsDouble());
    double thatLatRadians = Math.toRadians(that.latitudeAsDouble());
    double latDelta = thisLatRadians - thatLatRadians;
    double longDelta = Math.toRadians(longitudeAsDouble() - that.longitudeAsDouble());
    
    double sineHalfLatDelta = Math.sin(latDelta / 2);
    double sineHalfLongDelta = Math.sin(longDelta / 2);
    double a = (sineHalfLatDelta * sineHalfLatDelta)
        + (Math.cos(thisLatRadians) * Math.cos(thatLatRadians)
           * sineHalfLongDelta * sineHalfLongDelta);
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return c * RADIUS_KM;
  }
  
  @Override
  public int hashCode() {
    return Objects.hashCode(latitude, longitude);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) { 
      return true;
    }
    
    if (!(obj instanceof LatLongCoordinates)) {
      return false;
    }
    
    LatLongCoordinates that = (LatLongCoordinates) obj;
    return latitude.equals(that.latitude) && longitude.equals(that.longitude);
  }

  @Override
  public String toString() {
    return String.format("[%s,%s]", latitude, longitude);
  }
  
  /**
   * Parse doubles, being flexible as to the current coordinate system.
   */
  private final static double flexibleParseDouble(String string) {
    int length = string.length();
    if (length == 0) {
      throw new NumberFormatException("Empty string");
    }
    
    boolean negative;
    int index;
    if (string.charAt(0) == '-') {
      negative = true;
      index = 1;
    } else {
      negative = false;
      index = 0;
    }
    
    double accumulator = 0;
    while (index < length) {
      char next = string.charAt(index);
      if (next == ',' || next == '.') {
        index++;
        double nextDecimal = 0.1;
        while (index < length) {
          next = string.charAt(index);
          if (next < '0' || next > '9') {
            if (next == 'E') {
              Integer exponent = Ints.tryParse(string.substring(index + 1));
              if (exponent == null) {
                throw new NumberFormatException("Not a valid double: " + string);                
              } else {
                accumulator *= Math.pow(10.0, exponent.intValue());
                index = length;
                break;
              }
            }
            throw new NumberFormatException("Not a valid double: " + string);
          } else {
            accumulator += nextDecimal * (next - '0');
          }
          nextDecimal /= 10.0;
          index++;
        }
      } else {
        if (next < '0' || next > '9') {
          throw new NumberFormatException("Not a valid double: " + string);
        } else {
          accumulator = (accumulator * 10.0) + (next - '0');
        }
      }
      index++;
    }
    
    return negative ? -accumulator : accumulator;
  }
}
