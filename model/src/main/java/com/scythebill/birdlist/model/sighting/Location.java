/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import java.util.Collection;

import com.google.common.base.Optional;
import com.google.common.base.Strings;

/**
 * A Location object represents a single location in the world.
 * TODO: add support for generic metadata (for extensibility + preservation
 * of unknown properties)
 */
public abstract class Location {
  private final String name;
  private final Type type;

  public enum Type {
    park("Park"),
    town("Town"),
    city("City"),
    county("County"),
    state("State/Province"),
    country("Country"),
    region("Region");
    
    private final String text;
    Type(String text) {
      this.text = text;
    }
    
    @Override
    public String toString() {
      return text;
    }
  }

  static public Builder builder() {
    return new Builder();
  }
  
  static public class Builder {
    String name;
    String description;
    Type type;
    String ebirdCode;
    Location parent;
    boolean privateLocation;
    String builtInPrefix;
    LatLongCoordinates latLong;

    public Builder setName(String name) {
      if (Strings.isNullOrEmpty(name)) {
        throw new IllegalArgumentException("Empty names are not allowed for locations.");
      }
      this.name = name;
      return this;
    }
    
    public Builder setDescription(String description) {
      this.description = description;
      return this;
    }

    public Builder setType(Type type) {
      this.type = type;
      return this;
    }
    
    public Builder setEbirdCode(String ebirdCode) {
      this.ebirdCode = ebirdCode;
      return this;
    }
    
    public Builder setLatLong(LatLongCoordinates latLong) {
      this.latLong = latLong;
      return this;  
    }
    
    public Builder setParent(Location parent) {
      this.parent = parent;
      return this;
    }

    public Builder setPrivate(boolean privateLocation) {
      this.privateLocation = privateLocation;
      return this;
    }

    public Builder setBuiltInPrefix(String builtInPrefix) {
      this.builtInPrefix = builtInPrefix;
      return this;
    }
    
    public Location build() {
      return new LocationImpl(this);
    }
  }

  protected Location(Type type, String name) {
    this.type = type;
    this.name = name;
  }

  /**
   * Returns the stored name.  Will always be unique within a parent, and does not change
   * by locale.
   * 
   * <p>For non-built-in locations, this will always be identical to {@link #getDisplayName()}).
   */
  public final String getModelName() {
    return name;
  }

  public final Type getType() {
    return type;
  }

  public Builder asBuilder() {
    Builder builder = new Builder()
        .setName(name)
        .setParent(getParent())
        .setEbirdCode(getEbirdCode())
        .setLatLong(getLatLong().orNull())
        .setType(getType())
        .setPrivate(isPrivate())
        .setDescription(getDescription());
    // TODO: built-in-prefix?  Don't *think* so?
    return builder;
  }
  
  /**
   * Returns a name suitable for display;  these may be locale-specific.
   * For non-built-in locations, this will always be identical to {@link #getModelName()}).
   * 
   * <p>There is currently no real requirement that display names be unique, which can
   * potentially cause some real weirdness, and cannot be globally prevented (for 
   * example, a locationset that has no duplicates in one language might easily have
   * duplicates in another).  This will need further analysis.
   */
  public String getDisplayName() {
    return getModelName();
  }
  
  public abstract void reparent(Location newIn);

  public abstract Collection<Location> contents();
  
  public abstract Location getContent(String name);

  public abstract boolean isIn(Location ancestor);

  public abstract String getEbirdCode();

  public abstract String getId();

  public abstract Location getParent();

  public abstract boolean isBuiltInLocation();
  
  public abstract Optional<LatLongCoordinates> getLatLong();
  
  public abstract String getDescription();
  
  public abstract boolean isPrivate();
}
