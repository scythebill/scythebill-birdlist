/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import java.util.Collection;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import com.google.common.base.CharMatcher;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.scythebill.birdlist.model.util.LocalePreferences;

/**
 * The default implementation of a Location.  At this time,
 * only a LocationImpl may be added to a LocationSet.  Other
 * locations are synthetic.
 */
class LocationImpl extends Location {
  private static final int DEFAULT_ID_LENGTH = 6;
  private static final String BUILT_IN_PREFIX = "_";
  private Map<String, Location> contents;
  private Location parent;
  private String id;
  private final String ebirdCode;
  private final String builtInPrefix;
  private final Optional<LatLongCoordinates> latLong;
  private final String description;
  private final boolean privateLocation;

  static class TranslatedLocations {
    final ResourceBundle bundle;
    final ImmutableSet<String> bundleKeys;
    TranslatedLocations(Locale locale) {
      this.bundle =
          ResourceBundle.getBundle("com.scythebill.birdlist.model.sighting.locations", locale);
      this.bundleKeys = ImmutableSet.copyOf(bundle.keySet());
    }
  }
  
  private static LoadingCache<Locale, TranslatedLocations> translatedLocationsCache = 
      CacheBuilder.newBuilder().build(new CacheLoader<Locale, TranslatedLocations>() {
        @Override
        public TranslatedLocations load(Locale locale) {
          return new TranslatedLocations(locale);
        }
      });
  
  private final static ImmutableMap<String, String> REGIONS_TO_KEYS =
      ImmutableMap.<String, String>builder()
      .put("North America", "NORTH_AMERICA")
      .put("Central America", "CENTRAL_AMERICA")
      .put("West Indies", "WEST_INDIES")
      .put("South America", "SOUTH_AMERICA")
      .put("Eurasia", "EURASIA")
      .put("Europe", "EUROPE")
      .put("Asia", "ASIA")
      .put("Africa", "AFRICA")
      .put("Australasia", "AUSTRALASIA")
      .put("Pacific Ocean", "PACIFIC_OCEAN")
      .put("Indian Ocean", "INDIAN_OCEAN")
      .put("Atlantic Ocean", "ATLANTIC_OCEAN")
      .put("Arctic Ocean", "ARCTIC_OCEAN")
      .put("South Polar Region", "SOUTH_POLAR_REGION")
      .build();
  
  LocationImpl(Location.Builder builder) {
    super(builder.type,
        Preconditions.checkNotNull(builder.name));
    this.ebirdCode = builder.ebirdCode;
    this.parent = builder.parent;
    this.builtInPrefix = builder.builtInPrefix != null
        ? BUILT_IN_PREFIX + builder.builtInPrefix + BUILT_IN_PREFIX
        : null;
    this.latLong = Optional.fromNullable(builder.latLong);
    this.description = Strings.emptyToNull(builder.description);
    this.privateLocation = builder.privateLocation;
  }

  @Override
  public void reparent(Location newIn) {
    setParent(Preconditions.checkNotNull(newIn));
  }
  
  void setParent(Location newParent) {
    if (newParent == parent) {
      return;
    }

    // The behavior is only really defined once the Location has been added to its parent,
    // which happens when an ID is assigned
    if (id == null) {
      throw new IllegalStateException("Reparenting a Location without an ID is unnecessary");
    } else if (newParent != null && newParent.getId() == null) {
      throw new IllegalStateException("Adding a ID'd child to an ID-less parent is broken");      
    }

    if (newParent != null && newParent.getContent(getModelName()) != null) {
      throw new IllegalStateException(String.format("Parent %s already has a child named %s",
          newParent.getModelName(), getModelName()));
    }

    if (parent != null) {
      ((LocationImpl) parent).removeChild(this);
    }

    parent = newParent;
    if (newParent != null) {
      ((LocationImpl) newParent).addChild(this);
    }
  }

  @Override
  public Location getContent(String name) {
    if (contents == null) {
      return null;
    }

    return contents.get(name);
  }

  @Override
  public Collection<Location> contents() {
    if (contents == null) {
      return Collections.emptySet();
    }
    
    return Collections.unmodifiableCollection(contents.values());
  }

  /**
   * Returns true if this location is "in" - equal to, or a descendant
   * of, "ancestor"
   */
  @Override
  public boolean isIn(Location ancestor) {
    if (ancestor == null) {
      throw new NullPointerException();
    }

    Location loc = this;
    do {
      if (loc == ancestor) {
        return true;
      }

      loc = loc.getParent();
    } while (loc != null);

    return false;
  }

  @Override
  public String getEbirdCode() {
    return ebirdCode;
  }

  private static final ImmutableSet<String> TRANSLATED_STATE_CODES = ImmutableSet.of(
      "EC-W",
      "PM",
      "CO-SAP");

  @Override
  public String getDisplayName() {
    if (isBuiltInLocation()) {
      String code = Locations.getLocationCode(this);
      // Purely as a hack to catch places where Locations.getLocationCode()
      // fails to handle US states correctly, limit the set of states where
      // translated names will be used.
      if (getType() == Location.Type.state
          && !TRANSLATED_STATE_CODES.contains(code)) {
        return getModelName();
      }
          
      Locale locale = LocalePreferences.instance().getUiLocale();
      TranslatedLocations translatedLocations = translatedLocationsCache.getUnchecked(locale);
      
      if (code != null && translatedLocations.bundleKeys.contains(code)) {
        return translatedLocations.bundle.getString(code);
      }
      if (getType() == Location.Type.region) {
        String key = REGIONS_TO_KEYS.get(getModelName());
        if (key != null) {
          return translatedLocations.bundle.getString(key);
        }
      }
    }
    
    return getModelName();
  }
  
  @Override
  public String getId() {
    return id;
  }

  @Override
  public Location getParent() {
    return parent;
  }

  @Override
  public int hashCode() {
    return (((getModelName() == null) ? 0 : getModelName().hashCode()) * 37)
        + ((parent == null) ? 0 : parent.hashCode());
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    }

    if (!(o instanceof LocationImpl)) {
      return false;
    }

    LocationImpl that = (LocationImpl) o;
    return Objects.equal(that.parent, parent)
        && Objects.equal(that.getModelName(), getModelName())
        && Objects.equal(that.getType(), getType())
        && Objects.equal(that.getDescription(), getDescription())
        && Objects.equal(that.getLatLong(), getLatLong());
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder("Location[name=");
    builder.append(getModelName());
    if (getType() != null) {
      builder.append(",type=").append(getType());
    }
    if (getParent() != null) {
      builder.append(",in=").append(getParent());
    }
    if (getLatLong().isPresent()) {
      builder.append(",latLong=").append(getLatLong().get());
    }
    if (getDescription() != null) {
      builder.append(",description=").append(getDescription());
    }
    builder.append("]");
    return builder.toString();
  }

  void locationAdded(String id) {
    setIdInternal(id);
    if (parent != null) {
      ((LocationImpl) parent).addChild(this);
    }
  }

  /**
   * Mark that this location has been added as a one-to-one replacement for an existing location.
   * 
   * It must:
   * <ul>
   * <li> Take over the ID
   * <li> Take over all the children
   * <li> Move into the existing parent as a replacement.
   * </ul> 
   */
  void locationAddedAsReplacement(LocationImpl oldLocation) {
    setIdInternal(oldLocation.getId());
    // Copy over all the children
    if (oldLocation.contents != null) {
      for (Location child : ImmutableList.copyOf(oldLocation.contents.values())) {
        ((LocationImpl) child).setParent(this); 
      }
    }

    if (oldLocation.getParent() != null) {
      if (oldLocation.getParent() == getParent()) {
        ((LocationImpl) oldLocation.getParent()).replaceChild(oldLocation, this);
      } else {
        ((LocationImpl) oldLocation.getParent()).removeChild((LocationImpl) oldLocation);
        ((LocationImpl) getParent()).addChild(this);
      }
    }
  }

  private void setIdInternal(String id) {
    this.id = id;
  }
  
  /** Reusable splitter that splits words on any Unicode whitespace. */
  private static final Splitter WHITESPACE_SPLITTER =
      Splitter.on(CharMatcher.whitespace()).omitEmptyStrings();

  String getDefaultId() {
    StringBuilder builder = new StringBuilder(DEFAULT_ID_LENGTH);
    // Split into words
    String[] nameParts = Iterables.toArray(
        WHITESPACE_SPLITTER.split(getModelName()), String.class); 
    // Add characters per word
    for (int i = 0; i < nameParts.length; i++) {
      String namePart = nameParts[i];
      int remainingChars = DEFAULT_ID_LENGTH - builder.length();
      int charsPerWord = (int) Math.round(Math.ceil(((float) remainingChars)
          / (nameParts.length - i)));
      addFirstIdChars(builder, namePart, charsPerWord);
    }
    
    // Trim down to four
    if (builder.length() > DEFAULT_ID_LENGTH) {
      builder.setLength(DEFAULT_ID_LENGTH);
    }
    
    return builtInPrefix != null
        ? builtInPrefix + builder.toString() 
        : builder.toString();
  }
  
  private void addFirstIdChars(StringBuilder buffer, String name, int charCount) {
    int len = name.length();
    for (int i = 0, added = 0; i < len; i++) {
      char ch = name.charAt(i);
      if (((ch >= 'A') && (ch <= 'Z')) || ((ch >= 'a') && (ch <= 'z'))) {
        buffer.append(Character.toLowerCase(ch));
        added++;
        if (added >= charCount)
          break;
      }
    }
  }

  void replaceChild(Location oldLocation, Location newLocation) {
    Preconditions.checkState(oldLocation.getParent() == this);
    Preconditions.checkState(newLocation.getParent() == this);
    contents.remove(oldLocation.getModelName());
    contents.put(newLocation.getModelName(), newLocation);
  }

  private void addChild(LocationImpl child) {
    if (contents == null) {
      contents = Maps.newLinkedHashMap();
    }

    String name = child.getModelName();
    if (contents.containsKey(name)) {
      throw new IllegalStateException("Location " + name + " already exists in " + getModelName());
    }
    
    contents.put(name, child);
  }

  private void removeChild(LocationImpl child) {
    String name = child.getModelName();
    if ((contents == null) || (contents.get(name) != child)) {
      throw new IllegalStateException(child.toString() + " was not in " + this);
    }
    
    contents.remove(name);
  }

  @Override
  public boolean isBuiltInLocation() {
    if (id == null) {
      return builtInPrefix != null;
    } else {
      return id.startsWith(BUILT_IN_PREFIX);
    }
  }

  @Override
  public Optional<LatLongCoordinates> getLatLong() {
    return latLong;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public boolean isPrivate() {
    return privateLocation;
  }
}
