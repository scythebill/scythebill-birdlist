/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.scythebill.birdlist.model.util.Dirty;
import com.scythebill.birdlist.model.util.DirtyImpl;

/**
 * Container of Location instances.
 * <p>
 * TODO: this is not thread-safe, and at least the id-to-location lookup
 * probably should be.
 */
public class LocationSet {
  private final Map<String, Location> idToLocationMap = Maps.newHashMap();
  private final Multimap<String, Location> nameToLocationMap = ArrayListMultimap.create();
  private final Map<String, Location> locationCodeToLocationMap = Maps.newHashMap();
  private final Multimap<String, Location> locationCodeToLocationMultimap = ArrayListMultimap.create();
  private final DirtyImpl dirty = new DirtyImpl(false);
  private final static Logger logger = Logger.getLogger(LocationSet.class.getName());
  private final Map<String, Location> restoredLocations = Maps.newHashMap();
  private Supplier<Multimap<String, Location>> displayNameToLocationMapSupplier; 
  
  /**
   * Countries that have location codes appearing twice, as they stretch across continents.
   * Accessing these locations by code is *inconsistent*, but currently irrelevant.
   */
  private final static ImmutableSet<String> REPEATED_LOCATION_CODES = ImmutableSet.of(
      "US",
      "ID",
      "RU",
      "TR",
      "XX",
      "BQ");

  /**
   * Builds a Locations object. Used to add location objects with pre-determined
   * IDs, typically only necessary when parsing a file.
   */
  public static class Builder {
    private final LocationSet locations = new LocationSet();

    public Builder() {
    }

    /**
     * Add a location with a pre-existing ID
     */
    public void addLocation(Location loc, String id) {
      locations.addLocation((LocationImpl) loc, id);
    }

    /**
     * Replace an old location object with a new one.
     * The locations must have the same parent;  the old one must have
     * been added to the location set already, and the new one not have
     * been added.
     */
    public void replace(Location oldLocation, Location newLocation) {
      locations.replace(oldLocation, newLocation);
    }

    /** Builds the Locations instance. */
    public LocationSet build() {
      return locations;
    }
  }

  private LocationSet() {
    resetDisplayNameToLocationMapSupplier(); 
  }

  /** Reset the cached implementation of display name -> locations. */
  private void resetDisplayNameToLocationMapSupplier() {
    displayNameToLocationMapSupplier = Suppliers.memoize(
        () -> {
          Multimap<String, Location> multimap = ArrayListMultimap.create();
          for (Location location : rootLocations()) {
            populateDisplayNameToLocationMap(location, multimap);
          }
          return multimap;
        });
  }
  
  private static void populateDisplayNameToLocationMap(Location location, Multimap<String, Location> multimap) {
    multimap.put(location.getDisplayName(), location);
    for (Location child : location.contents()) {
      populateDisplayNameToLocationMap(child, multimap);
    }
  }
  
  public Dirty getDirty() {
    return dirty;
  }

  public void clearDirty() {
    dirty.setDirty(false);
  }

  public void markDirty() {
    dirty.setDirty(true);
  }

  /** Return locations with a given model name. */
  public Collection<Location> getLocationsByModelName(String modelName) {
    return nameToLocationMap.get(modelName);
  }
  
  /** Return locations with a given display name. */
  public Collection<Location> getLocationsByDisplayName(String displayName) {
    return displayNameToLocationMapSupplier.get().get(displayName);
  }
  
  /**
   * Add a location, assigning it an ID, dirtying the location set, and attaching
   * it to its parent.   (If the parent has not yet been added, it will automatically
   * be added as a result of this call.)
   * 
   * @return the ID assigned the location
   */
  public String addLocation(Location loc) {
    if (loc.getParent() != null && loc.getParent().getId() == null) {
      addLocation(loc.getParent());
    }
    dirty.setDirty(true);

    LocationImpl impl = (LocationImpl) loc;
    String id = createId(impl);
    addLocation(impl, id);
    return id;
  }

  /**
   * Find a location by ID.  Returns null if no location has that ID.
   */
  public Location getLocation(String id) {
    return idToLocationMap.get(id);
  }

  /**
   * Find a location by eBird code.  Three notes:
   * - This will return an arbitrary result for locations where the same code appears multiple times.
   * - This will only return locations that have been stored;  anything not
   *   stored won't be here, and would have to be retrieved by going to
   *   PredefinedLocations. 
   * - You currently must use Locations.getLocationCode() style, not
   *   plain location.getEbirdCode(), because US states are stored wackily.
   */
  public Location getLocationByCode(String code) {
    return locationCodeToLocationMap.get(code);
  }

  /**
   * Finds all locations with an eBird code.
   */
  public Collection<Location> getLocationsByCode(String code) {
    return Collections.unmodifiableCollection(locationCodeToLocationMultimap.get(code));
  }

  /**
   * @return all locations that do not have a parent
   */
  public Collection<Location> rootLocations() {
    Set<Location> rootLocations = new HashSet<Location>();
    // TODO: optimize if necessary
    for (Location location : idToLocationMap.values()) {
      if (location.getParent() == null)
        rootLocations.add(location);
    }

    return Collections.unmodifiableSet(rootLocations);
  }

  /**
   * Add a location with a pre-existing ID. This indirectly adds the location to
   * its parent (if one exists).  New locations should be added with
   * {@link #addLocation(Location)}.
   */
  private String addLocation(LocationImpl loc, String id) {
    if (idToLocationMap.containsKey(id))
      throw new IllegalArgumentException("ID " + id
          + " has already been used for a location.");

    loc.locationAdded(id);
    idToLocationMap.put(id, loc);
    nameToLocationMap.put(loc.getModelName(), loc);
    String locationCode = Locations.getLocationCode(loc);
    if (locationCode != null) {
      Location preexisting = locationCodeToLocationMap.put(locationCode, loc);
      if (preexisting != null && !REPEATED_LOCATION_CODES.contains(locationCode)) {
        logger.warning("Multiple locations with code " + locationCode);
      }
      locationCodeToLocationMultimap.put(locationCode, loc);
    }

    resetDisplayNameToLocationMapSupplier();
    
    return id;
  }

  /**
   * Create an ID from a location. We try to build a four character ID from the
   * words in the name, then add a number suffix if necessary
   */
  private String createId(LocationImpl loc) {
    String baseId = loc.getDefaultId();
    String id = null;

    // And append a number suffix if necessary
    for (int i = 0; true; i++) {
      if (i == 0) {
        id = baseId;
      } else {
        id = baseId + Integer.toString(i);
      }
      
      if (!idToLocationMap.containsKey(id)) {
        break;
      }
    }

    return id;
  }

  /**
   * Replace an old location object with a new one.
   * The locations must have the same parent;  the old one must have
   * been added to the location set already, and the new one not have
   * been added.
   */
  void replace(Location oldLocation, Location newLocation) {
    Preconditions.checkArgument(newLocation.getId() == null, "New location already has an ID");
    Preconditions.checkArgument(oldLocation.getId() != null, "Old location does not have an ID");
    Preconditions.checkArgument(newLocation instanceof LocationImpl, "New location is of wrong type");
    Preconditions.checkArgument(oldLocation instanceof LocationImpl, "Old location is of wrong type");
    
    // Merge in the old location's state
    ((LocationImpl) newLocation).locationAddedAsReplacement(
        (LocationImpl) oldLocation);
    
    // And update the various maps
    Preconditions.checkState(oldLocation.getId().equals(newLocation.getId()));
    idToLocationMap.put(newLocation.getId(), newLocation);
    nameToLocationMap.remove(oldLocation.getModelName(), oldLocation);
    nameToLocationMap.put(newLocation.getModelName(), newLocation);
    
    String oldLocationCode = Locations.getLocationCode(oldLocation);
    if (oldLocationCode != null) {
      locationCodeToLocationMap.remove(oldLocationCode);
      locationCodeToLocationMultimap.remove(oldLocationCode, oldLocation);
    }
    String newLocationCode = Locations.getLocationCode(newLocation);
    if (newLocationCode != null) {
      locationCodeToLocationMap.put(newLocationCode, newLocation);
      locationCodeToLocationMultimap.put(newLocationCode, newLocation);
    }
    
    resetDisplayNameToLocationMapSupplier();
    
    dirty.setDirty(true);
  }

  /**
   * Remove a location.  Do not call directly - use 
   * {@link ReportSet#deleteLocation(Location)}.
   */
  void remove(Location location) {
    Preconditions.checkArgument(getLocation(location.getId()) == location,
        "Location is not in the ReportSet");
    Preconditions.checkArgument(location.getParent() != null,
        "Root locations may not be removed");
    Preconditions.checkArgument(location.contents().isEmpty(),
        "Location with children may not be removed");
    
    idToLocationMap.remove(location.getId());
    nameToLocationMap.remove(location.getModelName(), location);
    String locationCode = Locations.getLocationCode(location);
    if (locationCode != null) {
      locationCodeToLocationMap.remove(locationCode);
      locationCodeToLocationMultimap.remove(locationCode, location);
    }
    resetDisplayNameToLocationMapSupplier();
    
    ((LocationImpl) location).setParent(null);
  }
  
  /**
   * Ensure that a location has been added to this location set.  Useful
   * for locations resulting from PredefinedLocations.
   */
  public void ensureAdded(Location location) {
    // Make sure the parent is added.  And do so *even if this location has an ID*.
    // That may sound weird, but in some esoteric situations - reparenting a location
    // to a not-yet-added PredefinedLocation - the parent might not have an  ID even
    // if this location does!
    if (location.getParent() != null) {
      ensureAdded(location.getParent());
    }
    
    if (location.getId() != null) {
      return;
    }
    
    addLocation(location);
  }
  
  /**
   * Add a "restored" location, which had sightings but no associated locations!
   * These are bad, of course, but not granting users access to their sightings is worse.
   */
  public void addRestoredLocation(String locationId, Location location) {
    Preconditions.checkArgument(location.getId() == null);
    Preconditions.checkArgument(getLocation(locationId) == null);
    Preconditions.checkArgument(location.getParent() == null);
    
    addLocation((LocationImpl) location, locationId);
    dirty.setDirty(true);
    
    restoredLocations.put(locationId, location);
  }
  
  public Map<String, Location> restoredLocations() {
    return Collections.unmodifiableMap(restoredLocations);
  }
}
