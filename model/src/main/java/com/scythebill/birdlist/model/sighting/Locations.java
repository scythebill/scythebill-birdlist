/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;

/**
 * Utilities for Location objects.
 */
public class Locations {
  /** Predefined children that don't follow typical patterns. */
  private static final ImmutableMap<String, String> UNUSUAL_PARENT_CODES = 
      ImmutableMap.of("CW", "BQ", "SX", "BQ");
  private Locations() {}
  
  /**
   * Returns the Location ancestor with a given type, or null.
   * Will return the location itself if it is of the correct type.
   */
  public static Location getAncestorOfType(
      Location location, Location.Type type) {
    while (location.getType() != type) {
      location = location.getParent();
      if (location == null) {
        break;
      }
    }
    
    return location;
  }
  
  /**
   * Return the eBird code, ensuring that for states it is of
   * the form "[COUNTRY]-[STATE]", not just "[STATE]".
   */
  public static String getLocationCode(Location location) {
    String code = location.getEbirdCode();
    if (code == null) {
      return null;
    }
    
    // TODO: Clean up US states data to be "US-XX" out of the gate,
    // then kill this function!
    if (location.getType() == Location.Type.state
        && isUsChild(location.getParent())) {
      code = "US-" + code;
    }
    
    return code;
  }

  private static boolean isUsChild(Location location) {
    while (location != null) {
      if ("US".equals(location.getEbirdCode())) {
        return true;
      }
      
      location = location.getParent();
    }
    return false;
  }
 
  
  private static String getParentCode(String code) {
    // Try to find the parent by stripping off the last "-" section.
    int lastHyphen = code.lastIndexOf('-');
    if (lastHyphen < 0) {
      // See if there's an unexpected parent.
      return UNUSUAL_PARENT_CODES.get(code);
    }
    
    return code.substring(0, lastHyphen);
  }
  
  public static String getGroupedLocationPrefix(String locationName) {
    int locationGroupIndex = locationName.indexOf("--");
    if (locationGroupIndex < 0) {
      return null;
    }
    
    return locationName.substring(0, locationGroupIndex);
  }
  
  public static String getGroupedLocationGroupSuffix(String locationName) {
    int locationGroupIndex = locationName.indexOf("--");
    if (locationGroupIndex < 0) {
      return null;
    }
    
    return locationName.substring(locationGroupIndex + 2);
  }

  /**
   * Get a location by code.  If it's already created and attached, return that.
   * Otherwise, see if it does not yet exist, and if so create that.
   */
  public static Location getLocationByCodePossiblyCreating(
      LocationSet locations,
      PredefinedLocations predefinedLocations,
      String code) {
    // See if the location already exists.
    Location locationByCode = locations.getLocationByCode(code);
    if (locationByCode != null) {
      return locationByCode;
    }
    
    String parentCode = getParentCode(code);
    if (parentCode == null) {
      return null;
    }
    
    Collection<Location> multiplePossibleParents = locations.getLocationsByCode(parentCode);
    if (!multiplePossibleParents.isEmpty()) {
      for (Location parentLocation : multiplePossibleParents) {
        // Found a parent location.  Now see if PredefinedLocations can find a child
        // with that code.
        PredefinedLocation predefinedLocation = predefinedLocations.getPredefinedLocationChildByCode(parentLocation, code);
        if (predefinedLocation != null) {
          return predefinedLocation.create(locations, parentLocation);
        }
      }
    }
    
    // Recursively, find the parent location in the same way.
    Location parentLocation = getLocationByCodePossiblyCreating(
        locations, predefinedLocations, parentCode);
    if (parentLocation == null) {
      return null;
    }
    
    // Found a parent location.  Now see if PredefinedLocations can find a child
    // with that code.
    PredefinedLocation predefinedLocation = predefinedLocations.getPredefinedLocationChildByCode(parentLocation, code);
    if (predefinedLocation == null) {
      return null;
    }
    
    return predefinedLocation.create(locations, parentLocation);
  }
  
  
  /**
   * Returns true if descendent is identical to or a descendent of
   * location.
   */
  public static boolean isDescendentOfLocation(Location location,
      Location descendent) {
    while (descendent != null) {
      if (descendent == location) {
        return true;
      }
      
      descendent = descendent.getParent();
    }
    
    return false;
  }
  
  /**
   * Implements reparent() but optionally replacing any existing location in
   * that parent.
   */
  public static void reparentReplacingIfNeeded(
      Location child,
      Location newParent,
      ReportSet reportSet) {
    Location existingChild = newParent.getContent(child.getModelName());
    if (existingChild != null) {
      replaceLocation(child, existingChild, reportSet);
    }
    
    // Now, replace, which is safe.
    child.reparent(newParent);
  }
  
  /**
   * Takes a new Location - perhaps not yet added - and uses it in place of an 
   * old Location.  All sightings of the old Location will be reassigned,
   * and all children Locations reassigned as well.
   */
  public static void replaceLocation(
      Location newLocation, Location oldLocation, ReportSet reportSet) {
    // Change the name of the old location (so that adding the new location doesn't conflict)
    Location locationWithNewName = oldLocation.asBuilder()
        .setName("~~~~~" + oldLocation.getModelName())
        .setParent(oldLocation.getParent())
        .setType(oldLocation.getType())
        .build();
    reportSet.getLocations().replace(oldLocation, locationWithNewName);
    
    // Add the new location
    reportSet.getLocations().ensureAdded(newLocation);

    // ... and move all of the old location's children to directly be in the new location  
    for (Location child : ImmutableList.copyOf(locationWithNewName.contents())) {
      Location possiblyPreExistingChild = newLocation.getContent(child.getModelName());
      // If *that* child already exists in the new location, then it's necessary to recurse.
      // This is ... not efficient, since every level of recursion is going to do a complete iteration
      // through all sightings once "deleteLocation" is hit.  This is, however, very much a corner
      // case, and iterating through all sightings isn't that slow
      if (possiblyPreExistingChild != null) {
        replaceLocation(possiblyPreExistingChild, child, reportSet);
      } else {
        child.reparent(newLocation);
      }
    }
    // ... and move the old location into the new location
    locationWithNewName.reparent(newLocation);
    // ... and finally delete the old location from the report set
    // (which will update sightings accordingly)
    reportSet.deleteLocation(locationWithNewName);
  }

  /**
   * Returns the common ancestor location of two locations (which may be one
   * or both of those locations).
   */
  public static Location getCommonAncestor(
      Location first,
      Location second) {
    if (sameLocation(first, second)) {
      return first;
    }
    
    List<Location> firstPath = getLocationPath(first);
    List<Location> secondPath = getLocationPath(second);
    
    Location common = null;
    Iterator<Location> firstPathIterator = firstPath.iterator();
    Iterator<Location> secondPathIterator = secondPath.iterator();
    while (firstPathIterator.hasNext() && secondPathIterator.hasNext()) {
      Location firstEntry = firstPathIterator.next();
      Location secondEntry = secondPathIterator.next();
      if (sameLocation(firstEntry, secondEntry)) {
        common = firstEntry;
      } else {
        break;
      }
    }
    
    return common;
  
  }

  private static boolean sameLocation(Location firstEntry, Location secondEntry) {
    // Same location object
    if (firstEntry == secondEntry) {
      return true;
    } else {
      String firstCode = getLocationCode(firstEntry);
      String secondCode = getLocationCode(secondEntry);
      if (firstCode != null && secondCode != null && firstCode.equals(secondCode)) {
        // Different instance but same code - this can happen if multiple PredefinedLocations were
        // triggered independently.
        return true;
      }
    }
    return false;
  }

  private static List<Location> getLocationPath(Location location) {
    LinkedList<Location> list = Lists.newLinkedList();
    while (location != null) {
      list.addFirst(location);
      location = location.getParent();
    }
    return list;
  }

  /**
   * Given a location, see if it (or any of its parents) in fact maps to a different location that has
   * already been added, and return a different corresponding location.
   */
  public static Location normalizeLocation(
      Location location,
      LocationSet locations) {
    // If this location has already been added, there's nothing to normalize
    if (location.getId() != null) {
      return location;
    }
    // Ditto if it's a root location
    Location parent = location.getParent();
    if (parent == null) {
      return location;
    }
   
    // Otherwise, normalize the parent
    Location normalizedParent = normalizeLocation(parent, locations);
    // ... and see if this location exists in that normalized parent
    Location preexisting = normalizedParent.getContent(location.getModelName());
    if (preexisting != null) {
      // It does, so we want that location
      location = preexisting;
    } else if (normalizedParent != parent) {
      // It doesn't;  but the parent needed to be normalized, so switch the parent
      location = location.asBuilder().setParent(normalizedParent).build();
    }
    return location;
  }

  /**
   * Returns the first ancestor which is built-in.
   */
  public static Location getBuiltInAncestor(Location location) {
    while (location != null) {
      if (location.isBuiltInLocation()) {
        return location;
      }
      location = location.getParent();
    }
    
    return null;
  }
  
  /** Visits each location. */
  public static void visitLocations(LocationSet locationSet, Consumer<Location> visitor) {
    for (Location location : locationSet.rootLocations()) {
      visitLocations(location, visitor);
    }
  }
  
  /**
   * Returns a built-in location with a model name.  If there a multiple, returns an arbitrary one.
   */
  public static Location getBuiltInLocationByModelName(LocationSet locationSet, String modelName) {
    for (Location location : locationSet.getLocationsByModelName(modelName)) {
      if (location.isBuiltInLocation()) {
        return location;
      }
    }
    
    return null;
  }

  private static void visitLocations(Location location, Consumer<Location> visitor) {
    visitor.accept(location);
    for (Location child : location.contents()) {
      visitLocations(child, visitor);
    }
  }
}
