/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import java.io.IOException;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Ordering;
import com.google.common.io.Resources;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.Location.Type;

public class PredefinedLocations {
  private static final String PREDEFINED_LOCATION_PREFIX = "scyco";
  
  private final ImmutableMultimap<String, PredefinedLocationImpl> locations;
  private final ImmutableMultimap<String, UsCounty> usCounties;

  /** Publicly exposed API for a single predefined location. */
  public interface PredefinedLocation {
    Type getType();

    String getName();
    
    String getCode();

    Location create(LocationSet locations, Location parent);
  }
  
  static class UsCounty implements PredefinedLocation {
    private final String name;
    private final boolean isCity;
    private final String code;

    UsCounty(String name, boolean isCity, String code) {
      this.name = name;
      this.isCity = isCity;
      this.code = code;
    }
    
    @Override
    public Type getType() {
      return isCity ? Type.city : Type.county;
    }

    @Override
    public String getName() {
      return name;
    }

    @Override
    public String getCode() {
      return code;
    }
    
    @Override
    public Location create(LocationSet locations, Location parent) {
      // First, see if this location happens to already exist (by code).
      // It's possible that it's been reparented.
      Location preexisting = locations.getLocationByCode(code);
      if (preexisting != null) {
        return preexisting;
      }
      
      Location.Builder newLocation = Location.builder()
          .setBuiltInPrefix(PREDEFINED_LOCATION_PREFIX)
          .setName(name)
          .setType(getType())
          .setParent(parent)
          .setEbirdCode(code);
      return newLocation.build();
    }

    @Override
    public String toString() {
      if (isCity) {
        return getName() + " (City)";
      }
      return getName();
    }
  }
  
  static class PredefinedLocationImpl implements PredefinedLocation {
    private final String name;
    private final String id;
    private final Type type;
    private final String requiredGrandParent;

    PredefinedLocationImpl(
        String name, String id, Location.Type type, String requiredGrandParent) {
      Preconditions.checkArgument(!name.isEmpty());
      // This can be null, but it can't be an empty string
      Preconditions.checkArgument(!"".equals(requiredGrandParent));
      this.name = name;
      this.id = Preconditions.checkNotNull(Strings.emptyToNull(id));
      this.type = type;
      this.requiredGrandParent = requiredGrandParent;
    }

    @Override
    public String getName() {
      return name;
    }

    @Override
    public String getCode() {
      return id;
    }

    @Override
    public Type getType() {
      return type;
    }

    public String getRequiredGrandParent() {
      return requiredGrandParent;
    }
    
    @Override
    public String toString() {
      return getName();
    }

    @Override
    public Location create(LocationSet locations, Location parent) {
      // First, see if this location happens to already exist (by code).
      // It's possible that it's been reparented.
      if (id != null) {
        Location preexisting = locations.getLocationByCode(id);
        if (preexisting != null) {
          return preexisting;
        }
      }
      
      Location.Builder newLocation = Location.builder()
          .setBuiltInPrefix(PREDEFINED_LOCATION_PREFIX)
          .setName(name)
          .setType(type)
          .setParent(parent);
      if (id != null) {
        newLocation.setEbirdCode(id);
      }
      return newLocation.build();
    }
  }
  
  static private final Ordering<PredefinedLocation> PREDEFINED_ORDERING = new Ordering<PredefinedLocation>() {
    @Override
    public int compare(PredefinedLocation first, PredefinedLocation second) {
      return first.toString().compareToIgnoreCase(second.toString());
    }
  };
  
  /** Return a list of all the built-in locations that could potentially be in a location. */
  public ImmutableCollection<PredefinedLocation> getPredefinedLocations(Location location) {
    if (location.getEbirdCode() == null) {
      // Predefined locations only exist for locations with ebird codes. 
      return ImmutableList.of();
    }
    
    // Mostly, only countries contain predefined locations
    if (location.getType() != Location.Type.country) {
      // But states that are built-in and are inside built-in locations might.
      // Northern Ireland is one such location;  
      if (location.getType() == Location.Type.state) {
        if (!location.isBuiltInLocation() || !location.getParent().isBuiltInLocation()) {
          return ImmutableList.of();
        }
        
        // And don't get confused by "Belize" state in "Belize" country...
        if (location.getModelName().equals(location.getParent().getModelName())) {
          return ImmutableList.of();
        }
      } else {
        return ImmutableList.of();
      }
    }
    
    // Handle US states specifically
    if (location.isBuiltInLocation()
        && "United States".equals(location.getParent().getModelName())
        && location.getType() == Type.state) {
      ImmutableCollection<UsCounty> collection = usCounties.get(location.getEbirdCode());
      return ImmutableSortedSet.<PredefinedLocation>copyOf(PREDEFINED_ORDERING, collection);
    }
    
    ImmutableCollection<PredefinedLocationImpl> collection =
        ImmutableSortedSet.copyOf(PREDEFINED_ORDERING, getNamesAndIdsOfStates(location.getModelName()));
    // Ignore zero or 1 entry items 
    if (collection.size() < 2) {
      return ImmutableList.of();
    }
    
    ImmutableList.Builder<PredefinedLocation> locations = ImmutableList.builder();
    for (PredefinedLocationImpl predefined : collection) {
      // If this predefined location has a required grandparent, then look for it - but not just
      // as a grandparent, but as any ancestor.  If the user has left the location hierarchy alone,
      // grandparents work fine.  But if they've messed with hierarchy - e.g., moving Indonesia (Asia)
      // into an "East Asia" region - then that breaks.  So search further up the hierarchy.
      // This won't catch users doing *really* weird things, but so be it.
      if (predefined.getRequiredGrandParent() == null
          || hasPredefinedAncestorWithName(location.getParent(), predefined.getRequiredGrandParent())) {
        locations.add(predefined);
      }
    }
    
    return locations.build();
  }
  
  /** See if the predefined ancestor with that name exists. */
  private boolean hasPredefinedAncestorWithName(Location location, String name) {
    while (location != null) {
      if (location.isBuiltInLocation()) {
        // Abort as soon as a predefined location is found;  basically, skip over any
        // custom locations that are inserted, but don't skip over predefined locations.
        return name.equals(location.getModelName());
      }
      location = location.getParent();
    }
    
    return false;
  }
  
  /**
   * Returns a location if the given country has a predefined child matching the given name.
   * 
   * @deprecated {@link #getPredefinedLocationChildThatHasNotBeenCreatedYet} is probably more appropriate
   * for essentially all cases - it handles the possibility that the predefined child has already been created,
   * and has been moved away to another parent.
   */
  @Deprecated
  public PredefinedLocation getPredefinedLocationChild(Location country, String childName) {
    ImmutableCollection<PredefinedLocation> predefinedLocations = getPredefinedLocations(country);
    for (PredefinedLocation location : predefinedLocations) {
      if (namesAreEqualIgnoringCaseAndAccents(childName, location.getName())) {
        return location;
      }
    }
    
    return null;
  }

  /**
   * Returns a location if the given country has a predefined child matching the given name,
   * but only if that location has not been created yet.
   */
  public PredefinedLocation getPredefinedLocationChildThatHasNotBeenCreatedYet(LocationSet locationSet, Location country, String childName) {
    ImmutableCollection<PredefinedLocation> predefinedLocations = getPredefinedLocations(country);
    for (PredefinedLocation location : predefinedLocations) {
      if (namesAreEqualIgnoringCaseAndAccents(childName, location.getName())) {
        // If a location with this code already exists anywhere else, then it's already
        // been created.
        if (locationSet.getLocationByCode(location.getCode()) != null) {
          return null;
        }
        
        return location;
      }
    }
    
    return null;
  }

  /** Returns a location if the given country has a predefined child, grandchild, etc. matching the given name. */
  public Location createPredefinedLocationChildRecursively(LocationSet locations, Location country, String childName) {
    ImmutableCollection<PredefinedLocation> predefinedLocations = getPredefinedLocations(country);
    for (PredefinedLocation location : predefinedLocations) {
      if (namesAreEqualIgnoringCaseAndAccents(childName, location.getName())) {
        return location.create(locations, country);
      }
    }
    
    for (PredefinedLocation location : predefinedLocations) {
      Location found = createPredefinedLocationChildRecursively(
          locations,
          location.create(locations, country),
          childName);
      if (found != null) {
        return found;
      }
    }

    return null;
  }

  /** Returns true if the given country has a predefined child matching the given code. */
  public PredefinedLocation getPredefinedLocationChildByCode(Location country, String code) {
    ImmutableCollection<PredefinedLocation> predefinedLocations = getPredefinedLocations(country);
    for (PredefinedLocation location : predefinedLocations) {
      if (code.equals(location.getCode())) {
        return location;
      }
    }
    
    return null;
  }

  private boolean namesAreEqualIgnoringCaseAndAccents(String first, String second) {
    return StringUtils.stripAccents(first).equalsIgnoreCase(StringUtils.stripAccents(second));
  }

  ImmutableCollection<PredefinedLocationImpl> getNamesAndIdsOfStates(String country) {
    return locations.get(country);
  }
  
  ImmutableSet<String> countries() {
    return locations.keySet();
  }
  
  private PredefinedLocations(
      ImmutableMultimap<String, PredefinedLocationImpl> locations,
      ImmutableMultimap<String, UsCounty> usCounties) {
    this.locations = locations;
    this.usCounties = usCounties;
  }

  static private final ImmutableMap<String, Location.Type> TYPE_NAMES = ImmutableMap.of(
      "State", Location.Type.state,
      "County", Location.Type.county,
      "City", Location.Type.city,
      "Country in Country", Location.Type.country);
  
  static public PredefinedLocations loadAndParse() {
    try {
      ImmutableMultimap.Builder<String, PredefinedLocationImpl> builder = ImmutableMultimap.builder(); 
      URL url = Resources.getResource(PredefinedLocations.class, "all-states.csv");
      ImportLines importLines = CsvImportLines.fromUrl(url, Charsets.UTF_8);
      
      try {
        // Skip the header
        importLines.nextLine();
        while (true) {
          String[] nextLine = importLines.nextLine();
          if (nextLine == null) {
            break;
          }
          
          String type = nextLine[3];
          if ("Country".equals(type)) {
            continue;
          }
          
          Type locationType = TYPE_NAMES.get(type);
          Preconditions.checkState(locationType != null, "Unexpected type %s", type);
          String requiredParent = null;
          if (nextLine.length > 4) {
            requiredParent = nextLine[4];
          }
          builder.put(nextLine[2],
              new PredefinedLocationImpl(nextLine[1], nextLine[0], locationType, requiredParent));
        }        
      } finally {
        importLines.close();
      }

      // Now import the US counties
      ImmutableMultimap.Builder<String, UsCounty> usCounties = ImmutableMultimap.builder(); 
      URL countyUrl = Resources.getResource(PredefinedLocations.class, "us-counties.csv");
      ImportLines countyImport = CsvImportLines.fromUrl(countyUrl, Charsets.UTF_8);
      try {
        // Skip the header
        countyImport.nextLine();
        while (true) {
          String[] nextLine = countyImport.nextLine();
          if (nextLine == null) {
            break;
          }
          
          boolean isCity = nextLine.length >= 4 && "City".equals(nextLine[3]);
          String code = String.format("US-%s-%s", nextLine[1], nextLine[2]);
          usCounties.put(nextLine[1], new UsCounty(nextLine[0], isCity, code));
        }        
      } finally {
        countyImport.close();
      }

      return new PredefinedLocations(builder.build(), usCounties.build());
    } catch (IOException e) {
      throw new RuntimeException("Unexpected IOException", e);
    }
  }

  public void mergeInto(LocationSet locations, Location location) {
    ImmutableCollection<PredefinedLocationImpl> namesAndIdsOfStates =
        getNamesAndIdsOfStates(location.getModelName());
    for (PredefinedLocationImpl nameAndId : namesAndIdsOfStates) {
      // TODO: be more careful with searches here (especially around punctuation and diacritics?)
      Location existingChild = location.getContent(nameAndId.getName());
      if (existingChild != null) {
        continue;
      }
      
      Location.Builder builder = Location.builder()
          .setName(nameAndId.getName())
          .setType(nameAndId.getType())
          .setParent(location)
          .setBuiltInPrefix(PREDEFINED_LOCATION_PREFIX)
          .setEbirdCode(nameAndId.getCode());
     
      locations.addLocation(builder.build());      
    }
  }
}
