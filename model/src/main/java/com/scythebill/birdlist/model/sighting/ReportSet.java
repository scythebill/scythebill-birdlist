/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.annotation.Nullable;

import com.google.common.base.Preconditions;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.ExtendedTaxonomyChecklists;
import com.scythebill.birdlist.model.query.SightingPredicates;
import com.scythebill.birdlist.model.taxa.CompletedUpgrade;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.user.User;
import com.scythebill.birdlist.model.user.UserSet;
import com.scythebill.birdlist.model.util.AndDirty;
import com.scythebill.birdlist.model.util.Dirty;
import com.scythebill.birdlist.model.util.DirtyImpl;

/**
 * Combination of a Locations and a list of Sighting objects.
 * <p>
 * TODO: make thread-safe.
 */
public class ReportSet {
  /** Built-in location codes which are delete-able */
  private static final ImmutableSet<String> DELETEABLE_BUILT_IN_LOCATIONS =
      ImmutableSet.of(
          "CU-02", // "La Habana", split in two;  and then the name was reassigned to the City of Havana!
          "FI-AL",
          "FI-ES", // Etelä-Suomen lääni - split many ways 
          "FI-IS", // Itä-Suomen lääni - split many ways 
          "FI-LL",
          "FI-LS", // Länsi-Suomen lääni - split many ways
          "FI-OL", // Oulun lääni - split in two
          "LV-AI",
          "LV-AL",
          "LV-BL",
          "LV-BU",
          "LV-CE",
          "LV-DA",
          "LV-DGV",
          "LV-DO",
          "LV-GU",
          "LV-JEL",
          "LV-JK",
          "LV-JL",
          "LV-JUR",
          "LV-KR",
          "LV-KU",
          "LV-LE",
          "LV-LM",
          "LV-LPX",
          "LV-LU",
          "LV-MA",
          "LV-OG",
          "LV-PR",
          "LV-RE",
          "LV-REZ",
          "LV-RI",
          "LV-RIX",
          "LV-SA",
          "LV-TA",
          "LV-TU",
          "LV-VE",
          "LV-VEN",
          "LV-VK",
          "LV-VM",
          "PA-0",
          "AX",
          "RS-KM",
          "PM", // upgradeable to a state
          "LK-1",
          "LK-2",
          "LK-3",
          "LK-4",
          "LK-5",
          "LK-6",
          "LK-7",
          "LK-8",
          "LK-9",
          "OM-BA",
          "OM-JA",
          "OM-SH",
          "MD-LA",
          "MD-TI"
      );
  private final DirtyImpl dirty = new DirtyImpl(false);
  private final LocationSet locations;
  private final List<Sighting> sightings;
  private final Dirty externalDirty;
  private final Taxonomy taxonomy;
  private final ReentrantReadWriteLock readWriteLock;
  private final Map<Location, Checklist> checklists;
  private final Map<VisitInfoKey, VisitInfo> visitInfos;
  private UserSet userSet;
  private volatile CompletedUpgrade completedUpgrade;
  private ImmutableSet<SightingTaxon> spsToResolve;

  static class TaxonomyWithChecklists {
    final Taxonomy taxonomy;
    final ExtendedTaxonomyChecklists checklists;
    
    TaxonomyWithChecklists(Taxonomy taxonomy, ExtendedTaxonomyChecklists checklists) {
      this.taxonomy = taxonomy;
      this.checklists = checklists;
    }
  }
  
  private final Map<String, TaxonomyWithChecklists> extendedTaxonomies;
  private String loadedVersion;
  private String preferencesJson;
  private Taxonomy resolveTaxonomy;
  private ImmutableSet<String> oneTimeUpgrades = ImmutableSet.of();

  // TODO: don't take a generic "List" - it could be immutable, etc.
  // Probably need a much different data structure
  public ReportSet(
      LocationSet locations, List<Sighting> sightings, Taxonomy taxonomy) {
    this.locations = locations;
    this.sightings = sightings;
    this.taxonomy = Preconditions.checkNotNull(taxonomy);
    this.checklists = Maps.newLinkedHashMap();
    this.visitInfos = Maps.newLinkedHashMap();
    this.extendedTaxonomies = Maps.newLinkedHashMap();
    
    externalDirty = new AndDirty(dirty, locations.getDirty());
    readWriteLock = new ReentrantReadWriteLock();
  }

  /**
   * Lock for reading/writing the sightings.
   * TODO: use consistently.
   */
  public ReadWriteLock sightingsLock() {
    return readWriteLock;
  }
  
  public Dirty getDirty() {
    return externalDirty;
  }

  public void markDirty() {
    dirty.setDirty(true);
  }

  public void clearDirty() {
    dirty.setDirty(false);
    locations.clearDirty();
    if (userSet != null) {
      userSet.clearDirty();
    }
  }

  public LocationSet getLocations() {
    return locations;
  }

  // FIXME: need more efficient structure for adding/removing sightings?
  public List<Sighting> getSightings() {
    return Collections.unmodifiableList(sightings);
  }

  /**
   * Returns only those sightings compatible with the associated taxonomy.
   */
  public Iterable<Sighting> getSightings(Taxonomy compatibleTaxonomy) {
    return FluentIterable.from(getSightings())
        .filter(SightingPredicates.compatibleWith(compatibleTaxonomy));
  }


  public void setCompletedUpgrade(CompletedUpgrade completedUpgrade) {
    this.completedUpgrade = completedUpgrade;
    if (completedUpgrade != null) {
      markDirty();
    }
  }

  /**
   * Returns the UpgradeTracker used to load this taxonomy at startup.
   * Returns null if no upgrade was used.
   */
  public CompletedUpgrade getCompletedUpgrade() {
    return completedUpgrade;
  }
  
  public ReportSetMutator mutator() {
    return new ReportSetMutator(this);
  }
  
  /**
   * Adds a set of sightings to the ReportSet.
   */
  void addSightings(Collection<Sighting> newSightings) {
    // Validate all sightings before they're added.
    for (Sighting sighting : newSightings) {
      for (String id : sighting.getTaxon().getIds()) {
        Taxon taxon = sighting.getTaxonomy().getTaxon(id);
        if (taxon == null) {
          throw new IllegalArgumentException(String.format(
              "Sighting %s is not valid for %s",
              sighting.getTaxon(),
              sighting.getTaxonomy().getName()));
        }
      }
    }
    
    sightingsLock().writeLock().lock();
    try {
      dirty.setDirty(true);
      sightings.addAll(newSightings);
    } finally {
      sightingsLock().writeLock().unlock();
    }    
  }

  /**
   * Removes a set of sightings to the ReportSet.
   */
  void removeSightings(Collection<Sighting> newSightings) {
    sightingsLock().writeLock().lock();
    try {
      dirty.setDirty(true);
      sightings.removeAll(newSightings);
    } finally {
      sightingsLock().writeLock().unlock();
    }    
  }

  /**
   * Delete a location from the ReportSet.  Moves all sightings from
   * this location to its parent.
   */
  public void deleteLocation(Location location) {
    Preconditions.checkArgument(locations.getLocation(location.getId()) == location,
        "Location is not in the ReportSet");
    Preconditions.checkArgument(location.getParent() != null,
        "Root locations may not be removed");
    Preconditions.checkArgument(
        !location.isBuiltInLocation()
        || location.getEbirdCode() == null
        || DELETEABLE_BUILT_IN_LOCATIONS.contains(location.getEbirdCode()),
        "Built-in location %s may not be removed",
        location.getEbirdCode());
    Preconditions.checkArgument(location.contents().isEmpty(),
        "Location with children may not be removed");
    
    Location parentLocation = location.getParent();
    List<Sighting> sightingsToRemove = Lists.newArrayList();
    List<Sighting> sightingsToAdd = Lists.newArrayList();
    
    sightingsLock().writeLock().lock();
    try {
      dirty.setDirty(true);
      
      // Move all sightings for that location to the parent
      for (Sighting sighting : sightings) {
        if (location.getId().equals(sighting.getLocationId())) {
          Sighting updatedSighting = sighting.asBuilder().setLocation(parentLocation).build();
          sightingsToRemove.add(sighting);
          sightingsToAdd.add(updatedSighting);
        }
      }
      
      // And now replace all affected sightings
      sightings.removeAll(sightingsToRemove);
      sightings.addAll(sightingsToAdd);
      
      // Update all affected vist info - move to the parent, unless that is pre-occupied.
      
      // First, gather the affected keys
      Set<VisitInfoKey> infoKeysAffected = Sets.newHashSet();
      for (VisitInfoKey visitInfoKey : visitInfos.keySet()) {
        if (visitInfoKey.locationId().equals(location.getId())) {
          infoKeysAffected.add(visitInfoKey);
        }
      }
      
      // Now, move affected visit info to the parent location, and remove the affected data
      for (VisitInfoKey affectedKey : infoKeysAffected) {
        VisitInfoKey replacementKey = affectedKey.withLocationId(parentLocation.getId());
        if (!visitInfos.containsKey(replacementKey)) {
          VisitInfo affectedValue = visitInfos.get(affectedKey);
          visitInfos.put(replacementKey, affectedValue);
        }
        visitInfos.remove(affectedKey);
      }
    } finally {
      sightingsLock().writeLock().unlock();
    }
    
    checklists.remove(location);
    locations.remove(location);
  }

  /**
   * Attaches a set of "Sp."s to be resolved (in the base taxonomy).  This is not
   * currently persisted in an way across saves - it's used only to allow the upgrade
   * resolution UI to not hit intentional sp.s.
   */
  public void setSpsToResolve(ImmutableSet<SightingTaxon> spsToResolve, @Nullable Taxonomy resolveTaxonomy) {
    this.spsToResolve = spsToResolve;
    this.resolveTaxonomy = resolveTaxonomy;
  }

  public ImmutableSet<SightingTaxon> getSpsToResolve() {
    return spsToResolve;
  }

  @Nullable
  public Taxonomy getResolveTaxonomy() {
    return resolveTaxonomy;
  }

  /** Store the file format version of the loaded ReportSet. */
  public void setLoadedVersion(String loadedVersion) {
    this.loadedVersion = loadedVersion;
  }

  /** Get the file format version of the loaded ReportSet. */
  public String getLoadedVersion() {
    return this.loadedVersion;
  }

  public void setChecklist(Location location, Checklist checklist) {
    Preconditions.checkNotNull(location);
    Preconditions.checkNotNull(checklist);
    locations.ensureAdded(location);
    checklists.put(location, checklist);
  }

  public Checklist getChecklist(Location location) {
    return checklists.get(location);
  }

  public Map<Location, Checklist> checklists() {
    return Collections.unmodifiableMap(checklists);
  }

  public void removeChecklist(Location location) {
    checklists.remove(location);
  }
  
  public String getPreferencesJson() {
    return preferencesJson;
  }
  
  public void setPreferencesJson(String preferencesJson, boolean markDirty) {
    this.preferencesJson = preferencesJson;
    if (markDirty) {
      dirty.setDirty(true);
    }
  }

  public void setOneTimeUpgrades(Collection<String> oneTimeUpgrades) {
    this.oneTimeUpgrades = ImmutableSet.copyOf(oneTimeUpgrades);
  }
  
  public ImmutableSet<String> oneTimeUpgrades() {
    return oneTimeUpgrades;
  }
  
  public void addOneTimeUpgrader(String name) {
    oneTimeUpgrades = ImmutableSet.<String>builder()
        .addAll(oneTimeUpgrades)
        .add(name)
        .build();
  }
  
  public VisitInfo putVisitInfo(VisitInfoKey visitInfoKey, VisitInfo visitInfo) {
    Preconditions.checkNotNull(visitInfoKey);
    Preconditions.checkNotNull(visitInfo);
    return visitInfos.put(visitInfoKey, visitInfo);
  }
  
  public VisitInfo getVisitInfo(VisitInfoKey visitInfoKey) {
    return visitInfos.get(Preconditions.checkNotNull(visitInfoKey));
  }

  public VisitInfo removeVisitInfo(VisitInfoKey visitInfoKey) {
    return visitInfos.remove(Preconditions.checkNotNull(visitInfoKey));
  }

  public Map<VisitInfoKey, VisitInfo> visitInfos() {
    return Collections.unmodifiableMap(visitInfos);
  }

  public Taxonomy getTaxonomy() {
    return taxonomy;
  }
  
  public Taxonomy getExtendedTaxonomy(String id) {
    TaxonomyWithChecklists taxonomyWithChecklists = extendedTaxonomies.get(id);
    return taxonomyWithChecklists == null ? null : taxonomyWithChecklists.taxonomy;
  }

  public ExtendedTaxonomyChecklists getExtendedTaxonomyChecklist(String taxonomyId) {
    TaxonomyWithChecklists taxonomyWithChecklists = extendedTaxonomies.get(taxonomyId);
    return taxonomyWithChecklists == null ? null : taxonomyWithChecklists.checklists;
  }
  
  public boolean hasExtendedTaxonomyChecklists(String taxonomyId) {
    TaxonomyWithChecklists taxonomyWithChecklists = extendedTaxonomies.get(taxonomyId);
    if (taxonomyWithChecklists == null) {
      return false;
    }
    
    if (taxonomyWithChecklists.checklists == null) {
      return false;
    }
    
    return !taxonomyWithChecklists.checklists.getChecklists().isEmpty();
  }

  public void addExtendedTaxonomy(Taxonomy extendedTaxonomy, ExtendedTaxonomyChecklists checklists) {
    if (extendedTaxonomies.containsKey(extendedTaxonomy.getId())) {
      throw new IllegalStateException("Cannot add " + extendedTaxonomy.getName());
    }
    extendedTaxonomies.put(extendedTaxonomy.getId(), new TaxonomyWithChecklists(extendedTaxonomy, checklists));
  }

  public Collection<Taxonomy> extendedTaxonomies() {
    return extendedTaxonomies.values().stream()
        .map(twc -> twc.taxonomy)
        .collect(ImmutableList.toImmutableList());
  }

  public void removeExtendedTaxonomy(Taxonomy extendedTaxonomy) {
    // Double-check that no sightings are still present.  This is duplicative of an iteration
    // outside of this class, but ... this is not a place to be especially worried about performance.
    for (Sighting sighting : sightings) {
      if (sighting.getTaxonomy() == extendedTaxonomy) {
        throw new IllegalStateException("Sightings still present, can't remove this taxonomy");
      }
    }
    
    if (!extendedTaxonomies.containsKey(extendedTaxonomy.getId())) {
      throw new IllegalStateException("Extended taxonomy " + extendedTaxonomy.getId() + " not found");
    }
    
    extendedTaxonomies.remove(extendedTaxonomy.getId());
    
    markDirty();
  }

  public UserSet getUserSet() {
    return userSet;
  }
  
  /**
   * Set the entire user set. Only used at reportSet creation time or when first attaching a
   * userset.
   */
  public void setUserSet(UserSet userSet) {
    Preconditions.checkState(this.userSet == null, "Cannot reassign already present userSet");
    Preconditions.checkNotNull(userSet);
    this.userSet = userSet;
    userSet.getDirty().addDirtyListener(e -> {
      if (userSet.getDirty().isDirty()) {
        markDirty();
      }
    });
    
    markDirty();
  }
  
  public void clearUserSet() {
    this.userSet = null;
    ImmutableSet<User> users = ImmutableSet.of();
    for (Sighting sighting : getSightings()) {
      sighting.getSightingInfo().setUsers(users);
    }
    markDirty();
  }
  
  public void replaceLocation(Location oldLocation, Location newLocation) {
    locations.replace(oldLocation, newLocation);
    Checklist checklist = checklists.get(oldLocation);
    if (checklist != null) {
      checklists.remove(oldLocation);
      checklists.put(newLocation, checklist);
    }
  }
}
