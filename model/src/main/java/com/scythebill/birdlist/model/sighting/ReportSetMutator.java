/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTimeFieldType;
import org.joda.time.LocalTime;
import org.joda.time.ReadablePartial;

import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * Implements a report set mutation, which consists of a
 */
public final class ReportSetMutator {
  private final ReportSet reportSet;
  private final List<Sighting> added = Lists.newArrayList();
  private final List<Sighting> removed = Lists.newArrayList();
  private boolean mutated = false;
  private Optional<String> location = Optional.absent();
  // Yes, optional-of-optional.  A legit operation *might* be removing the date altogether.
  private Optional<Optional<ReadablePartial>> date = Optional.absent();
  // Yes, optional-of-optional.  A legit operation *might* be removing the start time altogether.
  private Optional<Optional<LocalTime>> startTime = Optional.absent();

  ReportSetMutator(ReportSet reportSet) {
    this.reportSet = reportSet;
  }
  
  public ReportSetMutator adding(Collection<Sighting> sightingsToAdd) {
    Preconditions.checkState(!mutated);
    added.addAll(sightingsToAdd);
    return this;
  }

  public ReportSetMutator removing(Collection<Sighting> sightingsToRemove) {
    Preconditions.checkState(!mutated);
    removed.addAll(sightingsToRemove);
    return this;
  }
  
  public ReportSetMutator withChangedLocation(String locationId) {
    Preconditions.checkState(!mutated);
    this.location = Optional.of(locationId);
    return this;
  }

  public ReportSetMutator withChangedDate(ReadablePartial date) {
    Preconditions.checkState(!mutated);
    this.date = Optional.of(date == null ? Optional.absent() : Optional.of(date));
    return this;
  }

  public ReportSetMutator withChangedStartTime(Optional<LocalTime> startTime) {
    Preconditions.checkState(!mutated);
    this.startTime = Optional.of(startTime);
    return this;
  }

  /** Execute the mutation, updating visit info as needed. */
  public void mutate() {
    mutated = true;
    
    // See if any fields of visit info key have been updated
    boolean visitInfoKeysChanged = location.isPresent()
        || date.isPresent()
        || startTime.isPresent();
    
    Map<VisitInfoKey, VisitInfo> existingVisitInfo = null;
    
    // Verify that all the new sightings are compliant with the new location/date/time
    if (visitInfoKeysChanged) {
      // Doesn't make sense to specify updates if you aren't both removing and adding
      Preconditions.checkState(!removed.isEmpty(),
          "Mutation specifies visit info key changes, but didn't remove anything?");
      Preconditions.checkState(!added.isEmpty(),
          "Mutation specifies visit info key changes, but didn't add anything?");
      
      for (Sighting sighting : added) {
        if (location.isPresent()) {
          Preconditions.checkArgument(Objects.equal(location.get(), sighting.getLocationId()));
        }
        
        if (date.isPresent()) {
          // Can't compare the whole date, as only the fields set in "date" will be changed
          ReadablePartial sightingDate = sighting.getDateAsPartial();
          if (date.get().isPresent()) {
            Preconditions.checkArgument(sightingDate != null, "No date stored at all");
            ReadablePartial updatedDate = date.get().get();
            if (updatedDate.isSupported(DateTimeFieldType.year())) {
              Preconditions.checkArgument(sightingDate.isSupported(DateTimeFieldType.year()), "Year not stored");
              Preconditions.checkArgument(
                  sightingDate.get(DateTimeFieldType.year()) == updatedDate.get(DateTimeFieldType.year()), "Year not equal");
            }
            
            if (updatedDate.isSupported(DateTimeFieldType.monthOfYear())) {
              Preconditions.checkArgument(sightingDate.isSupported(DateTimeFieldType.monthOfYear()), "Month not stored");
              Preconditions.checkArgument(
                  sightingDate.get(DateTimeFieldType.monthOfYear()) == updatedDate.get(DateTimeFieldType.monthOfYear()), "Month not equal");
            }
            
            if (updatedDate.isSupported(DateTimeFieldType.dayOfMonth())) {
              Preconditions.checkArgument(sightingDate.isSupported(DateTimeFieldType.dayOfMonth()), "Day not stored");
              Preconditions.checkArgument(
                  sightingDate.get(DateTimeFieldType.dayOfMonth()) == updatedDate.get(DateTimeFieldType.dayOfMonth()), "Day not equal");
            }
          } else {
            Preconditions.checkArgument(sightingDate == null, "Date is stored, should be removed");
          }
        }
        
        if (startTime.isPresent()) {
          Optional<LocalTime> value = startTime.get();
          if (value.isPresent()) {
            Preconditions.checkArgument(Objects.equal(value.get(), sighting.getTimeAsPartial()));
          } else {
            Preconditions.checkArgument(sighting.getTimeAsPartial() == null);
          }
        }
      }
    }
    
    // Grab all visit info for the removed sightings;  these will likely need to be either
    // copied, removed, or both.  If 
    if (visitInfoKeysChanged || added.isEmpty()) {
      existingVisitInfo = Maps.newLinkedHashMap();
      for (Sighting sighting : removed) {
        VisitInfoKey visitInfoKey = VisitInfoKey.forSighting(sighting);
        if (visitInfoKey != null && !existingVisitInfo.containsKey(visitInfoKey)) {
          VisitInfo visitInfo = reportSet.getVisitInfo(visitInfoKey);
          if (visitInfo != null) {
            existingVisitInfo.put(visitInfoKey, visitInfo);
          }
        }
      }
    }
    
    reportSet.addSightings(added);
    reportSet.removeSightings(removed);
    
    // If there's relevant visit info, and visit info keys have changed, time to 
    // copy or remove visit info.
    if (existingVisitInfo != null && !existingVisitInfo.isEmpty()) {
      // First, ignore anything where there's still visit info present.  Visit info should
      // stay with those existing sightings, not be arbitrarily cloned.
      for (Sighting sighting : reportSet.getSightings()) {
        VisitInfoKey visitInfoKey = VisitInfoKey.forSighting(sighting);
        if (visitInfoKey != null && existingVisitInfo.containsKey(visitInfoKey)) {
          existingVisitInfo.remove(visitInfoKey);
        }
      }
      
      // Any visit info left at this point is orphaned, and must be updated.
      if (!existingVisitInfo.isEmpty()) {
        if (added.isEmpty()) {
          for (VisitInfoKey visitInfoKey : existingVisitInfo.keySet()) {
            reportSet.removeVisitInfo(visitInfoKey);
          }
        } else {
          for (Map.Entry<VisitInfoKey, VisitInfo> entry : existingVisitInfo.entrySet()) {
            VisitInfoKey oldVisitInfoKey = entry.getKey();
            // If the date operation is *removing* the date, then there's no new visit info to write
            if (!date.isPresent() || date.get().isPresent()) {
              VisitInfoKey newVisitInfoKey = oldVisitInfoKey;
              if (location.isPresent()) {
                newVisitInfoKey = newVisitInfoKey.withLocationId(location.get());
              }
              if (date.isPresent()) {
                newVisitInfoKey = newVisitInfoKey.withDate(date.get().get());
              }
              if (startTime.isPresent()) {
                newVisitInfoKey = newVisitInfoKey.withStartTime(startTime.get());
              }
              Preconditions.checkState(!oldVisitInfoKey.equals(newVisitInfoKey));
              // Check that we're not going to overwrite existing visit info
              VisitInfo visitInfoAlreadyAtNewKey = reportSet.getVisitInfo(newVisitInfoKey);
              if (visitInfoAlreadyAtNewKey == null) {
                // ... if we're not overwriting anything, move the visit info to the new key
                reportSet.putVisitInfo(newVisitInfoKey, entry.getValue());
              } else {
                // We would be overwriting.  For the most part, there's little to be done, but
                // at a minimum see if there's any text that needs to be preserved.
                VisitInfo soonToBeLostVistInfo = entry.getValue();
                // TODO: consider if there's more that we want to preserve than just the comments.  Should
                // we take every field, see if it's set in the soon-to-be-lost visit info, and not set in the
                // existing visit info, and merge it over?
                if (soonToBeLostVistInfo.comments().isPresent()) {
                  String combinedComments;
                  if (!visitInfoAlreadyAtNewKey.comments().isPresent()) {
                    combinedComments = soonToBeLostVistInfo.comments().get();
                  } else {
                    combinedComments = visitInfoAlreadyAtNewKey.comments().get() + "\n" + soonToBeLostVistInfo.comments().get();
                  }
                  // ... and write the combined comments
                  reportSet.putVisitInfo(newVisitInfoKey, visitInfoAlreadyAtNewKey.asBuilder().withComments(combinedComments).build());
                }
              }
            }
            // ... and remove the old visit info
            reportSet.removeVisitInfo(oldVisitInfoKey);
          }
        }
      }
    }
    
    // And now that it's done, mark the report set as dirty
    reportSet.markDirty();
  }
}
