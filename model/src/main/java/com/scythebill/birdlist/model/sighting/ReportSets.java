/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.Resources;
import com.opencsv.CSVReader;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Utility class for report sets.
 */
public class ReportSets {
  public static final String VERSION_FORMAT_CURRENT = "16.0.0";

  /*
   * Prefixes used for built-in locations.  Going forward, this
   * would make it possible to have location packs that are
   * optionally installed (even after creation), and don't clobber
   * user-specified locations.
   */
  static private final String WORLD_REGIONS_PREFIX = "";
  static private final String WORLD_COUNTRIES_PREFIX = "ct";
  static private final String US_STATES_PREFIX = "usst";

  /**
   * Returns a new report set with a default set of locations and
   * no reports.
   * <p>
   * The location set is currently hard-coded:  all "countries" of the world,
   * and all US "states".  The term "country" here is used extremely loosely,
   * and does not consistently correspond to politically defined countries.
   * Instead, countries are defined to place records in ABA listing regions.
   * E.g., Socotra is politically part of Yemen, but is in the African
   * listing region, not Eurasia.
   * <p>
   * Similarly, countries are sometimes repeated when they occur in multiple
   * listing regions.  E.g., the United States is in North America, the
   * West Indies, and the Pacific Ocean.
   * <p>
   * For U.S. states, similar shenanigans are played - Puerto Rico and
   * Guam are not (today) states, but are listed as such.
   */
  static public ReportSet newReportSet(Taxonomy taxonomy) {
    LocationSet newLocations = newLocations();
    ReportSet reportSet = new ReportSet(newLocations, Lists.<Sighting>newArrayList(), taxonomy);
    reportSet.setLoadedVersion(VERSION_FORMAT_CURRENT);
    return reportSet;
  }
  
  static public LocationSet newLocations() {
    Map<String, Location> locationsMap = Maps.newHashMap();
    LocationSet.Builder builder = new LocationSet.Builder();
    LocationSet locations = builder.build();
    for (String topLevelRegion : ImmutableSet.of(
        "North America",
        "South America",
        "Eurasia",
        "Africa",
        "Australasia",
        "Pacific Ocean",
        "Indian Ocean",
        "Atlantic Ocean",
        "Arctic Ocean",
        "South Polar Region")) {
      Location location = Location.builder()
          .setName(topLevelRegion)
          .setType(Location.Type.region)
          .setBuiltInPrefix(WORLD_REGIONS_PREFIX)
          .build();
      locations.addLocation(location);      
      locationsMap.put(topLevelRegion, location);
    }
   
    Location eurasia = Preconditions.checkNotNull(locationsMap.get("Eurasia"));
    for (String eurasiaRegion : ImmutableSet.of("Europe", "Asia")) {
      Location location = Location.builder()
          .setName(eurasiaRegion)
          .setType(Location.Type.region)
          .setParent(eurasia)
          .setBuiltInPrefix(WORLD_REGIONS_PREFIX)
          .build();
      locations.addLocation(location);      
      locationsMap.put(eurasiaRegion, location);
    }
    
    Location northAmerica = Preconditions.checkNotNull(locationsMap.get("North America"));
    for (String northAmericaRegion : ImmutableSet.of("West Indies", "Central America")) {
      Location location = Location.builder()
          .setName(northAmericaRegion)
          .setType(Location.Type.region)
          .setBuiltInPrefix(WORLD_REGIONS_PREFIX)
          .setParent(northAmerica)
          .build();
      locations.addLocation(location);      
      locationsMap.put(northAmericaRegion, location);
    }
    
    Map<String, Location> unitedStatesByRegions = Maps.newHashMap();

    // Add all the countries.
    // TODO: make this optional;  birders that don't travel may not want it
    for (String[] line : csvContents("countries.csv")) {
      String country = line[0];
      Location parent = Preconditions.checkNotNull(locationsMap.get(line[1]));
      Location.Type type = Location.Type.country;
      if (line.length >= 4
          && line[3].equalsIgnoreCase("State")) {
        type = Location.Type.state;
      }
      
      Location.Builder countryBuilder = Location.builder()
          .setName(country)
          .setType(type)
          .setBuiltInPrefix(WORLD_COUNTRIES_PREFIX)
          .setParent(parent);
      Preconditions.checkArgument(line.length >= 2);
      Preconditions.checkArgument(!Strings.isNullOrEmpty(line[2]));
      countryBuilder.setEbirdCode(line[2]);
      
      Location location = countryBuilder.build();
      locations.addLocation(location);
      
      // Remember the various chunks of the US for use in storing states
      if ("United States".equals(country)) {
        unitedStatesByRegions.put(line[1], location);
      }
    }
    
    // Add all US states.
    // TODO: make this optional;  non-US birders may not want it
    for (String[] line : csvContents("us-states.csv")) {
      String state = line[0];
      String usRegion = line[1];
      String ebirdCode = line[2];
      String type = line.length >= 4 ? line[3] : null;
      
      Location parent = Preconditions.checkNotNull(unitedStatesByRegions.get(usRegion));
      Location location = Location.builder()
          .setName(state)
          .setParent(parent)
          .setEbirdCode(ebirdCode)
          // Set selected non-states to "country", to conform to EBird reporting rules - 
          // which make as much sense as calling them states.
          .setType("country".equals(type) ? Location.Type.country : Location.Type.state)
          .setBuiltInPrefix(US_STATES_PREFIX)
          .build();
      
      locations.addLocation(location);
    }

    // Add all UK locations
    Location europe = locationsMap.get("Europe");
    Location uk = europe.getContent("United Kingdom");
    PredefinedLocations predefinedLocations = PredefinedLocations.loadAndParse();
    predefinedLocations.mergeInto(locations, uk);
    
    return locations;
  }

  private static Iterable<String[]> csvContents(String csvFile) {
    URL url = Resources.getResource(ReportSets.class, csvFile);
    try (CSVReader csvReader = new CSVReader(
        Resources.asCharSource(url, Charsets.UTF_8).openBufferedStream())) {
      // Return the full contents of the file, skipping the first line (headers)
      return Iterables.skip(csvReader.readAll(), 1);
    } catch (IOException e) {
      // Should not occur - this is a local resource
      throw new RuntimeException(e);
    }
  }
}
