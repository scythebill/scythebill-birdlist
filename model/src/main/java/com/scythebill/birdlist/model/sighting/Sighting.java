/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import org.joda.time.LocalTime;
import org.joda.time.ReadablePartial;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * A single sighting of a species.
 * TODO: add support for generic metadata (for extensibility + preservation
 * of unknown properties)
 */
public class Sighting {
  private final SightingTaxon taxon;
  private ReadablePartial date;
  private String locationId;
  private SightingInfo sightingInfo;
  private Taxonomy taxonomy;
  private LocalTime time;
  
  static public class Builder {    
    private ReadablePartial date;
    private LocalTime time;
    private String locationId;
    private SightingTaxon taxon;
    private SightingInfo sightingInfo;
    private Taxonomy taxonomy;
    private Location location;
    
    private Builder() {
    }
    
    private Builder(Sighting baseSighting) {
      this.date = baseSighting.date;
      this.time = baseSighting.time;
      this.locationId = baseSighting.locationId;
      this.taxon = baseSighting.taxon;
      this.taxonomy = baseSighting.taxonomy;
      if (baseSighting.sightingInfo != null) {
        this.sightingInfo = baseSighting.sightingInfo.clone();
      }
    }
    
    public Builder setTaxonomy(Taxonomy taxonomy) {
      Preconditions.checkArgument(!(taxonomy instanceof MappedTaxonomy),
          "Mapped taxonomy may not be attached to Sighting");
      this.taxonomy = taxonomy;
      return this;
    }
    
    public Builder setDate(ReadablePartial date) {
      this.date = date;
      return this;
    }
    
    public ReadablePartial getDate() {
      return this.date;
    }

    public Builder setTime(LocalTime time) {
      this.time = time;
      return this;
    }
    
    public Builder setLocation(Location location) {
      this.locationId = Preconditions.checkNotNull(location.getId());
      this.location = location;
      return this;
    }

    public String getLocationId() {
      return locationId;
    }

    public Location getLocation() {
      return location;
    }
    
    public Builder setTaxon(Taxon taxon) {
      this.taxon = taxon.toBaseSightingTaxon();
      this.taxonomy = TaxonUtils.getBaseTaxonomy(taxon.getTaxonomy());
      return this;
    }
    
    public Builder setTaxon(SightingTaxon sightingTaxon) {
      this.taxon = sightingTaxon;
      return this;
    }
    
    public SightingTaxon getTaxon() {
      return this.taxon;
    }

    public SightingInfo getSightingInfo() {
      if (sightingInfo == null) {
        sightingInfo = new SightingInfo();
      }
      return sightingInfo;
    }  
    
    public Builder setSightingInfo(SightingInfo sightingInfo) {
      this.sightingInfo = sightingInfo;
      return this;
    }
    
    public Sighting build() {
      Sighting sighting = new Sighting(taxon, taxonomy);
      sighting.locationId = locationId;
      sighting.sightingInfo = sightingInfo;
      sighting.date = date;
      sighting.time = time;
      return sighting;
    }
  }
  
  /** Create a new sighting builder. */
  public static Builder newBuilder() {
    return new Builder();
  }

  // Constructor for the Builder
  private Sighting(
      SightingTaxon sightingTaxon, Taxonomy taxonomy) {
    this.taxon = Preconditions.checkNotNull(sightingTaxon, "Null taxon");
    this.taxonomy = Preconditions.checkNotNull(taxonomy, "Null taxonomy");
  }
  
  public Builder asBuilder() {
    return new Builder(this);
  }
  
  public String getLocationId() {
    return locationId;
  }

  public Taxonomy getTaxonomy() {
    return taxonomy;
  }
  
  public SightingTaxon getTaxon() {
    return taxon;
  }

  public ReadablePartial getDateAsPartial() {
    return date;
  }

  public LocalTime getTimeAsPartial() {
    return time;
  }

  public boolean hasSightingInfo() {
    return sightingInfo != null;
  }
  
  public SightingInfo getSightingInfo() {
    if (sightingInfo == null) {
      sightingInfo = new SightingInfo();
    }
    
    return sightingInfo;
  }

  /**
   * Sets the sighting info. For a sighting in a ReportSet, you must
   * call {@link ReportSet#markDirty()}.
   */
  public void setSightingInfo(SightingInfo sightingInfo) {
    this.sightingInfo = sightingInfo;
  }

  @Override public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("taxon", taxon)
        .add("where", locationId)
        .addValue(date)
        .addValue(time)
        .addValue(sightingInfo)
        .toString();
  }

  public Sighting mergeWith(Sighting newSighting) {
    Builder builder = asBuilder();
    builder.getSightingInfo().mergeWith(newSighting.getSightingInfo());
    return builder.build();
  }
}
