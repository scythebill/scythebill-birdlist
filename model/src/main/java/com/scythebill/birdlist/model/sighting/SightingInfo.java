/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.user.User;

/**
 * Metadata associated with a sighting.
 * TODO: add support for generic metadata (for extensibility + preservation
 * of unknown properties)
 */
public final class SightingInfo implements Cloneable {
  /** 
   * Various constants for the "countability" of an individual sighting. 
   */
  public enum SightingStatus {
    /** Default value - a wild species. */
    NONE("none", ""),
    /** A species whose record has not been accepted by a committee. */
    RECORD_NOT_ACCEPTED("notAccepted", "not accepted"),
    /** An introduced species, considered established. */
    INTRODUCED("intro", "introduced"),
    /** An introduced species, not considered established, usually not countable. */
    INTRODUCED_NOT_ESTABLISHED("notEstablished", "not established"),
    /** A dead species, usually not countable. */
    DEAD("dead", "dead"),
    /** A species seen by others. */
    NOT_BY_ME("notByMe", "not by me"),
    /** A domestic species, usually not countable. */
    DOMESTIC("domestic", "domestic"),
    /** An observation where the identification is not certain. */
    ID_UNCERTAIN("idUncertain", "uncertain"),
    /** An uncountably bad sighting. */
    UNSATISFACTORY_VIEWS("unsatisfactoryViews", "unsatisfactory views"),
    /** A countably bad sighting. */
    BETTER_VIEW_DESIRED("bvd", "better view desired"),
    /** An observation without the actual taxon;  instead just signs of the taxon (tracks, feathers, burrows, etc.) */
    SIGNS("signs", "signs"),
    /** An observation of a bird that is restrained, e.g. in-hand. Typically, not countable. */
    RESTRAINED("restrained", "restrained");
    
    private final String id;
    private final String export;
    private static final ImmutableMap<String, SightingStatus> byId;
    private static final ImmutableMap<String, SightingStatus> byExport;
    static {
      ImmutableMap.Builder<String, SightingStatus> builder = ImmutableMap.builder();
      ImmutableMap.Builder<String, SightingStatus> exportBuilder = ImmutableMap.builder();
      for (SightingStatus status : values()) {
        builder.put(status.getId(), status);
        exportBuilder.put(status.getExportText(), status);
      }
      
      byId = builder.build();
      byExport = exportBuilder.build();
    }
    
    static public SightingStatus forId(String id) {
      return byId.get(id);
    }

    static public SightingStatus forExportText(String exportText) {
      return byExport.get(exportText);
    }
    
    SightingStatus(String id, String export) {
      this.id = id;
      this.export = export;
    }
    
    /** External ID of the status, used for externalization */
    public String getId() {
      return id;
    }
    
    public String getExportText() {
      return export;
    }
  }

  /** 
   * Various constants for the breeding behavior observed. 
   */
  public enum BreedingBirdCode {
    /** Default value - no breeding behavior observed. */
    NONE("", ""),
    NEST_WITH_YOUNG("NY", "16"),
    NEST_WITH_EGGS("NE", "15"),
    CARRYING_FECAL_SAC("CS", "14"),
    FEEDING_YOUNG("FY", "12"),
    CARRYING_FOOD("CF", "14"),
    RECENTLY_FLEDGED("FL", "12"),
    OCCUPIED_NEST("ON", "13"),
    USED_NEST("UN", "11"),
    DISTRACTION_DISPLAY("DD", "10"),
    NEST_BUILDING("NB", "09"),
    CARRYING_NESTING_MATERIAL("CN", "09"),
    PHYSIOLOGICAL_EVIDENCE("PE", ""),
    WOODPECKER_WREN_NEST_BUILDING("B", "09"),
    AGITATED_BEHAVIOR("A", "07"),
    VISITING_PROBABLE_NEST_SITE("N", "06"),
    COURTSHIP_DISPLAY_OR_COPULATION("C", "05"),
    TERRITORIAL_DEFENSE("T", "04"),
    PAIR_IN_SUITABLE_HABITAT("P", "03"),
    MULTIPLE_SINGING_BIRDS("M", "02"),
    SINGING_BIRD_7_DAYS("S7", "02"),
    SINGING_BIRD("S", "02"),
    IN_APPROPRIATE_HABITAT("H", "01"),
    FLYOVER("F", "00");
    
    private final String id;
    private final String birdTrackId;
    private static final ImmutableMap<String, BreedingBirdCode> byId;
    static {
      ImmutableMap.Builder<String, BreedingBirdCode> builder = ImmutableMap.builder();
      for (BreedingBirdCode status : values()) {
        if (status != NONE) {
          builder.put(status.getId(), status);
        }
      }
      
      byId = builder.build();
    }
    
    static public BreedingBirdCode forId(String id) {
      return byId.get(id);
    }

    BreedingBirdCode(String id, String birdTrackId) {
      this.id = id;
      this.birdTrackId = birdTrackId;
    }
    
    /** External ID of the status, used for externalization */
    public String getId() {
      return id;
    }

    public String getBirdTrackId() {
      return birdTrackId;
    }
  }
  
  private String description;
  private ApproximateNumber number;
  private boolean female;
  private boolean male;
  private boolean adult;
  private boolean immature;
  private boolean heardOnly;
  private SightingStatus sightingStatus = SightingStatus.NONE;
  private boolean photographed;
  private BreedingBirdCode breedingBirdCode = BreedingBirdCode.NONE;
  private List<Photo> photos;
  private ImmutableSet<User> users;

  /**
   * @return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description the description to set
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * @return the female
   */
  public boolean isFemale() {
    return female;
  }

  /**
   * @param female the female to set
   */
  public void setFemale(boolean female) {
    this.female = female;
  }

  /**
   * @return the heardOnly
   */
  public boolean isHeardOnly() {
    return heardOnly;
  }

  /**
   * @param heardOnly the heardOnly to set
   */
  public void setHeardOnly(boolean heardOnly) {
    this.heardOnly = heardOnly;
  }

  /**
   * @return the immature
   */
  public boolean isImmature() {
    return immature;
  }

  /**
   * @param immature the immature to set
   */
  public void setImmature(boolean immature) {
    this.immature = immature;
  }

  public boolean isAdult() {
    return adult;
  }

  public void setAdult(boolean adult) {
    this.adult = adult;
  }

  /**
   * @return the male
   */
  public boolean isMale() {
    return male;
  }

  /**
   * @param male the male to set
   */
  public void setMale(boolean male) {
    this.male = male;
  }

  /**
   * @return the number
   */
  public ApproximateNumber getNumber() {
    return number;
  }

  /**
   * @param number the number to set
   */
  public SightingInfo setNumber(ApproximateNumber number) {
    this.number = number;
    return this;
  }
  
  public SightingStatus getSightingStatus() {
    return sightingStatus;
  }
  
  public void setSightingStatus(SightingStatus sightingStatus) {
    this.sightingStatus = sightingStatus;
  }

  public BreedingBirdCode getBreedingBirdCode() {
    return breedingBirdCode;
  }
  
  public void setBreedingBirdCode(BreedingBirdCode breedingBirdCode) {
    this.breedingBirdCode = breedingBirdCode;
  }

  /**
   * @return true if photographed
   */
  public boolean isPhotographed() {
    return photographed;
  }

  public void setPhotographed(boolean photographed) {
    this.photographed = photographed;
  }
  
  public List<Photo> getPhotos() {
    if (photos == null) {
      return ImmutableList.of();
    }
    
    return Collections.unmodifiableList(photos);
  }
  
  public void setPhotos(List<Photo> newPhotos) {
    if (newPhotos.isEmpty()) {
      photos = null;
    } else {
      photos = Lists.newArrayList(newPhotos);
    }
  }
  
  public ImmutableSet<User> getUsers() {
    if (users == null) {
      return ImmutableSet.of();
    }
    
    return users;
  }
  
  public void setUsers(Collection<User> collection) {
    if (collection.isEmpty()) {
      users = null;
    } else {
      users = ImmutableSet.copyOf(collection);
    }
  }
  
  @Override
  public SightingInfo clone() {
    try {
      SightingInfo cloned = (SightingInfo) super.clone();
      if (photos != null) {
        cloned.photos = Lists.newArrayList(photos);
      }
      return cloned;
    } catch (CloneNotSupportedException e) {
      throw new RuntimeException(e);
    }
  }

  public void mergeWith(SightingInfo that) {
    adult = adult || that.adult;
    female = female || that.female;
    male = male || that.male;
    immature = immature || that.immature;
    photographed = photographed || that.photographed;
    if (that.photos != null) {
      if (photos == null) {
        photos = Lists.newArrayList(); 
      }
      photos.addAll(that.photos);
    }
    
    heardOnly = heardOnly && that.heardOnly;
    if (breedingBirdCode == BreedingBirdCode.NONE) {
      breedingBirdCode = that.breedingBirdCode;
    }
    
    if (number != null && that.number != null) {
      number = number.plus(that.number);
    }
    
    if (sightingStatus.compareTo(that.sightingStatus) > 0) {
      sightingStatus = that.sightingStatus;
    }
    
    if (description == null) {
      description = that.description;
    } else if (that.description != null) {
      description = description + "\n" + that.description;
    }
    
    if (users == null || users.isEmpty()) {
      users = that.users;
    } else if (that.users != null) {
      users = ImmutableSet.<User>builder()
          .addAll(this.users)
          .addAll(that.users)
          .build();
    }
  }
}
