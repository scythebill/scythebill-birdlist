/*   
 * Copyright 2012 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import java.util.Collection;

import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Represents the "Taxon" seen at any one time, be it a species (or subspecies etc.)
 * or a "sp." (not conclusively identified beyond one of a few candidates) or
 * a hybrid between two taxa. 
 */
public interface SightingTaxon {
  enum Type {
    /** A single taxon. */
    SINGLE,
    /** One of N taxa, not specifically identified. */
    SP,
    /** A hybrid of two taxa. */
    HYBRID,
    /**
     * A single taxon, with a subspecies not found in the primary taxonomy.
     * This value will not be returned for a Resolved, only for a SightingTaoxn.
     */
    SINGLE_WITH_SECONDARY_SUBSPECIES
  }

  Type getType();
  
  /**
   * Return the taxon identifier, if there is exactly one.
   * 
   * @throws IllegalStateException if the sighting does not correspond to a single taxon
   */
  String getId();

  /**
   * Returns a subidentifier, if any.
   * 
   * @throws IllegalStateException if the sighting does not have a subidentifier.
   */
  String getSubIdentifier();

  /**
   * Returns all taxon IDs. 
   */
  Collection<String> getIds();

  /**
   * Returns true if the taxon is exactly that ID, or is worthy of display under that ID.
   */
  boolean shouldBeDisplayedWith(String id);

  /**
   * Resolves the SightingTaxon;  may map across taxonomies, at which point
   * Resolved.getSightingTaxon() might not return this instance!
   * 
   * TODO: should this support resolving from IOC back to Clements?
   */
  Resolved resolve(Taxonomy taxonomy);
  
  /**
   * Resolves the SightingTaxon within an instance.  Does not resolve across
   * taxonomies.  Generally, you don't want this.
   */
  Resolved resolveInternal(Taxonomy taxonomy);

  public interface Resolved {
    /**
     * Returns the type of the resolved.  This is not necessarily the same as the SightingTaxon.Type
     * that was resolved, and is never {@link SightingTaxon.Type#SINGLE_WITH_SECONDARY_SUBSPECIES}.
     */
    Type getType();
    
    /**
     * Given a taxonomic type, return a SightingTaxon that elevates all components to at least
     * that type.
     */
    SightingTaxon getParentOfAtLeastType(Taxon.Type smallestType);
  
    /**
     * Returns a parent with the requested taxonomic type (or {@code this} if
     * it's already of that type.)
     */
    Resolved resolveParentOfType(Taxon.Type taxonType);

    /** Gets a parent SightingTaxon in the current taxonomy. */
    SightingTaxon getParent();

    /**
     * Return the resolved taxon, if there is exactly one.
     * 
     * @throws IllegalStateException if the sighting does not correspond to a single taxon
     */
    Taxon getTaxon();

    Collection<Taxon> getTaxa();
    
    Taxon.Type getSmallestTaxonType();

    Taxon.Type getLargestTaxonType();

    String getCommonName();

    /** Returns true if the taxon has a common name. */
    boolean hasCommonName();

    /** Get a common name without appending any subspecies. */
    String getSimpleCommonName();
    
    /** Get the scientific name. */
    String getName();

    /** Get the full scientific name. */
    String getFullName();

    Species.Status getTaxonStatus();

    boolean isChildOf(Taxon parent);

    SightingTaxon getSightingTaxon();

    Taxonomy getTaxonomy();

    String getPreferredSingleName();

    String getPreferredSingleNameWithoutSpecies();
  }
}
