/*   
 * Copyright 2012 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.names.LocalNames;
import com.scythebill.birdlist.model.util.TaxonNames;


/**
 * Utilities for working with SightingTaxon instances.
 * <p>
 * TODOs:
 * - Support some way to take a sp. and resolve its ID to a single taxon
 * - Allow queries to include sp. counts
 * - Support in TaxonMapping (COULD BE POSTPONED UNTIL NEXT TAXON UPDATE)
 * - Clean up (?) for hybrids and sp.s - only makes some sense in Browse by species
 */
public class SightingTaxons {
  private SightingTaxons() {
  }
  
  public static SightingTaxon newSightingTaxon(String singleId) {
    return new SingleSightingTaxon(singleId);
  }

  public static SightingTaxon newSightingTaxonWithSecondarySubspecies(
      String singleId, String subspeciesId) {
    return new SingleWithSubspeciesSightingTaxon(singleId, subspeciesId);
  }

  public static SightingTaxon newSpTaxon(Iterable<String> taxa) {
    return new SpSightingTaxon(taxa);
  }
  
  /**
   * Return a single SightingTaxon is there's one, a Sp SightingTaxon if > 1.
   */
  public static SightingTaxon newPossiblySpTaxon(Collection<String> taxa) {
    if (taxa.size() == 1) {
      return SightingTaxons.newSightingTaxon(Iterables.getOnlyElement(taxa));
    } else {
      return SightingTaxons.newSpTaxon(taxa);
    }    
  }

  public static SightingTaxon newHybridTaxon(Iterable<String> taxa) {
    return new HybridSightingTaxon(taxa);
  }

  public static SightingTaxon.Resolved newResolved(Taxon taxon) {
    return new SingleSightingTaxon(taxon.getId()).new SingleResolved(taxon);
  }

  public static Resolved newSpResolved(Collection<Taxon> taxa) {
    ImmutableSet.Builder<String> ids = ImmutableSet.builder();
    for (Taxon taxon : taxa) {
      ids.add(taxon.getId());
    }
    return new SpSightingTaxon(ids.build()).new SpResolved(taxa);
  }
  
  public static Resolved newPossiblySpResolved(Collection<Taxon> taxa) {
    if (taxa.size() == 1) {
      return newResolved(taxa.iterator().next());
    }
    
    return newSpResolved(taxa);
  }

  /**
   * SightingTaxon implementation for a single taxon.
   */
  static class SingleSightingTaxon implements SightingTaxon {
    private final String taxonId;

    public SingleSightingTaxon(String taxonId) {
      this.taxonId = Preconditions.checkNotNull(taxonId);
    }

    @Override
    public Type getType() {
      return Type.SINGLE;
    }

    @Override
    public String getId() {
      return taxonId;
    }

    @Override
    public String getSubIdentifier() {
      throw new IllegalStateException();
    }
    
    @Override
    public Collection<String> getIds() {
      return ImmutableList.of(taxonId);
    }

    @Override
    public boolean shouldBeDisplayedWith(String id) {
      return taxonId.equals(id);
    }

    @Override
    public Resolved resolve(Taxonomy taxonomy) {
      return taxonomy.resolveInto(this);
    }

    @Override
    public Resolved resolveInternal(Taxonomy taxonomy) {
      Taxon taxon = taxonomy.getTaxon(getId());
      if (taxon == null) {
        throw new NullPointerException("Could not get taxon for \"" + getId() + "\"");
      }
      return new SingleResolved(taxon);
    }

    class SingleResolved extends BaseResolved  {
      private final Taxon taxon;

      public SingleResolved(Taxon taxon) {
        this.taxon = Preconditions.checkNotNull(taxon);
      }

      @Override
      public Type getType() {
        return Type.SINGLE;
      }
      
      @Override
      public Taxon.Type getSmallestTaxonType() {
        return taxon.getType();
      }

      @Override
      public Taxon.Type getLargestTaxonType() {
        return taxon.getType();
      }

      @Override
      public SightingTaxon getParent() {
        return new SingleSightingTaxon(taxon.getParent().getId());
      }

      @Override
      public SightingTaxon getParentOfAtLeastType(Taxon.Type smallestType) {
        Taxon parent = taxon;
  
        while (parent.getType().compareTo(smallestType) < 0) {
          parent = parent.getParent();
          Preconditions.checkState(parent != null);
        }
        
        if (parent == taxon) {
          return SingleSightingTaxon.this;
        }
        return newSightingTaxon(parent.getId());
      }

      @Override
      public Resolved resolveParentOfType(Taxon.Type type) {
        Taxon parent = taxon;
  
        while (parent.getType().compareTo(type) != 0) {
          parent = parent.getParent();
          Preconditions.checkState(parent != null);
        }
        
        if (parent == taxon) {
          return this;
        }
        
        return newResolved(parent);
      }

      @Override
      public String getCommonName() {
        return localNames().getCommonName(taxon);
      }

      @Override
      public String getSimpleCommonName() {
        return localNames().getSimpleCommonName(taxon);
      }

      @Override
      public boolean hasCommonName() {
        return taxon.getCommonName() != null;
      }
      
      @Override
      public String getName() {
        return taxon.getName();
      }

      @Override
      public String getFullName() {
        return TaxonUtils.getFullName(taxon);
      }

      @Override
      public Status getTaxonStatus() {
        return TaxonUtils.getTaxonStatus(taxon);
      }

      @Override
      public boolean isChildOf(Taxon parent) {
        return TaxonUtils.isChildOf(parent, taxon);
      }

      @Override
      public Taxon getTaxon() {
        return taxon;
      }

      @Override
      public Taxonomy getTaxonomy() {
        return taxon.getTaxonomy();
      }
      
      @Override
      public Collection<Taxon> getTaxa() {
        return ImmutableSet.of(taxon);
      }

      @Override
      public SightingTaxon getSightingTaxon() {
        return SingleSightingTaxon.this;
      }
    }
   
    @Override
    public int hashCode() {
      return taxonId.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (!(obj instanceof SingleSightingTaxon)) {
        return false;
      }
      
      SingleSightingTaxon that = (SingleSightingTaxon) obj;
      return that.taxonId.equals(taxonId);
    }
    
    @Override
    public String toString() {
      return MoreObjects.toStringHelper(this)
          .add("taxonId", taxonId)
          .toString();
    }
  }

  static final class SingleWithSubspeciesSightingTaxon implements SightingTaxon {
    private final String taxonId;
    private final String subspeciesId;

    public SingleWithSubspeciesSightingTaxon(String taxonId, String subspeciesId) {
      this.taxonId = Preconditions.checkNotNull(taxonId);
      this.subspeciesId = Preconditions.checkNotNull(subspeciesId);
    }

    @Override
    public Type getType() {
      return Type.SINGLE_WITH_SECONDARY_SUBSPECIES;
    }

    @Override
    public String getId() {
      return taxonId;
    }

    @Override
    public String getSubIdentifier() {
      return subspeciesId;
    }
    
    @Override
    public Collection<String> getIds() {
      return ImmutableList.of(taxonId);
    }

    @Override
    public boolean shouldBeDisplayedWith(String id) {
      return taxonId.equals(id);
    }

    @Override
    public Resolved resolve(Taxonomy taxonomy) {
      return taxonomy.resolveInto(this);
    }

    @Override
    public Resolved resolveInternal(Taxonomy taxonomy) {
      Taxon taxon = taxonomy.getTaxon(getId());
      Taxon subspecies = findSubspecies(taxon);
      
      // TODO(awiner): if subspecies is non-null, then we really should resolve
      // to a special Resolved where getSightingTaxon *is not* "this", but is
      // instead local to the taxonomy that has the subspecies (and consequently
      // has type SINGLE)
      return new SingleWithSubspeciesResolved(MoreObjects.firstNonNull(subspecies, taxon));
    }
    
    private Taxon findSubspecies(Taxon taxon) {
      for (Taxon child : taxon.getContents()) {
        if (child.getName().equals(subspeciesId)) { 
          return child;
        }
      }
      
      return null;
    }

    class SingleWithSubspeciesResolved extends BaseResolved  {
      private final Taxon taxon;

      public SingleWithSubspeciesResolved(Taxon taxon) {
        this.taxon = taxon;
      }

      @Override
      public Type getType() {
        return Type.SINGLE;
      }
      
      @Override
      public Taxon.Type getSmallestTaxonType() {
        return taxon.getType();
      }

      @Override
      public Taxon.Type getLargestTaxonType() {
        return taxon.getType();
      }

      @Override
      public SightingTaxon getParent() {
        return new SingleSightingTaxon(taxon.getParent().getId());
      }

      @Override
      public SightingTaxon getParentOfAtLeastType(Taxon.Type smallestType) {
        Taxon parent = taxon;
  
        while (parent.getType().compareTo(smallestType) < 0) {
          parent = parent.getParent();
          Preconditions.checkState(parent != null);
        }
        
        // Returning the taxon-with-subspecies is fine if you're looking for a subspecies, but breaks lookup
        // and maps badly if you're trying to get smallestType != subspecies.
        if (parent == taxon && smallestType == Taxon.Type.subspecies) {
          return SingleWithSubspeciesSightingTaxon.this;
        }
        return newSightingTaxon(parent.getId());
      }

      @Override
      public Resolved resolveParentOfType(Taxon.Type type) {
        Taxon parent = taxon;
  
        while (parent.getType().compareTo(type) != 0) {
          parent = parent.getParent();
          Preconditions.checkState(parent != null);
        }
        
        if (parent == taxon) {
          return this;
        }
        
        return newResolved(taxon);
      }

      @Override
      public String getCommonName() {
        return localNames().getCommonName(taxon);
      }

      @Override
      public boolean hasCommonName() {
        return taxon.getCommonName() != null;
      }
      
      @Override
      public String getSimpleCommonName() {
        return localNames().getSimpleCommonName(taxon);
      }

      @Override
      public String getName() {
        return taxon.getName();
      }

      @Override
      public String getFullName() {
        return TaxonUtils.getFullName(taxon);
      }

      @Override
      public Status getTaxonStatus() {
        return TaxonUtils.getTaxonStatus(taxon);
      }

      @Override
      public boolean isChildOf(Taxon parent) {
        return TaxonUtils.isChildOf(parent, taxon);
      }

      @Override
      public Taxon getTaxon() {
        return taxon;
      }

      @Override
      public Taxonomy getTaxonomy() {
        return taxon.getTaxonomy();
      }
      
      @Override
      public Collection<Taxon> getTaxa() {
        return ImmutableSet.of(taxon);
      }

      @Override
      public SightingTaxon getSightingTaxon() {
        return SingleWithSubspeciesSightingTaxon.this;
      }
    }
   
    @Override
    public int hashCode() {
      return Objects.hashCode(taxonId.hashCode(), subspeciesId.hashCode());
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (!(obj instanceof SingleWithSubspeciesSightingTaxon)) {
        return false;
      }
      
      SingleWithSubspeciesSightingTaxon that = (SingleWithSubspeciesSightingTaxon) obj;
      return that.taxonId.equals(taxonId) && that.subspeciesId.equals(subspeciesId);
    }
    
    @Override
    public String toString() {
      return MoreObjects.toStringHelper(this)
          .add("taxonId", taxonId)
          .add("ssp", subspeciesId)
          .toString();
    }
  }
  
  /**
   * SightingTaxon implementation for a set of species.
   */
  static abstract class MultipleSightingTaxon implements SightingTaxon {

    private final ImmutableSortedSet<String> taxa;

    public MultipleSightingTaxon(Iterable<String> taxa) {
      this.taxa = ImmutableSortedSet.copyOf(taxa);
      if (this.taxa.size() < 2) {
        throw new IllegalStateException("Taxa list too small for multiple");
      }
    }

    @Override
    public String getId() {
      throw new IllegalStateException("Multiple taxa on " + this + ", may not call getId().");
    }

    @Override
    public String getSubIdentifier() {
      throw new IllegalStateException();
    }
    
    @Override
    public Collection<String> getIds() {
      return taxa;
    }

    @Override
    public boolean shouldBeDisplayedWith(String id) {
      return taxa.contains(id);
    }

    @Override
    public Resolved resolve(Taxonomy taxonomy) {
      return taxonomy.resolveInto(this);
    }

    @Override
    public Resolved resolveInternal(Taxonomy taxonomy) {
      List<Taxon> resolvedTaxa = Lists.newArrayListWithCapacity(taxa.size());
      for (String id : taxa) {
        Taxon taxon = taxonomy.getTaxon(id);
        if (taxon == null) {
          throw new IllegalStateException("Could not resolve " + id + " in " + taxonomy.getName());
        }
        resolvedTaxa.add(taxon);
      }
      return newResolved(resolvedTaxa);
    }
    
    abstract protected Resolved newResolved(List<Taxon> taxa);
    
    @Override
    public int hashCode() {
      return taxa.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (!(obj.getClass() == getClass())) {
        return false;
      }
      
      MultipleSightingTaxon that = (MultipleSightingTaxon) obj;
      return that.taxa.equals(taxa);
    }
    
    @Override
    public String toString() {
      return MoreObjects.toStringHelper(this)
          .add("taxa", taxa)
          .toString();
    }
  }
    
  /** Base class for Sp and Hybrid resolved. */
  static abstract class MultipleResolved extends BaseResolved {
    private ImmutableSortedSet<Taxon> resolvedTaxa;

    public MultipleResolved(Iterable<Taxon> resolvedTaxa) {
      this.resolvedTaxa = ImmutableSortedSet.copyOf(
          TaxonUtils.ORDERING, resolvedTaxa);
    }

    abstract protected SightingTaxon newSightingTaxon(Set<String> ids);
    abstract protected SightingTaxon.Resolved newResolved(
        Set<String> ids, Iterable<Taxon> taxa);
    abstract protected Joiner nameJoiner();

    @Override
    public SightingTaxon getParent() {
      // Get everything up to the largest level, and then pick the parent from there.
      Taxon.Type currentLevel = getLargestTaxonType();
      ImmutableSet.Builder<String> parents = ImmutableSet.builder();
      for (Taxon taxon : resolvedTaxa) {
        Taxon taxonAtLevel = TaxonUtils.getParentOfTypeOrNull(taxon, currentLevel);
        parents.add(taxonAtLevel.getParent().getId());
      }

      ImmutableSet<String> built = parents.build();
      if (built.size() == 1) {
        return new SingleSightingTaxon(Iterables.getOnlyElement(built));
      }
      return newSightingTaxon(built);
    }
    
    @Override
    public SightingTaxon getParentOfAtLeastType(Taxon.Type smallestType) {
      ImmutableSet.Builder<String> parents = ImmutableSet.builder();
      for (Taxon taxon : resolvedTaxa) {
        parents.add(TaxonUtils.getParentOfAtLeastType(taxon, smallestType).getId());
      }
      
      ImmutableSet<String> built = parents.build();
      if (built.size() == 1) {
        return new SingleSightingTaxon(Iterables.getOnlyElement(built));
      }
      return newSightingTaxon(built);
    }

    @Override
    public SightingTaxon.Resolved resolveParentOfType(Taxon.Type type) {
      if (type == getLargestTaxonType() && type == getSmallestTaxonType()) {
        return this;
      }
      
      ImmutableSet.Builder<Taxon> parents = ImmutableSet.builder();
      for (Taxon taxon : resolvedTaxa) {
        parents.add(TaxonUtils.getParentOfType(taxon, type));
      }
      ImmutableSet<Taxon> built = parents.build();
      if (built.size() == 1) {
        return SightingTaxons.newResolved(Iterables.getOnlyElement(built));
      } else {
        ImmutableSet.Builder<String> ids = ImmutableSet.builder();
        for (Taxon taxon : built) {
          ids.add(taxon.getId());
        }
        
        return newResolved(ids.build(), built);
      }
    }
    
    @Override
    public Taxon getTaxon() {
      throw new IllegalStateException("Not a single taxon");
    }

    @Override
    public Collection<Taxon> getTaxa() {
      return resolvedTaxa;
    }

    @Override
    public Taxonomy getTaxonomy() {
      return resolvedTaxa.iterator().next().getTaxonomy();
    }

    @Override
    public Taxon.Type getSmallestTaxonType() {
      Taxon.Type smallestType = Taxon.Type.classTaxon;
      for (Taxon taxon : resolvedTaxa) {
        Taxon.Type type = taxon.getType();
        if (type.compareTo(smallestType) < 0) {
          smallestType = type;
        }
      }
      return smallestType;
    }

    @Override
    public Taxon.Type getLargestTaxonType() {
      Taxon.Type largestType = Taxon.Type.subspecies;
      for (Taxon taxon : resolvedTaxa) {
        Taxon.Type type = taxon.getType();
        if (type.compareTo(largestType) > 0) {
          largestType = type;
        }
      }
      return largestType;
    }

    @Override
    public String getCommonName() {
      List<String> names = Lists.newArrayList();
      for (Taxon taxon : resolvedTaxa) {
        names.add(localNames().getCommonName(taxon));
      }

      switch (getLargestTaxonType()) {
        case species:
          return TaxonNames.joinFromEnd(names, nameJoiner());
        case group:
        case subspecies:
          return TaxonNames.joinFromStart(names, nameJoiner());
        default:
          throw new IllegalArgumentException();
      }
    }

    @Override
    public String getSimpleCommonName() {
      List<String> names = Lists.newArrayList();
      for (Taxon taxon : resolvedTaxa) {
        String simpleCommonName = localNames().getSimpleCommonName(taxon);
        // Append each name, but don't bother if it matches the previous one.
        // (Specifically, this cleans up cases where there's a sp. of several forms
        // but they're all subspecies of the same species).
        if (names.isEmpty() || !simpleCommonName.equals(names.get(names.size() - 1))) {
          names.add(simpleCommonName);
        }
      }

      switch (getLargestTaxonType()) {
        case species:
          return TaxonNames.joinFromEnd(names, nameJoiner());
        case group:
        case subspecies:
          return TaxonNames.joinFromStart(names, nameJoiner());
        default:
          throw new IllegalArgumentException();
      }
    }

    @Override
    public boolean hasCommonName() {
      for (Taxon taxon : resolvedTaxa) {
        if (taxon.getCommonName() == null) {
          return false;
        }
      }
      return true;
    }
    
    @Override
    public String getFullName() {
      List<String> names = Lists.newArrayList();
      for (Taxon taxon : resolvedTaxa) {
        names.add(TaxonUtils.getFullName(taxon));
      }
      return TaxonNames.joinFromStart(names, nameJoiner());
    }

    @Override
    public String getName() {
      // TODO: better algorithm for joining (but is this ever really called)?
      List<String> names = Lists.newArrayList();
      for (Taxon taxon : resolvedTaxa) {
        names.add(taxon.getName());
      }
      return nameJoiner().join(names);
    }

    @Override
    public boolean isChildOf(Taxon parent) {
      for (Taxon taxon : resolvedTaxa) {
        if (!TaxonUtils.isChildOf(parent, taxon)) { 
          return false;
        }
      }
      return true;
    }
    
    @Override
    public int hashCode() {
      return resolvedTaxa.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      
      if (obj == null) {
        return false;
      }
      
      if (obj.getClass() != getClass()) {
        return false;
      }
      
      MultipleResolved that = (MultipleResolved) obj;
      return that.resolvedTaxa.equals(resolvedTaxa);
    }
    
    @Override
    public String toString() {
      return MoreObjects.toStringHelper(this)
          .add("resolvedTaxa", resolvedTaxa)
          .toString();
    }
  }

  /** Implementation of SightingTaxon for sp.s */
  static final class SpSightingTaxon extends MultipleSightingTaxon {
    public SpSightingTaxon(Iterable<String> taxa) {
      super(taxa);
    }

    @Override
    public Type getType() {
      return Type.SP;
    }

    @Override
    protected Resolved newResolved(List<Taxon> taxa) {
      return new SpResolved(taxa);
    }

    private static final Joiner NAME_JOINER = Joiner.on('/');
    
    private class SpResolved extends MultipleResolved {

      public SpResolved(Iterable<Taxon> resolvedTaxa) {
        super(resolvedTaxa);
      }

      @Override
      public Type getType() {
        return Type.SP;
      }

      @Override
      public SightingTaxon getSightingTaxon() {
        return SpSightingTaxon.this;
      }

      @Override
      public Status getTaxonStatus() {
        Status status = null;
        for (Taxon taxon : getTaxa()) {
          Status taxonStatus = TaxonUtils.getTaxonStatus(taxon);
          if (status == null) {
            status = taxonStatus;
          } else {
            if (status.compareTo(taxonStatus) > 0) {
              status = taxonStatus;
            }
          }
        }
        return status;
      }

      @Override
      protected SightingTaxon newSightingTaxon(Set<String> ids) {
        return new SpSightingTaxon(ids);
      }

      @Override
      protected Resolved newResolved(Set<String> ids, Iterable<Taxon> taxa) {
        return new SpSightingTaxon(ids).new SpResolved(taxa);
      }

      @Override
      protected Joiner nameJoiner() {
        return NAME_JOINER;
      }
    }
  }

  /** Implementation of SightingTaxon for sp.s */
  static final class HybridSightingTaxon extends MultipleSightingTaxon {
    public HybridSightingTaxon(Iterable<String> taxa) {
      super(taxa);
    }

    @Override
    public Type getType() {
      return Type.HYBRID;
    }

    @Override
    protected Resolved newResolved(List<Taxon> taxa) {
      return new HybridResolved(taxa);
    }

    private static final Joiner NAME_JOINER = Joiner.on(" x ");
    
    private class HybridResolved extends MultipleResolved {

      public HybridResolved(Iterable<Taxon> resolvedTaxa) {
        super(resolvedTaxa);
      }

      @Override
      public Type getType() {
        return Type.HYBRID;
      }

      @Override
      public SightingTaxon getSightingTaxon() {
        return HybridSightingTaxon.this;
      }

      @Override
      protected SightingTaxon newSightingTaxon(Set<String> ids) {
        return new HybridSightingTaxon(ids);
      }

      @Override
      protected Resolved newResolved(Set<String> ids, Iterable<Taxon> taxa) {
        return new HybridSightingTaxon(ids).new HybridResolved(taxa);
      }

      @Override
      protected Joiner nameJoiner() {
        return NAME_JOINER;
      }

      @Override
      public Status getTaxonStatus() {
        return Status.LC;
      }
    }
  }

  abstract static class BaseResolved implements SightingTaxon.Resolved {
    @Override
    public int hashCode() {
      return getSightingTaxon().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (!(obj instanceof SightingTaxon.Resolved)) {
        return false;
      }
      SightingTaxon.Resolved that = (SightingTaxon.Resolved) obj;
      return getSightingTaxon().equals(that.getSightingTaxon())
          && getTaxonomy() == that.getTaxonomy();
    }
    
    @Override
    public String toString() {
      return MoreObjects.toStringHelper(this)
          .add("taxon", getSightingTaxon())
          .toString();
    }
    
    @Override
    public String getPreferredSingleName() {
      return localNames().isCommonNamePreferred() ? getCommonName() : getFullName();
    }
    
    @Override
    public String getPreferredSingleNameWithoutSpecies() {
      if (!getLargestTaxonType().isAtOrLowerLevelThan(Taxon.Type.group)) {
        return getPreferredSingleName();
      }
      
      String preferredName = getPreferredSingleName();
      Resolved speciesParent = getParentOfAtLeastType(Taxon.Type.species).resolveInternal(getTaxonomy());
      String speciesCommonName = speciesParent.getCommonName();
      if (preferredName.startsWith(speciesCommonName)) {
        return CharMatcher.whitespace().trimLeadingFrom(preferredName.substring(speciesCommonName.length()));
      }

      String speciesSciName = speciesParent.getFullName();
      if (preferredName.startsWith(speciesSciName)) {
        return CharMatcher.whitespace().trimLeadingFrom(preferredName.substring(speciesSciName.length()));
      }
      
      return preferredName;
    }
    
    protected LocalNames localNames() {
      return getTaxonomy().getLocalNames();
    }
  }
}

