/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import java.util.Map;

import org.joda.time.Duration;

import com.google.common.base.CharMatcher;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * Storage of information describing a single visit - keyed by (location/date/start-time).
 */
public class VisitInfo {
  public enum VisitField {
    START_TIME, // not actually a VisitInfo field - it's part of the VisitInfoKey
    DURATION,
    DISTANCE,
    AREA,
    PARTY_SIZE,
    COMPLETE_CHECKLIST
  }
  
  /**
   * I don't entirely trust the ebird names, as they have a great deal of arbitrary
   * constants in them, and vary from one download form to another.  Attempt to normalize
   * by stripping a bunch of characters, converting to lowercase, and dropping "ebird"
   * from the start.
   */
  private static final CharMatcher LETTERS_TO_STRIP_FROM_EBIRD_NAMES = CharMatcher.anyOf(" -");

  public enum ObservationType {
    INCIDENTAL("Incidental", "incidental",
        ImmutableSet.<VisitField>of(),
        ImmutableSet.of(
            VisitField.DISTANCE,
            VisitField.DURATION,
            VisitField.AREA,
            VisitField.COMPLETE_CHECKLIST),
        "Casual",
        "eBird - Casual Observation",
        "Zufällig",
        "Ustekabekoa",
        "Observation informelle",
        "Casuale",
        "אקראית",
        "偶発的",
        "Tilfeldig",
        "Observação Casual",
        "Случайное наблюдение",
        "Tesadüfi",
        "Випадкове",
        "附帶紀錄",
        "偶然记录"),
    TRAVELING("Traveling", "traveling",
        ImmutableSet.of(
            VisitField.START_TIME,
            VisitField.DURATION,
            VisitField.DISTANCE,
            VisitField.PARTY_SIZE),
        ImmutableSet.of(
            VisitField.AREA),
        "Trasa",
        "Strecke",
        "Con Desplazamiento",
        "Mugitzen",
        "Relevé en mouvement ou transect",
        "Itinerante",
        "ניידת",
        "移動",
        "Turbasert",
        "Percurso",
        "Contagem de Percurso",
        "Наблюдение во время передвижения",
        "Seyyar",
        "Маршрутне",
        "行進計數",
        "行进中观鸟",
        "eBird - Traveling Count"),
    STATIONARY("Stationary", "stationary",
        ImmutableSet.of(
            VisitField.START_TIME,
            VisitField.DURATION,
            VisitField.PARTY_SIZE),
        ImmutableSet.of(
            VisitField.DISTANCE,
            VisitField.AREA),
        "eBird - Stationary Count",
        "Statické",
        "Stationär",
        "Estacionario",
        "Geldirik",
        "Relevé ponctuel ou stationnaire",
        "Stazionaria",
        "נייחת",
        "定点",
        "Stasjonær",
        "Estacionária",
        "Contagem Estacionária",
        "Наблюдение из одной точки",
        "Sabit",
        "Точкове",
        "定點計數",
        "定点观鸟"),
    HISTORICAL("Historical", "historical",
        ImmutableSet.<VisitField>of(),
        ImmutableSet.<VisitField>of(),
        "Historické",
        "Historisch",
        "Historikoa",
        "Historique",
        "Storico",
        "היסטורי",
        "過去の",
        "Historisk",
        "Histórico",
        "История",
        "Sandıktan",
        "Історичне",
        "歷史資料",
        "历史记录"),
    AREA("Area", "area",
        ImmutableSet.of(
            VisitField.START_TIME,
            VisitField.DURATION,
            VisitField.AREA,
            VisitField.PARTY_SIZE),
        ImmutableSet.of(
            VisitField.DISTANCE),
        "eBird - Exhaustive Area Count"),
    BANDING("Banding", "banding",
        ImmutableSet.of(
            VisitField.START_TIME,
            VisitField.DURATION,
            VisitField.PARTY_SIZE),
        ImmutableSet.of(
            VisitField.DISTANCE),
        "PriMig - Pri Mig Banding Protocol",
        "Pri Mig Banding Protocol",
        "Banding Protocol"),
    PELAGIC_PROTOCOL("eBird Pelagic Protocol", "pelagic",
        ImmutableSet.of(
            VisitField.START_TIME,
            VisitField.DURATION,
            VisitField.DISTANCE,
            VisitField.PARTY_SIZE),
        ImmutableSet.of(
            VisitField.AREA)),
    NOCTURNAL_BIRDING("Nocturnal Birding", "nocturnal",
        ImmutableSet.of(
            VisitField.START_TIME,
            VisitField.DURATION,
            VisitField.DISTANCE),
        ImmutableSet.of(
            VisitField.AREA),
        "Nocturnal Count"),
    NOCTURNAL_FLIGHT_CALL_COUNT("Nocturnal Flight Call Count", "flight_call",
        ImmutableSet.of(
            VisitField.START_TIME,
            VisitField.DURATION,
            VisitField.PARTY_SIZE),
        ImmutableSet.of(
            VisitField.AREA,
            VisitField.DISTANCE),
        "eBird--Nocturnal Flight Call Count"),
    // Oiled birds have a lot of extra fields.  Consider supporting in the future.
    // OILED_BIRDS("Oiled Birds"),
    RANDOM("Random", "random",
        ImmutableSet.of(
            VisitField.START_TIME,
            VisitField.DURATION,
            VisitField.DISTANCE,
            VisitField.PARTY_SIZE),
        ImmutableSet.of(
            VisitField.AREA),
        "eBird Random Location Count");
    
    private final String text;
    private final String id;
    private final ImmutableSet<VisitField> requiredFields;
    private final ImmutableSet<VisitField> hiddenFields;
    private final ImmutableSet<String> otherEBirdNames;
    private static final ImmutableMap<String, ObservationType> ID_MAP;
    private static final ImmutableMap<String, ObservationType> EBIRD_ID_MAP;
    private static final ImmutableMap<String, ObservationType> NAMES_MAP;
    static {
      ImmutableMap.Builder<String, ObservationType> builder = ImmutableMap.builder();
      ImmutableMap.Builder<String, ObservationType> ebirdBuilder = ImmutableMap.builder();
      Map<String, ObservationType> namesMap = Maps.newHashMap();
      for (ObservationType observationType : values()) {
        builder.put(observationType.id(), observationType);
        ebirdBuilder.put(observationType.eBirdId(), observationType);
        namesMap.put(normalizeName(observationType.toString()), observationType);
        for (String name : observationType.otherEBirdNames) {
          namesMap.put(normalizeName(name), observationType);
        }
      }
      ID_MAP = builder.build();
      EBIRD_ID_MAP = ebirdBuilder.build();
      NAMES_MAP = ImmutableMap.copyOf(namesMap);
    }
    
    ObservationType(
        String text,
        String id,
        ImmutableSet<VisitField> requiredFields,
        ImmutableSet<VisitField> hiddenFields,
        String... otherEBirdNames) {
      this.text = text;
      this.id = id;
      Preconditions.checkState(Sets.intersection(requiredFields, hiddenFields).isEmpty());
      this.requiredFields = requiredFields;
      this.hiddenFields = hiddenFields;
      this.otherEBirdNames = ImmutableSet.copyOf(otherEBirdNames);
    }
    
    private static String normalizeName(String string) {
      string = LETTERS_TO_STRIP_FROM_EBIRD_NAMES.removeFrom(string.toLowerCase());
      if (string.startsWith("ebird")) {
        string = string.substring(5);
      }
      return string;
    }

    @Override
    public String toString() {
      return text;
    }

    public ImmutableSet<VisitField> getHiddenFields() {
      return hiddenFields;
    }

    public ImmutableSet<VisitField> getRequiredFields() {
      return requiredFields;
    }

    /**
     * Return the ID for persistent storage.
     */
    public String id() {
      return id;
    }

    /** Return the ID for eBird exports. */
    public String eBirdId() {
      // For now, just return the ID - but should eBird ever change
      // its format, support the difference
      return id();
    }

    public static ObservationType fromId(String id) {
      return ID_MAP.get(id);
    }

    public static ObservationType fromEBirdId(String ebirdId) {
      return EBIRD_ID_MAP.get(ebirdId);
    }

    public static ObservationType fromName(String name) {
      return NAMES_MAP.get(normalizeName(name));
    }

  }

  private final ObservationType observationType;
  private final Area area;
  private final Duration duration;
  private final Distance distance;
  private final Integer partySize;
  private final boolean completeChecklist;
  private final String comments;
  
  private VisitInfo(Builder builder) {
    Preconditions.checkState(builder.observationType != null, "No observationType");
    observationType = builder.observationType;
    
    ImmutableSet<VisitField> requiredFields = observationType.getRequiredFields();
    ImmutableSet<VisitField> hiddenFields = observationType.getHiddenFields();
    
    if (hiddenFields.contains(VisitField.AREA)) {
      area = null;
    } else {
      if (builder.area == null) {
        Preconditions.checkState(!requiredFields.contains(VisitField.AREA),
            "Observation type %s requires an area", observationType);
      }
      area = builder.area;
    }

    if (hiddenFields.contains(VisitField.DURATION)) {
      duration = null;
    } else {
      if (builder.duration == null) {
        Preconditions.checkState(!requiredFields.contains(VisitField.DURATION),
            "Observation type %s requires a duration", observationType);
      }
      duration = builder.duration;
    }

    if (hiddenFields.contains(VisitField.DISTANCE)) {
      distance = null;
    } else {
      if (builder.distance == null) {
        Preconditions.checkState(!requiredFields.contains(VisitField.DISTANCE),
            "Observation type %s requires a distance", observationType);
      }
      distance = builder.distance;
    }

    if (hiddenFields.contains(VisitField.PARTY_SIZE)) {
      partySize = null;
    } else {
      if (builder.partySize == null) {
        Preconditions.checkState(!requiredFields.contains(VisitField.PARTY_SIZE),
            "Observation type %s requires a party size", observationType);
      }
      partySize = builder.partySize;
    }
    
    completeChecklist = builder.completeChecklist;
    comments = Strings.emptyToNull(builder.comments);
  }

  public ObservationType observationType() {
    return observationType;
  }
  
  public Optional<Duration> duration() {
    return Optional.fromNullable(duration);
  }
  
  public Optional<Distance> distance() {
    return Optional.fromNullable(distance);
  }
  
  public Optional<Area> area() {
    return Optional.fromNullable(area);
  }
  
  public Optional<Integer> partySize() {
    return Optional.fromNullable(partySize);
  }
  
  public Optional<String> comments() {
    return Optional.fromNullable(comments);
  }
  
  public boolean completeChecklist() {
    return completeChecklist;
  }
  
  public Builder asBuilder() {
    Builder builder = builder().withObservationType(observationType);
    if (duration != null) {
      builder.withDuration(duration);
    }
    if (distance != null) {
      builder.withDistance(distance);
    }
    if (area != null) {
      builder.withArea(area);
    }
    if (partySize != null) {
      builder.withPartySize(partySize);
    }
    if (comments != null) {
      builder.withComments(comments);
    }
    if (completeChecklist) {
      builder.withCompleteChecklist(true);
    }
    return builder;
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  public static class Builder {
    private String comments;
    private boolean completeChecklist;
    private Integer partySize;
    private Area area;
    private Distance distance;
    private Duration duration;
    private ObservationType observationType;

    private Builder() {}
    
    public Builder withObservationType(ObservationType observationType) {
      this.observationType = observationType;
      return this;
    }
    
    public Builder withArea(Area area) {
      this.area = Preconditions.checkNotNull(area);
      return this;
    }
    
    public Builder withDistance(Distance distance) {
      this.distance = distance;
      return this;
    }
    
    public Builder withDuration(Duration duration) {
      this.duration = duration;
      return this;
    }
    
    public Builder withComments(String comments) {
      this.comments = Strings.emptyToNull(comments);
      return this;
    }
    
    public Builder withCompleteChecklist(boolean completeChecklist) {
      this.completeChecklist = completeChecklist;
      return this;
    }
    
    public Builder withPartySize(int partySize) {
      Preconditions.checkState(partySize > 0);
      this.partySize = partySize;
      return this;
    }

    /**
     * Builds the VisitInfo. 
     * @throws IllegalStateException if the visit info is not in a legal
     * state (required fields are missing)
     */
    public VisitInfo build() {
      return new VisitInfo(this);
    }
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("observationType", observationType)
        .add("area", area)
        .add("duration", duration)
        .add("distance", distance)
        .add("partySize", partySize)
        .add("completeChecklist", completeChecklist)
        .add("comments", comments)
        .toString();
  }

  /** Returns true if there is interesting data (beyond just the observation type) */
  public boolean hasData() {
    return area != null
        || distance != null
        || duration != null
        || comments != null
        || partySize != null
        || completeChecklist
        // Need to save incidental observation types explicitly.  Scythebill
        // defaults to historical, so without this we drop that observation
        // type on the ground.
        || observationType == ObservationType.INCIDENTAL;
  }
  
  @Override
  public int hashCode() {
    return Objects.hashCode(area, comments, completeChecklist, distance, duration, observationType, partySize);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof VisitInfo)) {
      return false;
    }
    VisitInfo that = (VisitInfo) obj;
    return Objects.equal(area, that.area)
        && Objects.equal(comments, that.comments)
        && (completeChecklist == that.completeChecklist) 
        && Objects.equal(distance, that.distance)
        && Objects.equal(duration, that.duration)
        && Objects.equal(observationType, that.observationType)
        && Objects.equal(partySize, that.partySize);
  }
}
