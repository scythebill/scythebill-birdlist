/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import javax.annotation.Nullable;

import org.joda.time.LocalTime;
import org.joda.time.ReadablePartial;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.io.TimeIO;

public final class VisitInfoKey {
  private final String locationId;
  private final ReadablePartial date;
  private final LocalTime startTime;

  public VisitInfoKey(String locationId, ReadablePartial date, @Nullable LocalTime startTime) {
    this.locationId = Preconditions.checkNotNull(locationId);
    this.date = Preconditions.checkNotNull(date);
    this.startTime = startTime;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }
    
    if (!(obj instanceof VisitInfoKey)) {
      return false;
    }
    
    VisitInfoKey that = (VisitInfoKey) obj;
    return locationId.equals(that.locationId)
        && date.equals(that.date)
        && Objects.equal(startTime, that.startTime);
  }

  @Override
  public int hashCode() {
    return (31 * 
        ((31 * locationId.hashCode()) + date.hashCode()))
        + (startTime == null ? 0 : startTime.hashCode());
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("locationId", locationId)
        .add("date", PartialIO.toString(date))
        .add("startTime", startTime == null ? "null" : TimeIO.toString(startTime))
        .toString();
  }

  public String locationId() {
    return locationId;
  }

  public ReadablePartial date() {
    return date;
  }

  public Optional<LocalTime> startTime() {
    return Optional.fromNullable(startTime);
  }

  public static VisitInfoKey forSighting(Sighting sighting) {
    if (sighting.getLocationId() == null
        || sighting.getDateAsPartial() == null) {
      return null;
    }
    
    return new VisitInfoKey(sighting.getLocationId(), sighting.getDateAsPartial(), sighting.getTimeAsPartial());
  }

  /**
   * Returns true if a sighting matches this visit info key.
   */
  public boolean matches(Sighting sighting) {
    if (!Objects.equal(sighting.getLocationId(), locationId)) {
      return false;
    }

    if (!Objects.equal(sighting.getDateAsPartial(), date)) {
      return false;
    }

    if (!Objects.equal(sighting.getTimeAsPartial(), startTime)) {
      return false;
    }
    
    return true;
  }

  public VisitInfoKey withLocationId(String newLocationId) {
    return new VisitInfoKey(newLocationId, date, startTime);
  }

  public VisitInfoKey withDate(ReadablePartial newDate) {
    return new VisitInfoKey(locationId, newDate, startTime);
  }

  public VisitInfoKey withStartTime(Optional<LocalTime> newStartTime) {
    return new VisitInfoKey(locationId, date, newStartTime.orNull());
  }
}
