/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import com.google.common.collect.Ordering;
import com.scythebill.birdlist.model.query.SightingComparators;

/**
 * Ordering for {@link VisitInfoKey}s.  Ordering is by ascending date and time,
 * and lexicographically by location IDs (the latter probably needing to
 * be revisited in the future).
 */
public final class VisitInfoKeyOrdering extends Ordering<VisitInfoKey> {
  @Override
  public int compare(VisitInfoKey left, VisitInfoKey right) {
    int compareDates = SightingComparators.comparePartials(left.date(), right.date());
    if (compareDates != 0) {
      return compareDates;
    }
    
    if (left.startTime().isPresent()) {
      if (right.startTime().isPresent()) {
        int compareTimes = SightingComparators.comparePartials(left.startTime().get(), right.startTime().get());
        if (compareTimes != 0) {
          return compareTimes;
        }
      } else {
        // Left has start time, right does not
        return 1;
      }
    } else if (right.startTime().isPresent()) {
      // Right has start time, left does not
      return -1;
    }
    
    return left.locationId().compareTo(right.locationId());
  }

}
