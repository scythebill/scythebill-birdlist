/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.edits;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.annotations.SerializeAsJson;
import com.scythebill.birdlist.model.user.User;
import com.scythebill.birdlist.model.user.UserSet;

/**
 * Preferences for a set of chosen users (for use in data entry).
 */
@SerializeAsJson
public class ChosenUsers {
  // ImmutableSet would be better... but not GSON compatible.
  private LinkedHashSet<String> userIds = new LinkedHashSet<>();

  @Inject
  public ChosenUsers() {
  }
  
  public ImmutableSet<User> getUsers(UserSet userSet) {
    if (userSet == null) {
      return ImmutableSet.of();
    }
    return userIds.stream().map(userSet::userById).collect(ImmutableSet.toImmutableSet());
  }

  public void setUsers(Collection<User> users) {
    userIds = users.stream().map(User::id).collect(Collectors.toCollection(LinkedHashSet::new));
  }
}
