/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.edits;

import java.util.List;

import javax.annotation.Nullable;

import org.joda.time.LocalTime;
import org.joda.time.ReadablePartial;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.annotations.SerializeAsJson;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.io.TimeIO;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.sighting.VisitInfoKeyOrdering;

/**
 * A list of recent edits.
 */
@SerializeAsJson
public class RecentEdits {
  private static final int MAX_RECENT_EDITS = 10;
  private List<RecentEdit> recentEdits;

  public RecentEdits() {
    this.recentEdits = Lists.newArrayList();
  }
  
  RecentEdits(Iterable<RecentEdit> recentEdits) {
    this.recentEdits = Lists.newArrayList(recentEdits);
  }

  /** Find the last N edits in a ReportSet. */
  public static List<RecentEdit> getLastEdits(
      ReportSet reportSet, int count) {
    List<RecentEdit> recentEdits = Lists.newArrayList();
    RecentEdit lastEdit = null;
    for (Sighting sighting : Lists.reverse(reportSet.getSightings())) {
      ReadablePartial date = sighting.getDateAsPartial();
      String locationId = sighting.getLocationId();
      // Ignore any sightings that are missing either a date or a location.
      if (locationId == null || date == null) {
        continue;
      }

      ReadablePartial startTime = sighting.getTimeAsPartial();
      String dateAsString = PartialIO.toString(date);
      String timeAsString = startTime == null ? null : TimeIO.toString(startTime);
      
      if (lastEdit != null) {
        if (Objects.equal(dateAsString, lastEdit.date)
            && Objects.equal(locationId, lastEdit.locationId)
            && Objects.equal(timeAsString, lastEdit.startTime)) {
          continue;
        }
      }
      
      lastEdit = new RecentEdit(locationId, dateAsString, timeAsString);
      if (!recentEdits.contains(lastEdit)) {
        recentEdits.add(lastEdit);
        if (recentEdits.size() == count) {
          return recentEdits;
        }
      }
    }
    return recentEdits;
  }
  
  public static class RecentEdit {
    private final String locationId;
    private final String date;
    private final String startTime;
    
    RecentEdit(String locationId, String date, String startTime) {
      this.locationId = locationId;
      this.date = date;
      this.startTime = startTime;
    }
    
    RecentEdit(String locationId, ReadablePartial partial, ReadablePartial startTime) {
      this(locationId, PartialIO.toString(partial), startTime == null ? null : TimeIO.toString(startTime));
    }
    
    public String getLocationId() {
      return locationId;
    }
    
    public ReadablePartial getDate() {
      return PartialIO.fromString(date);
    }

    @Nullable
    public LocalTime getStartTime() {
      return startTime == null ? null : TimeIO.fromString(startTime);
    }

    @Override
    public int hashCode() {
      return Objects.hashCode(locationId, date, startTime);
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (!(o instanceof RecentEdit)) {
        return false;
      }
      
      RecentEdit that = (RecentEdit) o;
      return Objects.equal(locationId, that.locationId)
          && Objects.equal(date, that.date)
          && Objects.equal(startTime, that.startTime);
    }
  }

  /** Returns a modifiable copy of the recent edits list. */
  public List<RecentEdit> getRecentEditsList() {
    return Lists.newArrayList(recentEdits);
  }
  
  public void add(String locationId, ReadablePartial date, @Nullable ReadablePartial startTime) {
    RecentEdit recentEdit = new RecentEdit(locationId, date, startTime);
    int alreadyPresentIndex = recentEdits.indexOf(recentEdit);
    if (alreadyPresentIndex < 0) {
      // Not found in the list.  Add to the front, and remove from the end if the list
      // is now too large
      recentEdits.add(0, recentEdit);
      while (recentEdits.size() > MAX_RECENT_EDITS) {
        recentEdits.remove(MAX_RECENT_EDITS);
      }
    } else if (alreadyPresentIndex != 0) {
      // Already in the list, but not at the front;  just move it to the front.
      recentEdits.remove(alreadyPresentIndex);
      recentEdits.add(0, recentEdit);
    }
  }

  /** Add at most {@code MAX_RECENT_EDITS} visits. */
  public void add(Iterable<VisitInfoKey> visitInfoKeys) {
    // Sort the visit infos so we use the most recent first.
    List<VisitInfoKey> mostRecentFirst = new VisitInfoKeyOrdering().reverse().sortedCopy(visitInfoKeys);
    // ... then, only use MAX_RECENT_EDITS from the front - but add *those* in reverse order 
    List<VisitInfoKey> trimmedSizeThenLastFirst = new VisitInfoKeyOrdering().sortedCopy(
        Iterables.limit(mostRecentFirst, MAX_RECENT_EDITS));
    for (VisitInfoKey visitInfoKey : trimmedSizeThenLastFirst) {
      add(visitInfoKey); 
    }
  }

  public void add(VisitInfoKey visitInfoKey) {
    add(visitInfoKey.locationId(), visitInfoKey.date(), visitInfoKey.startTime().orNull());
  }
}
