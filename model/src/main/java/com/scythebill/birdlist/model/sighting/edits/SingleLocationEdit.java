/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.edits;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.joda.time.LocalTime;
import org.joda.time.Partial;
import org.joda.time.ReadablePartial;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.user.User;
import com.scythebill.birdlist.model.util.Dirty;
import com.scythebill.birdlist.model.util.DirtyImpl;
import com.scythebill.birdlist.model.util.ResolvedComparator;

/**
 * Deltas to a report set with a list of sightings at a single place and time.
 */
public class SingleLocationEdit {
  private ReadablePartial date;
  private Location location;
  private List<Resolved> taxa = Lists.newArrayList();
  private Map<Resolved, SightingInfo> sightings = Maps.newLinkedHashMap();
  private String locationName;
  private ImmutableSet<Sighting> existingSightings;
  private DirtyImpl dirty = new DirtyImpl(false);
  private LocalTime time;
  private VisitInfo visitInfo;
  private VisitInfoKey snapshottedVisitInfoKey;
  private boolean completeChecklist;
  private ImmutableSet<User> users;
  private ImmutableSet<User> usersOnSpeciesList;

  public SingleLocationEdit(ReadablePartial date, Collection<User> defaultUsers) {
    this.date = date;
    this.users = ImmutableSet.copyOf(defaultUsers);
  }

  public void apply(ReportSet reportSet, Taxonomy currentTaxonomy) {
    Preconditions.checkNotNull(location, "No location set");
    LocationSet locations = reportSet.getLocations();
    if (location.getId() == null) {
      locations.ensureAdded(location);
    }

    List<Sighting> newSightings = Lists.newArrayList();
    for (Resolved taxon : taxa) {
      SightingTaxon sightingTaxon = taxon.getSightingTaxon();
      SightingInfo info = sightings.get(taxon);
      
      Taxonomy taxonomy = taxon.getTaxonomy();
      if (taxonomy instanceof MappedTaxonomy) {
        MappedTaxonomy mappedTaxonomy = (MappedTaxonomy) taxonomy;
        sightingTaxon = mappedTaxonomy.getMapping(sightingTaxon);
      }
      
      Sighting.Builder builder = Sighting.newBuilder()
          .setTaxonomy(TaxonUtils.getBaseTaxonomy(taxonomy))
          .setLocation(location)
          .setTaxon(sightingTaxon);
      if (date != null) {
        builder.setDate(new Partial(date));
      }

      if (time != null) {
        builder.setTime(time);
      }
      
      if (info != null) {
        builder.setSightingInfo(info);
      }

      newSightings.add(builder.build());
    }
    
    reportSet.mutator()
        .removing(existingSightings)
        .adding(newSightings)
        .mutate();

    if (visitInfo != null) {
      VisitInfoKey visitInfoKey = new VisitInfoKey(location.getId(), date, time);
      // Merge in the "completeChecklist" state
      VisitInfo visitInfoToStore = visitInfo;
      if (completeChecklist) {
        visitInfoToStore = visitInfoToStore.asBuilder().withCompleteChecklist(true).build();
      }
      // If the visit info data is basically meaningless, drop it all 
      if (!visitInfoToStore.hasData()) {
        reportSet.removeVisitInfo(visitInfoKey);
      } else {
        reportSet.putVisitInfo(visitInfoKey, visitInfoToStore);
      }
    }
   
    dirty.setDirty(false);
    snapshotExistingSightings(reportSet, currentTaxonomy);
    // TODO: clear the list of taxa?  Only allow apply once?
  }

  public ReadablePartial getDate() {
    return date;
  }

  public void setDate(ReadablePartial date) {
    this.date = date;
  }

  public LocalTime getTime() {
    return time;
  }

  public void setTime(LocalTime time) {
    this.time = time;
  }

  public void setUsers(Collection<User> users) {
    this.users = ImmutableSet.copyOf(users);
  }
  
  public ImmutableSet<User> getUsers() {
    return users;
  }

  /** Sets the "users" as they were last encountered on the species list panel. */
  public void setUsersOnSpeciesList(Collection<User> users) {
    this.usersOnSpeciesList = users == null ? null : ImmutableSet.copyOf(users);
  }
  
  public ImmutableSet<User> getUsersOnSpeciesList() {
    return usersOnSpeciesList;
  }

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  /** Store a location name.  Only relevant if location == null. */
  public void setLocationName(String locationName) {
    this.locationName = locationName;
  }

  public String getLocationName() {
    return locationName;
  }

  public List<Resolved> taxa() {
    return Collections.unmodifiableList(taxa);
  }

  public SightingInfo getSightingInfo(Resolved resolved) {
    SightingInfo sighting = sightings.get(resolved);
    if (sighting == null) {
      sighting = new SightingInfo();
      // Default the set of users for each sighting to the base,
      // though it can be edited.   
      if (users != null && !users.isEmpty()) {
        sighting.setUsers(users);
      }
      sightings.put(resolved, sighting);
    }

    return sighting;
  }

  public void swapTaxa(Resolved oldTaxon, Resolved newTaxon) {
    // An oddity: mutableTaxa() is only updated on exit, so this swap
    // only needs to mess with sightings
    if (sightings.containsKey(oldTaxon)) {
      SightingInfo info = sightings.remove(oldTaxon);
      sightings.put(newTaxon, info);
    }
    edited();
  }
  
  /**
   * Copy the SightingInfo from the previous to the updated taxonomy, and update
   * the list of taxa (trusting the new list determined by the caller).
   */
  public void setTaxonomy(Taxonomy updated, Iterable<Resolved> updatedTaxa) {
    // Add all the updated taxa
    List<Resolved> newTaxa = Lists.newArrayList(updatedTaxa);
    Map<Resolved, SightingInfo> newSightings = Maps.newLinkedHashMap();
    for (Resolved previousResolved : taxa) {
      SightingInfo previousSighting = getSightingInfo(previousResolved);
      // Incompatible taxonomies:  simply preserve the old resolved and its sighting info
      if (!TaxonUtils.areCompatible(previousResolved.getTaxonomy(), updated)) {
        newSightings.put(previousResolved, previousSighting);
        // ... and make sure we store the previous taxon
        newTaxa.add(previousResolved);
      } else {
        Taxonomy previous = previousResolved.getTaxonomy();
        if (updated == previous) {
          newSightings.put(previousResolved, previousSighting);
        } else if (updated instanceof MappedTaxonomy) {
          MappedTaxonomy mapped = (MappedTaxonomy) updated;
          Preconditions.checkState(previous == mapped.getBaseTaxonomy());
          Resolved resolved = mapped.resolveInto(previousResolved.getSightingTaxon());
          if (resolved != null) {
            newSightings.put(resolved, previousSighting);
          }
        } else {
          MappedTaxonomy mapped = (MappedTaxonomy) previous;
          SightingTaxon baseMapping = mapped.getMapping(previousResolved.getSightingTaxon());
          Resolved resolved = updated.resolveInto(baseMapping);
          if (baseMapping != null) {
            newSightings.put(resolved, previousSighting);
          }
        }
      }
    }
    taxa = newTaxa;
    sightings = newSightings;
  }

  public void setVisitInfo(VisitInfo visitInfo) {
    this.visitInfo = visitInfo;
  }

  public VisitInfo getVisitInfo() {
    return this.visitInfo;
  }

  public void setLocationText(String whereText) {
    this.setLocationName(whereText);
  }
  
  public Dirty getDirty() {
    return dirty;
  }
  
  public enum ExistingSightingSnapshotState {
    NONE,
    NO_CHANGE,
    DATE_CHANGED,
    DATE_AND_LOCATION_CHANGED,
    LOCATION_CHANGED,
    TIME_CHANGED,
    DATE_AND_TIME_CHANGED,
    DATE_AND_AND_TIME_AND_LOCATION_CHANGED,
    TIME_AND_LOCATION_CHANGED,
  }
  
  /**
   * Returns the current state of the edit, relative to any existing snapshot of data.
   * Ignores whether additional sightings are added or sighting info changed (that's
   * handled by {@link #isDirty()}, but does account for whether a non-empty snapshot has been
   * taken, and if so, whether the date and location is now changed.
   */
  public ExistingSightingSnapshotState getSnapshotState() {
    if (existingSightings == null || existingSightings.isEmpty()) {
      return ExistingSightingSnapshotState.NONE;
    }
    
    Sighting sample = existingSightings.iterator().next();    
    boolean dateHasChanged = !Objects.equal(date, sample.getDateAsPartial());
    boolean timeHasChanged = !Objects.equal(time, sample.getTimeAsPartial());
    String locationId = location == null ? null : location.getId();
    
    if (Objects.equal(locationId, sample.getLocationId())) {
      if (timeHasChanged) {
        return dateHasChanged ? ExistingSightingSnapshotState.DATE_AND_TIME_CHANGED : ExistingSightingSnapshotState.TIME_CHANGED;
      } else {
        return dateHasChanged ? ExistingSightingSnapshotState.DATE_CHANGED : ExistingSightingSnapshotState.NO_CHANGE;
      }
    } else {
      if (timeHasChanged) {
        return dateHasChanged ? ExistingSightingSnapshotState.DATE_AND_AND_TIME_AND_LOCATION_CHANGED: ExistingSightingSnapshotState.TIME_AND_LOCATION_CHANGED;
      } else {
        return dateHasChanged ? ExistingSightingSnapshotState.DATE_AND_LOCATION_CHANGED : ExistingSightingSnapshotState.LOCATION_CHANGED;
      }
    }
  }
  
  public void clearSnapshot() {
    taxa.clear();
    sightings.clear();
    setVisitInfo(null);
    snapshottedVisitInfoKey = null;
  }
  
  /**
   * Takes a snapshot of existing sightings.  This will entirely erase any user-entered
   * sightings or taxa, so it should not be called after the user has entered any data
   * without confirming with the user.
   */
  public void snapshotExistingSightings(ReportSet reportSet, Taxonomy taxonomy) {
    String locationId = location == null ? null : location.getId();
    
    taxa.clear();
    sightings.clear();
    
    boolean hasVisitInfo = visitInfo != null && visitInfo.hasData();
    // If the visit info was loaded from storage, and we're re-snapshotting, drop
    // the snapshotted visit info if it hasn't been edited
    // TODO: should "visitInfo == null" comparisons here and below also check 
    // visitInfo.hasData()?  Sequence to test:
    // - Enter a visit with visit info
    // - Start entering a new sighting, and go to the visit info page;  enter
    //   an observationType but nothing more
    // - Go back, and switch to the previous visit.  Now go forward - you should
    // see the visit info from that previous visit.
    if (snapshottedVisitInfoKey != null
        && hasVisitInfo
        && visitInfo.equals(reportSet.getVisitInfo(snapshottedVisitInfoKey))) {
      // This is ... an odd choice to do here.  It's *probably* something that would have
      // been better done in SingleLocationWhenAndWherePanel, which has a better notion
      // for when the visit info should probably be cleared out.
      setVisitInfo(null);
      completeChecklist = false;
    }
    
    // Set of sightings that precisely match the current location/date/time
    ImmutableSet.Builder<Sighting> existingSightings = ImmutableSet.builder();
    // Set of sightings that precisely match the current location/date/time, *except*
    // they're missing a time.  Used to support adding a starting time to an existing
    // batch of sightings.
    ImmutableSet.Builder<Sighting> existingSightingsWithoutTime = ImmutableSet.builder();

    if (locationId != null) {
      // Snapshot visit info, if possible (there's a location and date)
      if (date != null) {
        VisitInfoKey visitInfoKey = new VisitInfoKey(locationId, date, time);
        if (!hasVisitInfo) {
          setVisitInfo(reportSet.getVisitInfo(visitInfoKey));
          if (visitInfo != null) {
            completeChecklist = visitInfo.completeChecklist();
          }
        }
        // Store the key used to retrieve the visit info.  This makes it clear
        // that the current visit info was retrieved from storage, not newly entered
        snapshottedVisitInfoKey = visitInfoKey;
      }
      
      for (Sighting sighting : reportSet.getSightings()) {
        if (locationId.equals(sighting.getLocationId())
            && Objects.equal(sighting.getDateAsPartial(), date)) {
          if (Objects.equal(sighting.getTimeAsPartial(), time)) {
            // Full match
            existingSightings.add(sighting);
          } else if (sighting.getTimeAsPartial() == null) {
            // Match except the existing sighting has no time
            existingSightingsWithoutTime.add(sighting);
          }
        }
      }
    }

    // Snapshot the sightings that match completely...
    this.existingSightings = existingSightings.build();
    if (this.existingSightings.isEmpty()) {
      // ... unless there aren't any, in which case snapshot the ones
      // that are just missing a time.
      this.existingSightings = existingSightingsWithoutTime.build();
    }

    // And add all the taxa (sorted) into the list, and grab a copy of the existing sighting info
    TreeSet<Resolved> sortedResolved = Sets.newTreeSet(new ResolvedComparator());
    for (Sighting existingSighting : this.existingSightings) {
      // Resolve using the current taxonomy if compatible, the sighting's taxonomy if not
      Taxonomy sightingTaxonomy = taxonomy;
      if (!TaxonUtils.areCompatible(taxonomy, existingSighting.getTaxonomy())) {
        sightingTaxonomy = existingSighting.getTaxonomy();
      }
      Resolved resolved = existingSighting.getTaxon().resolve(sightingTaxonomy);
      
      sortedResolved.add(resolved);
      // Store the taxon against a cloned sighting (sighting info is mutable - ugh), so
      // edits don't immediately mutate storage.
      sightings.put(resolved, existingSighting.getSightingInfo().clone());
    }
    
    taxa.addAll(sortedResolved);
  }

  /** Mark the model as edited. */
  public void edited() {
    dirty.setDirty(true);
  }

  /**
   * Returns all the taxa entered that are compatible with a given taxonomy.
   */
  public List<Resolved> getCompatibleTaxa(Taxonomy taxonomy) {
    List<Resolved> compatibleTaxa = Lists.newArrayList();
    for (Resolved resolved : taxa) {
      if (TaxonUtils.areCompatible(taxonomy, resolved.getTaxonomy())) {
        compatibleTaxa.add(resolved);
      }
    }
    return compatibleTaxa;
  }

  /**
   * Returns the total number of taxa that incompatible with the given taxonomy.
   */
  public int getIncompatibleTaxonomyCount(Taxonomy taxonomy) {
    int i = 0;
    for (Resolved resolved : taxa) {
      if (!TaxonUtils.areCompatible(taxonomy, resolved.getTaxonomy())) {
        i++;
      }
    }
    
    return i;
  }

  /**
   * Update the taxa associated with this taxonomy:
   * - All the taxa passed in are included
   * - All taxa in *other* (incompatible) taxonomies are kept
   * - All taxa in this taxonomy that *aren't* in the given list are dropped 
   */
  public void syncTaxa(Taxonomy taxonomy, ImmutableList<Resolved> taxaForThisTaxonomy) {
    List<Resolved> newTaxa = Lists.newArrayList(taxaForThisTaxonomy);
    for (Resolved taxon : taxa) {
      if (!TaxonUtils.areCompatible(taxonomy, taxon.getTaxonomy())) {
        newTaxa.add(taxon);
      }
    }
    if (!taxa.equals(newTaxa)) {
      edited();
    }
    taxa = newTaxa;
  }
  
  /**
   * Returns a collection of sighting info so they can be modified en masse, independent of taxa.
   */
  public Collection<SightingInfo> getSightingInfos() {
    return sightings.values();
  }  
  
  /** Returns true if there's no data in the edit. */
  public boolean isEmpty() {
    return taxa.isEmpty();
  }
}
