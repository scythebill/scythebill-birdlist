/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Collection;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Creates a few countries that may be missing as of 12.1. 
 */
class AddNewCountriesIn121 implements Upgrader {
  @Inject
  AddNewCountriesIn121() {
  }

  @Override
  public String getVersion() {
    return "12.1.1";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    addIfNecessary(locations,
        "French Southern and Antarctic Lands",
        "TF",
        "Indian Ocean",
        null);
    addIfNecessary(locations,
        "Ashmore and Cartier Islands",
        "AC",
        "Australasia",
        null);
    addIfNecessary(locations,
        "Coral Sea Islands",
        "CS",
        "Australasia",
        null);
    addIfNecessary(locations,
        "Prince Edward and Marion Islands",
        "ZA-WC-EdwardMarion",
        "Indian Ocean",
        null);
    addIfNecessary(locations,
        "Kosovo",
        "XK",
        "Europe",
        Location.Type.country);
  }

  private void addIfNecessary(
      LocationSet locations,
      String name,
      String code,
      String parent,
      Location.Type type) {
    if (locations.getLocationByCode(code) == null) {
      Location parentLocation = getBuiltInLocation(locations, parent);
      if (parentLocation != null) {
        Location existing = parentLocation.getContent(name);
        if (existing == null) {
          Location newLocation = Location.builder()
              .setBuiltInPrefix("ct")
              .setEbirdCode(code)
              .setName(name)
              .setParent(parentLocation)
              .setType(type)
              .build();
          locations.addLocation(newLocation);
          locations.markDirty();
        }
      }
    }
  }
      
  private Location getBuiltInLocation(LocationSet locations, String name) {
    Collection<Location> locationsByName = locations.getLocationsByModelName(name);
    for (Location location : locationsByName) {
      if (location.isBuiltInLocation()) {
        return location;
      }
    }
    return null;
  }
}
