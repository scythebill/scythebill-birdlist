package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.logging.Logger;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Performs a variety of country-code cleanup from 13.6.
 */
final class CountryCodeCleanupIn136 implements Upgrader {
  private static final Logger logger = Logger.getLogger(CountryCodeCleanupIn136.class.getName());
  private static final ImmutableList<String> LOCATIONS_TO_REMOVE = ImmutableList
      .of(
          "CU-02", // "La Habana", split in two;  and then the name was reassigned to the City of Havana!
          "FI-ES", // Etelä-Suomen lääni - split many ways 
          "FI-IS", // Itä-Suomen lääni - split many ways 
          "FI-LS", // Länsi-Suomen lääni - split many ways
          "FI-OL", // Oulun lääni - split in two
          // Latvia reorganization;  these have been split in many ways
          "LV-AI",
          "LV-AL",
          "LV-BL",
          "LV-BU",
          "LV-CE",
          "LV-DA",
          "LV-DGV",
          "LV-DO",
          "LV-JEL",
          "LV-JK",
          "LV-JL",
          "LV-JUR",
          "LV-KR",
          "LV-KU",
          "LV-LE",
          "LV-LM",
          "LV-LPX",
          "LV-LU",
          "LV-MA",
          "LV-OG",
          "LV-PR",
          "LV-RE",
          "LV-REZ",
          "LV-RI",
          "LV-RIX",
          "LV-SA",
          "LV-TA",
          "LV-TU",
          "LV-VE",
          "LV-VK",
          "LV-VM"
          );


  private static final ImmutableMap<String, String> LOCATIONS_TO_MERGE = ImmutableMap.<String, String>builder()
      .put(
          // Comarca de San Blas changed to Kuna Yala, but eBird left both
          // locations lying around!
          "PA-0", "PA-KY")
      .put(
          // The Åland Islands are no longer a top-level country in eBird
          "AX", "FI-01")
      .put(
          // ... and the Finnish province has been renamed
          "FI-AL", "FI-01")
      .put(
          // Lapland has been renamed
          "FI-LL", "FI-10")
      .put(
          // Gulbene
          "LV-GU", "LV-033")
      .put(
          // Ventspils
          "LV-VEN", "LV-106") 
      .put(
          // The "Kosovo-Metohija" province of Serbia is treated as Kosovo 
          "RS-KM", "XK")
      .build();

  private final PredefinedLocations predefinedLocations;

  @Inject
  CountryCodeCleanupIn136(PredefinedLocations predefinedLocations) {
    this.predefinedLocations = predefinedLocations;
  }
  
  @Override
  public String getVersion() {
    return "13.7.0";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    
    LOCATIONS_TO_MERGE.forEach((fromCode, intoCode) -> {
      Location from = locations.getLocationByCode(fromCode);
      if (from != null) {
        // Need to ensure that the predefined location gets created first
        Location into = locations.getLocationByCode(intoCode);
        if (into == null) {
          into = Locations.getLocationByCodePossiblyCreating(
              locations,
              predefinedLocations,
              intoCode);
          if (into == null) {
            logger.warning(
                "Could not find country with predefined code " + intoCode
                    + " to clean up " + fromCode);
            return;
          }
        }
        
        if (into.getModelName().equals(from.getModelName())) {
          // If the names of the before-and-after are the same, then all that really needs to
          // happen is updating the location codes.
          Location fromWithUpdatedCode = from.asBuilder().setEbirdCode(intoCode).build();
          reportSet.replaceLocation(from, fromWithUpdatedCode);
        } else {
          // Make sure "into" has been added
          locations.ensureAdded(into);
          
          // Now move "from" into "into", safely
          Locations.reparentReplacingIfNeeded(from, into, reportSet);
          // And delete "from"
          deleteSafely(reportSet, from);
        }
      }
    });

    for (String locationCodeToRemove : LOCATIONS_TO_REMOVE) {
      Location location = locations.getLocationByCode(locationCodeToRemove);
      if (location != null) {
        deleteSafely(reportSet, location);
      }
    }
  }
  
  private void deleteSafely(ReportSet reportSet, Location location) {
    // Move all the children up
    for (Location child : Sets.newLinkedHashSet(location.contents())) {
      Locations.reparentReplacingIfNeeded(child, location.getParent(), reportSet);
    }
    // Then delete the obsolete location
    reportSet.deleteLocation(location);
  }

}
