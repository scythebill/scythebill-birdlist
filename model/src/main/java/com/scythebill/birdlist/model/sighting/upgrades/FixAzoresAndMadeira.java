/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Collection;
import java.util.logging.Logger;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Fixes up the Azores and Madeira locations in a few ways.
 */
class FixAzoresAndMadeira implements Upgrader {
  private final static Logger logger = Logger.getLogger(FixAzoresAndMadeira.class.getName());
  @Inject
  FixAzoresAndMadeira() {
  }

  @Override
  public String getVersion() {
    return "10.0.0";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    Location atlanticOcean = getBuiltInLocation(locations, "Atlantic Ocean");
    upgradeAzores(locations, atlanticOcean, reportSet);    
    upgradeMadeira(reportSet, locations);    
  }

  private void upgradeAzores(
      LocationSet locations,
      Location atlanticOcean,
      ReportSet reportSet) {
    // New Azores - if needed.
    Location newAzores = Location.builder()
        .setBuiltInPrefix("faam")
        .setName("Azores")
        .setParent(atlanticOcean)
        .setType(Location.Type.state)
        .setEbirdCode("PT-20")
        .build();

    Collection<Location> allAzores = locations.getLocationsByModelName("Azores");
    if (allAzores.isEmpty()) {
      // No Azores.  Create if possible
      if (atlanticOcean == null) {
        logger.warning("No Atlantic Ocean!  Cannot add Azores");
      } else {
        locations.addLocation(newAzores);
      }
    } else {
      // OK, someone's already got an Azores.  Look for one in the Atlantic?
      for (Location oldAzores : allAzores) {
        if (oldAzores.isBuiltInLocation()) {
          // Found a built-in Azores?  Done.
          return;
        } else if (oldAzores.getParent() == atlanticOcean) {
          // Found one, let's upgrade this (and only this)
          Locations.replaceLocation(newAzores, oldAzores, reportSet);
          return;
        }
      }
    }
  }

  private void upgradeMadeira(ReportSet reportSet, LocationSet locations) {
    Location madeira = getBuiltInLocation(locations, "Madeira");
    if (madeira == null) {
      logger.warning("Madeira is missing");
    } else if (!"PT-30".equals(madeira.getEbirdCode())) {
      Location fixed = Location.builder()
          .setEbirdCode("PT-30")
          .setName(madeira.getModelName())
          .setParent(madeira.getParent())
          .setType(Location.Type.state)
          .build();
      reportSet.replaceLocation(madeira, fixed);
    }
  }

  private Location getBuiltInLocation(LocationSet locations, String name) {
    Collection<Location> locationsByName = locations.getLocationsByModelName(name);
    for (Location location : locationsByName) {
      if (location.isBuiltInLocation()) {
        return location;
      }
    }
    return null;
  }
}
