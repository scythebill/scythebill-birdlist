/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Collection;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Fixes a missing ebird code for the Canary Islands.
 */
class FixCanaryIslandsEbirdCode implements Upgrader {
  @Inject
  FixCanaryIslandsEbirdCode() {
  }

  @Override
  public String getVersion() {
    return "9.3.1";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    Collection<Location> canaries = locations.getLocationsByModelName("Canary Islands");
    for (Location canary : ImmutableList.copyOf(canaries)) {
      if (canary.getEbirdCode() == null
          && canary.getType() == Location.Type.country) {
        Location fixed = Location.builder()
            .setEbirdCode("IC")
            .setName(canary.getModelName())
            .setParent(canary.getParent())
            .setType(canary.getType())
            .build();
        reportSet.replaceLocation(canary, fixed);
      }
    }
  }
}
