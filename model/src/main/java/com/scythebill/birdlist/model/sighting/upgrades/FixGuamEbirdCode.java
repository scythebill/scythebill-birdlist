/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Collection;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Fixes a faulty ebird code for Guam.  It was "GT", but that's Guatemala.
 */
class FixGuamEbirdCode implements Upgrader {
  @Inject
  FixGuamEbirdCode() {
  }

  @Override
  public String getVersion() {
    return "9.3.1";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    Collection<Location> guams = locations.getLocationsByModelName("Guam");
    for (Location guam : ImmutableList.copyOf(guams)) {
      if ("GT".equals(guam.getEbirdCode())) {
        Location fixed = Location.builder()
            .setEbirdCode("GU")
            .setName(guam.getModelName())
            .setParent(guam.getParent())
            .setType(guam.getType())
            .build();
        reportSet.replaceLocation(guam, fixed);
      }
    }
  }
}
