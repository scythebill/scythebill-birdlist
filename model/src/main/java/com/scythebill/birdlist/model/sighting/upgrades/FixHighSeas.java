/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Collection;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Adds High Seas to each of the oceans.
 */
class FixHighSeas implements Upgrader {
  @Inject
  FixHighSeas() {
  }

  @Override
  public String getVersion() {
    return "11.0.0";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    // If there's any High Seas, abort
    if (locations.getLocationByCode("XX") != null) {
      return;
    }
    
    Location atlanticOcean = getBuiltInLocation(locations, "Atlantic Ocean");
    Location arcticOcean = getBuiltInLocation(locations, "Arctic Ocean");
    Location indianOcean = getBuiltInLocation(locations, "Indian Ocean");
    Location pacificOcean = getBuiltInLocation(locations, "Pacific Ocean");
    Location southPolar = getBuiltInLocation(locations, "South Polar Region");
    addHighSeasTo(locations, atlanticOcean);
    addHighSeasTo(locations, arcticOcean);
    addHighSeasTo(locations, indianOcean);
    addHighSeasTo(locations, pacificOcean);
    addHighSeasTo(locations, southPolar);
  }

  private void addHighSeasTo(LocationSet locations, Location location) {
    if (location == null) {
      return;
    }
    
    Location existingHighSeas = location.getContent("High Seas");
    if (existingHighSeas != null) {
      return;
    }
    
    Location highSeas = Location.builder()
        .setBuiltInPrefix("faam")
        .setName("High Seas")
        .setParent(location)
        .setType(Location.Type.country)
        .setEbirdCode("XX")
        .build();
    locations.addLocation(highSeas);
  }

  private Location getBuiltInLocation(LocationSet locations, String name) {
    Collection<Location> locationsByName = locations.getLocationsByModelName(name);
    for (Location location : locationsByName) {
      if (location.isBuiltInLocation()) {
        return location;
      }
    }
    return null;
  }
}
