/*
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxons;

/**
 * Fixes sightings that had the broken "sp." mapping of Vinaceous-breasted and Guadeloupe Amazon!
 * This was broken in the IOC 10.2 and IOC 11.1 mappings.
 */
class FixIocVinaceousBreastedAmazonMapping implements OneTimeUpgrader {

  @Inject
  FixIocVinaceousBreastedAmazonMapping() {}

  @Override
  public String getName() {
    return "VinBreastedAmazonIoc";
  }

  @Override
  public boolean upgrade(ReportSet reportSet) {
    List<Sighting> sightingsToRemove = new ArrayList<>();
    List<Sighting> sightingsToAdd = new ArrayList<>();
    for (Sighting sighting : reportSet.getSightings()) {
      if (sighting.getTaxonomy().isBuiltIn()
          && sighting.getTaxon().getIds().contains("spAmazo1vio")
          && sighting.getTaxon().getIds().contains("spAmazo1vin")) {
        Set<String> taxa = new LinkedHashSet<>();
        // Keep everything except Guadeloupe Amazon
        taxa.addAll(sighting.getTaxon().getIds());
        taxa.remove("spAmazo1vio");
        SightingTaxon updated = SightingTaxons.newPossiblySpTaxon(taxa);

        sightingsToRemove.add(sighting);
        sightingsToAdd.add(sighting.asBuilder().setTaxon(updated).build());
      }
    }

    if (sightingsToAdd.isEmpty()) {
      return false;
    }

    reportSet.mutator().adding(sightingsToAdd).removing(sightingsToRemove).mutate();
    return true;
  }
}
