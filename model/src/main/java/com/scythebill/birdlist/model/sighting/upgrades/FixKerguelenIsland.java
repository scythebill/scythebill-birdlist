/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Collection;

import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.query.SightingPredicates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * If the Kerguelen Island exists, and is in the Indian Ocean, move it to
 * French Southern etc., *unless* it's been left alone by the user in which
 * case just delete it!
 */
class FixKerguelenIsland implements Upgrader {
  @Inject
  FixKerguelenIsland() {}

  @Override
  public String getVersion() {
    return "12.1.1";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    Location indianOcean = getBuiltInLocation(locations, "Indian Ocean");
    if (indianOcean != null) {
      Location kerguelen = indianOcean.getContent("Kerguelen Island");
      if (kerguelen != null) {
        // If nothing was added to Kerguelen, and there's no sightings there,
        // then just delete it.
        if (kerguelen.contents().isEmpty()
            && !anySightingsIn(reportSet, kerguelen)) {
          reportSet.deleteLocation(kerguelen);
        } else {
          // Otherwise, move it to French Southern and Antarctic Lands 
          Location frenchSouthernAndAntarctica = locations.getLocationByCode("TF");
          if (frenchSouthernAndAntarctica != null) {
            kerguelen.reparent(frenchSouthernAndAntarctica);
            locations.markDirty();
          }
        }
      }
    }
  }

  private boolean anySightingsIn(ReportSet reportSet, Location kerguelen) {
    return Iterables.any(
        reportSet.getSightings(),
        SightingPredicates.in(kerguelen, reportSet.getLocations()));
  }

  private Location getBuiltInLocation(LocationSet locations, String name) {
    Collection<Location> locationsByName = locations.getLocationsByModelName(name);
    for (Location location : locationsByName) {
      if (location.isBuiltInLocation()) {
        return location;
      }
    }
    return null;
  }
}
