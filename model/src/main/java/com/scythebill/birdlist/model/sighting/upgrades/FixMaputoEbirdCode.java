/*   
 * Copyright 2017 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Fixes the eBird code for the Maputo state in Mozambique.
 */
final class FixMaputoEbirdCode implements Upgrader {
  @Inject
  FixMaputoEbirdCode() {
  }

  @Override
  public String getVersion() {
    return "13.5.1";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    Location wrongMaputo = locations.getLocationByCode("MZ-MPM");
    if (wrongMaputo != null) {
      Location fixed = wrongMaputo.asBuilder().setEbirdCode("MZ-L").build();
      reportSet.replaceLocation(wrongMaputo, fixed);
    }
  }
}
