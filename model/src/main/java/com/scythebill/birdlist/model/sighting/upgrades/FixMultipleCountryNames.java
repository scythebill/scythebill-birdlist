/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Map;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Fixes a number of country name issues.
 */
class FixMultipleCountryNames {
  private final ImmutableMap<String, String> codeToNameMap;

  public FixMultipleCountryNames(Map<String, String> codeToNameMap) {
    this.codeToNameMap = ImmutableMap.copyOf(codeToNameMap);
  }
  
  public boolean upgrade(ReportSet reportSet) {
    boolean madeChanges = false;
    LocationSet locations = reportSet.getLocations();
    for (Map.Entry<String, String> codeAndName : codeToNameMap.entrySet()) {
      for (Location location : ImmutableList.copyOf(locations.getLocationsByCode(codeAndName.getKey()))) {
        if (!codeAndName.getValue().equals(location.getModelName())) {
          String newName = codeAndName.getValue();
          fixName(reportSet, locations, location, newName);
          madeChanges = true;
        }
      }
    }
    
    return madeChanges;
  }

  /**
   * Fix a single name.
   * <p>
   * Should be easy, no?
   * No.
   * <p>
   * Need to make sure that we don't overwrite any locations that have already been added under
   * that name.
   */
  private void fixName(ReportSet reportSet, LocationSet locations, Location location, String newName) {
    Location existingWithName = location.getParent().getContent(newName);
    if (existingWithName == null) {
      // Simple case - there's nothing with this existing name, it's a trivial transform
      Location fixedName = location.asBuilder().setName(newName).build();
      reportSet.replaceLocation(location, fixedName);
      return;
    }
    
    // The fix is going to be moving all children in, then the existing location.  Make
    // sure none of that will cause a conflict.  If it does, bail.
    if (location.getContent(newName) != null) {
      return;
    }
    
    for (Location content : existingWithName.contents()) {
      if (location.getContent(content.getModelName()) != null) {
        return;
      }
    }
        
    // First, move all the children of the existing location to the desired endpoint
    // (and clone the list to avoid concurrent modification issues)
    for (Location content : ImmutableList.copyOf(existingWithName.contents())) {
      content.reparent(location);
    }
    // Now, move the existing location in the old location
    existingWithName.reparent(location);
    // And delete existingWithName, to move all the sightings up a level
    reportSet.deleteLocation(existingWithName);
    // And now, we can safely rename 
    Location fixedName = location.asBuilder().setName(newName).build();
    reportSet.replaceLocation(location, fixedName);      
  }
}
