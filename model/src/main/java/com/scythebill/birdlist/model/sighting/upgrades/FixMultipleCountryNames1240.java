/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Fixes a number of country name issues.
 */
class FixMultipleCountryNames1240 implements Upgrader {
  private final static ImmutableMap<String, String> CODE_TO_NAME = ImmutableMap.<String, String>builder()
      .put("CC", "Cocos (Keeling) Islands")
      .put("HM", "Heard Island and McDonald Islands")
      .put("RE", "Réunion")
      .put("MF", "Saint Martin (French part)")
      .put("VC", "Saint Vincent and the Grenadines")
      .put("ST", "São Tomé and Príncipe")
      .put("GS", "South Georgia and South Sandwich Islands")
      .put("SH", "Saint Helena, Ascension, and Tristan da Cunha")
      .put("MP", "Northern Mariana Islands")
      .put("VI", "Virgin Islands (U.S.)")
      .put("MM", "Myanmar")
      .build();

  private final FixMultipleCountryNames fixer;

  @Inject
  FixMultipleCountryNames1240() {
    fixer = new FixMultipleCountryNames(CODE_TO_NAME);
  }

  @Override
  public String getVersion() {
    return "12.4.0";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    fixer.upgrade(reportSet);
  }
}
