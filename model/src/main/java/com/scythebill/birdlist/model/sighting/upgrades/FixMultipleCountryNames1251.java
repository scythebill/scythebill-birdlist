/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Fixes a number of country/state name issues in 12.5.1.
 */
class FixMultipleCountryNames1251 implements Upgrader {
  private final static ImmutableMap<String, String> CODE_TO_NAME = ImmutableMap.<String, String>builder()
      .put("CA-YT", "Yukon Territory")
      .put("IN-OR", "Odisha")
      .put("IN-PY", "Puducherry")
      .put("IN-UL", "Uttarakhand")
      .put("IS-1", "Höfuðborgarsvæði")
      // Sudan and South Sudan reshuffled
      .put("SD-NO", "Northern")
      .put("SD-DN", "North Darfur")  
      .put("SD-KH", "Khartoum")
      .put("SD-KA", "Kassala")
      .put("SD-KN", "North Kordofan")  
      .put("SD-DS", "South Darfur")
      .put("SD-DW", "West Darfur")
      .put("SD-KS", "South Kordofan")  
      .put("SS-BW", "Western Bahr el Ghazal")
      .put("SS-BN", "Northern Bahr el Ghazal")
      .put("SS-EW", "Western Equatoria")
      .put("SS-EC", "Central Equatoria")
      .put("SS-EE", "Eastern Equatoria")
      .put("SS-JG", "Jonglei")
      .put("SS-WR", "Warrap")
      .put("SS-UY", "Unity")
      .put("SS-NU", "Upper Nile")
      .put("SD-NB", "Blue Nile")
      .build();
  private final FixMultipleCountryNames fixer;

  @Inject
  FixMultipleCountryNames1251() {
    fixer = new FixMultipleCountryNames(CODE_TO_NAME);
  }

  @Override
  public String getVersion() {
    return "12.5.1";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    fixer.upgrade(reportSet);
  }
}
