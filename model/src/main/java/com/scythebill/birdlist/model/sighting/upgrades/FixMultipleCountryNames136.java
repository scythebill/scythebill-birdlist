/*   
 * Copyright 2017 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Fixes country/state name issues in 13.6.0.
 */
class FixMultipleCountryNames136 implements Upgrader {
  private final static ImmutableMap<String, String> CODE_TO_NAME = ImmutableMap.<String, String>builder()
      .put("AZ-LA", "Länkäran Municipality")
      .put("AZ-SA", "Şəki Municipality")
      .put("AZ-SS", "Susa Municipality")
      .put("AZ-YE", "Yevlax Municipality")
      .put("CA-QC", "Quebec")
      .put("IN-OR", "Odisha")
      .put("IN-UL", "Uttarakhand")
      .put("LA-VT", "Vientiane Prefecture")
      .put("RU-SA", "Sakha, Respublika")
      .put("RU-SAK", "Sakhalinskaja oblast'")
      .put("TR-33", "Mersin")
      .put("UZ-TK", "Toshkent City")
      .put("TW-CHA", "Changhua County")
      .put("TW-CYI", "Chiayi City")
      .put("TW-CYQ", "Chiayi County")
      .put("TW-HSQ", "Hsinchu County")
      .put("TW-HSZ", "Hsinchu City")
      .put("TW-HUA", "Hualien County")
      .put("TW-ILA", "Yilan County")
      .put("TW-KEE", "Keelung City")
      .put("TW-KHH", "Kaohsiung City")
      .put("TW-KHQ", "Kaohsiung County")
      .put("TW-KIN", "Kinmen County")
      .put("TW-LIE", "Lienchiang County")
      .put("TW-MIA", "Miaoli County")
      .put("TW-NAN", "Nantou County")
      .put("TW-PEN", "Penghu County")
      .put("TW-PIF", "Pingtung County")
      .put("TW-TAO", "Taoyuan City")
      .put("TW-TNN", "Tainan City")
      .put("TW-TNQ", "Tainan County")
      .put("TW-TPQ", "New Taipei City")
      .put("TW-TTT", "Taitung County")
      .put("TW-TXG", "Taichung City")
      .put("TW-YUN", "Yunlin County")
      // Must come after removing CU-02!
      .put("CU-03", "La Habana")
      .build();
  private final FixMultipleCountryNames fixer;

  @Inject
  FixMultipleCountryNames136() {
    fixer = new FixMultipleCountryNames(CODE_TO_NAME);
  }

  @Override
  public String getVersion() {
    return "13.7.0";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    fixer.upgrade(reportSet);
  }
}
