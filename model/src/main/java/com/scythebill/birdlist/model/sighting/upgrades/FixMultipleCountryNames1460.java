/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Fixes country/state name issues in 14.6.0.
 */
class FixMultipleCountryNames1460 implements Upgrader {
  private final static ImmutableMap<String, String> CODE_TO_NAME = ImmutableMap.<String, String>builder()
      .put("MK", "North Macedonia")
      .build();
  private final FixMultipleCountryNames fixer;

  @Inject
  FixMultipleCountryNames1460() {
    fixer = new FixMultipleCountryNames(CODE_TO_NAME);
  }

  @Override
  public String getVersion() {
    return "14.6.0";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    fixer.upgrade(reportSet);
  }
}
