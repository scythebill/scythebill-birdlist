/*   
 * Copyright 2017 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Fixes country/state name issues in 14.4.0.
 */
class FixMultipleCountryNames2019 implements OneTimeUpgrader {
  private final static ImmutableMap<String, String> CODE_TO_NAME = ImmutableMap.<String, String>builder()
      .put("GR-69","Mount Athos Autonomous Region")
      .put("GR-A","Eastern Macedonia and Thrace")
      .put("GR-B","Central Macedonia")
      .put("GR-C","Western Macedonia")
      .put("GR-D","Epirus")
      .put("GR-E","Thessaly")
      .put("GR-F","Ionian Islands")
      .put("GR-G","Western Greece")
      .put("GR-H","Central Greece")
      .put("GR-I","Attica")
      .put("GR-J","Peloponnese")
      .put("GR-K","Northern Aegean")
      .put("GR-L","Southern Aegean")
      .put("GR-M","Crete")
      .put("MX-DIF","Ciudad de México")
      .put("RU-MOW","Moscow")
      .put("TJ-GB","Kŭhistoni Badakhshon")
      .put("VI-SC","Saint Croix") 
      .put("VI-SJ","Saint John") 
      .put("VI-ST","Saint Thomas")
      .build();

  private final FixMultipleCountryNames fixer;

  @Inject
  FixMultipleCountryNames2019() {
    fixer = new FixMultipleCountryNames(CODE_TO_NAME);
  }

  @Override
  public String getName() {
    return "CountryNames2019";
  }

  @Override
  public boolean upgrade(ReportSet reportSet) {
    return fixer.upgrade(reportSet);
  }
}
