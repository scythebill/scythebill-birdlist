/*   
 * Copyright 2023 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Fixes country/state name issues in 2022.
 */
class FixMultipleCountryNames2023 implements OneTimeUpgrader {
  private final static ImmutableMap<String, String> CODE_TO_NAME = ImmutableMap.<String, String>builder()
      .put("OM-BU","Al Buraimi")
      .put("OM-DA", "A'Dakhiliya")
      .put("OM-MA", "Muscat")
      .put("OM-WU", "Al Wusta")
      .put("OM-ZA", "A'Dhahirah")
      .put("RU-AD","Adygea")
      .put("RU-AL","Altai Republic")
      .put("RU-ALT","Altai Krai")
      .put("RU-AMU","Amur Oblast")
      .put("RU-ARK","Arkhangelsk Oblast")
      .put("RU-AST","Astrakhan Oblast")
      .put("RU-BA","Bashkortostan")
      .put("RU-BEL","Belgorod Oblast")
      .put("RU-BRY","Bryansk Oblast")
      .put("RU-BU","Buryatia")
      .put("RU-CE","Chechnya")
      .put("RU-CHE","Chelyabinsk Oblast")
      .put("RU-CHI","Zabaykalsky Krai")
      .put("RU-CHU","Chukotka Autonomous Okrug")
      .put("RU-CU","Chuvashia")
      .put("RU-DA","Dagestan")
      .put("RU-IN","Ingushetia")
      .put("RU-IRK","Irkutsk Oblast")
      .put("RU-IVA","Ivanovo Oblast")
      .put("RU-KAM","Kamchatka Krai")
      .put("RU-KB","Kabardino-Balkaria")
      .put("RU-KC","Karachay-Cherkessia")
      .put("RU-KDA","Krasnodar Krai")
      .put("RU-KEM","Kemerovo Oblast")
      .put("RU-KGD","Kaliningrad Oblast")
      .put("RU-KGN","Kurgan Oblast")
      .put("RU-KHA","Khabarovsk Krai")
      .put("RU-KHM","Khanty-Mansi Autonomous Okrug")
      .put("RU-KIR","Kirov Oblast")
      .put("RU-KK","Khakassia")
      .put("RU-KL","Kalmykia")
      .put("RU-KLU","Kaluga Oblast")
      .put("RU-KO","Komi")
      .put("RU-KOS","Kostroma Oblast")
      .put("RU-KR","Republic of Karelia")
      .put("RU-KRS","Kursk Oblast")
      .put("RU-KYA","Krasnoyarsk Krai")
      .put("RU-LEN","Leningrad Oblast")
      .put("RU-LIP","Lipetsk Oblast")
      .put("RU-MAG","Magadan Oblast")
      .put("RU-ME","Mari El")
      .put("RU-MO","Mordovia")
      .put("RU-MOS","Moscow Oblast")
      .put("RU-MUR","Murmansk Oblast")
      .put("RU-NEN","Nenets Autonomous Okrug")
      .put("RU-NGR","Novgorod Oblast")
      .put("RU-NIZ","Nizhny Novgorod Oblast")
      .put("RU-NVS","Novosibirsk Oblast")
      .put("RU-OMS","Omsk Oblast")
      .put("RU-ORE","Orenburg Oblast")
      .put("RU-ORL","Oryol Oblast")
      .put("RU-PER","Perm Krai")
      .put("RU-PNZ","Penza Oblast")
      .put("RU-PRI","Primorsky Krai")
      .put("RU-PSK","Pskov Oblast")
      .put("RU-ROS","Rostov Oblast")
      .put("RU-RYA","Ryazan Oblast")
      .put("RU-SA","Sakha")
      .put("RU-SAK","Sakhalin Oblast")
      .put("RU-SAM","Samara Oblast")
      .put("RU-SAR","Saratov Oblast")
      .put("RU-SE","North Ossetia-Alania")
      .put("RU-SMO","Smolensk Oblast")
      .put("RU-SPE","Saint Petersburg")
      .put("RU-STA","Stavropol Krai")
      .put("RU-SVE","Sverdlovsk Oblast")
      .put("RU-TA","Tatarstan")
      .put("RU-TAM","Tambov Oblast")
      .put("RU-TOM","Tomsk Oblast")
      .put("RU-TUL","Tula Oblast")
      .put("RU-TVE","Tver Oblast")
      .put("RU-TY","Tuva")
      .put("RU-TYU","Tyumen Oblast")
      .put("RU-UD","Udmurtia")
      .put("RU-ULY","Ulyanovsk Oblast")
      .put("RU-VGG","Volgograd Oblast")
      .put("RU-VLA","Vladimir Oblast")
      .put("RU-VLG","Vologda Oblast")
      .put("RU-VOR","Voronezh Oblast")
      .put("RU-YAN","Yamalo-Nenets Autonomous Okrug")
      .put("RU-YAR","Yaroslavl Oblast")
      .put("RU-YEV","Jewish Autonomous Oblast")
      .build();

  private final FixMultipleCountryNames fixer;

  @Inject
  FixMultipleCountryNames2023() {
    fixer = new FixMultipleCountryNames(CODE_TO_NAME);
  }

  @Override
  public String getName() {
    return "CountryNames2023";
  }

  @Override
  public boolean upgrade(ReportSet reportSet) {
    return fixer.upgrade(reportSet);
  }
}
