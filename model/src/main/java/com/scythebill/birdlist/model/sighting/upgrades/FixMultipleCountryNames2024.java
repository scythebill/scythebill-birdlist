/*   
 * Copyright 2024 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Fixes country/state name issues in 2024.
 */
class FixMultipleCountryNames2024 implements OneTimeUpgrader {
  private final static ImmutableMap<String, String> CODE_TO_NAME = ImmutableMap.<String, String>builder()
      .put("MX-MEX","Estado de México")
      // Fix names on old and new codes, since the one-time upgrader path runs
      // separately from the Upgrader code.
      .put("FR-F", "Centre-Val de Loire")
      .put("FR-CVL", "Centre-Val de Loire")
      .put("FR-PDL", "Pays de la Loire")
      .put("FR-R", "Pays de la Loire")
      .put("TW-KHH", "Kaohsiung")
      .put("TW-TNN", "Tainan")
      .put("TW-TXG", "Taichung")
      .build();

  private final FixMultipleCountryNames fixer;

  @Inject
  FixMultipleCountryNames2024() {
    fixer = new FixMultipleCountryNames(CODE_TO_NAME);
  }

  @Override
  public String getName() {
    return "CountryNames2024";
  }

  @Override
  public boolean upgrade(ReportSet reportSet) {
    return fixer.upgrade(reportSet);
  }
}
