/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Map;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Base class for fixing faulty eBird codes.
 */
abstract class FixMultipleEbirdCodes implements Upgrader {
  protected FixMultipleEbirdCodes() {
  }
  
  protected abstract ImmutableMap<String, String> oldCodeToNewCode();
  
  /**
   * Set of location codes to remove.  These must all
   * be included in ReportSet.DELETEABLE_BUILT_IN_LOCATIONS!
   */
  protected ImmutableSet<String> codesToRemove() {
    return ImmutableSet.of();
  }
  
  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    for (Map.Entry<String, String> entry : oldCodeToNewCode().entrySet()) {
      // Don't bother if there's already a location at the target value
      if (locations.getLocationByCode(entry.getValue()) != null) {
        continue;
      }
      
      for (Location location : ImmutableList.copyOf(locations.getLocationsByCode(entry.getKey()))) {
        Location fixed = location.asBuilder()
            .setEbirdCode(entry.getValue())
            .build();
        reportSet.replaceLocation(location, fixed);
      }
    }
    
    for (String codeToRemove : codesToRemove()) {
      for (Location location : ImmutableList.copyOf(locations.getLocationsByCode(codeToRemove))) {
        deleteSafely(reportSet, location);
      }
    }
  }

  private void deleteSafely(ReportSet reportSet, Location location) {
    // Move all the children up
    for (Location child : ImmutableSet.copyOf(location.contents())) {
      Locations.reparentReplacingIfNeeded(child, location.getParent(), reportSet);
    }
    // Then delete the obsolete location
    reportSet.deleteLocation(location);
  }
}
