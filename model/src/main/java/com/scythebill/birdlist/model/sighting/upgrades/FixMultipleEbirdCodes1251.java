/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;

/**
 * Fixes some eBird codes that were off in 12.5.1 and earlier.
 */
class FixMultipleEbirdCodes1251 extends FixMultipleEbirdCodes {
  private static final ImmutableMap<String, String> OLD_CODE_TO_NEW_CODE =
      ImmutableMap.<String, String>builder()
          .put("CA-YK", "CA-YT")
          .put("ID-ML", "ID-MA")
          .put("SD-02", "SD-DN")
          .put("SD-01", "SD-NO")
          .put("SD-03", "SD-KH")
          .put("SD-05", "SD-KA")
          .put("SD-09", "SD-KN")
          .put("SD-11", "SD-DS")
          .put("SD-12", "SD-DW")
          .put("SD-13", "SD-KS")
          .put("SD-14", "SS-BW")
          .put("SD-15", "SS-BN")
          .put("SD-16", "SS-EW")
          .put("SD-17", "SS-EC")
          .put("SD-19", "SS-EE")
          .put("SD-20", "SS-JG")
          .put("SD-21", "SS-WR")
          .put("SD-22", "SS-UY")
          .put("SD-23", "SS-NU")
          .put("SD-24", "SD-NB")
          .put("IE-CW", "IE-L-CW")
          .put("IE-CN", "IE-U-CN")
          .put("IE-CE", "IE-M-CE")
          .put("IE-CO", "IE-M-CO")
          .put("IE-DL", "IE-U-DL")
          .put("IE-D", "IE-L-D")
          .put("IE-G", "IE-C-G")
          .put("IE-KY", "IE-M-KY")
          .put("IE-KE", "IE-L-KE")
          .put("IE-KK", "IE-L-KK")
          .put("IE-LS", "IE-L-LS")
          .put("IE-LM", "IE-C-LM")
          .put("IE-LK", "IE-M-LK")
          .put("IE-LD", "IE-L-LD")
          .put("IE-LH", "IE-L-LH")
          .put("IE-MO", "IE-C-MO")
          .put("IE-MH", "IE-L-MH")
          .put("IE-MN", "IE-U-MN")
          .put("IE-OY", "IE-L-OY")
          .put("IE-RN", "IE-C-RN")
          .put("IE-SO", "IE-C-SO")
          .put("IE-TA", "IE-M-TA")
          .put("IE-WD", "IE-M-WD")
          .put("IE-WH", "IE-L-WH")
          .put("IE-WX", "IE-L-WX")
          .put("IE-WW", "IE-L-WW")
          .build();

  
  @Inject FixMultipleEbirdCodes1251() { }
  
  @Override
  protected ImmutableMap<String, String> oldCodeToNewCode() {
    return OLD_CODE_TO_NEW_CODE;
  }

  @Override
  public String getVersion() {
    return "12.5.1";
  }
}
