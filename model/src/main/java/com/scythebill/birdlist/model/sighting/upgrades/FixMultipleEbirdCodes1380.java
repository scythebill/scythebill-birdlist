/*   
 * Copyright 2017 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;

/**
 * Fixes some eBird codes that were off in 13.8.0 and earlier.
 */
class FixMultipleEbirdCodes1380 extends FixMultipleEbirdCodes {
  private static final ImmutableMap<String, String> OLD_CODE_TO_NEW_CODE =
      //AN-BO -> BQ-BO AN-CU -> CW AN-SB -> BQ-SA AN-SE -> BQ-SE AN-SM -> SX
      ImmutableMap.<String, String>builder()
          .put("AN-BO", "BQ-BO")
          .put("AN-CU", "CW")
          .put("AN-SB", "BQ-SA")
          .put("AN-SE", "BQ-SE")
          .put("AN-SM", "SX")
          .put("AN", "BQ")
          .build();

  
  @Inject FixMultipleEbirdCodes1380() { }
  
  @Override
  protected ImmutableMap<String, String> oldCodeToNewCode() {
    return OLD_CODE_TO_NEW_CODE;
  }

  @Override
  public String getVersion() {
    return "13.8.1";
  }
}
