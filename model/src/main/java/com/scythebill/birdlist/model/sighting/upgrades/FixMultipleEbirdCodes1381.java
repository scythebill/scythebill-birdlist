/*   
 * Copyright 2017 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;

/**
 * Fixes some eBird codes that were off in 13.8.1 and earlier.
 */
class FixMultipleEbirdCodes1381 extends FixMultipleEbirdCodes {
  private static final ImmutableMap<String, String> OLD_CODE_TO_NEW_CODE =
      ImmutableMap.<String, String>builder()
          .put("US-MP-110", "MP-SA")
          .put("US-MP-120", "MP-TI")
          .put("US-MP-085", "MP-NI")
          .put("US-MP-100", "MP-RO")
          .put("US-VI-010", "VI-SC")
          .put("US-VI-020", "VI-SJ")
          .put("US-VI-030", "VI-ST")
          .put("US-PR-001", "PR-AJ")
          .put("US-PR-003", "PR-AD")
          .put("US-PR-005", "PR-AL")
          .put("US-PR-007", "PR-AB")
          .put("US-PR-009", "PR-AI")
          .put("US-PR-011", "PR-AN")
          .put("US-PR-013", "PR-AC")
          .put("US-PR-015", "PR-AR")
          .put("US-PR-017", "PR-BC")
          .put("US-PR-019", "PR-BQ")
          .put("US-PR-021", "PR-BY")
          .put("US-PR-023", "PR-CR")
          .put("US-PR-025", "PR-CG")
          .put("US-PR-027", "PR-CA")
          .put("US-PR-029", "PR-CV")
          .put("US-PR-031", "PR-CN")
          .put("US-PR-033", "PR-CT")
          .put("US-PR-035", "PR-CY")
          .put("US-PR-037", "PR-CB")
          .put("US-PR-039", "PR-CL")
          .put("US-PR-041", "PR-CD")
          .put("US-PR-043", "PR-CO")
          .put("US-PR-045", "PR-CM")
          .put("US-PR-047", "PR-CZ")
          .put("US-PR-049", "PR-CU")
          .put("US-PR-051", "PR-DO")
          .put("US-PR-053", "PR-FJ")
          .put("US-PR-054", "PR-FL")
          .put("US-PR-055", "PR-GC")
          .put("US-PR-057", "PR-GM")
          .put("US-PR-059", "PR-GL")
          .put("US-PR-061", "PR-GB")
          .put("US-PR-063", "PR-GR")
          .put("US-PR-065", "PR-HA")
          .put("US-PR-067", "PR-HO")
          .put("US-PR-069", "PR-HU")
          .put("US-PR-071", "PR-IS")
          .put("US-PR-073", "PR-JY")
          .put("US-PR-075", "PR-JD")
          .put("US-PR-077", "PR-JC")
          .put("US-PR-079", "PR-LJ")
          .put("US-PR-081", "PR-LR")
          .put("US-PR-083", "PR-LM")
          .put("US-PR-085", "PR-LP")
          .put("US-PR-087", "PR-LZ")
          .put("US-PR-089", "PR-LQ")
          .put("US-PR-091", "PR-MT")
          .put("US-PR-093", "PR-MR")
          .put("US-PR-095", "PR-MB")
          .put("US-PR-097", "PR-MG")
          .put("US-PR-099", "PR-MC")
          .put("US-PR-101", "PR-MV")
          .put("US-PR-103", "PR-NG")
          .put("US-PR-105", "PR-NR")
          .put("US-PR-107", "PR-OR")
          .put("US-PR-109", "PR-PT")
          .put("US-PR-111", "PR-PN")
          .put("US-PR-113", "PR-PO")
          .put("US-PR-115", "PR-QB")
          .put("US-PR-117", "PR-RC")
          .put("US-PR-119", "PR-RG")
          .put("US-PR-121", "PR-SB")
          .put("US-PR-123", "PR-SA")
          .put("US-PR-125", "PR-SG")
          .put("US-PR-127", "PR-SJ")
          .put("US-PR-129", "PR-SL")
          .put("US-PR-131", "PR-SS")
          .put("US-PR-133", "PR-SI")
          .put("US-PR-135", "PR-TA")
          .put("US-PR-137", "PR-TB")
          .put("US-PR-139", "PR-TJ")
          .put("US-PR-141", "PR-UT")
          .put("US-PR-143", "PR-VA")
          .put("US-PR-145", "PR-VB")
          .put("US-PR-147", "PR-VQ")
          .put("US-PR-149", "PR-VL")
          .put("US-PR-151", "PR-YB")
          .put("US-PR-153", "PR-YU")
          .build();

  
  @Inject FixMultipleEbirdCodes1381() { }
  
  @Override
  protected ImmutableMap<String, String> oldCodeToNewCode() {
    return OLD_CODE_TO_NEW_CODE;
  }

  @Override
  public String getVersion() {
    return "13.8.2";
  }
}
