/*   
 * Copyright 2020 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed", "in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;

/**
 * Fixes some eBird codes that were off in 14.9.0 and earlier.
 */
class FixMultipleEbirdCodes1490 extends FixMultipleEbirdCodes {
  private static final ImmutableMap<String, String> OLD_CODE_TO_NEW_CODE =
      ImmutableMap.<String, String>builder()
          // A lot of North Macedonia changes
          .put("MK-02", "MK-802")
          .put("MK-03", "MK-201")
          .put("MK-04", "MK-501")
          .put("MK-05", "MK-401")
          .put("MK-06", "MK-601")
          .put("MK-07", "MK-402")
          .put("MK-08", "MK-602")
          .put("MK-10", "MK-403")
          .put("MK-11", "MK-404")
          .put("MK-12", "MK-301")
          .put("MK-13", "MK-101")
          .put("MK-14", "MK-202")
          .put("MK-16", "MK-603")
          .put("MK-18", "MK-405")
          .put("MK-19", "MK-604")
          .put("MK-20", "MK-102")
          .put("MK-21", "MK-303")
          .put("MK-22", "MK-304")
          .put("MK-23", "MK-203")
          .put("MK-24", "MK-103")
          .put("MK-25", "MK-502")
          .put("MK-26", "MK-406")
          .put("MK-27", "MK-503")
          .put("MK-30", "MK-605")
          .put("MK-32", "MK-806")
          .put("MK-33", "MK-204")
          .put("MK-34", "MK-807")
          .put("MK-35", "MK-606")
          .put("MK-36", "MK-104")
          .put("MK-37", "MK-205")
          .put("MK-40", "MK-307")
          .put("MK-41", "MK-407")
          .put("MK-42", "MK-206")
          .put("MK-43", "MK-701")
          .put("MK-44", "MK-702")
          .put("MK-45", "MK-504")
          .put("MK-46", "MK-505")
          .put("MK-47", "MK-703")
          .put("MK-48", "MK-704")
          .put("MK-49", "MK-105")
          .put("MK-50", "MK-607")
          .put("MK-51", "MK-207")
          .put("MK-52", "MK-308")
          .put("MK-53", "MK-506")
          .put("MK-54", "MK-106")
          .put("MK-55", "MK-507")
          .put("MK-56", "MK-408")
          .put("MK-58", "MK-310")
          .put("MK-59", "MK-810")
          .put("MK-60", "MK-208")
          .put("MK-61", "MK-311")
          .put("MK-62", "MK-508")
          .put("MK-63", "MK-209")
          .put("MK-64", "MK-409")
          .put("MK-65", "MK-705")
          .put("MK-66", "MK-509")
          .put("MK-67", "MK-107")
          .put("MK-69", "MK-108")
          .put("MK-70", "MK-812")
          .put("MK-71", "MK-706")
          .put("MK-72", "MK-312")
          .put("MK-73", "MK-410")
          .put("MK-74", "MK-813")
          .put("MK-75", "MK-608")
          .put("MK-76", "MK-609")
          .put("MK-78", "MK-313")
          .put("MK-80", "MK-109")
          .put("MK-81", "MK-210")
          .put("MK-82", "MK-816")
          .put("MK-83", "MK-211")
          .build();

  
  @Inject FixMultipleEbirdCodes1490() { }
  
  @Override
  protected ImmutableMap<String, String> oldCodeToNewCode() {
    return OLD_CODE_TO_NEW_CODE;
  }

  @Override
  public String getVersion() {
    return "14.9.1";
  }
}
