/*   
 * Copyright 2023 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed", "in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;

/**
 * Fixes some eBird codes that were off in 15.7.6 and earlier.
 */
class FixMultipleEbirdCodes1580 extends FixMultipleEbirdCodes {
  private static final ImmutableMap<String, String> OLD_CODE_TO_NEW_CODE =
      ImmutableMap.<String, String>builder()
          .build();

  
  @Inject FixMultipleEbirdCodes1580() { }
  
  @Override
  protected ImmutableMap<String, String> oldCodeToNewCode() {
    return OLD_CODE_TO_NEW_CODE;
  }

  
  @Override
  protected ImmutableSet<String> codesToRemove() {
    return ImmutableSet.of(
        "LK-1",
        "LK-2",
        "LK-3",
        "LK-4",
        "LK-5",
        "LK-6",
        "LK-7",
        "LK-8",
        "LK-9",
        "OM-BA",
        "OM-JA",
        "OM-SH");
  }

  @Override
  public String getVersion() {
    return "15.8.0";
  }
}
