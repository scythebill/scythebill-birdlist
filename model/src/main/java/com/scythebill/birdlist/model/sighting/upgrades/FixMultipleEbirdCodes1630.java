/*   
 * Copyright 2024 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;

/**
 * Fixes eBird code changes for 16.3.0.
 */
class FixMultipleEbirdCodes1630 extends FixMultipleEbirdCodes {

  @Inject
  FixMultipleEbirdCodes1630() {
  }
  
  @Override
  public String getVersion() {
    return "16.3.0";
  }

  @Override
  protected ImmutableMap<String, String> oldCodeToNewCode() {
    return ImmutableMap.<String, String>builder()
        // 2016: France Metropolitan Regions -> First-level metropolitan subdivisions
        // These are regions which have unique mappings
        .put("FR-E", "FR-BRE")
        .put("FR-F", "FR-CVL")
        .put("FR-H", "FR-COR")
        .put("FR-J", "FR-IDF")
        .put("FR-R", "FR-PDL")
        .put("FR-U", "FR-PAC")
        .build();
  }

  @Override
  protected ImmutableSet<String> codesToRemove() {
    return ImmutableSet.of(
        "MD-LA",
        "MD-TI");
  }
  
}
