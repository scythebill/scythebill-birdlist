/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Base class for fixing location parents.
 */
abstract class FixParents implements Upgrader {
  private final PredefinedLocations predefinedLocations;

  protected FixParents(PredefinedLocations predefinedLocations) {
    this.predefinedLocations = predefinedLocations;
  }
  
  protected abstract ImmutableMap<String, String> locationToParent();
  
  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    for (Map.Entry<String, String> entry : locationToParent().entrySet()) {
      Location location = locations.getLocationByCode(entry.getKey());
      // Doesn't exist, don't bother
      if (location == null) {
        continue;
      }
        
      Location neededParent = Locations.getLocationByCodePossiblyCreating(
          locations, predefinedLocations, entry.getValue());
      if (neededParent == null) {
        continue;
      }
      
      if (location.getParent() != neededParent) {
        locations.ensureAdded(neededParent);
        location.reparent(neededParent);
      }
    }
  }
}
