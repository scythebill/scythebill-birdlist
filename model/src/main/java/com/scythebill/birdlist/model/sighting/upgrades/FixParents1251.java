package com.scythebill.birdlist.model.sighting.upgrades;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;

class FixParents1251 extends FixParents {

  @Inject
  FixParents1251(PredefinedLocations predefinedLocations) {
    super(predefinedLocations);
  }

  private final ImmutableMap<String, String> LOCATION_TO_PARENT = ImmutableMap.<String, String>builder()
      // Get former Sudanese states into South Sudan
      .put("SS-BW", "SS")
      .put("SS-BN", "SS")
      .put("SS-EW", "SS")
      .put("SS-EC", "SS")
      .put("SS-EE", "SS")
      .put("SS-JG", "SS")
      .put("SS-WR", "SS")
      .put("SS-UY", "SS")
      .put("SS-NU", "SS")
      // Get Irish counties into Irish provinces 
      .put("IE-L-CW", "IE-L") 
      .put("IE-U-CN", "IE-U")
      .put("IE-M-CE", "IE-M")
      .put("IE-M-CO", "IE-M")
      .put("IE-U-DL", "IE-U")
      .put("IE-L-D", "IE-L")
      .put("IE-C-G", "IE-C")
      .put("IE-M-KY", "IE-M")
      .put("IE-L-KE", "IE-L")
      .put("IE-L-KK", "IE-L")
      .put("IE-L-LS", "IE-L")
      .put("IE-C-LM", "IE-C")
      .put("IE-M-LK", "IE-M")
      .put("IE-L-LD", "IE-L")
      .put("IE-L-LH", "IE-L")
      .put("IE-C-MO", "IE-C")
      .put("IE-L-MH", "IE-L")
      .put("IE-U-MN", "IE-U")
      .put("IE-L-OY", "IE-L")
      .put("IE-C-RN", "IE-C")
      .put("IE-C-SO", "IE-C")
      .put("IE-M-TA", "IE-M")
      .put("IE-M-WD", "IE-M")
      .put("IE-L-WH", "IE-L")
      .put("IE-L-WX", "IE-L")
      .put("IE-L-WW", "IE-L")
      .build();
  
  
  @Override
  public String getVersion() {
    return "12.5.1";
  }

  @Override
  protected ImmutableMap<String, String> locationToParent() {
    return LOCATION_TO_PARENT;
  }

}
