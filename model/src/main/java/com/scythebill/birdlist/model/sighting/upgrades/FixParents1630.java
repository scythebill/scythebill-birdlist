/*   
 * Copyright 2024 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;

class FixParents1630 extends FixParents {

  @Inject
  FixParents1630(PredefinedLocations predefinedLocations) {
    super(predefinedLocations);
  }

  private final ImmutableMap<String, String> LOCATION_TO_PARENT = ImmutableMap.<String, String>builder()
      // Get France Metropolitan Regions -> First-level metropolitan subdivisions
      // These are regions which have non-unique mappings.
      .put("FR-C", "FR-ARA")
      .put("FR-V", "FR-ARA")
      .put("FR-D", "FR-BFC")
      .put("FR-I", "FR-BFC")
      .put("FR-A", "FR-GES")
      .put("FR-G", "FR-GES")
      .put("FR-M", "FR-GES")
      .put("FR-O", "FR-HDF")
      .put("FR-S", "FR-HDF")
      .put("FR-B", "FR-NAQ")
      .put("FR-L", "FR-NAQ")
      .put("FR-T", "FR-NAQ")
      .put("FR-P", "FR-NOR")
      .put("FR-Q", "FR-NOR")
      .put("FR-K", "FR-OCC")
      .put("FR-N", "FR-OCC")
      // Russian rejiggerings
      .put("RU-AGB", "RU-CHI")
      .put("RU-UOB", "RU-IRK")
      // St. Lucia changes
      .put("LC-04", "LC-06")
      .put("LC-09", "LC-08")
      // Taiwan changes
      .put("TW-KHQ", "TW-KHH")
      .put("TW-TNQ", "TW-TNN")
      .put("TW-TXQ", "TW-TXG")
      // Tobago as a single region
      .put("TT-ETO", "TT-TOB")
      .put("TT-WTO", "TT-TOB")
      .build();
  
  
  @Override
  public String getVersion() {
    return "16.3.0";
  }

  @Override
  protected ImmutableMap<String, String> locationToParent() {
    return LOCATION_TO_PARENT;
  }

}
