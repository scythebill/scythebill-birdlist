/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Collection;

import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.query.SightingPredicates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * If Shag Rocks exist, and is in the South Polar Region, move it to
 * South Georgia and South Sandwich Islands., *unless* it's been left
 * alone by the user in which case just delete it!
 */
class FixShagRocks implements Upgrader {
  @Inject
  FixShagRocks() {}

  @Override
  public String getVersion() {
    return "12.1.1";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    Location southPolar = getBuiltInLocation(locations, "South Polar Region");
    if (southPolar != null) {
      Location shagRocks = southPolar.getContent("Shag Rocks");
      if (shagRocks != null) {
        // If nothing was added to Shag Rocks, and there's no sightings there,
        // then just delete it.
        if (shagRocks.contents().isEmpty()
            && !anySightingsIn(reportSet, shagRocks)) {
          reportSet.deleteLocation(shagRocks);
        } else {
          // Otherwise, move it to South Georgia and South Sandwich Islands 
          Location southGeorgia = locations.getLocationByCode("GS");
          if (southGeorgia != null) {
            shagRocks.reparent(southGeorgia);
            locations.markDirty();
          }
        }
      }
    }
  }

  private boolean anySightingsIn(ReportSet reportSet, Location kerguelen) {
    return Iterables.any(
        reportSet.getSightings(),
        SightingPredicates.in(kerguelen, reportSet.getLocations()));
  }

  private Location getBuiltInLocation(LocationSet locations, String name) {
    Collection<Location> locationsByName = locations.getLocationsByModelName(name);
    for (Location location : locationsByName) {
      if (location.isBuiltInLocation()) {
        return location;
      }
    }
    return null;
  }
}
