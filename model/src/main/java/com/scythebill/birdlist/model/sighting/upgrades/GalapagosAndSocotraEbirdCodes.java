/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Collection;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Gives Galapagos and Socotra ebird codes.  The Galapagos code is legit - EC-W, 
 * but the Socotra one is iffy - Socotra is part of YE-HD, but only a subset. 
 */
class GalapagosAndSocotraEbirdCodes implements Upgrader {
  @Inject
  GalapagosAndSocotraEbirdCodes() {
  }

  @Override
  public String getVersion() {
    return "10.0.0";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    Location galapagos = getBuiltInLocation(locations, "Galapagos Islands");
    if (galapagos != null && !"EC-W".equals(galapagos.getEbirdCode())) {
      Location newGalapagos = Location.builder()
          .setName(galapagos.getModelName())
          .setType(galapagos.getType())
          .setParent(galapagos.getParent())
          .setEbirdCode("EC-W")
          .build();
      reportSet.replaceLocation(galapagos, newGalapagos);
    }

    Location socotra = getBuiltInLocation(locations, "Socotra");
    if (socotra != null && !"YE-HD-SOC".equals(socotra.getEbirdCode())) {
      Location newSocotra = Location.builder()
          .setName(socotra.getModelName())
          .setType(socotra.getType())
          .setParent(socotra.getParent())
          .setEbirdCode("YE-HD-SOC")
          .build();
      reportSet.replaceLocation(socotra, newSocotra);
    }
}

  private Location getBuiltInLocation(LocationSet locations, String name) {
    Collection<Location> locationsByName = locations.getLocationsByModelName(name);
    for (Location location : locationsByName) {
      if (location.isBuiltInLocation()) {
        return location;
      }
    }
    return null;
  }
}
