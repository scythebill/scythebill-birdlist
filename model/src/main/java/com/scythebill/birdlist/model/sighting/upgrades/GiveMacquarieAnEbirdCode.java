/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Collection;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Gives Macquarie Island a completely fictional eBird code.  In practice,
 * though it's "good enough", because it puts the sightings inside AU-TAS, and that
 * is in fact where the Macquarie Islands belong!
 */
class GiveMacquarieAnEbirdCode implements Upgrader {
  private static final String MACQUARIE_CODE = "AU-TAS-Macquarie";

  @Inject
  GiveMacquarieAnEbirdCode() {
  }

  @Override
  public String getVersion() {
    return "12.1.1";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    Location macquarie = getBuiltInLocation(locations, "Macquarie Island");
    if (macquarie != null && !MACQUARIE_CODE.equals(macquarie.getEbirdCode())) {
      Location newMacquarie = Location.builder()
          .setName(macquarie.getModelName())
          .setType(macquarie.getType())
          .setParent(macquarie.getParent())
          .setEbirdCode(MACQUARIE_CODE)
          .build();
      reportSet.replaceLocation(macquarie, newMacquarie);
    }
  }

  private Location getBuiltInLocation(LocationSet locations, String name) {
    Collection<Location> locationsByName = locations.getLocationsByModelName(name);
    for (Location location : locationsByName) {
      if (location.isBuiltInLocation()) {
        return location;
      }
    }
    return null;
  }
}
