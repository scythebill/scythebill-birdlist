package com.scythebill.birdlist.model.sighting.upgrades;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Clean up any instances where the Falkland Islands are in the UK.  Some imports botched this.
 */
class KeepFalklandIslandsInTheAtlantic implements Upgrader {
  @Inject
  KeepFalklandIslandsInTheAtlantic() {}

  @Override
  public String getVersion() {
    return "13.3.0";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    Location falklands = locations.getLocationByCode("FK");
    if (falklands == null) {
      return;
    }
    
    Location unitedKingdom = locations.getLocationByCode("GB");
    if (unitedKingdom != null) {
      // Look for Falklands in the UK
      Location falklandsInTheUk = unitedKingdom.getContent("Falkland Islands");
      // If it exists - and it's not just the built-in Falklands with the appropriate code, then...
      if (falklandsInTheUk != null && falklandsInTheUk != falklands) {
        // ... replace it.
        Locations.replaceLocation(falklands, falklandsInTheUk, reportSet);
      }
    }
  }
}
