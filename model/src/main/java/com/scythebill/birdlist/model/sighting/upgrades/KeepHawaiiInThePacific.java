package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Collection;
import java.util.Optional;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Clean up any instances where Hawaii is in North America.  Some imports botched this.
 */
class KeepHawaiiInThePacific implements Upgrader {
  @Inject
  KeepHawaiiInThePacific() {}

  @Override
  public String getVersion() {
    return "13.3.0";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    Location hawaii = locations.getLocationByCode("US-HI");
    if (hawaii == null) {
      return;
    }
    
    Collection<Location> allUnitedStates = locations.getLocationsByModelName("United States");
    Optional<Location> northAmericanUnitedStates = findNorthAmericanUnitedStates(allUnitedStates);
    if (northAmericanUnitedStates.isPresent()) {
      // Look for Hawaii in North America
      Location hawaiiInNorthAmerica = northAmericanUnitedStates.get().getContent("Hawaii");
      // If it exists - and it's not just the built-in Hawaii with the appropriate code, then...
      if (hawaiiInNorthAmerica != null && hawaiiInNorthAmerica != hawaii) {
        // ... replace it.
        Locations.replaceLocation(hawaii, hawaiiInNorthAmerica, reportSet);
      }
    }
  }

  private Optional<Location> findNorthAmericanUnitedStates(Collection<Location> allUnitedStates) {
    return allUnitedStates.stream()
        .filter(l -> l.getParent().getModelName().equals("North America"))
        .findAny();
  }

}
