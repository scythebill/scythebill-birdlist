/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Makes St. Pierre et Miquelon a state, so that it plays well with "total ticks".
 */
class MakeStPierreEtMiquelonAState implements Upgrader {
  @Inject
  MakeStPierreEtMiquelonAState() {
  }

  @Override
  public String getVersion() {
    return "13.4.0";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    Location saintPierre = locations.getLocationByCode("PM");
    if (saintPierre != null
        && saintPierre.getType() != Location.Type.state) {
      Location fixed = saintPierre.asBuilder()
          .setType(Location.Type.state)
          .build();
      Locations.replaceLocation(fixed, saintPierre, reportSet);
    }
  }
}
