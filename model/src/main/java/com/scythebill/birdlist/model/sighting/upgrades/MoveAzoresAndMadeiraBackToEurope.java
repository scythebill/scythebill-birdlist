/*   
 * Copyright 2017 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Collection;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Moves the Azores and Madeira back to Europe, where the ABA committee confirms they should be.
 */
class MoveAzoresAndMadeiraBackToEurope implements Upgrader {
  @Inject
  MoveAzoresAndMadeiraBackToEurope() {
  }

  @Override
  public String getVersion() {
    return "13.8.2";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    Location europe = findEurope(locations);
    if (europe == null) {
      return;
    }
    Location azores = locations.getLocationByCode("PT-20");
    Location madeira = locations.getLocationByCode("PT-30");
    
    moveToEuropeIfInAtlantic(reportSet, azores, europe);
    moveToEuropeIfInAtlantic(reportSet, madeira, europe);
  }

  /**
   * Only bother moving if they are in the Atlantic.  If someone has specifically moved these into Portugal
   * (for example), then let them stay there.
   */
  private void moveToEuropeIfInAtlantic(ReportSet reportSet, Location country, Location europe) {
    if (country != null
        && country.getParent().getModelName().equals("Atlantic Ocean")) {
      Locations.reparentReplacingIfNeeded(country, europe, reportSet);
      country.reparent(europe);
      reportSet.markDirty();
    }
  }

  private Location findEurope(LocationSet locations) {
    Collection<Location> europes = locations.getLocationsByModelName("Europe");
    for (Location europe : europes) {
      if (europe.isBuiltInLocation()) {
        return europe;
      }
    }
    return null;
  }
}
