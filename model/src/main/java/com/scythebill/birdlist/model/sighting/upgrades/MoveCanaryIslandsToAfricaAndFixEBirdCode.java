/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Collection;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Location.Type;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Makes sure the Canary Islands country has a proper ISO code (ES-CN, not IC).
 * Merges the Canarias province, if one exists, out of Spain and into the Canary Islands. 
 *  
 */
class MoveCanaryIslandsToAfricaAndFixEBirdCode implements Upgrader {
  private static final String CORRECT_CANARY_ISLANDS_EBIRD_CODE = "ES-CN";
  private static final String CANARY_ISLANDS_NAME = "Canary Islands";

  @Inject
  MoveCanaryIslandsToAfricaAndFixEBirdCode() {
  }

  @Override
  public String getVersion() {
    return "12.4.0";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    // Immediately, find if there's entries under the proper eBird code (but inside Spain)
    Location canariasProvince = locations.getLocationByCode(CORRECT_CANARY_ISLANDS_EBIRD_CODE);
    
    // There is an "ES-CN", but it's not in "ES".  Immediately terminate.
    if (canariasProvince != null
        && !"ES".equals(canariasProvince.getParent().getEbirdCode())) {
      return;
    }
    
    Location africa = getBuiltInLocation(locations, "Africa");
    if (africa != null) {      
      // If it exists there, it was created as "IC"
      Location canaryIslands = locations.getLocationByCode("IC");
      if (canaryIslands == null) {
        // Don't create the canary islands anew if they already exist
        if (canariasProvince != null) {
          return;
        }
        
        // Doesn't exist, needs to be created
        if (africa.getContent(CANARY_ISLANDS_NAME) == null) {
          canaryIslands = Location.builder()
              .setBuiltInPrefix("ct")
              .setEbirdCode(CORRECT_CANARY_ISLANDS_EBIRD_CODE)
              .setName(CANARY_ISLANDS_NAME)
              .setParent(africa)
              .setType(Type.state)
              .build();
          locations.addLocation(canaryIslands);
          locations.markDirty();
        }
      } else {
        // Does exist, verify it's in Africa, then fix its ebird code
        if (canaryIslands.getParent() == africa
            && !canaryIslands.getEbirdCode().equals(CORRECT_CANARY_ISLANDS_EBIRD_CODE)) {
          Location fixed = Location.builder()
              .setEbirdCode(CORRECT_CANARY_ISLANDS_EBIRD_CODE)
              .setName(canaryIslands.getModelName())
              .setParent(canaryIslands.getParent())
              .setType(canaryIslands.getType())
              .build();
          reportSet.replaceLocation(canaryIslands, fixed);
          canaryIslands = fixed;
        }
      }
      
      if (canariasProvince != null) {
        // At this point, there was an earlier "Canarias" province *and* a Canary Islands "country".
        // Move everything in canarias province into the Canary Islands
        for (Location child : ImmutableList.copyOf(canariasProvince.contents())) {
          child.reparent(canaryIslands);
        }
        // Then move the now-empty province into the Canary Islands 
        canariasProvince.reparent(canaryIslands);
        // And replace it with a location that has no code
        Location clearTheCode = Location.builder()
            .setName(canariasProvince.getModelName())
            .setParent(canariasProvince.getParent())
            .setType(canariasProvince.getType())
            .build();
        reportSet.replaceLocation(canariasProvince, clearTheCode);
        reportSet.deleteLocation(clearTheCode);
      }
    }
    
    
  }

  private Location getBuiltInLocation(LocationSet locations, String name) {
    Collection<Location> locationsByName = locations.getLocationsByModelName(name);
    for (Location location : locationsByName) {
      if (location.isBuiltInLocation()) {
        return location;
      }
    }
    return null;
  }
}
