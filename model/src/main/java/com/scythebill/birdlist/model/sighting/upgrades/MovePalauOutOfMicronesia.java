/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Collection;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Moves Palau out of Micronesia.  This was an erroneous choice made earlier that
 * confuses the checklist code.
 */
class MovePalauOutOfMicronesia implements Upgrader {
  @Inject
  MovePalauOutOfMicronesia() {
  }

  @Override
  public String getVersion() {
    return "12.4.0";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    Collection<Location> palaus = locations.getLocationsByModelName("Palau");
    Collection<Location> micronesias = locations.getLocationsByModelName("Micronesia");
    Location palau = getLocationWithEbirdCode(palaus, "PW");
    Location micronesia = getLocationWithEbirdCode(micronesias, "FM");
    if (palau != null && micronesia != null && palau.getParent() == micronesia) {
      palau.reparent(micronesia.getParent());
      locations.markDirty();
    }
  }

  private Location getLocationWithEbirdCode(Collection<Location> locations, String ebirdCode) {
    for (Location location : locations) {
      if (ebirdCode.equals(location.getEbirdCode())) {
        return location;
      }
    }
    return null;
  }
}
