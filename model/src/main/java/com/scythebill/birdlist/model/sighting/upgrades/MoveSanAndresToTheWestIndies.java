/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Collection;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Location.Type;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Moves "San Andres/Providencia" to the West Indies. 
 */
class MoveSanAndresToTheWestIndies implements Upgrader {
  private static final String SAN_ANDRES_NAME = "San Andrés, Providencia y Santa Catalina";

  @Inject
  MoveSanAndresToTheWestIndies() {
  }

  @Override
  public String getVersion() {
    return "12.1.1";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    
    // Fix San Andres etc.
    Location westIndies = getBuiltInLocation(locations, "West Indies");
    if (westIndies != null) {
      Location sanAndres = locations.getLocationByCode("CO-SAP");
      if (sanAndres == null) {
        // Doesn't exist, needs to be created in the West Indies
        if (westIndies.getContent(SAN_ANDRES_NAME) == null) {
          sanAndres = Location.builder()
              .setBuiltInPrefix("ct")
              .setEbirdCode("CO-SAP")
              .setName(SAN_ANDRES_NAME)
              .setParent(westIndies)
              .setType(Type.state)
              .build();
          locations.addLocation(sanAndres);
          locations.markDirty();
        }
      } else {
        // Does exist, see if it's in Colombia, if so move to the West Indies
        if ("CO".equals(sanAndres.getParent().getEbirdCode())) {
          sanAndres.reparent(westIndies);
          locations.markDirty();
        }
      }
    }
  }

  private Location getBuiltInLocation(LocationSet locations, String name) {
    Collection<Location> locationsByName = locations.getLocationsByModelName(name);
    for (Location location : locationsByName) {
      if (location.isBuiltInLocation()) {
        return location;
      }
    }
    return null;
  }
}
