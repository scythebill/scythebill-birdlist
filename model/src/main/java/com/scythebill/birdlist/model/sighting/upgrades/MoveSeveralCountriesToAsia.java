/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Collection;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Moves the countries of the Caucasus and Cyprus into Asia, to conform with
 * listing regions.
 */
class MoveSeveralCountriesToAsia implements Upgrader {
  @Inject
  MoveSeveralCountriesToAsia() {
  }

  @Override
  public String getVersion() {
    return "10.0.0";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    Location asia = findAsia(locations);
    if (asia == null) {
      return;
    }
    Location cyprus = findEuropeanCountry(locations, "Cyprus", "CY");
    Location armenia = findEuropeanCountry(locations, "Armenia", "AM");
    Location azerbaijan = findEuropeanCountry(locations, "Azerbaijan", "AZ");
    Location georgia = findEuropeanCountry(locations, "Georgia", "GE");
    
    moveToAsia(locations, cyprus, asia);
    moveToAsia(locations, armenia, asia);
    moveToAsia(locations, azerbaijan, asia);
    moveToAsia(locations, georgia, asia);
  }

  private void moveToAsia(LocationSet locations, Location country, Location asia) {
    if (country != null) {
      country.reparent(asia);
      locations.markDirty();
    }
  }

  private Location findEuropeanCountry(
      LocationSet locations, String name, String ebirdCode) {
    Collection<Location> countries = locations.getLocationsByModelName(name);
    for (Location country : countries) {
      if (country.isBuiltInLocation()
          && ebirdCode.equals(country.getEbirdCode())
          && country.getParent() != null
          && "Europe".equals(country.getParent().getModelName())) {
        return country;
      }
    }
    
    return null;
  }
  
  private Location findAsia(LocationSet locations) {
    Collection<Location> asias = locations.getLocationsByModelName("Asia");
    for (Location asia : asias) {
      if (asia.isBuiltInLocation()) {
        return asia;
      }
    }
    return null;
  }
}
