/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Collection;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Moves the countries of Australasia that are *not* placed there by the ABA into
 * the region.
 */
class MoveSeveralCountriesToAustralasia implements Upgrader {
  @Inject
  MoveSeveralCountriesToAustralasia() {
  }

  @Override
  public String getVersion() {
    return "13.7.0";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    Location australasia = findAustralasia(locations);
    if (australasia == null) {
      return;
    }
    Location vanuatu = findPacificCountry(locations, "Vanuatu", "VU");
    Location newCaledonia = findPacificCountry(locations, "New Caledonia", "NC");
    Location newZealand = findPacificCountry(locations, "New Zealand", "NZ");
    Location solomonIslands = findPacificCountry(locations, "Solomon Islands", "SB");
    Location norfolk = findPacificCountry(locations, "Norfolk Island", "NF");
    Location macquarie = findPacificCountry(locations, "Macquarie Island", "AU-TAS-Macquarie");
    
    moveToAustralasia(vanuatu, australasia, reportSet);
    moveToAustralasia(newCaledonia, australasia, reportSet);
    moveToAustralasia(newZealand, australasia, reportSet);
    moveToAustralasia(solomonIslands, australasia, reportSet);
    moveToAustralasia(norfolk, australasia, reportSet);
    moveToAustralasia(macquarie, australasia, reportSet);
  }

  private void moveToAustralasia(Location country, Location australasia, ReportSet reportSet) {
    if (country != null) {
      Locations.reparentReplacingIfNeeded(country, australasia, reportSet);
      reportSet.markDirty();
    }
  }

  private Location findPacificCountry(
      LocationSet locations, String name, String ebirdCode) {
    Collection<Location> countries = locations.getLocationsByModelName(name);
    for (Location country : countries) {
      if (country.isBuiltInLocation()
          && ebirdCode.equals(country.getEbirdCode())
          && country.getParent() != null
          && "Pacific Ocean".equals(country.getParent().getModelName())) {
        return country;
      }
    }
    
    return null;
  }
  
  private Location findAustralasia(LocationSet locations) {
    Collection<Location> australasias = locations
        .getLocationsByModelName("Australasia");
    for (Location australasia : australasias) {
      if (australasia.isBuiltInLocation()) {
        return australasia;
      }
    }
    return null;
  }
}
