package com.scythebill.birdlist.model.sighting.upgrades;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Find any manually entered locations that should be replaced by built-in locations.
 */
class ReassignNewBuiltInLocations implements Upgrader {
  private Provider<PredefinedLocations> predefinedLocationsProvider;

  @Inject
  ReassignNewBuiltInLocations(Provider<PredefinedLocations> predefinedLocationsProvider) {
    // Save the predefined locations as a provider (to avoid loading unnecessarily)
    this.predefinedLocationsProvider = predefinedLocationsProvider;
  }

  @Override
  public String getVersion() {
    // Always check for locations that should be replaced with built-in locations.
    return "9999";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    for (Location location : ImmutableList.copyOf(locations.rootLocations())) {
      upgrade(reportSet, location);
    }
  }

  private void upgrade(ReportSet reportSet, Location location) {
    // If the location is built-in and the parent is not, see if it should be replaced
    // by a built-in location.
    if (!location.isBuiltInLocation()
        && location.getParent() != null
        && location.getParent().isBuiltInLocation()) {
      // See if one of the predefined locations has a matching name
      String locationName = location.getModelName();
      // ... but catch " County", in case someone entered "Foo County" instead of just "Foo".
      if (locationName.endsWith(" County")) {
        locationName = locationName.substring(0, locationName.length() - " County".length());
      }

      PredefinedLocation predefinedLocation = predefinedLocationsProvider.get()
          .getPredefinedLocationChild(location.getParent(), locationName);
      // No predefined match- return (the children of this location won't be assignable either)
      if (predefinedLocation == null) {
        return;
      }
      
      String name = predefinedLocation.getName();
      // Ignore it if the parent location already has something matching that exact name,
      // that *isn't* this child itself.  getPredefinedLocationChild() returns inexact matches. 
      if (location.getParent().getContent(name) != null
          && location.getParent().getContent(name) != location) {
        return;
      }
      
      // And also - if the location exists by code, but has been moved elsewhere, don't
      // try recreating it.
      if (reportSet.getLocations().getLocationByCode(predefinedLocation.getCode()) != null) {
        return;
      }
    
      // Create the new location
      Location newLocation = predefinedLocation.create(reportSet.getLocations(), location.getParent());
      
      // Replace the old with the new
      Locations.replaceLocation(newLocation, location, reportSet);

      // and continue (to iterate over the children, if needed)
      location = newLocation;
    }
    
    // Iterate over the children (making a copy of the list, since it might change during iteration) 
    for (Location child : ImmutableList.copyOf(location.contents())) {
      upgrade(reportSet, child);
    }
  }
}
