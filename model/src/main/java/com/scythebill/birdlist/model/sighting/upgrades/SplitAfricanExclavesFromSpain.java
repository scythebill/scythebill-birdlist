/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Collection;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Location.Type;

/**
 * Splits the two African exclaves out of Spain. 
 */
class SplitAfricanExclavesFromSpain implements Upgrader {
  private final static String CEUTA_MELILLA_CODE = "ES-CEUMEL";
  
  @Inject
  SplitAfricanExclavesFromSpain() {
  }

  @Override
  public String getVersion() {
    return "12.1.1";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    if (locations.getLocationByCode(CEUTA_MELILLA_CODE) != null) {
      return;
    }
    
    Location africa = getBuiltInLocation(locations, "Africa");
    if (africa == null) {
      return;
    }
    
    Location ceutaAndMelilla = Location.builder()
        .setBuiltInPrefix("ct")
          .setEbirdCode(CEUTA_MELILLA_CODE)
          .setName("Ceuta and Melilla")
          .setType(Type.state)
          .setParent(africa)
          .build();
    locations.addLocation(ceutaAndMelilla);
    
    
    Location ceuta = locations.getLocationByCode("ES-CE");
    if (ceuta != null
        && "ES".equals(ceuta.getParent().getEbirdCode())) {
      ceuta.reparent(ceutaAndMelilla);
    }
    
    Location melilla = locations.getLocationByCode("ES-ML");
    if (melilla != null
        && "ES".equals(melilla.getParent().getEbirdCode())) {
      melilla.reparent(ceutaAndMelilla);
    }
  }

  private Location getBuiltInLocation(LocationSet locations, String name) {
    Collection<Location> locationsByName = locations.getLocationsByModelName(name);
    for (Location location : locationsByName) {
      if (location.isBuiltInLocation()) {
        return location;
      }
    }
    return null;
  }
}
