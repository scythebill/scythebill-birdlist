/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Collection;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Location.Type;

/**
 * Splits the Netherlands Antilles between the West Indies and South America. 
 */
class SplitNetherlandsAntilles implements Upgrader {
  private static final String NETHERLANDS_ANTILLES = "Netherlands Antilles";

  @Inject
  SplitNetherlandsAntilles() {
  }

  @Override
  public String getVersion() {
    return "12.1.1";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    LocationSet locations = reportSet.getLocations();
    Location southAmerica = getBuiltInLocation(locations, "South America");
    Location westIndies = getBuiltInLocation(locations, "West Indies");
    if (southAmerica != null && westIndies != null) {
      if (southAmerica.getContent(NETHERLANDS_ANTILLES) != null) {
        return;
      }
      
      Location westIndiesNetherlandsAntilles = westIndies.getContent(NETHERLANDS_ANTILLES);
      if (westIndiesNetherlandsAntilles == null) {
        return;
      }
      
      // OK, there's work to do.  First, create Netherlands Antilles in South America
      Location southAmericaNetherlandsAntilles = Location.builder()
          .setBuiltInPrefix("ct")
          .setEbirdCode("AN")
          .setName(NETHERLANDS_ANTILLES)
          .setType(Type.country)
          .setParent(southAmerica)
          .build();
      locations.addLocation(southAmericaNetherlandsAntilles);
      
      // Move Bonaire
      Location bonaire = westIndiesNetherlandsAntilles.getContent("Bonaire");
      if (bonaire != null) {
        bonaire.reparent(southAmericaNetherlandsAntilles);
      }

      // Move Curaçao
      Location curaco = westIndiesNetherlandsAntilles.getContent("Curaçao");
      if (curaco != null) {
        curaco.reparent(southAmericaNetherlandsAntilles);
      }
    }
  }

  private Location getBuiltInLocation(LocationSet locations, String name) {
    Collection<Location> locationsByName = locations.getLocationsByModelName(name);
    for (Location location : locationsByName) {
      if (location.isBuiltInLocation()) {
        return location;
      }
    }
    return null;
  }
}
