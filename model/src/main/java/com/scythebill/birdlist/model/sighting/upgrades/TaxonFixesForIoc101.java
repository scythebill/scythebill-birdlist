/*
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxons;

/**
 * "Fixes" sightings for IOC 10.1 in two ways.
 * <p>
 * First, removes any "spuh" sightings for Vinaceous-breasted Parrot and Guadalupe Parrot! These
 * were just a simple bug in IOC 9.2.
 * <p>
 * Second, handle the IOC 10.1 addition of subspecies "deignani", necessary to handle Deignan's
 * Prinia. The upgrader will look for sightings of the cooki subspecies and map them to: - Just
 * cooki if in Myanmar - Just deignani if in Thailand, Laos, or Cambodia - Both cooki and deignani
 * otherwise [This was deleted for Clements 2022]
 */
class TaxonFixesForIoc101 implements OneTimeUpgrader {

  @Inject
  TaxonFixesForIoc101() {}

  @Override
  public String getName() {
    return "TaxonFixesForIoc101";
  }

  @Override
  public boolean upgrade(ReportSet reportSet) {
    List<Sighting> sightingsToRemove = new ArrayList<>();
    List<Sighting> sightingsToAdd = new ArrayList<>();
    for (Sighting sighting : reportSet.getSightings()) {
      if (sighting.getTaxonomy().isBuiltIn()) {
        if (sighting.getTaxon().getType().equals(SightingTaxon.Type.SP)
            && sighting.getTaxon().getIds().contains("spAmazo1vin")
            && sighting.getTaxon().getIds().contains("spAmazo1vio")) {
          // Violaceous and Guadeloupe Amazon together - fix! Mercifiully,
          // there is no birder that has seen Guadeloupe Amazon (extinct for centuries!)
          // so this can safely be fixed.
          SightingTaxon updated = SightingTaxons.newSightingTaxon("spAmazo1vin");
          sightingsToRemove.add(sighting);
          sightingsToAdd.add(sighting.asBuilder().setTaxon(updated).build());
        }
      }
    }

    if (sightingsToAdd.isEmpty()) {
      return false;
    }

    reportSet.mutator().adding(sightingsToAdd).removing(sightingsToRemove).mutate();
    return true;
  }
}
