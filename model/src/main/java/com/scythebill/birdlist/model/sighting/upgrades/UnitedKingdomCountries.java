package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Collection;
import java.util.Map;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Adds the countries of the United Kingdom to any already present United Kingdom, and moves counties
 * inside those countries as needed.
 */
class UnitedKingdomCountries implements Upgrader {
  private Provider<PredefinedLocations> predefinedLocationsProvider;

  @Inject
  UnitedKingdomCountries(Provider<PredefinedLocations> predefinedLocationsProvider) {
    // Save the predefined locations as a provider (to avoid loading unnecessarily)
    this.predefinedLocationsProvider = predefinedLocationsProvider;
  }

  @Override
  public String getVersion() {
    return "9.3.1";
  }

  @Override
  public void upgrade(ReportSet reportSet) {
    PredefinedLocations predefinedLocations = predefinedLocationsProvider.get();
    LocationSet locations = reportSet.getLocations();
    Collection<Location> uks = locations.getLocationsByModelName("United Kingdom");
    if (uks.size() != 1) {
      return;
    }
    
    Location uk = Iterables.getOnlyElement(uks);
    ImmutableCollection<PredefinedLocation> ukChildren = predefinedLocations.getPredefinedLocations(uk);
    Preconditions.checkState(!ukChildren.isEmpty());
    
    // Add England (etc.)
    Map<PredefinedLocation, Location> countriesOfTheUk = Maps.newHashMap();
    for (PredefinedLocation ukChild : ukChildren) {
      Location existingChild = uk.getContent(ukChild.getName());
      if (existingChild != null) {
        countriesOfTheUk.put(ukChild, existingChild);
      } else {
        Location newChild = ukChild.create(locations, uk);
        locations.ensureAdded(newChild);
        countriesOfTheUk.put(ukChild, newChild);
      }
    }
    
    // Now move any non-built-in children of the UK.
    // (Make a copy to avoid concurrent modifications)
    for (Location ukChild : ImmutableList.copyOf(uk.contents())) {
      if (ukChild.isBuiltInLocation()) {
        continue;
      }
      
      // See if this child corresponds to a predefined child of one of those countries.
      // (E.g., if someone had created a "Kent" county in the U.K.; this will automatically
      // move it into England).
      // In "ReassignNewBuiltInLocations", Scythebill will swap that location for the 
      // actual prebuilt location.
      for (Location countryOfTheUk : countriesOfTheUk.values()) {
        if (predefinedLocations.getPredefinedLocationChild(countryOfTheUk, ukChild.getModelName()) != null) {
          ukChild.reparent(countryOfTheUk);
        }
      }
    }
  }
}
