/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;

/**
 * Guice bindings for upgraders.
 */
public final class UpgraderModule extends AbstractModule {

  @Override
  protected void configure() {
    // Generic upgraders run after taxonomic resolution for upgrades.
    // Each runs arbitrarily many times, limited only by a version check
    Multibinder<Upgrader> upgraders = Multibinder.newSetBinder(binder(), Upgrader.class);
    upgraders.addBinding().to(UnitedKingdomCountries.class);
    upgraders.addBinding().to(FixHighSeas.class);
    upgraders.addBinding().to(AddNewCountriesIn121.class);
    upgraders.addBinding().to(SplitAfricanExclavesFromSpain.class);
    upgraders.addBinding().to(KeepHawaiiInThePacific.class);
    upgraders.addBinding().to(KeepFalklandIslandsInTheAtlantic.class);
    // And reassign built-in locations.  (This must come after adding new countries.) 
    upgraders.addBinding().to(ReassignNewBuiltInLocations.class);
    upgraders.addBinding().to(FixGuamEbirdCode.class);
    upgraders.addBinding().to(FixSloveniaEbirdCode.class);
    upgraders.addBinding().to(FixCanaryIslandsEbirdCode.class);
    upgraders.addBinding().to(MovePalauOutOfMicronesia.class);
    upgraders.addBinding().to(MoveSeveralCountriesToAsia.class);
    upgraders.addBinding().to(FixAzoresAndMadeira.class);
    upgraders.addBinding().to(GalapagosAndSocotraEbirdCodes.class);
    upgraders.addBinding().to(FixUruguayEbirdCode.class);
    upgraders.addBinding().to(FixKerguelenIsland.class);
    upgraders.addBinding().to(FixShagRocks.class);
    upgraders.addBinding().to(GiveMacquarieAnEbirdCode.class);
    upgraders.addBinding().to(GiveEasterIslandAnEbirdCode.class);
    upgraders.addBinding().to(GiveJuanFernandezAnEbirdCode.class);
    upgraders.addBinding().to(MoveSanAndresToTheWestIndies.class);
    upgraders.addBinding().to(SplitNetherlandsAntilles.class);
    upgraders.addBinding().to(MoveCanaryIslandsToAfricaAndFixEBirdCode.class);
    upgraders.addBinding().to(FixMultipleEbirdCodes1251.class);
    upgraders.addBinding().to(MakeStPierreEtMiquelonAState.class);
    upgraders.addBinding().to(FixMaputoEbirdCode.class);
    upgraders.addBinding().to(CountryCodeCleanupIn136.class);
    upgraders.addBinding().to(FixMultipleEbirdCodes1380.class);
    upgraders.addBinding().to(FixMultipleEbirdCodes1381.class);
    upgraders.addBinding().to(FixMultipleEbirdCodes1490.class);
    upgraders.addBinding().to(FixMultipleEbirdCodes1580.class);
    upgraders.addBinding().to(FixMultipleEbirdCodes1630.class);
    // Do some reparenting
    upgraders.addBinding().to(FixParents1251.class);
    upgraders.addBinding().to(FixParents1630.class);
    upgraders.addBinding().to(MoveSeveralCountriesToAustralasia.class);
    upgraders.addBinding().to(MoveAzoresAndMadeiraBackToEurope.class);
    upgraders.addBinding().to(MoveIsleOfManAndChannelIslandsOutOfUk.class);
    // and finally fix country names, which had better come after changing country codes
    // (since the fixes rely on country codes to find items)
    upgraders.addBinding().to(FixMultipleCountryNames1240.class);
    upgraders.addBinding().to(FixMultipleCountryNames1251.class);
    upgraders.addBinding().to(FixMultipleCountryNames134.class);
    upgraders.addBinding().to(FixMultipleCountryNames136.class);
    upgraders.addBinding().to(FixMultipleCountryNames1380.class);
    upgraders.addBinding().to(FixMultipleCountryNames1460.class);
    
    // A newer, probably better upgrader, guaranteed to run exactly once.
    // These also run *before* doing IOC upgrade resolution, and can therefore
    // be used to do extra taxonomic "fixups" for changes made to the eBird/Clements
    // taxonomy that depart from the "real" taxonomy but support compatibility with IOC.
    Multibinder<OneTimeUpgrader> oneTimeUpgraders =
        Multibinder.newSetBinder(binder(), OneTimeUpgrader.class);
    oneTimeUpgraders.addBinding().to(TaxonFixesForIoc101.class);
    oneTimeUpgraders.addBinding().to(FixMultipleCountryNames2019.class);
    oneTimeUpgraders.addBinding().to(FixMultipleCountryNames2023.class);
    oneTimeUpgraders.addBinding().to(FixMultipleCountryNames2024.class);
    oneTimeUpgraders.addBinding().to(FixIocVinaceousBreastedAmazonMapping.class);
  }
}
