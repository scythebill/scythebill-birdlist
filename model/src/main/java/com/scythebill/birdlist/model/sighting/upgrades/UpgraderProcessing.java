/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting.upgrades;

import java.util.Set;

import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.util.Versions;

/**
 * Processes upgrades on a ReportSet.
 */
public class UpgraderProcessing {
  private final ImmutableSet<Upgrader> upgraders;

  @Inject
  UpgraderProcessing(Set<Upgrader> upgraders) {
    this.upgraders = ImmutableSet.copyOf(upgraders);
  }
  
  /** Upgrades the ReportSet. */
  public void upgradeReportSet(ReportSet reportSet) {
    for (Upgrader upgrader : upgraders) {
      if (reportSet.getLoadedVersion() == null
          || Versions.versionOrdering().compare(
              reportSet.getLoadedVersion(), upgrader.getVersion()) < 0) {
        upgrader.upgrade(reportSet);
      }
    }
  }
}
