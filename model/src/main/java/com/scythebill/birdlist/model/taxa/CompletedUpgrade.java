/*   
 * Copyright 2017 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.taxa;

import java.util.Collection;

import com.google.common.collect.ImmutableSet;
import com.scythebill.birdlist.model.sighting.SightingTaxon;

/** Reports from a completed upgrade. */
public interface CompletedUpgrade {
  String getNewTaxonomyName();
  Taxonomy getForcedTaxonomy();
  String getPreviousTaxonomyId();
  Collection<SightingTaxon> warnings();
  Collection<SightingTaxon> sps(Taxonomy taxonomy);

  /**
   * Adds additional "spuh"s to reconcile after an upgrade.
   *
   * @param onlyForTaxonomy only apply these sighting taxa if the current taxonomy is the one given. 
   */
  void applyAdditionalSps(Taxonomy onlyForTaxonomy, ImmutableSet<SightingTaxon> additionalSps);
}