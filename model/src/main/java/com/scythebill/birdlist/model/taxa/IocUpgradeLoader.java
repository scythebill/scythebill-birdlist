/*   
 * Copyright 2017 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.taxa;

import java.io.IOException;

import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableSet;
import com.google.common.io.CharSource;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxons;

/**
 * Supports loading the content of an "IOC Upgrade" file.
 * <p>
 * Such files are simply CSV Files where only the first column is used (other columns have common names for readability).
 * The first column is a slash-separated list of eBird/Clements IDs, naming a SightingTaxon which should
 * be looked at for upgrades.
 */
public class IocUpgradeLoader {
  private static final Splitter SP_SPLITTER = Splitter.on('/');
  private String id;
  private CharSource mappingSupplier;

  public IocUpgradeLoader(
      String id,
      CharSource mappingSupplier) {
    this.id = id;
    this.mappingSupplier = mappingSupplier;
  }
  
  public String getId() {
    return id;
  }

  public ImmutableSet<SightingTaxon> affectedSightingTaxa() throws IOException {
    ImmutableSet.Builder<SightingTaxon> affectedSightingTaxa = ImmutableSet.builder();
    try (ImportLines lines = CsvImportLines.fromReader(mappingSupplier.openBufferedStream())) {
      String[] line;
      while ((line = lines.nextLine()) != null) {
        SightingTaxon affectedTaxon = SightingTaxons.newPossiblySpTaxon(
            SP_SPLITTER.splitToList(line[0]));
        affectedSightingTaxa.add(affectedTaxon);
      }
    }
    return affectedSightingTaxa.build();
  }
}
