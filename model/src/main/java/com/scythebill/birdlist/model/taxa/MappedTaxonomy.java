package com.scythebill.birdlist.model.taxa;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multiset;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Taxon.Type;

/**
 * A taxonomy that is a mapping layer on top of the base taxonomy.
 * Since all sightings are stored relative to the base taxonomy, care must be
 * exercise when working with SightingTaxon and Resolved values in a mapped taxonomy.
 * <p>
 * To map from here to the base taxonomy, use {@link #getMapping}.
 * <p>
 * To map from the base taxonomy into this, use {@link #resolveInto}.
 */
public class MappedTaxonomy extends TaxonomyImpl {
  private final Taxonomy base;
  
  /** A map from taxon ID (in this taxonomy) to sighting taxa in the base taxonomy. */
  private Map<String, SightingTaxon> mappingsToBase;
  /**
   * A map from taxon ID (in the base taxonomy) to taxa in this taxonomy,
   * when those mappings are unique.
   */
  private Map<String, String> baseToMapped;
  /**
   * A multimap from a taxon ID (in the base taxonomy) to taxa in this taxonomy,
   * when those mappings are not unique.
   */
  private Multimap<String, String> duplicateBaseToMapped;

  public MappedTaxonomy(
      Taxonomy base,
      String id,
      String name) {
    super(id, name, null, null);
    this.base = Preconditions.checkNotNull(base);
  }

  @Override
  public SightingTaxon toBaseSightingTaxon(String id) {
    SightingTaxon mapping = getMapping(id);
    if (mapping == null) {
      throw new IllegalArgumentException(
              "There is no base mapping for " + id + ", should not have been allowed!");      
    }
    
    return mapping;
  }
  
  /** Return the base taxonomy. */
  public Taxonomy getBaseTaxonomy() {
    return base;
  }
  
  /**
   * Attach mappings to this taxonomy.
   */
  public synchronized void setMappings(MappingBuilder mappingBuilder) {
    // Mappings from this to the base can be copied over.
    this.mappingsToBase = mappingBuilder.mappingsToBase;
    
    // But the other direction requires some cleanup.
    this.baseToMapped = Maps.newHashMap();
    this.duplicateBaseToMapped = HashMultimap.create();
    
    for (Entry<String, Collection<String>> entry : mappingBuilder.baseToMapped.asMap().entrySet()) {
      String baseId = entry.getKey();
      Collection<String> mappedIds = entry.getValue();
      // If there's only one mapping from a base, done
      if (mappedIds.size() == 1) {
        baseToMapped.put(baseId, Iterables.getOnlyElement(mappedIds));
      } else {
        // If there's more than one, that could either mean that it's just not a unique mapping (rare)
        // ... or more likely, that there's:
        //   - a mapped subspecies that maps to a group
        //   - and a mapped species that maps to that group and other groups as well
        //  E.g., Eastern Barn Owl ssp. crassirostris maps to Barn Owl (crassirostris group)
        //  and Eastern Barn Owl maps to Barn Owl (crassirostris) and Barn Own (Australian) 
        List<Taxon> mappedTaxa = Lists.newArrayList();
        Multiset<Taxon.Type> typeCounts = HashMultiset.create();
        for (String mappedId : mappedIds) {
          Taxon mappedTaxon = getTaxon(mappedId);
          mappedTaxa.add(mappedTaxon);
          typeCounts.add(mappedTaxon.getType());
        }
        if (mappedTaxa.size() == 2
            && typeCounts.count(Taxon.Type.species) == 1
            && typeCounts.count(Taxon.Type.subspecies) == 1) {
         for (Taxon mappedTaxon : mappedTaxa) {
           if (mappedTaxon.getType() == Taxon.Type.subspecies) {
             baseToMapped.put(baseId, mappedTaxon.getId());
             break;
           }
         }
        } else {
          duplicateBaseToMapped.putAll(baseId, mappedIds);
        }
      }
    }
  }
  
  public void addMapping(Taxon taxon, SightingTaxon mapping) {
    Preconditions.checkArgument(taxon.getTaxonomy() == this);
    Preconditions.checkState(!mappingsToBase.containsKey(taxon.getId()));
    // This API could be supported for other options, but this is the only one needed right now,
    // so it's not worth complicating this code unnecessarily.
    Preconditions.checkArgument(mapping.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES);
    mappingsToBase.put(taxon.getId(), mapping);
  }
  
  public static class MappingBuilder {
    private final Map<String, SightingTaxon> mappingsToBase = Maps.newHashMap();
    private final Multimap<String, String> baseToMapped = HashMultimap.create();

    public void registerTaxonMapping(
        Taxon taxon, SightingTaxon baseMapping) {
      SightingTaxon existing = mappingsToBase.put(taxon.getId(), baseMapping);
      Preconditions.checkState(existing == null, "Mapping already existed for " + taxon.getId());
      
      if (baseMapping.getType() != SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
        for (String id : baseMapping.getIds()) {
          baseToMapped.put(id, taxon.getId());
        }
      }
    }
  
    public void removeTaxonMapping(Taxon taxon) {
      SightingTaxon removed = mappingsToBase.remove(taxon.getId());
      for (String id : removed.getIds()) {
        baseToMapped.remove(id, taxon.getId());
      }
    }

    public SightingTaxon getExactMapping(Taxon taxon) {
      return mappingsToBase.get(taxon.getId());
    }
  }
  
  /**
   * Return the precise mapped taxon, not attempting synthesis from children.
   */
  public SightingTaxon getExactMapping(Taxon taxon) {
    return mappingsToBase.get(taxon.getId());
  }
  
  /**
   * Returns the base-taxonomy mapping that should be used for this taxon.
   * If there is no precise mapping, use children to produce a sp. mapping.
   */
  public SightingTaxon getMapping(String id) {
    SightingTaxon mapping = mappingsToBase.get(id);
    if (mapping != null) {
      return mapping;
    }
    
    Taxon taxon = getTaxon(id);
    if (taxon == null) {
      throw new NullPointerException("Cannot map;  no taxon for \"" + id + "\"");
    }
    return getMapping(taxon);
  }
  
  /**
   * Returns the base taxonomy mapping that should be used for this sighting taxon.
   */
  public SightingTaxon getMapping(SightingTaxon sightingTaxon) {
    if (sightingTaxon.getType() == SightingTaxon.Type.SINGLE) {
      return getMapping(sightingTaxon.getId());
    } else {
      Set<String> baseIds = Sets.newHashSet();
      for (String mappedId : sightingTaxon.getIds()) {
        SightingTaxon baseTaxon = getMapping(mappedId);
        if (baseTaxon != null) {
          baseIds.addAll(baseTaxon.getIds());
        }
      }
      if (baseIds.isEmpty()) {
        return null;
      } else if (baseIds.size() == 1) {
        return SightingTaxons.newSightingTaxon(Iterables.getOnlyElement(baseIds));
      } else {
        if (sightingTaxon.getType() == SightingTaxon.Type.SP) {
          // On mapping back to the base taxonomy:  see if the mapping is in fact
          // a "sp." of all constituents.
          Taxon parentTaxon = base.getTaxon(baseIds.iterator().next()).getParent();
          if (parentTaxon.getType().compareTo(Taxon.Type.species) <= 0) {
            Set<String> allImmediateChildrenIds = Sets.newHashSet();
            for (Taxon child : parentTaxon.getContents()) {
              allImmediateChildrenIds.add(child.getId());
            }
            if (allImmediateChildrenIds.equals(baseIds)) {
              return SightingTaxons.newSightingTaxon(parentTaxon.getId());
            }
          }
          return SightingTaxons.newSpTaxon(baseIds);
        } else {
          Preconditions.checkState(sightingTaxon.getType() == SightingTaxon.Type.HYBRID);
          // As elsewhere, this creates some bogus state, where you might have a 
          // three-way "hybrid" (Mallard x Mexican Duck x Gadwall), when it really should
          // be (Mallard/Mexican Duck x Gadwall).  But the latter isn't supported.
          return SightingTaxons.newHybridTaxon(baseIds);
        }
      }
    }
  }
  
  /**
   * Returns the base-taxonomy mapping that should be used for this taxon.
   * If there is no precise mapping, use children to produce a sp. mapping.
   */
  public SightingTaxon getMapping(Taxon taxon) {
    SightingTaxon mapping = getExactMapping(taxon);
    if (mapping != null || taxon.getContents().isEmpty()) {
      return mapping;
    }
    
    // Special-case family/order mappings by grabbing the first species therein,
    // mapping it to a species, then going up to the family.  Not used for sightings,
    // but quite relevant for mapping selections in the UI.
    if (taxon.getType() == Taxon.Type.family
          || taxon.getType() == Taxon.Type.order) {
      Taxon firstSpecies = TaxonUtils.getFirstChildOfType(taxon, Taxon.Type.species);
      SightingTaxon speciesMapping = getMapping(firstSpecies);
      if (speciesMapping != null) {
        return speciesMapping.resolve(base).getParentOfAtLeastType(taxon.getType());
      }
    }
    
    // Gather the mapping of all of the children
    Set<String> baseTaxaIds = Sets.newLinkedHashSet();
    for (Taxon child : taxon.getContents()) {
      SightingTaxon childMapping = getExactMapping(child);
      if (childMapping != null
          // ... but skip single-with-secondary-subspecies, as they'll all just map up
          // to the species and confuse everything.
          && childMapping.getType() != SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
        baseTaxaIds.addAll(childMapping.getIds());
      }
    }

    // Used to not allow MappedTaxonomy taxa to be defined in terms of a mix of levels
    // (e.g. a base species plus a separate base subspecies from a different species).
    // We do now (as of IOC 8.2)
    // baseTaxaIds = TaxonUtils.mapToCommonLevel(base, baseTaxaIds);
    
    if (baseTaxaIds.isEmpty()) {
      return null;
    }
    return SightingTaxons.newPossiblySpTaxon(baseTaxaIds);
  }
  

  /**
   * Return the precise mapped taxon, not attempting synthesis from children.
   */
  public SightingTaxon getExactMappingFromBase(String id) {
    if (baseToMapped.containsKey(id)) {
      return SightingTaxons.newSightingTaxon(baseToMapped.get(id));
    } else {
      if (duplicateBaseToMapped.containsKey(id)) {
        return SightingTaxons.newSpTaxon(duplicateBaseToMapped.get(id));
      }
    }
    return null;
  }

  /**
   * Map a sighting taxon from the base taxonomy to this taxonomy.
   */
  @Override
  public SightingTaxon.Resolved resolveInto(SightingTaxon sightingTaxon) {
    if (sightingTaxon.getType() == SightingTaxon.Type.SINGLE) {
      return map(sightingTaxon.getId());
    } else if (sightingTaxon.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
      Resolved resolved = map(sightingTaxon.getId());
      if (resolved == null) {
        return null;
      }
      
      for (Taxon resolvedTaxon : resolved.getTaxa()) {
        // If the "parent" is a subspecies, then move up before looking for a subspecies.
        // This is specifically necessary for the following odd case:
        //  Buteo buteo (group buteo) maps to one IOC subspecies
        //  But a second IOC subspecies (harterti) really should fall under that group (if Clements defined it).
        // In that case, IOC harterti maps up to the Clements Buteo buteo (buteo group), which maps to
        // IOC Buteo buteo ssp. buteo,  which of course doesn't have a harterti child.  Hop up a level,
        // and the child can be found.
        if (resolvedTaxon.getType() == Taxon.Type.subspecies) {
          resolvedTaxon = resolvedTaxon.getParent();
        }
        for (Taxon child : resolvedTaxon.getContents()) {
          if (child.getName().equals(sightingTaxon.getSubIdentifier())) {
            return SightingTaxons.newResolved(child);
          }
        }
      }
      
      return resolved;
    } else {
      // Multiple taxa.  First, gather all the taxa that are involved.
      // While doing so, gather:
      //    - whether they're all at the same taxonomic level
      //    - whether they're all children of the same parent
      boolean first = true;
      Taxon.Type highestType = null;
      boolean multipleTypes = false;
      List<SightingTaxon.Resolved> resolveds = Lists.newArrayList();
      // See if all of the mappings are the immediate children of a single parent
      String singleParent = null;
      for (String id : sightingTaxon.getIds()) {
        Resolved resolved = Preconditions.checkNotNull(map(id), "No mapping from base taxon %s", id);
        resolveds.add(resolved);
        
        SightingTaxon parent = resolved.getParent();
        if (first) {
          highestType = resolved.getLargestTaxonType();
          if (parent.getType() == SightingTaxon.Type.SINGLE) {
            singleParent = parent.getId();
          }
          first = false;
        } else {
          if (highestType != resolved.getLargestTaxonType()) {
            multipleTypes = true;
            if (resolved.getLargestTaxonType().compareTo(highestType) > 0) {
              highestType = resolved.getLargestTaxonType();
            }
          }
          if (parent.getType() == SightingTaxon.Type.SINGLE) {
            String nextSingleParent = parent.getId();
            if (!nextSingleParent.equals(singleParent)) {
              singleParent = null;
            }
          } else {
            singleParent = null;
          }
        }
      }
      
      Set<String> ids = Sets.newHashSet();
      // If there are multiple types, map each upward to the max common type
      if (multipleTypes) {
        for (Resolved resolved : resolveds) {
          SightingTaxon parent = resolved.getParentOfAtLeastType(highestType);
          ids.addAll(parent.getIds());
        }
      } else {
        for (Resolved resolved : resolveds) {
          ids.addAll(resolved.getSightingTaxon().getIds());
        }
        // If:
        //   - These were all children of the same parent species
        //   - ... and this set of children constitute *all* the mappable children
        //     (e.g., there's 2 children in this set, the parent species has 3 subspecies,
        //      but one of those subspecies has no mapping)
        if (singleParent != null) {
          int mappableChildren = 0;
          Taxon singleParentTaxon = getTaxon(singleParent);
          if (singleParentTaxon.getType().compareTo(Taxon.Type.species) <= 0) {
            for (Taxon child : singleParentTaxon.getContents()) {
              SightingTaxon mapping = getMapping(child);
              // Count all children that are mappable.  Ignore SINGLE_WITH_SECONDARY_SUBSPECIES,
              // which are not considered mappable here (because getMapping() does strange things)
              if (mapping != null 
                  && mapping.getType() != SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
                mappableChildren++;
              }
            }
            if (mappableChildren == ids.size()) {
              return SightingTaxons.newResolved(singleParentTaxon);
            }
          }
        }
      }
      
      if (ids.size() == 1) {
        return SightingTaxons.newSightingTaxon(Iterables.getOnlyElement(ids))
            .resolveInternal(this);
      } else switch (sightingTaxon.getType()) {
        case SP:
          return SightingTaxons.newSpTaxon(ids).resolveInternal(this);
        case HYBRID:
          // This is partially wrong.  For example, in Clements, a Barn x Pacific Swallow
          // would actually come out as a Barn x Pacific x Hill Swallow, instead of the
          // correct Barn x Pacific/Hill Swallow - but there's no support for hybrid
          // of a "sp.", and I'm not especially interested in adding one for this corner case.
          return SightingTaxons.newHybridTaxon(ids).resolveInternal(this);
        case SINGLE:
        default:
          throw new AssertionError();        
      }
    }
  }

  /**
   * Map a single taxon.
   * 
   * @param id a taxon ID in the base taxonomy
   * @return a resolved instance in this taxonomy
   */
  private SightingTaxon.Resolved map(String id) {
    // A somewhat easy case:  multiple mappings
    if (duplicateBaseToMapped.containsKey(id)) {
      SightingTaxon sps = SightingTaxons.newSpTaxon(duplicateBaseToMapped.get(id));
      return sps.resolveInternal(this);
    }

    String mapped = baseToMapped.get(id);
    // The easy case:  a single mapping, done
    if (mapped != null) {
      Taxon taxon = getTaxon(mapped);
      return SightingTaxons.newResolved(taxon);
    }
    
    Taxon baseTaxon = base.getTaxon(id);
    if (baseTaxon == null) {
      throw new IllegalStateException("Could not get a mapping for " + id);
    }
    // Special-case family/order:  find the first species, map that one
    // into the mapped taxonomy, then up to the family/order
    if (baseTaxon.getType() == Taxon.Type.family
        || baseTaxon.getType() == Taxon.Type.order) {
      Taxon firstSpecies = TaxonUtils.getFirstChildOfType(baseTaxon, Taxon.Type.species);
      Resolved resolvedSpecies = map(firstSpecies.getId());
      if (resolvedSpecies != null) {
        return resolvedSpecies.getParentOfAtLeastType(baseTaxon.getType())
            .resolveInternal(this);
      }
      return null;
    }
    
    // Next, if there's contents of the base, try to map each of the contents
    if (!baseTaxon.getContents().isEmpty()) {
      boolean allChildrenMappable = true;
      Set<String> mappedChildren = Sets.newHashSetWithExpectedSize(4);
      for (Taxon child : baseTaxon.getContents()) {
        if (duplicateBaseToMapped.containsKey(child.getId())) {
          mappedChildren.addAll(duplicateBaseToMapped.get(child.getId()));
        } else {
          String mappedChild = baseToMapped.get(child.getId());
          // If a child can't be mapped, then move up to the parent.
          // Unless it's a species, and this child is a group,
          // in which case *try all the grandchildren*
          if (mappedChild == null) {
            if (baseTaxon.getType() == Taxon.Type.species && !child.getContents().isEmpty()) {
              for (Taxon grandchild : child.getContents()) {
                String mappedGrandchild = baseToMapped.get(grandchild.getId());
                if (mappedGrandchild != null) {
                  mappedChildren.add(mappedGrandchild);
                }
              }
            } else {
              allChildrenMappable = false;
              // Used to break here, but see "awful gilli hack" below 
              //break;
            }
          } else {
            mappedChildren.add(mappedChild);
          }
        }
      }
      
      if (allChildrenMappable
          // AWFUL GILLI HACK.  In IOC 9.1, the "Japanese" and "Mountain" White-eyes are
          // dramatically different from Clements.  This exposes an earlier problem 
          // with the "gilli" taxon - IOC synonymizes this form with a ssp.
          // of Lowland White-eye, yet Clements leaves it around as a ssp. of Mountain
          // White-eye!
          || mappedChildren.size() > 3) {
        // If by some chance all the children map to the same taxon, then we're done
        if (mappedChildren.size() == 1) {
          Taxon taxon = getTaxon(Iterables.getOnlyElement(mappedChildren));
          return SightingTaxons.newResolved(taxon);
        }
        
        // Nope, multiple taxa.  Resolve each, then:
        //  - if there's multiple types, raise all to the highest type
        //   - if there's too many (and highest type is ssp.), then raise
        // each one level
        ImmutableSet.Builder<Taxon> taxaBuilder = ImmutableSet.builder();
        Taxon.Type highestType = null;
        boolean multipleTypes = false;
        for (String mappedChild : mappedChildren) {
          Taxon taxon = getTaxon(mappedChild);
          taxaBuilder.add(taxon);
          
          if (highestType == null) {
            highestType = taxon.getType();
          } else if (highestType != taxon.getType()) {
            if (!multipleTypes) {
//              System.err.println("MULTIPLE TYPES, MAPPING CHILDREN OF " + id + "(" + mappedChildren + ")");
            }
            multipleTypes = true;
            if (taxon.getType().compareTo(highestType) > 0) {
              highestType = taxon.getType();
            }
          }
        }
        
        ImmutableSet<Taxon> taxa = taxaBuilder.build();
        // If there's more than 3 subspecies, then force up to species level
        if (!multipleTypes && highestType == Taxon.Type.subspecies
            && taxa.size() > 3) {
          multipleTypes = true;
          highestType = Taxon.Type.species;
        }
        // Multiple types - map each up to a common level
        if (multipleTypes) {
          taxaBuilder = ImmutableSet.builder();
          for (Taxon taxon : taxa) {
            taxaBuilder.add(TaxonUtils.getParentOfType(taxon, highestType));
          }
          taxa = taxaBuilder.build();
        }
        
        if (taxa.size() == 1) {
          return SightingTaxons.newResolved(Iterables.getOnlyElement(taxa));
        } else {
          return SightingTaxons.newSpResolved(taxa);
        }
      }
    }
    // No children.  Or not all of the children were mappable.
    // So, instead, map upwards, then upwards again
    Taxon parentTaxon = baseTaxon.getParent();
    Resolved resolvedParent = resolveParent(parentTaxon);
    if (resolvedParent == null) {
      resolvedParent = resolveParent(parentTaxon.getParent());
    }
    
    // OK - simplistic mapping of parents didn't work *either*.  If this is something at lower than species level,
    // look at the first adjacent siblings that *do* have a mapping.  Merge those.
    if (resolvedParent == null && baseTaxon.getType().isLowerLevelThan(Type.species)) {
      Set<Taxon> taxa = new LinkedHashSet<>();
      
      // Find the first previous taxon that can be mapped (if any)
      Taxon previous = TaxonUtils.getPreviousSibling(baseTaxon);
      while (true) {
        if (previous == null) {
          break;
        }
        
        Resolved mappedPrevious = map(previous.getId());
        if (mappedPrevious != null) {
          taxa.addAll(mappedPrevious.getTaxa());
          break;
        }
        previous = TaxonUtils.getPreviousSibling(previous);
      }
      
      // Find the first next taxon that can be mapped (if any)
      Taxon next = TaxonUtils.getNextSibling(baseTaxon);
      while (true) {
        if (next == null) {
          break;
        }
        
        Resolved mappedNext = map(next.getId());
        if (mappedNext != null) {
          taxa.addAll(mappedNext.getTaxa());
          break;
        }
        next = TaxonUtils.getNextSibling(next);
      }
      
      // If anything was found, then resolve up to the parent.
      // The move up to a parent is necessary so you don't end up saying a subspecies is a "spuh" of two
      // different mapped subspecies - it really isn't.  It's even only "probably" the close species - this
      // is just the best we can do when most mappings have failed.
      if (!taxa.isEmpty()) {
        resolvedParent = SightingTaxons.newPossiblySpResolved(taxa).resolveParentOfType(Taxon.Type.species);
      }
    }
    
    return resolvedParent;
  }

  /**
   * A smaller resolution algorithm, used when mapping upwards after giving up locally.
   */
  private SightingTaxon.Resolved resolveParent(Taxon baseTaxon) {
    String mapped;
    if (duplicateBaseToMapped.containsKey(baseTaxon.getId())) {
      return SightingTaxons.newSpTaxon(duplicateBaseToMapped.get(baseTaxon.getId()))
          .resolveInternal(this);
    } else {
      mapped = baseToMapped.get(baseTaxon.getId());
      // The easy case:  a single mapping, done
      if (mapped != null) {
        Taxon taxon = getTaxon(mapped);
        return SightingTaxons.newResolved(taxon);
      } else {
        return null;
      }
    }
  }
}
