/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.taxa;

import com.google.common.collect.ImmutableList;

/** Interface for species (and groups/subspecies) */
public interface Species extends Taxon {
  public String getRange();
  public ImmutableList<String> getAlternateNames();
  public ImmutableList<String> getAlternateCommonNames();
  public String getMiscellaneousInfo();
  public String getTaxonomicInfo();
  
  public enum Status {
    LC,
    NE,
    DD,
    NT,
    VU,
    EN,
    CR,
    EW,
    EX,
    IN,
    DO,
    UN
  }
}
