/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.taxa;

import java.util.List;

import com.google.common.collect.ImmutableList;

/** Default implementation of Species (also used for groups and subspecies). */
public class SpeciesImpl extends TaxonImpl implements Species {
  private String taxonomicInfo;
  private String miscellaneousInfo;
  private ImmutableList<String> alternateNames = ImmutableList.of();
  private ImmutableList<String> alternateCommonNames = ImmutableList.of();
  private String range;
  private Status status;

  public SpeciesImpl(Taxon parent) {
    this(Taxon.Type.species, parent);
  }

  public SpeciesImpl(Taxon.Type type, Taxon parent) {
    super(type, parent);
  }

  @Override
  public ImmutableList<String> getAlternateCommonNames() {
    return alternateCommonNames;
  }

  public void setAlternateCommonNames(List<String> alternateCommonNames) {
    this.alternateCommonNames = ImmutableList.copyOf(alternateCommonNames);
  }

  @Override
  public ImmutableList<String> getAlternateNames() {
    return alternateNames;
  }

  public void setAlternateNames(List<String> alternateNames) {
    this.alternateNames = ImmutableList.copyOf(alternateNames);
  }

  @Override
  public String getMiscellaneousInfo() {
    return miscellaneousInfo;
  }

  public void setMiscellaneousInfo(String miscellaneousInfo) {
    this.miscellaneousInfo = miscellaneousInfo;
  }

  @Override
  public String getRange() {
    return range;
  }

  public void setRange(String range) {
    this.range = range;
  }

  @Override
  public String getTaxonomicInfo() {
    return taxonomicInfo;
  }

  public void setTaxonomicInfo(String taxonomicInfo) {
    this.taxonomicInfo = taxonomicInfo;
  }

  @Override
  public Status getStatus() {
    if (status == null) {
      return Status.LC;
    }
    return status;
  }

  public void setStatus(Status status) {
    this.status = (status == Status.LC) ? null : status;
  }
}
