/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.taxa;

import java.util.List;

import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.taxa.Species.Status;

/**
 * A single taxon of any level.
 * TODO: add support for generic metadata (for extensibility + preservation
 * of unknown properties)
 */
public interface Taxon {
  public enum Type {
    subspecies,
    group,
    species,
    genus,
    family,
    order,
    classTaxon("class"),
    phylum;

    private String name;
    Type() {
      this(null);
    }

    Type(String name) {
      this.name = name;
    }

    public boolean isAtOrLowerLevelThan(Taxon.Type type) {
      return compareTo(type) <= 0;
    }

    public boolean isLowerLevelThan(Taxon.Type type) {
      return compareTo(type) < 0;
    }
    
    public String getName() {
      if (name != null) {
        return name;
      }

      return name();
    }
  }

  Type getType();
  Taxon getParent(); 
  List<Taxon> getContents();
  Taxon findByName(String name);
  Taxon findByName(String name, Type type);
  String getCommonName();
  String getName();
  Taxonomy getTaxonomy();
  String getId();
  int getTaxonomyIndex();
  String getAccountId();
  
  /**
   * Converts a Taxon into a (single) SightingTaxon in the base taxonomy.
   */
  SightingTaxon toBaseSightingTaxon();

  /**
   * Mark that the object has been built.
   * TODO: change to a Builder pattern.  This API is extremely confusing.
   */
  void built();
  
  /**
   * Two taxa are equal if they have the same type, the same name, and have
   * an equal parent.
   * TODO: "an equal parent" is NOT an appropriate comparison!
   * If genus "G" is found in family "F", and another genus "G" is in family "F2",
   * that's fine.  But if "G" is in subfamily "SF" in family "F", that's NOT ok.
   * Then again, should this be handled by equals(), or by another validation?    
   */
  @Override
  boolean equals(Object o);
  
  /** Sets if the taxon should be marked disabled. */
  void setDisabled(boolean disabled);
  
  /** Returns if the taxon is disabled (and should not be allowed entry). */
  boolean isDisabled();
  
  Status getStatus();  
}
