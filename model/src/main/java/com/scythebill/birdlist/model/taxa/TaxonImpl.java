/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.taxa;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Objects;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.taxa.Species.Status;

/**
 * Base implementation of Taxon.
 * TODO: eliminate setters (except, probably, for "contents"), use a builder pattern.
 */
public class TaxonImpl implements Taxon {
  private final Type type;
  private Taxon parent;
  private String commonName;
  private String name;
  private List<Taxon> contents;
  private Taxonomy taxonomy;
  private int index;
  private boolean disabled;
  private String accountId;

  public TaxonImpl(Type type, Taxonomy tx) {
    this(type, null, tx);
  }

  public TaxonImpl(Type type, Taxon parent) {
    this(type, parent, (parent == null) ? null : parent.getTaxonomy());
  }

  private TaxonImpl(Type type, Taxon parent, Taxonomy tx) {
    if (type == null) {
      throw new NullPointerException();
    }

    this.taxonomy = tx;
    this.type = type;
    setParent(parent);
  }

  /**
   * Marks that a taxon is fully built, and registers it against
   * the Taxonomy, assigning it an ID.  Do not call for the "root" taxon,
   * which should be registered using TaxonomyImpl.setRoot(), or for
   * any taxon that has an ID.
   * TODO(awiner): DELETE THIS - it is too confusing to call correctly
   */
  @Override
  public void built() {
    getTaxonomy().registerWithNewId(this);
  }

  public void setParent(Taxon parent) {
    this.parent = parent;
    if (parent != null) {
      if (parent.getType().compareTo(getType()) <= 0) {
        throw new IllegalStateException("Cannot add " + getType() +
            " to " + parent.getType());
      }
      taxonomy = parent.getTaxonomy();
      if (contents != null) {
        for (Taxon child : contents) {
          ((TaxonImpl) child).setParent(this);
        }
      }
    }
  }

  @Override
  public Taxonomy getTaxonomy() {
    return taxonomy;
  }

  @Override
  public Taxon getParent() {
    return parent;
  }

  @Override
  public Type getType() {
    return type;
  }

  @Override
  public int getTaxonomyIndex() {
    return index;
  }

  @Override
  public List<Taxon> getContents() {
    if (contents == null) {
      contents = new ArrayList<Taxon>();
    }

    return contents;
  }

  /**
   * Search for a direct child by name.
   */
  @Override
  public Taxon findByName(String name) {
    if (contents == null) {
      return null;
    }

    for (Taxon taxon : contents) {
      if (name.equals(taxon.getName())) {
        return taxon;
      }
    }

    return null;
  }

  /**
   * Search through all children and their children, etc. to find a taxon by name
   * and type.
   * More efficient implementations are possible than
   * this default implementation.
   */
  @Override
  public Taxon findByName(String name, Type type) {
    if (type == getType()) {
      if (name.equals(getName())) {
        return this;
      }
      return null;
    } else {
      if (contents == null) {
        return null;
      }

      for (Taxon taxon : contents) {
        Taxon found = taxon.findByName(name, type);
        if (found != null) {
          return found;
        }
      }
    }

    return null;
  }

  @Override
  public String getCommonName() {
    return commonName;
  }

  public void setCommonName(String commonName) {
    this.commonName = commonName;
  }

  @Override
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String getAccountId() {
    return accountId;
  }
  
  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }
                                             
  @Override
  public String getId() {
    if (taxonomy == null) {
      throw new IllegalStateException("Taxonomy not set on " + getName() + ", " + getParent()
          + ", id not available");
    }

    return taxonomy.getId(this);
  }

  @Override
  public SightingTaxon toBaseSightingTaxon() {
    return taxonomy.toBaseSightingTaxon(getId());
  }

  @Override
  public void setDisabled(boolean disabled) {
    this.disabled = disabled;
  }

  @Override
  public boolean isDisabled() {
    return disabled;
  }

  void setIndex(int index) {
    this.index = index;
  }

  @Override
  public String toString() {
    String className = getClass().getName();
    className = className.substring(className.lastIndexOf('.') + 1);

    return className + "[" + getName() + "," + getType() + ",parent=" + getParent() + "]";
  }

  @Override
  public Status getStatus() {
    return Status.LC;
  }
  
  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (!(o instanceof Taxon)) {
      return false;
    }

    Taxon that = (Taxon) o;
    return type == that.getType()
        && Objects.equal(name, that.getName())
        && Objects.equal(parent, that.getParent());

  }

  @Override
  public int hashCode() {
    return ((type.hashCode() * 31) ^
        (name == null ? 0 : name.hashCode()));
  }
}
