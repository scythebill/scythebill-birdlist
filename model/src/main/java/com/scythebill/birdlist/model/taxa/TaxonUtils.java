/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.taxa;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import javax.annotation.Nullable;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.names.LocalNames;
import com.scythebill.birdlist.model.util.ResolvedComparator;

/**
 * Utilities for working with taxa.
 * <p>
 * TODO: rename class to Taxons, document, refactor etc.
 */
public class TaxonUtils {

  private TaxonUtils() {
  }

  public static final Ordering<Taxon> ORDERING = new Ordering<Taxon>() {
    @Override
    public int compare(Taxon left, Taxon right) {
      return left.getTaxonomyIndex() - right.getTaxonomyIndex();
    }
  };
  
  /**
   * Returns true if {@code parent} is a parent of the taxon identified by childId. 
   */
  public static boolean isChildOfOrSame(Taxon parent, String childId) {
    if (childId.equals(parent.getId())) {
      return true;
    }
    
    Taxon child = parent.getTaxonomy().getTaxon(childId);
    return isChildOf(parent, child);
  }

  /**
   * Returns true if {@code taxon} or any of its children are within {@code taxonIds}.
   */
  public static boolean isItselfOrAChildIn(Taxon taxon, Collection<String> taxonIds) {
    if (taxonIds.contains(taxon.getId())) {
      return true;
    }
    
    for (Taxon child : taxon.getContents()) {
      if (isItselfOrAChildIn(child, taxonIds)) {
        return true;
      }
    }
    return false;
  }

  public static boolean isChildOf(Taxon parent, Taxon child) {
    while (child.getType().compareTo(parent.getType()) < 0) {
      child = child.getParent();
      if (child == parent) {
        return true;
      }
    }
    
    return false;
  }
  
  public static Taxon getParentOfAtLeastType(Taxon taxon, Type type) {
    Taxon parent = taxon;
    while (parent.getType().compareTo(type) < 0) {
      parent = parent.getParent();
      Preconditions.checkState(parent != null);
    }
    
    return parent;
  }

  public static Taxon getParentOfType(Taxon taxon, Type type) {
    Taxon parent = getParentOfTypeOrNull(taxon, type);
    Preconditions.checkState(parent != null);
    return parent;
  }

  public static boolean hasParentOfType(Taxon taxon, Type type) {
    Taxon parent = getParentOfTypeOrNull(taxon, type);
    return parent != null;
  }
  
  public static Taxon getParentOfTypeOrNull(Taxon taxon, Type type) {
    Taxon parent = taxon;
    while (parent != null && parent.getType() != type) {
      parent = parent.getParent();
    }
    
    return parent;
  }

  /**
   * Returns the full scientific name of a taxon.
   */
  public static String getFullName(Taxon taxon) {
    // TODO: this assumes the parent is a genus, not a superspecies 
    if (taxon.getType() == Type.species) {
      return taxon.getParent().getName() + " " + taxon.getName();
    } else if (taxon.getType() == Type.group || taxon.getType() == Type.subspecies) {
      Taxon species = getParentOfType(taxon, Type.species);
      return getFullName(species) + " " + taxon.getName();
    }
    
    return taxon.getName();
  }

  public static String getCommonName(Taxon taxon) {
    LocalNames localNames = taxon.getTaxonomy().getLocalNames();
    return localNames.getCommonName(taxon);
  }
  
  /** Get the common name in English. */
  public static String getEnglishCommonName(Taxon taxon) {
    return LocalNames.trival().getCommonName(taxon);
  }

  /** Get a common name without appending any subspecies. */
  public static String getSimpleCommonName(Taxon taxon) {
    LocalNames localNames = taxon.getTaxonomy().getLocalNames();
    return localNames.getSimpleCommonName(taxon);
  }
    
  /**
   * Returns an abbreviated scientific name of a taxon.
   */
  public static String getAbbreviatedName(Taxon taxon) {
    // TODO: this assumes the parent is a genus, not a superspecies 
    if (taxon.getType() == Type.species) {
      return taxon.getParent().getName().substring(0, 1) + "." + taxon.getName();
    } else if (taxon.getType() == Type.group || taxon.getType() == Type.subspecies) {
      Taxon species = getParentOfType(taxon, Type.species);
      Taxon genus = species.getParent();
      return genus.getName().substring(0, 1) + "."
          + species.getName().substring(0, 1) + "." + taxon.getName();
    }
    
    return taxon.getName();
  }

  /**
   * @param list a list of taxa, which must be sorted
   * @param taxon the new taxon
   * @return the index at which the taxon was inserted, or -1 if
   *     it was already present in the list.
   */
  static public int sortedInsert(List<Resolved> list, Resolved taxon) {
    Preconditions.checkNotNull(list);
    Preconditions.checkNotNull(taxon);
    
    int index = Collections.binarySearch(list, taxon, new ResolvedComparator());
    if (index >= 0) {
      return -1;
    }
    
    index = -(index + 1);
    list.add(index, taxon);
    return index;  
  }
  
  static public int countChildren(
      Taxon taxon, Taxon.Type type, Checklist checklist, Checklist.Status... excluding) {
    return countChildren(taxon, type, checklist, ImmutableSet.copyOf(excluding));
  }
  
  static public int countFamilies(
      Taxonomy taxonomy, @Nullable Checklist checklist, Checklist.Status... excluding) {
    if (checklist == null) {
      AtomicInteger familyCount = new AtomicInteger();
      // Assume that - for a global checklist - there's something in each family for every status.
      // This is not true, for example, if "status" is extinct and family is Mohoidae.  <Sigh>.
      TaxonUtils.visitTaxa(taxonomy, (Taxon taxon) -> {
        if (taxon.getType() == Taxon.Type.family) {
          familyCount.incrementAndGet();
          return false;
        }
        return true;
      });
      return familyCount.get();
    } else {
      ImmutableSet<Checklist.Status> invalidStatuses = ImmutableSet.copyOf(excluding);
      long count = checklist.getTaxa(taxonomy).stream()
          // Skip anything with an invalid status
         .filter(taxon -> !invalidStatuses.contains(checklist.getStatus(taxonomy, taxon)))
          // ... and then map up to the family level
         .map(taxon -> taxon.resolveInternal(taxonomy))
         .map(resolved -> resolved.getParentOfAtLeastType(Taxon.Type.family))
         // and count the number of distinct elements
         .distinct()
         .count();
      return (int) count;
    }
    
  }
  
  static public int countChildren(
      Taxon taxon, Taxon.Type type, Checklist checklist, Set<Checklist.Status> excluding) {
    int total = 0;
    for (Taxon child : taxon.getContents()) {
      if (child.getType() == type) {
        if (checklist == null || checklist.includesTaxon(child, excluding)) {
          total++;
        }
      } else {
        total += countChildren(child, type, checklist, excluding);
      }
    }

    return total;
  }

  /** Return the first child of a given type with a Taxon, or null if there isn't one. */ 
  public static Taxon getFirstChildOfType(Taxon taxon, final Taxon.Type type) {
    final AtomicReference<Taxon> foundTaxon = new AtomicReference<Taxon>();
    visitTaxa(taxon, new TaxonVisitor() {
      @Override public boolean visitTaxon(Taxon taxon) {
        if (foundTaxon.get() != null) {
          return false;          
        }
        
        if (taxon.getType() == type) {
          foundTaxon.set(taxon);
          return false;
        }
        
        return true;
      }
    });
    return foundTaxon.get();
  }

  public static void visitTaxa(Taxonomy taxonomy, TaxonVisitor visitor) {
    visitTaxa(taxonomy.getRoot(), visitor);
  }
  
  public static void visitTaxa(Taxon taxon, TaxonVisitor visitor) {
    if (visitor.visitTaxon(taxon)) {
      for (Taxon child : taxon.getContents()) {
        visitTaxa(child, visitor);
      }      
    }
  }

  /**
   * Given a collection of taxa, return a set of taxa ID
   * normalized to a taxonomic level.  (E.g., return all
   * species in a set of families, species, and subspecies).
   * The returned set of taxa ID will all be at exactly that level.
   * <p>
   * This drops all non-single Taxa, which must be processed separately.
   */
  static public Set<String> getTaxaAtLevel(
      Iterable<Resolved> taxa,
      Taxon.Type type) {
    Set<String> set = Sets.newHashSet();
    for (Resolved taxon : taxa) {
      if (taxon.getType() == SightingTaxon.Type.SINGLE) {
        // Drop anything that is at a larger level
        if (taxon.getSmallestTaxonType().compareTo(type) <= 0) {
          Taxon parent = TaxonUtils.getParentOfTypeOrNull(taxon.getTaxon(), type);
          if (parent != null) {
            set.add(parent.getId());
          }
        }
      } else {
        // For non-single, raise to the desired level, and if at *that* level it's
        // a single taxon, then include.
        if (taxon.getSmallestTaxonType().compareTo(type) < 0) {
          // TODO: if this is called for a "group" level, but elevates to a "species" level, what breaks?
          SightingTaxon parent = taxon.getParentOfAtLeastType(type);
          if (parent.getType() == SightingTaxon.Type.SINGLE) {
            set.add(parent.getId());
          }
        }
      }
    }
    
    return set;
  }
  
  /**
   * Given a collection of IDs, returns a List of all taxa in
   * that, optionally adding in the families in that collection,
   * in taxonomic order.
   * @param taxonomy the taxonomy all must match
   * @param ids the collection of String IDs;  should have a fast
   *     contains() implementation so this is not O(N).
   * @param includeFamilies if true, the returned list will include
   *     the family ancestors of each contained taxon
   */
  static public List<Taxon> getTaxa(
      Taxonomy taxonomy,
      Collection<String> ids,
      boolean includeFamilies) {
    List<Taxon> list =  new ArrayList<Taxon>(ids.size());
    _addTaxa(list, taxonomy, taxonomy.getRoot(), ids, includeFamilies, null);
    return list;
  }

  static private boolean _addTaxa(
      List<Taxon> list,
      Taxonomy taxonomy,
      Taxon taxon,
      Collection<String> ids,
      boolean includeFamilies,
      Taxon currentFamily) {
    boolean added = false;
    if (ids.contains(taxonomy.getId(taxon))) {
      if (includeFamilies && (currentFamily != null)) {
        list.add(currentFamily);
      }
      
      list.add(taxon);
      
      added = true;
      currentFamily = null;
    }

    if (taxon.getType() == Taxon.Type.family) {
      currentFamily = taxon;
    }

    for (Taxon child : taxon.getContents()) {
      if (_addTaxa(list, taxonomy, child, ids, includeFamilies, currentFamily)) {
        added = true;
        currentFamily = null;
      }
    }
    
    return added;
  }


  public static String getRange(Species species) {
    if (species.getRange() != null) {
      return species.getRange();
    }
    
    StringBuilder rangesOfChildren = new StringBuilder();
    for (Taxon child : species.getContents()) {
      if (!(child instanceof Species)) {
        continue;
      }
      
      Species childSpecies = (Species) child;
      String childRange = TaxonUtils.getRange(childSpecies);
      if (childRange != null) {
        if (rangesOfChildren.length() > 0) {
          rangesOfChildren.append(";");
        }

        rangesOfChildren.append(childRange);
      }
    }
    
    if (rangesOfChildren.length() == 0) {
      return null;
    }
    
    return rangesOfChildren.toString();
  }

  /**
   * Find the worst status of this taxon or any of its parents (up to species).
   * Specifically, omitted status is considered "LC", but that's not really relevant
   * for subspecies/groups (since status is almost always just not available at that
   * level, and if the species is endangered, all its subspecies are that or worse.) 
   */
  public static Status getTaxonStatus(Taxon taxon) {
    Status status = Status.LC;
    do {
      Status taxonStatus = ((Species) taxon).getStatus();
      if (taxonStatus.compareTo(status) > 0) {
        status = taxonStatus;
      }
      taxon = taxon.getParent();
    } while (taxon.getType().compareTo(Taxon.Type.species) <= 0);
    
    return status;
  }
  
  public static Taxon getSharedParent(Taxon first, Taxon second) {
    if (first == second) {
      return first;
    }
    
    if (first.getType() == second.getType()) {
      return getSharedParent(first.getParent(), second.getParent());
    }
    
    if (first.getType().compareTo(second.getType()) < 0) {
      return getSharedParent(first.getParent(), second);
    } else {
      return getSharedParent(first, second.getParent());
    }
  }
  
  /**
   * Returns the previous sibling of a taxon.
   */
  public static Taxon getPreviousSibling(Taxon taxon) {
    Taxon parent = taxon.getParent();
    if (parent == null) {
      return null;
    }
    
    Taxon previous = null;
    for (Taxon child : parent.getContents()) {
      if (child == taxon) {
        return previous;
      }
      
      previous = child;
    }
    
    return null;
  }

  /**
   * Returns the next sibling of a taxon.
   */
  public static Taxon getNextSibling(Taxon taxon) {
    Taxon parent = taxon.getParent();
    if (parent == null) {
      return null;
    }
    
    boolean found = false;
    for (Taxon child : parent.getContents()) {
      if (found) {
        return child;
      }
      
      if (child == taxon) {
        found = true;
      }
    }
    
    return null;
  }

  public static Taxonomy getBaseTaxonomy(Taxonomy taxonomy) {
    if (taxonomy instanceof MappedTaxonomy) {
      return ((MappedTaxonomy) taxonomy).getBaseTaxonomy();
    }
    return taxonomy;
  }

  public static boolean areCompatible(Taxonomy taxonomy, Taxonomy taxonomy2) {
    return getBaseTaxonomy(taxonomy) == getBaseTaxonomy(taxonomy2);
  }
  
  /**
   * Given a set of IDs, and a taxonomy, returns a set where all of the IDs are at the
   * same (necessarily, higher) level.
   */
  public static Set<String> mapToCommonLevel(Taxonomy taxonomy, Set<String> taxonIds) {
    Taxon.Type highestType = null;
    boolean multipleTypes = false;
    List<Taxon> taxa = Lists.newArrayList();
    for (String taxonId : taxonIds) {
      Taxon taxon = taxonomy.getTaxon(taxonId);
      taxa.add(taxon);
      if (highestType == null) {
        highestType = taxon.getType();
      } else if (highestType != taxon.getType()) {
        multipleTypes = true;
        if (taxon.getType().compareTo(highestType) > 0) {
          highestType = taxon.getType();
        }
      }
    }
    
    if (multipleTypes) {
      taxonIds = Sets.newLinkedHashSet();
      for (Taxon taxon : taxa) {
        taxonIds.add(TaxonUtils.getParentOfType(taxon, highestType).getId());
      }
    }
    
    return taxonIds;
  }

  /** 
   * Returns all descendants of a given type.
   */
  public static List<Taxon> getDescendantsOfType(Taxon taxon, Type type) {
    List<Taxon> list = new ArrayList<>(); 
    TaxonUtils.visitTaxa(taxon, new TaxonVisitor() {
      @Override
      public boolean visitTaxon(Taxon taxon) {
        if (taxon.getType() == type) {
          list.add(taxon);
          return false;
        }
        return true;
      }
    });
    
    return list;
  }
    
  /**
   * Given a set of taxa, attempts to return a simpler definition.  This is particularly
   * useful for cases where earlier IOC mappings required long lists of subspecies, but
   * changes to eBird/Clements taxonomy now supports simple group or even species mappings,
   * and also 
   * <p>
   * The input set of taxa must be IDs in the associated taxonomy.
   */
  public static Collection<String> simplifyTaxa(
      Taxonomy taxonomy, Collection<String> taxa) {
    // Nothing simpler than 1 species!
    if (taxa.size() == 1) {
      return taxa;
    }
    
    for (String id : taxa) {
      Taxon taxon = taxonomy.getTaxon(id);
      // Can't simplify species.
      if (taxon.getType() == Taxon.Type.species) {
        continue;
      }
      
      // Gather all the children of this taxon's parents
      Taxon parent = taxon.getParent();
      Set<String> childIds = Sets.newLinkedHashSet();
      for (Taxon child : parent.getContents()) {
        childIds.add(child.getId());
      }
      
      // If every child of the parent is present, then clearly removing the children
      // and replacing with just the parent would be simpler
      if (taxa.containsAll(childIds)) {
        Set<String> simplifiedIds = Sets.newLinkedHashSet(taxa);
        simplifiedIds.removeAll(childIds);
        simplifiedIds.add(parent.getId());
        // Return early, but only after giving the code another chance to simplify taxa.  It
        // can take a few recursions to simplify extremely over-complex taxa
        return simplifyTaxa(taxonomy, simplifiedIds);
      }
    }
    
    // No simplification found.
    return taxa;
  }
}
