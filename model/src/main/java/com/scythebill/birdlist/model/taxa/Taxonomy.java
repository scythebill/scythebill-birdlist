/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.taxa;

import java.util.Collection;

import com.google.common.collect.BiMap;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.names.LocalNames;
import com.scythebill.birdlist.model.util.AlternateName;
import com.scythebill.birdlist.model.util.Indexer;

/**
 * A collection of Taxon objects, responsible for identifying the IDs of the
 * taxa.
 *
 */
public interface Taxonomy {
  /**
   * Get the unique ID of this taxonomy, used to make sure reports
   * aren't opened against incorrect taxonomies.
   */
  String getId();

  /**
   * Get a user-readable name of the taxonomy.
   */
  String getName();

  /**
   * Gets additional credits for the taxonomy file (like IUCN Red List data).
   */
  Collection<String> additionalCredits();
  
  /**
   * Return the root Taxon, for example Aves.
   */
  Taxon getRoot();

  /**
   * Returns the ID of a taxon, or null if the taxon is not registered.
   */
  String getId(Taxon taxon);

  /**
   * Returns the Taxon for an ID, or null if there is no taxon with that ID in this taxonomy.
   */
  Taxon getTaxon(String id);

  /**
   * Return an unmodifiable view of the taxonomy.
   */
  BiMap<String, Taxon> asBimap();

  /**
   * A utility function to find a species given its full (scientific) name.
   * TODO(awiner): extract from this interface
   */
  Taxon findSpecies(String fullName);

  /**
   * Registers a new species, adding a default ID.
   * @throws IllegalArgumentException if taxon is already present
   */
  void registerWithNewId(Taxon taxon);

  /**
   * Removes a taxon from this grouping.
   * @param taxon
   */
  void unregister(Taxon taxon);

  /**
   * Return the total count of species.
   */
  int getSpeciesCount();

  Resolved resolveInto(SightingTaxon sightingTaxon);

  /** Stores a common-name indexer. */
  void setCommonIndexer(Indexer<String> commonIndexer);

  /**
   * Gets a common-name indexer.
   */
  Indexer<String> getCommonIndexer();
  
  /** Stores a scientific-name indexer. */
  void setScientificIndexer(Indexer<String> sciIndexer);

  /** Gets a scientific-name indexer. */
  Indexer<String> getScientificIndexer();

  /** Stores a common-name indexer. */
  void setAlternateCommonIndexer(Indexer<AlternateName<String>> alternateCommonIndexer);

  /**
   * Gets a localized common-name indexer.
   */
  Indexer<String> getLocalizedCommonIndexer();

  /**
   * Gets a common-name indexer.
   */
  Indexer<AlternateName<String>> getAlternateCommonIndexer();
  
  /** Stores a scientific-name indexer. */
  void setAlternateScientificIndexer(Indexer<AlternateName<String>> alternateSciIndexer);

  /** Gets a scientific-name indexer. */
  Indexer<AlternateName<String>> getAlternateScientificIndexer();

  /** Converts a taxon ID to a SightingTaxon in the base taxonomy. */
  SightingTaxon toBaseSightingTaxon(String id);
  
  /** Returns the local names database for this taxonomy. */
  LocalNames getLocalNames();
  
  /** Returns true if a taxonomy is built-in. */
  boolean isBuiltIn();
  
  /** Returns a URL to display a taxon account. */
  String getTaxonAccountUrl(String taxonAccountId);

  /** Return the "title" of an account link. typically just the name of a site (eBird, iNaturalist). */
  String getAccountLinkTitle();
}
