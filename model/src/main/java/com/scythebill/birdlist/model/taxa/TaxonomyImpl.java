/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.taxa;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.CharMatcher;
import com.google.common.base.Preconditions;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.names.LocalNames;
import com.scythebill.birdlist.model.util.AlternateName;
import com.scythebill.birdlist.model.util.Indexer;

/** Base implementation of Taxonomy. */
public class TaxonomyImpl implements Taxonomy {
  private Taxon root;
  private final BiMap<String, Taxon> taxa = HashBiMap.create();
  private final List<String> additionalCredits = Lists.newArrayList();
  private final String name;
  private final String id;
  private LocalNames localNames = LocalNames.trival();
  private volatile int speciesCount = 0;
  private volatile int taxonCount = 0;
  private volatile Indexer<String> commonIndexer;
  private volatile Indexer<String> scientificIndexer;
  private final String accountUrlFormat;
  private final String accountLinkTitle;
  
  public TaxonomyImpl(String id, String name, String accountUrlFormat, String accountLinkTitle) {
    this.id = id;
    this.name = name;
    this.accountUrlFormat = accountUrlFormat;
    this.accountLinkTitle = accountLinkTitle;
  }
  
  public TaxonomyImpl() {
    this(null, null, null, null);
  }

  /**
   * Assigns the root taxon, and marks the root as built.
   */
  public void setRoot(Taxon taxon) {
    Preconditions.checkState(root == null, "Root to taxonomy already set");

    root = taxon;
  }

  @Override
  public Taxon getRoot() {
    return root;
  }

  @Override
  public Taxon findSpecies(String fullName) {
    int space = fullName.indexOf(' ');
    if (space < 1) {
      throw new IllegalArgumentException("'" + fullName + "' does not contain a space");
    }
    String genusName = fullName.substring(0, space);
    Taxon genus = root.findByName(genusName, Type.genus);

    if (genus == null) {
      return null;
    }

    String speciesName = fullName.substring(space + 1);
    return genus.findByName(speciesName, Type.species);
  }

  @Override
  public String getId(Taxon taxon) {
    return taxa.inverse().get(taxon);
  }

  @Override
  public Taxon getTaxon(String id) {
    return taxa.get(id);
  }

  private final static CharMatcher ILLEGAL_ID_CHARACTERS = CharMatcher.anyOf("|,");

  public void register(Taxon taxon, String id) {
    if ((taxon == null) || (id == null)) {
      throw new NullPointerException();
    }

    if (taxon.getType() == Type.species) {
      speciesCount++;
    }
    
    if (ILLEGAL_ID_CHARACTERS.matchesAnyOf(id)) {
      throw new IllegalArgumentException("Illegal ID: " + id);
    }

    Taxon existingTaxonForKey = taxa.put(id, taxon);
    if (existingTaxonForKey != null) {
      // Restore the earlier state (though in practice this is irrelevant)
      taxa.put(id, existingTaxonForKey);
      throw new IllegalArgumentException(id + " is already present as " + existingTaxonForKey);
    }
    
    ((TaxonImpl) taxon).setIndex(taxonCount ++);
  }

  @Override
  public void registerWithNewId(Taxon taxon) {
    Preconditions.checkNotNull(taxon);

    String id = _calculateId(taxon);

    register(taxon, id);
  }

  @Override
  public void unregister(Taxon taxon) {
    Preconditions.checkArgument(taxon.getTaxonomy() == this, "Not registered with this taxonomy");
    taxa.inverse().remove(taxon);

    if (taxon.getType() == Type.species) {
      speciesCount--;
    }
  }

  @Override
  public BiMap<String, Taxon> asBimap() {
    return Maps.unmodifiableBiMap(taxa);
  }

  private String _calculateId(Taxon taxon) {
    StringBuilder builder = new StringBuilder(16);
    builder.append(ID_PREFIXES.get(taxon.getType()));
    int shortNameLength = 3;
    switch (taxon.getType()) {
      case species:
      case group:
      case subspecies:
        Taxon parent = taxon.getParent();
        if (parent == null) {
          throw new NullPointerException("" + taxon + " does not have a parent.");
        }
        String parentPrefix = ID_PREFIXES.get(parent.getType());
        String parentName = parent.getId().substring(parentPrefix.length());
        builder.append(parentName);
        break;
      case genus:
        shortNameLength = 5;
        break;

      default:
        break;
    }

    builder.append(_getShortName(taxon.getName(), shortNameLength));
    int baseLength = builder.length();

    String id = null;

    for (int i = 0; true; i++) {
      if (i > 0) {
        builder.append(Integer.toString(i));
      }
      id = builder.toString();
      if (!taxa.containsKey(id)) {
        break;
      }

      builder.setLength(baseLength);
    }

    return id;
  }

  static private String _getShortName(String fullName, int shortNameLength) {
    if (fullName == null) {
      return "";
    }
    return CharMatcher.whitespace().trimFrom(
        fullName.substring(0, Math.min(shortNameLength, fullName.length())));
  }
  static private final Map<Taxon.Type, String> ID_PREFIXES = new HashMap<Type, String>();
  private Indexer<AlternateName<String>> alternateCommonIndexer;
  private Indexer<AlternateName<String>> alternateSciIndexer;

  static {
    ID_PREFIXES.put(Taxon.Type.classTaxon, "class");
    ID_PREFIXES.put(Taxon.Type.family, "fam");
    ID_PREFIXES.put(Taxon.Type.genus, "ge");
    ID_PREFIXES.put(Taxon.Type.order, "ord");
    ID_PREFIXES.put(Taxon.Type.phylum, "phylum");
    ID_PREFIXES.put(Taxon.Type.species, "sp");
    ID_PREFIXES.put(Taxon.Type.group, "gr");
    ID_PREFIXES.put(Taxon.Type.subspecies, "ssp");
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override public int getSpeciesCount() {
    return speciesCount;
  }

  @Override public Collection<String> additionalCredits() {
    return Collections.unmodifiableList(additionalCredits);
  }
  
  public void addAdditionalCredit(String additionalCredit) {
    additionalCredits.add(additionalCredit);
  }

  @Override
  public Resolved resolveInto(SightingTaxon sightingTaxon) {
    // TODO: if sightingTaxon happened to be an IOC sightingTaxon,
    // You Will Not Go To Space Today.
    return sightingTaxon.resolveInternal(this);
  }

  @Override
  public void setCommonIndexer(Indexer<String> commonIndexer) {
    this.commonIndexer = commonIndexer;
  }

  @Override
  public Indexer<String> getCommonIndexer() {
    return commonIndexer; 
  }

  @Override
  public void setScientificIndexer(Indexer<String> sciIndexer) {
    this.scientificIndexer = sciIndexer;
  }

  @Override
  public Indexer<String> getScientificIndexer() {
    return scientificIndexer;
  }

  @Override
  public SightingTaxon toBaseSightingTaxon(String id) {
    return SightingTaxons.newSightingTaxon(id);
  }

  @Override
  public void setAlternateCommonIndexer(Indexer<AlternateName<String>> alternateCommonIndexer) {
    this.alternateCommonIndexer = alternateCommonIndexer;
  }

  @Override
  public Indexer<AlternateName<String>> getAlternateCommonIndexer() {
    return alternateCommonIndexer;
  }

  @Override
  public void setAlternateScientificIndexer(Indexer<AlternateName<String>> alternateSciIndexer) {
    this.alternateSciIndexer = alternateSciIndexer;
  }

  @Override
  public Indexer<AlternateName<String>> getAlternateScientificIndexer() {
    return alternateSciIndexer;
  }

  @Override
  public LocalNames getLocalNames() {
    return localNames;
  }
  
  public void setLocalNames(LocalNames localNames) {
    this.localNames = Preconditions.checkNotNull(localNames);
  }

  @Override
  public Indexer<String> getLocalizedCommonIndexer() {
    return localNames.getIndexer();
  }

  @Override
  public boolean isBuiltIn() {
    return true;
  }
  
  @Override
  public String getTaxonAccountUrl(String taxonAccountId) {
    if (accountUrlFormat == null) {
      return null;
    }
    
    return accountUrlFormat.replace("{id}", taxonAccountId);
  }
  
  @Override
  public String getAccountLinkTitle() {
    return accountLinkTitle;
  }
}
