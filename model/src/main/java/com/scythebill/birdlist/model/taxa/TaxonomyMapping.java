package com.scythebill.birdlist.model.taxa;

import com.scythebill.birdlist.model.sighting.SightingTaxon;

/**
 * A loaded taxonomy mapping.
 */
public interface TaxonomyMapping {
  /** Returns the mapping for a single taxon. */
  SightingTaxon mapSingleTaxon(String id);
  /** Returns true if the taxon deserves a warning. */
  boolean shouldWarn(SightingTaxon newTaxonId);
  /** Returns the mapping for a "single-with-secondary-subspecies", or null if no explicit mapping exists. */ 
  SightingTaxon mapSingleWithSecondarySubspecies(SightingTaxon sightingTaxon);
  /** Returns the taxonomy ID from which this maps. */
  String getPreviousTaxonomyId();
}
