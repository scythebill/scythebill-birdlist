/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.taxa;

import java.io.IOException;
import java.io.Reader;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;

import com.google.common.base.Splitter;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.io.CharSource;
import com.opencsv.CSVReader;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxons;

/**
 * A loader for a single taxonomy mapping.
 */
public class TaxonomyMappingLoader {
  private final static Splitter MULTI_SPLITTER = Splitter.on(','); 
  
  private final String id;
  private final CharSource mappingSupplier;
  private final CharSource iocSspSupplement;

  public TaxonomyMappingLoader(
      String id,
      CharSource mappingSupplier,
      @Nullable CharSource iocSspSupplement) {
    this.id = id;
    this.mappingSupplier = mappingSupplier;
    this.iocSspSupplement = iocSspSupplement;
  }
  
  public String getId() {
    return id;
  }

  public TaxonomyMapping loadMapping() throws IOException {
    return new LoadedMapping();
  }

  private class LoadedMapping implements TaxonomyMapping {
    private final Map<String, SightingTaxon> mapping = Maps.newHashMap();
    private final Map<SightingTaxon, SightingTaxon> sspSupplement = Maps.newHashMap();
    private final Set<SightingTaxon> warnings = Sets.newHashSet();

    LoadedMapping() throws IOException {
      Reader input = mappingSupplier.openBufferedStream();
      CSVReader csv = new CSVReader(input);
      try {
        String[] line;
        while ((line = csv.readNext()) != null) {
          String from = line[1];
          String to = line[2];
          if (!from.isEmpty() && !to.isEmpty()) {
            if (mapping.containsKey(from)) {
              throw new IllegalStateException("Two mappings for " + from);
            }
  
            // Sp. mappings
            if ("MULT".equals(line[0])) {
              Iterable<String> split = MULTI_SPLITTER.split(to);
              SightingTaxon spTaxon = SightingTaxons.newSpTaxon(split);
              mapping.put(from, spTaxon);
            } else {
              mapping.put(from, SightingTaxons.newSightingTaxon(to));
            }
          }
          
          if ("WARN".equals(line[0])) {
            warnings.add(SightingTaxons.newSightingTaxon(line[2]));
          }
        }
      } finally {
        csv.close();
      }
      
      if (iocSspSupplement != null) {
        Reader supplementInput = iocSspSupplement.openBufferedStream();
        CSVReader supplementCsv = new CSVReader(supplementInput);
        try {
          String[] line;
          while ((line = supplementCsv.readNext()) != null) {
            SightingTaxon iocSsp = SightingTaxons.newSightingTaxonWithSecondarySubspecies(
                line[0], line[1]);
            SightingTaxon mappedSsp = SightingTaxons.newSightingTaxon(line[2]);
            sspSupplement.put(iocSsp, mappedSsp);
          }
        } finally {
          supplementCsv.close();
        }
      }
    }

    @Override
    public SightingTaxon mapSingleTaxon(String id) {
      return mapping.get(id);
    }

    @Override
    public SightingTaxon mapSingleWithSecondarySubspecies(
        SightingTaxon sightingTaxon) {
      return sspSupplement.get(sightingTaxon);
    }    

    @Override
    public boolean shouldWarn(SightingTaxon newTaxonId) {
      return warnings.contains(newTaxonId);
    }

    @Override
    public String getPreviousTaxonomyId() {
      return getId();
    }
  }
}
