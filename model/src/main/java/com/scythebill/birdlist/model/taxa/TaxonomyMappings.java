/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.taxa;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.sighting.SightingTaxon;

/**
 * Registers all current mappings.
 */
public class TaxonomyMappings {
  private final Map<String, TaxonomyMappingLoader> mappingLoaders;
  private final Map<String, IocUpgradeLoader> iocLoaders;
  private final Checklists checklists;

  @Inject
  public TaxonomyMappings(
      Set<TaxonomyMappingLoader> mappingLoaders,
      Set<IocUpgradeLoader> iocLoaders,
      Checklists checklists) {
    this.checklists = checklists;
    this.mappingLoaders = mappingLoaders.stream().collect(
        Collectors.toMap(TaxonomyMappingLoader::getId, l -> l));
    this.iocLoaders = iocLoaders.stream().collect(
        Collectors.toMap(IocUpgradeLoader::getId, l -> l));
  }
  
  public UpgradeTracker getTracker(
      String recordedTaxonomyId, MappedTaxonomy mappedTaxonomy) throws IOException {
    TaxonomyMappingLoader loader = mappingLoaders.get(recordedTaxonomyId);
    if (loader == null) {
      throw new UnsupportedTaxonomyException(
          "No mapping for taxonomy " + recordedTaxonomyId, recordedTaxonomyId);
    }
    TaxonomyMapping mapping = loader.loadMapping();
    return new UpgradeTrackerImpl(checklists, mappedTaxonomy, mapping);
  }
  
  public ImmutableSet<SightingTaxon> getIocAffectedSightingTaxa(String recordedIocTaxonomy) throws IOException {
    IocUpgradeLoader iocUpgradeLoader = iocLoaders.get(recordedIocTaxonomy);
    if (iocUpgradeLoader == null) {
      return ImmutableSet.of();
    }
    
    return iocUpgradeLoader.affectedSightingTaxa();
  }

  public void registerMapping(TaxonomyMappingLoader mapping, String fromId) {
    mappingLoaders.put(fromId, mapping);
  }
}
