/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.taxa;

import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.SightingTaxon;

/** Handles a single taxonomy upgrade */
public interface UpgradeTracker {
  /** Return the SightingTaxon for an upgrade of a single taxon. */
  SightingTaxon getTaxonId(String recordedTaxonId, boolean shouldWarn);

  /** Return the SightingTaxon for an upgrade of a single taxon. */
  SightingTaxon getTaxonIdOfSightingWithSsp(SightingTaxon recordedTaxonid, boolean shouldWarn);

  /**
   * Return the SightingTaxon for an upgrade of a single taxon, but aggressively
   * resolve against a built-in checklist to eliminate "sp." sightings.
   */
  SightingTaxon getChecklistResolvedTaxonId(Taxonomy taxonomy, String recordedTaxonId, Location location);

  /** Return the set of a taxa IDs for an incoming set of IDs. */
  Iterable<String> getTaxonId(Taxonomy taxonomy, Iterable<String> taxa, boolean shouldWarn);
  
  CompletedUpgrade getCompletedUpgrade();
}