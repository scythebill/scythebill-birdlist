/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.taxa;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Type;
import com.scythebill.birdlist.model.sighting.SightingTaxons;

class UpgradeTrackerImpl implements UpgradeTracker, CompletedUpgrade {
  private final Set<SightingTaxon> needsWarning = Sets.newHashSet();
  private final Set<SightingTaxon> sps = Sets.newHashSet();
  private final Checklists checklists;
  private final MappedTaxonomy mappedTaxonomy;
  private final TaxonomyMapping mapping;
  private ImmutableSet<SightingTaxon> additionalSps;
  private Taxonomy taxonomyForAdditionalSps;

  UpgradeTrackerImpl(
      Checklists checklists, MappedTaxonomy mappedTaxonomy, TaxonomyMapping mapping) throws IOException {
    this.checklists = checklists;
    this.mappedTaxonomy = mappedTaxonomy;
    this.mapping = mapping;
  }

  @Override
  public SightingTaxon getTaxonId(String recordedTaxonId, boolean shouldWarn) {
    // Return the species, and if it's of interest (for warning or "sp."),
    // then note it accordingly.
    SightingTaxon newTaxonId = getTaxonIdInternal(recordedTaxonId);
    if (shouldWarn) {
      if (mapping.shouldWarn(newTaxonId)) {
        needsWarning.add(newTaxonId);
      }
      if (newTaxonId.getType() == Type.SP) {
        sps.add(newTaxonId);
      }
    }
    return newTaxonId;
  }
  
  /** Find the mapped species, without any particular warnings or "sp." */
  private SightingTaxon getTaxonIdInternal(String recordedTaxonId) {
    return mapping.mapSingleTaxon(recordedTaxonId);
  }

  @Override
  public SightingTaxon getTaxonIdOfSightingWithSsp(SightingTaxon recordedTaxonId, boolean shouldWarn) {
    // TODO: send all SightingTaxon types through this one chokepoint
    Preconditions.checkState(recordedTaxonId.getType() ==
        SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES);
    
    // First, handle built-in, explicit mappings of ssps
    SightingTaxon explicitMapping = mapping.mapSingleWithSecondarySubspecies(recordedTaxonId);
    if (explicitMapping != null) {
      return explicitMapping;
    }
    
    // Second, figure out what new Clements taxon the primary key points to 
    SightingTaxon mainTaxonMapping = getTaxonId(recordedTaxonId.getId(), shouldWarn);
    // ... then map *that* back to an IOC form
    Resolved resolved = mainTaxonMapping.resolve(mappedTaxonomy);
    if (resolved == null) {
      // Bail early, but this should never happen (it means the Clements form
      // can't be represented in IOC).
      return mainTaxonMapping;
    }
    
    Taxon foundSubspecies = null;
    for (Taxon iocTaxon : resolved.getTaxa()) {
      // ... walk the IOC form back up to a species, and look for a subspecies
      // with a matching name
      foundSubspecies = findSubspecies(
          TaxonUtils.getParentOfType(iocTaxon, Taxon.Type.species),
          recordedTaxonId.getSubIdentifier());
      // Found it!  Break.
      if (foundSubspecies != null) {
        break;
      }
    }
    
    // Couldn't find it, alas.
    if (foundSubspecies == null) {
      return attemptToPreserveSspMapping(recordedTaxonId, mainTaxonMapping);
    }
    
    // Found it!
    SightingTaxon newMapping = mappedTaxonomy.getMapping(foundSubspecies);
    if (newMapping == null) {
      // ... but the new form isn't mapped.  Return the ssp. and move on
      return attemptToPreserveSspMapping(recordedTaxonId, mainTaxonMapping);
    }
    
    // A real mapping, eureka - return it!
    return newMapping;
  }

  private SightingTaxon attemptToPreserveSspMapping(SightingTaxon recordedTaxonId,
      SightingTaxon mainTaxonMapping) {
    // If the Clements form is a "single", we can at least keep around the subidentifier
    // in hopes of later resurrections
    if (mainTaxonMapping.getType() == SightingTaxon.Type.SINGLE) {
      return SightingTaxons.newSightingTaxonWithSecondarySubspecies(
          mainTaxonMapping.getId(), recordedTaxonId.getSubIdentifier());
    }
    // ... but if it's a Sp., game over, just return the new mapping.
    return mainTaxonMapping;
  }

  /** Search for a subspecies with a given name, */
  private Taxon findSubspecies(Taxon iocTaxon, String subIdentifier) {
    if (iocTaxon.getType() == Taxon.Type.subspecies) {
      if (subIdentifier.equals(iocTaxon.getName())) {
        return iocTaxon;
      }
    }
    for (Taxon child : iocTaxon.getContents()) {
      Taxon found = findSubspecies(child, subIdentifier);
      if (found != null) {
        return found;
      }
    }
    return null;
  }

  @Override
  public SightingTaxon getChecklistResolvedTaxonId(
      Taxonomy taxonomy, String recordedTaxonId, Location location) {
    // Map the sighting to a taxon
    SightingTaxon newTaxonId = mapping.mapSingleTaxon(recordedTaxonId);
    // ... and if it's a SP.
    if (newTaxonId.getType() == Type.SP) {
      Checklist checklist = checklists.getNearestBuiltInChecklist(
          taxonomy, /*reportset=*/null, location);
      if (checklist != null) {
        Set<String> plausibleIds = Sets.newHashSet();
        for (String id : newTaxonId.getIds()) {
          SightingTaxon sightingTaxon = SightingTaxons.newSightingTaxon(id);
          Resolved resolved = sightingTaxon.resolve(taxonomy);
          // Terminate early if it's not a species, because checklists only resolve down
          // to the species level.
          if (resolved.getSmallestTaxonType() != Taxon.Type.species) {
            return newTaxonId;
          }
          
          if (checklist.getStatus(taxonomy, sightingTaxon) != null) {
            plausibleIds.add(id);
          }
        }
        
        // None of the taxa are in the checklists.  Give up!
        if (plausibleIds.isEmpty()) {
          return newTaxonId;
        }
        
        return SightingTaxons.newPossiblySpTaxon(plausibleIds);
      }
    }
    
    return newTaxonId;
  }

  @Override
  public Collection<SightingTaxon> warnings() {
    return Collections.unmodifiableSet(needsWarning);
  }

  @Override
  public Collection<SightingTaxon> sps(Taxonomy taxonomy) {
    if (taxonomyForAdditionalSps != null && additionalSps != null && taxonomy == taxonomyForAdditionalSps) {
      return Collections.unmodifiableSet(Sets.union(sps, additionalSps));
    }
    return Collections.unmodifiableSet(sps);
  }

  @Override
  public String getPreviousTaxonomyId() {
    return mapping.getPreviousTaxonomyId();
  }

  @Override
  public Iterable<String> getTaxonId(Taxonomy taxonomy, Iterable<String> taxa, boolean shouldWarn) {
    // First, map all of the taxa.
    Set<String> mappedTaxa = Sets.newLinkedHashSet();
    for (String taxon : taxa) {
      mappedTaxa.addAll(getTaxonIdInternal(taxon).getIds());
    }
    
    return TaxonUtils.simplifyTaxa(mappedTaxonomy.getBaseTaxonomy(), mappedTaxa);
  }

  @Override
  public CompletedUpgrade getCompletedUpgrade() {
    return this;
  }

  @Override
  public String getNewTaxonomyName() {
    return mappedTaxonomy.getBaseTaxonomy().getName();
  }

  @Override
  public Taxonomy getForcedTaxonomy() {
    return null;
  }

  @Override
  public void applyAdditionalSps(Taxonomy onlyForTaxonomy,
      ImmutableSet<SightingTaxon> additionalSps) {
    this.additionalSps = additionalSps;
    this.taxonomyForAdditionalSps = onlyForTaxonomy;
  }    
}
