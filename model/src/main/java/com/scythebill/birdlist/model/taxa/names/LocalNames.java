/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.taxa.names;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import com.google.common.base.Preconditions;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.inject.Singleton;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences.AvailableClementsLocale;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences.AvailableIocLocale;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences.AvailableLocale;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences.LocaleOption;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences.ScientificOrCommon;
import com.scythebill.birdlist.model.util.Indexer;

/**
 * Utilities for loading and using localized names.
 */
@Singleton
public class LocalNames {
  private final NamesLoader loader;
  private final NamesPreferences namesPreferences;
  private final LoadingCache<String, Map<String, String>> namesCache =
      CacheBuilder.newBuilder().maximumSize(2).build(new CacheLoader<String, Map<String, String>>() {
        @Override
        public Map<String, String> load(String locale) throws Exception {
          return loader.load(locale);
        }        
      });
  private final LoadingCache<String, Indexer<String>> indexerCache =
      CacheBuilder.newBuilder().maximumSize(2).build(new CacheLoader<String, Indexer<String>>() {
        @Override
        public Indexer<String> load(String locale) throws Exception {
          return newIndexer(namesCache.get(locale));
        }        
      });

  private static final LocalNames TRIVIAL = new LocalNames(new NamesLoader() {
    @Override public Map<String, String> load(String locale) {
      return ImmutableMap.of();
    }

    @Override
    public Set<String> findPresentNames(String locale, Set<String> taxonNames) {
      return ImmutableSet.of();
    }

  }, new NamesPreferences(), LocaleOption.CLEMENTS); // CLEMENTS isn't "right", but we don't care here
  private LocaleOption localeOption;
  
  public static LocalNames trival() {
    return TRIVIAL;
  }
  
  public static LocalNames trival(NamesPreferences namesPreferences) {
    return new LocalNames(new NamesLoader() {
      @Override public Map<String, String> load(String locale) {
        return ImmutableMap.of();
      }

      @Override
      public Set<String> findPresentNames(String locale, Set<String> taxonNames) {
        return ImmutableSet.of();
      }
    }, namesPreferences, LocaleOption.CLEMENTS);
  }

  public Indexer<String> getIndexer() {
    Map<String, String> namesMap = namesMap();
    if (namesMap.isEmpty()) {
      return null;
    }
    
    try {
      return indexerCache.get(namesPreferences.getLocale(localeOption));
    } catch (ExecutionException e) {
      throw new RuntimeException(e);
    }
  }

  public LocalNames(
      NamesLoader loader,
      NamesPreferences namesPreferences,
      NamesPreferences.LocaleOption localeOption) {
    this.loader = loader;
    this.namesPreferences = namesPreferences;
    this.localeOption = localeOption;
  }
  
  public String getCommonName(Taxon taxon) {
    if (taxon.getCommonName() != null) {
      Map<String, String> map = namesMap();
      String localityName = map.get(taxon.getId());
      if (localityName != null) {
        return localityName;
      }

      return taxon.getCommonName();
    }
      
    Taxon parentWithCommonName = getParentWithCommonName(taxon);
    if (parentWithCommonName == null) {
      return TaxonUtils.getFullName(taxon);
    }
    return getCommonName(parentWithCommonName)
        + " (" + TaxonUtils.getAbbreviatedName(taxon) + ")";
  }
  
  /** Get a common name without appending any subspecies. */
  public String getSimpleCommonName(Taxon taxon) {
    Taxon parentWithCommonName = getParentWithCommonName(taxon);
    if (parentWithCommonName == null) {
      return TaxonUtils.getFullName(
          TaxonUtils.getParentOfType(taxon, Taxon.Type.species));
    }
    return getCommonName(parentWithCommonName);
  }
  
  private static Taxon getParentWithCommonName(Taxon taxon) {
    while (taxon.getCommonName() == null) {
      taxon = taxon.getParent();
      if (taxon.getType().compareTo(Taxon.Type.species) > 0) {
        return null;
      }
    }
    
    return taxon;
  }

  private Map<String, String> namesMap() {
    Map<String, String> map;
    try {
      map = namesCache.get(namesPreferences.getLocale(localeOption));
    } catch (ExecutionException e) {
      throw new RuntimeException(e.getCause());
    }
    return map;
  }

  public boolean isCommonNamePreferred() {
    return namesPreferences.scientificOrCommon == ScientificOrCommon.COMMON_FIRST
        || namesPreferences.scientificOrCommon == ScientificOrCommon.COMMON_ONLY;
  }

  public String compoundName(Taxon taxon, boolean abbreviateGenus, Status status) {
    String sciName = !abbreviateGenus
        ? TaxonUtils.getFullName(taxon)
        : TaxonUtils.getAbbreviatedName(taxon);
    Taxon parentWithCommonName = getParentWithCommonName(taxon);
    if (parentWithCommonName == null) {
      return String.format("%s - %s", sciName, status);
    }
    
    switch (namesPreferences.scientificOrCommon) {
      // For common- or scientific-first, use the common name from the parent
      // with the common name, so a subspecies doesn't appear twice
      case COMMON_FIRST:
        return String.format("%s - %s (%s)",
            getCommonName(parentWithCommonName), status, sciName);
      case SCIENTIFIC_FIRST:
        return String.format("%s - %s (%s)",
            sciName, status, getCommonName(parentWithCommonName));
      // If only a common name is being used, then use the common
      // name of the direct taxon (which may include a subspecies)
      case COMMON_ONLY:
        return String.format("%s - %s",
            getCommonName(taxon), status);
      case SCIENTIFIC_ONLY:
        return String.format("%s - %s", sciName, status);
      default:
        throw new AssertionError();
    }
  }

  public String compoundName(Taxon taxon, boolean abbreviateGenus) {
    String sciName = !abbreviateGenus
        ? TaxonUtils.getFullName(taxon)
        : TaxonUtils.getAbbreviatedName(taxon);
    Taxon parentWithCommonName = getParentWithCommonName(taxon);
    if (parentWithCommonName == null) {
      return sciName;
    }
    
    switch (namesPreferences.scientificOrCommon) {
      // For common- or scientific-first, use the common name from the parent
      // with the common name, so a subspecies doesn't appear twice
      case COMMON_FIRST:
        return String.format("%s (%s)",
            getCommonName(parentWithCommonName), sciName);
      case SCIENTIFIC_FIRST:
        return String.format("%s (%s)",
            sciName, getCommonName(parentWithCommonName));
      case COMMON_ONLY:
        // If only a common name is being used, then use the common
        // name of the direct taxon (which may include a subspecies)
        return getCommonName(taxon);
      case SCIENTIFIC_ONLY:
        return sciName;
      default:
        throw new AssertionError();
    }
  }

  private Indexer<String> newIndexer(Map<String, String> map) {
    Indexer<String> indexer = new Indexer<String>();
    
    for (Map.Entry<String, String> entry : map.entrySet()) {
      indexer.add(entry.getValue(), entry.getKey());
    }
    
    return indexer;
  }

  /**
   * Returns an abbreviated scientific name... unless the user has asked not to see scientific
   * names at all, in which case we show something as close to a common name as we can get.
   */
  public String abbreviatedScientificName(Taxon taxon) {
    if (namesPreferences.scientificOrCommon == ScientificOrCommon.COMMON_ONLY) {
      return getCommonName(taxon);
    } else {
      return TaxonUtils.getAbbreviatedName(taxon); 
    }
  }

  public Set<AvailableLocale> availableLocales(Taxonomy taxonomy) {
    if (taxonomy.isBuiltIn()) {
      if (taxonomy instanceof MappedTaxonomy) {
        return ImmutableSet.<AvailableLocale>copyOf(NamesPreferences.AvailableIocLocale.values());
      } else {
        return ImmutableSet.<AvailableLocale>copyOf(NamesPreferences.AvailableClementsLocale.values());
      }
    }
    
    return ImmutableSet.of();
  }
  
  /**
   * Look for which locales are possible locales given an "among" set and a set of taxon names that
   * need resolution.
   */
  public Multimap<AvailableLocale, String> possibleTranslations(
      Set<AvailableLocale> among, Set<String> taxonNames) {
    Multimap<AvailableLocale, String> possibilities = Multimaps.newMultimap(
        new LinkedHashMap<AvailableLocale, Collection<String>>(), LinkedHashSet::new);

    for (AvailableLocale locale : among) {
      Set<String> presentNames = loader.findPresentNames(locale.code(), taxonNames);
      if (!presentNames.isEmpty()) {
        possibilities.putAll(locale, presentNames);
      }
    }
    
    return possibilities;
  }

  public AvailableLocale currentLocale(Taxonomy taxonomy) {
    if (!taxonomy.isBuiltIn()) {
      return null;
    }
    
    boolean isIoc = taxonomy instanceof MappedTaxonomy;
    String locale = isIoc ? namesPreferences.locale : namesPreferences.clementsLocale;
    for (AvailableLocale value : (isIoc ? AvailableIocLocale.values() : AvailableClementsLocale.values())) {
      if (value.code().equals(locale) ) {
        return value;
      }
    }
    return isIoc ? AvailableIocLocale.ENGLISH : AvailableClementsLocale.ENGLISH;
  }

  public void setLocale(Taxonomy taxonomy, AvailableLocale locale) {
    Preconditions.checkArgument(taxonomy.isBuiltIn());
    
    if (taxonomy instanceof MappedTaxonomy) {
      namesPreferences.locale = locale.code();
    } else {
      namesPreferences.clementsLocale = locale.code();
    }
  }
}
