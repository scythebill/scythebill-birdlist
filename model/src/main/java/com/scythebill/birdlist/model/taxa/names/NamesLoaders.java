/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.taxa.names;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;

/**
 * Loader classes for localized names.
 */
public class NamesLoaders {
  private static final Logger logger = Logger.getLogger(NamesLoaders.class.getName());
  private NamesLoaders() {}
  
  public static NamesLoader fromFiles(final String filePattern) {
    return new NamesLoaderImpl(filePattern) {
      @Override protected ImportLines importLines(String formatted) {
        File file = new File(formatted);
        if (!file.exists()) {
          return null;
          
        }
        try {
          return CsvImportLines.fromFile(file, Charsets.UTF_8);
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }
    };
  }
  
  public static NamesLoader fromUrls(final String urlPattern) {
    return new NamesLoaderImpl(urlPattern) {
      @Override protected ImportLines importLines(String formatted) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        URL resource = classLoader.getResource(formatted);
        if (resource == null) {
          return null;
        }
        
        try {
          return CsvImportLines.fromUrl(resource, Charsets.UTF_8);
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }
    };
  }

  static abstract class NamesLoaderImpl implements NamesLoader {
    private final String pattern;

    NamesLoaderImpl(String pattern) {
      this.pattern = pattern;
    }
    
    @Override
    public Map<String, String> load(String locale) {
      String formatted = String.format(pattern, locale);
      try {
        ImportLines importLines = importLines(formatted); 
        if (importLines == null) {
          // No file, return gracefully
          return ImmutableMap.of(); 
        }
        try {
          ImmutableMap.Builder<String, String> map = ImmutableMap.builder();
          while (true) {
            String[] nextLine = importLines.nextLine();
            if (nextLine == null) {
              return map.build();
            }
            
            map.put(nextLine[0], nextLine[1]);
          }
        } finally {
          importLines.close();
        }
      } catch (IOException e) {
        logger.severe("Could not load " + formatted);
        return ImmutableMap.of();
      }
    }
    
    @Override
    public Set<String> findPresentNames(String locale, Set<String> taxonNames) {
      String formatted = String.format(pattern, locale);
      try {
        ImportLines importLines = importLines(formatted); 
        if (importLines == null) {
          // No file, return gracefully
          return ImmutableSet.of(); 
        }

        ImmutableSet.Builder<String> builder = ImmutableSet.builder();
        while (true) {
          String[] nextLine = importLines.nextLine();
          if (nextLine == null) {
            return builder.build();
          }

          if (taxonNames.contains(nextLine[1])) {
            builder.add(nextLine[1]);
          }
        }
      } catch (IOException e) {
        return ImmutableSet.of();
      }
        
    }

    abstract protected ImportLines importLines(String formatted);  
  }
}
