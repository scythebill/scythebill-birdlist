/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.taxa.names;

import java.util.Locale;

import com.google.inject.Singleton;
import com.scythebill.birdlist.model.annotations.Preference;

/**
 * Preferences for names.
 */
@Singleton
public final class NamesPreferences {
  public interface AvailableLocale {
    String code();
    Locale locale();
    @Override
    String toString();    
  }
  
  public enum AvailableIocLocale implements AvailableLocale {
    ENGLISH(new Locale("en")),
    BRITISH(new Locale("en", "GB")) {
      @Override
      public String toString() {
        return "English (BOU names)";
      }
    },
    AFRIKAANS(new Locale("af")),
    ARABIC(new Locale("ar")),
    BELARUSSIAN(new Locale("be")),
    BULGARIAN(new Locale("bg")),
    CATALAN(new Locale("ca")),
    CHINESE(new Locale("zh")),
    CHINESE_TRADITIONAL(new Locale("zh", "TW")),
    CROATIAN(new Locale("hr")),
    CZECH(new Locale("cs")),
    DANISH(new Locale("da")),
    DUTCH(new Locale("nl")),
    ESTONIAN(new Locale("et")),
    FINNISH(new Locale("fi")),
    FRENCH(new Locale("fr")),
    GERMAN(new Locale("de")),
    GREEK(new Locale("el")),
    HEBREW(new Locale("he")),
    HUNGARIAN(new Locale("hu")),
    ICELANDIC(new Locale("is")),
    INDONESIAN(new Locale("id")),
    ITALIAN(new Locale("it")),
    JAPANESE(new Locale("ja")),
    KOREAN(new Locale("ko")),
    LATVIAN(new Locale("lv")),
    LITHUANIAN(new Locale("lt")),
    MACEDONIAN(new Locale("mk")),
    MALAYALAM(new Locale("ml")),
    NORWEGIAN(new Locale("no")),
    NORTHERN_SAMI(new Locale("se")),
    PERSIAN(new Locale("fa")),
    POLISH(new Locale("pl")),
    PORTUGUESE(new Locale("pt")),
    PORTUGUESE_PORTUGAL(new Locale("pt", "PT")),
    ROMANIAN(new Locale("ro")),
    RUSSIAN(new Locale("ru")),
    SERBIAN(new Locale("sr")),
    SLOVAK(new Locale("sk")),
    SLOVENIAN(new Locale("sl")),
    SPANISH(new Locale("es")),
    SWEDISH(new Locale("sv")),
    THAI(new Locale("th")),
    TURKISH(new Locale("tr")),
    UKRAINIAN(new Locale("uk"));
    
    private final Locale locale;

    private AvailableIocLocale(Locale locale) {
      this.locale = locale;
    }
    
    @Override
    public String code() {
      return locale.toString();
    }
    
    @Override
    public Locale locale() {
      return locale;
    }
    
    @Override
    public String toString() {
      return locale.getDisplayName(locale);
    }
  }
  
  public enum AvailableClementsLocale implements AvailableLocale {
    ENGLISH(new Locale("en")),
    BRITISH(new Locale("en", "GB")) {
      @Override
      public String toString() {
        return "English (BOU names)";
      }
    },
    ENGLISH_IOC(new Locale("en", "IOC")) {
      @Override
      public String toString() {
        return "English (IOC names)";
      }
    },
    AUSTRALIAN(new Locale("en", "AU")),
    ENGLISH_INDIA(new Locale("en", "IN")),
    NEW_ZEALAND(new Locale("en", "NZ")),
    SOUTH_AFRICA(new Locale("en", "ZA")),
    ALBANIAN(new Locale("sq")),
    ARABIC(new Locale("ar")),
    ARMENIAN(new Locale("hy")),
    AZERBAIJANI(new Locale("az")),
    BASQUE(new Locale("eu")),
    BULGARIAN(new Locale("bg")),
    CHINESE(new Locale("zh")),
    CHINESE_TRADITIONAL(new Locale("zh", "TW")),
    CROATIAN(new Locale("hr")),
    CZECH(new Locale("cs")),
    DANISH(new Locale("da")),
    DUTCH(new Locale("nl")),
    FINNISH(new Locale("fi")),
    FAROESE(new Locale("fo")),
    FRENCH(new Locale("fr")),
    FRENCH_CANADA(new Locale("fr", "CA")),
    FRENCH_GUADELOUPE(new Locale("fr", "GP")),
    FRENCH_HAITI(new Locale("fr", "HT")),
    GALICIAN(new Locale("gl")),
    GERMAN(new Locale("de")),
    GREEK(new Locale("el")),
    HEBREW(new Locale("he")),
    HUNGARIAN(new Locale("hu")),
    ICELANDIC(new Locale("is")),
    INDONESIAN(new Locale("id")),
    ITALIAN(new Locale("it")),
    JAPANESE(new Locale("ja")),
    KOREAN(new Locale("ko")),
    LATVIAN(new Locale("lv")),
    LITHUANIAN(new Locale("lt")),
    MALAYAM(new Locale("ml")),
    MARATHI(new Locale("mr")),
    MONGOLIAN(new Locale("mn")),
    NORWEGIAN(new Locale("no")),
    PERSIAN(new Locale("fa")),
    POLISH(new Locale("pl")),
    PORTUGUESE_ANGOLA(new Locale("pt", "AO")),
    PORTUGUESE_BRAZIL(new Locale("pt", "BR")),
    PORTUGUESE_PORTUGAL(new Locale("pt", "PT")),
    ROMANIAN(new Locale("ro")),
    RUSSIAN(new Locale("ru")),
    SERBIAN(new Locale("sr")),
    SLOVAKIAN(new Locale("sk")),
    SLOVENIAN(new Locale("sl")),
    SPANISH(new Locale("es")),
    SPANISH_ARGENTINA(new Locale("es", "AR")),
    SPANISH_CHILE(new Locale("es", "CL")),
    SPANISH_CUBA(new Locale("es", "CU")),
    SPANISH_DOMINICAN_REPUBLIC(new Locale("es", "DO")),
    SPANISH_ECUADOR(new Locale("es", "EC")),
    SPANISH_SPAIN(new Locale("es", "ES")),
    SPANISH_MEXICO(new Locale("es", "MX")),
    SPANISH_PANAMA(new Locale("es", "PA")),
    SPANISH_PERU(new Locale("es", "PE")),
    SPANISH_PUERTO_RICO(new Locale("es", "PR")),
    SPANISH_URUGURAY(new Locale("es", "UY")),
    SPANISH_VENEZUELA(new Locale("es", "VE")),
    SWEDISH(new Locale("sv")),
    THAI(new Locale("th")),
    TURKISH(new Locale("tr")),
    UKRAINIAN(new Locale("uk"));
    
    
    private final Locale locale;

    private AvailableClementsLocale(Locale locale) {
      this.locale = locale;
    }
    
    @Override
    public String code() {
      return locale.toString();
    }
    
    @Override
    public Locale locale() {
      return locale;
    }
    
    @Override
    public String toString() {
      return locale.getDisplayName(locale);
    }
  }

  public enum ScientificOrCommon {
    COMMON_FIRST,
    SCIENTIFIC_FIRST,
    SCIENTIFIC_ONLY,
    COMMON_ONLY;
   
    public boolean includesCommon() {
      return this != SCIENTIFIC_ONLY;
    }
    
    public boolean includesScientific() {
      return this != COMMON_ONLY;
    }
  }
  
  public enum LocaleOption {
    CLEMENTS, IOC
  }
  
  public String getLocale(LocaleOption option) {
    switch (option) {
      case CLEMENTS:
        return clementsLocale;
      case IOC:
        return locale;
    }
    throw new AssertionError("Unexpected option " + option);
  }
  
  @Preference public String locale = bestLocale(Locale.getDefault(), AvailableIocLocale.values(), "en");
  @Preference public String clementsLocale = bestLocale(Locale.getDefault(), AvailableClementsLocale.values(), "en");
  @Preference public ScientificOrCommon scientificOrCommon = ScientificOrCommon.COMMON_FIRST;
  
  private static final String bestLocale(Locale locale, AvailableLocale[] availableLocales, String defaultLocale) {
    // Trim off anything other than language and country for comparisons here
    if (locale.hasExtensions() || !"".equals(locale.getVariant())) {
      locale = new Locale(locale.getLanguage(), locale.getCountry());
    }
    AvailableLocale preferred = null;
    for (AvailableLocale availableLocale : availableLocales) {
      if (availableLocale.locale().equals(locale)) {
        return availableLocale.code();
      }
      
      if (preferred == null && locale.getLanguage().equals(availableLocale.locale().getLanguage())) {
        preferred = availableLocale;
      }
    }
    
    return preferred == null ? defaultLocale : preferred.code();
  }
}
