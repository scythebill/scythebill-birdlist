/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.user;

import java.util.Comparator;
import java.util.Objects;

import javax.annotation.Nullable;

import com.google.common.base.CharMatcher;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

/**
 * A single user.  All interesting manipulations come through the relevant {@link UserSet}.
 */
public class User {
  /**
   * CharMatcher of characters that may not be present in an abbreviation.  This ensures that
   * these characters can all be used as separators in a list of users.
   */
  public final static CharMatcher NOT_ELIGIBLE_ABBREVIATION_CHARS = CharMatcher.anyOf(",/ \n");

  public static Comparator<User> comparator() {
    // Sort by name, then by user.  Note that Comparator.comparing() does not support null,
    // hence the need for nullToEmpty().
    // TODO: would be nice to have a better by-name sort-order (last-name then first-name, but ... meh?)
    return Comparator.comparing((User user) -> Strings.nullToEmpty(user.name()))
        .thenComparing(user -> Strings.nullToEmpty(user.abbreviation()));
  }

  private final String name;
  private final String abbreviation;
  private final String id;
  
  private User(Builder builder) {
    this.name = Strings.emptyToNull(builder.name);
    this.abbreviation = Strings.emptyToNull(builder.abbreviation);
    if (this.name == null && this.abbreviation == null) {
      throw new IllegalStateException("Users must have either a name or an abbreviation");
    }
    this.id = Preconditions.checkNotNull(builder.id);
  }

  public String id() {
    return id;
  }
  
  @Nullable
  /** The name;  null if none is set (never empty). */
  public String name() {
    return name;
  }
  
  @Nullable
  /** The abbreviation;  null if none is set (never empty). */
  public String abbreviation() {
    return abbreviation;
  }
  
  // TODO: would be really nice to find a way to invalidate a User object after an asBuilder()/build() pair,
  // since any existing references are necesssarily broken.
  public Builder asBuilder() {
    return new Builder(this);
  }
  
  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this).omitNullValues().add("name", name)
        .add("abbreviation", abbreviation).add("id", id).toString();
  }
  
  public static class Builder {
    private String id;
    private String abbreviation;
    private String name;
    
    Builder() {
    }

    Builder(User user) {
      this.id = user.id;
      this.abbreviation = user.abbreviation;
      this.name = user.name;
    }

    public Builder setName(String name) {
      this.name = CharMatcher.whitespace().trimAndCollapseFrom(name, ' ');
      return this;
    }

    public Builder setAbbreviation(String abbreviation) {
      this.abbreviation = abbreviation;
      return this;
    }

    Builder setId(String id) {
      Preconditions.checkState(this.id == null, "Cannot overwrite an ID");
      this.id = id;
      return this;
    }
    
    User build() {
      return new User(this);
    }

    public String name() {
      return name;
    }
    
    public String abbreviation() {
      return abbreviation;
    }

    String id() {
      return id;
    }

    public boolean isValid(UserSet userSet) {
      if (Strings.isNullOrEmpty(abbreviation) && Strings.isNullOrEmpty(name)) {
        return false;
      }
      
      if (!Strings.isNullOrEmpty(abbreviation)
          && User.NOT_ELIGIBLE_ABBREVIATION_CHARS.matchesAnyOf(abbreviation)) {
        return false;
      }
      
      // Invalid if there's something with an existing abbreviation
      if (!Strings.isNullOrEmpty(abbreviation)
          && userSet.hasUserWithAbbreviation(abbreviation)) {
        // Unless that user has the same ID... in which case we'd be replacing.
        User userByAbbreviation = userSet.userByAbbreviation(abbreviation);
        return userByAbbreviation.id().equals(id);
      }
      
      return true;
    }
    
    @Override
    public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      
      if (!(o instanceof User.Builder)) {
        return false;
      }
      
      User.Builder that = (User.Builder) o;
      return Strings.nullToEmpty(that.abbreviation).equals(Strings.nullToEmpty(abbreviation))
          && Strings.nullToEmpty(that.name).equals(Strings.nullToEmpty(name));
    }
    
    @Override
    public int hashCode() {
      return Objects.hash(abbreviation, name);
    }
    
  }
  
}
