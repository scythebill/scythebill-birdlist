/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.user;

import java.text.BreakIterator;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import com.google.common.base.CharMatcher;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.scythebill.birdlist.model.util.Abbreviations;
import com.scythebill.birdlist.model.util.Dirty;
import com.scythebill.birdlist.model.util.DirtyImpl;
import com.scythebill.birdlist.model.util.Abbreviations.AbbreviationConfig;

/**
 * Storage for a set of users.  In particular, manages ID assignment, and retrieval by IDs
 * and abbreviations.
 */
public class UserSet {
  public final static CharMatcher NOT_ELIGIBLE_ID_CHARS = CharMatcher.anyOf(",/ ");
  private final Map<String, User> users = Maps.newLinkedHashMap();
  private final Map<String, User> usersByAbbreviation = Maps.newLinkedHashMap();
  private final DirtyImpl dirty;

  public UserSet() {
    this.dirty = new DirtyImpl(false);
  }
  
  /**
   * Adds a user from a builder.
   * <p>
   * Replaces if the builder already has an ID, otherwise
   */
  public User addUser(User.Builder builder) {
    if (builder.abbreviation() != null
        && User.NOT_ELIGIBLE_ABBREVIATION_CHARS.matchesAnyOf(builder.abbreviation())) {
      throw new IllegalStateException("Abbreviations may not contain any of \",/ \"");
    }
    User user;
    if (builder.id() == null) {
      // New user, compute a new ID
      String name = builder.name();
      String idBase = null;

      // If there's no name, then use the abbreviation
      if (Strings.isNullOrEmpty(name)) {
        idBase = Strings.nullToEmpty(builder.abbreviation()).toLowerCase();
      } else {
        idBase = "";
        BreakIterator iterator = BreakIterator.getWordInstance();
        iterator.setText(name);
        int start = iterator.first();
        for (int end = iterator.next(); end != BreakIterator.DONE; start = end, end =
            iterator.next()) {
          String substring = name.substring(start, Math.min(end, start + 2));
          if (CharMatcher.whitespace().matchesAllOf(substring)) {
            continue;
          }
          idBase += substring.toLowerCase();
        }
      }

      idBase = NOT_ELIGIBLE_ID_CHARS.removeFrom(idBase);
      if (idBase.isEmpty()) {
        idBase = "u";
      }

      int i = 0;
      while (true) {
        String id = (i == 0) ? idBase : idBase + i;
        if (users.containsKey(id)) {
          i++;
          continue;
        }

        builder.setId(id);
        break;
      }

    }

    user = builder.build();
    User existingUser = users.get(user.id());

    if (user.abbreviation() != null) {
      User existingUserByAbbreviation = usersByAbbreviation.get(user.abbreviation());
      // Verify the abbreviation is unique, if it exists; then remove the old mapping
      if (existingUserByAbbreviation != null) {
        if (existingUserByAbbreviation != existingUser) {
          throw new IllegalStateException(
              "There is already a user with this abbreviation: " + existingUserByAbbreviation);
        }
      }
    }
    
    // With validation complete, update maps 
    if (existingUser != null && existingUser.abbreviation() != null) {
      usersByAbbreviation.remove(existingUser.abbreviation());
    }
    if (user.abbreviation() != null) {
      usersByAbbreviation.put(user.abbreviation(), user);
    }
    
    users.put(user.id(), user);
    dirty.setDirty(true);
    return user;
  }

  public void removeUser(User user) {
    if (!users.containsKey(user.id())) {
      throw new IllegalStateException("User " + user + " is not present");
    }
    if (user.abbreviation() != null && !usersByAbbreviation.containsKey(user.abbreviation())) {
      throw new IllegalStateException("User " + user + " abbreviation is not present");
    }
    users.remove(user.id());
    if (user.abbreviation() != null) {
      usersByAbbreviation.remove(user.abbreviation());
    }
    dirty.setDirty(true);
  }

  /**
   * Returns a user by ID.
   * 
   * @throws IllegalArgumentException if no user exists with that ID.
   */
  public User userById(String id) {
    User user = users.get(Preconditions.checkNotNull(id));
    if (user == null) {
      throw new IllegalArgumentException("No user with ID " + id);
    }
    return user;
  }

  /**
   * Returns a user by abbreviation.
   * 
   * @throws IllegalArgumentException if no user exists with that abbreviation.
   */
  public User userByAbbreviation(String abbreviation) {
    User user = usersByAbbreviation.get(Preconditions.checkNotNull(abbreviation));
    if (user == null) {
      throw new IllegalArgumentException("No user with abbreviation " + abbreviation);
    }
    return user;
  }

  /**
   * Instantiates a new User.Builder which already has an ID (and therefore comes from storage).
   */
  public User.Builder newUserBuilderFromStorage(String id) {
    return new User.Builder().setId(id);
  }

  /**
   * Instantiates a new User.Builder
   */
  public User.Builder newUserBuilder() {
    return new User.Builder();
  }
  
  /**
   * Returns all users.  This is not modifiable.
   */
  public Collection<User> allUsers() {
    return Collections.unmodifiableCollection(users.values());
  }

  public boolean hasUserWithAbbreviation(String abbreviation) {
    return usersByAbbreviation.containsKey(abbreviation);
  }

  public Dirty getDirty() {
    return dirty;
  }
  
  public void clearDirty() {
    dirty.setDirty(false);
  }
  
  static AbbreviationConfig abbreviationConfig = new AbbreviationConfig();
  static {
    abbreviationConfig.ignoredCharacters = User.NOT_ELIGIBLE_ABBREVIATION_CHARS;
    abbreviationConfig.maxPerWordChars = 3;
    abbreviationConfig.maxWords = 5;
  }
  
  /**
   * Returns a default abbreviation for a given name.
   *
   * @param name current name
   * @param currentUser the user being edited, or empty if there is no user being edited
   * @return an available abbreviation, or empty if no valid option could be found. 
   */
  public Optional<String> computeDefaultAbbreviation(String name, Optional<User> currentUser) {
    return Abbreviations.abbreviate(
        name,
        abbreviation -> !isAbbreviationAvailable(abbreviation, currentUser),
        abbreviationConfig);
  }

  private boolean isAbbreviationAvailable(String abbreviation, Optional<User> currentUser) {
    // If the abbreviation is unused, then it's good.
    if (!hasUserWithAbbreviation(abbreviation)) {
      return true;
    }
    
    if (currentUser.isPresent()) {
      // If the abbreviation is *the current* user, then it's also fine.
      User userByAbbreviation = userByAbbreviation(abbreviation);
      if (userByAbbreviation == currentUser.get()) {
        return true;
      }
    }
    
    return false;
  }
}
