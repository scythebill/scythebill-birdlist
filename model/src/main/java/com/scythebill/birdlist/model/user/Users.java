/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.user;

import com.google.common.collect.ImmutableSet;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;

/**
 * Utilities for working with users.
 */
public class Users {
  private Users() {}
  
  /**
   * Finds all users for a given visit.
   */
  public static ImmutableSet<User> usersForVisitInfoKey(ReportSet reportSet, VisitInfoKey visitInfoKey) {
    // No users - short-circuit
    if (reportSet.getUserSet() == null) {
      return ImmutableSet.of();
    }
    
    ImmutableSet.Builder<User> users = ImmutableSet.builder();
    for (Sighting sighting : reportSet.getSightings()) {
      if (sighting.hasSightingInfo() && visitInfoKey.matches(sighting)) {
        users.addAll(sighting.getSightingInfo().getUsers());
      }
    }
    
    return users.build();
  }
                                                                              
}
