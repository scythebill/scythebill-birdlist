/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.util;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;

/**
 * Utilities for abbreviating names.
 */
public class Abbreviations {
  public static class AbbreviationConfig {
    public CharMatcher ignoredCharacters = CharMatcher.none();
    public int maxPerWordChars = 3;
    public int maxWords = 4;
  }
  
  private Abbreviations() {}

  /**
   * Computes an abbreviation of a name.
   * @param ignoredCharacters characters which should not be included in an abbreviation
   * @param available a predicate identifying if an abbreviation is available.
   */
  public static Optional<String> abbreviate(String name, Predicate<String> reserved, AbbreviationConfig config) {
    if (name.isEmpty()) {
      return Optional.empty();
    }

    // Split the string, but around any whitespace as well as any characters not allowed for
    // abbreviations.  (These may be overlapping sets, but who cares?)
    List<String> nameParts = Splitter.on(config.ignoredCharacters.or(CharMatcher.whitespace()))
        .omitEmptyStrings().limit(config.maxWords).splitToList(name);
    if (nameParts.isEmpty()) {
      return Optional.empty();
    }
    StringBuilder builder = new StringBuilder(name.length());
    String abbreviation = null;
    for (int i = nameParts.size(); i <= nameParts.size() * config.maxPerWordChars; i++) {
      builder.setLength(0);
      int lettersForEach = i / nameParts.size();
      int leftover = i % nameParts.size();
      for (int j = 0; j < nameParts.size(); j++) {
        int length = j < leftover ? lettersForEach + 1 : lettersForEach;
        String part = nameParts.get(j);
        builder.append(part.substring(0, Math.min(length, part.length())));
      }
      
      abbreviation = builder.toString();
      // Terminate as soon as anything is available
      if (!reserved.test(abbreviation)) {
        break;
      }
    }
    
    int extraCount = 1;
    while (true) {
      String abbreviationWithSuffix = (extraCount == 1) ? abbreviation : (abbreviation + extraCount);
      if (!reserved.test(abbreviationWithSuffix)) {
        return Optional.of(abbreviationWithSuffix);
      }
      extraCount++;
    }
  }
}
