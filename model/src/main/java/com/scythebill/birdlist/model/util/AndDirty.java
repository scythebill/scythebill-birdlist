/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.util;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Arrays;
import java.util.List;

import com.google.common.collect.ImmutableList;

public class AndDirty extends DirtyImpl {
  private final List<Dirty> dirtyChildren;

  public AndDirty(Dirty... dirtyChildren) {
    super(false);
    this.dirtyChildren = ImmutableList.copyOf(Arrays.asList(dirtyChildren));
    setDirty(computeDirty());
    
    PropertyChangeListener recompute = new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent event) {
        setDirty(computeDirty());
      }
    };
    
    for (Dirty dirty : dirtyChildren) {
      dirty.addDirtyListener(recompute);
    }
  }
  
  private boolean computeDirty() {
    for (Dirty dirty : dirtyChildren) {
      if (dirty.isDirty()) {
        return true;
      }
    }
    
    return false;
  }
}
