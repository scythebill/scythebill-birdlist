/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.util;

import static java.lang.Math.max;

import java.util.List;
import java.util.Map;

import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * Highly influenced by:
 * http://blog.notdot.net/2007/4/Damn-Cool-Algorithms-Part-1-BK-Trees
 * 
 * @author Nirav Thaker
 */
public class BKTree<K, V> {
  private final BKNode<K, V> root;
  private final Metric<K> metric;
  private Function<K, K> keyTransform;

  /**
   * Create a new BKTree.
   */
  static public <K, V> BKTree.Builder<K, V> builder(Metric<K> metric) {
    return new Builder<K, V>(metric); 
  }
  
  /**
   * Performs a fuzzy search.
   */
  public List<V> search(K q, int maxDistance) {
    if (root == null) { 
      return ImmutableList.of();
    }
    
    List<V> accumulator = Lists.newArrayList(); 
    root.search(keyTransform.apply(q), metric, maxDistance, accumulator);
    return accumulator;
  }

  private BKTree(BKNode<K, V> root, Metric<K> metric, Function<K, K> keyTransform) {
    this.root = root;
    this.metric = metric;
    this.keyTransform = keyTransform;
  }
  
  public static class Builder<K, V> {
    private BKNode<K, V> root;
    private final Metric<K> metric;
    private Function<K, K> keyTransform = Functions.identity();
  
    public BKTree<K, V> build() {
      return new BKTree<K, V>(root, metric, keyTransform);
    }
    
    private Builder(Metric<K> metric) {
      this.metric = Preconditions.checkNotNull(metric);
    }
    
    /**
     * Adds a node to the BKTree.
     */
    public void add(K key, V value) {
      BKNode<K, V> newNode = new BKNode<K, V>(keyTransform.apply(key), value);
      if (root == null) {
        root = newNode;
      }
      addInternal(root, newNode);
    }
  
    private void addInternal(BKNode<K, V> src, BKNode<K, V> newNode) {
      if (src.equals(newNode))
        return;
      int distance = metric.distance(src.key, newNode.key);
      BKNode<K, V> bkNode = src.childAtDistance(distance);
      if (bkNode == null) {
        src.addChild(distance, newNode);
      } else
        addInternal(bkNode, newNode);
    }

    public Builder<K, V> withTransform(Function<K, K> transform) {
      this.keyTransform = transform;
      return this;
    }
  }

  static class BKNode<K, V> {
    private final K key;
    private final V value;
    // TODO(awiner): replace with sparse arrays, esp. when size is small?
    private final Map<Integer, BKNode<K, V>> children = Maps.newHashMap();

    public BKNode(K key, V value) {
      this.key = Preconditions.checkNotNull(key);
      this.value = value;
    }

    BKNode<K, V> childAtDistance(int pos) {
      return children.get(pos);
    }

    private void addChild(int pos, BKNode<K, V> child) {
      children.put(pos, child);
    }

    void search(K query, Metric<K> metric, int maxDistance, List<V> accumulator) {
      int distance = metric.distance(this.key, query);
      // If this node is a match, add it
      if (distance <= maxDistance) {
        accumulator.add(this.value);
      }
      // No children:  we're done.
      if (children.size() == 0) {
        return;
      }
      // For all values from  
      for (int i = max(1, distance - maxDistance); i <= distance + maxDistance; i++) {
        BKNode<K, V> child = children.get(i);
        if (child == null) {
          continue;
        }
        child.search(query, metric, maxDistance, accumulator);
      }
    }
  }

}
