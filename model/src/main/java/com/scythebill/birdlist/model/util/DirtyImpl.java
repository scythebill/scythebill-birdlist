/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.util;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import org.joda.time.Instant;

import com.google.common.collect.Lists;

public class DirtyImpl implements Dirty {
  private volatile boolean dirty;
  private final List<PropertyChangeListener> listeners =
    Lists.newLinkedList();
  private volatile Instant dirtySince;

  public DirtyImpl(boolean dirty) {
    this.dirty = dirty;
    if (dirty) {
      dirtySince = new Instant();
    }
  }

  @Override public void addDirtyListener(PropertyChangeListener listener) {
    listeners.add(listener);
  }

  public void setDirty(boolean newDirty) {
    if (newDirty != dirty) {
      dirty = newDirty;
      dirtySince = dirty ? new Instant() : null;
      firePropertyChanged();
    }
  }

  private void firePropertyChanged() {
    PropertyChangeEvent event = new PropertyChangeEvent(this, "dirty", !dirty, dirty);
    for (PropertyChangeListener listener : listeners) {
      listener.propertyChange(event);
    }
  }

  @Override public boolean isDirty() {
    return dirty;
  }

  @Override
  public Instant dirtySince() {
    return dirtySince;
  }

}
