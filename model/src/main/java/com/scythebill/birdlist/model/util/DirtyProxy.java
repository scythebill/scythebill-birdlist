/*   
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.util;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.joda.time.Instant;

/**
 * An implementation of {@link Dirty} which can be either inert or proxying to
 * an underlying Dirty instace.
 */
public class DirtyProxy implements Dirty {

  private final DirtyImpl dirty = new DirtyImpl(false);
  private final PropertyChangeListener changeListener = this::proxyChangeListener;
  private Dirty proxiedDirty;

  public DirtyProxy() {}

  @Override
  public boolean isDirty() {
    return dirty.isDirty();
  }

  @Override
  public Instant dirtySince() {
    return dirty.dirtySince();
  }

  @Override
  public void addDirtyListener(PropertyChangeListener listener) {
    dirty.addDirtyListener(listener);
  }

  /**
   * Sets this "DirtyProxy" to reflect the state of the underlying dirty.
   */
  public void setProxiedDirty(Dirty proxiedDirty) {
    this.proxiedDirty = proxiedDirty;
    if (proxiedDirty != null) {
      proxiedDirty.addDirtyListener(changeListener);
      dirty.setDirty(proxiedDirty.isDirty());
    } else {
      dirty.setDirty(false);
    }
  }

  private void proxyChangeListener(PropertyChangeEvent e) {
    if (proxiedDirty != null) {
      dirty.setDirty(proxiedDirty.isDirty());
    }
  }
}
