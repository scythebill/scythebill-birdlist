/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multiset;

/**
 * An Indexer takes a mapping from string N-grams to values and allows rapid
 * access to those values based on ad hoc abbreviations.
 * <p>
 * For example, a user can find "Wilson's Warbler" by typing "w", "ww",
 * "wiw", "wiwa", "wwar", "wilw", "wilswar", etc.
 *   
 */
public class Indexer<T> {
  /** Computed tries for each word */
  private List<Trie<T>> tries;
  private int maxWords;
  private WordSeparator wordSeparator;

  private static final Splitter WORD_SEPARATORS = Splitter.on(
      CharMatcher.whitespace().or(CharMatcher.anyOf("-.,/"))).omitEmptyStrings();
  private static final CharMatcher CHARACTERS_TO_DROP = CharMatcher.anyOf("'()");
  
  private ImmutableMultimap<String, String> alternateIndexEntries = ImmutableMultimap.of();
  
  public Indexer() {
    this(5);
  }

  public Indexer(int maxWords) {
    this.maxWords = maxWords;
    tries = new ArrayList<Trie<T>>(maxWords);
    wordSeparator = new WordSeparator(maxWords);
  }

  public Multiset<Integer> computeSizes() {
    Multiset<Integer> sizes = HashMultiset.create();
    for (Trie<T> trie : tries) {
      sizes.addAll(trie.computeNodeSizes());
    }
    return sizes;
  }
  
  /**
   * Find a single string (or multiple strings, if it consists of
   * multiple words).
   */
  public Collection<T> find(String entry) {
    if ((entry == null) || (entry.length() == 0))
      return Collections.emptyList();

    entry = CHARACTERS_TO_DROP.removeFrom(Trie.normalizeString(entry));
    // If the user inserted spaces, e.g. "he gu", then use that as the one-and-only
    // set of tokens.
    List<String> tokens = ImmutableList.copyOf(WORD_SEPARATORS.split(entry));
    if (tokens.size() > 1) {
      return find(tokens);
    }

    // ... otherwise, try every combination of tokens
    int length = entry.length();

    LinkedHashSet<T> fullResults = new LinkedHashSet<T>();

    Iterable<int[]> tokenBoundariesForThisLength = wordSeparator
        .getBoundaries(length);

    for (int[] tokenBoundaries : tokenBoundariesForThisLength) {
      tokens = _createListOfTokens(entry, tokenBoundaries);
      Collection<T> results = find(tokens);
      fullResults.addAll(results);
    }

    return fullResults;
  }

  static private List<String> _createListOfTokens(String entry, int[] boundaries) {
    List<String> l = new ArrayList<String>(boundaries.length);
    int start = 0;
    for (int i = 0; i < boundaries.length; i++) {
      l.add(entry.substring(start, boundaries[i]));
      start = boundaries[i];
    }

    return l;
  }

  /**
   * Find the available values given a List of tokens
   * 
   */
  // This is currently taking about 11ms.  It could certainly
  // be optimized - for one thing, we repeatedly compute the same results;
  // in the example given below, we process token 2 against trie 4 all of 6 times!!!
  public Collection<T> find(List<String> tokens) {
    int tokenCount = tokens.size();
    int trieCount = tries.size();
    if (tokenCount > trieCount)
      return Collections.emptySet();

    // Example for explanation: 3 tokens (0-2), 5 tries (0-4)
    // Need to process each token:
    // Need to process token 0, starting at tries 0, 1, and 2
    //   Then process token 1, starting at tries 1, 2, and 3, or 2 and 3, or just 3
    //     Then process token 2, starting at tries 2, 3, 4, or 3, 4, or 4, or 3, 4, or 4, or 4
    //   AND SO FORTH!
    // So, start at token 0, with tries 0 or higher
    return _find(tokens, 0, 0, null);
  }

  private Collection<T> _find(List<String> tokens, int tokenNumber,
      int minimumTrie, Collection<T> within) {
    // Use a LinkedHashSet for order stability
    Collection<T> results = new LinkedHashSet<T>();
    String token = tokens.get(tokenNumber);
    // If we're processing token 0 of 3, and have five tries, 
    // we have 2 remaining tokens after this, so we'll need
    // at least two more tries, so the last trie we can use
    // is #2
    // If we're processing token 0 of 2, and have two tries,
    // we have one remaining token after this, and need at least
    // one more trie, so the last trie we can use is this one (zero)
    int lastAvailableTrie = tries.size() - (tokens.size() - tokenNumber);
    for (int i = minimumTrie; i <= lastAvailableTrie; i++) {
      // Use a LinkedHashSet for order stability
      Collection<T> ithTrieResults = new LinkedHashSet<T>();
      tries.get(i).findMatches(ithTrieResults, token, within);
      // If this pass through the trie did not come up empty, and there's
      // more results to look for, recurse into the next trie, limiting
      // that search based on the results of the former search.
      if (!ithTrieResults.isEmpty() && (tokenNumber + 1 < tokens.size())) {
        Collection<T> nextResults = _find(tokens, tokenNumber + 1, i + 1,
            ithTrieResults);
        
        // Now, merge the two sets.  "nextResults" is guaranteed to be a subset
        // of "ithTrieResults" (since we searched within ithTreeResults).  However,
        // we want to keep the *order* of ithTreeResults, so that as users type,
        // the order doesn't keep bouncing around.  Consequently, drop values
        // from "ithTreeResults" that aren't in "nextResults".
        
        // ... but optimize the trivial cases of nothing found or one item found
        ithTrieResults = nextResults;
        if (nextResults.isEmpty() || nextResults.size() == 1) {
          ithTrieResults = nextResults;
        } else {
          Iterator<T> iter = ithTrieResults.iterator();
          while (iter.hasNext()) {
            if (!nextResults.contains(iter.next())) {
              iter.remove();
            }
          }
        }
      }

      results.addAll(ithTrieResults);
    }

    return results;
  }

  /** Adds a single name to the index */
  public void add(String name, T value) {
    Iterator<String> tokens = WORD_SEPARATORS.split(name).iterator(); 
    for (int i = 0; i < maxWords && tokens.hasNext(); i++) {
      String word = CHARACTERS_TO_DROP.removeFrom(tokens.next());
      if (tries.size() == i)
        tries.add(new Trie<T>());

      Trie<T> trie = tries.get(i);
      trie.add(word, value);
      for (String alternate : alternateIndexEntries.get(word)) {
        trie.add(alternate, value);
      }
    }
  }

  /** Removes a name from the index */
  public void remove(String name, T value) {
    Iterator<String> tokens = WORD_SEPARATORS.split(name).iterator(); 
    for (int i = 0; i < maxWords && tokens.hasNext(); i++) {
      String word = CHARACTERS_TO_DROP.removeFrom(tokens.next());
      if (tries.size() == i)
        break;

      Trie<T> trie = tries.get(i);
      trie.remove(word, value);
      for (String alternate : alternateIndexEntries.get(word)) {
        trie.remove(alternate, value);
      }
    }
  }

  public void setAlternateIndexEntries(ImmutableMultimap<String, String> alternateIndexEntries) {
    this.alternateIndexEntries = alternateIndexEntries;
  }

}
