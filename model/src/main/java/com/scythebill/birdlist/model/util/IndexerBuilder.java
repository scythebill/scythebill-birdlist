/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.util;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.common.base.Function;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Builds an index off of a Taxonomy.  Supports reporting progress on the indexing. 
 */
public class IndexerBuilder implements Callable<IndexerBuilder.Indexers>, Progress {

  private volatile int max = 10000;
  private final AtomicInteger current = new AtomicInteger(0);
  private final ToString<Taxon> namer;
  private final Future<? extends Taxonomy> taxonomyFuture;
  private Function<Species, Iterable<String>> alternatesFunction;
  private final boolean includeSubspecies;
  
  public IndexerBuilder(
      Future<? extends Taxonomy> taxonomyFuture,
      ToString<Taxon> namer,
      Function<Species, Iterable<String>> alternatesFunction,
      boolean includeSubspecies) {
    this.taxonomyFuture = taxonomyFuture;
    this.namer = namer;
    this.alternatesFunction = alternatesFunction;
    this.includeSubspecies = includeSubspecies;
  }
  
  public static class Indexers {
    public Indexer<String> indexer;
    public Indexer<AlternateName<String>> alternateIndexer;
  }
  
  @Override public Indexers call() throws Exception {
    Taxonomy taxonomy = taxonomyFuture.get();
    max = taxonomy.getSpeciesCount();
    Indexers indexers = new Indexers();
    indexers.indexer = new Indexer<String>();
    indexers.alternateIndexer = new Indexer<AlternateName<String>>();
    addToIndex(taxonomy, indexers, namer, taxonomy.getRoot());
    return indexers;
  }

  @Override public long current() {
    return current.get();
  }

  @Override public long max() {
    return max;
  }
  
  private void addToIndex(
      Taxonomy taxonomy,
      Indexers indexers,
      ToString<Taxon> namer,
      Taxon taxon) {
    if (taxon instanceof Species) {
      if (!taxon.isDisabled()) {
        String id = taxonomy.getId(taxon);
        String name = namer.getString(taxon);
        if (name != null) {
          indexers.indexer.add(name, id);
        }
        
        if (alternatesFunction != null) {
          for (String alternate : alternatesFunction.apply((Species) taxon)) {
            String nameToIndex = trimSuffixes(alternate);
            indexers.alternateIndexer.add(nameToIndex,
                AlternateName.forNameAndId(alternate, id));
          }
        }
      }
      if (taxon.getType() == Taxon.Type.species) {
        current.incrementAndGet();
      }

      // If not including subspecies, no need to recurse further
      if (!includeSubspecies) {
        return;
      }
    }
    
    for (Taxon child : taxon.getContents()) {
      addToIndex(taxonomy, indexers, namer, child);
    }
  }
  
  private String trimSuffixes(String alternate) {
    int indexOf = alternate.indexOf(" -");
    if (indexOf > 0) {
      return alternate.substring(0, indexOf);
    }
    return alternate;
  }
}