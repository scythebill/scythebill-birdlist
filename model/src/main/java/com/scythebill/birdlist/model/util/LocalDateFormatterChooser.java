/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;

import org.joda.time.DateTimeFieldType;
import org.joda.time.LocalDate;
import org.joda.time.Partial;
import org.joda.time.chrono.GJChronology;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.common.base.CharMatcher;
import com.google.common.base.MoreObjects;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

/**
 * A class which looks at date strings and guesses the format. 
 * 
 * <p>Current assumptions:
 * 
 * <li> Strings are already trimmed.
 */
public final class LocalDateFormatterChooser {
  /**
   * A single chosen format.
   */
  public static class ChosenFormat {
    private final DateTimeFormatter formatter;
    private final ImmutableSet<DateTimeFieldType> fieldTypes;
    private final String pattern;
    
    public DateTimeFormatter formatter() {
      return formatter;
    }
    
    public Partial parse(String date) {
      Partial partial = new Partial(GJChronology.getInstance());
      LocalDate localDate = formatter.parseLocalDate(date);
      if (fieldTypes.contains(DateTimeFieldType.year())) {
        partial = partial.with(DateTimeFieldType.year(), localDate.getYear());
      }
      if (fieldTypes.contains(DateTimeFieldType.monthOfYear())) {
        partial = partial.with(DateTimeFieldType.monthOfYear(), localDate.getMonthOfYear());
      }
      if (fieldTypes.contains(DateTimeFieldType.dayOfMonth())) {
        partial = partial.with(DateTimeFieldType.dayOfMonth(), localDate.getDayOfMonth());
      }
      
      return partial;
    }
    
    static ImmutableList<ChosenFormat> forPattern(String pattern) {
      ImmutableSet.Builder<DateTimeFieldType> fields = ImmutableSet.builder();
      if (pattern.indexOf('y') >= 0) {
        fields.add(DateTimeFieldType.year());
      }
      if (pattern.indexOf('M') >= 0) {
        fields.add(DateTimeFieldType.monthOfYear());
      }
      if (pattern.indexOf('d') >= 0) {
        fields.add(DateTimeFieldType.dayOfMonth());
      }
      ChosenFormat chosenFormatInDefaultLocale =
          new ChosenFormat(DateTimeFormat.forPattern(pattern), fields.build(), pattern);
      if (Locale.getDefault().getLanguage().equals(Locale.ENGLISH.getLanguage())) {
        return ImmutableList.of(chosenFormatInDefaultLocale);
      } else {
        return ImmutableList.of(chosenFormatInDefaultLocale,
            chosenFormatInDefaultLocale.withLocale(Locale.US));
      }
    }

    private ChosenFormat withLocale(Locale locale) {
      return new ChosenFormat(formatter.withLocale(locale), fieldTypes, pattern);
    }

    private ChosenFormat(DateTimeFormatter formatter, ImmutableSet<DateTimeFieldType> fieldTypes, String pattern) {
      this.formatter = formatter;
      this.fieldTypes = fieldTypes;
      this.pattern = pattern;
    }

    @Override
    public String toString() {
      return MoreObjects.toStringHelper(this).add("pattern", pattern).toString();
    }
  }
  
  // TODO: come up with a real list of characters to split on, e.g. by iterating through all the
  // possibilities.
  private static final CharMatcher COMPONENT_SEPARATORS =
      CharMatcher.anyOf("/-,").or(CharMatcher.whitespace());
  private static final CharMatcher COMPONENT_SEPARATORS_OR_DIGIT =
      CharMatcher.anyOf("/-,").or(CharMatcher.whitespace()).or(CharMatcher.inRange('0', '9'));
  private static final CharMatcher COMPONENT_TEXT = COMPONENT_SEPARATORS.negate();
  private static final ImmutableList<String> MONTH_OR_DAY_NUMBER_FORMATS = ImmutableList.of("M", "d");
  private static final ImmutableList<String> MONTH_TEXT_FORMATS = ImmutableList.of("MMM");
  private static final ImmutableList<String> YEAR_NUMBER_FORMATS = ImmutableList.of("yy");
  private static final ImmutableList<String> YEAR_OR_DAY_NUMBER_FORMATS = ImmutableList.of("yy", "d");
  private static final ImmutableList<String> YEAR_OR_DAY_OR_MONTH_NUMBER_FORMATS = ImmutableList.of("yy", "M", "d");

  /**
   * Choose formatters based on a list of dates.
   */
  public ImmutableList<ChosenFormat> chooseFormatters(Collection<String> dates) {
    // First step: partition all incoming dates by the set of separators used
    ArrayListMultimap<String, String> datesBySeparators = ArrayListMultimap.create(5, dates.size());
    for (String date : dates) {
      String retainedSeparators = COMPONENT_SEPARATORS.retainFrom(date);
      datesBySeparators.put(retainedSeparators, date);
    }

    // Sort those separators in descending order of frequency (so that the returned list of
    // DateTimeFormatters considers the most common cases first)
    SortedSet<String> separators = new TreeSet<>(
        Comparator.<String, Integer>comparing(s -> datesBySeparators.get(s).size()).reversed()
        .thenComparing(s -> s.length())
        .thenComparing(Comparator.<String>naturalOrder()));
    separators.addAll(datesBySeparators.keySet());

    ImmutableList.Builder<ChosenFormat> formatters = ImmutableList.builder(); 
    // Now, for the fun part: each separator set represents a series of similar dates, each of which
    // can generate some hypotheses for how to parse them. Once we reduce any set of hypotheses to a single
    // acceptable one for this separator, we can use that remaining hypothesis to generate a
    // DateTimeFormatter, and then on to the next separator.
    // TODO: do we have issues where the dates for a single separator set require multiple formats?
    // I suppose someone might have 01-13-2017 and Jan-14-2017 in a single file.  That would be ... mean(?),
    // but the code should deal.
    // TODO: how do we deal with ambiguous cases (1-2-2017), where we can never really reduce to just one
    // hypothesis?.  Some options:
    // - heuristically choose the one that is nearest to a default system ordering for the locale
    // - ask the user (which would require choosing a different return type with enough polymorphism 
    for (String separator : separators) {
      chooseFormatter(datesBySeparators.get(separator)).ifPresent(formatters::add);
    }

    return formatters.build();
  }

  private Optional<ChosenFormat> chooseFormatter(List<String> dates) {
    String firstDate = dates.get(0);
    // Pick some hypotheses from the first date
    List<ChosenFormat> hypotheses = computeHypotheses(firstDate);
    
    // If there's more than one, test on subsequent dates until there's just one left
    // TODO: only bother for the first N dates?
    if (hypotheses.size() > 1) {
      for (String date : dates) {
        int successful = 0;
        for (ChosenFormat hypothesis : hypotheses) {
          try {
            hypothesis.formatter().parseLocalDate(date);
            successful++;
          } catch (IllegalArgumentException e) {
          }
        }
        
        if (successful == hypotheses.size() || successful == 0) {
          continue;
        }
        
        List<ChosenFormat> newHypotheses = new ArrayList<>(successful);
        for (ChosenFormat hypothesis : hypotheses) {
          try {
            hypothesis.formatter().parseLocalDate(date);
            newHypotheses.add(hypothesis);
          } catch (IllegalArgumentException e) {
          }
        }
        
        hypotheses = newHypotheses;
        if (hypotheses.size() == 1) {
          break;
        }
      }      
    }
    
    // TODO: discard some alternatives more proactively.
    // Example: yy-d is clearly less likely than yy-M 
    return hypotheses.stream().findFirst();
  }

  /**
   * Identifies a list of hypothetical formats for a single date.  There is no guarantee
   * that the hypotheses are valid even for this date.
   */
  private ImmutableList<ChosenFormat> computeHypotheses(String date) {
    List<String> hypotheses = new ArrayList<>(2);
    hypotheses.add("");
    
    int position = 0;
    while (true) {
      // Add the next component.
      // If the current character is *not* a digit, then skip to the next separator *or* a digit.
      // This handles cases like "Dec2005", which is undesirable, but meaningful.
      int nextSeparator =
          CharMatcher.inRange('0', '9').matches(date.charAt(position))
          ? COMPONENT_SEPARATORS.indexIn(date, position)
          : COMPONENT_SEPARATORS_OR_DIGIT.indexIn(date, position);
      String component = (nextSeparator < 0) ? date.substring(position) : date.substring(position, nextSeparator);
      List<String> possibleFormats = computePossibleFormats(component);
      List<String> newHypotheses = new ArrayList<>(hypotheses.size() * possibleFormats.size());
      for (String possibleFormat : possibleFormats) {
        for (String hypothesis : hypotheses) {
          if (!hypothesis.contains(possibleFormat)) {
            newHypotheses.add(hypothesis + possibleFormat);
          }
        }
      }
      hypotheses = newHypotheses;
      
      if (nextSeparator < 0) {
        break;
      }
      
      // And add the next separator
      int nextComponent = COMPONENT_TEXT.indexIn(date, nextSeparator);
      String separator = (nextComponent < 0) ? date.substring(nextSeparator) : date.substring(nextSeparator, nextComponent);
      for (int i = 0; i < hypotheses.size(); i++) {
        hypotheses.set(i, hypotheses.get(i) + separator);
      }
      
      if (nextComponent < 0) {
        break;
      }
      
      position = nextComponent;
    }
    
    return hypotheses.stream()
        .map(ChosenFormat::forPattern)
        .flatMap(Collection::stream)
        .collect(ImmutableList.toImmutableList());
  }

  /**
   * Compute plausible formats for a single component of a date. 
   */
  private List<String> computePossibleFormats(String component) {
    if (CharMatcher.inRange('0', '9').matchesAllOf(component)) {
      if (component.length() == 1) {
        // Years are assumed to never, ever be a single digit.
        return MONTH_OR_DAY_NUMBER_FORMATS;
      } else if (component.length() > 2) {
        // More than 2 digits == year
        return YEAR_NUMBER_FORMATS;
      } else {
        // 2 digits: need to look at the value.
        int intComponent = Integer.parseInt(component);
        if (intComponent > 31) {
          return YEAR_NUMBER_FORMATS;
        } else if (intComponent > 12) {
          return YEAR_OR_DAY_NUMBER_FORMATS;
        } else {
          return YEAR_OR_DAY_OR_MONTH_NUMBER_FORMATS;
        }
      }
    } else {
      return MONTH_TEXT_FORMATS;
    }
  }
}
