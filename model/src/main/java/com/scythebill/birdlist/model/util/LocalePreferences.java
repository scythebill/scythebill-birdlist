package com.scythebill.birdlist.model.util;

import java.util.Locale;
import java.util.prefs.Preferences;

import com.google.common.base.Strings;

/**
 * Preferences for the overall locale of Scythebill.
 * 
 * <p>Gets used in core places like LocaleImpl such that it is impractical
 * to implement via Guice and {@code PreferencesManager}. Instead, a plain
 * singleton that interacts with the Java preferences system directly.   
 */
public class LocalePreferences {
  private static LocalePreferences instance = new LocalePreferences();

  private Locale localeAtStartup;
  private String uiLocale;
  
  private LocalePreferences() {
    Preferences preferences = Preferences.userNodeForPackage(getClass());
    uiLocale = preferences.get("uiLocale", "");
    localeAtStartup = isDefault() ? Locale.getDefault() : Locale.forLanguageTag(uiLocale);
  }
  
  /** Returns the singleton instance. */
  public static LocalePreferences instance() {
    return instance;
  }
  
  /** Returns the locale used for the UI of Scythebill on this run. */
  public Locale getUiLocale() {
    return localeAtStartup;
  }
  
  /** Returns the locale currently saved for the next run of Scythebill. */
  public Locale getSavedLocale() {
    if (isDefault()) {
      return Locale.getDefault();
    }
    
    return Locale.forLanguageTag(uiLocale);
  }
  
  /**
   * Returns true if {@link #getSavedLocale()} is directly returning the system default,
   * and not a saved locale that happens to be the system default.
   */ 
  public boolean isDefault() {
    return Strings.isNullOrEmpty(uiLocale);
  }

  /**
   * Saves the locale for the next run of Scythebill. 
   */
  public void saveLocale(Locale locale) {
    uiLocale = locale == null ? "" : locale.toLanguageTag();
    Preferences preferences = Preferences.userNodeForPackage(getClass());
    preferences.put("uiLocale", uiLocale);    
  }
}
