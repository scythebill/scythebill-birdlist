package com.scythebill.birdlist.model.util;

import java.util.Iterator;

import javax.annotation.Nullable;

import com.google.common.collect.Ordering;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Taxon;

public class ResolvedComparator extends Ordering<Resolved> {
  @Override public int compare(@Nullable Resolved left, @Nullable Resolved right) {
    if (left.getType() == SightingTaxon.Type.SINGLE) {
      // Single-Single:  just compare indices
      if (right.getType() == SightingTaxon.Type.SINGLE) {
        return left.getTaxon().getTaxonomyIndex() - right.getTaxon().getTaxonomyIndex();
      } else {
        int maxIndex = getMaxIndex(right);
        return left.getTaxon().getTaxonomyIndex() <= maxIndex  ? -1 : 1;
      }
    } else if (right.getType() == SightingTaxon.Type.SINGLE) {
      int maxIndex = getMaxIndex(left);
      return maxIndex < right.getTaxon().getTaxonomyIndex() ? -1 : 1;      
    }

    // Multiple-multiple: start by comparing each max 
    int leftMax = getMaxIndex(left);
    int rightMax = getMaxIndex(right);
    if (leftMax != rightMax) {
      return leftMax - rightMax;
    }
    
    // Otherwise compare each sub-taxon sequentially (note that they're
    // always sorted taxonomically).
    Iterator<Taxon> leftIterator = left.getTaxa().iterator();
    Iterator<Taxon> rightIterator = right.getTaxa().iterator();
    while (leftIterator.hasNext() && rightIterator.hasNext()) {
      Taxon nextLeft = leftIterator.next();
      Taxon nextRight = rightIterator.next();
      if (nextLeft != nextRight) {
        return nextLeft.getTaxonomyIndex() - nextRight.getTaxonomyIndex();
      }
    }
    
    // Whichever ran out first comes first
    if (leftIterator.hasNext()) {
      return 1;
    } else if (rightIterator.hasNext()) {
      return -1;
    }
    
    // And finally compare types.  (Specifically, "Sp." comes before hybrids)
    return left.getType().compareTo(right.getType());
  }

  private int getMaxIndex(Resolved right) {
    int max = -1;
    for (Taxon taxon : right.getTaxa()) {
      max = Math.max(max, taxon.getTaxonomyIndex());
    }
    return max;
  }

}
