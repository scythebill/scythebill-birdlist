/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.util;

import java.util.Comparator;

import com.scythebill.birdlist.model.taxa.Taxon;

/**
 * Compares taxa by taxonomic order.
 */
public class TaxonComparator implements Comparator<Taxon> {
  public TaxonComparator() {
  }

  @Override
  public int compare(Taxon first, Taxon second) {
    if (first.getTaxonomy() != second.getTaxonomy()) {
      throw new IllegalArgumentException(
          String.format("%1$s and %2$s are not in the same taxonomy", first, second));
    }
    // Eliminate the possibility that they are the same
    if (first == second) {
      return 0;
    }
    
    int firstDepth = getDepth(first);
    int secondDepth = getDepth(second);
    // First is the root:  first < second
    if (firstDepth == 0) {
      return -1;
    }

    // Second is the root: second < first
    if (secondDepth == 0) {
      return 1;
    }

    // If first is deeper than second, move first up to
    // a common level
    if (firstDepth > secondDepth) {
      first = getAncestor(first, firstDepth - secondDepth);
    }
    
    // Vice versa
    if (secondDepth > firstDepth) {
      second = getAncestor(second, secondDepth - firstDepth);
    }

    // If one is an ancestor of the other, then the one
    // at greater depth comes last
    if (first == second) {
      return firstDepth - secondDepth;
    }
    
    // Walk up until we get to the level just below being the same parent
    while (first.getParent() != second.getParent()) {
      first = first.getParent();
      second = second.getParent();
    }

    // And, finally, we have a common ancestor - which one comes first?
    Taxon parent = first.getParent();
    return parent.getContents().indexOf(first) - parent.getContents().indexOf(second);
  }

  static private Taxon getAncestor(Taxon taxon, int levels) {
    while (levels > 0) {
      levels--;
      taxon = taxon.getParent();
    }
    
    return taxon;
  }
  
  /**
   * @return the "depth" of a taxon - 0 for the root,
   * 1 for a child of the root, etc.
   */
  static private int getDepth(Taxon taxon) {
    int depth = 0;
    while ((taxon = taxon.getParent()) != null) {
      depth++;
    }
  
    return depth;
  }
}
