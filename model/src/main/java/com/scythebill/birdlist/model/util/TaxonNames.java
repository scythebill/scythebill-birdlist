/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.util;

import java.util.List;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * Utilities for processing the names of taxa.
 */
public class TaxonNames {

  private static final Splitter SPLIT_NAMES = Splitter.on(' ');
  private static final Joiner JOIN_NAMES = Joiner.on(' ');
  
  /**
   * Join a list of names which may share common content at the end.  For example, 
   * join "Glaucous-winged Gull" and "Western Gull" to "Glaucous-winged/Western Gull". 
   */
  public static final String joinFromEnd(List<String> names, Joiner join) {
    if (names.size() == 1) {
      return names.get(0);
    }
    List<List<String>> components = Lists.newArrayList();
    int shortest = Integer.MAX_VALUE;
    for (String name : names) {
      List<String> split = SPLIT_NAMES.splitToList(name);
      components.add(split);
      shortest = Math.min(shortest, split.size());
    }
    
    // If one of the names has just one entry, then no abbreviation is possible
    if (shortest == 1) {
      return join.join(names);
    }
    
    List<String> commonNames = Lists.newArrayList();
    while (true) {
      String nameOfFirst = getNthFromEnd(components.get(0), commonNames.size());
      if (nameOfFirst == null) {
        break;
      }
      
      boolean allMatch = true;
      for (List<String> component : Iterables.skip(components, 1)) {
        String name = getNthFromEnd(component, commonNames.size());
        if (name == null || !name.equals(nameOfFirst)) {
          allMatch = false;
          break;
        }
      }
      if (!allMatch) {
        break;
      }
      commonNames.add(0, nameOfFirst);
      if (commonNames.size() >= shortest - 1) {
        break;
      }
    }
    
    List<String> nonCommonNames = Lists.newArrayList();
    for (List<String> component : components) {
      nonCommonNames.add(JOIN_NAMES.join(
              component.subList(0, component.size() - commonNames.size())));
    }
    commonNames.add(0, join.join(nonCommonNames));
    return JOIN_NAMES.join(commonNames);
  }

  private static String getNthFromEnd(List<String> list, int nth) {
    int index = list.size() - nth - 1;
    return index < 0 ? null : list.get(index);
  }

  /**
   * Join a list of names which may share common content at the start.  For example, 
   * join Larus occidentalis and Larus glaucescens into Larus occidentalis/glaucescens. 
   */
  public static final String joinFromStart(List<String> names, Joiner join) {
    if (names.size() == 1) {
      return names.get(0);
    }
    List<List<String>> components = Lists.newArrayList();
    for (String name : names) {
      components.add(Lists.newArrayList(SPLIT_NAMES.split(name)));
    }
    
    List<String> commonNames = Lists.newArrayList();
    while (true) {
      String nameOfFirst = getNthFromStart(components.get(0), commonNames.size());
      if (nameOfFirst == null) {
        break;
      }
      
      boolean allMatch = true;
      for (List<String> component : Iterables.skip(components, 1)) {
        String name = getNthFromStart(component, commonNames.size());
        if (name == null || !name.equals(nameOfFirst)) {
          allMatch = false;
          break;
        }
      }
      if (!allMatch) {
        break;
      }
      commonNames.add(nameOfFirst);
    }
    
    List<String> nonCommonNames = Lists.newArrayList();
    for (List<String> component : components) {
      nonCommonNames.add(JOIN_NAMES.join(component.subList(
              commonNames.size(), component.size())));
    }
    commonNames.add(join.join(nonCommonNames));
    return JOIN_NAMES.join(commonNames);
  }

  private static String getNthFromStart(List<String> list, int nth) {
    return nth >= list.size() ? null : list.get(nth);
  }
}
