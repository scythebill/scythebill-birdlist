package com.scythebill.birdlist.model.util;

import java.util.concurrent.Future;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.IndexerBuilder.Indexers;

/**
 * Builds and registers indexers for a taxonomy.
 * 
 * TODO: consider moving this to be part of the TaxonomyImpl class?
 */
public class TaxonomyIndexer {
  private final IndexerBuilder commonIndexLoader;
  private final IndexerBuilder sciIndexLoader;
  private final Future<? extends Taxonomy> taxonomyFuture;

  public TaxonomyIndexer(final Future<? extends Taxonomy> taxonomyFuture) {
    this.taxonomyFuture = taxonomyFuture;
    Function<Species, Iterable<String>> alternateCommonNames = Species::getAlternateCommonNames;
    commonIndexLoader = new IndexerBuilder(taxonomyFuture,
          new ToString<Taxon>() {
        @Override public String getString(Taxon taxon) {
          return taxon.getCommonName();
        }
    
        @Override public String getPreviewString(Taxon taxon) {
          return getString(taxon);
        }
    }, alternateCommonNames, true /* include subspecies */);
    Function<Species, Iterable<String>> alternateNames = Species::getAlternateNames;
    sciIndexLoader = new IndexerBuilder(taxonomyFuture,
        new ToString<Taxon>() {
      @Override public String getString(Taxon taxon) {
        return TaxonUtils.getFullName(taxon);
      }

      @Override public String getPreviewString(Taxon taxon) {
        return getString(taxon);
      }
    }, alternateNames, false /* don't include subspecies */);
  }

  public ListenableFuture<?> load(ListeningExecutorService executorService) {
    ListenableFuture<Indexers> commonIndexerFuture = executorService.submit(commonIndexLoader);
    Futures.addCallback(commonIndexerFuture, new FutureCallback<Indexers>() {
      @Override public void onSuccess(Indexers result) {
        Futures.getUnchecked(taxonomyFuture).setCommonIndexer(result.indexer);
        Futures.getUnchecked(taxonomyFuture).setAlternateCommonIndexer(result.alternateIndexer);
      }

      @Override
      public void onFailure(Throwable t) {
        throw new AssertionError(t);
      }
    }, executorService);
    ListenableFuture<Indexers> sciIndexerFuture = executorService.submit(sciIndexLoader);
    Futures.addCallback(sciIndexerFuture, new FutureCallback<Indexers>() {
      @Override public void onSuccess(Indexers result) {
        Futures.getUnchecked(taxonomyFuture).setScientificIndexer(result.indexer);
        Futures.getUnchecked(taxonomyFuture).setAlternateScientificIndexer(result.alternateIndexer);
      }

      @Override
      public void onFailure(Throwable t) {
        throw new AssertionError(t);
      }
    }, executorService);
    
    return Futures.allAsList(ImmutableList.of(commonIndexerFuture, sciIndexerFuture));
  }
  
  public Progress getCommonIndexLoader() {
    return commonIndexLoader;
  }

  public Progress getScientificIndexLoader() {
    return sciIndexLoader;
  }
  
  
}
