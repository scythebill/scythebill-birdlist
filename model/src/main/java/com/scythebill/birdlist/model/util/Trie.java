/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.util;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Lists;
import com.google.common.collect.Multiset;

/**
 * Implements a prefix trie.  Values (of arbitrary type) are associated with strings, and
 * can be looked up by any prefix of that string.
 * <p>
 * TODO: not inherently thread-safe , so needs to be published in a thread-safe manner.
 * Consider support an immutable subset API, where publishing to that API can do a memory
 * optimization step to trim down resource requirements and use more efficient memory structures
 * (e.g., instead of a List of Nodes, using an array-by-character-value).  Worth measuring first
 * how much memory these tries are actually using.
 */
public class Trie<T> {
  private final List<Node<T>> nodes;

  public Trie() {
    nodes = Lists.newArrayListWithCapacity(5);
  }
  
  public Multiset<Integer> computeNodeSizes() {
    Multiset<Integer> sizes = HashMultiset.create();
    for (Node<T> node : nodes) {
      node.computeNodeSizes(sizes);
    }
    return sizes;
  }

  /**
   * Add a value associated with a name.
   */
  public void add(String name, T value) {
    // See if one of the children has a node
    for (int i = 0; i < nodes.size(); i++) {
      Node<T> node = nodes.get(i);
      Node<T> destinationNode = node.add(name, 0, value);
      // Didn't find anything - next iteration
      if (destinationNode == null)
        continue;
      // Non-null, so we did find something:  but is 
      // it a new node replacing this existing node?

      // Yes, replace that node with its new form
      if (destinationNode != node) {
        nodes.set(i, destinationNode);
      }

      // At any rate, the node has been added - so we're done
      return;
    }

    // Couldn't find it - we need a new top-level node
    nodes.add(new LeafNode<T>(value, name));
  }

  /**
   * Find all values that match a prefix
   */
  public Collection<T> findMatches(String prefix) {
    List<T> results = Lists.newArrayList();
    findMatches(results, prefix, null);
    return results;
  }

  /**
   * Find all values that match a prefix, filling in
   * a results collection.
   */
  public void findMatches(Collection<T> results, String prefix,
      Collection<T> within) {
    for (Node<T> node : nodes) {
      if (node.findMatches(prefix, results, within)) {
        break;
      }
    }
  }

  public boolean containsPrefixed(String string) {
    for (Node<T> node : nodes) {
      if (node.containsPrefixes(string, 0)) {
        return true;
      }
    }
    
    return false;
  }

  /**
   * Remove a particular object associated with a name
   */
  public boolean remove(String name, T value) {
    // See if one of the children has a node
    for (int i = 0; i < nodes.size(); i++) {
      Node<T> node = nodes.get(i);
      RemoveAction action = node.remove(name, value);
      switch (action) {
        case NOT_FOUND_BUT_DONE_LOOKING:
          return false;
        case FOUND_WITHIN:
          return true;
        case FOUND_AND_IM_GONE:
          nodes.remove(i);
          return true;
        case NOT_FOUND:
          break;
      }
    }

    return false;
  }

  /** Base class for a node */
  static private abstract class Node<T> {
    abstract public boolean findMatches(String prefix, Collection<T> results,
        Collection<T> within);

    abstract public boolean containsPrefixes(String string, int index);

    protected void computeNodeSizes(Multiset<Integer> sizes) {
    }

    abstract public Node<T> add(String name, int index, T value);

    abstract public RemoveAction remove(String name, T value);

    abstract protected void allValues(Collection<T> results,
        Collection<T> within);
  }

  static private class InternalNode<T> extends Node<T> {
    private final List<Node<T>> children;
    private final char character;

    public InternalNode(char c) {
      character = c;
      children = Lists.newArrayList();
    }

    @Override public String toString() {
      return "[Internal:" + character + "]";
    }

    @Override
    public boolean containsPrefixes(String string, int index) {
      if (string.length() > index
          && string.charAt(index) == character) {
        for (Node<T> child : children) {
          if (child.containsPrefixes(string, index + 1)) {
            return true;
          }
        }
      }
      return false;
    }

    @Override
    protected void computeNodeSizes(Multiset<Integer> sizes) {
      sizes.add(children.size());
      for (Node<T> child : children) {
        child.computeNodeSizes(sizes);
      }
    }
    
    @Override public boolean findMatches(String prefix, Collection<T> results,
        Collection<T> within) {
      if (normalizedCharAt(prefix, 0) == character) {
        if (prefix.length() == 1) {
          allValues(results, within);
        } else {
          String prefixMinusFirst = prefix.substring(1);
          for (Node<T> node : children) {
            if (node.findMatches(prefixMinusFirst, results, within))
              break;
          }
        }

        // This is our prefix.  Whether or not we found any real results,
        // we're done finding matches
        return true;
      }

      return false;
    }

    @Override protected void allValues(Collection<T> results,
        Collection<T> within) {
      for (Node<T> node : children) {
        node.allValues(results, within);
      }
    }

    @Override public Node<T> add(String name, int index, T value) {
      // See if this is our responsibility
      if (name.length() > index && normalizedCharAt(name, index) == character) {
        // See if one of the children has a node
        for (int i = 0; i < children.size(); i++) {
          Node<T> node = children.get(i);
          Node<T> destinationNode = node.add(name, index + 1, value);
          // Didn't find anything - next iteration
          if (destinationNode == null)
            continue;
          // Non-null, so we did find something:  but is 
          // it a new node replacing this existing node?

          // Yes, replace that node with its new form
          if (destinationNode != node) {
            children.set(i, destinationNode);
          }

          // Return ourselves, indicating that the node was added, but to
          // ourselves so no replacement is needed
          return this;
        }

        // Couldn't find it - we need a new node
        children.add(new LeafNode<T>(value, name.substring(index + 1)));
        return this;
      }

      // Not us - add the node somewhere else
      return null;
    }

    @Override public RemoveAction remove(String name, T value) {
      // See if this is our responsibility
      if (name.length() > 0 && normalizedCharAt(name, 0) == character) {
        name = name.substring(1);
        // See if one of the children has a node
        for (int i = 0; i < children.size(); i++) {
          Node<T> node = children.get(i);
          RemoveAction action = node.remove(name, value);
          if (action == RemoveAction.NOT_FOUND)
            continue;
          if (action == RemoveAction.FOUND_AND_IM_GONE) {
            children.remove(i);
          }

          break;
        }

        return (children.isEmpty() ? RemoveAction.FOUND_AND_IM_GONE
            : RemoveAction.FOUND_WITHIN);
      }

      return RemoveAction.NOT_FOUND;
    }
  }
  
  /** List of possible states when recursing through a trie removing a node. */ 
  static private enum RemoveAction {
    NOT_FOUND, NOT_FOUND_BUT_DONE_LOOKING, FOUND_WITHIN, FOUND_AND_IM_GONE
  }

  static private class LeafNode<T> extends Node<T> {
    private String suffix;
    private short length;
    private char firstChar;
    private final List<T> values;

    public LeafNode(T firstValue, String suffix) {
      values = Lists.newArrayList();
      values.add(firstValue);
      this.suffix = normalizeString(suffix);
      storeLengthAndFirstChar();
    }

    @Override
    public boolean containsPrefixes(String string, int index) {
      if (string.length() < suffix.length() + index) {
        return false;
      }
        
      return string.regionMatches(index, suffix, 0, suffix.length());
    }
    
    @Override public boolean findMatches(String prefix, Collection<T> results,
        Collection<T> within) {
      // If this prefix is in fact a match, then add all of the target results
      // Presumably, this is faster than using startsWith()...
      //      if (suffix.startsWith(prefix.substring(index)))
      if (suffix.startsWith(prefix)) {
        allValues(results, within);
        return true;
      } else {
        return false;
      }
    }

    @Override protected void allValues(Collection<T> results,
        Collection<T> within) {
      if (within == null) {
        results.addAll(values);
      } else {
        int size = values.size();
        for (int i = 0; i < size; i++) {
          T value = values.get(i);
          if (within.contains(value))
            results.add(value);
        }
      }
    }

    private boolean _matches(String prefix, int index) {
      // Presumably, this is faster than using startsWith()...
      //      if (suffix.startsWith(prefix.substring(index)))
      if (prefix.length() - index != length)
        return false;

      return suffix.regionMatches(0, prefix, index, length);

    }

    @Override public RemoveAction remove(String name, T value) {
      if (name.equals(suffix)) {
        values.remove(value);
        return (values.isEmpty() ? RemoveAction.FOUND_AND_IM_GONE
            : RemoveAction.FOUND_WITHIN);
      } else {
        return RemoveAction.NOT_FOUND;
      }
    }

    @Override public Node<T> add(String name, int index, T value) {
      // If we have a complete match, then just add it to ourselves and we're done
      if (_matches(name, index)) {
        values.add(value);
        return this;
      }

      if (name.length() == index)
        return null;

      // First character matches:  Have to create a new InternalNode matching
      // our first character, and hang the new entry and all-but-the-first-character
      // of ourselves off that new node. 
      char c = normalizedCharAt(name, index);
      if (c == firstChar) {
        InternalNode<T> newNode = new InternalNode<T>(c);
        // Strip ourselves down by one character
        suffix = suffix.substring(1);
        storeLengthAndFirstChar();

        // Put ourselves in the parent
        newNode.children.add(this);
        // And return this new node after re-running the add
        return newNode.add(name, index, value);
      }

      return null;
    }

    private void storeLengthAndFirstChar() {
      length = (short) suffix.length();
      if (length == 0)
        firstChar = (char) 0;
      else
        firstChar = normalizedCharAt(suffix, 0);
    }

    @Override public String toString() {
      return "[Leaf:" + firstChar + "," + suffix + "]";
    }
  }

  /**
   * Normalize a string for searching. 
   * 
   * TODO: refactor this out of the Trie code;  normalization should ideally be
   * pluggable.
   */
  static public String normalizeString(String s) {
    int length = s.length();
    StringBuilder builder = null;
    int i = 0;
    
    // Find the first character that does not need to be normalized
    for (; i < length; i++) {
      char c = s.charAt(i);
      if (normalizeChar(c) != c) {
        builder = new StringBuilder(length);
        builder.append(s.substring(0, i));
        break;
      }
    }
    
    // If nothing had to be normalized (typically, an all-lowercase word) then
    // return the original string
    if (builder == null) {
      return s;
    }
    
    // Else quit
    for (; i < length; i++) {
      builder.append(normalizedCharAt(s, i));
    }
    return builder.toString();
  }

  static private char normalizedCharAt(String s, int index) {
    return normalizeChar(s.charAt(index));
  }

  static private final char[] normalizedChars = new char[0x17f];
  static {
    for (int i = 0; i < normalizedChars.length; i++) {
      normalizedChars[i] = normalizeCharSlow((char) i);
    }
  }
  
  // Diacritical normalization courtesy of https://stackoverflow.com/a/10831704.
  
  /**
   * Mirror of the unicode table from 0100 to 017f without diacritics.
   */
  private static final String NORMALIZED_100_TABLE = "aaaaaaccccccccdd"
      + "ddeeeeeeeeeegggg" + "gggghhhhiiiiiiii" + "iijjjjkkklllllll"
      + "lllnnnnnnnnnoooo" + "oooorrrrrrssssss" + "ssttttttuuuuuuuu"
      + "uuuuwwyyyzzzzzzf";

  static private char normalizeChar(char c) {
    if (c > 0x17f) {
      return c;
    }
    return normalizedChars[c];
  }
  
  // Normalization for ISO-8859-1 character set:  strip accents,
  // cedillas, tildes, and lowercase
  static private char normalizeCharSlow(char c) {
    c = Character.toLowerCase(c);
    switch (c) {
      case 224:
      case 225:
      case 226: 
      case 227:
      case 228:
      case 229:
        return 'a';
      case 231:
        return 'c';
      case 232:
      case 233:
      case 234:
      case 235:
        return 'e';
      case 236:
      case 237:
      case 238:
      case 239:
        return 'i';
      case 241:
        return 'n';
      case 240:
      case 242:
      case 243:
      case 244:
      case 245:
      case 246:
      case 248:
        return 'o';
      case 249:
      case 250:
      case 251:
      case 252:
        return 'u';
      case 253:
      case 255:
        return 'y';
      default:
        if (c >= 0x100 && c <= 0x17f) {
          return NORMALIZED_100_TABLE.charAt(c - 0x100);
        }
        return c;
    }
  }
}
