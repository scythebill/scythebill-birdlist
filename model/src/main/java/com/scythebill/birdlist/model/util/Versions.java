/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.util;

import java.util.Iterator;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Ordering;

/**
 * Utilities for processing version strings.
 */
public class Versions {
  private final static CharMatcher ALLOWED_CHARACTERS = CharMatcher.anyOf("0123456789.");
  private final static Splitter VERSION_SPLITTER = Splitter.on('.').omitEmptyStrings();
  
  public static Ordering<String> versionOrdering() {
    return new Ordering<String>() {
      @Override public int compare(String left, String right) {
        left = ALLOWED_CHARACTERS.retainFrom(left);
        right = ALLOWED_CHARACTERS.retainFrom(right);
        Iterator<String> leftIter = VERSION_SPLITTER.split(left).iterator();
        Iterator<String> rightIter = VERSION_SPLITTER.split(right).iterator();
        
        while (leftIter.hasNext() && rightIter.hasNext()) {
          int nextLeft = Integer.parseInt(leftIter.next());
          int nextRight = Integer.parseInt(rightIter.next());
          if (nextLeft != nextRight) {
            return nextLeft - nextRight; // ??
          }
        }
        
        if (leftIter.hasNext()) {
          return 1;
        } else if (rightIter.hasNext()) {
          return -1;
        }
        return 0;
      }
    };
  }
}
