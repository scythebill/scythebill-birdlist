/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.util;

import java.util.Comparator;
import java.util.Set;

import com.google.common.collect.Sets;

/** 
 * Utility for calculating word separators.
 */
public class WordSeparator {
  // Much above 15 and splitting gets reaaaaally slow (it's 2^N)
  private static final int MAX_LENGTH_TO_SPLIT = 15;
  private final int maxWords;
  
  public WordSeparator(int maxWords) {
    this.maxWords = maxWords;
  }
  
 /**
  * Returns an ordered list of boundary arrays, where each array is
  * at most maxWords integers, the last integer is the length itself,
  * and otherwise an increasing positive set of integers.  For example,
  * for getBoundaries(3, 3), it would return:
  * {3}, {2, 3}, {1, 3}, {1, 2, 3}
  * where the first means "take the whole 3 letters",
  * the second means "split 0-2, and 2-3",
  * and the last means "split 0-1, 1-2, 2-3".
  */
  public Iterable<int[]> getBoundaries(int length) {
    return getBoundaries(length, maxWords);
  }
  
  /**
   * Helper for getBoundaries().  Result of this could be globally
   * cached if it ever matters.
   */
  private static Iterable<int[]> getBoundaries(int length, int maxWords) {
    Set<int[]> list = Sets.newTreeSet(_PREFER_SHORTER_AND_LOWER);
    length = Math.min(length, MAX_LENGTH_TO_SPLIT);
    
    for (int i = 0; i < (1 << length - 1); i++) {
      int bitCount = countBits(i) + 1;
      if (bitCount > maxWords) {
        continue;
      }
      
      int[] separator = new int[bitCount];
      separator[bitCount - 1] = length;
      
      int bitIndex = 0;
      int bit = 1;
      int separatorIndex = 0;
      while (bit <= i) {
        if ((bit & i) != 0) {
          separator[separatorIndex++] = bitIndex + 1;
        }
        
        bit <<= 1;
        bitIndex++;
      }
      
      list.add(separator);
    }
    
    return list;
  }
  
  private static int countBits(int i) {
    int count = 0;
    while (i > 0) {
      count++;
      i = i & (i - 1);
    }
    
    return count;
  }
  
  /**
   * Comparator for choosing a "better" array.  Basically, we choose
   * (1) The shorter array (less words matched) the better
   * (2) Take the product of the length of each substring - higher is better.
   *    This effectively takes more balanced strings. (wi/wa over w/iwa).
   * (3) A tiebeaker just comparing each entry, take more characters. 
   */
  private static final Comparator<int[]> _PREFER_SHORTER_AND_LOWER = new Comparator<int[]>() {
    @Override
    public int compare(int[] o1, int[] o2) {
      if (o1.length != o2.length) {
        return o1.length - o2.length;
      }
      
      int diffOfProducts = product(o2) - product(o1);
      if (diffOfProducts != 0) {
        return diffOfProducts;
      }
      
      return pairwise(o1, o2);
    }

  };

  private static int pairwise(int[] o1, int[] o2) {
    for (int i = 0; i < o1.length; i++) {
      int delta = o2[i] - o1[i];
      if (delta != 0) {
        return delta;
      }
    }

    return 0;
  }

  private static int product(int[] array) {
    int product = 1;
    int prev = 0;
    // no: want product of the diffs between each *maximized*
    for (int i : array) {
      product *= (i - prev);
      prev = i;
    }
    
    return product;
  }
}
