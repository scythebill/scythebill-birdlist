/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.xml;

import java.util.Collection;

import com.scythebill.birdlist.model.checklist.ExtendedTaxonomyChecklists;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;
import com.scythebill.xml.ParseContext;

/**
 * Simple class for parsing an "extended" taxonomy - one which is not considered "built-in".
 */
public class ExtendedTaxonomyNodeParser extends XmlTaxonImport.TaxonomyImplParser {
  public static final String ELEMENT_TAXONOMY = XmlTaxonExport.ELEMENT_TAXONOMY;
  private static final Object CHECKLIST_BUILDER_KEY = new Object();
  
  public static void startChecklistParsing(ParseContext parseContext) {
    parseContext.setProperty(CHECKLIST_BUILDER_KEY, new ExtendedTaxonomyChecklists.Builder());
  }
  
  public static ExtendedTaxonomyChecklists endChecklistParsing(ParseContext parseContext) {
    ExtendedTaxonomyChecklists.Builder builder = (ExtendedTaxonomyChecklists.Builder) parseContext.getProperty(CHECKLIST_BUILDER_KEY);
    parseContext.setProperty(CHECKLIST_BUILDER_KEY, null);
    return builder.build();
  }
  
  public static boolean canParseChecklists(ParseContext parseContext) {
    return parseContext.getProperty(CHECKLIST_BUILDER_KEY) != null;
  }
  
  public static void addChecklistElements(ParseContext parseContext, SightingTaxon taxon,
      Species.Status status, Collection<String> checklistIds) {
    ExtendedTaxonomyChecklists.Builder builder = (ExtendedTaxonomyChecklists.Builder) parseContext.getProperty(CHECKLIST_BUILDER_KEY);
    builder.addEntries(taxon, status, checklistIds);
  }
  
  @Override
  protected TaxonomyImpl newTaxonomy(String id, String name, String accountUrlFormat, String accountUrlTitle) {
    return new TaxonomyImpl(id, name, accountUrlFormat, accountUrlTitle) {
      @Override
      public boolean isBuiltIn() {
        return false;
      }
    };
  }          
}
