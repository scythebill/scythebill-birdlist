/*   
 * Copyright 2023 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.xml;

import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.collect.TreeMultimap;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Utilities for parsing extended taxonomies.
 */
public class ExtendedTaxonomyParsing {
  private final static Pattern CHECKLIST_WITH_STATUS = Pattern.compile("(.*)\\((.*)\\)");

  private ExtendedTaxonomyParsing() {}
  
  public static TreeMultimap<String, String> buildTransposedChecklistWithStatuses(
      Map<String, Checklist> checklistsByLocationCode,
      Taxonomy taxonomy) {
    TreeMultimap<String, String> transposedChecklists = TreeMultimap.create();
    for (String locationCode : checklistsByLocationCode.keySet()) {
      Checklist checklist = checklistsByLocationCode.get(locationCode);
      for (SightingTaxon taxon : checklist.getTaxa(taxonomy)) {
        Checklist.Status status = checklist.getStatus(taxonomy, taxon);
        String locationCodeWithStatus;
        switch (status) {
          case NATIVE:
            locationCodeWithStatus = locationCode;
            break;
          case ENDEMIC:
            locationCodeWithStatus = locationCode + "(E)";
            break;
          case ESCAPED:
            locationCodeWithStatus = locationCode + "(ES)";
            break;
          case EXTINCT:
            locationCodeWithStatus = locationCode + "(EX)";
            break;
          case RARITY:
            locationCodeWithStatus = locationCode + "(R)";
            break;
          case RARITY_FROM_INTRODUCED:
            locationCodeWithStatus = locationCode + "(RI)";
            break;
          case INTRODUCED:
            locationCodeWithStatus = locationCode + "(I)";
            break;
          default:
            throw new AssertionError("Unexpected status " + status);
        }
        
        transposedChecklists.put(taxon.getId(), locationCodeWithStatus);
      }
    }
    
    return transposedChecklists;
  }
  
  public record LocationAndStatus (String locationCode, Checklist.Status status) {
    public LocationAndStatus {
      Objects.requireNonNull(locationCode);
    }
    
    public LocationAndStatus(String locationCode) {
      this(locationCode, Checklist.Status.NATIVE);
    }
  }
  
  public static LocationAndStatus locationAndStatus(String locationCodeWithStatus) {
    Matcher matcher = CHECKLIST_WITH_STATUS.matcher(locationCodeWithStatus);
    Checklist.Status checklistStatus;
    if (!matcher.matches()) {
      return new LocationAndStatus(locationCodeWithStatus);
    } else {
      String locationCode = matcher.group(1);
      String statusString = matcher.group(2);
      switch (statusString) {
        case "I":
          checklistStatus = Checklist.Status.INTRODUCED;
          break;
        case "ES":
          checklistStatus = Checklist.Status.ESCAPED;
          break;
        case "EX":
          checklistStatus = Checklist.Status.EXTINCT;
          break;
        case "R":
        case "V":
          checklistStatus = Checklist.Status.RARITY;
          break;
        case "RI":
          checklistStatus = Checklist.Status.RARITY_FROM_INTRODUCED;
          break;
        case "E":
          checklistStatus = Checklist.Status.ENDEMIC;
          break;
        default:
          checklistStatus = Checklist.Status.NATIVE;
          break;
      }
      
      return new LocationAndStatus(locationCode, checklistStatus);
    }
  }
}
