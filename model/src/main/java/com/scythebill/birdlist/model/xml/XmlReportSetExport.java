/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.xml;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalTime;
import org.joda.time.ReadablePartial;

import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.ClementsChecklist;
import com.scythebill.birdlist.model.io.IndentingResponseWriter;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.io.ResponseWriter;
import com.scythebill.birdlist.model.io.TimeIO;
import com.scythebill.birdlist.model.io.XmlResponseWriter;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Photo;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo.BreedingBirdCode;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.user.User;

/**
 * Writes out Locations and Sightings to a single XML file
 * TODO: Should there be support for multiple users for sightings (single location
 *   DB, multiple sighting DBs?)
 */
public class XmlReportSetExport {
  private static final Logger logger = Logger.getLogger(XmlReportSetExport.class.getName());
  
  static final String ATTRIBUTE_VERSION = "version";
  static final String ATTRIBUTE_TAXONOMY_ID = "taxonomy";
  static final String ATTRIBUTE_NAME = "name";
  static final String ATTRIBUTE_ID = "id";
  static final String ATTRIBUTE_EBIRD_CODE = "ebirdCode";
  static final String ELEMENT_STATE = "state";
  static final String ELEMENT_REGION = "region";
  static final String ELEMENT_COUNTY = "county";
  static final String ELEMENT_COUNTRY = "country";
  static final String ELEMENT_CITY = "city";
  static final String ELEMENT_TOWN = "town";
  static final String ELEMENT_PARK = "park";
  static final String ELEMENT_LOCATION = "location";
  static final String ELEMENT_PREFS = "prefs";
  static final String ELEMENT_DESCRIPTION = "description";
  static final String ATTRIBUTE_ADULT = "adult";
  static final String ATTRIBUTE_HEARD_ONLY = "heardOnly";
  static final String ATTRIBUTE_IMMATURE = "immature";
  static final String ATTRIBUTE_FEMALE = "female";
  static final String ATTRIBUTE_MALE = "male";
  static final String ATTRIBUTE_COUNT = "count";
  static final String ATTRIBUTE_DATE = "date";
  static final String ATTRIBUTE_TIME = "time";
  static final String ATTRIBUTE_LOCATION = "loc";
  static final String ATTRIBUTE_LATITUDE = "lat";
  static final String ATTRIBUTE_LONGITUDE = "long";
  static final String ATTRIBUTE_TAXON = "taxon";
  static final String ATTRIBUTE_SP = "sp";
  static final String ATTRIBUTE_HYBRID = "hybrid";
  static final String ATTRIBUTE_TAXON_WITH_SSP = "taxonSsp";
  static final String ATTRIBUTE_USERS = "users";
  // Called "popStatus" from when this used to be PopulationStatus
  static final String ATTRIBUTE_SIGHTING_STATUS = "popStatus";
  static final String ATTRIBUTE_BREEDING_CODE = "breeding";
  static final String ATTRIBUTE_PHOTOGRAPHED = "photo";
  static final String ATTRIBUTE_PRIVATE = "private";
  static final String ELEMENT_SIGHTING = "sighting";
  static final String ELEMENT_PHOTO = "photo";
  static final String ATTRIBUTE_FAVORITE = "favorite";
  static final String ELEMENT_LOCATIONS = "locations";
  static final String ELEMENT_REPORT_SET = "report-set";
  static final String ELEMENT_CHECKLIST = "checklist";
  static final String ELEMENT_CHECKLIST_ENTRY = "entry";
  static final String ATTRIBUTE_CHECKLIST_STATUS = "status";
  static final String ELEMENT_VISIT_INFO = "visit-info";
  static final String ELEMENT_USER = "user";
  static final String ATTRIBUTE_OBSERVATION_TYPE = "observationType";
  static final String ELEMENT_DISTANCE = "distance";
  static final String ELEMENT_DURATION = "duration";
  static final String ELEMENT_AREA = "area";
  static final String ELEMENT_PARTY_SIZE = "partySize";
  static final String ELEMENT_COMPLETE = "complete";
  static final String ELEMENT_COMMENT = "comments";
  static final String ATTRIBUTE_ABBREVIATION = "abbrev";
  static final String ELEMENT_ONE_TIME_UPGRADE = "one-time-upgrade"; 
  
  static final char SP_SEPARATOR = ',';
  static final Splitter SP_SPLITTER = Splitter.on(SP_SEPARATOR);
  static final Splitter HYBRID_SPLITTER = SP_SPLITTER;
  static final Joiner SP_JOINER = Joiner.on(SP_SEPARATOR);
  static final Joiner HYBRID_JOINER = SP_JOINER;
  static final char TAXON_WITH_SSP_SEPARATOR = '|';
  static final Splitter TAXON_WITH_SSP_SPLITTER = Splitter.on(TAXON_WITH_SSP_SEPARATOR);
  static final Joiner TAXON_WITH_SSP_JOINER = Joiner.on(TAXON_WITH_SSP_SEPARATOR);

  static final char USER_SEPARATOR = ',';
  static final Splitter USER_SPLITTER = Splitter.on(USER_SEPARATOR);

  // close() responsibility is on the caller;  this class flushes its buffers.
  @SuppressWarnings("resource")
  public void export(Writer out, String encoding, ReportSet reports, Taxonomy taxonomy)
      throws IOException {
    ResponseWriter rw = new XmlResponseWriter(out, encoding);
    rw = new IndentingResponseWriter(rw);
    rw.startDocument();
    rw.startElement(ELEMENT_REPORT_SET);
    rw.writeAttribute(ATTRIBUTE_VERSION, ReportSets.VERSION_FORMAT_CURRENT);
    rw.writeAttribute(ATTRIBUTE_TAXONOMY_ID, taxonomy.getId());
    
    // Write each extended taxonomy
    for (Taxonomy extendedTaxonomy : reports.extendedTaxonomies()) {
      new XmlTaxonExport()
          .withChecklists(reports.getExtendedTaxonomyChecklist(extendedTaxonomy.getId()))
          .writeTaxonomy(extendedTaxonomy, rw);
    }
    
    rw.startElement(ELEMENT_LOCATIONS);
    writeLocations(rw, reports.getLocations().rootLocations());
    rw.endElement(ELEMENT_LOCATIONS);
    
    // Write the UserSet, *before* any sightings
    if (reports.getUserSet() != null) {
      for (User user : reports.getUserSet().allUsers()) {
        rw.startElement(ELEMENT_USER);
        rw.writeAttribute(ATTRIBUTE_ID, user.id());
        if (user.name() != null) {
          rw.writeAttribute(ATTRIBUTE_NAME, user.name());
        }
        if (user.abbreviation() != null) {
          rw.writeAttribute(ATTRIBUTE_ABBREVIATION, user.abbreviation());
        }
        rw.endElement(ELEMENT_USER);
      }
    }

    writeSightings(rw, reports.getSightings(), taxonomy);
    
    for (Map.Entry<Location, Checklist> checklistsEntry : reports.checklists().entrySet()) {
      if (checklistsEntry.getKey().getId() == null) {
        logger.warning(String.format(
            "Location %s does not have an ID, cannot write checklist",
            checklistsEntry.getKey().getModelName()));
        continue;
      }
      rw.startElement(ELEMENT_CHECKLIST);
      rw.writeAttribute(ATTRIBUTE_LOCATION, checklistsEntry.getKey().getId());
      
      ClementsChecklist clementsChecklist = (ClementsChecklist) checklistsEntry.getValue();
      for (Map.Entry<SightingTaxon, Checklist.Status> checklistEntry : clementsChecklist.baseChecklistMap().entrySet()) {
        rw.startElement(ELEMENT_CHECKLIST_ENTRY);
        // TODO: support checklists of non-core-taxonomies
        writeSightingTaxon(rw, checklistEntry.getKey());
        String statusText = checklistEntry.getValue().text();
        if (statusText != null) {
          rw.writeAttribute(ATTRIBUTE_CHECKLIST_STATUS, statusText);
        }
        rw.endElement(ELEMENT_CHECKLIST_ENTRY);
      }
      
      rw.endElement(ELEMENT_CHECKLIST);
    }
    
    for (Map.Entry<VisitInfoKey, VisitInfo> visitInfoEntry : reports.visitInfos().entrySet()) {
      rw.startElement(ELEMENT_VISIT_INFO);
      VisitInfoKey key = visitInfoEntry.getKey();
      rw.writeAttribute(ATTRIBUTE_LOCATION, key.locationId());
      rw.writeAttribute(ATTRIBUTE_DATE, PartialIO.toString(key.date()));
      Optional<LocalTime> startTime = key.startTime();
      if (startTime.isPresent()) {
        rw.writeAttribute(ATTRIBUTE_TIME, TimeIO.toString(startTime.get()));
      }
      VisitInfo visitInfo = visitInfoEntry.getValue();
      rw.writeAttribute(ATTRIBUTE_OBSERVATION_TYPE, visitInfo.observationType().id());
      
      if (visitInfo.area().isPresent()) {
        rw.startElement(ELEMENT_AREA);
        rw.writeText(visitInfo.area().get().hectares());
        rw.endElement(ELEMENT_AREA);
      }

      if (visitInfo.distance().isPresent()) {
        rw.startElement(ELEMENT_DISTANCE);
        rw.writeText(visitInfo.distance().get().kilometers());
        rw.endElement(ELEMENT_DISTANCE);
      }
      
      if (visitInfo.duration().isPresent()) {
        long standardMinutes = visitInfo.duration().get().getStandardMinutes();
        // There've been bugs that produced negative durations; ignore anything that isn't positive. 
        if (standardMinutes > 0) {
          rw.startElement(ELEMENT_DURATION);
          rw.writeText(visitInfo.duration().get().getStandardMinutes());
          rw.endElement(ELEMENT_DURATION);
        }
      }
      
      if (visitInfo.partySize().isPresent()) {
        rw.startElement(ELEMENT_PARTY_SIZE);
        rw.writeText(visitInfo.partySize().get());
        rw.endElement(ELEMENT_PARTY_SIZE);
      }
      
      if (visitInfo.comments().isPresent()) {
        rw.startElement(ELEMENT_COMMENT);
        rw.writeText(visitInfo.comments().get());
        rw.endElement(ELEMENT_COMMENT);
      }

      if (visitInfo.completeChecklist()) {
        rw.startElement(ELEMENT_COMPLETE);
        rw.endElement(ELEMENT_COMPLETE);
      }
      
      rw.endElement(ELEMENT_VISIT_INFO);
    }

    if (!StringUtils.isEmpty(reports.getPreferencesJson())) {
      rw.startElement(ELEMENT_PREFS);
      rw.writeText(reports.getPreferencesJson());
      rw.endElement(ELEMENT_PREFS);
    }
    for (String oneTimeUpgrade : reports.oneTimeUpgrades()) {
      rw.startElement(ELEMENT_ONE_TIME_UPGRADE);
      rw.writeText(oneTimeUpgrade);
      rw.endElement(ELEMENT_ONE_TIME_UPGRADE);
    }
    
    rw.endElement(ELEMENT_REPORT_SET);
    
    rw.endDocument();
    rw.flush();
  }

  private void writeSightings(ResponseWriter rw, Collection<Sighting> sightings, Taxonomy coreTaxonomy)
      throws IOException {
    for (Sighting sighting : sightings) {
      rw.startElement(ELEMENT_SIGHTING);
      if (sighting.getTaxonomy() != coreTaxonomy) {
        rw.writeAttribute(ATTRIBUTE_TAXONOMY_ID, sighting.getTaxonomy().getId());
      }
      
      writeSightingTaxon(rw, sighting.getTaxon());
      if (sighting.getLocationId() != null) {
        rw.writeAttribute(ATTRIBUTE_LOCATION, sighting.getLocationId());
      }
      ReadablePartial dateAsPartial = sighting.getDateAsPartial();
      if (dateAsPartial != null) {
        String formattedDate = PartialIO.toString(dateAsPartial);
        rw.writeAttribute(ATTRIBUTE_DATE, formattedDate);
      }
      
      ReadablePartial timeAsPartial = sighting.getTimeAsPartial();
      if (timeAsPartial != null) {
        String formattedTime = TimeIO.toString(timeAsPartial);
        rw.writeAttribute(ATTRIBUTE_TIME, formattedTime);
      }

      if (sighting.getSightingInfo().getNumber() != null) {
        rw.writeAttribute(ATTRIBUTE_COUNT, sighting.getSightingInfo().getNumber());
      }
      if (sighting.getSightingInfo().isMale()) {
        rw.writeAttribute(ATTRIBUTE_MALE, Boolean.TRUE);
      }
      if (sighting.getSightingInfo().isFemale()) {
        rw.writeAttribute(ATTRIBUTE_FEMALE, Boolean.TRUE);
      }
      if (sighting.getSightingInfo().isImmature()) {
        rw.writeAttribute(ATTRIBUTE_IMMATURE, Boolean.TRUE);
      }
      if (sighting.getSightingInfo().isAdult()) {
        rw.writeAttribute(ATTRIBUTE_ADULT, Boolean.TRUE);
      }
      if (sighting.getSightingInfo().isHeardOnly()) {
        rw.writeAttribute(ATTRIBUTE_HEARD_ONLY, Boolean.TRUE);
      }
      if (sighting.getSightingInfo().isPhotographed()) {
        rw.writeAttribute(ATTRIBUTE_PHOTOGRAPHED, Boolean.TRUE);
      }
      if (sighting.getSightingInfo().getSightingStatus() != SightingStatus.NONE) {
        rw.writeAttribute(ATTRIBUTE_SIGHTING_STATUS,
            sighting.getSightingInfo().getSightingStatus().getId());
      }
      if (sighting.getSightingInfo().getBreedingBirdCode() != BreedingBirdCode.NONE) {
        rw.writeAttribute(ATTRIBUTE_BREEDING_CODE,
            sighting.getSightingInfo().getBreedingBirdCode().getId());
      }
      Collection<User> users = sighting.getSightingInfo().getUsers();
      if (!users.isEmpty()) {
        rw.writeAttribute(ATTRIBUTE_USERS, userIdsAsString(users));
      }

      String description = sighting.getSightingInfo().getDescription();
      if (description != null) {
        rw.writeText(description);
      }
      
      for (Photo photo : sighting.getSightingInfo().getPhotos()) {
        rw.startElement(ELEMENT_PHOTO);
        if (photo.isFavorite()) {
          rw.writeAttribute(ATTRIBUTE_FAVORITE, Boolean.TRUE);
        }
        
        rw.writeText(photo.getUri().toString());
        rw.endElement(ELEMENT_PHOTO);
      }
      rw.endElement(ELEMENT_SIGHTING);
    }
  }

  private String userIdsAsString(Collection<User> users) {
    StringBuilder builder = new StringBuilder(users.size() * 5);
    for (User user : users) {
      if (builder.length() > 0) {
        builder.append(USER_SEPARATOR);
      }
      String id = user.id();
      if (id.indexOf(USER_SEPARATOR) >= 0) {
        throw new IllegalStateException("ID of " + user + " contains a comma");
      }
      builder.append(id);
    }
    
    return builder.toString();
  }

  public void writeSightingTaxon(ResponseWriter rw, SightingTaxon sightingTaxon) throws IOException {
    switch (sightingTaxon.getType()) {
      case SINGLE:
        rw.writeAttribute(ATTRIBUTE_TAXON, sightingTaxon.getId());
        break;
      case SINGLE_WITH_SECONDARY_SUBSPECIES:
        rw.writeAttribute(ATTRIBUTE_TAXON_WITH_SSP,
            XmlReportSetExport.TAXON_WITH_SSP_JOINER.join(
                sightingTaxon.getId(), sightingTaxon.getSubIdentifier()));
        break;
      case SP:
        rw.writeAttribute(ATTRIBUTE_SP,
                XmlReportSetExport.SP_JOINER.join(sightingTaxon.getIds()));
        break;
      case HYBRID:
        rw.writeAttribute(ATTRIBUTE_HYBRID,
                XmlReportSetExport.HYBRID_JOINER.join(sightingTaxon.getIds()));
        break;
      default:
        throw new IllegalStateException("Unexpected type: " + sightingTaxon.getType());
    }
  }

  private void writeLocations(ResponseWriter rw, Collection<Location> locations)
      throws IOException {
    for (Location location : locations) {
      String element;

      if (location.getType() == null) {
        element = ELEMENT_LOCATION;
      } else {
        switch (location.getType()) {
          case city:
            element = ELEMENT_CITY;
            break;
          case town:
            element = ELEMENT_TOWN;
            break;
          case country:
            element = ELEMENT_COUNTRY;
            break;
          case county:
            element = ELEMENT_COUNTY;
            break;
          case region:
            element = ELEMENT_REGION;
            break;
          case park:
            element = ELEMENT_PARK;
            break;
          case state:
            element = ELEMENT_STATE;
            break;
          default:
            element = ELEMENT_LOCATION;
            break;
        }
      }
      rw.startElement(element);
      rw.writeAttribute(ATTRIBUTE_ID, location.getId());
      rw.writeAttribute(ATTRIBUTE_NAME, location.getModelName());
      if (!Strings.isNullOrEmpty(location.getEbirdCode())) {
        rw.writeAttribute(ATTRIBUTE_EBIRD_CODE, location.getEbirdCode());
      }
      if (location.getLatLong().isPresent()) {
        rw.writeAttribute(ATTRIBUTE_LATITUDE, location.getLatLong().get().latitude());
        rw.writeAttribute(ATTRIBUTE_LONGITUDE, location.getLatLong().get().longitude());
      }
      if (location.isPrivate()) {
        rw.writeAttribute(ATTRIBUTE_PRIVATE, Boolean.TRUE);
      }
      if (location.getDescription() != null) {
        rw.startElement(ELEMENT_DESCRIPTION);
        rw.writeText(location.getDescription());
        rw.endElement(ELEMENT_DESCRIPTION);
      }
      
      writeLocations(rw, location.contents());
      rw.endElement(element);
    }
  }
}
