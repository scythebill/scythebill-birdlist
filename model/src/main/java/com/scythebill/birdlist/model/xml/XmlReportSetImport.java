/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.xml;

import java.io.IOException;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.joda.time.DateTimeFieldType;
import org.joda.time.Duration;
import org.joda.time.LocalTime;
import org.joda.time.Partial;
import org.joda.time.ReadablePartial;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.google.common.base.CharMatcher;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.primitives.Floats;
import com.google.common.primitives.Ints;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.checklist.ClementsChecklist;
import com.scythebill.birdlist.model.checklist.ExtendedTaxonomyChecklists;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.io.TimeIO;
import com.scythebill.birdlist.model.sighting.ApproximateNumber;
import com.scythebill.birdlist.model.sighting.Area;
import com.scythebill.birdlist.model.sighting.Distance;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Location.Type;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.LocationSet.Builder;
import com.scythebill.birdlist.model.sighting.Photo;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo.BreedingBirdCode;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfo.ObservationType;
import com.scythebill.birdlist.model.sighting.VisitInfo.VisitField;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.TaxonomyMappings;
import com.scythebill.birdlist.model.taxa.UpgradeTracker;
import com.scythebill.birdlist.model.user.User;
import com.scythebill.birdlist.model.user.UserSet;
import com.scythebill.birdlist.model.util.Versions;
import com.scythebill.xml.BaseNodeParser;
import com.scythebill.xml.NodeParser;
import com.scythebill.xml.ParseContext;
import com.scythebill.xml.StringParser;
import com.scythebill.xml.TreeBuilder;

/**
 * Reads an XML-formatted report set.
 */
public class XmlReportSetImport {
  private final static ImmutableMap<String, Status> STATUS_TEXT;
  private final static Logger logger = Logger.getLogger(XmlReportSetImport.class.getName());
  
  static {
    ImmutableMap.Builder<String, Status> builder = ImmutableMap.builder();
    for (Status status : Status.values()) {
      if (status.text() != null) {
        builder.put(status.text(), status);
      }
    }
    STATUS_TEXT = builder.build();
  }
  static public final String REPORT_SET_SUFFIX = ".bsxm";
  
  /**
   * @param in a reader on the XML file
   * @param taxonomy the Taxonomy for the file
   * @return the ReportSet
   * @throws IOException
   */
  public ReportSet importReportSet(
      Reader in,
      Taxonomy taxonomy,
      Optional<MappedTaxonomy> optionalMappedTaxonomy,
      TaxonomyMappings mappings)
      throws IOException, SAXException {
    TreeBuilder<ReportSet> builder = new TreeBuilder<ReportSet>(null,
        ReportSet.class);
    return builder.parse(
        new InputSource(in),
        new ReportSetParser(taxonomy, optionalMappedTaxonomy, mappings));
  }

  static private class ReportSetParser extends BaseNodeParser {
    private final Taxonomy taxonomy;
    private final Optional<MappedTaxonomy> optionalMappedTaxonomy;
    private LocationSet locations;
    private List<Sighting> sightings;
    private final Map<Location, Checklist> checklists = Maps.newLinkedHashMap();
    private final Map<VisitInfoKey, VisitInfo> visitInfos = Maps.newLinkedHashMap();
    private final Map<String, Taxonomy> extendedTaxonomies = Maps.newLinkedHashMap();
    private final Map<String, ExtendedTaxonomyChecklists> extendedTaxonomyChecklists = Maps.newLinkedHashMap();

    /** Use a cache to simplify load-time parsing and to memoize immutable structures for reduced memory usage. */
    private final LoadingCache<String, ImmutableSet<User>> usersCache =
        CacheBuilder.newBuilder().build(new CacheLoader<String, ImmutableSet<User>>() {
          @Override
          public ImmutableSet<User> load(String usersString) throws Exception {
            return usersStringToUserSet(usersString);
          }
        });
    private final TaxonomyMappings mappings;
    private UpgradeTracker taxonomyTracker;
    private String version;
    private String prefs;
    private UserSet userSet;
    private final ImmutableSet.Builder<String> oneTimeUpgrades = ImmutableSet.builder();

    public ReportSetParser(
        Taxonomy taxonomy,
        Optional<MappedTaxonomy> optionalMappedTaxonomy,
        TaxonomyMappings mappings) {
      this.taxonomy = taxonomy;
      this.optionalMappedTaxonomy = optionalMappedTaxonomy;
      this.mappings = mappings;
    }

    @Override public void addCompletedChild(ParseContext context,
        String namespaceURI, String localName, Object child)
        throws SAXParseException {
      if (child instanceof LocationSet) {
        locations = (LocationSet) child;
      } else if (child instanceof Sighting) {
        sightings.add((Sighting) child);
      } else if (child instanceof ChecklistAndLocation) {
        ChecklistAndLocation checklistAndLocation = (ChecklistAndLocation) child;
        checklists.put(checklistAndLocation.location, checklistAndLocation.checklist);
      } else if (child instanceof VisitInfoAndKey) {
        VisitInfoAndKey visitInfoAndKey = (VisitInfoAndKey) child;
        visitInfos.put(visitInfoAndKey.key, visitInfoAndKey.visitInfo);
      } else if (XmlReportSetExport.ELEMENT_PREFS.equals(localName)) {
        this.prefs = (String) child;
      } else if (XmlReportSetExport.ELEMENT_ONE_TIME_UPGRADE.equals(localName)) {
        this.oneTimeUpgrades.add((String) child);
      } else if (child instanceof Taxonomy) {
        Taxonomy childTaxonomy = (Taxonomy) child;
        ExtendedTaxonomyChecklists parsedChecklists = ExtendedTaxonomyNodeParser.endChecklistParsing(context);
        extendedTaxonomies.put(childTaxonomy.getId(), childTaxonomy);
        extendedTaxonomyChecklists.put(childTaxonomy.getId(), parsedChecklists);
      }
    }

    @Override public Object endElement(ParseContext context,
        String namespaceURI, String localName) throws SAXParseException {
      ReportSet reportSet = new ReportSet(locations, sightings, taxonomy);
      reportSet.setPreferencesJson(prefs, false);
      reportSet.setCompletedUpgrade(
          taxonomyTracker == null ? null : taxonomyTracker.getCompletedUpgrade());
      if (version != null) {
        reportSet.setLoadedVersion(version);
      }
      // Add any extended taxonomies
      for (Taxonomy extendedTaxonomy : extendedTaxonomies.values()) {
        reportSet.addExtendedTaxonomy(extendedTaxonomy, extendedTaxonomyChecklists.get(extendedTaxonomy.getId()));
      }
      // Attach any loaded checklists
      for (Map.Entry<Location, Checklist> entry : checklists.entrySet()) {
        reportSet.setChecklist(entry.getKey(), entry.getValue());
      }
      // Attach all visit info
      for (Map.Entry<VisitInfoKey, VisitInfo> entry : visitInfos.entrySet()) {
        reportSet.putVisitInfo(entry.getKey(), entry.getValue());
      }
      if (userSet != null) {
        reportSet.setUserSet(userSet);
      }
      reportSet.setOneTimeUpgrades(oneTimeUpgrades.build());
      // Forget all the existing dirtiness...
      reportSet.clearDirty();
      return reportSet;
    }

    @Override public NodeParser startChildElement(ParseContext context,
        String namespaceURI, String localName, Attributes attrs)
        throws SAXParseException {
      if (XmlReportSetExport.ELEMENT_LOCATIONS.equals(localName)) {
        if (locations != null)
          throw new SAXParseException("Duplicate <locations> elements", context
              .getLocator());
        return new LocationSetParser();
      } else if (XmlReportSetExport.ELEMENT_SIGHTING.equals(localName)) {
        if (locations == null)
          throw new SAXParseException(
              "No <locations> element, or <sighting> element before <locations>",
              context.getLocator());
        return new SightingsParser();
      } else if (XmlReportSetExport.ELEMENT_CHECKLIST.equals(localName)) {
        return new ChecklistParser();
      } else if (XmlReportSetExport.ELEMENT_PREFS.equals(localName)) {
        return new StringParser();
      } else if (XmlReportSetExport.ELEMENT_ONE_TIME_UPGRADE.equals(localName)) {
        return new StringParser();
      } else if (XmlReportSetExport.ELEMENT_VISIT_INFO.equals(localName)) {
        return new VisitInfoParser();
      } else if (XmlReportSetExport.ELEMENT_USER.equals(localName)) {
        if (userSet == null) {
          userSet = new UserSet();
        }
        return new UserParser();
      } else if (ExtendedTaxonomyNodeParser.ELEMENT_TAXONOMY.equals(localName)) {
        ExtendedTaxonomyNodeParser.startChecklistParsing(context);
        return new ExtendedTaxonomyNodeParser();
      }

      return null;
    }

    @Override public void startElement(ParseContext context,
        String namespaceURI, String localName, Attributes attrs)
        throws SAXParseException {
      
      // Read the version of this file.  If it's newer than the current, crash-and-burn, since
      // parsing will likely fail anyway.
      String version = attrs.getValue(XmlReportSetExport.ATTRIBUTE_VERSION);
      if (version != null &&
          !Versions.versionOrdering().max(
              version, ReportSets.VERSION_FORMAT_CURRENT).equals(ReportSets.VERSION_FORMAT_CURRENT)) {
        throw new VersionTooNewException(version);
      }
      this.version = version;
      sightings = new ArrayList<Sighting>(5000);
      String taxonomyId = attrs.getValue("taxonomy");
      // TODO: remove, and make taxonomy a required attribute, once old report sets
      // minus this attribute are out of the way
      if (taxonomyId == null) {
        taxonomyId = "clements6_4";
      }
      
      if (taxonomy.getId().equals(taxonomyId)) {
        taxonomyTracker = null;
      } else {
        try {
          Preconditions.checkState(optionalMappedTaxonomy.isPresent(),
              "IOC taxonomy must be present during upgrades");
          taxonomyTracker = mappings.getTracker(taxonomyId, optionalMappedTaxonomy.get());
        } catch (IOException e) {
          throw new SAXParseException("Failed to load taxonomy updater",
              context.getLocator(),
              e);
        }
      }
    }

    enum ParseBehavior {
      WARN,
      DONT_WARN_AND_AGGRESSIVELY_RESOLVE
    }
    
    SightingTaxon parseSightingTaxon(
        ParseContext context,
        Attributes attrs,
        ParseBehavior parseBehavior,
        Location location,
        Taxonomy sightingTaxonomy)
        throws SAXParseException {
      boolean isCoreTaxonomy = sightingTaxonomy == taxonomy;
      
      String taxonId = attrs.getValue(XmlReportSetExport.ATTRIBUTE_TAXON);
      SightingTaxon sightingTaxon;
      boolean shouldWarn = parseBehavior == ParseBehavior.WARN;
      if (taxonId != null) {
        // Map the taxonomy ID through the tracker (core taxonomy only)
        if (isCoreTaxonomy && taxonomyTracker != null) {
          sightingTaxon = taxonomyTracker.getTaxonId(taxonId, shouldWarn);
          if (sightingTaxon.resolve(taxonomy) == null) {
            throw new SAXParseException("Invalid mapping " + sightingTaxon, context.getLocator());
          }
          
          // For incoming non-sp's, if the mapping turned it into a Sp, and aggressive resolution
          // is desired, use checklists to figure out which species to use
          if (parseBehavior == ParseBehavior.DONT_WARN_AND_AGGRESSIVELY_RESOLVE
              && sightingTaxon.getType() == SightingTaxon.Type.SP) {
            sightingTaxon = taxonomyTracker.getChecklistResolvedTaxonId(taxonomy, taxonId, location);
          }
        } else {
          Taxon taxon = sightingTaxonomy.getTaxon(taxonId);
          // FIXME: be less brutal when there is a failure to find a taxon!!!
          // Doing this nicely will likely require including a taxonId ->
          // scientific
          // name, or some other such syntax. Reducing the odds will likely
          // require never really "deleting" species - always moving them
          // under another taxon, marking as hybrid, or invalid, etc.
          
          // HACK: 10.2.0 changed the ID of ssp. waltoni of Pink-rumped Rosefinch
          // without a mapping rule!  Catch data in this state.
          if (taxon == null && "sspCarpo3davwal".equals(taxonId)) {
            taxonId = "sspCarpo3eoswal";
            taxon = taxonomy.getTaxon(taxonId);
          }
          
          if (taxon == null) {
            throw new SAXParseException("Could not find taxon id " + taxonId,
                context.getLocator());
          }
          sightingTaxon = SightingTaxons.newSightingTaxon(taxonId);
        }
      } else {
        String uncertain = attrs.getValue(XmlReportSetExport.ATTRIBUTE_SP);
        if (uncertain == null) {
          String hybrid = attrs.getValue(XmlReportSetExport.ATTRIBUTE_HYBRID);
          if (hybrid == null) {
            String withSsp = attrs.getValue(XmlReportSetExport.ATTRIBUTE_TAXON_WITH_SSP);
            if (withSsp == null) {
              throw new SAXParseException(
                  "None of taxon, sp, hybrid, taxonWithSsp attributes found",
                  context.getLocator());
            }
            ImmutableList<String> taxa = ImmutableList.copyOf(
                    XmlReportSetExport.TAXON_WITH_SSP_SPLITTER.split(withSsp));
            if (taxa.size() != 2) {
              throw new SAXParseException("Invalid taxonWithSsp attribute: " + withSsp,
                  context.getLocator());
            }
            
            sightingTaxon = SightingTaxons.newSightingTaxonWithSecondarySubspecies(
                taxa.get(0), taxa.get(1));

            // Taxonomy tracker:  send the "ssp-base-taxon", through the mapper.
            if (isCoreTaxonomy && taxonomyTracker != null) {
              sightingTaxon = taxonomyTracker.getTaxonIdOfSightingWithSsp(sightingTaxon, shouldWarn);
            }
          } else {
            // Hybrids
            Iterable<String> taxa = XmlReportSetExport.HYBRID_SPLITTER.split(hybrid);
            // If mapping is needed, send all through the tracker.  If the end result is one species
            // (e.g., a lump), then it's back to a single species.  Otherwise, it's a hybrid of the mapped taxa.
            if (isCoreTaxonomy && taxonomyTracker != null) {
              Iterable<String> mappedTaxa = taxonomyTracker.getTaxonId(taxonomy, taxa, shouldWarn);
              if (Iterables.size(mappedTaxa) == 1) {
                sightingTaxon = SightingTaxons.newSightingTaxon(Iterables.getOnlyElement(mappedTaxa));
              } else {
                sightingTaxon = SightingTaxons.newHybridTaxon(mappedTaxa);
              }
            } else {
              // No mapping, just produce the hybrid
              sightingTaxon = SightingTaxons.newHybridTaxon(taxa);
            }
          }
        } else {
          Iterable<String> taxa = XmlReportSetExport.SP_SPLITTER.split(uncertain);
          // If mapping is needed, send all through the tracker.  If the end result is one species
          // (e.g., a lump), then it's back to a single species.  Otherwise, it's a hybrid of the mapped taxa.
          if (isCoreTaxonomy  && taxonomyTracker != null) {
            Iterable<String> mappedTaxa = taxonomyTracker.getTaxonId(taxonomy, taxa, shouldWarn);
            if (Iterables.size(mappedTaxa) == 1) {
              sightingTaxon = SightingTaxons.newSightingTaxon(Iterables.getOnlyElement(mappedTaxa));
            } else {
              sightingTaxon = SightingTaxons.newSpTaxon(mappedTaxa);
            }
          } else {
            // No mapping, just produce the sp.
            sightingTaxon = SightingTaxons.newSpTaxon(taxa);
          }
        }
      }
      
      return sightingTaxon;
    }

    Taxonomy findSightingTaxonomy(ParseContext context, Attributes attrs)
        throws SAXParseException {
      String taxonomyId = attrs.getValue(XmlReportSetExport.ATTRIBUTE_TAXONOMY_ID);
      Taxonomy sightingTaxonomy;
      if (taxonomyId == null) {
        sightingTaxonomy = taxonomy;
      } else {
        sightingTaxonomy = extendedTaxonomies.get(taxonomyId);
        if (sightingTaxonomy == null) {
          throw new SAXParseException("Invalid taxonomyId " + taxonomyId, context.getLocator());
        }
      }
      return sightingTaxonomy;
    }
    
    UserSet getUserSet() {
      if (userSet == null) {
        userSet = new UserSet();
      }
      
      return userSet;
    }

    private ImmutableSet<User> usersStringToUserSet(String usersString) {
      ImmutableSet.Builder<User> users  = ImmutableSet.builder();
      for (String userId : XmlReportSetExport.USER_SPLITTER.split(usersString)) {
        User user;
        try {
          user = getUserSet().userById(userId);
        } catch (IllegalArgumentException e) {
          user = getUserSet()
              .addUser(getUserSet().newUserBuilderFromStorage(userId).setAbbreviation(userId));
        }
        users.add(user);
      }
      return users.build();
    }

    public class PhotosParser extends BaseNodeParser {

      private boolean favorite;
      private StringBuilder buffer = new StringBuilder();

      @Override
      public void startElement(ParseContext context, String namespaceURI, String localName,
          Attributes attrs) throws SAXParseException {
        this.favorite = "true".equals(attrs.getValue(XmlReportSetExport.ATTRIBUTE_FAVORITE));
      }

      @Override
      public Object endElement(ParseContext context, String namespaceURI, String localName)
          throws SAXParseException {
        URI uri;
        try {
          uri = new URI(buffer.toString());
        } catch (URISyntaxException e) {
          logger.log(Level.SEVERE, "Could not parse photo " + buffer, e);
          return null;
        }
        
        return new Photo(uri, favorite);
      }

      @Override
      public void addText(ParseContext context, char[] text, int start, int length)
          throws SAXParseException {
        buffer.append(text, start, length);
      }
      
    }

    public class SightingsParser extends BaseNodeParser {
      private Sighting sighting;
      private StringBuilder description;
      private List<Photo> photos;

      @Override public void addText(ParseContext context, char[] text,
          int start, int length) throws SAXParseException {
        if (description == null) {
          description = new StringBuilder();
        }
        description.append(text, start, length);
      }

      @Override public Object endElement(ParseContext context,
          String namespaceURI, String localName) throws SAXParseException {
        if (description != null) {
          // Clean up the fallout from Issue #428, which appended whitespace to
          // the end of sighting descriptions that had photos on each save!
          sighting.getSightingInfo().setDescription(
              CharMatcher.whitespace().trimTrailingFrom(description.toString()));
        }
        if (photos != null) {
          sighting.getSightingInfo().setPhotos(photos);
        }
        return sighting;
      }

      @Override public void startElement(ParseContext context,
          String namespaceURI, String localName, Attributes attrs)
          throws SAXParseException {
        String locationId = attrs.getValue(XmlReportSetExport.ATTRIBUTE_LOCATION);        
        Location location;
        
        if (locationId == null) {
          location = null;
        } else {
          location = locations.getLocation(locationId);
          
          // UGH.  The location wasn't found.
          if (location == null) {
            // Create a new location, and store it as a root location.
            location = Location.builder()
                .setName("RESTORED LOCATION \"" + locationId + "\"")
                .build();
            locations.addRestoredLocation(locationId, location);
          }
        }

        Taxonomy sightingTaxonomy = findSightingTaxonomy(context, attrs);
        SightingTaxon sightingTaxon = parseSightingTaxon(
            context, attrs, ParseBehavior.WARN, location, sightingTaxonomy);

        Sighting.Builder sighting = Sighting.newBuilder()
            .setTaxonomy(sightingTaxonomy)
            .setTaxon(sightingTaxon);
        if (location != null) {
          sighting.setLocation(location);
        }

        String dateStr = attrs.getValue(XmlReportSetExport.ATTRIBUTE_DATE);
        if (dateStr != null) {
          try {
            ReadablePartial datePartial = dateFromStringWithCenturyFix(dateStr);
            sighting.setDate(datePartial);
          } catch (IllegalArgumentException iae) {
            logError(context, "Could not parse date " + dateStr, iae);
          }
        }

        String timeStr = attrs.getValue(XmlReportSetExport.ATTRIBUTE_TIME);
        if (timeStr != null) {
          try {
            LocalTime timePartial = TimeIO.fromString(timeStr);
            sighting.setTime(timePartial);
          } catch (IllegalArgumentException iae) {
            logError(context, "Could not parse time " + timeStr, iae);
          }
        }

        String countStr = attrs.getValue(XmlReportSetExport.ATTRIBUTE_COUNT);
        if (countStr != null) {
          ApproximateNumber number = ApproximateNumber.tryParse(countStr);
          if (number != null) {
            sighting.getSightingInfo().setNumber(number);
          }
        }

        if ("true".equals(attrs.getValue(XmlReportSetExport.ATTRIBUTE_MALE))) {
          sighting.getSightingInfo().setMale(true);
        }
        if ("true".equals(attrs.getValue(XmlReportSetExport.ATTRIBUTE_FEMALE))) {
          sighting.getSightingInfo().setFemale(true);
        }
        if ("true"
            .equals(attrs.getValue(XmlReportSetExport.ATTRIBUTE_IMMATURE))) {
          sighting.getSightingInfo().setImmature(true);
        }
        if ("true"
            .equals(attrs.getValue(XmlReportSetExport.ATTRIBUTE_ADULT))) {
          sighting.getSightingInfo().setAdult(true);
        }
        if ("true"
            .equals(attrs.getValue(XmlReportSetExport.ATTRIBUTE_PHOTOGRAPHED))) {
          sighting.getSightingInfo().setPhotographed(true);
        }
        if ("true".equals(attrs
            .getValue(XmlReportSetExport.ATTRIBUTE_HEARD_ONLY))) {
          sighting.getSightingInfo().setHeardOnly(true);
        }
        
        String sightingStatusId = attrs.getValue(XmlReportSetExport.ATTRIBUTE_SIGHTING_STATUS);
        if (sightingStatusId != null) {
          SightingStatus sightingStatus = SightingStatus.forId(sightingStatusId);
          if (sightingStatus == null) {
            logWarning(context, "Invalid sighting status: " + sightingStatusId);
          } else {
            sighting.getSightingInfo().setSightingStatus(sightingStatus);
          }
        }

        String breedingId = attrs.getValue(XmlReportSetExport.ATTRIBUTE_BREEDING_CODE);
        if (breedingId != null) {
          BreedingBirdCode breedingCode = BreedingBirdCode.forId(breedingId);
          if (breedingCode == null) {
            logWarning(context, "Invalid breeding code: " + breedingId);
          } else {
            sighting.getSightingInfo().setBreedingBirdCode(breedingCode);
          }
        }
        
        String users = attrs.getValue(XmlReportSetExport.ATTRIBUTE_USERS);
        if (users != null) {
          // Run through a cache so that we produce the same object for the same series of users,
          // which should significantly reduce memory usage.
          ImmutableSet<User> usersSet = usersCache.getUnchecked(users);
          sighting.getSightingInfo().setUsers(usersSet);
        }
        this.sighting = sighting.build();
      }

      @Override
      public NodeParser startChildElement(ParseContext context, String namespaceURI,
          String localName, Attributes attrs) throws SAXParseException {
        if (XmlReportSetExport.ELEMENT_PHOTO.equals(localName)) {
          return new PhotosParser();
        }
        
        return super.startChildElement(context, namespaceURI, localName, attrs);
      }

      @Override
      public void addCompletedChild(ParseContext context, String namespaceURI, String localName,
          Object child) throws SAXParseException {
        if (child instanceof Photo) {
          if (photos == null) {
            photos = Lists.newArrayListWithCapacity(2);
          }
          photos.add((Photo) child);
        }
      }
    }

    static class ChecklistAndLocation {
      public final Checklist checklist;
      public final Location location;

      public ChecklistAndLocation(Checklist checklist, Location location) {
        this.checklist = Preconditions.checkNotNull(checklist);
        this.location = Preconditions.checkNotNull(location);
      }
      
    }
    
    class ChecklistParser extends BaseNodeParser {
      private final Map<SightingTaxon, Checklist.Status> taxa = Maps.newLinkedHashMap();
      private Location location;
      
      @Override
      public void startElement(ParseContext context, String namespaceURI, String localName,
          Attributes attrs) throws SAXParseException {
        String locId = getRequiredAttribute(context, attrs, XmlReportSetExport.ATTRIBUTE_LOCATION);
        location = locations.getLocation(locId);
        if (location == null) {
          // See https://code.google.com/p/scythebill-birdlist/issues/detail?id=135 ; throwing an exception
          // *feels* better, but not breaking loading of a report is better.
          logger.warning("Unknown location " + locId + ";  can't parse checklist");
        }
      }
      
      @Override
      public Object endElement(ParseContext context, String namespaceURI, String localName)
          throws SAXParseException {
        // No location, no checklist... see above
        if (location == null) {
          return null;
        }
        
        ClementsChecklist clementsChecklist = new ClementsChecklist(taxa);
        return new ChecklistAndLocation(clementsChecklist, location);
      }
      
      @Override
      public NodeParser startChildElement(ParseContext context, String namespaceURI,
          String localName, Attributes attrs) throws SAXParseException {
        if (XmlReportSetExport.ELEMENT_CHECKLIST_ENTRY.equals(localName)) {
          Taxonomy sightingTaxonomy = findSightingTaxonomy(context, attrs);
          SightingTaxon sightingTaxon = parseSightingTaxon(
              context, attrs, ParseBehavior.DONT_WARN_AND_AGGRESSIVELY_RESOLVE, location, sightingTaxonomy);
          Checklist.Status status = Checklist.Status.NATIVE;
          String statusText = attrs.getValue(XmlReportSetExport.ATTRIBUTE_CHECKLIST_STATUS);
          if (statusText != null
              && STATUS_TEXT.containsKey(statusText)) {
            status = STATUS_TEXT.get(statusText);
          }
          // TODO: Do something with a non-default taxonomy! 
          taxa.put(sightingTaxon, status);
          
          return BaseNodeParser.getIgnoreParser();
        }
        
        return super.startChildElement(context, namespaceURI, localName, attrs);
      }
    }

    static class VisitInfoAndKey {

      final VisitInfo visitInfo;
      final VisitInfoKey key;

      public VisitInfoAndKey(VisitInfo visitInfo, VisitInfoKey key) {
        this.visitInfo = visitInfo;
        this.key = key;
      }

    }

    class VisitInfoParser extends BaseNodeParser {
      private VisitInfoKey key;
      private VisitInfo.Builder visitInfo;
      
      @Override
      public void startElement(ParseContext context, String namespaceURI, String localName,
          Attributes attrs) throws SAXParseException {
        
        String locId = getRequiredAttribute(context, attrs, XmlReportSetExport.ATTRIBUTE_LOCATION);
        Location location = locations.getLocation(locId);
        if (location == null) {
          // See https://code.google.com/p/scythebill-birdlist/issues/detail?id=135 ; throwing an exception
          // *feels* better, but not breaking loading of a report is better.
          logger.warning("Unknown location " + locId + ";  can't parse checklist");
        }
        
        String dateStr = getRequiredAttribute(context, attrs, XmlReportSetExport.ATTRIBUTE_DATE);
        ReadablePartial date = dateFromStringWithCenturyFix(dateStr);
        String timeStr = attrs.getValue(XmlReportSetExport.ATTRIBUTE_TIME);
        LocalTime time = Strings.isNullOrEmpty(timeStr)
            ? null
            : TimeIO.fromString(timeStr);
        key = new VisitInfoKey(locId, date, time);
        
        visitInfo = VisitInfo.builder();
        String observationTypeStr = getRequiredAttribute(context, attrs, XmlReportSetExport.ATTRIBUTE_OBSERVATION_TYPE); 
        ObservationType observationType = ObservationType.fromId(observationTypeStr);
        if (observationType == null) {
          logger.warning("Unknown observationType " + observationTypeStr);
          // Just use HISTORICAL, which supports all fields
          observationType = ObservationType.HISTORICAL;
        }
        // Not exactly sure how data gets into this state - there's clearly a bug! - but snap
        // back to HISTORICAL if the observation type needs start time and doesn't have it.
        if (!key.startTime().isPresent()
            && observationType.getRequiredFields().contains(VisitField.START_TIME)) {
          observationType = ObservationType.HISTORICAL;
        }
        visitInfo.withObservationType(observationType);
      }
      
      @Override
      public Object endElement(ParseContext context, String namespaceURI, String localName)
          throws SAXParseException {
        // No location, no visit info... see above
        if (visitInfo == null) {
          return null;
        }
        
        VisitInfo built;
        try {
          built = visitInfo.build();
        } catch (IllegalStateException e) {
          // OK, so the visit couldn't be built as it's somehow invalid.  Switch to historical.
          logger.log(Level.WARNING, "Couldn't build visit info; using historical", e);
          visitInfo.withObservationType(ObservationType.HISTORICAL);
          built = visitInfo.build();
        }
        
        return new VisitInfoAndKey(built, key);
      }
      
      @Override
      public NodeParser startChildElement(ParseContext context, String namespaceURI,
          String localName, Attributes attrs) throws SAXParseException {
        if (visitInfo == null) {
          return BaseNodeParser.getIgnoreParser();
        }
        
        if (XmlReportSetExport.ELEMENT_AREA.equals(localName)) {
          return new StringParser();
        } else if (XmlReportSetExport.ELEMENT_DISTANCE.equals(localName)) {
          return new StringParser();
        } else if (XmlReportSetExport.ELEMENT_DURATION.equals(localName)) {
          return new StringParser();
        } else if (XmlReportSetExport.ELEMENT_PARTY_SIZE.equals(localName)) {
          return new StringParser();
        } else if (XmlReportSetExport.ELEMENT_COMMENT.equals(localName)) {
          return new StringParser();
        } else if (XmlReportSetExport.ELEMENT_COMPLETE.equals(localName)) {
          visitInfo.withCompleteChecklist(true);
          return BaseNodeParser.getIgnoreParser();
        }
        
        return super.startChildElement(context, namespaceURI, localName, attrs);
      }

      @Override
      public void addCompletedChild(ParseContext context, String namespaceURI, String localName,
          Object child) throws SAXParseException {
        if (visitInfo == null) {
          return;
        }
        
        String string = (String) child;
        if (string != null) {
          string = CharMatcher.whitespace().trimFrom(string);
        }
        if (XmlReportSetExport.ELEMENT_AREA.equals(localName)) {
          Float parsed = Floats.tryParse(string);
          if (parsed != null
              && parsed > 0.0f) {
            visitInfo.withArea(Area.inHectares(parsed));
          }
        } else if (XmlReportSetExport.ELEMENT_DISTANCE.equals(localName)) {
          Float parsed = Floats.tryParse(string);
          if (parsed != null
              && parsed > 0.0f) {
            visitInfo.withDistance(Distance.inKilometers(parsed));
          }
        } else if (XmlReportSetExport.ELEMENT_DURATION.equals(localName)) {
          Integer parsed = Ints.tryParse(string);
          if (parsed != null
              && parsed > 0) {
            visitInfo.withDuration(Duration.standardMinutes(parsed));
          }
        } else if (XmlReportSetExport.ELEMENT_PARTY_SIZE.equals(localName)) {
          Integer parsed = Ints.tryParse(string);
          if (parsed != null
              && parsed > 0) {
            visitInfo.withPartySize(parsed);
          }
        } else if (XmlReportSetExport.ELEMENT_COMMENT.equals(localName)) {
          if (!string.isEmpty()) {
            visitInfo.withComments(string);
          }
        }
      }
    }

    class UserParser extends BaseNodeParser {
      private User.Builder builder;
      
      @Override
      public void startElement(ParseContext context, String namespaceURI, String localName,
          Attributes attrs) throws SAXParseException {
        String id = getRequiredAttribute(context, attrs, XmlReportSetExport.ATTRIBUTE_ID);
        builder = getUserSet().newUserBuilderFromStorage(id);
        
        String name = attrs.getValue(XmlReportSetExport.ATTRIBUTE_NAME);
        if (name != null) {
          builder.setName(name);
        }
        String abbreviation = attrs.getValue(XmlReportSetExport.ATTRIBUTE_ABBREVIATION);
        if (abbreviation != null) {
          builder.setAbbreviation(abbreviation);
        }
      }

      @Override
      public Object endElement(ParseContext context, String namespaceURI, String localName)
          throws SAXParseException {
        getUserSet().addUser(builder);
        return null;
      }
      
    }
  }

  /**
   * Parser for Locations objects.
   */
  static public class LocationSetParser extends BaseNodeParser {
    private LocationSet.Builder locationsBuilder;
    
    @Override public Object endElement(ParseContext context,
        String namespaceURI, String localName) throws SAXParseException {
      return locationsBuilder.build();
    }

    @Override public void startElement(ParseContext context,
        String namespaceURI, String localName, Attributes attrs)
        throws SAXParseException {
      locationsBuilder = new LocationSet.Builder();
    }

    @Override public NodeParser startChildElement(ParseContext context,
        String namespaceURI, String localName, Attributes attrs)
        throws SAXParseException {
      return getLocationParser(localName, null, locationsBuilder);
    }
  }

  /**
   * Parser for Location elements.
   */
  static public class LocationParser extends BaseNodeParser {
    private Location location;
    private final Type type;
    private final Location parent;
    private final LocationSet.Builder locations;

    public LocationParser(Type type, Location parent,
        LocationSet.Builder locationsBuilder) {
      this.type = type;
      this.parent = parent;
      locations = locationsBuilder;
    }

    @Override public NodeParser startChildElement(ParseContext context,
        String namespaceURI, String localName, Attributes attrs)
        throws SAXParseException {
      return getLocationParser(localName, location, locations);
    }

    @Override public void startElement(ParseContext context,
        String namespaceURI, String localName, Attributes attrs)
        throws SAXParseException {
      String id = getRequiredAttribute(context, attrs,
          XmlReportSetExport.ATTRIBUTE_ID);
      String name = getRequiredAttribute(context, attrs,
          XmlReportSetExport.ATTRIBUTE_NAME);
      if (name.isEmpty()) {
        name = "(Unnamed)";
      }
      
      // See if something has gone wrong and the saved location name is
      // a duplicate. The only time this happened before was when tabs
      // were written un-escaped, such that when re-read they turned into spaces.
      // That said, this is a Bad Bug, since it makes the file unreadable, so hack
      // to read the file.
      if (parent != null) {
        Location previousChild = parent.getContent(name);
        if (previousChild != null) {
          for (int i = 0; i < 10; i++) {
            String duplicateName = duplicateName(name, i);
            if (parent.getContent(duplicateName) == null) {
              name = duplicateName;
              break;
            }
          }
        }
      }
      
      Location.Builder locationBuilder = Location.builder()
          .setName(name)
          .setType(type)
          .setParent(parent);
      String ebirdCode = attrs.getValue(XmlReportSetExport.ATTRIBUTE_EBIRD_CODE);
      if (ebirdCode != null) {
        locationBuilder.setEbirdCode(ebirdCode);
      }
      
      String latitude = attrs.getValue(XmlReportSetExport.ATTRIBUTE_LATITUDE);
      String longitude = attrs.getValue(XmlReportSetExport.ATTRIBUTE_LONGITUDE);
      if (latitude != null && longitude != null) {
        try {
          LatLongCoordinates latLong = LatLongCoordinates.withLatAndLong(latitude, longitude);
          locationBuilder.setLatLong(latLong);
        } catch (IllegalArgumentException e) {
          // Failed parse, move on but log
          logger.warning(String.format("Invalid lat/long: %s, %s", latitude, longitude));
        }
      }
      if ("true".equals(attrs.getValue(XmlReportSetExport.ATTRIBUTE_PRIVATE))) {
        locationBuilder.setPrivate(true);
      }
      
      location = locationBuilder.build();
      locations.addLocation(location, id);
    }
    
    private String duplicateName(String name, int i) {
      if (!name.endsWith(" duplicate")) {
        name = name + " duplicate";
      }
      
      if (i == 0) {
        return name;
      }
      
      return name + " " + i;
    }

    
    @Override
    public void addCompletedChild(ParseContext context, String namespaceURI, String localName,
        Object child) throws SAXParseException {
      if (child instanceof String) {
        Location locationWithDescription = location.asBuilder().setDescription((String) child).build();
        locations.replace(location, locationWithDescription);
        location = locationWithDescription;
      }
    }

    @Override public Object endElement(ParseContext context,
        String namespaceURI, String localName) throws SAXParseException {
      // Not actually used, but the right thing to do
      return location;
    }
  }

  /**
   * Create the right NodeParser for a location.
   */
  static private NodeParser getLocationParser(String localName,
      Location parent, Builder builder) {
    // Description within a location
    if (localName.equals(XmlReportSetExport.ELEMENT_DESCRIPTION)) {
      return new StringParser();
    }
    
    Location.Type type;
    if (localName.equals(XmlReportSetExport.ELEMENT_COUNTRY)) {
      type = Location.Type.country;
    } else if (localName.equals(XmlReportSetExport.ELEMENT_TOWN)) {
      type = Location.Type.town;
    } else if (localName.equals(XmlReportSetExport.ELEMENT_PARK)) {
      type = Location.Type.park;
    } else if (localName.equals(XmlReportSetExport.ELEMENT_CITY)) {
      type = Location.Type.city;
    } else if (localName.equals(XmlReportSetExport.ELEMENT_COUNTY)) {
      type = Location.Type.county;
    } else if (localName.equals(XmlReportSetExport.ELEMENT_STATE)) {
      type = Location.Type.state;
    } else if (localName.equals(XmlReportSetExport.ELEMENT_REGION)) {
      type = Location.Type.region;
    } else if (localName.equals(XmlReportSetExport.ELEMENT_LOCATION)) { 
      type = null;
    } else {
      // unknown name, bail
      return null;
    }
    
    return new LocationParser(type, parent, builder);
  }

  /**
   * Convert a string to a ReadablePartial, fixing up years for a Scythebill-12-and-earlier
   * bug in some eBird imports.
   */
  private static ReadablePartial dateFromStringWithCenturyFix(String dateStr) {
    ReadablePartial datePartial = PartialIO.fromString(dateStr);
    if (datePartial.isSupported(DateTimeFieldType.year())) {
      int year = datePartial.get(DateTimeFieldType.year());
      // Some types of Scythebill eBird imports would mistakenly import the year
      // "14" as "14", not "2014"!  Fix up any ancient records when reading in.
      if (year < 100) {
        if (year < 50) {
          year += 2000;
        } else {
          year += 1900;
        }
      }
      datePartial = new Partial(datePartial).with(DateTimeFieldType.year(), year);
    }
    return datePartial;
  }
}
