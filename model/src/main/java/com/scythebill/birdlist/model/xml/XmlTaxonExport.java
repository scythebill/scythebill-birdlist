/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.xml;

import java.io.IOException;
import java.io.Writer;
import java.util.Set;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.TreeMultimap;
import com.scythebill.birdlist.model.checklist.ExtendedTaxonomyChecklists;
import com.scythebill.birdlist.model.io.IndentingResponseWriter;
import com.scythebill.birdlist.model.io.ResponseWriter;
import com.scythebill.birdlist.model.io.XmlResponseWriter;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonExport;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * The XML file format:
 * TODO: fix the below, as it's changed since the docs were written
 * <taxonomy>
 *   <class>
 *     <order>
 *       <family>
 *         <subfamily>?
 *           <genus>
 *             <superspecies>?
 *               <species>
 *                 <subspecies-group>?
 *                   <subspecies/>
 *   <hybrid/>
 * </taxonomy>        
 *         
 * Each parent other than <taxonomy>, extends taxon        
 *   
 * <taxon name="..." id="...">
 *   <common-name lang="en-US"?/>+
 *   <alternate-name/>*
 *   <alternate-common-name/>*
 *   <metadata>*
 *   
 *   </metadata>
 * </taxon>
 */
public class XmlTaxonExport implements TaxonExport {
  static final String ELEMENT_TAXONOMY = "taxonomy";
  static public final String NAMESPACE = "http://www.adamwiner.com/checklist/1.0";
  static final String SINGLE_MAPPING = "map";
  static final String SP_MAPPING = "spmap";
  static final String SSP_MAPPING = "sspmap";
  static final String NO_MAPPING = "nomap";
  static final String ELEMENT_CHECKLISTS = "checklists";
  private ExtendedTaxonomyChecklists checklists;
  private TreeMultimap<String, String> transposedChecklists;
  
  public XmlTaxonExport withChecklists(ExtendedTaxonomyChecklists checklists) {
    this.checklists = checklists;
    return this;
  }
  
  @Override
  // close() responsibility is on the caller;  this class flushes its buffers.
  @SuppressWarnings("resource")
  public void export(Writer out, Taxonomy taxonomy, String encoding)
      throws IOException {
    ResponseWriter rw = new XmlResponseWriter(out, encoding);
    rw = new IndentingResponseWriter(rw);
    rw.startDocument();
    writeTaxonomy(taxonomy, rw);
    rw.endDocument();
    rw.flush();
  }

  public void writeTaxonomy(Taxonomy taxonomy, ResponseWriter rw) throws IOException {
    if (checklists != null) {
      transposedChecklists = ExtendedTaxonomyParsing
          .buildTransposedChecklistWithStatuses(checklists.getChecklists(), taxonomy);
    }
      
    rw.startElement(ELEMENT_TAXONOMY);
    rw.writeAttribute("xmlns", NAMESPACE);
    if (taxonomy.getId() != null) {
      rw.writeAttribute("id", taxonomy.getId());
    }
    if (taxonomy.getName() != null) {
      rw.writeAttribute("name", taxonomy.getName());
    }
    // THe format isn't directly exposed, but we can get close enough with a hardcoded ID
    String dummyAccountUrl = taxonomy.getTaxonAccountUrl("d33db33f");
    if (dummyAccountUrl != null) {
      rw.writeAttribute("accountUrlFormat", dummyAccountUrl.replace("d33db33f", "{id}"));
    }
    String accountLinkTitle = taxonomy.getAccountLinkTitle();
    if (accountLinkTitle != null) {
      rw.writeAttribute("accountLinkTitle", accountLinkTitle);
    }
    for (String credit : taxonomy.additionalCredits()) {
      rw.startElement("additional-credit");
      rw.writeText(credit);
      rw.endElement("additional-credit");
    }
    
    writeTaxon(rw, taxonomy.getRoot());
    rw.endElement(ELEMENT_TAXONOMY);
  }

  private void writeTaxon(ResponseWriter rw, Taxon taxon) throws IOException {
    String name = taxon.getType().getName();
    rw.startElement(name, taxon);

    rw.writeAttribute("name", taxon.getName());
    rw.writeAttribute("id", taxon.getId());
    if (taxon.getAccountId() != null) {
      rw.writeAttribute("accountId", taxon.getAccountId());
    }
    if (taxon.getCommonName() != null) {
      rw.startElement("common-name");
      rw.writeText(taxon.getCommonName());
      rw.endElement("common-name");
    }

    if (taxon instanceof Species) {
      Species sp = (Species) taxon;
      if (hasMetadata(sp)) {
        rw.startElement("metadata");
        writeSpeciesMetadata(rw, sp);
        rw.endElement("metadata");
      }

      if (transposedChecklists != null) {
        Set<String> locationCodes = transposedChecklists.get(taxon.getId());
        if (!locationCodes.isEmpty()) {
          rw.startElement(ELEMENT_CHECKLISTS);
          rw.writeText(Joiner.on(',').join(locationCodes));
          rw.endElement(ELEMENT_CHECKLISTS);
        }
      }
      
      if (taxon.getTaxonomy() instanceof MappedTaxonomy) {
        MappedTaxonomy mapped = (MappedTaxonomy) taxon.getTaxonomy();
        SightingTaxon mapping = mapped.getExactMapping(taxon);
        if (mapping != null) {
          switch (mapping.getType()) {
            case SINGLE:
              rw.startElement(SINGLE_MAPPING);
              rw.writeText(mapping.getId());
              rw.endElement(SINGLE_MAPPING);
              break;
            case SINGLE_WITH_SECONDARY_SUBSPECIES:
              rw.startElement(SINGLE_MAPPING);
              rw.writeText(mapping.getId());
              rw.endElement(SINGLE_MAPPING);
              rw.startElement(SSP_MAPPING);
              rw.writeText(mapping.getSubIdentifier());
              rw.endElement(SSP_MAPPING);
              break;
            case SP:
              rw.startElement(SP_MAPPING);
              rw.writeText(Joiner.on(',').join(mapping.getIds()));
              rw.endElement(SP_MAPPING);
              break;
            default:
              throw new IllegalStateException("Unexpected mapping: " + mapping);
          }
        } else {
          rw.startElement(NO_MAPPING);
          rw.endElement(NO_MAPPING);
        }
      }

    }
    
    for (Taxon child : taxon.getContents()) {
      writeTaxon(rw, child);
    }
    rw.endElement(name);
  }

  private boolean hasMetadata(Species sp) {
    if (sp.getStatus() != Species.Status.LC
        || sp.getRange() != null
        || sp.getTaxonomicInfo() != null
        || !sp.getAlternateNames().isEmpty()
        || !sp.getAlternateCommonNames().isEmpty()
        || sp.getMiscellaneousInfo() != null) {
      return true;
    }
    
    return false;
  }
  
  private void writeSpeciesMetadata(ResponseWriter rw, Species sp)
      throws IOException {
    if (sp.getStatus() != Species.Status.LC) {
      rw.startElement("status");
      rw.writeText(sp.getStatus().toString());
      rw.endElement("status");
    }

    if (!Strings.isNullOrEmpty(sp.getRange())) {
      rw.startElement("range");
      rw.writeText(sp.getRange());
      rw.endElement("range");
    }

    if (!Strings.isNullOrEmpty(sp.getTaxonomicInfo())) {
      rw.startElement(ELEMENT_TAXONOMY);
      rw.writeText(sp.getTaxonomicInfo());
      rw.endElement(ELEMENT_TAXONOMY);
    }

    for (String alternateName : sp.getAlternateNames()) {
      rw.startElement("alternate-names");
      rw.writeText(alternateName);
      rw.endElement("alternate-names");
    }

    for (String alternateCommonName : sp.getAlternateCommonNames()) {
      rw.startElement("alternate-common-names");
      rw.writeText(alternateCommonName);
      rw.endElement("alternate-common-names");
    }

    if (!Strings.isNullOrEmpty(sp.getMiscellaneousInfo())) {
      rw.startElement("miscellaneous");
      rw.writeText(sp.getMiscellaneousInfo());
      rw.endElement("miscellaneous");
    }
  }
}
