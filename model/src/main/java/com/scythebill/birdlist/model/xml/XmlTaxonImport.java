/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.xml;

import java.io.IOException;
import java.io.Reader;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy.MappingBuilder;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.SpeciesImpl;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonImpl;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;
import com.scythebill.xml.BaseNodeParser;
import com.scythebill.xml.NodeParser;
import com.scythebill.xml.ParseContext;
import com.scythebill.xml.ParseContextImpl;
import com.scythebill.xml.ParserFactory;
import com.scythebill.xml.ParserManager;
import com.scythebill.xml.StringParser;
import com.scythebill.xml.TreeBuilder;

/**
 * Reads in a taxonomy from an XML-formatted file.
 */
public class XmlTaxonImport {
  static private final ParserManager PARSER_MANAGER = createParserManager();
  static private final Map<String, Taxon.Type> TAXON_TYPES_BY_NAME =
      ImmutableMap.<String, Taxon.Type>builder()
      .put("class", Taxon.Type.classTaxon)
      .put("family", Taxon.Type.family)
      .put("genus", Taxon.Type.genus)
      .put("order", Taxon.Type.order)
      .put("species", Taxon.Type.species)
      .put("subspecies", Taxon.Type.subspecies)
      .put("group", Taxon.Type.group)
      .put("phylum", Taxon.Type.phylum)
      .build();
  static private final Object MAPPED_TAXONOMY_MAPPINGS_KEY = new Object();

  public Taxonomy importTaxa(Reader in) throws IOException, SAXException {
    TreeBuilder<Taxonomy> builder = new TreeBuilder<Taxonomy>(PARSER_MANAGER,
        Taxonomy.class);
    return builder.parse(new InputSource(in));
  }

  public MappedTaxonomy importMappedTaxa(Reader in, Taxonomy base) throws IOException, SAXException {
    TreeBuilder<MappedTaxonomy> builder = new TreeBuilder<MappedTaxonomy>(PARSER_MANAGER,
        MappedTaxonomy.class);
    ParseContextImpl parseContextImpl = new ParseContextImpl();
    // Store off the base Taxonomy so it can be stored properly when the MappedTaxonomy is created
    parseContextImpl.setProperty(Taxonomy.class, base);
    // Store off a map to map taxa to their mappings (the way the Taxonomy is built up,
    // these can't be registered when the SightingTaxon is identified).
    IdentityHashMap<Taxon, SightingTaxon> mappings = Maps.newIdentityHashMap();
    parseContextImpl.setProperty(MAPPED_TAXONOMY_MAPPINGS_KEY, mappings);
    MappedTaxonomy mappedTaxonomy = builder.parse(new InputSource(in), parseContextImpl);
    
    MappingBuilder mappingBuilder = new MappedTaxonomy.MappingBuilder();
    // Register all recorded mappings
    for (Map.Entry<Taxon, SightingTaxon> mapping : mappings.entrySet()) {
      mappingBuilder.registerTaxonMapping(mapping.getKey(), mapping.getValue());
    }
    mappedTaxonomy.setMappings(mappingBuilder);
    disableUnmappedTaxa(mappedTaxonomy, mappedTaxonomy.getRoot());
    return mappedTaxonomy;
  }
  
  /**
   * Disable any taxon
   * 
   * @return true if any of the children of this taxon had mappings
   */
  private boolean disableUnmappedTaxa(MappedTaxonomy mappedTaxonomy, Taxon taxon) {
    boolean foundMapping = false;
    for (Taxon child : taxon.getContents()) {
      foundMapping = disableUnmappedTaxa(mappedTaxonomy, child) || foundMapping;
    }
    
    if (!foundMapping) {
      if (mappedTaxonomy.getExactMapping(taxon) != null) {
        foundMapping = true;
      } else {
        taxon.setDisabled(true);
      }
    }
    
    return foundMapping;
  }

  @SuppressWarnings("unchecked")
  static private IdentityHashMap<Taxon, SightingTaxon> taxonMappings(ParseContext context) {
    return (IdentityHashMap<Taxon, SightingTaxon>) context.getProperty(MAPPED_TAXONOMY_MAPPINGS_KEY);
  }

  static private ParserManager createParserManager() {
    ParserManager pm = new ParserManager();
    pm.registerFactory(MappedTaxonomy.class, XmlTaxonExport.NAMESPACE,
            new MappedTaxonomyFactory());
    pm.registerFactory(Taxonomy.class, XmlTaxonExport.NAMESPACE,
        new TaxonomyFactory());
    pm.registerFactory(Taxon.class, XmlTaxonExport.NAMESPACE,
        new TaxonFactory());
    return pm;
  }

  static class TaxonomyFactory implements ParserFactory {
    @Override
    public NodeParser getParser(ParseContext context, String namespaceURI,
        String localName) {
      return new TaxonomyImplParser();
    }
  }

  static private class MappedTaxonomyFactory implements ParserFactory {
    @Override
    public NodeParser getParser(ParseContext context, String namespaceURI,
        String localName) {
      return new MappedTaxonomyImplParser((Taxonomy) context.getProperty(Taxonomy.class));
    }
  }
  
  static private class MappedTaxonomyImplParser extends TaxonomyImplParser {
    private final Taxonomy base;

    public MappedTaxonomyImplParser(Taxonomy base) {
      this.base = Preconditions.checkNotNull(base);
    }
    
    @Override
    protected TaxonomyImpl newTaxonomy(String id, String name, String accountIdFormat, String accountLinkTitle) {
      return new MappedTaxonomy(base, id, name);
    }
  }
  
  static class TaxonomyImplParser extends BaseNodeParser {
    private TaxonomyImpl taxonomy;
    
    public TaxonomyImplParser() {
    }

    @Override public void addCompletedChild(ParseContext context,
        String namespaceURI, String localName, Object child)
        throws SAXParseException {
      if ("additional-credit".equals(localName)) {
        taxonomy.addAdditionalCredit((String) child);
      } else {
        taxonomy.setRoot((Taxon) child);
      }
    }

    @Override public Object endElement(ParseContext context,
        String namespaceURI, String localName) throws SAXParseException {
      setTaxonomy(context, null);
      return taxonomy;
    }

    @Override public NodeParser startChildElement(ParseContext context,
        String namespaceURI, String localName, Attributes attrs)
        throws SAXParseException {
      if ("additional-credit".equals(localName)) {
        return new StringParser();
      }
      
      return TaxonFactory.INSTANCE.getParser(context, namespaceURI, localName);
    }

    @Override public void startElement(ParseContext context,
        String namespaceURI, String localName, Attributes attrs)
        throws SAXParseException {
      String id = getRequiredAttribute(context, attrs, "id");
      String name = getRequiredAttribute(context, attrs, "name");
      String accountUrlFormat = attrs.getValue("accountUrlFormat");
      String accountLinkTitle = attrs.getValue("accountLinkTitle");
      taxonomy = newTaxonomy(id, name, accountUrlFormat, accountLinkTitle);
      // Store off the taxonomy for later access
      setTaxonomy(context, taxonomy);
    }
    
    protected TaxonomyImpl newTaxonomy(String id, String name, String accountUrlFormat, String accountUrlTitle) {
      return new TaxonomyImpl(id, name, accountUrlFormat, accountUrlTitle);
    }
  }

  static private class TaxonFactory implements ParserFactory {
    static ParserFactory INSTANCE = new TaxonFactory();
    
    @Override
    public NodeParser getParser(ParseContext context, String namespaceURI,
        String localName) {
      Taxon.Type type = TAXON_TYPES_BY_NAME.get(localName);
      if (type == null)
        return null;

      if (type == Taxon.Type.species || type == Taxon.Type.subspecies
          || type == Taxon.Type.group)
        return new SpeciesParser(type);

      return new TaxonParser(type);
    }
  }

  /**
   * Parser for <species> elements.
   */
  static private class SpeciesParser extends TaxonParser {
    private boolean hasMetadata;
    private SpeciesImpl species;
    private ImmutableList.Builder<String> alternateNames;
    private ImmutableList.Builder<String> alternateCommonNames;
    
    public SpeciesParser(Taxon.Type type) {
      super(type);
    }

    @Override public NodeParser startChildElement(ParseContext context,
        String namespaceURI, String localName, Attributes attrs)
        throws SAXParseException {
      if (hasMetadata) {
        if ("alternate-names".equals(localName)
            || "alternate-common-names".equals(localName)
            || "range".equals(localName) || "miscellaneous".equals(localName)
            || "status".equals(localName) || "taxonomy".equals(localName)) {
          return new StringParser();
        }

        return null;
      } else if ("metadata".equals(localName)) {
        hasMetadata = true;
        return this;
      } else if (XmlTaxonExport.SINGLE_MAPPING.equals(localName)
          || XmlTaxonExport.SP_MAPPING.equals(localName)
          || XmlTaxonExport.SSP_MAPPING.equals(localName)) {
        return new StringParser();
      } else if (XmlTaxonExport.NO_MAPPING.equals(localName)) {
        return BaseNodeParser.getIgnoreParser();
      } else if (XmlTaxonExport.ELEMENT_CHECKLISTS.equals(localName)
          && ExtendedTaxonomyNodeParser.canParseChecklists(context)) {
        return new StringParser();
      }

      return super.startChildElement(context, namespaceURI, localName, attrs);
    }

    @Override public void addCompletedChild(ParseContext context,
        String namespaceURI, String localName, Object child)
        throws SAXParseException {
      if (hasMetadata) {
        String s = (String) child;
        if ("alternate-names".equals(localName)) {
          if (alternateNames == null) {
            alternateNames = ImmutableList.builder();
          }
          alternateNames.add(s);
        } else if ("alternate-common-names".equals(localName)) {
          if (alternateCommonNames == null) {
            alternateCommonNames = ImmutableList.builder();
          }
          alternateCommonNames.add(s);
        } else if ("miscellaneous".equals(localName)) {
          species.setMiscellaneousInfo(s);
        } else if ("taxonomy".equals(localName)) {
          species.setTaxonomicInfo(s);
        } else if ("range".equals(localName)) {
          species.setRange(s);
        } else if ("status".equals(localName)) {
          species.setStatus(Enum.valueOf(Species.Status.class, s.trim()));
        } else {
          throw new SAXParseException("Unexpected metadata: " + localName,
              context.getLocator());
        }
      } else if (XmlTaxonExport.SINGLE_MAPPING.equals(localName)) {
        IdentityHashMap<Taxon, SightingTaxon> taxonMappings = taxonMappings(context);
        if (taxonMappings != null) {
          taxonMappings.put(species, SightingTaxons.newSightingTaxon((String) child));
        }
      } else if (XmlTaxonExport.SP_MAPPING.equals(localName)) {
        ImmutableList<String> ids = ImmutableList.copyOf(Splitter.on(',').split((String) child));
        IdentityHashMap<Taxon, SightingTaxon> taxonMappings = taxonMappings(context);
        if (taxonMappings != null) {
          try {
            taxonMappings.put(species, SightingTaxons.newSpTaxon(ids));
          } catch (IllegalStateException e) {
            SAXParseException saxE =
                new SAXParseException("Could not parse SP mapping", context.getLocator());
            saxE.initCause(e);
            throw saxE;
          }
        }
      } else if (XmlTaxonExport.SSP_MAPPING.equals(localName)) {
        IdentityHashMap<Taxon, SightingTaxon> taxonMappings = taxonMappings(context);
        if (taxonMappings != null) {
          // Grab the result of the prior SINGLE_MAPPING, and append the new SSP mapping
          SightingTaxon sightingTaxon = taxonMappings(context).get(species);
          if (sightingTaxon == null || sightingTaxon.getType() != SightingTaxon.Type.SINGLE) {
            throw new SAXParseException("Unexpected spp mapping " + child, context.getLocator());
          }
          if (taxonMappings != null) {
            taxonMappings.put(species,
                SightingTaxons.newSightingTaxonWithSecondarySubspecies(sightingTaxon.getId(), (String) child));
          }
        }
      } else if (XmlTaxonExport.ELEMENT_CHECKLISTS.equals(localName)) {
        String s = (String) child;
        List<String> codes = Splitter.on(',').splitToList(s);
        if (!codes.isEmpty()) {
          ExtendedTaxonomyNodeParser.addChecklistElements(context, SightingTaxons.newSightingTaxon(id), species.getStatus(), codes);
        }
      } else {
        super.addCompletedChild(context, namespaceURI, localName, child);
      }
    }

    @Override public void endChildElement(ParseContext context,
        String namespaceURI, String localName) throws SAXParseException {
      if ("metadata".equals(localName)) {
        hasMetadata = false;
      }
    }

    @Override protected TaxonImpl createTaxon(Taxon.Type type, Taxonomy taxonomy) {
      // Ehhh, ignore the taxonomy:  it'll get set when setParent() is called
      species = new SpeciesImpl(type, null);
      return species;
    }

    @Override public Object endElement(ParseContext context,
        String namespaceURI, String localName) throws SAXParseException {
      if (alternateNames != null) {
        species.setAlternateNames(alternateNames.build());
      }
      if (alternateCommonNames != null) {
        species.setAlternateCommonNames(alternateCommonNames.build());
      }
      return super.endElement(context, namespaceURI, localName);
    }
  }

  static private class TaxonParser extends BaseNodeParser {
    private final Taxon.Type type;
    private TaxonImpl taxon;
    protected String id;

    public TaxonParser(Taxon.Type type) {
      this.type = type;
    }

    @Override public void addCompletedChild(ParseContext context,
        String namespaceURI, String localName, Object child)
        throws SAXParseException {
      if ("common-name".equals(localName))
        taxon.setCommonName((String) child);
      else if (child instanceof Taxon) {
        // TODO: something more elegant than the need to add() and setParent()?!?
        taxon.getContents().add((Taxon) child);
        ((TaxonImpl) child).setParent(taxon);
      }

      super.addCompletedChild(context, namespaceURI, localName, child);
    }

    @Override public Object endElement(ParseContext context,
        String namespaceURI, String localName) throws SAXParseException {
      return taxon;
    }

    @Override public NodeParser startChildElement(ParseContext context,
        String namespaceURI, String localName, Attributes attrs)
        throws SAXParseException {
      if ("common-name".equals(localName)) {
        return new StringParser();
      }

      return TaxonFactory.INSTANCE.getParser(context, namespaceURI, localName);
    }

    @Override public void startElement(ParseContext context,
        String namespaceURI, String localName, Attributes attrs)
        throws SAXParseException {
      TaxonomyImpl taxonomy = _getTaxonomy(context);

      taxon = createTaxon(type, taxonomy);
      taxon.setName(getRequiredAttribute(context, attrs, "name"));

      id = getRequiredAttribute(context, attrs, "id");
      if (id != null) {
        taxonomy.register(taxon, id);
      }
      
      String accountId = attrs.getValue("accountId");
      if (accountId != null) {
        taxon.setAccountId(accountId);
      }
      
    }

    protected TaxonImpl createTaxon(Taxon.Type type, Taxonomy taxonomy) {
      return new TaxonImpl(type, taxonomy);
    }
  }

  static private TaxonomyImpl _getTaxonomy(ParseContext context) {
    return (TaxonomyImpl) context.getProperty(TaxonomyImpl.class);
  }

  static private void setTaxonomy(ParseContext context, TaxonomyImpl taxonomy) {
    context.setProperty(TaxonomyImpl.class, taxonomy);
  }
}
