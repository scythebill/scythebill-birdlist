/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.io;

import java.util.Locale;
import org.joda.time.DateTimeFieldType;
import org.joda.time.Partial;
import org.joda.time.ReadablePartial;
import org.joda.time.chrono.GJChronology;

import junit.framework.TestCase;

public class PartialIOTest extends TestCase {
  public void testToString() {
    Partial year = (new Partial(GJChronology.getInstance())).with(
            DateTimeFieldType.year(), 1999);
    assertEquals("1999", PartialIO.toString(year));
    assertRoundTrip(year);

    Partial yearMonth = (new Partial(GJChronology.getInstance()))
       .with(DateTimeFieldType.year(), 1999)
       .with(DateTimeFieldType.monthOfYear(), 8);
    assertEquals("1999-08", PartialIO.toString(yearMonth));
    assertRoundTrip(yearMonth);
  }
  
  public void testToShortUserString() {
    ReadablePartial yearMonthDay = PartialIO.fromString("2010-08-16");
    assertEquals("8/16/10", PartialIO.toShortUserString(yearMonthDay, Locale.US));
    assertEquals("16/08/2010", PartialIO.toShortUserString(yearMonthDay, Locale.UK));
  }
  
  public void testFromString() {
    ReadablePartial year = PartialIO.fromString("1999");
    assertEquals(1, year.size());
    assertEquals(1999, year.get(DateTimeFieldType.year()));

    ReadablePartial yearMonthDay = PartialIO.fromString("2010-08-16");
    assertEquals(3, yearMonthDay.size());
    assertEquals(2010, yearMonthDay.get(DateTimeFieldType.year()));
    assertEquals(8, yearMonthDay.get(DateTimeFieldType.monthOfYear()));
    assertEquals(16, yearMonthDay.get(DateTimeFieldType.dayOfMonth()));

    ReadablePartial time = PartialIO.fromString("h20-16-58");
    assertEquals(3, time.size());
    assertEquals(20, time.get(DateTimeFieldType.hourOfDay()));
    assertEquals(58, time.get(DateTimeFieldType.secondOfMinute()));
  }
  
  public void testFromString_error() {
    try {
      PartialIO.fromString("y1f");
      fail();
    } catch (IllegalArgumentException iae) {
      // OK
    }
    try {
      PartialIO.fromString("y1f");
      fail();
    } catch (IllegalArgumentException iae) {
      // OK
    }
    try {
      PartialIO.fromString("Y1999");
      fail();
    } catch (IllegalArgumentException iae) {
      // OK
    }
    try {
      PartialIO.fromString("m");
      fail();
    } catch (IllegalArgumentException iae) {
      // OK
    }
    try {
      PartialIO.fromString("m19-m17");
      fail();
    } catch (IllegalArgumentException iae) {
      // OK
    }
  }
  
  private void assertRoundTrip(Partial p) {
    assertEquals(p, PartialIO.fromString(PartialIO.toString(p)));
  }
}
