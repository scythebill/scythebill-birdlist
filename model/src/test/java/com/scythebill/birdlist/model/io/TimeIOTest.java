/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.io;

import static org.junit.Assert.assertEquals;

import java.util.Locale;

import org.joda.time.LocalTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TimeIOTest {
  @Test
  public void fromString_doesNotSetSeconds() {
    LocalTime fromString = TimeIO.fromString("13:59");
    assertEquals(0, fromString.getSecondOfMinute());
    assertEquals(0, fromString.getMillisOfSecond());
  }

  @Test
  public void fromExternalString_dropsSecondsWhenSecondsPresent() {
    LocalTime fromString = TimeIO.fromExternalString("13:59:11");
    assertEquals(0, fromString.getSecondOfMinute());
    assertEquals(0, fromString.getMillisOfSecond());
  }

  @Test
  public void fromExternalString_supportsAM() {
    assertEquals(
        TimeIO.fromExternalString("1:13"),
        TimeIO.fromExternalString("1:13 AM"));
  }

  @Test
  public void fromExternalString_supportsAM_spanishLocale() {
    Locale original = Locale.getDefault();
    try {
      Locale.setDefault(new Locale("es"));
      assertEquals(
          TimeIO.fromExternalString("1:13"),
          TimeIO.fromExternalString("1:13 AM"));
    } finally {
      Locale.setDefault(original);
    }
  }

  @Test
  public void fromExternalString_supportsPM() {
    assertEquals(
        TimeIO.fromExternalString("13:27"),
        TimeIO.fromExternalString("1:27 PM"));
  }
  
  @Test
  public void fromExternalString_supportsPM_spanishLocale() {
    Locale original = Locale.getDefault();
    try {
      Locale.setDefault(new Locale("es"));
      assertEquals(
          TimeIO.fromExternalString("13:13"),
          TimeIO.fromExternalString("1:13 PM"));
    } finally {
      Locale.setDefault(original);
    }
  }
  @Test
  public void normalizeIsANoOpForParsed() {
    LocalTime fromString = TimeIO.fromString("13:59");
    assertEquals(fromString, TimeIO.normalize(fromString));
  }
  
  @Test
  public void fromExternalString_twoComponent12HrTime() {
    LocalTime fromString = TimeIO.fromExternalString("07:53 PM");
    assertEquals(19, fromString.getHourOfDay());
    assertEquals(53, fromString.getMinuteOfHour());
    
    fromString = TimeIO.fromExternalString("7:53 PM");
    assertEquals(19, fromString.getHourOfDay());
    assertEquals(53, fromString.getMinuteOfHour());

    fromString = TimeIO.fromExternalString("11:53 AM");
    assertEquals(11, fromString.getHourOfDay());
    assertEquals(53, fromString.getMinuteOfHour());

    fromString = TimeIO.fromExternalString("11:53AM");
    assertEquals(11, fromString.getHourOfDay());
    assertEquals(53, fromString.getMinuteOfHour());
  }

  @Test
  public void fromExternalString_twoComponent24HrTime() {
    LocalTime fromString = TimeIO.fromExternalString("19:53");
    assertEquals(19, fromString.getHourOfDay());
    assertEquals(53, fromString.getMinuteOfHour());
    
    fromString = TimeIO.fromExternalString("07:53");
    assertEquals(7, fromString.getHourOfDay());
    assertEquals(53, fromString.getMinuteOfHour());

    fromString = TimeIO.fromExternalString("11:53");
    assertEquals(11, fromString.getHourOfDay());
    assertEquals(53, fromString.getMinuteOfHour());
  }

  @Test
  public void fromExternalString_threeComponent12HrTime() {
    LocalTime fromString = TimeIO.fromExternalString("07:53:54 PM");
    assertEquals(19, fromString.getHourOfDay());
    assertEquals(53, fromString.getMinuteOfHour());
    assertEquals(0, fromString.getSecondOfMinute());
    
    fromString = TimeIO.fromExternalString("7:53:54 PM");
    assertEquals(19, fromString.getHourOfDay());
    assertEquals(53, fromString.getMinuteOfHour());
    assertEquals(0, fromString.getSecondOfMinute());

    fromString = TimeIO.fromExternalString("11:53:54 AM");
    assertEquals(11, fromString.getHourOfDay());
    assertEquals(53, fromString.getMinuteOfHour());
    assertEquals(0, fromString.getSecondOfMinute());

    fromString = TimeIO.fromExternalString("11:53:54AM");
    assertEquals(11, fromString.getHourOfDay());
    assertEquals(53, fromString.getMinuteOfHour());
    assertEquals(0, fromString.getSecondOfMinute());
  }

  @Test
  public void fromExternalString_threeComponent24HrTime() {
    LocalTime fromString = TimeIO.fromExternalString("13:53:54");
    assertEquals(13, fromString.getHourOfDay());
    assertEquals(53, fromString.getMinuteOfHour());
    assertEquals(0, fromString.getSecondOfMinute());

    fromString = TimeIO.fromExternalString("5:53:54");
    assertEquals(5, fromString.getHourOfDay());
    assertEquals(53, fromString.getMinuteOfHour());
    assertEquals(0, fromString.getSecondOfMinute());


    fromString = TimeIO.fromExternalString("09:02:54");
    assertEquals(9, fromString.getHourOfDay());
    assertEquals(2, fromString.getMinuteOfHour());
    assertEquals(0, fromString.getSecondOfMinute());
  }
}
