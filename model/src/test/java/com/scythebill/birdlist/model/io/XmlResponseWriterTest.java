package com.scythebill.birdlist.model.io;

import static org.junit.Assert.assertEquals;

import java.io.StringWriter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class XmlResponseWriterTest {

  @Test
  public void testSurrogatesInAttributes() throws Exception {
    StringWriter sw = new StringWriter();
    XmlResponseWriter out = new XmlResponseWriter(sw, "UTF-8");
    out.startDocument();
    out.startElement("e");
    out.writeAttribute("a", "\uD83D\uDE02");
    out.endElement("e");
    out.endDocument();
    out.close();
    String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
        "<e a=\"&#x1f602;\"/>";
    assertEquals(expected, sw.toString());
  }

  @Test
  public void testSurrogatesInText() throws Exception {
    StringWriter sw = new StringWriter();
    XmlResponseWriter out = new XmlResponseWriter(sw, "UTF-8");
    out.startDocument();
    out.writeText("\uD83D\uDE02");
    out.endDocument();
    out.close();
    String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
        "&#x1f602;";
    assertEquals(expected, sw.toString());
  }
}
