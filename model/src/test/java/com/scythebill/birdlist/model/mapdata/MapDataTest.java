/*   
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.mapdata;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.scythebill.birdlist.model.sighting.LatLongCoordinates;

@RunWith(JUnit4.class)
public class MapDataTest {

  @Test
  public void usa() {
    Assert.assertEquals(getISOCode(37.7749, -122.4194), "US-CA");
    Assert.assertEquals(getISOCode(21.3069, -157.8583), "US-HI");
  }
  
  @Test
  public void england() {
    Assert.assertEquals(getISOCode(51.5074, -0.1278), "GB-ENG");
  }
  
  @Test
  public void france() {
    Assert.assertEquals(getISOCode(48.8566, 2.3522), "FR");
  }
  
  @Test
  public void brazil() {
    Assert.assertEquals(getISOCode(-12.688302, -40.109304), "BR-BA");
  }

  @Test
  public void newZealand() {
    Assert.assertEquals(getISOCode(-36.8509, 174.7645), "NZ");
  }
  
  @Test
  public void newCaledonia() {
    Assert.assertEquals(getISOCode(-22.2735, 166.4481), "NC");
  }

  @Test
  public void tahiti() {
    Assert.assertEquals(getISOCode(-17.6509, -149.4260), "PF");
  }

  @Test
  public void australiaPelagic() {
    Assert.assertEquals(getISOCode(-31.836564, 114.450832), "AU-WA");
  }

  @Test
  public void galapagos() {
    Assert.assertEquals(getISOCode(-0.823276, -89.455163), "EC-W");
  }

  @Test
  public void mainlandEcuador() {
    Assert.assertEquals(getISOCode(-2.690346, -79.380247), "EC");
  }

  private String getISOCode(double latitude, double longitude) {
    return MapData.instance().getISOCode(
        LatLongCoordinates.withLatAndLong(latitude, longitude));
  }
      
}
