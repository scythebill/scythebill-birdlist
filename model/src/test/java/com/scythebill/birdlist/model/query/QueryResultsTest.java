package com.scythebill.birdlist.model.query;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;

import junit.framework.TestCase;

import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.io.Resources;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.xml.XmlTaxonImport;

public class QueryResultsTest extends TestCase {
  private Taxonomy taxonomy;
  private Location location;

  @Override
  protected void setUp() throws Exception {
    XmlTaxonImport importer = new XmlTaxonImport();
    URL testTaxonUrl = Resources.getResource("testTaxonomy.xml");
    InputStream stream = testTaxonUrl.openStream();
    taxonomy = importer.importTaxa(new InputStreamReader(stream, "UTF-8"));
    
    location = Location.builder().setName("Somewhere").build();
    LocationSet locationSet = new LocationSet.Builder().build();
    locationSet.addLocation(location);
  }
  
  public void testGetTaxaAsList() {
    QueryResults.Builder builder = new QueryResults.Builder();
    builder.withTaxonFilter(Predicates.<Resolved>alwaysTrue());
    SightingTaxon greaterRhea = SightingTaxons.newSightingTaxon("spRheaame");
    SightingTaxon ostrich = SightingTaxons.newSightingTaxon("spStrutcam");
    
    builder.addSighting(newSighting(greaterRhea), greaterRhea);
    builder.addSighting(newSighting(ostrich), ostrich);
    builder.addSighting(newSighting(greaterRhea), greaterRhea);
    builder.addSighting(newSighting(ostrich), ostrich);
    
    QueryResults results = new QueryResults(
            taxonomy, null, null,
            new PredicateQueryDefinition(Predicates.<Sighting>alwaysTrue()), builder, Taxon.Type.species, null);
    List<Resolved> taxaAsList = results.getTaxaAsList();
    assertEquals(4, taxaAsList.size());
    assertEquals("Ostrich", taxaAsList.get(0).getCommonName());
    assertEquals(Taxon.Type.family, taxaAsList.get(0).getSmallestTaxonType());
    assertEquals("Ostrich", taxaAsList.get(1).getCommonName());
    assertEquals(Taxon.Type.species, taxaAsList.get(1).getSmallestTaxonType());
    assertEquals("Rheas", taxaAsList.get(2).getCommonName());
    assertEquals(Taxon.Type.family, taxaAsList.get(2).getSmallestTaxonType());
    assertEquals("Greater Rhea", taxaAsList.get(3).getCommonName());
  }

  public void testGetTaxaWithSingleAndSp() {
    QueryResults.Builder builder = new QueryResults.Builder();
    builder.withTaxonFilter(Predicates.<Resolved>alwaysTrue());
    SightingTaxon greaterRhea = SightingTaxons.newSightingTaxon("spRheaame");
    SightingTaxon lesserRhea = SightingTaxons.newSightingTaxon("spRheapen");
    SightingTaxon sp = SightingTaxons.newSpTaxon(
        ImmutableList.of("spRheaame", "spRheapen"));
    
    builder.addSighting(newSighting(sp), sp);
    builder.addSighting(newSighting(greaterRhea), greaterRhea);
    builder.addSighting(newSighting(lesserRhea), lesserRhea);
    builder.addSighting(newSighting(greaterRhea), greaterRhea);
    
    QueryResults results = new QueryResults(
            taxonomy, null, null,
            new PredicateQueryDefinition(Predicates.<Sighting>alwaysTrue()),
            builder, Taxon.Type.species, null);
    List<Resolved> taxaAsList = results.getTaxaAsList();
    assertEquals(4, taxaAsList.size());
    assertEquals("Rheas", taxaAsList.get(0).getCommonName());
    assertEquals("Greater Rhea", taxaAsList.get(1).getCommonName());
    assertEquals("Lesser Rhea", taxaAsList.get(2).getCommonName());
    assertEquals("Greater/Lesser Rhea", taxaAsList.get(3).getCommonName());
    
    assertEquals(ImmutableSet.of("spRheaame", "spRheapen"),
        results.getCountableSpeciesIds(taxonomy));
    assertEquals(2, results.getCountableSpeciesSize(taxonomy, false));
    assertEquals(2, results.getCountableSpeciesSize(taxonomy, true));
  }

  public void testGetTaxaWithSp() {
    QueryResults.Builder builder = new QueryResults.Builder();
    builder.withTaxonFilter(Predicates.<Resolved>alwaysTrue());
    SightingTaxon sp = SightingTaxons.newSpTaxon(
        ImmutableList.of("spRheaame", "spRheapen"));
    
    builder.addSighting(newSighting(sp), sp);
    
    QueryResults results = new QueryResults(
            taxonomy, null, null,
            new PredicateQueryDefinition(Predicates.<Sighting>alwaysTrue()),
            builder, Taxon.Type.species, null);
    List<Resolved> taxaAsList = results.getTaxaAsList();
    assertEquals(2, taxaAsList.size());
    assertEquals("Rheas", taxaAsList.get(0).getCommonName());
    assertEquals("Greater/Lesser Rhea", taxaAsList.get(1).getCommonName());
    
    assertTrue(results.getCountableSpeciesIds(taxonomy).isEmpty());
    assertEquals(0, results.getCountableSpeciesSize(taxonomy, false));
    assertEquals(1, results.getCountableSpeciesSize(taxonomy, true));
    assertTrue(results.getCountableGroupsAndSpeciesIds(taxonomy).isEmpty());
    assertTrue(results.getCountableSubspeciesGroupsAndSpeciesIds(taxonomy).isEmpty());
  }

  public void testGetCountableSpeciesIds_groupSp() {
    QueryResults.Builder builder = new QueryResults.Builder();
    builder.withTaxonFilter(Predicates.<Resolved>alwaysTrue());
    SightingTaxon groupSp = SightingTaxons.newSpTaxon(
        ImmutableList.of("grRheapentar", "grRheapenpen"));
    
    builder.addSighting(newSighting(groupSp), groupSp);
    
    QueryResults results = new QueryResults(
            taxonomy, null, null,
            new PredicateQueryDefinition(Predicates.<Sighting>alwaysTrue()),
            builder, Taxon.Type.group, null);
    List<Resolved> taxaAsList = results.getTaxaAsList();
    assertEquals(2, taxaAsList.size());
    assertEquals("Rheas", taxaAsList.get(0).getCommonName());
    assertEquals("Lesser Rhea (Puna)/(Darwin's)", taxaAsList.get(1).getCommonName());

    assertEquals(ImmutableSet.of("spRheapen"),
        results.getCountableSpeciesIds(taxonomy));
    assertEquals(ImmutableSet.of("spRheapen"),
        results.getCountableGroupsAndSpeciesIds(taxonomy));
    assertEquals(ImmutableSet.of("spRheapen"),
        results.getCountableSubspeciesGroupsAndSpeciesIds(taxonomy));
  }

  public void testGetCountableSpeciesIds_subspSp() {
    QueryResults.Builder builder = new QueryResults.Builder();
    builder.withTaxonFilter(Predicates.<Resolved>alwaysTrue());
    
    SightingTaxon sp = SightingTaxons.newSpTaxon(
        ImmutableList.of("sspRheapentartar", "sspRheapentargar"));
    builder.addSighting(newSighting(sp), sp);
    
    QueryResults results = new QueryResults(
            taxonomy, null, null,
            new PredicateQueryDefinition(Predicates.<Sighting>alwaysTrue()),
            builder, Taxon.Type.subspecies, null);
    List<Resolved> taxaAsList = results.getTaxaAsList();
    assertEquals(2, taxaAsList.size());
    assertEquals("Rheas", taxaAsList.get(0).getCommonName());
    assertEquals("Lesser Rhea (Puna) (R.p.garleppi)/(R.p.tarapacensis)",
        taxaAsList.get(1).getCommonName());

    assertEquals(ImmutableSet.of("spRheapen"),
        results.getCountableSpeciesIds(taxonomy));
    assertEquals(ImmutableSet.of("grRheapentar"),
        results.getCountableGroupsAndSpeciesIds(taxonomy));
    assertEquals(ImmutableSet.of("grRheapentar"),
        results.getCountableSubspeciesGroupsAndSpeciesIds(taxonomy));
  }

  public void testGetCountableSpeciesIds_mixSp() {
    QueryResults.Builder builder = new QueryResults.Builder();
    builder.withTaxonFilter(Predicates.<Resolved>alwaysTrue());

    SightingTaxon sspSp = SightingTaxons.newSpTaxon(
        ImmutableList.of("sspRheapentartar", "sspRheapentargar"));
    builder.addSighting(newSighting(sspSp), sspSp);

    SightingTaxon groupSp = SightingTaxons.newSpTaxon(
            ImmutableList.of("grRheapentar", "grRheapenpen"));        
    builder.addSighting(newSighting(groupSp), groupSp);
    
    SightingTaxon sp = SightingTaxons.newSpTaxon(
            ImmutableList.of("spRheaame", "spRheapen"));        
    builder.addSighting(newSighting(sp), sp);
    
    QueryResults results = new QueryResults(
            taxonomy, null, null,
            new PredicateQueryDefinition(Predicates.<Sighting>alwaysTrue()),
            builder, Taxon.Type.subspecies, null);
    List<Resolved> taxaAsList = results.getTaxaAsList();
    assertEquals(4, taxaAsList.size());
    assertEquals("Rheas", taxaAsList.get(0).getCommonName());
    assertEquals("Lesser Rhea (Puna) (R.p.garleppi)/(R.p.tarapacensis)",
            taxaAsList.get(1).getCommonName());
    assertEquals("Lesser Rhea (Puna)/(Darwin's)", taxaAsList.get(2).getCommonName());
    assertEquals("Greater/Lesser Rhea", taxaAsList.get(3).getCommonName());

    assertEquals(ImmutableSet.of("spRheapen"),
        results.getCountableSpeciesIds(taxonomy));
    assertEquals(ImmutableSet.of("grRheapentar"),
        results.getCountableGroupsAndSpeciesIds(taxonomy));
    assertEquals(ImmutableSet.of("grRheapentar"),
        results.getCountableSubspeciesGroupsAndSpeciesIds(taxonomy));
  }

  private Sighting newSighting(SightingTaxon taxon) {
    return Sighting.newBuilder()
        .setTaxonomy(taxonomy)
        .setLocation(location)
        .setTaxon(taxon)
        .build();
  }
}
