/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.joda.time.DateTimeFieldType;
import org.joda.time.Partial;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.common.collect.ImmutableSortedSet;
import com.scythebill.birdlist.model.sighting.ApproximateNumber;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;

/**
 * Tests for SightingComparators.
 */
@RunWith(JUnit4.class)
public class SightingComparatorsTest {
  
  @Test
  public void comparePartials() {
    Partial jan91 = (new Partial())
        .with(DateTimeFieldType.year(), 1991)
        .with(DateTimeFieldType.monthOfYear(), 1);
    Partial jan92 = (new Partial())
        .with(DateTimeFieldType.year(), 1992)
        .with(DateTimeFieldType.monthOfYear(), 1);
    Partial feb91 = (new Partial())
        .with(DateTimeFieldType.year(), 1991)
        .with(DateTimeFieldType.monthOfYear(), 2);
    // Fifth of an unspecified month in '92;  should
    // sort before anything in a specific month in '92
    Partial fifthOf92 = (new Partial())
        .with(DateTimeFieldType.year(), 1992)
        .with(DateTimeFieldType.dayOfMonth(), 5);
    
    assertTrue(SightingComparators.comparePartials(jan91, jan91) == 0);
    assertTrue(SightingComparators.comparePartials(jan91, jan92) < 0);
    assertTrue(SightingComparators.comparePartials(jan92, jan91) > 0);

    assertTrue(SightingComparators.comparePartials(feb91, jan91) > 0);
    assertTrue(SightingComparators.comparePartials(jan91, feb91) < 0);

    assertTrue(SightingComparators.comparePartials(feb91, jan92) < 0);
    assertTrue(SightingComparators.comparePartials(jan92, feb91) > 0);
    
    assertTrue(SightingComparators.comparePartials(fifthOf92, jan92) < 0);
    assertTrue(SightingComparators.comparePartials(jan92, fifthOf92) > 0);

    assertTrue(SightingComparators.comparePartials(feb91, fifthOf92) < 0);
    assertTrue(SightingComparators.comparePartials(fifthOf92, feb91) > 0);
  }

  @Test
  public void compareCount() {
    Taxonomy taxonomy = new TaxonomyImpl(); 
    Sighting noCount = Sighting.newBuilder()
        .setTaxonomy(taxonomy)
        .setTaxon(SightingTaxons.newSightingTaxon("id")).build();
    Sighting oneCount = Sighting.newBuilder()
        .setTaxonomy(taxonomy)
        .setTaxon(SightingTaxons.newSightingTaxon("id"))
        .setSightingInfo(new SightingInfo().setNumber(ApproximateNumber.exact(1)))
        .build();
    Sighting tenCount = Sighting.newBuilder()
        .setTaxonomy(taxonomy)
        .setTaxon(SightingTaxons.newSightingTaxon("id"))
        .setSightingInfo(new SightingInfo().setNumber(ApproximateNumber.tryParse("~10")))
        .build();
    
    ImmutableSortedSet<Sighting> set = ImmutableSortedSet.orderedBy(SightingComparators.preferHigherCounts())
       .add(oneCount, tenCount, noCount)
       .build();
    assertTrue(set.last().hasSightingInfo());
    assertEquals(ApproximateNumber.tryParse("~10"), set.last().getSightingInfo().getNumber());
    assertFalse(set.first().hasSightingInfo());
  }
  
}
