/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import org.joda.time.DateTimeFieldType;
import org.joda.time.Partial;

import com.google.common.base.Predicate;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.Taxon;

public class SightingPredicatesTest extends SightingTestCase {
  
  public void testAll() {
    Predicate<Sighting> all = SightingPredicates.all();
    assertTrue(all.apply(newSighting(usa, taxon)));    
  }
  
  public void testIn() {
    Predicate<Sighting> inUsa = SightingPredicates.in(usa, locations);
    
    assertTrue(inUsa.apply(newSighting(usa, taxon)));
    assertTrue(inUsa.apply(newSighting(california, taxon)));
    assertFalse(inUsa.apply(newSighting(mexico, taxon)));
    assertFalse(inUsa.apply(newSighting(northAmerica, taxon)));
  }
  
  public void testAfterOrEquals() {
    Partial february = (new Partial()).with(DateTimeFieldType.monthOfYear(), 2);
    Partial february10 = (new Partial()).with(DateTimeFieldType.monthOfYear(), 2)
        .with(DateTimeFieldType.dayOfMonth(), 10);
    Partial february20 = (new Partial()).with(DateTimeFieldType.monthOfYear(), 2)
      .with(DateTimeFieldType.dayOfMonth(), 20);
    Partial in2002 = (new Partial()).with(DateTimeFieldType.year(), 2002);
    
    Predicate<Sighting> inOrAfterFebruary = SightingPredicates.afterOrEquals(february);
    Predicate<Sighting> inOrAfterFebruary10 = SightingPredicates.afterOrEquals(february10);
    Predicate<Sighting> inOrAfterFebruary20 = SightingPredicates.afterOrEquals(february20);
    Predicate<Sighting> inOrAfter2002 = SightingPredicates.afterOrEquals(in2002);
    Sighting sighting = newSighting(usa, taxon);

    assertFalse(inOrAfterFebruary.apply(sighting));
    assertFalse(inOrAfterFebruary10.apply(sighting));
    assertFalse(inOrAfterFebruary20.apply(sighting));
    assertFalse(inOrAfter2002.apply(sighting));
    
    Partial january2001 = (new Partial()).with(DateTimeFieldType.year(), 2001)
        .with(DateTimeFieldType.monthOfYear(), 1)
        .with(DateTimeFieldType.dayOfMonth(), 15);
    Partial february2002 = (new Partial()).with(DateTimeFieldType.year(), 2002)
        .with(DateTimeFieldType.monthOfYear(), 2)
        .with(DateTimeFieldType.dayOfMonth(), 15);
    Partial march2003 = (new Partial()).with(DateTimeFieldType.year(), 2003)
        .with(DateTimeFieldType.monthOfYear(), 3)
        .with(DateTimeFieldType.dayOfMonth(), 15);
    
    
    sighting = sighting.asBuilder().setDate(january2001).build();
    assertFalse(inOrAfterFebruary.apply(sighting));
    assertFalse(inOrAfterFebruary10.apply(sighting));
    assertFalse(inOrAfterFebruary20.apply(sighting));
    assertFalse(inOrAfter2002.apply(sighting));

    sighting = sighting.asBuilder().setDate(february2002).build();
    assertTrue(inOrAfterFebruary.apply(sighting));
    assertTrue(inOrAfterFebruary10.apply(sighting));
    assertFalse(inOrAfterFebruary20.apply(sighting));
    assertTrue(inOrAfter2002.apply(sighting));
    
    sighting = sighting.asBuilder().setDate(march2003).build();
    assertTrue(inOrAfterFebruary.apply(sighting));
    assertTrue(inOrAfterFebruary10.apply(sighting));    
    assertTrue(inOrAfterFebruary20.apply(sighting));
    assertTrue(inOrAfter2002.apply(sighting));
  }

  private Sighting newSighting(Location location, Taxon taxon) {
    return Sighting.newBuilder()
        .setLocation(location)
        .setTaxon(taxon)
        .build();
  }

}
