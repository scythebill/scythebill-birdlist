/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.query;

import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonImpl;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;

import junit.framework.TestCase;

public abstract class SightingTestCase extends TestCase {

  private TaxonomyImpl taxonomy;
  protected Taxon taxon;
  protected LocationSet locations;
  protected Location northAmerica;
  protected Location usa;
  protected Location california;
  protected Location mexico;

  @Override
  protected void setUp() throws Exception {
    super.setUp();

    taxonomy = new TaxonomyImpl();
    taxon = new TaxonImpl(Taxon.Type.family, taxonomy);
    taxonomy.setRoot(taxon);
    taxon.built();

    LocationSet.Builder builder = new LocationSet.Builder();
    northAmerica = Location.builder()
        .setName("North America").setType(Location.Type.region).build();
    usa = Location.builder()
        .setName("United States").setType(Location.Type.country).setParent(northAmerica).build();
    california = Location.builder()
        .setName("California").setParent(usa).setType(Location.Type.state).build();
    mexico = Location.builder()
        .setName("Mexico").setParent(northAmerica).setType(Location.Type.country).build();
    builder.addLocation(northAmerica, "na");
    builder.addLocation(usa, "usa");
    builder.addLocation(california, "cal");
    builder.addLocation(mexico, "mex");

    locations = builder.build();
  }

}