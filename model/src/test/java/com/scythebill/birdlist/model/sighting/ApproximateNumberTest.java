/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ApproximateNumberTest {
  @Test
  public void verifyTryParse() {
    Assert.assertEquals("10", ApproximateNumber.tryParse("10").toString());
    Assert.assertEquals("~10", ApproximateNumber.tryParse("~10").toString());
    Assert.assertEquals("~10", ApproximateNumber.tryParse("  ~10\t \n").toString());
  }

  @Test
  public void invalidValues() {
    Assert.assertNull(ApproximateNumber.tryParse("~BC"));
    Assert.assertNull(ApproximateNumber.tryParse("~"));
    Assert.assertNull(ApproximateNumber.tryParse(""));
    Assert.assertNull(ApproximateNumber.tryParse("-45"));
    Assert.assertNull(ApproximateNumber.tryParse(" "));
    Assert.assertNull(ApproximateNumber.tryParse("1~"));
  }

  @Test
  public void verifyEquals() {
    Assert.assertEquals(ApproximateNumber.tryParse("10"), ApproximateNumber.exact(10));
    Assert.assertFalse(ApproximateNumber.tryParse("~10").equals(ApproximateNumber.exact(10)));
  }
}
