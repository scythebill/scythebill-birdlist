/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class DistanceTest {
  @Test
  public void verifySymmetry() {
    Assert.assertEquals(2.0f, Distance.inKilometers(2.0f).kilometers(), 0.000);
    Assert.assertEquals(0.111f, Distance.inKilometers(0.111f).kilometers(), 0.000);
    Assert.assertEquals(100.325f, Distance.inKilometers(100.325f).kilometers(), 0.000);
  }
}
