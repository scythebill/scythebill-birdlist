/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multiset;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;

/**
 * Basic verifications of ebird codes in predefined locations.
 */
@RunWith(JUnit4.class)
public class EbirdCodesTest {
  @Test
  public void verifyUniqueEbirdCodes() {
    LocationSet locations = ReportSets.newReportSet(new TaxonomyImpl()).getLocations();
    PredefinedLocations predefined = PredefinedLocations.loadAndParse();
    Multiset<String> ebirdCodes = HashMultiset.create();
    
    for (Location root : locations.rootLocations()) {
      findEbirdCodes(root, predefined, ebirdCodes, locations);
    }
    
    ImmutableSet<String> multiContinentLocations = ImmutableSet.of(
        "ID", // Indonesia
        "US", // USA
        "TR", // Turkey
        "TR-34", // Istanbul
        "RU", // Russia
        "BQ", // Caribbean Netherlands
        "XX"); // High Seas
    for (String ebirdCode : ebirdCodes.elementSet()) {
      if (multiContinentLocations.contains(ebirdCode)) {
        Assert.assertTrue("Only one entry for " + ebirdCode, ebirdCodes.count(ebirdCode) > 1);
      } else {
        Assert.assertEquals("Multiple entries for " + ebirdCode, 1, ebirdCodes.count(ebirdCode));
      }
    }
  }

  private void findEbirdCodes(
      Location location,
      PredefinedLocations predefinedLocations,
      Multiset<String> ebirdCodes, LocationSet locations) {
    if (location.getEbirdCode() != null) {
      String ebirdCode = Locations.getLocationCode(location);
      ebirdCodes.add(ebirdCode);
    }
    
    for (Location child : location.contents()) {
      findEbirdCodes(child, predefinedLocations, ebirdCodes, locations);
    }
    
    for (PredefinedLocation predefined : predefinedLocations.getPredefinedLocations(location)) {
      if (location.getContent(predefined.getName()) == null) {
        findEbirdCodes(predefined.create(locations, location), predefinedLocations, ebirdCodes, locations);
      }
    }
  }
}
