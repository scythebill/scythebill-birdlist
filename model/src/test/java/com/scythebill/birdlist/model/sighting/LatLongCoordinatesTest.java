/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class LatLongCoordinatesTest {

  @Test
  public void distance() {
    LatLongCoordinates sanFrancisco = LatLongCoordinates.withLatAndLong(37.7833, 122.4167);
    LatLongCoordinates losAngeles = LatLongCoordinates.withLatAndLong(34.0500, 118.2500);
    
    Assert.assertEquals(sanFrancisco.kmDistance(losAngeles), 559.0, 1.0);
    Assert.assertEquals(losAngeles.kmDistance(sanFrancisco), 559.0, 1.0);
    Assert.assertEquals(sanFrancisco.kmDistance(sanFrancisco), 0.0, 0.0001);
  }

  @Test
  public void distanceNearTheDateLine() {
    // Make sure distances across 180 longitude don't go the long way around!
    LatLongCoordinates oneSide = LatLongCoordinates.withLatAndLong(0, -179.99);
    LatLongCoordinates otherSide = LatLongCoordinates.withLatAndLong(0, 179.99);
    
    Assert.assertTrue(oneSide.kmDistance(otherSide) < 3.0);
  }
  
  @Test
  public void commaDecimalsAreSupported() {
    LatLongCoordinates sanFranciscoWithCommas = LatLongCoordinates.withLatAndLong("37,7833", "122,4167");
    LatLongCoordinates sanFranciscoWithPeriods = LatLongCoordinates.withLatAndLong("37.7833", "122.4167");
    Assert.assertEquals(sanFranciscoWithCommas.latitudeAsDouble(), sanFranciscoWithPeriods.latitudeAsDouble(), 0.0001);
    Assert.assertEquals(sanFranciscoWithCommas.longitudeAsDouble(), sanFranciscoWithPeriods.longitudeAsDouble(), 0.0001);
  }
}
