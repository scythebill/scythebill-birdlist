/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.common.collect.Iterables;

@RunWith(JUnit4.class)
public class LocationSetTest {

  private LocationSet locations;
  private Location root;
  private Location somewhere;

  @Before
  public void createLocationSet() {
    locations = new LocationSet.Builder().build();
    root = Location.builder()
        .setName("Root")
        .setType(Location.Type.region)
        .build();
    locations.addLocation(root);
    
    somewhere = Location.builder()
      .setName("Somewhere")
      .setType(Location.Type.country)
      .setParent(root)
      .build();
    locations.addLocation(somewhere);
  }
  
  @Test
  public void replace() {
    Location elsewhere = Location.builder()
        .setName("Elsewhere")
        .setType(Location.Type.country)
        .setParent(root)
        .build();
    locations.replace(somewhere, elsewhere);
    
    assertEquals(somewhere.getId(), elsewhere.getId());
    assertSame(elsewhere, locations.getLocation(elsewhere.getId()));
    assertSame(elsewhere, Iterables.getOnlyElement(
            locations.getLocationsByModelName(elsewhere.getModelName())));
    assertSame(elsewhere, root.getContent(elsewhere.getModelName()));
    assertNull(root.getContent(somewhere.getModelName()));
    assertEquals(1, root.contents().size());
  }

  @Test
  public void replaceWithNewType() {
    Location somewhereState = Location.builder()
        .setName("Somewhere")
        .setType(Location.Type.state)
        .setParent(root)
        .build();
    locations.replace(somewhere, somewhereState);
    
    assertEquals(somewhere.getId(), somewhereState.getId());
    assertSame(somewhereState, locations.getLocation(somewhereState.getId()));
    assertSame(somewhereState, Iterables.getOnlyElement(
            locations.getLocationsByModelName(somewhereState.getModelName())));
    assertSame(somewhereState, root.getContent(somewhereState.getModelName()));
    assertEquals(1, root.contents().size());
  }
  
  @Test
  public void replacePreservesChildren() {
    Location newRoot = Location.builder()
        .setName("New Root")
        .setType(Location.Type.region)
        .build();
    // The test also verifies replacement of root locations
    locations.replace(root, newRoot);
    
    assertEquals(root.getId(), newRoot.getId());
    assertSame(newRoot, locations.getLocation(newRoot.getId()));
    assertSame(newRoot, Iterables.getOnlyElement(
        locations.getLocationsByModelName(newRoot.getModelName())));
    assertEquals(1, newRoot.contents().size());
    assertSame(somewhere, newRoot.getContent(somewhere.getModelName()));
    assertSame(newRoot, somewhere.getParent());
  }
  
  @Test
  public void remove() {
    locations.remove(somewhere);
    assertNull(locations.getLocation(somewhere.getId()));
    assertTrue(locations.getLocationsByModelName(somewhere.getModelName()).isEmpty());
    assertNull(somewhere.getParent());
    assertTrue(somewhere.contents().isEmpty());
    assertNull(root.getContent(somewhere.getModelName()));
    assertTrue(root.contents().isEmpty());
  }
  
  @Test
  public void addNewHierarchy() {
    Location county = Location.builder()
        .setName("Some County")
        .setParent(somewhere)
        .build();
    Location city = Location.builder()
        .setName("Some City")
        .setParent(county)
        .build();
    Location home = Location.builder()
        .setName("Some Home")
        .setParent(city)
        .build();
    locations.addLocation(home);
    assertTrue(locations.getDirty().isDirty());
    assertTrue(somewhere.contents().contains(county));
    assertTrue(county.contents().contains(city));
    assertTrue(city.contents().contains(home));
    assertTrue(home.contents().isEmpty());
    assertNotNull(county.getId());
    assertNotNull(city.getId());
    assertNotNull(home.getId());
    // Verify indices
    assertSame(county, locations.getLocation(county.getId()));
    assertSame(city, locations.getLocation(city.getId()));
    assertSame(home, locations.getLocation(home.getId()));
    assertSame(county, Iterables.getOnlyElement(locations.getLocationsByModelName(county.getModelName())));
    assertSame(city, Iterables.getOnlyElement(locations.getLocationsByModelName(city.getModelName())));
    assertSame(home, Iterables.getOnlyElement(locations.getLocationsByModelName(home.getModelName())));
  }
}
