/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.scythebill.birdlist.model.taxa.TaxonomyImpl;

@RunWith(JUnit4.class)
public class LocationsTest {
  private LocationSet locations;
  private PredefinedLocations predefinedLocations;

  @Before
  public void startUp() {
    locations = ReportSets.newReportSet(new TaxonomyImpl()).getLocations();
    predefinedLocations = PredefinedLocations.loadAndParse();
  }
  
  @Test
  public void getLocationByCodePossiblyCreating() {
    Location california = Locations.getLocationByCodePossiblyCreating(
        locations, predefinedLocations, "US-CA");
    assertEquals("California", california.getModelName());
    assertTrue(california.isBuiltInLocation());
    assertNotNull(california.getId());
    
    Location sanFrancisco = Locations.getLocationByCodePossiblyCreating(
        locations, predefinedLocations, "US-CA-075");
    assertEquals("San Francisco", sanFrancisco.getModelName());
    assertTrue(sanFrancisco.isBuiltInLocation());
    assertNull(sanFrancisco.getId());
    assertSame(california, sanFrancisco.getParent());
    
    Location madeira = Locations.getLocationByCodePossiblyCreating(
        locations, predefinedLocations, "PT-30");
    assertEquals("Madeira", madeira.getModelName());

    Location galapagos = Locations.getLocationByCodePossiblyCreating(
        locations, predefinedLocations, "EC-W");
    assertEquals("Galapagos Islands", galapagos.getModelName());

    Location scotland = Locations.getLocationByCodePossiblyCreating(
        locations, predefinedLocations, "GB-SCT");
    assertEquals("Scotland", scotland.getModelName());

    Location stirling = Locations.getLocationByCodePossiblyCreating(
        locations, predefinedLocations, "GB-SCT-STG");
    assertEquals("Stirling", stirling.getModelName());
    assertSame(scotland, stirling.getParent());
    
    Location uk = Locations.getLocationByCodePossiblyCreating(locations, predefinedLocations, "GB");
    assertSame(uk, scotland.getParent());
    
    assertNull(Locations.getLocationByCodePossiblyCreating(
        locations, predefinedLocations, "US-CA-999"));
    assertNull(Locations.getLocationByCodePossiblyCreating(
        locations, predefinedLocations, "US-CA-75"));
    assertNull(Locations.getLocationByCodePossiblyCreating(
        locations, predefinedLocations, "GB-FOO"));
    assertNull(Locations.getLocationByCodePossiblyCreating(
        locations, predefinedLocations, "ZZ"));
  }
}
