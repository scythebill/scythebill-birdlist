/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multisets;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocationImpl;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;

/**
 * Tests {@link PredefinedLocations}, in particular verifying that the state(etc.) list
 * has sensible content that can be properly linked up to the predefined countries. 
 */
@RunWith(JUnit4.class)
public class PredefinedLocationsTest {
  private PredefinedLocations predefinedLocations;
  private LocationSet locations;

  @Before
  public void initialize() throws Exception {
    predefinedLocations = PredefinedLocations.loadAndParse();
    locations = ReportSets.newReportSet(new TaxonomyImpl()).getLocations();
  }

  @Test
  public void verifyAllCountriesHaveSomeStates() {
    Multimap<String, Location> countries = HashMultimap.create();
    // United States is handled separately in Scythebill.
    // Canary Islands has "exceptional" ISO-3166-1 status!
    Set<String> countriesWithoutStates = ImmutableSet.of(
        "United States",
        "Canary Islands",
        "Azores",
        "Galapagos Islands",
        "Madeira",
        "Montserrat",
        "Guadeloupe",
        "Anguilla",
        "Falkland Islands",
        "Faroe Islands",
        "Palestinian Territory",
        "Cocos (Keeling) Islands",
        "Greenland",
        "Bouvet Island",
        "Svalbard",
        "Guernsey",
        "Jersey",
        "Isle of Man",
        "Mayotte",
        "Réunion",
        "Western Sahara",
        "Gibraltar",
        "Monaco",
        "Turks and Caicos Islands",
        "British Virgin Islands",
        "Aruba",
        "Martinique",
        "Bermuda",
        "Antarctica",
        "Singapore",
        "French Guiana",
        "Saint Martin (French part)",
        "Saint Barthélemy",
        "American Samoa",
        "Hong Kong",
        "Macau",
        "Christmas Island",
        "Vatican City",
        "Clipperton Island",
        "Pitcairn Islands",
        "Cook Islands",
        "Wallis and Futuna",
        "Niue",
        "Tokelau",
        "Guam",
        "Socotra",
        "High Seas",
        "San Andrés, Providencia y Santa Catalina",
        "Coral Sea Islands",
        "Easter Island",
        "Norfolk Island",
        "New Caledonia",
        "Heard Island and McDonald Islands",
        "Prince Edward and Marion Islands",
        "French Southern and Antarctic Lands",
        "South Georgia and South Sandwich Islands",
        "British Indian Ocean Territory",
        "Juan Fernandez Islands",
        "Ashmore and Cartier Islands",
        "Macquarie Island",
        "Kosovo");
    Set<String> countryCodesThatDontMatch =
        ImmutableSet.of("GG", "IM", "JE", "ES-CE", "ES-ML", "SX", "CW");
    gatherCountries(locations.rootLocations(), countries,
        ImmutableSet.of(Location.Type.country), true);
    for (Location country : countries.values()) {
      if (countriesWithoutStates.contains(country.getModelName())) {
        continue;
      }
      
      Collection<PredefinedLocation> collection =
          predefinedLocations.getPredefinedLocations(country);
      Assert.assertFalse("Country " + country + " has no states",  collection.isEmpty());
      
      // And make sure that we don't have any countries where the location's code doesn't
      // match the states code.
      for (PredefinedLocation predefinedLocation : collection) {
        if (!countryCodesThatDontMatch.contains(predefinedLocation.getCode())) {
          Assert.assertTrue(
              String.format("Country (%s) and state (%s) codes don't match",
                  country.getEbirdCode(), predefinedLocation.getCode()),
              predefinedLocation.getCode().startsWith(country.getEbirdCode()));
        }
      }
    }
  }

  @Test
  public void verifyAllCountriesShareStatePrefix() {
    // A few top-level countries reside in other countries.  Exclude those here. 
    ImmutableSet<String> topLevelCountriesInCountries = ImmutableSet.of(
        // Guernsey, Jersey, Isle of Man
        "GG", "JE", "IM",
        // Sint Maarten, Curaçao
        "SX", "CW"
        );
    for (String country : predefinedLocations.countries()) {
      ImmutableCollection<PredefinedLocationImpl> collection =
          predefinedLocations.getNamesAndIdsOfStates(country);
      Set<String> prefixes = Sets.newHashSet();
      for (PredefinedLocationImpl location : collection) {
        // Skip top-level countries
        if (topLevelCountriesInCountries.contains(location.getCode())) {
          continue;
        }
        
        Assert.assertTrue("No prefix in " + location.getCode(),
            location.getCode().indexOf('-') > 0);
        prefixes.add(location.getCode().substring(0, location.getCode().indexOf('-')));
      }
      
      Assert.assertTrue("Multiple prefixes " + prefixes, prefixes.size() == 1);
    }
    
  }

  /**
   * Verify that every parent in PredefinedLocations is something that is explicitly
   * in Locations (so that we don't have mispellings that make the predefined locations
   * file not line up). 
   */
  @Test
  public void verifyAllPredefinedCountriesAreAdded() {
    Multimap<String, Location> countries = HashMultimap.create();
    gatherCountries(locations.rootLocations(), countries,
        ImmutableSet.of(Location.Type.country, Location.Type.state), false);
    Set<String> names = FluentIterable.from(countries.values())
        .transform(new Function<Location, String> () {
          @Override
          public String apply(Location location) {
            return location.getModelName();
          }
        }).toSortedSet(Ordering.<String>natural());
    
    // The Ireland states are a bit of an exception - we include the counties as a
    // second level beneath the four provinces, but the provinces aren't in Locations
    // out-of-the-box.
    ImmutableSet<String> exceptions = ImmutableSet.of(
        "Connaught",
        "Leinster",
        "Munster",
        "Ulster");
    for (String country : predefinedLocations.countries()) {
      if (exceptions.contains(country)) {
        continue;
      }
      Assert.assertTrue("Country "+ country + " is not registered",
          names.contains(country));
    }
  }

  @Test
  public void verifyAllRequiredParentsAreAccurate() {
    Multimap<String, Location> countries = HashMultimap.create();
    gatherCountries(locations.rootLocations(), countries,
        ImmutableSet.of(Location.Type.country, Location.Type.state), false);
    for (String country : predefinedLocations.countries()) {
      ImmutableCollection<PredefinedLocationImpl> collection =
          predefinedLocations.getNamesAndIdsOfStates(country);
      for (PredefinedLocationImpl predefinedLocation : collection) {
        if (predefinedLocation.getRequiredGrandParent() == null) {
          continue;
        }
        
        // If there's a required parent, it had better be for a country that appears twice.
        Assert.assertTrue(countries.get(country).size() > 1
            // Or for the annoying case of Niger, a country that is also the name of a Nigerian state
            || "Niger".equals(country));
        
        boolean foundRequiredParent = false;
        for (Location location : countries.get(country)) {
          if (location.getParent().getModelName().equals(predefinedLocation.getRequiredGrandParent())) {
            foundRequiredParent = true;
            break;
          }
        }
        
        Assert.assertTrue("Could not find required parent for " + predefinedLocation.getName(),
            foundRequiredParent);
      }
    }
  }
  
  @Test
  public void noDuplicateNames() {
    for (Location location : locations.rootLocations()) {
      verifyNoDuplicateNamesIn(location);
    }
  }
  
  private void verifyNoDuplicateNamesIn(Location location) {
    List<String> names = Lists.newArrayList();
    for (Location child : location.contents()) {
      verifyNoDuplicateNamesIn(child);
      names.add(child.getModelName());
    }
    
    for (PredefinedLocation predefinedChild : predefinedLocations.getPredefinedLocations(location)) {
      Location existing = location.getContent(predefinedChild.getName());
      if (existing != null) {
        Assert.assertEquals(existing.getEbirdCode(), predefinedChild.getCode());
      } else {
        names.add(predefinedChild.getName());
        verifyNoDuplicateNamesIn(predefinedChild.create(locations, location));
      }
    }
    
    ImmutableSet<String> namesSet = ImmutableSet.copyOf(names);
    HashMultiset<String> namesMultiset = HashMultiset.create(names);
    Multisets.removeOccurrences(namesMultiset, HashMultiset.create(namesSet));
    if (!namesMultiset.isEmpty()) {
      throw new AssertionError(String.format("Duplicate names in %s:\n    %s",
          location.getModelName(),
          Joiner.on(",").join(namesMultiset.elementSet())));
    }
  }

  private void gatherCountries(
      Collection<Location> collection,
      Multimap<String, Location> countries,
      ImmutableSet<Location.Type> types,
      boolean requireCode) {
    for (Location location : collection) {
      if (types.contains(location.getType())
          // Sometime skip "built-in" locations that don't have an ebird code
          // (specifically, Easter Island)
          && (!requireCode || location.getEbirdCode() != null)) {
        countries.put(location.getModelName(), location);
      }
      
      gatherCountries(location.contents(), countries, types, requireCode);
    }
  }
}
