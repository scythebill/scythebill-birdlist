package com.scythebill.birdlist.model.sighting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import org.joda.time.LocalTime;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.Resources;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.io.TimeIO;
import com.scythebill.birdlist.model.sighting.VisitInfo.ObservationType;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.xml.XmlTaxonImport;

@RunWith(JUnit4.class)
public class ReportSetMutatorTest {
  private ReportSet reportSet;
  private static Taxonomy taxonomy;

  @BeforeClass
  public static void loadTaxonomy() throws Exception {
    XmlTaxonImport importer = new XmlTaxonImport();
    URL testTaxonUrl = Resources.getResource("testTaxonomy.xml");
    InputStream stream = testTaxonUrl.openStream();
    taxonomy = importer.importTaxa(new InputStreamReader(stream, "UTF-8"));
    stream.close();
  }
  
  @Before
  public void init() {
    reportSet = ReportSets.newReportSet(taxonomy);
  }
  
  @Test
  public void modifyingDate() {
    Sighting ostrich = Sighting.newBuilder()
        .setTaxonomy(taxonomy)
        .setTaxon(SightingTaxons.newSightingTaxon("spStrutcam"))
        .setLocation(reportSet.getLocations().getLocationByCode("AZ"))
        .setDate(PartialIO.fromString("2010-11-12"))
        .build();
    reportSet.addSightings(ImmutableList.of(ostrich)); 
    
    VisitInfo visitInfo = VisitInfo.builder().withObservationType(ObservationType.HISTORICAL).withComments("comment").build();
    reportSet.putVisitInfo(VisitInfoKey.forSighting(ostrich), visitInfo);
    
    Sighting newDate = ostrich.asBuilder()
        .setDate(PartialIO.fromString("2012-9-8"))
        .build();
    
    reportSet.mutator()
        .adding(ImmutableList.of(newDate))
        .removing(ImmutableList.of(ostrich))
        .withChangedDate(newDate.getDateAsPartial())
        .mutate();
    
    assertTrue(reportSet.getDirty().isDirty());
    assertEquals(ImmutableList.of(newDate), reportSet.getSightings());
    // VisitInfo should have moved to the new date
    assertEquals(ImmutableMap.of(VisitInfoKey.forSighting(newDate), visitInfo),
        reportSet.visitInfos());
  }

  @Test
  public void modifyingDateWithSightingsStillPresent() {
    Sighting ostrich = Sighting.newBuilder()
        .setTaxonomy(taxonomy)
        .setTaxon(SightingTaxons.newSightingTaxon("spStrutcam"))
        .setLocation(reportSet.getLocations().getLocationByCode("AZ"))
        .setDate(PartialIO.fromString("2010-11-12"))
        .build();
    Sighting rhea = ostrich.asBuilder()
        .setTaxon(SightingTaxons.newSightingTaxon("spRheaame"))
        .build();
    // Add two sightings at the same key
    reportSet.addSightings(ImmutableList.of(ostrich, rhea)); 
    
    VisitInfo visitInfo = VisitInfo.builder().withObservationType(ObservationType.HISTORICAL).withComments("comment").build();
    reportSet.putVisitInfo(VisitInfoKey.forSighting(ostrich), visitInfo);
    
    Sighting newDate = ostrich.asBuilder()
        .setDate(PartialIO.fromString("2012-9-8"))
        .build();
    
    // Again, replace ostrich with a new sighting
    reportSet.mutator()
        .adding(ImmutableList.of(newDate))
        .removing(ImmutableList.of(ostrich))
        .withChangedDate(newDate.getDateAsPartial())
        .mutate();
    
    assertTrue(reportSet.getDirty().isDirty());
    assertEquals(ImmutableList.of(rhea, newDate), reportSet.getSightings());
    // VisitInfo should have stayed at the old date
    assertEquals(ImmutableMap.of(VisitInfoKey.forSighting(rhea), visitInfo),
        reportSet.visitInfos());
    
    // Now replace Rhea with the new date
    Sighting newRhea = rhea.asBuilder()
        .setDate(newDate.getDateAsPartial())
        .build();
    reportSet.mutator()
        .adding(ImmutableList.of(newRhea))
        .removing(ImmutableList.of(rhea))
        .withChangedDate(newDate.getDateAsPartial())
        .mutate();
    
    // As the last one, the visit info will remain
    assertEquals(ImmutableMap.of(VisitInfoKey.forSighting(newRhea), visitInfo),
        reportSet.visitInfos());
  }

  @Test
  public void visitInfoAlreadyPresent() {
    Sighting ostrich = Sighting.newBuilder()
        .setTaxonomy(taxonomy)
        .setTaxon(SightingTaxons.newSightingTaxon("spStrutcam"))
        .setLocation(reportSet.getLocations().getLocationByCode("AZ"))
        .setDate(PartialIO.fromString("2010-11-12"))
        .build();
    Sighting rhea = ostrich.asBuilder()
        .setTaxon(SightingTaxons.newSightingTaxon("spRheaame"))
        .setDate(PartialIO.fromString("2011-11-12"))
        .build();
    // Add two sightings at different keys
    reportSet.addSightings(ImmutableList.of(ostrich, rhea)); 
    
    VisitInfo ostrichVisitInfo = VisitInfo.builder()
        .withObservationType(ObservationType.HISTORICAL).withComments("ostrich").build();
    reportSet.putVisitInfo(VisitInfoKey.forSighting(ostrich), ostrichVisitInfo);
    VisitInfo rheaVisitInfo = VisitInfo.builder()
        .withObservationType(ObservationType.HISTORICAL).withComments("rhea").build();
    reportSet.putVisitInfo(VisitInfoKey.forSighting(rhea), rheaVisitInfo);
    
    // Move the ostrich to the rhea's date
    Sighting newDate = ostrich.asBuilder()
        .setDate(rhea.getDateAsPartial())
        .build();
    reportSet.mutator()
        .adding(ImmutableList.of(newDate))
        .removing(ImmutableList.of(ostrich))
        .withChangedDate(newDate.getDateAsPartial())
        .mutate();
    
    // Both sightings are present
    assertEquals(ImmutableList.of(rhea, newDate), reportSet.getSightings());
    // ... and the visit info is merged.
    VisitInfo mergedVisitInfo = rheaVisitInfo.asBuilder()
        .withComments(rheaVisitInfo.comments().get() + "\n" + ostrichVisitInfo.comments().get())
        .build();
    assertEquals(ImmutableMap.of(VisitInfoKey.forSighting(rhea), mergedVisitInfo),
        reportSet.visitInfos());
  }

  @Test
  public void removingLast() {
    Sighting ostrich = Sighting.newBuilder()
        .setTaxonomy(taxonomy)
        .setTaxon(SightingTaxons.newSightingTaxon("spStrutcam"))
        .setLocation(reportSet.getLocations().getLocationByCode("AZ"))
        .setDate(PartialIO.fromString("2010-11-12"))
        .build();
    reportSet.addSightings(ImmutableList.of(ostrich)); 
    
    VisitInfo visitInfo = VisitInfo.builder().withObservationType(ObservationType.HISTORICAL).withComments("comment").build();
    reportSet.putVisitInfo(VisitInfoKey.forSighting(ostrich), visitInfo);
    
    reportSet.mutator()
        .removing(ImmutableList.of(ostrich))
        .mutate();
    
    assertTrue(reportSet.getDirty().isDirty());
    assertTrue(reportSet.getSightings().isEmpty());
    assertTrue(reportSet.visitInfos().isEmpty());
  }

  @Test
  public void modifyingLocation() {
    Sighting ostrich = Sighting.newBuilder()
        .setTaxonomy(taxonomy)
        .setTaxon(SightingTaxons.newSightingTaxon("spStrutcam"))
        .setLocation(reportSet.getLocations().getLocationByCode("AZ"))
        .setDate(PartialIO.fromString("2010-11-12"))
        .build();
    reportSet.addSightings(ImmutableList.of(ostrich)); 
    
    VisitInfo visitInfo = VisitInfo.builder().withObservationType(ObservationType.HISTORICAL).withComments("comment").build();
    reportSet.putVisitInfo(VisitInfoKey.forSighting(ostrich), visitInfo);
    
    Sighting newLocation = ostrich.asBuilder()
        .setLocation(reportSet.getLocations().getLocationByCode("BE"))
        .build();
    
    reportSet.mutator()
        .adding(ImmutableList.of(newLocation))
        .removing(ImmutableList.of(ostrich))
        .withChangedLocation(newLocation.getLocationId())
        .mutate();
    
    // VisitInfo should have moved to the new date
    assertEquals(ImmutableMap.of(VisitInfoKey.forSighting(newLocation), visitInfo),
        reportSet.visitInfos());
  }

  @Test
  public void addingStartTime() {
    Sighting ostrich = Sighting.newBuilder()
        .setTaxonomy(taxonomy)
        .setTaxon(SightingTaxons.newSightingTaxon("spStrutcam"))
        .setLocation(reportSet.getLocations().getLocationByCode("AZ"))
        .setDate(PartialIO.fromString("2010-11-12"))
        .build();
    reportSet.addSightings(ImmutableList.of(ostrich)); 
    
    VisitInfo visitInfo = VisitInfo.builder().withObservationType(ObservationType.HISTORICAL).withComments("comment").build();
    reportSet.putVisitInfo(VisitInfoKey.forSighting(ostrich), visitInfo);
    
    Sighting withTime = ostrich.asBuilder()
        .setTime(TimeIO.fromString("06:30"))
        .build();
    
    reportSet.mutator()
        .adding(ImmutableList.of(withTime))
        .removing(ImmutableList.of(ostrich))
        .withChangedStartTime(Optional.of(withTime.getTimeAsPartial()))
        .mutate();
    
    // VisitInfo should have moved to the new date
    assertEquals(ImmutableMap.of(VisitInfoKey.forSighting(withTime), visitInfo),
        reportSet.visitInfos());
  }

  @Test
  public void removingStartTime() {
    Sighting ostrich = Sighting.newBuilder()
        .setTaxonomy(taxonomy)
        .setTaxon(SightingTaxons.newSightingTaxon("spStrutcam"))
        .setLocation(reportSet.getLocations().getLocationByCode("AZ"))
        .setDate(PartialIO.fromString("2010-11-12"))
        .setTime(TimeIO.fromString("06:30"))
        .build();
    reportSet.addSightings(ImmutableList.of(ostrich)); 
    
    VisitInfo visitInfo = VisitInfo.builder().withObservationType(ObservationType.HISTORICAL).withComments("comment").build();
    reportSet.putVisitInfo(VisitInfoKey.forSighting(ostrich), visitInfo);
    
    Sighting withoutTime = ostrich.asBuilder()
        .setTime(null)
        .build();
    
    reportSet.mutator()
        .adding(ImmutableList.of(withoutTime))
        .removing(ImmutableList.of(ostrich))
        .withChangedStartTime(Optional.<LocalTime>absent())
        .mutate();
    
    // VisitInfo should have moved to the new date
    assertEquals(ImmutableMap.of(VisitInfoKey.forSighting(withoutTime), visitInfo),
        reportSet.visitInfos());
  }
}
