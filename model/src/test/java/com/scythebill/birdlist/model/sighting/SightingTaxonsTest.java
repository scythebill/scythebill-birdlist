/*   
 * Copyright 2012 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.sighting;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import junit.framework.TestCase;

import com.google.common.collect.ImmutableList;
import com.google.common.io.Resources;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.xml.XmlTaxonImport;

/**
 * Tests of {@link SightingTaxons}.
 */
public class SightingTaxonsTest extends TestCase {
  private Taxonomy taxonomy;

  @Override
  protected void setUp() throws Exception {
    XmlTaxonImport importer = new XmlTaxonImport();
    URL testTaxonUrl = Resources.getResource("testTaxonomy.xml");
    InputStream stream = testTaxonUrl.openStream();
    taxonomy = importer.importTaxa(new InputStreamReader(stream, "UTF-8"));
    stream.close();
  }

  public void testSpSightingTaxon() {
    SightingTaxon sightingTaxon = SightingTaxons.newSpTaxon(
        ImmutableList.of("spRheaame", "spRheapen"));
    assertEquals(SightingTaxon.Type.SP, sightingTaxon.getType());
    try {
      sightingTaxon.getId();
      fail();
    } catch (Exception e) {
    }
    
    assertTrue(sightingTaxon.shouldBeDisplayedWith("spRheaame"));
    assertTrue(sightingTaxon.shouldBeDisplayedWith("spRheapen"));
    assertFalse(sightingTaxon.shouldBeDisplayedWith("grRheapentar"));
    assertFalse(sightingTaxon.shouldBeDisplayedWith("spStrutcam"));
    
    Resolved resolved = sightingTaxon.resolve(taxonomy);
    assertEquals("Greater/Lesser Rhea", resolved.getCommonName());
    assertEquals("Greater/Lesser Rhea", resolved.getSimpleCommonName());
    assertEquals("americana/pennata", resolved.getName());
    assertEquals("Rhea americana/pennata", resolved.getFullName());
    assertEquals(Taxon.Type.species, resolved.getLargestTaxonType());
    assertEquals(Taxon.Type.species, resolved.getSmallestTaxonType());
    assertEquals(Species.Status.NT, resolved.getTaxonStatus());
    assertEquals(sightingTaxon, resolved.getParentOfAtLeastType(Taxon.Type.group));
    assertEquals(sightingTaxon, resolved.getParentOfAtLeastType(Taxon.Type.species));
    assertEquals("geRhea", resolved.getParentOfAtLeastType(Taxon.Type.genus).getId());
    assertTrue(resolved.isChildOf(taxonomy.getTaxon("geRhea")));
    assertTrue(resolved.isChildOf(taxonomy.getTaxon("famRhe")));
    assertFalse(resolved.isChildOf(taxonomy.getTaxon("famStr")));    
  }

  public void testSpSightingTaxonMultipleLevels() {
    SightingTaxon sightingTaxon = SightingTaxons.newSpTaxon(
        ImmutableList.of("grStrutcammol", "sspStrutcam[caaus"));
    assertEquals(SightingTaxon.Type.SP, sightingTaxon.getType());
    try {
      sightingTaxon.getId();
      fail();
    } catch (Exception e) {
    }
    
    Resolved resolved = sightingTaxon.resolve(taxonomy);
    assertEquals(Taxon.Type.group, resolved.getLargestTaxonType());
    assertEquals(Taxon.Type.subspecies, resolved.getSmallestTaxonType());
    assertEquals(
        SightingTaxons.newSpTaxon(ImmutableList.of("grStrutcam[ca", "grStrutcammol")),
        resolved.getParentOfAtLeastType(Taxon.Type.group));
    assertEquals(
        SightingTaxons.newSightingTaxon("spStrutcam"),
        resolved.getParentOfAtLeastType(Taxon.Type.species));
    assertEquals(
        SightingTaxons.newSightingTaxon("spStrutcam"),
        resolved.getParent());
    assertEquals("spStrutcam", resolved.getParentOfAtLeastType(Taxon.Type.species).getId());
    assertEquals("geStrut", resolved.getParentOfAtLeastType(Taxon.Type.genus).getId());
    assertTrue(resolved.isChildOf(taxonomy.getTaxon("spStrutcam")));
    assertTrue(resolved.isChildOf(taxonomy.getTaxon("famStr")));
    assertFalse(resolved.isChildOf(taxonomy.getTaxon("famRhe")));    
    assertFalse(resolved.isChildOf(taxonomy.getTaxon("grStrutcam[ca")));    
  }
}
