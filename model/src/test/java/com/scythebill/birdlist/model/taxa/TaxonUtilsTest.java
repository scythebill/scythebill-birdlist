/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.taxa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.io.Resources;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.xml.XmlTaxonImport;

public class TaxonUtilsTest {
  private static Taxonomy taxonomy;

  @BeforeClass
  public static void loadTaxonomy() throws Exception {
    XmlTaxonImport importer = new XmlTaxonImport();
    URL testTaxonUrl = Resources.getResource("testTaxonomy.xml");
    InputStream stream = testTaxonUrl.openStream();
    taxonomy = importer.importTaxa(new InputStreamReader(stream, "UTF-8"));
    stream.close();
  }

  /**
   * Test of sortedInsert method, of class TaxonUtils.
   */
  @Test
  public void testSortedInsert() {
    Resolved genus = SightingTaxons.newSightingTaxon("geRhea").resolve(taxonomy);
    Resolved greater = SightingTaxons.newSightingTaxon("spRheaame").resolve(taxonomy);
    Resolved lesser = SightingTaxons.newSightingTaxon("spRheapen").resolve(taxonomy);
    Resolved lesserGroup = SightingTaxons.newSightingTaxon("grRheapenpen").resolve(taxonomy);
    Resolved greaterLesserSp = SightingTaxons.newSpTaxon(
            ImmutableSet.of("spRheaame", "spRheapen")).resolve(taxonomy);

    List<Resolved> list = Lists.newArrayList();
    assertEquals(0, TaxonUtils.sortedInsert(list, greater));
    assertEquals(1, TaxonUtils.sortedInsert(list, lesserGroup));
    assertEquals(1, TaxonUtils.sortedInsert(list, lesser));
    assertEquals(2, TaxonUtils.sortedInsert(list, greaterLesserSp));
    
    // TODO: greaterLesserSp should come after lesserGroup too.  Not a big deal, given how
    // this function is used (just data entry).  Ordering is correct in generated reports. 
    assertEquals(Lists.newArrayList(greater, lesser, greaterLesserSp, lesserGroup), list);

    assertEquals(0, TaxonUtils.sortedInsert(list, genus));
    assertEquals(Lists.newArrayList(genus, greater, lesser, greaterLesserSp, lesserGroup), list);

    assertEquals(-1, TaxonUtils.sortedInsert(list, greater));
    assertEquals(-1, TaxonUtils.sortedInsert(list, lesser));
    assertEquals(-1, TaxonUtils.sortedInsert(list, lesserGroup));
    assertEquals(-1, TaxonUtils.sortedInsert(list, greaterLesserSp));

    list = Lists.newArrayList();
    assertEquals(0, TaxonUtils.sortedInsert(list, lesserGroup));
    assertEquals(0, TaxonUtils.sortedInsert(list, lesser));
    assertEquals(0, TaxonUtils.sortedInsert(list, greater));
    assertEquals(Lists.newArrayList(greater, lesser, lesserGroup), list);
  }
 
  @Test
  public void previousSibling() {
    Taxon taxon = taxonomy.getTaxon("sspStrutcam[casyr");
    Taxon previous = TaxonUtils.getPreviousSibling(taxon);
    assertEquals("sspStrutcam[cacam", previous.getId());
    assertNull(TaxonUtils.getPreviousSibling(previous));
  }
  
  @Test
  public void nextSibling() {
    Taxon taxon = taxonomy.getTaxon("sspStrutcam[camas");
    Taxon next = TaxonUtils.getNextSibling(taxon);
    assertEquals("sspStrutcam[caaus", next.getId());
    assertNull(TaxonUtils.getNextSibling(next));
  }
}
