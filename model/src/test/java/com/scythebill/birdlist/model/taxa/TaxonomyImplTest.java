/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.taxa;

import junit.framework.TestCase;

public class TaxonomyImplTest extends TestCase {
  private TaxonomyImpl taxonomy;
  private Taxon root;
  private TaxonImpl genus;
  private TaxonImpl species;
  private TaxonImpl subspecies;
  
  public TaxonomyImplTest(String testName) {
    super(testName);
  }

  @Override
  protected void setUp() throws Exception {
    super.setUp();

    taxonomy = new TaxonomyImpl();
    root = new TaxonImpl(Taxon.Type.classTaxon, taxonomy);
    root.built();
    taxonomy.setRoot(root);
    
    genus = new TaxonImpl(Taxon.Type.genus, root);
    genus.setName("Turdus");
    genus.built();
    root.getContents().add(genus);
    
    species = new TaxonImpl(Taxon.Type.species, genus);
    species.setName("migratorius");
    species.built();
    genus.getContents().add(species);
    
    subspecies = new TaxonImpl(Taxon.Type.subspecies, species);
    subspecies.setName("migratorius");
    subspecies.built();
    species.getContents().add(subspecies);
  }

  /**
   * Test of setRoot method, of class TaxonomyImpl.
   */
  public void testSetRoot() {
    Taxon taxon = new TaxonImpl(Taxon.Type.classTaxon, taxonomy);

    try {
      taxonomy.setRoot(taxon);
      fail();
    } catch (IllegalStateException e) {
      // OK
    }
  }

  /**
   * Test of getRoot method, of class TaxonomyImpl.
   */
  public void testGetRoot() {
    assertSame(root, taxonomy.getRoot());
  }

  /**
   * Test of findSpecies method, of class TaxonomyImpl.
   */
  public void testFindSpecies() {
    assertSame(species, taxonomy.findSpecies("Turdus migratorius"));
    assertNull(taxonomy.findSpecies("Turdus turdus"));
    assertNull(taxonomy.findSpecies("Not real"));
    try {
      taxonomy.findSpecies("NoSpace");
      fail();
    } catch (IllegalArgumentException t) {
      // OK
    }
  }

  /**
   * Test of getId method, of class TaxonomyImpl.
   */
  public void testGetId() {
    assertNotNull(taxonomy.getId(root));
    assertNotNull(taxonomy.getId(genus));
    assertNotNull(taxonomy.getId(species));
    assertNotNull(taxonomy.getId(subspecies));
    
    assertEquals(taxonomy.getId(species), species.getId());
  }

  /**
   * Test of getTaxon method, of class TaxonomyImpl.
   */
  public void testGetTaxon() {
    assertSame(root, taxonomy.getTaxon(taxonomy.getId(root)));
    assertSame(genus, taxonomy.getTaxon(taxonomy.getId(genus)));
    assertSame(species, taxonomy.getTaxon(taxonomy.getId(species)));
    assertSame(subspecies, taxonomy.getTaxon(taxonomy.getId(subspecies)));
  }

  /**
   * Test of register method, of class TaxonomyImpl.
   */
  public void testRegister() {
    TaxonImpl blackbird = new TaxonImpl(Taxon.Type.species, genus);
    blackbird.setName("merula");
    taxonomy.register(blackbird, "CustomId");
    
    assertEquals("CustomId", blackbird.getId());
    
    try {
      taxonomy.register(blackbird, "AnotherId");
      fail();
    } catch (IllegalArgumentException iae) {
      assertEquals("CustomId", blackbird.getId());
    }
    
    TaxonImpl taita = new TaxonImpl(Taxon.Type.species, genus);
    taita.setName("helleri");
    try {
      taxonomy.register(taita, "CustomId");
      fail();
    } catch (IllegalArgumentException iae) {
      assertNull(taxonomy.getId(taita));
    }
    
    try {
      taxonomy.register(taita, "foo,bar");
      fail();
    } catch (IllegalArgumentException iae) {
    }

    try {
      taxonomy.register(taita, "foo|bar");
      fail();
    } catch (IllegalArgumentException iae) {
    }
}

  /**
   * Test of registerWithNewId method, of class TaxonomyImpl.
   */
  public void testRegisterWithNewId() {
    TaxonImpl blackbird = new TaxonImpl(Taxon.Type.species, genus);
    blackbird.setName("merula");
    assertNull(blackbird.getId());
    taxonomy.registerWithNewId(blackbird);
    assertNotNull(blackbird.getId());
    assertSame(blackbird, taxonomy.getTaxon(blackbird.getId()));
  }

  /**
   * Test of unregister method, of class TaxonomyImpl.
   */
  public void testUnregister() {
    String id = species.getId();
    taxonomy.unregister(species);
    assertNull(species.getId());
    assertNull(taxonomy.getTaxon(id));
  }
}
