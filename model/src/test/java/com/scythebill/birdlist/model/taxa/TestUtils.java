/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.taxa;

class TestUtils {
  public static Taxon addFamily(String name, Taxon root) {
    TaxonImpl family = new TaxonImpl(Taxon.Type.family, root);
    family.setName(name);
    family.built();
    root.getContents().add(family);
    return family;
  }

  public static Taxon addGenus(String name, Taxon family) {
    TaxonImpl genus = new TaxonImpl(Taxon.Type.genus, family);
    genus.setName(name);
    genus.built();
    family.getContents().add(genus);
    return genus;
  }

  public static Taxon addSpecies(String name, Taxon genus) {
    TaxonImpl species = new TaxonImpl(Taxon.Type.species, genus);
    species.setName(name);
    species.built();
    genus.getContents().add(species);
    return species;
  }

}
