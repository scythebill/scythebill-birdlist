/*
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.scythebill.birdlist.model.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Test;

public class UserSetTest {
  private final UserSet userSet = new UserSet();

  @Test
  public void addTwoUsersWithTheSameStartingLettersInName() {
    User.Builder aw1 = userSet.newUserBuilder().setName("Adam Winer");
    User user1 = userSet.addUser(aw1);
    assertEquals("adwi", user1.id());

    User.Builder aw2 = userSet.newUserBuilder().setName("Adrian Winer");
    User user2 = userSet.addUser(aw2);
    assertEquals("adwi1", user2.id());

    assertSame(user1, userSet.userById(user1.id()));
    assertSame(user2, userSet.userById(user2.id()));
  }

  @Test
  public void usersRetrievableByAbbreviation() {
    User.Builder aw = userSet.newUserBuilder().setAbbreviation("AW");
    User user1 = userSet.addUser(aw);
    assertEquals("aw", user1.id());

    User.Builder hw = userSet.newUserBuilder().setAbbreviation("HW");
    User user2 = userSet.addUser(hw);
    assertEquals("hw", user2.id());

    assertSame(user1, userSet.userByAbbreviation("AW"));
    assertSame(user2, userSet.userByAbbreviation("HW"));
  }

  @Test
  public void changingAbbreviation() {
    User.Builder aw = userSet.newUserBuilder().setAbbreviation("AW");
    User user1 = userSet.addUser(aw);
    assertSame(user1, userSet.userByAbbreviation("AW"));

    User changed = userSet.addUser(user1.asBuilder().setAbbreviation("WA"));

    // Can be found by new abbreviation
    assertSame(changed, userSet.userByAbbreviation("WA"));
    assertEquals("WA", changed.abbreviation());
    // Name still not set
    assertNull(changed.name());
    try {
      // Cannot be found by old abbreviation
      userSet.userByAbbreviation("AW");
      fail("Expected exception getting missing abbreviation");
    } catch (IllegalArgumentException e) {
    }
  }

  @Test
  public void changingName() {
    User.Builder aw = userSet.newUserBuilder().setName("Adam Winer").setAbbreviation("AW");
    User user1 = userSet.addUser(aw);

    User changed = userSet.addUser(user1.asBuilder().setName("Not Adam Winer"));
    // ID is still the same
    assertEquals(user1.id(), changed.id());
    // Abbreviation and name are set appropriately
    assertEquals("Not Adam Winer", changed.name());
    assertEquals("AW", changed.abbreviation());

    // Still find-able by ID and abbreviation
    assertSame(changed, userSet.userById(changed.id()));
    assertSame(changed, userSet.userByAbbreviation("AW"));
  }

  @Test
  public void duplicateAbbreviationsNotAllowed() {
    User.Builder aw = userSet.newUserBuilder().setAbbreviation("AW");
    User user1 = userSet.addUser(aw);
    assertSame(user1, userSet.userByAbbreviation("AW"));

    try {
      userSet.addUser(userSet.newUserBuilder().setAbbreviation("AW"));
      fail();
    } catch (IllegalStateException e) {
    }


    // Original user is still available
    assertSame(user1, userSet.userById(user1.id()));
    assertSame(user1, userSet.userByAbbreviation("AW"));
  }


  @Test
  public void commasAreNotInIds() {
    User.Builder aw1 = userSet.newUserBuilder().setName(",,");
    User user1 = userSet.addUser(aw1);
    Assert.assertFalse(user1.id().contains(","));
  }
}
