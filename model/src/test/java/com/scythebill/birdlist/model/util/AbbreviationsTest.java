/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Optional;
import java.util.Set;

import org.junit.Test;

import com.google.common.collect.ImmutableSet;
import com.scythebill.birdlist.model.util.Abbreviations.AbbreviationConfig;

public class AbbreviationsTest {

  @Test
  public void usesFullAllowedWordsPerCharacter() {
    Set<String> reserved = ImmutableSet.of("A", "Ad");
    AbbreviationConfig config = new AbbreviationConfig();
    config.maxPerWordChars = 3;
    Optional<String> abbreviation = Abbreviations.abbreviate("Adam", reserved::contains, config);
    assertTrue(abbreviation.isPresent());
    assertEquals("Ada", abbreviation.get());
  }

}
