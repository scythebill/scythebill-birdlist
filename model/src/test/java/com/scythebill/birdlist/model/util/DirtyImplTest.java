/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.util;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import junit.framework.TestCase;

public class DirtyImplTest extends TestCase {

  public void testDirtyImpl() {
    DirtyImpl dirty = new DirtyImpl(false);
    // TODO: add EasyMock dependency
    TestPropertyChangeListener listener = new TestPropertyChangeListener();
    dirty.addDirtyListener(listener);
    
    dirty.setDirty(true);
    
    assertTrue(listener.called);
    assertEquals(Boolean.TRUE, listener.newValue);
    assertEquals(Boolean.FALSE, listener.oldValue);
  }

  public void testDirtyImpl_sameValue() {
    DirtyImpl dirty = new DirtyImpl(false);
    // TODO: add EasyMock dependency
    TestPropertyChangeListener listener = new TestPropertyChangeListener();
    dirty.addDirtyListener(listener);
    
    dirty.setDirty(false);
    
    assertFalse(listener.called);
  }

  public static class TestPropertyChangeListener implements PropertyChangeListener {
    
    boolean called;
    Object oldValue;
    Object newValue;

    @Override
    public void propertyChange(PropertyChangeEvent event) {
      called = true;
      oldValue = event.getOldValue();
      newValue = event.getNewValue();
    }
    
  }
}
