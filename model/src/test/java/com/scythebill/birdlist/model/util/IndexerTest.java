/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.util;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.ImmutableList;
import com.google.common.io.Resources;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.xml.XmlTaxonImport;

public class IndexerTest {

  private static Indexer<String> commonIndexer;
  private static Indexer<String> sciIndexer;
  private static Taxonomy taxonomy;

  @BeforeClass
  public static void setUp() throws Exception {
    XmlTaxonImport importer = new XmlTaxonImport();
    URL testTaxonUrl = Resources.getResource("fullTaxonomy.xml");
    InputStream stream = testTaxonUrl.openStream();
    taxonomy = importer.importTaxa(new InputStreamReader(stream, "UTF-8"));
    stream.close();
    
    commonIndexer = new Indexer<String>();
    sciIndexer = new Indexer<String>();
    addToIndex(taxonomy, commonIndexer, sciIndexer, taxonomy.getRoot());
  }

  @Test  
  public void testFind() {
    commonIndexer.find("wilwa");

    assertEquals(ImmutableList.of(
            "Willow Warbler",
            "Wilson's Warbler",
            "Willie-wagtail"
            ),
        ImmutableList.copyOf(commonIndexer.find("wilwa")));

    assertEquals(ImmutableList.of(
            "Willow Warbler",
            "Wilson's Warbler",
            "Willie-wagtail"
            ),
        ImmutableList.copyOf(commonIndexer.find("WilWa")));

    assertEquals(ImmutableList.of(
            "Wilson's Storm-Petrel",
            "Wilson's Plover",
            "Wilson's Snipe",
            "Wilson's Phalarope",
            "Wilson's Bird-of-paradise",
            "Wilson's Warbler",
            "Williamson's Sapsucker",
            "Willard's Sooty Boubou"),
        ImmutableList.copyOf(commonIndexer.find("wils"))); 
    
    assertEquals(ImmutableList.of(
        "Wilson's Storm-Petrel",
        "Wilson's Plover",
        "Wilson's Snipe",
        "Wilson's Phalarope",
        "Wilson's Bird-of-paradise",
        "Wilson's Warbler"),
      ImmutableList.copyOf(commonIndexer.find("wilsons"))); 

    assertEquals(ImmutableList.of(
        "Wilson's Storm-Petrel",
        "Wilson's Snipe",
        "Williamson's Sapsucker",
        "Willard's Sooty Boubou"),
        ImmutableList.copyOf(commonIndexer.find("wil s")));

    assertEquals("Number of matches for 'h'", 688,
        sciIndexer.find("h").size());
    
    assertEquals(ImmutableList.of("Black Skimmer"),
        ImmutableList.copyOf(commonIndexer.find("blskim")));
  }

  @Test  
  public void testFindPerformance() {
    long startTime = System.nanoTime();
    for (int i = 0; i < 100; i++) {
      commonIndexer.find("wiwa");
      sciIndexer.find("phy");
      commonIndexer.find("h");
    }
    
    long endTime = System.nanoTime();
    double elapsedSeconds = (endTime - startTime) / 1E9;
    System.out.println("Total time: " + elapsedSeconds);
//    assertTrue(endTime - startTime < 3000000000L);
  }

  @Test  
  public void testFindOfInitialCapital() {
    Indexer<String> indexer = new Indexer<String>();
    indexer.add("Jamaica", "success");
    assertEquals(1, indexer.find("J").size());
    assertEquals(1, indexer.find("j").size());
  }
  
  static private void addToIndex(Taxonomy taxonomy, Indexer<String> common,
      Indexer<String> sci, Taxon taxon) throws IOException {
    if (taxon.getType() == Taxon.Type.species) {
      String commonName = taxon.getCommonName();
      String sciName = taxon.getParent().getName() + " " + taxon.getName();

      // Ordinarily, we'd just use the ID.  But it's easier to write
      // tests this way
      common.add(commonName, commonName);
      sci.add(sciName, sciName);
    } else {
      for (Taxon child : taxon.getContents()) {
        addToIndex(taxonomy, common, sci, child);
      }
    }
  }
}
