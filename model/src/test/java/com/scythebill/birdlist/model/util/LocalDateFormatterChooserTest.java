/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.joda.time.Partial;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.truth.Truth;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.util.LocalDateFormatterChooser.ChosenFormat;

@RunWith(JUnit4.class)
public final class LocalDateFormatterChooserTest {

  @BeforeClass
  public static void setLocale() {
    // Explicitly set the locale to something *other* than en-US to test the fix for
    // https://bitbucket.org/scythebill/scythebill-birdlist/issues/534/localedateformatterchooser-assumes-locale
    Locale.setDefault(Locale.forLanguageTag("es-MX"));
  }
  
  @Test
  public void year() {
    Truth.assertThat(parseDates("2005", "98"))
        .containsExactly(PartialIO.fromString("2005"), PartialIO.fromString("1998"))
        .inOrder();
  }

  @Test
  public void monthAndYear() {
    Truth.assertThat(parseDates("Nov 2005", "March 98", "Jan 2007"))
        .containsExactly(PartialIO.fromString("2005-11"), PartialIO.fromString("1998-03"), PartialIO.fromString("2007-01"))
        .inOrder();
  }

  @Test
  public void monthAndShortYear() {
    Truth.assertThat(parseDates("Nov 05", "Jan 99"))
        .containsExactly(PartialIO.fromString("2005-11"), PartialIO.fromString("1999-01"))
        .inOrder();
  }

  @Test
  public void mixedFormats() {
    Truth.assertThat(parseDates("Nov 2005", "1999"))
        .containsExactly(PartialIO.fromString("2005-11"), PartialIO.fromString("1999"))
        .inOrder();
  }

//  @Test
//  public void nonUsMonths() {
//    Truth.assertThat(parseDates("nov. 2005", "abr. 1999"))
//        .containsExactly(PartialIO.fromString("2005-11"), PartialIO.fromString("1999-04"))
//        .inOrder();
//  }

  @Test
  public void monthAndYearWithExtraSpace() {
    Truth.assertThat(parseDates("Nov  2005", "March  98"))
        .containsExactly(PartialIO.fromString("2005-11"), PartialIO.fromString("1998-03"))
        .inOrder();
  }

  @Test
  public void monthAndYearWithNoSpace() {
    Truth.assertThat(parseDates("Nov2005", "March98"))
        .containsExactly(PartialIO.fromString("2005-11"), PartialIO.fromString("1998-03"))
        .inOrder();
  }

  private List<Partial> parseDates(String... dates) {
    List<Partial> parsed = new ArrayList<>();
    ImmutableList<ChosenFormat> formatters =
        new LocalDateFormatterChooser().chooseFormatters(ImmutableSet.copyOf(dates));
    for (String date : dates) {
      for (int i = 0; i < formatters.size(); i++) {
        ChosenFormat formatter = formatters.get(i);
        try {
          parsed.add(formatter.parse(date));
          break;
        } catch (IllegalArgumentException e) {
          // Rethrow if all formatters failed
          if (i == formatters.size() - 1) {
            throw e;
          }
        }
      }
    }
    return parsed;
  }
}