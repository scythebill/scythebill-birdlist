/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.util;

import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonImpl;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;
import junit.framework.TestCase;

public class TaxonComparatorTest extends TestCase {
  private TaxonomyImpl taxonomy;
  private Taxon root;
  private Taxon turdidae;
  private Taxon turdus;
  private Taxon robin;
  private Taxon taita;
  
  private TaxonComparator comparator;
  

  public TaxonComparatorTest(String testName) {
    super(testName);
  }

  @Override
  protected void setUp() throws Exception {
    super.setUp();

    taxonomy = new TaxonomyImpl();
    root = new TaxonImpl(Taxon.Type.classTaxon, taxonomy);
    taxonomy.setRoot(root);
    
    turdidae = addFamily("Turdidae", root);
    turdus = addGenus("Turdus", turdidae);
    robin = addSpecies("migratorius", turdus);
    taita = addSpecies("helleri", turdus);
    
    comparator = new TaxonComparator();
  }

  @Override
  protected void tearDown() throws Exception {
    super.tearDown();
  }

  /**
   * Test of compare method, of class TaxonComparator.
   */
  public void testCompare() {
    assertTrue(comparator.compare(robin, robin) == 0);
    assertTrue(comparator.compare(robin, taita) < 0);
    assertTrue(comparator.compare(taita, robin) > 0);
    assertTrue(comparator.compare(robin, turdus) > 0);
    assertTrue(comparator.compare(taita, turdus) > 0);
    assertTrue(comparator.compare(turdus, robin) < 0);
    assertTrue(comparator.compare(turdus, taita) < 0);

    assertTrue(comparator.compare(robin, turdidae) > 0);
    assertTrue(comparator.compare(robin, root) > 0);
  }

  private Taxon addFamily(String name, Taxon root) {
    TaxonImpl family = new TaxonImpl(Taxon.Type.family, root);
    family.setName(name);
    family.built();
    root.getContents().add(family);
    return family;
  }

  private Taxon addGenus(String name, Taxon family) {
    TaxonImpl genus = new TaxonImpl(Taxon.Type.genus, family);
    genus.setName(name);
    genus.built();
    family.getContents().add(genus);
    return genus;
  }

  private Taxon addSpecies(String name, Taxon genus) {
    TaxonImpl species = new TaxonImpl(Taxon.Type.species, genus);
    species.setName(name);
    species.built();
    genus.getContents().add(species);
    return species;
  }
}
