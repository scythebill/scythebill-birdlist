/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.util;

import static com.scythebill.birdlist.model.util.TaxonNames.joinFromEnd;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;

@RunWith(JUnit4.class)
public class TaxonNamesTest {

  @Test
  public void joinFromEndWithCommonSuffixes() {
    String joined = joinFromEnd(
        ImmutableList.of("Foo Sparrow", "Bar Sparrow"),
        Joiner.on('/'));
    assertEquals("Foo/Bar Sparrow", joined);
  }

  @Test
  public void joinFromEndWithNoCommonSuffixes() {
    String joined = joinFromEnd(
        ImmutableList.of("Foo Sparrow", "Bar Warbler"),
        Joiner.on('/'));
    assertEquals("Foo Sparrow/Bar Warbler", joined);
  }

  @Test
  public void joinFromEndWithFullOverlap() {
    String joined = joinFromEnd(
        ImmutableList.of("Foo Sparrow", "Greater Foo Sparrow"),
        Joiner.on('/'));
    assertEquals("Foo/Greater Foo Sparrow", joined);

    joined = joinFromEnd(
        ImmutableList.of("Greater Foo Sparrow", "Foo Sparrow"),
        Joiner.on('/'));
    assertEquals("Greater Foo/Foo Sparrow", joined);  }

  @Test
  public void joinFromEndWithSingleWord() {
    String joined = joinFromEnd(
        ImmutableList.of("Sparrow", "Greater Foo Sparrow"),
        Joiner.on('/'));
    assertEquals("Sparrow/Greater Foo Sparrow", joined);

    joined = joinFromEnd(
        ImmutableList.of("Greater Foo Sparrow", "Sparrow"),
        Joiner.on('/'));
    assertEquals("Greater Foo Sparrow/Sparrow", joined);  }
  }
