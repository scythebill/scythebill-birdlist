/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.model.util;

import java.util.Collections;

import junit.framework.TestCase;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Lists;
import com.google.common.collect.Multiset;

public class TrieTest extends TestCase {

  public void testFindMatches_emptyTrie() {
    Trie<Integer> t = new Trie<Integer>();
    assertEquals(Collections.<String>emptyList(), t.findMatches("foo"));
  }

  private Trie<Integer> createDefaultTrie() {
    Trie<Integer> t = new Trie<>();
    t.add("baby", 0);
    t.add("bachelor", 1);
    t.add("boy", 2);
    t.add("ba", 5);
    t.add("Cowboy", 3);
    return t;
  }
  
  public void testFindMatches() {
    Trie<Integer> t = createDefaultTrie();
    assertEqualsInAnyOrder(Lists.newArrayList(0, 1, 2, 5), t.findMatches("b"));
    assertEqualsInAnyOrder(Lists.newArrayList(0, 1, 5), t.findMatches("ba"));
    assertEqualsInAnyOrder(Lists.newArrayList(0), t.findMatches("bab"));
    assertEqualsInAnyOrder(Lists.newArrayList(0), t.findMatches("baby"));
    assertEqualsInAnyOrder(Lists.newArrayList(3), t.findMatches("c"));
    assertEqualsInAnyOrder(Collections.<Integer>emptyList(), t.findMatches("d"));
//  assertEqualsInAnyOrder(Lists.newArrayList(3), t.findMatches(""));
  }
  
  public void testContainsPrefixed() {
    Trie<Integer> t = createDefaultTrie();
    assertTrue(t.containsPrefixed("bar"));
    assertTrue(t.containsPrefixed("cowboyfoo"));
    assertTrue(t.containsPrefixed("cowboy-foo"));
    assertFalse(t.containsPrefixed("cowbob-foo"));
    assertFalse(t.containsPrefixed("bbo"));
  }

  public void testAdd() {
    Trie<Integer> t = createDefaultTrie();
    t.add("bachelor", 17);

    assertEqualsInAnyOrder(Lists.newArrayList(0, 1, 2, 5, 17), t.findMatches("b"));
    assertEqualsInAnyOrder(Lists.newArrayList(0, 1, 5, 17), t.findMatches("ba"));
    assertEqualsInAnyOrder(Lists.newArrayList(0), t.findMatches("baby"));
    assertEqualsInAnyOrder(Lists.newArrayList(3), t.findMatches("c"));
    assertEqualsInAnyOrder(Collections.<Integer>emptyList(), t.findMatches("d"));
  }

  public void testRemove() {
    Trie<Integer> t = createDefaultTrie();
    t.remove("baby", 0);

    assertEqualsInAnyOrder(Lists.newArrayList(1, 2, 5), t.findMatches("b"));
    assertEqualsInAnyOrder(Lists.newArrayList(1, 5), t.findMatches("ba"));
    assertEqualsInAnyOrder(Collections.<Integer>emptyList(), t.findMatches("baby"));
    assertEqualsInAnyOrder(Lists.newArrayList(3), t.findMatches("c"));
    assertEqualsInAnyOrder(Collections.<Integer>emptyList(), t.findMatches("d"));
  }

  public void testRemove_NonExistentString() {
    Trie<Integer> t = createDefaultTrie();
    t.remove("dodo", 17);

    assertEqualsInAnyOrder(Lists.newArrayList(0, 1, 2, 5), t.findMatches("b"));
    assertEqualsInAnyOrder(Lists.newArrayList(0, 1, 5), t.findMatches("ba"));
    assertEqualsInAnyOrder(Lists.newArrayList(0), t.findMatches("baby"));
    assertEqualsInAnyOrder(Lists.newArrayList(3), t.findMatches("c"));
    assertEqualsInAnyOrder(Collections.<Integer>emptyList(), t.findMatches("d"));
  }

  public void testRemove_NonExistentValue() {
    Trie<Integer> t = createDefaultTrie();
    t.remove("bachelor", 17);

    assertEqualsInAnyOrder(Lists.newArrayList(0, 1, 2, 5), t.findMatches("b"));
    assertEqualsInAnyOrder(Lists.newArrayList(0, 1, 5), t.findMatches("ba"));
    assertEqualsInAnyOrder(Lists.newArrayList(0), t.findMatches("baby"));
    assertEqualsInAnyOrder(Lists.newArrayList(3), t.findMatches("c"));
    assertEqualsInAnyOrder(Collections.<Integer>emptyList(), t.findMatches("d"));
  }

  public void testFindMatches_AccentedCharacters() {
    Trie<Integer> t = createDefaultTrie();

    assertEqualsInAnyOrder(Lists.newArrayList(0, 1, 5), t.findMatches("ba"));
    // Various accented a's
    assertEqualsInAnyOrder(Lists.newArrayList(0, 1, 5), t.findMatches("b\u00c0"));
    assertEqualsInAnyOrder(Lists.newArrayList(0, 1, 5), t.findMatches("b\u00c1"));
    assertEqualsInAnyOrder(Lists.newArrayList(0, 1, 5), t.findMatches("b\u00c2"));
    assertEqualsInAnyOrder(Lists.newArrayList(0, 1, 5), t.findMatches("b\u00c3"));
    assertEqualsInAnyOrder(Lists.newArrayList(0, 1, 5), t.findMatches("b\u00c4"));
  }
  
  public void testNormalization_someTurkishCharacters() {
    assertEquals("igdir", Trie.normalizeString("Iğdır"));
    assertEquals("sirnak", Trie.normalizeString("Şırnak"));
    // This (one state in Azerbaijan) doesn't work yet;  it requires
    // https://en.wikipedia.org/wiki/IPA_Extensions support
    // assertEquals("seki", Trie.normalizeString("Şəki"));
  }
  
  static public <T> void assertEqualsInAnyOrder(
      Iterable<T> first, Iterable<T> second) {
    Multiset<T> firstMulti = HashMultiset.create(first);
    Multiset<T> secondMulti = HashMultiset.create(second);
    assertEquals(firstMulti, secondMulti);

  }
}
