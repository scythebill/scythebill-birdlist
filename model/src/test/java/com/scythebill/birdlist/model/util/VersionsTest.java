package com.scythebill.birdlist.model.util;

import java.util.List;

import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;

import junit.framework.TestCase;

public class VersionsTest extends TestCase {
  public void testOrdering() {
    ImmutableList<String> increasingVersions = ImmutableList.of(
        "9", "9.2", "9.2", "9.2.1", "9.10beta", "9.11", "9.11.0", "10");
    
    for (List<String> permutation : Collections2.permutations(increasingVersions)) {
      assertEquals(increasingVersions,
          Versions.versionOrdering().sortedCopy(permutation));
    }
    
  }
}
