package com.scythebill.birdlist.model.xml;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.common.base.Charsets;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.io.Resources;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.sighting.Area;
import com.scythebill.birdlist.model.sighting.Distance;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfo.ObservationType;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.TaxonomyMappings;

@RunWith(JUnit4.class)
public class XmlReportSetTest {
  private Taxonomy taxonomy;
  private TaxonomyMappings taxonomyMappings;

  @Before
  public void setUp() throws Exception {
    XmlTaxonImport importer = new XmlTaxonImport();
    URL testTaxonUrl = Resources.getResource("testTaxonomy.xml");
    InputStream stream = testTaxonUrl.openStream();
    taxonomy = importer.importTaxa(new InputStreamReader(stream, "UTF-8"));
    stream.close();
    
    taxonomyMappings = new TaxonomyMappings(
        ImmutableSet.of(), ImmutableSet.of(), new Checklists(null));
  }
  
  @Test
  public void importAndExport() throws Exception {
    String inSightings = Resources.toString(
        Resources.getResource(getClass(), "testSightings.xml"),
        Charsets.UTF_8);
    ReportSet inReportSet = new XmlReportSetImport().importReportSet(
        new StringReader(inSightings), taxonomy,
        Optional.<MappedTaxonomy>absent(), taxonomyMappings);
    assertEquals(6, inReportSet.getSightings().size());
    StringWriter writer = new StringWriter();
    new XmlReportSetExport().export(writer, "UTF-8", inReportSet, taxonomy);
    assertEquals(inSightings, writer.toString());
  }

  /**
   * Verify that there's no funkiness with exporting and importing
   * visit info in comma-decimal languages.
   */
  @Test
  public void visitInfoRoundtrippingInFrance() throws Exception {
    Locale original = Locale.getDefault();
    Locale.setDefault(Locale.FRANCE);
    try {
      ReportSet reportSet = ReportSets.newReportSet(taxonomy);
      Location france = reportSet.getLocations().getLocationByCode("FR");
      Sighting sighting = Sighting.newBuilder()
          .setTaxonomy(taxonomy)
          .setLocation(france)
          .setDate(PartialIO.fromString("2010-05-05"))
          .setTaxon(SightingTaxons.newSightingTaxon("spRheapen"))
          .build();
      VisitInfo visitInfo = VisitInfo.builder()
          .withObservationType(ObservationType.HISTORICAL)
          .withArea(Area.inHectares(5.0f))
          .withDistance(Distance.inKilometers(6.0f))
          .build();
      VisitInfoKey visitInfoKey = VisitInfoKey.forSighting(sighting);
      
      // Add a sighting
      reportSet.mutator()
          .adding(ImmutableList.of(sighting))
          .mutate();
      // And associated visit info
      reportSet.putVisitInfo(visitInfoKey, visitInfo);
      
      // Write out to a string
      StringWriter writer = new StringWriter();
      new XmlReportSetExport().export(writer, "UTF-8", reportSet, taxonomy);
      
      // And read back in
      ReportSet inReportSet = new XmlReportSetImport().importReportSet(
          new StringReader(writer.toString()), taxonomy,
          Optional.<MappedTaxonomy>absent(), taxonomyMappings);
      
      assertEquals(visitInfo, inReportSet.visitInfos().get(visitInfoKey));
    } finally {
      Locale.setDefault(original);
    }
  }

  @Test
  public void duplicateLocationNames() throws Exception {
    String inSightings = Resources.toString(
        Resources.getResource(getClass(), "sightingsWithDuplicateLocationName.xml"),
        Charsets.UTF_8);
    ReportSet inReportSet = new XmlReportSetImport().importReportSet(
        new StringReader(inSightings), taxonomy,
        Optional.<MappedTaxonomy>absent(), taxonomyMappings);
    assertEquals(2, inReportSet.getSightings().size());
    String locationId = inReportSet.getSightings().get(1).getLocationId();
    assertEquals("Foo Bar duplicate", inReportSet.getLocations().getLocation(locationId).getDisplayName());
  }
}