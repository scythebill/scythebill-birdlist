/*   
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.text.similarity.LevenshteinDistance;
import org.xml.sax.SAXException;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.internal.Lists;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.opencsv.CSVReader;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Type;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.tools.util.SortedExportLines;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;

public class ChainChecklistMappings {
  @Parameter(names = "-directory", required = true)
  private String directory;
  
  @Parameter(names = "-clements", required = true)
  private String clementsFile;


private Taxonomy clements;

  private final static Splitter MULTI_SPLITTER = Splitter.on(','); 
  private final static Logger logger = Logger.getLogger(ChainChecklistMappings.class.getName());

  private final static String CURRENT_VERSION = "2024";
  private final static ImmutableList<String> VERSIONS = ImmutableList.of(
      "65",
      "651",
      "66",
      "67",
      "68",
      "69",
      "2015",
      "2016",
      "2017",
      "2018",
      "2019",
      "2021",
      "2022",
      "2023");
      
  private final static ImmutableSet<String> NO_SSP_VERSIONS = ImmutableSet.of(
      "65",
      "651",
      "66",
      "67");
      
  public static void main(String[] args) throws Exception {
    final ChainChecklistMappings chainChecklistMappings = new ChainChecklistMappings();
    new JCommander(chainChecklistMappings, args);
    
    chainChecklistMappings.execute();
  }

  private void execute() throws IOException, SAXException {
    clements = ToolTaxonUtils.getTaxonomy(clementsFile);
    String lastVersion = Iterables.getLast(VERSIONS);
    
    LoadedMapping lastMapping =
        new LoadedMapping(
            new File(directory, String.format("%sto%s.csv", lastVersion, CURRENT_VERSION)));

    // Chain the mapping files.  This can happen in any particular version:  it takes, say:
    //   2015to2018.csv 
    //   2018to2019.csv
    // and forms
    //   2015to2019.csv
    // Deleting the old "to2018" versions happens manually
    for (String version : VERSIONS) {
      if (!version.equals(lastVersion)) {
        List<LoadedMapping> loadedMappings = Lists.newArrayList();
        loadedMappings.add(
            new LoadedMapping(
                new File(directory, String.format("%sto%s.csv", version, lastVersion))));
        loadedMappings.add(lastMapping);
        
        String output = String.format("%sto%s.csv", version, CURRENT_VERSION);
        System.out.println("Generating " + output);
        executeOneVersion(loadedMappings,
            null /* sspMappingFile */,
            null /* laterSspMappingFile */,
            output);
      }
    }
    
    // Chain the "sspmap" mapping files (which clean up IOC "sspmap" mappings that
    // now have valid Clements targets).  This happens in reverse, because it takes
    //   - 2018to2019.csv
    //   - 68to2018iocssp.csv
    //   - 69to2019iocssp.csv
    // ... and generates
    //    68to2019iocssp.csv
    
    // The first "later" ssp mapping file is the one for just this year
    String laterSspMappingFile = String.format("%sto%siocssp.csv", lastVersion, CURRENT_VERSION);
    for (String version : Iterables.skip(VERSIONS.reverse(), 1)) {
      
      // Some early versions have no SSPs 
      if (NO_SSP_VERSIONS.contains(version)) {
        break;
      }
      
      String output = String.format("%sto%siocssp.csv", version, CURRENT_VERSION);
      String sspMappingFile = String.format("%sto%siocssp.csv", version, lastVersion);
      
      System.out.println("Generating " + output);
      executeOneVersion(
          ImmutableList.of(lastMapping),
          sspMappingFile,
          laterSspMappingFile,
          output);
//      laterSspMappingFile = output;
    }
  }
  
  private void executeOneVersion(
      List<LoadedMapping> loadedMappings,
      String sspMappingFile,
      String laterSspMappingFile,
      String output) throws IOException, SAXException {
    LoadedSspMapping sspMapping = null;
    if (sspMappingFile != null) {
      sspMapping = new LoadedSspMapping(new File(directory, sspMappingFile)); 
    }
    
    ExportLines exportLines = new SortedExportLines(
        CsvExportLines.fromWriter(
            new BufferedWriter(
                new OutputStreamWriter(
                    new FileOutputStream(new File(directory, output)),
                    Charsets.UTF_8))));
    try {
      LoadedMapping first = loadedMappings.get(0);
      for (String originalTaxon : first.mapping.keySet()) {
        SightingTaxon current = SightingTaxons.newSightingTaxon(originalTaxon);
        for (LoadedMapping mapping : loadedMappings) {
          Set<String> newIds = Sets.newHashSet();
          for (String id : current.getIds()) {
            SightingTaxon mapped = mapping.mapping.get(id);
            if (mapped == null) {
              logger.warning("Could not find mapping for " + id + " in " + mapping.file.getName());
            } else {
              newIds.addAll(mapped.getIds());
            }
          }
          if (newIds.isEmpty()) {
            logger.warning("... and dropping " + originalTaxon);
            current = null;
            break;
          } else {
            current = SightingTaxons.newPossiblySpTaxon(newIds);
          }
        }
        
        if (current != null) {
          if (sspMapping == null) {
            String newName;
            Resolved resolved = current.resolve(clements);
            // Use the same (odd) rules from TaxonomyComparer
            if (current.getType() == Type.SP) {
              newName = resolved.getCommonName();
            } else {
              Taxon taxon = resolved.getTaxon();
              if (taxon.getCommonName() == null) {
                newName = TaxonUtils.getFullName(taxon);
              } else {
                newName = taxon.getCommonName();
              }
            }
            String originalName = first.originalNames.get(originalTaxon);
  
            // ... and similar weird rules from TaxonomyComparer
            String type;
            if (current.getType() == Type.SP) {
              type = "MULT";
            } else if (current.getId().equals(originalTaxon)) {
              type = "SAME";
            } else {
              Taxon.Type oldType = typeFromName(originalTaxon); 
              Taxon.Type newType = typeFromName(current.getId());
              if (oldType == newType) {
                type = (LevenshteinDistance.getDefaultInstance().apply(originalName, newName) <= 3)
                    ? "SAME" : "SM??";
              } else if (oldType.compareTo(newType) < 0) {
                type = "SPLT";
              } else {
                type = "LUMP";
              }
            }
            
            String[] line = new String[]{
                type,
                originalTaxon,
                Joiner.on(',').join(current.getIds()),
                first.originalNames.get(originalTaxon),
                newName
            };
            exportLines.nextLine(line);
          } else {
            // SSP mapping file.  If we've got one (or more) ssp mapping for the original taxon
            // here, then we can generate the *new* mapping for it
            if (sspMapping.mapping.containsKey(originalTaxon)) {
              Collection<SightingTaxon> sightingTaxa = sspMapping.mapping.get(originalTaxon);
              if (current.getType() == Type.SINGLE) {
                for (SightingTaxon sightingTaxon : sightingTaxa) {
                  String[] line = new String[] {
                    sightingTaxon.getId(),
                    sightingTaxon.getSubIdentifier(),
                    current.getId()
                  };
                  exportLines.nextLine(line);
                }
              }
            }
          }
        }
      }
      
      // For the "later" ssp mapping file, include all verbatim.  This assumes that those later
      // files have already been fully mapped.
      if (sspMapping != null) {
        LoadedSspMapping laterSspMapping = new LoadedSspMapping(
            new File(directory, laterSspMappingFile));
        for (Map.Entry<String, SightingTaxon> entry : laterSspMapping.mapping.entries()) {
          String[] line = new String[] {
            entry.getValue().getId(),
            entry.getValue().getSubIdentifier(),
            entry.getKey()
          };
          exportLines.nextLine(line);
        }
      }
    } finally {
      exportLines.close();
    }
  }

  private Taxon.Type typeFromName(String taxonId) {
    if (taxonId.startsWith("ssp")) {
      return Taxon.Type.subspecies;
    } else if (taxonId.startsWith("gr")) {
      return Taxon.Type.group;
    } else {
      return Taxon.Type.species;
    }
  }

  private class LoadedMapping {
    private final Map<String, SightingTaxon> mapping = Maps.newLinkedHashMap();
    private final Map<String, String> originalNames = Maps.newLinkedHashMap();
    private final Set<SightingTaxon> warnings = Sets.newLinkedHashSet();
    private final File file;

    LoadedMapping(File file) throws IOException {
      this.file = file;
      System.out.println("Loading " + file.getName());
      Reader input = new BufferedReader(new FileReader(file));
      CSVReader csv = new CSVReader(input);
      try {
        String[] line;
        while ((line = csv.readNext()) != null) {
          String from = line[1];
          String to = line[2];
          if (!from.isEmpty() && !to.isEmpty()) {
            if (mapping.containsKey(from)) {
              throw new IllegalStateException("Two mappings for " + from);
            }
            
            originalNames.put(from, line[3]);
  
            // Sp. mappings
            if ("MULT".equals(line[0])) {
              Iterable<String> split = MULTI_SPLITTER.split(to);
              SightingTaxon spTaxon = SightingTaxons.newSpTaxon(split);
              mapping.put(from, spTaxon);
            } else if (!"NEW".equals(line[0])) {
              mapping.put(from, SightingTaxons.newSightingTaxon(to));
            }
          }
          
          if ("WARN".equals(line[0])) {
            warnings.add(SightingTaxons.newSightingTaxon(line[2]));
          }
        }
      } finally {
        csv.close();
      }
    }
  }
  
  private class LoadedSspMapping {
    final Multimap<String, SightingTaxon> mapping = 
        LinkedHashMultimap.create();

    LoadedSspMapping(File file) throws IOException {
      System.out.println("Loading " + file.getName());
      Reader input = new BufferedReader(new FileReader(file));
      CSVReader csv = new CSVReader(input);
      try {
        String[] line;
        while ((line = csv.readNext()) != null) {
          SightingTaxon sspTaxon = SightingTaxons.newSightingTaxonWithSecondarySubspecies(
              line[0], line[1]);
          String newId = line[2];
          mapping.put(newId, sspTaxon);
        }
      } finally {
        csv.close();
      }
    }
  }
}
