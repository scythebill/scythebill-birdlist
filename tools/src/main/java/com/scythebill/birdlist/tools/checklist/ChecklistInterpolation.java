/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import java.io.File;
import java.util.Map;
import java.util.SortedSet;

import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableMultiset;
import com.google.common.collect.Maps;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.tools.checklist.LoadedChecklist.LoadedRow;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;
import com.scythebill.birdlist.ui.panels.FilePreferences;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.util.FileDialogs;
import com.scythebill.birdlist.ui.util.FileDialogs.Flags;

/**
 * Uses multiple known "good" checklists to correct an existing checklist.
 * All corrections should be considered hypothetical (especially additions);  this
 * is most accurate for removing species and for setting rarity status.
 */
public class ChecklistInterpolation {
  @Parameter(names = "-checklistFolder", required = true)
  private String checklistsFolder;

  @Parameter(names = "-ioc", required = true)
  private String iocFileName;

  @Parameter(names = "-clements", required = true)
  private String clementsFileName;

  /** If true, then if any of the surrounding countries consider it a rarity, make it one. */
  @Parameter(names = "-favorRarity")
  private boolean favorRarities = true;

  private Taxonomy taxonomy;

  private LoadedChecklist checklist;

  public static void main(String[] args) throws Exception {
    final ChecklistInterpolation interpolation = new ChecklistInterpolation();
    new JCommander(interpolation, args);
    
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        try {
          interpolation.execute();
          System.exit(0);
        } catch (Exception e) {
          e.printStackTrace();
          System.exit(1);
        }
      }
    });
  }

  void execute() throws Exception {
    FilePreferences filePreferences = new FilePreferences();
    filePreferences.lastViewedDirectory = checklistsFolder;
    Flags flags = new Flags() {

      @Override
      public boolean useNativeSaveDialog() {
        return true;
      }

      @Override
      public boolean useNativeOpenDialog() {
        return true;
      }
    };
    FileDialogs fileDialogs = new FileDialogs(
        filePreferences, flags, null);
    File openFile = fileDialogs.openFile(null, "Checklist to fix", new FileFilter() {
      @Override
      public String getDescription() {
        return "CSV files";
      }
      
      @Override
      public boolean accept(File file) {
        return file.isDirectory()
            || file.getName().endsWith(".csv");
      }
    }, FileType.OTHER);
    if (openFile == null) {
      return;
    }
    
    String checklistFileName = openFile.getAbsolutePath();
    checklist = LoadedChecklist.loadChecklist(new File(checklistFileName), null);

    File[] goodFiles = fileDialogs.openFiles(null, "Surrounding good checklists", new FileFilter() {
      @Override
      public String getDescription() {
        return "CSV files";
      }
      
      @Override
      public boolean accept(File file) {
        return file.isDirectory()
            || file.getName().endsWith(".csv");
      }
    }, FileType.OTHER);
    
    taxonomy = ToolTaxonUtils.getMappedTaxonomy(clementsFileName, iocFileName);
 
    Map<String, Status> current = processChecklist(checklist);
    Map<String, Multiset<Status>> good = Maps.newHashMap();
    for (File goodFile : goodFiles) {
      String goodChecklistFileName = goodFile.getAbsolutePath();
      LoadedChecklist goodChecklist = LoadedChecklist.loadChecklist(new File(goodChecklistFileName), null);
      Map<String, Status> goodStatuses = processChecklist(goodChecklist);
      for (Map.Entry<String, Status> entry : goodStatuses.entrySet()) {
        if (!good.containsKey(entry.getKey())) {
          good.put(entry.getKey(), HashMultiset.<Status>create());
        }
        // Skip escapees
        if (entry.getValue() == Status.ESCAPED) {
          continue;
        }
        
        good.get(entry.getKey()).add(entry.getValue());
      }
    }
    
    SortedSet<String[]> corrections = Sets.newTreeSet(new Ordering<String[]>() {
      @Override public int compare(String[] a, String[] b) {
        Taxon aTaxon = taxonomy.getTaxon(a[2]);
        Taxon bTaxon = taxonomy.getTaxon(b[2]);
        return aTaxon.getTaxonomyIndex() - bTaxon.getTaxonomyIndex();
      }
    });
    
    for (String toAdd : Sets.difference(good.keySet(), current.keySet())) {
      ImmutableMultiset<Status> highestCountsFirst = Multisets.copyHighestCountFirst(good.get(toAdd));
      Status preferredStatus = highestCountsFirst.iterator().next();
      // Don't add species that are universally rarities
      if (highestCountsFirst.elementSet().size() == 1
          && preferredStatus == Status.RARITY) {
        continue;
      }
      
      if (favorRarities
          && highestCountsFirst.contains(Status.RARITY)) {
        preferredStatus = Status.RARITY;
      }
      
      corrections.add(new String[]{
          "add",
          "ioc",
          toAdd,
          Strings.nullToEmpty(preferredStatus.text()),
          taxonomy.getTaxon(toAdd).getCommonName(),
          highestCountsFirst.toString()
      });
    }

    for (String toRemove : Sets.difference(current.keySet(), good.keySet())) {
      Status currentStatus = current.get(toRemove);
      corrections.add(new String[]{
          "remove",
          "ioc",
          toRemove,
          "",
          taxonomy.getTaxon(toRemove).getCommonName() + " (was " + currentStatus + ")"
      });
    }
    
    for (String compareStatus : Sets.intersection(current.keySet(), good.keySet())) {
      Status currentStatus = current.get(compareStatus);
      ImmutableMultiset<Status> highestCountsFirst = Multisets.copyHighestCountFirst(good.get(compareStatus));
      Status preferredStatus = highestCountsFirst.iterator().next();
      if (favorRarities
          && highestCountsFirst.contains(Status.RARITY)) {
        preferredStatus = Status.RARITY;
      }

      if (currentStatus != preferredStatus) {
        corrections.add(new String[]{
            "setStatus",
            "ioc",
            compareStatus,
            Strings.nullToEmpty(preferredStatus.text()),
            taxonomy.getTaxon(compareStatus).getCommonName(),
            highestCountsFirst.toString()
        });
      }
    }
    
    for (String[] correction: corrections) {
      System.out.println(Joiner.on(',').join(correction));
    }
  }

  private Map<String, Status> processChecklist(LoadedChecklist checklist) {
    Map<String, Status> current = Maps.newHashMap();
    for (LoadedRow row : checklist.rows) {
      String statusStr = row.iocStatus;
      Status status = Status.NATIVE;
      if (statusStr.equals("introduced")) {
        status = Status.INTRODUCED;
      } else if (statusStr.equals("escaped")) {
        status = Status.ESCAPED;
      } else if (statusStr.equals("rarity")) {
        status = Status.RARITY;
      } else if (statusStr.equals("extinct")) {
        status = Status.EXTINCT;
      }
      
      current.put(row.iocId, status);
    }
    return current;
  }
}
