package com.scythebill.birdlist.tools.checklist;

import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;

public class ChecklistLocations {
  private final LocationSet locations;
  private final PredefinedLocations predefinedLocations;
  final ImmutableMap<String, Location> locationsByCode;

  public ChecklistLocations() {
    this.locations = ReportSets.newReportSet(new TaxonomyImpl()).getLocations();
    this.predefinedLocations = PredefinedLocations.loadAndParse();
    Map<String, Location> locationsByCode = Maps.newHashMap();
    for (Location root : locations.rootLocations()) {
      buildLocationsMap(locationsByCode, root);
    }
    this.locationsByCode = ImmutableMap.copyOf(locationsByCode);
  }

  private void buildLocationsMap(Map<String, Location> locationsByCode, Location location) {
    if (location.getEbirdCode() != null) {
      String ebirdCode = location.getEbirdCode();
      if (location.getType() == Location.Type.state
          && location.getParent().getModelName().equals("United States")) {
        ebirdCode = "US-" + ebirdCode;
      }
      locationsByCode.put(ebirdCode, location);
      // Put locations with split checklists in with their parents too
      if (ebirdCode.equals("ID") || ebirdCode.equals("RU") || ebirdCode.equals("XX")) {
        String ebirdCodeWithParent = ebirdCode + "-" + location.getParent().getModelName();
        locationsByCode.put(ebirdCodeWithParent, location);
      }
    }

    for (Location child : location.contents()) {
      buildLocationsMap(locationsByCode, child);
    }

    for (PredefinedLocation predefined : predefinedLocations.getPredefinedLocations(location)) {
      buildLocationsMap(locationsByCode, predefined.create(locations, location));
    }
  }
}
