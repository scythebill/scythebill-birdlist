/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * A parsed checklist row - before any attempts to align this with a taxonomy.
 */
class ChecklistRow {
  public enum Status {
    NATIVE("native"),
    INTRODUCED("introduced"),
    RARITY("rarity"),
    ESCAPED("escaped"),
    EXTINCT("extinct");
    
    private final String name;

    private Status(String name) {
      this.name = name;
    }
    
    public String getName() {
      return name;
    }
  }

  private String commonName;
  private String scientificName;
  private Status status = Status.NATIVE;
  
  public ChecklistRow() {
  }

  public String getCommonName() {
    return commonName;
  }

  public void setCommonName(String commonName) {
    this.commonName = commonName;
  }

  public String getScientificName() {
    return scientificName;
  }

  public void setScientificName(String scientificName) {
    this.scientificName = scientificName;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }
  
  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}
