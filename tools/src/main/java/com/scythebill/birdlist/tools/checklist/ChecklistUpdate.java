/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.xml.sax.SAXException;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.FileConverter;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.tools.checklist.LoadedSplitChecklist.LoadedRow;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;

/**
 * Updates split checklists for a single taxonomic update. 
 */
class ChecklistUpdate {
  @Parameter(names = "-baseFolder", required = true, converter = FileConverter.class)
  private File baseFolder;

  @Parameter(names = "-outputFolder", required = true, converter = FileConverter.class)
  private File outputFolder;

  @Parameter(names = "-oldClements", required = true, converter = FileConverter.class)
  private File oldClements;

  @Parameter(names = "-oldIoc", converter = FileConverter.class)
  private File oldIoc;

  @Parameter(names = "-newClements", required = true, converter = FileConverter.class)
  private File newClements;

  @Parameter(names = "-newIoc", converter = FileConverter.class)
  private File newIoc;

  @Parameter(names = "-mappingFile", required = true, converter = FileConverter.class)
  private File mappingFile;

  private Taxonomy oldTaxonomy;

  private Taxonomy newTaxonomy;

  public static void main(String[] args) throws Exception {
    new ChecklistUpdate(args).run();
  }
  
  private ChecklistUpdate(String[] args) throws IOException, SAXException {
    new JCommander(this, args);
      
    Preconditions.checkState((oldIoc == null) == (newIoc == null));
    
    if (newIoc == null) {
      oldTaxonomy = ToolTaxonUtils.getTaxonomy(oldClements.getPath());
      newTaxonomy = ToolTaxonUtils.getTaxonomy(newClements.getPath());
    } else {
      oldTaxonomy = ToolTaxonUtils.getMappedTaxonomy(oldClements.getPath(), oldIoc.getPath());
      newTaxonomy = ToolTaxonUtils.getMappedTaxonomy(newClements.getPath(), newIoc.getPath());
    }
  }
  
  private void run() throws IOException {
    
    Preconditions.checkState(baseFolder.exists());

    if (!outputFolder.exists()) {
      outputFolder.mkdirs();
    }
    Preconditions.checkState(outputFolder.exists());
    
    Multimap<String, String> oldSpeciesToNewSpecies = loadMappings(mappingFile, oldTaxonomy, newTaxonomy);
    
    File[] files = baseFolder.listFiles((dir, name) -> name.endsWith(".csv"));
    for (File file : files) {
      System.err.println("Loaded from " + file);
      LoadedSplitChecklist checklist = LoadedSplitChecklist.loadChecklist(file, null);
      for (int i = 0; i < checklist.rows.size(); i++) {
        LoadedRow loadedRow = checklist.rows.get(i);
        Collection<String> newIds = oldSpeciesToNewSpecies.get(loadedRow.id);
        if (newIds.isEmpty()) {
          throw new IllegalStateException("No mapping for " + loadedRow.id);
        }
        
        SightingTaxon newTaxon = SightingTaxons.newPossiblySpTaxon(newIds);
        Resolved resolved = newTaxon.resolveInternal(newTaxonomy);
        // Up to a species mapping.
        newTaxon = resolved.getParentOfAtLeastType(Taxon.Type.species);
        resolved = newTaxon.resolveInternal(newTaxonomy);
        
        loadedRow.id = Joiner.on('/').join(newTaxon.getIds());
        loadedRow.name = resolved.getCommonName();
      }
      
      checklist.replaceFolder(outputFolder);
      checklist.write(newTaxonomy);
    }
  }

  private static Multimap<String, String> loadMappings(
      File priorMapping, Taxonomy oldTaxonomy, Taxonomy newTaxonomy) throws IOException {
    Multimap<String, String> oldTaxonToNewTaxa = HashMultimap.create(); 
    ImportLines lines = CsvImportLines.fromFile(priorMapping, Charsets.UTF_8);
    
    // Load the mappings of old taxa to new taxa
    try { 
      String[] nextLine;
      while ((nextLine = lines.nextLine()) != null) {
        if (!"".equals(nextLine[1])) {
          String oldId = nextLine[1];
          Preconditions.checkNotNull(oldTaxonomy.getTaxon(oldId),
              "Missing old taxon: %s", Arrays.asList(nextLine));
          oldTaxonToNewTaxa.putAll(oldId, Splitter.on(',').split(nextLine[2]));
        }
      }
    } finally {
      lines.close();
    }

    return oldTaxonToNewTaxa;
  }

}
