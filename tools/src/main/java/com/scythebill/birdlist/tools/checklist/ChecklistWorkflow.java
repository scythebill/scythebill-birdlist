/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

/**
 * The full workflow of downloading, parsing, etc. checklists.
 */
public class ChecklistWorkflow {
  public static void main(String[] args) throws Exception {
    new ChecklistWorkflow(args).run();
  }

  @Parameter(names = "-download", required = true)
  private String downloadFolder;

  @Parameter(names = "-clements", required = true)
  private String clements;

  @Parameter(names = "-iocToParse", required = true)
  private String iocToParse;

  @Parameter(names = "-iocToUpdate")
  private String iocToUpdate;

  @Parameter(names = "-iocUpdateFolder")
  private String iocUpdateFolder;

  @Parameter(names = "-iocUpdateMapping")
  private String iocUpdateMapping;

  @Parameter(names = "-correctionsFolder", required = true)
  private String correctionsFolder;

  @Parameter(names = "-correctedFolder", required = true)
  private String correctedFolder;

  @Parameter(names = "-reconciledFolder", required = true)
  private String reconciledFolder;

  @Parameter(names = "-mapsFolder")
  private String mapsFolder;

  ChecklistWorkflow(String[] args) {
    new JCommander(this, args);
  }
  
  public void run() throws Exception {    
//    String folderWithCurrentIoc = downloadFolder;
//    String currentIoc = iocToParse;
//    if (iocToUpdate != null) {
//      System.out.println("Upgrading IOC...");
//      IocChecklistUpdate.main(new String[]{
//          downloadFolder,
//          iocUpdateFolder,
//          iocUpdateMapping,
//          iocToParse,
//          clements,
//          iocToUpdate});
//      folderWithCurrentIoc = iocUpdateFolder;
//      currentIoc = iocToUpdate;
//    }
//    
//    System.out.println("Correcting...");
//    CorrectChecklists.main(new String[] {
//       "-clements", clements,
//       "-ioc", currentIoc,
//       "-input", folderWithCurrentIoc,
//       "-corrected", correctedFolder,
//       "-corrections", correctionsFolder
//    });
//    
//    System.out.println("Reconciling...");
//    ReconcileChecklists.main(new String[] {
//       "-correctionsFolder",  correctionsFolder,
//       "-outputFolder", reconciledFolder,
//       correctedFolder,
//       clements,
//       currentIoc});
//
//    System.out.println("Finding endemics...");
//    FindEndemics.main(new String[] {
//        reconciledFolder,
//        clements,
//        currentIoc});
//    
//    System.out.println("Transposing...");
//    TransposeRanges.main(new String[] {
//        reconciledFolder,
//        clements,
//        currentIoc});
//
//    if (mapsFolder != null) {
//      System.out.println("Writing maps...");
//      ShowSpeciesMap.main(new String[] {
//          "-clements", clements,
//          "-ioc", currentIoc,
//          "-checklistsFolder", reconciledFolder,
//          "-outputFolder", mapsFolder
//      });
//    }
    System.exit(1);
  }
}
