/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import java.io.File;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.FileConverter;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;

/**
 * Applies corrections on top of checklists.
 */
public class CorrectChecklists {
  public static void main(MappedTaxonomy ioc, String[] args) throws Exception {
    new CorrectChecklists(ioc, args).run();
  }

  @Parameter(names = "-input", required = true)
  private String inputFolder;

  @Parameter(names = "-corrected", required = true, converter = FileConverter.class)
  private File correctedFolder;

  @Parameter(names = "-corrections", required = true, converter = FileConverter.class)
  private File correctionsFolder;

  @Parameter(names = "-correctClements")
  private boolean correctClements;
  
  private MappedTaxonomy ioc;

  CorrectChecklists(MappedTaxonomy ioc, String[] args) {
    new JCommander(this, args);
    this.ioc = ioc;
  }
  
  private void run() throws Exception {
    File[] files = correctionsFolder.listFiles((dir, name) -> name.endsWith(".corrections"));
    
    if (!correctedFolder.exists()) {
      correctedFolder.mkdirs();
    } else {
      // Clear out the corrected folder (so that copyMissingChecklists won't fail to
      // overwrite stale data)
      LoadedSplitChecklist.clearChecklists(correctedFolder);
    }
    
    // Correct each file
    for (File file : files) {
      LoadedSplitChecklistCorrections corrections = LoadedSplitChecklistCorrections.loadCorrections(file);
      
      String checklistFileName = file.getName().replace(".corrections", ".csv");
      File checklistFile = new File(inputFolder, checklistFileName);
      LoadedSplitChecklist checklist;
      if (checklistFile.exists()) {
        checklist = LoadedSplitChecklist.loadChecklist(checklistFile, null);
      } else {
        // Some checklists are entirely corrections - starting from nothing
        checklist = LoadedSplitChecklist.newEmptyChecklist(checklistFile, null);
      }
      checklist.replaceFolder(correctedFolder);
      
      boolean changed = corrections.correct(
          checklist, 
          correctClements ? LoadedSplitChecklistCorrections.TaxonomyType.clements : LoadedSplitChecklistCorrections.TaxonomyType.ioc,
          ioc);
      checklist.write(correctClements ? ioc.getBaseTaxonomy() : ioc);
      if (changed) {
        System.err.println("Updating " + file.getName());
        corrections.write();
      }
    }
    
    // For files that had no corrections, copy verbatim
    LoadedSplitChecklist.copyMissingChecklists(new File(inputFolder), correctedFolder);
  }
}
