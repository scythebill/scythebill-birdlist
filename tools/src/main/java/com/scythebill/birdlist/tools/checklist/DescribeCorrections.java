/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Map;
import java.util.Set;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;
import com.scythebill.birdlist.tools.checklist.LoadedChecklistCorrections.Checklist;
import com.scythebill.birdlist.tools.checklist.LoadedChecklistCorrections.LoadedCorrection;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;

/**
 * Describes all the IOC corrections.
 */
public class DescribeCorrections {
  public static void main(String[] args) throws Exception {
    new DescribeCorrections(args).run();
  }

  @Parameter(names = "-clements", required = true)
  private String clementsFileName;

  @Parameter(names = "-ioc", required = true)
  private String iocFileName;

  @Parameter(names = "-corrections", required = true)
  private String correctionsFolder;

  private MappedTaxonomy ioc;
  private final LocationSet locations;
  private final PredefinedLocations predefinedLocations;
  private final ImmutableMap<String, Location> locationsByCode;

  DescribeCorrections(String[] args) {
    new JCommander(this, args);
    this.locations = ReportSets.newReportSet(new TaxonomyImpl()).getLocations();
    this.predefinedLocations = PredefinedLocations.loadAndParse();
    Map<String, Location> locationsByCode = Maps.newHashMap();
    for (Location root : locations.rootLocations()) {
      buildLocationsMap(locationsByCode, root);
    }
    this.locationsByCode = ImmutableMap.copyOf(locationsByCode);
  }
  
  private void run() throws Exception {
    ioc = ToolTaxonUtils.getMappedTaxonomy(clementsFileName, iocFileName);
    File[] files = new File(correctionsFolder).listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        return name.endsWith(".corrections");
      }
    });
    
    // Correct each file
    for (File file : files) {
      LoadedChecklistCorrections corrections = LoadedChecklistCorrections.loadCorrections(file);
      Location location = locationsByCode.get(corrections.getId());
      
      boolean printedAnything = false;
      Set<String> ignoreIds = Sets.newHashSet();
      for (LoadedCorrection row : corrections.rows) {
        if (row.taxonomy != Checklist.ioc) {
          continue;
        }
        
        if (row.id.contains("/")) {
          ignoreIds.addAll(ImmutableList.copyOf(Splitter.on('/').split(row.id)));
          continue;
        }
        
        Taxon taxon = ioc.getTaxon(row.id);
        if (ignoreIds.contains(row.id)) {
          continue;
        }
        
        if (!printedAnything) {
          System.out.println();
          System.out.println(location.getModelName());
          printedAnything = true;
        }
        switch (row.action) {
          case add:
            System.out.println("Add " + taxon.getCommonName());
            break;
          case remove:
            System.out.println("Remove " + taxon.getCommonName());
            break;
          case setStatus:
            if ("".equals(row.status)) {
              continue;
            }
            System.out.println("Status of " + taxon.getCommonName() + " is " + row.status);
            break;
        }
      }
    }
  }

  private void buildLocationsMap(Map<String, Location> locationsByCode, Location location) {
    if (location.getEbirdCode() != null) {
      String ebirdCode = location.getEbirdCode();
      if (location.getType() == Location.Type.state
          && location.getParent().getModelName().equals("United States")) {
        ebirdCode = "US-" + ebirdCode;
      }
      locationsByCode.put(ebirdCode, location);
    }

    for (Location child : location.contents()) {
      buildLocationsMap(locationsByCode, child);
    }

    for (PredefinedLocation predefined : predefinedLocations.getPredefinedLocations(location)) {
      buildLocationsMap(locationsByCode, predefined.create(locations, location));
    }
  }
}
