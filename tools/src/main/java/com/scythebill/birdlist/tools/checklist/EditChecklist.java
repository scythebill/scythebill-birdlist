/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;

import org.xml.sax.SAXException;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.SettableFuture;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.tools.checklist.LoadedSplitChecklist.LoadedRow;
import com.scythebill.birdlist.tools.checklist.LoadedSplitChecklistCorrections.Action;
import com.scythebill.birdlist.tools.checklist.LoadedSplitChecklistCorrections.LoadedCorrection;
import com.scythebill.birdlist.tools.checklist.LoadedSplitChecklistCorrections.TaxonomyType;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.components.SpeciesIndexerPanelConfigurer;
import com.scythebill.birdlist.ui.components.table.ExpandableTable;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.Column;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.ColumnLayout;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.RowState;
import com.scythebill.birdlist.ui.components.table.LabelColumn;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontPreferences;
import com.scythebill.birdlist.ui.panels.FilePreferences;
import com.scythebill.birdlist.ui.panels.SpeciesInfoDescriber;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.util.FileDialogs;
import com.scythebill.birdlist.ui.util.FileDialogs.Flags;
import com.scythebill.birdlist.ui.util.ListListModel;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Tool for editing checklists.
 */
public class EditChecklist {
  @Parameter(names = "-ioc", required = true)
  private String iocFileName;

  @Parameter(names = "-clements", required = true)
  private String clementsFileName;

  @Parameter(names = "-correctionsFolder", required = true)
  private String correctionsFolder;

  @Parameter(names = "-defaultChecklistFolder")
  private String defaultChecklistFolder;

  @Parameter(names = "-editClements")
  private boolean editClements;

  @Parameter(names = "-applyOnlyOneTaxonomy",
      description = "By default, corrections are applied to both taxonomies. "
          + "But if this is true, corrections will only be made to the primary "
          + "taxonomy, and not also mapped to the other.")
  private boolean applyOnlyOneTaxonomy;

  private MappedTaxonomy ioc;
  private TaxonomyType taxonomyType;
  private LoadedSplitChecklist checklist;
  private LoadedSplitChecklistCorrections corrections;
  private ListListModel<LoadedRow> listListModel;

  private ExpandableTable<LoadedRow> table;
  private JEditorPane speciesInfoText;
  private Taxonomy taxonomy;

  private ReportSet reportSet;

  
  public static void main(String[] args) throws Exception {
    final EditChecklist editChecklist = new EditChecklist();
    new JCommander(editChecklist, args);
    editChecklist.init();
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        try {
          editChecklist.start();
        } catch (Exception e) {
          e.printStackTrace();
          System.exit(1);
        }
      }
    });
  }
  
  private void init() throws IOException, SAXException {
    ioc = ToolTaxonUtils.getMappedTaxonomy(clementsFileName, iocFileName);
    taxonomy = editClements ? ioc.getBaseTaxonomy() : ioc;
    taxonomyType = editClements ? TaxonomyType.clements : TaxonomyType.ioc;
    reportSet = ReportSets.newReportSet(ioc.getBaseTaxonomy());
  }

  private void start() throws IOException {
    runOnce().addListener(
        () -> SwingUtilities.invokeLater(() -> {
          try {
            start();
          } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
          }
        }),
        MoreExecutors.directExecutor());
  }

  private ListenableFuture<Void> runOnce() throws IOException {
    FilePreferences filePreferences = new FilePreferences();
    if (defaultChecklistFolder != null) {
      filePreferences.lastViewedDirectory = defaultChecklistFolder;
    }
    Flags flags = new Flags() {

      @Override
      public boolean useNativeSaveDialog() {
        return true;
      }

      @Override
      public boolean useNativeOpenDialog() {
        return true;
      }
    };
    FileDialogs fileDialogs = new FileDialogs(filePreferences, flags, null);
    File openFile = fileDialogs.openFile(null, "Open checklist", new FileFilter() {
      @Override
      public String getDescription() {
        return "CSV files";
      }

      @Override
      public boolean accept(File file) {
        return file.isDirectory() || file.getName().endsWith(".csv");
      }
    }, FileType.OTHER);
    if (openFile == null) {
      System.exit(0);
    }
    checklist = LoadedSplitChecklist.loadChecklist(openFile, null);
    corrections = LoadedSplitChecklistCorrections.loadCorrections(checklist, correctionsFolder);
    corrections.correct(checklist, taxonomyType, ioc);
    checklist.sort(taxonomy);
    return showFrame();    
  }

  private ListenableFuture<Void> showFrame() {
    SettableFuture<Void> future = SettableFuture.create();
    JFrame frame = new JFrame();
    frame.setTitle(checklist.file.getName());
    Container contentPane = frame.getContentPane();
    GroupLayout layout = new GroupLayout(contentPane);
    contentPane.setLayout(layout);
    
    JButton save = new JButton("Save");
    save.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent event) {
        try {
          corrections.write();
        } catch (IOException e) {
          throw new AssertionError(e);
        }
      }
    });
    
    final IndexerPanel<String> indexer = new IndexerPanel<String>();
    SpeciesIndexerPanelConfigurer.unconfigured().configure(indexer, taxonomy);
    
    final JButton add = new JButton("Add");
    add.addActionListener(e -> add(indexer.getValue(), ""));
    
    final JButton addAsRarity = new JButton("Rarity");
    addAsRarity.addActionListener(e -> add(indexer.getValue(), "rarity"));
    
    final JButton select = new JButton("Select");
    select.addActionListener(e -> select(indexer.getValue()));
    
    add.setEnabled(false);
    addAsRarity.setEnabled(false);
    select.setEnabled(false);
    indexer.addPropertyChangeListener("value", new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        boolean present = evt.getNewValue() != null
            && (findRow((String) evt.getNewValue()) >= 0);
        add.setEnabled(evt.getNewValue() != null && !present);
        addAsRarity.setEnabled(add.isEnabled());
        select.setEnabled(present);
      }
    });
    
    indexer.addPropertyChangeListener("selectedValue",
        evt -> setSpeciesText((String) evt.getNewValue()));

    JScrollPane speciesInfoScrollPane = new JScrollPane();
    
    speciesInfoText = new JEditorPane();
    speciesInfoText.setContentType("text/html");
    speciesInfoText.setBackground(Color.WHITE);
    speciesInfoText.setPreferredSize(new Dimension(480, 100));
    speciesInfoText.setMaximumSize(new Dimension(Short.MAX_VALUE, 250));
    speciesInfoScrollPane.setViewportView(speciesInfoText);
    speciesInfoScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
    speciesInfoScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    speciesInfoText.setEditable(false);

    listListModel = new ListListModel<LoadedRow>(checklist.rows);
    table = new ExpandableTable<LoadedRow>(
        LoadedRow.class, new FontManager(new FontPreferences()));
    table.addColumn(new LabelColumn<LoadedSplitChecklist.LoadedRow>(
        ColumnLayout.weightedWidth(1.0f), "Species") {
      @Override
      protected String getText(LoadedRow value) {
        return value.name;
      }
    });
    table.addColumn(new EditStatusColumn());
    
    table.addColumn(new DeleteColumn());
    
    table.setModel(listListModel);
    table.addPropertyChangeListener("selectedIndex", new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        int selectedIndex = table.getSelectedIndex();
        if (selectedIndex < 0) {
          setSpeciesText((String) null);
        } else {
          setSpeciesText(checklist.rows.get(selectedIndex).id);
        }
      }
    });

    
    JScrollPane scrollPane = new JScrollPane();
    scrollPane.setViewportView(table);
    scrollPane.setColumnHeaderView(table.getColumnHeader());
    scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

    layout.setHorizontalGroup(layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
            .addComponent(indexer)
            .addComponent(add)
            .addComponent(addAsRarity)
            .addComponent(select))
        .addComponent(indexer)
        .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
        .addComponent(speciesInfoScrollPane, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
        .addComponent(save));
    layout.setVerticalGroup(layout.createSequentialGroup()
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(indexer)
            .addComponent(add)
            .addComponent(addAsRarity)
            .addComponent(select))
        .addComponent(indexer)
        .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
        .addComponent(speciesInfoScrollPane, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addComponent(save));
    
    UIUtils.maximizeWindow(frame);
    Dimension size = frame.getSize();
    size.width = 800;
    frame.setSize(size);
    frame.setVisible(true);
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    frame.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(WindowEvent e) {
        future.set(null);
      }
    });
    return future;
  }

  private int findRow(String id) {
    for (int i = 0; i < checklist.rows.size(); i++) {
      LoadedRow loadedRow = checklist.rows.get(i);
      if (id.equals(loadedRow.id)) {
        return i;
      }
    }
    return -1;
  }
  
  private void select(String id) {
    int row = findRow(id);
    if (row >= 0) {
      table.setSelectedIndex(row);
    }
  }

  private void add(String id, String status) {
    LoadedRow row = new LoadedRow();
    row.id = Preconditions.checkNotNull(id);
    row.name = taxonomy.getTaxon(id).getCommonName();
    row.status = status;
    // TODO: add clements - irrelevant here, though
    
    int binarySearch =
        Collections.binarySearch(checklist.rows, row, LoadedSplitChecklist.ordering(taxonomy));
    // Already present, ignore
    if (binarySearch > 0) {
      return;
    }
    
    int insertionPoint = -(binarySearch + 1);
    listListModel.asList().add(insertionPoint, row);
    
    LoadedCorrection correction = new LoadedCorrection();
    correction.action = Action.add;
    correction.taxonomyType = taxonomyType;
    correction.id = id;
    correction.status = status;
    corrections.rows.add(correction);
    
    if (!applyOnlyOneTaxonomy) {
      corrections.rows.add(correction.mapToAlternateTaxonomy(ioc));
    }
    
    select(id);
  }
  
  private void remove(LoadedRow row) {
    listListModel.asList().remove(row);
    LoadedCorrection correction = new LoadedCorrection();
    correction.action = Action.remove;
    correction.taxonomyType = taxonomyType;
    correction.id = row.id;
    corrections.rows.add(correction);
    
    if (!applyOnlyOneTaxonomy) {
      corrections.rows.add(correction.mapToAlternateTaxonomy(ioc));      
    }
  }
  
  private void setStatus(LoadedRow row, String status) {
    row.status = status;
    LoadedCorrection correction = new LoadedCorrection();
    correction.action = Action.setStatus;
    correction.taxonomyType = taxonomyType;
    correction.id = row.id;
    correction.status = row.status;
    corrections.rows.add(correction);

    if (!applyOnlyOneTaxonomy) {
      corrections.rows.add(correction.mapToAlternateTaxonomy(ioc));
    }
  }
  
  private class DeleteColumn implements Column<LoadedRow> {

    @Override
    public JComponent createComponent(final LoadedRow value, Set<RowState> states) {
      JButton delete = new JButton("Delete");
      delete.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          remove(value);
        }
      });
      return delete;
    }

    @Override
    public ColumnLayout getWidth() {
      return ColumnLayout.fixedWidth(80);
    }

    @Override
    public boolean sizeComponentsToFit() {
      return false;
    }

    @Override
    public void updateComponentValue(Component component, LoadedRow value) {
    }

    @Override
    public void updateComponentState(Component component, LoadedRow value, Set<RowState> states) {
    }

    @Override
    public String getName() {
      return "Delete";
    }
  }
  
  private class EditStatusColumn implements Column<LoadedRow> {
    @Override
    public JComponent createComponent(final LoadedRow value, Set<RowState> states) {
      final JComboBox<String> combo = new JComboBox<>(new String[]{"", "rarity", "extinct", "introduced", "rarity-from-introduced", "escaped"}); 
      if ("rarity".equals(value.status)) {
        combo.setSelectedIndex(1);
      }
      if ("extinct".equals(value.status)) {
        combo.setSelectedIndex(2);
      }
      if ("introduced".equals(value.status)) {
        combo.setSelectedIndex(3);
      }
      if ("rarity-from-introduced".equals(value.status)) {
        combo.setSelectedIndex(4);
      }
      if ("escaped".equals(value.status)) {
        combo.setSelectedIndex(5);
      }
      combo.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          setStatus(value, (String) combo.getSelectedItem());
        }
      });
      return combo;
    }

    @Override
    public ColumnLayout getWidth() {
      return ColumnLayout.fixedWidth(150);
    }

    @Override
    public boolean sizeComponentsToFit() {
      return false;
    }

    @Override
    public void updateComponentValue(Component component, LoadedRow value) {
    }

    @Override
    public void updateComponentState(Component component, LoadedRow value, Set<RowState> states) {
    }

    @Override
    public String getName() {
      return "Status";
    }
  }
  
  private void setSpeciesText(String id) {
    if (id == null) {
      speciesInfoText.setText("");
      return;
    }
    
    String fontFamily = speciesInfoText.getFont().getFamily();
    int fontSize = speciesInfoText.getFont().getSize();
    StringBuilder builder = new StringBuilder("<html><div style='font: " + fontSize + "pt " + fontFamily + "'>");

    Resolved taxon = SightingTaxons.newResolved(taxonomy.getTaxon(id));
    if (taxon != null) {
      SpeciesInfoDescriber speciesInfoDescriber = new SpeciesInfoDescriber(
          ioc.getBaseTaxonomy(), ioc);
      speciesInfoDescriber.toHtmlText(taxon, reportSet, builder, false);
    }
    builder.append("</div></html>");
    speciesInfoText.setText(builder.toString());
    speciesInfoText.select(0, 0);
    
  }
}
