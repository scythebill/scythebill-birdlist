/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import com.google.common.io.Files;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.tools.checklist.LoadedSplitChecklist.LoadedRow;

/**
 * Finds endemics in a set of checklists.
 */
public class FindEndemics {
  private final File checklistsFolder;
  private final ImmutableMap<String, Location> locationsByCode;
  private final Multimap<String, LoadedSplitChecklist> checklistById = HashMultimap.create();
  private final Multimap<String, LoadedSplitChecklist> stateChecklistById = HashMultimap.create();
  private final List<LoadedSplitChecklist> checklists = new ArrayList<>(); 
  private final static ImmutableSet<String> INCLUDED_STATES = ImmutableSet.of(
      "YE-HD-SOC",
      "PT-20",
      "PT-30",
      "EC-W",
      "AN-BO",
      "AN-CU",
      "AU-TAS-Macquarie",
      "CL-VS-Easter",
      "CL-VS-JuanFernandez",
      "CO-SAP",
      "US-HI",
      "UM-71", // Midway
      "SH-AC",
      "SH-SH",
      "SH-TA",
      "ES-CN"
      );

  public static void main(Taxonomy taxonomy, String[] args) throws Exception {
    File checklistsFolder = new File(args[0]);
    File outFolder = new File(args[1]);
    Preconditions.checkState(checklistsFolder.exists());
    if (!outFolder.exists()) {
      Preconditions.checkState(outFolder.mkdirs());
    }

    FindEndemics findEndemics = new FindEndemics(checklistsFolder);
    findEndemics.loadAllChecklists();
    findEndemics.findEndemics();
    findEndemics.writeAllChecklists(taxonomy, outFolder);
    findEndemics.copyIgnoredFiles(checklistsFolder, outFolder);
  }

  private void writeAllChecklists(Taxonomy taxonomy, File outFolder) throws IOException {
    for (LoadedSplitChecklist checklist : checklists) {
      checklist.replaceFolder(outFolder);
      checklist.write(taxonomy);
    }
  }

  private void findEndemics() throws IOException {
    for (String id : checklistById.keySet()) {
      Collection<LoadedSplitChecklist> collection = checklistById.get(id);
      if (collection.size() == 1
          // At least for now, treat a species as "endemic" if it's only found in both halves
          // of a split country.  In practice, this applies to Indonesia, and species that
          // cross between Asia and Australasia (e.g. Sulawesi to Sula, or Nusa Tenggara
          // to the Moluccas)
          || (collection.size() == 2
              && allElementsHaveSameTrimmedRegion(collection))) {
        for (LoadedSplitChecklist checklist : collection) {
          boolean foundOne = false;
          for (LoadedRow row : checklist.rows) {
            if (row.id.equals(id)
                && Strings.isNullOrEmpty(row.status)) {
              foundOne = true;
              row.status = "endemic";
            }
          }
        
          if (!foundOne) {
            System.err.println("Expected endemic " + id + " in " + checklist.file.getName());
          }
        }
      }
    }

    for (String id : stateChecklistById.keySet()) {
      Collection<LoadedSplitChecklist> collection = stateChecklistById.get(id);
      // If there's only one state in the collection;  and there's only one country;
      // and the state is inside the country, then it's an endemic of the state.
      if (collection.size() == 1
          && checklistById.get(id).size() == 1
          && countryContainsState(
              Iterables.getOnlyElement(checklistById.get(id)).getId(),
              Iterables.getOnlyElement(collection).getId())) {
        LoadedSplitChecklist checklist = Iterables.getOnlyElement(collection);
        boolean foundOne = false;
        for (LoadedRow row : checklist.rows) {
          if (row.id.equals(id)
              && Strings.isNullOrEmpty(row.status)) {
            foundOne = true;
            row.status = "endemic";
          }
        }
        
        if (!foundOne) {
          System.err.println("Expected endemic " + id + " in " + checklist.file.getName());
        }
      }
    }
  }

  private boolean allElementsHaveSameTrimmedRegion(Collection<LoadedSplitChecklist> collection) {
    return collection.stream()
        .map(LoadedSplitChecklist::getId)
        .map(FindEndemics::trimRegionFromCountry)
        .distinct()
        .count() == 1;
        
  }

  private boolean countryContainsState(String country, String state) {
    // Trim the region from the country, to ensure that Indonesian state endemics get marked as such,
    // since "ID-IJ" is inside "ID-Australasia"!
    country = trimRegionFromCountry(country);    
    return state.startsWith(country);
  }

  // Trim off any region suffix from a country
  private static String trimRegionFromCountry(String country) {
    if (country.endsWith("-Asia")) {
      country = country.substring(0, country.length() - "-Asia".length());
    } else if (country.endsWith("-Europe")) {
      country = country.substring(0, country.length() - "-Europe".length());
    } else if (country.endsWith("-Australasia")) {
      country = country.substring(0, country.length() - "-Australasia".length());
    }
    return country;
  }
  
  FindEndemics(File checklistsFolder) {
    this.checklistsFolder = checklistsFolder;
    
    this.locationsByCode = ImmutableMap.copyOf(
        new ChecklistLocations().locationsByCode);
  }

  private final static ImmutableSet<String> IGNORE_STATUS = ImmutableSet.of(
      ChecklistRow.Status.ESCAPED.getName(),
      ChecklistRow.Status.EXTINCT.getName(),
      ChecklistRow.Status.RARITY.getName(),
      ChecklistRow.Status.INTRODUCED.getName());
  
  private void loadAllChecklists() throws IOException {
    File[] files = checklistsFolder.listFiles((dir, name) -> name.endsWith(".csv")
        // Ignore "High Seas" for endemic calculations 
        && !ignoreFileForEndemics(name));
    for (File file : files) {
      LoadedSplitChecklist checklist = LoadedSplitChecklist.loadChecklist(file, locationsByCode);
      checklists.add(checklist);
      
      if (checklist.location.getType() == Location.Type.country
          || INCLUDED_STATES.contains(checklist.getId())) {
        for (LoadedSplitChecklist.LoadedRow row : checklist.rows) {
          if (!IGNORE_STATUS.contains(row.status)) {
            checklistById.put(row.id, checklist);
          }
        }
      } else if (checklist.location.getType() == Location.Type.state
          && !checklist.getId().startsWith("GB") && !checklist.getId().startsWith("IE")) {
        for (LoadedSplitChecklist.LoadedRow row : checklist.rows) {
          if (!IGNORE_STATUS.contains(row.status)) {
            stateChecklistById.put(row.id, checklist);
          }
        }
        
      }
    }
  }

  /**
   * Copy any files that were ignored by "loadAllChecklists".
   */
  private void copyIgnoredFiles(File checklistsFolder, File outFolder) throws IOException {
    File[] files = checklistsFolder.listFiles((dir, name) -> name.endsWith(".csv")
        // Ignore "High Seas" for endemic calculations 
        && ignoreFileForEndemics(name));
    for (File file : files) {
      File outFile = new File(outFolder, file.getName());
      Files.copy(file, outFile);
    }
  }

  private boolean ignoreFileForEndemics(String name) {
    // Ignore "High Seas" files for endemic calculations
    return name.startsWith("XX-");
  }
}
