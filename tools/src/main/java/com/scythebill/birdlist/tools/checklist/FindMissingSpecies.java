/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Map;

import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;

/**
 * Finds endemics in a set of checklists.
 */
public class FindMissingSpecies {
  private final LocationSet locations;
  private final PredefinedLocations predefinedLocations;
  private final File checklistsFolder;
  private final ImmutableMap<String, Location> locationsByCode;
  private final Multimap<String, LoadedChecklist> checklistByClementsId =
      HashMultimap.create();
  private final Multimap<String, LoadedChecklist> checklistByIocId =
      HashMultimap.create();

  public static void main(String[] args) throws Exception {
    File checklistsFolder = new File(args[0]);
    Preconditions.checkState(checklistsFolder.exists());
    MappedTaxonomy ioc = ToolTaxonUtils.getMappedTaxonomy(args[1], args[2]);

    FindMissingSpecies findMissing = new FindMissingSpecies(checklistsFolder);
    findMissing.loadAllChecklists();
    System.err.println("Looking for missing Clements:");
    findMissing.findMissingClements(ioc.getBaseTaxonomy().getRoot());
    System.err.println("Looking for missing IOC:");
    findMissing.findMissingIoc(ioc.getRoot());
  }

  private void findMissingClements(Taxon taxon) throws IOException {
    if (taxon.getType() == Taxon.Type.species) {
      if (!checklistByClementsId.containsKey(taxon.getId())) {
//        if (((Species) taxon).getStatus() != Status.EX) {
          System.err.println(taxon.getCommonName());
          System.err.println(((Species) taxon).getRange());
//        }
      }
    } else {
      for (Taxon child : taxon.getContents()) {
        findMissingClements(child);
      }
    }
  }

  private void findMissingIoc(Taxon taxon) throws IOException {
    if (taxon.getType() == Taxon.Type.species) {
      if (!checklistByIocId.containsKey(taxon.getId())) {
        if (((Species) taxon).getStatus() != Status.EX) {
          System.err.println(taxon.getCommonName());
          System.err.println(((Species) taxon).getRange());
        }
      }
    } else {
      for (Taxon child : taxon.getContents()) {
        findMissingIoc(child);
      }
    }
  }
  
  FindMissingSpecies(File checklistsFolder) {
    this.checklistsFolder = checklistsFolder;
    this.locations = ReportSets.newReportSet(new TaxonomyImpl()).getLocations();
    this.predefinedLocations = PredefinedLocations.loadAndParse();
    Map<String, Location> locationsByCode = Maps.newHashMap();
    for (Location root : locations.rootLocations()) {
      buildLocationsMap(locationsByCode, root);
    }
    this.locationsByCode = ImmutableMap.copyOf(locationsByCode);
  }

  private void loadAllChecklists() throws IOException {
    File[] files = checklistsFolder.listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        return name.endsWith(".csv");
      }
    });
    for (File file : files) {
      LoadedChecklist checklist = LoadedChecklist.loadChecklist(file, locationsByCode);
      for (LoadedChecklist.LoadedRow row : checklist.rows) {
        checklistByClementsId.put(row.clementsId, checklist);
        checklistByIocId.put(row.iocId, checklist);
      }
    }
  }

  private void buildLocationsMap(Map<String, Location> locationsByCode, Location location) {
    if (location.getEbirdCode() != null) {
      String ebirdCode = location.getEbirdCode();
      if (location.getType() == Location.Type.state
          && location.getParent().getModelName().equals("United States")) {
        ebirdCode = "US-" + ebirdCode;
      }
      locationsByCode.put(ebirdCode, location);
    }

    for (Location child : location.contents()) {
      buildLocationsMap(locationsByCode, child);
    }

    for (PredefinedLocation predefined : predefinedLocations.getPredefinedLocations(location)) {
      buildLocationsMap(locationsByCode, predefined.create(locations, location));
    }
  }
}
