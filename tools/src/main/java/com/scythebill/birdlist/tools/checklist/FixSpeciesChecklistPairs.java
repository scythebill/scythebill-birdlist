/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.TreeSet;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.base.Joiner;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.SettableFuture;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.tools.checklist.LoadedChecklistCorrections.Action;
import com.scythebill.birdlist.tools.checklist.LoadedChecklistCorrections.Checklist;
import com.scythebill.birdlist.tools.checklist.LoadedChecklistCorrections.LoadedCorrection;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.components.SpeciesIndexerPanelConfigurer;
import com.scythebill.birdlist.ui.util.ListListModel;
import com.scythebill.birdlist.ui.util.UIUtils;

class FixSpeciesChecklistPairs {
  @Parameter(names = "-ioc", required = true)
  private String iocFileName;

  @Parameter(names = "-clements", required = true)
  private String clementsFileName;

  @Parameter(names = "-correctionsFolder", required = true)
  private String correctionsFolder;

  @Parameter(names = "-checklistsFolder")
  private String checklistsFolder;

  private final Multimap<String, LoadedChecklist> checklistByIocId = LinkedHashMultimap.create();
  private MappedTaxonomy ioc;
  private ChecklistLocations locations;

  public static void main(String[] args) throws Exception {
    FixSpeciesChecklistPairs fix = new FixSpeciesChecklistPairs();
    new JCommander(fix, args);
    
    fix.loadTaxonomy();
    fix.loadLocations();
    fix.loadAllChecklists();
    
    while (true) {
      SpeciesPair speciesPair = fix.findSpeciesPairToFix();
      if (speciesPair != null) {
        List<SingleCorrection> correctionsList = fix.fixSpeciesPair(speciesPair);
        if (correctionsList != null) {
          for (SingleCorrection correction : correctionsList) {
            fix.applyCorrection(speciesPair, correction);
          }
        }
      }
    }
  }

  /** Apply a single correction. */
  private void applyCorrection(SpeciesPair speciesPair, SingleCorrection correction) throws IOException {
    LoadedChecklistCorrections corrections = LoadedChecklistCorrections
        .loadCorrections(correction.checklist, correctionsFolder);
    // For move, first remove the old
    if (correction.type == SingleCorrection.Type.move) {
      LoadedCorrection removeCorrection = new LoadedCorrection();
      removeCorrection.action = Action.remove;
      removeCorrection.taxonomy = Checklist.ioc;
      removeCorrection.id = speciesPair.oldId;
      corrections.rows.add(removeCorrection);
    }
    
    // Now, add the new ID
    LoadedCorrection addCorrection = new LoadedCorrection();
    addCorrection.action = Action.add;
    addCorrection.taxonomy = Checklist.ioc;
    addCorrection.id = speciesPair.newId;
    corrections.rows.add(addCorrection);
    
    corrections.write();
  }

  private void loadAllChecklists() throws IOException {
    File[] files = new File(checklistsFolder).listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        return name.endsWith(".csv");
      }
    });
    for (File file : files) {
      LoadedChecklist checklist = LoadedChecklist.loadChecklist(file, locations.locationsByCode);
      for (LoadedChecklist.LoadedRow row : checklist.rows) {
        checklistByIocId.put(row.iocId, checklist);
      }
    }
  }

  private void loadTaxonomy() throws Exception {
    ioc = ToolTaxonUtils.getMappedTaxonomy(clementsFileName, iocFileName);
  }

  private void loadLocations() {
    this.locations = new ChecklistLocations();
  }

  private SpeciesPair findSpeciesPairToFix() {
    final SettableFuture<SpeciesPair> done = SettableFuture.create();
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(new BorderLayout());
        frame.getContentPane().add(new SpeciesPairPanel(done));
        frame.setBounds(0, 0, 600, 600);
        UIUtils.keepWindowOnScreen(frame);
        frame.setVisible(true);
        closeWhenDone(done, frame);
      }
    });

    return Futures.getUnchecked(done);
  }
  
  private List<SingleCorrection> fixSpeciesPair(final SpeciesPair speciesPair) {
    final SettableFuture<List<SingleCorrection>> done = SettableFuture.create();
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(new BorderLayout());
        frame.getContentPane().add(new EditSpeciesPairPanel(speciesPair, done));
        frame.setBounds(0, 0, 2000, 2000);
        UIUtils.keepWindowOnScreen(frame);
        frame.setVisible(true);
        closeWhenDone(done, frame);
      }
    });

    return Futures.getUnchecked(done);
  }

  static <T> void closeWhenDone(ListenableFuture<T> future, final Frame frame) {
    Futures.addCallback(future, new FutureCallback<T>() {
      @Override
      public void onFailure(Throwable t) {
        onSuccess(null);
      }

      @Override
      public void onSuccess(T value) {
        SwingUtilities.invokeLater(new Runnable() {
          @Override public void run() {
            frame.dispose();
          }
        });
      }
    }, MoreExecutors.directExecutor());
  }
  
  static class SpeciesPair {
    public final String oldId;
    public final String newId;

    SpeciesPair(String oldId, String newId) {
      this.oldId = oldId;
      this.newId = newId;
    }
  }
  
  class SpeciesPairPanel extends JPanel {
    private IndexerPanel<String> oldIndexer;
    private IndexerPanel<String> newIndexer;

    SpeciesPairPanel(final SettableFuture<SpeciesPair> done) {
      BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
      setLayout(layout);
      
      add(new JLabel("Species that's present too often"));
      add(oldIndexer = iocIndexer());
      add(new JLabel("Species that should replace it (somewhere)"));
      add(newIndexer = iocIndexer());
      add(new JButton(new AbstractAction("Done") {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          String oldId = oldIndexer.getValue();
          String newId = newIndexer.getValue();
          if (oldId != null && newId != null) {
            done.set(new SpeciesPair(oldId, newId));
          }
        }
      }));
    }
    
    private IndexerPanel<String> iocIndexer() {
      IndexerPanel<String> indexer = new IndexerPanel<String>();
      SpeciesIndexerPanelConfigurer.unconfigured().configure(indexer, ioc);
      return indexer;
    }
  }
  
  static class SingleCorrection {
    enum Type { move, add };

    final SingleCorrection.Type type;
    final LoadedChecklist checklist;
    
    SingleCorrection(
        SingleCorrection.Type type, LoadedChecklist checklist) {
      this.type = type;
      this.checklist = checklist;
    }
  }
  
  class EditSpeciesPairPanel extends JPanel {
    private ListListModel<ChecklistListEntry> oldListModel;
    private List<SingleCorrection> corrections = Lists.newArrayList();
    private JTextArea newIsMovedToArea;
    private JTextArea newIsAddedToArea;

    /** Wraps a checklist in an object providing toString and comparison. */
    class ChecklistListEntry implements Comparable<ChecklistListEntry> {
      private LoadedChecklist checklist;

      ChecklistListEntry(LoadedChecklist checklist) {
        this.checklist = checklist;
      }
      
      @Override
      public String toString() {
        Location location = checklist.location;
        if (checklist.location.getType() == Location.Type.state
            || checklist.location.getType() == Location.Type.county) {
          return String.format("%s, %s, %s",
              location.getParent().getParent().getModelName(),
              location.getParent().getModelName(),
              location.getModelName());
        } else {
          return String.format("%s, %s",
              location.getParent().getModelName(),
              location.getModelName());
        }
      }

      @Override
      public int compareTo(ChecklistListEntry other) {
        return toString().compareTo(other.toString());
      }
    }
    
    EditSpeciesPairPanel(SpeciesPair pair, final SettableFuture<List<SingleCorrection>> done) {
      Collection<LoadedChecklist> oldChecklists = getLocations(pair.oldId);
      Collection<LoadedChecklist> newChecklists = getLocations(pair.newId);
      oldChecklists.removeAll(newChecklists);
      
      BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
//      GroupLayout layout = new GroupLayout(this);
      setLayout(layout);
      
      String oldName = ioc.getTaxon(pair.oldId).getCommonName();
      String newName = ioc.getTaxon(pair.newId).getCommonName();
      
      JLabel oldIsIn = new JLabel(oldName + " is in:");
      JLabel newHeader = new JLabel(newName);
      
      // Identify all the locations where the "new" species is already found
      JLabel newIsAlreadyIn = new JLabel(newName + " was already in:");
      JTextArea newIsAlreadyArea = newTextArea();
      JScrollPane newIsAlreadyScroll = newScrollPane(newIsAlreadyArea);
      TreeSet<ChecklistListEntry> newIsAlready = Sets.newTreeSet();
      for (LoadedChecklist newChecklist : newChecklists) {
        newIsAlready.add(new ChecklistListEntry(newChecklist));
      }
      newIsAlreadyArea.setText(Joiner.on(", ").join(newIsAlready));
      

      JLabel newIsAddedTo = new JLabel(newName + " is added to:");
      newIsAddedToArea = newTextArea();
      JScrollPane newIsAddedToScroll = newScrollPane(newIsAddedToArea); 

      JLabel newIsMovedTo = new JLabel(newName + " is moved to:");
      newIsMovedToArea = newTextArea();
      JScrollPane newIsMovedToScroll = newScrollPane(newIsMovedToArea); 

      // Build the list of places the "old" species is currently claimed from 
      final JList<ChecklistListEntry> oldIsInList = new JList<>();
      TreeSet<ChecklistListEntry> oldTreeSet = Sets.newTreeSet();
      for (LoadedChecklist oldChecklist : oldChecklists) {
        oldTreeSet.add(new ChecklistListEntry(oldChecklist));
      }
      oldListModel = new ListListModel<ChecklistListEntry>(Lists.newArrayList(oldTreeSet));
      oldIsInList.setModel(oldListModel);
      
      JButton move = new JButton(new AbstractAction("Move") {
        @Override public void actionPerformed(ActionEvent e) {
          for (ChecklistListEntry entry : oldIsInList.getSelectedValuesList()) {
            corrections.add(new SingleCorrection(SingleCorrection.Type.move, entry.checklist));
            oldListModel.asList().remove(entry);
            updateCorrections();
          }
        }
      });

      JButton add = new JButton(new AbstractAction("Add") {
        @Override public void actionPerformed(ActionEvent e) {
          for (ChecklistListEntry entry : oldIsInList.getSelectedValuesList()) {
            corrections.add(new SingleCorrection(SingleCorrection.Type.add, entry.checklist));
            oldListModel.asList().remove(entry);
            updateCorrections();
          }
        }
      });

      JButton ignore = new JButton(new AbstractAction("Ignore") {
        @Override public void actionPerformed(ActionEvent e) {
          for (ChecklistListEntry selectedValue : oldIsInList.getSelectedValuesList()) {
            oldListModel.asList().remove(selectedValue);
          }
        }
      });

      JButton cancel = new JButton(new AbstractAction("Cancel") {
        @Override public void actionPerformed(ActionEvent e) {
          done.set((List<SingleCorrection>) null);
        }
      });
      JButton save = new JButton(new AbstractAction("Save") {
        @Override public void actionPerformed(ActionEvent e) {
          done.set(corrections);
        }
      });
      
      Box topRight = Box.createVerticalBox();
      topRight.add(oldIsIn);
      topRight.add(newScrollPane(oldIsInList));
      
      Box topLeft = Box.createVerticalBox();
      topLeft.add(newHeader);
      topLeft.add(add);
      topLeft.add(move);
      topLeft.add(ignore);
      
      Box top = Box.createHorizontalBox();
      top.add(topLeft);
      top.add(topRight);
      
      Box buttons = Box.createHorizontalBox();
      buttons.add(cancel);
      buttons.add(save);
      
      add(top);
      add(newIsAlreadyIn);
      add(newIsAlreadyScroll);
      add(newIsAddedTo);
      add(newIsAddedToScroll);
      add(newIsMovedTo);
      add(newIsMovedToScroll);
      add(buttons);
    }
    
    private void updateCorrections() {
      TreeSet<ChecklistListEntry> moves = Sets.newTreeSet();
      TreeSet<ChecklistListEntry> adds = Sets.newTreeSet();
      for (SingleCorrection correction : corrections) {
        if (correction.type == SingleCorrection.Type.add) {
          adds.add(new ChecklistListEntry(correction.checklist));
        } else {
          moves.add(new ChecklistListEntry(correction.checklist));
        }
      }
      
      newIsAddedToArea.setText(Joiner.on(", ").join(adds));
      newIsMovedToArea.setText(Joiner.on(", ").join(moves));
    }

    private JTextArea newTextArea() {
      JTextArea textArea = new JTextArea(4, 70);
      textArea.setLineWrap(true);
      textArea.setEditable(false);
      return textArea;
    }

    private JScrollPane newScrollPane(JComponent component) {
      JScrollPane scrollPane = new JScrollPane();
      scrollPane.setViewportView(component);
      scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
      scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      return scrollPane;
    }

    private List<LoadedChecklist> getLocations(String id) {
      return Lists.newArrayList(checklistByIocId.get(id));
    }
  }
}
