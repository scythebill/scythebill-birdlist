/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.FileConverter;
import com.google.common.base.Joiner;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.SettableFuture;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.tools.checklist.LoadedSplitChecklistCorrections.Action;
import com.scythebill.birdlist.tools.checklist.LoadedSplitChecklistCorrections.TaxonomyType;
import com.scythebill.birdlist.tools.checklist.LoadedSplitChecklistCorrections.LoadedCorrection;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.components.SpeciesIndexerPanelConfigurer;
import com.scythebill.birdlist.ui.util.ListListModel;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Read checklists and allow updating the range of an entire species.
 */
class FixSpeciesRange {
  @Parameter(names = "-ioc", required = true)
  private String iocFileName;

  @Parameter(names = "-clements", required = true)
  private String clementsFileName;

  @Parameter(names = "-correctionsFolder", required = true, converter = FileConverter.class)
  private File correctionsFolder;

  @Parameter(names = "-checklistsFolder", required = true, converter = FileConverter.class)
  private File checklistsFolder;
  
  @Parameter(names = "-fixClements")
  private boolean fixClements;

  @Parameter(names = "-applyOnlyOneTaxonomy",
      description = "By default, corrections are applied to both taxonomies. "
          + "But if this is true, corrections will only be made to the primary "
          + "taxonomy, and not also mapped to the other.")
  private boolean applyOnlyOneTaxonomy;

  private final Multimap<String, LoadedSplitChecklist> checklistByTaxonId = LinkedHashMultimap.create();
  private final Map<String, LoadedSplitChecklist> checklistByChecklistId = Maps.newHashMap();
  /** Primary taxonomy for the tool. */
  private Taxonomy taxonomy;
  /** IOC taxonomy, no matter what {@code taxonomy} points to. */ 
  private MappedTaxonomy iocTaxonomy;
  private ChecklistLocations locations;

  public static void main(String[] args) throws Exception {
    FixSpeciesRange fix = new FixSpeciesRange();
    new JCommander(fix, args);
    
    fix.loadTaxonomy();
    fix.loadLocations();
    fix.loadAllChecklists();
    
    while (true) {
      SpeciesToFix speciesToFix = fix.fixSpecies();
      if (speciesToFix != null) {
        List<SingleCorrection> correctionsList = fix.fixSpecies(speciesToFix);
        if (correctionsList != null) {
          for (SingleCorrection correction : correctionsList) {
            fix.applyCorrection(speciesToFix, correction);
          }
        }
      }
    }
  }

  /** Apply a single correction. */
  private void applyCorrection(SpeciesToFix speciesPair, SingleCorrection correction) throws IOException {
    LoadedSplitChecklistCorrections corrections = LoadedSplitChecklistCorrections
        .loadCorrections(correction.checklist, correctionsFolder.getPath());
    // For move, first remove the old
    if (correction.type == SingleCorrection.Type.remove) {
      LoadedCorrection removeCorrection = new LoadedCorrection();
      removeCorrection.action = Action.remove;
      removeCorrection.taxonomyType = getTaxonomy();
      removeCorrection.id = speciesPair.id;
      corrections.rows.add(removeCorrection);
      
      if (!applyOnlyOneTaxonomy) {
        corrections.rows.add(removeCorrection.mapToAlternateTaxonomy(iocTaxonomy));
      }
    } else {
      LoadedCorrection addCorrection = new LoadedCorrection();
      addCorrection.action = Action.add;
      addCorrection.taxonomyType = getTaxonomy();
      addCorrection.id = speciesPair.id;
      if (correction.type == SingleCorrection.Type.addAsRarity) {
        addCorrection.status = "rarity";
      } else if (correction.type == SingleCorrection.Type.addAsIntroduced) {
        addCorrection.status = "introduced";
      }
      corrections.rows.add(addCorrection);
      
      if (!applyOnlyOneTaxonomy) {
        corrections.rows.add(addCorrection.mapToAlternateTaxonomy(iocTaxonomy));
      }
    }
    
    corrections.write();
  }

  TaxonomyType getTaxonomy() {
    return fixClements ? TaxonomyType.clements : TaxonomyType.ioc;
  }

  private void loadAllChecklists() throws IOException {
    File[] files = checklistsFolder.listFiles((dir, name) -> name.endsWith(".csv"));
    for (File file : files) {
      LoadedSplitChecklist checklist = LoadedSplitChecklist.loadChecklist(file, locations.locationsByCode);
      checklistByChecklistId.put(checklist.getId(), checklist);
      for (LoadedSplitChecklist.LoadedRow row : checklist.rows) {
        checklistByTaxonId.put(getRowTaxonId(row), checklist);
      }
    }
    
    if (!correctionsFolder.exists()) {
      correctionsFolder.mkdirs();
    }
  }

  String getRowTaxonId(LoadedSplitChecklist.LoadedRow row) {
    return row.id;
  }

  private void loadTaxonomy() throws Exception {
    if (fixClements) {
      taxonomy = ToolTaxonUtils.getTaxonomy(clementsFileName);
      iocTaxonomy = ToolTaxonUtils.getMappedTaxonomy(taxonomy, iocFileName);
    } else {
      iocTaxonomy = ToolTaxonUtils.getMappedTaxonomy(clementsFileName, iocFileName);
      taxonomy = iocTaxonomy;
    }
  }

  private void loadLocations() {
    this.locations = new ChecklistLocations();
  }

  private SpeciesToFix fixSpecies() {
    final SettableFuture<SpeciesToFix> done = SettableFuture.create();
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(new BorderLayout());
        JLabel label = new JLabel(getLabel());
        label.setHorizontalAlignment(SwingConstants.CENTER);
        frame.getContentPane().add(label, BorderLayout.NORTH);
        frame.getContentPane().add(new SpeciesPanel(done));
        frame.setBounds(0, 0, 600, 600);
        UIUtils.keepWindowOnScreen(frame);
        frame.setVisible(true);
        closeWhenDone(done, frame);
      }
    });

    return Futures.getUnchecked(done);
  }
  
  private List<SingleCorrection> fixSpecies(final SpeciesToFix species) {
    final SettableFuture<List<SingleCorrection>> done = SettableFuture.create();
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(new BorderLayout());
        frame.getContentPane().add(new EditSpeciesPanel(species, done));
        frame.setBounds(0, 0, 2000, 2000);
        UIUtils.keepWindowOnScreen(frame);
        frame.setVisible(true);
        closeWhenDone(done, frame);
      }
    });

    return Futures.getUnchecked(done);
  }

  String getLabel() {
    String label = fixClements ? "Clements" : "IOC";
    if (applyOnlyOneTaxonomy) {
      label += " (only)";
    } else {
      label += " (fix all)";
    }
    return label;
  }

  static <T> void closeWhenDone(ListenableFuture<T> future, final Frame frame) {
    Futures.addCallback(future, new FutureCallback<T>() {
      @Override
      public void onFailure(Throwable t) {
        onSuccess(null);
      }

      @Override
      public void onSuccess(T value) {
        SwingUtilities.invokeLater(frame::dispose);
      }
    }, MoreExecutors.directExecutor());
  }
  
  static class SpeciesToFix {
    public final String id;
    public String textValue;

    SpeciesToFix(String id, String textValue) {
      this.id = id;
      this.textValue = textValue;
    }
  }
  
  class SpeciesPanel extends JPanel {
    private IndexerPanel<String> indexer;

    SpeciesPanel(final SettableFuture<SpeciesToFix> done) {
      BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
      setLayout(layout);
      
      add(new JLabel("Species to fix"));
      add(indexer = taxonomyIndexer());
      add(new JButton(new AbstractAction("Done") {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          String id = indexer.getValue();
          if (id != null) {
            String text = String.format("%s: %s", indexer.getTextValue(),
                TaxonUtils.getRange((Species) taxonomy.getTaxon(id)));
            done.set(new SpeciesToFix(id, text));
          }
        }
      }));
    }
    
    private IndexerPanel<String> taxonomyIndexer() {
      IndexerPanel<String> indexer = new IndexerPanel<String>();
      SpeciesIndexerPanelConfigurer.unconfigured().configure(indexer, taxonomy);
      return indexer;
    }
  }
  
  static class SingleCorrection {
    enum Type { remove, add, addAsRarity, addAsIntroduced };

    final SingleCorrection.Type type;
    final LoadedSplitChecklist checklist;
    
    SingleCorrection(
        SingleCorrection.Type type, LoadedSplitChecklist checklist) {
      this.type = type;
      this.checklist = checklist;
    }
  }
  
  class EditSpeciesPanel extends JPanel {
    private ListListModel<ChecklistListEntry> currentListModel;
    private ListListModel<ChecklistListEntry> notInListModel;
    private List<SingleCorrection> corrections = Lists.newArrayList();
    private JTextArea addedToArea;
    private JTextArea removedFromArea;

    /** Wraps a checklist in an object providing toString and comparison. */
    class ChecklistListEntry implements Comparable<ChecklistListEntry> {
      private LoadedSplitChecklist checklist;

      ChecklistListEntry(LoadedSplitChecklist checklist) {
        this.checklist = checklist;
      }
      
      @Override
      public String toString() {
        Location location = checklist.location;
        if ((checklist.location.getType() == Location.Type.state
            || checklist.location.getType() == Location.Type.county)
            && location.getParent().getParent() != null) {
          return String.format("%s, %s, %s",
              location.getParent().getParent().getModelName(),
              location.getParent().getModelName(),
              location.getModelName());
        } else {
          return String.format("%s, %s",
              location.getParent().getModelName(),
              location.getModelName());
        }
      }

      @Override
      public int compareTo(ChecklistListEntry other) {
        return toString().compareTo(other.toString());
      }
    }
    
    EditSpeciesPanel(SpeciesToFix speciesToFix, final SettableFuture<List<SingleCorrection>> done) {
      Collection<LoadedSplitChecklist> currentChecklists = getLocations(speciesToFix.id);
      Collection<LoadedSplitChecklist> missingChecklists = negate(currentChecklists);
      
      BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
      setLayout(layout);
      
      // Build the list of places the species is currently in 
      final JList<ChecklistListEntry> currentIsInList = new JList<ChecklistListEntry>();
      TreeSet<ChecklistListEntry> currentTreeSet = Sets.newTreeSet();
      for (LoadedSplitChecklist currentChecklist : currentChecklists) {
        currentTreeSet.add(new ChecklistListEntry(currentChecklist));
      }
      currentListModel = new ListListModel<ChecklistListEntry>(Lists.newArrayList(currentTreeSet));
      currentIsInList.setModel(currentListModel);

      final JList<ChecklistListEntry> notIsInList = new JList<>();
      TreeSet<ChecklistListEntry> notTreeSet = Sets.newTreeSet();
      for (LoadedSplitChecklist missingChecklist : missingChecklists) {
        notTreeSet.add(new ChecklistListEntry(missingChecklist));
      }
      notInListModel = new ListListModel<ChecklistListEntry>(Lists.newArrayList(notTreeSet));
      notIsInList.setModel(notInListModel);

      JLabel addedTo = new JLabel("Added to:");
      addedToArea = newTextArea();
      JScrollPane addedToScroll = newScrollPane(addedToArea); 

      JLabel removedFrom = new JLabel("Removed from:");
      removedFromArea = newTextArea();
      JScrollPane removedFromScroll = newScrollPane(removedFromArea); 

      JButton remove = new JButton(new AbstractAction("Remove") {
        @Override public void actionPerformed(ActionEvent e) {
          for (ChecklistListEntry entry : currentIsInList.getSelectedValuesList()) {
            corrections.add(new SingleCorrection(SingleCorrection.Type.remove, entry.checklist));
            currentListModel.asList().remove(entry);
            updateCorrections();
          }
        }
      });

      JButton add = new JButton(new AbstractAction("Add") {
        @Override public void actionPerformed(ActionEvent e) {
          for (ChecklistListEntry entry : notIsInList.getSelectedValuesList()) {
            corrections.add(new SingleCorrection(SingleCorrection.Type.add, entry.checklist));
            notInListModel.asList().remove(entry);
            updateCorrections();
          }
        }
      });

      JButton addAsRarity = new JButton(new AbstractAction("Rarity") {
        @Override public void actionPerformed(ActionEvent e) {
          for (ChecklistListEntry entry : notIsInList.getSelectedValuesList()) {
            corrections.add(new SingleCorrection(SingleCorrection.Type.addAsRarity, entry.checklist));
            notInListModel.asList().remove(entry);
            updateCorrections();
          }
        }
      });

      JButton addAsIntroduced = new JButton(new AbstractAction("Introduced") {
        @Override public void actionPerformed(ActionEvent e) {
          for (ChecklistListEntry entry : notIsInList.getSelectedValuesList()) {
            corrections.add(new SingleCorrection(SingleCorrection.Type.addAsIntroduced, entry.checklist));
            notInListModel.asList().remove(entry);
            updateCorrections();
          }
        }
      });

      JButton cancel = new JButton(new AbstractAction("Cancel") {
        @Override public void actionPerformed(ActionEvent e) {
          done.set((List<SingleCorrection>) null);
        }
      });
      JButton save = new JButton(new AbstractAction("Save") {
        @Override public void actionPerformed(ActionEvent e) {
          done.set(corrections);
        }
      });
      
      Box topRight = Box.createVerticalBox();
      topRight.add(new JLabel("Not found in"));
      topRight.add(add);
      topRight.add(addAsRarity);
      topRight.add(addAsIntroduced);
      topRight.add(newScrollPane(notIsInList));
      
      Box topLeft = Box.createVerticalBox();
      topLeft.add(new JLabel("Is found in"));
      topLeft.add(remove);
      topLeft.add(newScrollPane(currentIsInList));
      
      Box top = Box.createHorizontalBox();
      top.add(topLeft);
      top.add(topRight);
      
      Box buttons = Box.createHorizontalBox();
      buttons.add(cancel);
      buttons.add(save);
      
      add(new JLabel(speciesToFix.textValue));
      add(top);
      add(addedTo);
      add(addedToScroll);
      add(removedFrom);
      add(removedFromScroll);
      add(buttons);
    }
    
    private void updateCorrections() {
      TreeSet<ChecklistListEntry> removes = Sets.newTreeSet();
      TreeSet<ChecklistListEntry> adds = Sets.newTreeSet();
      for (SingleCorrection correction : corrections) {
        if (correction.type != SingleCorrection.Type.remove) {
          adds.add(new ChecklistListEntry(correction.checklist));
        } else {
          removes.add(new ChecklistListEntry(correction.checklist));
        }
      }
      
      addedToArea.setText(Joiner.on(", ").join(adds));
      removedFromArea.setText(Joiner.on(", ").join(removes));
    }

    private JTextArea newTextArea() {
      JTextArea textArea = new JTextArea(4, 70);
      textArea.setLineWrap(true);
      textArea.setEditable(false);
      return textArea;
    }

    private JScrollPane newScrollPane(JComponent component) {
      JScrollPane scrollPane = new JScrollPane();
      scrollPane.setViewportView(component);
      scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
      scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      return scrollPane;
    }

    private List<LoadedSplitChecklist> getLocations(String id) {
      return Lists.newArrayList(checklistByTaxonId.get(id));
    }

    private Collection<LoadedSplitChecklist> negate(Collection<LoadedSplitChecklist> currentChecklists) {
      Set<String> currentCodes = Sets.newHashSet();
      for (LoadedSplitChecklist checklist : currentChecklists) {
        currentCodes.add(checklist.getId());
      }

      List<LoadedSplitChecklist> negation = Lists.newArrayList();
      for (String code : locations.locationsByCode.keySet()) {
        if (!currentCodes.contains(code) && checklistByChecklistId.containsKey(code)) {
           negation.add(checklistByChecklistId.get(code));
        }
      }
      return negation;
    }
  }
}
