package com.scythebill.birdlist.tools.checklist;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nullable;

import com.google.common.base.Charsets;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.io.Files;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.ResolvedComparator;

class LoadedChecklist {
  public File file;
  public Location location;
  public List<LoadedChecklist.LoadedRow> rows = Lists.newArrayList();
  
  @Override
  public String toString() {
    return location.getModelName();
  }

  public String getId() {
    return file.getName().substring(0, file.getName().indexOf('.'));
  }
  
  public void replaceFolder(File newFolder) {
    file = new File(newFolder, file.getName());
  }
  
  static LoadedChecklist newEmptyChecklist(
      File file, @Nullable ImmutableMap<String, Location> locationsByCode) {
    LoadedChecklist checklist = new LoadedChecklist();
    checklist.file = file;

    String id = checklist.getId();    
    if (locationsByCode != null
        && !locationsByCode.containsKey(id)) {
      throw new IllegalArgumentException("Could not find location for ID " + id);
    }
    checklist.location = locationsByCode == null ? null : locationsByCode.get(id);
    
    return checklist;
  }
  
  static LoadedChecklist loadChecklist(
      File file, @Nullable ImmutableMap<String, Location> locationsByCode) throws IOException {
    LoadedChecklist checklist = newEmptyChecklist(file, locationsByCode);
    ImportLines importLines = CsvImportLines.fromFile(file, Charsets.UTF_8);
    try {
      // Skip the header
      importLines.nextLine();
      while (true) {
        String[] nextLine = importLines.nextLine();
        if (nextLine == null) {
          break;
        }
        LoadedChecklist.LoadedRow row = new LoadedChecklist.LoadedRow();
        row.clementsName = nextLine[0];
        row.clementsId = nextLine[1];
        row.clementsGroupId = nextLine[2];
        row.iocName = nextLine[3];
        row.iocId = nextLine[4];
        row.status = nextLine[5];
        if (nextLine.length > 6) {
          row.iocStatus = nextLine[6];
        } else {
          row.iocStatus = row.status;
        }
        checklist.rows.add(row);
      }
    } finally {
      importLines.close();
    }

    return checklist;
  }

  public static void clearChecklists(File folder) {
    File[] checklists = folder.listFiles(allCsv());
    
    for (File checklist : checklists) {
      if (!checklist.delete()) {
        throw new RuntimeException("Could not delete " + checklist);
      }
    }
  }

  public static void copyMissingChecklists(File oldFolder, File newFolder) throws IOException {
    // For files that had no corrections, copy verbatim
    File[] oldChecklists = oldFolder.listFiles(allCsv());
    
    for (File oldChecklist : oldChecklists) {
      File correctedChecklist = new File(newFolder, oldChecklist.getName());
      if (!correctedChecklist.exists()) {
        Files.copy(oldChecklist, correctedChecklist);
      }
    }
  }
  
  public void write(final Taxonomy iocChecklist) throws IOException {
    sort(iocChecklist);
    Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),
        Charsets.UTF_8), 10240);
    ExportLines exportLines = CsvExportLines.fromWriter(out);
    LoadedRow previous = null;
    try {
      exportLines.nextLine(new String[]{"Clements name", "Clements ID", "Clements group", "IOC name", "IOC id", "Status", "IOC Status"});
      for (LoadedChecklist.LoadedRow row : rows) {
        // Don't bother writing duplicate rows (which comes up a lot when merging)
        if (previous != null
            && previous.equalIds(row)) {
          continue;
        }
        
        exportLines.nextLine(new String[]{
           row.clementsName,
           row.clementsId,
           row.clementsGroupId,
           row.iocName,
           row.iocId,
           row.status,
           row.iocStatus
        });
        previous = row;
      }
    } finally {
      exportLines.close();
    }
  }

  public void sort(final Taxonomy iocChecklist) {
    Ordering<LoadedRow> ordering = ordering(iocChecklist);
    // Sort the checklist
    Collections.sort(rows, ordering);
  }

  /**
   * Return an ordering of LoadedRows.  The taxonomy passed must be IOC!
   */
  public static Ordering<LoadedRow> ordering(final Taxonomy iocChecklist) {
    final ResolvedComparator comparator = new ResolvedComparator();
    Ordering<LoadedRow> ordering = new Ordering<LoadedRow>() {
      @Override
      public int compare(LoadedRow first, LoadedRow second) {
        SightingTaxon firstTaxon = toSightingTaxon(first.iocId);
        SightingTaxon secondTaxon = toSightingTaxon(second.iocId);
        return comparator.compare(
            firstTaxon.resolveInternal(iocChecklist),
            secondTaxon.resolveInternal(iocChecklist));
      }
      
      private SightingTaxon toSightingTaxon(String id) {
        if (id.contains("/")) {
          return SightingTaxons.newSpTaxon(Splitter.on('/').split(id));
        } else {
          return SightingTaxons.newSightingTaxon(id);
        }
      }
    };
    return ordering;
  }
  
  static class LoadedRow implements Cloneable {
    public String iocStatus;
    public String clementsId;
    public String iocId;
    public String clementsName;
    public String iocName;
    public String status;
    public String clementsGroupId;
    
    @Override
    public LoadedRow clone() {
      try {
        return (LoadedRow) super.clone();
      } catch (CloneNotSupportedException e) {
        throw new RuntimeException(e);
      }
    }
    
    public boolean equalIds(LoadedRow other) {
      return Objects.equal(Strings.emptyToNull(clementsId), Strings.emptyToNull(other.clementsId))
          && Objects.equal(Strings.emptyToNull(iocId), Strings.emptyToNull(other.iocId))
          && Objects.equal(Strings.emptyToNull(clementsGroupId), Strings.emptyToNull(other.clementsGroupId));
    }
    
    @Override
    public String toString() {
      return MoreObjects.toStringHelper(this)
          .add("clementsId", clementsId)
          .add("clementsGroupId", clementsGroupId)
          .add("iocId", iocId)
          .toString();
    }
  }
  
  static FilenameFilter allCsv() {
    return (dir, name) -> name.endsWith(".csv");    
  }
}