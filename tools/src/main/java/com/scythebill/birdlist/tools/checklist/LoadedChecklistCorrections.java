/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Iterator;
import java.util.List;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Type;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.tools.checklist.LoadedChecklist.LoadedRow;

class LoadedChecklistCorrections {
  public File file;
  public final List<LoadedCorrection> rows = Lists.newArrayList();
  
  public String getId() {
    return file.getName().substring(0, file.getName().indexOf('.'));
  }
  
  static LoadedChecklistCorrections loadCorrections(File file) throws IOException {
    LoadedChecklistCorrections corrections = new LoadedChecklistCorrections();
    corrections.file = file;

    ImportLines importLines = CsvImportLines.fromFile(file, Charsets.UTF_8);
    try {
      // Skip the header
      importLines.nextLine();
      while (true) {
        String[] nextLine = importLines.nextLine();
        if (nextLine == null) {
          break;
        }
        LoadedCorrection row = new LoadedCorrection();
        row.action = Action.valueOf(nextLine[0]);
        row.taxonomy = Checklist.valueOf(nextLine[1]);
        row.id = nextLine[2];
        row.status = nextLine[3];
        corrections.rows.add(row);
      }
    } finally {
      importLines.close();
    }

    return corrections;
  }

  /** Correct a checklist. 
   * @param removeIfObsolete */
  public boolean correct(LoadedChecklist loadedChecklist, MappedTaxonomy ioc, boolean removeIfObsolete) {
    Iterator<LoadedCorrection> correctionIterator = rows.iterator();
    boolean anyChanges = false;
    while (correctionIterator.hasNext()) {
      LoadedCorrection correction = correctionIterator.next();
      if (correction.correct(loadedChecklist, ioc)) {
        // If the correction returned "true", that means it's obsolete;  remove it.
        if (removeIfObsolete) {
          correctionIterator.remove();
        }
        anyChanges = true;
      }
    }
    return anyChanges;
  }
  
  /**
   * Get the corrections for a checklist (from a different folder).
   */
  public static LoadedChecklistCorrections loadCorrections(
      LoadedChecklist checklist, String correctionsFolder) throws IOException {
    String id = checklist.getId();
    File file = new File(correctionsFolder, id + ".corrections");
    if (!file.exists()) {
      file.createNewFile();
      
      LoadedChecklistCorrections newCorrections = new LoadedChecklistCorrections();
      newCorrections.file = file;
      return newCorrections;
    } else {
      LoadedChecklistCorrections existingCorrections = LoadedChecklistCorrections.loadCorrections(file);
      return existingCorrections;
    }
  }

  /**
   * Get the corrections for a checklist (from a different folder).
   */
  public static LoadedChecklistCorrections loadCorrections(
      LoadedSplitChecklist checklist, String correctionsFolder) throws IOException {
    String id = checklist.getId();
    File file = new File(correctionsFolder, id + ".corrections");
    if (!file.exists()) {
      file.createNewFile();
      
      LoadedChecklistCorrections newCorrections = new LoadedChecklistCorrections();
      newCorrections.file = file;
      return newCorrections;
    } else {
      LoadedChecklistCorrections existingCorrections = LoadedChecklistCorrections.loadCorrections(file);
      return existingCorrections;
    }
  }

  public void write() throws IOException {
    Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),
        Charsets.UTF_8), 10240);
    ExportLines exportLines = CsvExportLines.fromWriter(out);
    try {
      exportLines.nextLine(new String[]{"Action", "Taxonomy", "ID", "Status"});
      for (LoadedCorrection row : rows) {
        exportLines.nextLine(new String[]{
           row.action.toString(),
           row.taxonomy.toString(),
           row.id,
           row.status
        });
      }
    } finally {
      exportLines.close();
    }
  }

  enum Action {
    add,
    remove,
    setStatus
  }
  
  enum Checklist {
    ioc,
    clements
  }

  static class LoadedCorrection implements Cloneable {
    public Action action;
    public Checklist taxonomy;
    public String id;
    public String status;
    
    @Override
    public LoadedCorrection clone() {
      try {
        return (LoadedCorrection) super.clone();
      } catch (CloneNotSupportedException e) {
        throw new RuntimeException(e);
      }
    }
    
    public boolean correct(
        LoadedChecklist loadedChecklist,
        MappedTaxonomy ioc) {
      switch (action) {
        case add:
          return add(loadedChecklist, ioc);
        case remove:
          return remove(loadedChecklist);
        case setStatus:
          return setStatus(loadedChecklist);
        default:
          throw new AssertionError();
      }
    }

    private boolean remove(LoadedChecklist loadedChecklist) {
      Iterator<LoadedRow> iterator = loadedChecklist.rows.iterator();
      boolean anyChanges = false;
      while (iterator.hasNext()) {
        LoadedRow row = iterator.next();
        if (taxonomy == Checklist.ioc) {
          if (idsAreEqual(row.iocId, id)) {
            iterator.remove();
            anyChanges = true; 
          }
        } else {
          if (idsAreEqual(row.clementsId, id)) {
            iterator.remove();
            anyChanges = true; 
          }
        }
      }
      // Return true if this correction is obsolete (no changes were made)
      return !anyChanges;
    }
    
    private final static Splitter SLASH_SPLITTER = Splitter.on('/');
    private boolean idsAreEqual(String first, String second) {
      if (first.equals(second)) {
        return true;
      }
      
      // For slash-sps, don't worry about order for matches
      if (first.contains("/") && second.contains("/")) {
        return ImmutableSet.copyOf(SLASH_SPLITTER.split(first)).equals(
            ImmutableSet.copyOf(SLASH_SPLITTER.split(second)));
      } else {
        return false;
      }
    }

    private boolean setStatus(LoadedChecklist loadedChecklist) {
      Iterator<LoadedRow> iterator = loadedChecklist.rows.iterator();
      boolean anyChanges = false;
      while (iterator.hasNext()) {
        LoadedRow row = iterator.next();
        if (taxonomy == Checklist.ioc) {
          if (idsAreEqual(row.iocId, id)) {
            if (!Objects.equal(row.status, status)
                || !Objects.equal(row.iocStatus, status)) {
              row.status = status;
              row.iocStatus = status;
              anyChanges = true;
            }
          }
        } else {
          if (idsAreEqual(row.clementsId, id)) {
            if (!Objects.equal(row.status, status)
                || !Objects.equal(row.iocStatus, status)) {
              row.status = status;
              row.iocStatus = status;
              anyChanges = true;
            }
          }
        }
      }
      
      // Return true if this record was obsolete (no changes were made)
      return !anyChanges;
    }

    private boolean add(LoadedChecklist loadedChecklist, MappedTaxonomy ioc) {
      for (LoadedRow row : loadedChecklist.rows) {
        if (taxonomy == Checklist.ioc) {
          if (idsAreEqual(id, row.iocId)) {
            return true;
          }
        } else {
          if (idsAreEqual(id, row.clementsId)) {
            return true;
          }
        }
      }
      
      LoadedRow row = new LoadedRow();
      row.iocStatus = status;
      row.status = status;

      if (taxonomy == Checklist.ioc) {
        row.iocId = id;

        SightingTaxon mapping;
        if (id.contains("/")) {
          SightingTaxon iocTaxon = SightingTaxons.newSpTaxon(SLASH_SPLITTER.split(id));
          row.iocName = iocTaxon.resolveInternal(ioc).getCommonName();
          mapping = ioc.getMapping(iocTaxon);
        } else {
          Taxon taxon = ioc.getTaxon(id);
          Preconditions.checkNotNull(taxon,
              "Taxon %s not found", id);
          row.iocName = taxon.getCommonName();
          
          // Find the clements mapping
          mapping = ioc.getMapping(taxon);
        }
        Preconditions.checkNotNull(mapping, "Could not map %s through %s", id, ioc.getId());
        Resolved resolve = mapping.resolve(ioc.getBaseTaxonomy());
        
        // Always bump up to at least Clements group
        if (resolve.getSmallestTaxonType() == Taxon.Type.subspecies) {
          mapping = resolve.getParentOfAtLeastType(Taxon.Type.group);
          resolve = mapping.resolve(ioc.getBaseTaxonomy());
        }

        // If it's a group sp., map upwards to species
        if (mapping.getType() == Type.SP
            && resolve.getSmallestTaxonType() == Taxon.Type.group) {
          mapping = resolve.getParentOfAtLeastType(Taxon.Type.species);
          resolve = mapping.resolve(ioc.getBaseTaxonomy());
        }
        
        // If it's still a group, then write the group to a special row and
        // the species in a main entry.
        if (resolve.getSmallestTaxonType() == Taxon.Type.group) {
          row.clementsGroupId = resolve.getTaxon().getId();
          mapping = resolve.getParentOfAtLeastType(Taxon.Type.species);
          resolve = mapping.resolve(ioc.getBaseTaxonomy());
        }
        
        // Hack some nasty resolution sub-cases
        if (mapping.getIds().size() > 1) {
          if (id.equals("spLarusveg")) {
            if (mapping.getIds().contains("spLarusarg")) {
              System.out.println("Swapping vega gull!");
              mapping = SightingTaxons.newSightingTaxon("spLarusarg");
              resolve = mapping.resolve(ioc.getBaseTaxonomy());
            } else {
              System.err.println("FAILED to swap vega gull!");
            }
          } else if (id.equalsIgnoreCase("spIolevir")) {
            if (mapping.getIds().contains("spIolepro")) {
              System.out.println("Swapping Olive Bulbul!");
              mapping = SightingTaxons.newSightingTaxon("spIolevir");
              resolve = mapping.resolve(ioc.getBaseTaxonomy());
            }
          }
        }
        
        row.clementsId = Joiner.on('/').join(mapping.getIds());
        row.clementsName = resolve.getCommonName();
        if (resolve.getSmallestTaxonType() == Taxon.Type.subspecies) {
          System.err.println("WHA? " + resolve.getCommonName());
        }
      } else {
        row.clementsId = id;
        
        Taxon taxon = ioc.getBaseTaxonomy().getTaxon(id);
        row.clementsName = taxon.getCommonName();
        
        // Resolve into the IOC taxonomy
        Resolved resolve = SightingTaxons.newSightingTaxon(taxon.getId()).resolve(ioc);
        // ... but bump right up to species
        if (resolve.getSmallestTaxonType() != Taxon.Type.species) {
          SightingTaxon species = resolve.getParentOfAtLeastType(Taxon.Type.species);
          resolve = species.resolveInternal(ioc);
        }
        
        row.iocId = Joiner.on('/').join(resolve.getSightingTaxon().getIds());
        row.iocName = resolve.getCommonName();
      }
      
      loadedChecklist.rows.add(row);
      return false;
    }
  }

  public void remove(Checklist checklist, String id) {
    LoadedCorrection correction = new LoadedCorrection();
    correction.action = Action.remove;
    correction.taxonomy = checklist;
    correction.id = id;
    rows.add(correction);
  }

  public void add(Checklist checklist, String id, String status) {
    LoadedCorrection correction = new LoadedCorrection();
    correction.action = Action.add;
    correction.taxonomy = checklist;
    correction.id = id;
    correction.status = status;
    rows.add(correction);
  }

  public void setStatus(Checklist checklist, String id, String status) {
    LoadedCorrection correction = new LoadedCorrection();
    correction.action = Action.setStatus;
    correction.taxonomy = checklist;
    correction.id = id;
    correction.status = status;
    rows.add(correction);
  }
}