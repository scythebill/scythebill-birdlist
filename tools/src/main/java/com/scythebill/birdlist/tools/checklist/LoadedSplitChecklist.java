/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nullable;

import com.google.common.base.Charsets;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.io.Files;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.ResolvedComparator;

/** A loaded version of a "split" checklist (a single taxonomy). */
public class LoadedSplitChecklist {
  private static final ImmutableSet<String> AMBIGUOUS_NAMES =
      ImmutableSet.of("Georgia", "Indonesia", "Russia", "High Seas");
 
  public File file;
  public Location location;
  public List<LoadedSplitChecklist.LoadedRow> rows = Lists.newArrayList();
  
  @Override
  public String toString() {
    if (AMBIGUOUS_NAMES.contains(location.getModelName())) {
      return String.format("%s (%s)", location.getModelName(), location.getParent().getModelName());
    }
    return location.getModelName();
  }

  public String getId() {
    return file.getName().substring(0, file.getName().indexOf('.'));
  }
  
  public void replaceFolder(File newFolder) {
    file = new File(newFolder, file.getName());
  }
  
  static LoadedSplitChecklist newEmptyChecklist(
      File file, @Nullable ImmutableMap<String, Location> locationsByCode) {
    LoadedSplitChecklist checklist = new LoadedSplitChecklist();
    checklist.file = file;

    String id = checklist.getId();    
    if (locationsByCode != null
        && !locationsByCode.containsKey(id)) {
      throw new IllegalArgumentException("Could not find location for id " + id + ", " + file.getAbsolutePath());
    }
    checklist.location = locationsByCode == null ? null : locationsByCode.get(id);
    
    return checklist;
  }
  
  public static LoadedSplitChecklist loadChecklist(
      File file, @Nullable ImmutableMap<String, Location> locationsByCode) throws IOException {
    LoadedSplitChecklist checklist = newEmptyChecklist(file, locationsByCode);
    ImportLines importLines = CsvImportLines.fromFile(file, Charsets.UTF_8);
    try {
      // Skip the header
      importLines.nextLine();
      while (true) {
        String[] nextLine = importLines.nextLine();
        if (nextLine == null) {
          break;
        }
        LoadedSplitChecklist.LoadedRow row = new LoadedSplitChecklist.LoadedRow();
        row.name = nextLine[0];
        row.id = nextLine[1];
        row.status = nextLine[2];
        checklist.rows.add(row);
      }
    } finally {
      importLines.close();
    }

    return checklist;
  }

  public static void clearChecklists(File folder) {
    File[] checklists = folder.listFiles(allCsv());
    
    for (File checklist : checklists) {
      if (!checklist.delete()) {
        throw new RuntimeException("Could not delete " + checklist);
      }
    }
  }

  public static void copyMissingChecklists(File oldFolder, File newFolder) throws IOException {
    // For files that had no corrections, copy verbatim
    File[] oldChecklists = oldFolder.listFiles(allCsv());
    
    for (File oldChecklist : oldChecklists) {
      File correctedChecklist = new File(newFolder, oldChecklist.getName());
      if (!correctedChecklist.exists()) {
        Files.copy(oldChecklist, correctedChecklist);
      }
    }
  }
  
  public void write(final Taxonomy taxonomy) throws IOException {
    sort(taxonomy);
    cleanDuplicates();
    
    Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),
        Charsets.UTF_8), 10240);
    ExportLines exportLines = CsvExportLines.fromWriter(out);
    LoadedRow previous = null;
    try {
      exportLines.nextLine(new String[]{"Name", "ID", "Status"});
      for (LoadedSplitChecklist.LoadedRow row : rows) {
        // Don't bother writing duplicate rows (which comes up a lot when merging)
        if (previous != null
            && previous.equalIds(row)) {
          continue;
        }
        
        exportLines.nextLine(new String[]{
           row.name,
           row.id,
           row.status,
        });
        previous = row;
      }
    } finally {
      exportLines.close();
    }
  }

  public void sort(final Taxonomy taxonomy) {
    Ordering<LoadedRow> ordering = ordering(taxonomy);
    // Sort the checklist
    Collections.sort(rows, ordering);
  }
  
  /**
   * Table of status mapping for merges.
   * Not symmetric - it only has half the needed entries, so flip the entries and read twice.
   * Missing entries should be mapped to empty. 
   */
  private final static ImmutableTable<String, String, String> STATUS_MERGE_TABLE;
  static {
    ImmutableTable.Builder<String, String, String> builder = ImmutableTable.builder();
    builder.put("extinct", "endemic", "endemic");
    builder.put("extinct", "rarity", "rarity");
    builder.put("extinct", "introduced", "introduced");
    builder.put("introduced", "escaped", "introduced");
    // Not obvious at all what to do here;  neither option seems reasonable.
    // E.g. Skylark in B.C. could be introduced or a vagrant depending on ssp.
    // Which to select?  Obviously, it's not NATIVE, but NATIVE shows up as empty
    // everywhere, so that seems sufficient.
    // builder.put("introduced", "rarity", "introduced");
    builder.put("escaped", "rarity", "rarity");
    STATUS_MERGE_TABLE = builder.build();
  }

  /**
   * Find duplicates, and eliminate (after carefully merging statuses).
   */
  private void cleanDuplicates() {
    for (int i = 0; i < rows.size() - 1; i++) {
      LoadedRow row = rows.get(i);
      
      while (i < rows.size() - 1 && rows.get(i + 1).id.equals(row.id)) {
        LoadedRow nextRow = rows.get(i + 1);
        String status = row.status;
        if (!status.isEmpty()) {
          String nextStatus = nextRow.status;
          if (!status.equals(nextStatus)) {
            if (nextStatus.isEmpty()) {
              row.status = "";
            } else {
              String mergedStatus = STATUS_MERGE_TABLE.get(status, nextStatus);
              if (mergedStatus == null) {
                mergedStatus = STATUS_MERGE_TABLE.get(nextStatus, status);
              }
              row.status = Strings.nullToEmpty(mergedStatus);
            }
          }
        }
        
        rows.remove(i + 1);
      }
    }
  }

  /**
   * Return an ordering of LoadedRows.
   */
  public static Ordering<LoadedRow> ordering(final Taxonomy taxonomy) {
    final ResolvedComparator comparator = new ResolvedComparator();
    Ordering<LoadedRow> ordering = new Ordering<LoadedRow>() {
      @Override
      public int compare(LoadedRow first, LoadedRow second) {
        SightingTaxon firstTaxon = toSightingTaxon(first.id);
        SightingTaxon secondTaxon = toSightingTaxon(second.id);
        return comparator.compare(
            firstTaxon.resolveInternal(taxonomy),
            secondTaxon.resolveInternal(taxonomy));
      }
      
      private SightingTaxon toSightingTaxon(String id) {
        if (id.contains("/")) {
          return SightingTaxons.newSpTaxon(Splitter.on('/').split(id));
        } else {
          return SightingTaxons.newSightingTaxon(id);
        }
      }
    };
    return ordering;
  }
  
  static class LoadedRow implements Cloneable {
    public String id;
    public String name;
    public String status;
    
    @Override
    public LoadedRow clone() {
      try {
        return (LoadedRow) super.clone();
      } catch (CloneNotSupportedException e) {
        throw new RuntimeException(e);
      }
    }
    
    public boolean equalIds(LoadedRow other) {
      return Objects.equal(Strings.emptyToNull(id), Strings.emptyToNull(other.id));
    }
    
    @Override
    public String toString() {
      return MoreObjects.toStringHelper(this)
          .add("id", id)
          .toString();
    }
 
  }

  
  static FilenameFilter allCsv() {
    return (dir, name) -> name.endsWith(".csv");    
  }

  public boolean containsTaxon(String id) {
    for (LoadedRow row : rows) {
      if (row.id.equals(id)) {
        return true;
      }
    }
    
    return false;
  }
}