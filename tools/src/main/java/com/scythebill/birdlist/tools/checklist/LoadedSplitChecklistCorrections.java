/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Iterator;
import java.util.List;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.tools.checklist.LoadedSplitChecklist.LoadedRow;

/**
 * Corrections for "split" checklists, where each change refers to one taxonomy, without any implicit changes to other taxonomies.  
 */
public class LoadedSplitChecklistCorrections {
  public File file;
  public final List<LoadedCorrection> rows = Lists.newArrayList();
  
  public String getId() {
    return file.getName().substring(0, file.getName().indexOf('.'));
  }
  
  public static LoadedSplitChecklistCorrections loadCorrections(File file) throws IOException {
    LoadedSplitChecklistCorrections corrections = new LoadedSplitChecklistCorrections();
    corrections.file = file;

    ImportLines importLines = CsvImportLines.fromFile(file, Charsets.UTF_8);
    try {
      // Skip the header
      importLines.nextLine();
      while (true) {
        String[] nextLine = importLines.nextLine();
        if (nextLine == null) {
          break;
        }
        LoadedCorrection row = new LoadedCorrection();
        row.action = Action.valueOf(nextLine[0]);
        row.taxonomyType = TaxonomyType.valueOf(nextLine[1]);
        row.id = nextLine[2];
        row.status = nextLine[3];
        corrections.rows.add(row);
      }
    } finally {
      importLines.close();
    }

    return corrections;
  }

  /** Correct a checklist. */
  public boolean correct(
      LoadedSplitChecklist loadedChecklist, TaxonomyType checklist, MappedTaxonomy ioc) {
    Iterator<LoadedCorrection> correctionIterator = rows.iterator();
    boolean anyChanges = false;
    try {
      while (correctionIterator.hasNext()) {
        LoadedCorrection correction = correctionIterator.next();
        if (correction.correct(loadedChecklist, checklist, ioc)) {
          anyChanges = true;
        }
      }
    } catch (Exception e) {
      throw new RuntimeException("Failed to correct " + loadedChecklist.getId(), e);
    }
    return anyChanges;
  }
  
  /**
   * Get the corrections for a checklist (from a different folder).
   */
  public static LoadedSplitChecklistCorrections loadCorrections(
      LoadedChecklist checklist, String correctionsFolder) throws IOException {
    String id = checklist.getId();
    File file = new File(correctionsFolder, id + ".corrections");
    if (!file.exists()) {
      file.createNewFile();
      
      LoadedSplitChecklistCorrections newCorrections = new LoadedSplitChecklistCorrections();
      newCorrections.file = file;
      return newCorrections;
    } else {
      LoadedSplitChecklistCorrections existingCorrections = LoadedSplitChecklistCorrections.loadCorrections(file);
      return existingCorrections;
    }
  }

  /**
   * Get the corrections for a checklist (from a different folder).
   */
  public static LoadedSplitChecklistCorrections loadCorrections(
      LoadedSplitChecklist checklist, String correctionsFolder) throws IOException {
    return loadCorrections(checklist.getId(), correctionsFolder);
  }

  /**
   * Get the corrections for a checklist (from a different folder).
   */
  public static LoadedSplitChecklistCorrections loadCorrections(
      String checklistId, String correctionsFolder) throws IOException {
    File file = new File(correctionsFolder, checklistId + ".corrections");
    if (!file.exists()) {
      file.createNewFile();
      
      LoadedSplitChecklistCorrections newCorrections = new LoadedSplitChecklistCorrections();
      newCorrections.file = file;
      return newCorrections;
    } else {
      LoadedSplitChecklistCorrections existingCorrections = LoadedSplitChecklistCorrections.loadCorrections(file);
      return existingCorrections;
    }
  }

  public void write() throws IOException {
    Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),
        Charsets.UTF_8), 10240);
    ExportLines exportLines = CsvExportLines.fromWriter(out);
    try {
      exportLines.nextLine(new String[]{"Action", "Taxonomy", "ID", "Status"});
      for (LoadedCorrection row : rows) {
        exportLines.nextLine(new String[]{
           row.action.toString(),
           row.taxonomyType.toString(),
           row.id,
           row.status
        });
      }
    } finally {
      exportLines.close();
    }
  }

  public enum Action {
    add,
    remove,
    setStatus
  }
  
  public enum TaxonomyType {
    ioc,
    clements
  }

  public static class LoadedCorrection implements Cloneable {
    public Action action;
    public TaxonomyType taxonomyType;
    public String id;
    public String status;
    
    @Override
    public LoadedCorrection clone() {
      try {
        return (LoadedCorrection) super.clone();
      } catch (CloneNotSupportedException e) {
        throw new RuntimeException(e);
      }
    }
    
    public LoadedCorrection mapToAlternateTaxonomy(MappedTaxonomy ioc) {
      Taxonomy clements = ioc.getBaseTaxonomy();
      TaxonomyType otherChecklist = taxonomyType == TaxonomyType.clements ? TaxonomyType.ioc : TaxonomyType.clements;
      
      SightingTaxon otherTaxonomyTaxon;
      if (otherChecklist == TaxonomyType.clements) {
        // Mapping IOC *to* Clements
        SightingTaxon clementsMapping;
        if (id.contains("/")) {
          SightingTaxon iocTaxon = SightingTaxons.newSpTaxon(SLASH_SPLITTER.split(id));
          clementsMapping = ioc.getMapping(iocTaxon);
        } else {
          Taxon taxon = ioc.getTaxon(id);
          Preconditions.checkNotNull(taxon,
              "Taxon %s not found", id);
          clementsMapping = ioc.getMapping(taxon);
        }
        
        Preconditions.checkNotNull(clementsMapping, "Could not map %s through %s", id, ioc.getId());
        Resolved resolve = clementsMapping.resolve(clements);
        if (resolve.getSmallestTaxonType() != Taxon.Type.species) {
          otherTaxonomyTaxon = resolve.getParentOfAtLeastType(Taxon.Type.species);
        } else {
          otherTaxonomyTaxon = clementsMapping;
        }
      } else { // otherChecklist == ioc
        Taxon clementsTaxon = clements.getTaxon(id);
        
        // Resolve into the IOC taxonomy
        Resolved resolve = SightingTaxons.newSightingTaxon(clementsTaxon.getId()).resolve(ioc);
        // ... but bump right up to species
        if (resolve.getSmallestTaxonType() != Taxon.Type.species) {
          SightingTaxon species = resolve.getParentOfAtLeastType(Taxon.Type.species);
          resolve = species.resolveInternal(ioc);
        }
        
        otherTaxonomyTaxon = resolve.getSightingTaxon();
      }
      
      LoadedCorrection correction = new LoadedCorrection();
      correction.action = action;
      correction.taxonomyType = otherChecklist;
      correction.id = SLASH_JOINER.join(otherTaxonomyTaxon.getIds());
      correction.status = status;
      return correction;      
    }
    
    public boolean correct(
        LoadedSplitChecklist loadedChecklist,
        TaxonomyType checklist,
        MappedTaxonomy ioc) {
      
      if (this.taxonomyType != checklist) {
        return false;
      }

      switch (action) {
        case add:
          return add(loadedChecklist, ioc);
        case remove:
          return remove(loadedChecklist);
        case setStatus:
          return setStatus(loadedChecklist);
        default:
          throw new AssertionError();
      }
    }

    private boolean remove(LoadedSplitChecklist loadedChecklist) {
      Iterator<LoadedRow> iterator = loadedChecklist.rows.iterator();
      boolean anyChanges = false;
      while (iterator.hasNext()) {
        LoadedRow row = iterator.next();
        if (idsAreEqual(row.id, id)) {
          iterator.remove();
          anyChanges = true;
        }
      }
      // Return true if this correction is obsolete (no changes were made)
      return !anyChanges;
    }
    
    private final static Splitter SLASH_SPLITTER = Splitter.on('/');
    private final static Joiner SLASH_JOINER = Joiner.on('/');
    private boolean idsAreEqual(String first, String second) {
      if (first.equals(second)) {
        return true;
      }
      
      // For slash-sps, don't worry about order for matches
      if (first.contains("/") && second.contains("/")) {
        return ImmutableSet.copyOf(SLASH_SPLITTER.split(first)).equals(
            ImmutableSet.copyOf(SLASH_SPLITTER.split(second)));
      } else {
        return false;
      }
    }

    private boolean setStatus(LoadedSplitChecklist loadedChecklist) {
      Iterator<LoadedRow> iterator = loadedChecklist.rows.iterator();
      boolean anyChanges = false;
      while (iterator.hasNext()) {
        LoadedRow row = iterator.next();
        if (idsAreEqual(row.id, id)) {
          if (!Objects.equal(row.status, status)) {
            row.status = status;
            anyChanges = true;
          }
        }
      }
      
      // Return true if this record was obsolete (no changes were made)
      return !anyChanges;
    }

    private boolean add(LoadedSplitChecklist loadedChecklist, MappedTaxonomy ioc) {
      for (LoadedRow row : loadedChecklist.rows) {
        if (idsAreEqual(id, row.id)) {
          // true == no changes made 
          return true;
        }
      }

      SightingTaxon taxon;
      if (id.contains("/")) {
        taxon = SightingTaxons.newSpTaxon(SLASH_SPLITTER.split(id));
      } else {
        taxon = SightingTaxons.newSightingTaxon(id);
      }
      
      Resolved resolved;
      if (taxonomyType == TaxonomyType.ioc) {
        resolved = taxon.resolveInternal(ioc); 
      } else {
        resolved = taxon.resolveInternal(ioc.getBaseTaxonomy());
      }
      
      LoadedRow row = new LoadedRow();
      row.status = status;
      row.id = id;
      row.name = resolved.getCommonName();
      
      loadedChecklist.rows.add(row);
      return false;
    }
  }

  public void remove(TaxonomyType checklist, String id) {
    LoadedCorrection correction = new LoadedCorrection();
    correction.action = Action.remove;
    correction.taxonomyType = checklist;
    correction.id = id;
    rows.add(correction);
  }

  public void add(TaxonomyType checklist, String id, String status) {
    LoadedCorrection correction = new LoadedCorrection();
    correction.action = Action.add;
    correction.taxonomyType = checklist;
    correction.id = id;
    correction.status = status;
    rows.add(correction);
  }

  public void setStatus(TaxonomyType checklist, String id, String status) {
    LoadedCorrection correction = new LoadedCorrection();
    correction.action = Action.setStatus;
    correction.taxonomyType = checklist;
    correction.id = id;
    correction.status = status;
    rows.add(correction);
  }
}