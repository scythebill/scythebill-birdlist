/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.scythebill.birdlist.tools.checklist;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hssf.eventusermodel.HSSFEventFactory;
import org.apache.poi.hssf.eventusermodel.HSSFListener;
import org.apache.poi.hssf.eventusermodel.HSSFRequest;
import org.apache.poi.hssf.record.BOFRecord;
import org.apache.poi.hssf.record.BlankRecord;
import org.apache.poi.hssf.record.BoolErrRecord;
import org.apache.poi.hssf.record.BoundSheetRecord;
import org.apache.poi.hssf.record.FormulaRecord;
import org.apache.poi.hssf.record.LabelRecord;
import org.apache.poi.hssf.record.LabelSSTRecord;
import org.apache.poi.hssf.record.NoteRecord;
import org.apache.poi.hssf.record.NumberRecord;
import org.apache.poi.hssf.record.RKRecord;
import org.apache.poi.hssf.record.Record;
import org.apache.poi.hssf.record.SSTRecord;
import org.apache.poi.hssf.record.StringRecord;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.tools.checklist.ChecklistRow.Status;

/**
 * Parse a PrintableBirdChecklists excel file.
 */
public class ParsePrintableChecklistExcel {
  private POIFSFileSystem fs;
  private String filename;

  public ParsePrintableChecklistExcel(POIFSFileSystem fs) {
    this.fs = fs;
  }

  public ParsePrintableChecklistExcel(String filename) throws IOException,
      FileNotFoundException {
    this(new POIFSFileSystem(new FileInputStream(filename)));
    this.filename = filename; 
  }

  public ParsePrintableChecklistExcel(File file) throws IOException {
    this(new POIFSFileSystem(new FileInputStream(file)));
    this.filename = file.getName(); 
  }

  /**
   * Initiates the processing of the XLS file to CSV
   * @return 
   */
  public List<ChecklistRow> process() throws IOException {
    HSSFEventFactory factory = new HSSFEventFactory();
    HSSFRequest request = new HSSFRequest();
    Processor processor = new Processor();
    request.addListenerForAllRecords(processor);
    factory.processWorkbookEvents(request, fs);
    return processor.rows;
  }
  
  private final static Pattern COMMON_NAME_OCCURRENCE_PATTERN =
      Pattern.compile("(.*) \\((.+)\\)");
  private final static Map<String, ChecklistRow.Status> STATUS_BY_STRING;
  static {
    ImmutableMap.Builder<String, ChecklistRow.Status> status = ImmutableMap.builder();
    status.put("e", ChecklistRow.Status.ESCAPED);
    status.put("escape", ChecklistRow.Status.ESCAPED);
    status.put("i", ChecklistRow.Status.INTRODUCED);
    status.put("introduced", ChecklistRow.Status.INTRODUCED);
    status.put("moribund", ChecklistRow.Status.NATIVE);
    // Maybe introduced?  Treat as introduced.
    status.put("i?", ChecklistRow.Status.INTRODUCED);
    // Ship-assisted:  introduced?
    status.put("sa", ChecklistRow.Status.INTRODUCED);
    status.put("i & sa", ChecklistRow.Status.INTRODUCED);
    status.put("sa?", ChecklistRow.Status.INTRODUCED);
    status.put("wi", ChecklistRow.Status.NATIVE);
    status.put("w?i", ChecklistRow.Status.NATIVE);
    status.put("we", ChecklistRow.Status.NATIVE);
    status.put("we?", ChecklistRow.Status.NATIVE);
    // Reintroduced - native?
    status.put("r", ChecklistRow.Status.NATIVE);
    status.put("ri", ChecklistRow.Status.NATIVE);
    // "e?" seems to largely be records that are considered a bit dubious,
    // but overly so.  Mark them as native.
    status.put("e?", ChecklistRow.Status.NATIVE);
    status.put("x?", ChecklistRow.Status.NATIVE);
    status.put("extinct?", ChecklistRow.Status.NATIVE);
    status.put("x", ChecklistRow.Status.EXTINCT);
    status.put("Orchid Island only", ChecklistRow.Status.NATIVE);
    STATUS_BY_STRING = status.build();
  }

  private static final ImmutableSet<String> INTERESTING_STATUSES =
      ImmutableSet.of(/*"e?",*/ "we?", "w?i", "sa?", "sa", "moribund");
  class Processor implements HSSFListener {
    private final List<ChecklistRow> rows = Lists.newArrayList();
    private ChecklistRow currentChecklistRow;
    private int lastRowNumber = -1;
    // Records we pick up as we process
    private SSTRecord sstRecord;
    

    @Override
    public void processRecord(Record record) {
      int thisRow = -1;
      int thisColumn = -1;
      String thisStr = null;

      switch (record.getSid()) {
        // sids to ignore
        case BoundSheetRecord.sid:
        case BOFRecord.sid:
        case BlankRecord.sid:
        case NumberRecord.sid:
        default:
          return;
        // sids with data we wouldn't want to drop if it's there
        case BoolErrRecord.sid:
        case FormulaRecord.sid:
        case StringRecord.sid:
        case NoteRecord.sid:
        case RKRecord.sid:
          throw new IllegalStateException("Unexpected record: " + record);

        case SSTRecord.sid:
          sstRecord = (SSTRecord) record;
          break;
          
        case LabelRecord.sid:
          LabelRecord label = (LabelRecord) record;
          thisRow = label.getRow();
          thisColumn = label.getColumn();
          thisStr = label.getValue();
          break;
        case LabelSSTRecord.sid:
          LabelSSTRecord labelSst = (LabelSSTRecord) record;
          thisRow = labelSst.getRow();
          thisColumn = labelSst.getColumn();
          thisStr = sstRecord.getString(labelSst.getSSTIndex()).toString();
          break;
      }
  
      // Ignore the first two rows
      if (thisRow < 2) {
        return;
      }
      
      // Ignore all but the first two columns
      if (thisColumn >= 2) {
        return;
      }

      // Handle new row
      if (thisRow != -1 && thisRow != lastRowNumber) {
        if (currentChecklistRow != null) {
          if (currentChecklistRow.getScientificName() != null) {
            rows.add(currentChecklistRow);
          }
        }
        currentChecklistRow = new ChecklistRow();
        lastRowNumber = thisRow;
      }
      
      // If we got something to print out, do so
      if (thisStr != null) {
        if (thisColumn == 0) {
          Matcher matcher = COMMON_NAME_OCCURRENCE_PATTERN.matcher(thisStr);
          if (matcher.matches()) {
            currentChecklistRow.setCommonName(matcher.group(1));
            String statusString = matcher.group(2);
            
            if (INTERESTING_STATUSES.contains(statusString)) {
              System.err.println(filename + " " + thisStr);
            }
            Status status = STATUS_BY_STRING.get(matcher.group(2));
            if (status == null) {
              throw new IllegalStateException("Unknown status for " + thisStr);
            }
            currentChecklistRow.setStatus(status);
          } else {
            currentChecklistRow.setCommonName(thisStr);
          }
        } else if (thisColumn == 1) {
          currentChecklistRow.setScientificName(thisStr);
        }
      }
    }
  }

  public static void main(String[] args) throws Exception {
    if (args.length < 1) {
      System.out.println("Use:");
      System.out.println("  XLS2CSVmra <xls file> [min columns]");
      System.exit(1);
    }


    ParsePrintableChecklistExcel xls2csv = new ParsePrintableChecklistExcel(args[0]);
    List<ChecklistRow> rows = xls2csv.process();
    System.err.println(Joiner.on('\n').join(rows));
  }
}