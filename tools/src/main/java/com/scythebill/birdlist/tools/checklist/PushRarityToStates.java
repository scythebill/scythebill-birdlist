/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;
import com.scythebill.birdlist.tools.checklist.LoadedChecklistCorrections.Checklist;

/**
 * Finds endemics in a set of checklists.
 */
public class PushRarityToStates {
  private final LocationSet locations;
  private final PredefinedLocations predefinedLocations;
  private final File checklistsFolder;
  private final File correctionsFolder;
  private final ImmutableMap<String, Location> locationsByCode;
  private final HashMultimap<String, String> iocRaritiesByCountryCode = HashMultimap.create();
  private final static ImmutableSet<String> INCLUDED_STATES = ImmutableSet.of(
      "YE-HD-SOC",
      "PT-20",
      "PT-30",
      "EC-W");

  public static void main(String[] args) throws Exception {
    File checklistsFolder = new File(args[0]);
    Preconditions.checkState(checklistsFolder.exists());

    File correctionsFolder = new File(args[1]);
    Preconditions.checkState(correctionsFolder.exists());

    PushRarityToStates pushRarities = new PushRarityToStates(checklistsFolder, correctionsFolder);
    pushRarities.loadAllCountryChecklists();
    pushRarities.correctAllStateChecklists();
  }
  
  PushRarityToStates(File checklistsFolder, File correctionsFolder) {
    this.checklistsFolder = checklistsFolder;
    this.correctionsFolder = correctionsFolder;
    this.locations = ReportSets.newReportSet(new TaxonomyImpl()).getLocations();
    this.predefinedLocations = PredefinedLocations.loadAndParse();
    Map<String, Location> locationsByCode = Maps.newHashMap();
    for (Location root : locations.rootLocations()) {
      buildLocationsMap(locationsByCode, root);
    }
    this.locationsByCode = ImmutableMap.copyOf(locationsByCode);
  }

  private void loadAllCountryChecklists() throws IOException {
    File[] files = checklistsFolder.listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        return name.endsWith(".csv");
      }
    });
    for (File file : files) {
      LoadedChecklist checklist = LoadedChecklist.loadChecklist(file, locationsByCode);
      if (checklist.location.getType() == Location.Type.country) {
        for (LoadedChecklist.LoadedRow row : checklist.rows) {
          if ("rarity".equals(row.status)) {
            iocRaritiesByCountryCode.put(checklist.getId(), row.iocId);
          }
        }
      }
    }
  }

  private void correctAllStateChecklists() throws IOException {
    File[] files = checklistsFolder.listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        return name.endsWith(".csv");
      }
    });
    for (File file : files) {
      LoadedChecklist checklist = LoadedChecklist.loadChecklist(file, locationsByCode);
      // Find every *non*-country checklist (skipping the INCLUDED_STATES, which are states
      // that have their own checklists independent of the political country to which they belong.
      if (checklist.location.getType() != Location.Type.country
          && !INCLUDED_STATES.contains(checklist.getId())) {
        Set<String> countryRarities = getCountryRarities(checklist.getId()); 
        if (!countryRarities.isEmpty()) {
          Set<String> iocIdsThatShouldBeRarities = Sets.newLinkedHashSet();
          for (LoadedChecklist.LoadedRow row : checklist.rows) {
            if (countryRarities.contains(row.iocId)
                && Strings.isNullOrEmpty(row.iocStatus)) {
              iocIdsThatShouldBeRarities.add(row.iocId);
            }
          }
          
          if (!iocIdsThatShouldBeRarities.isEmpty()) {
            LoadedChecklistCorrections corrections = LoadedChecklistCorrections.loadCorrections(
                checklist, correctionsFolder.getAbsolutePath());
            for (String iocId : iocIdsThatShouldBeRarities) {
              corrections.setStatus(Checklist.ioc, iocId, "rarity");
            }
            corrections.write();
          }
        }
      }
    }
  }

  private Set<String> getCountryRarities(String id) {
    int lastHyphen = id.lastIndexOf('-');
    if (lastHyphen < 0) {
      return ImmutableSet.<String>of();
    }
    
    String parentId = id.substring(0, lastHyphen);
    if (iocRaritiesByCountryCode.containsKey(parentId)) {
      return iocRaritiesByCountryCode.get(parentId);
    }
    
    return ImmutableSet.<String>of();
  }

  private void buildLocationsMap(Map<String, Location> locationsByCode, Location location) {
    if (location.getEbirdCode() != null) {
      String ebirdCode = location.getEbirdCode();
      if (location.getType() == Location.Type.state
          && location.getParent().getModelName().equals("United States")) {
        ebirdCode = "US-" + ebirdCode;
      }
      locationsByCode.put(ebirdCode, location);
    }

    for (Location child : location.contents()) {
      buildLocationsMap(locationsByCode, child);
    }

    for (PredefinedLocation predefined : predefinedLocations.getPredefinedLocations(location)) {
      buildLocationsMap(locationsByCode, predefined.create(locations, location));
    }
  }
}
