package com.scythebill.birdlist.tools.checklist;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.FileConverter;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Ordering;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.SettableFuture;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.tools.checklist.LoadedSplitChecklistCorrections.TaxonomyType;
import com.scythebill.birdlist.ui.util.ListListModel;
import com.scythebill.birdlist.ui.util.UIUtils;

class ReconcileChecklists {
  private final File checklistsFolder;
  private final Multimap<String, LoadedSplitChecklist> checklistById =
      ArrayListMultimap.create();
  private final Map<String, LoadedSplitChecklistCorrections> correctionsMap = Maps.newHashMap();
  private Taxonomy taxonomy;
  private Parameters parameters;
  private ChecklistLocations locations;

  static class Parameters {
    @Parameter(names = "-correctionsFolder", required = true, converter = FileConverter.class)
    private File correctionsFolder;

    @Parameter(names = "-checklistsFolder", required = true, converter = FileConverter.class)
    private File checklistsFolder;
  }
  
  public static boolean main(Taxonomy taxonomy, String[] args) throws Exception {
    Parameters parameters = new Parameters();
    new JCommander(parameters, args);
    
    Preconditions.checkState(parameters.checklistsFolder.exists());

    ReconcileChecklists reconcile = new ReconcileChecklists(taxonomy, parameters); 

    reconcile.loadAllChecklists();
    
    return reconcile.reconcileAllSp() > 0;
  }

  ReconcileChecklists(Taxonomy taxonomy, Parameters parameters) {
    this.taxonomy = taxonomy;
    this.checklistsFolder = parameters.checklistsFolder;
    this.parameters = parameters;
    this.locations = new ChecklistLocations();
  }

  private void loadAllChecklists() throws IOException {
    File[] files = checklistsFolder.listFiles((dir, name) -> name.endsWith(".csv"));
    for (File file : files) {
      LoadedSplitChecklist checklist = LoadedSplitChecklist.loadChecklist(file, locations.locationsByCode);
      for (LoadedSplitChecklist.LoadedRow row : checklist.rows) {
        if (row.id.contains("/")) {
          checklistById.put(row.id, checklist);
        }
      }
    }
  }

  public int reconcileAllSp() throws IOException {
    int total = checklistById.keySet().size();
    int count = 0;
    for (String id : ImmutableSet.copyOf(checklistById.keySet())) {
      if (!id.contains("/")) {
        continue;
      }
      Collection<LoadedSplitChecklist> checklists = checklistById.get(id);
      reconcile(
          String.format("%s of %s (%s)", ++count, total, id),
          ImmutableList.copyOf(checklists),
          id, Splitter.on('/').split(id),
          taxonomy);
    }
    
    return total;
  }
  
  private void reconcile(
      String title,
      final Collection<LoadedSplitChecklist> checklists,
      final String spId,
      final Iterable<String> ids,
      final Taxonomy taxonomy) throws IOException {
    final SettableFuture<Void> done = SettableFuture.create();
    SwingUtilities.invokeLater(() -> {
      JFrame frame = new JFrame();
      frame.setTitle(title);
      frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
      frame.getContentPane().setLayout(new BorderLayout());
      frame.getContentPane().add(
          new ReconcilePanel(checklists, spId, ImmutableList.copyOf(ids), taxonomy, done));
      // Lazy code to fill the screen...
      frame.setBounds(0, 0, 2000, 2000);
      UIUtils.keepWindowOnScreen(frame);
      frame.setVisible(true);
    });

    Futures.getUnchecked(done);
    
    for (LoadedSplitChecklist checklist : checklists) {
      // Write the updated checklist
      checklist.write(taxonomy);
      // And update the index
      checklistById.values().remove(checklist);
      for (LoadedSplitChecklist.LoadedRow row : checklist.rows) {
        if (!row.id.contains("/")) {
          continue;
        }
        checklistById.put(row.id, checklist);
      }
    }
  }
  
  class ReconcilePanel extends JPanel {
    private ListListModel<LoadedSplitChecklist> originalModel;
    private final Map<String, ListListModel<LoadedSplitChecklist>> singleModels = Maps.newHashMap();
    private ListListModel<LoadedSplitChecklist> allModel;
    private JList<LoadedSplitChecklist> originalList;
    private String spId;
    private ImmutableList<String> ids;
    private SettableFuture<Void> doneFuture;
    private Taxonomy taxonomy;

    public ReconcilePanel(
        Collection<LoadedSplitChecklist> checklists, String spId,
        ImmutableList<String> ids,
        Taxonomy taxonomy,
        SettableFuture<Void> doneFuture) {
      this.spId = spId;
      this.ids = ids;
      this.taxonomy = taxonomy;
      this.doneFuture = doneFuture;
      setLayout(new BorderLayout());
      
      checklists = ImmutableSortedSet.copyOf(Ordering.usingToString(), checklists);
      JPanel center = new JPanel();
      add(center, BorderLayout.CENTER);
      center.setLayout(new GridLayout(2, 0, 5, 5));
      
      center.add(new JLabel(""));
      for (String id : ids) {
        final String idInLoop = id;
        JPanel buttonAndRange = new JPanel();
        buttonAndRange.setLayout(new BoxLayout(buttonAndRange, BoxLayout.Y_AXIS));
        center.add(buttonAndRange);
        Taxon taxon = taxonomy.getTaxon(id);
        JButton button = new JButton(taxon.getCommonName());
        JTextArea range = new JTextArea();
        range.setLineWrap(true);
        range.setText(TaxonUtils.getRange((Species) taxon));
        buttonAndRange.add(button);
        buttonAndRange.add(range);
        
        button.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            List<LoadedSplitChecklist> checklists = originalList.getSelectedValuesList();
            for (LoadedSplitChecklist checklist : checklists) {
              singleModels.get(idInLoop).asList().add(checklist);
              originalModel.asList().remove(checklist);
            }
          }         
        });
      }
      
      JButton all = new JButton("All");
      center.add(all);
      all.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          List<LoadedSplitChecklist> checklists = originalList.getSelectedValuesList();
          for (LoadedSplitChecklist checklist : checklists) {
            allModel.asList().add(checklist);
            originalModel.asList().remove(checklist);
          }
        }         
      });
      
      originalModel = new ListListModel<LoadedSplitChecklist>();
      originalModel.asList().addAll(checklists);
      JScrollPane originalScrollPane = newList(originalModel);
      originalList = (JList<LoadedSplitChecklist>) originalScrollPane.getViewport().getView();
      center.add(originalScrollPane);
      
      for (String id : ids) {
        ListListModel<LoadedSplitChecklist> listModel = new ListListModel<>();
        center.add(newList(listModel));
        singleModels.put(id, listModel);
      }
      
      allModel = new ListListModel<LoadedSplitChecklist>();
      center.add(newList(allModel));
      
      JButton done = new JButton("Done");
      done.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent event) {
          try {
            done();
          } catch (IOException e) {
            throw new RuntimeException(e);
          }
        }
      });
      add(done, BorderLayout.SOUTH);
    }
    
    private void done() throws IOException {
      TaxonomyType taxonomyType = taxonomy instanceof MappedTaxonomy ? TaxonomyType.ioc : TaxonomyType.clements;
      for (Map.Entry<String, ListListModel<LoadedSplitChecklist>> entry : singleModels.entrySet()) {
        for (LoadedSplitChecklist checklist : entry.getValue().asList()) {
          LoadedSplitChecklistCorrections corrections = getCorrections(checklist);
          // Add each individual "add"
          for (LoadedSplitChecklist.LoadedRow row : checklist.rows) {
            if (row.id.equals(spId)) {
              corrections.remove(taxonomyType, row.id);
              corrections.add(taxonomyType, entry.getKey(), row.status);
              break;
            }
          }
          corrections.write();
        }
      }
      
      for (LoadedSplitChecklist checklist : allModel.asList()) {
        LoadedSplitChecklistCorrections corrections = getCorrections(checklist);
        for (int i = 0; i < checklist.rows.size(); i++) {
          LoadedSplitChecklist.LoadedRow currentRow = checklist.rows.get(i);
          if (currentRow.id.equals(spId)) {
            corrections.remove(taxonomyType, currentRow.id);
            for (String id : ids) {
              corrections.add(taxonomyType, id, currentRow.status);
            }
            break;
          }
        }
        corrections.write();
      }
      
      SwingUtilities.getWindowAncestor(this).setVisible(false);
      doneFuture.set(null);
    }
    
    private JScrollPane newList(ListListModel<LoadedSplitChecklist> model) {
      JList<LoadedSplitChecklist> list = new JList<>(model);
      JScrollPane scrollPane = new JScrollPane(list);
      scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);;
      scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      return scrollPane;
    }
  }

  public LoadedSplitChecklistCorrections getCorrections(LoadedSplitChecklist checklist) throws IOException {
    String id = checklist.getId();
    if (correctionsMap.containsKey(id)) {
      return correctionsMap.get(id);
    }
    
    LoadedSplitChecklistCorrections corrections = LoadedSplitChecklistCorrections
        .loadCorrections(checklist, parameters.correctionsFolder.getPath());
    correctionsMap.put(id, corrections);
    return corrections;
  }
}
