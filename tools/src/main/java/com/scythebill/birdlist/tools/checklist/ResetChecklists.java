/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import java.io.File;
import java.io.IOException;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.FileConverter;
import com.google.common.base.Preconditions;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.tools.checklist.LoadedSplitChecklist.LoadedRow;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;

/**
 * Resets the checklist system to the latest version, by:
 * - Moving fully reconciled checklists into the "download" folder
 * - Erasing all corrections
 */
public class ResetChecklists {
  public static void main(String[] args) throws Exception {
    new ResetChecklists(args).run();
  }

  @Parameter(names = "-clements", required = true)
  private String clements;

  @Parameter(names = "-ioc", required = true)
  private String ioc;

  @Parameter(names = "-baseFolder", required = true, converter = FileConverter.class)
  private File baseFolder;

  @Parameter(names = "-correctionsFolder", required = true, converter = FileConverter.class)
  private File correctionsFolder;

  @Parameter(names = "-finalFolder", required = true, converter = FileConverter.class)
  private File finalFolder;

  ResetChecklists(String[] args) {
    new JCommander(this, args);
  }
  
  public void run() throws Exception {
    MappedTaxonomy ioc = ToolTaxonUtils.getMappedTaxonomy(clements, this.ioc);
    
    System.out.println("Copying final checklists (Clements)...");
    copyExistingFiles(ioc.getBaseTaxonomy(), new File(finalFolder, "clements"), new File(baseFolder, "clements"));

    System.out.println("Copying final checklists (IOC)...");
    copyExistingFiles(ioc, new File(finalFolder, "ioc"), new File(baseFolder, "ioc"));

    System.out.println("Deleting corrections...");
    File[] corrections = correctionsFolder.listFiles((dir, name) -> name.endsWith(".corrections"));    
    for (File correction : corrections) {
      correction.delete();
    }
    System.exit(0);
  }
  
  private void copyExistingFiles(Taxonomy taxonomy, File from, File to) throws IOException {
    Preconditions.checkState(
        from.exists(),
        "Folder %s (existing checklists) doesn't exist",
        from.getPath());
    Preconditions.checkState(
        to.exists(),
        "Folder %s (base) doesn't exist",
        to.getPath());
    
    File[] files = from.listFiles((dir, name) -> name.endsWith(".csv"));
        // Copy each checklist file back to the base
    for (File file : files) {
      LoadedSplitChecklist checklist = LoadedSplitChecklist.loadChecklist(file, null);
      // Clear "endemic" in all the base checklists, because that always gets recomputed
      for (LoadedRow row : checklist.rows) {
        if ("endemic".equals(row.status)) {
          row.status = "";
        }
      }
      checklist.replaceFolder(to);
      checklist.write(taxonomy);
    }
    
  }
}
