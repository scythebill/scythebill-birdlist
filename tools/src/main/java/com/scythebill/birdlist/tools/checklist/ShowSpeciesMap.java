/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.internal.Lists;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Splitter;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import com.google.common.io.Resources;
import com.google.common.net.UrlEscapers;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.SettableFuture;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.components.SpeciesIndexerPanelConfigurer;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Writes out species maps for all species.
 */
@SuppressWarnings("unused")
class ShowSpeciesMap {
  @Parameter(names = "-transposeFile")
  private String transposeFile;

  @Parameter(names = "-outputFolder")
  private String outputFolder;

  @Parameter(names = "-forcedRegion")
  private String forcedRegion;

  private Taxonomy taxonomy;
  
  private PredefinedLocations predefinedLocations = PredefinedLocations.loadAndParse();

  private com.scythebill.birdlist.ui.panels.ShowSpeciesMap uiMap;

  private ReportSet reportSet;
  
  public ShowSpeciesMap(Taxonomy baseTaxonomy, Taxonomy taxonomy) {
    this.taxonomy = taxonomy;
    this.reportSet = ReportSets.newReportSet(baseTaxonomy);
    this.uiMap = new com.scythebill.birdlist.ui.panels.ShowSpeciesMap(
        predefinedLocations, reportSet);
  }

  public static void main(Taxonomy baseTaxonomy, Taxonomy taxonomy, String[] args) throws Exception {
    ShowSpeciesMap showSpeciesMap = new ShowSpeciesMap(baseTaxonomy, taxonomy);
    new JCommander(showSpeciesMap, args);
    
    if (showSpeciesMap.outputFolder != null) {
      File outputFolder = new File(showSpeciesMap.outputFolder);
      if (!outputFolder.exists()) {
        outputFolder.mkdirs();
      }

      showSpeciesMap.writeAllRanges();
    } else {
      while (true) {
        SpeciesToShow speciesToShow = showSpeciesMap.pickSpecies();
        if (speciesToShow != null) {
          showSpeciesMap.showRange(speciesToShow);
        }
      }
    }
  }

  /** Statuses, ordered from most important to least. */
  private final static ImmutableMap<String, Integer> STATUS_INDICES = ImmutableMap.<String, Integer>builder()
      .put("endemic", 1)
      .put("regular", 2)
      .put("introduced", 4)
      .put("rarity", 3)
      .put("escaped", 5)
      .put("extinct", 6)
      .build();
  
  private final static Ordering<String> STATUS_ORDER = Ordering.explicit(ImmutableList.copyOf(STATUS_INDICES.keySet()));
  private final static ImmutableSet<String> SMALL_COUNTRIES = ImmutableSet.of(
      "PF",
      "CV",
      "CP",
      "CX",
      "CC",
      "AC",
      "CK",
      "FJ",
      "TF",
      "HM",
      "KI",
      "LI",
      "MH",
      "MU",
      "YT",
      "FM",
      "NR",
      "NU",
      "NF",
      "PN",
      "RE",
      "SC",
      "WS",
      "TO",
      "TK",
      "TV",
      "GS",
      "SH",
      "WF",
      "VU",
      "AD",
      "SM",
      "HK",
      "SG",
      "AQ",
      "BH",
      "BM",
      "AS",
      "KM",
      "SC",
      "GU",
      "PW",
      "MP",
      "TL",
      "IC",
      "TC",
      "FO");
      
  private void writeAllRanges() throws IOException {
    // Mark all the files that already existed
    Set<String> existingFiles = Sets.newHashSet(
        new File(outputFolder).list((dir, name) -> name.endsWith(".html")));
    TaxonUtils.visitTaxa(taxonomy, new TaxonVisitor() {
      @Override
      public boolean visitTaxon(Taxon taxon) {
        if (taxon.isDisabled()) {
          return false;
        }
        if (taxon instanceof Species) {
          File file = new File(
              outputFolder,
              String.format(
                  "%05d-%s.html",
                  taxon.getTaxonomyIndex(),
                  taxon.getCommonName()));
          existingFiles.remove(file.getName());
          try (Writer writer = new FileWriter(file)) {
            uiMap.writeRange(reportSet, (Species) taxon, writer);
          } catch (IOException e) {
            throw new RuntimeException(e);
          }
          return false;
        }
        
        return true;
      }
    });
    
    // Remove any files that existed in this directory and weren't newly written.
    for (String existingFile : existingFiles) {
      new File(outputFolder, existingFile).delete();
    }
  }

  private void showRange(SpeciesToShow speciesToShow) throws IOException {
    Species taxon = (Species) taxonomy.getTaxon(speciesToShow.id);
    File file = File.createTempFile("range", ".html");
    Writer writer = new FileWriter(file);
    try {
      uiMap.writeRange(reportSet, taxon, writer);
    } finally {
      writer.close();
    }
    
    Desktop.getDesktop().open(file);
  }
  
  private SpeciesToShow pickSpecies() {
    final SettableFuture<SpeciesToShow> done = SettableFuture.create();
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        JFrame frame = new JFrame();
        frame.setTitle("Show species map");
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(new BorderLayout());
        frame.getContentPane().add(new SpeciesPanel(done));
        frame.setBounds(0, 0, 600, 600);
        UIUtils.keepWindowOnScreen(frame);
        frame.setVisible(true);
        closeWhenDone(done, frame);
      }
    });

    return Futures.getUnchecked(done);
  }
  
  static <T> void closeWhenDone(ListenableFuture<T> future, final Frame frame) {
    Futures.addCallback(future, new FutureCallback<T>() {
      @Override
      public void onFailure(Throwable t) {
        onSuccess(null);
      }

      @Override
      public void onSuccess(T value) {
        SwingUtilities.invokeLater(new Runnable() {
          @Override public void run() {
            frame.dispose();
          }
        });
      }
    }, MoreExecutors.directExecutor());
  }
  
  static class SpeciesToShow {
    public final String id;

    SpeciesToShow(String id) {
      this.id = id;
    }
  }
  
  class SpeciesPanel extends JPanel {
    private IndexerPanel<String> indexer;

    SpeciesPanel(final SettableFuture<SpeciesToShow> done) {
      BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
      setLayout(layout);
      
      add(new JLabel("Species to map"));
      add(indexer = iocIndexer());
      add(new JButton(new AbstractAction("Done") {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          String id = indexer.getValue();
          if (id != null) {
            done.set(new SpeciesToShow(id));
          }
        }
      }));
    }
    
    private IndexerPanel<String> iocIndexer() {
      IndexerPanel<String> indexer = new IndexerPanel<String>();
      SpeciesIndexerPanelConfigurer.unconfigured().configure(indexer, taxonomy);
      return indexer;
    }
  }  
}
