/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;

/**
 * The full workflow of downloading, parsing, etc. checklists.
 */
public class SplitChecklistWorkflow {
  public static void main(String[] args) throws Exception {
    new SplitChecklistWorkflow(args).run();
  }

  @Parameter(names = "-baseFolder", required = true)
  private String baseFolder;

  @Parameter(names = "-clements", required = true)
  private String clements;

  @Parameter(names = "-ioc", required = true)
  private String ioc;

  @Parameter(names = "-correctionsFolder", required = true)
  private String correctionsFolder;

  @Parameter(names = "-correctedFolder", required = true)
  private String correctedFolder;

  @Parameter(names = "-finalFolder", required = true)
  private String finalFolder;

  @Parameter(names = "-mapsFolder")
  private String mapsFolder;

  @Parameter(names = "-mapsFolderClements")
  private String mapsFolderClements;

  SplitChecklistWorkflow(String[] args) {
    new JCommander(this, args);
  }
  
  public void run() throws Exception {
    Taxonomy clements = ToolTaxonUtils.getTaxonomy(this.clements);
    MappedTaxonomy ioc = ToolTaxonUtils.getMappedTaxonomy(clements, this.ioc);
    
    System.out.println("Correcting (Clements)...");
    CorrectChecklists.main(ioc, new String[] {
       "-input", baseFolder + "/clements",
       "-corrected", correctedFolder + "/clements",
       "-corrections", correctionsFolder,
       "-correctClements"
    });

    System.out.println("Correcting (IOC)...");
    CorrectChecklists.main(ioc, new String[] {
       "-input", baseFolder + "/ioc",
       "-corrected", correctedFolder + "/ioc",
       "-corrections", correctionsFolder
    });

    System.out.println("Reconciling (Clements)...");
    if (ReconcileChecklists.main(
        clements,
        new String[] {
            "-correctionsFolder", correctionsFolder,
            "-checklistsFolder", correctedFolder + "/clements",
        })) {
      System.out.println("Re-correcting (Clements)...");
      CorrectChecklists.main(ioc, new String[] {
         "-input", baseFolder + "/clements",
         "-corrected", correctedFolder + "/clements",
         "-corrections", correctionsFolder,
         "-correctClements"
      });      
    };
    
    System.out.println("Reconciling (IOC)...");
    if (ReconcileChecklists.main(
        ioc,
        new String[] {
            "-correctionsFolder", correctionsFolder,
            "-checklistsFolder", correctedFolder + "/ioc",
        })) {
      System.out.println("Re-correcting (IOC)...");
      CorrectChecklists.main(ioc, new String[] {
         "-input", baseFolder + "/ioc",
         "-corrected", correctedFolder + "/ioc",
         "-corrections", correctionsFolder
      });      
    };

    System.out.println("Finding endemics (Clements)...");
    FindEndemics.main(clements, new String[] {
        correctedFolder + "/clements",
        finalFolder + "/clements"});
    System.out.println("Finding endemics (IOC)...");
    FindEndemics.main(ioc, new String[] {
        correctedFolder + "/ioc",
        finalFolder + "/ioc"});
    
    System.out.println("Transposing (Clements)...");
    TransposeRanges.main(new String[] {
        finalFolder + "/clements",
        finalFolder + "/clements.transpose"});
    System.out.println("Transposing (IOC)...");
    TransposeRanges.main(new String[] {
        finalFolder + "/ioc",
        finalFolder + "/ioc.transpose"});
    
    if (mapsFolder != null) {
      System.out.println("Writing maps (IOC)...");
      ShowSpeciesMap.main(
          clements,
          ioc,
          new String[] {
              "-transposeFile", finalFolder + "/ioc.transpose",
              "-outputFolder", mapsFolder
      });
    }
    if (mapsFolderClements != null) {
      System.out.println("Writing maps (Clements)...");
      ShowSpeciesMap.main(
          clements,
          clements,
          new String[] {
              "-transposeFile", finalFolder + "/clements.transpose",
              "-outputFolder", mapsFolderClements
      });
    }

    System.exit(0);
  }
}
