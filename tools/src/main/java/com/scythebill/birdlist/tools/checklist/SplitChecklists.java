/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import org.xml.sax.SAXException;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.collect.ImmutableList;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;

/**
 * Tool for splitting the original checklists into separate IOC and eBird checklists.
 */
public class SplitChecklists {
  @Parameter(names = "-checklistFolder")
  private String checklistFolder;

  @Parameter(names = "-ebirdOutFolder")
  private String ebirdOutFolder;

  @Parameter(names = "-iocOutFolder")
  private String iocOutFolder;

  @Parameter(names = "-clements")
  private String clements;

  @Parameter(names = "-ioc")
  private String ioc;

  public static void main(String[] args) throws Exception {
    final SplitChecklists splitChecklists = new SplitChecklists();
    new JCommander(splitChecklists, args);
    splitChecklists.run();
  }
  
  private SplitChecklists() {
  }

  private void run() throws IOException, SAXException {
    MappedTaxonomy ioc = ToolTaxonUtils.getMappedTaxonomy(this.clements, this.ioc);
    Taxonomy clements = ioc.getBaseTaxonomy();
    
    if (!new File(checklistFolder).exists()) {
      throw new IllegalArgumentException("Folder " + checklistFolder + " does not exist");
    }
    File ebirdOutFolder = new File(this.ebirdOutFolder);
    if (!ebirdOutFolder.exists() && !ebirdOutFolder.mkdirs()) {
      throw new IllegalArgumentException("Could not create" + ebirdOutFolder);
    }
    File iocOutFolder = new File(this.iocOutFolder);
    if (!iocOutFolder.exists() && !iocOutFolder.mkdirs()) {
      throw new IllegalArgumentException("Could not create" + iocOutFolder);
    }
    
    ImmutableList<Path> paths = Files.list(new File(checklistFolder).toPath())
        .filter(path -> path.toFile().getName().endsWith(".csv"))
        .collect(ImmutableList.toImmutableList());
    
    for (Path path : paths) {
      File file = path.toFile();
      try (ImportLines importLines = CsvImportLines.fromFile(file, StandardCharsets.UTF_8)) {
        // Skip the header
        importLines.nextLine();
        File eBirdFile = new File(ebirdOutFolder, file.getName());
        try (ExportLines eBirdLines = CsvExportLines.toFile(eBirdFile, StandardCharsets.UTF_8)) {
          File iocFile = new File(iocOutFolder, file.getName());
          try (ExportLines iocLines = CsvExportLines.toFile(iocFile, StandardCharsets.UTF_8)) {
            eBirdLines.nextLine(new String[] {"Name", "ID", "Status"});
            iocLines.nextLine(new String[] {"Name", "ID", "Status"});
            
            while (true) {
              String[] line = importLines.nextLine();
              if (line == null) {
                break;
              }
              
              if (clements.getTaxon(line[1]) == null) {
                System.err.printf("CLEMENTS FILE %s BAD ID %s\n", file.getName(), line[1]);
              }
              if (ioc.getTaxon(line[4]) == null) {
                System.err.printf("IOC FILE %s BAD ID %s\n", file.getName(), line[4]);
              }
              eBirdLines.nextLine(new String[] {line[0], line[1], stripEndemic(line[5])});
              iocLines.nextLine(new String[] {line[3], line[4], stripEndemic(line[6])});
            }
          }
        }
      }
    }
  }

  private String stripEndemic(String status) {
    // Remove "endemic" from the base status 
    return "endemic".equals(status) ? "" : status;
  }
}
