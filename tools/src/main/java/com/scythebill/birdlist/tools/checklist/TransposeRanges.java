/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Map;

import com.beust.jcommander.internal.Maps;
import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Table;
import com.google.common.collect.Tables;

/**
 * Transposes species ranges across all checklists.
 */
public class TransposeRanges {
  private final File checklistsFolder;
  private final Table<String, String, String> idCountryStatus = createLinked();
  
  public static void main(String[] args) throws Exception {
    File checklistsFolder = new File(args[0]);
    File outFile = new File(args[1]);
    Preconditions.checkState(checklistsFolder.exists());

    TransposeRanges transposeRanges = new TransposeRanges(checklistsFolder);
    transposeRanges.loadAllChecklists();
    transposeRanges.writeTransposed(outFile);
  }
  
  TransposeRanges(File checklistsFolder) {
    this.checklistsFolder = checklistsFolder;
  }

  private void writeTransposed(
      File file) throws IOException {
    if (!file.exists()) {
      file.createNewFile();
    }
    
    Writer out = new BufferedWriter(
        new OutputStreamWriter(
            new FileOutputStream(file), Charsets.UTF_8));
    try {
      for (String id : idCountryStatus.rowKeySet()) {
        Map<String, String> row = idCountryStatus.row(id);
        Multimap<String, String> inverted = LinkedHashMultimap.create();
        Multimaps.invertFrom(Multimaps.forMap(row), inverted);
        
        for (String status : inverted.keySet()) {
          out.write(id);
          if (!"native".equals(status)) {
            out.write('/');
            out.write(status);
          }
          out.write('=');
          out.write(Joiner.on(',').join(inverted.get(status)));
          out.write('\n');
        }
      }
    } finally {
      out.close();
    }
  }

  private void loadAllChecklists() throws IOException {
    File[] files = checklistsFolder.listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        return name.endsWith(".csv");
      }
    });
    for (File file : files) {
      String name = file.getName();
      // Don't include British counties for now.
      if (name.startsWith("GB-") && CharMatcher.is('-').countIn(name) >= 2) {
        continue;
      }

      // ... or Irish counties.
      if (name.startsWith("IE-") && CharMatcher.is('-').matchesAnyOf(name)) {
        continue;
      }

      LoadedSplitChecklist checklist = LoadedSplitChecklist.loadChecklist(file, null /* no locations map */);
      name = name.substring(0, name.length() - 4);
      for (LoadedSplitChecklist.LoadedRow row : checklist.rows) {
        idCountryStatus.put(row.id, name, row.status.isEmpty() ? "native" : row.status);
      }
    }
  }
  
  static <R, C, V> Table<R, C, V> createLinked() {
    return Tables.newCustomTable(Maps.newLinkedHashMap(), Maps::newLinkedHashMap);
  }
}
