/*   
 * Copyright 2017 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist.ebird;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.joda.time.DateTimeFieldType;
import org.joda.time.Instant;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.base.CharMatcher;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Multiset;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;
import com.google.common.collect.Table;
import com.google.common.collect.TreeMultiset;
import com.google.common.io.ByteStreams;
import com.google.common.io.CharSink;
import com.google.common.io.Files;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.checklist.TransposedChecklistSynthesizer;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.tools.checklist.LoadedSplitChecklist;
import com.scythebill.birdlist.tools.checklist.LoadedSplitChecklistCorrections;
import com.scythebill.birdlist.tools.checklist.LoadedSplitChecklistCorrections.Action;
import com.scythebill.birdlist.tools.checklist.LoadedSplitChecklistCorrections.LoadedCorrection;
import com.scythebill.birdlist.tools.checklist.LoadedSplitChecklistCorrections.TaxonomyType;
import com.scythebill.birdlist.tools.checklist.ebird.EBirdStatusFile.StatusAndComment;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;

/**
 * Downloads and parses eBird barcharts, comparing them against the Scythebill checklists.
 */
public class DownloadAndCompareBarcharts {
  private static final RequestConfig REQUEST_CONFIG = RequestConfig.custom()
      .setConnectionRequestTimeout(5000).setConnectTimeout(30000).setSocketTimeout(30000).build();
  @SuppressWarnings("deprecation") // javaUpperCase() ignores Unicode supplementary.  Don't care here.
  private static final CharMatcher STATUS_CHARACTERS = CharMatcher.javaUpperCase().or(CharMatcher.whitespace());

  /**
   * False eBird "codes" used for separate checklists in Scythebill. Each of these will actually
   * require merging of Scythebill checklists via the synthetic checklist functionality.
   */
  private static final ImmutableSet<String> CODES_TO_SKIP = ImmutableSet.of(
      "GB", // Scythebill doesn't have a UK-wide checklist
      "SH",
      "US", // US list isn't particularly relevant; trust those state lists
      "AU-TAS-Macquarie", "CL-VS-Easter", "CL-VS-JuanFernandez", "ES-CEUMEL", "YE-HD-SOC",
      "ZA-WC-EdwardMarion",
      "RU-Asia",
      "RU-Europe",
      "ID-Asia",
      "ID-Australasia",
      "XX-Arctic Ocean", "XX-Atlantic Ocean", "XX-Indian Ocean", "XX-Pacific Ocean", "XX-South Polar Region");

  private static final ImmutableMultimap<String, String> CODES_TO_MERGE_SCYTHEBILL_CHECKLISTS =
      ImmutableMultimap.<String, String>builder().putAll("AU", "AU", "AU-TAS-Macquarie")
          .putAll("AU-TAS", "AU-TAS", "AU-TAS-Macquarie")
          .putAll("CL", "CL", "CL-VS-Easter", "CL-VS-JuanFernandez")
          .putAll("ES", "ES", "ES-CN", "ES-CEUMEL")
          .putAll("EC", "EC", "EC-W")
          .putAll("FI", "FI", "FI-01")
          .putAll("PT", "PT", "PT-20", "PT-30")
          .putAll("ZA", "ZA", "ZA-WC-EdwardMarion")
          .putAll("CO", "CO", "CO-SAP")
          .putAll("RU", "RU-Asia", "RU-Europe")
          .putAll("ID", "ID-Asia", "ID-Australasia")
          .putAll("XX", "XX-Arctic Ocean", "XX-Atlantic Ocean", "XX-Indian Ocean", "XX-Pacific Ocean", "XX-South Polar Region")
          .putAll("YE", "YE", "YE-HD-SOC").build();
  private static final ImmutableMultimap<String, String> CODES_TO_MERGE_EBIRD_BARCHARTS =
      ImmutableMultimap.<String, String>builder()
          .putAll("BQ", "BQ-SA", "BQ-SE", "SX").build();

  @Inject
  private Checklists checklists;
  @Inject
  private TransposedChecklistSynthesizer transposedChecklistSynthesizer;

  @Parameter(names = "-clements", required = true)
  private String clementsFileName;

  @Parameter(names = "-ioc", required = true)
  private String iocFileName;

  @Parameter(names = "-cacheDir", required = true)
  private String cacheDir;

  @Parameter(names = "-outputFile", required = true)
  private String outputFileName;

  @Parameter(names = "-ebirdSessionId", required = true)
  private String ebirdSessionId;

  /** Set to drop the "investigate" species, which can be recreated from the barcharts,
   * but are a pain to migrate for new taxonomies. */
  @Parameter(names = "-dontWriteInvestigate")
  private boolean dontWriteInvestigate = false;

  @Parameter(names = "-correctionsFolder", required = true)
  private String correctionsFolder;

  @Parameter(names = "-defaultChecklistFolder")
  private String defaultChecklistFolder;
  private ReportSet reportSet;
  private BasicCookieStore cookieStore = new BasicCookieStore();

  @Inject
  DownloadAndCompareBarcharts() {}

  public static void main(String[] args) throws Exception {
    Guice.createInjector().getInstance(DownloadAndCompareBarcharts.class).run(args);
  }

  private void run(String[] args) throws Exception {
    new JCommander(this, args);
    Taxonomy taxonomy = ToolTaxonUtils.getTaxonomy(clementsFileName);
    reportSet = ReportSets.newReportSet(taxonomy);
    EBirdTaxonomy ebirdTaxonomy = new EBirdTaxonomy(taxonomy);
    TsvBarchartParser barchartParser = new TsvBarchartParser(ebirdTaxonomy);
    
    LocationSet locations = ReportSets.newReportSet(taxonomy).getLocations();
    PredefinedLocations predefined = PredefinedLocations.loadAndParse();

    File ebirdBarchartsDir = new File(cacheDir);
    ebirdBarchartsDir.mkdirs();

    // Need to use an HTTP client to download barchart data; without a cookie,
    // it'll force a redirect. Standard Java libs can't deal with that.
    CloseableHttpClient httpClient = HttpClients.custom().setMaxConnPerRoute(3).setMaxConnTotal(20)
        .setDefaultCookieStore(null)
        .setDefaultCookieStore(cookieStore)
        .setConnectionManager(new PoolingHttpClientConnectionManager()).build();

    File outputFile = new File(outputFileName);
    outputFile.getParentFile().mkdirs();
    
    Table<String, String, StatusAndComment> knownStatusesByCodeAndSpecies =
        EBirdStatusFile.parse(outputFile);
    
    Multiset<String> countsByType = TreeMultiset.create();
    CharSink sink = Files.asCharSink(outputFile, StandardCharsets.UTF_8);    

    class EscapedSpecies {
      String species;
      String country;
      double frequency;
    }
    
    Comparator<EscapedSpecies> comparator = Comparator.comparing(e -> e.frequency);
    TreeSet<EscapedSpecies> escapedSpecies = new TreeSet<>(
        comparator.reversed());
    
    int year = new Instant().get(DateTimeFieldType.year());
    try (PrintWriter out = new PrintWriter(sink.openBufferedStream())) {
      ImmutableSortedSet<String> ebirdCodes = ImmutableSortedSet
          .copyOf(Sets.union(Sets.difference(checklists.checklistCodes(), CODES_TO_SKIP),
              CODES_TO_MERGE_SCYTHEBILL_CHECKLISTS.keySet()));
      for (String code : ebirdCodes) {
        // Ignore all counties
        if (CharMatcher.is('-').countIn(code) > 1) {
          continue;
        }
 
        ParsedBarchart parsedBarchart;
        if (CODES_TO_MERGE_EBIRD_BARCHARTS.containsKey(code)) {
          parsedBarchart = null;
          for (String eBirdSubcode : CODES_TO_MERGE_EBIRD_BARCHARTS.get(code)) {
            File downloaded = downloadedFileForCode(ebirdBarchartsDir, eBirdSubcode);
            downloadBarchartIfNeeded(httpClient, year, eBirdSubcode, downloaded);
            ParsedBarchart subchart = barchartParser.parseBarchart(downloaded);
            parsedBarchart = parsedBarchart == null ? subchart : parsedBarchart.plus(subchart);
          }
        } else {
          File downloaded = downloadedFileForCode(ebirdBarchartsDir, code);
          downloadBarchartIfNeeded(httpClient, year, code, downloaded);
    
          parsedBarchart = barchartParser.parseBarchart(downloaded);
        }
        Checklist checklist;
  
        if (CODES_TO_MERGE_SCYTHEBILL_CHECKLISTS.containsKey(code)) {
          checklist = transposedChecklistSynthesizer
              .synthesizeChecklistInternal(reportSet, CODES_TO_MERGE_SCYTHEBILL_CHECKLISTS.get(code));
        } else {
          checklist = checklists.getChecklist(code);
        }

        Set<String> missingSpecies = new LinkedHashSet<>();
        Set<String> maybeIntroduced = new LinkedHashSet<>();
        
        parsedBarchart.forEach((occurrence, species) -> {
          if (!checklist.includesTaxon(species.taxon())) {
            missingSpecies.add(species.name());
          } else if (checklist.getStatus(
              taxonomy, SightingTaxons.newSightingTaxon(species.taxon().getId())) == Checklist.Status.ESCAPED
              && occurrence > 15.0) {
            // For escaped species that are seen commonly (arbitrary cutoff),
            // note them as "maybe introduced" for periodic review
            maybeIntroduced.add(species.name());
          }
              
        });
        
        Map<String, StatusAndComment> statusAndCommentBySpecies = knownStatusesByCodeAndSpecies.row(code);
        
        // Invert the status by species into a Multimap of statuses -> species
        Multimap<String, String> speciesByStatus = Multimaps.newListMultimap(
            new TreeMap<String, Collection<String>>(),
            ArrayList::new);
            
        statusAndCommentBySpecies.forEach(
            (species, statusAndComment) -> speciesByStatus.put(statusAndComment.status, species)); 
        
        if (!missingSpecies.isEmpty()) {
          Location location = Locations.getLocationByCodePossiblyCreating(locations, predefined, code);          
          Preconditions.checkNotNull(location, "No location for %s", code);

          
          out.format("LOCATION: %s (%s)\n", code, location.getModelName());
          out.format("http://ebird.org/ebird/barchart?byr=1900&eyr=%s&bmo=1&emo=12&r=%s\n", year, code);

          missingSpecies.forEach(species -> {
            StatusAndComment statusAndComment = statusAndCommentBySpecies.get(species);
            if (statusAndComment == null) {
              // Copy every new species into the by-status multimap as "INVESTIGATE" 
              speciesByStatus.put("INVESTIGATE", species);              
            } else if (statusAndComment.status.startsWith("PAST ")) {
              // And revive any "PAST " errors as present errors.
              speciesByStatus.remove(statusAndComment.status, species);
              String newStatus = statusAndComment.status.substring(5);
              speciesByStatus.put(newStatus, species);
              statusAndCommentBySpecies.put(species, new StatusAndComment(newStatus, statusAndComment.comment));
            }
          });
          
          // And take everything that is *present* in the original file but *not* present in the eBird data
          // and remove it.
          Set<String> speciesInOriginalFile = Sets.newHashSet(speciesByStatus.values());
          SetView<String> difference = Sets.difference(speciesInOriginalFile, missingSpecies);
          if (!difference.isEmpty()) {
            System.err.printf("No longer an issue: %s : %s\n", code, difference);
          }
          speciesByStatus.values().removeAll(Sets.difference(speciesInOriginalFile, missingSpecies));
          
          // But any past errors in ebird *should* be preserved by amending to "PAST ".
          for (String removedSpecies : difference) {
            StatusAndComment statusAndComment = statusAndCommentBySpecies.get(removedSpecies);
            if (statusAndComment.status.endsWith("IN EBIRD")) {
              String newStatus = statusAndComment.status;
              if (!newStatus.startsWith("PAST ")) {
                newStatus = "PAST " + newStatus;
              }
              speciesByStatus.put(newStatus, removedSpecies);
              statusAndCommentBySpecies.put(removedSpecies, new StatusAndComment(newStatus, statusAndComment.comment));
            }
          }

          if (dontWriteInvestigate) {
            for (String species : ImmutableList.copyOf(speciesByStatus.get("INVESTIGATE"))) {
              StatusAndComment statusAndComment = statusAndCommentBySpecies.get(species);
              if (statusAndComment == null || statusAndComment.comment == null) {
                speciesByStatus.remove("INVESTIGATE", species);
              }
            }
          }

          // "Maybe introduced" is recomputed each time.
          speciesByStatus.removeAll("MAYBE INTRODUCED");

          // Print each status one-by-one
          for (String status : speciesByStatus.keySet()) {
            out.println(status);
            
            boolean isEscaped = isEscapedStatus(status);
            for (String species : speciesByStatus.get(status)) {
              StatusAndComment statusAndComment = statusAndCommentBySpecies.get(species);
              if (statusAndComment == null || statusAndComment.comment == null) {
                out.println(species);
              } else {
                out.format("%s:%s\n", species, statusAndComment.comment);
              }
              countsByType.add(status);
              if (isEscaped) {
                double frequency = parsedBarchart.getFrequency(species);
                EscapedSpecies escaped = new EscapedSpecies();
                escaped.country = code;
                escaped.species = species;
                escaped.frequency = frequency;
                
                escapedSpecies.add(escaped);
              }
            }
            out.println();
          }
          
          if (!maybeIntroduced.isEmpty()) {
            out.println("MAYBE INTRODUCED");
            for (String species : maybeIntroduced) {
              out.println(species);
              countsByType.add("MAYBE INTRODUCED");
            }
            out.println();
          }
        }
      }
    }

    MappedTaxonomy ioc = ToolTaxonUtils.getMappedTaxonomy(taxonomy, iocFileName);
    ImmutableListMultimap<String, EscapedSpecies> speciesByCountry = escapedSpecies.stream()
        .filter(s -> s.frequency > 1.0)
        .collect(ImmutableListMultimap.toImmutableListMultimap(
            s -> s.country,
            s -> s));
    for (String country : speciesByCountry.keySet()) {
      File file = new File(defaultChecklistFolder, country + ".csv");
      LoadedSplitChecklist checklist = LoadedSplitChecklist.loadChecklist(file, null);
      LoadedSplitChecklistCorrections corrections =
          LoadedSplitChecklistCorrections.loadCorrections(checklist, correctionsFolder);
      for (EscapedSpecies addAsEscaped : speciesByCountry.get(country)) {
        Taxon taxon = ebirdTaxonomy.getSpecies(addAsEscaped.species).taxon();
        // Skip the "add-as-escaped" if the Clements taxonomy already has it.
        if (!checklist.containsTaxon(taxon.getId())) {
          LoadedCorrection correction = new LoadedCorrection();
          correction.action = Action.add;
          correction.id = taxon.getId();
          correction.status = "escaped";
          correction.taxonomyType = TaxonomyType.clements;
          corrections.rows.add(correction);
          corrections.rows.add(correction.mapToAlternateTaxonomy(ioc));
        }
      }
      corrections.write();
    }
    
    System.out.println();
    System.out.println("STATUSES");
    countsByType.elementSet().forEach(
        s -> System.out.printf("%s: %d\n", s, countsByType.count(s)));
  }

  private boolean isEscapedStatus(String status) {
    return "CATEGORY E".equals(status) || "ESCAPEE".equals(status);
  }
  
  File downloadedFileForCode(File ebirdBarchartsDir, String code) {
    return new File(ebirdBarchartsDir, code + ".tsv");
  }

  void downloadBarchartIfNeeded(CloseableHttpClient httpClient, int year, String code,
      File downloaded)
      throws URISyntaxException, IOException, FileNotFoundException, ClientProtocolException {
    if (!downloaded.exists()) {
      URI uri = new URI(String.format(
          "https://ebird.org/barchartData?r=%s&fmt=tsv",
//          "http://ebird.org/ebird/barchartData?r=%s&bmo=1&emo=12&byr=1900&eyr=" + year + "&fmt=tsv",
          code));
      HttpGet httpGet = new HttpGet(uri);
      httpGet.setConfig(REQUEST_CONFIG);
      httpGet.addHeader("Cookie", "EBIRD_SESSIONID=" + ebirdSessionId);
 
      while (true) {
        cookieStore.clear();
        try (CloseableHttpResponse get = httpClient.execute(httpGet)) {
          HttpEntity entity = get.getEntity();
          System.out.println("Loading " + uri);
          InputStream inputStream = new BufferedInputStream(entity.getContent());
          ByteStreams.copy(inputStream, new FileOutputStream(downloaded));
          if (!get.containsHeader("Content-Disposition")) {
            System.err.println("Didn't return attachment. Sleeping 30 seconds and will retry");
            try {
              Thread.sleep(30000);
              continue;
            } catch (InterruptedException e) {
              throw new IllegalStateException(e);
            }
          }
          if (downloaded.length() < 100) {
            System.err.println("Failed download; sleeping 1 minute and will retry");
            try {
              Thread.sleep(60000);
              continue;
            } catch (InterruptedException e) {
              throw new IllegalStateException(e);
            }
          }
          // Slow things down juuust a bit to not be mean to eBird
          try {
            Thread.sleep(1000);
          } catch (InterruptedException e) {
            throw new IllegalStateException(e);
          }
          break;
        }
      }
    }
  }
}
