/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist.ebird;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import com.google.common.base.CharMatcher;
import com.google.common.collect.Table;
import com.google.common.collect.Tables;
import com.google.common.io.CharSource;
import com.google.common.io.Files;

/**
 * Reads the eBird status file.
 */
public class EBirdStatusFile {
  @SuppressWarnings("deprecation") // javaUpperCase() ignores Unicode supplementary.  Don't care here.
  private static final CharMatcher STATUS_CHARACTERS = CharMatcher.javaUpperCase().or(CharMatcher.whitespace());

  static class StatusAndComment {
    final String status;
    final String comment;
    
    StatusAndComment(String status, String comment) {
      this.status = status;
      this.comment = comment;
    }
    
    @Override
    public String toString() {
      if (comment == null) {
        return status;
      }
      
      return String.format("%s:%s", status, comment);
    }
  }
  
  public static Table<String, String, StatusAndComment> parse(File statusFile) throws IOException {
    Table<String, String, StatusAndComment> knownStatusesByCodeAndSpecies = Tables.newCustomTable(
        new TreeMap<String, Map<String, StatusAndComment>>(), // Sort by code
        LinkedHashMap::new); // But keep species in order

    CharSource charSource = Files.asCharSource(statusFile, StandardCharsets.UTF_8);
    try (BufferedReader reader = charSource.openBufferedStream()) {
      String code = null;
      String status = null;
      while (true) {
        String line = reader.readLine();
        if (line == null) {
          break;
        }
        
        if (line.startsWith("LOCATION: ")) {
          code = line.substring(10, line.indexOf('(') - 1);
        } else if (line.startsWith("http://")) {
          continue;
        } else if (STATUS_CHARACTERS.matchesAllOf(line)) {
          status = line;
        } else if (!line.isEmpty()) {
          if (status == null) {
            throw new IllegalStateException("No status found: " + line);
          } else if (code == null) {
            throw new IllegalStateException("No code found: " + line);
          } else {
            // Found a species, insert it.
            int colonIndex = line.indexOf(':');
            StatusAndComment statusAndComment;
            String species;
            if (colonIndex < 0) {
              statusAndComment = new StatusAndComment(status, null);
              species = line;
            } else {
              statusAndComment = new StatusAndComment(status, line.substring(colonIndex + 1));
              species = line.substring(0,  colonIndex);
            }
            knownStatusesByCodeAndSpecies.put(code, species, statusAndComment);
          }
        }
      }
    }
    
    
    return knownStatusesByCodeAndSpecies;
  }
}
