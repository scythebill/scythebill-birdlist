/*   
 * Copyright 2017 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist.ebird;

import com.google.common.collect.ImmutableMap;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Trivial encapsulation of the eBird taxonomy, searchable by name.
 */
class EBirdTaxonomy {
  private final ImmutableMap<String, EbirdSpecies> speciesByName;

  public final class EbirdSpecies {
    private final String name;
    private final Taxon taxon;

    public final String name() {
      return this.name;
    }
    
    public final Taxon taxon() {
      return this.taxon;
    }
    
    EbirdSpecies(String name, Taxon taxon) {
      this.name = name;
      this.taxon = taxon;
    }
    
    @Override
    public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      
      if (!(o instanceof EbirdSpecies)) {
        return false;
      }
      
      EbirdSpecies that = (EbirdSpecies) o;
      return that.name.equals(this.name);
    }
    
    @Override
    public int hashCode() {
      return name.hashCode();
    } 
    
    @Override
    public String toString() {
      return name;
    }
  }

  EBirdTaxonomy(Taxonomy taxonomy) {
    ImmutableMap.Builder<String, EbirdSpecies> idsByName = ImmutableMap.builder();
    TaxonUtils.visitTaxa(taxonomy, taxon -> {
      if ((taxon.getType().compareTo(Taxon.Type.genus) < 0) &&
        taxon.getCommonName() != null) {
        idsByName.put(taxon.getCommonName(),
            new EbirdSpecies(taxon.getCommonName(), taxon));
      }
      return true;
    });
    this.speciesByName = idsByName.build();
  }
  
  public EbirdSpecies getSpecies(String name) {
    return speciesByName.get(name);
  }
}
