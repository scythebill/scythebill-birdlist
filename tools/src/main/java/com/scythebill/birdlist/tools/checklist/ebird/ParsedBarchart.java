/*   
 * Copyright 2017 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist.ebird;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.BiConsumer;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Ordering;
import com.scythebill.birdlist.tools.checklist.ebird.EBirdTaxonomy.EbirdSpecies;

/**
 * Data from parsing a single barchart.
 */
class ParsedBarchart {
  private final ListMultimap<Double, EbirdSpecies> speciesByOccurrence = Multimaps.newListMultimap(
      new TreeMap<>(Ordering.natural().reversed()),
      ArrayList::new);

  public void addSpecies(EbirdSpecies species, double occurrence) {
    speciesByOccurrence.put(occurrence, species);
  }

  public void forEach(BiConsumer<Double, EbirdSpecies> consumer) {
    speciesByOccurrence.forEach(consumer);
  }
  
  private Map<EbirdSpecies, Double> mutableOccurrenceBySpecies() {
    LinkedHashMap<EbirdSpecies, Double> map = new LinkedHashMap<>(speciesByOccurrence.size());
    forEach((occurrence, species) -> map.put(species, occurrence));
    return map;
  }
  
  public ParsedBarchart plus(ParsedBarchart that) {
    ParsedBarchart sum = new ParsedBarchart();
    Map<EbirdSpecies, Double> mergedOccurrence = this.mutableOccurrenceBySpecies();
    that.forEach((occurrence, species) -> {
      Double thisOccurrence = mergedOccurrence.get(species);
      if (thisOccurrence == null) {
        mergedOccurrence.put(species, occurrence);
      } else {
        mergedOccurrence.put(species, thisOccurrence.doubleValue() + occurrence.doubleValue());
      }
    });
    
    mergedOccurrence.forEach(sum::addSpecies);
    return sum;
  }

  public double getFrequency(String species) {
    for (Map.Entry<Double, EbirdSpecies> entry : speciesByOccurrence.entries()) {
      if (entry.getValue().name().equals(species)) {
        return entry.getKey();
      }
    }
    return 0.0;
  }
}
