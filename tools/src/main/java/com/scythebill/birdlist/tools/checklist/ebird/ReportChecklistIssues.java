/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist.ebird;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.FileConverter;
import com.google.common.base.CaseFormat;
import com.google.common.base.Strings;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Table;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;
import com.scythebill.birdlist.tools.checklist.ebird.EBirdStatusFile.StatusAndComment;

/**
 * Tool to process the checklist status file to extract eBird errors.
 */
public class ReportChecklistIssues {
  @Parameter(names = "-outputFile", required = true, converter = FileConverter.class)
  private File outputFile;

  private Table<String, String, StatusAndComment> knownStatusesByCodeAndSpecies;

  public static void main(String[] args) throws Exception {
    ReportChecklistIssues reportChecklistIssues = new ReportChecklistIssues();
    new JCommander(reportChecklistIssues, args);
    reportChecklistIssues.run();
  }

  public ReportChecklistIssues() {
  }

  private void run() throws IOException {
    knownStatusesByCodeAndSpecies = EBirdStatusFile.parse(outputFile);
    Iterator<StatusAndComment> iterator = knownStatusesByCodeAndSpecies.values().iterator();
    while (iterator.hasNext()) {
      StatusAndComment next = iterator.next();
      if (!next.status.contains("EBIRD") || next.status.startsWith("PAST ")) {
        iterator.remove();
      }
    }
    LocationSet locationSet = ReportSets.newReportSet(new TaxonomyImpl()).getLocations();
    for (Location location : locationSet.rootLocations()) {
      outputEBirdIssues(location);
    }
  }

  private void outputEBirdIssues(Location location) {
    String code = Locations.getLocationCode(location);
    if (code != null) {
      Map<String, StatusAndComment> row = knownStatusesByCodeAndSpecies.row(code);
      if (!row.isEmpty()) {
        System.out.printf("%s (%s)\n", code, location.getModelName());
        Multimap<String, String> output = Multimaps.newListMultimap(new TreeMap<>(), ArrayList::new);
        row.forEach((species, statusAndComment) -> {
          output.put(
              statusAndComment.status,
              String.format("%s: %s", species, Strings.nullToEmpty(statusAndComment.comment)));
        });
        
        for (String status : output.keySet()) {
          if (!"ERROR IN EBIRD".equals(status)) {
            String statusOutput = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, status);
            statusOutput = statusOutput.replace(" in ebird", "");
            System.out.println(statusOutput);
          }
          for (String speciesAndComment : output.get(status)) {
            System.out.println(speciesAndComment);
          }
          System.out.println();
        }
      }
    }
    for (Location child : location.contents()) {
      outputEBirdIssues(child);
    }
  }
}
