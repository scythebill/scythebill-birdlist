/*   
 * Copyright 2017 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.checklist.ebird;

import static com.google.common.collect.Iterables.limit;
import static com.google.common.collect.Iterables.skip;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.xml.sax.SAXException;

import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.tools.checklist.ebird.EBirdTaxonomy.EbirdSpecies;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;

/**
 * Parses a single downloaded set of barchart data.
 */
final class TsvBarchartParser {
  // http://ebird.org/ebird/barchartData?r=NI&bmo=1&emo=12&byr=1900&eyr=2017&fmt=tsv
  // /Users/awiner/Downloads/ebird_NI__1900_2017_1_12_barchart.txt
  
  private final static Logger logger = Logger.getLogger(TsvBarchartParser.class.getName());
  
  public static void main(String[] args)  throws IOException, SAXException {
    Taxonomy taxonomy = ToolTaxonUtils.getTaxonomy(
        "/Developer/scythebill-git/birdlist/app/src/main/resources/taxon.xml");
    new TsvBarchartParser(new EBirdTaxonomy(taxonomy)).parseBarchart(new File(
        "/Users/awiner/Downloads/ebird_US-CA__1900_2019_1_12_barchart.txt"));
  }
  
  private final EBirdTaxonomy taxonomy;

  TsvBarchartParser(EBirdTaxonomy taxonomy) {
    this.taxonomy = taxonomy;
  }
  
  public ParsedBarchart parseBarchart(File file) throws IOException {
    CSVFormat format = CSVFormat.TDF.withIgnoreEmptyLines().withIgnoreSurroundingSpaces().withTrim();
    ParsedBarchart barchart = new ParsedBarchart();
    try (CSVParser parser = CSVParser.parse(file, StandardCharsets.UTF_8, format)) {
      boolean foundSampleSizeRow = false;
      for (CSVRecord record : parser) {
        if (!foundSampleSizeRow) {  
          if (record.get(0).equals("Sample Size:")) {
            foundSampleSizeRow = true;
          }
        } else {
          String speciesName = record.get(0);
          if (speciesName.contains("sp.")
              || speciesName.contains("/")
              || speciesName.contains(" x ")
              || speciesName.contains("hybrid")
              || speciesName.contains("Domestic")) {
            continue;
          }
          EbirdSpecies species = taxonomy.getSpecies(speciesName);
          if (species == null) {
            logger.warning("Could not find id for " + speciesName);
            continue;
          }
          
          double occurrence = getOccurrence(record);
          barchart.addSpecies(species, occurrence);
       }
      }
    }
    
    return barchart;
  }

  /**
   * For now, a very simple and dumb "occurrence" number.  Might be useful to be more weighted.
   */
  private double getOccurrence(CSVRecord record) {
    double sum = 0;
    int presentCount = 0;
    for (String string : limit(skip(record, 1), 48)) {
      if (!string.isEmpty()) {
        double d = Double.parseDouble(string);
        if (d > 0.0) {
          sum += Double.parseDouble(string);
          presentCount++;
        }
      }
    }
    return sum * (presentCount * presentCount) / 48.0;
  }

//  private String getCommonName(String string) {
//    int leftParen = string.indexOf('(');
//    if (leftParen < 0) {
//      return string;
//    }
//    
//    return string.substring(0, leftParen - 1);
//  }
}
