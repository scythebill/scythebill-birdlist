package com.scythebill.birdlist.tools.importer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.List;

import org.xml.sax.SAXException;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.MoreExecutors;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.TaxonomyIndexer;
import com.scythebill.birdlist.model.xml.XmlReportSetExport;
import com.scythebill.birdlist.model.xml.XmlTaxonImport;
import com.scythebill.birdlist.ui.imports.ComputedMappings;
import com.scythebill.birdlist.ui.imports.CsvSightingsImporter;
import com.scythebill.birdlist.ui.imports.DateFromYearMonthDayFieldMapper;
import com.scythebill.birdlist.ui.imports.DescriptionFieldMapper;
import com.scythebill.birdlist.ui.imports.FieldMapper;
import com.scythebill.birdlist.ui.imports.ImportedLocation;
import com.scythebill.birdlist.ui.imports.LineExtractor;
import com.scythebill.birdlist.ui.imports.LineExtractors;
import com.scythebill.birdlist.ui.imports.LocationShortcuts;
import com.scythebill.birdlist.ui.imports.FieldTaxonImporter;
import com.scythebill.birdlist.ui.imports.TaxonImporter;

/**
 * Imports sightings from ad-hoc CSV files.
 */
public class AdHocImporter extends CsvSightingsImporter {
  private final LineExtractor<String> locationIdExtractor = LineExtractors.joined(
      Joiner.on('|'),
      LineExtractors.stringFromIndex(4));

  private final LineExtractor<String> taxonomyIdExtractor = LineExtractors.joined(
      Joiner.on('|'),
      LineExtractors.stringFromIndex(1),
      LineExtractors.stringFromIndex(2),
      LineExtractors.stringFromIndex(3));
  
  public static Taxonomy getTaxonomy(String filename) throws IOException, SAXException {
    Reader reader = null;
    try {
      File file = new File(filename);
      reader = new BufferedReader(new FileReader(file));
      Taxonomy taxonomy = (new XmlTaxonImport()).importTaxa(reader);
      Futures.getUnchecked(new TaxonomyIndexer(Futures.immediateFuture(taxonomy))
              .load(MoreExecutors.newDirectExecutorService()));
      return taxonomy;
    } finally {
      reader.close();
    }
  }

  public static MappedTaxonomy getMappedTaxonomy(String baseTaxonomy, String filename)
          throws IOException, SAXException {
    Taxonomy base = getTaxonomy(baseTaxonomy);
    Reader reader = null;
    try {
      File file = new File(filename);
      reader = new BufferedReader(new FileReader(file));
      MappedTaxonomy taxonomy = (new XmlTaxonImport()).importMappedTaxa(reader, base);

      Futures.getUnchecked(new TaxonomyIndexer(Futures.immediateFuture(taxonomy))
              .load(MoreExecutors.newDirectExecutorService()));
      return taxonomy;
    } finally {
      reader.close();
    }
  }

  public static void main(String[] args) throws Exception {
    String file = args[0];
    MappedTaxonomy taxonomy = getMappedTaxonomy(args[1], args[2]);
    ReportSet reportSet = ReportSets.newReportSet(taxonomy.getBaseTaxonomy());
    
    CsvSightingsImporter sightingsImporter = new AdHocImporter(
        reportSet,
        taxonomy,
        null,
        PredefinedLocations.loadAndParse(),
        new File(file));
    sightingsImporter.parseTaxonomyIds();
    List<Sighting> imports = sightingsImporter.runImport();
    reportSet.mutator().adding(imports).mutate();;
    
    String outputFile = args[3];
    OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(outputFile), Charsets.UTF_8);
    new XmlReportSetExport().export(out, "UTF-8", reportSet, taxonomy.getBaseTaxonomy());
  }
  
  public AdHocImporter(
      ReportSet reportSet, Taxonomy taxonomy, Checklists checklists, PredefinedLocations predefinedLocations, File file) {
    super(reportSet, taxonomy, checklists, predefinedLocations, file, file);
  }

  @Override
  protected Charset getCharset() {
    return Charsets.ISO_8859_1;
  }
  
  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return taxonomyIdExtractor;
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    return new FieldTaxonImporter<>(taxonomy,
        LineExtractors.stringFromIndex(1),
        LineExtractors.stringFromIndex(2),
        LineExtractors.stringFromIndex(3));
  }

  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    locationShortcuts.setMaxDistance(1);

    ImportLines lines = CsvImportLines.fromFile(locationsFile, getCharset());
    try {
      // Skip over the header
      lines.nextLine();

      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }
        // Skip over "empty" lines returned when CSVReader encounters unexpected LFs
        if (line.length == 1) {
          continue;
        }
        
        ImportedLocation imported = new ImportedLocation();
        imported.country = Strings.emptyToNull(line[4]);
        imported.state = null;
        imported.county = null;

        String locationId = imported.addToLocationSet(reportSet, locations, locationShortcuts, predefinedLocations);
        String id = locationIdExtractor.extract(line);
        locationIds.put(id, locationId);
      }
    } finally {
      lines.close();
    }
  }

  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    // Skip the header line
    lines.nextLine();
    
    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
        
    mappers.add(new DateFromYearMonthDayFieldMapper<>(
        LineExtractors.intFromIndex(7),
        LineExtractors.intFromIndex(6),
        LineExtractors.intFromIndex(5)));
    mappers.add(new DescriptionFieldMapper<>(LineExtractors.stringFromIndex(9)));
    return new ComputedMappings<String[]>(
        new TaxonFieldMapper(taxonomyIdExtractor),
        new LocationMapper(locationIdExtractor),
        mappers);
        
  }
}
