package com.scythebill.birdlist.tools.importer;

import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.io.ImportLines;

public class AdHocToScythebillFormat {
  public static void main(String[] args) throws Exception {
    ImportLines fromFile = CsvImportLines.fromFile(new File(args[0]), Charsets.UTF_8);
    
    File outFile = new File(args[1]);
    outFile.createNewFile();
    FileWriter writer = new FileWriter(outFile);
    
    ExportLines toFile = CsvExportLines.fromWriter(writer);
    toFile.nextLine(new String[] {
        "Common",
        "Scientific",
        "Subspecies",
        "Index",
        "Date",
        "Number",
        "Female",
        "Male",
        "Immature",
        "Adult",
        "Heard only",
        "Status",
        "Breeding",
        "Photographed",
        "Description",
        "Region",
        "Country",
        "State",
        "County",
        "City"});
    
    while (true) {
      String[] nextLine = fromFile.nextLine();
      if (nextLine == null) {
        System.err.println("NULL");
        break;
      }
      
      if (nextLine.length < 3) {
        System.err.println("SHORT");
        continue;
      }
      
      if (nextLine[0].isEmpty()
          || nextLine[1].isEmpty()
          || nextLine[2].isEmpty()) {
        System.err.println("EMPTY");
        continue;
      }

      System.err.println(Arrays.asList(nextLine));
      String[] output = new String[20];
      output[0] = nextLine[1];
      output[1] = nextLine[2];
      String dateAndCountry = nextLine[0];
      output[16] = dateAndCountry;
      if (dateAndCountry.length() > 4) {
        String maybeYear = dateAndCountry.substring(dateAndCountry.length() - 4);
        if (CharMatcher.inRange('0', '9').matchesAllOf(maybeYear)) {
          output[4] = maybeYear;
          output[16] = dateAndCountry.substring(0, dateAndCountry.length() - 5);
        }
      }
      
      toFile.nextLine(output);
    }
    
    writer.close();
  }
}
