/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.importer;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.io.ImportLines;

/**
 * Transforms the text from http://butterfliesofamerica.com/list.htm into something
 * that is at least close to the needed CSV format for an extended taxonomy; 
 */
public class ButterfliesOfNAParser {
  private static final Pattern TRINOMIAL_PATTERN = Pattern.compile("^[A-Za-z\\\"]+ [a-z\\.]+ [a-z]+");
  private static final Pattern BINOMIAL_PATTERN = Pattern.compile("^[A-Za-z\\\"]+ [a-z\\.]+");
  private static final ImmutableMap<String, String> NEW_FAMILY = ImmutableMap.<String, String>builder()
      .put("Baronia brevicornis", "Papilionidae")
      .put("Pseudopieris nehemia", "Pieridae")
      .put("Iophanus pyrrhias", "Lycaenidae")
      .put("Euselasia pellonia", "Riodinidae")
      .put("Libytheana carinenta", "Nymphalidae")
      .put("Phocides polybius", "Hesperiidae")
      .build();
  private static final ImmutableMap<String, String> FAMILY_NAMES = ImmutableMap.<String, String>builder()
      .put("Papilionidae", "Swallowtails")
      .put("Pieridae", "Whites and Yellows")
      .put("Lycaenidae", "Gossamerwings")
      .put("Riodinidae", "Metalmarks")
      .put("Nymphalidae", "Brushfoots")
      .put("Hesperiidae", "Skippers")
      .build();
  
      
  public static void main(String[] args) throws IOException {
    ImportLines in = CsvImportLines.fromFile(new File(args[0]), StandardCharsets.UTF_8).withoutTrimming();;
    ExportLines out = CsvExportLines.toFile(new File(args[1]), StandardCharsets.UTF_8);
    new ButterfliesOfNAParser().run(in, out);
  }
  
  private String lastSpeciesCommonName = null;
  private String lastSpecies = null;

  private void run(ImportLines in, ExportLines out) throws IOException {
    // Skip the header
    in.nextLine();
    out.nextLine(new String[]{"ID", "butterfliesofamerica"});
    out.nextLine(new String[]{"Name", "North American Butterflies"});
    out.nextLine(new String[]{"Credits",
        "Warren, A.D., K.J. Davis, N.V. Grishin,\nJ.P. Pelham, E.M. Stangeland. 2012.\n"
        + "Interactive Listing of American Butterflies. [30-XII-12]\n"
        + "http://www.butterfliesofamerica.com/"});
    out.nextLine(new String[]{
        "Order", "Family", "Species", "Subspecies", "Common", "Distribution", "Extinct", "Notes"});
    out.nextLine(new String[]{"Lepidoptera", "", "", "", "Butterflies"});
    while (true) {
      String[] line = in.nextLine();
      if (line == null) {
        break;
      }
      
      if (line.length < 3) {
        continue;
      }
      
      // Families, etc. start with "".  Drop (most) families here, we'll add them manually later
      String scientific = line[0];
      if (scientific.startsWith(" ") || scientific.isEmpty()) {
        continue;
      }
      
      String distribution = line[2];
      String notes = "";
      // Remove the subspecies synonyms from the distribution 
      if (distribution.startsWith("[")) {
        notes = distribution.substring(1, distribution.indexOf(']'));
        int remainder = notes.length() + 3;
        if (remainder + 3 >= distribution.length()) {
          distribution = "";
        } else {
          distribution = CharMatcher.whitespace().trimFrom(distribution.substring(remainder));
        }
      }
      
      if (distribution.startsWith("MORE INFO")) {
        distribution = "";
      }
      
      String common = CharMatcher.whitespace().trimFrom(line[1]);
      
      String extinct = distribution.toLowerCase().contains("extinct") ? "1" : "";
      Matcher trinomialMatcher = TRINOMIAL_PATTERN.matcher(scientific);
      if (trinomialMatcher.find()) {
        String trinomial = trinomialMatcher.group();
        String binomial = trinomial.substring(0, trinomial.lastIndexOf(' '));
        
        // Output the binomial first, if needed
        if (!binomial.equals(lastSpecies)) {
          writeSpecies(out, binomial, common, "", "", "");
        }
        
        String subspecies = trinomial.substring(binomial.length() + 1);
        String subspeciesCommon = common.equals(lastSpeciesCommonName) ? "" : common;
        out.nextLine(new String[]{
            "", "", "", subspecies, subspeciesCommon, distribution, extinct, notes
        });
      } else {
        Matcher binomialMatcher = BINOMIAL_PATTERN.matcher(scientific);
        if (binomialMatcher.find()) {
          String binomial = binomialMatcher.group();
          if (scientific.endsWith("segregate)")) {
            if (!binomial.equals(lastSpecies)) {
              writeSpecies(out, binomial, common, "", "", "");
            }
            
            String segregateName = scientific.substring(scientific.lastIndexOf('(') + 1, scientific.length() - 1);
            out.nextLine(new String[]{
                "", "", "", segregateName, "", distribution, extinct, notes
            });
          } else {
            writeSpecies(out, binomial, common, distribution, extinct, notes);
          }
        } else {
          System.err.println("NO MATCH for " + Joiner.on(',').join(line));
        }
      }
    }
    
    in.close();
    out.close();
  }

  private void writeSpecies(ExportLines out, String binomial, String common,
      String distribution, String extinct, String notes) throws IOException {
    lastSpeciesCommonName = common;
    lastSpecies = binomial;
    
    // Write a new family if needed
    String newFamily = NEW_FAMILY.get(binomial);
    if (newFamily != null) {
      out.nextLine(new String[]{
          "", newFamily, "", "",
          Preconditions.checkNotNull(FAMILY_NAMES.get(newFamily))
      });
    }
    out.nextLine(new String[]{
        "", "", binomial, "", common, distribution, extinct, notes
    });
  }  
}
