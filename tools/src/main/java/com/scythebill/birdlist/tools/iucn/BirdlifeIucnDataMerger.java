/*   
 * Copyright 2012 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.iucn;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.text.similarity.LevenshteinDistance;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.SpeciesImpl;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.xml.XmlTaxonExport;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;

/**
 * Reads IUCN RedList status information from a Birdlist file and merges it into a taxonomy file.
 */
public class BirdlifeIucnDataMerger {
  private static final Logger logger = Logger.getLogger(BirdlifeIucnDataMerger.class
      .getName());
  private static final ImmutableMap<String, Status> STATUS_BY_STRING =
      Arrays.stream(Species.Status.values())
          .collect(ImmutableMap.toImmutableMap(Species.Status::toString, s -> s));
  
  private final Taxonomy taxonomy;

  private final Map<String, Taxon> commonNames = Maps.newHashMap();
  private final Map<String, Taxon> alternateCommonNames = Maps.newHashMap();
  private final Map<String, Taxon> genera = Maps.newHashMap();
  private final Map<Integer, Taxon> matchedTaxa = new LinkedHashMap<>();
  private IucnData iucnData;

  public static void main(String[] args) throws Exception {
    Taxonomy inTaxonomy;
    if (args.length == 4) {
      // Use arg 3 as the root taxonomy, arg 0 as the mapped taxonomy
      inTaxonomy = ToolTaxonUtils.getMappedTaxonomy(args[3], args[0]);
    } else {
      inTaxonomy = ToolTaxonUtils.getTaxonomy(args[0]);
    }
    
    IucnData iucnData = new IucnData();
    iucnData.parse(new File(args[1]));
      
    new BirdlifeIucnDataMerger(inTaxonomy, iucnData).run();

    try (OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(args[2]), Charsets.UTF_8)) {
      new XmlTaxonExport().export(out, inTaxonomy, "UTF-8");
    }
  }

  BirdlifeIucnDataMerger(
      Taxonomy taxonomy,
      IucnData iucnData) {
    this.taxonomy = taxonomy;
    this.iucnData = iucnData;
  }

  public void run() {
    int unmatched = 0;
    int matched = 0;
    // Pass 1: look for exact Genus + species matchings
    for (int id : iucnData.indexToStatus.keySet()) {
      Taxon taxon = taxonomy.findSpecies(iucnData.indexToSciName.get(id));
      if (taxon == null) {
        unmatched++;
      } else {
        matched++;
        matchedTaxa.put(id, taxon);
      }
    }
    
    logger.info("Pass 1, matches:" + matched);
    logger.info("Pass 1, no matches:" + unmatched);
    
    // Pass 2: exact common name matches
    buildIndicesOnTaxonomy();
    unmatched = 0;
    matched = 0;
    int dubiousMatches = 0;
    for (int id : unmatchedIds()) {
      String commonName = iucnData.indexToCommonName.get(id);
      Taxon taxon = commonNames.get(normalizeCommonName(commonName));

      if (taxon == null) {
        unmatched++;
      } else {
        matchedTaxa.put(id, taxon);
        String sciName = iucnData.indexToSciName.get(id);
        // Make sure the names are fairly close, or flag for differences
        if (!taxon.getName().equals(sciName) &&
            LevenshteinDistance.getDefaultInstance().apply(
                taxon.getName(), sciName) > 2) {
//          logger.log(Level.INFO, "Dubious match\n{0}\n{1}", new Object[]{taxon, sciName});
          
          dubiousMatches++;
        }        
        matched++;
      }
    }

    logger.info("Pass 2, matches:" + matched + "(dubious: " + dubiousMatches + ")");
    logger.info("Pass 2, no matches:" + unmatched);
    

    unmatched = 0;
    matched = 0;
    dubiousMatches = 0;
    for (int id : unmatchedIds()) {
      String commonName = iucnData.indexToCommonName.get(id);
      Taxon taxon = alternateCommonNames.get(normalizeCommonName(commonName));

      if (taxon == null) {
        unmatched++;
      } else {
        matchedTaxa.put(id, taxon);
        String sciName = iucnData.indexToSciName.get(id);
        // Make sure the names are fairly close, or flag for differences
        if (!taxon.getName().equals(sciName) &&
            LevenshteinDistance.getDefaultInstance().apply(
                taxon.getName(), sciName) > 2) {
        //  logger.log(Level.INFO, "Dubious match\n{0}\n{1}", new Object[]{taxon, sciName});
          
          dubiousMatches++;
        }        
        matched++;
      }
    }

    logger.info("Pass 3, matches:" + matched + "(dubious: " + dubiousMatches + ")");
    logger.info("Pass 3, no matches:" + unmatched);
    
    // Pass 4: try to find groups
    unmatched = 0;
    matched = 0;
    for (int id : unmatchedIds()) {
      String sciName = iucnData.indexToSciName.get(id);
      String genusName = sciName.substring(0, sciName.indexOf(' '));
      String speciesName = sciName.substring(sciName.indexOf(' ') + 1);
      
      Taxon genus = genera.get(genusName);
      if (genus != null) {
        Queue<Taxon> groups = gatherTaxa(genus, Taxon.Type.group);
        Taxon taxon = null;
        for (Taxon group : groups) {
          if (group.getName().equals(speciesName)
              || LevenshteinDistance.getDefaultInstance().apply(group.getName(), speciesName) <= 2) {
            taxon = group;
            break;
          }
        }
        
        if (taxon != null) {
          matchedTaxa.put(id, taxon);
          matched++;
          logger.log(Level.INFO, "Matched by group: " + sciName);
        } else {
          unmatched++;
        }
      }
    }

    logger.info("Pass 4, matches:" + matched);
    logger.info("Pass 4, no matches:" + unmatched);

    for (int id : iucnData.indexToStatus.keySet()) {
      Taxon matchedTaxon = matchedTaxa.get(id);
      if (matchedTaxon != null) {
        SpeciesImpl species = (SpeciesImpl) matchedTaxon;
        Species.Status status = iucnData.indexToStatus.get(id);
        if (species.getStatus() != status) {
          System.out.printf("%s: %s -> %s\n", species.getCommonName(), species.getStatus(), status);
          species.setStatus(status);
        }
      } else {
        logger.warning("UNMATCHED: "  + iucnData.indexToSciName.get(id) + ", " + iucnData.indexToCommonName.get(id) + ": " + iucnData.indexToStatus.get(id));
      }
    }
  }

  private Queue<Taxon> gatherTaxa(Taxon parent, final Taxon.Type type) {
    final Queue<Taxon> gathered = new LinkedList<Taxon>();
    TaxonUtils.visitTaxa(parent, new TaxonVisitor() {
      @Override
      public boolean visitTaxon(Taxon taxon) {
        if (taxon.getType() == type) {
          gathered.add(taxon);
          return false;
        }

        return true;
      }
    });

    return gathered;
  }

  private Iterable<Integer> unmatchedIds() {
    return Iterables.filter(iucnData.indexToStatus.keySet(), id -> !matchedTaxa.containsKey(id));
  }
  
  
  private String normalizeCommonName(String commonName) {
    return commonName.replace("-", " ").replace('ç', 'c').toUpperCase().replace("GREY", "GRAY");
  }
  
  private void buildIndicesOnTaxonomy() {
    TaxonUtils.visitTaxa(taxonomy, new TaxonVisitor() {
      @Override
      public boolean visitTaxon(Taxon taxon) {
        switch (taxon.getType()) {
          case species:
            commonNames.put(normalizeCommonName(taxon.getCommonName()), taxon);
            for (String alternateCommon : ((Species) taxon).getAlternateCommonNames()) {
              alternateCommonNames.put(normalizeCommonName(alternateCommon), taxon);
            }
            break;
          case genus:
            genera.put(taxon.getName(), taxon);
            break;
            
          case group:
            for (String alternateCommon : ((Species) taxon).getAlternateCommonNames()) {
              alternateCommonNames.put(normalizeCommonName(alternateCommon), taxon);
            }
            break;

          case subspecies:
            for (String alternateCommon : ((Species) taxon).getAlternateCommonNames()) {
              alternateCommonNames.put(normalizeCommonName(alternateCommon), taxon);
            }
            break;

          default:
            break;
        }

        return true;
      }

    });
  }

  static class IucnData {
    final Map<Integer, String> indexToSciName = new LinkedHashMap<>();
    final Map<Integer, Species.Status> indexToStatus = new LinkedHashMap<>();
    final Map<Integer, String> indexToCommonName = new LinkedHashMap<>();
    
    public void parse(File file) throws IOException {
      try (ImportLines importLines = CsvImportLines
          .fromFile(file, StandardCharsets.UTF_8)) {
        // Skip the header
        importLines.nextLine();
        while (true) {
          String[] line = importLines.nextLine();
          if (line == null) {
            break;
          }
          
          if (line.length < 5) {
            continue;
          }
          
          int index = Integer.parseInt(line[0]);
          indexToSciName.put(index, line[1]);
          indexToCommonName.put(index, line[2]);
          Species.Status status;
          
          // possible-extinct not supported in Scythebill
          if (line[4].equals("CR(PE)")) {
            status = Status.CR;
          } else {
            status = Preconditions.checkNotNull(STATUS_BY_STRING.get(line[4]), "No status for '%s'", line[4]);
          }
          indexToStatus.put(index, status);
        }
      }      
    }
  }
}
