package com.scythebill.birdlist.tools.reportset;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.nio.file.StandardOpenOption.WRITE;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.joda.time.DateTimeFieldType;
import org.joda.time.Partial;
import org.xml.sax.SAXException;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.checklist.TransposedChecklist;
import com.scythebill.birdlist.model.checklist.TransposedChecklists;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.xml.XmlReportSetExport;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;

public class CreateReportSetWithOneOfEveryTaxa {
  @Parameter(names = "-clements", required = true)
  private String clementsFileName;

  @Parameter(names = "-ioc", required = false,
      description = "Path to the IOC;  if omitted, one of each Clements taxa is generated.")
  private String iocFileName;

  @Parameter(names = "-output", required = true)
  private String output;

  @Parameter(names = "-withSubspecies", required = false)
  private boolean withSubspecies = false;

  @Parameter(names = "-sightingsCount", required = false)
  private int sightingsCount = 1;

  public static void main(String[] args) throws Exception {
    CreateReportSetWithOneOfEveryTaxa creator = new CreateReportSetWithOneOfEveryTaxa();
    new JCommander(creator, args);

    Taxonomy taxonomy = creator.loadTaxonomies();

    Path path = FileSystems.getDefault().getPath(creator.output);
    Files.createDirectories(path.getParent());

    try (Writer out = Files.newBufferedWriter(path, UTF_8, CREATE, WRITE, TRUNCATE_EXISTING)) {
      new XmlReportSetExport().export(out, "UTF-8", creator.newReportSet(taxonomy),
          TaxonUtils.getBaseTaxonomy(taxonomy));

    }
  }

  private ReportSet newReportSet(Taxonomy taxonomy) {
    ReportSet reportSet = ReportSets.newReportSet(TaxonUtils.getBaseTaxonomy(taxonomy));
    TransposedChecklist checklist =
        TransposedChecklists.instance().getTransposedChecklist(reportSet, taxonomy);
    PredefinedLocations predefined = PredefinedLocations.loadAndParse();
    ImmutableSet<Status> statuses =
        ImmutableSet.of(Status.ENDEMIC, Status.EXTINCT, Status.INTRODUCED, Status.NATIVE);
    
    Partial[] dates = new Partial[sightingsCount];
    for (int i = 0; i < sightingsCount; i++) {
      dates[i] = new Partial().with(DateTimeFieldType.year(), 1950 + i);
    }
    
    TaxonUtils.visitTaxa(taxonomy, taxon -> {
      // Recurse into genera and above
      if (taxon.getType().compareTo(Taxon.Type.species) > 0) {
        return true;
      }

      // Skip any explicitly disabled taxa
      if (taxon.isDisabled()) {
        return false;
      }

      // Otherwise, go! Generate one sighting for every location that has the species
      Iterable<String> locationsWithStatuses = checklist.locationsWithStatuses(taxon, statuses);
      if (Iterables.isEmpty(locationsWithStatuses)) {
        System.err.println("No checklist locations for " + taxon.getCommonName());
        // Just randomly put it in Colombia
        locationsWithStatuses = ImmutableSet.of("CO");
      }
      
      int locationsCount = Iterables.size(locationsWithStatuses);

      for (String locationCode : locationsWithStatuses) {
        List<Sighting> sightings = new ArrayList<>(locationsCount * sightingsCount); 
        Location location = getLocation(reportSet, predefined, locationCode);
        if (location == null) {
          System.err.println("No location for " + locationCode);
        } else {
          reportSet.getLocations().ensureAdded(location);
          for (int i = 0; i < sightingsCount; i++) {
            sightings.add(
                Sighting.newBuilder().setLocation(location).setDate(dates[i]).setTaxon(taxon).build());
          }
        }
        if (sightings.isEmpty()) {
          System.err.println("No sightings for " + taxon.getCommonName());
        } else {
          reportSet.mutator().adding(sightings).mutate();
        }
      }

      // Only recurse further when withSubspecies is true
      return withSubspecies;
    });
    // Store this version of the taxonomy if it's IOC
    if (taxonomy instanceof MappedTaxonomy) {
      String prefs = String.format(
          "{\n" + "\"com.scythebill.birdlist.ui.events.TaxonomyPreferences.PerReportSet\": {\n"
              + "  \"lastTaxonomyId\": \"%s\",\n" + "  \"lastIocVersion\": \"%s\"\n" + "  }\n}",
          taxonomy.getId(), taxonomy.getId());
      reportSet.setPreferencesJson(prefs, false);
    }
    return reportSet;
  }

  private Optional<Location> findLocationByParent(ReportSet reportSet, String countryCode, String regionName) {
    return reportSet.getLocations().getLocationsByCode(countryCode).stream()
        .filter(l -> l.getParent().getModelName().equals(regionName)).findAny();
  }
  
  Location getLocation(ReportSet reportSet, PredefinedLocations predefined, String locationCode) {
    if (locationCode.equals("US")) {
      // US is weird; it appears in three places, but only one has a legit checklist
      return findLocationByParent(reportSet, "US", "North America").get();
    }

    Location location = Locations.getLocationByCodePossiblyCreating(reportSet.getLocations(),
        predefined, locationCode);
    if (location == null) {
      // Handle the Indonesia and Russia lists
      List<String> names = Splitter.on('-').splitToList(locationCode);
      if (names.size() == 2) {
        location = findLocationByParent(reportSet, names.get(0), names.get(1)).orElse(null);
      }
    }
    return location;
  }

  private Taxonomy loadTaxonomies() throws IOException, SAXException {
    Taxonomy clements = ToolTaxonUtils.getTaxonomy(clementsFileName);
    if (iocFileName != null) {
      return ToolTaxonUtils.getMappedTaxonomy(clements, iocFileName);
    }
    return clements;
  }
}
