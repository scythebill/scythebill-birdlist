package com.scythebill.birdlist.tools.reportset;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;

import org.xml.sax.SAXException;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.base.Optional;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.xml.XmlReportSetExport;
import com.scythebill.birdlist.model.xml.XmlReportSetImport;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;
import com.scythebill.birdlist.ui.panels.FilePreferences;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.util.FileDialogs;
import com.scythebill.birdlist.ui.util.FileDialogs.Flags;

public final class RemoveDuplicates {
  @Parameter(names = "-clements", required = true)
  private String clementsFileName;
  private FileDialogs fileDialogs;
  private Taxonomy clements;
  private final FileFilter reportSetFilter = new FileFilter() {
    @Override
    public String getDescription() {
      return "Sightings file";
    }
    
    @Override
    public boolean accept(File f) {
      return f.getName().endsWith(".bsxm");
    }
  };
  
  public static void main(String[] args) throws Exception {
    final RemoveDuplicates removeDuplicates = new RemoveDuplicates();
    new JCommander(removeDuplicates, args);
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        try {
          removeDuplicates.loadTaxonomy();
          ReportSet reportSet = removeDuplicates.loadReportSet();
          if (reportSet != null) {
            removeDuplicates.removeDuplicates(reportSet);
            removeDuplicates.saveReportSet(reportSet);
          }
        } catch (Exception e) {
          e.printStackTrace();
          System.exit(1);
        }
      }
    });
  }
  
  private void saveReportSet(ReportSet reportSet) throws IOException {
    File saveFile = fileDialogs.saveFile(null, "Save report set", "duplicates-removed.bsxm", reportSetFilter, FileType.SIGHTINGS);
    if (saveFile != null) {
      new XmlReportSetExport().export(new FileWriter(saveFile), "UTF-8", reportSet, clements);
    }
  }

  private void removeDuplicates(ReportSet reportSet) {
    List<Sighting> sightingsToRemove = Lists.newArrayList();
    Multimap<VisitInfoKey, SightingTaxon> alreadyFound = HashMultimap.create();
    for (Sighting sighting : reportSet.getSightings()) {
      VisitInfoKey visitInfoKey = VisitInfoKey.forSighting(sighting);
      if (visitInfoKey == null) {
        continue;
      }
      if (alreadyFound.containsEntry(visitInfoKey, sighting.getTaxon())) {
        sightingsToRemove.add(sighting);
      } else {
        alreadyFound.put(visitInfoKey, sighting.getTaxon());
      }
    }

    System.err.println("REMOVING " + sightingsToRemove.size() + " duplicates");
    reportSet.mutator().removing(sightingsToRemove).mutate();
  }

  private void loadTaxonomy() throws IOException, SAXException {
    clements = ToolTaxonUtils.getTaxonomy(clementsFileName);
  }

  private ReportSet loadReportSet() throws FileNotFoundException, IOException, SAXException {
    File file = fileDialogs.openFile(null, "Open sightings file", reportSetFilter, FileType.SIGHTINGS);
    
    if (file == null) {
      return null;
    }
    
    return new XmlReportSetImport().importReportSet(
        new FileReader(file), clements, Optional.<MappedTaxonomy>absent(), null);
  }

  RemoveDuplicates() {
    Flags flags = new Flags() {

      @Override
      public boolean useNativeSaveDialog() {
        return true;
      }

      @Override
      public boolean useNativeOpenDialog() {
        return true;
      }
    };
    fileDialogs = new FileDialogs(
        new FilePreferences(), flags, null);
    
  }
}
