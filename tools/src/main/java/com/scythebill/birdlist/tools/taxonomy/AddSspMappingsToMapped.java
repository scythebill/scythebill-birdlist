/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.xml.XmlTaxonExport;

/**
 * Add "SINGLE_WITH_SSP" mappings. 
 */
public class AddSspMappingsToMapped implements TaxonVisitor {
  public static void main(String[] args) throws Exception {
    MappedTaxonomy taxonomy = CleanAlternateNamesFromMapped.getMappedTaxonomy(args[0], args[1]);

    TaxonUtils.visitTaxa(taxonomy.getRoot(), new AddSspMappingsToMapped());
    
    OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(args[2]), "UTF-8");
    (new XmlTaxonExport()).export(out, taxonomy, "UTF-8");
    out.close();
  }

  public AddSspMappingsToMapped() {
  }

  @Override
  public boolean visitTaxon(Taxon taxon) {
    if (taxon.getType() == Taxon.Type.subspecies) {
      MappedTaxonomy taxonomy = (MappedTaxonomy) taxon.getTaxonomy();
      SightingTaxon exactMapping = taxonomy.getExactMapping(taxon);
      if (exactMapping == null) {
        SightingTaxon newMapping = null;
        Taxon species = TaxonUtils.getParentOfType(taxon, Taxon.Type.species);
        SightingTaxon speciesMapping = taxonomy.getExactMapping(species);
        if (speciesMapping != null && isSingleTypeMapping(speciesMapping)) {
          newMapping = SightingTaxons.newSightingTaxonWithSecondarySubspecies(
              speciesMapping.getId(), taxon.getName());
        } else {
          int indexOf = taxon.getParent().getContents().indexOf(taxon);
          if (indexOf >= 0 && indexOf < taxon.getParent().getContents().size()) {
            // Try mapping the previous subspecies and following subspecies back to the
            // base taxonomy
            SightingTaxon previousMapping = null;
            SightingTaxon nextMapping = null;
            for (int i = indexOf - 1; i >= 0; i--) {
              Taxon previous = taxon.getParent().getContents().get(i);
              previousMapping = taxonomy.getExactMapping(previous);
              if (previousMapping != null) { 
                break;
              }
            }

            for (int i = indexOf + 1; i < taxon.getParent().getContents().size(); i++) {
              Taxon next = taxon.getParent().getContents().get(i);
              nextMapping = taxonomy.getExactMapping(next);
              if (nextMapping != null) { 
                break;
              }
            }

            // With a previous and next mapping found, resolve both back to the base taxonomy.
            // Then, find their shared parent, and use that.
            if (previousMapping != null
                && isSingleTypeMapping(previousMapping)
                && nextMapping != null
                && isSingleTypeMapping(nextMapping)) {
              Resolved previousResolved = previousMapping.resolve(taxonomy.getBaseTaxonomy());
              Resolved nextResolved = nextMapping.resolve(taxonomy.getBaseTaxonomy());
              Taxon sharedParent = TaxonUtils.getSharedParent(
                  previousResolved.getTaxon(), nextResolved.getTaxon());
              // As long as the shared parent is a species, it's fine.
              if (sharedParent.getType().compareTo(Taxon.Type.species) <= 0) {
                System.err.printf("Mapped %s to parent %s, based off mappings %s and %s\n",
                    TaxonUtils.getCommonName(taxon),
                    TaxonUtils.getCommonName(sharedParent),
                    previousResolved.getCommonName(),
                    nextResolved.getCommonName());
                newMapping = SightingTaxons.newSightingTaxonWithSecondarySubspecies(
                    sharedParent.getId(), taxon.getName());
              } else {
                System.err.printf("Could not map %s, because shared parent %s of %s and %s is not a species\n",
                    TaxonUtils.getFullName(taxon),
                    TaxonUtils.getFullName(sharedParent),
                    previousResolved.getCommonName(),
                    nextResolved.getCommonName());
              }
            } else if (previousMapping != null
                && isSingleTypeMapping(previousMapping)) {
              // We're at the end - so use the previous mapping.
              Resolved previousResolved = previousMapping.resolve(taxonomy.getBaseTaxonomy());
              Taxon previousParent = previousResolved.getTaxon();
              if (previousParent.getType() != Taxon.Type.species) {
                previousParent = previousParent.getParent();
              }
              if (previousParent.getType().compareTo(Taxon.Type.species) <= 0) {
                System.err.printf("Mapped %s to parent %s, based off previous mapping %s\n",
                    TaxonUtils.getCommonName(taxon),
                    TaxonUtils.getCommonName(previousParent),
                    previousResolved.getCommonName());
                newMapping = SightingTaxons.newSightingTaxonWithSecondarySubspecies(
                    previousParent.getId(), taxon.getName());
              } else {
                System.err.printf("Could not map %s, because parent %s of previous mapping %s is not a species\n",
                    TaxonUtils.getFullName(taxon),
                    TaxonUtils.getFullName(previousParent),
                    previousResolved.getCommonName());
              }
            } else if (nextMapping != null
                && isSingleTypeMapping(nextMapping)) {
              // We're at the end - so use the previous mapping.
              Resolved nextResolved = nextMapping.resolve(taxonomy.getBaseTaxonomy());
              Taxon nextParent = nextResolved.getTaxon();
              if (nextParent.getType() != Taxon.Type.species) {
                nextParent = nextParent.getParent();
              }
              if (nextParent.getType().compareTo(Taxon.Type.species) <= 0) {
                System.err.printf("Mapped %s to parent %s, based off next mapping %s\n",
                    TaxonUtils.getCommonName(taxon),
                    TaxonUtils.getCommonName(nextParent),
                    nextResolved.getCommonName());
                newMapping = SightingTaxons.newSightingTaxonWithSecondarySubspecies(
                    nextParent.getId(), taxon.getName());
              } else {
                System.err.printf("Could not map %s, because parent %s of next mapping %s is not a species\n",
                    TaxonUtils.getFullName(taxon),
                    TaxonUtils.getFullName(nextParent),
                    nextResolved.getCommonName());
              }
            }

            
            if (previousMapping == null && nextMapping == null) {
              System.err.printf("No previous or next for %s\n", TaxonUtils.getCommonName(taxon));
            }
          }
        }
        if (newMapping != null) {
          taxonomy.addMapping(taxon, newMapping);        
        } else {
          System.err.printf("Could not resolve %s\n",
              TaxonUtils.getFullName(taxon));
        }
      }      
    }
    return true;
  }

  private boolean isSingleTypeMapping(SightingTaxon previousMapping) {
    return previousMapping.getType() == SightingTaxon.Type.SINGLE
        || previousMapping.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES;
  }
}
