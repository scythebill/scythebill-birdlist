/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.util.List;
import java.util.Map;

import org.xml.sax.SAXException;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.MoreExecutors;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.SpeciesImpl;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.TaxonomyIndexer;
import com.scythebill.birdlist.model.xml.XmlTaxonExport;
import com.scythebill.birdlist.model.xml.XmlTaxonImport;

/**
 * Clean up the alternate names of a taxonomy.
 */
public class CleanAlternateNames implements TaxonVisitor {
  public static void main(String[] args) throws Exception {
    Taxonomy taxonomy = getTaxonomy(args[0]);
    Map<String, String> substitutes = Splitter.on(',').withKeyValueSeparator("=")
        .split(args[2]);

    TaxonUtils.visitTaxa(taxonomy.getRoot(), new CleanAlternateNames(substitutes));
    
    OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(args[1]), "UTF-8");
    
    (new XmlTaxonExport()).export(out, taxonomy, "UTF-8");
    out.close();
  }

  private final ImmutableMap<String, String> substitutes;

  public CleanAlternateNames(Map<String, String> substitutes) {
    this.substitutes = ImmutableMap.copyOf(substitutes);
  }

  public static Taxonomy getTaxonomy(String filename) throws IOException, SAXException {
    Reader reader = null;
    try {
      File file = new File(filename);
      reader = new BufferedReader(new FileReader(file));
      Taxonomy taxonomy = (new XmlTaxonImport()).importTaxa(reader);
      Futures.getUnchecked(new TaxonomyIndexer(Futures.immediateFuture(taxonomy))
              .load(MoreExecutors.newDirectExecutorService()));
      return taxonomy;
    } finally {
      if (reader != null) {
        reader.close();
      }
    }
  }

  private static final CharMatcher WORD_SEPARATORS = CharMatcher.anyOf(" -");
  
  @Override
  public boolean visitTaxon(Taxon taxon) {
    if (taxon instanceof Species) {
      Species species = (Species) taxon;
      if (!species.getAlternateCommonNames().isEmpty()) {
        String commonName = TaxonUtils.getParentOfType(taxon, Taxon.Type.species).getCommonName();
        if (commonName != null) {
          List<String> newCommonNames = Lists.newArrayList();
          for (String alternateCommonName : species.getAlternateCommonNames()) {
            if (alternateCommonName.contains("undescribed")
                || alternateCommonName.contains("sp. nov.")) {
              continue;
            }

            if (hasLastTwo(commonName)) {
              String lastTwo = getLastTwo(commonName);
              String alternateLastTwo = switchSeparator(lastTwo);
              
              // If the common name and alternate end with end sequences that only differ
              // in separator or capitalization (e.g. Fruit Dove vs. Fruit-Dove),
              // just use the one on the common name
              if (hasLastTwo(alternateCommonName)) {
                if (alternateCommonName.toLowerCase().endsWith(lastTwo.toLowerCase())
                    || alternateCommonName.toLowerCase().endsWith(alternateLastTwo.toLowerCase())) {
                  alternateCommonName = alternateCommonName.substring(
                          0, alternateCommonName.length() - lastTwo.length()) + lastTwo;
                }
              }
            } else {
              // If the main name is "Brushturkey" and the alternates are "Brush-Turkey",
              // swap in the main
              if (hasLastTwo(alternateCommonName)) {
                String lastTwo = getLastTwo(alternateCommonName);
                String noSeparator = removeSeparator(lastTwo);
                if (commonName.endsWith(noSeparator)) {
                  alternateCommonName = alternateCommonName.replace(lastTwo,  noSeparator);
                }
              }
            }
            
            // Swap "Gray" for "Grey" or vice versa
            for (Map.Entry<String, String> substituteEntry : substitutes.entrySet()) {
              String key = substituteEntry.getKey();
              String value = substituteEntry.getValue();
              if (alternateCommonName.contains(key)) {
                // ... but only bother for "Gray " or "Gray-";  ignore anything else (so
                // we don't replace "Gray's" with "Grey's"
                alternateCommonName = alternateCommonName.replace(key + " ", value + " ");
                alternateCommonName = alternateCommonName.replace(key + "-", value + "-");
              }
            }
  
            // And skip names that only differ in capitalization
            if (!alternateCommonName.equalsIgnoreCase(commonName)) {
              newCommonNames.add(alternateCommonName);
            }
            ((SpeciesImpl) species).setAlternateCommonNames(newCommonNames);
          }
        }
      }
      
      // Only keep "alternate' scientific names that are actually different
      if (!species.getAlternateNames().isEmpty()) {
        String sciName = TaxonUtils.getFullName(taxon);
        List<String> newSciNames = Lists.newArrayList();
        for (String alternateSciName : species.getAlternateNames()) {
          if (alternateSciName.contains("undescribed")
              || alternateSciName.contains("sp. nov.")) {
            continue;
          }
          
          if (!sciName.equalsIgnoreCase(alternateSciName)) {
            newSciNames.add(alternateSciName);
          }
        }
        ((SpeciesImpl) species).setAlternateNames(newSciNames);
      }
    }
    return true;
  }

  private String switchSeparator(String twoWords) {
    if (twoWords.contains(" ")) {
      return twoWords.replace(' ', '-');
    } else {
      return twoWords.replace('-', ' ');
    }
  }

  private boolean hasLastTwo(String commonName) {
    // Must be at least three words
    if (Iterables.size(
        Splitter.on(WORD_SEPARATORS).split(commonName)) > 2) {
      // And there'd better not be a - before the last one
      int lastSeparator = WORD_SEPARATORS.lastIndexIn(commonName);
      String absentLast = commonName.substring(0, lastSeparator);
      int penultimateSeparator = WORD_SEPARATORS.lastIndexIn(absentLast);
      if (absentLast.charAt(penultimateSeparator) == ' ') {
        return true;
      }
    }
    
    return false;
  }

  private String getLastTwo(String commonName) {
    int lastSeparator = WORD_SEPARATORS.lastIndexIn(commonName);
    String absentLast = commonName.substring(0, lastSeparator);
    int penultimateSeparator = WORD_SEPARATORS.lastIndexIn(absentLast);
    String lastTwo = commonName.substring(penultimateSeparator + 1);
    return lastTwo;
  }

  /**
   * Brush-Turkey -> Brushturkey.
   */
  private String removeSeparator(String name) {
    int separator = WORD_SEPARATORS.indexIn(name);
    return name.substring(0, separator) +
        name.substring(separator + 1).toLowerCase();
  }
}
