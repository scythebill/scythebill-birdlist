/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.util.Map;

import org.xml.sax.SAXException;

import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.MoreExecutors;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.TaxonomyIndexer;
import com.scythebill.birdlist.model.xml.XmlTaxonExport;
import com.scythebill.birdlist.model.xml.XmlTaxonImport;

/**
 * Cleanthe alternate names of a mapped taxonomy. 
 */
public class CleanAlternateNamesFromMapped extends CleanAlternateNames {
  public static void main(String[] args) throws Exception {
    MappedTaxonomy taxonomy = getMappedTaxonomy(args[0], args[1]);
    Map<String, String> substitutes = Splitter.on(',').withKeyValueSeparator("=")
        .split(args[3]);

    TaxonUtils.visitTaxa(taxonomy.getRoot(), new CleanAlternateNamesFromMapped(substitutes));
    
    OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(args[2]), "UTF-8");
    (new XmlTaxonExport()).export(out, taxonomy, "UTF-8");
    out.close();
  }

  public static MappedTaxonomy getMappedTaxonomy(
          String baseTaxonomy, String filename) throws IOException,
          SAXException {
    Taxonomy base = getTaxonomy(baseTaxonomy);
    Reader reader = null;
    try {
      File file = new File(filename);
      reader = new BufferedReader(new FileReader(file));
      MappedTaxonomy taxonomy = (new XmlTaxonImport()).importMappedTaxa(reader, base);
      
      Futures.getUnchecked(new TaxonomyIndexer(Futures.immediateFuture(taxonomy)).load(
          MoreExecutors.newDirectExecutorService()));
      return taxonomy;
    } finally {
      reader.close();
    }
  }

  public static Taxonomy getTaxonomy(String filename) throws IOException, SAXException {
    Reader reader = null;
    try {
      File file = new File(filename);
      reader = new BufferedReader(new FileReader(file));
      Taxonomy taxonomy = (new XmlTaxonImport()).importTaxa(reader);
      Futures.getUnchecked(new TaxonomyIndexer(Futures.immediateFuture(taxonomy))
              .load(MoreExecutors.newDirectExecutorService()));
      return taxonomy;
    } finally {
      reader.close();
    }
  }

  public CleanAlternateNamesFromMapped(Map<String, String> substitutes) {
    super(substitutes);
  }

  @Override
  public boolean visitTaxon(Taxon taxon) {
    super.visitTaxon(taxon);
    
//    if (taxon instanceof Species) {
//      Species species = (Species) taxon;
//      String commonName = species.getCommonName();
//      if (commonName != null) {
//        String endOfCurrentName = getSuffixOf(species.getCommonName());
//        List<String> alternates = Lists.newArrayList(species.getAlternateCommonNames());
//        for (int i = alternates.size() - 1; i >= 0; i--) {
//          String alternate = alternates.get(i);
//          // Remove any alternate that's just too close
//          if (StringUtils.getLevenshteinDistance(commonName, alternate) < 2) {
//            alternates.remove(i);
//          } else {
//            String alternateEnd = getSuffixOf(alternate);
//            if (StringUtils.getLevenshteinDistance(endOfCurrentName, alternateEnd) < 2) {
//              alternates.set(i, alternate.replace(alternateEnd, endOfCurrentName));
//            }
//          }
//        }
//        ((SpeciesImpl) species).setAlternateCommonNames(alternates);
//      }
//    }
    return true;
  }

  private String getSuffixOf(String string) {
    return Iterables.getLast(Splitter.on(' ').split(string));
  }

}
