/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.SpeciesImpl;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.TaxonImpl;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;
import com.scythebill.birdlist.model.xml.XmlTaxonExport;

/**
 * Loads a CSV file of the form from the Cornell Clements list. 
 */
public class ClementsCsvLoader {
  private final static Logger logger = Logger.getLogger(ClementsCsvLoader.class.getName());

  /**
   * sort v2024,species_code,Clements v2024b change,text for website v2024,category,English name,scientific name,
   * range,order,family,extinct,extinct year,sort v2023
   */
  /**
   * sort v2022,Clements v2022 change,text for website v2022,category,English name,scientific name,
   * authority,name and authority,range,order,family,extinct,extinct year,sort v2021,page 6.0
   */
  /**
   * sort v2021,Clements v2021 change,text for website v2021,category,English name,scientific name,
   * range,order,family,extinct,extinct year,sort v2019,page 6.0
   */
  /**
   * Clements 6.8 change,2013 Text for website,sort 6.8,Category,Scientific name,English name,
   * Range,Order,Family,Extinct,Extinction Year,Sort 6.7 etc...  Sort 6.6  Page 6.0
   */
  /**
   * Clements 6.7 change,2012 Text for website,sort 6.7,,CATEGORY,SCIENTIFIC NAME,ENGLISH NAME,
   * RANGE,ORDER,FAMILY,EXTINCT,EXTINCT_YEAR,SORT 6.6,SORT 6.5,PAGE 6.0
   */
  /**
   * Clements 6.6 change,Text for website,Sort 6.6,CATEGORY,SCIENTIFIC NAME,ENGLISH NAME,
   * RANGE,ORDER,FAMILY,EXTINCT,EXTINCT_YEAR,SORT 6.5,PAGE 6.0,Protected Sort 4 Aug 2010,,,,,,,
   */
  /**
   * "Sort 6.5","Sort 6.4","Page 6.0","Category","Extinct","Extinct year",
   * "Scientific name","English name","Range","Order","Family","Change","Change comment"
   */
  private static final int INDEX_SPECIES_CODE = 1;
  private static final int INDEX_CHANGE = 2;
  private static final int INDEX_CHANGE_COMMENT = 3;
  private static final int INDEX_CATEGORY = 4;
  private static final int INDEX_COMMON = 5;
  private static final int INDEX_SCIENTIFIC = 6;
  private static final int INDEX_RANGE = 7;
  private static final int INDEX_ORDER = 8;
  private static final int INDEX_FAMILY = 9;
  private static final int INDEX_EXTINCT = 10;
  private static final int INDEX_EXTINCT_YEAR = 11;
  
  private final Pattern familyNamePattern = Pattern.compile("^(.*) \\((.*)\\)$");
 
  private final static ImmutableMap<String, String> ORDER_COMMON_NAMES;
  static {
    ImmutableMap.Builder<String, String> orderCommonNames = ImmutableMap.builder();
    orderCommonNames.put("Struthioniformes", "Ostriches");
    orderCommonNames.put("Rheiformes", "Rheas");
    orderCommonNames.put("Tinamiformes", "Tinamous");
    orderCommonNames.put("Casuariiformes", "Cassowaries and Emu");
    orderCommonNames.put("Apterygiformes", "Kiwis");
    orderCommonNames.put("Anseriformes", "Waterfowl");
    orderCommonNames.put("Galliformes", "Megapodes to Pheasants");
    orderCommonNames.put("Gaviiformes", "Loons");
    orderCommonNames.put("Podicipediformes", "Grebes");
    orderCommonNames.put("Phoenicopteriformes", "Flamingos");
    orderCommonNames.put("Sphenisciformes", "Penguins");
    orderCommonNames.put("Procellariiformes", "Tubenoses");
    orderCommonNames.put("Phaethontiformes", "Tropicbirds");
    orderCommonNames.put("Ciconiiformes", "Storks");
    orderCommonNames.put("Suliformes", "Gannets, Cormorants, etc.");
    orderCommonNames.put("Pelecaniformes", "Ibises, Herons, Pelicans");
    orderCommonNames.put("Accipitriformes", "Hawks, Vultures, etc.");
    orderCommonNames.put("Musophagiformes", "Turacos");
    orderCommonNames.put("Otidiformes", "Bustards");
    orderCommonNames.put("Mesitornithiformes", "Mesites");
    orderCommonNames.put("Eurypygiformes", "Kagu and Sunbittern");
    orderCommonNames.put("Gruiformes", "Rails, Cranes, etc.");
    orderCommonNames.put("Charadriiformes", "Shorebirds, Gulls, etc.");
    orderCommonNames.put("Pterocliformes", "Sandgrouse");
    orderCommonNames.put("Columbiformes", "Pigeons and Doves");
    orderCommonNames.put("Cuculiformes", "Cuckoos");
    orderCommonNames.put("Opisthocomiformes", "Hoatzin");
    orderCommonNames.put("Strigiformes", "Owls");
    orderCommonNames.put("Nyctibiiformes", "Potoos");
    orderCommonNames.put("Steatornithiformes", "Oilbird");
    orderCommonNames.put("Podargiformes", "Frogmouths");
    orderCommonNames.put("Aegotheliformes", "Owlet-nightjars");
    orderCommonNames.put("Apodiformes", "Swifts and Hummingbirds");
    orderCommonNames.put("Caprimulgiformes", "Nightjars");
    orderCommonNames.put("Coliiformes", "Mousebirds");
    orderCommonNames.put("Leptosomiformes", "Cuckoo-Roller");
    orderCommonNames.put("Trogoniformes", "Trogons");
    orderCommonNames.put("Bucerotiformes", "Hornbills and Hoopoes");
    orderCommonNames.put("Coraciiformes", "Kingfishers, Rollers, etc.");
    orderCommonNames.put("Galbuliformes", "Puffbirds and Jacamars");
    orderCommonNames.put("Piciformes", "Woodpeckers, Barbets, etc.");
    orderCommonNames.put("Cariamiformes", "Seriemas");
    orderCommonNames.put("Falconiformes", "Falcons");
    orderCommonNames.put("Psittaciformes", "Parrots");
    orderCommonNames.put("Passeriformes", "Perching Birds");
    orderCommonNames.put("Cathartiformes", "New World Vultures");

    ORDER_COMMON_NAMES = orderCommonNames.build();
  }

  private static final ImmutableSet<String> IGNORED_CATEGORIES = ImmutableSet.of(
      "slash",
      "hybrid",
      "form",
      "domestic",
      "intergrade",
      "spuh",
      "family",
      "");
  private TaxonomyImpl aves;
  private String currentOrderName;
  private TaxonImpl currentOrder;
  private String currentFamilyName;
  private TaxonImpl currentFamily;  
  private String currentGenusName;
  private TaxonImpl currentGenus;
  private SpeciesImpl currentSpecies;
  private SpeciesImpl lineTaxon;
  private SpeciesImpl currentGroup;
  
  static public void main(String[] args) throws IOException {
    // Note - the CSV must have been exported using UTF-8
    File clementsCsv = new File(args[0]);
    ClementsCsvLoader loader = new ClementsCsvLoader();
    Taxonomy loadClementsCsv = loader.loadClementsCsv(clementsCsv);

    OutputStreamWriter out = new OutputStreamWriter(
        new FileOutputStream("/tmp/clements-taxon.xml"), "UTF-8");
    (new XmlTaxonExport()).export(out, loadClementsCsv, "UTF-8");
    out.close();
  }
  
  public ClementsCsvLoader() {
    aves = new TaxonomyImpl("clements2024", "eBird/Clements 2024", "https://ebird.org/species/{id}", "eBird"); 
    TaxonImpl root = new TaxonImpl(Type.classTaxon, aves);
    root.setCommonName("Birds");
    root.setName("Aves");
    root.built();
    aves.setRoot(root);
  }
  
  public Taxonomy loadClementsCsv(File file) throws IOException {
    Reader reader = new BufferedReader(
        new InputStreamReader(
            new FileInputStream(file),
            Charsets.UTF_8));
    try (ImportLines csv = CsvImportLines.fromReader(reader)) {
      // Skip the header
      csv.nextLine();
      while (true) {
        String[] nextLine = csv.nextLine();
        if (nextLine == null) {
          break;
        }
        
        try {
          for (int i = 0; i < nextLine.length; i++) {
            nextLine[i] = CharMatcher.whitespace().trimFrom(nextLine[i]);
          }
          processLine(nextLine);
        } catch (Exception e) {
          logger.log(Level.SEVERE, "Could not process line:\n" + Lists.newArrayList(nextLine), e);
        }
      }
    }
    
    return aves;
  }

  private void processLine(String[] nextLine) {
    String category = nextLine[INDEX_CATEGORY];
    Type type;
    if ("species".equals(category)) {
      type = Type.species;
    } else if ("group (polytypic)".equals(category)) {
      type = Type.group;
    } else if ("group (monotypic)".equals(category)) {
      type = Type.group;
    } else if ("subspecies".equals(category)) {
      type = Type.subspecies;
    } else if (IGNORED_CATEGORIES.contains(category)) {
      return;
    } else {
      throw new IllegalArgumentException("Unknown category: \"" + category + "\"");
    }
    
    String orderName = nextLine[INDEX_ORDER];
    
    if (!"".equals(orderName) && !orderName.equals(currentOrderName)) {
      currentOrderName = orderName;
      currentOrder = new TaxonImpl(Type.order, aves.getRoot());
      
      currentOrder.setName(orderName);
      if (!ORDER_COMMON_NAMES.containsKey(orderName)) {
        throw new IllegalStateException("No common name for order " + orderName);
      }
      currentOrder.setCommonName(ORDER_COMMON_NAMES.get(orderName));
      aves.getRoot().getContents().add(currentOrder);
      currentFamilyName = null;
      currentFamily = null;
      currentOrder.built();
    }
    
    String familyName = nextLine[INDEX_FAMILY];
    if (!"".equals(familyName) && !familyName.equals(currentFamilyName)) {
      currentFamilyName = familyName;
      currentFamily = new TaxonImpl(Type.family, currentOrder);
      Matcher m = familyNamePattern.matcher(familyName);
      if (!m.matches()) {
        throw new IllegalArgumentException("Pattern didn't match " + familyName);
      }
      
      currentFamily.setName(m.group(1));
      currentFamily.setCommonName(m.group(2));
      currentOrder.getContents().add(currentFamily);
      currentFamily.built();
    }
    
    String scientificName = nextLine[INDEX_SCIENTIFIC];
    String genusName = scientificName.substring(0, scientificName.indexOf(' '));
    scientificName = scientificName.substring(scientificName.indexOf(' ') + 1);
    
    if (!genusName.equals(currentGenusName)) {
      if (currentGenus != null) {
        UndescribedSpecies.maybeAddUndescribedForms(currentGenus, UndescribedSpecies.ClementsOrIoc.CLEMENTS);
      }

      currentGenusName = genusName;
      currentGenus = new TaxonImpl(Type.genus, currentFamily);

      currentGenus.setName(genusName);
      currentFamily.getContents().add(currentGenus);
      currentGenus.built();
    }
    
    if (type == Type.species) {
      currentSpecies = new SpeciesImpl(currentGenus);
      lineTaxon = currentSpecies;
      currentSpecies.setName(scientificName);
      currentGroup = null;
    } else if (type == Type.group) {
      currentGroup = new SpeciesImpl(Type.group, currentSpecies);
      lineTaxon = currentGroup;
      currentGroup.setName(scientificName.substring(scientificName.indexOf(' ') + 1));
    } else if (type == Type.subspecies) {
      lineTaxon = new SpeciesImpl(Type.subspecies, currentGroup != null ? currentGroup : currentSpecies);
      String expectedSpeciesName = scientificName.substring(0, scientificName.indexOf(' '));
      if (!expectedSpeciesName.equals(currentSpecies.getName())) {
        throw new IllegalArgumentException("Misordered species? " + Arrays.asList(nextLine));
      }
      lineTaxon.setName(scientificName.substring(scientificName.indexOf(' ') + 1));
    }
    
    boolean extinct = "1".equals(nextLine[INDEX_EXTINCT]);
    if (extinct) {
      lineTaxon.setStatus(Status.EX);
      if (!Strings.isNullOrEmpty(nextLine[INDEX_EXTINCT_YEAR])) {
        lineTaxon.setMiscellaneousInfo("Extinct in " + nextLine[INDEX_EXTINCT_YEAR]);
      }
    }
    
    if (!"".equals(nextLine[INDEX_RANGE])) {
      lineTaxon.setRange(nextLine[INDEX_RANGE]);
    }
    
    if (!"".equals(nextLine[INDEX_COMMON])) {
      lineTaxon.setCommonName(nextLine[INDEX_COMMON]);
    }
    
    if ((type == Type.group || type == Type.species)
        && !Strings.isNullOrEmpty(nextLine[INDEX_SPECIES_CODE])) {
      lineTaxon.setAccountId(nextLine[INDEX_SPECIES_CODE]);
    }
    
    String change = nextLine[INDEX_CHANGE];
    String changeLower = change.toLowerCase();
    if (changeLower.contains("split")
        || changeLower.contains("lump")
        || changeLower.contains("delet")
        || changeLower.contains("subspecies")
        || changeLower.contains("correct")
        || changeLower.contains("change")) {
      if (!changeLower.equals("name change - english name")) {
        lineTaxon.setTaxonomicInfo(nextLine[INDEX_CHANGE_COMMENT]);
      }
    }
      
    lineTaxon.getParent().getContents().add(lineTaxon);
    lineTaxon.built();
    
    // Don't parent subsequent subspecies to the group if the group is considered monotypic
    if ("group (monotypic)".equals(category)) {
      currentGroup = null;
    }

    // Add various feral forms as groups (so they show up in eBird exports)
    FeralAndDomesticForms.maybeAddFeralOrDomesticForm(lineTaxon, Type.group, /*clements=*/true);
    // Add Wild Pigeon as a group (so it shows up in eBird exports)
    SpeciesImpl wildPigeon = FeralAndDomesticForms.maybeAddWildPigeon(lineTaxon, Type.group);
    if (wildPigeon != null) {
      currentGroup = wildPigeon;
    }
  }
}
