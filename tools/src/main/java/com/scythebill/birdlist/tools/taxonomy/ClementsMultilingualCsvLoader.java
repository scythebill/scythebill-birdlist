package com.scythebill.birdlist.tools.taxonomy;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Logger;

import org.xml.sax.SAXException;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.internal.Maps;
import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableTable;
import com.google.common.io.Resources;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;

public class ClementsMultilingualCsvLoader {
  public static void main(String[] args) throws Exception {
    new ClementsMultilingualCsvLoader(args).run(); 
 }

  private ClementsMultilingualCsvLoader(String[] args) {
    new JCommander(this, args);
  }

  private static final Logger logger = Logger.getLogger(ClementsMultilingualCsvLoader.class.getName());

  @Parameter(names = "-clements", required = true)
  private String clementsFileName;

  @Parameter(names = "-downloadDir", required = true)
  private String downloadDir;

  @Parameter(names = "-outputDir", required = true)
  private String outputDir;

  private static final ImmutableSet<Locale> LOCALES =
      ImmutableSet.of(
          new Locale("en", "UK"),
          new Locale("en", "AU"),
          new Locale("en", "IN"),
          new Locale("en", "IOC"),
          new Locale("en", "NZ"),
          new Locale("en", "ZA"),
          new Locale("ar"),
          new Locale("az"),
          new Locale("bg"),
          new Locale("cs"),
          new Locale("da"),
          new Locale("de"),
          new Locale("el"),
          new Locale("es"),
          new Locale("es", "AR"),
          new Locale("es", "CL"),
          new Locale("es", "CU"),
          new Locale("es", "DO"),
          new Locale("es", "EC"),
          new Locale("es", "ES"),
          new Locale("es", "MX"),
          new Locale("es", "PA"),
          new Locale("es", "PE"),
          new Locale("es", "PR"),
          new Locale("es", "UY"),
          new Locale("es", "VE"),
          new Locale("eu"),
          new Locale("fa"),
          new Locale("fi"),
          new Locale("fo"),
          new Locale("fr"),
          new Locale("fr", "CA"),
          new Locale("fr", "GP"),
          new Locale("fr", "HT"),
          new Locale("gl"),
          new Locale("he"),
          new Locale("hr"),
          new Locale("hu"),
          new Locale("hy"),
          new Locale("is", "IS"),
          new Locale("in"),
          new Locale("it"),
          new Locale("iw"),
          new Locale("ja"),
          new Locale("ko"),
          new Locale("lv"),
          new Locale("lt"),
          new Locale("ml"),
          new Locale("mn"),
          new Locale("mr"),
          new Locale("nl"),
          new Locale("no"),
          new Locale("pl"),
          new Locale("pt", "AO"),
          new Locale("pt", "BR"),
          new Locale("pt", "PT"),
          new Locale("ro"),
          new Locale("ru"),
          new Locale("sk"),
          new Locale("sl"),
          new Locale("sq"),          
          new Locale("sr"),
          new Locale("sv"),
          new Locale("th"),
          new Locale("tr"),
          new Locale("uk"),
          new Locale("zh"),
          new Locale("zh", "SIM"));
  
  /** "taxa" which are treated by eBird as "forms" instead of subspecies. */ 
  private static final ImmutableSet<String> ODD_EBIRD_FORMS = ImmutableSet.of(
      "Larus fuscus taimyrensis",
      "Hydrobates castro castro",
      "Hydrobates castro bangsi",
      "Buteo jamaicensis abieticola",
      "Oenanthe picata picata",
      "Oenanthe picata capistrata",
      "Oenanthe picata opistholeuca");
  private Taxonomy clements;
  
  private void run() throws IOException, SAXException {
    clements = ToolTaxonUtils.getTaxonomy(clementsFileName);
    
    File downloadDirFile = new File(downloadDir);
    if (!downloadDirFile.exists()) {
      if (!downloadDirFile.mkdirs()) {
        throw new IllegalStateException("Could not create " + downloadDir);
      }
    }
    for (Locale locale : LOCALES) {
      String fileName = fileNameFor(locale);
      File cachedDownload = new File(downloadDirFile, fileName);
      if (!cachedDownload.exists()) {
        download(locale, cachedDownload);
      }
    }
    
    final Map<String, String> commonNamesById = Maps.newLinkedHashMap();
    final Map<String, String> idsByScientificNames = Maps.newLinkedHashMap();
    TaxonUtils.visitTaxa(clements, new TaxonVisitor() {
      @Override
      public boolean visitTaxon(Taxon taxon) {
        switch (taxon.getType()) {
          case group:
          case species:
            commonNamesById.put(taxon.getId(), taxon.getCommonName());
            idsByScientificNames.put(TaxonUtils.getFullName(taxon), taxon.getId());
            break;
          default:
            break;
        }
        
        return true;
      }
    });
    
    ImmutableTable.Builder<Locale, String, String> namesByLocaleAndId = ImmutableTable.builder();
    for (Locale locale : LOCALES) {
      System.err.println(locale);
      String fileName = fileNameFor(locale);
      File cachedDownload = new File(downloadDirFile, fileName);
      ImportLines importLines = CsvImportLines.fromFile(cachedDownload, Charsets.UTF_8);
      try {
        // Skip the header
        importLines.nextLine();
        while (true) {
          String[] line = importLines.nextLine();
          if (line == null) {
            break;
          }
          
          String sci = line[0];
          String common = line[1];
          
          if (common.isEmpty()) {
            continue;
          }
          
          String id = idsByScientificNames.get(sci);
          if (id == null) {
            // eBird names have a lot of spuhs and morphs etc. that aren't
            // in the Scythebill taxonomy
            if (!sci.contains("/") && !sci.contains("(") && !sci.contains("[")
                && !ODD_EBIRD_FORMS.contains(sci)) {
              logger.warning("No id for " + sci + " in " + locale);
            }
          } else {
            String baseCommon = commonNamesById.get(id);
            if (!baseCommon.equals(common)) {
              namesByLocaleAndId.put(locale, id, common);
            }
          }
        }
      } finally {
        importLines.close();
      }
    }
    
    ImmutableTable<Locale, String, String> built = namesByLocaleAndId.build();
    for (Locale locale : built.rowKeySet()) {
      Locale normalized = normalizeLocaleForOutput(locale);
      File outputFile = new File(outputDir,
          String.format("clements-names-%s.csv", normalized.toString()));
      outputFile.createNewFile();
      ExportLines exportLines = CsvExportLines.fromWriter(
          new BufferedWriter(new OutputStreamWriter(
              new FileOutputStream(outputFile), Charsets.UTF_8)));
      try {
        System.out.println("Locale: " + normalized + ", total " + built.row(locale).size());
        System.err.println(normalized.getDisplayName(normalized));
        ImmutableMap<String, String> localeRow = built.row(locale);
        TaxonUtils.visitTaxa(clements, new TaxonVisitor() {
          @Override
          public boolean visitTaxon(Taxon taxon) {
            switch (taxon.getType()) {
              case group:
              case species:
                String name = localeRow.get(taxon.getId());
                if (name != null) {
                  try {
                    exportLines.nextLine(new String[] {taxon.getId(), name});
                  } catch (IOException e) {
                    throw new IllegalStateException(e);
                  }
                }
                break;
              default:
                break;
            }
            
            return true;
          }
        });
        
//       for (Map.Entry<String, String> idAndName : built.row(locale).entrySet()) {
//          exportLines.nextLine(new String[]{idAndName.getKey(), idAndName.getValue()});
//        }
      } finally {
        exportLines.close();
      }        
    }
  }


  private void download(Locale locale, File cachedDownload) throws IOException {
    URL url = new URL(
        "https://api.ebird.org/v2/ref/taxonomy/ebird?cat=species,issf,domestic,form&fmt=csv&locale=" + locale.toString());
    System.out.println("Loading " + url);
    OutputStream output = new BufferedOutputStream(
        new FileOutputStream(cachedDownload));
    try {
      Resources.copy(url, output);
    } finally {
      output.close();
    }
  }

  /**
   * eBird APIs use some non-standardnames for Chinese locales.  For files name to be saved, flip around.
   */
  private Locale normalizeLocaleForOutput(Locale locale) {
    if (locale.equals(new Locale("zh"))) {
      return new Locale("zh", "TW");
    } else if (locale.equals(new Locale("zh", "SIM"))) {
      return new Locale("zh");
    } else if (locale.equals(new Locale("is", "IS"))) {
      // Icelandic does not need the country specifier!  
      return new Locale("is");
    } else if (locale.equals(new Locale("en", "UK"))) {
      // GB is the standard, not UK
      return new Locale("en", "GB");
    }

    return locale;
  }
  
  private String fileNameFor(Locale locale) {
    
    if (!locale.getCountry().isEmpty()) {
      return String.format("%s_%s.csv",
          locale.getLanguage(), locale.getCountry());
    }
    
    return String.format("%s.csv", locale.getLanguage());
  }
}
