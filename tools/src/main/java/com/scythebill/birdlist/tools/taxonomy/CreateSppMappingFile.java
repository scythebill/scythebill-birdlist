/*   
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.io.CharSink;
import com.google.common.io.Files;

/**
 * Produces a sspmap file from a diff.
 * <p>
 * Usage: When doing a Clements update, *before* submitting the IOC
 * taxon - but after having done the AddSspMappingsToMapped step -
 * do "git diff" and save the results.  Use that here!
 */
public class CreateSppMappingFile {
  @Parameter(names = "-diffFile", required = true)
  private String diffFile;

  @Parameter(names = "-outFile", required = true)
  private String outFile;

  
  public CreateSppMappingFile(String[] args) {
    new JCommander(this, args);
  }

  public static void main(String[] args) throws Exception {
    new CreateSppMappingFile(args).run(); 
 }

  private static final Pattern LINE_0 = Pattern.compile("-.*<map>(.*)</map>");
  private static final Pattern LINE_1 = Pattern.compile("-.*<sspmap>(.*)</sspmap>");
  private static final Pattern LINE_2 = Pattern.compile("\\+.*<map>(.*)</map>");
  
  private void run() throws IOException {
    CharSink sink = Files.asCharSink(new File(outFile), StandardCharsets.UTF_8);
    try (Writer out = sink.openBufferedStream()) {
      List<String> lines = Files.readLines(new File(diffFile), StandardCharsets.UTF_8);
      for (int i = 0; i < lines.size() - 2; i++) {
        String line0 = lines.get(i);
        Matcher matcher0 = LINE_0.matcher(line0);
        if (!matcher0.matches()) {
          continue;
        }
              
        String line1 = lines.get(i + 1);
        Matcher matcher1 = LINE_1.matcher(line1);
        if (!matcher1.matches()) {
          continue;
        }
        
        String line2 = lines.get(i + 2);
        Matcher matcher2 = LINE_2.matcher(line2);
        if (!matcher2.matches()) {
          continue;
        }
        
        out.append(String.format("\"%s\",\"%s\",\"%s\"\n",
            matcher0.group(1), matcher1.group(1), matcher2.group(1)));
      }
    }
    
  }
  
}
