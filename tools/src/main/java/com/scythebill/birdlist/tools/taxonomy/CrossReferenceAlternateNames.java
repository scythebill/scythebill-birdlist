/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.util.List;

import org.apache.commons.text.similarity.LevenshteinDistance;
import org.xml.sax.SAXException;

import com.google.common.collect.Lists;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.MoreExecutors;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.SpeciesImpl;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.TaxonomyIndexer;
import com.scythebill.birdlist.model.xml.XmlTaxonExport;
import com.scythebill.birdlist.model.xml.XmlTaxonImport;

/**
 * Clean the old, broken alternate names (one string) to lovely new alternate names (a list
 * of strings).
 */
public class CrossReferenceAlternateNames {
  public static void main(String[] args) throws Exception {
    MappedTaxonomy taxonomy = getMappedTaxonomy(args[0], args[1]);

    TaxonUtils.visitTaxa(taxonomy.getRoot(),
        new CrossReference(taxonomy, taxonomy.getBaseTaxonomy()));
    
    OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(args[2]), "UTF-8");
    (new XmlTaxonExport()).export(out, taxonomy, "UTF-8");
    out.close();

    out = new OutputStreamWriter(new FileOutputStream(args[3]), "UTF-8");
    (new XmlTaxonExport()).export(out, taxonomy.getBaseTaxonomy(), "UTF-8");
    out.close();
}

  public static MappedTaxonomy getMappedTaxonomy(
          String baseTaxonomy, String filename) throws IOException,
          SAXException {
    Taxonomy base = getTaxonomy(baseTaxonomy);
    Reader reader = null;
    try {
      File file = new File(filename);
      reader = new BufferedReader(new FileReader(file));
      MappedTaxonomy taxonomy = (new XmlTaxonImport()).importMappedTaxa(reader, base);
      
      Futures.getUnchecked(new TaxonomyIndexer(Futures.immediateFuture(taxonomy)).load(
          MoreExecutors.newDirectExecutorService()));
      return taxonomy;
    } finally {
      reader.close();
    }
  }

  public static Taxonomy getTaxonomy(String filename) throws IOException, SAXException {
    Reader reader = null;
    try {
      File file = new File(filename);
      reader = new BufferedReader(new FileReader(file));
      Taxonomy taxonomy = (new XmlTaxonImport()).importTaxa(reader);
      Futures.getUnchecked(new TaxonomyIndexer(Futures.immediateFuture(taxonomy))
              .load(MoreExecutors.newDirectExecutorService()));
      return taxonomy;
    } finally {
      reader.close();
    }
  }

  static class CrossReference implements TaxonVisitor {
    private MappedTaxonomy mapped;
    private Taxonomy base;
    private final LevenshteinDistance distance = LevenshteinDistance.getDefaultInstance();
    
    CrossReference(MappedTaxonomy mapped, Taxonomy base) {
      this.mapped = mapped;
      this.base = base;
    }

    @Override
    public boolean visitTaxon(Taxon taxon) {
      if (taxon instanceof Species) {
        Species species = (Species) taxon;        
        SightingTaxon baseSighting = mapped.getMapping(taxon);
        // Ignore anything that cannot be mapped, or is mapped multiply
        // (the latter, for now).
        if (baseSighting == null
           || baseSighting.getType() != SightingTaxon.Type.SINGLE) {
          return false;
        }
        Taxon baseTaxon = base.getTaxon(baseSighting.getId());
        // For now, omit IOC species that map to groups or subspecies.
        // Would be nice to add to Clements something like 
        // "Eastern Cattle Egret, in part".
//        if (baseTaxon.getType() != Taxon.Type.species
//            && baseTaxon.getType() != Taxon.Type.group) {
//          return false;
//        }
        
        // Ignore domestics
        if (baseTaxon.getStatus() == Status.DO
            || baseTaxon.getStatus() == Status.IN) {
          return false;
        }
        
        Species baseSpecies = (Species) baseTaxon;
        
        // There's no point adding an alternate when it exactly matches the base mapping.
        // (Or if it matches the name of the *species* parent of the base mapping,
        // when we're starting from a group.
        List<String> baseCommonNames = Lists.newArrayList();
        if (baseSpecies.getCommonName() != null) {
          baseCommonNames.add(baseSpecies.getCommonName());
        }
        if (baseSpecies.getType() == Taxon.Type.group) {
          baseCommonNames.add(baseSpecies.getParent().getCommonName());
        }
        
        // If the common names are distant, add alternates (if new) 
        if (nameIsNotInOrCloseTo(species.getCommonName(), baseCommonNames)) {
          List<String> iocCommonNames = Lists.newArrayList(species.getAlternateCommonNames());
          if (nameIsNotInOrCloseTo(baseSpecies.getCommonName(), iocCommonNames)) {
            // Don't add Clements group names to IOC names
            if (baseTaxon.getType() != Taxon.Type.group && baseSpecies.getCommonName() != null) {
              iocCommonNames.add(baseSpecies.getCommonName());
            }
            ((SpeciesImpl) species).setAlternateCommonNames(iocCommonNames);
          }
          
          List<String> baseAlternateCommonNames = Lists.newArrayList(baseSpecies.getAlternateCommonNames());
          if (nameIsNotInOrCloseTo(species.getCommonName(), baseAlternateCommonNames)) {
            if (species.getCommonName() != null) {
              baseAlternateCommonNames.add(species.getCommonName());
            }
            ((SpeciesImpl) baseSpecies).setAlternateCommonNames(baseAlternateCommonNames);
          }
        }
        

        // Don't bother with any scientific name alternates against Clements groups
        if (baseTaxon.getType() != Taxon.Type.group) {
          // Add an alternate scientific name if the species names are distant.
          // For now, don't bother when the generic names are distant (this'd produce *a lot*
          // of content).
          if (distance.apply(species.getName(), baseSpecies.getName()) > 2) {
            List<String> iocNames = Lists.newArrayList(species.getAlternateNames());
            if (nameIsNotInOrCloseTo(TaxonUtils.getFullName(baseSpecies), iocNames)) {
              iocNames.add(TaxonUtils.getFullName(baseSpecies));
              ((SpeciesImpl) species).setAlternateNames(iocNames);
            }
            
            List<String> baseAlternateNames = Lists.newArrayList(baseSpecies.getAlternateNames());
            if (nameIsNotInOrCloseTo(TaxonUtils.getFullName(species), baseAlternateNames)) {
              baseAlternateNames.add(TaxonUtils.getFullName(species));
              ((SpeciesImpl) baseSpecies).setAlternateNames(baseAlternateNames);
            }
          }
          
          String iocFullName = TaxonUtils.getFullName(species);
          List<String> iocNames = Lists.newArrayList(species.getAlternateNames());
          for (String alternateName : baseSpecies.getAlternateNames()) {
            if (distance.apply(iocFullName, alternateName) > 2 &&
                !iocNames.stream().anyMatch(s -> (distance.apply(s, alternateName) <= 2))) {
              iocNames.add(alternateName);
            }
          }
          ((SpeciesImpl) species).setAlternateNames(iocNames);
        }
      }
      return true;
    }

    private boolean nameIsNotInOrCloseTo(String otherName, List<String> names) {
      if (otherName == null) {
        return true;
      }
      
      // Ignore "undescribed" names
      if (otherName.contains("undescribed")
          || otherName.contains("sp. nov.")) {
        return false;
      }
      
      for (String name : names) {
        if (distance.apply(removeInPart(name), removeInPart(otherName)) <= 2) {
          return false;
        }
      }
      
      return true;
    }

    private String removeInPart(String name) {
      if (name.endsWith(" - in part")) {
        return name.substring(0, name.length() - " - in part".length());
      }
      
      return name;
    }
  }
}
