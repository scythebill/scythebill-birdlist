/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy;

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableMultimap;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.SpeciesImpl;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.TaxonUtils;

/** Adds Feral Rock Pigeon to taxonomies when loading. */
class FeralAndDomesticForms {
  static class FeralOrDomesticForm {
    private final String commonName;
    private final String groupName;
    private final String sspName;
    private final String range;
    private final Status status;

    FeralOrDomesticForm(
        String commonName,
        String sspName,
        String range,
        Status status) {
      this(commonName, commonName, sspName, range, status);
    }
    
    FeralOrDomesticForm(
        String commonName,
        String groupName,
        String sspName,
        String range,
        Status status) {
      this.commonName = commonName;
      this.groupName = groupName;
      this.sspName = sspName;
      this.range = range;
      this.status = status;
    }
    
    void addForm(SpeciesImpl currentTaxon, Type groupOrSsp) {
      SpeciesImpl feralOrDomesticForm = new SpeciesImpl(groupOrSsp, currentTaxon);
      if (groupOrSsp == Type.group) {
        feralOrDomesticForm.setName(groupName);
      } else {
        feralOrDomesticForm.setName(sspName);
      }
      feralOrDomesticForm.setCommonName(currentTaxon.getCommonName() + " " + commonName);
      if (range != null) {
        feralOrDomesticForm.setRange(range);
      }
      feralOrDomesticForm.setStatus(status);
      currentTaxon.getContents().add(feralOrDomesticForm);
      feralOrDomesticForm.built();
    }
  }
  
  private static ImmutableMultimap<String, FeralOrDomesticForm> getFeralAndDomesticForms(boolean clements) {
    ImmutableMultimap.Builder<String, FeralOrDomesticForm> builder = ImmutableMultimap.builder();
    builder.put("Columba livia",
        new FeralOrDomesticForm(
            "(Feral Pigeon)",
            "f. domestica",
            "Worldwide introduction",
            Status.IN));
    builder.put("Anser cygnoides",
        new FeralOrDomesticForm(
            "(Domestic type)",
            "f. domestica",
            null,
            Status.DO));
    builder.put("Anser anser",
        new FeralOrDomesticForm(
            "(Domestic type)",
            "f. domestica",
            "Widely domesticated",
            Status.DO));
    builder.put("Cairina moschata",
        new FeralOrDomesticForm(
            "(Domestic type)",
            "f. domestica",
            "Widely domesticated",
            Status.DO));
    builder.put("Anas platyrhynchos",
        new FeralOrDomesticForm(
            "(Domestic type)",
            "f. domestica",
            null,
            Status.DO));
    builder.put("Numida meleagris",
        new FeralOrDomesticForm(
            "(Domestic type)",
            "f. domestica",
            "Widely domesticated",
            Status.DO));
    builder.put("Pavo cristatus",
        new FeralOrDomesticForm(
            "(Domestic type)",
            "f. domestica",
            "Widely domesticated",
            Status.DO));
    builder.put("Gallus gallus",
        new FeralOrDomesticForm(
            "(Domestic type)",
            "f. domestica",
            "Widely domesticated",
            Status.DO));
    builder.put("Meleagris gallopavo",
        new FeralOrDomesticForm(
            "(Domestic type)",
            "f. domestica",
            "Widely domesticated",
            Status.DO));
    builder.put("Streptopelia roseogrisea",
        new FeralOrDomesticForm(
            "(Domestic type or Ringed Turtle-Dove)",
            "(Domestic type)",
            "risoria",
            "Widely domesticated",
            Status.DO));
    builder.put("Nymphicus hollandicus",  // Cockatiel
        new FeralOrDomesticForm(
            "(Domestic type)",
            "f. domestica",
            "Widely domesticated",
            Status.DO));
    builder.put("Melopsittacus undulatus",  // Budgerigar
        new FeralOrDomesticForm(
            "(Domestic type)",
            "f. domestica",
            "Widely domesticated",
            Status.DO));
    builder.put("Chrysolophus pictus",  // Golden Pheasant
        new FeralOrDomesticForm(
            "(Domestic type)",
            "f. domestica",
            "Widely domesticated",
            Status.DO));
    builder.put("Phasianus colchicus",  // Ring-necked Pheasant
        new FeralOrDomesticForm(
            "(Domestic type)",
            "f. domestica",
            "Widely domesticated",
            Status.DO));
    builder.put("Agapornis roseicollis",  // Rosy-faced Lovebird
        new FeralOrDomesticForm(
            "(Domestic type)",
            "f. domestica",
            "Widely domesticated",
            Status.DO));
    builder.put("Agapornis fischeri",  // Fischer's Lovebird
        new FeralOrDomesticForm(
            "(Domestic type)",
            "f. domestica",
            "Widely domesticated",
            Status.DO));
    builder.put("Agapornis personatus",  // Yellow-collared Lovebird
        new FeralOrDomesticForm(
            "(Domestic type)",
            "f. domestica",
            "Widely domesticated",
            Status.DO));
    builder.put("Lonchura striata",  // White-rumped Munia
        new FeralOrDomesticForm(
            "(Domestic type)",
            "f. domestica",
            "Widely domesticated",
            Status.DO));
    if (clements) {
      builder.put("Taeniopygia guttata", // Zebra finch
          new FeralOrDomesticForm(
              "(Domestic type)",
              "f. domestica",
              "Widely domesticated",
              Status.DO));
    } else {
      builder.put("Taeniopygia castanotis", // Australian Zebra finch
          new FeralOrDomesticForm(
              "(Domestic type)",
              "f. domestica",
              "Widely domesticated",
              Status.DO));
    }
    builder.put("Serinus canaria",  // Island Canary
        new FeralOrDomesticForm(
            "(Domestic type)",
            "f. domestica",
            "Widely domesticated",
            Status.DO));

    return builder.build();
  }
  
  static void maybeAddFeralOrDomesticForm(
      SpeciesImpl currentTaxon, Type groupOrSsp, boolean clements) {
    ImmutableMultimap<String, FeralOrDomesticForm> feralAndDomesticForms = getFeralAndDomesticForms(clements);
    
    if (currentTaxon.getType() == Type.species) {
      ImmutableCollection<FeralOrDomesticForm> forms = feralAndDomesticForms.get(TaxonUtils.getFullName(currentTaxon));
      for (FeralOrDomesticForm form : forms) {
        form.addForm(currentTaxon, groupOrSsp);
      }
    }
  }

  /** Adds "Wild type" pigeon where needed. */
  static SpeciesImpl maybeAddWildPigeon(
      SpeciesImpl currentTaxon, Type wildPigeonType) {
    if (currentTaxon.getType() == Type.species
        && "livia".equals(currentTaxon.getName())
        && "Columba".equals(currentTaxon.getParent().getName())) {
      SpeciesImpl wildPigeon = new SpeciesImpl(wildPigeonType, currentTaxon);
      wildPigeon.setName("(Wild type)");
      wildPigeon.setCommonName("Rock Pigeon (Wild type)");
      currentTaxon.getContents().add(wildPigeon);
      wildPigeon.built();
      return wildPigeon;
    }
    
    return null;
  }
}
