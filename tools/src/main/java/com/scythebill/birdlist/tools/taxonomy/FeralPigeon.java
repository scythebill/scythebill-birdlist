/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy;

import com.scythebill.birdlist.model.taxa.SpeciesImpl;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.Taxon.Type;

/** Adds Feral Rock Pigeon to taxonomies when loading. */
class FeralPigeon {
  static SpeciesImpl maybeAddFeralPigeon(
      SpeciesImpl currentTaxon, Type feralPigeonType) {
    if (currentTaxon.getType() == Type.species
        && "livia".equals(currentTaxon.getName())
        && "Columba".equals(currentTaxon.getParent().getName())) {
      SpeciesImpl feralPigeon = new SpeciesImpl(feralPigeonType, currentTaxon);
      if (feralPigeonType == Type.group) {
        feralPigeon.setName("(Feral Pigeon)");
      } else {
        feralPigeon.setName("f. domestica");
      }
      feralPigeon.setCommonName("Rock Pigeon (Feral Pigeon)");
      feralPigeon.setRange("Worldwide introduction");
      feralPigeon.setStatus(Status.IN);
      currentTaxon.getContents().add(feralPigeon);
      feralPigeon.built();
      return feralPigeon;
    }
    
    return null;
  }

  static SpeciesImpl maybeAddWildPigeon(
      SpeciesImpl currentTaxon, Type wildPigeonType) {
    if (currentTaxon.getType() == Type.species
        && "livia".equals(currentTaxon.getName())
        && "Columba".equals(currentTaxon.getParent().getName())) {
      SpeciesImpl wildPigeon = new SpeciesImpl(wildPigeonType, currentTaxon);
      wildPigeon.setName("(Wild type)");
      wildPigeon.setCommonName("Rock Pigeon (Wild type)");
      currentTaxon.getContents().add(wildPigeon);
      wildPigeon.built();
      return wildPigeon;
    }
    
    return null;
  }
}
