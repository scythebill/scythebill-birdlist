/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.apache.commons.io.output.NullWriter;
import org.xml.sax.SAXException;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Sets;
import com.google.common.collect.Streams;
import com.google.common.io.Files;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Type;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.TaxonomyMapping;
import com.scythebill.birdlist.model.taxa.TaxonomyMappingLoader;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;

/**
 * Tool for examining old mapping files and taxonomies and producing compact reports
 * of taxonomic differences, so online reports of "splits and lumps" can be produced.
 */
public class GenerateDifferencesReport {
  /**
   * Current taxa that can't be mapped from older taxa, because they were added
   * in the current taxonomy and lie at an ambiguous position.
   * These *should* be handled by mapping them to a "spuh" of the mapping from
   * previous and subsequent form (with a mapping);  or the parent form if there
   * isn't both a previous and subsequent form with a mapping.
   */
  private static final ImmutableSet<String> KNOWN_MISSING_MAPPINGS =
      ImmutableSet.of(
          "African Reed Warbler (A.b.ambiguus)",
          "Eurasian Reed Warbler (Siwa)",
          "Inaccessible Island Finch (Upland)",
          "Goldcrest (western Canary Islands)",
          "Emu (D.n.baudinianus)",
          "Philippine Cuckoo-Dove (M.t.septentrionalis)",
          "Philippine Cuckoo-Dove (M.t.phaea)",
          "Philippine Cuckoo-Dove (M.t.tenuirostris)",
          "Curve-billed Scythebill (Tupana)",
          "Curve-billed Scythebill (Tapajos)",
          "Black-naped Oriole (Philippine) (O.c.yamamurae)",
          "Gray-eyed Bulbul (Gray-eyed) (I.p.myitkyinensis)",
          // Not thrilled with this one, but Olive Bulbul taxonomy back then was way different
          "Olive Bulbul (Olive) (I.v.viridescens)",
          "Assam Laughingthrush (T.c.ailaoshanense)",
          "Square-tailed Drongo-Cuckoo (S.l.brachyurus)");
  
  private static final Joiner SLASH_JOINER = Joiner.on('/');
  
  /**
   * Mapping from an IOC version to the Clements version that was associated.
   * This is not guaranteed to be unique, since Clements updates during an IOC
   * version... but here it is always "the Clements version that was in use
   * when the checked-in IOC version was built". 
   */
  private static final ImmutableMap<String, String> IOC_TO_CLEMENTS_VERSION =
      ImmutableMap.<String, String>builder()
      .put("41", "68")
      .put("51", "69")
      .put("61", "2015")
      .put("71", "2016")
      .put("81", "2017")
      .put("82", "2018")
      .put("91", "2018")
      .put("92", "2019")
      .put("101", "2019")
      .put("102", "2019")
      .put("111", "2019")
      .put("112", "2021")
      .put("121", "2021")
      .put("122", "2022")
      .put("131", "2022")
      .put("132", "2023")
      .put("141", "2023")
      .put("142", "")
      .put("151", "")
      .build();
  @Parameter(names = "-currentTaxonomy", required = true)
  private String currentTaxonomyFile;

  @Parameter(names = "-currentIocTaxonomy")
  private String currentIocTaxonomyFile;

  @Parameter(names = "-oldTaxonomyFormat", required = true)
  private String oldTaxonomyFormat;

  @Parameter(names = "-oldIocTaxonomyFormat")
  private String oldIocTaxonomyFormat;

  @Parameter(names = "-updateFileFormat", required = true)
  private String updateFileFormat;

  @Parameter(names = "-iocSspUpdateFileFormat")
  private String iocSspUpdateFileFormat;

  @Parameter(names = "-onlyNoteComplexSplits")
  private boolean onlyNoteComplexSplits;

  @Parameter(names = "-outputFileFormat")
  private String outputFileFormat;

  private Taxonomy currentTaxonomy;

  private Taxonomy currentBaseTaxonomy;

  public GenerateDifferencesReport(String[] args) {
    new JCommander(this, args);
  }

  public static void main(String[] args) throws Exception {
    new GenerateDifferencesReport(args).run(); 
 }

  private void run() throws IOException, SAXException {
    if (currentIocTaxonomyFile == null) {
      runClements();
    } else {
      runIoc();
    }
  }

  private void runClements() throws IOException, SAXException {
    currentBaseTaxonomy = currentTaxonomy = ToolTaxonUtils.getTaxonomy(currentTaxonomyFile);
    runOneOldClements("65");
    runOneOldClements("66");
    runOneOldClements("67");
    runOneOldClements("68");
    runOneOldClements("69");
    runOneOldClements("2015");
    runOneOldClements("2016");
    runOneOldClements("2017");
    runOneOldClements("2018");
    runOneOldClements("2019");
    runOneOldClements("2021");
    runOneOldClements("2022");
    runOneOldClements("2023");
  }
      
  private void runIoc() throws IOException, SAXException {
    Preconditions.checkNotNull(currentIocTaxonomyFile);
    Preconditions.checkNotNull(oldIocTaxonomyFormat);
    currentBaseTaxonomy = ToolTaxonUtils.getTaxonomy(currentTaxonomyFile); 
    currentTaxonomy = ToolTaxonUtils.getMappedTaxonomy(currentBaseTaxonomy, currentIocTaxonomyFile);
    runOneOldIoc("51");
    runOneOldIoc("61");
    runOneOldIoc("71");
    runOneOldIoc("81");
    runOneOldIoc("82");
    runOneOldIoc("91");
    runOneOldIoc("92");
    runOneOldIoc("101");
    runOneOldIoc("102");
    runOneOldIoc("111");
    runOneOldIoc("112");
    runOneOldIoc("121");
    runOneOldIoc("122");
    runOneOldIoc("131");
    runOneOldIoc("132");
    runOneOldIoc("141");
    runOneOldIoc("142");
  }

  private void runOneOldClements(String version) throws IOException, SAXException {
    System.out.println("Starting Clements version " + version);
    Taxonomy oldTaxonomy = ToolTaxonUtils.getTaxonomy(String.format(oldTaxonomyFormat, version));
    File updateFile = new File(String.format(updateFileFormat, version));
    Preconditions.checkState(updateFile.exists(), "%s does not exist", updateFile.getAbsolutePath());
    
    TaxonomyMapping mapping = new TaxonomyMappingLoader("ignored-id",
        Files.asCharSource(updateFile, StandardCharsets.UTF_8), null).loadMapping();
    runOneOld(oldTaxonomy, taxon -> mapping.mapSingleTaxon(taxon.getId()), version);
  }
   
  private void runOneOldIoc(String version) throws IOException, SAXException {
    String clementsVersion = Preconditions.checkNotNull(IOC_TO_CLEMENTS_VERSION.get(version));
    System.out.printf("Starting IOC version %s (Clements %s)\n", version, clementsVersion);
    TaxonomyMapping mapping;
    Taxonomy oldBaseTaxonomy;
    if (clementsVersion.isEmpty()) {
      mapping = null;
      oldBaseTaxonomy = currentBaseTaxonomy;
    } else {
      oldBaseTaxonomy = ToolTaxonUtils.getTaxonomy(String.format(oldTaxonomyFormat, clementsVersion));
      File updateFile = new File(String.format(updateFileFormat, clementsVersion));
      Preconditions.checkState(updateFile.exists());
      
      File iocUpdateSspFile = new File(String.format(iocSspUpdateFileFormat, clementsVersion));

      Preconditions.checkState(iocUpdateSspFile.exists(), "Can't find update file %s", iocUpdateSspFile);
      
      mapping = new TaxonomyMappingLoader("ignored-id",
          Files.asCharSource(updateFile, StandardCharsets.UTF_8),
          Files.asCharSource(iocUpdateSspFile, StandardCharsets.UTF_8)).loadMapping();
    }
    
    MappedTaxonomy oldTaxonomy = ToolTaxonUtils.getMappedTaxonomy(oldBaseTaxonomy, String.format(oldIocTaxonomyFormat, version));
    OldTaxonMapper mapper = new OldTaxonMapper() {
      @Override
      public SightingTaxon mapSingleTaxon(Taxon taxon) {
        if (taxon.isDisabled()) {
          return null;
        }
        
        // First, map to Clements in the old base taxonomy
        SightingTaxon oldBaseSightingTaxon = oldTaxonomy.getMapping(taxon);
        if (oldBaseSightingTaxon == null) {
          System.err.println("Could not map old IOC taxon " + TaxonUtils.getCommonName(taxon));
          return null;
        }
        
        // No mapping = the "old" base taxonomy is actually the current base taxonomy 
        if (mapping == null) {
          return oldBaseSightingTaxon;
        }
        
        SightingTaxon currentBaseSightingTaxon;
        if (oldBaseSightingTaxon.getType() == Type.SINGLE) {
          currentBaseSightingTaxon = mapping.mapSingleTaxon(oldBaseSightingTaxon.getId());
        } else if (oldBaseSightingTaxon.getType() == Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
          currentBaseSightingTaxon = mapping.mapSingleWithSecondarySubspecies(oldBaseSightingTaxon);
          if (currentBaseSightingTaxon == null) {
            currentBaseSightingTaxon = mapping.mapSingleTaxon(oldBaseSightingTaxon.getId());
          }
        } else {
          Set<String> mappedTaxa = Sets.newLinkedHashSet();
          for (String id : oldBaseSightingTaxon.getIds()) {
            mappedTaxa.addAll(mapping.mapSingleTaxon(id).getIds());
          }
          currentBaseSightingTaxon = SightingTaxons.newPossiblySpTaxon(simplifyTaxa(mappedTaxa));
        }
        
        return currentBaseSightingTaxon;
      }

      /**
       * Given a set of taxa, attempts to return a simpler definition.  This is particularly
       * useful for cases where earlier IOC mappings required long lists of subspecies, but
       * changes to eBird/Clements taxonomy now supports simple group or even species mappings.
       * <p>
       * The method exclusively attempts to simplify in terms of the eBird/Clements taxonomy
       * (fewest taxa). 
       */
      private Set<String> simplifyTaxa(Set<String> taxa) {
        // Nothing simpler than 1 species!
        if (taxa.size() == 1) {
          return taxa;
        }
        
        for (String id : taxa) {
          Taxon taxon = currentBaseTaxonomy.getTaxon(id);
          // Can't simplify species.
          if (taxon.getType() == Taxon.Type.species) {
            continue;
          }
          
          // Gather all the children of this taxon's parents
          Taxon parent = taxon.getParent();
          Set<String> childIds = Sets.newLinkedHashSet();
          for (Taxon child : parent.getContents()) {
            childIds.add(child.getId());
          }
          
          // If every child of the parent is present, then clearly removing the children
          // and replacing with just the parent would be simpler
          if (taxa.containsAll(childIds)) {
            Set<String> simplifiedIds = Sets.newLinkedHashSet(taxa);
            simplifiedIds.removeAll(childIds);
            simplifiedIds.add(parent.getId());
            // Return early, but only after giving the code another chance to simplify taxa.  It
            // can take a few recursions to simplify extremely over-complex taxa
            return simplifyTaxa(simplifiedIds);
          }
        }
        
        // No simplification found.
        return taxa;
      }
    };
    runOneOld(oldTaxonomy, mapper, version);
  }

  interface OldTaxonMapper {
    // From an "old" taxon, produce the SightingTaxon for the current base taxonomy
    SightingTaxon mapSingleTaxon(Taxon taxon);    
  }
  
  private void runOneOld(Taxonomy oldTaxonomy, OldTaxonMapper mapper, String version) throws IOException {
    Multimap<String, String> currentTaxaToOldSources = LinkedHashMultimap.create();
    // Map of an old taxonomy ID to a spuh SightingTaxon.  SightingTaxon is *not*
    // resolved back to "base" - this is a SightingTaxon in the current taxonomy, possibly IOC.
    Map<String, SightingTaxon> oldTaxaToCurrentSpuhs = new LinkedHashMap<>(); 
    
    // Multimap of new species IDs where old->current is a single species
    Multimap<String, String> currentToOldSingleMappings = LinkedHashMultimap.create(); 
    
    // Look for taxa that map to > 2 species
    TaxonUtils.visitTaxa(oldTaxonomy, new TaxonVisitor() {
      @Override
      public boolean visitTaxon(Taxon taxon) {
        if (taxon.getType() == Taxon.Type.species) {
          if (isIgnoredTaxon(taxon)) {
            return false;
          }
          
          SightingTaxon mapped = mapper.mapSingleTaxon(taxon);
          if (mapped == null) {
            // Some long-extinct species have been removed the taxonomy over time;  this is not a concern.
            if (taxon.getStatus() != Status.EX && !taxon.isDisabled()) {
              System.err.println("No mapping for " + TaxonUtils.getCommonName(taxon) + " from " + oldTaxonomy.getName());
            }
            return false;
          }
          Resolved currentSpecies = Preconditions
              .checkNotNull(mapped.resolve(currentTaxonomy), "Couldn't resolve %s from %s", mapped, taxon.getCommonName())
              .resolveParentOfType(Taxon.Type.species);
          if (currentSpecies.getType() == SightingTaxon.Type.SP) {
            SightingTaxon currentTaxa = currentSpecies.getSightingTaxon();
            
            // See if any of the new taxa to which this species maps overlaps with some split produced by a
            // different taxon.  If so, need to merge!
            // Example:
            //   - Northern Shrike->Northern/Great Gray Shrike
            //   - Southern Gray Shrike->Iberian/Great Gray Shrike
            Optional<String> overlappingCurrentTaxon =
                currentTaxa.getIds().stream().filter(currentTaxaToOldSources::containsKey).findFirst();
            if (overlappingCurrentTaxon.isPresent()) {
              // TODO: it isn't *quite* right to stop here, as there could be still more overlaps with *different* taxa.
              // Don't think that's happened yet.
              
              // In the above example:  this would produce a collection containing just Northern Shrike 
              Collection<String> oldOverlappingSources = currentTaxaToOldSources.get(overlappingCurrentTaxon.get());
              for (String oldOverlappingSource : oldOverlappingSources) {
                // This would produce "Northern/Great Gray Shrike"
                SightingTaxon currentSpuhFromOverlap = oldTaxaToCurrentSpuhs.get(oldOverlappingSource);
                Set<String> mergedTaxa = new LinkedHashSet<>(currentTaxa.getIds());
                mergedTaxa.addAll(currentSpuhFromOverlap.getIds());
                currentTaxa = SightingTaxons.newSpTaxon(mergedTaxa);
              }
              
              // currentTaxa is now the full list (in the example, Northern/Iberian/Great Gray Shrike).
              // Update oldTaxaToCurrentSpuhs.  (In the example above, update Northern Shrike to point to the three-way spuh
              for (String oldOverlappingSource : oldOverlappingSources) {
                oldTaxaToCurrentSpuhs.put(oldOverlappingSource, currentTaxa);
              }
              
              // currentTaxaToOldSources will get updated below
            }
            
            oldTaxaToCurrentSpuhs.put(taxon.getId(), currentTaxa);
            for (String newId : currentTaxa.getIds()) {
              currentTaxaToOldSources.put(newId, taxon.getId());
            }
          } else {
            currentToOldSingleMappings.put(currentSpecies.getTaxon().getId(), taxon.getId());
          }
          return false;
        }
        return true;
      }
    });
    
    // Look for overlap among the splits, specifically:
    // if (A) maps to (C, D), that looks like a split
    // but if (B) maps to part of (C), then it's really (A, B) -> (C, D),
    // and yeah, there *could* be a split for some users since things 
    // changed, but it's not as clear.
    // Therefore, spend another pass, and look for old species *or* subspecies
    // whose mapping hits a subset of what *looked* like a split    
    TaxonUtils.visitTaxa(oldTaxonomy, new TaxonVisitor() {
      @Override
      public boolean visitTaxon(Taxon taxon) {
        // If it already showed up as a "split", don't care.
        if (oldTaxaToCurrentSpuhs.containsKey(taxon.getId())) {
          return false;
        }
        
        if (taxon.getType().isAtOrLowerLevelThan(Taxon.Type.species)) {
          SightingTaxon mapped = mapper.mapSingleTaxon(taxon);
          if (mapped == null) {
            // Some long-extinct species have been removed the taxonomy over time;  this is not a concern.
            if (taxon.getStatus() != Status.EX) {
              System.err.println("No mapping for " + TaxonUtils.getCommonName(taxon) + " from " + oldTaxonomy.getName());
            }
            return false;
          }
          Resolved resolvedTaxon = mapped.resolve(currentTaxonomy);
          if (resolvedTaxon == null) {
            throw new IllegalStateException("Could not map " + TaxonUtils.getCommonName(taxon) + " from " + mapped);
          }
          Resolved currentSpecies = resolvedTaxon.resolveParentOfType(Taxon.Type.species);
          SightingTaxon currentTaxa = currentSpecies.getSightingTaxon();
          
          // Failure scenario.  This happened for:
          //   Clamorous Reed-Warbler (Brown) had a ssp "sumbae" moved to Australian Reed-Warbler
          //   ... but Clamorous Reed-Warbler wasn't treated as a "split" on upgrade (since sumbae
          //       is a fringe case.)
          //   Note an error, but move on.
          if (currentTaxa.getType() == SightingTaxon.Type.SP) {
            System.err.printf("Mapping failure: taxon %s is a split (%s) but parent %s was not.\n",
                TaxonUtils.getCommonName(taxon), currentSpecies.getCommonName(), TaxonUtils.getCommonName(taxon.getParent()));
            return false;
          }

          String currentId = currentTaxa.getId();
          if (currentTaxaToOldSources.containsKey(currentId)) {
            // Merge the old taxa
            Set<String> overlappingOldTaxa = new LinkedHashSet<>(currentTaxaToOldSources.get(currentId));
            SightingTaxon overlappingNewTaxa = oldTaxaToCurrentSpuhs.get(overlappingOldTaxa.iterator().next());
            
            // Overlap has been detected looking at a mapping of a group or subspecies, but the
            // data we're gathering on old taxa is purely at a species level.  Jump up to the species level
            // before writing anything to oldTaxaToCurrentSpuhs or currentTaxaToOldSources
            Taxon oldSpecies = TaxonUtils.getParentOfType(taxon, Taxon.Type.species);
            oldTaxaToCurrentSpuhs.put(oldSpecies.getId(), overlappingNewTaxa);
            overlappingOldTaxa.add(oldSpecies.getId());            
            currentTaxaToOldSources.putAll(currentId, overlappingOldTaxa);
            
            // No need to examine the children if overlap happened in the parents.
            return false;
          }
        }
        
        return true;
      }
    });
    
    // Convert the data gathered so far into a mapping of SET(old taxa) -> SET(current taxa) 
    Comparator<SightingTaxon> sightingTaxa = Comparator.comparingInt(
        // Just take the index of the *first* taxon, which is good enough to sort the overall list.
        taxon -> taxon.resolveInternal(currentTaxonomy).getTaxa().iterator().next().getTaxonomyIndex());
    Multimap<SightingTaxon, String> currentSpuhsToOldTaxa =
        Multimaps.invertFrom(Multimaps.forMap(oldTaxaToCurrentSpuhs),
            MultimapBuilder.treeKeys(sightingTaxa).arrayListValues().build());
    
    // Merge in any lumps (single new taxa mapped in from multiple old taxa)
    for (String currentTaxonId : currentToOldSingleMappings.keySet()) {
      Collection<String> oldSources = currentToOldSingleMappings.get(currentTaxonId);
      if (oldSources.size() > 1) {
        currentSpuhsToOldTaxa.putAll(SightingTaxons.newSightingTaxon(currentTaxonId), oldSources);
      }
    }

    ExportLines outputFile; 
    if (outputFileFormat != null) {
      outputFile = CsvExportLines.toFile(new File(String.format(outputFileFormat, version)), StandardCharsets.UTF_8);
    } else {
      outputFile = CsvExportLines.fromWriter(NullWriter.NULL_WRITER);
    }

    outputFile.nextLine(new String[] {
       "Index",
       "Type",
       "Old Common",
       "Old Sci",
       "New IDs"
    });
    try {
      AtomicInteger bundleCount = new AtomicInteger();
      
      // At this point, there's a single mapping of SET(old taxa) -> SET(current taxa),
      // and the job becomes figuring out how to identify splits and lumps
      // given current sightings.  That's trivial if SET(old taxa) is size 1, but for
      // anything else, the challenge is going through the species, groups, and subspecies
      // in the current sightings and figuring out which old taxa they would have mapped to.
      currentSpuhsToOldTaxa.asMap().forEach((currentSightingTaxon, oldSpeciesIds) -> {
        int currentIndex = bundleCount.incrementAndGet();
        Resolved oldResolved = SightingTaxons.newPossiblySpTaxon(oldSpeciesIds).resolveInternal(oldTaxonomy);
        Resolved newResolved = currentSightingTaxon.resolveInternal(currentTaxonomy);
        
        if (!onlyNoteComplexSplits
            || (oldResolved.getType() == SightingTaxon.Type.SP && newResolved.getType() == SightingTaxon.Type.SP)) {
          System.out.printf("%s->%s\n",
              SightingTaxons.newPossiblySpTaxon(oldSpeciesIds).resolveInternal(oldTaxonomy).getCommonName(),
              currentSightingTaxon.resolveInternal(currentTaxonomy).getCommonName());
        }
        
        // Output format:  compress all the various "current IDs" that happen to map to one or more old species
        // INDEX,currentId1/id2/id3/etc...,oldCommonName1/oldCommonName2,oldSciName1/oldSciName2
        // where:
        //  - INDEX is used to bundle the various types of mappings in (set[OLD])->(set[NEW])
        //  - None of the "slashes" are "compressed" (e.g., not Red/Cassia Crossbill, but instead Red Crossbill/Cassia Crossbill)
        // 
        // Processing then is a run through every sighting containing those IDs, grouping by INDEX, to give:
        //  - a set of new species
        //  - a set of definite old species (from rows where "old" doesn't have any slashes)
        //  - a set of "maybe" old species (from rows where "old" has slashes)
        //  Remove all the "definite" from the "maybe", and then for each bundle you have M..N old species, and N' new species,
        // and can report it as possible lumps and splits.
          
        if (oldResolved.getTaxa().size() == 1) {
          // Simple split.
          String[] simpleSplitDefinition = new String[5];
          simpleSplitDefinition[0] = Integer.toString(currentIndex);
          simpleSplitDefinition[1] = "simple";
          simpleSplitDefinition[2] = oldResolved.getCommonName();
          simpleSplitDefinition[3] = oldResolved.getFullName();
          simpleSplitDefinition[4] = SLASH_JOINER.join(newResolved.getSightingTaxon().getIds()); 
          try {
            outputFile.nextLine(simpleSplitDefinition);
          } catch (IOException e) {
            throw new RuntimeException(e);
          }
        } else {
          // For each old species, and all of its groups and subspecies, see what current taxa
          // are mapped to.
          Multimap<String, String> currentTaxaToOldSpecies = LinkedHashMultimap.create();
          for (String oldSpeciesId : oldSpeciesIds) {
            Taxon oldSpecies = oldTaxonomy.getTaxon(oldSpeciesId);
            TaxonUtils.visitTaxa(oldSpecies, new TaxonVisitor() {
              @Override
              public boolean visitTaxon(Taxon oldTaxon) {
                if (oldTaxon.isDisabled()) {
                  return false;
                }
                
                // mapSingleTaxon produces a base taxonomy mapping.
                SightingTaxon currentMapping = mapper.mapSingleTaxon(oldTaxon);
                // ... so map to the current taxonomy and then back to a SightingTaxon.
                Resolved resolved = currentMapping.resolve(currentTaxonomy);

                while (true) {
                  currentMapping = resolved.getSightingTaxon();
                  for (String currentId : currentMapping.getIds()) {
                    // Something *within* oldSpecies would have mapped to this current taxon.
                    // Ergo, presence of this current taxon implies that in the old taxonomy,
                    // you would have recorded the old species.  Store that.
                    currentTaxaToOldSpecies.put(currentId, oldSpeciesId); 
                  }
                  
                  // Now, make sure you *also* enter that mapping from old->current for each *parent*.
                  // For example, if Ceyx rufidorsa rufidorsa maps to Ceyx rufidorsa in a new taxonomy,
                  // then clearly Ceyx rufidorsa itself does too (at least in part - it might map
                  // to more than that)
                  if (resolved.getSmallestTaxonType().isLowerLevelThan(Taxon.Type.species)) {
                    resolved = resolved.getParent().resolveInternal(currentTaxonomy);
                  } else {
                    break;
                  }
                }
                return true;
              }
            });
          }
  
          Multimap<String, String> oldSpeciesSetsToCurrentTaxa = LinkedHashMultimap.create();
          
          // Look through all current taxa to find out which would fall under which old species.
          currentSightingTaxon.getIds().forEach(id -> {
            TaxonUtils.visitTaxa(currentTaxonomy.getTaxon(id), new TaxonVisitor() {
              @Override
              public boolean visitTaxon(Taxon taxon) {
                if (taxon.isDisabled()) {
                  return false;
                }
                Collection<String> oldSpeciesIds = currentTaxaToOldSpecies.get(taxon.getId());
                
                // Empty, and there's children: grab all the mappings of all the children
                if (oldSpeciesIds.isEmpty() && !taxon.getContents().isEmpty()) {
                  Set<String> oldSpeciesIdsFromChildren = new LinkedHashSet<>();
                  TaxonUtils.visitTaxa(taxon,
                      child -> {
                        oldSpeciesIdsFromChildren.addAll(currentTaxaToOldSpecies.get(child.getId()));
                        return true;
                      });
                  
                  oldSpeciesIds = oldSpeciesIdsFromChildren;
                }
                
                // Still empty: it's not a species.  Start by looking for the previous child
                // that has a defined mapping, and the next child that has a defined mapping.
                if (oldSpeciesIds.isEmpty() && taxon.getType() != Taxon.Type.species) {
                  Taxon parent = taxon.getParent();
                  boolean foundCurrentTaxonYet = false;
                  Taxon previousMappedTaxon = null;
                  Taxon nextMappedTaxon = null;
                  for (Taxon sibling : parent.getContents()) {
                    if (sibling == taxon) {
                      foundCurrentTaxonYet = true;
                    } else if (!currentTaxaToOldSpecies.get(sibling.getId()).isEmpty()) {
                      if (!foundCurrentTaxonYet) {
                        previousMappedTaxon = sibling;
                      } else {
                        nextMappedTaxon = sibling;
                        break;
                      }
                    }
                  }
                  
                  // Merge the mappings from those.
                  Set<String> oldSpeciesIdsFromSiblings = new LinkedHashSet<>();
                  if (previousMappedTaxon != null) {
                    oldSpeciesIdsFromSiblings.addAll(currentTaxaToOldSpecies.get(previousMappedTaxon.getId()));
                  }
                  if (nextMappedTaxon != null) {
                    oldSpeciesIdsFromSiblings.addAll(currentTaxaToOldSpecies.get(nextMappedTaxon.getId()));
                  }
                  oldSpeciesIds = oldSpeciesIdsFromSiblings;
                  
                  // That failed too?  Then just use the mappings for the parent.
                  if (oldSpeciesIds.isEmpty()) {
                    oldSpeciesIds = currentTaxaToOldSpecies.get(parent.getId());
                  }
                }
                
                if (oldSpeciesIds.isEmpty()) {
                  System.err.printf("No mappings for %s\n", TaxonUtils.getCommonName(taxon));
  //                if (!KNOWN_MISSING_MAPPINGS.contains(TaxonUtils.getCommonName(taxon))) {
  //                }
                } else {
  //                if (oldSpeciesIds.size() > 1 && !onlyNoteComplexSplits) {
  //                System.out.printf("(Multiple mappings for %s)\n", TaxonUtils.getCommonName(taxon));
  //                }
                  oldSpeciesSetsToCurrentTaxa.put(SLASH_JOINER.join(oldSpeciesIds), taxon.getId());
                }
                
                return true;
              }
            });
            
          });
          
          oldSpeciesSetsToCurrentTaxa.asMap().forEach((oldSpeciesSet, collectionOfNewTaxaIds) -> {
            // Get the list of species that this represents in the old taxonomy 
            List<Taxon> oldSpeciesList = Streams.stream(Splitter.on('/').split(oldSpeciesSet))
                .map(oldTaxonomy::getTaxon)
                .collect(ImmutableList.toImmutableList());
            String[] splitDefinition = new String[5];
            splitDefinition[0] = Integer.toString(currentIndex);
            splitDefinition[1] = "complex";
            splitDefinition[2] = oldSpeciesList.stream().map(Taxon::getCommonName).collect(Collectors.joining("/"));
            splitDefinition[3] = oldSpeciesList.stream().map(taxon -> TaxonUtils.getFullName(taxon)).collect(Collectors.joining("/"));
            splitDefinition[4] = SLASH_JOINER.join(collectionOfNewTaxaIds); 
            try {
              outputFile.nextLine(splitDefinition);
            } catch (IOException e) {
              throw new RuntimeException(e);
            }
            
          });
  //        Set<String> oldSpeciesIdsWithUniqueMappings = new LinkedHashSet<>();
  //        currentTaxaToOldSpecies.asMap().forEach((__, c) -> { if (c.size() == 1) { oldSpeciesIdsWithUniqueMappings.add(Iterables.getOnlyElement(c));} } );
  //        
  //        if (oldSpeciesIdsWithUniqueMappings.size() < oldSpeciesIds.size()) {
  //          System.err.printf("Can't resolve lump: %s\n",
  //             SightingTaxons.newPossiblySpTaxon(Sets.difference(ImmutableSet.copyOf(oldSpeciesIds), oldSpeciesIdsWithUniqueMappings)).resolve(oldTaxonomy).getCommonName());
  //        }
        }
      });
    } finally {
      outputFile.close();
    }
        
    // TODO: if (A) -> (C, D), and (B) -> (D, E), then really
    // it's (A, B) -> (C, D, E), which is funky.  This happened for
    // Nightingale Finch -> Inaccessible/Nightingale Island Finch,
    // Wilkins's Finch -> Inaccessible Island/Wilkins's Finch... but
    // I'm not *that* worried about correctly reporting numbers for
    // the masses of people who'd somehow seen both of the old forms!
  }

  private boolean isIgnoredTaxon(Taxon taxon) {
    // Doubt anyone ever ticked "Liberian Greenbul" 
    if ("Liberian Greenbul".equals(taxon.getCommonName())) {
      return true;
    }
    
    // Ditto 
    if ("Bogota Sunangel".equals(taxon.getCommonName())) {
      return true;
    }

    if (taxon.getStatus() == Status.EX) {
      return true;
    }
    
    return false;
  }

}
