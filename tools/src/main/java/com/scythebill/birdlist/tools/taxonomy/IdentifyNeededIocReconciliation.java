/*   
 * Copyright 2017 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.stream.Collectors;

import org.xml.sax.SAXException;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.base.Optional;
import com.google.common.io.Files;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Type;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.xml.XmlReportSetImport;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;

/**
 * Tool to process a prior report set (containing all IOC species) and find which of
 * those have become "spuhs" in the latest IOC upgrade.
 */
public class IdentifyNeededIocReconciliation {
  @Parameter(names = "-ioc", required = true)
  private String iocFileName;

  @Parameter(names = "-clements", required = true)
  private String clementsFileName;

  @Parameter(names = "-priorReport", required = true)
  private String priorReportFile;

  @Parameter(names = "-out", required = true)
  private String outFile;

  private MappedTaxonomy ioc;

  public static void main(String[] args) throws Exception {
    IdentifyNeededIocReconciliation identifyNeededIocReconciliation = new IdentifyNeededIocReconciliation();
    new JCommander(identifyNeededIocReconciliation, args);
    identifyNeededIocReconciliation.run();
  }

  private void run() throws IOException, SAXException {
    ioc = ToolTaxonUtils.getMappedTaxonomy(clementsFileName, iocFileName);
    
    Reader in = Files.newReader(new File(priorReportFile), StandardCharsets.UTF_8);
    ReportSet reportSet = new XmlReportSetImport().importReportSet(in, ioc.getBaseTaxonomy(), Optional.of(ioc), null);
    
    HashSet<SightingTaxon> alreadyProcessed = new HashSet<>();
    try (ExportLines out = CsvExportLines.toFile(new File(outFile), StandardCharsets.UTF_8)) {
      for (Sighting sighting : reportSet.getSightings()) {
        SightingTaxon taxon = sighting.getTaxon();
        Resolved resolved = taxon.resolve(ioc);
        if (resolved == null) {
          throw new NullPointerException("Could not resolve " + taxon);
        }
        if (resolved.getSmallestTaxonType() != Taxon.Type.species) {
          resolved = resolved.getParentOfAtLeastType(Taxon.Type.species).resolveInternal(ioc);
        }
        
        if (resolved.getType() == Type.SP) {
          if (alreadyProcessed.add(taxon)) {
            out.nextLine(new String[]{
                taxon.getIds().stream().collect(Collectors.joining("/")),
                resolved.getCommonName()
            });
          }
        }
      }
    }
  }
}
