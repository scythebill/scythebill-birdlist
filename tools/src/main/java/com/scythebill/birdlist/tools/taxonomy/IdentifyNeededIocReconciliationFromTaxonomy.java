/*   
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.stream.Collectors;

import org.xml.sax.SAXException;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Type;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;

/**
 * Tool to process an entire prior IOC taxonomy and find which of those have become "spuhs" in the
 * latest IOC upgrade.
 * <p>
 * This was definitely preferable for IOC 11.1->11.2, but does require that the old IOC and new IOC
 * are both mapped to the same Clements!
 */
public class IdentifyNeededIocReconciliationFromTaxonomy {
  @Parameter(names = "-oldIoc", required = true)
  private String oldIocFileName;

  @Parameter(names = "-newIoc", required = true)
  private String newIocFileName;

  @Parameter(names = "-clements", required = true)
  private String clementsFileName;

  @Parameter(names = "-out", required = true)
  private String outFile;

  private MappedTaxonomy oldIoc;
  private MappedTaxonomy newIoc;

  public static void main(String[] args) throws Exception {
    IdentifyNeededIocReconciliationFromTaxonomy identifyNeededIocReconciliation = new IdentifyNeededIocReconciliationFromTaxonomy();
    new JCommander(identifyNeededIocReconciliation, args);
    identifyNeededIocReconciliation.run();
  }

  private void run() throws IOException, SAXException {
    Taxonomy clements = ToolTaxonUtils.getTaxonomy(clementsFileName);
    oldIoc = ToolTaxonUtils.getMappedTaxonomy(clements, oldIocFileName);
    newIoc = ToolTaxonUtils.getMappedTaxonomy(clements, newIocFileName);
    
    HashSet<String> alreadyProcessed = new HashSet<>();
    try (ExportLines out = CsvExportLines.toFile(new File(outFile), StandardCharsets.UTF_8)) {
      TaxonUtils.visitTaxa(oldIoc, new TaxonVisitor() {
        @Override
        public boolean visitTaxon(Taxon taxon) {
          if (taxon.getType() == Taxon.Type.species || taxon.getType() == Taxon.Type.subspecies) {
            SightingTaxon mapping = oldIoc.getMapping(taxon.getId());
            if (mapping != null) {
              Resolved resolved = mapping.resolve(newIoc);
              if (resolved == null) {
                throw new NullPointerException("Could not resolve " + taxon);
              }
              if (resolved.getSmallestTaxonType() != Taxon.Type.species) {
                resolved = resolved.getParentOfAtLeastType(Taxon.Type.species).resolveInternal(newIoc);
              }
              
              if (resolved.getType() == Type.SP) {
                String oldIds = mapping.getIds().stream().collect(Collectors.joining("/"));
                if (alreadyProcessed.add(oldIds)) {
                  try {
                    out.nextLine(new String[]{
                        oldIds,
                        resolved.getCommonName()
                    });
                  } catch (IOException e) {
                    throw new RuntimeException(e);
                  }
                }
              }
            }
          }
          return true;
        }
      });
    }
  }
}
