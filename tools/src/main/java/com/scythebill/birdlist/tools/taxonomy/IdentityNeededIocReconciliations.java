/*   
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.collect.ImmutableList;

public class IdentityNeededIocReconciliations {
  @Parameter(names = "-ioc", required = true)
  private String iocFileName;

  @Parameter(names = "-clements", required = true)
  private String clementsFileName;

  @Parameter(names = "-priorReportPattern", required = true)
  private String priorReportPattern;

  @Parameter(names = "-outPattern", required = true)
  private String outPattern;
  
  private static final ImmutableList<String> IOC_VERSIONS =
      ImmutableList.of(
          "6.3",
          "6.4",
          "7.1",
          "7.2",
          "7.3",
          "8.1",
          "8.2",
          "9.1",
          "9.2",
          "10.1",
          "10.2",
          "11.1",
          "11.2",
          "12.1",
          "12.2",
          "13.1",
          "13.2",
          "14.1",
          "14.2"
          );

  public static void main(String[] args) throws Exception {
    IdentityNeededIocReconciliations identifyNeededIocReconciliation = new IdentityNeededIocReconciliations();
    new JCommander(identifyNeededIocReconciliation, args);
    identifyNeededIocReconciliation.run();
  }

  private void run() throws Exception {
    for (String version : IOC_VERSIONS) {
      System.out.println("Starting version " + version);
      IdentifyNeededIocReconciliation.main(new String[] {
        "-ioc", iocFileName,
        "-clements", clementsFileName,
        "-priorReport", String.format(priorReportPattern, version),
        "-out", String.format(outPattern, version)
      });
      
    }
  }

}
