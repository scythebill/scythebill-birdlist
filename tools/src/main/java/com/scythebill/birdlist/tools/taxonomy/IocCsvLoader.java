/*   
 * Copyright 2012 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.common.base.CaseFormat;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.opencsv.CSVReader;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.SpeciesImpl;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.TaxonImpl;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;
import com.scythebill.birdlist.model.xml.XmlTaxonExport;

/**
 * Loads a CSV file of the form from the IOC world list. 
 */
public class IocCsvLoader {
  // 3.2 Notes:
  //
  // Italian Sparrow -> House Sparrow, should be House x Spanish
  // Rufous-backed Kingfisher -> Oriental Dwarf, reverse mapping would need
  //  to be sp.
  
  private final static ImmutableMap<String, String> ORDER_COMMON_NAMES;
  static {
    ImmutableMap.Builder<String, String> orderCommonNames = ImmutableMap.builder();
    orderCommonNames.put("Tinamiformes", "Tinamous");
    orderCommonNames.put("Struthioniformes", "Ostriches");
    orderCommonNames.put("Rheiformes", "Rheas");
    orderCommonNames.put("Casuariiformes", "Cassowaries and Emu");
    orderCommonNames.put("Apterygiformes", "Kiwis");
    orderCommonNames.put("Anseriformes", "Waterfowl");
    orderCommonNames.put("Galliformes", "Megapodes to Pheasants");
    orderCommonNames.put("Gaviiformes", "Loons");
    orderCommonNames.put("Sphenisciformes", "Penguins");
    orderCommonNames.put("Procellariiformes", "Tubenoses");
    orderCommonNames.put("Podicipediformes", "Grebes");
    orderCommonNames.put("Phoenicopteriformes", "Flamingos");
    orderCommonNames.put("Phaethontiformes", "Tropicbirds");
    orderCommonNames.put("Ciconiiformes", "Storks");
    orderCommonNames.put("Pelecaniformes", "Ibises, Herons, Pelicans");
    orderCommonNames.put("Suliformes", "Gannets, Cormorants, etc.");
    orderCommonNames.put("Accipitriformes", "Hawks, Vultures, etc.");
    orderCommonNames.put("Otidiformes", "Bustards");
    orderCommonNames.put("Mesitornithiformes", "Mesites");
    orderCommonNames.put("Cariamiformes", "Seriemas");
    orderCommonNames.put("Eurypygiformes", "Kagu and Sunbittern");
    orderCommonNames.put("Gruiformes", "Rails, Cranes, etc.");
    orderCommonNames.put("Charadriiformes", "Shorebirds, Gulls, etc.");
    orderCommonNames.put("Pterocliformes", "Sandgrouse");
    orderCommonNames.put("Columbiformes", "Pigeons, Doves");
    orderCommonNames.put("Opisthocomiformes", "Hoatzin");
    orderCommonNames.put("Musophagiformes", "Turacos");
    orderCommonNames.put("Cuculiformes", "Cuckoos");
    orderCommonNames.put("Strigiformes", "Owls");
    orderCommonNames.put("Caprimulgiformes", "Nightjars");
    orderCommonNames.put("Steatornithiformes", "Oilbird");
    orderCommonNames.put("Nyctibiiformes", "Potoos");
    orderCommonNames.put("Podargiformes", "Frogmouths");
    orderCommonNames.put("Aegotheliformes", "Owlet-nightjars");
    orderCommonNames.put("Apodiformes", "Swifts and Hummingbirds");
    orderCommonNames.put("Coliiformes", "Mousebirds");
    orderCommonNames.put("Trogoniformes", "Trogons");
    orderCommonNames.put("Leptosomiformes", "Cuckoo Roller");
    orderCommonNames.put("Coraciiformes", "Kingfishers, Rollers, etc.");
    orderCommonNames.put("Bucerotiformes", "Hornbills and Hoopoes");
    orderCommonNames.put("Piciformes", "Woodpeckers, Barbets, etc.");
    orderCommonNames.put("Falconiformes", "Falcons");
    orderCommonNames.put("Psittaciformes", "Parrots");
    orderCommonNames.put("Passeriformes", "Perching Birds");
    ORDER_COMMON_NAMES = orderCommonNames.build();
  }
  
  private final static Logger logger = Logger.getLogger(IocCsvLoader.class.getName());
  /**
   * "Order"
   * "Family (Scientific)"
   * "Family (English)"
   * "Genus"
   * "Species (Scientific)"
   * "Subspecies"
   * "Authority"
   * "Species (English)"
   * "Breeding Range General Region"
   * "Breeding Range Subregion(s)"
   * "Nonbreeding Range"
   * "Code"
   * "Comment"
   */
  public static final int INDEX_INFRACLASS = 0;
  public static final int INDEX_PARVCLASS = 1;
  public static final int INDEX_ORDER = 2;
  public static final int INDEX_FAMILY_SCIENTIFIC = 3;
  public static final int INDEX_FAMILY_COMMON = 4;
  public static final int INDEX_GENUS = 5;
  public static final int INDEX_SPECIES_SCIENTIFIC = 6;
  public static final int INDEX_SUBSPECIES = 7;
  public static final int INDEX_AUTHORITY = 8;
  public static final int INDEX_SPECIES_ENGLISH = 9;
  public static final int INDEX_BREEDING_RANGE_REGION = 10;
  public static final int INDEX_BREEDING_RANGE_SUBREGION = 11;
  public static final int INDEX_NONBREEDING_RANGE = 12;
  public static final int INDEX_CODE = 13;
  public static final int INDEX_COMMENT = 14;
  
  private static final ImmutableMap<String, String> REGION_NAMES = ImmutableMap.<String, String>builder()
          .put("SA", "South America")
          .put("NA", "North America")
          .put("AF", "Africa")
          .put("EU", "Eurasia")
          .put("NA, EU", "Holarctic")
          .put("EU, NA", "Holarctic")
          .put("NA, MA", "North and Middle America")
          .put("MA", "Middle America")
          .put("OR", "Oriental Region")
          .put("PO", "Pacific Ocean")
          .put("PO, AO", "Pacific and Atlantic Oceans")
          .put("IO", "Indian Ocean")
          .put("AO", "Atlantic Ocean")
          .put("IO, AO", "Indian and Atlantic Oceans")
          .put("TrO", "Tropical Oceans")
          .put("TO", "Temperate Oceans")
          .put("NO", "Northern Oceans")
          .put("SO", "Southern Oceans")
          .put("AN", "Antarctica")
          .put("AU", "Australasia")
          .put("LA", "Latin America")
          .put("PAL", "Palearctic")
          .build();
 
  private TaxonomyImpl aves;
  private TaxonImpl currentOrder;
  private TaxonImpl currentFamily;
  private TaxonImpl currentGenus;
  private SpeciesImpl currentSpecies;
  
  static public void main(String[] args) throws Exception {
    // Note - the CSV must have been exported using UTF-8
    File iocCsv = new File(args[0]);
    IocCsvLoader loader = new IocCsvLoader();
    Taxonomy loadIocCsv = loader.loadCsv(iocCsv);

    OutputStreamWriter out = new OutputStreamWriter(
        new FileOutputStream(args.length > 1 ? args[1] : "/tmp/taxon.xml"), "UTF-8");
    (new XmlTaxonExport()).export(out, loadIocCsv, "UTF-8");
    out.close();
  }
  
  public IocCsvLoader() {
    aves = new TaxonomyImpl("ioc_151", "IOC World Bird List 15.1 (February 2025)", null, null); 
    TaxonImpl root = new TaxonImpl(Type.classTaxon, aves);
    root.setCommonName("Birds");
    root.setName("Aves");
    root.built();
    aves.setRoot(root);
  }
  
  public Taxonomy loadCsv(File file) throws Exception {
    Reader reader = new BufferedReader(
        new InputStreamReader(
            new FileInputStream(file),
            "UTF-8"));
    try (CSVReader csv = new CSVReader(reader, ',')) {
      // Skip over all lines until the line with the real headings emerges
      while (true) {
        String[] headerLine = csv.readNext();
        if ("Order".equals(headerLine[INDEX_ORDER])) {
          break;
        }
      }
      while (true) {
        String[] nextLine = csv.readNext();
        if (nextLine == null) {
          break;
        }
        
        try {
          processLine(nextLine);
        } catch (Exception e) {
          logger.log(Level.SEVERE, "Could not process line:\n" + Lists.newArrayList(nextLine), e);
        }
      }
    }
    
    return aves;
  }

  private void processLine(String[] nextLine) {
    // Ignore infra-class entries
    String infraClass = nextLine[INDEX_INFRACLASS];
    if (!infraClass.isEmpty()) {
      return;
    }
    // Ignore parv-class entries
    String parvClass = nextLine[INDEX_PARVCLASS];
    if (!parvClass.isEmpty()) {
      return;
    }
    String orderName = nextLine[INDEX_ORDER];
    String familyName = nextLine[INDEX_FAMILY_SCIENTIFIC];
    String genusName = nextLine[INDEX_GENUS];
    String speciesName = nextLine[INDEX_SPECIES_SCIENTIFIC];
    String subspeciesName = nextLine[INDEX_SUBSPECIES];
    
    boolean nameIndicatesExtinction = false;
    if (!"".equals(orderName)) {
      orderName = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, orderName);
      orderName = trimMark(orderName);
      currentOrder = new TaxonImpl(Type.order, aves.getRoot());
      
      currentOrder.setName(orderName);
      if (!ORDER_COMMON_NAMES.containsKey(orderName)) {
        throw new IllegalStateException("No common name for order " + orderName);
      }
      currentOrder.setCommonName(ORDER_COMMON_NAMES.get(orderName));
      aves.getRoot().getContents().add(currentOrder);
      currentOrder.built();
      return;
    } else if (!"".equals(familyName)) {
      currentFamily = new TaxonImpl(Type.family, currentOrder);      
      currentFamily.setName(trimMark(familyName));
      currentFamily.setCommonName(nextLine[INDEX_FAMILY_COMMON]);
      currentOrder.getContents().add(currentFamily);
      currentFamily.built();
      return;
    } else if (!"".equals(genusName)) {
      if (currentGenus != null) {
        UndescribedSpecies.maybeAddUndescribedForms(currentGenus, UndescribedSpecies.ClementsOrIoc.IOC);
      }
      
      currentGenus = new TaxonImpl(Type.genus, currentFamily);
      currentGenus.setName(trimMark(genusName));
      currentFamily.getContents().add(currentGenus);
      currentGenus.built();
      return;
    }
    
    SpeciesImpl currentTaxon;
    if (!"".equals(speciesName)) {
      nameIndicatesExtinction = endsWithMark(speciesName);
      currentSpecies = new SpeciesImpl(Type.species, currentGenus);
      currentSpecies.setName(trimMark(speciesName));
      currentSpecies.setCommonName(nextLine[INDEX_SPECIES_ENGLISH]);
      currentTaxon = currentSpecies;
    } else {
      nameIndicatesExtinction = endsWithMark(subspeciesName);
      Preconditions.checkState(!"".equals(subspeciesName));
      currentTaxon = new SpeciesImpl(Type.subspecies, currentSpecies);
      currentTaxon.setName(trimMark(subspeciesName));
    }
    
    currentTaxon.setRange(nextLine[INDEX_BREEDING_RANGE_SUBREGION]);
    if (isShortRange(currentTaxon.getRange())
        || "widespread".equalsIgnoreCase(currentTaxon.getRange())) {
      String regionName = expandRegionNameAbbreviations(nextLine[INDEX_BREEDING_RANGE_REGION]);
      if (currentTaxon.getRange().isEmpty()) {
        currentTaxon.setRange(regionName);
      } else {
        currentTaxon.setRange(currentTaxon.getRange() + " " + regionName);
      }
    }
    if (!"".equals(nextLine[INDEX_NONBREEDING_RANGE])) {
      currentTaxon.setRange(currentTaxon.getRange() + " > " + nextLine[INDEX_NONBREEDING_RANGE]);
    }
    if (!"".equals(nextLine[INDEX_COMMENT])) {
      currentTaxon.setTaxonomicInfo(trimItalics(nextLine[INDEX_COMMENT]));
    }
    
    if (nameIndicatesExtinction) {
      currentTaxon.setStatus(Status.EX);
    } else {
      if (!"".equals(nextLine[INDEX_CODE])) {
        Iterable<String> split = Splitter.on(',').split(nextLine[INDEX_CODE]);
        if (Iterables.contains(split, "EXT")) {
          currentTaxon.setStatus(Status.EX);
        }
      }
    }
    
    // Set to "DD" to prevent test failures from a failed mapping
    if ("Deignan's Babbler".equals(currentTaxon.getCommonName())) {
      currentTaxon.setStatus(Status.DD);
    }
      
    currentTaxon.getParent().getContents().add(currentTaxon);
    currentTaxon.built();
    
    FeralAndDomesticForms.maybeAddFeralOrDomesticForm(currentTaxon, Type.subspecies, /*clements=*/false);
  }
  
  /** Find instances of ranges like "n" or "sc, se", so we know to include the region. */
  private boolean isShortRange(String string) {
    if (string.length() < 2) {
      return true;
    }
    
    for (String token : Splitter.on(',').trimResults().split(string)) {
      if (token.length() > 2) {
        return false;
      }
    }
    
    return true;
  }
  
  private String expandRegionNameAbbreviations(String string) {
    StringBuilder rangeString = new StringBuilder();
    StringTokenizer tokenizer = new StringTokenizer(string, " ,", true);
    while (tokenizer.hasMoreTokens()) {
      String token = tokenizer.nextToken();
      if (REGION_NAMES.containsKey(token)) {
        rangeString.append(REGION_NAMES.get(token));
      } else {
        rangeString.append(token);
      }
    }
    return rangeString.toString();
  }

  private String trimItalics(String text) {
    return text.replace("<i>", "").replace("</i>", "").replace("<I>", "").replace("</I>", "");
  }
  
  private String trimMark(String name) {
    if (endsWithMark(name)) {
      return name.replace("\u2020", "").trim();
    }
    return name;
  }

  private boolean endsWithMark(String name) {
    return name.endsWith(" \u2020")
        || name.endsWith(" \u2020\u2020")
        || name.endsWith("\u2020");
  }
}
