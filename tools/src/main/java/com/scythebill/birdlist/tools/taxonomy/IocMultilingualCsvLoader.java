package com.scythebill.birdlist.tools.taxonomy;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Logger;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Maps;
import com.google.common.io.Resources;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.BKTree;
import com.scythebill.birdlist.model.util.Metrics;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;

public class IocMultilingualCsvLoader {
  private static final int ORDER_INDEX = 1;
  private static final int FAMILY_INDEX = 2;
  private static final int SCIENTIFIC_INDEX = 3;
  private static final int ENGLISH_INDEX = 4;
  private static final Logger logger = Logger.getLogger(IocMultilingualCsvLoader.class.getCanonicalName());

  public static void main(String[] args) throws Exception {
     new IocMultilingualCsvLoader(args).run(); 
  }
  
  @Parameter(names = "-ioc", required = true)
  private String iocFileName;

  @Parameter(names = "-input", required = true)
  private String inputName;

  @Parameter(names = "-outputDir", required = true)
  private String outputDir;

  private Taxonomy ioc;

  private final static ImmutableSet<String> NOT_LOCALE_NAMES = ImmutableSet.of(
      "Seq.", "Order", "Family", "IOC_13.2", "English");
  
  private final static ImmutableMap<String, Locale> HEADER_TO_LOCALE =
      ImmutableMap.<String, Locale>builder()
      .put("Arabic", new Locale("ar"))
      .put("Belarusian", new Locale("be"))
      .put("Bulgarian", new Locale("bg"))
      .put("Catalan", new Locale("ca"))
      .put("Chinese", new Locale("zh"))
      .put("Chinese (Traditional)", new Locale("zh", "TW"))
      .put("Croatian", new Locale("hr"))
      .put("Czech", new Locale("cs"))
      .put("Danish", new Locale("da"))
      .put("Dutch", new Locale("nl"))
      .put("Finnish", new Locale("fi"))
      .put("French", new Locale("fr"))
      .put("German", new Locale("de"))
      .put("Greek", new Locale("el"))
      .put("Hebrew", new Locale("iw"))
      .put("Italian", new Locale("it"))
      .put("Japanese", new Locale("ja"))
      .put("Korean", new Locale("ko"))
      .put("Lithuanian", new Locale("lt"))
      .put("Macedonian", new Locale("mk"))
      .put("Malayalam", new Locale("ml"))
      .put("Norwegian", new Locale("no"))
      .put("Polish", new Locale("pl"))
      .put("Persian", new Locale("fa"))
      .put("Portuguese (Lusophone)", new Locale("pt"))
      .put("Portuguese (Portuguese)", new Locale("pt", "PT"))
      .put("Romanian", new Locale("ro"))
      .put("Russian", new Locale("ru"))
      .put("Serbian", new Locale("sr"))
      .put("Slovak", new Locale("sk"))
      .put("Spanish", new Locale("es"))
      .put("Swedish", new Locale("sv"))
      .put("Turkish", new Locale("tr"))
      .put("Ukrainian", new Locale("uk"))
      .put("Afrikaans", new Locale("af"))
      .put("Estonian", new Locale("et"))
      .put("Hungarian", new Locale("hu"))
      .put("Icelandic", new Locale("is"))
      .put("Indonesian", new Locale("in"))
      .put("Latvian", new Locale("lv"))
      .put("Northern Sami", new Locale("se"))
      .put("Slovenian", new Locale("sl"))
      .put("Thai", new Locale("th"))
      .build();
  
  private BKTree<String, Taxon> commonNameTree;
  private BKTree<String, Taxon> sciNameTree;
  private Map<Integer, Locale> indexToLocale;  
  
  IocMultilingualCsvLoader(String[] args) {
    new JCommander(this, args);
  }

  private void run() throws Exception {
    ioc = ToolTaxonUtils.getTaxonomy(iocFileName);
    buildNameIndices();
    
    ImmutableTable.Builder<Locale, String, String> namesByLocaleAndId = ImmutableTable.builder();
    
    ImportLines importLines = CsvImportLines.fromFile(new File(inputName), Charsets.UTF_8);
    int totalSpecies = 0;
    try {
      String[] header = importLines.nextLine();
      
      // Make sure everything is as expected in the headers
      Preconditions.checkState("Order".equals(header[ORDER_INDEX]));
      Preconditions.checkState("Family".equals(header[FAMILY_INDEX]));
      Preconditions.checkState(header[SCIENTIFIC_INDEX].startsWith("IOC_"));
      Preconditions.checkState("English".equals(header[ENGLISH_INDEX]));
      indexToLocale = Maps.newLinkedHashMap();
      for (int i = 0; i < header.length; i++) {
        Locale locale = HEADER_TO_LOCALE.get(header[i]);
        if (locale != null) {
          indexToLocale.put(i, locale);
        } else if (!NOT_LOCALE_NAMES.contains(header[i])) {
          System.err.println("Line 1: New locale " + header[i]);
        }
      }
      
//      System.err.println(HEADER_TO_LOCALE.size());
//      System.err.println(indexToLocale.size());
//      System.err.println(indexToLocale);
      
      while (true) {
        String[] line = importLines.nextLine();
        if (line == null) {
          break;
        }
        
        String english = line[ENGLISH_INDEX];
        String scientific = line[SCIENTIFIC_INDEX];
        
        if (Strings.isNullOrEmpty(english)) {
          logger.warning("Empty english on " + Joiner.on(',').join(line));
        }
        if (Strings.isNullOrEmpty(scientific)) {
          logger.warning("Empty scientific on " + Joiner.on(',').join(line));
        }
        
        totalSpecies++;
        
        List<Taxon> search = commonNameTree.search(english, 0);
        if (search.isEmpty()) {
          logger.warning("No common name entries for " + english);
        } else if (search.size() > 1) {
          logger.warning("Too many common name entries for " + english);          
        } else {
          Taxon taxon = search.get(0);
          for (int i = 0; i < line.length; i++) {
            Locale locale = indexToLocale.get(i);
            if (locale != null &&
                !Strings.isNullOrEmpty(line[i])) {
              namesByLocaleAndId.put(locale, taxon.getId(), line[i]);
            }
          }
        }
      }
    } finally {
      importLines.close();
    }

    ImportLines britishBirds = CsvImportLines.fromUrl(
        Resources.getResource(getClass(), "british-birds.csv"),
        Charsets.UTF_8);
    try {
      while (true) {
        String[] line = britishBirds.nextLine();
        if (line == null) {
          break;
        }
        
        String britishName = line[0];
        String sciName = line[1];
        
        List<Taxon> search = sciNameTree.search(sciName, 0);
        if (search.isEmpty()) {
          logger.warning("UK: No sci name entries for " + sciName);
        } else if (search.size() > 1) {
          logger.warning("UK: Too many sci name entries for " + sciName);          
        } else {
          Taxon taxon = search.get(0);
          namesByLocaleAndId.put(Locale.UK, taxon.getId(), britishName);
        }
      }      
    } finally {
      britishBirds.close();
    }
    System.out.println("Total species: " + totalSpecies);
    
    ImmutableTable<Locale, String, String> built = namesByLocaleAndId.build();
    for (Locale locale : built.rowKeySet()) {
      File outputFile = new File(outputDir,
          String.format("names-%s.csv", locale.toString()));
      outputFile.createNewFile();
      ExportLines exportLines = CsvExportLines.fromWriter(
          new BufferedWriter(new OutputStreamWriter(
              new FileOutputStream(outputFile), Charsets.UTF_8)));
      try {
        System.out.println("Locale: " + locale + ", total " + built.row(locale).size());
        System.err.println(locale.getDisplayName(locale));
        for (Map.Entry<String, String> idAndName : built.row(locale).entrySet()) {
          exportLines.nextLine(new String[]{idAndName.getKey(), idAndName.getValue()});
        }
      } finally {
        exportLines.close();
      }        
    }
  }

  private void buildNameIndices() {
    // For every common name, store it in a BKTree to make finding species
    // by (closely-spelled) names easy.
    BKTree.Builder<String, Taxon> commonNameTree = BKTree.builder(Metrics.levenshteinDistance());
    BKTree.Builder<String, Taxon> sciNameTree = BKTree.builder(Metrics.levenshteinDistance());
    buildIndicesForTaxonomy(commonNameTree, sciNameTree);
    this.commonNameTree = commonNameTree.build();
    this.sciNameTree = sciNameTree.build();
  }

  private void buildIndicesForTaxonomy(
      final BKTree.Builder<String, Taxon> commonNameTreeBuilder,
      final BKTree.Builder<String, Taxon> sciNameTreeBuilder) {
    TaxonUtils.visitTaxa(ioc, new TaxonVisitor() {
      @Override
      public boolean visitTaxon(Taxon taxon) {
        switch (taxon.getType()) {
        case species:
          // Store all names, common or otherwise
          commonNameTreeBuilder.add(taxon.getCommonName(), taxon);
          sciNameTreeBuilder.add(TaxonUtils.getFullName(taxon), taxon);
          break;
        default:
          break;
        }

        return true;
      }
    });
  }
}
