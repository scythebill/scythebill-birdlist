/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;

/**
 * Utility for scanning HBW names and seeing which can be mapped.
 */
public class LookForHbwNames {
  @Parameter(names = "-clements", required = true)
  private String clementsFileName;

  @Parameter(names = "-ioc", required = true)
  private String iocFileName;

  @Parameter(names = "-hbw", required = true)
  private String hbwCsvFile;

  public LookForHbwNames(String[] args) {
    new JCommander(this, args);
  }

  public static void main(String[] args) throws Exception {
    new LookForHbwNames(args).run(); 
  }

  private void run() throws Exception {
//    Taxonomy taxonomy = ToolTaxonUtils.getMappedTaxonomy(clementsFileName, iocFileName);
    Taxonomy taxonomy = ToolTaxonUtils.getTaxonomy(clementsFileName);
    Set<String> primaryNames = new LinkedHashSet<>();
    Set<String> alternateNames = new LinkedHashSet<>();
    
    TaxonUtils.visitTaxa(taxonomy, new TaxonVisitor() {
      @Override
      public boolean visitTaxon(Taxon taxon) {
        if (taxon instanceof Species) {
          Species species = (Species) taxon;
          if (taxon.getCommonName() != null) {
            primaryNames.add(normalize(taxon.getCommonName()));
          }
          for (String alternate : species.getAlternateCommonNames()) {
            if (alternate.endsWith(" - in part")) {
              alternate = alternate.substring(0, alternate.length() - " - in part".length());
            }
            alternateNames.add(normalize(alternate));
          }
        }
        return true;
      }
    });
    
    AtomicInteger primaryCount = new AtomicInteger();
    AtomicInteger alternateCount = new AtomicInteger();
    AtomicInteger missingCount = new AtomicInteger();
    try (ImportLines importLines = CsvImportLines.fromFile(new File(hbwCsvFile), StandardCharsets.UTF_8)) {
      // Skip the header
      importLines.nextLine();
      while (true) {
        String[] line = importLines.nextLine();
        if (line == null) {
          break;
        }
        if (line.length < 4) {
          continue;
        }

        // Ignore extinct species
        if (line[3].equals("EX")) {
          continue;
        }
        
        String name = normalize(line[1]);
        if (primaryNames.contains(name)) {
          primaryCount.incrementAndGet();
        } else if (alternateNames.contains(name)) {
          alternateCount.incrementAndGet();
        } else {
          System.err.println(line[1]);
          missingCount.incrementAndGet();
        }
      }
    }
    
    System.out.printf("Primary: %d;  Alternate: %d;  Missing:  %d\n",
        primaryCount.get(), alternateCount.get(), missingCount.get());
  }
  
  private String normalize(String name) {
    name = name.toLowerCase();
    name = name.replace("-", " ");
    name = name.replace("grey", "gray");
    name = name.replace("steamer duck", "steamerduck");
    name = name.replace("brush turkey", "brushturkey");
    name = name.replace("wood pigeon", "woodpigeon");
    name = name.replace("fairy wren", "fairywren");
    name = name.replace("shrike thrush", "shrikethrush");
    name = name.replace("bush shrike", "bushshrike");
    name = name.replace("brush finch", "brushfinch");
    name = name.replace("colour", "color");
    name = name.replace("chequer", "checker");
    name = name.replace("racquet", "racket");
    name = name.replace("violet ear", "violetear");
    name = name.replace("native hen", "nativehen");
    name = name.replace("cacholote", "cachalote");
    name = name.replace("ç", "c");
    name = name.replace("ñ", "n");
    return name;
  }
}
