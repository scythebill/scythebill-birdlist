/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.base.CaseFormat;
import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.scythebill.birdlist.model.checklist.ExtendedTaxonomyChecklists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.SpeciesImpl;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.TaxonImpl;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;
import com.scythebill.birdlist.ui.imports.LocationShortcuts;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.taxonomy.ExtendedTaxonomyCsvExporter;

/**
 * Loads a Taxonomy from https://www.mammalwatching.com/resources/global-mammal-checklist/
 * into basic XML format.  Used as a test bed for attaching taxonomies to reportsets,
 * since all the interesting code depends on that.
 */
public class MammalWatchingChecklistParser {
  @Parameter(names = "-file", required = true)
  private String fileName;

  @Parameter(names = "-out", required = true)
  private String outFile;

  private int commonNameIndex;

  private Integer alternateSciIndex;

  private int iucnRedListStatusIndex;

  private Integer countriesIndex;

  private LocationShortcuts locationShortcuts;

  private int alternateCommonIndex;

  private int rangeIndex;

  private int taxonomyNotesIndex;

  private static final Splitter COUNTRIES_SPLITTER = Splitter.on('|').trimResults().omitEmptyStrings();
  private static final Set<String> MULTI_REGION_LOCATION_CODES = ImmutableSet.of(
      "RU",
      "ID",
      "TR",
      "BQ",
      "US");

  public MammalWatchingChecklistParser(String[] args) {
    new JCommander(this, args);
  }

  public static void main(String[] args) throws Exception {
    new MammalWatchingChecklistParser(args).run(); 
 }

  private void run() throws IOException {
    TaxonomyImpl taxonomy = new TaxonomyImpl("mammal-watching", "Mammal Watching Sep. 2024", null, null);
    taxonomy.addAdditionalCredit("Jon Hall mammalwatching.com (based on the Mammal Diversity Database)");
    TaxonImpl root = new TaxonImpl(Type.classTaxon, taxonomy);
    root.setCommonName("Mammals");
    root.setName("Mammalia");
    root.built();
    taxonomy.setRoot(root);
    
    ImportLines importLines = CsvImportLines.fromFile(new File(fileName), Charsets.UTF_8);
    String[] line = null;
    ExtendedTaxonomyChecklists.Builder checklistBuilder = new ExtendedTaxonomyChecklists.Builder();
    locationShortcuts = new LocationShortcuts(ReportSets.newReportSet(taxonomy).getLocations(), PredefinedLocations.loadAndParse());
    
    try {
      Map<String, Integer> indexMap = toIndexMap(importLines.nextLine());
      int orderIndex = indexMap.get("order");
      int familyIndex = indexMap.get("family");
      int sciIndex = indexMap.get("scientificname");
      alternateSciIndex = indexMap.get("synonyms");
      alternateCommonIndex = indexMap.get("othercommonnames");
      commonNameIndex = indexMap.get("commonname");
      iucnRedListStatusIndex = indexMap.get("iucnstatus");
      countriesIndex = indexMap.get("countrydistribution");
      rangeIndex = indexMap.get("distributionnotes");
      taxonomyNotesIndex = indexMap.get("taxonomynotes");

      TaxonImpl currentOrder = null;
      TaxonImpl currentFamily = null;
      TaxonImpl currentGenus = null;
      SpeciesImpl currentSpecies = null;
      while ((line = importLines.nextLine()) != null) {
        String order = line[orderIndex];
        order = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, order);
        // Blank lines, skip
        if (order.isEmpty()) {
          System.err.println("MISSING ORDER: " + Arrays.asList(line));
          continue;
        }
        if (currentOrder == null    
            || !currentOrder.getName().equals(order)) {
          currentOrder = new TaxonImpl(Type.order, root);
          
          currentOrder.setName(order);
          root.getContents().add(currentOrder);
          currentFamily = null;
          currentOrder.built();
        }
        
        String family = line[familyIndex];
        if (family.isEmpty() && currentFamily != null) {
          family = currentFamily.getName();
        }
        if (family.isEmpty()) {
          System.err.println("MISSING FAMILY: " + Arrays.asList(line));
          continue; 
        }
        
        family = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, family);
        if (currentFamily == null
            || !currentFamily.getName().equals(family)) {
          currentFamily = new TaxonImpl(Type.family, currentOrder);
          currentFamily.setName(family);
          currentFamily = addToParentIfNotPresent(currentFamily, currentOrder);
          currentGenus = null;
        }

        String sciName = line[sciIndex];
        List<String> sciNameSplit = Splitter.on(' ').trimResults().omitEmptyStrings().splitToList(sciName);
        if (sciNameSplit.size() != 2) {
          System.err.println("UNEXPECTED SCI NAME: " + Arrays.asList(line));
          continue;
        }
        String genus = sciNameSplit.get(0); 
        if (genus.isEmpty()) {
          continue;
        }
        
        if (currentGenus == null
            || !currentGenus.getName().equals(genus)) {
          currentGenus = new TaxonImpl(Type.genus, currentFamily);
          currentGenus.setName(genus);
          currentGenus = addToParentIfNotPresent(currentGenus, currentFamily);
          currentSpecies = null;
        }

        String species = sciNameSplit.get(1);
        if (species.isEmpty()) {
          continue;
        }
        
        if (currentSpecies == null
            || !currentSpecies.getName().equals(species)) {
          currentSpecies = new SpeciesImpl(Type.species, currentGenus);
          currentSpecies.setName(species);
          String countries = line[countriesIndex];
          setAdditionalFields(currentSpecies, line);
          currentGenus.getContents().add(currentSpecies);
          currentSpecies.built();
          
          if (countries.equalsIgnoreCase("Domesticated")) {
            currentSpecies.setStatus(Status.DO);
          } else if (!countries.isEmpty()) {
            Collection<String> locationCodes = toLocationCodes(COUNTRIES_SPLITTER.splitToList(countries));
            checklistBuilder.addEntries(SightingTaxons.newSightingTaxon(
                currentSpecies.getId()), currentSpecies.getStatus(), locationCodes);
            if (locationCodes.size() <= 5) {
              currentSpecies.setRange(countries.replace("|", ", "));
            } else {
              // TODO: replace this with the logic used by the iNaturalist code
              Set<String> regionNames = new LinkedHashSet<>();
              for (String locationCode : locationCodes ) {
                // Skip multi-region codes, as they can give you bad info (e.g. Indonesia
                // might add Australasia);  assume that the other country codes will save you
                if (MULTI_REGION_LOCATION_CODES.contains(locationCode)) {
                  continue;
                }
                Location location = locationShortcuts.getLocationFromEBirdCode(locationCode);
                if (location == null) {
                  System.err.println("Could not backtrack to location from " + locationCode);
                } else {
                  Location region = Locations.getAncestorOfType(location, Location.Type.region);
                  if (region == null) {
                    System.err.println("No region parent of " + locationCode);
                  } else {
                    regionNames.add(region.getDisplayName());
                  }
                }
              }
              
              currentSpecies.setRange(Joiner.on(", ").join(regionNames));
            }
          }
          
          String explicitRange = line[rangeIndex];
          if (!explicitRange.isBlank()) {
            currentSpecies.setRange(explicitRange);
          }
        }
      }
    } catch (Exception e) {
      if (line != null) {
        Logger.getAnonymousLogger().log(Level.WARNING, "Failed on line " + Arrays.asList(line), e);
      }
      throw e;
    } finally {
      importLines.close();
    }
    try (FileWriter out = new FileWriter(new File(outFile))) {
      new ExtendedTaxonomyCsvExporter().writeExtendedTaxonomy(
          taxonomy, checklistBuilder.build(), out);
    }
  }

  private static final ImmutableMap<String, String> EXTRA_MAPPINGS = ImmutableMap.<String, String>builder()
      .put("Alaska", "US")
      .put("Andaman Islands", "IN")
      .put("Bonaire", "BQ-BO")
      .put("Cote d'Ivoire", "CI")
      .put("CuraÃ§ao", "CW")
      .put("Curacao", "CW")
      .put("Falkland Islands (Malvinas)", "FK")
      .put("Galapagos", "EC-W")
      .put("Hawai'i", "US-HI")
      .put("Kerguelen Islands", "TF")
      .put("Nicobar Islands", "IN")
      .put("Palestine", "PS")
      .put("Pitcairn", "PN")
      .put("Prince Edward Islands", "ZA-WC-EdwardMarion")
      .put("Republic of the Congo", "CG")
      .put("Saba", "BQ")
      .put("Saint Helena", "SH-SH")
      .put("Saint Martin", "MF")
      .put("Sao Tome and Principe", "ST")
      .put("SÃ£o TomÃ© and PrÃ­ncipe", "ST")
      .put("SÃ£o TomÃ© and Principe", "ST")
      .put("Sint Eustatius", "BQ")
      .put("South Georgia and the South Sandwich Islands", "GS")
      .put("United States Virgin Islands", "VI")      
      .put("Trinidad", "TT")
      .build();
  private Collection<String> toLocationCodes(List<String> countryNames) {
    Set<String> locationCodes = new LinkedHashSet<>();
    for (String countryName : countryNames) {
      if (countryName.equals("NA")) {
        // Used for "Humans" and one data deficient, unknown species in Jan. 2024
        continue;
      }
      countryName = countryName.replace("&", "and");
      countryName = countryName.replace("?", "");
      Location location = locationShortcuts.getCountryInexactMatch(countryName, null, null);
      if (location != null) {
        locationCodes.add(Locations.getLocationCode(location));
      } else if (location == null && EXTRA_MAPPINGS.containsKey(countryName)) {
        locationCodes.add(EXTRA_MAPPINGS.get(countryName));
      } else {
        System.err.println("Could not find country " + countryName);
      }
    }
    return locationCodes;
  }

  private TaxonImpl addToParentIfNotPresent(TaxonImpl child, TaxonImpl parent) {
    Taxon existingChild = parent.findByName(child.getName());
    if (existingChild != null) {
      return (TaxonImpl) existingChild;
    }
    
    parent.getContents().add(child);
    child.built();
    return child;
  }
  

  private static final Splitter COMMON_NAME_SPLITTER = Splitter.on(CharMatcher.anyOf("|,;")).trimResults().omitEmptyStrings();
  private static final Splitter SCI_NAME_SPLITTER = Splitter.on('|').trimResults().omitEmptyStrings();
  
  private String normalizeName(String name) {
    // Get rid of fancy apostrophes
    name = name.replace('\u2019', '\'');
    name = name.replace("Ã©", "é");
    name = name.replace("Ã£", "ã");
    name = name.replace("Ã£", "ã");
    name = name.replace("Ã§", "ç");
    name = name.replace("Ã¼", "ü");
    name = name.replace("Ã´", "ô");
    // And non-breaking spaces
    return name.replace('\u00a0', ' ');
  }
  
  private void setAdditionalFields(SpeciesImpl species, String[] line) {
    String commonName = line[commonNameIndex];
    if (!commonName.isEmpty()) {
      commonName = normalizeName(commonName);
      species.setCommonName(commonName);
    }
    
    String alternateCommonNames = line[alternateCommonIndex];
    if (!alternateCommonNames.isEmpty()) {
      List<String> alternateNames = new ArrayList<>();
      for (String alternateName : COMMON_NAME_SPLITTER.split(alternateCommonNames)) {
        alternateNames.add(normalizeName(alternateName));
      }
      species.setAlternateCommonNames(alternateNames);
    }

    if (alternateSciIndex != null) {
      String alternateSciNames = line[alternateSciIndex];
      if (!alternateSciNames.isEmpty()) {
        List<String> alternateNames = new ArrayList<>();
        for (String sciName : SCI_NAME_SPLITTER.split(alternateSciNames)) {
          alternateNames.add(sciName.replace(" subspecies ", " "));
        }
        species.setAlternateNames(alternateNames);
      }
    }
    
    String statusText = line[iucnRedListStatusIndex];
    if (!"".equals(statusText) && !"NA".equals(statusText)) {
      try {
        statusText = statusText.substring(0, Math.min(2, statusText.length()));
        Status status = Status.valueOf(statusText);
        if (status != Status.LC) {
          species.setStatus(status);
        }
      } catch (IllegalArgumentException e) {
        System.err.println("Status " + statusText + " not found on " + Arrays.asList(line));
      } 
    }
    
    String taxonomyNotes = line[taxonomyNotesIndex];
    if (!"".equals(taxonomyNotes) && !"NA".equals(taxonomyNotes)) {
      species.setTaxonomicInfo(taxonomyNotes);
    }
  }

  private Map<String, Integer> toIndexMap(String[] header) {
    Map<String, Integer> indexMap = Maps.newHashMapWithExpectedSize(header.length);
    for (int i = 0; i < header.length; i++) {
      String name = CharMatcher.javaLetter().retainFrom(header[i]).toLowerCase();
      indexMap.put(name, i);
    }
    return indexMap;
  }
  
 
}
