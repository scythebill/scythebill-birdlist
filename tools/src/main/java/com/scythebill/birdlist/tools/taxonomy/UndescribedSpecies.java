package com.scythebill.birdlist.tools.taxonomy;

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSet;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.SpeciesImpl;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonImpl;

class UndescribedSpecies {
  enum ClementsOrIoc { CLEMENTS, IOC };
  
  static class Undescribed {
    ImmutableSet<ClementsOrIoc> already;
    ImmutableMap<ClementsOrIoc, String> names;
    String range;
    
    Undescribed range(String range) {
      this.range = range;
      return this;
    }
  }
  
  private static Undescribed undescribed(String name, ClementsOrIoc... already) {
    Undescribed undescribed = new Undescribed();
    undescribed.already = ImmutableSet.copyOf(already);
    undescribed.names = ImmutableMap.of(
        ClementsOrIoc.CLEMENTS, name + " (undescribed form)",
        ClementsOrIoc.IOC, name + " (undescribed)");
    return undescribed;
  }
  
  private static Undescribed undescribed(String name, String iocName, ClementsOrIoc... already) {
    Undescribed undescribed = new Undescribed();
    undescribed.already = ImmutableSet.copyOf(already);
    undescribed.names = ImmutableMap.of(
        ClementsOrIoc.CLEMENTS, name + " (undescribed form)",
        ClementsOrIoc.IOC, iocName + " (undescribed form)");
    return undescribed;
  }

  private static final ImmutableMultimap<String, Undescribed> generaWithUndescribedSpecies = new ImmutableMultimap.Builder<String, Undescribed>()
      .put("Strix", undescribed("San Isidro Owl")
          .range("Andean foothills in Ecuador;  perhaps a hybrid"))
      .put("Ninox", undescribed("White-spotted Boobook")
          .range("Known only from Gunug Rorekatimbu, central Sulawesi"))
      .put("Herpsilochmus", undescribed("Inambari-Tambopata Antwren")
          .range("Andean foothills in southeastern Peru (Puno)"))
      .put("Herpsilochmus", undescribed("Loreto Antwren")
          .range("NE Peru (north of the Amazon, east of the Napo)"))
      // Use Clements Antbird taxonomy for these two (tho Cordillera Azul is now described)
      .put("Sciaphylax", undescribed("Aripuana Antbird", ClementsOrIoc.IOC)
          .range("central Amazonian Brazil, along the east bank of the Rio Madeira (between the Aripuanã and the Jiparaná rivers)"))
      // ... but put them in Myrmeciza for IOC
      .put("Myrmeciza", undescribed("Aripuana Antbird", ClementsOrIoc.CLEMENTS)
          .range("central Amazonian Brazil, along the east bank of the Rio Madeira (between the Aripuanã and the Jiparaná rivers)"))
      .put("Scytalopus", undescribed("Lambayeque Tapaculo")
          .range("western Andes of Peru (Lambayeque/Cajamarca border)"))
      .put("Deconychura", undescribed("Yungas Woodcreeper")
          .range("eastern slope of Andes in Ecuador and Peru"))
      .put("Heliobletus", undescribed("Bahia Treehunter")
          .range("Serras of southern Bahia, Brazil"))
      .put("Phacellodomus", undescribed("Mantaro Thornbird")
          .range("Andes of central Peru (Mantaro valley in Junín)"))
      .put("Synallaxis", undescribed("Amazonian Spinetail")
          .range("locally distributed along the southern fringe "
              + "of Amazonian Brazil, from Mato Grosso east to Maranhão"))
      .put("Synallaxis", undescribed("Mantaro Spinetail")
          .range("Andes of central Peru (Mantaro valley in Junin)"))
      .put("Stigmatura", undescribed("Orinoco Wagtail-Tyrant")
          .range("Venezuela (Orinoco delta)"))
      .put("Myiornis", undescribed("Maranhao-Piaui Pygmy-Tyrant", "Maranhao-Piaui Pygmy Tyrant")
          .range("eastern Brazil in eastern Pará, Maranhão, and Piauí"))
      .put("Microeca", undescribed("Bismarck Flyrobin")
          .range("New Britain and New Ireland (Bismarck Archipelago)"))
      .put("Pheugopedius", undescribed("Mantaro Wren")
          .range("Andes of central Peru (Mantaro drainage in Junín and probaby Huancavelica)"))
      .put("Erythrura", undescribed("Mount Mutis Parrotfinch")
          .range("Timor"))
      .put("Rallina", undescribed("Great Nicobar Crake")
          .range("Great Nicobar Island"))
      .put("Buteo", undescribed("Elgin Buzzard")
          .range("South Africa"))
      .put("Certhiaxis", undescribed("Tocantins Spinetail")
          .range("c Brazil"))
      .put("Pachyptila", undescribed("Gough Prion")
          .range("Gough Island; non-breeding range unknown"))
      .put("Macropygia", undescribed("Palawan Cuckoo-Dove", "Palawan Cuckoo Dove")
          .range("Philippines; vocally distinct Palawan population like M. tenuirostris may represent an undescribed taxon"))
      .put("Phylloscopus", undescribed("Banggai Leaf Warbler")
          .range("Banggai Islands, Indonesia"))
      .put("Phylloscopus", undescribed("Taliabu Leaf Warbler")
          .range("Taliabu Island, Indonesia"))
      .put("Zosterops", undescribed("Obi White-eye")
          .range("Obi Island, Indonesia"))
      .put("Cyornis", undescribed("Togian Jungle-Flycatcher", "Togian Jungle Flycatcher")
          .range("Togian Islands, Indonesia"))
      .put("Dicrurus", undescribed("Bacan Drongo")
          .range("Bacan Islands, Indonesia"))
      .put("Ptilinopous", undescribed("Manui Fruit-Dove", "Manui Fruit Dove")
          .range("Manui (Menui) Island, SE off Sulawesi, Indonesia"))
      .put("Pseudobulweria", undescribed("Lava Petrel")
          .range("Breeding sites unknown; reported at sea near Solomons Islands"))
      .put("Thamnophilus", undescribed("Inirida Antshrike")
          .range("Inirida, Guainía, Colombia"))
      .put("Grallaricula", undescribed("Cali Antpitta")
          .range("w Andes of Colombia"))
      .put("Phylloscopus", undescribed("Selayar Leaf Warbler").range("Selayar (s of Sulawesi)"))
      .put("Brachypteryx", undescribed("Mindanao Shortwing").range("submontane Mindanao, Philippines"))
      .put("Odontophorus", undescribed("Pasco Wood-Quail", "Pasco Wood Quail").range("east slope of Andes of central Peru (Pasco, and probably also Junín)"))
      .put("Megascops", undescribed("Puntarenas Screech-Owl", "Puntarenas Screech Owl").range("Lowlands of Puntarenas and e. San José, Costa Rica (possibly e. to Chiriquí, Panama)"))
      .put("Cisticola", undescribed("Teke Cisticola").range("Bateke Plateau, Congo-Gabon border)"))
      .put("Riparia", undescribed("Ethiopian Martin").range("Central Ethiopia"))
      .put("Anthreptes", undescribed("Mafwemiro Sunbird").range("Rubeho Mountains, Tanzania"))
      .put("Cacomantis", undescribed("Tanimbar Brush Cuckoo").range("Tanimbar Islands (Banda Sea)"))
      .put("Lampornis", undescribed("Azuero Mountain-gem").range("mountains of Azuero Peninsula (western Panama)"))
      .put("Basileuterus", undescribed("Azuero Warbler").range("mountains of Azuero Peninsula (western Panama)"))
      .put("Psilopogon", undescribed("Meratus Barbet").range("montane forest of Meratus Mountains, in southeastern Borneo"))
      .put("Grallaria", undescribed("Paisa Antpitta").range("Antioquia, Colombia"))
      .put("Scytalopus", undescribed("Turimiquire Tapaculo").range("ne Venezuela"))
      .put("Myzomela", undescribed("Obi Myzomela").range("Obi (northern Moluccas)"))
      .put("Spelaeornis", undescribed("Lisu Wren-Babbler").range("Mugaphi Peak (eastern Arunachal Pradesh)"))
      .put("Pterorhinus", undescribed("Meratus Laughingthrush").range("montane forest of Meratus Mountains, in southeastern Borneo"))
      .put("Dicaeum", undescribed("Meratus Flowerpecker").range("montane forest of Meratus Mountains, in southeastern Borneo"))
      .put("Spinus", undescribed("Monte Desert Siskin").range("w Argentina"))
      .build();

  static void maybeAddUndescribedForms(TaxonImpl currentGenus, ClementsOrIoc clementsOrIoc) {
    ImmutableCollection<Undescribed> collection = generaWithUndescribedSpecies.get(currentGenus.getName());
    int count = 1;
    for (Undescribed undescribed : collection) {
      if (!undescribed.already.contains(clementsOrIoc)) {
        SpeciesImpl speciesImpl = new SpeciesImpl(Taxon.Type.species, currentGenus);
        speciesImpl.setCommonName(undescribed.names.get(clementsOrIoc));
        String name;
        if (collection.size() > 1) {
          name = String.format(clementsOrIoc == ClementsOrIoc.CLEMENTS
            ? "[undescribed form %s]" : "sp. nov. %s", count++);
        } else {
          name = clementsOrIoc == ClementsOrIoc.CLEMENTS
              ? "[undescribed form]" : "sp. nov.";
        }
        speciesImpl.setName(name);
        if (undescribed.range != null) {
          speciesImpl.setRange(undescribed.range);
        }
        speciesImpl.setTaxonomicInfo("Not yet formally described.");
        currentGenus.getContents().add(speciesImpl);
        speciesImpl.setStatus(Status.UN);
        speciesImpl.built();
      }
    }
  }
}
