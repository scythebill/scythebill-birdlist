/**
 *  * Copyright 2023 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy.inaturalist;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.google.common.collect.Streams;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.tools.taxonomy.inaturalist.TaxaApi.PlaceType;
import com.scythebill.birdlist.tools.taxonomy.inaturalist.TaxaApi.PlacesPlace;
import com.scythebill.birdlist.tools.taxonomy.inaturalist.TaxaApi.SearchTaxon;
import com.scythebill.birdlist.tools.util.LocationTools;

public class AugmentExtendedTaxonomy {

  public static void main(String[] args) throws Exception {
    new AugmentExtendedTaxonomy().augmentExtendedTaxonomyCsv(
        "/tmp/odonata.csv", "/tmp/odonata-augmented.csv");
  }

  private final CloseableHttpClient httpClient;
  private final TaxaApi api;
  private final LocationTools locationTools;
  int orderColumn = -1;
  int familyColumn = -1;
  int speciesColumn = -1;
  int subspeciesColumn = -1;
  int rangeColumn = -1;
  int commonColumn = -1;
  int locationCodesColumn = -1;
  
  AugmentExtendedTaxonomy() {
    httpClient = HttpClients.custom()
        .disableCookieManagement()
        .setMaxConnPerRoute(3)
        .setMaxConnTotal(20)
        .setConnectionManager(new PoolingHttpClientConnectionManager())
        .setDefaultRequestConfig(RequestConfig.custom().setSocketTimeout(60000).build())
        .build();
    api = new TaxaApi(httpClient, getApiCacheDirectory());
    locationTools = new LocationTools();

  }
  
  public void augmentExtendedTaxonomyCsv(String fileIn, String fileOut) throws Exception {
    ImportLines inLines = CsvImportLines.fromFile(new File(fileIn), Charsets.UTF_8);
    ExportLines outLines = CsvExportLines.toFile(new File(fileOut), Charsets.UTF_8);
    
    boolean augmentOrderAndFamilyNames = false;
    
    boolean wroteHeader = false;
    Map<String, Integer> headerMap = new HashMap<>();

    String currentGenus = "";

    int lineCount = 0;
    int genusColumn = -1;
    while (true) {
      String[] nextLine = inLines.nextLine();
      lineCount++;
      if (lineCount % 100 == 1) {
        System.out.println("At line #" + (lineCount - 1));
      }
      if (nextLine == null) {
        outLines.close();
        inLines.close();
        break;
      }
      if (nextLine.length < 3) {
        outLines.nextLine(nextLine);
        continue;
      }
      
      if (!wroteHeader) {
        outLines.nextLine(nextLine);
        for (int i = 0; i < nextLine.length; i++) {
          headerMap.put(nextLine[i], i);
        }
        if (!headerMap.containsKey("Order")) {
          throw new IllegalArgumentException("No Order column");
        }
        orderColumn = headerMap.get("Order");
        if (!headerMap.containsKey("Family")) {
          throw new IllegalArgumentException("No Family column");
        }
        familyColumn = headerMap.get("Family");
        if (!headerMap.containsKey("Genus")) {
          throw new IllegalArgumentException("No Genus column");
        }
        genusColumn = headerMap.get("Genus");
        if (!headerMap.containsKey("Species")) {
          throw new IllegalArgumentException("No Species column");
        }
        speciesColumn = headerMap.get("Species");
        if (!headerMap.containsKey("Subspecies")) {
          throw new IllegalArgumentException("No Subspecies column");
        }
        subspeciesColumn = headerMap.get("Subspecies");
        if (!headerMap.containsKey("Range")) {
          throw new IllegalArgumentException("No Range column");
        }
        rangeColumn = headerMap.get("Range");
        if (!headerMap.containsKey("Location Codes")) {
          throw new IllegalArgumentException("No Location Codes column");
        }
        locationCodesColumn = headerMap.get("Location Codes");
        if (!headerMap.containsKey("Common")) {
          throw new IllegalArgumentException("No Common column");
        }
        commonColumn = headerMap.get("Common");
        wroteHeader = true;
        continue;
      }
      
      if (!nextLine[genusColumn].isBlank()) {
        currentGenus = nextLine[genusColumn];
      }
      // No common name - find one for orders and families.
      if (augmentOrderAndFamilyNames) {
        if (nextLine[commonColumn].isBlank()) {
          if (!nextLine[orderColumn].isBlank()
             && nextLine[familyColumn].isBlank()) {
            getTaxonCommonName(nextLine[orderColumn]).ifPresent(
                s -> nextLine[commonColumn]= s);
          }
  
          if (!nextLine[familyColumn].isBlank()
              && nextLine[genusColumn].isBlank()) {
             getTaxonCommonName(nextLine[familyColumn]).ifPresent(
                 s -> nextLine[commonColumn]= s);
           }
        }
      }
      
      boolean isSpecies = !nextLine[speciesColumn].isBlank()
          && nextLine[subspeciesColumn].isBlank();
      if (isSpecies) {
//        ImmutableMap<String, String> locationCodes = toLocationCodeMap(nextLine);
//        if (!Sets.intersection(locationCodes.keySet(), TaxaApi.COUNTRIES_WITH_STATE_CHECKLISTS).isEmpty()) {
//          augmentSpeciesStates(nextLine, currentGenus, locationCodes);
//        }
        
//        if (!Sets.intersection(locationCodes.keySet(), TaxaApi.COUNTRIES_WITH_PROVINCE_CHECKLISTS).isEmpty()) {
//          augmentSpeciesProvinces(nextLine, currentGenus, locationCodes);
//        }
        if (nextLine[locationCodesColumn].isBlank()) {
          augmentSpeciesLocation(nextLine, currentGenus);
        }
//        augmentIntroducedRange(nextLine, currentGenus);
        
      }
      
      outLines.nextLine(nextLine);
    } 
  }

  private ImmutableMap<String, String> toLocationCodeMap(String[] nextLine) {
    ImmutableMap<String, String> locationCodes = Streams.stream(
        Splitter.on(',').omitEmptyStrings().split(nextLine[locationCodesColumn]))
        .collect(ImmutableMap.toImmutableMap(
            s -> {
            int indexOf = s.indexOf('(');
            return indexOf < 0 ? s : s.substring(0, indexOf);
          },
          s -> {
            int indexOf = s.indexOf('(');
            return indexOf < 0 ? "" : s.substring(indexOf + 1, s.lastIndexOf(')'));
          }));
    return locationCodes;
  }
  
  private void augmentSpeciesLocation(String[] nextLine, String currentGenus) throws IOException, InterruptedException {
    String sciName = currentGenus + " " + nextLine[speciesColumn];
    // Load both *all* places, *and* just the country places.  This should be unnecessary,
    // but for some taxa (Sympetrum nantouensis, for example), Taiwan is returned only for
    // the "all places" list, but not for the country places list.
    List<PlacesPlace> allPlaces = api.places(sciName, null, null);
    List<PlacesPlace> countryPlaces = api.places(sciName, null, PlaceType.country);
    allPlaces = ImmutableList.<PlacesPlace>builder().addAll(allPlaces).addAll(countryPlaces).build();
    
    if (!allPlaces.isEmpty()) {
      Set<String> placeCodes = api.toPlaceCodes(allPlaces);
      if (!Sets.intersection(placeCodes, TaxaApi.COUNTRIES_WITH_STATE_CHECKLISTS).isEmpty()) {
        var statePlaces = api.places(sciName, null, PlaceType.state);
        placeCodes = api.toPlaceCodes(
            ImmutableList.<PlacesPlace>builder().addAll(allPlaces).addAll(statePlaces).build());
      } else if (placeCodes.stream().anyMatch(
          s -> s.startsWith("US") || s.startsWith("AU-"))) {
        System.err.println("Missing a country code! " + nextLine[speciesColumn]);
      }
      Set<String> introducedPlaceCodes;
      if (placeCodes.size() > 1) {
        List<PlacesPlace> introducedPlaces = api.places(sciName, Checklist.Status.INTRODUCED,
            // TODO: this might need to be done both for countries and for states
            null);
        introducedPlaceCodes = api.toPlaceCodes(introducedPlaces);
      } else {
        introducedPlaceCodes = ImmutableSet.of();
      }
      
      CharMatcher hyphen = CharMatcher.is('-');
      List<String> countries = placeCodes.stream()
          .filter(s -> !introducedPlaceCodes.contains(s))
          .filter(hyphen::matchesNoneOf).toList();
      List<String> states = placeCodes.stream()
          .filter(s -> !introducedPlaceCodes.contains(s))
          .filter(hyphen::matchesAnyOf).toList();

      List<String> locationCodes = new ArrayList<>();
      for (String placeCode : placeCodes) {
        if (introducedPlaceCodes.contains(placeCode)) {
          locationCodes.add(placeCode + "(I)");
        } else {
          boolean isCountry = hyphen.matchesNoneOf(placeCode);
          boolean endemic = (isCountry && countries.size() == 1)
              || (!isCountry && states.size() == 1 && countries.size() == 1);
          locationCodes.add(endemic ? placeCode + "(E)" : placeCode);
        }
      }
      
      nextLine[locationCodesColumn] = Joiner.on(',').join(locationCodes);
      
      if (!locationCodes.isEmpty()) {
        nextLine[rangeColumn] = locationTools.getRange(placeCodes, introducedPlaceCodes);
      }
    }
  }

  private void augmentSpeciesStates(String[] nextLine, String currentGenus,
      ImmutableMap<String, String> locationCodesToStatus) throws IOException, InterruptedException {
    String sciName = currentGenus + " " + nextLine[speciesColumn];
    var statePlaces = api.places(sciName, null, PlaceType.state);
    if (statePlaces.isEmpty()) {
      return;
    }
    
    LinkedHashMap<String, String> newCodes = new LinkedHashMap<>(locationCodesToStatus);
    if (locationCodesToStatus.containsKey("US")) {
      ImmutableSet<String> usStates = api.getStates(statePlaces, "US");
      if (usStates.size() != 1) {
        for (String code : usStates) {
          newCodes.put(code, "");
        }
      } else {
        String code = usStates.iterator().next();
        if (code.equals("US-HI")) {
          newCodes.put("US-HI", locationCodesToStatus.get("US"));
          newCodes.remove("US");
        } else {
          newCodes.put(code, locationCodesToStatus.get("US"));
        }
      }
    }

    if (locationCodesToStatus.containsKey("AU")) {
      ImmutableSet<String> auStates = api.getStates(statePlaces, "AU");
      if (auStates.size() != 1) {
        for (String code : auStates) {
          newCodes.put(code, "");
        }
      } else {
        String code = auStates.iterator().next();
        newCodes.put(code, locationCodesToStatus.get("AU"));
      }
    }
    
    if (locationCodesToStatus.containsKey("EC")) {
      ImmutableSet<String> ecStates = api.getStates(statePlaces, "EC");
      if (ecStates.contains("EC-W") || ecStates.contains("EC-GA")) {
        newCodes.put("EC-W", locationCodesToStatus.get("EC"));
        if (ecStates.size() == 1) {
          newCodes.remove("EC");
        }
      }
    }
    
    System.err.printf("Location codes for %s was %s\n", sciName, nextLine[locationCodesColumn]);
    nextLine[locationCodesColumn] =
        newCodes.entrySet().stream().map(e -> {
          if (e.getValue().isBlank()) {
            return e.getKey();
          }
          return String.format("%s(%s)", e.getKey(), e.getValue());
        }).collect(Collectors.joining(","));
    System.err.printf("... is now %s\n", nextLine[locationCodesColumn]);
  }


  private void augmentSpeciesProvinces(String[] nextLine, String currentGenus,
      ImmutableMap<String, String> locationCodesToStatus) throws IOException, InterruptedException {
    String sciName = currentGenus + " " + nextLine[speciesColumn];
    List<PlacesPlace> provincePlaces = 
        ImmutableList.copyOf(Iterables.concat(
            api.places(sciName, null, PlaceType.province),
            api.places(sciName, null, PlaceType.territory)));
    if (provincePlaces.isEmpty()) {
      return;
    }
    
    LinkedHashMap<String, String> newCodes = new LinkedHashMap<>(locationCodesToStatus);
    if (locationCodesToStatus.containsKey("CA")) {
      ImmutableSet<String> caStates = api.getStates(provincePlaces, "CA");
      if (caStates.size() != 1) {
        for (String code : caStates) {
          newCodes.put(code, "");
        }
      } else {
        String code = caStates.iterator().next();
        newCodes.put(code, locationCodesToStatus.get("CA"));
      }
    }
        
    System.err.printf("Location codes for %s was %s\n", sciName, nextLine[locationCodesColumn]);
    nextLine[locationCodesColumn] =
        newCodes.entrySet().stream().map(e -> {
          if (e.getValue().isBlank()) {
            return e.getKey();
          }
          return String.format("%s(%s)", e.getKey(), e.getValue());
        }).collect(Collectors.joining(","));
    System.err.printf("... is now %s\n", nextLine[locationCodesColumn]);
  }
  
  private void augmentIntroducedRange(String[] nextLine, String currentGenus) throws IOException, InterruptedException {
    ImmutableMap<String, String> locationCodesToStatus = toLocationCodeMap(nextLine);
    String sciName = currentGenus + " " + nextLine[speciesColumn];
    List<PlacesPlace> introducedCountries = api.places(sciName, Status.INTRODUCED, PlaceType.country);
    if (introducedCountries.isEmpty()) {
      return;
    }
    
    Set<String> introducedPlaceCodes = api.toPlaceCodes(introducedCountries);
    LinkedHashMap<String, String> newCodes = new LinkedHashMap<>();
    for (var entry : locationCodesToStatus.entrySet()) {
      String code = entry.getKey();
      if (code.indexOf('-') == 2) {
        String country = code.substring(0, 2);
        if (introducedPlaceCodes.contains(country)) {
          newCodes.put(entry.getKey(), "I");
          continue;
        }
      }
      newCodes.put(entry.getKey(), entry.getValue());
    }
    for (String introducedPlaceCode : introducedPlaceCodes) {
      newCodes.put(introducedPlaceCode, "I");
    }
    
    System.err.printf("Location codes for %s was %s\n", sciName, nextLine[locationCodesColumn]);
    nextLine[locationCodesColumn] =
        newCodes.entrySet().stream().map(e -> {
          if (e.getValue().isBlank()) {
            return e.getKey();
          }
          return String.format("%s(%s)", e.getKey(), e.getValue());
        }).collect(Collectors.joining(","));
    System.err.printf("... is now %s\n", nextLine[locationCodesColumn]);
    
  }

  private Optional<String> getTaxonCommonName(String taxon) throws IOException, InterruptedException {
    List<SearchTaxon> results = api.searchTaxonByName(taxon);
    for (SearchTaxon result : results) {
      if (taxon.equals(result.name)) {
        if (!Strings.isNullOrEmpty(result.preferredCommonName)) {
          return Optional.of(result.preferredCommonName);
        }
        if (!Strings.isNullOrEmpty(result.englishCommonName)) {
          return Optional.of(result.englishCommonName);
        }
      }
    }
    
    return Optional.empty();
  }
  
  private File getApiCacheDirectory() {
    // Separate cache from the "main" cache (all different URLs, anyway)
    File file = new File("/Users/awiner/Developer/inaturalist/augmentcache");
    if (!file.exists()) {
      file.mkdirs();
    }
    return file;
  }
}
