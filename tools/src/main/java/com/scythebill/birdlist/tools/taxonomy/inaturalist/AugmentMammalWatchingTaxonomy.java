/**
 *  * Copyright 2024 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy.inaturalist;

import static java.util.Comparator.comparing;
import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.nullsFirst;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.text.similarity.LevenshteinDistance;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.google.common.collect.Streams;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.tools.taxonomy.inaturalist.TaxaApi.PlaceType;
import com.scythebill.birdlist.tools.taxonomy.inaturalist.TaxaApi.PlacesPlace;
import com.scythebill.birdlist.tools.taxonomy.inaturalist.TaxaApi.SearchTaxon;
import com.scythebill.birdlist.tools.util.LocationTools;

public class AugmentMammalWatchingTaxonomy {

  public static void main(String[] args) throws Exception {
    AugmentMammalWatchingTaxonomy augmenter = new AugmentMammalWatchingTaxonomy();
    augmenter.preParseExtendedTaxonomyCsv("/tmp/mammal-watching.csv");
    augmenter.augmentExtendedTaxonomyCsv(
        "/tmp/mammal-watching.csv", "/tmp/mammal-watching-augmented.csv");
  }

  private final CloseableHttpClient httpClient;
  private final TaxaApi api;
  private final LocationTools locationTools;
  int orderColumn = -1;
  int familyColumn = -1;
  int speciesColumn = -1;
  int subspeciesColumn = -1;
  int rangeColumn = -1;
  int commonColumn = -1;
  int locationCodesColumn = -1;
  private int statusColumn;
  private Map<String, DwcaTaxon> taxaMap;
  private Set<String> existingSpecies;
  private Integer accountIdColumn;
  
  AugmentMammalWatchingTaxonomy() throws IOException {
    httpClient = HttpClients.custom()
        .disableCookieManagement()
        .setMaxConnPerRoute(3)
        .setMaxConnTotal(20)
        .setConnectionManager(new PoolingHttpClientConnectionManager())
        .setDefaultRequestConfig(RequestConfig.custom().setSocketTimeout(60000).build())
        .build();
    api = new TaxaApi(httpClient, getApiCacheDirectory());
    locationTools = new LocationTools();
    List<DwcaTaxon> taxa = new DwcaParser(
        new File("/Users/awiner/Downloads/inaturalist-taxonomy.dwca")).parse(
            ImmutableSet.of("Mammalia"), Taxon.Type.classTaxon);
    taxaMap = organizeTaxa(taxa);
  }
  
  private Map<String, DwcaTaxon> organizeTaxa(List<DwcaTaxon> taxa) {
    
    taxa.sort(comparing(DwcaTaxon::getGenusName, nullsFirst(naturalOrder()))
        .thenComparing(comparing(DwcaTaxon::getSpeciesName, nullsFirst(naturalOrder())))
        .thenComparing(comparing(DwcaTaxon::getSubspeciesName, nullsFirst(naturalOrder()))));
    
    Map<String, DwcaTaxon> taxaMap = new LinkedHashMap<>();
    DwcaTaxon currentGenus = null;
    DwcaTaxon currentSpecies = null;
    for (DwcaTaxon taxon : taxa) {
      if (taxon.getGenusName() == null) {
        continue;
      }
      
      if (taxon.speciesName == null) {
        currentGenus = taxon;
        currentSpecies = null;
        currentGenus.children = new ArrayList<>();
      } else if (taxon.subspeciesName == null) {
        currentSpecies = taxon;
        currentSpecies.children = new ArrayList<>();
        currentGenus.children.add(taxon);
        taxon.parent = currentGenus;
        taxaMap.put(taxon.genusName + " " + taxon.speciesName, taxon);
      } else {
        if (currentSpecies == null) {
          System.err.printf("No species for %s %s %s\n", taxon.genusName, taxon.speciesName, taxon.subspeciesName);
        } else {
          currentSpecies.children.add(taxon);
          taxon.parent = currentSpecies;
          taxaMap.put(taxon.genusName + " " + taxon.speciesName + " " + taxon.subspeciesName, taxon);
        }
      }
     }

    return taxaMap;
  }

  public void preParseExtendedTaxonomyCsv(String fileIn) throws Exception {
    existingSpecies = new LinkedHashSet<>();
    try (ImportLines inLines = CsvImportLines.fromFile(new File(fileIn), Charsets.UTF_8)) {
      Map<String, Integer> headerMap = new HashMap<>();
      int genusColumn = -1;
      String currentGenus = "";
      while (true) {
        String[] nextInLine = inLines.nextLine();
        if (nextInLine == null) {
          break;
        }
        if (nextInLine.length < 3) {
          continue;
        }

        if (headerMap.isEmpty()) {
          for (int i = 0; i < nextInLine.length; i++) {
            headerMap.put(nextInLine[i], i);
          }
          
          if (!headerMap.containsKey("Genus")) {
            throw new IllegalArgumentException("No Genus column");
          }
          genusColumn = headerMap.get("Genus");
          if (!headerMap.containsKey("Species")) {
            throw new IllegalArgumentException("No Species column");
          }
          speciesColumn = headerMap.get("Species");
          continue;
        }

        if (!nextInLine[genusColumn].isBlank()) {
          currentGenus = nextInLine[genusColumn];
        }
        
        if (!nextInLine[speciesColumn].isBlank()) {
          existingSpecies.add(currentGenus + " " + nextInLine[speciesColumn]);
        }
      }
    }
  }

  public void augmentExtendedTaxonomyCsv(String fileIn, String fileOut) throws Exception {
    ImportLines inLines = CsvImportLines.fromFile(new File(fileIn), Charsets.UTF_8);
    ExportLines outLines = CsvExportLines.toFile(new File(fileOut), Charsets.UTF_8);
    
    boolean wroteHeader = false;
    Map<String, Integer> headerMap = new HashMap<>();

    String currentGenus = "";

    int lineCount = 0;
    int genusColumn = -1;
    int alternateCommonColumn = -1;
    int headerLength = 0;
    Set<String> preHeaderEntries = new HashSet<>();
    while (true) {
      String[] nextInLine = inLines.nextLine();
      lineCount++;
      if (lineCount % 100 == 1) {
        System.out.println("At line #" + (lineCount - 1));
      }
      if (nextInLine == null) {
        outLines.close();
        inLines.close();
        break;
      }
      if (nextInLine.length < 3) {
        outLines.nextLine(nextInLine);
        if (nextInLine.length == 2) {
          preHeaderEntries.add(nextInLine[0]);
        }
        continue;
      }
      
      if (!wroteHeader) {
        if (!preHeaderEntries.contains("Account URL Format")) {
          outLines.nextLine(new String[] {"Account URL Format", "https://www.inaturalist.org/taxa/{id}"});
        }
        if (!preHeaderEntries.contains("Account Link Title")) {
          outLines.nextLine(new String[] {"Account Link Title", "iNaturalist"});
        }
        
        for (int i = 0; i < nextInLine.length; i++) {
          headerMap.put(nextInLine[i], i);
        }
        if (!headerMap.containsKey("Account ID")) {
          List<String> headerList = new ArrayList<>();
          headerList.addAll(Arrays.asList(nextInLine));
          headerList.add("Account ID");
          headerMap.put("Account ID", headerList.size() - 1);
          outLines.nextLine(headerList.toArray(new String[headerList.size()]));
          headerLength = headerList.size();
        } else {
          headerLength = nextInLine.length;
          outLines.nextLine(nextInLine);
        }
        if (!headerMap.containsKey("Order")) {
          throw new IllegalArgumentException("No Order column");
        }
        orderColumn = headerMap.get("Order");
        if (!headerMap.containsKey("Family")) {
          throw new IllegalArgumentException("No Family column");
        }
        familyColumn = headerMap.get("Family");
        if (!headerMap.containsKey("Genus")) {
          throw new IllegalArgumentException("No Genus column");
        }
        genusColumn = headerMap.get("Genus");
        if (!headerMap.containsKey("Species")) {
          throw new IllegalArgumentException("No Species column");
        }
        speciesColumn = headerMap.get("Species");
        if (!headerMap.containsKey("Subspecies")) {
          throw new IllegalArgumentException("No Subspecies column");
        }
        subspeciesColumn = headerMap.get("Subspecies");
        if (!headerMap.containsKey("Range")) {
          throw new IllegalArgumentException("No Range column");
        }
        rangeColumn = headerMap.get("Range");
        if (!headerMap.containsKey("Status")) {
          throw new IllegalArgumentException("No Status column");
        }
        statusColumn = headerMap.get("Status");
        if (!headerMap.containsKey("Location Codes")) {
          throw new IllegalArgumentException("No Location Codes column");
        }
        locationCodesColumn = headerMap.get("Location Codes");
        if (!headerMap.containsKey("Common")) {
          throw new IllegalArgumentException("No Common column");
        }
        commonColumn = headerMap.get("Common");
        if (!headerMap.containsKey("Alternate Common")) {
          throw new IllegalArgumentException("No Alternate Common column");
        }
        alternateCommonColumn = headerMap.get("Alternate Common");
        if (!headerMap.containsKey("Account ID")) {
          throw new IllegalArgumentException("Account ID");
        }
        accountIdColumn = headerMap.get("Account ID");
        wroteHeader = true;
        continue;
      }
      
      // Extend the line to fit the full header
      String[] nextLine;
      if (nextInLine.length == headerLength) {
        nextLine = nextInLine;
      } else {
        nextLine = new String[headerLength];
        Arrays.fill(nextLine, "");
        for (int i = 0; i < nextInLine.length; i++) {
          nextLine[i] = nextInLine[i];
        }
      }

      if (!nextLine[genusColumn].isBlank()) {
        currentGenus = nextLine[genusColumn];
      }
      // No common name - find one for orders and families.
      if (nextLine[commonColumn].isBlank()) {
        if (!nextLine[orderColumn].isBlank()
           && nextLine[familyColumn].isBlank()) {
          getTaxonCommonName(nextLine[orderColumn]).ifPresent(
              s -> nextLine[commonColumn]= s);
        }

        if (!nextLine[familyColumn].isBlank()
            && nextLine[genusColumn].isBlank()) {
           getTaxonCommonName(nextLine[familyColumn]).ifPresent(
               s -> nextLine[commonColumn]= s);
         }
      }
      
      boolean isSpecies = !nextLine[speciesColumn].isBlank()
          && nextLine[subspeciesColumn].isBlank();
      if (isSpecies) {
        String sciName = currentGenus + " " + nextLine[speciesColumn];
        DwcaTaxon dwcaTaxon = taxaMap.get(sciName);
        if (dwcaTaxon != null) {
          nextLine[accountIdColumn] = "" + dwcaTaxon.iNaturalistId;

          if (!Strings.isNullOrEmpty(dwcaTaxon.englishName)) {
            if (!normalizeName(dwcaTaxon.englishName).equals(normalizeName(nextLine[commonColumn]))
                && LevenshteinDistance.getDefaultInstance().apply(
                    normalizeName(dwcaTaxon.englishName), normalizeName(Strings.nullToEmpty(nextLine[commonColumn]))) > 2
                && !normalizeName(Strings.nullToEmpty(nextLine[alternateCommonColumn])).contains(normalizeName(dwcaTaxon.englishName))) {
              if (Strings.isNullOrEmpty(nextLine[alternateCommonColumn])) {
                nextLine[alternateCommonColumn] = dwcaTaxon.englishName;
              } else {
                nextLine[alternateCommonColumn] += "/" + dwcaTaxon.englishName; 
              }
            }
          }
        }
        ImmutableMap<String, String> locationCodes = toLocationCodeMap(nextLine);
        if (!Sets.intersection(locationCodes.keySet(), TaxaApi.COUNTRIES_WITH_STATE_CHECKLISTS).isEmpty()) {
          augmentSpeciesStates(nextLine, currentGenus, locationCodes);
          locationCodes = toLocationCodeMap(nextLine);
        }
      
        if (!Sets.intersection(locationCodes.keySet(), TaxaApi.COUNTRIES_WITH_PROVINCE_CHECKLISTS).isEmpty()) {
          augmentSpeciesProvinces(nextLine, currentGenus, locationCodes);
        }
        augmentIntroducedRange(nextLine, currentGenus);        
      }
      
      outLines.nextLine(nextLine);

      if (isSpecies) {
        String[] speciesLine = nextLine;
        String sciName = currentGenus + " " + nextLine[speciesColumn];
        DwcaTaxon speciesTaxon = taxaMap.get(sciName);
        if (speciesTaxon != null
            && speciesTaxon.children != null
            && !speciesTaxon.children.isEmpty()) {
          boolean anyAlreadyExistAsSpecies = false;
          for (DwcaTaxon child : speciesTaxon.children) {
            // Ignore nominate
            if (speciesTaxon.speciesName.equals(child.subspeciesName)) {
              continue;
            }
            String potentialSpeciesName = currentGenus + " " + child.subspeciesName;
            if (existingSpecies.contains(potentialSpeciesName)) {
              anyAlreadyExistAsSpecies = true;
              System.err.println("FOUND SUBSPECIES OF " + sciName);
              System.err.println("BUT " + potentialSpeciesName + " ALREADY EXISTS.");
              break;
            }
          }
          
          if (!anyAlreadyExistAsSpecies) {
            for (DwcaTaxon child : speciesTaxon.children) {
              String[] sspLine = new String[nextLine.length];
              sspLine[subspeciesColumn] = child.subspeciesName;
              if (!Strings.isNullOrEmpty(child.englishName)
                  && !child.englishName.equals(speciesLine[commonColumn])) {
                sspLine[commonColumn] = child.englishName;
              }
              sspLine[accountIdColumn] = "" + child.iNaturalistId;
              // In practice, none of the data has range or status, but, what the heck.
              if (!Strings.isNullOrEmpty(child.range)) {
                sspLine[rangeColumn] = child.range;
              }
              if (child.status != null) {
                sspLine[statusColumn] = child.status.toString();
              }
              outLines.nextLine(sspLine);
            }
          }
          
        }
      }
    } 
  }

  private static final CharMatcher NORMALIZED_NAME = CharMatcher.anyOf("-. ");
  
  private String normalizeName(String name) {
    return NORMALIZED_NAME.removeFrom(name).toLowerCase(Locale.ENGLISH);    
  }

  private ImmutableMap<String, String> toLocationCodeMap(String[] nextLine) {
    ImmutableMap<String, String> locationCodes = Streams.stream(
        Splitter.on(',').omitEmptyStrings().split(nextLine[locationCodesColumn]))
        .collect(ImmutableMap.toImmutableMap(
            s -> {
            int indexOf = s.indexOf('(');
            return indexOf < 0 ? s : s.substring(0, indexOf);
          },
          s -> {
            int indexOf = s.indexOf('(');
            return indexOf < 0 ? "" : s.substring(indexOf + 1, s.lastIndexOf(')'));
          }));
    return locationCodes;
  }

  private void augmentSpeciesStates(String[] nextLine, String currentGenus,
      ImmutableMap<String, String> locationCodesToStatus) throws IOException, InterruptedException {
    String sciName = currentGenus + " " + nextLine[speciesColumn];
    var statePlaces = api.places(sciName, null, PlaceType.state);
    if (statePlaces.isEmpty()) {
      return;
    }
    
    LinkedHashMap<String, String> newCodes = new LinkedHashMap<>(locationCodesToStatus);
    if (locationCodesToStatus.containsKey("US")) {
      ImmutableSet<String> usStates = api.getStates(statePlaces, "US");
      if (usStates.size() != 1) {
        for (String code : usStates) {
          newCodes.put(code, "");
        }
      } else {
        String code = usStates.iterator().next();
        if (code.equals("US-HI")) {
          newCodes.put("US-HI", locationCodesToStatus.get("US"));
          newCodes.remove("US");
        } else {
          newCodes.put(code, locationCodesToStatus.get("US"));
        }
      }
    }

    if (locationCodesToStatus.containsKey("AU")) {
      ImmutableSet<String> auStates = api.getStates(statePlaces, "AU");
      if (auStates.size() != 1) {
        for (String code : auStates) {
          newCodes.put(code, "");
        }
      } else {
        String code = auStates.iterator().next();
        newCodes.put(code, locationCodesToStatus.get("AU"));
      }
    }
    
    if (locationCodesToStatus.containsKey("EC")) {
      ImmutableSet<String> ecStates = api.getStates(statePlaces, "EC");
      if (ecStates.contains("EC-W") || ecStates.contains("EC-GA")) {
        newCodes.put("EC-W", locationCodesToStatus.get("EC"));
        if (ecStates.size() == 1) {
          newCodes.remove("EC");
        }
      }
    }
    
//    System.err.printf("Location codes for %s was %s\n", sciName, nextLine[locationCodesColumn]);
    nextLine[locationCodesColumn] =
        newCodes.entrySet().stream().map(e -> {
          if (e.getValue().isBlank()) {
            return e.getKey();
          }
          return String.format("%s(%s)", e.getKey(), e.getValue());
        }).collect(Collectors.joining(","));
//    System.err.printf("... is now %s\n", nextLine[locationCodesColumn]);
  }


  private void augmentSpeciesProvinces(String[] nextLine, String currentGenus,
      ImmutableMap<String, String> locationCodesToStatus) throws IOException, InterruptedException {
    String sciName = currentGenus + " " + nextLine[speciesColumn];
    List<PlacesPlace> provincePlaces = 
        ImmutableList.copyOf(Iterables.concat(
            api.places(sciName, null, PlaceType.province),
            api.places(sciName, null, PlaceType.territory)));
    if (provincePlaces.isEmpty()) {
      return;
    }
    
    LinkedHashMap<String, String> newCodes = new LinkedHashMap<>(locationCodesToStatus);
    if (locationCodesToStatus.containsKey("CA")) {
      ImmutableSet<String> caStates = api.getStates(provincePlaces, "CA");
      if (caStates.size() != 1) {
        for (String code : caStates) {
          newCodes.put(code, "");
        }
      } else {
        String code = caStates.iterator().next();
        newCodes.put(code, locationCodesToStatus.get("CA"));
      }
    }
        
//    System.err.printf("Location codes for %s was %s\n", sciName, nextLine[locationCodesColumn]);
    nextLine[locationCodesColumn] =
        newCodes.entrySet().stream().map(e -> {
          if (e.getValue().isBlank()) {
            return e.getKey();
          }
          return String.format("%s(%s)", e.getKey(), e.getValue());
        }).collect(Collectors.joining(","));
//    System.err.printf("... is now %s\n", nextLine[locationCodesColumn]);
  }
  
  private void augmentIntroducedRange(String[] nextLine, String currentGenus) throws IOException, InterruptedException {
    ImmutableMap<String, String> locationCodesToStatus = toLocationCodeMap(nextLine);
    String sciName = currentGenus + " " + nextLine[speciesColumn];
    List<PlacesPlace> introducedCountries = api.places(sciName, Status.INTRODUCED, PlaceType.country);
    if (introducedCountries.isEmpty()) {
      return;
    }
    
    Set<String> introducedPlaceCodes = api.toPlaceCodes(introducedCountries);
    LinkedHashMap<String, String> newCodes = new LinkedHashMap<>();
    for (var entry : locationCodesToStatus.entrySet()) {
      String code = entry.getKey();
      if (code.indexOf('-') == 2) {
        String country = code.substring(0, 2);
        if (introducedPlaceCodes.contains(country)) {
          newCodes.put(entry.getKey(), "I");
          continue;
        }
      }
      newCodes.put(entry.getKey(), entry.getValue());
    }
    for (String introducedPlaceCode : introducedPlaceCodes) {
      newCodes.put(introducedPlaceCode, "I");
    }
    
    nextLine[locationCodesColumn] =
        newCodes.entrySet().stream().map(e -> {
          if (e.getValue().isBlank()) {
            return e.getKey();
          }
          return String.format("%s(%s)", e.getKey(), e.getValue());
        }).collect(Collectors.joining(","));
  }

  private Optional<String> getTaxonCommonName(String taxon) throws IOException, InterruptedException {
    List<SearchTaxon> results = api.searchTaxonByName(taxon);
    for (SearchTaxon result : results) {
      if (taxon.equals(result.name)) {
        if (!Strings.isNullOrEmpty(result.preferredCommonName)) {
          return Optional.of(result.preferredCommonName);
        }
        if (!Strings.isNullOrEmpty(result.englishCommonName)) {
          return Optional.of(result.englishCommonName);
        }
      }
    }
    
    return Optional.empty();
  }
  
  private File getApiCacheDirectory() {
    // Separate cache from the "main" cache (all different URLs, anyway)
    File file = new File("/Users/awiner/Developer/inaturalist/augmentcache");
    if (!file.exists()) {
      file.mkdirs();
    }
    return file;
  }
}
