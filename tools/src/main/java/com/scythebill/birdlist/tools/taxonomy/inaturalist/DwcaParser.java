/*   
 * Copyright 2023 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy.inaturalist;

import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.google.common.util.concurrent.Uninterruptibles;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.tools.taxonomy.inaturalist.TaxaApi.ListedTaxon;
import com.scythebill.birdlist.tools.taxonomy.inaturalist.TaxaApi.PlaceType;
import com.scythebill.birdlist.tools.taxonomy.inaturalist.TaxaApi.PlacesPlace;
import com.scythebill.birdlist.tools.taxonomy.inaturalist.TaxaApi.SearchTaxon;
import com.scythebill.birdlist.tools.taxonomy.inaturalist.TaxaApi.TaxonDetails;
import com.scythebill.birdlist.tools.taxonomy.inaturalist.TaxaApi.TaxonSearchStrategy;
import com.scythebill.birdlist.tools.taxonomy.inaturalist.TaxaApi.WaitUntilTomorrowException;
import com.scythebill.birdlist.tools.util.LocationTools;
import com.scythebill.birdlist.ui.imports.LocationShortcuts;

/**
 * Parses files from https://www.inaturalist.org/taxa/inaturalist-taxonomy.dwca.zip
 */
public class DwcaParser {
  public static void main(String[] args) throws Exception {
    DwcaParser parser = new DwcaParser(
        new File("/Users/awiner/Downloads/inaturalist-taxonomy.dwca"));
    while (true) {
      try {
        doFish(parser);
      } catch (SocketTimeoutException e) {
        Uninterruptibles.sleepUninterruptibly(10, TimeUnit.SECONDS);
        System.err.println("Socked timed out");
        // ignore
        continue;
      } catch (WaitUntilTomorrowException e) {
        int hour = LocalTime.now().getHour();
        int hoursToWait = 24 - hour;
        System.err.printf("Too many requests in a day;  waiting %d hours\n", hoursToWait);
        Uninterruptibles.sleepUninterruptibly(hoursToWait, TimeUnit.HOURS);
        continue;
      }
      break;
    }
  }
  
  private static void doOdonates(DwcaParser parser) throws Exception {
//    List<DwcaTaxon> taxa = parser.parse(ImmutableSet.of("Odonata"), Taxon.Type.order);
    List<DwcaTaxon> taxa = parser.taxaFromApi(47792, "Odonata", Taxon.Type.subspecies, TaxonSearchStrategy.subordersAndFamilies);
   
    parser.augmentWithDetails(taxa);
    parser.augmentWithAllPlaces(taxa);
    
    new DwcaTaxaToExtendedTaxonomyCsv().writeTaxa(
        taxa,
        new File("/tmp/odonata.csv"),
        "WorldOdonata",
        "Odonates of the World");    
  }

  private static void doAmphibians(DwcaParser parser) throws Exception {
    List<DwcaTaxon> taxa = parser.taxaFromApi(20978, "Amphibia", Taxon.Type.subspecies,
        TaxonSearchStrategy.ordersAndFamilies);
    
    parser.augmentWithDetails(taxa);
    parser.augmentWithAllPlaces(taxa);
    
    new DwcaTaxaToExtendedTaxonomyCsv().writeTaxa(
        taxa,
        new File("/tmp/amphibia.csv"),
        "WorldAmphibians",
        "Amphibians of the World (August 2023)");    
  }

  private static void doFish(DwcaParser parser) throws Exception {
    List<DwcaTaxon> jawlessFish = parser.taxaFromApi(797045, "Agnatha", Taxon.Type.species,
        TaxonSearchStrategy.ordersAndFamilies);
    List<DwcaTaxon> rayFinnedFish = parser.taxaFromApi(47178, "Actinopterygii", Taxon.Type.species,
        TaxonSearchStrategy.ordersAndFamilies);
    List<DwcaTaxon> sharksAndRays = parser.taxaFromApi(47273, "Elasmobranchii", Taxon.Type.species,
        TaxonSearchStrategy.ordersAndFamilies);
    List<DwcaTaxon> lobeFinnedFish = parser.taxaFromApi(85497, "Sarcopterygii", Taxon.Type.species,
        TaxonSearchStrategy.ordersAndFamilies);
    List<DwcaTaxon> taxa = new ArrayList<>();
    taxa.addAll(jawlessFish);
    taxa.addAll(rayFinnedFish);
    taxa.addAll(sharksAndRays);
    taxa.addAll(lobeFinnedFish);
    parser.augmentWithDetails(taxa);
    parser.augmentWithAllPlaces(taxa);
    
    new DwcaTaxaToExtendedTaxonomyCsv().writeTaxa(
        taxa,
        new File("/tmp/fish.csv"),
        "WorldFish",
        "Fish of the World (February 2025)");    
  }

  private static void doReptiles(DwcaParser parser) throws Exception {
    List<DwcaTaxon> taxa = parser.taxaFromApi(26036, "Reptilia", Taxon.Type.subspecies,
        TaxonSearchStrategy.ordersAndFamilies);
    
    parser.augmentWithDetails(taxa);
    parser.augmentWithAllPlaces(taxa);
    
    new DwcaTaxaToExtendedTaxonomyCsv().writeTaxa(
        taxa,
        new File("/tmp/reptilia.csv"),
        "WorldReptiles",
        "Reptiles of the World");    
  }

  private static void doMacromoths(DwcaParser parser) throws Exception {
    List<DwcaTaxon> mimallonoidea = parser.taxaFromApi(119526, "Lepidoptera", Taxon.Type.subspecies, TaxonSearchStrategy.superfamiliesAndFamiles);
    List<DwcaTaxon> lasiocampoidea = parser.taxaFromApi(56583, "Lepidoptera", Taxon.Type.subspecies, TaxonSearchStrategy.superfamiliesAndFamiles);
    List<DwcaTaxon> bombycoidea = parser.taxaFromApi(47214, "Lepidoptera", Taxon.Type.subspecies, TaxonSearchStrategy.superfamiliesAndFamiles);
    List<DwcaTaxon> noctuoidea = parser.taxaFromApi(47607, "Lepidoptera", Taxon.Type.subspecies, TaxonSearchStrategy.superfamiliesAndFamiles);
    List<DwcaTaxon> drepanoidea = parser.taxaFromApi(84429, "Lepidoptera", Taxon.Type.subspecies, TaxonSearchStrategy.superfamiliesAndFamiles);
    List<DwcaTaxon> geometroidea = parser.taxaFromApi(49531, "Lepidoptera", Taxon.Type.subspecies, TaxonSearchStrategy.superfamiliesAndFamiles);
    List<DwcaTaxon> calliduloidea = parser.taxaFromApi(121882, "Lepidoptera", Taxon.Type.subspecies, TaxonSearchStrategy.superfamiliesAndFamiles);
    List<DwcaTaxon> taxa = new ArrayList<>();
    taxa.addAll(mimallonoidea);
    taxa.addAll(lasiocampoidea);
    taxa.addAll(bombycoidea);
    taxa.addAll(noctuoidea);
    taxa.addAll(drepanoidea);
    taxa.addAll(geometroidea);
    taxa.addAll(calliduloidea);
    
//    parser.augmentWithDetails(taxa);
//    parser.augmentWithPlaces(taxa);
    
    new DwcaTaxaToExtendedTaxonomyCsv().writeTaxa(
        taxa,
        new File("/tmp/macromoths.csv"),
        "WorldMacromoths",
        "Macromoths of the World");    
  }

  private static void doButterflies(DwcaParser parser) throws Exception {
//  List<DwcaTaxon> taxa = parser.parse(ImmutableSet.of("Hesperiidae", "Papilionidae",
//      "Pieridae", "Lycaenidae", "Riodinidae", "Nymphalidae"), Taxon.Type.family);
  List<DwcaTaxon> taxa = parser.taxaFromApi(47224, "Papilionoidea", Taxon.Type.subspecies, TaxonSearchStrategy.familiesAndSubfamilies);
  
  parser.augmentWithDetails(taxa);
  parser.augmentWithAllPlaces(taxa);
  
  new DwcaTaxaToExtendedTaxonomyCsv().writeTaxa(
      taxa,
      new File("/tmp/butterflies.csv"),
      "WorldButterflies",
      "Butterflies of the World");    
}

  private static void doSquirrels(DwcaParser parser) throws Exception {
    List<DwcaTaxon> taxa = parser.parse(ImmutableSet.of("Sciuridae"), Taxon.Type.family);
    
    parser.augmentWithDetails(taxa);
    parser.augmentWithPlaces(taxa);
    
    new DwcaTaxaToExtendedTaxonomyCsv().writeTaxa(
        taxa,
        new File("/tmp/squirrels.csv"),
        "WorldSquirrels",
        "Squirrels of the World");    
  }
  
  private final File directory;
  private final LocationShortcuts locationShortcuts;
  private final LocationTools locationTools;
  private final TaxaApi api;

  DwcaParser(File directory) {
    this.directory = directory;
    this.locationTools = new LocationTools();
    this.locationShortcuts =
        new LocationShortcuts(ReportSets.newLocations(), PredefinedLocations.loadAndParse());
    CloseableHttpClient httpClient = HttpClients.custom()
        .disableCookieManagement()
        .setMaxConnPerRoute(3)
        .setMaxConnTotal(20)
        .setConnectionManager(new PoolingHttpClientConnectionManager())
        .build();
    this.api = new TaxaApi(httpClient, getApiCacheDirectory());
  }
  
  public Map<String, Integer> namesToId() throws IOException {
    File taxaFile = new File(directory, "taxa.csv");
    ImportLines taxaLines = CsvImportLines.fromFile(taxaFile, Charsets.UTF_8);
    // Skip header
    taxaLines.nextLine();
    
    Map<String, Integer> namesToId = new HashMap<>();
    while (true) {
      String[] nextLine = taxaLines.nextLine();
      if (nextLine == null) {
        break;
      }
      
      if (nextLine.length < 6) {
        continue;
      }
      
      String taxonRank = nextLine[3];
      if (!taxonRank.equals("species")) {
        continue;
      }
      int id = Integer.parseInt(nextLine[0]);
      String sciName = nextLine[1];
      String commonName = nextLine[4];
      namesToId.put(sciName, id);
      namesToId.put(commonName, id);
    }
    return namesToId;
  }

  public List<DwcaTaxon> parse(ImmutableSet<String> taxaNames, Taxon.Type level) throws IOException {
    File taxaFile = new File(directory, "taxa.csv");
    ImportLines taxaLines = CsvImportLines.fromFile(taxaFile, Charsets.UTF_8);
    // Skip header
    taxaLines.nextLine();
    List<DwcaTaxon> taxa = new ArrayList<>();
    
    Map<Integer, DwcaTaxon> taxaById = new LinkedHashMap<>();
    ImmutableSet<String> taxonRanks = ImmutableSet.of("order", "family", "genus", "species", "subspecies");
    Set<String> orders = new LinkedHashSet<>();
    Set<String> names = new LinkedHashSet<>();
    while (true) {
      String[] nextLine = taxaLines.nextLine();
      if (nextLine == null) {
        break;
      }
      
      if (nextLine.length < 16) {
        continue;
      }
      
      String taxonRank = nextLine[14];
      if (!taxonRanks.contains(taxonRank)) {
        continue;
      }
      
      String className = nextLine[6];
      String orderName = Strings.emptyToNull(nextLine[7]);
      String familyName = Strings.emptyToNull(nextLine[8]);
      if (level == Taxon.Type.classTaxon) {
        if (!taxaNames.contains(className)) {
          continue;
        }
      } else if (level == Taxon.Type.order) {
        if (!taxaNames.contains(orderName)) {
          continue;
        }
      } else if (level == Taxon.Type.family) {
        if (!taxaNames.contains(familyName)) {
          continue;
        }
        
        if (!orders.contains(orderName)) {
          DwcaTaxon taxon = new DwcaTaxon();
          taxon.className = className;
          taxon.orderName = orderName;
          
          taxa.add(taxon);
          orders.add(orderName);
        }
      } else {
        throw new IllegalArgumentException("Only class/order/family supported");
      }
      
      DwcaTaxon taxon = new DwcaTaxon();
      taxon.className = className;
      taxon.orderName = orderName;
      taxon.familyName = familyName;
      taxon.genusName = Strings.emptyToNull(nextLine[9]);
      String sciName = nextLine[13];
      int space = sciName.indexOf(' ');
      if (space > 0) {
        // Always overwrite with the genus name - strangely different! - in the full species name
        taxon.genusName = sciName.substring(0, space);
      }
      taxon.speciesName = Strings.emptyToNull(nextLine[10]);
      if ("species".equals(taxonRank)) {
        if (CharMatcher.is(' ').countIn(sciName) > 1) {
          taxon.speciesName = sciName.substring(space + 1).replace(' ', '-');
          taxon.status = Species.Status.UN;
        }
      }
      
      taxon.subspeciesName = Strings.emptyToNull(nextLine[11]);
      taxon.iNaturalistId = Integer.parseInt(nextLine[0]);
      if (sciName.contains("Bematistes")) {
        System.err.println(sciName + ": " + taxon.iNaturalistId);
      }
      
      String fullName = taxon.toFullName();
      if (names.contains(fullName)) {
        System.err.printf("Duplicate entry in taxa, dropping: %s, #%s\n", fullName, taxon.iNaturalistId);
        continue;
      }
      
      names.add(fullName);
      
      
      Preconditions.checkState(!taxaById.containsKey(taxon.iNaturalistId),
          "Duplicate taxa: %s", taxon.iNaturalistId);
      taxa.add(taxon);
      taxaById.put(taxon.iNaturalistId, taxon);
    }
    
    File englishNamesFile = new File(directory, "VernacularNames-english.csv");
    ImportLines englishNamesLines = CsvImportLines.fromFile(englishNamesFile, Charsets.UTF_8);
    // Skip header
    englishNamesLines.nextLine();
    while (true) {
      String[] nextLine = englishNamesLines.nextLine();
      if (nextLine == null) {
        break;
      }
      
      if (nextLine.length < 2) {
        continue;
      }
      
      int iNaturalistId = Integer.parseInt(nextLine[0]);
      DwcaTaxon dwcaTaxon = taxaById.get(iNaturalistId);
      if (dwcaTaxon != null && dwcaTaxon.englishName == null) {
        dwcaTaxon.englishName = nextLine[1];
      }
    }
    
    return taxa;
  }

  public List<DwcaTaxon> taxaFromApi(int rootINaturalistTaxonId,
      String className, Taxon.Type depth, TaxonSearchStrategy taxonSearchStrategy) throws IOException, InterruptedException {
    List<SearchTaxon> searchTaxa = api.searchTaxon(rootINaturalistTaxonId, depth, taxonSearchStrategy);
    Map<Integer, SearchTaxon> byId = new LinkedHashMap<>(searchTaxa.size());
    searchTaxa.forEach(t -> byId.put(t.id, t));
    
    List<DwcaTaxon> taxa = new ArrayList<>(searchTaxa.size());
    Set<String> encounteredGenera = new LinkedHashSet<>();
    Set<String> addedFamilies = new LinkedHashSet<>();
    for (SearchTaxon searchTaxon : searchTaxa) {
      DwcaTaxon taxon = new DwcaTaxon();
      taxon.iNaturalistId = searchTaxon.id;
      taxon.englishName = searchTaxon.englishCommonName;
      taxon.className = className;
      
      boolean validTaxon = true;
      // The ancestor list starts at "life", then, Animalia, all the way down to the current taxon,
      // so this iteration order will fill in items in descending size
      for (Integer ancestorId : searchTaxon.ancestorIds) {
        SearchTaxon ancestor = byId.get(ancestorId);
        if (ancestor != null) {
          switch (ancestor.rank) {
            case "order":
            case "suborder":
            case "superfamily":
              taxon.orderName = ancestor.name;
              break;              
            case "family":
              if (taxonSearchStrategy == TaxonSearchStrategy.familiesAndSubfamilies) {
                taxon.orderName = ancestor.name;
              } else {
                if (taxon.orderName == null) {
                  System.err.println("DIDN'T FIND ORDER " + searchTaxon);
                  continue;
                }
                taxon.familyName = ancestor.name;
              }
              break;
            case "subfamily":
              if (taxon.orderName == null) {
                System.err.println("DIDN'T FIND ORDER " + searchTaxon);
                continue;
              }
              taxon.familyName = ancestor.name;
              break;
            case "species":
              if (taxon.orderName == null) {
                System.err.println("DIDN'T FIND ORDER " + searchTaxon);
                validTaxon = false;
                continue;
              }
              int space = ancestor.name.indexOf(' ');
              if (space < 0) {
                System.err.println("SPECIES WITHOUT GENUS? " + ancestor);
                validTaxon = false;
                continue;
              }
              if (taxon.familyName == null) {
                System.out.println("Dind't find family for " + searchTaxon.name + "; calling incertae sedis");
                taxon.familyName = taxon.orderName + " incertae sedis";
                if (!addedFamilies.contains(taxon.familyName)) { 
                  DwcaTaxon family = new DwcaTaxon();
                  family.iNaturalistId = -1;
                  family.className = taxon.className;
                  family.orderName = taxon.orderName;
                  family.familyName = taxon.familyName;
                  family.englishName = taxonSearchStrategy == TaxonSearchStrategy.familiesAndSubfamilies
                      ? family.orderName + " (no subfamily)"
                      : family.orderName + " (no family)";
                  addedFamilies.add(family.familyName);
                  taxa.add(family);
                }
              }
              taxon.genusName = ancestor.name.substring(0, space);
              if (!encounteredGenera.contains(taxon.genusName)) {
                // Add the genus - no ID known.
                DwcaTaxon genus = new DwcaTaxon();
                genus.iNaturalistId = -1;
                genus.className = taxon.className;
                genus.orderName = taxon.orderName;
                genus.familyName = taxon.familyName;
                genus.genusName = taxon.genusName;
                taxa.add(genus);
                encounteredGenera.add(taxon.genusName);
              }
              
              taxon.speciesName = ancestor.name.substring(space + 1);
              break;
            case "subspecies":
              taxon.subspeciesName = ancestor.name;
              break;
            default:
              System.err.println("UNEXPECTED ANCESTOR RANK " + ancestor);
              continue;
          }
        }
      }
      
      if (validTaxon) {
        taxa.add(taxon);
      }
    }
    return taxa;
  }
  
  private void augmentWithDetails(List<DwcaTaxon> taxa) throws InterruptedException, IOException {
    List<DwcaTaxon> buffer = new ArrayList<>();
    for (int i = 0; i < taxa.size(); i++) {
      DwcaTaxon taxon = taxa.get(i);
      if (taxon.speciesName != null && taxon.subspeciesName == null) {
        buffer.add(taxon);
        if (buffer.size() == 30) {
          augmentBufferWithDetails(api, buffer);
          
          System.out.printf("Augmenting at %s of %s\n", i, taxa.size());
          buffer.clear();
        }
      }
    }
    
    if (!buffer.isEmpty()) {
      augmentBufferWithDetails(api, buffer);
    }
  }

  private void augmentWithPlaces(List<DwcaTaxon> taxa) throws InterruptedException, IOException {
    for (int i = 0; i < taxa.size(); i++) {
      if (i % 30 == 0) {
        System.out.printf("Augment with places, %d of %d\n", i, taxa.size());
      }
      DwcaTaxon taxon = taxa.get(i);
      if (taxon.speciesName != null && taxon.subspeciesName == null) {
        // Load both *all* places, *and* just the country places.  This should be unnecessary,
        // but for some taxa (Sympetrum nantouensis, for example), Taiwan is returned only for
        // the "all places" list, but not for the country places list.
        List<PlacesPlace> allPlaces = api.places(taxon.iNaturalistId, null, null);
        List<PlacesPlace> countryPlaces = api.places(taxon.iNaturalistId, null,
            PlaceType.country);
        allPlaces = ImmutableList.<PlacesPlace>builder().addAll(allPlaces).addAll(countryPlaces).build();
        
        if (!allPlaces.isEmpty()) {
          taxon.locationCodes = new LinkedHashSet<>();
          taxon.incompleteChecklists = false;
          Set<String> placeCodes =  api.toPlaceCodes(allPlaces);
          List<PlacesPlace> statePlaces = new ArrayList<>();
          
          if (!Sets.intersection(placeCodes, TaxaApi.COUNTRIES_WITH_STATE_CHECKLISTS).isEmpty()) {
            statePlaces.addAll(api.places(taxon.iNaturalistId, null, PlaceType.state));
          }
          if (!Sets.intersection(placeCodes, TaxaApi.COUNTRIES_WITH_PROVINCE_CHECKLISTS).isEmpty()) {
            statePlaces.addAll(api.places(taxon.iNaturalistId, null, PlaceType.province));
            statePlaces.addAll(api.places(taxon.iNaturalistId, null, PlaceType.territory));
          }
          
          if (!statePlaces.isEmpty()) {
            placeCodes = api.toPlaceCodes(
                ImmutableList.<PlacesPlace>builder().addAll(allPlaces).addAll(statePlaces).build());
          }
          
          Set<String> introducedPlaceCodes;
          if (placeCodes.size() > 1) {
            List<PlacesPlace> introducedPlaces = api.places(taxon.iNaturalistId, Checklist.Status.INTRODUCED,
                // TODO: this might need to be done both for countries and for states
                null);
            introducedPlaceCodes = api.toPlaceCodes(introducedPlaces);
          } else {
            introducedPlaceCodes = ImmutableSet.of();
          }
          
          CharMatcher hyphen = CharMatcher.is('-');
          List<String> countries = placeCodes.stream()
              .filter(s -> !introducedPlaceCodes.contains(s))
              .filter(hyphen::matchesNoneOf).toList();
          List<String> states = placeCodes.stream()
              .filter(s -> !introducedPlaceCodes.contains(s))
              .filter(hyphen::matchesAnyOf).toList();

          for (String placeCode : placeCodes) {
            if (introducedPlaceCodes.contains(placeCode)) {
              taxon.locationCodes.add(placeCode + "(I)");
            } else {
              boolean isCountry = hyphen.matchesNoneOf(placeCode);
              boolean endemic = (isCountry && countries.size() == 1)
                  || (!isCountry && states.size() == 1 && countries.size() == 1);
              taxon.locationCodes.add(endemic ? placeCode + "(E)" : placeCode);
            }
          }
          
          if (!taxon.locationCodes.isEmpty()) {
            taxon.range = locationTools.getRange(placeCodes, introducedPlaceCodes);
          }
        }
      }
    }
  }
  
  private void augmentWithAllPlaces(List<DwcaTaxon> taxa) throws InterruptedException, IOException {
    for (int i = 0; i < taxa.size(); i++) {
      if (i % 30 == 0) {
        System.out.printf("Augment with places, %d of %d\n", i, taxa.size());
      }
      DwcaTaxon taxon = taxa.get(i);
      if (taxon.speciesName != null && taxon.subspeciesName == null) {
        List<PlacesPlace> allPlaces = api.allPlaces(taxon.iNaturalistId);        
        if (!allPlaces.isEmpty()) {
          taxon.locationCodes = new LinkedHashSet<>();
          taxon.incompleteChecklists = false;
          Set<String> placeCodes =  api.toPlaceCodes(allPlaces);          
          Set<String> introducedPlaceCodes;
          if (placeCodes.size() > 1) {
            List<PlacesPlace> introducedPlaces = api.places(taxon.iNaturalistId, Checklist.Status.INTRODUCED,
                // TODO: this might need to be done both for countries and for states
                null);
            introducedPlaceCodes = api.toPlaceCodes(introducedPlaces);
          } else {
            introducedPlaceCodes = ImmutableSet.of();
          }
          
          CharMatcher hyphen = CharMatcher.is('-');
          List<String> countries = placeCodes.stream()
              .filter(s -> !introducedPlaceCodes.contains(s))
              .filter(hyphen::matchesNoneOf).toList();
          List<String> states = placeCodes.stream()
              .filter(s -> !introducedPlaceCodes.contains(s))
              .filter(hyphen::matchesAnyOf).toList();

          for (String placeCode : placeCodes) {
            if (introducedPlaceCodes.contains(placeCode)) {
              taxon.locationCodes.add(placeCode + "(I)");
            } else {
              boolean isCountry = hyphen.matchesNoneOf(placeCode);
              boolean endemic = (isCountry && countries.size() == 1)
                  || (!isCountry && states.size() == 1 && countries.size() == 1);
              taxon.locationCodes.add(endemic ? placeCode + "(E)" : placeCode);
            }
          }
          
          if (!taxon.locationCodes.isEmpty()) {
            taxon.range = locationTools.getRange(placeCodes, introducedPlaceCodes);
          }
        }
      }
    }
  }

  private void augmentBufferWithDetails(
      TaxaApi api, List<DwcaTaxon> taxa) throws IOException, InterruptedException {
    Preconditions.checkArgument(taxa.size() <= 30);
    List<Integer> taxonIds =
        taxa.stream().map(t -> t.iNaturalistId).collect(ImmutableList.toImmutableList());
    List<TaxonDetails> taxonDetails = api.taxonDetails(taxonIds);
    
    Preconditions.checkState(
        taxa.size() == taxonDetails.size());
    for (int i = 0; i < taxa.size(); i++) {
      TaxonDetails detail = taxonDetails.get(i);
      DwcaTaxon taxon = taxa.get(i);
      if (!detail.isActive) {
        // For inactive taxa, swap in the ID.  (Would be better to fetch the current name, etc.,
        // but this at least makes place lookup work.)
        if (detail.currentSynonymousTaxonIds != null
            && detail.currentSynonymousTaxonIds.size() == 1) {
          taxon.iNaturalistId = detail.currentSynonymousTaxonIds.get(0);;
        } else {
          taxon.inactive = true;
        }
      }
      if (detail.conservationStatus != null) {
        try {
          taxon.status = detail.conservationStatus.getStatusEnum();
        } catch (IllegalArgumentException e) {
          System.err.println("Invalid conservation status in " + detail);
        }
      }
      
      if (detail.flagCounts != null && detail.flagCounts.unresolved > 0) {
        taxon.unresolvedFlags = true;
      }
      if (detail.listedTaxaCount > 0) {
        if (detail.listedTaxaCount > detail.listedTaxa.size()) {
          taxon.incompleteChecklists = true;
        }
        
        for (ListedTaxon listedTaxon : detail.listedTaxa) {
          if (listedTaxon.place != null
              && listedTaxon.place.adminLevel != null
              && listedTaxon.place.adminLevel == 0) {
            String countryName = listedTaxon.place.name;
            Location country = locationShortcuts.getCountryInexactMatch(countryName, null, null);
            if (country == null) {
              System.err.println("Unknown country " + listedTaxon.place);
            } else {
              if (taxon.locationCodes == null) {
                taxon.locationCodes = new LinkedHashSet<>();
              }
              taxon.locationCodes.add(Locations.getLocationCode(country));
            }
          }
        }
      }
    }    
  }

  private File getApiCacheDirectory() {
//    File file = new File(System.getProperty("java.io.tmpdir"), "inaturalistapicache");
    File file = new File("/Users/awiner/Developer/inaturalist/cache");
    if (!file.exists()) {
      file.mkdirs();
    }
    return file;
  }  
}
