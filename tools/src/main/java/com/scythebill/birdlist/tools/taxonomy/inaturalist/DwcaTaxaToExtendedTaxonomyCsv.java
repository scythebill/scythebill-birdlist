/*   
 * Copyright 2023 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy.inaturalist;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.ExportLines;

public class DwcaTaxaToExtendedTaxonomyCsv {
  private static final ImmutableList<String> HEADER_LINE = ImmutableList.of(
      "Order",
      "Family",
      "Genus",
      "Species",
      "Subspecies",
      "Common",
      "Alternate Common",
      "Alternate Scientific",
      "Range",
      "Status",
      "Notes",
      "Account ID");
  private static final ImmutableList<String> HEADER_LINE_WITH_CHECKLISTS = ImmutableList.<String>builder()
      .addAll(HEADER_LINE)
      .add("Location Codes")
      .build();
  private static final Joiner LOCATION_CODE_JOINER = Joiner.on(',');
  private static final Logger logger = Logger.getLogger(DwcaTaxaToExtendedTaxonomyCsv.class.getName());

  public void writeTaxa(Iterable<DwcaTaxon> taxa, File out,
      String taxonomyId, String taxonomyName) throws IOException {
    Set<DwcaTaxon> orders = new LinkedHashSet<>();
    Multimap<String, DwcaTaxon> familiesByOrder = ArrayListMultimap.create();
    Map<String, DwcaTaxon> familiesByName = new LinkedHashMap<>();
    Multimap<String, DwcaTaxon> generaByFamily = ArrayListMultimap.create();
    Multimap<String, String> genusNamesByFamily = ArrayListMultimap.create();
    Multimap<String, DwcaTaxon> speciesByGenus = ArrayListMultimap.create();
    Multimap<String, DwcaTaxon> subspeciesBySpecies = ArrayListMultimap.create();
    
    boolean hasChecklists = false;
    for (DwcaTaxon taxon : taxa) {
      if (taxon.inactive) {
        continue;
      }
      
      if (taxon.locationCodes != null) {
        hasChecklists = true;
      }

      if (taxon.subspeciesName != null) {
        String species = taxon.genusName + " " + taxon.speciesName;
        subspeciesBySpecies.put(species, taxon);
      } else if (taxon.speciesName != null) {
        speciesByGenus.put(taxon.genusName, taxon);
      } else if (taxon.genusName != null) {
        generaByFamily.put(taxon.familyName, taxon);
        genusNamesByFamily.put(taxon.familyName, taxon.genusName);
      } else if (taxon.familyName != null) {
        familiesByOrder.put(taxon.orderName, taxon);
        familiesByName.put(taxon.familyName, taxon);
      } else if (taxon.orderName != null) {
        orders.add(taxon);
      }
    }
    
    // Add missing genera:  iNat is weird in that it'll put species in a "genus" (via scientific name) that
    // doesn't match the genus.
    for (DwcaTaxon species : speciesByGenus.values()) {
      Preconditions.checkState(familiesByName.containsKey(species.familyName));
      DwcaTaxon family = familiesByName.get(species.familyName);
      
      if (!genusNamesByFamily.containsEntry(species.familyName, species.genusName)) {
        System.err.println("Adding missing genus " + species.genusName + " to family " + species.familyName);
        DwcaTaxon taxon = new DwcaTaxon();
        taxon.className = species.className;
        taxon.orderName = species.orderName;
        taxon.familyName = species.familyName;
        taxon.genusName = species.genusName;
        generaByFamily.put(taxon.familyName, taxon);
        genusNamesByFamily.put(taxon.familyName, taxon.genusName);
      }      
    }
    
    String[] header = hasChecklists
        ? HEADER_LINE_WITH_CHECKLISTS.toArray(new String[0])
        : HEADER_LINE.toArray(new String[0]);
    try (ExportLines export = CsvExportLines.toFile(out, Charsets.UTF_8)) {
      export.nextLine(new String[]{"Taxonomy ID", taxonomyId});
      export.nextLine(new String[]{"Name", taxonomyName});
      export.nextLine(new String[]{"Credits", "iNaturalist: https://www.inaturalist.org"});
      export.nextLine(new String[]{"Account URL Format", "https://www.inaturalist.org/taxa/{id}"});
      export.nextLine(new String[]{"Account Link Title", "iNaturalist"});
      
      export.nextLine(header);
      
      for (DwcaTaxon order : orders) {
        String[] line = new String[header.length];
        line[0] = order.orderName;
        line[5] = order.englishName;
        line[11] = Integer.toString(order.iNaturalistId);
        export.nextLine(line);
        
        for (DwcaTaxon family : familiesByOrder.get(order.orderName)) {
          line = new String[header.length];
          line[1] = family.familyName;
          line[5] = family.englishName;
          line[11] = Integer.toString(family.iNaturalistId);
          export.nextLine(line);
          
          for (DwcaTaxon genus : generaByFamily.get(family.familyName)) {
            for (DwcaTaxon species : speciesByGenus.get(genus.genusName)) {
              line = new String[header.length];
              line[2] = genus.genusName;
              line[3] = species.speciesName;
              line[5] = species.englishName;
              line[11] = Integer.toString(species.iNaturalistId);
              if (species.range != null) {
                line[8] = species.range;
              } else if (species.unresolvedFlags) {
                // Skip any taxa with no range but an unresolved flag. 
                continue;
              }
              
              if (species.status != null) {
                line[9] = species.status.name();
              }
              addChecklists(line, species);
              export.nextLine(line);
              
              String fullSpeciesName = line[2] + " " + line[3];
              for (DwcaTaxon subspecies : subspeciesBySpecies.get(fullSpeciesName)) {
                line = new String[header.length];
                line[2] = genus.genusName;
                line[3] = species.speciesName;
                line[4] = subspecies.subspeciesName;
                line[5] = subspecies.englishName;
                line[11] = Integer.toString(subspecies.iNaturalistId);
                if (subspecies.range != null) {
                  line[8] = subspecies.range;
                }
                if (subspecies.status != null) {
                  line[9] = subspecies.status.name();
                }
                addChecklists(line, subspecies);
                export.nextLine(line);
              }
            }
          }
        }
      }
    }
  }

  private void addChecklists(String[] line, DwcaTaxon taxon) {
    if (taxon.incompleteChecklists) {
      System.err.println("Incomplete checklists for " + taxon);
    }
    if (taxon.locationCodes != null) {
      line[12] = LOCATION_CODE_JOINER.join(taxon.locationCodes);
    }
  }
}
