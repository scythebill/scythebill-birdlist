/*   
 * Copyright 2023 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy.inaturalist;

import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.scythebill.birdlist.model.taxa.Species.Status;

public class DwcaTaxon {
  public String className;
  public String orderName;
  public String familyName;
  public String genusName;
  public String speciesName;
  @Nullable  String subspeciesName;
  
  public String englishName;
  int iNaturalistId;
  public Status status;
  public boolean incompleteChecklists;
  public Set<String> locationCodes;
  public String range;
  public boolean inactive;
  public boolean unresolvedFlags;
  
  public List<DwcaTaxon> children;
  public DwcaTaxon parent;

  public String toFullName() {
    if (subspeciesName != null) {
      return String.format("%s %s %s", genusName, speciesName, subspeciesName);
    } else if (speciesName != null) {
      return String.format("%s %s", genusName, speciesName);
    } else if (genusName != null) {
      return genusName;
    } else if (familyName != null) {
      return familyName;
    } else if (orderName != null) {
      return orderName;
    } else {
      return className;
    }
  }
  
  public String getGenusName() {
    return genusName;
  }
  
  public String getSpeciesName() {
    return speciesName;
  }
  
  public String getSubspeciesName() {
    return subspeciesName;
  }
  
  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
  }
}

