/*   
 * Copyright 2023 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.taxonomy.inaturalist;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.google.common.hash.Hashing;
import com.google.common.io.CharStreams;
import com.google.common.io.Files;
import com.google.common.primitives.Ints;
import com.google.common.reflect.TypeToken;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.Taxon;

public class TaxaApi {
  public static class WaitUntilTomorrowException extends IllegalStateException {
    public WaitUntilTomorrowException(String message) {
      super(message);
    }
  }

  private static final RequestConfig REQUEST_CONFIG = RequestConfig.custom()
      .setConnectionRequestTimeout(5000).setConnectTimeout(10000).setSocketTimeout(10000).build();
  /**
   * Maximum number of iNaturalist requests allowed per-day, per
   * https://www.inaturalist.org/pages/api+recommended+practices as of Feb. 20 2023.
   */
  private static final int MAX_API_PER_DAY = 10000;
  private CloseableHttpClient httpClient;
  private Gson gson;
  private File cacheDirectory;
  // Intentionally global.  (Ignoring thread safety, though.)
  private static Instant lastRead;


  public static void main(String[] args) throws Exception {
    CloseableHttpClient httpClient = HttpClients.custom()
        .disableCookieManagement()
        .setMaxConnPerRoute(3)
        .setMaxConnTotal(20)
        .setConnectionManager(new PoolingHttpClientConnectionManager())
        .build();
    TaxaApi taxaApi = new TaxaApi(httpClient, new File("/Users/awiner/Developer/inaturalist/cache"));
//    List<SearchTaxon> results = taxaApi.searchTaxon(59166, Taxon.Type.subspecies, TaxonSearchStrategy.familiesAndSubfamilies);
//    try (FileWriter fw = new FileWriter("/tmp/results.txt")) {
//      fw.write(results.toString());
//    }
    
    List<PlacesPlace> allPlaces = taxaApi.allPlaces("Eutropis carinata");
    System.out.println(taxaApi.toPlaceCodes(allPlaces));
//    System.out.println(taxaApi.taxonDetails(ImmutableList.of(179990)));
  }

  public TaxaApi(CloseableHttpClient httpClient, File cacheDirectory) {
    this.httpClient = httpClient;
    this.gson = new GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
    if (cacheDirectory != null) {
      if (!cacheDirectory.exists()) {
        cacheDirectory.mkdirs();
      }
      Preconditions.checkArgument(cacheDirectory.isDirectory());
      this.cacheDirectory = cacheDirectory;
    }
  }
  
  enum TaxonSearchStrategy {
    ordersAndFamilies,
    familiesAndSubfamilies,
    subordersAndFamilies,
    superfamiliesAndFamiles
  }

  public List<SearchTaxon> searchTaxon(int rootINaturalistTaxonId,
      Taxon.Type depth, TaxonSearchStrategy taxonSearchStrategy) throws IOException, InterruptedException {
    String rank;
    switch (taxonSearchStrategy) {
      case ordersAndFamilies:
        rank = "order,family,species";
        break;
      case familiesAndSubfamilies:
        rank = "family,subfamily,species";
        break;
      case subordersAndFamilies:
        rank = "suborder,family,species";
        break;
      case superfamiliesAndFamiles:
        rank = "superfamily,family,species";
        break;
      default:
        throw new AssertionError("Unknown search strategy " + taxonSearchStrategy);
    } 
    if (depth == Taxon.Type.subspecies) {
      rank += ",subspecies";
    }

    List<SearchTaxon> taxa = new ArrayList<>();
    Integer idAbove = null;
    while (true) {
      URI uri = taxonSearchUri(rootINaturalistTaxonId, idAbove, rank);
      System.out.println("Loading " + uri);
      try (Reader reader = read(uri)) {
        TaxonSearchResult result = gson.fromJson(reader, TaxonSearchResult.class);
        taxa.addAll(result.results);
        if (result.results.isEmpty()) {
          break;
        }
        idAbove = Iterables.getLast(result.results).id;
      }
    }
    return taxa;
  }
  
  public List<SearchTaxon> searchTaxonByName(String name) throws IOException, InterruptedException {
    URI uri = URI.create(String.format(
        "https://api.inaturalist.org/v1/taxa?q=%s&order=desc&order_by=observations_count&is_active=yes", name));
    try (Reader reader = read(uri)) {
      TaxonSearchResult result = gson.fromJson(reader, TaxonSearchResult.class);
      return result.results;
    }
  }
  
  public List<TaxonDetails> taxonDetails(Collection<Integer> taxa) throws IOException, InterruptedException {
    URI uri = taxonDetailsUri(taxa);
    try (Reader reader = read(uri)) {
      try {
        TaxonDetailsResult result = gson.fromJson(reader, TaxonDetailsResult.class);
        return result.results;
      } catch (JsonSyntaxException e) {
        throw new JsonSyntaxException("Failed to read from " + uri + "; " + e.getMessage());
      }
    }
  }
  
  enum PlaceType {
    state,
    province,
    territory,
    country
  }
  
  public List<PlacesPlace> places(int taxonId, @Nullable Checklist.Status status,
      @Nullable PlaceType placeType) throws IOException, InterruptedException {
    URI uri = placesUri(taxonId, status, placeType);
    try (Reader reader = read(uri)) {
      Type listType = new TypeToken<ArrayList<PlacesPlace>>() {}.getType();
      try {
        return gson.fromJson(reader, listType);
      } catch (JsonSyntaxException e) {
        System.err.println("Failed to parse text from " + uri);
        System.err.println("See cache file " + cacheFile(uri));
        throw e;
      }
    }    
  }

  public List<PlacesPlace> places(String sciName, @Nullable Checklist.Status status,
      @Nullable PlaceType placeType) throws IOException, InterruptedException {
    URI uri = placesUri(sciName, status, placeType);
    try (Reader reader = read(uri)) {
      Type listType = new TypeToken<ArrayList<PlacesPlace>>() {}.getType();
      return gson.fromJson(reader, listType);
    }    
  }

  public List<PlacesPlace> allPlaces(String sciName) throws IOException, InterruptedException {
    List<PlacesPlace> places = new ArrayList<>();
    Type listType = new TypeToken<ArrayList<PlacesPlace>>() {}.getType();
    int page = 0;
    while (true) {
      URI uri = allPlacesUri(sciName, page);
      try (Reader reader = read(uri)) {
        List<PlacesPlace> pagePlaces = gson.fromJson(reader, listType);
        if (pagePlaces.isEmpty()) {
          return places;
        } else {
          places.addAll(pagePlaces);
        }
      }    
      
      page++;
    }
  }

  public List<PlacesPlace> allPlaces(int taxonId) throws IOException, InterruptedException {
    List<PlacesPlace> places = new ArrayList<>();
    Type listType = new TypeToken<ArrayList<PlacesPlace>>() {}.getType();
    int page = 0;
    while (true) {
      URI uri = allPlacesUri(taxonId, page);
      try (Reader reader = read(uri)) {
        List<PlacesPlace> pagePlaces = gson.fromJson(reader, listType);
        if (pagePlaces.isEmpty()) {
          return places;
        } else {
          places.addAll(pagePlaces);
        }
      }    
      
      page++;
    }
  }

  public Set<String> toPlaceCodes(Collection<PlacesPlace> places) {
    LinkedHashSet<String> placeCodes = new LinkedHashSet<>();
    for (PlacesPlace place : places) {
      if (place.getType() == Location.Type.country) {
        String placeCode = place.getCode();
        if (placeCode == null) {
          System.err.println("Null code on " + place);
          continue;
        }
        // Treat Jarvis Bay as New South Wales
        if (placeCode.equals("AU-JB")) {
          placeCode = "AU-NSW";
        }
        if (placeCode.equals("EC")) {
          Set<String> states = getStates(places, "EC");
          // If the *only* state is the Galapagos, then just add that (using both
          // the wrong code that iNaturalist uses today and the right code).
          // Otherwise add both the Galapagos and Ecuador if it occurs in both.
          // Otherwise, just Ecuador.
          if (states.contains("EC-W")) {
            placeCodes.add("EC-W");
            if (states.size() > 1) {
              placeCodes.add("EC");
            }
          } else {
            placeCodes.add("EC");
          }
        } else if (placeCode.equals("ES")) {
          Set<String> states = getStates(places, "ES");
          if (states.contains("ES-CN")) {
            placeCodes.add("ES-CN");
            if (states.size() > 1) {
              placeCodes.add("ES");
            }
          } else {
            placeCodes.add("ES");
          }
        } else if (placeCode.equals("CO")) {
          Set<String> states = getStates(places, "CO");
          if (states.contains("CO-SAP")) {
            placeCodes.add("CO-SAP");
            if (states.size() > 1) {
              placeCodes.add("CO");
            }
            
          } else {
            placeCodes.add("CO");
          }
        } else if (placeCode.equals("YE")) {
          // Socotra hack: if Socotra shows up, but the only state that shows up is YE-HD,
          // then consider it as only found in Socotra.  This is almost certainly wrong
          // for some taxa - something must be restricted to Socotra and the adjacent
          // Yemen mainland - but I suspect this is right more often than it's wrong, and
          // this way we'll at least get to generate a list of Socotra endemics.
          Set<String> states = getStates(places, "YE");
          if (states.contains("YE-HD-SOC")) {
            placeCodes.add("YE-HD-SOC");
            if (states.size() > 2 || !states.contains("YE-HD")) {
              placeCodes.add("YE");
            }
          } else {
            placeCodes.add("YE");
          }
        } else if (placeCode.equals("PT")) {
          Set<String> states = getStates(places, "PT");
          ImmutableSet<String> azoresAndMadeira = ImmutableSet.of("PT-20", "PT-30");
          Set<String> presentStates = Sets.intersection(states, azoresAndMadeira);
          if (presentStates.isEmpty()) {
            placeCodes.add("PT");
          } else {
            placeCodes.addAll(presentStates);
            if (states.size() > presentStates.size()) {
              placeCodes.add("PT");
            }
          }           
        } else if (COUNTRIES_WITH_STATE_CHECKLISTS.contains(placeCode)
            || COUNTRIES_WITH_PROVINCE_CHECKLISTS.contains(placeCode)) {
          ImmutableSet<String> states = getStates(places, placeCode);
          if (states.size() == 1 && states.contains("US-HI")) {
            placeCodes.add("US-HI");
          } else {
            placeCodes.add(placeCode);
            placeCodes.addAll(states);
          }
        } else {
          placeCodes.add(placeCode);
        }
      }
    }
    return placeCodes;
  }
  
  /**
   * Countries with state checklists.
   */
  public static final ImmutableSet<String> COUNTRIES_WITH_STATE_CHECKLISTS = ImmutableSet.of("US", "AU", "ID");

  /**
   * Countries with province checklists.
   */
  public static final ImmutableSet<String> COUNTRIES_WITH_PROVINCE_CHECKLISTS = ImmutableSet.of("CA");
  
  public ImmutableSet<String> getStates(Collection<PlacesPlace> places, String countryCode) {
    ImmutableSet.Builder<String> states = ImmutableSet.builder();
    String countryCodeStart = countryCode + "-";
    for (PlacesPlace place : places) {
      String placeCode = place.getCode();
      if (place.getType() == Location.Type.state
          && placeCode != null
          && placeCode.startsWith(countryCodeStart)) {
        states.add(placeCode);
      }
    }
    return states.build();
  }

  private URI placesUri(int taxonId, @Nullable Checklist.Status status,
      @Nullable PlaceType placeType) {
    String uri = String.format("https://www.inaturalist.org/places.json?taxon=%s&per_page=200", taxonId);
    if (status != null) {
      switch (status) {
        case NATIVE:
          uri += "&establishment_means=native";
          break;
        case INTRODUCED:
          uri += "&establishment_means=introduced";
          break;
        case ENDEMIC:
          uri += "&establishment_means=endemic";
          break;
        default:
          throw new IllegalArgumentException("Status " + status + " not supported");
      }
    }
    
    if (placeType != null) {
      switch (placeType) {
        case state:
          uri += "&place_type=state";
          break;
        case province:
          uri += "&place_type=province";
          break;
        case territory:
          uri += "&place_type=territory";
          break;
        case country:
          uri += "&place_type=country";
          break;
        default:
          throw new IllegalArgumentException("Place type " + placeType + " not supported");
      }
    }
    
    return URI.create(uri);
  }
  
  private URI placesUri(String sciName, @Nullable Checklist.Status status,
      @Nullable PlaceType placeType) {
    String uri = String.format("https://www.inaturalist.org/places.json?taxon=%s&per_page=200", sciName.replace(' ', '+'));
    if (status != null) {
      switch (status) {
        case NATIVE:
          uri += "&establishment_means=native";
          break;
        case INTRODUCED:
          uri += "&establishment_means=introduced";
          break;
        case ENDEMIC:
          uri += "&establishment_means=endemic";
          break;
        default:
          throw new IllegalArgumentException("Status " + status + " not supported");
      }
    }
    
    if (placeType != null) {
      switch (placeType) {
        case state:
          uri += "&place_type=state";
          break;
        case province:
          uri += "&place_type=province";
          break;
        case territory:
          uri += "&place_type=territory";
          break;
        case country:
          uri += "&place_type=country";
          break;
        default:
          throw new IllegalArgumentException("Place type " + placeType + " not supported");
      }
    }
    
    return URI.create(uri);
  }

  private URI allPlacesUri(String sciName, int page) {
    String uri = String.format("https://www.inaturalist.org/places.json?taxon=%s&per_page=200", sciName.replace(' ', '+'));
    if (page > 0) {
      // Pages are one-indexed in the API
      uri += "&page=" + (page + 1);
    }
    return URI.create(uri);
  }
  
  private URI allPlacesUri(int taxonId, int page) {
    String uri = String.format("https://www.inaturalist.org/places.json?taxon=%s&per_page=200", taxonId);
    if (page > 0) {
      // Pages are one-indexed in the API
      uri += "&page=" + (page + 1);
    }
    return URI.create(uri);
  }
  
  private URI taxonSearchUri(int rootINaturalistTaxonId, @Nullable Integer idAbove, String rank) {
    String uriString = String.format(
        "https://api.inaturalist.org/v1/taxa?is_active=true&all_names=false&per_page=200"
            + "&locale=en_US&order=asc&order_by=id"
            + "&taxon_id=%s&rank=%s", rootINaturalistTaxonId, rank);
    if (idAbove != null) {
      uriString += "&id_above=" + idAbove.intValue();
    }
    return URI.create(uriString);
  }
  
  private URI taxonDetailsUri(Collection<Integer> taxonIds) {
    Preconditions.checkArgument(taxonIds.size() <= 30);
    return URI.create(
        String.format("https://api.inaturalist.org/v1/taxa/%s",
            Joiner.on(',').join(taxonIds)));
  }

  static class ApiConservationStatus {
    private static final Map<String, Status> ODD_STATUS = ImmutableMap.<String, Status>builder()
        .put("NEAR THREATENED", Status.NT)
        .put("EXTINCT", Status.EX)
        .put("E", Status.EN)
        // NatureServer ranks.  These aren't really directly comparable to IUCN.
        .put("G5T3", Status.NT)
        .put("G5T2", Status.VU)
        .put("G5T1", Status.EN)
        .put("G3", Status.NT)
        .put("G2G3", Status.NT)
        .put("G1G3", Status.NT)
        .put("G1G2", Status.VU)
        .put("G1G2Q", Status.VU)
        .put("G2G4", Status.LC)
        .put("G3G4", Status.LC)
        .put("G2", Status.VU)
        .put("G1", Status.EN)
        .put("T3", Status.NT)
        .put("T2", Status.VU)
        .put("T1", Status.EN)
        .put("T", Status.EN)
        .put("SCHEDULE II", Status.VU)
        .put("A2D", Status.VU)
        .put("THREATENED - NATIONALLY ENDANGERED", Status.EN)
        .build();
    
    String status;
    Status getStatusEnum() {
      if (status == null) {
        return Status.LC;
      }
      
      status = status.toUpperCase();
      // Odd incorrect data
      if (ODD_STATUS.containsKey(status)) {
        return ODD_STATUS.get(status);
      }
      
      return Status.valueOf(status.toUpperCase(Locale.ENGLISH));
    }

    @Override
    public String toString() {
      return ReflectionToStringBuilder.toString(this, ToStringStyle.SIMPLE_STYLE);
    }
  }
  
  static class SearchTaxon {
    String name;
    int id;
    int rankLevel;
    String rank;
    String englishCommonName;
    String preferredCommonName;
    boolean extinct;
    ApiConservationStatus conservationStatus;
    List<Integer> ancestorIds;

    @Override
    public String toString() {
      return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
  
  static class TaxonSearchResult {
    int totalResults;
    int page;
    List<SearchTaxon> results;
    
    @Override
    public String toString() {
      return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
  }
  
  static class ListedTaxonPlace {
    String name;
    Integer adminLevel;

    @Override
    public String toString() {
      return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }
  }
  
  static class ListedTaxon {
    String establishmentMeans;
    ListedTaxonPlace place;

    @Override
    public String toString() {
      return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }
  }

  static class TaxonDetails {
    String name;
    int id;
    int rankLevel;
    boolean isActive = true;
    FlagCounts flagCounts;
    List<Integer> currentSynonymousTaxonIds;
    String preferredCommonName;
    ApiConservationStatus conservationStatus;
    int listedTaxaCount;
    List<ListedTaxon> listedTaxa;
    
    @Override
    public String toString() {
      return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
  }
  
  static class TaxonDetailsResult {
    int totalResults;
    List<TaxonDetails> results;

    @Override
    public String toString() {
      return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
  }
  
  static class FlagCounts {
    int resolved;
    int unresolved;
  }

  static class PlacesPlace {
    String name;
    String code;
    Integer adminLevel;
    
    Location.Type getType() {
      if (adminLevel == null) {
        if ("YE-HD-SOC".equals(getCode())) {
          return Location.Type.state;
        }
        return null;
      }
      
      switch (adminLevel.intValue()) {
        case 0:
          return Location.Type.country;
        case 10:
          return Location.Type.state;
        case -10:
          return Location.Type.region;
        default:
          return null;
      }
    }
    
    private static final ImmutableMap<String, String> INAT_CODE_TO_EBIRD_CODE =
        ImmutableMap.<String, String>builder()
        .put("AU-NS", "AU-NSW")
        .put("AU-VI", "AU-VIC")
        .put("AU-CT", "AU-ACT")
        .put("AU-QL", "AU-QLD")
        .put("AU-TS", "AU-TAS")
        .put("CA-NF", "CA-NL")
        .put("PT-AC", "PT-20")
        .put("PT-MA", "PT-30")
        .put("EC-GA", "EC-W")
        .put("CO-SA", "CO-SAP")
        .put("ID-AC","ID-SM")
        .put("ID-BB","ID-SM")
        .put("ID-BE","ID-SM")
        .put("ID-JA","ID-SM")
        .put("ID-LA","ID-SM")
        .put("ID-SU","ID-SM")
        .put("ID-RI","ID-SM")
        .put("ID-KR","ID-SM")
        .put("ID-SS","ID-SM")
        .put("ID-SB","ID-SM")
        // Super ugly: iNat uses "SL" as the code for Sumatera Selatan, which is supposed to
        // be ID-SS, whereas ID-SL is what eBird uses for Sulawesi!
        .put("ID-SL","ID-SM")
        .put("ID-BA","ID-NU")
        .put("ID-NT","ID-NU")
        .put("ID-NB","ID-NU")
        .put("ID-BT","ID-JW")
        .put("ID-JT","ID-JW")
        .put("ID-JI","ID-JW")
        .put("ID-JK","ID-JW")
        .put("ID-JB","ID-JW")
        .put("ID-JR","ID-JW")
        .put("ID-YO","ID-JW")
        .put("ID-KT","ID-KA")
        .put("ID-KI","ID-KA")
        .put("ID-KU","ID-KA")
        .put("ID-KS","ID-KA")
        .put("ID-KB","ID-KA")
        .put("ID-PT","ID-IJ")
        .put("ID-IB","ID-IJ")
        .put("ID-PE","ID-IJ")
        .put("ID-PA","ID-IJ")
        .put("ID-PS","ID-IJ")
        .put("ID-PB","ID-IJ")
        .put("ID-ST","ID-SL")
        .put("ID-SE","ID-SL")
        .put("ID-SW","ID-SL")
        .put("ID-GO","ID-SL")
        .put("ID-SA","ID-SL")
        .put("ID-SG","ID-SL")
        .put("ID-SN","ID-SL")
        .put("ID-SR","ID-SL")
        .put("ID-MU","ID-MA")
        .build();
    
    /** Codes for names that are missing codes in iNaturalist. */
    private static final ImmutableMap<String, String> CODES_FOR_NAMES = ImmutableMap.of(
        "Kosovo", "XK",
        "Clipperton Island", "CP",
        "Republic of Congo", "CG",
        "Socotra Archipelago", "YE-HD-SOC");

    public String getCode() {
      if (code == null) {
        return CODES_FOR_NAMES.get(name);
      }
      // Sardinia is given code "SR" in iNat, which is wrong (that's Suriname)
      if (name.equals("Sardinia")) {
        return null;
      }
      return INAT_CODE_TO_EBIRD_CODE.getOrDefault(code, code);
    }

    @Override
    public String toString() {
      return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }
  }
  
  private Reader read(URI uri) throws IOException, InterruptedException {
    File cacheFile = null;
    if (cacheDirectory != null) {
      cacheFile = cacheFile(uri);
      if (cacheFile.exists()) {
        try (Reader reader = new FileReader(cacheFile, Charsets.UTF_8)) {
          String content = CharStreams.toString(reader);
          if (content.startsWith("URL:")) {
            // Trim the first line from the content
            content = content.substring(content.indexOf('\n'));
          }
          return new StringReader(content);
        }
      }
    }
    
    while (true) {
      incrementApiQueryCount();
      HttpGet httpGet = new HttpGet(uri);
      httpGet.setConfig(REQUEST_CONFIG);
      try (CloseableHttpResponse get = httpClient.execute(httpGet)) {
        Instant now = Instant.now();
        // Limit to 0.5 QPS with a simple sleep.
        // The *documented* limit is 1 QPS, but I've found I get 429's at that
        // rate.  At any rate, at 10,000 requests per day, why bother pushing the QPS?
        // At 0.5 QPS, I exhaust that request limit in less than 6 hours.
        if (lastRead != null) {
          Duration duration = Duration.between(lastRead, now);
          if (duration.compareTo(Duration.ofSeconds(2)) < 0) {
            Thread.sleep(Duration.ofSeconds(2).minus(duration).toMillis() + 1);
          }
        }
        lastRead = now;
        if (get.getStatusLine().getStatusCode() == 429) {
          System.err.println("TOO MANY REQUESTS; sleeping for 1 minute");
          Thread.sleep(60000);
          continue;
        }
        HttpEntity entity = get.getEntity();
        String content = CharStreams.toString(new InputStreamReader(
          new BufferedInputStream(entity.getContent()), Charsets.UTF_8));
        if (cacheFile != null) {
          try (FileWriter writer = new FileWriter(cacheFile)) {
            writer.write("URL:");
            writer.write(uri.toASCIIString());
            writer.write('\n');
            writer.write(content);
          }
        }
        return new StringReader(content);
      }
    }
  }

  private File cacheFile(URI uri) {
    File cacheFile;
    String hash = Hashing.farmHashFingerprint64().hashUnencodedChars(uri.toString()).toString();
    cacheFile = new File(cacheDirectory, hash + ".cache");
    return cacheFile;
  }
  
  private File apiCountFile() {
    String today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    return new File(System.getProperty("java.io.tmpdir"),
        String.format("inaturalist-api-count-" + today + ".txt"));
  }
  
  private static boolean printedApiCountFile = false;
  
  /** Bumps up a local file each time to ensure we don't surpass maximum requests per day. */
  private void incrementApiQueryCount() throws IOException {
    File apiCountFile = apiCountFile();
    if (!apiCountFile.exists()) {
      Files.asCharSink(apiCountFile, Charsets.UTF_8).write("1");
      return;
    }
    
    String content = Files.asCharSource(apiCountFile, Charsets.UTF_8).read();
    Integer parsed = Ints.tryParse(content);
    if (parsed == null) {
      throw new IllegalStateException("Could not parse " + apiCountFile + " contents: "  + content);
    }
    
    if (parsed.intValue() >= MAX_API_PER_DAY) {
      throw new WaitUntilTomorrowException(
          String.format("Too many requests in a day (max %d).\nDelete file \"%s\" tomorrow and restart.",
              MAX_API_PER_DAY, apiCountFile));
    }
    
    Files.asCharSink(apiCountFile, Charsets.UTF_8).write(Integer.toString(parsed.intValue() + 1));
    if (!printedApiCountFile) {
      printedApiCountFile = true;
      System.out.println("iNaturalist API query counted at " + apiCountFile.getAbsolutePath());
    }
  }
}
