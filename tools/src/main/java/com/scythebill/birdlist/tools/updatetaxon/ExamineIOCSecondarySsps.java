package com.scythebill.birdlist.tools.updatetaxon;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;

public class ExamineIOCSecondarySsps {
  public static void main(String[] args) throws Exception {
    new ExamineIOCSecondarySsps(args).run();
  }

  @Parameter(names = "-originalIoc", required = true)
  private String originalIoc;

  @Parameter(names = "-originalClements", required = true)
  private String originalClements;

  @Parameter(names = "-newIoc", required = true)
  private String newIoc;

  @Parameter(names = "-newClements", required = true)
  private String newClements;

  @Parameter(names = "-clementsUpdateMappings", variableArity = true, required = true)
  private List<String> clementsUpdateMappings = new ArrayList<String>();

  private Taxonomy originalClementsT;

  private MappedTaxonomy originalIocT;

  private Taxonomy newClementsT;

  private MappedTaxonomy newIocT;

  private Map<String, SightingTaxon> oldToNewClements;

  ExamineIOCSecondarySsps(String[] args) {
    new JCommander(this, args);
  }

  private void run() throws Exception {
    originalClementsT = ToolTaxonUtils.getTaxonomy(originalClements);
    originalIocT = ToolTaxonUtils.getMappedTaxonomy(originalClementsT, originalIoc);
    
    newClementsT = ToolTaxonUtils.getTaxonomy(newClements);
    newIocT = ToolTaxonUtils.getMappedTaxonomy(newClementsT, newIoc);
    
    Map<String, SightingTaxon> mapping = loadMappings(new File(clementsUpdateMappings.get(0)));
    for (String extraMapping : Iterables.skip(clementsUpdateMappings, 1)) {
      Map<String, SightingTaxon> loadedExtraMapping = loadMappings(new File(extraMapping));
      mapping = joinMappings(mapping, loadedExtraMapping);
    }
    oldToNewClements = mapping;
    
    examineRecursively(originalIocT.getRoot());
  }

  private void examineRecursively(Taxon taxon) {
    if (taxon.getType() == Taxon.Type.subspecies) {
      SightingTaxon mapping = originalIocT.getMapping(taxon);
      if (mapping != null && mapping.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
        Taxon clementsTaxon = Preconditions.checkNotNull(originalClementsT.getTaxon(mapping.getId()));
        SightingTaxon newClementsId = Preconditions.checkNotNull(oldToNewClements.get(clementsTaxon.getId()));
        Resolved resolved = newClementsId.resolve(newIocT);
        Taxon foundSubspecies = null;
        for (Taxon iocTaxon : resolved.getTaxa()) {
          foundSubspecies = findSubspecies(
              TaxonUtils.getParentOfType(iocTaxon, Taxon.Type.species),
              mapping.getSubIdentifier());
          if (foundSubspecies != null) {
            break;
          }
        }
        
        if (foundSubspecies == null) {
          System.err.printf("No mapping for %s\n", TaxonUtils.getCommonName(taxon));
          System.err.printf("\"%s\",\"%s\",\"\"\n",
              mapping.getId(), mapping.getSubIdentifier());
        } else {
          SightingTaxon newMapping = newIocT.getMapping(foundSubspecies);
          if (newMapping == null) {
            System.err.println("Found the new subspecies BUT IT'S NOT MAPPED? "
                + TaxonUtils.getCommonName(taxon));
          } else if (newMapping.getType() == SightingTaxon.Type.SINGLE) {
            System.err.printf("Now a single mapping for %s!\n", TaxonUtils.getCommonName(taxon));
          }
          if (newClementsId.getType() == SightingTaxon.Type.SP) {
            System.err.printf("Mapping for %s went through a Sp!\n", TaxonUtils.getCommonName(taxon));
          } else {
//            System.out.printf("Preserved Mapping for %s\n", TaxonUtils.getCommonName(taxon));
          }
        }
      }
    } else {
      for (Taxon child : taxon.getContents()) {
        examineRecursively(child);
      }
    }
  }

  private Taxon findSubspecies(Taxon iocTaxon, String subIdentifier) {
    if (iocTaxon.getType() == Taxon.Type.subspecies) {
      if (subIdentifier.equals(iocTaxon.getName())) {
        return iocTaxon;
      }
    }
    for (Taxon child : iocTaxon.getContents()) {
      Taxon found = findSubspecies(child, subIdentifier);
      if (found != null) {
        return found;
      }
    }
    return null;
  }

  private final static Splitter MULTI_SPLITTER = Splitter.on(','); 

  private static Map<String, SightingTaxon> joinMappings(
      Map<String, SightingTaxon> first, Map<String, SightingTaxon> second) {
    Map<String, SightingTaxon> joined = Maps.newHashMap();
    for (String key : first.keySet()) {
      SightingTaxon firstMapping = first.get(key);
      SightingTaxon joinedMapping;
      if (firstMapping.getType() == SightingTaxon.Type.SINGLE) {
        SightingTaxon secondMapping = second.get(firstMapping.getId());
        if (secondMapping == null && "sspCarpo3davwal".equals(firstMapping.getId())) {
          // HACK: 10.2.0 changed the ID of ssp. waltoni of Pink-rumped Rosefinch
          // without a mapping rule!  Catch data in this state.
          secondMapping = second.get("sspCarpo3eoswal");
        }
        joinedMapping = Preconditions.checkNotNull(secondMapping);
      } else {
        Set<String> joinedIds = Sets.newHashSet();
        for (String firstId : firstMapping.getIds()) {
          joinedIds.addAll(second.get(firstId).getIds());
        }
        joinedMapping = SightingTaxons.newPossiblySpTaxon(joinedIds);
      }
      joined.put(key, joinedMapping);
    }
    
    return joined;
  }
  
  private static Map<String, SightingTaxon> loadMappings(File priorMapping) throws IOException {
    Map<String, SightingTaxon> oldTaxaToNewTaxa = Maps.newHashMap();
    ImportLines lines = CsvImportLines.fromFile(priorMapping, Charsets.UTF_8);
    
    // Load the mappings of old taxa to new taxa
    try { 
      String[] nextLine;
      while ((nextLine = lines.nextLine()) != null) {
        if ("MULT".equals(nextLine[0])) {
          oldTaxaToNewTaxa.put(nextLine[1],
              SightingTaxons.newSpTaxon(MULTI_SPLITTER.split(nextLine[2])));
        } else if (!"".equals(nextLine[1])) {
          oldTaxaToNewTaxa.put(nextLine[1], SightingTaxons.newSightingTaxon(nextLine[2]));
        }
      }
    } finally {
      lines.close();
    }
    
    return oldTaxaToNewTaxa;
  }
}
