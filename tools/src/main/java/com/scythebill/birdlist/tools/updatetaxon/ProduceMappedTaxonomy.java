package com.scythebill.birdlist.tools.updatetaxon;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Queues;
import com.google.common.collect.Sets;
import com.opencsv.CSVReader;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy.MappingBuilder;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.SpeciesImpl;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.TaxonImpl;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.TaxonComparator;
import com.scythebill.birdlist.model.xml.XmlTaxonExport;

/**
 * Produces a mapped taxonomy.
 * <p>
 * Examples to consider:
 * 
 * Scarlet-rumped Cacique:
 * - In Clements, three groups
 * - In IOC, two species;  two of the groups map to subspecies of one of those
 * So mapping needs to be:
 * - Each group maps exactly to a subspecies
 * - No automatic mapping for species, so instead create a mapping by mapping
 *  each of the contents - which gives you two subspecies and a species - then
 *  raising all of those to the highest common level - which gives you two species -
 *  then writing that out as a Sp. mapping among two species
 *  
 * Caspian Gull:
 * - In Clements, three groups, Caspian, Steppe, Mongolian.
 * - In IOC:
 *      Caspian -> Caspian Gull
 *      Steppe -> barabensis Lesser Black-backed Gull!
 *      Mongolian -> mongolicus Vega Gull!
 * So each group maps one-to-one to a species or subspecies, raise all those two
 * species, so it's a three-way Sp.!
 */
  public class ProduceMappedTaxonomy {
  private final static Logger logger = Logger.getLogger(ProduceMappedTaxonomy.class.getName());
  private Taxonomy primaryT;
  private Taxonomy secondaryT;
  private Multimap<String, String> secondaryToPrimary;
  /** Map from secondary taxa to primary taxa.  Must be one-to-one. (Is that true?) */
  private BiMap<String, SightingTaxon> finalMappings = HashBiMap.create();
  private Multimap<String, String> primaryToSecondary;
  private Map<String, String> hardcoded;

  public static void main(String[] args) throws Exception {
    System.out.println("LOADING PRIMARY");
    Taxonomy primaryT = TaxonomyComparer.getTaxonomy(args[0]);
    System.out.println("LOADING SECONDARY");
    Taxonomy secondaryT = TaxonomyComparer.getTaxonomy(args[1]);
    Multimap<String, String> secondaryToPrimary = loadMapping(args[2]);
    
    HashMap<String, String> hardcoded;
    if (args.length > 3) {
      hardcoded = parseHardcodedMappings(args[3]);
    } else {
      hardcoded = new HashMap<String, String>();
    }

    new ProduceMappedTaxonomy(primaryT, secondaryT, secondaryToPrimary, hardcoded)
        .produceMappings();
  }
  
  public void produceMappings() throws Exception {
    String taxonToExamine = "spTuracman";
    logger.warning(String.format("Taxon %s maps to %s",
        taxonToExamine, secondaryToPrimary.get(taxonToExamine)));
    
    System.out.println("PASS 1 (SIMPLE),  START: " + secondaryToPrimary.keySet().size());
    Queue<String> toBeMapped = pass1SingleTaxa();
    
    if (toBeMapped.contains(taxonToExamine)) {
      logger.warning(String.format("Taxon %s still present after pass %s", taxonToExamine, 1));
    }
    
    trimBadGroupMappings(primaryT.getRoot());
    
    System.out.println("PASS 2 (GROUP + SSP.s), START: " + toBeMapped.size());    
    toBeMapped = pass2GroupWithSubspecies(toBeMapped);
    if (toBeMapped.contains(taxonToExamine)) {
      logger.warning(String.format("Taxon %s still present after pass %s", taxonToExamine, 2));
    }

    System.out.println("PASS 3 (SP., one Group, + SSPs), START: " + toBeMapped.size());
    toBeMapped = pass3SpeciesAndOneGroup(toBeMapped);
    if (toBeMapped.contains(taxonToExamine)) {
      logger.warning(String.format("Taxon %s still present after pass %s", taxonToExamine, 3));
    }
        
    System.out.println("PASS 4, START (SP. and SSPs): " + toBeMapped.size());
    toBeMapped = pass4SpeciesAndSubspecies(toBeMapped);
    if (toBeMapped.contains(taxonToExamine)) {
      logger.warning(String.format("Taxon %s still present after pass %s", taxonToExamine, 4));
    }

    System.out.println("PASS 5, START (SP. and multiple Groups): " + toBeMapped.size());
    toBeMapped = pass5SpeciesAndMultipleGroups(toBeMapped);
    if (toBeMapped.contains(taxonToExamine)) {
      logger.warning(String.format("Taxon %s still present after pass %s", taxonToExamine, 5));
    }

    System.out.println("PASS 6, START (MULTIPLE SP.): " + toBeMapped.size());
    toBeMapped = pass6MultipleSpecies(toBeMapped);
    if (toBeMapped.contains(taxonToExamine)) {
      logger.warning(String.format("Taxon %s still present after pass %s", taxonToExamine, 6));
    }

    System.out.println("PASS 7 (SP., multiple Groups, + SSPs), START: " + toBeMapped.size());
    toBeMapped = pass7SpeciesAndMultipleGroups(toBeMapped);
    if (toBeMapped.contains(taxonToExamine)) {
      logger.warning(String.format("Taxon %s still present after pass %s", taxonToExamine, 7));
    }

    System.out.println("PASS 8 (multiple Groups, no sp's), START: " + toBeMapped.size());
    toBeMapped = pass8MultipleGroups(toBeMapped);
    if (toBeMapped.contains(taxonToExamine)) {
      logger.warning(String.format("Taxon %s still present after pass %s", taxonToExamine, 8));
    }
    
    System.out.println("PASS 9 (species and subspecies outside), START: " + toBeMapped.size());
    toBeMapped = pass9SpeciesAndSubspeciesNotInParent(toBeMapped);
    if (toBeMapped.contains(taxonToExamine)) {
      logger.warning(String.format("Taxon %s still present after pass %s", taxonToExamine, 9));
    }

    System.out.println("PASS 10 (multiple groups), START: " + toBeMapped.size());
    toBeMapped = pass10MultipleGroupsInDifferentSpecies(toBeMapped);
    if (toBeMapped.contains(taxonToExamine)) {
      logger.warning(String.format("Taxon %s still present after pass %s", taxonToExamine, 10));
    }

    // A final pass: trust the mapping file!
    System.out.println("PASS 11 (trust whatever's left), START: " + toBeMapped.size());
    while (!toBeMapped.isEmpty()) {
      String first = toBeMapped.remove();
      Collection<String> taxa = secondaryToPrimary.get(first);
      finalMappings.put(first, SightingTaxons.newPossiblySpTaxon(taxa));
      System.out.println(first + " maps to " + taxa);
    }
    
    performCleanup(primaryT.getRoot());
    
    MappedTaxonomy taxonomy = new MappedTaxonomy(primaryT, secondaryT.getId(), secondaryT.getName());
    Taxon root = secondaryT.getRoot();
    TaxonImpl clonedRoot = new TaxonImpl(root.getType(), taxonomy);
    clonedRoot.setCommonName(root.getCommonName());
    clonedRoot.setName(root.getName());
    clonedRoot.built();
    taxonomy.setRoot(clonedRoot);
    
    MappingBuilder mappingBuilder = new MappingBuilder();
    cloneChildrenAndBuildFinalMappings(taxonomy, mappingBuilder, secondaryT.getRoot(), clonedRoot);
    taxonomy.setMappings(mappingBuilder);
    
    XmlTaxonExport export = new XmlTaxonExport();
    
    Writer out = new OutputStreamWriter(
            new FileOutputStream("/tmp/mapped-taxon.xml"),
            Charsets.UTF_8);
    export.export(out, taxonomy, "UTF-8");
    out.close();
    
    testAllMappingsFromPrimary(primaryT.getRoot(), taxonomy);
    testAllMappingsFromSecondary(taxonomy.getRoot(), taxonomy);
  }

  /**
   * Problem:  Collared Aracari (Stripe-billed) has two subspecies, both of
   * which are separately split.  Because that group therefore can't uniquely be mapped
   * to a species by TaxonomyComparer, it maps it to the species Collared Aracari, which
   * is really wrong.  So look for Groups where:
   * - The group maps to species X
   * - No subspecies map to children of X
   */
  private void trimBadGroupMappings(Taxon primaryTaxon) {
    if (primaryTaxon.getType() == Taxon.Type.group && !primaryTaxon.getContents().isEmpty()) {
      Collection<String> secondaryIds = primaryToSecondary.get(primaryTaxon.getId());
      if (secondaryIds.size() == 1) {
        String secondaryId = Iterables.getOnlyElement(secondaryIds);
        Taxon secondaryMapping = secondaryT.getTaxon(secondaryId);
        if (secondaryMapping.getType() == Taxon.Type.species) {
          boolean anyNormalMappings = false;
          for (Taxon subspecies : primaryTaxon.getContents()) {
            String secondaryTaxonIdForSubspecies = finalMappings.inverse().get(
                SightingTaxons.newSightingTaxon(subspecies.getId()));
            if (secondaryTaxonIdForSubspecies == null
                || TaxonUtils.isChildOfOrSame(secondaryMapping, secondaryTaxonIdForSubspecies)) {
              anyNormalMappings = true;
              break;
            }
          }
          if (!anyNormalMappings) {
            logger.info("Cleaning up bad group mapping for " + TaxonUtils.getCommonName(primaryTaxon));
            primaryToSecondary.remove(primaryTaxon.getId(), secondaryId);
            secondaryToPrimary.remove(secondaryId, primaryTaxon.getId());
          }
        }
      }
    }
    
    for (Taxon child : primaryTaxon.getContents()) {
      trimBadGroupMappings(child);
    }
  }
  
  private void performCleanup(Taxon primaryTaxon) {
    for (Taxon child : primaryTaxon.getContents()) {
      performCleanup(child);
    }
    
    // Don't bother with cleanup of extinct species
    if (primaryTaxon instanceof Species) {
      Species primarySpecies = (Species) primaryTaxon;
      if (primarySpecies.getStatus() == Status.EX) {
        return;
      }
    }
    
    // Look for species that are uniquely mapped to a subspecies.
    // Example: Clements Olivaceous Schiffornis maps to ssp. olivacea of
    // IOC Thrush-like Schiffornis, but is not present in the sp. mapping
    // for Thrush-like Schiffornis.  Add it there.
    // TODO: groups too?
    if (primaryTaxon.getType() == Taxon.Type.species) {
      String secondaryMapping =
          finalMappings.inverse().get(SightingTaxons.newSightingTaxon(primaryTaxon.getId()));
      if (secondaryMapping != null) {
        Taxon secondaryTaxon = secondaryT.getTaxon(secondaryMapping);
        if (secondaryTaxon.getType() == Taxon.Type.subspecies) {
          Taxon secondaryParent = secondaryTaxon.getParent();
          // Now, see what the secondary species maps to in the primary taxonomy
          SightingTaxon parentMapping = finalMappings.get(secondaryParent.getId());
          // If there's a mapping, and this taxon isn't already in that mapping...
          if (parentMapping != null
              && !parentMapping.getIds().contains(primaryTaxon.getId())) {
            // ... then see if any of the siblings of this species are in there
            boolean containsSibling = false;
            for (String possibleSiblingId : parentMapping.getIds()) {
              Taxon possibleSibling = primaryT.getTaxon(possibleSiblingId);
              if (possibleSibling.getParent() == primaryTaxon.getParent()) {
                containsSibling = true;
                break;
              }
            }
            
            if (containsSibling) {
              SightingTaxon newParentMapping = SightingTaxons.newSpTaxon(
                  Iterables.concat(parentMapping.getIds(), ImmutableList.of(primaryTaxon.getId())));
              logger.warning(String.format("Adding %s to mapping of %s",
                  TaxonUtils.getCommonName(primaryTaxon), TaxonUtils.getCommonName(secondaryParent)));
              finalMappings.put(secondaryParent.getId(), newParentMapping);
              
              // When we're converting a single mapping to a multiple, that's a lump.  Look for
              // the nominate subspecies.  If it has no mapping, then it's likely a lump of a monotypic
              // species (which is therefore no longer monotypic).  Because there was no subspecies before,
              // no mapping was created for it;  use the "single" mapping for the species.
              // Example: Island Collared-Dove and Philippine Collared-Dove lumped into Island Collared Dove
              // with two subspecies;  map the nominate subspecies to Island Collared-Dove.
              if (parentMapping.getType() == SightingTaxon.Type.SINGLE) {
                for (Taxon child : secondaryParent.getContents()) {
                  // Found the nominate subspecies
                  if (child.getName().equals(secondaryParent.getName())) {
                    if (!finalMappings.containsKey(child.getId())) {
                      finalMappings.put(child.getId(), parentMapping);
                      logger.warning(String.format("And mapping nominate ssp. of %s to %s",
                          TaxonUtils.getCommonName(child),
                          parentMapping.resolve(primaryT).getCommonName()));
                    }
                    break;
                  }
                }
              }
            }
          }
        }
      }
    }
    
    // For any Clements group, see if the group got mapped to an IOC species, while
    // the Clements species got mapped to a different IOC species!
    if (primaryTaxon.getType() == Taxon.Type.group) {
      String secondaryGroupMapping =
          finalMappings.inverse().get(SightingTaxons.newSightingTaxon(primaryTaxon.getId()));
      String secondarySpeciesMapping =
          finalMappings.inverse().get(SightingTaxons.newSightingTaxon(primaryTaxon.getParent().getId()));
      if (secondaryGroupMapping != null && secondarySpeciesMapping != null) {
        Taxon secondaryGroupTaxon = secondaryT.getTaxon(secondaryGroupMapping);
        Taxon secondarySpeciesTaxon = secondaryT.getTaxon(secondarySpeciesMapping);
        if (secondaryGroupTaxon.getType() == Taxon.Type.species
            && secondarySpeciesTaxon.getType() == Taxon.Type.species
            && secondaryGroupTaxon != secondarySpeciesTaxon) {
          // group and species both map to secondary species, but not the same species!  Deleting the
          // secondary mapping for the species is the simplest resolution, but then that introduces
          // a new problem.  Take Cattle Egret.  Cattle Egret (Asian) maps to Eastern Cattle Egret
          // (fine), but there are no mappings for Cattle Egret (ibis) and Cattle Egret (seychelles).
          // So deleting the mapping from Cattle Egret leaves it unmappable.  Which is BAD.
          // So in fact what needs to be done is:
          //    - Walk through all the groups in primaryTaxon.getParent()
          //    - Find all the ones that either have no mappings, or map to secondarySpeciesTaxon
          // ... then map all those group IDs to 
          finalMappings.inverse().remove(SightingTaxons.newSightingTaxon(primaryTaxon.getParent().getId()));
          Set<String> ids = Sets.newHashSet();
          for (Taxon sibling : primaryTaxon.getParent().getContents()) {
            if (sibling == primaryTaxon) {
              continue;
            }
            if (sibling.getType() != Taxon.Type.group) {
              continue;
            }
            // Ignore domestic and introduced forms here
            if (sibling.getStatus() == Status.DO || sibling.getStatus() == Status.IN) {
              continue;
            }
            String siblingMapping = finalMappings.inverse().get(SightingTaxons.newSightingTaxon(sibling.getId()));
            if (siblingMapping == null || siblingMapping.equals(secondarySpeciesTaxon.getId())) {
              ids.add(sibling.getId());
            }
          }
          
          if (ids.isEmpty()) {            
            logger.warning(String.format("Removed mapping of %s to %s, because %s maps to %s",
                    TaxonUtils.getCommonName(primaryTaxon.getParent()),
                    TaxonUtils.getCommonName(secondarySpeciesTaxon),
                    TaxonUtils.getCommonName(primaryTaxon),
                    TaxonUtils.getCommonName(secondaryGroupTaxon)));
          } else {
            SightingTaxon newMapping = SightingTaxons.newPossiblySpTaxon(ids);
            logger.warning(String.format("Now mapping %s from %s, because %s maps to %s",
                    TaxonUtils.getCommonName(secondarySpeciesTaxon),
                    newMapping.resolve(primaryT).getCommonName(),
                    TaxonUtils.getCommonName(primaryTaxon),
                    TaxonUtils.getCommonName(secondaryGroupTaxon)));
            finalMappings.put(secondarySpeciesTaxon.getId(), newMapping);
          }
        }
      }
    }

    // Look for "naked nominate" mappings, where there's
    // (A) A nominate subspecies in Clements
    // (B) That has no mapping at all in IOC
    //
    // These usually indicate that IOC considers a species
    // monotypic, even when Clements considers it to have multiple species.
    //
    // But there are exceptions, e.g. Clements Amazona farinosa farinosa, 
    // which *should* map to "Southern Mealy Amazon".  But instead "Southern Mealy Amazon" maps
    // to "Mealy Amazon".  To tell the difference:
    // 1. Go to the Clements species. (Clements Amazona farinosa)
    // 2. Look at every single subspecies in Clements.
    //   2a.  If *none* of those subspecies have any mappings in IOC, then it looks like
    //        IOC has made this into a monotypic species.
    //   2b.  But if one of those subspecies *does* have a mapping, then we have a problem
    //       (Because it makes no sense to have the nominate subspecies unmapped, but others mapped!)
    //
    // When we find this, we need to adjust the mappings:
    //  (A) REMOVE the existing mapping from Clements species to IOC species
    //  (B) ADD a mapping from the (nominate) Clements subspecies to the IOC species
    if (primaryTaxon.getType() == Taxon.Type.subspecies
        // Ignore anything with group parents
        && primaryTaxon.getParent().getType() == Taxon.Type.species
        && primaryTaxon.getName().equals(primaryTaxon.getParent().getName())) {
      String secondarySspMapping =
          finalMappings.inverse().get(SightingTaxons.newSightingTaxon(primaryTaxon.getId()));
      // No mapping of the ssp. - naked nominate!
      if (secondarySspMapping == null) {
        Taxon primarySpecies = TaxonUtils.getParentOfType(primaryTaxon, Taxon.Type.species);
        String secondarySpeciesMapping =
            finalMappings.inverse().get(SightingTaxons.newSightingTaxon(primarySpecies.getId()));
        if (secondarySpeciesMapping != null) {
          boolean anyMappings = false;
          for (Taxon primaryChildSubspecies : primarySpecies.getContents()) {
            String secondarySspMappingOfChild = 
                finalMappings.inverse().get(SightingTaxons.newSightingTaxon(primaryChildSubspecies.getId()));
            if (secondarySspMappingOfChild != null) {
              anyMappings = true;
              break;
            }
          }
          // Found a mapping, we're at 2B!
          if (anyMappings) {
            Taxon secondarySpecies = secondaryT.getTaxon(secondarySpeciesMapping);
            Preconditions.checkNotNull(secondarySpecies, "No secondary species for primary species %s", primarySpecies.getId());
            finalMappings.remove(secondarySpeciesMapping);
            finalMappings.put(
                secondarySpeciesMapping, SightingTaxons.newSightingTaxon(primaryTaxon.getId()));
            logger.warning(String.format("Fixing naked nominate: %s now maps to %s, instead of %s",
                TaxonUtils.getCommonName(secondarySpecies),
                TaxonUtils.getCommonName(primaryTaxon),
                TaxonUtils.getCommonName(primarySpecies)));
          }
        } else {
          logger.warning("Could not find any secondary mapping for " + primarySpecies.getCommonName());
        }
      }
    }
  }
  
  private void testAllMappingsFromSecondary(Taxon taxon, MappedTaxonomy taxonomy) {
    if (taxon instanceof Species) {
      // See what happens when the taxon is mapped to Clements, then back to IOC
      SightingTaxon mapping = taxonomy.getExactMapping(taxon);
      if (mapping != null) {
        Resolved resolved = taxonomy.resolveInto(mapping);
        if (resolved.getType() == SightingTaxon.Type.SINGLE) {
          if (resolved.getTaxon() != taxon) {
            logger.severe(String.format("%s maps back to %s!",
                    TaxonUtils.getCommonName(taxon),
                    resolved.getCommonName()));
          }
        } else {
          System.err.println(String.format("%s non-unique back to %s!",
                  TaxonUtils.getCommonName(taxon),
                  resolved.getCommonName()));          
        }
      }
    }
    for (Taxon child : taxon.getContents()) {
      testAllMappingsFromSecondary(child, taxonomy);
    }
  }

  private void testAllMappingsFromPrimary(Taxon taxon, MappedTaxonomy taxonomy) {
    if (taxon instanceof Species) {
      String id = taxon.getId();
      Resolved resolved = taxonomy.resolveInto(SightingTaxons.newSightingTaxon(id));
      if (resolved == null) {
        System.err.println("COULD NOT MAP " + TaxonUtils.getCommonName(taxon));
        taxonomy.resolveInto(SightingTaxons.newSightingTaxon(taxon.getId()));
      } else if (resolved.getType() == SightingTaxon.Type.SP) {
        // Group -> ssp. "sps" isn't that interesting, don't output
        if (resolved.getLargestTaxonType() != Taxon.Type.subspecies
            || taxon.getType() != Taxon.Type.group) {
//          System.err.println(String.format("SP MAPPING %s -> %s",
//                  TaxonUtils.getCommonName(taxon),
//                  resolved.getCommonName()));
        }
      }
      
    }
    for (Taxon child : taxon.getContents()) {
      testAllMappingsFromPrimary(child, taxonomy);
    }
  }

  /**
   * Multiple groups, not all in the same species.  Raise all to species, and produce a Sp.
   */
  private Queue<String> pass10MultipleGroupsInDifferentSpecies(Queue<String> toBeMapped) {
    Queue<String> nextPass = Queues.newArrayDeque();
    for (String key : toBeMapped) {
      Collection<String> collection = secondaryToPrimary.get(key);
      List<Taxon> taxa = toTaxaSorted(primaryT, collection);
      if (countType(taxa, Taxon.Type.group) > 1) {
        Set<String> speciesIds = Sets.newHashSet();
        for (Taxon taxon : taxa) {
          Taxon species = TaxonUtils.getParentOfType(taxon, Taxon.Type.species);
          speciesIds.add(species.getId());
        }
        logger.warning(String.format("Multiple groups: %s <- %s, from %s",
                TaxonUtils.getCommonName(secondaryT.getTaxon(key)),
                speciesIds,
                collection));
        if (speciesIds.size() == 1) {
          finalMappings.put(key, SightingTaxons.newSightingTaxon(Iterables.getOnlyElement(speciesIds)));
        } else {
          finalMappings.put(key, SightingTaxons.newSpTaxon(speciesIds));
        }
      } else {
        nextPass.add(key);
      }
    }
    return nextPass;
  }

  /**
   * Species and subspecies not in that species.  Typically means something is very
   * wrong in the primary.
   */
  private Queue<String> pass9SpeciesAndSubspeciesNotInParent(Queue<String> toBeMapped) {
    Queue<String> nextPass = Queues.newArrayDeque();
    for (String key : toBeMapped) {
      Collection<String> collection = secondaryToPrimary.get(key);
      List<Taxon> taxa = toTaxaSorted(primaryT, collection);
      if (countType(taxa, Taxon.Type.species) == 1
          && countType(taxa, Taxon.Type.subspecies) > 0
          && countType(taxa, Taxon.Type.group) == 0) {
        Taxon species = findFirstTaxonOfType(taxa, Taxon.Type.species);
        finalMappings.put(key, SightingTaxons.newSightingTaxon(species.getId()));
        logger.warning(String.format("Subspecies out of species: %s <- %s",
                TaxonUtils.getCommonName(secondaryT.getTaxon(key)),
                collection));
      } else {
        nextPass.add(key);
      }
    }
    return nextPass;
  }


  /**
   * Pass 8:  there are multiple groups, but no subspecies.  Map to a Sp. of the groups.
   */
  private Queue<String> pass8MultipleGroups(Queue<String> toBeMapped) {
    Queue<String> nextPass;
    nextPass = Queues.newArrayDeque();
    for (String key : toBeMapped) {
      Collection<String> collection = secondaryToPrimary.get(key);
      if (collection.size() >= 2) {
        List<Taxon> taxa = toTaxaSorted(primaryT, collection);
        if (countType(taxa, Taxon.Type.species) == 0
            && countType(taxa, Taxon.Type.group) > 1
            && countType(taxa, Taxon.Type.subspecies) == 0) {
          List<String> groupIds = Lists.newArrayList(); 
          for (Taxon taxon : taxa) {
            if (taxon.getType() == Taxon.Type.group) {
              groupIds.add(taxon.getId());
            }
          }
          finalMappings.put(key, SightingTaxons.newSpTaxon(groupIds));
          continue;
        }
      }
      nextPass.add(key);
    }
    return nextPass;
  }

  /**
   * Pass 7:  a secondary sp. mapped to one primary sp. and multiple groups (or subspecies).
   * Map all to the species.
   */
  private Queue<String> pass7SpeciesAndMultipleGroups(Queue<String> toBeMapped) {
    Queue<String> nextPass;
    nextPass = Queues.newArrayDeque();
    for (String key : toBeMapped) {
      Collection<String> collection = secondaryToPrimary.get(key);
      if (collection.size() >= 2) {
        List<Taxon> taxa = toTaxaSorted(primaryT, collection);
        if (taxa.get(0).getType() == Taxon.Type.species
            && countType(taxa, Taxon.Type.species) == 1
            && countType(taxa, Taxon.Type.group) > 1
            && allAreDescendentsOf(taxa.get(0), taxa.subList(1, taxa.size()))) {
          finalMappings.put(key, SightingTaxons.newSightingTaxon(taxa.get(0).getId()));
          continue;
        }
      }
      nextPass.add(key);
    }
    return nextPass;
  }

  /**
   * Lumps: multiple species map to a single taxon.  Produce a "sp" taxon.
   */
  private Queue<String> pass6MultipleSpecies(Queue<String> toBeMapped) {
    Queue<String> nextPass = Queues.newArrayDeque();
    for (String key : toBeMapped) {
      Collection<String> collection = secondaryToPrimary.get(key);
      List<Taxon> taxa = toTaxaSorted(primaryT, collection);
      if (countType(taxa, Taxon.Type.species) == 2) {
        List<String> speciesIds = Lists.newArrayList(); 
        for (Taxon taxon : taxa) {
          if (taxon.getType() == Taxon.Type.species) {
            speciesIds.add(taxon.getId());
          }
        }
        finalMappings.put(key, SightingTaxons.newSpTaxon(speciesIds));
        logger.warning(String.format("Lump: %s <- %s",
            TaxonUtils.getCommonName(secondaryT.getTaxon(key)),
            collection));
      } else {
        nextPass.add(key);
      }
    }
    return nextPass;
  }

  /**
   * Species and multiple of its groups.  Map the species to species, groups
   * will be left unmapped.
   */
  private Queue<String> pass5SpeciesAndMultipleGroups(Queue<String> toBeMapped) {
    Queue<String> nextPass = Queues.newArrayDeque();
    for (String key : toBeMapped) {
      Collection<String> collection = secondaryToPrimary.get(key);
      List<Taxon> taxa = toTaxaSorted(primaryT, collection);
      if (taxa.get(0).getType() == Taxon.Type.species
          && countType(taxa, Taxon.Type.species) == 1
          && countType(taxa, Taxon.Type.group) > 1
          && countType(taxa, Taxon.Type.subspecies) == 0
          && allAreDescendentsOf(taxa.get(0), taxa.subList(1, taxa.size()))) {
        finalMappings.put(key, SightingTaxons.newSightingTaxon(taxa.get(0).getId()));
      } else {
        nextPass.add(key);
      }
    }
    return nextPass;
  }

  /**
   * Species and (some) of its subspecies.  Means that a primary subspecies or two
   * is missing from the secondary taxonomy;  map species to species, and the
   * subspecies will get no mappings.
   */
  private Queue<String> pass4SpeciesAndSubspecies(Queue<String> toBeMapped) {
    Queue<String> nextPass = Queues.newArrayDeque();
    for (String key : toBeMapped) {
      Collection<String> collection = secondaryToPrimary.get(key);
      List<Taxon> taxa = toTaxaSorted(primaryT, collection);
      if (taxa.get(0).getType() == Taxon.Type.species
          && countType(taxa, Taxon.Type.species) == 1
          && countType(taxa, Taxon.Type.group) == 0
          && allAreDescendentsOf(taxa.get(0), taxa.subList(1, taxa.size()))) {
        // Two possibilities:  this is a species, in which case we definitely want to map to the species,
        // or this is a subspecies, in which case we're about to map an (IOC) subspecies up to the (Clements)
        // species even though there might be a legit direct subspecies mapping!
        Taxon taxon = secondaryT.getTaxon(key);
        if (taxon.getType() == Taxon.Type.species) {
          if (taxa.size() > 2) {
            System.err.printf("PASS4 SPECIES, MAP %s to %s, not other %s\n",
                TaxonUtils.getFullName(taxon), TaxonUtils.getFullName(taxa.get(0)), taxa.size() - 1);
          } else {
            System.err.printf("PASS4 SPECIES, MAP %s to %s, not other %s\n",
                TaxonUtils.getFullName(taxon), TaxonUtils.getFullName(taxa.get(0)), TaxonUtils.getFullName(taxa.get(1)));
          }
          finalMappings.put(key, SightingTaxons.newSightingTaxon(taxa.get(0).getId()));
        } else {
          if (taxa.size() == 2) {
            System.err.printf("PASS4 SSP, MAP %s to %s\n", TaxonUtils.getFullName(taxon), TaxonUtils.getFullName(taxa.get(1)));
            finalMappings.put(key, SightingTaxons.newSightingTaxon(taxa.get(1).getId()));
          } else {
            logger.severe("PASS 4 FAILURE " + key);
          }
        }
      } else {
        nextPass.add(key);
      }
    }
    return nextPass;
  }

  /**
   * Catch all the cases with:
   * - one primary species
   * - one primary group
   * - N primary subspecies (within said species)
   * - And map the secondary taxon to the primary group.
   */
  private Queue<String> pass3SpeciesAndOneGroup(Queue<String> toBeMapped) {
    Queue<String> nextPass;
    // Pass 3: look for secondary sp. mapped to one primary sp. and one primary gr.
    // Map the secondary sp. to the primary
    nextPass = Queues.newArrayDeque();
    for (String key : toBeMapped) {
      Collection<String> collection = secondaryToPrimary.get(key);
      if (collection.size() >= 2) {
        List<Taxon> taxa = toTaxaSorted(primaryT, collection);
        if (taxa.get(0).getType() == Taxon.Type.species
            && taxa.get(1).getType() == Taxon.Type.group
            && taxa.get(1).getParent() == taxa.get(0)
            && taxa.get(1).getStatus() != Status.DO
            && countType(taxa, Taxon.Type.group) == 1
            && allAreDescendentsOf(taxa.get(0), taxa.subList(2, taxa.size()))) {
          finalMappings.put(key, SightingTaxons.newSightingTaxon(taxa.get(1).getId()));
          continue;
        }
      }
      nextPass.add(key);
    }
    return nextPass;
  }

  private int countType(List<Taxon> taxa, Type type) {
    int count = 0;
    for (Taxon taxon : taxa) {
      if (taxon.getType() == type) {
        count++;
      }
    }
    return count;
  }

  // Pass 2: look for secondary taxon mapped to a primary group and some subspecies
  // of that group.  In this case, there's subspecies of the primary that are missing
  // from the group, and we'll ignore those subspecies in the mapping (they'll have
  // to get collapsed upwards)
  private Queue<String> pass2GroupWithSubspecies(Queue<String> toBeMapped) {
    Queue<String> nextPass = Queues.newArrayDeque();
    for (String key : toBeMapped) {
      Collection<String> collection = secondaryToPrimary.get(key);
      if (collection.size() >= 2) {
        List<Taxon> taxa = toTaxaSorted(primaryT, collection);
        if (taxa.get(0).getType() == Taxon.Type.group
            && allAreDescendentsOf(taxa.get(0), taxa.subList(1, taxa.size()))) {
          finalMappings.put(key, SightingTaxons.newSightingTaxon(taxa.get(0).getId()));
          continue;
        }
      }
      nextPass.add(key);
    }
    return nextPass;
  }

  private Queue<String> pass1SingleTaxa() {
    Queue<String> toBeMapped = Queues.newArrayDeque();
    // Pass 1: all single mappings
    for (String key : secondaryToPrimary.keySet()) {
      Collection<String> collection = secondaryToPrimary.get(key);
      if (collection.size() == 1) {
        finalMappings.put(key, SightingTaxons.newSightingTaxon(Iterables.getOnlyElement(collection)));
      } else {
        toBeMapped.add(key);
      }
    }
    return toBeMapped;
  }

  /** Returns true if every taxon in the list is a descendent of taxon. */ 
  private boolean allAreDescendentsOf(Taxon taxon, List<Taxon> subList) {
    for (Taxon child : subList) {
      if (!TaxonUtils.isChildOf(taxon, child)) {
        return false;
      }
    }
    return true;
  }

  ProduceMappedTaxonomy(
      Taxonomy primaryT,
      Taxonomy secondaryT,
      Multimap<String, String> secondaryToPrimary,
      Map<String, String> hardcoded) {
    this.primaryT = primaryT;
    this.secondaryT = secondaryT;
    this.secondaryToPrimary = secondaryToPrimary;
    this.hardcoded = hardcoded;
    primaryToSecondary = HashMultimap.create();
    Multimaps.invertFrom(secondaryToPrimary, primaryToSecondary);
  }

  private Set<String> gatherChildrenOfType(Taxon taxon, Taxon.Type type) {
    Set<String> taxa = Sets.newHashSet();
    for (Taxon child : taxon.getContents()) {
      if (child.getType() == type) {
        taxa.add(child.getId());
      }
    }
    return taxa;
  }
  
  private Taxon findFirstTaxonOfType(Collection<Taxon> taxa, Taxon.Type type) {
    for (Taxon taxon : taxa) {
      if (taxon.getType() == type) {
        return taxon;
      }
    }
    return null;
  }
  
  private static List<Taxon> toTaxaSorted(Taxonomy primaryT, Collection<String> collection) {
    List<Taxon> list = Lists.newArrayList();
    for (String id : collection) {
      list.add(Preconditions.checkNotNull(primaryT.getTaxon(id), "No primary taxon %s", id));
    }
    Collections.sort(list, new TaxonComparator());
    return list;
  }

  private static Multimap<String, String> loadMapping(String fileName) throws IOException {
    CSVReader reader = new CSVReader(new FileReader(new File(fileName)));
    HashMultimap<String, String> mappings = HashMultimap.create();
    while (true) {
      String[] readNext = reader.readNext();
      if (readNext == null) {
        break;
      }
      if (!readNext[1].isEmpty()) {
        if (readNext[2].contains("/")) {
          for (String id : Splitter.on('/').split(readNext[2])) {
            mappings.put(id, readNext[1]);
          }
        } else {
          mappings.put(readNext[2], readNext[1]);
        }
      }
    }
    reader.close();
    return mappings;
  }
  
  private Taxon cloneTaxon(Taxon taxon, Taxon clonedParent) {
    TaxonImpl clone;
    if (taxon instanceof Species) {
      clone = new SpeciesImpl(taxon.getType(), clonedParent);
    } else {
      clone = new TaxonImpl(taxon.getType(), clonedParent);
    }
    
    clone.setCommonName(taxon.getCommonName());
    clone.setName(taxon.getName());
    
    if (taxon instanceof Species) {
      SpeciesImpl cloneSpecies = (SpeciesImpl) clone;
      Species species = (Species) taxon;
      cloneSpecies.setAlternateCommonNames(species.getAlternateCommonNames());
      cloneSpecies.setAlternateNames(species.getAlternateNames());
      cloneSpecies.setMiscellaneousInfo(species.getMiscellaneousInfo());
      cloneSpecies.setRange(species.getRange());
      cloneSpecies.setStatus(species.getStatus());
      cloneSpecies.setTaxonomicInfo(species.getTaxonomicInfo());
    }
    
    return clone;
  }

  private void cloneChildrenAndBuildFinalMappings(
          MappedTaxonomy taxonomy, MappingBuilder mappingBuilder, Taxon parent, Taxon clonedParent) {
    for (Taxon child : parent.getContents()) {
      Taxon clonedChild = cloneTaxon(child, clonedParent);
      clonedParent.getContents().add(clonedChild);
      clonedChild.built();

      cloneChildrenAndBuildFinalMappings(taxonomy, mappingBuilder, child, clonedChild);

      String childId = child.getId();
      SightingTaxon mapping = finalMappings.get(childId);
      if (mapping != null) {
        // Verify species-level mappings aren't mapping inappropriately
        // Specifically, we have cases (like Lesser Rhea), where:
        //   - one of the Clements groups can only be mapped to the species
        //   - the other Clements groups map cleanly to subspecies (since they're monotypic)
        // ... and the naive outcome of that was that we were outputting the secondary species
        // mapped to a primary group, even though subspecies of that were getting mapped to other groups
        if (child.getType() == Taxon.Type.species) {
          Resolved resolved = mapping.resolve(primaryT);
          if (resolved.getSmallestTaxonType() == Taxon.Type.group) {
            boolean foundAnotherGroup = false;
            // So, we've got a case with a species mapping to a group.
            // Walk through all the newly created secondary children, and if
            // any of *those* also map to a group, then the species->group mapping is bogus.
            Set<String> additionalGroups = Sets.newHashSet();
            for (Taxon clonedGrandchild : child.getContents()) {
              SightingTaxon grandchildMapping = mappingBuilder.getExactMapping(clonedGrandchild);
              if (grandchildMapping != null) {
                Resolved resolvedGrandchild = grandchildMapping.resolve(primaryT);
                // Skip domestic forms
                if (resolvedGrandchild.getTaxonStatus() == Status.DO
                    || resolvedGrandchild.getTaxonStatus() == Status.IN) {
                  continue;
                }
                if (resolvedGrandchild.getSmallestTaxonType() == Taxon.Type.group) {
                  foundAnotherGroup = true;
                  additionalGroups.addAll(resolvedGrandchild.getSightingTaxon().getIds());
                }
              }
            }
            
            if (foundAnotherGroup) {
              additionalGroups.addAll(resolved.getSightingTaxon().getIds());
              
              SightingTaxon speciesParent = resolved.getParent();
              Taxon speciesParentTaxon = speciesParent.resolve(primaryT).getTaxon();
              Set<String> allGroups = gatherChildrenOfType(speciesParentTaxon, Taxon.Type.group);
              if (allGroups.equals(additionalGroups)) {
                logger.log(Level.FINE, "Safe multi-group mapping trim: from {0} to {1} + {2}",
                        new Object[]{TaxonUtils.getCommonName(clonedChild),
                        resolved.getCommonName(),
                        additionalGroups});
                mapping = resolved.getParent();
              } else {
                if (additionalGroups.size() <= 12) {
                  logger.log(Level.FINE, "Creating multi-group mapping: from {0} to {1}",
                          new Object[]{TaxonUtils.getCommonName(clonedChild),
                          additionalGroups});
                  mapping = SightingTaxons.newSpTaxon(additionalGroups);
                } else {
                  logger.log(Level.WARNING, "Trimming multi-group mapping: {0} to {1}, missing {2}",
                          new Object[]{TaxonUtils.getCommonName(clonedChild),
                          additionalGroups,
                          Sets.difference(allGroups, additionalGroups)});
                  mapping = resolved.getParent();
                }
              }
            }
          }
        }
        addMapping(mappingBuilder, clonedChild, mapping);
      } else if (clonedChild.getType().compareTo(Taxon.Type.species) <= 0) {
        if (addHardcodedMapping(mappingBuilder, clonedChild)) {
          continue;
        }
        
        // For anything that is a species (or smaller, though that's basically irrelevant here),
        // see if there is only *one* mappable child.  If so, then move that mapping up a level.
        // This typically comes up when:
        // - Clements has a monotypic group
        // - There's a whole bunch of subspecies in IOC
        SightingTaxon foundMapping = null;
        Taxon mappedGrandchild = null;
        boolean foundMultiple = false;
        for (Taxon clonedGrandchild : clonedChild.getContents()) {
          SightingTaxon grandchildMapping = mappingBuilder.getExactMapping(clonedGrandchild);
          if (grandchildMapping != null) {
            if (foundMapping != null) {
              foundMultiple = true;
              break;
            } else {
              foundMapping = grandchildMapping;
              mappedGrandchild = clonedGrandchild;
            }
          }
        }
        if (!foundMultiple && foundMapping != null) {
          mappingBuilder.removeTaxonMapping(mappedGrandchild);
          addMapping(mappingBuilder, clonedChild, foundMapping);
        }
      }
    }
  }

  private void addMapping(MappingBuilder mappingBuilder, Taxon taxon,
      SightingTaxon baseMapping) {
    // Check for fully hardcoded mappings - those that the above heuristics don't get. 
    if (addHardcodedMapping(mappingBuilder, taxon)) {
      return;
    }
    
    mappingBuilder.registerTaxonMapping(taxon, baseMapping);
  }
  
  private boolean addHardcodedMapping(MappingBuilder mappingBuilder, Taxon taxon) {
    if (hardcoded.containsKey(taxon.getId())) {
      String mapping = hardcoded.get(taxon.getId());
      // An explicit no-mapping
      if (!mapping.isEmpty()) {
        // A mapping, possibly a sp.
        SightingTaxon sightingTaxon = SightingTaxons.newPossiblySpTaxon(
            Splitter.on('/').splitToList(mapping));
        mappingBuilder.registerTaxonMapping(taxon, sightingTaxon);
      }
      return true;
    } else {
      return false;
    }
  }
  
  private static HashMap<String, String> parseHardcodedMappings(String fileName) throws IOException {
    HashMap<String, String> mappings = new HashMap<String, String>();
    try (var reader = new CSVReader(new FileReader(new File(fileName)))) {
      while (true) {
        String[] readNext = reader.readNext();
        if (readNext == null) {
          break;
        }
        // Skip comment lines
        if (readNext[0].startsWith("#")) {
          continue;
        }
        if (readNext.length != 2) {
          throw new IllegalArgumentException("Unexpected hardcoded line: " + Joiner.on(',').join(readNext));
        }
        mappings.put(readNext[0].trim(), readNext[1].trim());
      }
    }
    return mappings;
  }


}
