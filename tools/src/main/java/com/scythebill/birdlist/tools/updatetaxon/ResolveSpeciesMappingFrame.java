package com.scythebill.birdlist.tools.updatetaxon;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.google.common.base.Joiner;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.SpeciesImpl;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.components.SpeciesIndexerPanelConfigurer;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.util.TaxonToCommonName;

class ResolveSpeciesMappingFrame {
  private static final Executor frameExecutor = Executors.newSingleThreadExecutor();
  private final TaxonomyData from;
  private final TaxonomyData to;

  public ResolveSpeciesMappingFrame(
      TaxonomyData from,
      TaxonomyData to) {
    this.from = from;
    this.to = to;
  }
  
  public ListenableFuture<String> resolve(String fromTaxon, String progressText) {
    final JDialog dialog = new JDialog(null, Dialog.ModalityType.APPLICATION_MODAL);
    Species species = (Species) from.getTaxonomy().getTaxon(fromTaxon);
    MapSpeciesPanel mapSpeciesPanel = new MapSpeciesPanel(species, progressText);
    mapSpeciesPanel.result().addListener(new Runnable() {
      @Override public void run() {
        try {
          SwingUtilities.invokeAndWait(() -> dialog.setVisible(false));
        } catch (InterruptedException e) {
          Thread.interrupted();
          throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
          throw new RuntimeException(e);
        }
      }
    }, frameExecutor);
    dialog.setContentPane(mapSpeciesPanel);
    dialog.pack();
    dialog.setVisible(true);
    return mapSpeciesPanel.result();
  }
  
  class MapSpeciesPanel extends JPanel {
    private final Species fromTaxon;
    private IndexerPanel<String> indexerPanel;
    private final SettableFuture<String> result = SettableFuture.create();

    MapSpeciesPanel(Species fromTaxon, String progressText) {
      this.fromTaxon = fromTaxon;
      
      initUI(progressText);
    }
    
    public ListenableFuture<String> result() {
      return result;
    }

    private void initUI(String progressText) {
      setPreferredSize(new Dimension(800, 800));
      BoxLayout boxLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
      setLayout(boxLayout);
      
      JEditorPane fromInfoText = new JEditorPane();
      fromInfoText.setContentType("text/html");
      fromInfoText.setPreferredSize(new Dimension(Short.MAX_VALUE, 100));
      fromInfoText.setMaximumSize(new Dimension(Short.MAX_VALUE, 150));
      setTaxonText(fromInfoText, fromTaxon);
      to.getCommonIndexer();
      to.getSciIndexer();
      indexerPanel = new IndexerPanel<String>();
      SpeciesIndexerPanelConfigurer.unconfigured().configure(indexerPanel, to.getTaxonomy());
      JButton button = new JButton();
      button.setText("Resolve");
      button.addActionListener(new ActionListener() {
        @Override public void actionPerformed(ActionEvent arg0) {
          String value = indexerPanel.getValue();
          if (value != null) {
            result.set(value);
          }
        }
      });
      add(new JLabel(progressText + ": Mapping " + TaxonUtils.getCommonName(fromTaxon)));
      add(new JLabel("<html><i>" + TaxonUtils.getFullName(fromTaxon)));
      add(new JLabel(from.getTaxonomy().getName()));
      add(fromInfoText);
      
      final JEditorPane toInfoText = new JEditorPane();
      toInfoText.setContentType("text/html");
      toInfoText.setPreferredSize(new Dimension(Short.MAX_VALUE, 100));
      toInfoText.setMaximumSize(new Dimension(Short.MAX_VALUE, 150));
      indexerPanel.addPropertyChangeListener("value", new PropertyChangeListener() {
        @Override public void propertyChange(PropertyChangeEvent arg0) {
          String value = indexerPanel.getValue();
          if (value != null) {
            Taxon toTaxon = to.getTaxonomy().getTaxon(value);
            setTaxonText(toInfoText, (Species) toTaxon);
          }
        }        
      });
      
      add(indexerPanel);
      add(toInfoText);
      add(new JLabel(to.getTaxonomy().getName()));
      add(button);
      
      
      // Button to add after:
      //   - Find the genus for "to"
      //   - Make a copy of "from" going in that genus
      //   - Add the species, and return that
      // Anything that's new and a monotypic genus would fail
      JButton addAfter = new JButton();
      addAfter.setText("Add after");
      addAfter.addActionListener(new ActionListener() {
        @Override public void actionPerformed(ActionEvent arg0) {
          String value = indexerPanel.getValue();
          if (value != null) {
            Taxon afterSpecies = to.getTaxonomy().getTaxon(value);
            Taxon toGenus = TaxonUtils.getParentOfType(afterSpecies, Taxon.Type.genus);
            Taxon fromGenus = TaxonUtils.getParentOfType(fromTaxon, Taxon.Type.genus);
            
            SpeciesImpl toSpecies = new SpeciesImpl(Taxon.Type.species, toGenus);
            toSpecies.setCommonName(fromTaxon.getCommonName());
            toSpecies.setTaxonomicInfo(fromTaxon.getTaxonomicInfo());
            toGenus.getContents().add(toSpecies);
            toSpecies.built();
            result.set(toSpecies.getId());
          }
        }
      });
      add(addAfter);
    }

    private void setTaxonText(JEditorPane pane, Species taxon) {
      StringBuilder builder = new StringBuilder("<html><div style='font: 11pt'>");
      if (taxon != null) {
        builder.append(TaxonToCommonName.getInstance().getString(taxon)).append("<br>");
        builder.append("<i>").append(TaxonUtils.getFullName(taxon)).append("</i>");
        if (taxon instanceof Species) {
          Species species = (Species) taxon;
          if (species.getStatus() != Status.LC) {
            builder.append("<br><b>");
            builder.append(Messages.getText(species.getStatus()));
            builder.append("</b>");
          }

          if (!species.getAlternateCommonNames().isEmpty()) {
            builder.append("<br><b>Alternate names:</b> ");
            builder.append(Joiner.on('/').join(species.getAlternateCommonNames()));
          }

          String range = TaxonUtils.getRange(species);
          if (range != null) {
            builder.append("<br><b>Range:</b> ");
            builder.append(range);
          }

          if (species.getTaxonomicInfo() != null) {
            builder.append("<br><b>Taxonomy:</b> ");
            builder.append(species.getTaxonomicInfo());
          }
        }
      }
      builder.append("</div></html>");
      pane.setText(builder.toString());
      pane.select(0, 0);
    }    
  }
}
