package com.scythebill.birdlist.tools.updatetaxon;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.google.common.base.Joiner;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.SpeciesImpl;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.components.SpeciesIndexerPanelConfigurer;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.util.SubspeciesToString;
import com.scythebill.birdlist.ui.util.TaxonToCommonName;
import com.scythebill.birdlist.ui.util.ToStringListCellRenderer;

/**
 * Brutally copied from ResolveSpeciesMappingFrame.
 * TODO: factor out common functionality (or just delete the other one and use this)
 */
class ResolveSubspeciesOrGroupMappingFrame {
  private static final Executor frameExecutor = Executors.newSingleThreadExecutor();
  private final TaxonomyData from;
  private final TaxonomyData to;

  public ResolveSubspeciesOrGroupMappingFrame(
      TaxonomyData from,
      TaxonomyData to) {
    this.from = from;
    this.to = to;
  }
  
  public ListenableFuture<String> resolve(String fromTaxon, Taxon initialSpecies, String progressText) {
    final JDialog dialog = new JDialog(null, Dialog.ModalityType.APPLICATION_MODAL);
    Species species = (Species) from.getTaxonomy().getTaxon(fromTaxon);
    MapSubspeciesOrGroupPanel mapSpeciesPanel = new MapSubspeciesOrGroupPanel(species, initialSpecies, progressText);
    mapSpeciesPanel.result().addListener(new Runnable() {
      @Override public void run() {
        try {
          SwingUtilities.invokeAndWait(() -> dialog.setVisible(false));
        } catch (InterruptedException e) {
          Thread.interrupted();
          throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
          throw new RuntimeException(e);
        }
      }
    }, frameExecutor);
    dialog.setContentPane(mapSpeciesPanel);
    dialog.pack();
    dialog.setVisible(true);
    return mapSpeciesPanel.result();
  }
  
  class MapSubspeciesOrGroupPanel extends JPanel {
    private final Species fromTaxon;
    private IndexerPanel<String> indexerPanel;
    private final SettableFuture<String> result = SettableFuture.create();
    private Taxon resolvedTaxon = null;
    

    MapSubspeciesOrGroupPanel(
            Species fromTaxon, Taxon initialSpecies, String progressText) {
      this.fromTaxon = fromTaxon;
      
      initUI(progressText, initialSpecies);
    }
    
    public ListenableFuture<String> result() {
      return result;
    }

    private void initUI(String progressText, Taxon initialSpecies) {
      setPreferredSize(new Dimension(800, 800));
      BoxLayout boxLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
      setLayout(boxLayout);
      
      JEditorPane fromInfoText = new JEditorPane();
      fromInfoText.setContentType("text/html");
      fromInfoText.setPreferredSize(new Dimension(Short.MAX_VALUE, 100));
      fromInfoText.setMaximumSize(new Dimension(Short.MAX_VALUE, 150));
      setTaxonText(fromInfoText, fromTaxon);
      
      to.getCommonIndexer();
      to.getSciIndexer();
      indexerPanel = new IndexerPanel<String>();
      SpeciesIndexerPanelConfigurer.unconfigured().configure(indexerPanel, to.getTaxonomy());
      
      JButton button = new JButton();
      button.setText("Resolve");
      button.addActionListener(new ActionListener() {
        @Override public void actionPerformed(ActionEvent arg0) {
          if (resolvedTaxon != null) {
            result.set(resolvedTaxon.getId());
          }
        }
      });
      add(new JLabel(progressText + ": Mapping " + TaxonUtils.getCommonName(fromTaxon)));
      add(new JLabel("<html><i>" + TaxonUtils.getFullName(fromTaxon)));
      add(new JLabel(from.getTaxonomy().getName()));
      add(fromInfoText);
      
      final JEditorPane toInfoText = new JEditorPane();
      toInfoText.setContentType("text/html");
      toInfoText.setPreferredSize(new Dimension(Short.MAX_VALUE, 100));
      toInfoText.setMaximumSize(new Dimension(Short.MAX_VALUE, 150));
      final JComboBox subspecies = new JComboBox();
      subspecies.setRenderer(new ToStringListCellRenderer<Taxon>(
          new SubspeciesToString(), Taxon.class, subspecies.getRenderer()));
      subspecies.setModel(new DefaultComboBoxModel());
      subspecies.addActionListener(new ActionListener() {
        @Override public void actionPerformed(ActionEvent arg0) {
          resolvedTaxon = (Taxon) subspecies.getSelectedItem();
          setTaxonText(toInfoText, (Species) resolvedTaxon);
        }
      });
      indexerPanel.addPropertyChangeListener("value", new PropertyChangeListener() {
        @Override public void propertyChange(PropertyChangeEvent arg0) {
          String value = indexerPanel.getValue();
          if (value != null) {
            Taxon toTaxon = to.getTaxonomy().getTaxon(value);
            resolvedTaxon = toTaxon;
            setTaxonText(toInfoText, (Species) toTaxon);
            buildSubspeciesModel(toTaxon, subspecies);
          }
        }
      });
      
      add(indexerPanel);
      add(subspecies);
      add(toInfoText);
      add(new JLabel(to.getTaxonomy().getName()));
      add(button);
      
      
      // Button to add after:
      //   - Find the genus for "to"
      //   - Make a copy of "from" going in that genus
      //   - Add the species, and return that
      // Anything that's new and a monotypic genus would fail
      JButton addAfter = new JButton();
      addAfter.setText("Add after");
      addAfter.addActionListener(new ActionListener() {
        @Override public void actionPerformed(ActionEvent arg0) {
          if (true) throw new UnsupportedOperationException("Not implemented yet");
          // Need to insert at the right taxonomic level - group into species, subspecies into group or species
          if (resolvedTaxon != null) {
            Taxon afterSpecies = resolvedTaxon;
            Taxon toGenus = TaxonUtils.getParentOfType(afterSpecies, Taxon.Type.genus);
            Taxon fromGenus = TaxonUtils.getParentOfType(fromTaxon, Taxon.Type.genus);
            
            SpeciesImpl toSpecies = new SpeciesImpl(Taxon.Type.species, toGenus);
            toSpecies.setCommonName(fromTaxon.getCommonName());
            toSpecies.setTaxonomicInfo(fromTaxon.getTaxonomicInfo());
            toGenus.getContents().add(toSpecies);
            toSpecies.built();
            result.set(toSpecies.getId());
          }
        }
      });
      add(addAfter);
      
      if (initialSpecies != null) {
        indexerPanel.setValue(initialSpecies.getId());
      }
    }
    
    /** Set up the subspecies model */
    private void buildSubspeciesModel(Taxon taxon, JComboBox subspeciesCombo) {
      subspeciesCombo.putClientProperty("species", taxon);
      final DefaultComboBoxModel model = new DefaultComboBoxModel();
      
      final Taxon species = TaxonUtils.getParentOfType(taxon, Type.species);
      model.addElement(species);
      TaxonUtils.visitTaxa(species, new TaxonVisitor() {
        @Override public boolean visitTaxon(Taxon visited) {
          if (species != visited) {
            model.addElement(visited);
          }
          
          return true;
        }
        
      });
      
      subspeciesCombo.setEnabled(model.getSize() > 1);
      
      // And if the taxon is itself a subspecies/group, select it
      if (taxon.getType().compareTo(Type.species) < 0) {
        model.setSelectedItem(taxon);
      }
      
      subspeciesCombo.setModel(model);
    }

    private void setTaxonText(JEditorPane pane, Species taxon) {
      StringBuilder builder = new StringBuilder("<html><div style='font: 11pt'>");
      if (taxon != null) {
        builder.append(TaxonToCommonName.getInstance().getString(taxon)).append("<br>");
        builder.append("<i>").append(TaxonUtils.getFullName(taxon)).append("</i>");
        if (taxon instanceof Species) {
          Species species = (Species) taxon;
          if (species.getStatus() != Status.LC) {
            builder.append("<br><b>");
            builder.append(Messages.getText(species.getStatus()));
            builder.append("</b>");
          }

          if (!species.getAlternateCommonNames().isEmpty()) {
            builder.append("<br><b>Alternate names:</b> ");
            builder.append(Joiner.on('/').join(species.getAlternateCommonNames()));
          }

          String range = TaxonUtils.getRange(species);
          if (range != null) {
            builder.append("<br><b>Range:</b> ");
            builder.append(range);
          }

          if (species.getTaxonomicInfo() != null) {
            builder.append("<br><b>Taxonomy:</b> ");
            builder.append(species.getTaxonomicInfo());
          }
        }
      }
      builder.append("</div></html>");
      pane.setText(builder.toString());
      pane.select(0, 0);
    }    
  }
}
