/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.updatetaxon;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.xml.sax.SAXException;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Splitter;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Collections2;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Multiset;
import com.google.common.collect.Queues;
import com.google.common.collect.Sets;
import com.google.common.util.concurrent.Futures;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.SpeciesImpl;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.xml.XmlTaxonExport;
import com.scythebill.birdlist.model.xml.XmlTaxonImport;
import com.scythebill.birdlist.tools.util.ToolTaxonUtils;

/**
 * Compares a new and old taxonomy file, and generates a mapping file plus a
 * modified "new" taxonomy file (with extra data merged in from the old
 * taxonomy).
 * <p>
 */
public class TaxonomyComparer {
  private static final Logger logger = Logger.getLogger(TaxonomyComparer.class
      .getName());  
  
  private final Taxonomy newT;
  private final Taxonomy oldT;

  private final Map<String, String> mappings = Maps.newLinkedHashMap();

  private final Map<String, Taxon> newGenusNames = Maps.newHashMap();
  private final Multimap<String, Taxon> newSpeciesNames = ArrayListMultimap
      .create();
  private final Multimap<String, Taxon> newCommonNames = ArrayListMultimap
      .create();
  // Mapping of old genus names to new genus names, for genera that appear to
  // have disappeared
  private final Multimap<String, String> genusMappings = HashMultimap.create();


  private final ImmutableMap<String, String> hardcoded;
  private final Map<String, String> newHardcoded;

  private ImmutableMap<String, Resolved> hardcodedSps;

  @Parameter(names = "-oldTaxonomy")
  private String oldTaxonomy;

  @Parameter(names = "-newTaxonomy")
  private String newTaxonomy;

  @Parameter(names = "-hardcodedMappings")
  private String hardcodedMappings;

  @Parameter(names = "-modifiedTaxonomy")
  private String modifiedTaxonomy;

  @Parameter(names = "-mappingFile")
  private String mappingFile = "/tmp/mapping.csv";

  @Parameter(names = "-iocTaxonMappings")
  private boolean iocTaxonMappings;

  @Parameter(names = "-disableLookForSpMappings")
  private boolean disableLookForSpMappings;

  public static void main(String[] args) throws Exception {
    TaxonomyComparer taxonomyComparer = new TaxonomyComparer(args);

    taxonomyComparer.compare();

    // Write out a mapping file
    taxonomyComparer.writeMappingFile();

    taxonomyComparer.writeModifiedTaxonomyFile();
    
    System.exit(0);
  }
  
  private void writeModifiedTaxonomyFile() throws IOException {
    if (modifiedTaxonomy != null) {
      File modifiedTaxonomyFile = new File(this.modifiedTaxonomy);
      if (!modifiedTaxonomyFile.exists()) {
        modifiedTaxonomyFile.createNewFile();
      }
      try (Writer taxonomyOut = new OutputStreamWriter(new FileOutputStream(modifiedTaxonomyFile),
          "UTF-8")) {
        new XmlTaxonExport().export(taxonomyOut, newT, "UTF-8");
      }
    }
  }
  
  private void writeMappingFile() throws IOException {
    // Write out a mapping file
    File mappingFile = new File(this.mappingFile);
    if (!mappingFile.exists()) {
      mappingFile.createNewFile();
    }
    Writer out = new OutputStreamWriter(new FileOutputStream(mappingFile),
        "UTF-8");
    
    class SortingCsvWriter implements ExportLines {
      private final CSVWriter csvWriter;
      private final ArrayList<String[]> lines;

      public SortingCsvWriter(CSVWriter csvWriter) {
        this.csvWriter = csvWriter;
        this.lines = new ArrayList<>();        
      }
      
      @Override
      public void close() throws IOException {
        Joiner joiner = Joiner.on(',');
        Collections.sort(lines,
            Comparator.comparing(line -> joiner.join(line)));
        csvWriter.writeAll(lines);
        csvWriter.close();
      }

      @Override
      public void nextLine(String[] line) throws IOException {
        lines.add(line);
      }
    }
    
    try (SortingCsvWriter csv = new SortingCsvWriter(new CSVWriter(out))) {
      writeMapping(csv);
    }
    
  }

  private static Map<String, String> parseHardcodedMappings(String fileName) throws IOException {
    CSVReader reader = new CSVReader(new FileReader(new File(fileName)));
    LinkedHashMap<String, String> mappings = new LinkedHashMap<>();
    while (true) {
      String[] readNext = reader.readNext();
      if (readNext == null) {
        break;
      }
      // Skip comment lines or empty lines
      if ((readNext.length == 1 && readNext[0].isEmpty())
          || readNext[0].startsWith("#")) {
        continue;
      }
      if (readNext.length != 2) {
        System.err.println("SKIPPING " + Joiner.on(',').join(readNext));
      }
      mappings.put(readNext[0], readNext[1]);
    }
    reader.close();
    return mappings;
  }

  /**
   * Situation to warn:
   * 
   * OLD: A
   *     / \
   *    /   \ 
   *   A1    A2
   * 
   * NEW: A'(==A,A1) B(==A2)
   * 
   * This applies for group-elevated to species and subspecies elevated to
   * group. Detect by, I think, multiple mappings onto a single target. Only
   * need to warn on "A" records - e.g., for the mapping that is same-level, not
   * all the others.
   * 
   * Another:
   * 
   * OLD: A / \ / A1 --\ / \ | A11 A12 A13
   * 
   * NEW: A / \ / \ A1 A2 / \ \ A11 A12 A21(==A13)
   * 
   * Problem is warning on records of A1! Looks like the trick is recognizing
   * the existence of the unmapped node A2. So unmapped nodes have the following
   * properties: - Require notice of a new form - And require a warning on
   * migration when they: - Contain mapped children - There are sightings
   * assigned to the mapping of their prior parent(!!!) Q: what about
   * grandparent? Answer, yes, in theory, but it's hard to imagine such a
   * scenario.
   * 
   * Yet another:
   * 
   * atricapillus Group -> atricapillus/tacarcunae ssp. tacarcunae -> ssp.
   * tacarcunae ssp, atricapillus -> ssp. atricapillus ssp. costaricensis ->
   * group costaricensis
   * 
   * UGH. Nothing's unmapped. So, in fact, need to warn on records mapped to
   * former parents that contained now-elevated, when there's nothing unmapped.
   * 
   * And yet another: if a subspecies moves from group A to group B, then any
   * records for group A are suspect. *THEREFORE*, go through the mapping, find
   * any forms F where Mapping(Parent(F)) != Parent(Mapping(F)). If so, warn on
   * all records for Mapping(Parent(F)). This appears to cover all cases, and
   * behavior of grandparents is irrelevant (e.g., if you assigned a sighting to
   * a group, and that group moved from one species to another, but all the
   * subspecies of that group are unchanged, then sightings assigned to the
   * original species would need warnings, but sightings on the group or its
   * subspecies are A-OK.
   */
  private Set<String> findMappingWarnings() {
    Set<String> warnings = Sets.newLinkedHashSet();
    Multimap<String, String> formerlyContainedText = ArrayListMultimap.create();
    
    // Per above discussion
    for (Map.Entry<String, String> mapping : mappings.entrySet()) {
      Taxon oldTaxon = Preconditions.checkNotNull(oldT.getTaxon(mapping.getKey()), "No old taxon found for %s", mapping.getKey());
      String id = oldTaxon.getId();
      
      Preconditions.checkNotNull(oldTaxon, "Could not find taxon for \"%s\"", mapping.getKey());
      // I do not remember what the purpose of this code was below.  It went very badly for
      // the Clements 2019->2021 update, because this code is the critical trigger for recognizing
      // splits.  In practice, what happens is:
      //  - the code arrives here with a group or subspecies
      //  - that group or subspecies is what's getting split out of the main species
      //  - but *if* that group or subspecies couldn't be automatically mapped - so I needed to manually
      //    resolve it - then it'll be in the hardcoded list, and all of this code gets skipped, and
      //    therefore the split is ignored.  This happend for 9 species (and a few groups)
      // Depending on what this code was actually trying to do, it might work to check if oldParent has a
      // hardcoded key?
      
//      // Ignore anything with a hardcoded key
//      if (hardcoded.containsKey(oldTaxon.getId())) {
//        continue;
//      }
      
      Taxon newTaxon = newT.getTaxon(mapping.getValue());
      Preconditions.checkNotNull(newTaxon, "No new taxon for %s->%s", mapping.getKey(), mapping.getValue());
      if (oldTaxon instanceof Species) {
        Species oldSpecies = (Species) oldTaxon;
        if (oldSpecies.getStatus() != Species.Status.LC && oldSpecies.getStatus() != Species.Status.DO) {
          ((SpeciesImpl) newTaxon).setStatus(oldSpecies.getStatus());
        }
      }
      
      // Ignore extinct forms for warnings - no reason to force resolution on people
      // from forms they couldn't have possibly seen.
      if (((Species) newTaxon).getStatus() == Species.Status.EX) {
        continue;
      }
      
      if (oldTaxon.getType() == Taxon.Type.species) {
        continue;
      }

      Taxon oldParent = oldTaxon.getParent();
      String mappedOldParentId = Preconditions.checkNotNull(mappings
          .get(oldParent.getId()));
      String mappedNewParentId = newTaxon.getParent().getId();

      if (!mappedOldParentId.equals(mappedNewParentId)) {
        Taxon mappedOldParent = newT.getTaxon(mappedOldParentId);
        // If the mapped old parent would still be an ancestor of the new taxon,
        // no warning is needed - records on the old parent would still be legit
        if (TaxonUtils.isChildOfOrSame(mappedOldParent, newTaxon.getId())) {
          continue;
        }

        Taxon newTaxonToSee = newTaxon;
        if (newTaxonToSee.getType() == Taxon.Type.subspecies) {
          newTaxonToSee = newTaxonToSee.getParent();
        }
        formerlyContainedText.put(mappedOldParent.getId(), TaxonUtils.getCommonName(newTaxonToSee));
        warnings.add(mappedOldParentId);
      }
    }

    // Store taxonomic info on the species that was split
    if (copyTaxonomyNotes()) {
      for (Map.Entry<String, Collection<String>> entry : formerlyContainedText.asMap().entrySet()) {
        SpeciesImpl splitTaxon = (SpeciesImpl) newT.getTaxon(entry.getKey());
        String taxonText = "Formerly included " + Joiner.on(", ").join(
            Sets.newLinkedHashSet(entry.getValue())) + ".";
        String oldTaxonText = splitTaxon.getTaxonomicInfo();
        if (oldTaxonText != null) {
          taxonText += "  "+ oldTaxonText;
        }
        
        splitTaxon.setTaxonomicInfo(taxonText);
      }
    }
    
    return warnings;
  }
  
   /**
   * For each "warned" taxon, see if there's a "Sp." mapping that would be preferable to a warning.
   * @param hardcodedSps 
   */
  private Map<String, Resolved> lookForSpMappings(Set<String> newTaxaWithWarnings,
      Map<String, Resolved> hardcodedSps) {
    Multimap<String, String> reverseMappings = HashMultimap.create();
    Multimaps.invertFrom(Multimaps.forMap(mappings), reverseMappings);
    Iterator<String> newTaxaWithWarningIterator = newTaxaWithWarnings.iterator();
    
    // Create a sorted map
    Map<String, Resolved> spMappings = Maps.newTreeMap(new Comparator<String>() {
      @Override
      public int compare(String o1, String o2) {
        Taxon taxon1 = Preconditions.checkNotNull(oldT.getTaxon(o1), "Null taxon %s", o1);
        Taxon taxon2 = Preconditions.checkNotNull(oldT.getTaxon(o2), "Null taxon %s", o2);
        return taxon1.getTaxonomyIndex() - taxon2.getTaxonomyIndex();
      }
    });
    while (newTaxaWithWarningIterator.hasNext()) {
      String newTaxaWithWarning = newTaxaWithWarningIterator.next();
      String newCommonName = SightingTaxons.newSightingTaxon(newTaxaWithWarning).resolveInternal(newT)
          .getCommonName();
      
      // Find all the old taxa that map to this new form.  There'll often be groups and ssp. all from
      // inside one sp. - in cases where polytypic forms are split to monotypic.  Map everything up
      // to a single taxonomic level to start, as that'll clear out those
      Set<Taxon> oldTaxa = getSetOfCommonTaxonomicLevel(reverseMappings.get(newTaxaWithWarning), oldT);
      if (oldTaxa.size() > 1) {
        System.err.println("NEW TAXA:" + newCommonName);
        for (Taxon oldSp : oldTaxa) {
          System.err.println("OLD TAXA:" + SightingTaxons.newResolved(oldSp).getCommonName());
        }
        // This is probably indicative of an error, so crash-and-burn.  It happened in the past for a failed group mapping
        // "Japanese" Great Tit being mapped to "Great" Great Tit not Japanese Tit.
//        throw new IllegalArgumentException(
//            String.format("A new taxon (%s - %s) was being assigned an excessively complex mapping;  this is probably an error:\n  %s",
//                newTaxaWithWarning, newCommonName,
//                oldTaxa.stream().map(TaxonUtils::getCommonName).collect(Collectors.joining("/"))));
      }
      
      for (Taxon oldTaxon : oldTaxa) {
//      Taxon oldTaxon = Iterables.getOnlyElement(oldTaxa);
        if (oldTaxon.getContents().isEmpty()) {
          throw new IllegalArgumentException("Warning generated for taxon with no children?" + TaxonUtils.getCommonName(oldTaxon));
        }
        
        // Now, figure out what all the descendants of that map to
        Set<String> mappedChildIds = Sets.newHashSet();
        TaxonUtils.visitTaxa(oldTaxon, new TaxonVisitor() {
          @Override
          public boolean visitTaxon(Taxon oldDescendant) {
            if (oldDescendant != oldTaxon) {
              mappedChildIds.add(Preconditions.checkNotNull(mappings.get(oldDescendant.getId())));
            }
            return true;
          }
        });
        Set<Taxon> newTaxa = getSetOfCommonTaxonomicLevel(mappedChildIds, newT);
        Collection<String> simplified = TaxonUtils.simplifyTaxa(newT, newTaxa.stream().map(Taxon::getId).collect(ImmutableSet.toImmutableSet()));
        if (simplified.size() > 1 && simplified.size() < newTaxa.size()) {
          newTaxa = simplified.stream().map(newT::getTaxon).collect(ImmutableSet.toImmutableSet());
        }
           
        if (newTaxa.size() == 1) {
          System.err.println("CAN'T ELIMINATE WARNINGS FOR :" + newCommonName);
        } else {
          newTaxaWithWarningIterator.remove();
          // ... and blow off anything with an explicit mapping
          // 2022 update: As in findMappingWarnings(), I have no idea what this line is for.
          // Manual mappings to a stand-in are fine, and can't bypass detection of splits.
          // For 2022, commenting this out fixed 2 mappings (both when splits were accompanied
          // by a change of genus)
          // But putting it *back* for 2023. What this line accomplishes: in 2023,
          // the Mentawai ssp. of "Sumatran" Drongo was moved to Hair-crested Drongo.
          // It seems highly likely that virtually everyone with raw "Sumatran" Drongo
          // saw the mainland form, not the Mentawai one, so hardcoding the mapping
          // is useful, and overriding it with a sp is confusing.
          if (hasHardcodedMapping(oldTaxon)) {
            continue;
          }
          
          Resolved spResolved = SightingTaxons.newSpResolved(ImmutableSet.copyOf(newTaxa));
          spMappings.put(oldTaxon.getId(), spResolved);
        }
      }
    }
    spMappings.putAll(hardcodedSps);
    
    System.err.println("FOUND SP MAPPINGS");
    for (Map.Entry<String, Resolved> entry : spMappings.entrySet()) {
      System.err.println(TaxonUtils.getCommonName(oldT.getTaxon(entry.getKey())) + " to "
          + entry.getValue().getCommonName());
    }
    return spMappings;
  }
  
  private Set<Taxon> getSetOfCommonTaxonomicLevel(Collection<String> ids, Taxonomy taxonomy) {
    // Never go down to subspecies
    Taxon.Type largestType = Taxon.Type.group;
    for (String id : ids) {
      Taxon.Type type = taxonomy.getTaxon(id).getType();
      if (largestType.compareTo(type) <  0) {
        largestType = type;
      }
    }
    
    Set<Taxon> set = Sets.newHashSet();
    try {
      for (String id : ids) {
        set.add(TaxonUtils.getParentOfType(taxonomy.getTaxon(id), largestType));
      }
      
      // If this is groups, and there's more than 10, don't return it.  Go up to species level.
      if (largestType == Taxon.Type.species || set.size() <= 10) {
        return set;
      }
    } catch (IllegalStateException e) {
    }

    // If raising everything to a group failed (it might), then raise everything to a species.
    set.clear();
    for (String id : ids) {
      set.add(TaxonUtils.getParentOfType(taxonomy.getTaxon(id), Taxon.Type.species));
    }
    
    return set;
  }
  
  private void writeMapping(ExportLines csv) throws IOException {
    Multimap<String, String> duplicateMappings = HashMultimap.create();
    
    // Find all taxa in the new taxonomy that deserve warnings - because anything mapped 
    // there is potentially confused.
    final Set<String> newTaxaWithWarnings = findMappingWarnings();
    
    Map<String, Resolved> spMappings = disableLookForSpMappings
        ? ImmutableMap.<String, Resolved>of()
        : lookForSpMappings(newTaxaWithWarnings, hardcodedSps);
    
    for (Map.Entry<String, String> mapping : mappings.entrySet()) {
      String oldId = mapping.getKey();
      Taxon oldTaxon = oldT.getTaxon(oldId);
      String oldName;
      if (oldTaxon.getCommonName() != null) {
        oldName = oldTaxon.getCommonName();
      } else {
        oldName = TaxonUtils.getFullName(oldTaxon);
      }

      if (spMappings.containsKey(oldId)) {
        Resolved resolved = spMappings.get(oldId);
        csv.nextLine(new String[]{"MULT",
            oldTaxon.getId(),
            Joiner.on(',').join(resolved.getSightingTaxon().getIds()),
            oldName,
            resolved.getCommonName()});
        continue;
      }
      
      String newId = Preconditions.checkNotNull(mapping.getValue());
      Taxon newTaxon = newT.getTaxon(newId);
      if (newTaxon == null) {
        throw new IllegalStateException("Mapping from " + oldTaxon + " to "+ newId + " failed");
      }
      String newName;
      if (newTaxon.getCommonName() != null) {
        newName = newTaxon.getCommonName();
      } else {
        newName = TaxonUtils.getFullName(newTaxon);
      }

      if (oldTaxon instanceof SpeciesImpl && newTaxon instanceof SpeciesImpl) {
        copyInfo((SpeciesImpl) oldTaxon, (SpeciesImpl) newTaxon);
      }
      String type;
      int compare = oldTaxon.getType().compareTo(newTaxon.getType());
      if (compare < 0) {
        type = "SPLT";
      } else if (compare == 0) {
        if (LevenshteinDistance.getDefaultInstance().apply(oldName, newName) <= 3) {
          type = "SAME";
        } else {
          type = "SM??";
        }

      } else {
        type = "LUMP";
      }

      String[] array = ImmutableList.of(type, oldTaxon.getId(),
          newTaxon.getId(), oldName, newName).toArray(new String[5]);
      csv.nextLine(array);
      
      duplicateMappings.put(newId, oldId);
    }
    
    // For review purposes, log everything with duplicate mappings for review
    int duplicateMappingCount = 0;
    for (String key : duplicateMappings.keySet()) {
      Collection<String> mapped = duplicateMappings.get(key);
      if (mapped.size() > 1) {
        duplicateMappingCount++;
//        System.err.println("Multiple mapping #" + duplicateMappingCount);
//        System.err.println("To: " + key);
//        System.err.println("From: " + mapped);
      }
    }

    final Multiset<String> mappedForms = HashMultiset.create(mappings.values());
    // Output anything that doesn't have any mappings at all
    TaxonUtils.visitTaxa(newT, new TaxonVisitor() {
      @Override
      public boolean visitTaxon(Taxon taxon) {
        if (taxon.getType().compareTo(Taxon.Type.species) <= 0) {
          if (!mappedForms.contains(taxon.getId())) {
            try {
              if (newTaxaWithWarnings.contains(taxon.getId())) {
                  csv.nextLine(new String[] { "NWWR", "", taxon.getId(), "",
                      TaxonUtils.getCommonName(taxon) });
                newTaxaWithWarnings.remove(taxon.getId());
              } else {
                csv.nextLine(new String[] { "NEW", "", taxon.getId(), "",
                    TaxonUtils.getCommonName(taxon) });
              }
            } catch (IOException e) {
              throw new IllegalStateException(e);
            }
          }
        }

        return true;
      }
    });

    for (String taxonIdWithWarning : newTaxaWithWarnings) {
      Taxon newTaxonWithWarning = newT.getTaxon(taxonIdWithWarning);
      csv.nextLine(new String[] { "WARN", "", taxonIdWithWarning, "",
          TaxonUtils.getCommonName(newTaxonWithWarning) });
    }
  }

  private void copyInfo(SpeciesImpl oldTaxon, SpeciesImpl newTaxon) {
    if (oldTaxon.getType() == newTaxon.getType()) {
      List<String> alternateCommonNames = Lists.newArrayList(oldTaxon.getAlternateCommonNames());
      if (oldTaxon.getCommonName() != null) {
        String newCommonName = newTaxon.getCommonName() == null ? "" : newTaxon
            .getCommonName();
        if (LevenshteinDistance.getDefaultInstance().apply(oldTaxon.getCommonName(),
            newCommonName) > 3) {
          if (!alternateCommonNames.contains(oldTaxon.getCommonName())) {
            alternateCommonNames.add(0, oldTaxon.getCommonName());
          }
        }
      }

      newTaxon.setAlternateCommonNames(alternateCommonNames);

      List<String> alternateNames = Lists.newArrayList(oldTaxon.getAlternateNames());
      if (LevenshteinDistance.getDefaultInstance().apply(oldTaxon.getName(), newTaxon
          .getName()) > 3) {
        String oldFullName = TaxonUtils.getFullName(oldTaxon);
        if (!alternateNames.contains(oldFullName)) {
          alternateNames.add(0, oldFullName);
        }
      }      
      newTaxon.setAlternateNames(alternateNames);

      if (StringUtils.isNotEmpty(oldTaxon.getRange())
          && StringUtils.isEmpty(newTaxon.getRange())) {
        newTaxon.setRange(oldTaxon.getRange());
      }

      if (StringUtils.isNotEmpty(oldTaxon.getMiscellaneousInfo())
          && StringUtils.isEmpty(newTaxon.getMiscellaneousInfo())) {
        newTaxon.setMiscellaneousInfo(oldTaxon.getMiscellaneousInfo());
      }
    }
  }

  public static Taxonomy getTaxonomy(String filename) throws IOException,
      SAXException {
    File file = new File(filename);
    Reader reader = null;
    try {
      reader = new BufferedReader(new FileReader(file));
      return (new XmlTaxonImport()).importTaxa(reader);
    } finally {
      if (reader != null) {
        reader.close();
      }
    }
  }

  public TaxonomyComparer(Taxonomy oldT, Taxonomy newT, ImmutableMap<String, String> hardcoded,
      ImmutableMap<String, Resolved> hardcodedSps) {
    this.oldT = oldT;
    this.newT = newT;
    this.hardcoded = hardcoded;
    this.hardcodedSps = hardcodedSps;
    newHardcoded = Maps.newLinkedHashMap();
  }
  
  public TaxonomyComparer(String[] args) throws IOException, SAXException {
    new JCommander(this, args);
    System.out.println("LOADING OLD");
    oldT = getTaxonomy(oldTaxonomy);
    System.out.println("LOADING NEW");
    newT = getTaxonomy(newTaxonomy);
    
    Map<String, String> hardcoded;
    if (hardcodedMappings != null) {
      System.out.println("HARDCODED MAPPINGS FROM " + hardcodedMappings);
      hardcoded = parseHardcodedMappings(hardcodedMappings);
    } else {
      System.out.println("NO HARDCODED MAPPINGS");
      hardcoded = new LinkedHashMap<String, String>();
    }

    // Map of all harcoded Sps
    Map<String, Resolved> hardcodedSps = new LinkedHashMap<>();
    for (Map.Entry<String, String> oldAndNew : hardcoded.entrySet()) {
      if (oldT.getTaxon(oldAndNew.getKey()) == null) {
        logger.severe(String.format("Hardcoded old mapping not found: %s", oldAndNew.getKey()));
      }

      if (oldAndNew.getValue().contains("/")) {
        hardcodedSps.put(
            oldAndNew.getKey(),
            SightingTaxons.newSpTaxon(Splitter.on('/').split(oldAndNew.getValue())).resolve(newT));
      } else if (newT.getTaxon(oldAndNew.getValue()) == null) {
        logger.severe(String.format("Hardcoded new mapping not found: %s", oldAndNew.getValue()));
      }
    }
    hardcoded.keySet().removeAll(hardcodedSps.keySet());
    this.hardcoded = ImmutableMap.copyOf(hardcoded);
    this.hardcodedSps = ImmutableMap.copyOf(hardcodedSps);
    
    newHardcoded = new LinkedHashMap<>();
  }
  
  public void writeNewHardcodedList() throws IOException {
    FileWriter out = new FileWriter("/tmp/hardcoded.csv");
    CSVWriter csv = new CSVWriter(out);
    for (Map.Entry<String, String> entry : newHardcoded.entrySet()) {
      csv.writeNext(new String[]{entry.getKey(), entry.getValue()});
    }
    csv.close();
  }

  public void compare() throws IOException {
    // Run through the new taxonomy, building indices
    buildIndicesForNew();

    // First overall pass - find mappings for all species
    final Queue<Taxon> unmatchedSpecies = gatherTaxa(oldT, Taxon.Type.species);
    int foundByWholeName = 0;

    Queue<Taxon> nextPass = Lists.newLinkedList();

    // Pass 1: the easy stuff - Genus + species is unchanged; or a hardcoded
    // mapping
    // is available
    logger.info("Mapping species: pass 1 starts with: "
        + unmatchedSpecies.size());
    Taxon taxon;

    mappings.putAll(hardcoded);
    while ((taxon = unmatchedSpecies.poll()) != null) {
      if (hasHardcodedMapping(taxon)) {
        continue;
      } else {
        Taxon findByWholeSciName = newT.findSpecies(TaxonUtils.getFullName(taxon));
        if (findByWholeSciName != null) {
          mappings.put(taxon.getId(), findByWholeSciName.getId());
          // TODO(awiner): add alternate common names if available
          foundByWholeName++;
        } else {
          nextPass.add(taxon);
        }
      }
    }

    unmatchedSpecies.addAll(nextPass);
    nextPass.clear();

    logger.info("Mapping species: pass 2 starts with "
        + unmatchedSpecies.size());
    int commonNameMatches = 0;

    int lumpsFound = 0;

    // Pass 2: try to match common names. Make sure the species name is either
    // unchanged, or hasn't changed much (spelling changes and gender agreement
    // changes fall into this category), so we don't inadvertently perform false
    // mappings in the case of more complex renamings.
    while ((taxon = unmatchedSpecies.poll()) != null) {
      Collection<Taxon> collection = newCommonNames.get(
          normalizeCommonName(taxon.getCommonName()));
      if (collection.isEmpty()) {
        logger.log(Level.FINE, "No common name matches for {0}", taxon
            .getCommonName());
        nextPass.add(taxon);
      } else if (collection.size() == 1) {
        Taxon match = collection.iterator().next();
        if (ToolTaxonUtils.equalsOrClose(match.getName(), taxon.getName())) {
          logger.log(Level.FINE, "Match! {0}\nWas:{1}, now {2}", new Object[] {
              taxon.getCommonName(), TaxonUtils.getFullName(taxon),
              TaxonUtils.getFullName(match) });
          commonNameMatches++;
          mappings.put(taxon.getId(), match.getId());

          // For help with later mappings, see if this mapping identifies a
          // change in genus
          Taxon oldGenus = TaxonUtils.getParentOfType(taxon, Type.genus);
          Taxon newGenus = TaxonUtils.getParentOfType(match, Type.genus);
          if (!oldGenus.getName().equals(newGenus.getName())) {
            genusMappings.put(oldGenus.getName(), newGenus.getName());
          }
        } else {
          // Matching common name, but significant distance on the scientific
          // name, strongly suggests
          // a lump. Move to an alternate list.
          // System.out.println("PROBABLE LUMP");
          // System.out.println("OLD: "+ TaxonUtils.getFullName(taxon) + ", NEW:
          // " + TaxonUtils.getFullName(match));
          Taxon subspeciesOrGroup = ToolTaxonUtils.findMatchWithin(match, taxon.getName());
          if (subspeciesOrGroup != null) {
            lumpsFound++;

            System.out.println("LUMP: " + TaxonUtils.getFullName(taxon)
                + ", NEW: " + TaxonUtils.getFullName(subspeciesOrGroup));
            mappings.put(taxon.getId(), subspeciesOrGroup.getId());
          } else {
            // Bad news -
            logger.log(Level.WARNING, "PROBABLE LUMP BUT NOT FOUND: {0}",
                TaxonUtils.getFullName(taxon));
            nextPass.add(taxon);
          }
        }
      } else {
        // Should never happen - abort processing
        throw new IllegalStateException("Multiple common name matches for "
            + taxon.getCommonName());
      }
    }

    unmatchedSpecies.addAll(nextPass);
    nextPass.clear();

    logger.info("Mapping species: pass 3 starts with "
        + unmatchedSpecies.size());

    // Pass 3: look for lumps
    while ((taxon = unmatchedSpecies.poll()) != null) {
      Taxon oldGenus = getParentOfType(taxon, Taxon.Type.genus);

      // Try looking for a subspecies in any species, in any of the genera
      // that the old genus mapped to
      Collection<String> genusNames = Lists.newArrayList(oldGenus.getName());
      genusNames.addAll(genusMappings.get(oldGenus.getName()));

      Taxon lumpMatch = null;

      for (String genusName : genusNames) {
        Taxon newGenus = newGenusNames.get(genusName);
        if (newGenus != null) {
          Taxon subspeciesOrGroup = ToolTaxonUtils.findMatchWithin(newGenus, taxon
              .getName());
          if (subspeciesOrGroup != null) {
            // If the old species was polytypic, and it's being matched down to a
            // single subspecies, then map to the parent.  This fixes, for example:
            // "LUMP","spPoeci2hyr","sspPoeci2lughyr","Caspian Tit","Poecile lugubris hyrcanus"
            // ... where Caspian Tit's lump should have been mapped back up to Great Tit,
            // not down to the nominate subspecies.
            if (!taxon.getContents().isEmpty()
                && subspeciesOrGroup.getType() == Taxon.Type.subspecies) {
              lumpMatch = subspeciesOrGroup.getParent();
            } else {
              lumpMatch = subspeciesOrGroup;
            }
            break;
          }
        }
      }

      if (lumpMatch != null) {
        if (lumpMatch.getType() == taxon.getType()) {
          // Ignore extinct species that have gotten this far 
          if (taxon.getStatus() == Status.EX) {
            logger.log(Level.SEVERE, "Still no matches for extinct: {0}", TaxonUtils
                .getFullName(taxon));
            nextPass.add(taxon);
            continue;
          } else {
            logger.log(Level.INFO,
                "Genus + common name change: {0} -> {1}, {2} -> {3}",
                new Object[] { TaxonUtils.getFullName(taxon),
                    TaxonUtils.getFullName(lumpMatch), taxon.getCommonName(),
                    lumpMatch.getCommonName() });
          }
        } else {
          logger.log(Level.INFO, "Lump match: {0} -> {1}",
              new Object[] { TaxonUtils.getFullName(taxon),
                  TaxonUtils.getFullName(lumpMatch) });
          lumpsFound++;
        }

        mappings.put(taxon.getId(), lumpMatch.getId());
      } else {
        logger.log(Level.SEVERE, "Still no matches: {0}", TaxonUtils
            .getFullName(taxon));
        nextPass.add(taxon);
      }
    }

    // Check that all species have been mapped (but don't worry too much about extinct species)
    Collection<Taxon> notExtinct = Collections2.filter(nextPass, new Predicate<Taxon>() {
      @Override public boolean apply(Taxon t) {
        return ((Species) t).getStatus() != Status.EX;
      }
    });
    
    TaxonomyData fromTaxonData = new TaxonomyData(oldT);
    TaxonomyData toTaxonData = new TaxonomyData(newT);
    if (!notExtinct.isEmpty()) {
      ResolveSpeciesMappingFrame mappingFrame = new ResolveSpeciesMappingFrame(
          fromTaxonData, toTaxonData);
      int count = 1;
      for (Taxon notExtinctTaxon : notExtinct) {
        while (true) {
          String progress = String.format("%s of %s", count, notExtinct.size());
          final String mapped = Futures.getUnchecked(mappingFrame.resolve(
              notExtinctTaxon.getId(), progress));
          if (mapped != null) {
            if (mappings.containsValue(mapped)) {
              Set<String> keys = Maps.filterEntries(mappings, new Predicate<Map.Entry<String, String>>() {
                @Override public boolean apply(Entry<String, String> entry) {
                  return mapped.equals(entry.getValue());
                }
              }).keySet();
              if (JOptionPane.showConfirmDialog(
                  null,
                  keys,
                  "Duplicate mapping",
                  JOptionPane.OK_CANCEL_OPTION) == JOptionPane.CANCEL_OPTION) {
                continue;
              }
            }
            mappings.put(notExtinctTaxon.getId(), mapped);
            newHardcoded.put(notExtinctTaxon.getId(), mapped);
            writeNewHardcodedList();
            break;
          }
        }
        count++;
      }
    }

    String groupOfInterest = "";
    
    Queue<Taxon> unmatchedGroups = gatherTaxa(oldT, Taxon.Type.group);
    nextPass.clear();
    logger.info("Mapping groups: pass 1 starts with " + unmatchedGroups.size());

    // Groups, pass 1:
    // For each old group, find its species parent, and where that species maps
    // to in the updated taxonomy. See if the group still exists (it probably
    // will)
    Taxon group;
    while ((group = unmatchedGroups.poll()) != null) {
      if (group.getId().equals(groupOfInterest)) {
        logTaxonOfInterest(group, "Group pass 1");
      }
      // Skip anything with a pre-defined mapping
      if (hasHardcodedMapping(group)) {
        continue;
      }
      
      Taxon oldSpecies = TaxonUtils.getParentOfType(group, Taxon.Type.species);
      Taxon newSpecies = Preconditions.checkNotNull(newT.getTaxon(mappings
          .get(oldSpecies.getId())), "No mapping for %s", oldSpecies.getName());
      boolean found = false;
      
      // First check exact equals
      for (Taxon newChild : newSpecies.getContents()) {
        // Don't try to match a non-monotypic group onto a subspecies.  (Canonical
        // example:  Striped Woodhaunter, subulatus group, don't map to IOC
        // subulatus ssp.
        if (newChild.getType() == Taxon.Type.subspecies
            && !group.getContents().isEmpty()) {
          continue;
        }
        if (newChild.getName().equals(group.getName())) {
          logger
              .log(Level.FINE, "Group match: {0} -> {1}", new Object[] {
                  TaxonUtils.getFullName(group),
                  TaxonUtils.getFullName(newChild) });
          mappings.put(group.getId(), newChild.getId());
          found = true;
          break;
        }
      }

      if (!found) {
        for (Taxon newChild : newSpecies.getContents()) {
          // Don't try to match a non-monotypic group onto a subspecies.  (Canonical
          // example:  Striped Woodhaunter, subulatus group, don't map to IOC
          // subulatus ssp.
          if (newChild.getType() == Taxon.Type.subspecies
              && !group.getContents().isEmpty()) {
            continue;
          }
          if (ToolTaxonUtils.equalsOrClose(newChild.getName(), group.getName())) {
            logger
                .log(Level.INFO, "Group match: {0} -> {1}", new Object[] {
                    TaxonUtils.getFullName(group),
                    TaxonUtils.getFullName(newChild) });
            mappings.put(group.getId(), newChild.getId());
            found = true;
            break;
          }
        }
      }

      if (!found) {
        nextPass.add(group);
      }
    }

    unmatchedGroups.addAll(nextPass);
    nextPass.clear();
    logger.info("Mapping groups: pass 2 starts with " + unmatchedGroups.size());

    // Groups, pass 2:
    // Look for splits: groups elevated to species (in the same genus).
    // Also, catch group renames
    Pattern groupPattern = Pattern.compile("\\[(.*) Group\\]");
    while ((group = unmatchedGroups.poll()) != null) {
      if (group.getId().equals(groupOfInterest)) {
        logTaxonOfInterest(group, "Group pass 2");
      }
      Taxon oldSpecies = TaxonUtils.getParentOfType(group, Taxon.Type.species);
      Taxon newSpecies = Preconditions.checkNotNull(newT.getTaxon(mappings
          .get(oldSpecies.getId())));
      Taxon newGenus = TaxonUtils.getParentOfType(newSpecies, Taxon.Type.genus);

      // Extract the groupName; in a number of cases, the group name is in an
      // "[xyz Group]" form.
      String groupName = group.getName();
      Matcher matcher = groupPattern.matcher(groupName);
      if (matcher.matches()) {
        groupName = matcher.group(1);
      }
      // Find the possible match within the genus. findMatchWithinGenus() will
      // dive down to subspecies to look for matches, but then return the group. So,
      // this handles the following scenarios:
      // (1) Split:
      // (2) Group moved as part of a split:
      // Momotus momota [lessonii Group] -> Momotus coeruliceps [lessonii Group]
      // (3) Group renamed (old group name is now a subspecies in a different
      // group, because
      // the group has been renamed):
      // Falco peregrinus tundrius -> Falco peregrinus tundrius/calidus
      Taxon possibleSplit = ToolTaxonUtils.findMatchWithin(newGenus, groupName);
      if (possibleSplit != null) {
        // But a wrinkle:  if the old group had no children...
        if (group.getContents().isEmpty()) {
          // ... then this may be a case where two groups became a species, and
          // each group was sunk as a subspecies.  (This comes up for the Clements->IOC mapping.)
          // So look for a matching subspecies.
          Taxon subspecies = ToolTaxonUtils.findMatchWithin(possibleSplit, groupName);
          if (subspecies != null) {
            possibleSplit = subspecies;
          }
        } else {
          // Whereas if the group is *not* empty, and is being mapped to a single subspecies, then
          // that's almost certainly wrong:  map up to the species, as the group is no longer valid.
          if (possibleSplit.getType() == Taxon.Type.subspecies) {
            possibleSplit = TaxonUtils.getParentOfType(possibleSplit, Taxon.Type.species);
          }
        }
        
        if (possibleSplit.getType() == Taxon.Type.group) {
          logger.log(Level.FINE, "Found rename: {0} -> {1}", new Object[] {
              TaxonUtils.getFullName(group),
              TaxonUtils.getFullName(possibleSplit) });
        } else if (possibleSplit.getType() == Taxon.Type.species) {
          logger.log(Level.FINE, "Found split: {0} -> {1}", new Object[] {
              TaxonUtils.getFullName(group),
              TaxonUtils.getFullName(possibleSplit) });
        }
        mappings.put(group.getId(), possibleSplit.getId());
      } else {
        nextPass.add(group);
      }
    }


    Queue<Taxon> groupsToReconsider = Queues.newArrayDeque();
    if (!nextPass.isEmpty()) {
      if (forceUnfoundGroupsToSpecies()) {
        groupsToReconsider.addAll(nextPass);
      } else {
        logger.info("Need to resolve groups manually: pass 3 starts with " + nextPass.size());
        ResolveSubspeciesOrGroupMappingFrame mappingFrame = new ResolveSubspeciesOrGroupMappingFrame(
            fromTaxonData, toTaxonData);
        int count = 1;
        for (Taxon unmapped : nextPass) {
          if (unmapped.getId().equals(groupOfInterest)) {
            logTaxonOfInterest(unmapped, "Group pass 3");
          }
          // For groups that need to get hardcode-mapped to a sp... which is weird...
          // ... then come through here with an arbitrary one of those, and let the hardcoded sp.
          // kick in when we write out the final mapping
          if (hardcodedSps.containsKey(unmapped.getId())) {
            String arbitraryId = hardcodedSps.get(unmapped.getId()).getSightingTaxon().getIds().iterator().next();
            mappings.put(unmapped.getId(), arbitraryId);
          } else {
            while (true) {
              String progress = String.format("%s of %s", count, nextPass.size());
              final String mapped = Futures.getUnchecked(mappingFrame.resolve(
                  unmapped.getId(), null, progress));
              if (mapped != null) {
                if (mappings.containsValue(mapped)) {
                  Set<String> keys = Maps.filterEntries(mappings, new Predicate<Map.Entry<String, String>>() {
                    @Override public boolean apply(Entry<String, String> entry) {
                      return mapped.equals(entry.getValue());
                    }
                  }).keySet();
                  if (JOptionPane.showConfirmDialog(
                      null,
                      keys,
                      "Duplicate mapping",
                      JOptionPane.OK_CANCEL_OPTION) == JOptionPane.CANCEL_OPTION) {
                    continue;
                  }
                }
                mappings.put(unmapped.getId(), mapped);
                newHardcoded.put(unmapped.getId(), mapped);
                writeNewHardcodedList();
                break;
              }
            }
          }
          count++;
        }
      }
    }

    Queue<Taxon> unmatchedSsps = gatherTaxa(oldT, Taxon.Type.subspecies);
    nextPass.clear();
    logger.fine("Mapping subspecies: pass 1 starts with "
        + unmatchedSsps.size());

    // For each old subspecies, find its parent, and where that parent maps
    // to in the updated taxonomy. See if the subspecies still exists (it
    // probably will)
    Taxon ssp;
    while ((ssp = unmatchedSsps.poll()) != null) {
      // Skip anything with a pre-defined mapping
      if (hasHardcodedMapping(ssp)) {
        continue;
      }

      Taxon oldParent = getMappedParent(ssp, mappings);
      String mappingOfOldParent = mappings.get(oldParent.getId());
      Taxon probableNewParent = Preconditions.checkNotNull(
          newT.getTaxon(mappingOfOldParent));
      boolean found = false;
      // Search for exact subspecies matches
      Queue<Taxon> subspecies = gatherTaxa(probableNewParent, Taxon.Type.subspecies);
      for (Taxon newChild : subspecies) {
        if (newChild.getName().equals(ssp.getName())) {
          logger.log(Level.FINE, "Ssp match: {0} -> {1}", new Object[] {
              TaxonUtils.getFullName(ssp), TaxonUtils.getFullName(newChild) });
          mappings.put(ssp.getId(), newChild.getId());
          found = true;
          break;
        }
      }

      // Search for close subspecies matches
      if (!found) {
        for (Taxon newChild : subspecies) {
          if (ToolTaxonUtils.equalsOrClose(newChild.getName(), ssp.getName())) {
            boolean exactMatch = hasExactSubspeciesMatch(oldParent, newChild);
            if (!exactMatch) {
              logger.log(Level.FINE, "Ssp match: {0} -> {1}", new Object[] {
                  TaxonUtils.getFullName(ssp), TaxonUtils.getFullName(newChild) });
              mappings.put(ssp.getId(), newChild.getId());
              found = true;
              break;
            }
          }
        }
      }

      if (!found) {
        nextPass.add(ssp);
      }
    }

    unmatchedSsps.addAll(nextPass);
    nextPass.clear();
    logger.info("Mapping subspecies: pass 2 starts with "
        + unmatchedSsps.size());

    // Pass 2: search for subspecies moved into new groups
    while ((ssp = unmatchedSsps.poll()) != null) {
      Taxon oldSpecies = TaxonUtils.getParentOfType(ssp, Taxon.Type.species);
      Taxon probableNewSpecies = Preconditions.checkNotNull(newT
          .getTaxon(mappings.get(oldSpecies.getId())));
      boolean found = false;
      for (Taxon newChild : gatherTaxa(probableNewSpecies,
          Taxon.Type.subspecies)) {
        if (newChild.getName().equals(ssp.getName())) {
          logger.log(Level.FINE, "Ssp moved to new group: {0} -> {1}",
              new Object[] { TaxonUtils.getFullName(ssp),
                  TaxonUtils.getFullName(newChild) });
          mappings.put(ssp.getId(), newChild.getId());
          found = true;
          break;
        }
      }

      if (!found) {
        for (Taxon newChild : gatherTaxa(probableNewSpecies, Taxon.Type.subspecies)) {
          if (ToolTaxonUtils.equalsOrClose(newChild.getName(), ssp.getName())) {
            boolean exactMatch = hasExactSubspeciesMatch(oldSpecies, newChild);
            if (!exactMatch) {
              logger.log(Level.FINE, "Ssp moved to new group: {0} -> {1}",
                  new Object[] { TaxonUtils.getFullName(ssp),
                      TaxonUtils.getFullName(newChild) });
              mappings.put(ssp.getId(), newChild.getId());
              found = true;
              break;
            }
          }
        }
      }

      if (!found) {
        nextPass.add(ssp);
      }
    }

    unmatchedSsps.addAll(nextPass);
    nextPass.clear();
    logger.info("Mapping subspecies: pass 3 starts with "
        + unmatchedSsps.size());
    // Pass 3: search for subspecies promoted to groups
    while ((ssp = unmatchedSsps.poll()) != null) {
      Taxon oldSpecies = TaxonUtils.getParentOfType(ssp, Taxon.Type.species);
      Taxon probableNewSpecies = Preconditions.checkNotNull(newT
          .getTaxon(mappings.get(oldSpecies.getId())));
      boolean found = false;
      for (Taxon newChild : gatherTaxa(probableNewSpecies, Taxon.Type.group)) {
        if (newChild.getName().equals(ssp.getName())) {
          logger.log(Level.FINE, "Ssp moved to new group: {0} -> {1}",
              new Object[] { TaxonUtils.getFullName(ssp),
                  TaxonUtils.getFullName(newChild) });
          mappings.put(ssp.getId(), newChild.getId());
          found = true;
          break;
        }
      }

      if (!found) {
        for (Taxon newChild : gatherTaxa(probableNewSpecies, Taxon.Type.group)) {
          if (ToolTaxonUtils.equalsOrClose(newChild.getName(), ssp.getName())) {
            boolean exactMatch = hasExactSubspeciesMatch(oldSpecies, newChild);
            if (!exactMatch) {
              logger.log(Level.FINE, "Ssp moved to new group: {0} -> {1}",
                  new Object[] { TaxonUtils.getFullName(ssp),
                      TaxonUtils.getFullName(newChild) });
              mappings.put(ssp.getId(), newChild.getId());
              found = true;
              break;
            }
          }
        }
      }

      if (!found) {
        nextPass.add(ssp);
      }
    }

    unmatchedSsps.addAll(nextPass);
    nextPass.clear();
    logger.info("Mapping subspecies: pass 4 starts with "
        + unmatchedSsps.size());

    // Pass 4: search for subspecies now in a different species
    while ((ssp = unmatchedSsps.poll()) != null) {
      Taxon oldSpecies = TaxonUtils.getParentOfType(ssp, Taxon.Type.species);
      Taxon probableNewSpecies = Preconditions.checkNotNull(newT
          .getTaxon(mappings.get(oldSpecies.getId())));
      Taxon probableNewGenus = TaxonUtils.getParentOfType(probableNewSpecies,
          Taxon.Type.genus);
      boolean found = false;
      for (Taxon newChild : gatherTaxa(probableNewGenus, Taxon.Type.subspecies)) {
        if (newChild.getName().equals(ssp.getName())) {
          logger.log(Level.INFO, "Ssp moved to new species: {0} -> {1}",
              new Object[] { TaxonUtils.getFullName(ssp),
                  TaxonUtils.getFullName(newChild) });
          mappings.put(ssp.getId(), newChild.getId());
          found = true;
          break;
        }
      }

      if (!found) {
        for (Taxon newChild : gatherTaxa(probableNewGenus, Taxon.Type.group)) {
          if (newChild.getName().equals(ssp.getName())) {
            logger.log(Level.INFO,
                "Ssp moved to new species, elevated to group: {0} -> {1}",
                new Object[] { TaxonUtils.getFullName(ssp),
                    TaxonUtils.getFullName(newChild) });
            mappings.put(ssp.getId(), newChild.getId());
            found = true;
            break;
          }
        }
      }

      if (!found) {
        for (Taxon newChild : gatherTaxa(probableNewGenus, Taxon.Type.species)) {
          if (newChild.getName().equals(ssp.getName())) {
            logger.log(Level.INFO,
                "Ssp moved to new species, elevated to species: {0} -> {1}",
                new Object[] { TaxonUtils.getFullName(ssp),
                    TaxonUtils.getFullName(newChild) });
            mappings.put(ssp.getId(), newChild.getId());
            found = true;
            break;
          }
        }
      }

      if (!found) {
        nextPass.add(ssp);
      }
    }

    unmatchedSsps.addAll(nextPass);
    nextPass.clear();
    logger.info("Mapping subspecies: pass 5 starts with "
        + unmatchedSsps.size());

    if (true) {
      // SKIP THIS PASS.  This produced lots of pathological behavior, especially
      // when subspecies taxonomy changes significantly;  it seems to get about a
      // couple things magically right but produces a ton of false moves that could
      // break things quite badly.
      nextPass.addAll(unmatchedSsps);
    } else {
      // Pass 5, same thing but allowing minor renaming of ssps
      while ((ssp = unmatchedSsps.poll()) != null) {
        Taxon oldSpecies = TaxonUtils.getParentOfType(ssp, Taxon.Type.species);
        Taxon probableNewSpecies = Preconditions.checkNotNull(newT
            .getTaxon(mappings.get(oldSpecies.getId())));
        Taxon probableNewGenus = TaxonUtils.getParentOfType(probableNewSpecies,
            Taxon.Type.genus);
        boolean found = false;
        for (Taxon newChild : gatherTaxa(probableNewGenus, Taxon.Type.subspecies)) {
          if (ToolTaxonUtils.equalsOrClose(newChild.getName(), ssp.getName())) {
            boolean exactMatch = hasExactSubspeciesMatch(oldSpecies, newChild);
            if (!exactMatch) {
              logger.log(Level.FINE, "Ssp moved to new species: {0} -> {1}",
                  new Object[] { TaxonUtils.getFullName(ssp),
                      TaxonUtils.getFullName(newChild) });
              mappings.put(ssp.getId(), newChild.getId());
              found = true;
              break;
            }
          }
        }
  
        if (!found) {
          for (Taxon newChild : gatherTaxa(probableNewGenus, Taxon.Type.group)) {
            if (ToolTaxonUtils.equalsOrClose(newChild.getName(), ssp.getName())) {
              boolean exactMatch = hasExactSubspeciesMatch(oldSpecies, newChild);
              if (!exactMatch) {
                logger.log(Level.FINE,
                    "Ssp moved to new species, elevated to group: {0} -> {1}",
                    new Object[] { TaxonUtils.getFullName(ssp),
                        TaxonUtils.getFullName(newChild) });
                mappings.put(ssp.getId(), newChild.getId());
                found = true;
                break;
              }
            }
          }
        }
  
        if (!found) {
          for (Taxon newChild : gatherTaxa(probableNewGenus, Taxon.Type.species)) {
            if (ToolTaxonUtils.equalsOrClose(newChild.getName(), ssp.getName())) {
              boolean exactMatch = hasExactSubspeciesMatch(oldSpecies, newChild);
              if (!exactMatch) {
                logger.log(Level.FINE,
                    "Ssp moved to new species, elevated to species: {0} -> {1}",
                    new Object[] { TaxonUtils.getFullName(ssp),
                        TaxonUtils.getFullName(newChild) });
                mappings.put(ssp.getId(), newChild.getId());
                found = true;
                break;
              }
            }
          }
        }
  
        if (!found) {
          nextPass.add(ssp);
        }
      }
    }
    
    // Now, reprocess groups
    if (!groupsToReconsider.isEmpty()) {
      logger.warning(String.format("Raising %s groups to species", groupsToReconsider.size()));
      for (Taxon groupTaxon : groupsToReconsider) {
        if (!groupTaxon.getContents().isEmpty()) {
          Set<Taxon> mappedParentSpecies = Sets.newHashSet();
          for (Taxon child : groupTaxon.getContents()) {
            String childMapping = mappings.get(child.getId());
            if (childMapping != null) {
              Taxon mappedChild = newT.getTaxon(childMapping);
              Taxon speciesOfMappedChild = TaxonUtils.getParentOfType(mappedChild, Taxon.Type.species);
              mappedParentSpecies.add(speciesOfMappedChild);
            }
          }
          
          if (mappedParentSpecies.size() == 1) {
            Taxon mapTo = Iterables.getOnlyElement(mappedParentSpecies);
            logger.fine(String.format("Found unique mapping from %s to %s",
                    TaxonUtils.getCommonName(groupTaxon), TaxonUtils.getCommonName(mapTo)));
            mappings.put(groupTaxon.getId(), mapTo.getId());
            continue;
          } else if (mappedParentSpecies.size() > 1) {
            Set<String> mappedNames = FluentIterable.from(mappedParentSpecies)
                    .transform(new Function<Taxon, String>() {
                      @Override public String apply(Taxon input) {
                        return TaxonUtils.getCommonName(input);
                      }
                    }).toSet();
            logger.warning(String.format("Non-unique mapping from %s to %s",
                    TaxonUtils.getCommonName(groupTaxon), mappedNames));
          } else {
            logger.warning(String.format("Could not find any mapping for %s",
                    TaxonUtils.getCommonName(groupTaxon)));            
          }
        }
        
        Taxon parent = groupTaxon.getParent();
        String parentMapping = Preconditions.checkNotNull(mappings.get(parent.getId()));
        mappings.put(groupTaxon.getId(), parentMapping);
      }
    }

    if (!nextPass.isEmpty()) {
      if (forceUnfoundSubspeciesToSpecies()) {
        logger.warning(String.format("Raising %s ssp.s to species", nextPass.size()));
        for (Taxon unmapped : nextPass) {
          Taxon parent = unmapped.getParent();
          String parentMapping = Preconditions.checkNotNull(mappings.get(parent.getId()));
          mappings.put(unmapped.getId(), parentMapping);
        }
      } else {
        ResolveSubspeciesOrGroupMappingFrame mappingFrame = new ResolveSubspeciesOrGroupMappingFrame(
            fromTaxonData, toTaxonData);
        int count = 1;
        for (Taxon unmapped : nextPass) {
          // For groups that need to get hardcode-mapped to a spuh... which is weird...
          // ... then come through here with an arbitrary one of those, and let the hardcoded sp.
          // kick in when we write out the final mapping
          if (hardcodedSps.containsKey(unmapped.getId())) {
            String arbitraryId = hardcodedSps.get(unmapped.getId()).getSightingTaxon().getIds().iterator().next();
            mappings.put(unmapped.getId(), arbitraryId);
          } else {
            while (true) {
              String progress = String.format("%s of %s", count, nextPass.size());
              Taxon speciesParent = TaxonUtils.getParentOfAtLeastType(unmapped, Taxon.Type.species);
              Taxon mappedSpecies = newT.getTaxon(mappings.get(speciesParent.getId()));
              final String mapped = Futures.getUnchecked(mappingFrame.resolve(
                  unmapped.getId(), mappedSpecies, progress));
              if (mapped != null) {
                if (mappings.containsValue(mapped)) {
                  Set<String> keys = Maps.filterEntries(mappings, new Predicate<Map.Entry<String, String>>() {
                    @Override public boolean apply(Entry<String, String> entry) {
                      return mapped.equals(entry.getValue());
                    }
                  }).keySet();
                  if (JOptionPane.showConfirmDialog(
                      null,
                      keys,
                      "Duplicate mapping",
                      JOptionPane.OK_CANCEL_OPTION) == JOptionPane.CANCEL_OPTION) {
                    continue;
                  }
                }
                mappings.put(unmapped.getId(), mapped);
                newHardcoded.put(unmapped.getId(), mapped);
                writeNewHardcodedList();
                break;
              }
            }
          }
          count++;
        }
      }
    }
  }

  private boolean hasExactSubspeciesMatch(Taxon oldParent, Taxon newChild) {
    // But ignore close matches when there's an exact match for "newChild" in the
    // old taxonomy.  E.g., subspecies "lutea" and "luteola" of Red-billed Leiothrix.
    // In the IOC, only "lutea" exists;  it's an error to map luteola to lutea.
    boolean exactMatch = false;
    for (Taxon oldSubspecies : gatherTaxa(oldParent, Taxon.Type.subspecies)) {
      if (oldSubspecies.getName().equals(newChild.getName())) {
        exactMatch = true;
        break;
      }
    }
    return exactMatch;
  }

  private String taxaQueueToString(Iterable<Taxon> nextPass) {
    return Joiner.on('\n').join(Iterables.transform(nextPass, TaxonUtils::getFullName));
  }

  private Taxon getParentOfType(Taxon taxon, Type type) {
    return TaxonUtils.getParentOfType(taxon, type);
  }

  private void buildIndicesForNew() {
    TaxonUtils.visitTaxa(newT, new TaxonVisitor() {
      @Override
      public boolean visitTaxon(Taxon taxon) {
        switch (taxon.getType()) {
          case genus:
            newGenusNames.put(taxon.getName(), taxon);
            break;
          case species:
            newSpeciesNames.put(taxon.getName(), taxon);
            newCommonNames.put(normalizeCommonName(taxon.getCommonName()), taxon);
            break;
          default:
            break;
          }

          return true;
        }
    });

  }

  private static String normalizeCommonName(String commonName) {
    return commonName.replace('-', ' ');
  }


  private Queue<Taxon> gatherTaxa(Taxonomy taxonomy, final Taxon.Type type) {
    return gatherTaxa(taxonomy.getRoot(), type);
  }

  private Queue<Taxon> gatherTaxa(Taxon parent, final Taxon.Type type) {
    final Queue<Taxon> gathered = new LinkedList<Taxon>();
    TaxonUtils.visitTaxa(parent, new TaxonVisitor() {
      @Override
      public boolean visitTaxon(Taxon taxon) {
        if (taxon.getType() == type) {
          gathered.add(taxon);
          return false;
        }

        return true;
      }
    });

    return gathered;
  }
  
  private Taxon getMappedParent(Taxon taxon, Map<String, String> mappings) {
    while ((taxon = taxon.getParent()) != null) {
      if (mappings.containsKey(taxon.getId())) {
        break;
      }
    }
    return taxon;
  }
  
  private boolean hasHardcodedMapping(Taxon taxon) {
    return hasHardcodedMapping(taxon.getId());
  }

  private boolean hasHardcodedMapping(String taxonId) {
    return hardcoded.containsKey(taxonId);
  }

  private boolean copyTaxonomyNotes() {
    // Don't bother for IOC
    return !iocTaxonMappings;
  }
  
  // Simplify processing of IOC mappings for subspecies and groups.  Specifically, these are only used for:
  // - producing taxonomy mappings
  // - and mapping checklists
  // - and there's no *absolute* requirement that every inbound form gets mapped, as there is for a Clements
  // upgrade.
  
  private boolean forceUnfoundGroupsToSpecies() {
    return iocTaxonMappings;
  }
  
  private boolean forceUnfoundSubspeciesToSpecies() {
    return iocTaxonMappings;
  }
  
  /**
   * Function for noting a taxon of interest is encountered.  Extracted to
   * a function for simplicity of setting breakpoints.
   */
  private static void logTaxonOfInterest(Taxon taxon, String pass) {
    System.err.printf("%s - taxon of interest = %s\n", pass, taxon.getId());
  }
}
