/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.updatetaxon;

import com.google.common.util.concurrent.Futures;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.Indexer;
import com.scythebill.birdlist.model.util.IndexerBuilder;
import com.scythebill.birdlist.model.util.IndexerBuilder.Indexers;
import com.scythebill.birdlist.model.util.ToString;

class TaxonomyData {
  private final Taxonomy taxonomy;
  private Indexers sciIndexer;
  private Indexers comIndexer;

  public TaxonomyData(Taxonomy taxonomy) {
    this.taxonomy = taxonomy;
  }
  
  public Indexer<String> getSciIndexer() {
    if (this.sciIndexer == null) {
      try {
        this.sciIndexer = new IndexerBuilder(Futures.immediateFuture(getTaxonomy()),
          new ToString<Taxon>() {
            @Override public String getString(Taxon taxon) {
              return TaxonUtils.getFullName(taxon);
            }
  
            @Override public String getPreviewString(Taxon taxon) {
              return getString(taxon);
            }
          }, null, false).call();
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
      taxonomy.setScientificIndexer(sciIndexer.indexer);
    }
    return this.sciIndexer.indexer;
  }

  public Indexer<String> getCommonIndexer() {
    if (this.comIndexer == null) {
      try {
        this.comIndexer = new IndexerBuilder(Futures.immediateFuture(getTaxonomy()),
          new ToString<Taxon>() {
            @Override public String getString(Taxon taxon) {
              return taxon.getCommonName();
            }
  
            @Override public String getPreviewString(Taxon taxon) {
              return getString(taxon);
            }
          }, null, true).call();
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
      taxonomy.setCommonIndexer(comIndexer.indexer);
    }
    return this.comIndexer.indexer;
  }

  public Taxonomy getTaxonomy() {
    return taxonomy;
  }
}