package com.scythebill.birdlist.tools.util;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.clustering.DBSCANClusterer;
import org.apache.commons.math3.ml.clustering.DoublePoint;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.common.primitives.Doubles;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;

/**
 * Playing around with ConcaveHull algorithms, for computing region
 * boundaries off hotspot eBird lists.
 *
 * From:
 * @author Udo Schlegel - Udo.3.Schlegel(at)uni-konstanz.de
 * @version 1.0
 *
 * This is an implementation of the algorithm described by Adriano Moreira and Maribel Yasmina Santos:
 * CONCAVE HULL: A K-NEAREST NEIGHBOURS APPROACH FOR THE COMPUTATION OF THE REGION OCCUPIED BY A SET OF POINTS.
 * GRAPP 2007 - International Conference on Computer Graphics Theory and Applications; pp 61-68.
 *
 * https://repositorium.sdum.uminho.pt/bitstream/1822/6429/1/ConcaveHull_ACM_MYS.pdf
 *
 * With help from https://github.com/detlevn/QGIS-ConcaveHull-Plugin/blob/master/concavehull.py
 */
public class ConcaveHull {
  public static void main(String[] args) throws IOException {
    LoadedPoints loadedPoints = new LoadedPoints();
    loadedPoints.load(
        new File("/Users/awiner/Developer/scythebill-git/birdlist/tools/src/main/resources/MY-hotspot.csv"));
    loadedPoints.plot();
  }
  
  static class LoadedPoints {
    List<Point2D> loadedPoints = new ArrayList<>();
    List<List<Point2D>> points = new ArrayList<>();
    List<List<Point2D>> concaveHulls = new ArrayList<>();
    
    double epsilon = 0.1;
    int minimumPoints = 10;
    private boolean partition;
    
    public void load(File file) throws IOException {
      ImportLines importLines = CsvImportLines.fromFile(file, StandardCharsets.UTF_8);
      while (true) {
        String[] nextLine = importLines.nextLine();
        if (nextLine == null) {
          break;
        }
        
        if (nextLine.length >= 6) {
          Double latitude = Doubles.tryParse(nextLine[4]);
          Double longitude = Doubles.tryParse(nextLine[5]);
          if (latitude == null || longitude == null) {
            System.err.println("Couldn't parse lat/long from " + Joiner.on(',').join(nextLine));
          } else {
            // Flip latitude, since up should be north (larger)
            loadedPoints.add(new Point2D.Double(longitude, -latitude));
          }
        }
      }      
    }
    
    public void partition() {
      if (!partition) {
        points.clear();
        points.add(loadedPoints);
        return;
      }
      
      DBSCANClusterer<DoublePoint> dbscanClusterer = new DBSCANClusterer<>(epsilon, minimumPoints);
      List<Cluster<DoublePoint>> clusters = dbscanClusterer.cluster(loadedPoints.stream()
          .map(point -> new DoublePoint(new double[] {point.getX(), point.getY()}))
          .collect(ImmutableList.toImmutableList()));
      points.clear();
      for (Cluster<DoublePoint> cluster : clusters) {
        points.add(cluster.getPoints().stream().map(point -> {
          double[] array = point.getPoint();
          return new Point2D.Double(array[0], array[1]);
        }).collect(ImmutableList.toImmutableList()));
      }
      
      System.err.println("Cluster count: " + clusters.size());
    }

    public void calculateConcaveHulls() {
      concaveHulls.clear();
      for (List<Point2D> partition : points) {
        List<Point2D> concaveHull = calculateConcaveHull(partition, 100, 100);
        if (concaveHull == null) {
          System.err.println("Failed hull");
        } else {
          concaveHulls.add(concaveHull);
        }
      }
    }

    public void plot() {
      JFrame frame = new JFrame();
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      JPanel panel = new JPanel() {

        @Override
        public void paint(Graphics g) {
          g.setColor(Color.WHITE);
          g.fillRect(0, 0, getWidth(), getHeight());
          
          Rectangle2D boundingBox = boundingBox();
          double heightScalingRatio = boundingBox.getHeight() / getHeight();
          double widthScalingRatio = boundingBox.getWidth() / getWidth();
          double scalingRatio = Math.max(heightScalingRatio, widthScalingRatio);
          
          g.setColor(Color.RED);
          g.drawRect(0, 0, (int) (boundingBox.getWidth() / scalingRatio),
              (int) (boundingBox.getHeight() / scalingRatio));
          
          g.setColor(Color.BLUE);
          for (List<Point2D> concaveHull : concaveHulls) {
            for (int i = 1; i < concaveHull.size(); i++) {
              drawFrom(g, boundingBox, concaveHull.get(i - 1), concaveHull.get(i), scalingRatio);
            }
          }
        }
        
        private void drawFrom(
            Graphics g, Rectangle2D boundingBox, Point2D from, Point2D to, double scalingRatio) {
          g.drawLine(
              (int) ((from.getX() - boundingBox.getX()) / scalingRatio),
              (int) ((from.getY() - boundingBox.getY()) / scalingRatio),
              (int) ((to.getX() - boundingBox.getX()) / scalingRatio),
              (int) ((to.getY() - boundingBox.getY()) / scalingRatio));
                  
        }
      };
      panel.setPreferredSize(new Dimension(500, 500));
      
      JFormattedTextField epsilonField = new JFormattedTextField();
      epsilonField.setValue(Double.valueOf(0.1f));
      epsilonField.addActionListener(e -> {
        epsilon = ((Double) epsilonField.getValue()).doubleValue();
        partition();
        calculateConcaveHulls();
        panel.repaint();
      });
      JFormattedTextField minimumPointsField = new JFormattedTextField();
      minimumPointsField.setValue(Integer.valueOf(10));
      minimumPointsField.addActionListener(e -> {
        minimumPoints = ((Number) minimumPointsField.getValue()).intValue();
        partition();
        calculateConcaveHulls();
        panel.repaint();
      });
      
      JCheckBox partitionCheckBox = new JCheckBox("partition?");
      partitionCheckBox.addActionListener(e -> {
        this.partition = partitionCheckBox.isSelected();
        partition();
        calculateConcaveHulls();
        panel.repaint();
      });
      
      JPanel layout = new JPanel();
      layout.setLayout(new BoxLayout(layout, BoxLayout.Y_AXIS));
      layout.add(panel);
      layout.add(partitionCheckBox);
      layout.add(epsilonField);
      layout.add(minimumPointsField);
      frame.setContentPane(layout);
      
      frame.pack();
      frame.setVisible(true);
    }
    
    public Rectangle2D boundingBox() {
      double minX = Double.POSITIVE_INFINITY;
      double minY = Double.POSITIVE_INFINITY;
      double maxX = Double.NEGATIVE_INFINITY;
      double maxY = Double.NEGATIVE_INFINITY;
      
      for (List<Point2D> partition : points) {
        for (Point2D point : partition) {
          if (point.getX() < minX) {
            minX = point.getX();
          }
          if (point.getY() < minY) {
            minY = point.getY();
          }
          if (point.getX() > maxX) {
            maxX = point.getX();
          }
          if (point.getY() > maxY) {
            maxY = point.getY();
          }
        }
      }
      
      return new Rectangle2D.Double(minX, minY, maxX - minX, maxY - minY);
    }

    private Double euclideanDistance(Point2D a, Point2D b) {
      return a.distance(b);
    }
    
    class DistanceAndPoint {
      public DistanceAndPoint(double distance, Point2D p) {
        this.distance = distance;
        this.point = p;
      }
      final double distance;
      final Point2D point;
    }
    
    private List<Point2D> kNearestNeighbors(List<Point2D> l, Point2D q, int k) {
      List<DistanceAndPoint> nearestList = new ArrayList<>();
      for (Point2D o : l) {
          nearestList.add(new DistanceAndPoint(euclideanDistance(q, o), o));
      }

      Collections.sort(nearestList, Comparator.comparingDouble(
          o -> o.distance));

      List<Point2D> result = new ArrayList<>();

      for (int i = 0; i < Math.min(k, nearestList.size()); i++) {
          result.add(nearestList.get(i).point);
      }

      return result;
    }

    private Point2D findMinYPoint(List<Point2D> l) {
      return l.stream().min(
          Comparator.comparingDouble(Point2D::getY)).get();
    }

    private double calculateAngle(Point2D o1, Point2D o2) {
      return Math.atan2(o2.getY() - o1.getY(), o2.getX() - o1.getX());
    }

    private double angleDifference(double a1, double a2) {
      // calculate angle difference in clockwise directions as radians
      if ((a1 > 0 && a2 >= 0) && a1 > a2) {
        return Math.abs(a1 - a2);
      } else if ((a1 >= 0 && a2 > 0) && a1 < a2) {
        return 2 * Math.PI + a1 - a2;
      } else if ((a1 < 0 && a2 <= 0) && a1 < a2) {
        return 2 * Math.PI + a1 + Math.abs(a2);
      } else if ((a1 <= 0 && a2 < 0) && a1 > a2) {
        return Math.abs(a1 - a2);
      } else if (a1 <= 0 && 0 < a2) {
        return 2 * Math.PI + a1 - a2;
      } else if (a1 >= 0 && 0 >= a2) {
        return a1 + Math.abs(a2);
      } else {
        return 0.0;
      }
    }

    private List<Point2D> sortByAngle(List<Point2D> l, Point2D q, double a) {
      // Sort by angle descending
      Collections.sort(l,
          Comparator.comparingDouble(point -> angleDifference(a, calculateAngle(q, point))));
      return l;
    }

    private boolean intersect(Point2D l1p1, Point2D l1p2, Point2D l2p1, Point2D l2p2) {
      // calculate part equations for line-line intersection
      double a1 = l1p2.getY() - l1p1.getY();
      double b1 = l1p1.getX() - l1p2.getX();
      double c1 = a1 * l1p1.getX() + b1 * l1p1.getY();
      double a2 = l2p2.getY() - l2p1.getY();
      double b2 = l2p1.getX() - l2p2.getX();
      double c2 = a2 * l2p1.getX() + b2 * l2p1.getY();
      // calculate the divisor
      double tmp = (a1 * b2 - a2 * b1);

      // calculate intersection point x coordinate
      double pX = (c1 * b2 - c2 * b1) / tmp;

      // check if intersection x coordinate lies in line line segment
      if ((pX > l1p1.getX() && pX > l1p2.getX()) || (pX > l2p1.getX() && pX > l2p2.getX())
          || (pX < l1p1.getX() && pX < l1p2.getX()) || (pX < l2p1.getX() && pX < l2p2.getX())) {
        return false;
      }

      // calculate intersection point y coordinate
      double pY = (a1 * c2 - a2 * c1) / tmp;

      // check if intersection y coordinate lies in line line segment
      if ((pY > l1p1.getY() && pY > l1p2.getY()) || (pY > l2p1.getY() && pY > l2p2.getY())
          || (pY < l1p1.getY() && pY < l1p2.getY()) || (pY < l2p1.getY() && pY < l2p2.getY())) {
        return false;
      }

      return true;
    }

    private boolean pointInPolygon(Point2D p, List<Point2D> pp) {
      boolean result = false;
      for (int i = 0, j = pp.size() - 1; i < pp.size(); j = i++) {
        if ((pp.get(i).getY() > p.getY()) != (pp.get(j).getY() > p.getY())
            && (p.getX() < (pp.get(j).getX() - pp.get(i).getX()) * (p.getY() - pp.get(i).getY())
                / (pp.get(j).getY() - pp.get(i).getY()) + pp.get(i).getX())) {
          result = !result;
        }
      }
      return result;
    }

    public List<Point2D> calculateConcaveHull(List<Point2D> pointArrayList, int k, int maxK) {

      // the resulting concave hull
      ArrayList<Point2D> concaveHull = new ArrayList<>();

      // optional remove duplicates
      LinkedHashSet<Point2D> set = new LinkedHashSet<>(pointArrayList);
      ArrayList<Point2D> pointArraySet = new ArrayList<>(set);

      // k has to be greater than 3 to execute the algorithm
      int kk = Math.max(k, 3);

      // return Points if already Concave Hull
      if (pointArraySet.size() < 3) {
        return pointArraySet;
      }

      // make sure that k neighbors can be found
      kk = Math.min(kk, pointArraySet.size() - 1);

      // find first point and remove from point list
      Point2D firstPoint = findMinYPoint(pointArraySet);
      concaveHull.add(firstPoint);
      Point2D currentPoint = firstPoint;
      pointArraySet.remove(firstPoint);

      double previousAngle = 0.0;
      int step = 2;

      while ((currentPoint != firstPoint || step == 2) && pointArraySet.size() > 0) {

        // after 3 steps add first point to dataset, otherwise hull cannot be closed
        if (step == 5) {
          pointArraySet.add(firstPoint);
        }

        // get k nearest neighbors of current point
        List<Point2D> kNearestPoints = kNearestNeighbors(pointArraySet, currentPoint, kk);

        // sort points by angle clockwise
        List<Point2D> clockwisePoints = sortByAngle(kNearestPoints, currentPoint, previousAngle);

        // check if clockwise angle nearest neighbors are candidates for concave hull
        boolean its = true;
        int i = -1;
        while (its && i < clockwisePoints.size() - 1) {
          i++;

          int lastPoint = 0;
          if (clockwisePoints.get(i) == firstPoint) {
            lastPoint = 1;
          }

          // check if possible new concave hull point intersects with others
          int j = 2;
          its = false;
          while (!its && j < concaveHull.size() - lastPoint) {
            its = intersect(concaveHull.get(step - 2), clockwisePoints.get(i),
                concaveHull.get(step - 2 - j), concaveHull.get(step - 1 - j));
            j++;
          }
        }

        // if there is no candidate increase k - try again
        if (its) {
          if (k + 1 > maxK) {
            return null;
          }
          return calculateConcaveHull(pointArrayList, k + 1, maxK);
        }

        // add candidate to concave hull and remove from dataset
        currentPoint = clockwisePoints.get(i);
        concaveHull.add(currentPoint);
        pointArraySet.remove(currentPoint);

        // calculate last angle of the concave hull line
        previousAngle = calculateAngle(concaveHull.get(step - 1), concaveHull.get(step - 2));

        step++;

      }

      // Check if all points are contained in the concave hull
      boolean insideCheck = true;
      int i = pointArraySet.size() - 1;

      while (insideCheck && i > 0) {
        insideCheck = pointInPolygon(pointArraySet.get(i), concaveHull);
        i--;
      }

      // if not all points inside - try again
      if (!insideCheck) {
        if (k + 1 > maxK) {
          return null;
        }
        return calculateConcaveHull(pointArrayList, k + 1, maxK);
      } else {
        return concaveHull;
      }

    }
  }
}
