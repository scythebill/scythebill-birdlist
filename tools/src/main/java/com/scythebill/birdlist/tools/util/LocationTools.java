/**
 *  * Copyright 2023 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.ui.imports.LocationShortcuts;

public class LocationTools {
  private final LocationShortcuts locationShortcuts;

  public LocationTools() {
    this.locationShortcuts =
        new LocationShortcuts(ReportSets.newLocations(), PredefinedLocations.loadAndParse());    
  }
  
  public String getRange(Set<String> placeCodes, Set<String> introducedPlaceCodes) {
    if (placeCodes.isEmpty()) {
      return "";
    }
    CharMatcher hyphen = CharMatcher.is('-');
    List<String> countries = placeCodes.stream()
        .filter(s -> !introducedPlaceCodes.contains(s))
        .filter(hyphen::matchesNoneOf).toList();
    List<String> states = placeCodes.stream()
        .filter(s -> !introducedPlaceCodes.contains(s))
        .filter(hyphen::matchesAnyOf).toList();
    
    if (countries.size() == 0 && states.size() == 1) {
      return codeToLocationName(states.get(0));
    } else if (countries.size() == 1) {
      String country = codeToLocationName(countries.get(0));
      if (states.size() >= 1 && states.size() <= 3) {
        return country + ": " + Joiner.on(", ").join(
            states.stream().map(this::codeToLocationName).toList());
      } else {
        return country;
      }
    } else if (countries.size() <= 3) {
      return Joiner.on(", ").join(
          countries.stream().map(this::codeToLocationName).toList());
    } else {
      List<String> regions = new ArrayList<>();
      if (!Sets.intersection(NORTH_AMERICA_CODES, placeCodes).isEmpty()) {
        regions.add("North America");
      }
      if (!Sets.intersection(CENTRAL_AMERICA_CODES, placeCodes).isEmpty()) {
        regions.add("Central America");
      }
      if (!Sets.intersection(SOUTH_AMERICA_CODES, placeCodes).isEmpty()) {
        regions.add("South America");
      }
      
      ImmutableList<String> americas = ImmutableList.of("North America", "Central America", "South America");
      if (regions.containsAll(americas)) {
        regions.removeAll(americas);
        regions.add("Americas");
      } else if (!Sets.intersection(WEST_INDIES_CODES, placeCodes).isEmpty()) {
        // Don't bother adding "West Indies" if it's already present in the rest of
        // the Americas.
        regions.add("West Indies");
      }

      if (!Sets.intersection(EUROPE_CODES, placeCodes).isEmpty()) {
        regions.add("Europe");
      }
      if (!Sets.intersection(ASIA_CODES, placeCodes).isEmpty()) {
        if (regions.contains("Europe")) {
          regions.remove("Europe");
          regions.add("Eurasia");
        } else {
          regions.add("Asia");
        }
      }
      

      if (!Sets.intersection(AUSTRALASIA_CODES, placeCodes).isEmpty()) {
        regions.add("Australasia");
      }

      if (!Sets.intersection(AFRICA_CODES, placeCodes).isEmpty()) {
        regions.add("Africa");
      }
      
      if (!Sets.intersection(ARCTIC_CODES, placeCodes).isEmpty()) {
        regions.add("Arctic Ocean");
      }
      if (!Sets.intersection(ATLANTIC_CODES, placeCodes).isEmpty()) {
        regions.add("Atlantic Ocean");
      }
      if (!Sets.intersection(INDIAN_OCEAN_CODES, placeCodes).isEmpty()) {
        regions.add("Indian Ocean");
      }
      if (!Sets.intersection(PACIFIC_OCEAN_CODES, placeCodes).isEmpty()) {
        regions.add("Pacific Ocean");
      }
      if (!Sets.intersection(SOUTH_POLAR_REGION_CODES, placeCodes).isEmpty()) {
        regions.add("South Polar Region");
      }
      
      return Joiner.on(", ").join(regions);
    }
  }

  private String codeToLocationName(String code) {
    Location location = locationShortcuts.getLocationFromEBirdCode(code);
    if (location == null) {
      return code;
    }
    
    return location.getDisplayName();
  }


  private static final ImmutableSet<String> AFRICA_CODES = ImmutableSet.of(
      "DZ",
      "AO",
      "BJ",
      "BW",
      "BF",
      "BI",
      "CM",
      "CV",
      "CF",
      "TD",
      "CG",
      "CD",
      "DJ",
      "EG",
      "ES-CN",
      "GQ",
      "ER",
      "ET",
      "GA",
      "GM",
      "GH",
      "GN",
      "GW",
      "CI",
      "KE",
      "LS",
      "LR",
      "LY",
      "MW",
      "ML",
      "MR",
      "MA",
      "MZ",
      "NA",
      "NE",
      "NG",
      "RW",
      "ST",
      "SN",
      "SL",
      "SO",
      "ZA",
      "SS",
      "SD",
      "SZ",
      "TZ",
      "TG",
      "TN",
      "UG",
      "EH",
      "ZM",
      "ZW",
      "YE-HD-SOC");

  private static final ImmutableSet<String> ARCTIC_CODES = ImmutableSet.of("FO", "SJ");
      
  private static final ImmutableSet<String> ASIA_CODES = ImmutableSet.of(
      "AF",
      "AM",
      "AZ",
      "BH",
      "BD",
      "BT",
      "BN",
      "KH",
      "CN",
      "CY",
      "TL",
      "GE",
      "HK",
      "IN",
      "ID",
      "IR",
      "IQ",
      "IL",
      "JP",
      "JO",
      "KZ",
      "KW",
      "KG",
      "LA",
      "LB",
      "MO",
      "MY",
      "MN",
      "MM",
      "NP",
      "KP",
      "OM",
      "PK",
      "PS",
      "PH",
      "QA",
      "RU",
      "SA",
      "SG",
      "KR",
      "LK",
      "SY",
      "TW",
      "TJ",
      "TH",
      "TR",
      "TM",
      "AE",
      "UZ",
      "VN",
      "YE");
      
  private static final ImmutableSet<String> ATLANTIC_CODES = ImmutableSet.of("BV", "FK', SH");
  
  private static final ImmutableSet<String> AUSTRALASIA_CODES = ImmutableSet.of(
      "AU",
      "AC",
      "CS",
      "ID",
      "NC",
      "NZ",
      "NF",
      "PG",
      "SB",
      "VU");
  
  private static final ImmutableSet<String> CENTRAL_AMERICA_CODES = ImmutableSet.of(
      "BZ",
      "CR",
      "SV",
      "GT",
      "HN",
      "NI",
      "PA");
  
  private static final ImmutableSet<String> EUROPE_CODES = ImmutableSet.of(
      "AL",
      "AD",
      "AT",
      "BY",
      "BE",
      "BA",
      "BG",
      "HR",
      "CZ",
      "DK",
      "EE",
      "FI",
      "FR",
      "DE",
      "GI",
      "GR",
      "GG",
      "HU",
      "IS",
      "IE",
      "IM",
      "IT",
      "JE",
      "XK",
      "LV",
      "LI",
      "LT",
      "LU",
      "MT",
      "MD",
      "MC",
      "ME",
      "NL",
      "MK",
      "NO",
      "PL",
      "PT",
      "PT-20",
      "PT-30",
      "RO",
      "RU",
      "SM",
      "RS",
      "SK",
      "SI",
      "ES",
      "SE",
      "CH",
      "TR",
      "UA",
      "GB",
      "VA");
  
  private static final ImmutableSet<String> INDIAN_OCEAN_CODES = ImmutableSet.of(
      "IO",
      "CX",
      "CC",
      "KM",
      "TF",
      "MG",
      "MV",
      "MU",
      "YT",
      "RE",
      "SC");
  
  private static final ImmutableSet<String> NORTH_AMERICA_CODES = ImmutableSet.of("BM", "CA", "GL", "MX", "PM", "US");
  
  private static final ImmutableSet<String> PACIFIC_OCEAN_CODES = ImmutableSet.of(
      "CP",
      "CK",
      "FJ",
      "PF",
      "EC-W",
      "KI",
      "MH",
      "FM",
      "NR",
      "NU",
      "PW",
      "PN",
      "WS",
      "TO",
      "TK",
      "TV",
      "US-HI",
      "UM",
      "WF",
      "MP",
      "AS",
      "GU");
      
  private static final ImmutableSet<String> SOUTH_AMERICA_CODES = ImmutableSet.of(
      "AR",
      "AW",
      "BO",
      "BR",
      "BQ",
      "CL",
      "CO",
      "EC",
      "GF",
      "GY",
      "PY",
      "PE",
      "SR",
      "TT",
      "UY",
      "VE");
  
  private static final ImmutableSet<String> SOUTH_POLAR_REGION_CODES = ImmutableSet.of("AQ", "HM", "GS");
  
  private static final ImmutableSet<String> WEST_INDIES_CODES = ImmutableSet.of(
      "AI",
      "AG",
      "BS",
      "BB",
      "CO-SAP",
      "VG",
      "BQ",
      "KY",
      "CU",
      "DM",
      "DO",
      "GD",
      "GP",
      "HT",
      "JM",
      "MQ",
      "MS",
      "BL",
      "KN",
      "LC",
      "MF",
      "VC",
      "TC",
      "PR",
      "VI");
}
