package com.scythebill.birdlist.tools.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.io.CharStreams;
import com.google.common.io.Resources;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;

/**
 * Generates the "all-states" list.  Uses a download from eBird, combined with
 * a fair bit of hardcoding.
 */
public class ParseEBirdLocations {
  private static final RequestConfig REQUEST_CONFIG = RequestConfig.custom()
      .setConnectionRequestTimeout(5000).setConnectTimeout(30000).setSocketTimeout(30000).build();
  
  private static final ImmutableSet<String> COUNTRIES_TO_OMIT =
      ImmutableSet.of(
          "United States" // Handled directly (albeit for no particularly good reason)
          );
  private static final ImmutableMap<String, String> COUNTRIES_WITH_SPECIFIC_SUBLOCATION_TYPES=
      ImmutableMap.of("Puerto Rico", "County");
  private static final ImmutableSet<String> COUNTRIES_IN_COUNTRIES =
      ImmutableSet.of(
          "GG",
          "IM",
          "JE",
          "GB-ENG",
          "GB-SCT",
          "GB-WLS"
          );
  private static final ImmutableSet<String> LOCATIONS_TO_OMIT =
      ImmutableSet.of(
          // treated as countries so that the regions are correct
          "PT-20",
          "PT-30",
          "CO-SAP",
          "EC-W",
          "NO-22", // Should be part of Svalbard
          "ES-CE",
          "ES-ML",
          "ES-CN"
          );
  private static final ImmutableSet<String> WEST_INDIES_LOCATIONS =
      ImmutableSet.of(
          "BQ-SA", "BQ-SE");
  private static final ImmutableSet<String> SOUTH_AMERICA_LOCATIONS =
      ImmutableSet.of(
          "BQ-BO");
  private static final ImmutableSet<String> AFRICA_LOCATIONS =
      ImmutableSet.of(
          // Niger hack:  Nigeria (country) has a Niger state.
          // Explicitly adding the region to the Niger states ensured that
          // the states of Niger didn't get considered as sub-states of the
          // Niger state of Nigeria!
          "NE-1", "NE-2", "NE-3", "NE-4", "NE-5", "NE-6", "NE-7", "NE-8");
  private static final ImmutableSet<String> EUROPE_LOCATIONS = ImmutableSet.of(
      "RU-AD", "RU-ARK", "RU-AST", "RU-BA", "RU-BEL", "RU-BRY", "RU-CE",
      "RU-CU", "RU-DA", "RU-IN", "RU-IVA", "RU-KB", "RU-KC", "RU-KDA", "RU-KGD",
      "RU-KL", "RU-KIR", "RU-KLU", "RU-KO", "RU-KOS", "RU-KR", "RU-KRS",
      "RU-LEN", "RU-LIP", "RU-ME", "RU-MO", "RU-MOS", "RU-MOW", "RU-MUR",
      "RU-NEN", "RU-NGR", "RU-NIZ", "RU-ORE", "RU-ORL", "RU-PER", "RU-PNZ",
      "RU-PSK", "RU-ROS", "RU-RYA", "RU-SAM", "RU-SAR", "RU-SE", "RU-SMO",
      "RU-SPE", "RU-STA", "RU-TA", "RU-TAM", "RU-TUL", "RU-TVE", "RU-UD",
      "RU-ULY", "RU-VGG", "RU-VLA", "RU-VLG", "RU-VOR", "RU-YAR",
      "TR-22", "TR-39", "TR-59");
  private static final ImmutableSet<String> ASIA_LOCATIONS = ImmutableSet.of(
      "RU-AGB", "RU-AL", "RU-ALT", "RU-AMU", "RU-BU", "RU-CHE", "RU-CHI",
      "RU-CHU", "RU-IRK", "RU-KAM", "RU-KEM", "RU-KGN", "RU-KHA", "RU-KHM",
      "RU-KK", "RU-KYA", "RU-MAG", "RU-NVS", "RU-OMS", "RU-PRI", "RU-SA",
      "RU-SAK", "RU-SVE", "RU-TOM", "RU-TY", "RU-TYU", "RU-UOB", "RU-YAN",
      "RU-YEV",
      "TR-01", "TR-02", "TR-03", "TR-04", "TR-05", "TR-06", "TR-07", "TR-08",
      "TR-09", "TR-10", "TR-11", "TR-12", "TR-13", "TR-14", "TR-15", "TR-16",
      "TR-17", "TR-18", "TR-19", "TR-20", "TR-21", "TR-23", "TR-24", "TR-25",
      "TR-26", "TR-27", "TR-28", "TR-29", "TR-30", "TR-31", "TR-32", "TR-33",
      "TR-35", "TR-36", "TR-37", "TR-38", "TR-40", "TR-41", "TR-42",
      "TR-43", "TR-44", "TR-45", "TR-46", "TR-47", "TR-48", "TR-49", "TR-50",
      "TR-51", "TR-52", "TR-53", "TR-54", "TR-55", "TR-56", "TR-57", "TR-58",
      "TR-60", "TR-61", "TR-62", "TR-63", "TR-64", "TR-65", "TR-66", "TR-67",
      "TR-68", "TR-69", "TR-70", "TR-71", "TR-72", "TR-73", "TR-74", "TR-75",
      "TR-76", "TR-77", "TR-78", "TR-79", "TR-80", "TR-81",
      "ID-JW","ID-KA","ID-NU","ID-SL","ID-SM");

  private static final ImmutableSet<String> AUSTRALASIA_LOCATIONS = ImmutableSet.of(
      "ID-MA","ID-IJ");
  
  private static final ImmutableMap<String, String> LOCATIONS_WITH_EXPLICIT_REGIONS;
  static {
    ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
    WEST_INDIES_LOCATIONS.forEach(s -> builder.put(s, "West Indies"));
    SOUTH_AMERICA_LOCATIONS.forEach(s -> builder.put(s, "South America"));
    AFRICA_LOCATIONS.forEach(s -> builder.put(s, "Africa"));
    EUROPE_LOCATIONS.forEach(s -> builder.put(s, "Europe"));
    ASIA_LOCATIONS.forEach(s -> builder.put(s, "Asia"));
    AUSTRALASIA_LOCATIONS.forEach(s -> builder.put(s, "Australasia"));
    LOCATIONS_WITH_EXPLICIT_REGIONS = builder.build();
  }
   
  /**
   * Manually inserted Irish counties.  TODO: download this from eBird.
   */
  private static final ImmutableList<String> IRELAND_COUNTIES = ImmutableList.of(
      "IE-C-G","Galway","Connaught","County",
      "IE-C-LM","Leitrim","Connaught","County",
      "IE-C-MO","Mayo","Connaught","County",
      "IE-C-RN","Roscommon","Connaught","County",
      "IE-C-SO","Sligo","Connaught","County",
      "IE-L-CW","Carlow","Leinster","County",
      "IE-L-D","Dublin","Leinster","County",
      "IE-L-KE","Kildare","Leinster","County",
      "IE-L-KK","Kilkenny","Leinster","County",
      "IE-L-LS","Laois","Leinster","County",
      "IE-L-LD","Longford","Leinster","County",
      "IE-L-LH","Louth","Leinster","County",
      "IE-L-MH","Meath","Leinster","County",
      "IE-L-OY","Offaly","Leinster","County",
      "IE-L-WH","Westmeath","Leinster","County",
      "IE-L-WX","Wexford","Leinster","County",
      "IE-L-WW","Wicklow","Leinster","County",
      "IE-M-CE","Clare","Munster","County",
      "IE-M-CO","Cork","Munster","County",
      "IE-M-KY","Kerry","Munster","County",
      "IE-M-LK","Limerick","Munster","County",
      "IE-M-TA","Tipperary","Munster","County",
      "IE-M-WD","Waterford","Munster","County",
      "IE-U-CN","Cavan","Ulster","County",
      "IE-U-DL","Donegal","Ulster","County",
      "IE-U-MN","Monaghan","Ulster","County");
  
  private static final ImmutableList<String> CEUTA_AND_MELILLA = ImmutableList.of(
      "ES-CEUMEL","","Ceuta and Melilla (Spain)","Country",
      "ES-CE","Ceuta","Ceuta and Melilla (Spain)","City",
      "ES-ML","Melilla","Ceuta and Melilla (Spain)","City");

  /**
   * Manually inserted UK counties.  TODO: download this from eBird.
   */
  private static final ImmutableList<String> UK_COUNTIES = ImmutableList.of(
      "GB-ENG-BDF","Bedfordshire","England","County",
      "GB-ENG-BRC","Berkshire","England","County",
      "GB-ENG-BST","Bristol","England","County",
      "GB-ENG-BKM","Buckinghamshire","England","County",
      "GB-ENG-CAM","Cambridgeshire","England","County",
      "GB-ENG-CHS","Cheshire","England","County",
      "GB-ENG-CON","Cornwall","England","County",
      "GB-ENG-CMA","Cumbria","England","County",
      "GB-ENG-DBY","Derbyshire","England","County",
      "GB-ENG-DEV","Devon","England","County",
      "GB-ENG-DOR","Dorset","England","County",
      "GB-ENG-DUR","Durham","England","County",
      "GB-ENG-ERY","East Riding of Yorkshire","England","County",
      "GB-ENG-ESX","East Sussex","England","County",
      "GB-ENG-ESS","Essex","England","County",
      "GB-ENG-GLS","Gloucestershire","England","County",
      "GB-ENG-HAL","Halton","England","County",
      "GB-ENG-HAM","Hampshire","England","County",
      "GB-ENG-HEF","Herefordshire","England","County",
      "GB-ENG-HRT","Hertfordshire","England","County",
      "GB-ENG-IOW","Isle of Wight","England","County",
      "GB-ENG-KEN","Kent","England","County",
      "GB-ENG-LAN","Lancashire","England","County",
      "GB-ENG-LEC","Leicestershire","England","County",
      "GB-ENG-LIN","Lincolnshire","England","County",
      "GB-ENG-LND","London","England","County",
      "GB-ENG-MAN","Manchester","England","County",
      "GB-ENG-KWL","Merseyside","England","County",
      "GB-ENG-NFK","Norfolk","England","County",
      "GB-ENG-NYK","North Yorkshire","England","County",
      "GB-ENG-NBL","Northumberland","England","County",
      "GB-ENG-NTH","Northamptonshire","England","County",
      "GB-ENG-NTT","Nottinghamshire","England","County",
      "GB-ENG-OXF","Oxfordshire","England","County",
      "GB-ENG-RUT","Rutland","England","County",
      "GB-ENG-SHR","Shropshire","England","County",
      "GB-ENG-SOM","Somerset","England","County",
      "GB-ENG-BNS","South Yorkshire","England","County",
      "GB-ENG-STS","Staffordshire","England","County",
      "GB-ENG-STT","Stockton-on-Tees","England","County",
      "GB-ENG-SFK","Suffolk","England","County",
      "GB-ENG-SRY","Surrey","England","County",
      "GB-ENG-GAT","Tyne and Wear","England","County",
      "GB-ENG-WAR","Warwickshire","England","County",
      "GB-ENG-SAW","West Midlands","England","County",
      "GB-ENG-WIL","Wiltshire","England","County",
      "GB-ENG-WSX","West Sussex","England","County",
      "GB-ENG-WKF","West Yorkshire","England","County",
      "GB-ENG-WOR","Worcestershire","England","County",
      "GB-NIR-ANT","Antrim","Northern Ireland","County",
      "GB-NIR-ARM","Armagh","Northern Ireland","County",
      "GB-NIR-BFS","Belfast","Northern Ireland","County",
      "GB-NIR-DRY","Derry","Northern Ireland","County",
      "GB-NIR-DOW","Down","Northern Ireland","County",
      "GB-NIR-FER","Fermanagh","Northern Ireland","County",
      "GB-NIR-NYM","Newry and Mourne","Northern Ireland","County",
      "GB-NIR-OMH","Omagh","Northern Ireland","County",
      "GB-SCT-ABE","Aberdeen City","Scotland","County",
      "GB-SCT-ABD","Aberdeenshire","Scotland","County",
      "GB-SCT-ANS","Angus","Scotland","County",
      "GB-SCT-AGB","Argyll and Bute","Scotland","County",
      "GB-SCT-CLK","Clackmannanshire","Scotland","County",
      "GB-SCT-DGY","Dumfries and Galloway","Scotland","County",
      "GB-SCT-DND","Dundee City","Scotland","County",
      "GB-SCT-EAY","East Ayrshire","Scotland","County",
      "GB-SCT-EDU","East Dunbartonshire","Scotland","County",
      "GB-SCT-ELN","East Lothian","Scotland","County",
      "GB-SCT-EDH","Edinburgh","Scotland","City",
      "GB-SCT-ELS","Eilean Siar","Scotland","County",
      "GB-SCT-FAL","Falkirk","Scotland","County",
      "GB-SCT-FIF","Fife","Scotland","County",
      "GB-SCT-GLG","Glasgow","Scotland","City",
      "GB-SCT-HLD","Highland","Scotland","County",
      "GB-SCT-MLN","Midlothian","Scotland","County",
      "GB-SCT-MRY","Moray","Scotland","County",
      "GB-SCT-NAY","North Ayrshire","Scotland","County",
      "GB-SCT-NLK","North Lanarkshire","Scotland","County",
      "GB-SCT-ORK","Orkney Islands","Scotland","County",
      "GB-SCT-PKN","Perthshire and Kinross","Scotland","County",
      "GB-SCT-RFW","Renfrewshire","Scotland","County",
      "GB-SCT-SCB","Scottish Borders","Scotland","County",
      "GB-SCT-ZET","Shetland Islands","Scotland","County",
      "GB-SCT-SAY","South Ayrshire","Scotland","County",
      "GB-SCT-SLK","South Lanarkshire","Scotland","County",
      "GB-SCT-STG","Stirling","Scotland","County",
      "GB-SCT-WDU","West Dunbartonshire","Scotland","County",
      "GB-SCT-WLN","West Lothian","Scotland","County",
      "GB-WLS-AGY","Anglesey","Wales","County",
      "GB-WLS-BGW","Blaenau Gwent","Wales","County",
      "GB-WLS-BGE","Bridgend","Wales","County",
      "GB-WLS-CAY","Caerphilly","Wales","County",
      "GB-WLS-CRF","Cardiff","Wales","County",
      "GB-WLS-CMN","Carmarthenshire","Wales","County",
      "GB-WLS-CGN","Ceredigion","Wales","County",
      "GB-WLS-CWY","Conwy","Wales","County",
      "GB-WLS-DEN","Denbighshire","Wales","County",
      "GB-WLS-FLN","Flintshire","Wales","County",
      "GB-WLS-GWN","Gwynedd","Wales","County",
      "GB-WLS-MTY","Merthyr Tydfil","Wales","County",
      "GB-WLS-MON","Monmouthshire","Wales","County",
      "GB-WLS-NTL","Neath Port Talbot","Wales","County",
      "GB-WLS-NWP","Newport","Wales","County",
      "GB-WLS-PEM","Pembrokeshire","Wales","County",
      "GB-WLS-POW","Powys","Wales","County",
      "GB-WLS-RCT","Rhondda, Cynon, Taff","Wales","County",
      "GB-WLS-SWA","Swansea","Wales","County",
      "GB-WLS-TOF","Torfaen","Wales","County",
      "GB-WLS-VGL","Vale of Glamorgan","Wales","County",
      "GB-WLS-WRX","Wrexham","Wales","County");
  
 public static void main(String[] args) throws IOException {
   String countries = downloadCountries();
    
    File out = new File(args[0]);
    
    LocationSet locations = ReportSets.newReportSet(new TaxonomyImpl()).getLocations();
    PredefinedLocations predefinedLocations = PredefinedLocations.loadAndParse();
    ImportLines fromCountries = CsvImportLines.fromReader(new StringReader(countries));
    
    Multimap<String, String[]> linesByCountry = Multimaps.newMultimap(
        new TreeMap<>(),
        ArrayList::new);
    
    // Skip the header
    fromCountries.nextLine();
    
    while (true) {
      String[] countryLine = fromCountries.nextLine(); 
      if (countryLine == null) {
        break;
      }
    
      String countryCode = countryLine[0];
      String countryName = countryLine[1];
      
      Location countryLoc = Locations.getLocationByCodePossiblyCreating(
          locations, predefinedLocations, countryCode);
      Preconditions.checkNotNull(countryLoc, "No country with code %s", countryCode);
      String country = countryLoc.getModelName();
      if (COUNTRIES_IN_COUNTRIES.contains(countryCode)) {
        linesByCountry.put(country, new String[]{
            countryCode,
            country,
            "United Kingdom",
            "Country in Country"
         });          
      } else {
        linesByCountry.put(country, new String[]{
            countryCode,
            "",
            country,
            "Country"
         });          
      }
      if (!country.equals(countryName)) {
        System.err.printf("Scythebill name: %s;  eBird name: %s\n",
            country,
            countryName);
      }
      
//      toLine[0] = line[1];
//      toLine[1] = line[2];
//      toLine[2] = country;
//      toLine[3] = COUNTRIES_IN_COUNTRIES.contains(line[1])
//          ? "Country in Country"
//          : COUNTRIES_WITH_SPECIFIC_SUBLOCATION_TYPES.getOrDefault(country, "State");
//      linesByCountry.put(country, toLine);
      String states = downloadStates(countryCode);
      ImportLines fromStates= CsvImportLines.fromReader(new StringReader(states));
      // Skip the header
      fromStates.nextLine();
      
      List<String[]> parsedStates = new ArrayList<>(); 
      while (true) {
        String[] stateLine = fromStates.nextLine(); 
        if (stateLine == null) {
          break;
        }
        parsedStates.add(stateLine);
      }
      
      if (parsedStates.isEmpty()) {
        // For historical reasons, write at least one empty "state".  Would be
        // nice to just drop these countries altogether from the states list.
        linesByCountry.put(country,
            new String[] {countryCode + "-", country, country, "State"});
      } else {
        // Sort by the abbreviation (the first part)
        Collections.sort(parsedStates, Comparator.comparing(line -> line[0]));
        
        String locationType = 
            COUNTRIES_WITH_SPECIFIC_SUBLOCATION_TYPES.getOrDefault(country, "State");
        for (String[] stateLine : parsedStates) {
          String stateCode = stateLine[0];
          String stateName = stateLine[1];
          String locationTypeForState = COUNTRIES_IN_COUNTRIES.contains(stateCode)
              ? "Country in Country" : locationType;
          linesByCountry.put(country,
              new String[] {stateCode, stateName, country, locationTypeForState});
        }
      }
    }
    
    // TODO: would like export that doesn't quote everything.  As is,
    // need to take the export, open in a Spreadsheet, and resave
    CSVFormat minimalQuoting = CSVFormat.RFC4180.withQuoteMode(QuoteMode.MINIMAL).withRecordSeparator('\n');
    Writer to = Files.newBufferedWriter(out.toPath(), StandardCharsets.UTF_8);
    try (CSVPrinter printer = new CSVPrinter(to, minimalQuoting)) {
      printer.printRecord(
          "CODE","STATE/PROVINCE NAME","COUNTRY NAME","TYPE");
      
      for (String country : linesByCountry.keySet()) {
        if (COUNTRIES_TO_OMIT.contains(country)) {
          continue;
        }
        
        Collection<String[]> lines = linesByCountry.get(country);
        for (String[] line : lines) {
          String code = line[0];
          // Skip anything like "AR-" if it isn't the only child
          // TODO: maybe skip everything also that is just two lines?
          if (code.length() == 3 && lines.size() > 2) {
            continue;
          }
          // Skip anything that is specially handled
          if (LOCATIONS_TO_OMIT.contains(code)) {
            continue;
          }
          
          if (LOCATIONS_WITH_EXPLICIT_REGIONS.containsKey(code)) {
            Preconditions.checkState(line.length == 4);
            printer.printRecord(
                line[0],
                line[1],
                line[2],
                line[3],
                LOCATIONS_WITH_EXPLICIT_REGIONS.get(code));
          } else {
            printer.printRecord((Object[]) line);
          }
        }
        
        if ("Ireland".equals(country)) {
          // eBird changed from Irish counties to provinces;  to avoid losing data,
          // Scythebill kept the counties.
          for (int i = 0; i < IRELAND_COUNTIES.size(); i+= 4) {
            printer.printRecord(IRELAND_COUNTIES.subList(i, i + 4));
          }          
        } else if ("Spain".equals(country)) {
          // Ceuta and Melilla are treated as parts of a separate African
          // "country"
          for (int i = 0; i < CEUTA_AND_MELILLA.size(); i+= 4) {
            printer.printRecord(CEUTA_AND_MELILLA.subList(i, i + 4));
          }          
        } else if ("United Kingdom".equals(country)) {
          // Include the Great Britain counties explicitly
          for (int i = 0; i < UK_COUNTIES.size(); i+= 4) {
            printer.printRecord(UK_COUNTIES.subList(i, i + 4));
          }          
        }
      }
    }
    
    System.out.println("Updated states are in " + out);
  }


  private static String downloadCountries() throws IOException {
    return downloadEbird("country/world.csv");
  }

  private static String downloadStates(String countryCode) throws IOException {
    return downloadEbird(String.format("subnational1/%s.csv", countryCode));
  }

  private static String downloadEbird(String resource) throws IOException {
    // Read and write from a cache file so that - at least once the first pass is done -
    // iterating on the code doesn't require re-downloading every location
    File cacheFile = new File("/tmp/ebird-locations-" + resource.replace('/', '-'));
    if (cacheFile.exists()) {
      return new String(Files.readAllBytes(cacheFile.toPath()), StandardCharsets.UTF_8);
    }
    
    URI uri = URI.create("https://api.ebird.org/v2/ref/region/list/" + resource);
    HttpGet httpGet = new HttpGet(uri);
    String ebirdKey = Resources.toString(Resources.getResource("ebirdapikey.txt"), Charsets.UTF_8);
    httpGet.setHeader("X-eBirdApiToken", ebirdKey);
    httpGet.setConfig(REQUEST_CONFIG);

    CloseableHttpClient httpClient = HttpClients.custom().setMaxConnPerRoute(3).setMaxConnTotal(20)
        .setDefaultCookieStore(new BasicCookieStore())
        .setConnectionManager(new PoolingHttpClientConnectionManager()).build();
    try (CloseableHttpResponse get = httpClient.execute(httpGet)) {
      HttpEntity entity = get.getEntity();
      System.out.println("Loading " + uri);
      InputStream inputStream = new BufferedInputStream(entity.getContent());
      Reader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
      String string = CharStreams.toString(reader);
      
      Files.write(cacheFile.toPath(), string.getBytes(StandardCharsets.UTF_8));
      return string;
    }
  }
}
