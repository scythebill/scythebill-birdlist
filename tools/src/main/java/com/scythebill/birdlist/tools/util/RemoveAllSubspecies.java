/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.util;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.List;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.google.common.base.Charsets;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.xml.XmlReportSetExport;
import com.scythebill.birdlist.model.xml.XmlReportSetImport;

public class RemoveAllSubspecies {
  public static void main(String[] args) throws Exception {
    new RemoveAllSubspecies(args).run();
  }

  @Parameter(names = "-report", required = true)
  private String report;

  @Parameter(names = "-clements", required = true)
  private String clements;

  @Parameter(names = "-output", required = true)
  private String output;


  RemoveAllSubspecies(String[] args) {
    new JCommander(this, args);
  }
  
  public void run() throws Exception {
    Taxonomy taxonomy = ToolTaxonUtils.getTaxonomy(clements);
    FileReader fileReader = new FileReader(new File(report));
    ReportSet reportSet = new XmlReportSetImport().importReportSet(
        fileReader, taxonomy, Optional.<MappedTaxonomy>absent(), null);
    fileReader.close();
    
    List<Sighting> sightingsToRemove = Lists.newArrayList();
    List<Sighting> sightingsToAdd = Lists.newArrayList();
    for (Sighting sighting : reportSet.getSightings()) {
      if (!sighting.getTaxonomy().isBuiltIn()) {
        continue;
      }
      Resolved resolved = sighting.getTaxon().resolve(taxonomy);
      if (resolved.getSmallestTaxonType() != Taxon.Type.species) {
        SightingTaxon species = resolved.getParentOfAtLeastType(Taxon.Type.species);
        Sighting updated = sighting.asBuilder().setTaxon(species).build();
        sightingsToRemove.add(sighting);
        sightingsToAdd.add(updated);
      }
    }
    
    reportSet.mutator()
        .removing(sightingsToRemove)
        .adding(sightingsToAdd)
        .mutate();
    
    FileWriter fileWriter = new FileWriter(output);
    new XmlReportSetExport().export(fileWriter, Charsets.UTF_8.name(), reportSet, taxonomy);
    fileWriter.close();
    
    System.exit(0);
  }
}
