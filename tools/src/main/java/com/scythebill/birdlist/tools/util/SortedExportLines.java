package com.scythebill.birdlist.tools.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.google.common.base.Joiner;
import com.scythebill.birdlist.model.io.ExportLines;

/**
 * Writes lines in sorted order.
 */
public class SortedExportLines implements ExportLines {
  private final ExportLines exportLines;
  private final ArrayList<String[]> lines;

  public SortedExportLines(ExportLines exportLines) {
    this.exportLines = exportLines;
    this.lines = new ArrayList<>();        
  }
  
  @Override
  public void close() throws IOException {
    Joiner joiner = Joiner.on(',');
    Collections.sort(lines,
        Comparator.comparing(line -> joiner.join(line)));
    for (String[] line : lines) {
      exportLines.nextLine(line);
    }
    exportLines.close();
  }

  @Override
  public void nextLine(String[] line) throws IOException {
    lines.add(line);
  }
}
