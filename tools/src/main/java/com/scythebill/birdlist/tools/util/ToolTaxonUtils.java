/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.tools.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.text.similarity.LevenshteinDistance;
import org.xml.sax.SAXException;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.MoreExecutors;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.TaxonomyIndexer;
import com.scythebill.birdlist.model.xml.XmlTaxonImport;

public class ToolTaxonUtils {
  private final static Logger logger = Logger.getLogger(ToolTaxonUtils.class.getName());
  
  /** Look for a group/subspecies match within a genus. */
  public static Taxon findMatchWithin(final Taxon parent, final String name) {
    final List<Taxon> matches = Lists.newArrayList();
    // Do exact match first
    TaxonUtils.visitTaxa(parent, new TaxonVisitor() {
      @Override
      public boolean visitTaxon(Taxon taxon) {
        if (taxon != parent && name.equals(taxon.getName())) {
          matches.add(taxon);
          return false;
        }

        return true;
      }
    });

    // Then do close-enough match. Note: do not do both together,
    // as that causes confusion when names are close, e.g. Cinclodes
    // albiventris and albidiventris!
    if (matches.isEmpty()) {
      TaxonUtils.visitTaxa(parent, new TaxonVisitor() {
        @Override
        public boolean visitTaxon(Taxon taxon) {
          if (taxon != parent && equalsOrClose(name, taxon.getName())) {
            matches.add(taxon);
            return false;
          }

          return true;
        }
      });
    }

    if (matches.isEmpty()) {

      return null;
    }

    // >1 match indicates something's gone badly wrong. Terminate.
    if (matches.size() > 1) {
      // This once threw an exception, but nastiness ensued for:
      // Lonchura nana -> Lemuresthes nana, while left behind:
      // Lonchura maja and Lonchura vana!
      logger.log(Level.SEVERE, "Multiple equivalent matches for {0} in {1}: {2}",
          new Object[]{name, parent.getName(), Joiner.on(',').join(matches)});
      return null;
      // Taxon bestFit = matches.get(0);
      // for (Taxon match : Iterables.skip(matches, 1)) {
      // if (bestFit.getType() == match.getType()) {
      // throw new IllegalStateException(
      // "Multiple equivalent matches for " + name + " in " + parent.getName() +
      // "\n" +
      // TaxonUtils.getFullName(bestFit) + " and " +
      // TaxonUtils.getFullName(match));
      // }
      //        
      // if (bestFit.getType().compareTo(match.getType()) < 0) {
      // bestFit = match;
      // }
      // }
    }

    Taxon match = matches.get(0);
    // Prefer groups over subspecies. This pure heuristic assumes that when
    // species get
    // lumped, and a sighting was only at the species level, it's probable that
    // the
    // sighting wasn't identified to species.
    if ((match.getType() == Type.subspecies)
        && (match.getParent().getType() == Type.group)) {
      match = match.getParent();
    }

    return match;
  }

  public static boolean equalsOrClose(String a, String b) {
    return (a.equals(b) || LevenshteinDistance.getDefaultInstance().apply(a, b) <= 2);
  }


  public static MappedTaxonomy getMappedTaxonomy(
          String baseTaxonomy, String filename) throws IOException,
          SAXException {
    Taxonomy base = getTaxonomy(baseTaxonomy);
    return getMappedTaxonomy(base, filename);
  }

  public static Taxonomy getTaxonomy(String filename) throws IOException, SAXException {
    Reader reader = null;
    try {
      File file = new File(filename);
      reader = new BufferedReader(new FileReader(file));
      Taxonomy taxonomy = (new XmlTaxonImport()).importTaxa(reader);
      Futures.getUnchecked(new TaxonomyIndexer(Futures.immediateFuture(taxonomy))
              .load(MoreExecutors.newDirectExecutorService()));
      return taxonomy;
    } finally {
      if (reader != null) {
        reader.close();
      }
    }
  }

  public static MappedTaxonomy getMappedTaxonomy(Taxonomy base, String filename) throws IOException, SAXException {
    Reader reader = null;
    try {
      File file = new File(filename);
      reader = new BufferedReader(new FileReader(file));
      MappedTaxonomy taxonomy = (new XmlTaxonImport()).importMappedTaxa(reader, base);
      
      Futures.getUnchecked(new TaxonomyIndexer(Futures.immediateFuture(taxonomy)).load(
          MoreExecutors.newDirectExecutorService()));
      return taxonomy;
    } finally {
      if (reader != null) {
        reader.close();
      }
    }
  }
}
