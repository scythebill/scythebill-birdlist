/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.util.Map;

import javax.swing.Action;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Global object responsible for vending shared top-level actions.
 * These actions delegate to local actions that may vary depending on
 * which window is active or which field has focus.
 */
@Singleton
public class ActionBroker {
  public static final String CUT = "cut";
  public static final String COPY = "copy";
  public static final String PASTE = "paste";
  public static final String SAVE = "save";
  public static final String SAVE_A_COPY_AS = "saveACopyAs";
  public static final String CLOSE = "close";
  public static final String PRINT = "print";
  public static final String EXPORT = "export";
  public static final String OPEN_CONTAINING = "openContaining";
  public static final String IMPORT = "import";
  public static final String MANAGE_TAXONOMIES = "manageTaxonomies";
  public static final String IDENTIFY_CHECKLIST_ERRORS = "identifyChecklistErrors";
  public static final String IDENTIFY_CHECKLIST_RARITIES = "identifyChecklistRarities";
  public static final String RECONCILE_AGAINST_CHECKLISTS = "recocileAgainstChecklists";
  public static final String IMPORT_CHECKLISTS = "importChecklists";
  public static final String FULL_EXPORT = "fullExport";
  public static final String PREFERENCES = "preferences";
  
  private final Map<String, DelegatedAction> actions = Maps.newHashMap();
  
  @Inject
  public ActionBroker() {
  }
  
  public Action createAction(String name) {
    if (actions.containsKey(name)) {
      return actions.get(name);
    }
    
    Preconditions.checkState(!actions.containsKey(name));
    
    DelegatedAction action = new DelegatedAction();
    actions.put(name, action);
    return action;
  }
  
  public void publishAction(String name, Action delegateTo) {
    DelegatedAction action = actions.get(name);
    if (action != null) {
      action.setDelegate(delegateTo);
    }
  }
  
  public void unpublishAction(String name, Action delegateTo) {
    DelegatedAction action = actions.get(name);
    if (action != null && action.getDelegate() == delegateTo) {
      action.setDelegate(null);
    }
  }
}
