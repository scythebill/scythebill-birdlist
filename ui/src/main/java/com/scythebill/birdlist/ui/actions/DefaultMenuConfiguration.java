/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import com.google.inject.Inject;
import com.scythebill.birdlist.ui.fonts.DecreaseFontSizeAction;
import com.scythebill.birdlist.ui.fonts.IncreaseFontSizeAction;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.AboutFrame;
import com.scythebill.birdlist.ui.util.Alerts;

public class DefaultMenuConfiguration extends MinimalMenuConfiguration {

  private final QuitAction quitAction;
  private final AboutFrame aboutFrame;
  private final Alerts alerts;

  @Inject
  public DefaultMenuConfiguration(
      ActionBroker actionBroker,
      OpenAction openAction,
      NewReportSetAction newAction,
      IncreaseFontSizeAction increaseFontSizeAction,
      DecreaseFontSizeAction decreaseFontSizeAction,
      QuitAction quitAction,
      AboutFrame aboutFrame,
      Alerts alerts) {
    super(actionBroker, openAction, newAction, increaseFontSizeAction, decreaseFontSizeAction,
        alerts);
    this.quitAction = quitAction;
    this.aboutFrame = aboutFrame;
    this.alerts = alerts;
  }

  @Override
  public JMenu getFileMenu() {
    JMenu menu = super.getFileMenu();
    
    menu.addSeparator();

    int keyModifier = Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx();
    JMenuItem quitItem = new JMenuItem();
    quitItem.setAction(quitAction);
    quitItem.setText(Messages.getMessage(Name.QUIT_MENU));
    quitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, keyModifier));
    menu.add(quitItem);
    
    return menu;
  }

  @Override
  public JMenu getHelpMenu() {
    JMenu menu = new JMenu();
    menu.setText(Messages.getMessage(Name.HELP_MENU));

    JMenuItem aboutItem = new JMenuItem();
    aboutItem.setAction(new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent event) {
        aboutFrame.show();
      }      
    });
    aboutItem.setText(Messages.getMessage(Name.ABOUT_SCYTHEBILL));
    menu.add(aboutItem);

    JMenuItem showManualItem = new JMenuItem();
    showManualItem.setAction(new ShowManualAction(alerts));
    showManualItem.setText(Messages.getMessage(Name.SCYTHEBILL_MANUAL));
    menu.add(showManualItem);

    JMenuItem emailGroup = new JMenuItem();
    emailGroup.setAction(new ShowEMailGroupAction(alerts));
    emailGroup.setText(Messages.getMessage(Name.SCYTHEBILL_EMAIL_GROUP));
    menu.add(emailGroup);

    JMenuItem downloadLatest = new JMenuItem();
    downloadLatest.setAction(new DownloadLatestAction(alerts));
    downloadLatest.setText(Messages.getMessage(Name.DOWNLOAD_LATEST));
    menu.add(downloadLatest);

    JMenuItem reportIssueItem = new JMenuItem();
    reportIssueItem.setAction(new ReportIssueAction(alerts));
    reportIssueItem.setText(Messages.getMessage(Name.REPORT_ISSUE));
    menu.add(reportIssueItem);

    JMenuItem openSourceItem = new JMenuItem();
    openSourceItem.setAction(new OpenSourceLicensesAction());
    openSourceItem.setText(Messages.getMessage(Name.OPEN_SOURCE_LICENSES));
    menu.add(openSourceItem);

    return menu;
  }

}
