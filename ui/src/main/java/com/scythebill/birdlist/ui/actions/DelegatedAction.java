/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 * Action that updates its enabled state and action behavior
 * as a delegate is updated. 
 */
public class DelegatedAction extends AbstractAction
     implements PropertyChangeListener {
  private Action delegate;

  public DelegatedAction() {
    setEnabled(false);
  }
  
  public Action getDelegate() {
    return delegate;
  }

  public void setDelegate(Action delegate) {
    if (this.delegate != null) {
      this.delegate.removePropertyChangeListener(this);
      if (this.delegate instanceof Attachable) {
        ((Attachable) this.delegate).unattach();
      }
    }
    
    this.delegate = delegate;
    if (delegate == null) {
      setEnabled(false);
    } else {
      setEnabled(delegate.isEnabled());
      delegate.addPropertyChangeListener(this);
      if (delegate instanceof Attachable) {
        ((Attachable) delegate).attach();
      }
    }
  }

  @Override public void actionPerformed(ActionEvent event) {
    if (delegate != null) {
      delegate.actionPerformed(event);
    }
  }

  @Override public void propertyChange(PropertyChangeEvent event) {
    if ("enabled".equals(event.getPropertyName())) {
      setEnabled(this.delegate.isEnabled());
    }
  }
}