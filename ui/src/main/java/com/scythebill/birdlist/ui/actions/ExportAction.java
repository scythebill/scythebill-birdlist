/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.google.inject.Inject;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Action for export... which just navigates to Reports..
 */
public class ExportAction extends AbstractAction {
  private final NavigableFrame navigableFrame;

  @Inject
  public ExportAction(NavigableFrame navigableFrame) {
    super(Messages.getMessage(Name.EXPORT_MENU), null);
    this.navigableFrame = navigableFrame;
  }

  @Override
  public void actionPerformed(ActionEvent evt) {
    navigableFrame.navigateTo("reports");
  }
}
