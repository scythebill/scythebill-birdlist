/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.awt.Component;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 * Forwards ActionEvents to a delegate.  (Does not currently forward
 * any other Action APIs.)
 */
public class ForwardingAction extends AbstractAction {
  private final Component forwardingComponent;
  private final Action delegate;

  protected ForwardingAction(Component forwardingComponent, Action delegate) {
    this.forwardingComponent = forwardingComponent;
    this.delegate = delegate;
  }
  
  @Override public void actionPerformed(ActionEvent event) {
    delegate.actionPerformed(new ActionEvent(
        forwardingComponent,
        event.getID(),
        event.getActionCommand(),
        event.getWhen(),
        event.getModifiers()));
  }

}
