/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.joda.time.LocalDate;
import org.joda.time.chrono.GJChronology;

import com.google.common.base.Predicates;
import com.google.common.io.ByteSink;
import com.google.common.io.Files;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.scythebill.birdlist.model.export.FullReportExport;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.query.QueryProcessor;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;

/**
 * Implements the full export-to-csv action.
 */
public class FullExportAction extends AbstractAction {
  private final FileDialogs fileDialogs;
  private final ReportSet reportSet;
  private final TaxonomyStore taxonomyStore;
  private final Alerts alerts;
  private final String reportSetFileName;

  @Inject
  public FullExportAction(
      FileDialogs fileDialogs,
      ReportSet reportSet,
      @Named("com.adamwiner.birdlist.ui.reportFile") String reportSetFileName,
      TaxonomyStore taxonomyStore,
      Alerts alerts) {
    this.fileDialogs = fileDialogs;
    this.reportSet = reportSet;
    this.reportSetFileName = reportSetFileName;
    this.taxonomyStore = taxonomyStore;
    this.alerts = alerts;
  }
  
  static private Frame getParentFrame(Object object) {
    Component c = (Component) object;
    while (c != null) {
      if (c instanceof Frame) {
        return (Frame) c;
      }

      c = c.getParent();
    }

    return null;
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    String today = PartialIO.toString(new LocalDate(GJChronology.getInstance()));
    String exportFileName = String.format("export-%s-%s.csv",
        reportSetFileName,
        today);
    
    final File file = fileDialogs.saveFile(getParentFrame(event.getSource()),
        Messages.getMessage(Name.PICK_FILE_TITLE),
        exportFileName,
        new FileNameExtensionFilter(
            Messages.getMessage(Name.CSV_FILES), "csv"), FileType.OTHER);
    if (file != null) {
      QueryProcessor queryProcessor = new QueryProcessor(reportSet, taxonomyStore.getTaxonomy());
      // For full Scythebill exports, allow everything and always write subspecies
      QueryResults queryResults = queryProcessor.runQuery(
          Predicates.<Sighting>alwaysTrue(), Predicates.<Sighting>alwaysTrue(), Taxon.Type.subspecies);
      ByteSink outSupplier = Files.asByteSink(file);
      try {
        new FullReportExport().exportReportSet(outSupplier, reportSet, taxonomyStore.getTaxonomy(),
            queryResults);
      } catch (IOException e) {
        alerts.reportError(e);
      }
    }
  }
}
