/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.Set;

import javax.swing.AbstractAction;

import org.apache.commons.text.StringEscapeUtils;

import com.google.common.base.Charsets;
import com.google.common.base.Predicate;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multiset;
import com.google.common.collect.Sets;
import com.google.common.io.Resources;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.TransposedChecklist;
import com.scythebill.birdlist.model.checklist.TransposedChecklists;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.panels.reports.QueryPreferences;
import com.scythebill.birdlist.ui.util.DesktopUtils;

/**
 * Shows a map of all world lifers.
 */
public class IdentifyLifersFromChecklistsAction extends AbstractAction {
  private final ReportSet reportSet;
  private final TaxonomyStore taxonomyStore;
  private final PredefinedLocations predefinedLocations;
  private final QueryPreferences queryPreferences;

  /** eBird Locations not supported by Google. */
  private final ImmutableMap<String, String> EBIRD_LOCATIONS_NOT_IN_GOOGLE = ImmutableBiMap.of(
      "UM-71", "US-HI",
      "BQ-BO", "BQ");

  /** Location IDs that are supported in both eBird and Google, but with different codes. */ 
  private final ImmutableBiMap<String, String> EBIRD_TO_GOOGLE_COUNTRIES = ImmutableBiMap.of(
      "FI-01", "AX");

  private final ImmutableMap<String, String> STATE_TO_COUNTRY_CODE = ImmutableMap.of(
      "CW", "BQ");

  @Inject
  IdentifyLifersFromChecklistsAction(
      TaxonomyStore taxonomyStore,
      ReportSet reportSet,
      PredefinedLocations predefinedLocations,
      QueryPreferences queryPreferences) {
    this.reportSet = reportSet;
    this.taxonomyStore = taxonomyStore;
    this.predefinedLocations = predefinedLocations;
    this.queryPreferences = queryPreferences;
  }
  
  @Override
  public void actionPerformed(ActionEvent action) {
    Taxonomy taxonomy = taxonomyStore.getTaxonomy();
    
    // Find which species *have* been seen
    final Set<String> seenSpecies = Sets.newHashSet();
    Predicate<Sighting> countablePredicate = queryPreferences.getCountablePredicate(
        taxonomy, /*skipSightingStatus=*/false, null);
    for (Sighting sighting : reportSet.getSightings(taxonomy)) {
      if (!countablePredicate.apply(sighting)) {
        continue;
      }
      Resolved resolved = sighting.getTaxon().resolve(taxonomy);
      SightingTaxon taxon = resolved.getParentOfAtLeastType(Taxon.Type.species);
      if (taxon.getType() == SightingTaxon.Type.SINGLE
          || taxon.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
        seenSpecies.add(taxon.getId());
      }
    }
    
    final TransposedChecklist transposed = TransposedChecklists.instance().getTransposedChecklist(
        reportSet, taxonomy);
    if (transposed == null) {
      throw new IllegalArgumentException("No transposed checklists for " + taxonomy.getId());
    }

    final ImmutableSet<Checklist.Status> allowedStatuses =
        queryPreferences.countIntroduced 
          ? ImmutableSet.of(
              Checklist.Status.ENDEMIC,
              Checklist.Status.NATIVE,
              Checklist.Status.INTRODUCED)
          : ImmutableSet.of(
              Checklist.Status.ENDEMIC,
              Checklist.Status.NATIVE);
    final Multiset<String> countryCounts = HashMultiset.create();
    TaxonUtils.visitTaxa(taxonomy, new TaxonVisitor() {
      @Override public boolean visitTaxon(Taxon taxon) {
        if (taxon.getType() == Taxon.Type.species
            && !seenSpecies.contains(taxon.getId())) {
          // Ignore as potential lifers anything undescribed if query preferences are set as such.
          if (!queryPreferences.countUndescribed && taxon.getStatus() == Species.Status.UN) {
            return false;
          }
          for (String location : normalizeCountries(transposed.locationsWithStatuses(taxon, allowedStatuses))) {
            countryCounts.add(location);
          }
          return false;
        }
        
        return true;
      }

      private Set<String> normalizeCountries(Iterable<String> values) {
        Set<String> set = Sets.newHashSet();
        for (String value : values) {
          if (EBIRD_TO_GOOGLE_COUNTRIES.containsKey(value)) {
            value = EBIRD_TO_GOOGLE_COUNTRIES.get(value);
          } else if (EBIRD_LOCATIONS_NOT_IN_GOOGLE.containsKey(value)) {
            value = EBIRD_LOCATIONS_NOT_IN_GOOGLE.get(value);
          }
          
          int indexOf = value.indexOf('-');
          if (indexOf < 0) {
            set.add(value);
          } else {
            set.add(value.substring(0, indexOf));
          }
        }
        return set;
      }
    });
    
    try {
      String startTemplate = Resources.toString(
          Resources.getResource(getClass(), "lifer.start.html.template"),
          Charsets.UTF_8);
      String endTemplate = Resources.toString(
          Resources.getResource(getClass(), "lifer.end.html.template"),
          Charsets.UTF_8);
  
      File file = File.createTempFile("lifers", ".html");
      Writer writer = new FileWriter(file);
      try {
        writer.write(String.format(startTemplate, taxonomy.getName()));
        
        for (String country : countryCounts.elementSet()) {
          String countryName = getCountryName(country);
          writer.write(
              String.format("[{v:\"%s\",f:\"%s\"}, %d],",
                  country,
                  StringEscapeUtils.escapeJson(countryName),
                  countryCounts.count(country)));
        }
             
        writer.write(endTemplate);
      } finally {
        writer.close();
      }
      DesktopUtils.openHtmlFileInBrowser(file);
    } catch (IOException e) {
      throw new IllegalStateException("Couldn't write", e);
    }
  }

  private String getCountryName(String country) {
    // Map back to eBird if necessary
    if (EBIRD_TO_GOOGLE_COUNTRIES.containsValue(country)) {
      country = EBIRD_TO_GOOGLE_COUNTRIES.inverse().get(country);
    }
    LocationSet locations = reportSet.getLocations();
    Location location = locations.getLocationByCode(country);
    if (location != null) {
      return location.getDisplayName();
    } else {
      Collection<Location> possibleLocations;
      int hyphen = country.indexOf('-');
      if (hyphen > 0) {
        String justCountry = country.substring(0, hyphen);
        possibleLocations = locations.getLocationsByCode(justCountry);
      } else if (STATE_TO_COUNTRY_CODE.containsKey(country)) {
        String actualCountry = STATE_TO_COUNTRY_CODE.get(country);
        possibleLocations = locations.getLocationsByCode(actualCountry);
      } else {
        possibleLocations = ImmutableList.of();
      }
      
      for (Location possibleLocation : possibleLocations) {
        PredefinedLocation predefinedChild = predefinedLocations.getPredefinedLocationChildByCode(possibleLocation, country);
        if (predefinedChild != null) {
          return predefinedChild.getName();
        }
      }
    }
    
    return country;
  }
}
