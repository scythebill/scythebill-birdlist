/*   
 * Copyright 2020 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.awt.Component;

import javax.annotation.Nullable;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.guice.CurrentVersion;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * Run over all sightings and all (relevant) checklists and identify species
 * that are rarities in the checklists.  Outputs an HTML file describing all the issues.
 */
public class IdentifyRaritySightingsAction extends IdentifySpeciesMissingFromChecklistsAction {

  @Inject
  IdentifyRaritySightingsAction(
      TaxonomyStore taxonomyStore,
      ReportSet reportSet,
      Checklists checklists,
      Alerts alerts,
      @Nullable @CurrentVersion String versionInfo) {
    super(taxonomyStore, reportSet, checklists, alerts, versionInfo);
  }
  
  @Override
  protected boolean statusIsReportable(Status status) {
    return status == Status.RARITY || status == Status.RARITY_FROM_INTRODUCED;
  }

  @Override
  protected void showNoSightingsFound(Alerts alerts, Component source) {
    alerts.showMessage(source, Name.NO_RARITIES_FOUND, Name.NO_SIGHTINGS_ARE_RARITIES);
  }

  @Override
  protected Name description() {
    return Name.CHECKLIST_RARITIES_DESCRIPTION;
  }

  @Override
  protected Name title() {
    return Name.CHECKLIST_RARITIES_TITLE;
  }
}
