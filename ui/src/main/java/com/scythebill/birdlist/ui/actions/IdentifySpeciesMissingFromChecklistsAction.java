/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;

import javax.annotation.Nullable;
import javax.swing.AbstractAction;

import com.google.common.base.Charsets;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.checklist.ExtendedTaxonomyChecklists;
import com.scythebill.birdlist.model.io.HtmlResponseWriter;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.ResolvedComparator;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.guice.CurrentVersion;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.DesktopUtils;

/**
 * Run over all sightings and all (relevant) checklists and identify species
 * that are missing from the checklists.  Outputs an HTML file describing all the issues.
 */
public class IdentifySpeciesMissingFromChecklistsAction extends AbstractAction {
  private final ReportSet reportSet;
  private final Checklists checklists;
  private final Alerts alerts;
  private final TaxonomyStore taxonomyStore;
  private final String versionInfo;
  private final static ImmutableSet<String> CODES_WITH_MULTIPLE_REGIONS =
      ImmutableSet.of("XX", "RU", "ID");

  @Inject
  IdentifySpeciesMissingFromChecklistsAction(
      TaxonomyStore taxonomyStore,
      ReportSet reportSet,
      Checklists checklists,
      Alerts alerts,
      @Nullable @CurrentVersion String versionInfo) {
    this.reportSet = reportSet;
    this.checklists = checklists;
    this.alerts = alerts;
    this.taxonomyStore = taxonomyStore;
    this.versionInfo = versionInfo;
  }
  
  @Override
  public void actionPerformed(ActionEvent action) {
    if (!taxonomyStore.isBirdTaxonomy()) {
      ExtendedTaxonomyChecklists extendedTaxonomyChecklists =
          reportSet.getExtendedTaxonomyChecklist(taxonomyStore.getTaxonomy().getId());
      if (extendedTaxonomyChecklists == null
          || extendedTaxonomyChecklists.getChecklists() == null
          || extendedTaxonomyChecklists.getChecklists().isEmpty()) {
        throw new IllegalStateException("Only supported for taxonomies with checklists.");
      }
    }
    
    // First, sort all the sightings by location (for any location that has a checklist).
    // This pre-sort makes the next loop waaaaay faster since it doesn't have to reload checklists all the time.
    // It also nicely means there's no need to repeatedly check the same taxa against the same locations.
    Multimap<Location, SightingTaxon> sightingsByLocationWithChecklist = LinkedHashMultimap.create();
    for (Sighting sighting : reportSet.getSightings(taxonomyStore.getTaxonomy())) {
      if (sighting.getLocationId() != null) {
        Location location = reportSet.getLocations().getLocation(sighting.getLocationId());
        // Ignore any and all escapees, uncertain, or unaccepted records
        if (sighting.getSightingInfo().getSightingStatus() == SightingStatus.INTRODUCED_NOT_ESTABLISHED
            || sighting.getSightingInfo().getSightingStatus() == SightingStatus.ID_UNCERTAIN 
            || sighting.getSightingInfo().getSightingStatus() == SightingStatus.DOMESTIC
            || sighting.getSightingInfo().getSightingStatus() == SightingStatus.RECORD_NOT_ACCEPTED) {
          continue;
        }
        
        while (location != null) {
          if (checklists.hasChecklist(taxonomyStore.getTaxonomy(), reportSet, location)) {
            sightingsByLocationWithChecklist.put(location, sighting.getTaxon());
          }
          location = location.getParent();
        }
      }
    }
     
    // Then, subset those to just the ones that are missing.
    Taxonomy taxonomy = taxonomyStore.getTaxonomy();
    ListMultimap<Location, Resolved> missingSightings = ArrayListMultimap.create();
    for (Location location : sightingsByLocationWithChecklist.keySet()) {
      Checklist checklist = checklists.getChecklist(reportSet, taxonomyStore.getTaxonomy(), location);
      // Skip synthetic checklists
      if (checklist.isSynthetic()) {
        continue;
      }
      for (SightingTaxon taxon : sightingsByLocationWithChecklist.get(location)) {
        Resolved resolved = taxon.resolve(taxonomy);
        if (resolved.getSmallestTaxonType() != Taxon.Type.species) {
          resolved = resolved.getParentOfAtLeastType(Taxon.Type.species).resolveInternal(taxonomy);
        }

        // Ignore hybrids and sp.s.
        if (resolved.getType() != SightingTaxon.Type.SINGLE
            && resolved.getType() != SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
          continue;
        }
        
        Status status = checklist.getStatus(taxonomy, resolved.getSightingTaxon());
        if (statusIsReportable(status)) {
          missingSightings.put(location, resolved);
        }
      }
    }
    
    if (missingSightings.isEmpty()) {
      showNoSightingsFound(alerts, (Component) action.getSource());
    } else {
      try {
        File temp = File.createTempFile("ChecklistIssues", ".html");
        Writer out = new BufferedWriter(
            new OutputStreamWriter(
                new FileOutputStream(temp),
                Charsets.UTF_8));
        HtmlResponseWriter rw = new HtmlResponseWriter(out, Charsets.UTF_8.name());
        rw.startDocument();
        rw.startElement("html");
        rw.startElement("head");

        rw.startElement("meta");
        rw.writeAttribute("http-equiv", "Content-Type");
        rw.writeAttribute("content", "text/html; charset=UTF-8");
        rw.endElement("meta");
        rw.startElement("title");
        rw.writeText(Messages.getMessage(title()));
        rw.endElement("title");
        rw.endElement("head");
        rw.startElement("body");

        rw.startElement("p");
        rw.writeText(Messages.getMessage(description()));
        rw.endElement("p");

        for (Location location : reportSet.getLocations().rootLocations()) {
          outputChecklistIssues(missingSightings, rw, location);
        }
          
        rw.startElement("p");
        rw.writeText(
            Messages.getFormattedMessage(Name.TAXONOMY_WITH_NAME, taxonomy.getName()));
        rw.endElement("p");

        if (versionInfo != null) {
          rw.startElement("p");
          rw.writeText(
              Messages.getFormattedMessage(Name.SCYTHEBILL_VERSION, versionInfo));
          rw.endElement("p");
        }

        rw.endElement("body");
        rw.endElement("html");
        rw.endDocument();
        rw.close();
        DesktopUtils.openHtmlFileInBrowser(temp);
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
  }

  private void outputChecklistIssues(
      ListMultimap<Location, Resolved> missingSightings,
      HtmlResponseWriter rw,
      Location location) throws IOException {
    if (missingSightings.containsKey(location)) {
      Checklist checklist = checklists.getChecklist(reportSet, taxonomyStore.getTaxonomy(), location);
      List<Resolved> list = missingSightings.get(location);
      rw.startElement("h2");
      
      String locationName;
      // For locations like "Indonesia" and "Russia", include the parent to disambiguate the
      // multiple checklists
      if (CODES_WITH_MULTIPLE_REGIONS.contains(Locations.getLocationCode(location))) {
        locationName = String.format("%s (%s)", location.getDisplayName(), location.getParent().getDisplayName());
      } else {
        locationName = location.getDisplayName();
      }
      
      if (!checklist.isBuiltIn()) {
        rw.writeText(Messages.getFormattedMessage(Name.CUSTOM_CHECKLIST_FORMAT, locationName));
      } else {
        rw.writeText(locationName);
      }
      
      rw.endElement("h2");
      rw.startElement("ul");
      ImmutableSortedSet<Resolved> sorted = ImmutableSortedSet.copyOf(new ResolvedComparator(), list);
      for (Resolved resolved : sorted) {
        rw.startElement("li");
        rw.writeText(String.format("%s (%s)",
            resolved.getCommonName(),
            resolved.getFullName()));
        if (resolved.getTaxon() instanceof Species) {
          rw.startElement("br");
          rw.endElement("br");
          String range = TaxonUtils.getRange((Species) resolved.getTaxon());
          if (range != null) {
            rw.writeText(
                Messages.getFormattedMessage(Name.EXPECTED_RANGE_FORMAT, range));
          }
        }
        rw.endElement("li");
      }
      rw.endElement("ul");
    }
    
    for (Location child : location.contents()) {
      outputChecklistIssues(missingSightings, rw, child);
    }
  }
  
  protected boolean statusIsReportable(Status status) {
    return status == null;
  }

  protected Name description() {
    return Name.CHECKLIST_ISSUES_DESCRIPTION;
  }

  protected Name title() {
    return Name.CHECKLIST_ISSUES;
  }

  protected void showNoSightingsFound(Alerts alerts, Component source) {
    alerts.showMessage(source, Name.NO_ISSUES_FOUND, Name.ALL_SIGHTINGS_CONFORM);
  }

}
