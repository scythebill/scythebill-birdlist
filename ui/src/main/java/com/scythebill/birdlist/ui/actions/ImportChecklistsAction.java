/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.checklist.ClementsChecklistReader;
import com.scythebill.birdlist.model.checklist.ClementsChecklistReader.ChecklistAndLocation;
import com.scythebill.birdlist.model.checklist.ClementsChecklistReader.InvalidTaxonomyException;
import com.scythebill.birdlist.model.io.HtmlResponseWriter;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.TaxonomyMappings;
import com.scythebill.birdlist.ui.guice.Clements;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;

/**
 * Imports custom checklists into Scythebill.
 */
public class ImportChecklistsAction extends AbstractAction {
  private final ReportSet reportSet;
  private final PredefinedLocations predefinedLocations;
  private final Alerts alerts;
  private final FileDialogs fileDialogs;
  private final Taxonomy taxonomy;
  private final TaxonomyMappings taxonomyMappings;

  @Inject
  ImportChecklistsAction(
      @Clements Taxonomy taxonomy,
      ReportSet reportSet,
      PredefinedLocations predefinedLocations,
      Checklists checklists,
      Alerts alerts,
      FileDialogs fileDialogs,
      TaxonomyMappings taxonomyMappings) {
    this.taxonomy = taxonomy;
    this.reportSet = reportSet;
    this.predefinedLocations = predefinedLocations;
    this.alerts = alerts;
    this.fileDialogs = fileDialogs;
    this.taxonomyMappings = taxonomyMappings;
  }
  
  @Override
  public void actionPerformed(ActionEvent action) {
    File[] files = fileDialogs.openFiles(null,
        Messages.getMessage(Name.IMPORT_CHECKLISTS_TITLE), new FileFilter() {
      
      @Override
      public String getDescription() {
        return Messages.getMessage(Name.SCYTHEBILL_CHECKLISTS);
      }
      
      @Override
      public boolean accept(File file) {
        // TODO: use a different file suffix?
        return file.getName().endsWith(".csv");
      }
    }, FileType.OTHER);
    
    importFiles(files);
  }

  public boolean isClementsChecklist(File file) {
    return ClementsChecklistReader.isClementsChecklist(file);
  }
  
  public void importFiles(File[] files) {
    ClementsChecklistReader reader = new ClementsChecklistReader(
        taxonomy, predefinedLocations, taxonomyMappings);
    List<ChecklistAndLocation> list = Lists.newArrayList();
    for (File file : files) {
      try {
        list.add(reader.read(reportSet, file));
      } catch (IllegalArgumentException | IOException e) {
        alerts.showError(null,
            Name.COULDNT_READ_CHECKLIST,
            Name.NOT_VALID_CHECKLIST_FORMAT,
            HtmlResponseWriter.htmlEscape(file.getName()));
      } catch (InvalidTaxonomyException e) {
        alerts.showError(null,
            Name.COULDNT_READ_CHECKLIST,
            Name.CHECKLIST_TAXONOMY_OBSOLETE,
            HtmlResponseWriter.htmlEscape(file.getName()));
      }
    }
    
    // See if any of the checklists already exist.
    int countThatAlreadyExist = 0;
    for (ChecklistAndLocation checklistAndLocation : list) {
      if (reportSet.getChecklist(checklistAndLocation.location) != null) {
        countThatAlreadyExist++;
      }
    }
    
    if (countThatAlreadyExist > 0) {
      Name message;

      // All of the checklists exist!
      if (countThatAlreadyExist == list.size()) {
        if (countThatAlreadyExist == 1) {
          message = Name.OVERWRITE_SINGLE_CHECKLIST;
        } else {
          message = Name.OVERWRITE_MULTIPLE_CHECKLISTS;
        }
      } else {
        message = Name.OVERWRITE_SOME_CHECKLISTS;
      }

      if (alerts.showOkCancel(null, Name.OVERWRITE_CHECKLISTS_TITLE, message) !=
          JOptionPane.OK_OPTION) {
        return;
      }
    }
    
    for (ChecklistAndLocation checklistAndLocation : list) {
      // Ensure the location exists
      reportSet.getLocations().ensureAdded(
          checklistAndLocation.location);
      // And save the checklist
      reportSet.setChecklist(
          checklistAndLocation.location,
          checklistAndLocation.checklist);
    }
    
    if (!list.isEmpty()) {
      if (list.size() == 1) {
        alerts.showMessage(null,
            Name.ADDED_CHECKLIST_TITLE,
            Name.ADDED_CHECKLIST_FORMAT, list.get(0).location.getDisplayName());
      } else {
        alerts.showMessage(null,
            Name.ADDED_CHECKLISTS_TITLE,
            Name.ADDED_CHECKLISTS_FORMAT, list.size());
      }
    }
  }
}
