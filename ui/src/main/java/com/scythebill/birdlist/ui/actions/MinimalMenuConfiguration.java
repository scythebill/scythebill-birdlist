/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import com.google.inject.Inject;
import com.scythebill.birdlist.ui.fonts.DecreaseFontSizeAction;
import com.scythebill.birdlist.ui.fonts.IncreaseFontSizeAction;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * MenuConfiguration with a minimum of options - generally used for MacOS.
 */
public class MinimalMenuConfiguration implements MenuConfiguration {
  private final ActionBroker actionBroker;
  private final OpenAction openAction;
  private final NewReportSetAction newAction;
  private final IncreaseFontSizeAction increaseFontSizeAction;
  private final DecreaseFontSizeAction decreaseFontSizeAction;
  private final Alerts alerts;

  @Inject
  public MinimalMenuConfiguration(
      ActionBroker actionBroker,
      OpenAction openAction,
      NewReportSetAction newAction,
      IncreaseFontSizeAction increaseFontSizeAction,
      DecreaseFontSizeAction decreaseFontSizeAction,
      Alerts alerts) {
    this.actionBroker = actionBroker;
    this.openAction = openAction;
    this.newAction = newAction;
    this.increaseFontSizeAction = increaseFontSizeAction;
    this.decreaseFontSizeAction = decreaseFontSizeAction;
    this.alerts = alerts;
  }

  @Override
  public JMenu getFileMenu() {
    JMenu menu = new JMenu();
    menu.setText(Messages.getMessage(Name.FILE_MENU));
    
    int keyModifier = Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx();

    JMenuItem closeItem = new JMenuItem();
    closeItem.setAction(actionBroker.createAction(ActionBroker.CLOSE));
    closeItem.setText(Messages.getMessage(Name.CLOSE_MENU));
    closeItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, keyModifier));
    menu.add(closeItem);    

    JMenuItem saveItem = new JMenuItem();
    saveItem.setAction(actionBroker.createAction(ActionBroker.SAVE));
    saveItem.setText(Messages.getMessage(Name.SAVE_MENU));
    saveItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, keyModifier));
    menu.add(saveItem);

    JMenuItem saveACopyAsItem = new JMenuItem();
    saveACopyAsItem.setAction(actionBroker.createAction(ActionBroker.SAVE_A_COPY_AS));
    saveACopyAsItem.setText(Messages.getMessage(Name.SAVE_A_COPY_AS));
    menu.add(saveACopyAsItem);

    JMenuItem openContainingItem = new JMenuItem();
    openContainingItem.setAction(actionBroker.createAction(ActionBroker.OPEN_CONTAINING));
    openContainingItem.setText(Messages.getMessage(Name.OPEN_CONTAINING_FOLDER));
    menu.add(openContainingItem);

    menu.addSeparator();

    JMenuItem importItem = new JMenuItem();
    importItem.setAction(actionBroker.createAction(ActionBroker.IMPORT));
    importItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, keyModifier));
    importItem.setText(Messages.getMessage(Name.IMPORT_SIGHTINGS_MENU));
    menu.add(importItem);

    JMenuItem exportItem = new JMenuItem();
    exportItem.setAction(actionBroker.createAction(ActionBroker.EXPORT));
    exportItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, keyModifier));
    exportItem.setText(Messages.getMessage(Name.EXPORT_MENU));
    menu.add(exportItem);

    JMenuItem printItem = new JMenuItem();
    printItem.setAction(actionBroker.createAction(ActionBroker.PRINT));
    printItem.setText(Messages.getMessage(Name.PRINT_MENU));
    printItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, keyModifier));
    menu.add(printItem);
    
    // Remove the "Full CSV export..." for now.  People are using it as a "full backup", but
    // it won't even do that with extended taxonomies.
//    JMenuItem fullExportItem = new JMenuItem();
//    fullExportItem.setAction(actionBroker.createAction(ActionBroker.FULL_EXPORT));
//    fullExportItem.setText("Full CSV export...");
//    menu.add(fullExportItem);
//
    JMenuItem manageTaxonomiesItem = new JMenuItem();
    manageTaxonomiesItem.setAction(actionBroker.createAction(ActionBroker.MANAGE_TAXONOMIES));
    manageTaxonomiesItem.setText(Messages.getMessage(Name.MANAGE_TAXONOMIES_MENU));
    menu.add(manageTaxonomiesItem);

    menu.addSeparator();

    JMenuItem importChecklistsItem = new JMenuItem();
    importChecklistsItem.setAction(
        actionBroker.createAction(ActionBroker.IMPORT_CHECKLISTS));
    importChecklistsItem.setText(Messages.getMessage(Name.IMPORT_CHECKLISTS_MENU));
    menu.add(importChecklistsItem);

    JMenuItem checklistErrorsItem = new JMenuItem();
    checklistErrorsItem.setAction(
        actionBroker.createAction(ActionBroker.IDENTIFY_CHECKLIST_ERRORS));
    checklistErrorsItem.setText(Messages.getMessage(Name.VERIFY_AGAINST_CHECKLISTS));
    menu.add(checklistErrorsItem);

    JMenuItem checklistRaritiesItem = new JMenuItem();
    checklistRaritiesItem.setAction(
        actionBroker.createAction(ActionBroker.IDENTIFY_CHECKLIST_RARITIES));
    checklistRaritiesItem.setText(Messages.getMessage(Name.FIND_CHECKLIST_RARITIES));
    menu.add(checklistRaritiesItem);

    JMenuItem reconcileAgainstChecklistsItem = new JMenuItem();
    reconcileAgainstChecklistsItem.setAction(
        actionBroker.createAction(ActionBroker.RECONCILE_AGAINST_CHECKLISTS));
    reconcileAgainstChecklistsItem.setText(Messages.getMessage(Name.RECONCILE_AGAINST_CHECKLISTS));
    menu.add(reconcileAgainstChecklistsItem);

    menu.addSeparator();
    
    JMenuItem newItem = new JMenuItem();
    newItem.setAction(newAction);
    newItem.setText(Messages.getMessage(Name.NEW_SIGHTINGS_FILE_MENU));
    menu.add(newItem);
    
    JMenuItem openItem = new JMenuItem();
    openItem.setAction(openAction);
    openItem.setText(Messages.getMessage(Name.OPEN_SIGHTINGS_FILE_MENU));
    menu.add(openItem);
        
    return menu;
  }

  @Override
  public JMenu getHelpMenu() {
    JMenu menu = new JMenu();
    menu.setText(Messages.getMessage(Name.HELP_MENU));
    
    JMenuItem showManualItem = new JMenuItem();
    showManualItem.setAction(new ShowManualAction(alerts));
    showManualItem.setText(Messages.getMessage(Name.SCYTHEBILL_MANUAL));
    menu.add(showManualItem);

    JMenuItem emailGroup = new JMenuItem();
    emailGroup.setAction(new ShowEMailGroupAction(alerts));
    emailGroup.setText(Messages.getMessage(Name.SCYTHEBILL_EMAIL_GROUP));
    menu.add(emailGroup);

    JMenuItem downloadLatest = new JMenuItem();
    downloadLatest.setAction(new DownloadLatestAction(alerts));
    downloadLatest.setText(Messages.getMessage(Name.DOWNLOAD_LATEST));
    menu.add(downloadLatest);

    JMenuItem reportIssueItem = new JMenuItem();
    reportIssueItem.setAction(new ReportIssueAction(alerts));
    reportIssueItem.setText(Messages.getMessage(Name.REPORT_ISSUE));
    menu.add(reportIssueItem);

    JMenuItem openSourceItem = new JMenuItem();
    openSourceItem.setAction(new OpenSourceLicensesAction());
    openSourceItem.setText(Messages.getMessage(Name.OPEN_SOURCE_LICENSES));
    menu.add(openSourceItem);
    
    return menu;
  }

  @Override
  public JMenu getViewMenu() {
    JMenu menu = new JMenu();
    menu.setText(Messages.getMessage(Name.VIEW_MENU));
    
    int keyModifier = Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx();

    JMenuItem increaseFontSizeItem = new JMenuItem();
    increaseFontSizeItem.setAction(increaseFontSizeAction);
    increaseFontSizeItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_EQUALS, keyModifier));
    menu.add(increaseFontSizeItem);

    JMenuItem decreaseFontSizeItem = new JMenuItem();
    decreaseFontSizeItem.setAction(decreaseFontSizeAction);
    decreaseFontSizeItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_MINUS, keyModifier));
    menu.add(decreaseFontSizeItem);

    return menu;
  }

}
