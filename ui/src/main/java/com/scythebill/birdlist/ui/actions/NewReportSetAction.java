/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.awt.event.ActionEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.AbstractAction;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.io.HtmlResponseWriter;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.xml.XmlReportSetExport;
import com.scythebill.birdlist.model.xml.XmlReportSetImport;
import com.scythebill.birdlist.ui.app.FrameRegistry;
import com.scythebill.birdlist.ui.guice.Clements;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;

/**
 * Action for creating new ReportSets.
 * TODO: the proper course would be showing a save dialog only
 * after adding entries.
 */
public final class NewReportSetAction extends AbstractAction {
  private final static Logger logger = Logger.getLogger(NewReportSetAction.class.getName());
  private final FrameRegistry frameRegistry;
  private final Supplier<Taxonomy> taxonomySupplier;
  private final FileDialogs fileDialogs;
  private final Alerts alerts;

  @Inject public NewReportSetAction(
      FrameRegistry frameRegistry,
      FileDialogs fileDialogs,
      @Clements Supplier<Taxonomy> taxonomySupplier,
      Alerts alerts) {
    this.frameRegistry = frameRegistry;
    this.fileDialogs = fileDialogs;
    this.taxonomySupplier = taxonomySupplier;
    this.alerts = alerts;
  }

  @Override public void actionPerformed(ActionEvent event) {
    ReportSet newReportSet = ReportSets.newReportSet(taxonomySupplier.get());
    saveAndOpenNewReportSet(newReportSet);
  }
  
  /**
   * Save a new ReportSet file.
   * 
   * @return true if the file was successfully saved
   */
  public boolean saveAndOpenNewReportSet(ReportSet reportSet) {
    while (true) {
      
      
      String username = System.getProperty("user.name");
      String fileName;
      if (username == null) {
        fileName = "List" + XmlReportSetImport.REPORT_SET_SUFFIX;
      } else {
        fileName = String.format("List-%s%s",
            username, XmlReportSetImport.REPORT_SET_SUFFIX);
      }
      File saveFile =
          fileDialogs.saveFile(null, Messages.getMessage(Name.NEW_LIST_TITLE), fileName, null, FileType.SIGHTINGS);

      if (saveFile == null) {
        return false;
      }
      
      // Make sure the file ends with the proper suffix;  but if
      // renaming gives us a pre-existing file, abort, since the
      // UI wouldn't have warned the user
      if (!saveFile.getName().endsWith(XmlReportSetImport.REPORT_SET_SUFFIX)) {
        saveFile = new File(saveFile.getParent(), saveFile.getName()
            + XmlReportSetImport.REPORT_SET_SUFFIX);
        if (saveFile.exists()) {
          alerts.showError(null, Name.FILE_ALREADY_EXISTS_TITLE,
              Name.FILE_ALREADY_EXISTS_FORMAT,
              HtmlResponseWriter.htmlEscape(saveFile.getName()));
          continue;
        }
      }
  
      // Try to create the file.  If that fails, the directory is likely not writable.
      try {
        if (!saveFile.exists()) {
          if (!saveFile.createNewFile()) {
            throw new IOException("Could not create new file");
          }
        }
      } catch (IOException e) {
        alerts.showError(null, Name.COULD_NOT_SAVE_TITLE,
            Name.COULD_NOT_SAVE_FORMAT,
            HtmlResponseWriter.htmlEscape(saveFile.getParentFile().getName()));
        continue;
      }

      // Write the newly created file
      try {
        FileOutputStream output = new FileOutputStream(saveFile);
        Writer writer = new BufferedWriter(new OutputStreamWriter(output,
            Charsets.UTF_8));
        try {
          Taxonomy taxonomy = Preconditions.checkNotNull(taxonomySupplier.get(),
              "Taxonomy should be loaded before this is available");
          XmlReportSetExport rse = new XmlReportSetExport();
          rse.export(writer, Charsets.UTF_8.name(), reportSet, taxonomy);
        } finally {
          writer.close();
        }
      } catch (IOException ioe) {
        logger.log(Level.WARNING, "Could not create file", ioe);
        alerts.showError(null, Name.COULD_NOT_CREATE_TITLE, Name.ERROR_OCCURRED_TRY_AGAIN);
        return false;
      }

      frameRegistry.displayLoadedReportSet(saveFile, reportSet);
      
      return true;
    }
  }
}