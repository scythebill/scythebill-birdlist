/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.xml.XmlReportSetImport;
import com.scythebill.birdlist.ui.app.FrameRegistry;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.util.FileDialogs;

/** Action for opening ReportSets. */
public final class OpenAction extends AbstractAction {
  private final FrameRegistry frameRegistry;
  private final FileDialogs fileDialogs;
  private final FileFilter fileFilter = new FileNameExtensionFilter(
      Messages.getMessage(Name.SCYTHEBILL_LISTFILES),
      XmlReportSetImport.REPORT_SET_SUFFIX.substring(1));
  

  @Inject
  public OpenAction(
      FrameRegistry frameRegistry,
      FileDialogs fileDialogs) {
    this.frameRegistry = frameRegistry;
    this.fileDialogs = fileDialogs;
  }

  @Override public void actionPerformed(ActionEvent event) {
    File openFile =
        fileDialogs.openFile(
            null, Messages.getMessage(Name.OPEN_LIST_TITLE), fileFilter, FileType.SIGHTINGS);
    if (openFile != null) {
      frameRegistry.startLoadingReportSet(openFile.getAbsolutePath(), null);
    }
  }
}