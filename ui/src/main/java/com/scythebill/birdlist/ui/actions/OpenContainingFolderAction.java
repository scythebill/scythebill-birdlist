/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.AbstractAction;

import com.google.inject.Inject;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * Opens a containing folder.
 */
public class OpenContainingFolderAction extends AbstractAction {

  private final File file;
  private final Alerts alerts;

  @Inject
  public OpenContainingFolderAction(File file, Alerts alerts) {
    this.file = file;
    this.alerts = alerts;
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    try {
      Desktop.getDesktop().open(file.getParentFile());
    } catch (IOException e) {
      alerts.reportError(e);
    }
  }
}
