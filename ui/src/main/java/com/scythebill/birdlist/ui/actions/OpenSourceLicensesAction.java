/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.io.Resources;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Shows open source licenses for projects depend
 */
public class OpenSourceLicensesAction extends AbstractAction {
  enum License {
    GUICE("Google Guice", "LICENSE.guice"),
    GUAVA("Google Guava", "LICENSE.guava"),
    JODA_TIME("Joda Time", "LICENSE.joda-time"),
    OPEN_CSV("opencsv", "LICENSE.opencsv"),
    COMMONS_LANG("Apache Commons Lang", "LICENSE.apache-commons-lang"),
    POI("Apache POI", "LICENSE.apache-poi"),
    HTTP_COMPONENTS("Apache HTTP Components", "LICENSE.apache-http-components"),
    GSON("Google Gson", "LICENSE.gson"),
    GEOTOOLS("Geotools", "LICENSE.geotools");
    
    private final String name;
    private final String resource;
    
    private License(String name, String resource) {
      this.name = name;
      this.resource = resource;
    }
    
    public String projectName() {
      return name;
    }
    
    public String read() {
      try {
        URL url = Resources.getResource("licenses/" + resource);
        return Resources.toString(url, Charsets.UTF_8);
      } catch (IllegalArgumentException e) {
        // Resource doesn't exist:  move along
        return null;
      } catch (IOException e) {
        // Should not occur when reading resources
        throw new RuntimeException(e);
      }
    }
  }
  
  @Override
  public void actionPerformed(ActionEvent e) {
    List<String> names = Lists.newArrayList();
    final List<String> licenseText = Lists.newArrayList();

    for (License license : License.values()) {
      String text = license.read();
      // Skip licenses that can't be read (which should just be Quaqua on
      // non-MacOS builds)
      if (text != null) {
        names.add(license.projectName());
        licenseText.add(text);
      }
    }
    
    final JComboBox<String> licenses = new JComboBox<String>(names.toArray(new String[0]));
    final JTextArea textArea = new JTextArea();
    textArea.setText(licenseText.get(0));
    textArea.setColumns(60);
    textArea.setRows(20);
    textArea.setWrapStyleWord(true);
    textArea.setLineWrap(true);
    textArea.setEditable(false);
    textArea.select(0, 0);
    final JScrollPane scrollPane = new JScrollPane(
            textArea,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

    licenses.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        textArea.setText(licenseText.get(licenses.getSelectedIndex()));
        textArea.select(0, 0);
      }
    });
    String message = Messages.getMessage(Name.SCYTHEBILL_COULD_NOT_BE_BUILT);
    
    JOptionPane.showMessageDialog(null, new Object[]{message, licenses, "", scrollPane}, "",
        JOptionPane.PLAIN_MESSAGE);
  }
  

}
