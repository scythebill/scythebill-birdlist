/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.awt.event.ActionEvent;
import java.lang.Thread.UncaughtExceptionHandler;

import javax.swing.AbstractAction;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.scythebill.birdlist.ui.app.FrameRegistry;
import com.scythebill.birdlist.ui.prefs.PreferencesManager;

/**
 * Action for handling quitting the application.
 */
@Singleton
public class QuitAction extends AbstractAction {
  private final PreferencesManager preferencesManager;
  private final FrameRegistry frameRegistry;
  
  @Inject
  public QuitAction(PreferencesManager preferencesManager,
      FrameRegistry frameRegistry) {
    this.preferencesManager = preferencesManager;
    this.frameRegistry = frameRegistry;
  }
  
  /**
   * Attempts to quit.
   * @return true if the application should proceed with quitting, false otherwise
   */
  public boolean tryQuit() {
    try {
      // Flush all preferences before quitting (or even trying to quit)
      preferencesManager.save();
      
      // Make sure all window state has been saved
      if (!frameRegistry.saveBeforeQuitting()) {
        return false;
      }
    } catch (RuntimeException e) {
      // Failing to quit because of an uncaught exception/bug is simply too annoying.
      // Show an alert, then quit!
      UncaughtExceptionHandler handler = Thread.currentThread().getUncaughtExceptionHandler();
      if (handler != null) {
        handler.uncaughtException(Thread.currentThread(), e);
      }
    }
    
    return true;
  }

  @Override public void actionPerformed(ActionEvent event) {
    // If all windows were successfully closed, quit.
    if (tryQuit()) {
      System.exit(0);
    }
  }
}
