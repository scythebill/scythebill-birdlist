/*   
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.awt.event.ActionEvent;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.ChecklistResolution;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.location.ChecklistResolutionResults;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * Supports automatic resolution of "sp." taxa against checklists.
 * <p>
 * TODO: same thing with custom checklists.
 */
public class ReconcileAgainstChecklistsAction extends AbstractAction {

  private final TaxonomyStore taxonomyStore;
  private final ReportSet reportSet;
  private final Alerts alerts;
  private final Checklists checklists;
  private FontManager fontManager;

  @Inject
  public ReconcileAgainstChecklistsAction(
      TaxonomyStore taxonomyStore,
      ReportSet reportSet,
      Checklists checklists,
      FontManager fontManager,
      Alerts alerts) {
    this.taxonomyStore = taxonomyStore;
    this.reportSet = reportSet;
    this.checklists = checklists;
    this.fontManager = fontManager;
    this.alerts = alerts;
  }
  
  @Override
  public void actionPerformed(ActionEvent e) {
    if (!taxonomyStore.isBirdTaxonomy()) {
      throw new IllegalStateException("Not supported except for birds.");
    }
    
    Set<SightingTaxon> taxaToResolve = new LinkedHashSet<>();
    Taxonomy taxonomy = taxonomyStore.getTaxonomy();
    for (Sighting sighting : reportSet.getSightings(taxonomyStore.getTaxonomy())) {
      Resolved resolved = sighting.getTaxon().resolve(taxonomy);
      if (resolved.getType() == SightingTaxon.Type.SP && resolved.getLargestTaxonType() == Taxon.Type.species) {
        taxaToResolve.add(sighting.getTaxon());
      }
    }
    
    if (taxaToResolve.isEmpty()) {
      alerts.showMessage(e.getSource(), Name.NO_RECONCILED_SPS_TITLE,
          Name.NO_RECONCILED_SPS_MESSAGE);
      return;
    }
    
    ChecklistResolution checklistResolution = new ChecklistResolution(checklists, ImmutableSet.of());
    ImmutableSet<SightingTaxon> remainingTaxa =
        checklistResolution.attemptChecklistResolution(reportSet, taxaToResolve, taxonomy);
    ChecklistResolution checklistResolutionWithoutRarities = new ChecklistResolution(checklists,
        ImmutableSet.of(Checklist.Status.RARITY, Checklist.Status.RARITY_FROM_INTRODUCED));
    ImmutableSet<SightingTaxon> remainingTaxaWithoutRarities = checklistResolutionWithoutRarities
        .attemptChecklistResolution(reportSet, taxaToResolve, taxonomy);

    if ((remainingTaxa == null || checklistResolution.sightingsToAdd().isEmpty())
        && (remainingTaxaWithoutRarities == null || checklistResolutionWithoutRarities.sightingsToAdd().isEmpty())) {
      // Don't care much about remaining taxa here, but if it's null, resolution
      // produced no results.  Ditto empty sightingsToAdd
      alerts.showMessage(e.getSource(), Name.NO_RECONCILED_SPS_TITLE,
          Name.NO_RECONCILED_SPS_MESSAGE);
      return;
    }
    
    if (checklistResolution.sightingsToAdd().size() == checklistResolutionWithoutRarities.sightingsToAdd().size()) {
      int totalSightings = checklistResolution.sightingsToAdd().size();
      int partialImprovements = checklistResolution.simplifiedButNotFullyResolved();
      
      int arg1 = 0;
      int arg2 = 0;
      Name messageFormat;
      if (partialImprovements == 0) {
        arg1 = totalSightings;
        messageFormat = totalSightings == 1 ? Name.SIGHTING_CAN_BE_RESOLVED
            : Name.SIGHTINGS_CAN_BE_RESOLVED;
      } else if (partialImprovements == totalSightings) {
        arg1 = totalSightings;
        messageFormat = totalSightings == 1 ? Name.SIGHTING_CAN_BE_PARTIALLY_RESOLVED
            : Name.SIGHTINGS_CAN_BE_PARTIALLY_RESOLVED;
      } else {
        arg1 = totalSightings - partialImprovements;
        arg2 = partialImprovements;
        messageFormat = Name.SIGHTINGS_CAN_BE_FULLY_AND_PARTIALLY_RESOLVED;
      }
      
      JComponent panel =
          ChecklistResolutionResults.asPanel(checklistResolution, taxonomy, fontManager);
      
      int okOrCancel = alerts.showOkCancelWithPanel(e.getSource(),
          alerts.getFormattedDialogMessage(
              Name.RESULTS_TITLE, messageFormat, arg1, arg2), panel);
      if (okOrCancel == JOptionPane.OK_OPTION) {
        reportSet.mutator()
            .removing(checklistResolution.sightingsToRemove())
            .adding(checklistResolution.sightingsToAdd())
            .mutate();
      }
    } else if (checklistResolution.sightingsToAdd().size() == 0) {
      int totalSightings = checklistResolutionWithoutRarities.sightingsToAdd().size();
      int partialImprovements = checklistResolutionWithoutRarities.simplifiedButNotFullyResolved();
      
      int arg1 = 0;
      int arg2 = 0;
      Name messageFormat;
      if (partialImprovements == 0) {
        arg1 = totalSightings;
        messageFormat = totalSightings == 1 ? Name.SIGHTING_CAN_BE_RESOLVED_USING_RARITIES
            : Name.SIGHTINGS_CAN_BE_RESOLVED_USING_RARITIES;
      } else if (partialImprovements == totalSightings) {
        arg1 = totalSightings;
        messageFormat = totalSightings == 1 ? Name.SIGHTING_CAN_BE_PARTIALLY_RESOLVED_USING_RARITIES
            : Name.SIGHTINGS_CAN_BE_PARTIALLY_RESOLVED_USING_RARITIES;
      } else {
        arg1 = totalSightings - partialImprovements;
        arg2 = partialImprovements;
        messageFormat = Name.SIGHTINGS_CAN_BE_FULLY_AND_PARTIALLY_RESOLVED_USING_RARITIES;
      }
      
      JComponent panel =
          ChecklistResolutionResults.asPanel(checklistResolutionWithoutRarities, taxonomy, fontManager);
      
      int okOrCancel = alerts.showOkCancelWithPanel(e.getSource(),
          alerts.getFormattedDialogMessage(
              Name.RESULTS_TITLE, messageFormat, arg1, arg2), panel);
      if (okOrCancel == JOptionPane.OK_OPTION) {
        reportSet.mutator()
            .removing(checklistResolutionWithoutRarities.sightingsToRemove())
            .adding(checklistResolutionWithoutRarities.sightingsToAdd())
            .mutate();
      }
    } else {
      int totalSightings = checklistResolution.sightingsToAdd().size();
      int partialImprovements = checklistResolution.simplifiedButNotFullyResolved();
      
      int arg1 = 0;
      int arg2 = 0;
      Name messageFormat;
      if (partialImprovements == 0) {
        arg1 = totalSightings;
        messageFormat = totalSightings == 1 ? Name.SIGHTING_CAN_BE_RESOLVED
            : Name.SIGHTINGS_CAN_BE_RESOLVED;
      } else if (partialImprovements == totalSightings) {
        arg1 = totalSightings;
        messageFormat = totalSightings == 1 ? Name.SIGHTING_CAN_BE_PARTIALLY_RESOLVED
            : Name.SIGHTINGS_CAN_BE_PARTIALLY_RESOLVED;
      } else {
        arg1 = totalSightings - partialImprovements;
        arg2 = partialImprovements;
        messageFormat = Name.SIGHTINGS_CAN_BE_FULLY_AND_PARTIALLY_RESOLVED;
      }
      
      String formattedMessage = alerts.getFormattedDialogMessage(
          Name.RESULTS_TITLE, messageFormat, arg1, arg2);
      
      String messageWithoutRarities;
      int totalSightingsWithoutRarities = checklistResolutionWithoutRarities.sightingsToAdd().size();
      int partialImprovementsWithoutRarities = checklistResolutionWithoutRarities.simplifiedButNotFullyResolved();
      if (partialImprovementsWithoutRarities == 0) {
        if (totalSightingsWithoutRarities == 1) {
          messageWithoutRarities = Messages.getMessage(Name.SIGHTING_CAN_BE_RESOLVED_USING_RARITIES);
        } else {
          messageWithoutRarities =
              Messages.getFormattedMessage(Name.SIGHTINGS_CAN_BE_RESOLVED_USING_RARITIES, totalSightingsWithoutRarities); 
        }
      } else if (partialImprovementsWithoutRarities == totalSightingsWithoutRarities) {        
        if (totalSightingsWithoutRarities == 1) {
          messageWithoutRarities = Messages.getMessage(Name.SIGHTING_CAN_BE_PARTIALLY_RESOLVED_USING_RARITIES);
        } else {
          messageWithoutRarities =
              Messages.getFormattedMessage(Name.SIGHTINGS_CAN_BE_PARTIALLY_RESOLVED_USING_RARITIES, totalSightingsWithoutRarities); 
        }
      } else {
        messageWithoutRarities =
            Messages.getFormattedMessage(Name.SIGHTINGS_CAN_BE_FULLY_AND_PARTIALLY_RESOLVED_USING_RARITIES,
                totalSightingsWithoutRarities - partialImprovementsWithoutRarities,
                partialImprovementsWithoutRarities);
      }
      
      JComponent panel = Box.createVerticalBox();
      panel.add(
          ChecklistResolutionResults.asPanel(checklistResolution, taxonomy, fontManager));
      panel.add(Box.createVerticalStrut(20));
      panel.add(new JLabel("<html>" +
          messageWithoutRarities));
      panel.add(Box.createVerticalStrut(5));
      panel.add(
          ChecklistResolutionResults.asPanel(checklistResolutionWithoutRarities, taxonomy, fontManager));

      int option = alerts.showOptionsWithPanel(e.getSource(),
          formattedMessage,
          panel,
          Messages.getMessage(Name.CANCEL_BUTTON),
          Messages.getMessage(Name.USE_RARITIES_BUTTON),
          Messages.getMessage(Name.DONT_USE_RARITIES_BUTTON));
      
      if (option == 1) {
        reportSet.mutator()
            .removing(checklistResolutionWithoutRarities.sightingsToRemove())
            .adding(checklistResolutionWithoutRarities.sightingsToAdd())
            .mutate();
      } else if (option == 2) {
        reportSet.mutator()
            .removing(checklistResolution.sightingsToRemove())
            .adding(checklistResolution.sightingsToAdd())
            .mutate();
      }
    }
  }
}