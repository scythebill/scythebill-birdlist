/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.awt.event.ActionEvent;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.swing.AbstractAction;

import com.google.common.base.Charsets;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.io.HtmlResponseWriter;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.xml.XmlReportSetExport;
import com.scythebill.birdlist.model.xml.XmlReportSetImport;
import com.scythebill.birdlist.ui.guice.Clements;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;

/**
 * Action responsible for saving a copy of report sets.
 */
public class SaveACopyAsAction extends AbstractAction {

  private final ReportSet reportSet;
  private final FileDialogs fileDialogs;
  private final File originalFile;
  private final Taxonomy clements;
  private final Alerts alerts;

  @Inject
  public SaveACopyAsAction(
      ReportSet reportSet,
      File file,
      FileDialogs fileDialogs,
      Alerts alerts,
      @Clements Taxonomy clements) {
    super(Messages.getMessage(Name.SAVE_A_COPY_AS));
    this.reportSet = reportSet;
    this.originalFile = file;
    this.fileDialogs = fileDialogs;
    this.alerts = alerts;
    this.clements = clements;
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    String originalFileName = originalFile.getName();
    int lastPeriod = originalFileName.lastIndexOf('.');
    String fileName;
    if (lastPeriod < 0) {
      fileName = originalFileName + "-copy.bsxm";
    } else {
      fileName = originalFileName.substring(0, lastPeriod) + "-copy.bsxm";
    }
    
    File saveFile = fileDialogs.saveFile(null,
        Messages.getFormattedMessage(Name.COPY_OF_FORMAT, originalFile.getName()), fileName, null, FileType.SIGHTINGS);
    if (saveFile == null) {
      return;
    }
    
    // Make sure the file ends with the proper suffix;  but if
    // renaming gives us a pre-existing file, abort, since the
    // UI wouldn't have warned the user.
    if (!saveFile.getName().endsWith(XmlReportSetImport.REPORT_SET_SUFFIX)) {
      saveFile = new File(saveFile.getParent(), saveFile.getName()
          + XmlReportSetImport.REPORT_SET_SUFFIX);
      if (saveFile.exists()) {
        alerts.showError(null, Name.FILE_ALREADY_EXISTS_TITLE,
            Name.FILE_ALREADY_EXISTS_FORMAT,
            HtmlResponseWriter.htmlEscape(saveFile.getName()));
        return;
      }
    }

    // Try to create the file.  If that fails, the directory is likely not writable.
    try {
      if (!saveFile.exists()) {
        if (!saveFile.createNewFile()) {
          throw new IOException("Could not create new file");
        }
      }
    } catch (IOException e) {
      alerts.showError(null, Name.COULD_NOT_SAVE_TITLE,
          Name.COULD_NOT_SAVE_FORMAT,
          HtmlResponseWriter.htmlEscape(saveFile.getParentFile().getName()));
      return;
    }

    OutputStream fileOutputStream;
    try {
      fileOutputStream = new FileOutputStream(saveFile);
    } catch (FileNotFoundException e) {
      // File *should* exist, we just created it
      throw new RuntimeException(e);
    }
    
    Writer writer = new OutputStreamWriter(
        new BufferedOutputStream(fileOutputStream),
        Charsets.UTF_8);
    
    try {
      new XmlReportSetExport().export(writer, "UTF-8", reportSet, clements);
    } catch (IOException e) {
      throw new RuntimeException("Failed to save a copy", e);
    } finally {
      try {
        writer.close();
      } catch (IOException e) {
        throw new RuntimeException(e);        
      }
    }
  }
  
}
