/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.AbstractAction;

import com.google.inject.Inject;
import com.scythebill.birdlist.ui.app.ReportSetSaver;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;

/**
 * Action responsible for saving report sets.
 */
public class SaveAction extends AbstractAction {
  private final ReportSetSaver reportSetSaver;
  private final Alerts alerts;

  @Inject
  public SaveAction(
      ReportSetSaver reportSetSaver,
      Alerts alerts) {
    super(Messages.getMessage(Name.SAVE_MENU));
    this.reportSetSaver = reportSetSaver;
    this.alerts = alerts;    
    setEnabled(reportSetSaver.getDirty().isDirty());
    reportSetSaver.getDirty().addDirtyListener(
        event -> setEnabled(Boolean.TRUE.equals(event.getNewValue())));
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    try {
      reportSetSaver.save();
    } catch (IOException e) {
      FileDialogs.showFileSaveError(alerts, e, reportSetSaver.file());
    }
  }
  
}
