/*   
 * Copyright 2023 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.URI;

import javax.swing.AbstractAction;

import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.DesktopUtils;

/** Action for showing the Scythebill e-mail group page. */
final class ShowEMailGroupAction extends AbstractAction {
  private final Alerts alerts;

  ShowEMailGroupAction(Alerts alerts) {
    this.alerts = alerts;
  }

  @Override public void actionPerformed(ActionEvent event) {
    try {
      DesktopUtils.openUrlInBrowser(
          URI.create("https://groups.google.com/g/scythebill-users/about"), alerts);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}