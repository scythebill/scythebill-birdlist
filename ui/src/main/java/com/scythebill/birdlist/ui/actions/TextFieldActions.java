/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.TransferHandler;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.JTextComponent;

import com.google.inject.Inject;
import com.scythebill.birdlist.ui.util.FocusTracker;

/**
 * Utilities for connecting text fields to the ActionBroker infrastructure 
 */
public class TextFieldActions {
  private static final Logger logger = Logger.getLogger(TextFieldActions.class.getName());
  private final ActionBroker broker;

  @Inject
  public TextFieldActions(ActionBroker broker) {
    this.broker = broker;
  }
  
  /** Installs TextFieldActions globally. */
  public void install(JComponent component) {
    new FocusTracker(component) {
      private Action cut;
      private Action copy;
      private Action paste;

      @Override
      protected boolean isTargetOfTracker(Component potentialTarget) {
        return (potentialTarget instanceof JTextComponent)
            && super.isTargetOfTracker(potentialTarget); 
      }
      
      @Override
      protected boolean includeTextComponents() {
        return true;
      }
      
      @Override
      protected void focusLost(Component child) {
        if (cut != null) {
          broker.unpublishAction("cut", cut);
          cut = null;
        }

        if (copy != null) {
          broker.unpublishAction("copy", copy);
          copy = null;
        }

        if (paste != null) {
          broker.unpublishAction("paste", paste);
          paste = null;
        }
      }
      
      @Override
      protected void focusGained(Component child) {
        JTextComponent text = (JTextComponent) child;
        if (text.getTransferHandler() != null) {
          cut = new TextFieldCutCopy(text, TransferHandler.getCutAction());
          broker.publishAction("cut", cut);
          copy = new TextFieldCutCopy(text, TransferHandler.getCopyAction());
          broker.publishAction("copy", copy);
          
          try {
            DataFlavor[] availableDataFlavors = Toolkit.getDefaultToolkit().getSystemClipboard().getAvailableDataFlavors();
            DataFlavor textFlavor = DataFlavor.selectBestTextFlavor(availableDataFlavors);
            if (textFlavor != null) {
              paste = new ForwardingAction(text, TransferHandler.getPasteAction());
              broker.publishAction("paste", paste);
            }
          } catch (IllegalStateException e) {
            // On occasion, Windows getAvailableDataFlavors() will throw an IllegalStateException
            // from WClipboard.openClipboard.  Handle silently (there's nothing useful to do)
            logger.log(Level.WARNING, "Could not open clipboard, paste will not be enabled", e);
          } catch (IllegalArgumentException e) {
            // http://bugs.java.com/view_bug.do?bug_id=7173464 ;  this was supposedly resolved years
            // ago in Java 8, but has been recently reproduced by a user in 1.8.0_60.
            logger.log(Level.WARNING, "Could not open clipboard, paste will not be enabled", e);
          }
        }
      }
    };
  }

  /** Action delegate that only enables if there's a selection */
  private static class TextFieldCutCopy extends ForwardingAction implements Attachable, CaretListener {
    private final JTextComponent component;

    public TextFieldCutCopy(JTextComponent component, Action delegate) {
      super(component, delegate);
      this.component = component;
    }
    
    @Override public void attach() {
      component.addCaretListener(this);
    }

    @Override public void unattach() {
      component.removeCaretListener(this);
    }

    @Override public void caretUpdate(CaretEvent event) {
      updateEnabled();
    }

    private void updateEnabled() {
      setEnabled(component.getSelectionStart() != component.getSelectionEnd());
    }
    
  }
}
