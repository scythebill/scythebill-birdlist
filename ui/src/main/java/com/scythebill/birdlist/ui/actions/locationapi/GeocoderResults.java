/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions.locationapi;

import com.google.common.base.MoreObjects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.Location;

/**
 * Results of geocoding.
 */
public final class GeocoderResults {
  private final String name;
  private final LatLongCoordinates coordinates;
  private final Location preferredParent;
  private final Source source;
  public enum Source {
    GOOGLE("Google"),
    EBIRD("eBird");
    
    private final String name;
    private Source(String name) {
      this.name = name;
    }
    
    @Override
    public String toString() {
      return name;
    }
  }

  public GeocoderResults(Source source, String name, LatLongCoordinates coordinates, Location preferredParent) {
    this.source = source;
    // Empty names -> problem with anything that tries to create locations
    Preconditions.checkArgument(!Strings.isNullOrEmpty(name));
    this.name = name;
    this.coordinates = coordinates;
    this.preferredParent = preferredParent;
  }

  public String name() {
    return name;
  }
  
  public String nameWithSource() {
    return String.format("%s - %s", name, source);
  }

  public LatLongCoordinates coordinates() {
    return coordinates;
  }

  public Optional<Location> preferredParent() {
    return Optional.fromNullable(preferredParent);
  }
  
  public Source source() {
    return source;
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("source", source)
        .add("name", name)
        .add("coordinates", coordinates)
        .add("preferredParent", preferredParent)
        .toString();
  }

}
