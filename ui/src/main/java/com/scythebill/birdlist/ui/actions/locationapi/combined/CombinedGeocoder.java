/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions.locationapi.combined;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Collection;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.SettableFuture;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.ui.actions.locationapi.GeocoderResults;
import com.scythebill.birdlist.ui.actions.locationapi.ebird.EBirdGeocoder;
import com.scythebill.birdlist.ui.actions.locationapi.google.GoogleGeocoder;

/**
 * Geocoder implementation responsible for combining multiple geocoders.
 */
public class CombinedGeocoder {
  private final static Logger logger = Logger.getLogger(CombinedGeocoder.class.getName());
  private final GoogleGeocoder googleGeocoder;
  private final EBirdGeocoder eBirdGeocoder;

  @Inject
  public CombinedGeocoder(
      GoogleGeocoder googleGeocoder,
      EBirdGeocoder eBirdGeocoder) {
    this.googleGeocoder = googleGeocoder;
    this.eBirdGeocoder = eBirdGeocoder;
  }

  public ListenableFuture<ImmutableList<GeocoderResults>> geocode(
      LocationSet locationSet, Location location, Collection<LatLongCoordinates> geocodeResultsToIgnore) {
    checkNotNull(location);
    
    final ImmutableSet<LatLongCoordinates> coordsToIgnore = ImmutableSet.copyOf(geocodeResultsToIgnore);
    // TODO: ignore coords within a delta?
    final Predicate<GeocoderResults> acceptableResultPredicate = results -> !coordsToIgnore.contains(results.coordinates());
    ListenableFuture<ImmutableList<GeocoderResults>> googleResults = googleGeocoder.geocode(locationSet, location);
    ListenableFuture<ImmutableList<GeocoderResults>> ebirdResults = eBirdGeocoder.geocode(locationSet, location);
    
    final ListenableFuture<ImmutableList<GeocoderResults>> primaryResults;
    final ListenableFuture<ImmutableList<GeocoderResults>> secondaryResults;
    final String primaryName;
    final String secondaryName;
    
    primaryResults = ebirdResults;
    secondaryResults = googleResults;
    primaryName = "eBird";
    secondaryName = "Google";

    final SettableFuture<ImmutableList<GeocoderResults>> finalResults = SettableFuture.create();
    Futures.allAsList(ImmutableList.of(primaryResults, secondaryResults)).addListener(
        new Runnable() {
          @Override public void run() {
            Exception error = null;
            ImmutableList.Builder<GeocoderResults> results = ImmutableList.builder();
            final Set<String> currentNames = Sets.newHashSet(); 
            try {
              FluentIterable<GeocoderResults> filtered = FluentIterable.from(primaryResults.get())
                  .filter(acceptableResultPredicate);
              results.addAll(filtered);
              for (GeocoderResults result : filtered) {
                currentNames.add(result.name());
              }
            } catch (Exception e) {
              error = e;
              logger.log(Level.WARNING, primaryName + " geocoding failed", e);
            }
            
            Predicate<GeocoderResults> skippingPrimaryNames = Predicates.and(
                acceptableResultPredicate,
                r -> !currentNames.contains(r.name()));
            try {
              results.addAll(FluentIterable.from(secondaryResults.get())
                  .filter(skippingPrimaryNames));
            } catch (Exception e) {
              error = e;
              logger.log(Level.WARNING, secondaryName + " geocoding failed", e);
            }
            
            ImmutableList<GeocoderResults> built = results.build();
            if (built.isEmpty()
                && error != null) {
              // Propagate an error if neither read succeeded.
              finalResults.setException(error);
            } else {
              finalResults.set(built);
            }
          }
        },
        MoreExecutors.directExecutor());
    /*
    primaryResults.addListener(new Runnable() {
      @Override public void run() {
        try {
          // Google results win, if available
          ImmutableList<GeocoderResults> primaryList = FluentIterable.from(primaryResults.get())
              .filter(acceptableResultPredicate)
              .toList();
          if (!primaryList.isEmpty()) {
            finalResults.set(primaryList);
          }
        } catch (Exception e) {
          // Log and ignore the primary geocoding failure - use the secondary results
          logger.log(Level.WARNING, primaryName + " geocoding failed", e);
        }
        
        // Have to use the eBird results
        secondaryResults.addListener(new Runnable() {
          @Override public void run() {
            try {
              finalResults.set(FluentIterable.from(secondaryResults.get())
                  .filter(acceptableResultPredicate)
                  .toList());
            } catch (Exception e) {
              finalResults.setException(e);
            }
          }
          
        }, MoreExecutors.sameThreadExecutor());
      }
    }, MoreExecutors.sameThreadExecutor());
    */
    return finalResults;
  }

}
