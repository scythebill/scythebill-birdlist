/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions.locationapi.combined;

import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.google.common.util.concurrent.AsyncFunction;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.ui.actions.locationapi.GeocoderResults;
import com.scythebill.birdlist.ui.actions.locationapi.ReverseGeocoder;
import com.scythebill.birdlist.ui.actions.locationapi.ebird.EBirdGeocoder;
import com.scythebill.birdlist.ui.actions.locationapi.google.GoogleGeocoder;

/**
 * Combines eBird and Google reverse geocoding into a single set, where distance "wins".
 */
public class CombinedReverseGeocoder implements ReverseGeocoder {
  private final static Logger logger = Logger.getLogger(CombinedGeocoder.class.getName());
  private final GoogleGeocoder googleGeocoder;
  private final EBirdGeocoder ebirdGeocoder;
  
  @Inject
  CombinedReverseGeocoder(
      GoogleGeocoder googleGeocoder,
      EBirdGeocoder ebirdGeocoder) {
    this.googleGeocoder = googleGeocoder;
    this.ebirdGeocoder = ebirdGeocoder;
  }

  @Override
  public ListenableFuture<ImmutableList<GeocoderResults>> reverseGeocode(
      LocationSet locationSet, final LatLongCoordinates latLong) {
    ListenableFuture<ImmutableList<GeocoderResults>> googleResults = googleGeocoder.reverseGeocode(locationSet, latLong);
    ListenableFuture<ImmutableList<GeocoderResults>> ebirdResults = ebirdGeocoder.reverseGeocode(locationSet, latLong);
    
    final ImmutableList<ListenableFuture<ImmutableList<GeocoderResults>>> allResultsAsList = ImmutableList.of(
        googleResults,
        ebirdResults);
    
    // Log failures for either 
    Futures.addCallback(ebirdResults, new FutureCallback<ImmutableList<GeocoderResults>>() {
      @Override
      public void onFailure(Throwable t) {
        logger.log(Level.WARNING, "eBird reverse geocoding failed", t);
      }

      @Override
      public void onSuccess(ImmutableList<GeocoderResults> results) {}
    }, MoreExecutors.directExecutor());
    Futures.addCallback(googleResults, new FutureCallback<ImmutableList<GeocoderResults>>() {
      @Override
      public void onFailure(Throwable t) {
        logger.log(Level.WARNING, "Google reverse geocoding failed", t);
      }

      @Override
      public void onSuccess(ImmutableList<GeocoderResults> results) {}
    }, MoreExecutors.directExecutor());

    // Use an ordering that prioritizes results closest to the given lat-long
    final Ordering<GeocoderResults> ordering = new Ordering<GeocoderResults>() {
      @Override
      public int compare(GeocoderResults left, GeocoderResults right) {
        double leftDistance = latLong.kmDistance(left.coordinates());
        double rightDistance = latLong.kmDistance(right.coordinates());
        if (leftDistance == rightDistance) {
          return 0;
        } else if (leftDistance < rightDistance) {
          return -1;
        } else {
          return 1;
        }
      }
    };
    
    ListenableFuture<List<ImmutableList<GeocoderResults>>> successfulResults = Futures.successfulAsList(allResultsAsList);
    return Futures.transformAsync(successfulResults,
        new AsyncFunction<List<ImmutableList<GeocoderResults>>, ImmutableList<GeocoderResults>>() {
          @Override
          public ListenableFuture<ImmutableList<GeocoderResults>> apply(List<ImmutableList<GeocoderResults>> allResults) {
            // They all failed...  return the first error
            if (Iterables.all(allResults, Predicates.isNull())) {
              try {
                allResultsAsList.get(0).get();
              } catch (Throwable t) {
                return Futures.immediateFailedFuture(t);
              }
            }
            
            // Join the names together, and re-sort by distance from the coordinate
            ImmutableSortedSet<GeocoderResults> sortedBuilder = 
                ImmutableSortedSet.copyOf(ordering,
                    Iterables.concat(
                        // Make sure to drop "null", since anything that failed will be null
                        Iterables.filter(allResults, Predicates.notNull())));
            Set<String> names = Sets.newHashSet();
            ImmutableList.Builder<GeocoderResults> builder = ImmutableList.builder();
            // Then take that set in order, eliminate name duplicates (first, and therefore closest, wins)
            for (GeocoderResults result : sortedBuilder) {
              if (names.add(result.name())) {
                builder.add(result);
              }
            }
            return Futures.immediateFuture(builder.build());
          }
    }, MoreExecutors.directExecutor());
  }

}
