/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions.locationapi.ebird;

import java.io.BufferedInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Singleton;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Ordering;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningScheduledExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;
import com.scythebill.birdlist.ui.actions.locationapi.Geocoder;
import com.scythebill.birdlist.ui.actions.locationapi.GeocoderResults;
import com.scythebill.birdlist.ui.actions.locationapi.GeocoderResults.Source;
import com.scythebill.birdlist.ui.actions.locationapi.ReverseGeocoder;
import com.scythebill.birdlist.ui.guice.EBirdApiKey;

/**
 * Issues geocoding requests with the eBird API.  Geocoding requests will take a location
 * name (and optionally, country/state/etc.) and derive lat/long coordinates.
 */
// TODO: refresh the cache async (allow dirty results for ages, refresh offline)
@Singleton
public class EBirdGeocoder implements Geocoder, ReverseGeocoder {
  private static final RequestConfig REQUEST_CONFIG = RequestConfig.custom()
      .setConnectionRequestTimeout(5000)
      .setConnectTimeout(30000)
      .setSocketTimeout(30000)
      .build();
  private static final Logger logger = Logger.getLogger(EBirdGeocoder.class.getName());
  private final Cache<URI, Optional<EBirdHotspots>> resultCache = CacheBuilder.newBuilder()
      .maximumSize(1000)
      // eBird caching time is stated as 1 hour for this API
      .expireAfterWrite(1, TimeUnit.HOURS)
      .build();
  private final LoadingCache<URI, ListenableFuture<EBirdHotspots>> loadingCache = CacheBuilder.newBuilder()
      .maximumSize(50)
      .expireAfterWrite(1, TimeUnit.MINUTES)
      .build(new CacheLoader<URI, ListenableFuture<EBirdHotspots>>() {
        @Override
        public ListenableFuture<EBirdHotspots> load(URI uri) throws Exception {
          return loadHotspots(uri);
        }        
      });
  
  private final CloseableHttpClient httpClient;
  private final ListeningScheduledExecutorService executorService;
  private final PredefinedLocations predefinedLocations;
  
  /**
   * The US has an absolutely huge set of eBird locations - megabytes of content.  Don't try
   * to load it - if users want eBird data for the US, they'll have to pick at least a state.
   */
  private final static ImmutableSet<String> LOCATIONS_WITH_TOO_MANY_EBIRD_ENTRIES = ImmutableSet.of(
      "US");
  private static final int MAX_REVERSE_GEOCODE_RESULTS = 20;
  private String eBirdApikey;

  public static void main(String[] args) throws Exception {
    CloseableHttpClient httpClient = HttpClients.custom()
        .disableCookieManagement()
        .setMaxConnPerRoute(3)
        .setMaxConnTotal(20)
        .setConnectionManager(new PoolingHttpClientConnectionManager())
        .build();
    ListeningScheduledExecutorService executorService = MoreExecutors.listeningDecorator(Executors.newScheduledThreadPool(3));
    try {
      Gson gson = new Gson();
      EBirdGeocoder geocoder = new EBirdGeocoder(httpClient, executorService, gson,
          PredefinedLocations.loadAndParse(), "");
      LocationSet locations = ReportSets.newReportSet(new TaxonomyImpl()).getLocations();
//      Location california = locations.getLocationsByName("Peru").iterator().next();
//      Location city = Location.builder()
//          .setName("Burlingame").setParent(california).build();
//      Location park = Location.builder()
//          .setName("Golden Gate Park").setParent(california).build();
      
      //ImmutableList<GeocoderResults> results = geocoder.geocode(locations, park).get();
      ImmutableList<GeocoderResults> results = geocoder.reverseGeocode(locations, LatLongCoordinates.withLatAndLong(-25.685979,-54.44107)).get();
      System.err.println(Joiner.on("\n").join(results));
    } finally {
      executorService.shutdown();
    }
  }
  
  @Inject
  public EBirdGeocoder(
      CloseableHttpClient httpClient,
      ListeningScheduledExecutorService executorService,
      Gson gson,
      PredefinedLocations predefinedLocations,
      @EBirdApiKey String eBirdApikey) {
    this.httpClient = httpClient;
    this.executorService = executorService;
    this.predefinedLocations = predefinedLocations;
    this.eBirdApikey = eBirdApikey;
  }
 
  @Override
  public ListenableFuture<ImmutableList<GeocoderResults>> geocode(final LocationSet locationSet, final Location location) {    
    URIBuilder uriBuilder;
    final URI uri;
    try {
      uriBuilder = new URIBuilder("http://ebird.org/ws2.0/ref/hotspot/region");
      Location locationWithCode = getFirstParentWithCode(location);
      // eBird won't contain geocoding data about non-specific locations, don't even bother loading.
      // And short-circuit quickly if there's no location code available
      if (locationWithCode == location || locationWithCode == null) {
        return Futures.immediateFuture(ImmutableList.<GeocoderResults>of());
      }
      // Decline to load enormous eBird files - users will need to be more specific
      if (LOCATIONS_WITH_TOO_MANY_EBIRD_ENTRIES.contains(Locations.getLocationCode(locationWithCode))) {
        return Futures.immediateFuture(ImmutableList.<GeocoderResults>of());
      }
      
      uriBuilder.addParameter("r", Locations.getLocationCode(locationWithCode));
    
      uri = uriBuilder.build();
    } catch (URISyntaxException e) {
      throw new RuntimeException(e);
    }

    // Check for cached values;  short-circuit when present
    Optional<EBirdHotspots> ifPresent = resultCache.getIfPresent(uri);
    if (ifPresent != null) {
      if (ifPresent.isPresent()) {
        return Futures.immediateFuture(findGeocodedResults(locationSet, location, ifPresent.get()));
      } else {
        return Futures.immediateFuture(ImmutableList.<GeocoderResults>of());
      }
    }
    
    try {
      ListenableFuture<EBirdHotspots> loadingFuture = loadingCache.get(uri);
      return Futures.transform(
          loadingFuture,
          hotspots -> findGeocodedResults(locationSet, location, hotspots),
          executorService);
    } catch (ExecutionException e) {
      throw new RuntimeException(e);
    }    
  }
  
  @Override
  public ListenableFuture<ImmutableList<GeocoderResults>> reverseGeocode(
      final LocationSet locationSet, final LatLongCoordinates latLong) {
    // Format the URI *down* to 1 decimal place of precision.
    // One-tenth of a degree of latitude or longitude is *at least* 11.1km at the equator
    // (less away from it), and we're gathering results out to *25* kilometers, then
    // re-sorting.  So this will (significantly) improve the cache hit rate for the cache
    // without losing any precision.
    double roundedLat = ((int) (latLong.latitudeAsDouble() * 10.0)) / 10.0; 
    double roundedLong = ((int) (latLong.longitudeAsDouble() * 10.0)) / 10.0; 
    return reverseGeocode(locationSet, latLong, LatLongCoordinates.withLatAndLong(roundedLat, roundedLong), 25 /* km */);
  }
  
  /**
   * Reverse geocode with an explicit search center and max distance.  If these are chosen poorly, cache hit rates will be terrible;
   * choose lat/long that have been carefully rounded and a consistent maxKm that reaches past all adjacent lat/long rounding buckets.
   */
  public ListenableFuture<ImmutableList<GeocoderResults>> reverseGeocode(
        final LocationSet locationSet,
        final LatLongCoordinates latLong,
        final LatLongCoordinates searchCenter,
        int maxSearchKilometers) {
    URIBuilder uriBuilder;
    final URI uri;
    try {
      uriBuilder = new URIBuilder("http://ebird.org/ws2.0/ref/hotspot/geo");
      uriBuilder.addParameter("lat", String.format(Locale.US, "%3.2f", searchCenter.latitudeAsDouble()));
      uriBuilder.addParameter("lng", String.format(Locale.US, "%3.2f", searchCenter.longitudeAsDouble()));
      uriBuilder.addParameter("dist", Integer.toString(maxSearchKilometers));  // within 25 km
      uriBuilder.addParameter("fmt", "csv");
      uri = uriBuilder.build();
    } catch (URISyntaxException e) {
      throw new RuntimeException(e);
    }
    
    // Check for cached values;  short-circuit when present
    Optional<EBirdHotspots> ifPresent = resultCache.getIfPresent(uri);
    if (ifPresent != null) {
      if (ifPresent.isPresent()) {
        return Futures.immediateFuture(
            findReverseGeocodedResults(locationSet, latLong, ifPresent.get()));
      } else {
        return Futures.immediateFuture(ImmutableList.<GeocoderResults>of());
      }
    }
    
    try {
      ListenableFuture<EBirdHotspots> loadingFuture = loadingCache.get(uri);
      return Futures.transform(
          loadingFuture,
          hotspots -> findReverseGeocodedResults(locationSet, latLong, hotspots),
          executorService);
    } catch (ExecutionException e) {
      throw new RuntimeException(e);
    }    
  }
  
  /**
   * Loads hotspots, asynchronously, from a URI.
   */
  private ListenableFuture<EBirdHotspots> loadHotspots(URI uri) {
    ListenableFuture<EBirdHotspots> future = executorService.submit(new Callable<EBirdHotspots>() {
      @Override
      public EBirdHotspots call() throws Exception {
        if (Thread.interrupted()) {
          throw new CancellationException("Thread was interrupted");
        }
        
        HttpGet httpGet = new HttpGet(uri);
        httpGet.setConfig(REQUEST_CONFIG);
        httpGet.addHeader("X-eBirdApiToken", eBirdApikey);
        final CloseableHttpResponse get = httpClient.execute(httpGet);
        HttpEntity entity = get.getEntity();
        
        Reader reader = new InputStreamReader(
            new BufferedInputStream(entity.getContent()),
            // eBird API now appears to use UTF-8
            Charsets.UTF_8);
        
        EBirdHotspots eBirdHotspots = new EBirdHotspots();
        try {
          ImportLines importLines = CsvImportLines.fromReader(reader);
          String[] line;
          while ((line = importLines.nextLine()) != null) {
            eBirdHotspots.hotspots.add(new EBirdHotspot(line));
            if (Thread.interrupted()) {
              throw new CancellationException("Thread was interrupted");
            }
          }
        } catch (Exception e) {
          logger.log(Level.WARNING, "Failed to load from " + uri, e);
          return null;
        } finally {
          reader.close();
        }
        
        return eBirdHotspots;
      }
      
    });
    
    // Cache results when returned
    Futures.addCallback(future, new FutureCallback<EBirdHotspots>() {
      @Override public void onFailure(Throwable t) {
      }

      @Override
      public void onSuccess(EBirdHotspots results) {
        resultCache.put(uri, Optional.fromNullable(results));
      }
    }, executorService);
    
    return future;
  }

  private ImmutableList<GeocoderResults> findGeocodedResults(
      final LocationSet locationSet, final Location location, EBirdHotspots hotspots) {
    if (hotspots == null) {
      return ImmutableList.of();
    }
    ImmutableList<EBirdHotspot> hotspotList = hotspots.findByName(location.getModelName());
    if (hotspotList.isEmpty()) {
      return ImmutableList.of();
    }
    
    return FluentIterable.from(hotspotList)
        .transform(hotspot -> new GeocoderResults(
                Source.EBIRD,
                hotspot.name,
                hotspot.coordinates,
                findPreferredParent(locationSet, location, hotspot))).toList();
  }
  
  private ImmutableList<GeocoderResults> findReverseGeocodedResults(
      final LocationSet locationSet, final LatLongCoordinates coordinates, EBirdHotspots hotspots) {
    ImmutableCollection<EBirdHotspot> hotspotList = hotspots.findByDistance(coordinates, MAX_REVERSE_GEOCODE_RESULTS);
    if (hotspotList.isEmpty()) {
      return ImmutableList.of();
    }
    
    return FluentIterable.from(hotspotList)
        .transform(hotspot -> new GeocoderResults(
                Source.EBIRD,
                hotspot.name,
                hotspot.coordinates,
                findPreferredParentWithoutLocation(locationSet, hotspot))).toList();
  }

  private Location findPreferredParent(LocationSet locationSet, Location location, EBirdHotspot hotspot) {
    Location currentParent = getFirstParentWithCode(location);
    // If we couldn't find a parent with an ISO code, bail.
    // If there's a non-built-in location between this location and the parent, also bail:  the
    // user's been explicit about where to put this location, don't go elsewhere.
    if (currentParent == null || currentParent != location.getParent()) {
      return null;
    }
    
    if (currentParent.getType() == Location.Type.country
        && !Strings.isNullOrEmpty(hotspot.state)) {
      Location child = findChild(locationSet, currentParent, hotspot.state);
      if (child != null) {
        currentParent = child;
      }
    }
    
    if (currentParent.getType() != Location.Type.county
        && !Strings.isNullOrEmpty(hotspot.county)) {
      Location child = findChild(locationSet, currentParent, hotspot.county);
      if (child != null) {
        currentParent = child;
      }
    }

    return currentParent;
  }

  private Location findPreferredParentWithoutLocation(LocationSet locationSet, EBirdHotspot hotspot) {
    // See if the desired county already exists
    if (!Strings.isNullOrEmpty(hotspot.county)) {
      Location existingCounty = locationSet.getLocationByCode(hotspot.county);
      if (existingCounty != null) {
        // Yes, return immediately
        return existingCounty;
      }
    }
    
    Location currentParent = null;
    // See if the desired state already exists
    if (!Strings.isNullOrEmpty(hotspot.state)) {
      Location existingState = locationSet.getLocationByCode(hotspot.state);
      if (existingState != null) {
        // Yes, no need to look at country
        currentParent = existingState;
      }
    }
    
    // Need to look at the country
    if (currentParent == null) {
      // Find all "countries" with the associated code 
      Collection<Location> allCountries = locationSet.getLocationsByCode(hotspot.country);
      if (allCountries.isEmpty()) {
        return null;
      }
      
      // Find one with a matching state
      for (Location country : allCountries) {
        if (country.getType() == Location.Type.country
            && !Strings.isNullOrEmpty(hotspot.state)) {
          Location child = findChild(locationSet, country, hotspot.state);
          if (child != null) {
            currentParent = child;
            break;
          }
        }
      }
      if (currentParent == null) {
        // Did not find any matching state, use the first country
        currentParent = allCountries.iterator().next();
      }
    }
    
    if (currentParent.getType() != Location.Type.county
        && !Strings.isNullOrEmpty(hotspot.county)) {
      Location child = findChild(locationSet, currentParent, hotspot.county);
      if (child != null) {
        currentParent = child;
      }
    }

    return currentParent;
  }

  private Location findChild(LocationSet locationSet, Location parent, String code) {
    PredefinedLocation predefinedLocation = predefinedLocations.getPredefinedLocationChildByCode(parent, code);
    if (predefinedLocation != null) {
      return predefinedLocation.create(locationSet, parent);
    }
    
    return null;
  }
  
  private Location getFirstParentWithCode(Location location) {
    while (location != null) {
      if (location.getEbirdCode() != null) {
        break;
      }
      location = location.getParent();
    }
    return location;
  }

  static class EBirdHotspots {
    final List<EBirdHotspot> hotspots = new ArrayList<EBirdHotspot>();

    /**
     * Find all the hotspots with reasonable names.
     */
    ImmutableList<EBirdHotspot> findByName(String name) {
      // TODO: throw 'em all in a BKTree too?
      String normalized = normalizeName(name);
      ImmutableList.Builder<EBirdHotspot> builder = ImmutableList.builder();
      for (EBirdHotspot hotspot : hotspots) {
        if (normalized.equals(hotspot.normalizedName)
            || normalized.equals(hotspot.alternateNormalizedName)) {
          return ImmutableList.of(hotspot);
        } else if (startsWith(hotspot.normalizedName, normalized)
            || endsWith(hotspot.normalizedName, normalized)) {
          builder.add(hotspot);
        } else if (hotspot.alternateNormalizedName != null
            && (startsWith(hotspot.alternateNormalizedName, normalized)
                || endsWith(hotspot.alternateNormalizedName, normalized))) {
          builder.add(hotspot);
        }
      }
      
      return builder.build();
    }

    /**
     * Return all hotspots, sorted by distance.
     */
    public ImmutableCollection<EBirdHotspot> findByDistance(final LatLongCoordinates coordinates, int maxResults) {
      Ordering<EBirdHotspot> ordering = new Ordering<EBirdGeocoder.EBirdHotspot>() {
        @Override
        public int compare(EBirdHotspot left, EBirdHotspot right) {
          double leftDistance = coordinates.kmDistance(left.coordinates);
          double rightDistance = coordinates.kmDistance(right.coordinates);
          if (leftDistance == rightDistance) {
            return 0;
          } else if (leftDistance < rightDistance) {
            return -1;
          } else {
            return 1;
          }
        }
      };
      ImmutableCollection<EBirdHotspot> sortedResults = ImmutableSortedSet.copyOf(ordering, hotspots);
      if (sortedResults.size() > maxResults) {
        sortedResults = ImmutableList.copyOf(sortedResults).subList(0, maxResults);
      }
      
      return sortedResults;
    }
  }
  
  static class EBirdHotspot {
    final String locationId;
    final String country;
    final String state;
    final String county;
    final LatLongCoordinates coordinates;
    final String name;
    final String normalizedName;
    final String alternateNormalizedName;
    
    EBirdHotspot(String[] line) {
      locationId = line[0];
      country = line[1];
      state = line[2];
      county = line[3];
      coordinates = LatLongCoordinates.withLatAndLong(line[4], line[5]);
      name = line[6];
      normalizedName = normalizeName(name);
      alternateNormalizedName = removeParentheses(normalizedName);
    }
  }

  private final static CharMatcher REMOVE_FROM_NORMALIZER = CharMatcher.anyOf(".'\u02bb");
  private final static CharMatcher TREAT_AS_WHITESPACE = CharMatcher.whitespace().or(CharMatcher.anyOf("-"));
  
  private static String removeParentheses(String name) {
    int firstParenthesis;
    
    while ((firstParenthesis = name.indexOf('(')) >= 0) {
      int lastParenthesis = name.indexOf(')', firstParenthesis);
      if (lastParenthesis < 0) {
        name = name.substring(0, firstParenthesis);
      } else {
        name = name.substring(0, firstParenthesis) + name.substring(lastParenthesis + 1);
      }
      name = CharMatcher.whitespace().trimFrom(name);
    }
    
    return name;
  }
  
  private static String normalizeName(String name) {
    // Strip all accents, and ignore case
    String normalized = StringUtils.stripAccents(name).toLowerCase();
    // Remove periods etc.
    normalized = REMOVE_FROM_NORMALIZER.removeFrom(normalized);
    // Trim whitespace, and replace sequences with simple spaces.
    normalized = TREAT_AS_WHITESPACE.trimAndCollapseFrom(normalized, ' ');
    // Treat "Road" and "Rd" as the same, and a few others
    normalized = normalized.replace("road", "rd");
    normalized = normalized.replace("mountain", "mt");
    normalized = normalized.replace("mount", "mt");
    normalized = normalized.replace("saint", "st");
    return normalized;
  }

  private static boolean endsWith(String normalizedName, String normalized) {
    if (!normalizedName.endsWith(normalized)) {
      return false;
    }
    
    if (normalizedName.length() == normalized.length()) {
      return true;
    }
    
    // Make sure the character that comes right before is whitespace (or a hyphen)
    int precedingCharacter = normalizedName.length() - normalized.length() - 1;
    return TREAT_AS_WHITESPACE.matches(normalizedName.charAt(precedingCharacter));
  }

  private static boolean startsWith(String normalizedName, String normalized) {
    if (!normalizedName.startsWith(normalized)) {
      return false;
    }
    
    if (normalizedName.length() == normalized.length()) {
      return true;
    }
    
    // Make sure the character that comes right after is whitespace (or a hyphen)
    return TREAT_AS_WHITESPACE.matches(normalizedName.charAt(normalized.length()));
  }
}
