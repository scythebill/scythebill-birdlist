/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions.locationapi.google;

import java.io.BufferedInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.google.common.io.Resources;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningScheduledExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;
import com.scythebill.birdlist.ui.actions.locationapi.Geocoder;
import com.scythebill.birdlist.ui.actions.locationapi.GeocoderResults;
import com.scythebill.birdlist.ui.actions.locationapi.GeocoderResults.Source;
import com.scythebill.birdlist.ui.actions.locationapi.ReverseGeocoder;
import com.scythebill.birdlist.ui.guice.GoogleApiKey;

/**
 * Issues geocoding requests with the Google API.  Geocoding requests will take a location
 * name (and optionally, country/state/etc.) and derive lat/long coordinates.  Reverse geocoding
 * requests take a Lat/Long and derive a name, country, state.
 */
public class GoogleGeocoder implements Geocoder, ReverseGeocoder {
  private static final RequestConfig REQUEST_CONFIG = RequestConfig.custom()
      .setConnectionRequestTimeout(5000)
      .setConnectTimeout(10000)
      .setSocketTimeout(10000)
      .build();
  
  private static final Cache<URI, GoogleGeocodeResults> resultCache = CacheBuilder.newBuilder()
      .maximumSize(1000)
      // If someone keeps Scythebill open forever, drop these results after 1 day
      // to avoid enshrining stale data
      .expireAfterWrite(1, TimeUnit.DAYS)
      .build();
  
  private final CloseableHttpClient httpClient;
  private final ListeningScheduledExecutorService executorService;
  private final Gson gson;
  private final PredefinedLocations predefinedLocations;
  private final String googleApiKey;

  public static void main(String[] args) throws Exception {
    CloseableHttpClient httpClient = HttpClients.custom()
        .disableCookieManagement()
        .setMaxConnPerRoute(3)
        .setMaxConnTotal(20)
        .setConnectionManager(new PoolingHttpClientConnectionManager())
        .build();
    ListeningScheduledExecutorService executorService = MoreExecutors.listeningDecorator(Executors.newScheduledThreadPool(3));
    try {
      Gson gson = new Gson();
      GoogleGeocoder geocoder = new GoogleGeocoder(httpClient, executorService, gson,
          PredefinedLocations.loadAndParse(), Resources.toString(Resources.getResource("googleapikey.txt"), Charsets.UTF_8));
      LocationSet locations = ReportSets.newReportSet(new TaxonomyImpl()).getLocations();
      LatLongCoordinates latLong = LatLongCoordinates.withLatAndLong(-25.67602968,28.95082986);
      
      ImmutableList<GeocoderResults> results = geocoder.reverseGeocode(locations, latLong).get();
      System.err.println(Joiner.on('\n').join(results));
    } finally {
      executorService.shutdown();
    }
  }
  
  @Inject
  public GoogleGeocoder(
      CloseableHttpClient httpClient,
      ListeningScheduledExecutorService executorService,
      Gson gson,
      PredefinedLocations predefinedLocations,
      @Nullable @GoogleApiKey String googleApiKey) {
    this.httpClient = httpClient;
    this.executorService = executorService;
    this.gson = gson;
    this.predefinedLocations = predefinedLocations;
    this.googleApiKey = googleApiKey;
  }
 
  @Override
  public ListenableFuture<ImmutableList<GeocoderResults>> geocode(final LocationSet locationSet, final Location location) {
    URIBuilder uriBuilder;
    URI uriWithAddress;
    URI uriWithoutAddress;
    try {
      uriBuilder = new URIBuilder("https://maps.googleapis.com/maps/api/geocode/json");
      if (googleApiKey != null) {
        uriBuilder.addParameter("key", googleApiKey);
      }
  
      Multimap<String, String> components = LinkedHashMultimap.create();
      buildComponents(location.getParent(), components);
      
      List<String> parsedComponents = Lists.newArrayList();
      for (Map.Entry<String, String> componentEntry : components.entries()) {
        parsedComponents.add(componentEntry.getKey() + ":" + componentEntry.getValue()); 
      }
      uriBuilder.addParameter("components", Joiner.on('|').join(parsedComponents));
      
      uriWithoutAddress = uriBuilder.build();

      String address = getAddress(location);
      uriBuilder.addParameter("address", address);
      uriWithAddress = uriBuilder.build();
    } catch (URISyntaxException e) {
      throw new RuntimeException(e);
    }

    // Check for cached values;  short-circuit when present
    GoogleGeocodeResults withAddressResults = resultCache.getIfPresent(uriWithAddress);
    GoogleGeocodeResults withoutAddressResults = resultCache.getIfPresent(uriWithoutAddress);
    if (withAddressResults != null && withoutAddressResults != null) {
      return Futures.immediateFuture(toGeocoderResults(locationSet, location, withAddressResults, withoutAddressResults));
    }
    
    ListenableFuture<GoogleGeocodeResults> withAddressFuture = withAddressResults == null
        ? fetchGeocodeResults(uriWithAddress) : Futures.immediateFuture(withAddressResults);
    ListenableFuture<GoogleGeocodeResults> withoutAddressFuture = withoutAddressResults == null
        ? fetchGeocodeResults(uriWithoutAddress) : Futures.immediateFuture(withoutAddressResults);
    
    return Futures.transform(Futures.allAsList(withAddressFuture, withoutAddressFuture),
        resultsList -> toGeocoderResults(locationSet, location,
            resultsList.get(0), resultsList.get(1)), executorService);
  }

  private ListenableFuture<GoogleGeocodeResults> fetchGeocodeResults(final URI uri) {
    ListenableFuture<GoogleGeocodeResults> future = executorService.submit(new Callable<GoogleGeocodeResults>() {
      @Override
      public GoogleGeocodeResults call() throws Exception {
        if (Thread.interrupted()) {
          throw new CancellationException("Thread was interrupted");
        }
    
        HttpGet httpGet = new HttpGet(uri);
        httpGet.setConfig(REQUEST_CONFIG);
        final CloseableHttpResponse get = httpClient.execute(httpGet);
        HttpEntity entity = get.getEntity();
        Reader reader = new InputStreamReader(
            new BufferedInputStream(entity.getContent()),
            Charsets.UTF_8);
        try {
          return gson.fromJson(reader, GoogleGeocodeResults.class);
        } finally {
          reader.close();
        }
      }
    });
    
    
    // Cache results when returned
    Futures.addCallback(future, new FutureCallback<GoogleGeocodeResults>() {
      @Override public void onFailure(Throwable t) {
      }

      @Override
      public void onSuccess(GoogleGeocodeResults results) {
        resultCache.put(uri, results);
      }
    }, executorService);
    return future;
  }
  
  @Override
  public ListenableFuture<ImmutableList<GeocoderResults>> reverseGeocode(
      final LocationSet locationSet, final LatLongCoordinates latLong) {
    URIBuilder uriBuilder;
    final URI uri;
    try {
      uriBuilder = new URIBuilder("https://maps.googleapis.com/maps/api/geocode/json");
      if (googleApiKey != null) {
        uriBuilder.addParameter("key", googleApiKey);
      }
      uriBuilder.addParameter("latlng", String.format("%s,%s", latLong.latitudeAsCanonicalString(), latLong.longitudeAsCanonicalString()));  
      uri = uriBuilder.build();
    } catch (URISyntaxException e) {
      throw new RuntimeException(e);
    }

    // Check for cached values;  short-circuit when present
    GoogleGeocodeResults ifPresent = resultCache.getIfPresent(uri);
    if (ifPresent != null) {
      return Futures.immediateFuture(toReverseGeocoderResults(locationSet, latLong, ifPresent));
    }
    
    ListenableFuture<GoogleGeocodeResults> future = fetchReverseGeocodeResults(uri);

    return Futures.transform(future,
        results -> toReverseGeocoderResults(locationSet, latLong, results),
        executorService);
  }

  private ListenableFuture<GoogleGeocodeResults> fetchReverseGeocodeResults(final URI uri) {
    ListenableFuture<GoogleGeocodeResults> future = executorService.submit(new Callable<GoogleGeocodeResults>() {
      @Override
      public GoogleGeocodeResults call() throws Exception {
        if (Thread.interrupted()) {
          throw new CancellationException("Thread was interrupted");
        }
    
        HttpGet httpGet = new HttpGet(uri);
        httpGet.setConfig(REQUEST_CONFIG);
        final CloseableHttpResponse get = httpClient.execute(httpGet);
        HttpEntity entity = get.getEntity();
        Reader reader = new InputStreamReader(
            new BufferedInputStream(entity.getContent()),
            Charsets.UTF_8);
        try {
          return gson.fromJson(reader, GoogleGeocodeResults.class);
        } finally {
          reader.close();
        }
      }
    });
    
    
    // Cache results when returned
    Futures.addCallback(future, new FutureCallback<GoogleGeocodeResults>() {
      @Override public void onFailure(Throwable t) {
      }

      @Override
      public void onSuccess(GoogleGeocodeResults results) {
        resultCache.put(uri, results);
      }
    }, executorService);
    return future;
  }


  private ImmutableList<GeocoderResults> toGeocoderResults(
      LocationSet locationSet, Location location, GoogleGeocodeResults results,
      GoogleGeocodeResults withoutAddressResults) {
    if (results.results.isEmpty()) {
      return ImmutableList.of();
    }
    
    GoogleGeocodeResult firstResult = results.results.get(0);
    if (firstResult.geometry != null
        && "APPROXIMATE".equalsIgnoreCase(firstResult.geometry.location_type)) {
      // APPROXIMATE sometimes means that Google could not find the location at all, and is
      // just showing the center of the state/county etc.  Don't give the user false hope!
      // But, it also just means "this is a city, don't have an exact location".  So, compare
      // the bounds of this location to asking google for the location without any address field
      // at all (that is, asking for the bounds of the components).  If they're the same, then
      // it really is just a non-result.
      if (withoutAddressResults.results.isEmpty()
          || withoutAddressResults.results.get(0).geometry == null
          || firstResult.geometry == null
          || Objects.equals(firstResult.geometry.bounds, withoutAddressResults.results.get(0).geometry.bounds)) {
        return ImmutableList.of();
      }
    }
    
    if (firstResult.geometry != null
        && firstResult.geometry.location != null) {
      String lat = firstResult.geometry.location.lat;
      String lng = firstResult.geometry.location.lng;
      if (!Strings.isNullOrEmpty(lat) && !Strings.isNullOrEmpty(lng)) {
        GeocodeComponent state = firstResult.findAddressComponent("administrative_area_level_1");
        GeocodeComponent county = firstResult.findAddressComponent("administrative_area_level_2");
        Location preferredParent = findPreferredParent(locationSet, location, state, county);
        
        return ImmutableList.of(
            new GeocoderResults(
                Source.GOOGLE,
                location.getDisplayName(),
                LatLongCoordinates.withLatAndLong(lat, lng),
                preferredParent));
      }
    }
    
    return ImmutableList.of();
  }
  
  private ImmutableList<GeocoderResults> toReverseGeocoderResults(
      LocationSet locationSet, final LatLongCoordinates latLong, GoogleGeocodeResults results) {
    if (results.results.isEmpty()) {
      return ImmutableList.of();
    }
    
    Ordering<GeocoderResults> ordering = new Ordering<GeocoderResults>() {
      @Override
      public int compare(GeocoderResults left, GeocoderResults right) {
        double leftDistance = left.coordinates().kmDistance(latLong);
        double rightDistance = right.coordinates().kmDistance(latLong);
        if (leftDistance == rightDistance) {
          return 0;
        } else if (leftDistance < rightDistance) {
          return -1;
        } else {
          return 1;
        }
      }
    };
    
    ImmutableSortedSet.Builder<GeocoderResults> builder = ImmutableSortedSet.orderedBy(ordering);
    for (GoogleGeocodeResult result : results.results) {
      GeocodeComponent country = result.findAddressComponent("country");
      GeocodeComponent state = result.findAddressComponent("administrative_area_level_1");
      GeocodeComponent county = result.findAddressComponent("administrative_area_level_2");
      Location parent = findPreferredParent(locationSet, country, state,county);
      if (parent != null) {
        boolean allowCounty = Locations.getAncestorOfType(parent, Location.Type.county) == null; 
        String name = result.extractLocationName(allowCounty);
        // Skip anything with an empty name.  Don't think this should be happening,
        // but it's not supported.
        if (name.isEmpty()) {
          continue;
        }
        String lat = result.geometry.location.lat;
        String lng = result.geometry.location.lng;
        builder.add(new GeocoderResults(Source.GOOGLE, name, LatLongCoordinates.withLatAndLong(lat, lng), parent));
      }
    }
    
    List<GeocoderResults> list = Lists.newArrayList(builder.build());
    // Run through the list from the end to the start, and when we find items that
    // don't have a name and are *less* specific than the previous entry, drop them.
    // This eliminates countries etc.
    for (int i = list.size() - 1; i > 0; i--) {
      GeocoderResults last = list.get(i);
      GeocoderResults previous = list.get(i);
      if (last.name().isEmpty()) {
        Location lastPreferredParent = last.preferredParent().get();
        Location previousPreferredParent = previous.preferredParent().get();
        if (Locations.getCommonAncestor(lastPreferredParent, previousPreferredParent) == lastPreferredParent) {
          list.remove(i);
        }
      }
    }
    return ImmutableList.copyOf(list);
  }

  /**
   * Stop iterating up when you reach a city or town;  that should be enough for Google
   * to give a result, and adding more parents can just confuse things. 
   */
  private static final ImmutableSet<Location.Type> TYPES_TO_STOP_AT =
      ImmutableSet.of(Location.Type.city, Location.Type.town);
  /**
   * County/country will be handled with non-address fields, like "administrative_area".  State
   * *should* have, but does not.  Unfortunately, component filtering for administrative_area
   * influences results, but is not enforced, so:
   *   address=Springfield
   *   administrative_area=Illinois
   * comes back as Massachussetts!
   */
  private static final ImmutableSet<Location.Type> TYPES_TO_DROP_FROM_LOCATION =
      ImmutableSet.of(Location.Type.county, Location.Type.country);
  
  private String getAddress(Location location) {
    String name = location.getModelName();
    
    Location parent = location.getParent();
    while (parent != null
        && !TYPES_TO_DROP_FROM_LOCATION.contains(parent.getType())
        && !TYPES_TO_STOP_AT.contains(location.getType())) {
      name += ", " + parent.getModelName();
      parent = parent.getParent();
    }
    
    return name;
  }
  
  private void buildComponents(Location location, Multimap<String, String> components) {
    if (location == null) {
      return;
    }
    
    if (location.getType() != null) {
      switch (location.getType()) {
        case county:
          components.put("administrative_area", location.getModelName());
          break;
        case state:
          // Don't bother including eBird states for Indonesia - they simply confuse Google
          if (location.getEbirdCode() != null
              && location.getEbirdCode().startsWith("ID-")) {
            break;
          }
          components.put("administrative_area", location.getModelName());
          break;
        case country:
          String locationCode = Locations.getLocationCode(location);
          if (locationCode != null) {
            if (!"XX".equals(locationCode)) {
              if (locationCode.indexOf('-') > 0) {
                // Hack "GB-ENG" down to just "GB";  otherwise Google gets confused
                // and reports partial results unnecessarily
                locationCode = locationCode.substring(0, locationCode.indexOf('-'));
              }
              components.put("country", locationCode);
            }
          } else {
            components.put("country", location.getModelName());
          }
          // Stop at the first country;  otherwise, geocoding gets *very* confused with
          // the dual countries (which breaks Puerto Rico, for instance)
          return;
          
        default:
          break;
      }
    }
    
    buildComponents(location.getParent(), components);
  }  
  
  private final static ImmutableMap<String, String> INDONESIAN_PROVINCE_NAMES = ImmutableMap.<String, String>builder()
      .put("Special Region of Aceh", "Sumatera")
      .put("Aceh", "Sumatera")
      .put("Bali", "Nusa Tenggara")
      .put("Bangka–Belitung Islands", "Sumatera")
      .put("Bangka Belitung Islands", "Sumatera")
      .put("Banten", "Jawa")
      .put("Bengkulu", "Sumatera")
      .put("Central Java", "Jawa")
      .put("Central Kalimantan", "Kalimantan")
      .put("Central Sulawesi", "Sulawesi")
      .put("East Java", "Jawa")
      .put("East Kalimantan", "Kalimantan")
      .put("East Nusa Tenggara", "Nusa Tenggara")
      .put("Gorontalo", "Sulawesi")
      .put("Jakarta Special Capital Region", "Jawa")
      .put("Special Capital Region of Jakarta", "Jawa")
      .put("Jambi", "Sumatera")
      .put("Lampung", "Sumatera")
      .put("Maluku", "Maluku")
      .put("North Kalimantan", "Kalimantan")
      .put("North Maluku", "Maluku")
      .put("North Sulawesi", "Sulawesi")
      .put("North Sumatra", "Sumatera")
      .put("Papua", "Papua")
      .put("Special Region of Papua", "Papua")
      .put("Riau", "Sumatera")
      .put("Riau Islands", "Sumatera")
      .put("Southeast Sulawesi", "Sulawesi")
      .put("South Kalimantan", "Kalimantan")
      .put("South Sulawesi", "Sulawesi")
      .put("South Sumatra", "Sumatera")
      .put("West Java", "Jawa")
      .put("West Kalimantan", "Kalimantan")
      .put("West Nusa Tenggara", "Nusa Tenggara")
      .put("Special Region of West Papua", "Papua")
      .put("West Papua", "Papua")
      .put("West Sulawesi", "Sulawesi")
      .put("West Sumatra", "Sumatera")
      .put("Special Region of Yogyakarta", "Jawa")
      .put("Yogyakarta", "Jawa")
      .build();
  
  private Location findPreferredParent(LocationSet locationSet, Location location, GeocodeComponent state,
      GeocodeComponent county) {
    Location currentParent = getFirstParentWithCode(location);
    // If we couldn't find a parent with an ISO code, bail.
    // If there's a non-built-in location between this location and the parent, also bail:  the
    // user's been explicit about where to put this location, don't go elsewhere.
    if (currentParent == null || currentParent != location.getParent()) {
      return null;
    }
    
    if (currentParent.getType() == Location.Type.country
        && state != null
        && state.long_name != null) {
      String stateName = state.long_name;
      // Google supports the full ISO-3166-2 nameset.  eBird (and therefore Scythebill)
      // do not.  Map back to the actual province names.
      if ("ID".equals(currentParent.getEbirdCode())
          && INDONESIAN_PROVINCE_NAMES.containsKey(stateName)) {
        stateName = INDONESIAN_PROVINCE_NAMES.get(stateName);
      }
      
      Location child = findPredefinedChild(locationSet, currentParent, stateName);
      if (child != null) {
        currentParent = child;
      }
    }
    
    if (currentParent.getType() != Location.Type.county 
        && county != null
        && county.long_name != null) {
      String countyName = county.long_name;
      Location child = findPredefinedChild(locationSet, currentParent, countyName);
      if (child != null) {
        currentParent = child;
      }
    }

    return currentParent;
  }

  /**
   * Preferred parent routine for reverse geocoding.
   */
  private Location findPreferredParent(
      LocationSet locationSet,
      GeocodeComponent countryComponent,
      GeocodeComponent state,
      GeocodeComponent county) {
    if (countryComponent == null
        || countryComponent.short_name == null) {
      // TODO: need to track down all the (many) instances where the Scythebill country code
      // does not match the Google country code
      return null;
    }
    
    // Find all countries with that location code (e.g., the US)
    Collection<Location> allCountries = locationSet.getLocationsByCode(countryComponent.short_name);
    if (allCountries.isEmpty()) {
      return null;
    }
 
    Location currentParent = null;
    String stateCode = null;
    if (state != null && state.short_name != null) {
      // See if the state is directly defined;  if so, jump there
      stateCode = countryComponent.short_name + "-" + state.short_name;
      Location stateByCode = locationSet.getLocationByCode(stateCode);
      if (stateByCode != null) {
        currentParent = stateByCode;
      }
    }

    if (currentParent == null) {
      // For each parent, look for child with that name.
      for (Location country : allCountries) {
        if (country.getType() == Location.Type.country
            && state != null
            && state.long_name != null) {
          String stateName = state.long_name;
          // Google supports the full ISO-3166-2 nameset.  eBird (and therefore Scythebill)
          // do not.  Map back to the actual province names.
          if ("ID".equals(country.getEbirdCode())
              && INDONESIAN_PROVINCE_NAMES.containsKey(stateName)) {
            stateName = INDONESIAN_PROVINCE_NAMES.get(stateName);
          }
          
          // Try to find a child;  if that succeeded, then done - this version of the country must be legit
          // First, check by state code
          if (stateCode != null) {
            PredefinedLocation childByCode = predefinedLocations.getPredefinedLocationChildByCode(country, stateCode);
            if (childByCode != null) {
              // Found one - done
              currentParent = childByCode.create(locationSet, country);
              break;
            }
          }

          // Then, check by name
          Location child = findPredefinedChild(locationSet, country, stateName);
          if (child != null) {
            // Found a child by name - done
            currentParent = child;
            break;
          }
          
        }
      }
    }
    
    if (currentParent == null) {
      currentParent = allCountries.iterator().next();
    }


    if (currentParent.getType() != Location.Type.county 
        && county != null
        && county.long_name != null) {
      String countyName = county.long_name;
      Location child = findPredefinedChild(locationSet, currentParent, countyName);
      if (child != null) {
        currentParent = child;
      }
    }

    return currentParent;
  }

  /**
   * Find a predefined child with a given name.
   * In theory, it *might* be useful to support non-predefined children, but without care this
   * will go very wrong.  For example, take the city of Yavi in Jujuy Province, Argentina.
   * That geocodes as Yavi in Yavi County (really, Department) in Jujuy State.  Since the names
   * match, if you're not careful, you end up finding Yavi as its own preferred parent.
   * The simple fix is not supporting auto-geocoding into anything that's not predefined.
   */
  // TODO: support taking explicit codes too, so we can first match on ISO codes and *then* look for things
  // by name.  This will help situations where the Google and Scythebill names don't quite match.
  private Location findPredefinedChild(LocationSet locationSet, Location parent, String name) {
    Location content = parent.getContent(name);
    if (content != null && content.isBuiltInLocation()) {
      return content;
    }
    
    Location predefinedLocation = predefinedLocations.createPredefinedLocationChildRecursively(locationSet, parent, name);
    if (predefinedLocation != null) {
      return predefinedLocation;
    }
    
    // Try re-searching for a name dropping the last word.  This accounts for cases where Google
    // adds "County" to county names, or returns "Hebei Sheng" instead of "Hebei".
    int lastSpace = name.lastIndexOf(' ');
    if (lastSpace > 0) {
      return findPredefinedChild(locationSet, parent, name.substring(0, lastSpace));
    }
    
    return null;
  }
  
  private Location getFirstParentWithCode(Location location) {
    while (location != null) {
      if (location.getEbirdCode() != null) {
        break;
      }
      location = location.getParent();
    }
    return location;
  }

  static class GoogleGeocodeResults {
    String status;
    List<GoogleGeocodeResult> results;
    
    @Override
    public String toString() {
      return MoreObjects.toStringHelper(this)
          .add("status", status)
          .add("results", results)
          .omitNullValues()
          .toString();
    }
  }
  
  static class GoogleGeocodeResult {
    String formatted_address;
    List<GeocodeComponent> address_components;
    GeocodeGeometry geometry;
    
    @Override
    public String toString() {
      return MoreObjects.toStringHelper(this)
          .add("formatted_address", formatted_address)
          .add("address_components", address_components)
          .add("geometry", geometry)
          .omitNullValues()
          .toString();
    }

    public GeocodeComponent findAddressComponent(String type) {
      for (GeocodeComponent addressComponent : address_components) {
        if (addressComponent.types.contains(type)) {
          return addressComponent;
        }
      }
      return null;
    }

    private static final ImmutableSet<String> EXCLUDED_ADDRESS_COMPONENT_TYPES = ImmutableSet.of(
        "street_number");

    private static final ImmutableSet<String> TERMINATING_ADDRESS_COMPONENT_TYPES = ImmutableSet.of(
        "administrative_area_level_2",
        "administrative_area_level_1",
        "country");
    
    private static final ImmutableSet<String> TERMINATING_ADDRESS_COMPONENT_TYPES_ALLOWING_COUNTY = ImmutableSet.of(
        "administrative_area_level_1",
        "country");

    public String extractLocationName(boolean allowingCounty) {
      List<String> components = Lists.newArrayList();
      for (GeocodeComponent addressComponent : address_components) {
        if (!Sets.intersection(addressComponent.types, EXCLUDED_ADDRESS_COMPONENT_TYPES).isEmpty()) {
          continue;
        }
        if (!Sets.intersection(addressComponent.types,
            allowingCounty ? TERMINATING_ADDRESS_COMPONENT_TYPES_ALLOWING_COUNTY : TERMINATING_ADDRESS_COMPONENT_TYPES)
            .isEmpty()) {
          break;
        }
        components.add(addressComponent.long_name);
        // Once anything is present, omit the county
        allowingCounty = false;
      }
      return Joiner.on(' ').join(components);
    }
  }
  
  static class GeocodeComponent {
    String long_name;
    String short_name;
    Set<String> types;

    @Override
    public String toString() {
      return MoreObjects.toStringHelper(this)
          .add("long_name", long_name)
          .add("short_name", short_name)
          .add("types", types)
          .omitNullValues()
          .toString();
    }

  }
  
  static class GeocodeGeometry {
    GeocodeBounds bounds;
    GeocodeLocation location;
    String location_type;

    @Override
    public String toString() {
      return MoreObjects.toStringHelper(this)
          .add("location", location)
          .add("location_type", location_type)
          .add("bounds", bounds)
          .omitNullValues()
          .toString();
    }
  }

  static class GeocodeBounds {
    GeocodeLocation northeast;
    GeocodeLocation southwest;

    @Override
    public String toString() {
      return String.format("{northwest: %s, southeast: %s}", northeast, southwest);
    }

    @Override
    public int hashCode() {
      return Objects.hash(northeast, southwest);
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (!(obj instanceof GeocodeBounds)) {
        return false;
      }
      GeocodeBounds other = (GeocodeBounds) obj;
      return Objects.equals(northeast, other.northeast)
          && Objects.equals(southwest, other.southwest);
    }
    
  }
 
  static class GeocodeLocation {
    String lat;
    String lng;

    @Override
    public String toString() {
      return String.format("{lat: %s, lng: %s}", lat, lng);
    }
    
    @Override
    public int hashCode() {
      return Objects.hash(lat, lng);
    }
    
    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (!(obj instanceof GeocodeLocation)) {
        return false;
      }
      GeocodeLocation other = (GeocodeLocation) obj;
      return Objects.equals(lat, other.lat) && Objects.equals(lng, other.lng);
    }    
  }
}
