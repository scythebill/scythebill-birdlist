/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions.locationapi.google;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import javax.annotation.Nullable;
import javax.imageio.ImageIO;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;

import com.google.common.base.Preconditions;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningScheduledExecutorService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.ui.guice.GoogleApiKey;

/**
 * Loads images using the Google static image API.
 */
@Singleton
// TODO: support attaching labels to the associated marker
public class GoogleStaticGeoImageLoader {
  private static final RequestConfig REQUEST_CONFIG = RequestConfig.custom()
      .setConnectionRequestTimeout(5000)
      .setConnectTimeout(10000)
      .setSocketTimeout(10000)
      .build();
  private static final Cache<RequestKey, BufferedImage> cache = CacheBuilder.newBuilder()
      .maximumSize(100)
      // If someone keeps Scythebill open forever, drop these results after 1 day
      // to avoid enshrining stale data
      .expireAfterWrite(1, TimeUnit.DAYS)
      .build();
  /** Max free width. */
  private static final int MAX_WIDTH = 640;
  /** Max free height. */
  private static final int MAX_HEIGHT = 640;
  
  private final CloseableHttpClient httpClient;
  private final ListeningScheduledExecutorService executorService;
  private final String googleApiKey;

  @Inject
  public GoogleStaticGeoImageLoader(
      CloseableHttpClient httpClient,
      ListeningScheduledExecutorService executorService,
      @Nullable @GoogleApiKey String googleApiKey) {
    this.httpClient = httpClient;
    this.executorService = executorService;
    this.googleApiKey = googleApiKey;
  }
  
  public ListenableFuture<BufferedImage> asyncLoad(
      LatLongCoordinates coordinates,
      Dimension size,
      int zoomLevel) {
    final RequestKey requestKey = new RequestKey(coordinates, size, zoomLevel);
    BufferedImage ifPresent = cache.getIfPresent(requestKey);
    if (ifPresent != null) {
      return Futures.immediateFuture(ifPresent);
    }
    final AtomicReference<ListenableFuture<?>> reference = new AtomicReference<ListenableFuture<?>>();
    ListenableFuture<BufferedImage> future = executorService.submit(new Callable<BufferedImage>() {
      @Override
      public BufferedImage call() throws Exception {
        checkCancelled();
        InputStream inputStream = load(requestKey);
        try {
          checkCancelled();
          BufferedImage bufferedImage = ImageIO.read(inputStream);
          cache.put(requestKey, bufferedImage);
          return bufferedImage;
        } finally {
          inputStream.close();
        }
      }
      
      // TODO: this is crazy, surely there's a simpler way 
      // for a task to realize its containing future has been cancelled?
      private void checkCancelled() throws Exception {
        ListenableFuture<?> future = reference.get();
        if (future != null && future.isCancelled()) {
          throw new CancellationException();
        }
      }
    });
    reference.set(future);
    return future;
  }
  
  public Dimension allowedDimension(Dimension dimension) {
    return new Dimension(
        Math.min(dimension.width, MAX_WIDTH),
        Math.min(dimension.height, MAX_HEIGHT));
  }
  
  private InputStream load(RequestKey requestKey) throws IOException {
    String url = String.format("https://maps.googleapis.com/maps/api/staticmap?center=%s,%s"
        + "&language=%s&size=%sx%s&zoom=%s&maptype=terrain&markers=color%%3ared%%7c%s,%s",
        requestKey.coordinates.latitudeAsCanonicalString(),
        requestKey.coordinates.longitudeAsCanonicalString(),
        Locale.getDefault().getLanguage(),
        requestKey.width, requestKey.height,
        requestKey.zoomLevel,
        requestKey.coordinates.latitudeAsCanonicalString(), requestKey.coordinates.longitudeAsCanonicalString());
    if (googleApiKey != null) {
      url += "&key=" + googleApiKey;
    }
    
    try {
      HttpGet httpGet = new HttpGet(url);
      httpGet.setConfig(REQUEST_CONFIG);
      final CloseableHttpResponse get = httpClient.execute(httpGet);
      HttpEntity entity = get.getEntity();
      return new BufferedInputStream(entity.getContent());
    } catch (ClientProtocolException e) {
      throw new IOException(e);
    }
  }
  
  static final class RequestKey {
    final LatLongCoordinates coordinates;
    final int width;
    final int height;
    final int zoomLevel;
    
    RequestKey(LatLongCoordinates coordinates, Dimension size, int zoomLevel) {
      this.coordinates = Preconditions.checkNotNull(coordinates);
      this.width = size.width;
      this.height = size.height;
      this.zoomLevel = zoomLevel;
    }

    @Override
    public int hashCode() {
      int result = coordinates.hashCode();
      result = 31 * result + height;
      result = 31 * result + width;
      result = 31 * result + zoomLevel;
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      
      if (!(obj instanceof RequestKey)) {
        return false;
      }
      
      
      RequestKey that = (RequestKey) obj;
      return coordinates.equals(that.coordinates)
          && width == that.width
          && height == that.height
          && zoomLevel == that.zoomLevel;
    }
  }
}
