/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.app;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.name.Names;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.ui.events.EventsModule;
import com.scythebill.birdlist.ui.guice.InitialRunWindowModule;
import com.scythebill.birdlist.ui.guice.PerWindowActionsModule;
import com.scythebill.birdlist.ui.guice.ReportSetModule;
import com.scythebill.birdlist.ui.panels.PanelsModule;

/**
 * Creates the per-window injector with access to per-window state.
 */
public abstract class FrameFactory {
  // TODO: don't bind a filename;  just bind a File object
   private final static String REPORT_FILE_KEY_NAME = "com.adamwiner.birdlist.ui.reportFile";

   public FrameFactory() {     
   }
   
   /**
    * Creates the injector used for running the Open/New UI.
    */
   public Injector createOpenOrNewFrameInjector(
       Injector globalInjector, Startup startup) {
     return globalInjector.createChildInjector(Iterables.concat(
         ImmutableList.of(
             new InitialRunWindowModule(),
             new EventsModule(),
             startup.constructedObjectsModule()),
         getCustomPerWindowModules()));
   }

   /**
    * Creates the injector used for the main UI.
    */
   public Injector createMainFrameInjector(Injector globalInjector,
       Startup startup,
       final String reportSetFilename,
       final ReportSet reportSet) {
     return globalInjector.createChildInjector(Iterables.concat(
         getCustomPerWindowModules(),
         ImmutableList.of(
             new PerWindowActionsModule(),
             new ReportSetModule(),
             new PanelsModule(),
             new EventsModule(),
             new AbstractModule() {
               @Override protected void configure() {
                 bindConstant().annotatedWith(Names.named(REPORT_FILE_KEY_NAME))
                     .to(reportSetFilename);
                 bind(ReportSet.class).toInstance(reportSet);
               }
             },
             startup.constructedObjectsModule())));
   }

  abstract protected Iterable<? extends Module> getCustomPerWindowModules();
}
