/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.app;

import java.awt.EventQueue;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.swing.JOptionPane;

import com.google.common.base.Preconditions;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.MapMaker;
import com.google.common.io.Files;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.HtmlResponseWriter;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.upgrades.OneTimeUpgradeProcessor;
import com.scythebill.birdlist.model.sighting.upgrades.UpgraderProcessing;
import com.scythebill.birdlist.model.taxa.CompletedUpgrade;
import com.scythebill.birdlist.model.taxa.TaxonomyMappings;
import com.scythebill.birdlist.model.taxa.UnsupportedTaxonomyException;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences;
import com.scythebill.birdlist.model.xml.VersionTooNewException;
import com.scythebill.birdlist.ui.backup.BackupSaver;
import com.scythebill.birdlist.ui.backup.BegForBackups;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.FilePreferences;
import com.scythebill.birdlist.ui.panels.MainFrame;
import com.scythebill.birdlist.ui.panels.OpenOrNewListFrame;
import com.scythebill.birdlist.ui.panels.TaxonomyUpgradeNotice;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.prefs.PreferencesManager;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;

/**
 * Global registry of all frames.  Handles UI for loading framesets.
 * TODO: doesn't belong in this package.
 */
@Singleton
public class FrameRegistry {
  private static final Logger logger = Logger.getLogger(FrameRegistry.class.getName());

  private final Map<String, LoadedFrame> frames = new MapMaker().makeMap();
  private final Map<String, LoadedFrame> framesBeingReloaded = new MapMaker().makeMap();
  private final Map<String, Boolean> framesInProgress = new MapMaker().makeMap();
  private final ListeningExecutorService executor;
  private final Startup startup;
  /** Injector of global state */
  private final Injector globalInjector;
  private final FilePreferences filePreferences;
  private OpenOrNewListFrame openOrNewFrame;
  private final FrameFactory frameFactory;

  private final TaxonomyMappings taxonomyMappings;
  private final Checklists checklists;
  private final UpgraderProcessing upgraderProcessing;
  private final BackupSaver backupSaver;
  private final FontManager fontManager;
  private final PreferencesManager preferencesManager;
  private final FileDialogs fileDialogs;
  private final Alerts alerts;
  private final NamesPreferences namesPreferences;

  private OneTimeUpgradeProcessor oneTimeUpgradeProcessor;

  private BegForBackups begForBackups;


  static class LoadedFrame {
    final MainFrame mainFrame;
    final ReportSet reportSet;
    final Closeable backup;
    
    LoadedFrame(MainFrame mainFrame, ReportSet reportSet, Closeable backup) {
      this.backup = backup;
      this.mainFrame = Preconditions.checkNotNull(mainFrame);
      this.reportSet = Preconditions.checkNotNull(reportSet);
    }
  }
  
  @Inject
  public FrameRegistry(
      Injector globalInjector,
      Startup startup,
      ListeningExecutorService executor,
      PreferencesManager preferencesManager,
      FilePreferences filePreferences,
      NamesPreferences namesPreferences,
      FrameFactory frameFactory,
      BegForBackups begForBackups,
      BackupSaver backupSaver,
      TaxonomyMappings taxonomyMappings,
      UpgraderProcessing upgraderProcessing,
      OneTimeUpgradeProcessor oneTimeUpgradeProcessor,
      FontManager fontManager,
      Checklists checklists,
      FileDialogs fileDialogs,
      Alerts alerts) {
    this.globalInjector = globalInjector;
    this.startup = startup;
    this.executor = executor;
    this.preferencesManager = preferencesManager;
    this.filePreferences = filePreferences;
    this.namesPreferences = namesPreferences;
    this.frameFactory = frameFactory;
    this.begForBackups = begForBackups;
    this.backupSaver = backupSaver;
    this.taxonomyMappings = taxonomyMappings;
    this.upgraderProcessing = upgraderProcessing;
    this.oneTimeUpgradeProcessor = oneTimeUpgradeProcessor;
    this.fontManager = fontManager;
    this.checklists = checklists;
    this.fileDialogs = fileDialogs;
    this.alerts = alerts;
  }
  
  /**
   * Start loading a report set.
   * <p>
   * If the file is already loaded, activate the window.
   * <p>
   * If the file is in the process of loading, do nothing.
   * <p>
   * Otherwise, kick off a background task loading the file.
   * This shows progress during startup, but:
   * TODO: show some sort of progress even when startup has completed
   * TODO: change "filename" to be an an InputSupplier<InputStream>.
   * 
   * @param onFailure a runnable to be called - on the event dispatcher thread -
   * if a failure occurs (and after an error message has been shown)
   */
  public void startLoadingReportSet(
      String newFilename,
      @Nullable final Runnable onFailure) {
    LoadedFrame loadedFrame = frames.get(newFilename);
    if (loadedFrame != null) {
      // Already loaded
      loadedFrame.mainFrame.toFront();
    } else if (framesInProgress.containsKey(newFilename)) {
      // Still loading, ignore
      logger.info("Still loading " + newFilename);
    } else {
      String filename = potentiallyMoveFileOutOfDangerousLocation(newFilename);
      
      logger.info("Initiating load of " + filename);
      final File file = new File(filename);
      ReportSetLoader reportSetLoader = new ReportSetLoader(
          file,
          startup.getTaxonomyFuture(),
          startup.getIocTaxonomyFuture(),
          taxonomyMappings,
          checklists,
          namesPreferences,
          oneTimeUpgradeProcessor);
      // If we're in the progress of startup, pass this loader on to it
      startup.addProgress(reportSetLoader, 0.4);
     
      framesInProgress.put(filename, true);
      ListenableFuture<ReportSet> future = executor.submit(reportSetLoader);
      future.addListener(
          () -> {
            logger.info("Completed load of " + filename);
            EventQueue.invokeLater(() -> {
               onReportSetLoaded(file, future, onFailure);
            });
          },
          executor);
    }
  }
  
  private String potentiallyMoveFileOutOfDangerousLocation(String filename) {
    File file = new File(filename);
    Optional<String> dangerousPath = fileDialogs.findDangerousPath(file);
    if (dangerousPath.isPresent()) {
      if (alerts.showYesNo(null,
          Name.PICK_A_NEW_LOCATION,
          Name.SAVING_FILES_NOT_RECOMMENDED_FORMAT, dangerousPath.get())
          == JOptionPane.YES_OPTION) {
        String newFile = moveFileToNewLocation(file);
        if (newFile != null) {
          return newFile;
        }
      }
    }
    
    if (fileDialogs.isInsideZipFile(file)) {
      if (alerts.showYesNo(null, Name.PICK_A_NEW_LOCATION,
          Name.OPENING_FILES_IN_ZIP_WARNING) == JOptionPane.YES_OPTION) {
        String newFile = moveFileToNewLocation(file);
        if (newFile != null) {
          return newFile;
        }
      }
    }
    
    return filename;
  }

  private String moveFileToNewLocation(File file) {
    while (true) {
      File saveFile = fileDialogs.saveFile(null, Messages.getMessage(Name.NEW_LOCATION),
          file.getName(), null, FileType.SIGHTINGS);
      if (saveFile == null) {
        return null;
      }
      // Try to create the file.  If that fails, the directory is likely not writable.
      try {
        if (!saveFile.exists()) {
          if (!saveFile.createNewFile()) {
            throw new IOException("Could not create new file");
          }
        }
      } catch (IOException e) {
        FileDialogs.showFileSaveError(alerts, e, saveFile);
        // Try again.
        continue;
      }
      
      // Save file now exists;  try to write to it.
      try {
        Files.copy(file, saveFile);
      } catch (IOException e) {
        FileDialogs.showFileSaveError(alerts, e, saveFile);
        // Try again.
        continue;
      }
      
      return saveFile.getAbsolutePath();
    }
  }

  /**
   * Displays a report set that has already been loaded.
   * Used by the initial app launch code.
   */
  public void displayLoadedReportSet(
      File file,
      ReportSet loadedReportSet) {
    LoadedFrame loadedFrame = frames.get(file.getAbsolutePath());
    if (loadedFrame != null) {
      // Already loaded
      loadedFrame.mainFrame.toFront();
    } else {
      onReportSetLoaded(file, Futures.immediateFuture(loadedReportSet), null);
    }
  }

  private void onReportSetLoaded(
      final File file,
      Future<ReportSet> future,
      @Nullable final Runnable onFailure) {
    final String filename = file.getAbsolutePath();
    ReportSet loadedReportSet;
    try {
      loadedReportSet = future.get();
    } catch (Exception e) {
      if (e.getCause() instanceof VersionTooNewException) {
        String version = ((VersionTooNewException) e.getCause()).getVersion();
        alerts.showError(null,
            Name.USING_OLD_VERSION_TITLE,
            Name.USING_OLD_VERSION_FORMAT,
            HtmlResponseWriter.htmlEscape(file.getName()),
            HtmlResponseWriter.htmlEscape(version));
      } else if (e.getCause() instanceof UnsupportedTaxonomyException) {
        // This is not really a version... but it is a good fallback if I
        // botch things and fail to update the .bsxm version number when
        // updating the eBird taxonomy.
        String version = ((UnsupportedTaxonomyException) e.getCause()).getVersion();
        alerts.showError(null,
            Name.USING_OLD_VERSION_TITLE,
            Name.USING_OLD_VERSION_FORMAT,
            HtmlResponseWriter.htmlEscape(file.getName()),
            HtmlResponseWriter.htmlEscape(version));
      } else if (e.getCause() instanceof FileNotFoundException) {
        alerts.showError(null,
            Name.IO_EXCEPTION_TITLE,
            Name.IO_EXCEPTION_FORMAT,
            HtmlResponseWriter.htmlEscape(file.getName()),
            HtmlResponseWriter.htmlEscape(file.getParentFile().getName()));
      } else {
        alerts.showError(null,
            e,
            Name.BAD_FILE_TITLE,
            Name.BAD_FILE_FORMAT,
            HtmlResponseWriter.htmlEscape(file.getName()));
      }
      logger.log(Level.WARNING, "Error loading report set", e);
      framesInProgress.remove(filename);
      if (onFailure != null) {
        onFailure.run();
      }
      return;
    }
    
    ImmutableSet<SightingTaxon> spsToResolve = ImmutableSet.of();
    final ReportSet reportSet = loadedReportSet;
    // Perform any automatic version upgrades.
    upgraderProcessing.upgradeReportSet(reportSet);
    
    // Warn users of location corruption.
    if (!reportSet.getLocations().restoredLocations().isEmpty()) {
      alerts.showError(null,
          Name.CORRUPTED_LOCATIONS_TITLE,
          Name.CORRUPTED_LOCATIONS_FORMAT,
          HtmlResponseWriter.htmlEscape(file.getName()),
          reportSet.getLocations().restoredLocations().size());
    }
    
    if (reportSet.getCompletedUpgrade() != null) {
      // Mark the reportset as dirty.  For IOC upgrades, it's technically possible that this won't actually
      // change anything, but this will at least make sure that the per-report-set pref for "last IOC" does
      // get updated.
      reportSet.markDirty();
      
      CompletedUpgrade completedUpgrade = reportSet.getCompletedUpgrade();
      
      // We need to get the current TaxonomyStore object, so we can display a sensible UI here.
      // Cheat and show the "Open-Or-New" injector - technically, we don't need that entire state,
      // but it's awfully convenient.
      Injector storeInjector = frameFactory.createOpenOrNewFrameInjector(globalInjector, startup);
      TaxonomyStore taxonomyStore = storeInjector.getInstance(TaxonomyStore.class);
      TaxonomyUpgradeNotice taxonomyUpgradeNotice = new TaxonomyUpgradeNotice(
          startup.getTaxonomy(), alerts, fontManager, taxonomyStore);

      if (completedUpgrade.getForcedTaxonomy() != null) {
        spsToResolve = ImmutableSet.copyOf(
            taxonomyUpgradeNotice.showNonClementsUpgrade(reportSet, file, completedUpgrade));
      } else {
        // Force the taxonomy to eBird/Clements.  For years, I tried doing it with IOC or eBird, but in 2022
        // this really confused some users;  I don't think "their last taxonomy was IOC" is a particularly
        // strong signal that they only care about IOC, and failing to do this upgrade process as
        // eBird/Clements leaves their sightings with a lot of sp.'s.
        taxonomyStore.setTaxonomy(startup.getTaxonomy());
        spsToResolve = ImmutableSet.copyOf(
            taxonomyUpgradeNotice.showClementsUpgrade(reportSet, file, reportSet.getCompletedUpgrade(), backupSaver));
      }
      
      ImmutableSet<SightingTaxon> remainingTaxaAfterChecklistResolution = taxonomyUpgradeNotice
          .attemptChecklistResolution(reportSet, checklists, spsToResolve,
              completedUpgrade.getForcedTaxonomy() == taxonomyStore.getIoc());
      if (remainingTaxaAfterChecklistResolution != null) {
        spsToResolve = remainingTaxaAfterChecklistResolution;
      }
      reportSet.setSpsToResolve(spsToResolve, completedUpgrade.getForcedTaxonomy());
    }

    // Create an injector with globals, the startup-constructed objects,
    // and the report set
    Injector frameInjector = frameFactory.createMainFrameInjector(
        globalInjector, startup, filename, reportSet);
    
    startup.finish();
    
    // Hide the frame shown by default
    if (openOrNewFrame != null) {
      openOrNewFrame.dispose();
      openOrNewFrame = null;
    }
    
    // Navigate to "resolveTaxa" if necessary (there's sp's to clear up), otherwise
    // go to "main".
    boolean needToResolveTaxa =
        (reportSet.getSpsToResolve() != null)
        && !reportSet.getSpsToResolve().isEmpty();
    final MainFrame newFrame = MainFrame.start(
        frameInjector, needToResolveTaxa ? "resolveTaxa" : "main");
    
    Closeable backup = backupSaver.initiateBackup(reportSet, file);
    final LoadedFrame loadedFrame = new LoadedFrame(newFrame, reportSet, backup);
    frames.put(filename, loadedFrame);
    framesInProgress.remove(filename);
    LoadedFrame oldFrame = framesBeingReloaded.get(filename);
    if (oldFrame != null) {
      oldFrame.mainFrame.dispose();
    }
    
    filePreferences.setLastLoadedReportSets(frames.keySet());
    filePreferences.rememberFile(file, FileType.SIGHTINGS);

    newFrame.addWindowListener(new WindowAdapter() {
      @Override public void windowClosed(WindowEvent event) {
        logger.info("Closed " + filename);
        try {
          loadedFrame.backup.close();
        } catch (IOException e) {
          logger.log(Level.SEVERE, "Closing backup failed", e);
        }
        if (framesBeingReloaded.containsKey(filename)
            && framesBeingReloaded.get(filename).mainFrame == newFrame) {
          framesBeingReloaded.remove(filename);
        }
        
        if (frames.containsKey(filename)
            && frames.get(filename).mainFrame == newFrame) {
          frames.remove(filename);
        }
        
        if (frames.isEmpty() && framesBeingReloaded.isEmpty()) {
          handleNoVisibleFrames();
        } else {
          filePreferences.setLastLoadedReportSets(frames.keySet());
        }
      }
    });
    
    begForBackups.check(reportSet, file);
  }

  /** Returns true if there are no frames loaded or in the process of being loaded. */
  public boolean isEmpty() {
    return frames.isEmpty() && framesInProgress.isEmpty() && framesBeingReloaded.isEmpty();
  }
  
  public boolean saveBeforeQuitting() {
    // Iterate through all frames, making a copy of the full list
    // first (since closing windows will remove them from the list)
    for (LoadedFrame frame : ImmutableList.copyOf(frames.values())) {
      frame.mainFrame.toFront();
      if (!frame.mainFrame.saveBeforeClosing()) {
        // If any window cancels, stop there
        return false;
      }
      frame.mainFrame.dispose();
    }
    
    return true;
  }

  public void showNoOtherWindowFrame() {
    Injector openOrNewInjector = frameFactory.createOpenOrNewFrameInjector(
        globalInjector, startup);
    openOrNewFrame = openOrNewInjector.getInstance(OpenOrNewListFrame.class);
    openOrNewFrame.start();
    openOrNewFrame.addWindowListener(new WindowAdapter() {
      @Override public void windowClosing(WindowEvent event) {
        if (isEmpty()) {
          handleNoVisibleFrames();
        }
      }
      
    });
  }

  public Iterable<ReportSet> loadedReportSets() {
    return FluentIterable.from(frames.values()).transform(loadedFrame -> loadedFrame.reportSet);
  }
  
  /**
   * Called when there are no longer any visible frames.
   * (Except for, possibly, the About... frame, not handled here)
   */
  private void handleNoVisibleFrames() {
    preferencesManager.save();
    // TODO: is this correct behavior?
    System.exit(0);
  }

  public void reload(File reportSetFile, MainFrame mainFrame) {
    String fileName = reportSetFile.getPath();
    LoadedFrame loadedFrame = Preconditions.checkNotNull(frames.remove(fileName));
    framesBeingReloaded.put(fileName, loadedFrame);
    
    // Disable this frame, so it doesn't accept input while being reloaded.
    // TODO: would be better to make it appear grayed-out
    mainFrame.setEnabled(false);
    
    startLoadingReportSet(fileName, () -> {
      logger.warning("Failed to reload " + fileName);
      mainFrame.setEnabled(true);
      frames.put(fileName, loadedFrame);
      framesBeingReloaded.remove(fileName);
    });
  }
}
