package com.scythebill.birdlist.ui.app;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import com.scythebill.birdlist.model.io.ProgressInputStream;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.Progress;
import com.scythebill.birdlist.model.xml.XmlTaxonImport;

public class MappedTaxonomyLoader implements Callable<MappedTaxonomy>, Progress {
  private volatile ProgressInputStream progressStream;
  private final Future<Taxonomy> baseTaxonomy;
  private final TaxonomyReference reference;

  public MappedTaxonomyLoader(
      TaxonomyReference reference, Future<Taxonomy> baseTaxonomy) {
    this.reference = reference;
    this.baseTaxonomy = baseTaxonomy;
  }

  @Override
  public MappedTaxonomy call() throws Exception {
    progressStream = new ProgressInputStream(reference.streamProvider.openStream());
    BufferedReader reader = new BufferedReader(new InputStreamReader(
        progressStream, Charset.forName("UTF-8")));
    try {
      return (new XmlTaxonImport()).importMappedTaxa(reader, baseTaxonomy.get());
    } finally {
      reader.close();
    }
  }

  @Override
  public long current() {
    if (progressStream == null) {
      return 0L;
    }
    return progressStream.getCurrentPosition();
  }

  @Override
  public long max() {
    return reference.size;
  }
}
