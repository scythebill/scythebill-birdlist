/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.app;

import java.awt.Cursor;
import java.awt.event.ActionEvent;

import javax.swing.Action;
import javax.swing.JPanel;

/**
 * Interface for frames that support navigation.
 */
public interface NavigableFrame {
  /** Returns null if navigation was unsuccessful. */
  JPanel navigateTo(String target);
  /** Returns null if navigation was unsuccessful. */
  JPanel navigateToAndPush(String target, Action onCompletion);
  /** Returns true if navigation was successful. */
  boolean navigateTo(JPanel panel);
  void updateTitle();
  void complete(ActionEvent evt);
  void setCursor(Cursor cusor);
}
