/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;

/**
 * Handles the loading of files other than .bsxm files.  In particular, responsible
 * for loading .csv files - either eBird imports or Scythebill checklist files -
 * or .btxm files.
 * <p>
 * At startup, must wait until a .bsxm file is loaded.
 * After startup, should immediately hand import over to the currently visible frame.
 */
@Singleton
public class OtherFileLoaderRegistry {
  private final List<File> filesToProcess = Lists.newArrayList();
  private final List<ActionListener> listeners = Lists.newCopyOnWriteArrayList();
  
  @Inject
  OtherFileLoaderRegistry() {}
  
  public void addFiles(Collection<File> files) {
    synchronized (this) {
      filesToProcess.addAll(files);
    }
    
    for (ActionListener listener : listeners) {
      listener.actionPerformed(new ActionEvent(this, 0, ""));
    }
  }
  
  /** Get all current files, and clear the existing list. */
  public List<File> getAndClear() {
    List<File> files = Lists.newArrayList();
    synchronized (this) {
      files.addAll(filesToProcess);
      filesToProcess.clear();
    }
    return files;
  }
  
  public void addActionListener(ActionListener listener) {
    listeners.add(listener);
  }

  public void removeActionListener(ActionListener listener) {
    listeners.remove(listener);
  }
}
