/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.scythebill.birdlist.ui.app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import org.apache.commons.io.input.BOMInputStream;

import com.google.common.base.Charsets;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.gson.Gson;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.ChecklistResolution;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.checklist.ClementsChecklist;
import com.scythebill.birdlist.model.io.ProgressInputStream;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.upgrades.OneTimeUpgradeProcessor;
import com.scythebill.birdlist.model.taxa.CompletedUpgrade;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;
import com.scythebill.birdlist.model.taxa.TaxonomyMappings;
import com.scythebill.birdlist.model.taxa.names.LocalNames;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences;
import com.scythebill.birdlist.model.util.Progress;
import com.scythebill.birdlist.model.util.TaxonomyIndexer;
import com.scythebill.birdlist.model.xml.XmlReportSetImport;
import com.scythebill.birdlist.ui.events.TaxonomyPreferences;
import com.scythebill.birdlist.ui.events.TaxonomyPreferences.PerReportSet;
import com.scythebill.birdlist.ui.prefs.ReportPreferencesManager;

/**
 * Loads ReportSets, possibly in the background
 */
public class ReportSetLoader implements Callable<ReportSet>, Progress {
  private final File file;
  private final long size;
  private final Future<Taxonomy> taxonomyFuture;
  private volatile ProgressInputStream progressStream;
  private final TaxonomyMappings mappings;
  private final Future<? extends Taxonomy> iocFuture;
  private final NamesPreferences namesPreferences;
  private OneTimeUpgradeProcessor oneTimeUpgradeProcessor;
  private final Checklists checklists;

  public ReportSetLoader(
      File file,
      Future<Taxonomy> future,
      Future<? extends Taxonomy> iocFuture,
      TaxonomyMappings mappings,
      Checklists checklists,
      NamesPreferences namesPreferences,
      OneTimeUpgradeProcessor oneTimeUpgradeProcessor) {
    this.namesPreferences = namesPreferences;
    this.oneTimeUpgradeProcessor = oneTimeUpgradeProcessor;
    this.file = Preconditions.checkNotNull(file);
    this.taxonomyFuture = Preconditions.checkNotNull(future);
    this.iocFuture = Preconditions.checkNotNull(iocFuture);
    this.mappings = mappings;
    this.checklists = checklists;
    this.size = file.length();
  }

  @Override public ReportSet call() throws Exception {
    Taxonomy taxonomy = taxonomyFuture.get();
    MappedTaxonomy iocTaxonomy = (MappedTaxonomy) iocFuture.get();

    progressStream = new ProgressInputStream(new BOMInputStream(new FileInputStream(file)));

    ReportSet reportSet;    
    try (BufferedReader reader = new BufferedReader(
        new InputStreamReader(progressStream, Charsets.UTF_8))){
      XmlReportSetImport rsi = new XmlReportSetImport();
      reportSet = rsi.importReportSet(reader, taxonomy, Optional.of(iocTaxonomy), mappings);
    }
    
    loadExtendedTaxonomyIndexers(reportSet);
    
    // Run the "one-time" upgraders here, before computing
    // IOC upgrades.  These can directly mutate taxa, affecting
    // the computation.
    oneTimeUpgradeProcessor.upgradeReportSet(reportSet);
    
    if (reportSet.getCompletedUpgrade() == null) {
      computeIocUpgrade(reportSet, iocTaxonomy);
    } else {
      ImmutableSet<SightingTaxon> affectedIocSightingTaxa = affectedIocSightingTaxa(reportSet, iocTaxonomy);
      reportSet.getCompletedUpgrade().applyAdditionalSps(iocTaxonomy, affectedIocSightingTaxa);
    }
    return reportSet;
  }

  /**
   * See if the reportset needs any sort of an IOC upgrade.
   */
  private void computeIocUpgrade(ReportSet reportSet, MappedTaxonomy iocTaxonomy) throws IOException {
    ReportPreferencesManager reportPreferencesManager = new ReportPreferencesManager(reportSet, new Gson());
    TaxonomyPreferences.PerReportSet perReportSet =
        reportPreferencesManager.getPreference(TaxonomyPreferences.PerReportSet.class);
    String lastIocVersion = getLastIocVersion(perReportSet);
    if (lastIocVersion != null && !lastIocVersion.equals(iocTaxonomy.getId())) {
      // Update the last IOC version, but *only* that, and don't mark a general switch to IOC
      // over Clements/eBird.  This makes sure the user never sees another IOC upgrade notice.
      perReportSet.setLastIocVersion(iocTaxonomy);
      reportPreferencesManager.save(true);
      ImmutableSet<SightingTaxon> affectedSightingTaxa = mappings.getIocAffectedSightingTaxa(lastIocVersion);
      ImmutableSet<SightingTaxon> taxaFromReportSet = reportSet.getSightings().stream()
          .map(Sighting::getTaxon)
          .filter(affectedSightingTaxa::contains)
          .collect(ImmutableSet.toImmutableSet());
      
      updateChecklistForIocUpgrade(reportSet, affectedSightingTaxa, iocTaxonomy);
      if (!taxaFromReportSet.isEmpty()) {
        reportSet.setCompletedUpgrade(new CompletedUpgrade() {
          @Override
          public Collection<SightingTaxon> warnings() {
            return ImmutableSet.of();
          }
          
          @Override
          public Collection<SightingTaxon> sps(Taxonomy taxonomy) {
            return taxaFromReportSet;
          }
          
          @Override
          public String getPreviousTaxonomyId() {
            return lastIocVersion; 
          }

          @Override
          public String getNewTaxonomyName() {
            return iocTaxonomy.getName();
          }

          @Override
          public Taxonomy getForcedTaxonomy() {
            return iocTaxonomy;
          }

          @Override
          public void applyAdditionalSps(Taxonomy onlyForTaxonomy,
              ImmutableSet<SightingTaxon> additionalSps) {
            throw new UnsupportedOperationException();
          }
        });
      }
    }
  }

  /**
   * Forcibly update any custom checklists to simplify for the current IOC upgrade.
   * <p>
   * An actual UI would perhaps be nice, but this is far better than letting spuhs unnecessarily accumulate.
   */
  private void updateChecklistForIocUpgrade(ReportSet reportSet,
      ImmutableSet<SightingTaxon> affectedSightingTaxa, MappedTaxonomy iocTaxonomy) {
    ChecklistResolution checklistResolution = new ChecklistResolution(checklists, ImmutableSet.of());
    for (Location checklistLocation : reportSet.checklists().keySet()) {
      // See if there's a built-in checklist to attempt resolution
      Checklist builtInChecklist = checklists.getNearestBuiltInChecklist(
          iocTaxonomy, reportSet, checklistLocation);
      if (builtInChecklist == null) {
        continue;
      }

      Checklist customChecklist = reportSet.getChecklist(checklistLocation);
      ImmutableSet<SightingTaxon> affectedTaxaFromChecklist = customChecklist.getTaxa(iocTaxonomy.getBaseTaxonomy())
          .stream()
          .filter(affectedSightingTaxa::contains)
          .collect(ImmutableSet.toImmutableSet());
      // If there's any relevant taxa for this upgrade...
      if (!affectedTaxaFromChecklist.isEmpty()) {
        // Grab the old state of the checklist
        Map<SightingTaxon, Checklist.Status> checklistMap = new LinkedHashMap<>();
        for (SightingTaxon taxon : customChecklist.getTaxa(iocTaxonomy.getBaseTaxonomy())) {
          checklistMap.put(taxon, customChecklist.getStatus(iocTaxonomy.getBaseTaxonomy(), taxon));
        }
        
        for (SightingTaxon affectedTaxon : affectedTaxaFromChecklist) {
          // For each affected taxon, try to improve things
          SightingTaxon maybeImproved = checklistResolution.resolveAgainstChecklist(iocTaxonomy, /*iocResolution=*/true, affectedTaxon, builtInChecklist);
          if (maybeImproved != null) {
            // Verify it really *is* improved
            Resolved oldResolved = affectedTaxon.resolve(iocTaxonomy);
            Resolved newResolved = maybeImproved.resolve(iocTaxonomy);
            if (newResolved.getTaxa().size() < oldResolved.getTaxa().size()) {
              // If so, remove the old taxon and replace with the new one
              Checklist.Status previousStatus = checklistMap.remove(affectedTaxon);
              if (previousStatus != null) {
                checklistMap.put(maybeImproved, previousStatus);
              }
            }
          }
        }
        
        reportSet.setChecklist(checklistLocation, new ClementsChecklist(checklistMap));
      }
    }
  }

  private ImmutableSet<SightingTaxon> affectedIocSightingTaxa(
      ReportSet reportSet,
      MappedTaxonomy iocTaxonomy) throws IOException {
    ReportPreferencesManager reportPreferencesManager = new ReportPreferencesManager(reportSet, new Gson());
    TaxonomyPreferences.PerReportSet perReportSet =
        reportPreferencesManager.getPreference(TaxonomyPreferences.PerReportSet.class);
    String lastIocVersion = getLastIocVersion(perReportSet);
    // Skip if the user has never viewed IOC taxonomy at all, or if last time they viewed
    // was the current IOC taxonomy.
    // "Never viewed IOC taxonomy at all" isn't great; it implies that viewing exactly *once* forever
    // locks you into seeing IOC updates.
    if (lastIocVersion == null
        || lastIocVersion.equals(iocTaxonomy.getId())) {
      return ImmutableSet.of();
    }

    // Update the last IOC version, but *only* that, and don't mark a general switch to IOC
    // over Clements/eBird.  This makes sure the user never sees another IOC upgrade notice.
    perReportSet.setLastIocVersion(iocTaxonomy);
    reportPreferencesManager.save(true);
    ImmutableSet<SightingTaxon> affectedSightingTaxa = mappings.getIocAffectedSightingTaxa(lastIocVersion);
    return reportSet.getSightings().stream()
        .map(Sighting::getTaxon)
        .filter(affectedSightingTaxa::contains)
        .collect(ImmutableSet.toImmutableSet());
  }
  
  private String getLastIocVersion(PerReportSet perReportSet) {
    if (perReportSet != null) {
      return perReportSet.lastIocVersion();
    }
    
    return null;
  }

  private void loadExtendedTaxonomyIndexers(ReportSet reportSet) {
    // TODO: make this usefully threaded?
    for (Taxonomy taxonomy : reportSet.extendedTaxonomies()) {
      new TaxonomyIndexer(Futures.immediateFuture(taxonomy)).load(MoreExecutors.newDirectExecutorService());
      ((TaxonomyImpl) taxonomy).setLocalNames(LocalNames.trival(namesPreferences));
    }
  }

  @Override public long current() {
    return (progressStream == null) ? 0 : progressStream.getCurrentPosition();
  }

  @Override public long max() {
    return size;
  }  
}
