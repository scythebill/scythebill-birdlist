/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.app;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.FileTime;
import java.util.logging.Logger;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.AndDirty;
import com.scythebill.birdlist.model.util.Dirty;
import com.scythebill.birdlist.model.util.DirtyProxy;
import com.scythebill.birdlist.model.xml.XmlReportSetExport;
import com.scythebill.birdlist.ui.guice.Clements;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Class that handles the lifecycle of saving a ReportSet.
 */
public class ReportSetSaver {
  private static final Logger logger = Logger.getLogger(ReportSetSaver.class.getName());

  /**
   * Use 5 seconds of leeway before reporting a problem, to deal with
   * oddities in Samba.  
   */
  private static final long MODIFICATION_LEEWAY_MILLIS = 5000;
  private final String filename;
  private final ReportSet reportSet;
  private final Supplier<Taxonomy> taxonomySupplier;
  private long lastModifiedMillis;
  private final DirtyProxy editorDirtyProxy;
  private final AndDirty dirty;
  private Editor editor;

  /** Warning that a file was saved, but things ... went poorly. */
  class UnsafeSaveException extends IOException {
    UnsafeSaveException(IOException e) {
      super("Saved " + filename + ", but unsafely.", e);
    }
  }

  public class WindowsPermissionException extends IOException {
    private final File file;

    WindowsPermissionException (File file, IOException e) {
      super(e);
      this.file = file;
    }
    
    public File getFile() {
      return file;
    }
  }
  
  /**
   * Interface attached by a part of the UI that registers temporary edits, and thefore:
   * <ul>
   * <li>Needs to be able to register that the overall application state is "dirty" (to get a warning without losing results on Quit).
   * <li>Needs to get a chance to commit temporary edits when a user saves.
   * <li>Needs to be told a user is navigating to another part of the app, to have a chance to block navigation or commit temporary results. 
   * </ul>
   */
  public interface Editor {
    /** The Dirty status of this editor. */
    Dirty getDirty();

    /** Called to commit results prior to saving. */
    void beforeSave();

    /**
     * Called only if this Editor is dirty, before the user navigates to another page.
     * @return true if navigation can proceed, false if it should be blocked.
     */
    boolean beforeNavigation();
  }
  
  @Inject
  public ReportSetSaver(ReportSet reportSet,
      @Clements Supplier<Taxonomy> taxonomySupplier,
      @Named("com.adamwiner.birdlist.ui.reportFile") String filename) {
    this.reportSet = reportSet;
    this.taxonomySupplier = taxonomySupplier;
    this.filename = filename;
    this.lastModifiedMillis = new File(filename).lastModified();
    this.editorDirtyProxy = new DirtyProxy();
    this.dirty = new AndDirty(reportSet.getDirty(), editorDirtyProxy);
  }
  
  public ReportSet save() throws IOException {
    if (editor != null) {
      editor.beforeSave();
    }
    
    try {
      // Write to a temp file
      File tmpOutFile = tmpFile();
      // Create that temporary new file.  This may trigger an IOException, in which case we'll trigger the fallback path.
      if (!tmpOutFile.createNewFile()) {
        // File already existed - not a big deal
        logger.info("Temporary file already existed:" + tmpOutFile);
      }
      writeFile(tmpOutFile);
      
      File originalFile = new File(filename);
      copyCreationTimestamp(originalFile, tmpOutFile);
      File temp2 = tmp2File();
      if (temp2.exists()) {
        if (!temp2.delete()) {
          // Things may be going poorly... but it's that final "renameTo" which is a critical problem.
          logger.warning("Couldn't delete temporary file - swap may fail");
        }
      }
      
      // Now, do a "safe" swap of the two files
      if (!originalFile.renameTo(temp2)) {
        throw new IOException("Could not rename the original file to " + temp2.getName());
      }
      if (!tmpOutFile.renameTo(originalFile)) {
        throw new IOException("Could not rename the temporary swap file to " + originalFile.getName());
      }
      // Reportset is saved to the correct location, time to clearDirty()
      reportSet.clearDirty();
      if (!temp2.delete()) {        
        // Not a big deal... other than wasted disk space
        logger.info("Delete failed");
      }
      updateLastModifiedTimestampToNow();
    } catch (IOException e) {
      try {
        // Give up, and write the file directly to the requested location.
        writeFile(new File(filename));
        // ReportSet is saved to the correct location, time to clearDirty()
        reportSet.clearDirty();
        updateLastModifiedTimestampToNow();
      } catch (IOException finalException) {
        // And if that fails, log *both* exceptions
        finalException.initCause(e);
        // Windows reports FileNotFoundException when you try to write to
        // a file where you don't have permission to write. Report it as
        // such to callers so they can give a much better error message.
        // Have not actually verified this code works, though!
        if (UIUtils.isWindows()
            && finalException instanceof FileNotFoundException
            && finalException.getMessage() != null
            && finalException.getMessage().contains("Access is denied")) {
          throw new WindowsPermissionException(new File(filename), finalException);
        }
        throw finalException;
      }

      // The UnsafeSaveException idea triggers more support cost and user worry
      // then it seems to help.  Comment out.
      //   throw new UnsafeSaveException(e);
      return reportSet;
    } finally {
      // Whatever happens, try to clean up and delete the temporary files
      tmpFile().delete();
      tmp2File().delete();
    }
    
    updateLastModifiedTimestampToNow();
    
    return reportSet;
  }

  public Dirty getDirty() {
    return dirty;
  }

  public void attachEditor(Editor editor) {
    Preconditions.checkState(this.editor == null, "Editor already attached, not released");
    this.editor = Preconditions.checkNotNull(editor);
    editorDirtyProxy.setProxiedDirty(editor.getDirty());
  }
  
  public void releaseEditor() {
    Preconditions.checkState(this.editor != null, "Editor not attached");
    this.editor = null;
    editorDirtyProxy.setProxiedDirty(null);
  }
  
  public File file() {
    return new File(filename);
  }
  
  /**
   * Called before the user navigates to another page in this reportset.
   * @return true if navigation can proceed, false if it should be blocked.
   */
  public boolean beforeNavigation() {
    if (editor == null) {
      return true;
    }
    
    return !editor.getDirty().isDirty() || editor.beforeNavigation();
  }
  
  
  public boolean hasBeenModifiedElsewhere() {
    long newLastModifiedMillis = new File(filename).lastModified();
    if (newLastModifiedMillis > lastModifiedMillis + MODIFICATION_LEEWAY_MILLIS) {
      // Update the local last-modified value (to honor the interface
      // contract) and return true.
      lastModifiedMillis = newLastModifiedMillis;
      return true;
    }
    
    return false;
  }

  private void updateLastModifiedTimestampToNow() {
    // Update the last modified timestamp to the *max* of the
    // current timestamp and what the filesystem reports. There are
    // some user reports that Samba might be slow at reporting
    // the file system last-modified has changed, and mixing in the
    // current timestamp seems safe.
    lastModifiedMillis =
        Math.max(
            System.currentTimeMillis(),
            new File(filename).lastModified());
  }

  /**
   * Attempt to preserve creation timestamps.
   * 
   * This may or may not work on any one OS, but no harm trying.
   */
  private void copyCreationTimestamp(File from, File to) {
    try {
      BasicFileAttributeView fromAttributes =
          Files.getFileAttributeView(Paths.get(from.getAbsolutePath()), BasicFileAttributeView.class);
      FileTime fromCreationTime = fromAttributes.readAttributes().creationTime();
      
      BasicFileAttributeView toAttributes =
          Files.getFileAttributeView(Paths.get(to.getAbsolutePath()), BasicFileAttributeView.class);
      toAttributes.setTimes(null, null, fromCreationTime);
    } catch (IOException e) {
      logger.info("Can't clone creation times: " + e.getMessage());
    }
  }

  private File tmpFile() {
    return new File(filename + ".tmp");
  }

  private File tmp2File() {
    return new File(filename + ".tmp2");
  }

  private void writeFile(File tmpOutFile) throws IOException {
    XmlReportSetExport rse = new XmlReportSetExport();
    FileOutputStream output = new FileOutputStream(tmpOutFile);
    Writer writer = new BufferedWriter(new OutputStreamWriter(output, Charsets.UTF_8));
    try {
      rse.export(writer, Charsets.UTF_8.name(), reportSet,
          Preconditions.checkNotNull(taxonomySupplier.get()));
    } finally {
      writer.close();
    }
  }
}