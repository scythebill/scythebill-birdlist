/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.app;

import java.awt.EventQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.swing.SwingUtilities;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListenableScheduledFuture;
import com.google.common.util.concurrent.ListeningScheduledExecutorService;
import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Module;
import com.google.inject.Singleton;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;
import com.scythebill.birdlist.model.taxa.names.LocalNames;
import com.scythebill.birdlist.model.taxa.names.NamesLoader;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences.LocaleOption;
import com.scythebill.birdlist.model.util.Progress;
import com.scythebill.birdlist.model.util.TaxonomyIndexer;
import com.scythebill.birdlist.ui.guice.Clements;
import com.scythebill.birdlist.ui.guice.IOC;
import com.scythebill.birdlist.ui.panels.VersionUpgradeNotice;
import com.scythebill.birdlist.ui.uptodate.UpToDateLoader;
import com.scythebill.birdlist.ui.uptodate.UpToDateLoader.VersionInfo;

/**
 * Intializes global state in parallel at startup.
 */
@Singleton
public class Startup {
  private final TaxonomyLoader taxonomyLoader;
  private final ListeningScheduledExecutorService executorService;
  private Future<Taxonomy> taxonomyFuture;
  private Future<MappedTaxonomy> iocFuture;
  private Future<?> clementsIndices;
  private Future<?> iocIndices;
  private final StartupView startupView;
  private volatile boolean isStarting;
  private final UpToDateLoader upToDate;
  private final VersionUpgradeNotice upgradeNotice;
  private final TaxonomyReference iocRef;
  private final NamesLoader iocNamesLoader;
  private MappedTaxonomyLoader iocLoader;
  private NamesPreferences namesPreferences;

  @Inject
  public Startup(
      @Clements TaxonomyReference clementsRef,
      final @Clements NamesLoader clementsNamesLoader,
      @IOC TaxonomyReference iocRef,
      @IOC NamesLoader iocNamesLoader,
      NamesPreferences namesPreferences,
      ListeningScheduledExecutorService executorService,
      StartupView startupView,
      UpToDateLoader upToDate,
      VersionUpgradeNotice upgradeNotice) {
    this.namesPreferences = namesPreferences;
    this.taxonomyLoader = new TaxonomyLoader(clementsRef) {

      @Override
      public Taxonomy call() throws Exception {
        Taxonomy taxonomy = super.call();
        ((TaxonomyImpl) taxonomy).setLocalNames(
            new LocalNames(clementsNamesLoader, Startup.this.namesPreferences, LocaleOption.CLEMENTS));
        return taxonomy;
      }
      
    };

    this.iocRef = iocRef;
    this.iocNamesLoader = iocNamesLoader;
    this.executorService = executorService;
    this.startupView = startupView;
    this.upToDate = upToDate;
    this.upgradeNotice = upgradeNotice;
  }
  
  /**
   * Init code for immediate execution (not necessarily the event queue).
   */
  public synchronized void init() {
    taxonomyFuture = executorService.submit(taxonomyLoader);
    this.iocLoader = new MappedTaxonomyLoader(iocRef, taxonomyFuture) {
      @Override
      public MappedTaxonomy call() throws Exception {
        MappedTaxonomy loaded = super.call();
        // Attach a localized names loader 
        loaded.setLocalNames(new LocalNames(iocNamesLoader, namesPreferences, LocaleOption.IOC));
        return loaded;
      }
    };
    iocFuture = executorService.submit(iocLoader);
   
    Runnable checkUpToDate = new Runnable() {
      @Override public void run() {
        ListenableFuture<ImmutableList<VersionInfo>> upToDateFuture = upToDate.load();
        Futures.addCallback(upToDateFuture, new FutureCallback<ImmutableList<VersionInfo>>() {
          @Override public void onFailure(Throwable t) {
            throw new RuntimeException(t);
          }

          @Override public void onSuccess(final ImmutableList<VersionInfo> versionInfo) {
            // If there's a new version, tell the user about it
            if (!versionInfo.isEmpty()) {
              SwingUtilities.invokeLater(() -> upgradeNotice.showUpgrade(versionInfo));
            }
          }
        }, executorService);
      }
    };
    
    // Immediately at startup (and thereafter once a day) check if upgrade is needed.
    @SuppressWarnings("unused")
    ListenableScheduledFuture<?> unused = executorService.scheduleAtFixedRate(checkUpToDate, 0, 1, TimeUnit.DAYS);    
  }
  
  /**
   * Start loading globals.
   * @param onTaxonomyLoaded a Runnable invoked when the taxonomy is ready.  Will be
   *     invoked on the event queue thread.
   */
  public synchronized void start(final Runnable onTaxonomyLoaded) {
    TaxonomyIndexer clementsIndexer = new TaxonomyIndexer(taxonomyFuture);
    clementsIndices = clementsIndexer.load(executorService);

    TaxonomyIndexer iocIndexer = new TaxonomyIndexer(iocFuture);
    iocIndices = iocIndexer.load(executorService);
    
    startupView.addProgress(taxonomyLoader, 0.3);
    startupView.addProgress(iocLoader, 0.3);
    startupView.addProgress(clementsIndexer.getCommonIndexLoader(), 0.1);
    startupView.addProgress(clementsIndexer.getScientificIndexLoader(), 0.1);
    startupView.addProgress(iocIndexer.getCommonIndexLoader(), 0.1);
    startupView.addProgress(iocIndexer.getScientificIndexLoader(), 0.1);
    startupView.start();
    
    @SuppressWarnings("unused")
    ListenableFuture<Void> unused = executorService.submit(new Callable<Void>() {
      @Override public Void call() throws Exception {
        // Make sure all data has been loaded
        taxonomyFuture.get();
        clementsIndices.get();
        iocIndices.get();
        EventQueue.invokeLater(onTaxonomyLoaded);
        return null;
      }
    });
    
    isStarting = true;
  }
  
  public void addProgress(Progress progress, double fraction) {
    startupView.addProgress(progress, fraction);
  }
  
  public void finish() {
    if (isStarting) {
      startupView.stop();
      isStarting = false;
    }
  }
  
  public boolean isStarting() {
    return isStarting;
  }
  
  /** Return a module with all the objects loaded at startup */
  public Module constructedObjectsModule() {
    return new AbstractModule() {
      @Override protected void configure() {
        try {
          bind(Taxonomy.class).annotatedWith(Clements.class).toInstance(taxonomyFuture.get());
          bind(MappedTaxonomy.class).annotatedWith(IOC.class).toInstance(iocFuture.get());
        } catch (ExecutionException e) {
          throw new RuntimeException(e);
        } catch (InterruptedException e) {
          throw new RuntimeException(e);
        }
      }
    };
  }

  public synchronized Future<Taxonomy> getTaxonomyFuture() {
    return taxonomyFuture;
  }
  
  public synchronized Future<? extends Taxonomy> getIocTaxonomyFuture() {
    return iocFuture;
  }

  public synchronized Taxonomy getTaxonomy() {
    Preconditions.checkState(taxonomyFuture.isDone());
    try {
      return taxonomyFuture.get();
    } catch (ExecutionException e) {
      throw new RuntimeException(e);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

}
