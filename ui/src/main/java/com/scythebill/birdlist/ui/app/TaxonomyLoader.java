/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.app;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.concurrent.Callable;

import com.scythebill.birdlist.model.io.ProgressInputStream;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.Progress;
import com.scythebill.birdlist.model.xml.XmlTaxonImport;

/**
 * Loader class for importing a taxonomy file and identifying its progress.
 */
public class TaxonomyLoader implements Callable<Taxonomy>, Progress {
  private volatile ProgressInputStream progressStream;
  private final TaxonomyReference reference;

  public TaxonomyLoader(TaxonomyReference reference) {
    this.reference = reference;
  }

  @Override
  public Taxonomy call() throws Exception {
    progressStream = new ProgressInputStream(reference.streamProvider.openStream());
    BufferedReader reader = new BufferedReader(new InputStreamReader(
        progressStream, Charset.forName("UTF-8")));
    try {
      return (new XmlTaxonImport()).importTaxa(reader);
    } finally {
      reader.close();
    }
  }

  @Override
  public long current() {
    if (progressStream == null) {
      return 0L;
    }
    return progressStream.getCurrentPosition();
  }

  @Override
  public long max() {
    return reference.size;
  }
}
