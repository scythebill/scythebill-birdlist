/*   
 * Copyright 2012 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.app;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import com.google.common.io.ByteSource;
import com.google.common.io.Resources;

/**
 * Reference to a taxonomy for loading.
 */
public class TaxonomyReference {
  final ByteSource streamProvider;
  final long size;

  public TaxonomyReference(URL url) {
    this(Resources.asByteSource(url), getLength(url));
  }

  public TaxonomyReference(
          ByteSource byteSource, long length) {
    this.streamProvider = byteSource;
    this.size = length;
  }

  private static long getLength(URL url) {
    URLConnection openConnection = null;
    try {
      openConnection = url.openConnection();
      return openConnection.getContentLength();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}