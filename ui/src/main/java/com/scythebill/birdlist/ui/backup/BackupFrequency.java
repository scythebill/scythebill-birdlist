/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.backup;

import org.joda.time.Duration;

import com.scythebill.birdlist.ui.messages.Messages.Name;

public enum BackupFrequency {
  NEVER(null, Name.BACKUP_NEVER),
  // Testing only!
  OBSESSIVELY(Duration.standardSeconds(10), Name.BACKUP_OBSESSIVELY),
  WEEKLY(Duration.standardDays(7), Name.BACKUP_WEEKLY),
  MONTHLY(Duration.standardDays(30), Name.BACKUP_MONTHLY);

  private final Duration duration;
  private final Name text;
  private BackupFrequency(Duration duration, Name text) {
    this.duration = duration;
    this.text = text;
  }
  
  public Duration duration() {
    return duration;
  }
  
  @Override
  public String toString() {
    return text.toString();
  }
}
