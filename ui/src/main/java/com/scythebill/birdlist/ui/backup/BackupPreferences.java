/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.backup;

import com.scythebill.birdlist.model.annotations.Preference;

/**
 * Preferences for handling backups.
 */
public class BackupPreferences {
  @Preference public volatile BackupFrequency frequency = BackupFrequency.NEVER;
  @Preference public volatile String backupDirectory;
  
  /** The next time to beg a user to enable backups. */
  @Preference volatile long nextTimeToBeg = 0;
}
