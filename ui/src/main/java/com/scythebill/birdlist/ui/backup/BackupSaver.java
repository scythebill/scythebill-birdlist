/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.backup;

import java.awt.Component;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.swing.SwingUtilities;

import org.joda.time.Duration;
import org.joda.time.Instant;
import org.joda.time.LocalDate;
import org.joda.time.chrono.GJChronology;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.html.HtmlEscapers;
import com.google.common.util.concurrent.ListenableScheduledFuture;
import com.google.common.util.concurrent.ListeningScheduledExecutorService;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.xml.XmlReportSetExport;
import com.scythebill.birdlist.model.xml.XmlReportSetImport;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.prefs.PreferencesManager;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Saves backups for report-set files.
 */
@Singleton
public class BackupSaver {
  /** Check (and therefore nag) at most once every 5 days. */
  private final ListeningScheduledExecutorService executorService;
  private final BackupPreferences backupPreferences;
  private final CopyOnWriteArrayList<BackupEntry> entries = new CopyOnWriteArrayList<BackupEntry>();
  private final Alerts alerts;
  private final FileDialogs fileDialogs;
  private Instant lastWarnedOfBackupError;
  private Instant lastWarnedOfInvalidBackupDirectory;
  private static final int ERROR_COUNT_TO_START_WARNING = 2;

  @Inject
  BackupSaver(
      PreferencesManager preferencesManager,
      ListeningScheduledExecutorService executorService,
      Alerts alerts,
      FileDialogs fileDialogs) {
    this.alerts = alerts;
    this.fileDialogs = fileDialogs;
    backupPreferences = preferencesManager.getPreference(BackupPreferences.class);
    this.executorService = executorService;
  }
  
  /**
   * Starts backing up a file.  Returns a {@link Closeable} that can terminate the backup.
   */
  public Closeable initiateBackup(
      ReportSet reportSet, File file) {    
    final BackupEntry backupEntry = new BackupEntry(reportSet, file);
    
    BackupResult backupResult = backupEntry.maybeWriteNow(false /* don't force */);
    backupEntry.maybeSchedule(backupResult);
    
    entries.add(backupEntry);
    
    return () -> {
      backupEntry.close();
      entries.remove(backupEntry);
    };
  }
  
  
  /**
   * Saves a backup file immediately.
   */
  public void saveNow(
      ReportSet reportSet, File file) {    
    BackupEntry backupEntry = new BackupEntry(reportSet, file);
    backupEntry.maybeWriteNow(true /* force */);
  }
  
  /**
   * Forces a backup of all entries (if needed).
   */
  public void maybeSaveAll() {
    for (BackupEntry entry : entries) {
      BackupResult result = entry.maybeWriteNow(false /* don't force. */);
      entry.maybeSchedule(result);
    }
  }
  
  /**
   * Forces a backup of all entries, whether or not needed!
   */
  public void definitelySaveAll() {
    for (BackupEntry entry : entries) {
      BackupResult result = entry.maybeWriteNow(true);
      entry.maybeSchedule(result);
    }
  }

  enum BackupResult {
    NOT_NEEDED,
    ERROR,
    NEEDED
  }
  
  class BackupEntry {
    private final ReportSet reportSet;
    private final String fileName;
    private final File file;
    private volatile ListenableScheduledFuture<?> scheduledTask;
    private volatile boolean closed;
    private volatile int errorCount;

    BackupEntry(
        ReportSet reportSet,
        File file) {
      this.reportSet = reportSet;
      this.file = file;
      String fileName = file.getName();
      if (fileName.endsWith(XmlReportSetImport.REPORT_SET_SUFFIX)) {
        fileName = fileName.substring(0, fileName.length() - XmlReportSetImport.REPORT_SET_SUFFIX.length());
      }
      this.fileName = fileName;
    }
    
    void close() {
      closed = true;
      ListenableScheduledFuture<?> scheduledTask = this.scheduledTask;
      if (scheduledTask != null) {
        scheduledTask.cancel(false);
      }
    }

    private void write(boolean forceAWrite) throws IOException {
      if (backupPreferences.backupDirectory != null) {
        File directory = new File(backupPreferences.backupDirectory);
        if (directory.exists() && directory.isDirectory()) {
          // Scan until we find a filename that is free if necessary
          LocalDate localDate = new LocalDate(GJChronology.getInstance());
          File backupFile = null;
          for (int i = 0; i < 100; i++) {
            File possibleBackupFile = backupFileName(directory, localDate, i);
            if (!possibleBackupFile.exists()) {
              backupFile = possibleBackupFile;
              break;
            }
            
            // If not forcing a write, don't scan past the first instance
            if (!forceAWrite) {
              return;
            }
          }
          
          // Assuming an option was found, write a backup
          if (backupFile != null) {
            FileOutputStream backupOut = new FileOutputStream(backupFile);
            ZipOutputStream zipOut = new ZipOutputStream(
                new BufferedOutputStream(backupOut));
            // Change the name of the backup file to avoid confusing users that open the .bsxm in the zip
            ZipEntry entry = new ZipEntry(fileName + "-backup" + XmlReportSetImport.REPORT_SET_SUFFIX);
            zipOut.putNextEntry(entry);
            XmlReportSetExport export = new XmlReportSetExport();
            Writer out = null;
            try {
              out = new BufferedWriter(
                  new OutputStreamWriter(
                      zipOut,
                      Charsets.UTF_8));
              export.export(out, "UTF-8", reportSet, reportSet.getTaxonomy());
              out.flush();
              zipOut.closeEntry();
            } finally {
              if (out != null) {
                zipOut.close();
              }
            }
          }
        }
      }
    }

    BackupResult maybeWriteNow(boolean forceAWrite) {
      if (closed) {
        return BackupResult.NOT_NEEDED;
      }
      if (!forceAWrite && reportSet.getSightings().isEmpty()) {
        return BackupResult.NOT_NEEDED;
      }
      BackupResult needsBackup = forceAWrite ? BackupResult.NEEDED : needsBackup();
      if (needsBackup == BackupResult.NEEDED) {
        try {
          write(forceAWrite);
          errorCount = 0;
        } catch (IOException e) {
          if (((++errorCount) > ERROR_COUNT_TO_START_WARNING) || forceAWrite) {
            SwingUtilities.invokeLater(new Runnable() { 
              @Override
              public void run() {
                if (lastWarnedOfBackupError == null
                    || lastWarnedOfBackupError.plus(Duration.standardDays(1)).isBeforeNow()) {
                  lastWarnedOfBackupError = new Instant();
                  alerts.showError(null, Name.ERROR_BACKING_UP_TITLE,
                      Name.ERROR_BACKING_UP_FORMAT,
                      HtmlEscapers.htmlEscaper().escape(file.getName()));
                }
              }
            });
          }
          needsBackup = BackupResult.ERROR;
        }
      }
      return needsBackup;
    }
    
    private BackupResult needsBackup() {
      if (backupPreferences.frequency == BackupFrequency.NEVER) {
        return BackupResult.NOT_NEEDED;
      }
      
      if (backupPreferences.backupDirectory == null) {
        return BackupResult.NOT_NEEDED;
      }
      
      final File directory = new File(backupPreferences.backupDirectory);
      if (!directory.exists() || !directory.isDirectory()) {
        if ((++errorCount) > ERROR_COUNT_TO_START_WARNING) {
          SwingUtilities.invokeLater(new Runnable() { 
            @Override
            public void run() {
              if (lastWarnedOfInvalidBackupDirectory == null
                  || lastWarnedOfInvalidBackupDirectory.plus(Duration.standardDays(1)).isBeforeNow()) {
                lastWarnedOfInvalidBackupDirectory = new Instant();
                alerts.showError(null, Name.ERROR_BACKING_UP_TITLE,
                    Name.BACKUP_DIRECTORY_NOT_FOUND_FORMAT,
                    HtmlEscapers.htmlEscaper().escape(directory.getName()));
              }
            }
          });
        }
        return BackupResult.ERROR;
      }
      
      // Bail for any file that is (A) in the backup directory and (B) is named "-backup-"!
      // We don't want to backup the backups...
      if (file.getParentFile().getAbsolutePath().equals(backupPreferences.backupDirectory)
          && file.getName().contains("-backup-")) {
        return BackupResult.NOT_NEEDED;
      }

      // Scan backwards to find out if a new backup is needed.
      LocalDate today = new LocalDate(GJChronology.getInstance());      
      LocalDate date = today;
      if (backupPreferences.frequency == BackupFrequency.OBSESSIVELY) {
        return BackupResult.NEEDED;
      } else if (backupPreferences.frequency == BackupFrequency.WEEKLY) {
        for (int i = 0; i < 7; i++) {
          File backupFile = backupFileName(directory, date, 0);
          if (backupFile.exists()) {
            return BackupResult.NOT_NEEDED;
          }
          date = date.minusDays(1);
        }
        return BackupResult.NEEDED;
      } else {
        Preconditions.checkState(backupPreferences.frequency == BackupFrequency.MONTHLY);
        int todaysMonth = today.getMonthOfYear();
        while (date.getMonthOfYear() == todaysMonth) {
          File backupFile = backupFileName(directory, date, 0);
          if (backupFile.exists()) {
            return BackupResult.NOT_NEEDED;
          }
          date = date.minusDays(1);
        }
        return BackupResult.NEEDED;
      }
    }

    void maybeSchedule(BackupResult backupResult) {
      if (closed) {
        return;
      }
      
      if (backupPreferences.frequency == BackupFrequency.NEVER) {
        return;
      }
      
      Duration duration;
      if (backupPreferences.frequency == BackupFrequency.OBSESSIVELY) {
        duration = BackupFrequency.OBSESSIVELY.duration();
      } else {
        // Try again in 10 minutes if it failed the first time, then backing off (but to a max of 80 minutes)
        // (And we'll only show the error once a day).
        duration = backupResult == BackupResult.ERROR
            ? Duration.standardMinutes((long) (5 * Math.pow(2.0, Math.min(errorCount, 4))))
            : Duration.standardDays(1);
      }
      
      if (scheduledTask != null
          && !scheduledTask.isDone()) {
        scheduledTask.cancel(false);
      }
      
      scheduledTask = executorService.schedule(new Runnable() {
        @Override
        public void run() {
          scheduledTask = null;
          BackupResult result = maybeWriteNow(false /* don't force */);
          maybeSchedule(result);
        }
      }, duration.getMillis(), TimeUnit.MILLISECONDS);
    }
    
    private File backupFileName(File directory, LocalDate localDate, int attemptIndex) {
      String date = PartialIO.toString(localDate);
      String backupFileName = attemptIndex == 0
          ? String.format("%s-backup-%s.zip", fileName, date)
          : String.format("%s-backup-%s-%d.zip", fileName, date, attemptIndex);
      File backupFile = new File(directory, backupFileName);
      return backupFile;
    }
  }

  public boolean backupsAreEnabled() {
    if (backupPreferences.frequency == BackupFrequency.NEVER) {
      return false;
    }
    
    if (backupPreferences.backupDirectory == null) {
      return false;
    }
    
    File directory = new File(backupPreferences.backupDirectory);
    return directory.exists() && directory.isDirectory();
  }
  
  public boolean chooseBackupDirectory(Component component, File reportSetFile) {
    File current = null;
    if (backupPreferences.backupDirectory != null) {
      current = new File(backupPreferences.backupDirectory);
      if (!current.exists()) {
        current = null;
      } else if (!current.isDirectory()) {
        current = current.getParentFile();
      }
    }
    
    while (true) {
      File file = fileDialogs.openDirectory(
          UIUtils.findFrame(component), Messages.getMessage(Name.CHOOSE_A_BACKUP_DIRECTORY), current, FileType.BACKUPS);
      if (file == null) {
        return false;
      }
      
      if (!file.isDirectory()) {
        file = file.getParentFile();
        if (file == null) {
          return false;        
        }
      }
  
      try {
        File tempFile = File.createTempFile("scythebill", "tmp", file);
        tempFile.delete();
      } catch (IOException e) {
        alerts.showError(component,
            Name.COULD_NOT_WRITE_INTO_DIRECTORY_TITLE,
            Name.COULD_NOT_WRITE_INTO_DIRECTORY_FORMAT,
            HtmlEscapers.htmlEscaper().escape(file.getName()));
        continue;
      }

      backupPreferences.backupDirectory = file.getAbsolutePath();
      if (backupPreferences.backupDirectory.equals(reportSetFile.getParentFile().getAbsolutePath())) {
        alerts.showMessage(component,
            Name.PLEASE_RECONSIDER,
            Name.SAME_FOLDER_IS_NOT_SAFE);
      }
      
      if (backupPreferences.frequency == BackupFrequency.NEVER) {
        backupPreferences.frequency = BackupFrequency.MONTHLY;
      }
      return true;
    }
  }
}
