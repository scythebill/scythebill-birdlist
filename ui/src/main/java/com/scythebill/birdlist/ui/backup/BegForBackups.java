/*   
 * Copyright 2020 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.backup;

import java.io.File;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JOptionPane;

import org.joda.time.Duration;
import org.joda.time.Instant;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.prefs.PreferencesManager;
import com.scythebill.birdlist.ui.util.Alerts;

/** Code to beg a user to enable backups. */ 
public class BegForBackups {

  /** At 1000 sightings, beg users to enable backups. */
  private static final int MIN_SIZE_TO_BEG = 1000;
  
  private final BackupPreferences backupPreferences;
  private final BackupSaver backupSaver;
  private final Alerts alerts;

  private final static AtomicBoolean alreadyBegged = new AtomicBoolean(false);
  @Inject
  public BegForBackups(
      PreferencesManager preferencesManager,
      BackupSaver backupSaver,
      Alerts alerts) {
    this.backupPreferences = preferencesManager.getPreference(BackupPreferences.class);
    this.backupSaver = backupSaver;
    this.alerts = alerts;
  }
  
  public void check(ReportSet reportSet, File file) {
    // Backups are enabled, woo-hoo!
    if (backupPreferences.frequency != BackupFrequency.NEVER &&
        backupPreferences.backupDirectory != null) {
      return;
    }
    
    // Don't beg for small files
    if (reportSet.getSightings().size() < MIN_SIZE_TO_BEG) {
      return;
    }
    
    // Don't beg if the user doesn't want us to 
    Instant nextTimeToBeg = new Instant(backupPreferences.nextTimeToBeg);
    if (nextTimeToBeg.isAfterNow()) {
      return;
    }
    
    // Already begged once in this process
    if (alreadyBegged.getAndSet(true)) {
      return;
    }
    
    int option = alerts.showWithOptions(null, 
        Name.BACKUP_BEG_TITLE, Name.BACKUP_BEG_MESSAGE,
        Name.OK_BUTTON, Name.ASK_ME_LATER, Name.DONT_ASK_AGAIN);
    if (option == JOptionPane.CLOSED_OPTION || option == 1) {
      // Beg again in a week
      nextTimeToBeg = new Instant().plus(Duration.standardDays(7));
      backupPreferences.nextTimeToBeg = nextTimeToBeg.getMillis();
    } else if (option == 2) {
      backupPreferences.nextTimeToBeg = Long.MAX_VALUE;
    } else {
      if (backupSaver.chooseBackupDirectory(null, file)) {
        backupSaver.maybeSaveAll();
      }
    }
  }
}
