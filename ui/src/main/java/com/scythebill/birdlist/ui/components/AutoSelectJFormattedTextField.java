/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import java.awt.event.FocusEvent;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.swing.JFormattedTextField;
import javax.swing.SwingUtilities;

/**
 * Formatted text field that selects all when focusing in.
 */
public class AutoSelectJFormattedTextField extends JFormattedTextField {
  public AutoSelectJFormattedTextField() {
  }
  
  public AutoSelectJFormattedTextField(AbstractFormatter formatter) {
    super(formatter);
  }

  @Override
  protected void processFocusEvent(FocusEvent event) {
    try {
      // Introspection!  It'll either be a CausedFocusEvent in JDK 8 or earlier,
      // or the legit getCause() method in JDK 9.
      Method method = event.getClass().getMethod("getCause");
      Object result = method.invoke(event);
      if (result instanceof Enum) {
        if (((Enum<?>) result).name().contains("TRAVERSAL")) {
          SwingUtilities.invokeLater(this::selectAll);
        }
      }
    } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
      // Ah well... fail.
    }

    super.processFocusEvent(event);
  }
}
