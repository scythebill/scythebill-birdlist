/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.FlavorEvent;
import java.awt.datatransfer.FlavorListener;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.font.TextAttribute;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.TransferHandler.TransferSupport;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkEvent.EventType;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.joda.time.ReadablePartial;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.io.TimeIO;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.names.LocalNames;
import com.scythebill.birdlist.ui.actions.Attachable;
import com.scythebill.birdlist.ui.actions.ForwardingAction;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.components.SightingBulkEditPanel.UpdatedListener;
import com.scythebill.birdlist.ui.components.browser.BrowserPreviewRenderer;
import com.scythebill.birdlist.ui.components.browser.DefaultBrowserCellRenderer;
import com.scythebill.birdlist.ui.components.browser.JBrowser;
import com.scythebill.birdlist.ui.datatransfer.SightingTransferable;
import com.scythebill.birdlist.ui.datatransfer.SightingsGroup;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.ShowSpeciesMap;
import com.scythebill.birdlist.ui.panels.SpHybridDialog;
import com.scythebill.birdlist.ui.panels.SpeciesInfoDescriber;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;
import com.scythebill.birdlist.ui.util.LocationIdToString;
import com.scythebill.birdlist.ui.util.OpenMapUrl;
import com.scythebill.birdlist.ui.util.ResolvedWithSighting;
import com.scythebill.birdlist.ui.util.SightingFlags;
import com.scythebill.birdlist.ui.util.TaxonTreeModel;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Base class providing taxon (and sighting) browsing capabilities.
 * Subclasses provide the model.
 */
public abstract class BaseTaxonBrowserPanel extends JBrowser implements FontsUpdatedListener {
  protected final FontManager fontManager;
  private Location locationRoot;
  private Taxonomy taxonomy;
  private final ReportSet reportSet;
  private final SpeciesInfoDescriber speciesInfoDescriber;
  private final NewLocationDialog newLocationDialog;
  private final SpHybridDialog spHybridDialog;
  private final PredefinedLocations predefinedLocations;
  private final FileDialogs fileDialogs;
  private final Alerts alerts;
  private final OpenMapUrl openMapUrl;
  protected final NavigableFrame navigableFrame;

  private final static Logger logger = Logger.getLogger(BaseTaxonBrowserPanel.class.getName());

  public BaseTaxonBrowserPanel(
      FontManager fontManager,
      ReportSet reportSet,
      NewLocationDialog newLocationDialog,
      SpHybridDialog spHybridDialog,
      PredefinedLocations predefinedLocations,
      SpeciesInfoDescriber speciesInfoDescriber,
      OpenMapUrl openMapUrl,
      FileDialogs fileDialogs,
      Alerts alerts,
      NavigableFrame navigableFrame) {
    this.fontManager = fontManager;
    this.reportSet = reportSet;
    this.newLocationDialog = newLocationDialog;
    this.spHybridDialog = spHybridDialog;
    this.predefinedLocations = predefinedLocations;
    this.speciesInfoDescriber = speciesInfoDescriber;
    this.fileDialogs = fileDialogs;
    this.alerts = alerts;
    this.navigableFrame = navigableFrame;
    this.openMapUrl = openMapUrl;
    
    setCellRenderer(new TaxonBrowserCellRenderer());
    
    setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
    setPreviewRenderer(new TaxonPreviewRenderer());
    fontManager.applyTo(this);
  }
  
  public void setTaxonomy(Taxonomy taxonomy) {
    this.taxonomy = taxonomy;
  }
  
  public Taxonomy getTaxonomy() {
    return taxonomy;
  }

  /** Set the root location (which can be omitted in string display) */
  public void setLocationRoot(Location locationRoot) {
    this.locationRoot = locationRoot;
  }

  /** Gets the location root. */
  public Location getLocationRoot() {
    return locationRoot;
  }

  @Override
  public void doLayout() {
    // Significantly optimize layout performance by using fixed cell heights.
    // (This also avoids scrollbars resetting when things get bolded, or arrows get added, etc.
    Font font = fontManager.getTextFont();
    setFixedCellHeight(UIUtils.getFontHeight(getGraphics(), font) + 2);
    super.doLayout();
  }
  
  @Override public String convertValueToText(Object value, boolean selected,
      boolean expanded, boolean leaf, int row, boolean hasFocus) {
    if (value instanceof Taxon) {
      return convertTaxonToString((Taxon) value);
    } else if (value instanceof Resolved) {
      return convertResolvedToString((Resolved) value);
    } else if (value instanceof Sighting) {
      Sighting sighting = (Sighting) value;
      ReadablePartial date = sighting.getDateAsPartial();

      String text = "";

      if (date != null) {
        text = PartialIO.toShortUserString(date, getLocale());
      } else {
        text = Messages.getMessage(Name.NO_DATE_FOR_SIGHTING);
      }
      
      // Provide some indication if the sighting is for a "sp.".  This
      // needs to be on the resolved sighting taxon, not the original, as resolving
      // can take a Sp. to a Single or vice versa
      SightingTaxon.Type type = sighting.getTaxon().resolve(getTaxonomy()).getType();
      if (type == SightingTaxon.Type.SP) {
        text += " (sp)";
      } else if (type == SightingTaxon.Type.HYBRID) {
        text += " (hyb)";
      }
      
      text = SightingFlags.appendSightingInfo(text, sighting);

      if (sighting.getLocationId() != null) {
        String locationText = LocationIdToString.getString(
            getReportSet().getLocations(),
            sighting.getLocationId(), true, locationRoot);
        text += " " + locationText;
      } else {
        if (date == null || date.size() == 0) {
          text += " " + Messages.getMessage(Name.NO_DETAILS_FOR_SIGHTING);
        }
      }

      return text;
    } else if (value instanceof VisitInfoKey) {
      VisitInfoKey visitInfoKey = (VisitInfoKey) value;
      String text = PartialIO.toUserString(visitInfoKey.date(), getLocale());
      if (visitInfoKey.startTime().isPresent()) {
        text += ", " + TimeIO.toShortUserString(visitInfoKey.startTime().get(), getLocale());
      }
      return text;
    }
    
    return super.convertValueToText(value, selected, expanded, leaf, row, hasFocus);
  }

  protected boolean abbreviateGenus(Taxon taxon) {
    return true;
  }
  
  protected String convertTaxonToString(Taxon taxon) {
    String sciName;
    LocalNames localNames = taxon.getTaxonomy().getLocalNames();

    switch (taxon.getType()) {
      case species:
        sciName = !abbreviateGenus(taxon) ? TaxonUtils.getFullName(taxon)
            : TaxonUtils.getAbbreviatedName(taxon);
        Status status = getStatus(taxon);
        // Show anything worse-off than Vulnerable.
        // (Tried with everything beyond LC, and it was too busy)
        if (showStatus(status)) {
          return localNames.compoundName(taxon, abbreviateGenus(taxon), status);
        } else {
          return localNames.compoundName(taxon, abbreviateGenus(taxon));
        }
      case subspecies:
      case group:
        if (taxon.getCommonName() != null) {
          Status subspStatus = getStatus(taxon);
          if (showStatus(subspStatus)) {
            return localNames.compoundName(taxon, true, subspStatus);
          } else {
            return localNames.compoundName(taxon, true);
          }
        } else {
          return localNames.abbreviatedScientificName(taxon);
        }
      default:
        // Families and above:  show Common Name (Scientific Name) - unless
        // the two are the same (which they mostly are for orders)
        sciName = taxon.getName();
        if (taxon.getCommonName() != null
            && !taxon.getCommonName().equals(sciName)) {
          return localNames.compoundName(taxon, false);
        } else {
          return sciName;
        }
    }
  }

  private static final ImmutableSet<Status> STATUS_TO_SHOW =
      ImmutableSet.of(
          Status.EN,
          Status.CR,
          Status.EW,
          Status.EX);
  
  private boolean showStatus(Status status) {
    return STATUS_TO_SHOW.contains(status);
  }

  protected String convertResolvedToString(Resolved taxon) {
    if (taxon.getType() == SightingTaxon.Type.SINGLE) {
      return convertTaxonToString(taxon.getTaxon());
    } else {
      // TODO: something better?  These will generally be kinda long,
      // so being concise seems a good move.
      return taxon.getPreferredSingleName();
    }
  }

  private Status getStatus(Taxon taxon) {
    if (taxon instanceof Species) {
      return ((Species) taxon).getStatus();
    } else {
      return Status.LC;
    }
  }

  /**
   * Override to change a taxon's font.
   */
  protected Font deriveTaxonFont(Font base, Taxon taxon, boolean leaf) {
    if (taxon.isDisabled()) {
      base = base.deriveFont(ImmutableMap.of(
          TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON));
    }
    return base;
  }

  /**
   * Override to change a taxon's foreground color.
   */
  protected Color deriveTaxonForeground(
      Color foreground, Taxon taxon, boolean leaf, boolean selected) {
    return foreground;
  }

  /**
   * Override to change the font of a non-Taxon.
   */
  protected Font deriveOtherFont(Font base, Object value) {
    return base;
  }
  
  private Optional<String> getPathText(TreePath path) {
    Component browserCellRendererComponent = getCellRenderer().getBrowserCellRendererComponent(
        this, path.getLastPathComponent(), false, false, false, 0, false);
    return getText(browserCellRendererComponent);
  }
  
  private int comparePaths(TreePath left, TreePath right) {
    if (left.getPathCount() == right.getPathCount()) {
      return comparePathsOfEqualLength(left, right);
    } else if (left.getPathCount() > right.getPathCount()) {
      return comparePathsOfUnequalLength(left, right);
    } else {
      return -comparePathsOfUnequalLength(right, left);
    }
  }
  
  private int comparePathsOfUnequalLength(TreePath longer, TreePath shorter) {
    while (longer.getPathCount() > shorter.getPathCount()) {
      longer = longer.getParentPath();
    }
    
    return comparePathsOfEqualLength(longer, shorter);
  }
  
  private int comparePathsOfEqualLength(TreePath left, TreePath right) {
    while (true) {
      TreePath leftParent = left.getParentPath();
      TreePath rightParent = right.getParentPath();
      
      // Stop when the two paths have an equal penultimate element
      if (leftParent.getLastPathComponent().equals(rightParent.getLastPathComponent())) {
        break;
      }
      
      left = leftParent;
      right = rightParent;
    }
    
    TreeModel model = getModel();
    int leftIndex = model.getIndexOfChild(
        left.getParentPath().getLastPathComponent(),
        left.getLastPathComponent());
    int rightIndex = model.getIndexOfChild(
        right.getParentPath().getLastPathComponent(),
        right.getLastPathComponent());
    return leftIndex - rightIndex;
  }

  private Optional<String> getText(Component component) {
    if (component instanceof JLabel) {
      String text = ((JLabel) component).getText();
      if (!Strings.isNullOrEmpty(text)) {
        return Optional.of(text);
      }
    }
    
    if (component instanceof Container) {
      for (Component child : ((Container) component).getComponents()) {
        Optional<String> childText = getText(child);
        if (childText.isPresent()) {
          return childText;
        }
      }
    }
    
    return Optional.absent();
  }

  abstract protected void doMove(SightingsGroup sightings);
  
  protected boolean canImportSightings(TransferSupport transferSupport, TreePath target, SightingsGroup sightings) {
    Taxon taxon;
    if (target.getLastPathComponent() instanceof Taxon) {
      taxon = (Taxon) target.getLastPathComponent();
    } else if (target.getLastPathComponent() instanceof Resolved) {
      Resolved resolved = (Resolved) target.getLastPathComponent();
      if (resolved.getType() != SightingTaxon.Type.SINGLE) {
        return false;
      }
      taxon = resolved.getTaxon();
    } else if (target.getLastPathComponent() instanceof Sighting) {
      // Allow pasting sightings onto sightings, but not drop
      if (transferSupport.isDrop()) {
        return false;
      }
      // Grab the parent of the sighting, which will be a taxon
      Object penultimate = target.getParentPath().getLastPathComponent();
      if (!(penultimate instanceof Taxon)) {
        return false;
      }
      taxon = (Taxon) penultimate;
    } else {
      return false;
    }
    
    // Only allow drags onto species or smaller, and don't allow drags onto disabled species
    if ((taxon.getType().compareTo(Taxon.Type.species) > 0) 
        || taxon.isDisabled()) {
      return false;
    }
    
    if (transferSupport.isDrop() && taxon.equals(sightings.getSourceTaxon())) {
      return false;
    }
    
    return true;
  }

  protected boolean doImport(TransferSupport transferSupport,
      SightingsGroup sightings, TreePath target) {
    return false;
  }

  /**
   * TransferHandler implementation for taxon browsers.
   * By default, supports dragging sightings (singly or
   * as groups) onto species, as copy or move, but doesn't
   * define what a copy/move does.
   */
  protected class TaxonTransferHandler extends TransferHandler {
    @Override protected Transferable createTransferable(JComponent c) {
      // Force the current preview to be saved, otherwise the sightings
      // can change out from underneath the user.
      saveCurrentPreview();
      
      TreePath[] paths = getSelectionPaths();
      if (paths == null || paths.length == 0) {
        return null;
      }
      
      String pathsAsText = Stream.of(paths)
          .sorted(BaseTaxonBrowserPanel.this::comparePaths)
          .map(BaseTaxonBrowserPanel.this::getPathText)
          .filter(Optional::isPresent)
          .map(Optional::get)
          .collect(Collectors.joining("\n"));
      SightingsGroup sightings = getSightingsGroup(paths);
      StringSelection textTransferable = pathsAsText.isEmpty()
          ? null : new StringSelection(pathsAsText);
      if (sightings == null) {
        return textTransferable;
      }
      
      return new SightingTransferable(sightings, textTransferable);
    }

    @Override public int getSourceActions(JComponent component) {
      TreePath[] paths = getSelectionPaths();
      SightingsGroup sightings = getSightingsGroup(paths);
      if (sightings == null) {
        return COPY;
      }
      
      return COPY_OR_MOVE;
    }
    
    @Override public boolean canImport(JComponent component, DataFlavor[] flavors) {
      // For drag-and-drop, the legacy canImport() is badly broken.  Fail fast.
      throw new UnsupportedOperationException("Legacy canImport() not supported");
    }

    @Override public boolean canImport(TransferSupport transferSupport) {
      if (!canImportSightings()) {
        return false;
      }

      if (!transferSupport.isDataFlavorSupported(SightingsGroup.FLAVOR)) {
        return false;
      }
      
      TreePath target = getDropTarget(transferSupport);
      if (target == null) {
        return false;
      }
      
      SightingsGroup sightings = getSighting(transferSupport);
      if (sightings == null) {
        return false;
      }
      
      return canImportSightings(transferSupport, target, sightings);
    }

    private SightingsGroup getSighting(TransferSupport transferSupport) {
      return SightingsGroup.fromTransferable(transferSupport.getTransferable());
    }

    @Override protected void exportDone(JComponent source, Transferable data, int action) {
      if (action == NONE) {
        return;
      }
      
      if (action == MOVE) {
        if (data.isDataFlavorSupported(SightingsGroup.FLAVOR)) {
          SightingsGroup sightings = SightingsGroup.fromTransferable(data);
          doMove(sightings);
        }
      }
    }

    @Override public boolean importData(JComponent component, Transferable data) {
      throw new UnsupportedOperationException("Legacy importData() not supported.");
    }

    @Override public boolean importData(TransferSupport transferSupport) {
      if (!canImport(transferSupport)) {
        return false;        
      }

      SightingsGroup sightings = getSighting(transferSupport);
      TreePath target = getDropTarget(transferSupport);
      doImport(transferSupport, sightings, target);
      return true;
    }

    private TreePath getDropTarget(TransferSupport transferSupport) {
      if (!transferSupport.isDrop()) {
        TreePath[] paths = getSelectionPaths();
        if (paths == null || paths.length != 1) {
          return null;
        }
        
        return paths[0];
      } 
      
      Point locationInBrowser = SwingUtilities.convertPoint(transferSupport.getComponent(),
          transferSupport.getDropLocation().getDropPoint(),
          BaseTaxonBrowserPanel.this);
      
      TreePath path = getPathForLocation(locationInBrowser.x, locationInBrowser.y);
      if (path == null) {
        return null;
      }
      
      Rectangle bounds = getPathBounds(path);
      if (bounds == null) {
        return null;
      }
      
      if (!bounds.contains(locationInBrowser)) {
        return null;
      }
      
      return path;
    }

    /**
     * For an array of paths, return the SightingsGroup object that will be used 
     * for transfers.
     */
    private SightingsGroup getSightingsGroup(TreePath[] paths) {
      ImmutableList.Builder<Sighting> sightingsBuilder = ImmutableList.builder();
      ImmutableList.Builder<Integer> originalIndices = ImmutableList.builder();
      if (paths == null || paths.length == 0) {
        return null;
      }

      TreePath firstPath = paths[0];
      Object commonParent = firstPath.getParentPath().getLastPathComponent();
      
      for (TreePath path : paths) {
        Object last = path.getLastPathComponent();
        if (last instanceof Sighting) {
          sightingsBuilder.add((Sighting) last);
        } else if (last instanceof ResolvedWithSighting) {
          sightingsBuilder.add(((ResolvedWithSighting) last).getSighting());
        } else {
          return null;
        }
        
        ;
        // Scream if the sightings belong to two different parents.  (Note:  it's
        // perfectly reasonable for the sightings to be in two different taxa, e.g.,
        // if the user is viewing without subspecies and sightings belong to
        // multiple subspecies.)  JBrowser should never allow sightings in two
        // paths, so this exception would indicate something going badly wrong.
        Object parent = path.getParentPath().getLastPathComponent();
        if (!parent.equals(commonParent)) {
          throw new IllegalStateException("Multiple sightings from different parents");
        }
        
        // Remember the original index of each child.  This is convenient for implementing
        // removal - and since drops don't go into the original parent, the indices will
        // still be valid.
        originalIndices.add(getModel().getIndexOfChild(parent, last));
      }
      ImmutableList<Sighting> sightings = sightingsBuilder.build();
      
      // Grab all the visit info for these sightings, in case removing the sighting
      // eliminates the last entry.
      // Write to a hashmap, then copy to an ImmutableMap.  Under some circumstances,
      // it's possible to have two sightings from the same visit (e.g. a "sp" and
      // a non-sp), and this blows up if it's an ImmutableMap.
      Map<VisitInfoKey, VisitInfo> visitInfoMap = Maps.newLinkedHashMap();
      for (Sighting sighting : sightings) {
        VisitInfoKey key = VisitInfoKey.forSighting(sighting);
        if (key != null) {
          VisitInfo visitInfo = reportSet.getVisitInfo(key);
          if (visitInfo != null) {
            visitInfoMap.put(key, visitInfo);
          }
        }
      }
      
      return new SightingsGroup(
          sightings, ImmutableMap.copyOf(visitInfoMap), originalIndices.build(), paths[0].getParentPath());
    }
  }

  private class TaxonBrowserCellRenderer extends DefaultBrowserCellRenderer {
    public TaxonBrowserCellRenderer() {
      setOpenIcon(null);
      setClosedIcon(null);
      setLeafIcon(null);
    }

    @Override
    public Component getBrowserCellRendererComponent(JBrowser browser,
        Object value, boolean selected, boolean expanded, boolean leaf,
        int row, boolean hasFocus) {
      if (value == TaxonTreeModel.Node.SEPARATOR) {
        return new JSeparator(SwingConstants.HORIZONTAL);
      }
      
      Component cell = super.getBrowserCellRendererComponent(browser, value, selected,
          expanded, leaf, row, hasFocus);
      cell.setFont(fontManager.getTextFont());
      
      if (value instanceof Resolved) {
        Resolved resolved = (Resolved) value;
        if (resolved.getType() == SightingTaxon.Type.SINGLE) {
          cell.setFont(deriveTaxonFont(cell.getFont(), resolved.getTaxon(), leaf));
          cell.setForeground(
              deriveTaxonForeground(getForeground(), resolved.getTaxon(), leaf, selected));
        }
      } else if (value instanceof Taxon) {
        Taxon taxon = (Taxon) value;
        cell.setFont(deriveTaxonFont(cell.getFont(), taxon, leaf));
        cell.setForeground(
            deriveTaxonForeground(getForeground(), taxon, leaf, selected));
      } else {
        cell.setFont(deriveOtherFont(cell.getFont(), value));
      }
      
      return cell;
    }
    
  }
  
  protected void customizeSightingPreviewPanel(SightingBrowsePanel panel) {
  }
  
  protected void customizeSightingPreviewPanel(SightingBulkEditPanel sightingBulkEditPanel) {
  }

  /**
   * @return true iff getSightingsGroup() would return null (e.g., there's
   * a valid selection for cut-copy-drag)
   */
  protected boolean hasSightingsGroup(TreePath[] paths) {
    if (paths == null) {
      return false;
    }
    
    for (TreePath path : paths) {
      Object lastComponent = path.getLastPathComponent();
      if (!(lastComponent instanceof Sighting)
          && !(lastComponent instanceof ResolvedWithSighting)) {
        return false;
      }
    }
    
    return true;
  }

  protected ReportSet getReportSet() {
    return reportSet;
  }

  public void saveCurrentPreview() {
    Component c = getPreviewRendererComponent();
    if (c instanceof SightingBrowsePanel) {
      ((SightingBrowsePanel) c).save();
    } else if (c instanceof SightingBulkEditPanel) {
      ((SightingBulkEditPanel) c).save();
    }
  }
  
  protected class TaxonPreviewRenderer implements BrowserPreviewRenderer {
    @Override public Component getPreviewRendererComponent(
        final JBrowser browser,
        final TreePath[] paths) {
      
      final List<Sighting> sightings = Lists.newArrayList();
      final List<VisitInfoKey> visitInfoKeys = Lists.newArrayList();
      for (TreePath path : paths) {
        Object lastElement = path.getLastPathComponent();
        if (lastElement instanceof Resolved) {
          return newResolvedPreview((Resolved) lastElement);
        } else if (lastElement instanceof ResolvedWithSighting) {
          sightings.add(((ResolvedWithSighting) lastElement).getSighting());
        } else if (lastElement instanceof Sighting) {
          sightings.add((Sighting) lastElement);
        } else if (lastElement instanceof VisitInfoKey) {
          visitInfoKeys.add((VisitInfoKey) lastElement);
        } else {
          return new JLabel();
        }
      }
      
      // VisitInfoKey preview?
      if (!visitInfoKeys.isEmpty()) {
        // Can only support editing one visit info at a time - and not with sightings!
        if (visitInfoKeys.size() > 1 || !sightings.isEmpty()) {
          return new JLabel();
        }
        
        Preconditions.checkState(paths.length == 1);
        final VisitInfoKey visitInfoKey = visitInfoKeys.get(0);
        VisitInfoBrowsePanel.UpdatedListener updated = new VisitInfoBrowsePanel.UpdatedListener() {
          @Override public void visitInfoUpdated(VisitInfoKey updatedVisitInfoKey) {
            if (!visitInfoKey.equals(updatedVisitInfoKey)) {
              replaceVisitInfoKey(updatedVisitInfoKey);
            }
          }

          @Override
          public void visitInfoDeleted(VisitInfoKey key) {
            visitInfoKeyDeleted(key);
          }
        };
        
        VisitInfoBrowsePanel panel = new VisitInfoBrowsePanel(
            reportSet,
            visitInfoKey,
            reportSet.getVisitInfo(visitInfoKey),
            getVisitInfoPreferences(),
            alerts,
            fontManager,
            updated,
            navigableFrame,
            newLocationDialog,
            predefinedLocations,
            openMapUrl,
            getReturnToCurrentVisitInfoAction(visitInfoKey));
        fontManager.applyTo(panel);
        return panel;
      }
      
      if (sightings.size() == 1) {
        final Sighting sighting = Iterables.getOnlyElement(sightings);
        final TreePath path = paths[0];
        final int currentIndex = getSightingIndex(sighting, path.getParentPath());
        
        final AtomicReference<SightingBrowsePanel> panelReference = new AtomicReference<SightingBrowsePanel>();
        SightingBrowsePanel sightingBrowsePanel = new SightingBrowsePanel(
            taxonomy, getReportSet(), sighting,
            newLocationDialog,
            navigableFrame,
            new ReturnToPanel(navigableFrame, getReturnPanelName(), returnToSighting(sighting)),
            spHybridDialog,
            predefinedLocations,
            openMapUrl,
            fontManager,
            fileDialogs,
            alerts,
            new SightingBrowsePanel.UpdatedListener() {
              @Override
              public void sightingsUpdated(Sighting updatedSighting) {
                sightingsSwapped(updatedSighting, updatedSighting);
              }
              
              @Override
              public void sightingsSwapped(Sighting oldSighting, Sighting newSighting) {
                if (BaseTaxonBrowserPanel.this.isDisplayable()) {
                  // If the base panel isn't *already* invisible, then clear up the model accordingly
                  sightingsReplaced(path.getParentPath(),
                      ImmutableList.of(oldSighting),
                      ImmutableList.of(newSighting),
                      new int[]{currentIndex});
                  if (panelReference.get() != null && panelReference.get().isDisplayable()) {
                    reselectIfNecessary(path.getParentPath(), ImmutableList.of(newSighting));
                  }
                }
              }
            });
        panelReference.set(sightingBrowsePanel);
        
        fontManager.applyTo(sightingBrowsePanel);
        customizeSightingPreviewPanel(sightingBrowsePanel);
        return sightingBrowsePanel;
      } else {
        final TreePath parent = paths[0].getParentPath();
        final int[] indices = new int[paths.length];
        for (int i = 0; i < indices.length; i++) {
          indices[i] = getModel().getIndexOfChild(
              parent.getLastPathComponent(), paths[i].getLastPathComponent()); 
        }
        UpdatedListener updated = new UpdatedListener() {

          @Override
          public void sightingsUpdated(List<Sighting> sightings) {
            sightingsSwapped(sightings, sightings);
          }

          @Override
          public void sightingsSwapped(List<Sighting> oldSightings, List<Sighting> newSightings) {
            // If the base panel isn't *already* invisible, then clear up the model accordingly
            if (BaseTaxonBrowserPanel.this.isDisplayable()) {
              sightingsReplaced(parent, oldSightings, newSightings, indices);
              reselectIfNecessary(parent, newSightings);
            }
          }

          @Override
          public void sightingsRemoved(List<Sighting> sightings) {
            // This should only get hit for the reports panel
            throw new UnsupportedOperationException("Not supported here");
          }          
        };
        SightingBulkEditPanel sightingBulkEditPanel = new SightingBulkEditPanel(
            taxonomy, reportSet, sightings, updated, alerts,
            fontManager,
            allowLocationEditing()
                ? new WherePanel(reportSet, newLocationDialog, predefinedLocations, openMapUrl, "")
                : null,
            spHybridDialog);
        fontManager.applyTo(sightingBulkEditPanel);
        customizeSightingPreviewPanel(sightingBulkEditPanel);
        return sightingBulkEditPanel;
      }
    }

    private Component newResolvedPreview(Resolved resolved) {
      if (resolved.getLargestTaxonType().compareTo(Taxon.Type.species) > 0) {
        return new JLabel();
      }
      StringBuilder builder = new StringBuilder("<html>");
      speciesInfoDescriber.toHtmlText(resolved, reportSet, builder, true);
      
      JScrollPane speciesInfoScrollPane = new JScrollPane();
      
      JEditorPane speciesInfoText = new JEditorPane();
      speciesInfoText.setContentType("text/html");
      speciesInfoText.setBackground(Color.WHITE);
      speciesInfoText.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, true);
      speciesInfoScrollPane.setViewportView(speciesInfoText);
      speciesInfoScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
      speciesInfoScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      speciesInfoText.setEditable(false);
      speciesInfoText.setText(builder.toString());
      speciesInfoText.addHyperlinkListener(new HyperlinkListener() {
        @Override
        public void hyperlinkUpdate(HyperlinkEvent event) {
          if (event.getEventType() == EventType.ACTIVATED) {
            if (SpeciesInfoDescriber.isShowMapUrl(event.getURL())){
              String id = SpeciesInfoDescriber.getShowMapSpeciesId(event.getURL());
              try {
                ShowSpeciesMap showSpeciesMap = new ShowSpeciesMap(predefinedLocations, reportSet);
                showSpeciesMap.showRange(reportSet, taxonomy, id);
              } catch (IOException e) {
                alerts.showError(BaseTaxonBrowserPanel.this, e, Name.MAP_FAILED, Name.COULDNT_WRITE_MAP);
              }
            } else {
              if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                try {
                  Desktop.getDesktop().browse(event.getURL().toURI());
                } catch (IOException | URISyntaxException e) {
                  logger.log(Level.WARNING, "Couldn't open URL " + event.getURL(), e);
                }
              }
            }
          }
        }
      });
      return speciesInfoScrollPane;
    }
  }
  
  private void reselectIfNecessary(TreePath oldParentPath, List<Sighting> newSightings) {
    // If the taxon that was selected is no longer in existence, reselect the new sightings
    TreePath[] paths = new TreePath[newSightings.size()];
    for (int i = 0; i < newSightings.size(); i++) {
      paths[i] = pathToSighting(oldParentPath, newSightings.get(i));
    }
    
    setSelectionPaths(paths);
    ensurePathIsVisible(paths[0]);
    if (paths.length > 1) {
      ensurePathIsVisible(paths[paths.length - 1]);
    }
  }
  
  public interface PostNavigateAction {
    void postNavigate(JPanel panel);
  }

  static class ReturnToPanel extends AbstractAction {
    private final NavigableFrame navigableFrame;
    private final String panelName;
    private final PostNavigateAction postNavigateAction;

    ReturnToPanel(NavigableFrame navigableFrame, String panelName,
        PostNavigateAction postNavigateAction) {
      this.navigableFrame = navigableFrame;
      this.panelName = panelName;
      this.postNavigateAction = postNavigateAction;      
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
      JPanel panel = navigableFrame.navigateTo(panelName);
      if (panel != null) {
        postNavigateAction.postNavigate(panel);
      }
    }
  }
  
  protected abstract String getReturnPanelName();
  
  protected abstract PostNavigateAction returnToSighting(Sighting sighting);

  protected abstract TreePath pathToSighting(TreePath oldParentPath, Sighting sighting);

  protected abstract void sightingsReplaced(
      TreePath parent,
      List<Sighting> oldSightings,
      List<Sighting> newSightings,
      int[] indices);

  protected int getSightingIndex(Sighting sighting, TreePath parentPath) {
    return getModel().getIndexOfChild(parentPath.getLastPathComponent(), sighting);
  }

  /**
   * Wrapper to enable-disable the cut action.  Enable when selection indicates
   * the selection is only sightings, otherwise disable.
   */
  protected class CutActionWrapper extends ForwardingAction
      implements Attachable, TreeSelectionListener {
    public CutActionWrapper(Action wrapped) {
      super(BaseTaxonBrowserPanel.this, wrapped);
    }
    
    @Override public void attach() {
      getSelectionModel().addTreeSelectionListener(this);
      updateEnabled();
    }

    @Override public void unattach() {
      getSelectionModel().removeTreeSelectionListener(this);
    }

    @Override public void valueChanged(TreeSelectionEvent event) {
      updateEnabled();
    }    
    
    private void updateEnabled() {
      setEnabled(hasSightingsGroup(getSelectionPaths()));      
    }
  }

  /**
   * Wrapper to enable-disable the copy actions.  Enable when selection indicates
   * there is any selection that contains text.
   */
  protected class CopyActionWrapper extends ForwardingAction
      implements Attachable, TreeSelectionListener {
    public CopyActionWrapper(Action wrapped) {
      super(BaseTaxonBrowserPanel.this, wrapped);
    }
    
    @Override public void attach() {
      getSelectionModel().addTreeSelectionListener(this);
      updateEnabled();
    }

    @Override public void unattach() {
      getSelectionModel().removeTreeSelectionListener(this);
    }

    @Override public void valueChanged(TreeSelectionEvent event) {
      updateEnabled();
    }    
    
    private void updateEnabled() {
      setEnabled(hasText(getSelectionPaths()));      
    }
    
    private boolean hasText(TreePath[] paths) {
      if (paths == null) {
        return false;
      }
      
      for (TreePath path : paths) {
        if (getPathText(path).isPresent()) {
          return true;
        }
      }
      
      return false;
    }
  }

  /** If true, sightings can be pasted. */
  protected boolean canImportSightings() {
    return true;
  }
  
  /**
   * If true, the given "lastPathComponent" is an OK place to import sightings.
   */
  protected boolean canImportSightingsToLastPathComponent(Object lastPathComponent) {
    // Support pasting onto a sighting (which is really just pasting onto the parent).
    if (lastPathComponent instanceof Sighting || lastPathComponent instanceof ResolvedWithSighting) {
      return true;
    }
    
    Taxon taxon;
    
    if (lastPathComponent instanceof Taxon t) {
      taxon = t;
    } else if (lastPathComponent instanceof Resolved r) {
      taxon = r.getType() == SightingTaxon.Type.SINGLE ? r.getTaxon() : null;
    } else {
      taxon = null;
    }
    
    if (taxon == null) {
      return false;
    }
    
    return !taxon.isDisabled()
        && taxon.getType().compareTo(Taxon.Type.species) <= 0; 
  
  }
  
  /**
   * Wrapper to enable-disable pasting.  Enable only when the clipboard contains a SightingsGroup,
   * and when there is one selected taxon.
   */
  protected class PasteActionWrapper extends ForwardingAction
      implements Attachable, FlavorListener, TreeSelectionListener {

    public PasteActionWrapper(Action wrapped) {
      super(BaseTaxonBrowserPanel.this, wrapped);
    }
    
    @Override public void attach() {
      Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
      clipboard.addFlavorListener(this);
      getSelectionModel().addTreeSelectionListener(this);
      // Manually get the enabled state set initially
      updateEnabled();
    }

    private void updateEnabled() {
      setEnabled(pasteAvailable());
    }

    private boolean pasteAvailable() {
      if (!canImportSightings()) {
        return false;
      }
      
      Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
      try {
        if (!clipboard.isDataFlavorAvailable(SightingsGroup.FLAVOR)) {
          return false;
        }
      } catch (IllegalStateException e) {
        // Windows blows up sometimes checking for clipboard availability
        return false;
      }

      TreePath[] paths = getSelectionPaths();
      if (paths == null || paths.length != 1) {
        return false;
      }
      
      Object lastPathComponent = paths[0].getLastPathComponent();
      return canImportSightingsToLastPathComponent(lastPathComponent);
    }

    @Override public void unattach() {
      Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
      clipboard.removeFlavorListener(this);
      getSelectionModel().removeTreeSelectionListener(this);
    }

    @Override public void flavorsChanged(FlavorEvent e) {
      updateEnabled();
    }

    @Override public void valueChanged(TreeSelectionEvent e) {
      updateEnabled();
    } 
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    setFixedCellWidth(fontManager.scale(270));
    setMinimumCellWidth(fontManager.scale(200));
    setMinimumSize(fontManager.scale(new Dimension(1000, 800)));
  }
  
  protected boolean allowLocationEditing() {
    return true;
  }

  protected VisitInfoPreferences getVisitInfoPreferences() {
    throw new UnsupportedOperationException();
  }
  
  protected void replaceVisitInfoKey(VisitInfoKey updatedVisitInfoKey) {
    throw new UnsupportedOperationException();
  }

  protected void visitInfoKeyDeleted(VisitInfoKey deletedVisitInfoKey) {
    throw new UnsupportedOperationException();
  }

  protected Action getReturnToCurrentVisitInfoAction(VisitInfoKey visitInfoKey) {    
    return null;
  }
}
