/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.AbstractAction;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorListener;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.ResolvedComparator;
import com.scythebill.birdlist.ui.components.table.DelayedUI;
import com.scythebill.birdlist.ui.components.table.ExpandableTable;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.Column;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.ColumnLayout;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.DetailView;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.RowState;
import com.scythebill.birdlist.ui.components.table.SomeRowsTableRowsModel;
import com.scythebill.birdlist.ui.components.table.TableRowsModel;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.util.ListListModel;

/**
 * Panel supporting display of a species ExpandableTable, where all entries come from
 * the checklist.
 */
public class ChecklistSpeciesTablePanel extends SpeciesTablePanel {
  private ListListModel<Resolved> speciesListModel;
  private Checklist checklist;
  private boolean includeRarities;
  private Taxonomy speciesListModelTaxonomy;

  /** Creates a new ChecklistSpeciesTablePanel */
  public ChecklistSpeciesTablePanel(
      DetailView<Resolved> expandedRowView,
      String detailTitle,
      FontManager fontManager,
      Checklist checklist,
      boolean includeRarities, // should rarities be shown in the table?
      Taxonomy taxonomy) {
    super(expandedRowView, detailTitle, fontManager);
    
    this.checklist = checklist;
    this.includeRarities = includeRarities;
    
    speciesListModelTaxonomy = taxonomy;
    speciesListModel = newListModel(taxonomy);
    
    init();

    hookUpContents();    
  }

  /** Called after the species table is created, and before general columns are added. */
  @Override
  protected void speciesTableCreated() {
    // Track rows as they get enabled
    getSpeciesTable().setEnabledRowsModel(new SomeRowsTableRowsModel());
    getSpeciesTable().setHighlightEnabledRows(true);
    getSpeciesTable().getEnabledRowsModel().addListener(new TableRowsModel.Listener() {
      @Override public void rowRemoved(int row) {
        fireValueChanged();
      }
      
      @Override public void rowIncluded(int row) {
        fireValueChanged();
      }
    });
    
    // Start with the check column
    addColumn(new CheckColumn(ColumnLayout.fixedWidth(30)));
  }
  
  /** Create (or re-create) the list model. */
  private ListListModel<Resolved> newListModel(Taxonomy taxonomy) {
    TreeSet<Resolved> set = Sets.newTreeSet(new ResolvedComparator());
    // Resolve all taxa;  the checklists are already local to a taxonomy,
    // so use "resolveInternal"
    for (SightingTaxon taxon : this.checklist.getTaxa(taxonomy)) {
      // Skip rarities unless they're specifically included.
      if (!includeRarities) {
        Status status = this.checklist.getStatus(taxonomy, taxon);
        // ... and include extirpated species as rarities.  'Cause you're not gonna see them....
        if (status == Status.RARITY || status == Status.EXTINCT || status == Status.RARITY_FROM_INTRODUCED) {
          continue;
        }
      }
      set.add(taxon.resolveInternal(taxonomy));
    }
    ListListModel<Resolved> model = new ListListModel<Resolved>();
    model.asList().addAll(set);
    return model;
  }
  
  /** Add a shortcut for the space key. */
  private void hookUpContents() {
    KeyStroke upKey = KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0);
    getSpeciesTable().getInputMap().put(upKey, "space");
    getSpeciesTable().getActionMap().put("space", new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent event) {
        int row = getSpeciesTable().getSelectedIndex();
        if (row != ExpandableTable.NO_ROW) {
          TableRowsModel enabledRows = getSpeciesTable().getEnabledRowsModel();
          if (enabledRows.isIncluded(row)) {
            enabledRows.removeRow(row);
          } else {
            enabledRows.includeRow(row);
          }
        }
      }
    });
  }

  @Override
  public void addSpecies(Resolved resolved) {
    Preconditions.checkNotNull(resolved);
    
    addSpeciesInternal(resolved);
    
    fireValueChanged();
  }

  @Override
  public void removeSpecies(Resolved taxon) {
    Preconditions.checkNotNull(taxon);
    
    throw new UnsupportedOperationException();
  }

  @Override
  public void swapSpecies(
      Resolved currentTaxon, Resolved newTaxon) {
    int indexOf = speciesListModel.asList().indexOf(currentTaxon);
    Preconditions.checkState(indexOf >= 0, currentTaxon.getFullName() + " not found");

    speciesListModel.asList().set(indexOf, newTaxon);
  }

  @Override
  public void setSortedContents(Taxonomy taxonomy, List<Resolved> taxa) {
    Preconditions.checkNotNull(taxa);
    
    // Clear the entire selection
    TableRowsModel enabledRowsModel = getSpeciesTable().getEnabledRowsModel();
    for (int i = 0; i < speciesListModel.getSize(); i++) {
      if (enabledRowsModel.isIncluded(i)) {
        // TODO: not as efficient as it might be... but it almost certainly does
        // not matter as long as we don't end up computing the value at each addition
        // or removal (in which case this'd be N^2)
        enabledRowsModel.removeRow(i);
      }
    }

    // If this is called because the taxonomy has changed, it's time to rebuild the whole
    // backing checklist 
    if (taxonomy != speciesListModelTaxonomy) {
      speciesListModelTaxonomy = taxonomy;
      speciesListModel = newListModel(taxonomy);
      getSpeciesTable().setModel(speciesListModel);
    }
    
    for (Resolved resolved : taxa) {
      addSpecies(resolved);
    }
    
    fireValueChanged();
  }

  /** Add a single resolved. */
  private void addSpeciesInternal(Resolved resolved) {
    int searchResult = Collections.binarySearch(
        speciesListModel.asList(), 
        resolved,
        new ResolvedComparator());
    if (searchResult >= 0) {
      getSpeciesTable().getEnabledRowsModel().includeRow(searchResult);
    } else {
      int insertionIndex = -searchResult - 1;
      speciesListModel.asList().add(insertionIndex, resolved);
      getSpeciesTable().getEnabledRowsModel().includeRow(insertionIndex);
    }
  }
  
  /**
   * Returns a copy of the current species list.
   */
  @Override
  public ImmutableList<Resolved> getValue() {
    ImmutableList.Builder<Resolved> enabled = ImmutableList.builder();

    List<Resolved> resolved = speciesListModel.asList();
    TableRowsModel enabledRowsModel = getSpeciesTable().getEnabledRowsModel();
    for (int i = 0; i < resolved.size(); i++) {
      if (enabledRowsModel.isIncluded(i)) {
        enabled.add(resolved.get(i));
      }
    }
    
    return enabled.build();
  }

  @Override
  protected ListListModel<Resolved> getListModel() {
    return speciesListModel;
  }

  private final static Object KEY = new Object();
  /** Column with the checkbox for including (or not) underlying checks. */
  class CheckColumn implements Column<Resolved> {
    private final ActionListener listener;
    private ColumnLayout width;
    
    public CheckColumn(ColumnLayout width) {
      this.width = width;
      
      listener = new ActionListener() {
        /** Track clicks on the checkbox. */
        @Override public void actionPerformed(ActionEvent e) {
          ExpandableTable<Resolved> table = ChecklistSpeciesTablePanel.this.getSpeciesTable();
          // Move the focus to the table (so that up and down arrows work)
          table.requestFocusInWindow();
          JCheckBox checkBox = (JCheckBox) e.getSource();
          
          // Notify the table of the row status
          Resolved resolved = (Resolved) checkBox.getClientProperty(KEY);
          int row = getListModel().asList().indexOf(resolved);
          if (row >= 0) {
            boolean isSelected = checkBox.isSelected();
            // Toggle the row
            if (isSelected) {
              table.getEnabledRowsModel().includeRow(row);
            } else {
              table.getEnabledRowsModel().removeRow(row);
            }
            notifyUserModifiedValue();
            // and select that row. 
            table.setSelectedIndex(row);
          }
        }
      };
    }


    @Override public JComponent createComponent(Resolved value, Set<RowState> states) {
      JCheckBox checkBox = new JCheckBox() {
        @Override
        protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
          // Disable AncestorListeners...  at least on the Mac, AncestorListeners get added, which leads
          // to a ton of ComponentListeners getting added up the stack.
          if (!"parent".equals(propertyName) && !"ancestor".equals(propertyName)) {
            super.firePropertyChange(propertyName, oldValue, newValue);
          }
        }

        @Override
        public void updateUI() {
          // Delay creation of the backing Swing UI.  Makes initial rendering much faster.
          if (DelayedUI.instance().requestUICreation(this)) {
            super.updateUI();
          }
        }

        @Override
        public void addAncestorListener(AncestorListener listener) {
          // Blow off attempts to add ancestor listeners.
          // This is used (at least) in Aqua to handle default buttons,
          // which are irrelevant for checkboxes.
        }

      };
      // Prevent focus from going to the checkbox;  spacebar on the row has the same effect,
      // so this doesn't improve accessibility.
      checkBox.setFocusable(false);
      checkBox.addActionListener(listener);
      updateComponentValue(checkBox, value);
      updateComponentState(checkBox, value, states);
      return checkBox;
    }

    @Override public ColumnLayout getWidth() {
      return width;
    }

    @Override public void updateComponentValue(Component component, Resolved value) {
      JCheckBox checkBox = (JCheckBox) component;
      checkBox.putClientProperty(KEY, value);
    }
    
    @Override public void updateComponentState(Component component,
        Resolved value,
        Set<RowState> states) {
      JCheckBox checkBox = (JCheckBox) component;
      checkBox.setSelected(states.contains(RowState.ENABLED));
    }

    @Override
    public String getName() {
      // No name for the column
      return "";
    }

    @Override
    public boolean sizeComponentsToFit() {
      return false;
    }
  }

  // Listener for clicks on the label.  Double-clicks should select rows
  private final MouseListener labelListener = new MouseAdapter() {
    @Override public void mouseClicked(MouseEvent e) {
      Point tablePoint = SwingUtilities.convertPoint(
          e.getComponent(), e.getPoint(), getSpeciesTable());
      int row = getSpeciesTable().getRow(tablePoint.y);
      if (e.getClickCount() == 2) {
        TableRowsModel enabledRows = getSpeciesTable().getEnabledRowsModel();
        if (enabledRows.isIncluded(row)) {
          enabledRows.removeRow(row);
        } else {
          enabledRows.includeRow(row);
        }
      } else {
        redispatchMouseEventToParent(e);
      }
    }
  };
  
  @Override
  protected void customizeSpeciesLabel(JLabel label) {
    label.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    label.addMouseListener(labelListener);
  }
  
  private void redispatchMouseEventToParent(MouseEvent e) {
    e.getComponent().getParent().dispatchEvent(
        SwingUtilities.convertMouseEvent(e.getComponent(), e, e.getComponent().getParent()));
  }
}
