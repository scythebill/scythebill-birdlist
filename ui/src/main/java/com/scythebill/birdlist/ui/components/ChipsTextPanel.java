/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import static com.google.common.base.Preconditions.checkNotNull;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Insets;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EventListener;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.function.Predicate;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.scythebill.birdlist.model.util.Indexer;
import com.scythebill.birdlist.model.util.ToString;
import com.scythebill.birdlist.ui.components.table.DeleteColumn;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.fonts.FontPreferences;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * A panel supporting adding and removing "chips" of content. 
 */
public class ChipsTextPanel<T> extends JPanel implements FontsUpdatedListener {
  // Using deprecated methods just for test
  @SuppressWarnings("deprecation")
  public static void main(String[] args) {
    IndexerPanel<String> indexerPanel = new IndexerPanel<>();
    indexerPanel.setPreviewText("Enter a name");
    Indexer<String> indexer = new Indexer<>();
    indexer.add("Adam Winer", "Adam Winer");
    indexer.add("Howard Winer", "Howard Winer");
    indexerPanel.addIndexerGroup(new ToString<String>() {}, indexer);

    FontManager fontManager = new FontManager(new FontPreferences());
    ChipsTextPanel<String> chipsTextPanel = new ChipsTextPanel<>(s -> s, fontManager);
    JScrollPane scrollPane = new JScrollPane(chipsTextPanel,
        JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

    JFrame frame = new JFrame();
    frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.PAGE_AXIS));
    Box indexerAndButton = Box.createHorizontalBox();

    JButton addButton = new JButton("Add");
    indexerPanel.addPropertyChangeListener("value",
        e -> addButton.setEnabled(indexerPanel.getValue() != null));
    addButton.addActionListener(e -> chipsTextPanel.addChip(indexerPanel.getValue()));

    indexerAndButton.add(indexerPanel);
    indexerAndButton.add(addButton);

    frame.getContentPane().add(indexerAndButton);
    frame.getContentPane().add(scrollPane);

    fontManager.applyTo(frame);
    frame.pack();
    frame.setVisible(true);
  }

  private final Set<T> chips;
  private final Function<T, String> toString;
  private final FontManager fontManager;
  private int scaledWidth = 80;
  private Icon toggleOffIcon;
  private Icon toggleOnIcon;
  private Predicate<T> toggleOnPredicate;
  private Name offTooltipFormat;
  private Name onTooltipFormat;
  
  public ChipsTextPanel(Function<T, String> toStringFunc, FontManager fontManager) {
    this(toStringFunc, null /* unsorted */, fontManager);
  }
  
  public interface ChipsChangedListener extends EventListener {
    void chipsChanged();
  }

  public interface ChipToggledListener<T> extends EventListener {
    void chipToggled(T chip, boolean newValue);
  }

  public ChipsTextPanel(Function<T, String> toStringFunc, Comparator<? super T> comparator, FontManager fontManager) {
    this.toString = toStringFunc;
    this.fontManager = fontManager;
    this.chips = comparator == null ? new LinkedHashSet<>() : new TreeSet<>(comparator);
    resetContent();
  }

  /**
   * Enables an extra toggle feature;  callers must supply the icons and a definition for
   * how to extract "on".
   */
  public void addToggleFeature(
      Icon offIcon,
      Icon onIcon,
      Predicate<T> onPredicate,
      Name offTooltipFormat,
      Name onTooltipFormat) {
    this.toggleOffIcon = checkNotNull(offIcon);
    this.toggleOnIcon = checkNotNull(onIcon);
    this.toggleOnPredicate = checkNotNull(onPredicate);
    this.offTooltipFormat = offTooltipFormat;
    this.onTooltipFormat = onTooltipFormat;
  }
      
  public void addChip(T chipValue) {
    if (chips.add(chipValue)) {
      // TODO: make not N^2 at startup - don't actually set the content until attached
      resetContent();
      
      for (ChipsChangedListener listener : getListeners(ChipsChangedListener.class)) {
        listener.chipsChanged();
      }
    }
  }

  public void addAllChips(Collection<T> chips) {
    if (this.chips.addAll(chips)) {
      resetContent();
      
      for (ChipsChangedListener listener : getListeners(ChipsChangedListener.class)) {
        listener.chipsChanged();
      }
    }
  }

  public void removeChip(T chipValue) {
    if (chips.remove(chipValue)) {
      resetContent();

      for (ChipsChangedListener listener : getListeners(ChipsChangedListener.class)) {
        listener.chipsChanged();
      }
    }
  }

  public void setScaledWidth(int scaledWidth) {
    this.scaledWidth  = scaledWidth;
  }

  public void clear() {
    if (!chips.isEmpty()) {
      chips.clear();
      
      resetContent();

      for (ChipsChangedListener listener : getListeners(ChipsChangedListener.class)) {
        listener.chipsChanged();
      }
    }
  }

  public void addChipsChangedListener(ChipsChangedListener listener) {
    listenerList.add(ChipsChangedListener.class, listener);
  }
  
  public void addChipToggledListener(ChipToggledListener<T> listener) {
    listenerList.add(ChipToggledListener.class, listener);
  }
  
  @Override
  public void setEnabled(boolean enabled) {
    super.setEnabled(enabled);
    
    resetContent();
  }
  
  private boolean toggleFeatureEnabled() {
    return toggleOnIcon != null;
  }
  
  private void resetContent() {
    removeAll();
    for (T chip : chips) {
      Box box = Box.createHorizontalBox();
      String chipText = toString.apply(chip);
      if (isEnabled() && toggleFeatureEnabled()) {
        TextLink textLink = new TextLink("");
        boolean on = toggleOnPredicate.test(chip);
        textLink.setIcon(on ? toggleOnIcon : toggleOffIcon);
        textLink.setToolTipText(
            Messages.getFormattedMessage(
                on ? onTooltipFormat : offTooltipFormat,
                    chipText));
        textLink.addActionListener(e -> {
          toggleValue(chip, textLink);
        });
        box.add(textLink);
      }
      box.add(toComponent(chip, chipText));
      if (isEnabled()) {
        TextLink textLink = new TextLink("");
        textLink.setIcon(DeleteColumn.deleteIcon());
        textLink.setToolTipText(
            Messages.getFormattedMessage(Name.REMOVE_FORMAT, chipText));
        textLink.addActionListener(e -> removeChip(chip));
        box.add(textLink);
      }
      add(box);
      fontManager.applyTo(box);
    }
    
    if (chips.isEmpty()) {
      Box box = Box.createHorizontalBox();
      box.add(new JLabel(" "));
      box.add(new JLabel(new Icon() {
        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {}
        
        @Override 
        public int getIconWidth() {
          return 1;
        }
        
        @Override
        public int getIconHeight() {
          // TODO: come up with a better way to make the initial height big enough
          return 24;
        }
      }));
      add(box);
    }

    revalidate();
    repaint();
  }

  protected Component toComponent(T chip, String chipText) {
    return new JLabel(chipText);
  }

  static class FixedWidthFlowLayout extends FlowLayout {
    private int fixedWidth;

    FixedWidthFlowLayout(int fixedWidth) {
      this.fixedWidth = fixedWidth;
      setAlignOnBaseline(true);
    }

    @Override
    public Dimension preferredLayoutSize(Container target) {
      Dimension size = layoutSize(target, Component::getPreferredSize);
      // Prefer to stay up at the fixed width
      size.width = Math.max(size.width, fixedWidth);
      return size;
    }

    @Override
    public Dimension minimumLayoutSize(Container target) {
      return layoutSize(target, Component::getMinimumSize);
    }

    private Dimension layoutSize(Container target, Function<Component, Dimension> sizeFunction) {
      synchronized (target.getTreeLock()) {
        Dimension accumulatedDimension = new Dimension(0, 0);
        int nmembers = target.getComponentCount();
        boolean useBaseline = getAlignOnBaseline();
        int maxAscent = 0;
        int maxDescent = 0;
        int currentLineWidth = 0;
        int currentLineHeight = 0;
        for (int i = 0; i < nmembers; i++) {
          Component m = target.getComponent(i);
          if (m.isVisible()) {
            Dimension d = sizeFunction.apply(m);
            // Do we need to move to the next line?
            if (currentLineWidth > 0 && (currentLineWidth + getHgap() + d.width > fixedWidth)) {
              // Add in any accumulated baseline metrics for this line
              if (useBaseline) {
                currentLineHeight = Math.max(maxAscent + maxDescent, currentLineHeight);
              }

              accumulatedDimension.width = Math.max(accumulatedDimension.width, currentLineWidth);
              if (accumulatedDimension.height > 0) {
                accumulatedDimension.height += getVgap();
              }
              accumulatedDimension.height += currentLineHeight;

              maxAscent = 0;
              maxDescent = 0;
              currentLineWidth = 0;
              currentLineHeight = 0;
            }

            if (currentLineWidth < 0) {
              currentLineWidth += getHgap();
            }

            currentLineWidth += d.width;
            currentLineHeight = Math.max(currentLineHeight, d.height);

            if (useBaseline) {
              int baseline = m.getBaseline(d.width, d.height);
              if (baseline >= 0) {
                maxAscent = Math.max(maxAscent, baseline);
                maxDescent = Math.max(maxDescent, d.height - baseline);
              }
            }
          }
        }

        // Add in the stats for the last line
        if (useBaseline) {
          currentLineHeight = Math.max(maxAscent + maxDescent, currentLineHeight);
        }
        accumulatedDimension.width = Math.max(accumulatedDimension.width, currentLineWidth);
        if (accumulatedDimension.height > 0) {
          accumulatedDimension.height += getVgap();
        }
        accumulatedDimension.height += currentLineHeight;
        
        // Make sure it's at least one row high
        Graphics g = target.getGraphics();
        if (g != null) {
          accumulatedDimension.height = Math.max(
              accumulatedDimension.height,
              UIUtils.getFontHeight(g, target.getFont()));
        }

        Insets insets = target.getInsets();
        accumulatedDimension.width += insets.left + insets.right + getHgap() * 2;
        accumulatedDimension.height += insets.top + insets.bottom + getVgap() * 2;
        return accumulatedDimension;
      }
    }
  }

  public Collection<T> getChips() {
    return Collections.unmodifiableCollection(chips);
  }
  
  @Override
  public void fontsUpdated(FontManager fontManager) {
    FlowLayout layout = new FixedWidthFlowLayout(fontManager.scale(scaledWidth));
    layout.setAlignOnBaseline(true);
    layout.setAlignment(FlowLayout.LEADING);
    layout.setHgap(5);
    setLayout(layout);
  }

  private void toggleValue(T chip, TextLink textLink) {
    boolean newValue = !toggleOnPredicate.test(chip);
    textLink.setIcon(newValue ? toggleOnIcon : toggleOffIcon);
    textLink.setToolTipText(
        Messages.getFormattedMessage(
            newValue ? onTooltipFormat : offTooltipFormat,
                toString.apply(chip)));

    // Suppress warnings in here; the generic type of the listener is guaranteed where
    // the listener is added.
    for (@SuppressWarnings("rawtypes") ChipToggledListener rawListener :
        getListeners(ChipToggledListener.class)) {
      @SuppressWarnings("unchecked")
      ChipToggledListener<T> listener = (ChipToggledListener<T>) rawListener;
      listener.chipToggled(chip, newValue);
    }
  }
}
