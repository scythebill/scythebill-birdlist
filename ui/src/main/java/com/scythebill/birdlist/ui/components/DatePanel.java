/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.List;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultCaret;
import javax.swing.text.NumberFormatter;

import org.joda.time.Chronology;
import org.joda.time.DateTimeField;
import org.joda.time.DateTimeFieldType;
import org.joda.time.Partial;
import org.joda.time.ReadablePartial;
import org.joda.time.chrono.GJChronology;

import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Panel for inputting dates.  DatePanel must be used in a FontManager-managed
 * UI.
 */
public class DatePanel extends JPanel implements FontsUpdatedListener {
  private JFormattedTextField dayField;
  private JFormattedTextField yearField;
  private JComboBox<String> monthField;
  // Chronology used by DatePanel (so far)
  private final Chronology chrono = GJChronology.getInstance();
  private ReadablePartial value;
  private NumberFormatter dayFormatter;
  private Timer timer;
  
  // If true, do not send value change events
  private boolean dontUpdateValue;

  public DatePanel() {
    initGUI();
  }

  public ReadablePartial getValue() {
    return value;
  }

  @Override
  public void setFont(Font font) {
    super.setFont(font);
    if (dayField != null) {
      dayField.setFont(font);
      monthField.setFont(font);
      yearField.setFont(font);
    }
  }
  
  public void setValue(ReadablePartial newValue) {
    dontUpdateValue = true;
    if (newValue != null && newValue.size() == 0) {
      newValue = null;
    }
    
    if (newValue == null) {
      yearField.setText("");
      monthField.setSelectedIndex(0);
      dayField.setText("");
    } else {
      // Set fields year/month/day:  otherwise you're liable to fail leap days
      if (newValue.isSupported(DateTimeFieldType.year())) {
        int year = newValue.get(DateTimeFieldType.year());
        yearField.setValue(year);
      } else {
        yearField.setText("");
      }

      if (newValue.isSupported(DateTimeFieldType.monthOfYear())) {
        int month = newValue.get(DateTimeFieldType.monthOfYear());
        monthField.setSelectedIndex(month);
      } else {
        monthField.setSelectedIndex(0);
      }

      if (newValue.isSupported(DateTimeFieldType.dayOfMonth())) {
        int day = newValue.get(DateTimeFieldType.dayOfMonth());
        dayField.setValue(day);
      } else {
        dayField.setText("");
      }

    }
    
    dontUpdateValue = false;
    
    updateValue(newValue);
  }

  private void initGUI() {
    dayFormatter = new NumberFormatter(new DecimalFormat("##")) {
      @Override
      public Object stringToValue(String text) throws ParseException {
        if (Strings.isNullOrEmpty(text)) {
          return null;
        }

        return super.stringToValue(text);
      }
    };

    dayFormatter.setValueClass(Integer.class);
    dayFormatter.setMinimum(1);
    dayFormatter.setMaximum(31);

    dayField = new AutoSelectJFormattedTextField(dayFormatter);
    YearOrDayChanged dayListener = new YearOrDayChanged(dayField);
    dayField.addPropertyChangeListener("value", dayListener);
    dayField.getDocument().addDocumentListener(dayListener);
    dayField.setCaret(new DefaultCaret());
    attachAquaCaret(dayField);
    ComboBoxModel<String> monthFieldModel = 
        new DefaultComboBoxModel<String>(getMonthNames().toArray(new String[0]));
    monthField = new JComboBox<String>();
    monthField.setModel(monthFieldModel);
    monthField.setMaximumRowCount(monthFieldModel.getSize());
    monthField.addActionListener(new MonthChanged());
    monthField.setEditable(false);
    yearField = new AutoSelectJFormattedTextField(new YearFormatter());
    YearOrDayChanged yearListener = new YearOrDayChanged(yearField);
    yearField.addPropertyChangeListener("value", yearListener);
    yearField.getDocument().addDocumentListener(yearListener);
    attachAquaCaret(yearField);
    
    TextPrompt.addToField(Name.DAY_PROMPT, dayField).changeAlpha(0.5f);
    TextPrompt.addToField(Name.YEAR_PROMPT, yearField).changeAlpha(0.5f);
  }

  static private final List<String> getMonthNames() {
    List<String> months = Lists.newArrayList();
    months.add("");

    GJChronology chrono = GJChronology.getInstance();
    DateTimeField monthField = chrono.monthOfYear();
    for (int month = monthField.getMinimumValue(); month <= monthField.getMaximumValue(); month++) {
      months.add(monthField.getAsShortText(month, null));
    }

    return months;
  }

  private class MonthChanged implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      calculateNewValue();
    }
  }

  private class YearOrDayChanged implements PropertyChangeListener, DocumentListener {
    private JFormattedTextField field;

    public YearOrDayChanged(JFormattedTextField field) {
      this.field = field;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      calculateNewValue();
    }

    @Override
    public void changedUpdate(DocumentEvent event) {
      startTimerToCalculateNewValue(field);
    }

    @Override
    public void insertUpdate(DocumentEvent event) {
      startTimerToCalculateNewValue(field);
    }

    @Override
    public void removeUpdate(DocumentEvent event) {
      startTimerToCalculateNewValue(field);
    }
  }

  private void startTimerToCalculateNewValue(final JFormattedTextField field) {
    if (timer != null) {
      timer.stop();
    }
    timer = new Timer(100, new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent event) {
        try {
          field.commitEdit();
          calculateNewValue();
        } catch (ParseException e) {
          // OK
        }
      }
    });
    timer.setRepeats(false);
    timer.start();
  }

  private void calculateNewValue() {
    Partial newValue = new Partial(chrono);
    if (!Strings.isNullOrEmpty(yearField.getText())) {
      int year = (Integer) yearField.getValue();
      newValue = newValue.with(DateTimeFieldType.year(), year);
    }

    int month = monthField.getSelectedIndex();
    if (month > 0) {
      newValue = newValue.with(DateTimeFieldType.monthOfYear(), month);
    }

    int maxDay = chrono.dayOfMonth().getMaximumValue(newValue);
    dayFormatter.setMaximum(maxDay);

    if (!Strings.isNullOrEmpty(dayField.getText())) {
      int day = (Integer) dayField.getValue();
      // Clip the day if it exceeds the max value
      if (day > maxDay) {
        day = maxDay;
        dayField.setValue(day);
      }

      newValue = newValue.with(DateTimeFieldType.dayOfMonth(), day);
    }

    if (newValue.size() == 0) {
      newValue = null;
    }

    if (!dontUpdateValue) {
      updateValue(newValue);
    }
  }

  private void updateValue(ReadablePartial newValue) {
    ReadablePartial oldValue = value;
    value = newValue;
    if (!Objects.equal(oldValue, newValue)) {
      firePropertyChange("value", oldValue, newValue);
    }
  }

  // Force a formatted-field caret to the usual text field caret
  // TODO: still needed?
  private void attachAquaCaret(JFormattedTextField field) {
    JTextField tempField = new JTextField();
    field.setCaret(tempField.getCaret());    
  }

  public void setEditable(boolean editable) {
    dayField.setEditable(editable);
    // Enabled, not editable, for month - since it's a combobox
    monthField.setEnabled(editable);
    yearField.setEditable(editable);
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    GroupLayout layout = new GroupLayout(this);
    this.setLayout(layout);
    layout.setVerticalGroup(layout.createBaselineGroup(false, false)
        .addComponent(monthField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
        .addComponent(dayField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
        .addComponent(yearField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE));
    layout.setHorizontalGroup(layout.createSequentialGroup()
      .addComponent(dayField, PREFERRED_SIZE, fontManager.scale(55), PREFERRED_SIZE)
      .addPreferredGap(ComponentPlacement.RELATED)
      .addComponent(monthField, PREFERRED_SIZE, fontManager.scale(84), PREFERRED_SIZE)
      .addPreferredGap(ComponentPlacement.RELATED)
      .addComponent(yearField, PREFERRED_SIZE, fontManager.scale(70), PREFERRED_SIZE));
      
    layout.linkSize(SwingConstants.VERTICAL,  dayField, yearField, monthField);
  }
}
