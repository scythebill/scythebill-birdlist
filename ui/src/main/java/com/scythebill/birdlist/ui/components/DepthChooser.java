/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */package com.scythebill.birdlist.ui.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * JComboBox specialization for switching between species/group/subspecies.
 */
public class DepthChooser extends JComboBox<String> {
  private Type depth;

  public DepthChooser() {
    depth = Type.species;
    setModel(new DefaultComboBoxModel<>(new String[]{
        Messages.getMessage(Name.SPECIES_ONLY),
        Messages.getMessage(Name.EBIRD_GROUPS),
        Messages.getMessage(Name.SUBSPECIES_SHOWN)
    }));

    addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent e) {
        Taxon.Type depth;
        switch (getSelectedIndex()) {
          case 0:
            depth = Taxon.Type.species;
            break;
          case 1:
            depth = Taxon.Type.group;
            break;
          case 2:
            depth = Taxon.Type.subspecies;
            break;
          default:
            throw new IllegalArgumentException();
        }
        
        Type oldDepth = DepthChooser.this.depth;
        DepthChooser.this.depth = depth;
        firePropertyChange("depth", oldDepth, depth);
      }
    });
  }
  
  public Type getDepth() {
    return depth;
  }
  
  public void setDepth(Type depth) {
    switch (depth) {
      case species:
        setSelectedIndex(0);
        break;
      case group:
        setSelectedIndex(1);
        break;
      case subspecies:
        setSelectedIndex(2);
        break;
      default:
        throw new IllegalArgumentException("Unsupported type: " + depth);
    }
    
    this.depth = depth;
  }
}
