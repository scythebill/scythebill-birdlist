/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.components.table.DeleteColumn;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.ColumnLayout;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.DetailView;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.ListListModel;

/**
 * Panel supporting display of a species ExpandableTable, where all entries are explicitly added.
 */
public class DirectSpeciesTablePanel extends SpeciesTablePanel {
  private ListListModel<Resolved> speciesListModel;
  private AbstractAction deleteAction;

  /** Creates new form SpeciesListPanel */
  public DirectSpeciesTablePanel(DetailView<Resolved> expandedRowView, String detailTitle,
      FontManager fontManager) {
    super(expandedRowView, detailTitle, fontManager);
    
    init();
    hookUpContents();
  }

  private void hookUpContents() {
    deleteAction = new AbstractAction(Messages.getMessage(Name.DELETE_ACTION)) {
      @Override
      public void actionPerformed(ActionEvent event) {
        if (!isEditable()) {
          return;
        }

        Resolved resolved = getSelectedSpecies();
        if (resolved != null) {
          removeSpecies(resolved);
          notifyUserModifiedValue();
        }
      }
    };
    deleteAction.setEnabled(false);
    addPropertyChangeListener("selectedValue", new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        Resolved resolved = getSelectedSpecies();
        deleteAction.setEnabled(resolved != null);
      }
    });

    KeyStroke deleteKey = KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0);
    getSpeciesTable().getInputMap().put(deleteKey, "delete");
    getSpeciesTable().getActionMap().put("delete", deleteAction);
  }

  public Action getDeleteAction() {
    return deleteAction;
  }

  @Override
  public void addSpecies(Resolved resolved) {
    Preconditions.checkNotNull(resolved);
    TaxonUtils.sortedInsert(speciesListModel.asList(), resolved);
    fireValueChanged();
  }

  @Override
  public void removeSpecies(Resolved taxon) {
    Preconditions.checkNotNull(taxon);
    speciesListModel.asList().remove(taxon);
    fireValueChanged();
  }

  @Override
  public void swapSpecies(
      Resolved currentTaxon, Resolved newTaxon) {
    // Simplest approach:  just swap the species
    removeSpecies(currentTaxon);
    addSpecies(newTaxon);
  }

  @Override
  public void setSortedContents(Taxonomy taxonomy, List<Resolved> taxa) {
    Preconditions.checkNotNull(taxa);
    speciesListModel.asList().clear();
    speciesListModel.asList().addAll(taxa);
    fireValueChanged();
  }

  /**
   * Returns a copy of the current species list.
   */
  @Override
  public ImmutableList<Resolved> getValue() {
    return ImmutableList.copyOf(speciesListModel.asList());
  }

  @Override
  protected ListListModel<Resolved> getListModel() {
    if (speciesListModel == null) {
      speciesListModel = new ListListModel<Resolved>();
    }
    
    return speciesListModel;
  }

  public void addDeleteColumn() {
    addColumn(new DeleteColumn<Resolved>(ColumnLayout.fixedWidth(20), getSpeciesTable()) {
      @Override
      protected void deleteRow(int index) {
        speciesListModel.asList().remove(index);
        fireValueChanged();
        notifyUserModifiedValue();
      }

      @Override
      public String getName() {
        return "";
      }
    });
  }
}
