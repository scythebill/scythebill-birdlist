/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import java.awt.Color;

import javax.swing.JLabel;

import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Label that shows an error; just sets a color and toggles visibility as it becomes empty.
 */
public class ErrorLabel extends JLabel {
  public static final Color ERROR_COLOR = new Color(192, 0, 0);

  public ErrorLabel() {
    setToolTipText(Messages.getMessage(Name.ERROR_MESSAGE));
    setForeground(ERROR_COLOR);
    setVisible(false);
    
    addPropertyChangeListener("text", e -> {
      setVisible(!(e.getNewValue() == null || e.getNewValue().equals("")));
    });
  }
}
