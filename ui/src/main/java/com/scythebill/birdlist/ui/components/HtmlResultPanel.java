/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.standard.DialogTypeSelection;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.text.html.HTMLEditorKit;

import com.google.common.base.Strings;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Panel for showing HTML-formatted results within Scythebill for easy extraction.
 */
public class HtmlResultPanel extends JPanel {
  private JEditorPane editor;
  private JScrollPane scrollPane;
  private String plainText;
  private String documentName;
  private Map<DataFlavor, String> additionalFormats = new LinkedHashMap<>();

  public HtmlResultPanel(Name message, String html, FontManager fontManager) {
    editor = new JEditorPane();
    editor.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, Boolean.TRUE);
    HTMLEditorKit kit = new HTMLEditorKit();
    editor.setEditorKit(kit);
    editor.setDocument(kit.createDefaultDocument());
    editor.setText(html);

    editor.setEditable(false);
    scrollPane = new JScrollPane(editor);
    JLabel label = new JLabel("<html>" + Messages.getMessage(message));
    label.putClientProperty(FontManager.PLAIN_LABEL, true);
    
    GroupLayout layout = new GroupLayout(this);
    layout.setVerticalGroup(layout.createSequentialGroup()
        .addComponent(label)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, fontManager.scale(500), fontManager.scale(500)));
    layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
        .addComponent(label)
        .addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, fontManager.scale(600), fontManager.scale(600)));
    setLayout(layout);
    
    fontManager.applyTo(this);
  }
  
  public Action printAction() {
    if (!printIsAvailable()) {
      return null;
    }
    
    return new AbstractAction(Messages.getMessage(Name.PRINT_MENU)) {
      @Override
      public void actionPerformed(ActionEvent e) {
        print();
      }
    };
  }
  
  public void setDocumentName(String name) {
    this.documentName = name;
  }

  private boolean printIsAvailable() {
    // For the forseeable futurer, disable printIsAvailable().  While it might do
    // sort-of-kind-of-OK, results are unpredictable and not very configurable
    // (e.g. trip report tables that don't fit and get truncated, no good control
    // for font sizes, etc.), and it's likely to be far more of a support burden
    // than a useful feature.
    return false;
    // See if any print services are available;  if not, printing will fail.
//    PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null);
//    return services != null && services.length > 0;
  }
  
  private void print() {
    try {
      PrinterJob printerJob = PrinterJob.getPrinterJob();
      if (!Strings.isNullOrEmpty(documentName)) {
        printerJob.setJobName(documentName);
      }
      if (printerJob.printDialog(new HashPrintRequestAttributeSet(DialogTypeSelection.NATIVE))) {
        printerJob.setPrintable(editor.getPrintable(
            null /* headerFormat */,
            new MessageFormat("{0,number,integer}")));
        printerJob.print();
      }
    } catch (PrinterException e) {
      // Eventually, this should be a proper error, but in the short-term this seems
      // like a better 
      throw new RuntimeException(e);
    }
  }
  
  public void setPlainText(String plainText) {
    this.plainText = plainText;
  }
  
  public void copyToClipboard() {
    editor.selectAll();
    Transferable t = new HtmlAndPlainTextTransferable();
    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(t, null);
    editor.setCaretPosition(0);
  }
  
  public void selectAllWhenVisible() {
    addAncestorListener(new AncestorListener() {
      @Override
      public void ancestorRemoved(AncestorEvent event) {
      }
      
      @Override
      public void ancestorMoved(AncestorEvent event) {
      }
      
      @Override
      public void ancestorAdded(AncestorEvent event) {
        //editor.requestFocusInWindow();
        editor.selectAll();
        scrollPane.scrollRectToVisible(new Rectangle());
        editor.setCaretPosition(0);
      }
    });
  }
  
  class HtmlAndPlainTextTransferable implements Transferable {
    @Override
    public DataFlavor[] getTransferDataFlavors() {
      List<DataFlavor> flavors = new ArrayList<>();
      flavors.add(DataFlavor.allHtmlFlavor);
      if (plainText != null) {
        flavors.add(DataFlavor.stringFlavor);
      }

      flavors.addAll(additionalFormats.keySet());
      return flavors.toArray(new DataFlavor[flavors.size()]);
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
      if (flavor.equals(DataFlavor.allHtmlFlavor)) {
        return true;
      }
      
      if (plainText != null && flavor.equals(DataFlavor.stringFlavor)) {
        return true;
      }
      
      if (additionalFormats.containsKey(flavor)) {
        return true;
      }
      return false;
    }

    @Override
    public Object getTransferData(DataFlavor flavor)
        throws UnsupportedFlavorException, IOException {
      if (flavor.equals(DataFlavor.allHtmlFlavor)) {
        return editor.getText();
      }
      
      if (flavor.equals(DataFlavor.stringFlavor) && plainText != null) {
        return plainText;
      }
      
      if (additionalFormats.containsKey(flavor)) {
        return additionalFormats.get(flavor);
      }

      throw new UnsupportedFlavorException(flavor);
    }
    
  }

  public void addAdditionalFormat(DataFlavor dataFlavor, String text) {
    additionalFormats.put(dataFlavor, text);
  }
}
