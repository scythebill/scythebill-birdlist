/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.scythebill.birdlist.ui.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.annotation.Nullable;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultListCellRenderer;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLayeredPane;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.JTextComponent;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.util.AlternateName;
import com.scythebill.birdlist.model.util.Indexer;
import com.scythebill.birdlist.model.util.ToString;
import com.scythebill.birdlist.ui.components.TextPrompt.Prompt;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.util.DoubleClickListener;
import com.scythebill.birdlist.ui.util.ListListModel;

/**
 * Panel supporting the choice of a value by auto-completing against an
 * {@link Indexer}.
 */
public class IndexerPanel<T> extends JPanel implements FontsUpdatedListener {

  public enum LayoutStrategy {
    /** Show the popup below the indexer */
    BELOW,
    /** Show the popup to the right of the indexer */
    RIGHT;
  }

  public interface Mixer<T> {
    List<T> mix(List<T> original);
  }
  
  /**
   * If less than these number of values come out of the primary indexer, fall
   * back to the secondary (and then to the primary alternate, etc.)
   */
  private static final int SECONDARY_INDEXER_THRESHOLD = 5;
  private final List<ActionListener> actionListeners = Lists.newLinkedList();
  private LayoutStrategy layout = LayoutStrategy.RIGHT;
  private final Action hideAction = new AbstractAction() {
    @Override
    public void actionPerformed(ActionEvent action) {
      hidePopup();
    }
  };
  
  private short popupHeight = 150;

  private JTextField valueField;
  private JScrollPane scroller;
  private JList<Object> popupList;
  private List<IndexerGroup> indexerGroups = Lists.newArrayList();

  private Mixer<T> mixer;

  private T value;
  private T selectedValue;
  /** True if the indexer text is "dirty", and does not match the current value. */
  private boolean textIsDirty;
  private boolean dontUpdatePopup;
  private Action setValueAction;
  private boolean canShowPopupOnFocusGained = true;
  private Prompt previewPrompt;
  private boolean supportsNewValues;
  private Color defaultTextForeground;
  private Predicate<T> filter;
  private Supplier<Ordering<T>> sortOrderSupplier;
  private Supplier<Ordering<AlternateName<T>>> alternateSortOrderSupplier;
  
  private class IndexerGroup {
    ToString<T> toString;
    
    Indexer<T> indexer;
    Collection<T> matches;
    
    Indexer<AlternateName<T>> alternateIndexer;
    Collection<AlternateName<T>> alternateMatches;
  }

  /** Creates new form IndexerPanel */
  public IndexerPanel() {
    initComponents();
    valueField.getDocument().addDocumentListener(new DocumentListener() {
      @Override
      public void changedUpdate(DocumentEvent documentEvent) {
        textIsDirty = true;
        updatePopup();
      }

      @Override
      public void insertUpdate(DocumentEvent documentEvent) {
        textIsDirty = true;
        updatePopup();
      }

      @Override
      public void removeUpdate(DocumentEvent documentEvent) {
        textIsDirty = true;
        updatePopup();
      }
    });
    valueField.addFocusListener(new FocusListener() {
      @Override
      public void focusGained(FocusEvent focusEvent) {
        // If there is no value, and showing a popup on focus-gained is allowed,
        // try to show the popup.
        if (canShowPopupOnFocusGained && textIsDirty) {
           updatePopup();
         } else {
           valueField.selectAll();
         }
      }

      @Override
      public void focusLost(FocusEvent focusEvent) {
        if (focusEvent.getOppositeComponent() != popupList) {
          hidePopup();
        }
        if (textIsDirty && value == null) {
          setInvalidText(true);
        }
      }
    });

    // Make sure the popup is hidden if the indexer panel is removed
    addAncestorListener(new AncestorListener() {
      @Override
      public void ancestorAdded(AncestorEvent e) {
      }

      @Override
      public void ancestorMoved(AncestorEvent e) {
      }

      @Override
      public void ancestorRemoved(AncestorEvent e) {
        removePopup();
      }
    });

    // On ENTER, select the current value
    KeyStroke enterKey = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);
    setValueAction = new AbstractAction("setValue") {
      @Override
      public void actionPerformed(ActionEvent event) {
        if ((popupList == null) || !popupList.isShowing()) {
          if (IndexerPanel.this.getValue() != null) {
            fireActionPerformed();
          }
          return;
        }

        int selectedIndex = popupList.getSelectedIndex();
        setValueFromListIndex(selectedIndex);
      }
    };
    valueField.getInputMap().put(enterKey, "setValue");
    valueField.getActionMap().put("setValue", setValueAction);

    // Is this the correct way to redirect key strokes???
    valueField.addKeyListener(new KeyListener() {
      @Override
      public void keyPressed(KeyEvent keyEvent) {
        if ((popupList == null) || !popupList.isShowing())
          return;

        if ((keyEvent.getKeyCode() == KeyEvent.VK_DOWN)
            || (keyEvent.getKeyCode() == KeyEvent.VK_UP)) {
          keyEvent.setSource(popupList);
          popupList.dispatchEvent(keyEvent);
          keyEvent.consume();
        }
      }

      @Override
      public void keyReleased(KeyEvent keyEvent) {
        keyPressed(keyEvent);
      }

      @Override
      public void keyTyped(KeyEvent keyEvent) {
        keyPressed(keyEvent);
      }
    });

    KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    getInputMap(WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(escape, "hide");
    getActionMap().put("hide", hideAction);
  }

  @Override
  public void setEnabled(boolean enabled) {
    super.setEnabled(enabled);
    valueField.setEditable(enabled);
    valueField.setEnabled(enabled);
    if (!enabled) {
      hidePopup();
    }
  }
  
  public boolean getSupportsNewValues() {
    return supportsNewValues;
  }
  
  /**
   * If true, the IndexerPanel can be used to enter new, invalid values.
   */
  public void setSupportsNewValues(boolean supportsNewValues) {
    this.supportsNewValues = supportsNewValues;
  }

  public void addActionListener(ActionListener listener) {
    actionListeners.add(listener);
  }

  public void removeActionListener(ActionListener listener) {
    actionListeners.remove(listener);
  }
  
  public boolean isPopupVisible() {
    return scroller != null && scroller.isVisible();
  }

  private void fireActionPerformed() {
    ActionEvent event = new ActionEvent(this, ActionEvent.ACTION_PERFORMED,
        null);
    for (ActionListener listener : actionListeners) {
      listener.actionPerformed(event);
    }
  }

  public String getTextValue() {
    return valueField.getText();
  }


  public void setTextValue(String text) {
    valueField.setText(text);
  }

  public T getValue() {
    return value;
  }

  /**
   * Set the current value of the indexer.
   */
  public void setValue(T value) {
    internalSetValue(value);
    // Reset the text in the value field, but ignore the value change events
    dontUpdatePopup = true;
    String text = null;
    for (IndexerGroup indexerGroup : indexerGroups) {
      text = indexerGroup.toString.getString(value);
      if (text != null) {
        break;
      }
    }
    
    valueField.setText(text);
    valueField.select(0, 0);
    dontUpdatePopup = false;
    textIsDirty = false;
    setInvalidText(false);
  }

  public T getSelectedValue() {
    return selectedValue;
  }

  private void internalSetValue(T value) {
    T oldValue = this.value;
    this.value = value;
    if (!Objects.equal(oldValue, value)) {
      firePropertyChange("value", oldValue, value);
    }
  }

  public void clearValue() {
    internalSetValue(null);
    valueField.setText("");
    textIsDirty = false;
  }

  public int getColumns() {
    return valueField.getColumns();
  }

  public void setColumns(int columns) {
    valueField.setColumns(columns);
  }

  /** Hide the popup */
  private void hidePopup() {
    if (scroller != null) {
      scroller.setVisible(false);
      setSelectedValue(null);
      firePropertyChange("popupVisible", true, false);
    }
  }

  /** Remove the popup */
  private void removePopup() {
    if (scroller != null) {
      // Grab the bounds of the scroller, to force a repaint.
      // This helps with some visual artifacts, though it doesn't
      // fully solve the problem.
      JRootPane rootPane = SwingUtilities.getRootPane(scroller);
      Rectangle bounds = SwingUtilities.convertRectangle(
          scroller,
          scroller.getBounds(),
          rootPane);
          
      scroller.getParent().remove(scroller);
      scroller = null;
      popupList = null;
      rootPane.repaint(bounds);
    }
  }

  /** Update the popup with the current results of the search */
  private void updatePopup() {
    // If the field is in the middle of being manually updated, don't attempt
    // to update and show the popup
    if (dontUpdatePopup) {
      return;
    }
    
    // http://code.google.com/p/scythebill-birdlist/issues/detail?id=35
    // Don't attempt updates once the panel is hidden.
    if (!isShowing()) {
      return;
    }

    String valueFromField = valueField.getText();
    if (valueFromField == null) {
      return;
    }
    valueFromField = valueFromField.trim();

    final Set<T> foundMatches = Sets.newLinkedHashSet();
    Predicate<AlternateName<T>> alternateNotFoundYet =
        input -> !foundMatches.contains(input.getObject());
    
    // Clear out any existing state
    for (IndexerGroup indexerGroup : indexerGroups) {
      indexerGroup.matches = null;
      indexerGroup.alternateMatches = null;
    }
    
    for (IndexerGroup indexerGroup : indexerGroups) {
      if (indexerGroup.indexer != null) {
        // Search
        Collection<T> found = filter(indexerGroup.indexer.find(valueFromField));
        // Sort if available
        if (sortOrderSupplier != null) {
          found = sortOrderSupplier.get().sortedCopy(found);
        }
        // Remove everything that was already found
        found.removeAll(foundMatches);
        // Now remember, and add to the full list
        indexerGroup.matches = found;
        foundMatches.addAll(found);
      } else {
        // Alternate name index
        Preconditions.checkState(indexerGroup.alternateIndexer != null);
        Collection<AlternateName<T>> alternateFound =
            filterAlternateNames(indexerGroup.alternateIndexer.find(valueFromField));
        // Remove everything that was already found, then sort
        if (alternateSortOrderSupplier != null) {
          alternateFound =
              alternateSortOrderSupplier.get().sortedCopy(Iterables.filter(alternateFound, alternateNotFoundYet));
        } else {
          alternateFound = ImmutableList.copyOf(Iterables.filter(alternateFound, alternateNotFoundYet));
        }
        indexerGroup.alternateMatches = alternateFound;
        for (AlternateName<T> alternateMatch : alternateFound) {
          foundMatches.add(alternateMatch.getObject());
        }
      }
      
      // Stop once we've found enough
      if (foundMatches.size() > SECONDARY_INDEXER_THRESHOLD) {
        break;
      }
    }
        
    // Don't bother showing the popup if there's one match, which
    // is just the current value
    if (foundMatches.size() == 1
        && Objects.equal(value, foundMatches.iterator().next())
        && !isPopupVisible()) {
      return;
    }

    Set<Object> results = new LinkedHashSet<>();
    if (mixer != null) {
      boolean mixedYet = false;
      for (IndexerGroup indexerGroup : indexerGroups) {
        if (indexerGroup.indexer != null) {
          if (indexerGroup.matches != null) {
            // Apply the mixer to the first group encountered
            if (!mixedYet && !indexerGroup.matches.isEmpty()) {
              List<T> mixed = mixer.mix(ImmutableList.copyOf(indexerGroup.matches));
              indexerGroup.matches = mixed;
            }
            indexerGroup.matches = addToResults(results, indexerGroup.matches);
            // And note if mixing happened successfully
            if (!mixedYet && !indexerGroup.matches.isEmpty()) {
              mixedYet = true;
            }
          }
        } else {
          if (indexerGroup.alternateMatches != null) {
            indexerGroup.alternateMatches = addToResults(results, indexerGroup.alternateMatches);
          }
        }        
      }
    } else {
      // No mixer:  just add them all in sequence
      for (IndexerGroup indexerGroup : indexerGroups) {
        if (indexerGroup.indexer != null) {
          if (indexerGroup.matches != null) {
            indexerGroup.matches = addToResults(results, indexerGroup.matches);
          }
        } else {
          if (indexerGroup.alternateMatches != null) {
            indexerGroup.alternateMatches = addToResults(results, indexerGroup.alternateMatches);
          }
        }
      }
    }
    
    ListListModel<Object> resultsModel = new ListListModel<Object>(new ArrayList<Object>(results));
    if (popupList == null) {
      createPopup();
    }

    popupList.setVisibleRowCount(Math.min(10, resultsModel.getSize()));
    popupList.setModel(resultsModel);
    if (resultsModel.getSize() == 0) {
      scroller.setVisible(false);
      // Set the value to null without updating the text (illegal,
      // because we're perhaps in a mutation event, and we don't want
      // to set the text to "" anyway)
      internalSetValue(null);
      firePropertyChange("popupVisible", true, false);
      setInvalidText(!valueFromField.isEmpty());
    } else {
      setInvalidText(false);
      popupList.setSelectedIndex(0);
      popupList.scrollRectToVisible(popupList.getCellBounds(0, 0));
      showPopup();
    }
  }
  
  private Collection<AlternateName<T>> filterAlternateNames(Collection<AlternateName<T>> found) {
    if (filter == null) {
      return found;
    }
    
    return found.stream().filter(alternate -> filter.apply(alternate.getObject()))
        .collect(Collectors.toCollection(LinkedHashSet::new));
  }

  private Collection<T> filter(Collection<T> found) {
    if (filter == null) {
      return found;
    }
    
    return found.stream().filter(filter)
        .collect(Collectors.toCollection(LinkedHashSet::new));
  }

  /**
   * Adds the "source" collection to the "results" collection, and returns a collection
   * (with the same order) of only those elements that were succesfully added to the results.
   */
  private <U> Collection<U> addToResults(Collection<Object> results, Collection<U> source) {
    List<U> outResults = Lists.newArrayListWithExpectedSize(source.size());
    for (U sourceElement : source) {
      if (results.add(sourceElement)) {
        outResults.add(sourceElement);
      }
    }
    return outResults;
  }
  
  private void setInvalidText(boolean invalid) {
    // Don't indicate "invalid" text.
    if (supportsNewValues) {
      return;
    }
    
    if (invalid) {
      valueField.setForeground(RequiredLabel.REQUIRED_COLOR);
    } else {
      valueField.setForeground(defaultTextForeground);
    }
  }
  /**
   * Position and size the popup, and show it.
   */
  private void showPopup() {
    layoutPopup();
    scroller.setVisible(true);
    firePropertyChange("popupVisible", false, true);
  }

  private void layoutPopup() {
    JLayeredPane layered = getRootPane().getLayeredPane();
    Rectangle fieldBounds = SwingUtilities.convertRectangle(this, valueField
        .getBounds(), layered);

    Dimension size = scroller.getPreferredSize();

    scroller.setSize(Math.max(size.width, valueField.getWidth()), Math.min(
        size.height, getPopupHeight()));

    switch (getLayoutStrategy()) {
      case BELOW:
        scroller.setLocation(fieldBounds.x, fieldBounds.y + fieldBounds.height);
        break;
      case RIGHT:
        scroller.setLocation(fieldBounds.x + fieldBounds.width, fieldBounds.y);
        break;
    }
  }

  /** Creates the popup */
  private void createPopup() {
    popupList = new JList<Object>();
    popupList.setFont(valueField.getFont());
    popupList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    popupList.setCellRenderer(new ToStringListCellRenderer());
    popupList.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent event) {
        T castValue = listValueFromEntry(popupList.getSelectedValue());
        setSelectedValue(castValue);
      }
    });

    popupList.addFocusListener(new FocusListener() {
      @Override
      public void focusGained(FocusEvent focusEvent) {
      }

      @Override
      public void focusLost(FocusEvent focusEvent) {
        if (focusEvent.getOppositeComponent() != valueField)
          hidePopup();
      }
    });

    popupList.addMouseListener(new DoubleClickListener() {
      @Override
      protected void doubleClicked(MouseEvent event) {
        int index = popupList.locationToIndex(event.getPoint());
        setValueFromListIndex(index);
      }
    });

    JLayeredPane layered = getRootPane().getLayeredPane();
    scroller = new JScrollPane();
    scroller
        .setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    scroller.setViewportView(popupList);
    layered.add(scroller, JLayeredPane.POPUP_LAYER);

    KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    popupList.getInputMap(WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(escape,
        "hide");
    popupList.getActionMap().put("hide", hideAction);

    KeyStroke enter = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);
    popupList.getInputMap(WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(enter,
        "setValue");
    popupList.getActionMap().put("setValue", setValueAction);
  }

  private void setSelectedValue(T newSelectedValue) {
    T oldSelectedValue = selectedValue;
    selectedValue = newSelectedValue;
    firePropertyChange("selectedValue", oldSelectedValue, newSelectedValue);
  }

  public void addIndexerGroup(
      ToString<T> toString, Indexer<T> indexer) {
    IndexerGroup indexerGroup = new IndexerGroup();
    indexerGroup.toString = toString;
    indexerGroup.indexer = indexer;
    indexerGroups.add(indexerGroup);
  }
  
  public void addAlternateIndexerGroup(
      ToString<T> toString, Indexer<AlternateName<T>> indexer) {
    IndexerGroup indexerGroup = new IndexerGroup();
    indexerGroup.toString = toString;
    indexerGroup.alternateIndexer = indexer;
    indexerGroups.add(indexerGroup);
  }

  public LayoutStrategy getLayoutStrategy() {
    return layout;
  }

  public void setLayoutStrategy(LayoutStrategy layout) {
    this.layout = layout;
  }

  /**
   * Attach an ordering supplier to the panel.
   * 
   * <p>A Supplier is used to support Orderings that change over time (in particular, Orderings
   * generated by a Scorer instance).
   */
  public void setOrdering(Supplier<Ordering<T>> sortOrderSupplier) {
    this.sortOrderSupplier = sortOrderSupplier;
    if (sortOrderSupplier == null) {
      alternateSortOrderSupplier = null;
    } else {
      alternateSortOrderSupplier = new AlternateSortOrderSupplier<>(sortOrderSupplier);
    }
  }

  static class AlternateSortOrderSupplier<T>  implements Supplier<Ordering<AlternateName<T>>> {
    private final Supplier<Ordering<T>> supplier;
    AlternateSortOrderSupplier(Supplier<Ordering<T>> supplier) {
      this.supplier = supplier;
    }
    @Override
    public Ordering<AlternateName<T>> get() {
      Ordering<T> ordering = supplier.get();
      return new Ordering<AlternateName<T>>() {
        @Override public int compare(@Nullable AlternateName<T> left, @Nullable AlternateName<T> right) {
          T leftValue = left == null ? null : left.getObject();
          T rightValue = right == null ? null : right.getObject();
          return ordering.compare(leftValue, rightValue);
        }
      };
    }
    
  }
  
  public Supplier<Ordering<T>> getOrdering() {
    return sortOrderSupplier;
  }

  public Predicate<T> getFilter() {
    return filter;
  }
  
  /**
   * Attaches a filter which limits the set of options that can be displayed in the indexer.  Set to null
   * to disable (a predicate which always returns true behaves equivalently but is slower).
   */
  public void setFilter(Predicate<T> filter) {
    this.filter = filter;
  }
  
  private class ToStringListCellRenderer extends DefaultListCellRenderer {
    private static final long serialVersionUID = 1L;

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object cellValue,
        int index, boolean isSelected, boolean cellHasFocus) {
      int sizeSoFar = 0;
      for (IndexerGroup indexerGroup : indexerGroups) {
        int groupSize = indexerGroup.matches != null
            ? indexerGroup.matches == null ? 0 : indexerGroup.matches.size()
            : indexerGroup.alternateMatches == null ? 0 : indexerGroup.alternateMatches.size();
        if (index < sizeSoFar + groupSize) {
          if (indexerGroup.indexer != null) {
            @SuppressWarnings("unchecked")
            // The non-alternate matches are all of type T
            T castValue = (T) cellValue;
            cellValue = indexerGroup.toString.getPreviewString(castValue);
            break;
          } else {
            @SuppressWarnings("unchecked")
            // The alternates are all AlternateName<T>'s
            AlternateName<T> castValue = (AlternateName<T>) cellValue;
            cellValue = String.format("%s (%s)",
                indexerGroup.toString.getPreviewString(castValue.getObject()), castValue.getName());
            break;
          }
        } else {
          sizeSoFar += groupSize;
        }
      }
      
      return super.getListCellRendererComponent(list, cellValue, index,
          isSelected, cellHasFocus);
    }
  }

  private void initComponents() {
    valueField = new JTextField();
    valueField.setColumns(20);
    // Remove.   See https://bitbucket.org/scythebill/scythebill-birdlist/issues/455,
    // and specifically that when we set TextPrompt, it blows away the "search" aspect,
    // and that TextPrompt is basically more what I want than the rest of search.
    // valueField.putClientProperty("JTextField.variant", "search");
    defaultTextForeground = valueField.getForeground();
    add(valueField);
    
    GroupLayout layout = new GroupLayout(this);
    add(valueField);
    this.setLayout(layout); 
    layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
        .addComponent(valueField, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE));   
    layout.setVerticalGroup(layout.createSequentialGroup()
        .addComponent(valueField, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE));   
  }

  @Override
  public boolean requestFocusInWindow() {
    return valueField.requestFocusInWindow();
  }

  public short getPopupHeight() {
    return popupHeight;
  }

  public void setPopupHeight(short popupHeight) {
    this.popupHeight = popupHeight;
  }
  
  public void setPreviewText(Messages.Name name) {
    if (previewPrompt == null) {
      previewPrompt = TextPrompt.addToField(name, valueField).changeAlpha(0.5f);
    } else {
      previewPrompt.setText(name);
    }
  }
  
  @Deprecated
  public void setPreviewText(String previewText) {
    if (previewPrompt == null) {
      previewPrompt = TextPrompt.addToField(previewText, valueField).changeAlpha(0.5f);
    } else {
      previewPrompt.setText(previewText);
    }
  }

  public String getPreviewText() {
    return previewPrompt == null ? null : previewPrompt.getText();
  }

  public JTextComponent getTextComponent() {
    return valueField;
  }

  private void setValueFromListIndex(int selectedIndex) {
    if (selectedIndex >= 0) {
      T newValue = listValueFromEntry(popupList.getModel().getElementAt(selectedIndex));
      setValue(newValue);
      valueField.selectAll();
    } else {
      setValue(null);
      valueField.setText("");
    }

    hidePopup();
  }

  @SuppressWarnings("unchecked")
  private T listValueFromEntry(Object value) {
    T castValue;        
    if (value instanceof AlternateName) {
      castValue = ((AlternateName<T>) value).getObject();
    } else {
      castValue = (T) value;
    }
    return castValue;
  }

  public void setEditable(boolean editable) {
    valueField.setEditable(editable);
  }
  
  public boolean getCanShowPopupOnFocusGained() {
    return canShowPopupOnFocusGained;
  }

  /**
   * When set to true, the Indexer may show a popup merely from focusing in.
   * This defaults to true, but can cause problems in cases where entering unknown values
   * is allowed (esp. locations).
   */
  public void setCanShowPopupOnFocusGained(boolean canShowPopupOnFocusGained) {
    this.canShowPopupOnFocusGained = canShowPopupOnFocusGained;
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    if (scroller != null && scroller.isVisible()) {
      // Update the font explicitly, 
      popupList.setFont(fontManager.getTextFont());
      // ... and relayout the popup (later, so that the content has received layout)
      SwingUtilities.invokeLater(this::layoutPopup);
    } else {
      // If it isn't visible, it's easiest to just blow away the 
      // popup so it'll be recreated correctly.
      removePopup();

    }
  }

  public Mixer<T> getMixer() {
    return mixer;
  }

  public void setMixer(Mixer<T> mixer) {
    this.mixer = mixer;
  }

  @Override
  public int getBaseline(int width, int height) {
    return valueField.getBaseline(width, height);
  }
  
  public void removeAllIndexerGroups() {
    indexerGroups.clear();
  }
}
