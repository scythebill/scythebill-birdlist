/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import javax.annotation.Nullable;
import javax.swing.Action;
import javax.swing.JLabel;

import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Simple panel for displaying a title, a message and an OK button.
 */
public class MessagePanel extends OkCancelPanel {
  public MessagePanel(
      Action okAction,
      @Nullable Action cancelAction,
      Alerts alerts,
      Messages.Name title,
      Messages.Name message,
      Object... args) {
   super(okAction,
       cancelAction,
       new JLabel(alerts.getFormattedDialogMessage(title, message, args)),
       null);    
   UIUtils.focusOnEntry(this, okButton);
 }
}
