/*
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.scythebill.birdlist.ui.components;

import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JDialog;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;

/** Dialog for triggering new location entry. */
public class NewLocationDialog {
  public interface LocationReceiver {
    void locationAvailable(Location location);
  }

  private final Provider<NewLocationPanel> panelProvider;
  private final FontManager fontManager;
  private final Alerts alerts;

  @Inject
  public NewLocationDialog(Provider<NewLocationPanel> panelProvider, Alerts alerts,
      FontManager fontManager) {
    this.panelProvider = panelProvider;
    this.alerts = alerts;
    this.fontManager = fontManager;
  }

  /**
   * Request a new location with a specified name.
   * <p>
   * LocationReceiver will be called when the location is available (if it is). The Location has
   * *not* been added to its parent as of this point.
   */
  public void newLocation(String name, Window parent, LocationReceiver newLocation) {
    NewLocationPanel newLocationPanel = panelProvider.get();
    newLocationPanel.setLocationName(name);

    showNewLocationDialog(parent, newLocation, newLocationPanel);
  }


  /**
   * Request a new location with a specified parent location.
   * <p>
   * LocationReceiver will be called when the location is available (if it is). The Location has
   * *not* been added to its parent as of this point.
   */
  public void newLocation(Location location, Window windowAncestor, LocationReceiver newLocation) {
    NewLocationPanel newLocationPanel = panelProvider.get();
    newLocationPanel.setLocationName("");
    newLocationPanel.setParentLocation(location);

    showNewLocationDialog(windowAncestor, newLocation, newLocationPanel);
  }

  /**
   * Edit an existing location. Returns a new location that can be substituted.
   * <p>
   * LocationReceiver will be called when the location is available (if it is). The Location has
   * *not* been added to its parent as of this point.
   */
  public void editLocation(Location existingLocation, Window windowAncestor,
      LocationReceiver newLocation) {
    NewLocationPanel newLocationPanel = panelProvider.get();
    newLocationPanel.setExistingLocation(existingLocation);

    showNewLocationDialog(windowAncestor, newLocation, newLocationPanel);
  }

  private void showNewLocationDialog(Window parent, final LocationReceiver newLocation,
      final NewLocationPanel newLocationPanel) {
    ModalityType modality =
        Toolkit.getDefaultToolkit().isModalityTypeSupported(ModalityType.DOCUMENT_MODAL)
            ? ModalityType.DOCUMENT_MODAL
            : ModalityType.APPLICATION_MODAL;
    final JDialog dialog = new JDialog(parent, Messages.getMessage(Name.NEW_LOCATION), modality);
    Action cancelAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        dialog.dispose();
      }
    };
    final Action okAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent event) {
        Location location = newLocationPanel.getValueAfterValidating();
        if (location != null) {
          newLocation.locationAvailable(location);
          dialog.dispose();
        } else if (newLocationPanel.getValue() == null) {
          alerts.showError(newLocationPanel, Name.COULDNT_CREATE_LOCATION_TITLE,
              Name.COULDNT_CREATE_LOCATION_MESSAGE);

        }
      }
    };
    newLocationPanel.addPropertyChangeListener("complete", new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent event) {
        okAction.setEnabled(newLocationPanel.isComplete());
      }
    });

    OkCancelPanel okCancel =
        new OkCancelPanel(okAction, cancelAction, newLocationPanel, newLocationPanel);
    okCancel.setPreferredSize(fontManager.scale(new Dimension(900, 600)));
    dialog.setContentPane(okCancel);
    fontManager.applyTo(dialog);
    dialog.pack();
    dialog.setVisible(true);
  }
}
