/*   
 * Copyright 2012 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.base.Predicates;
import com.google.common.base.Strings;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.io.HtmlResponseWriter;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;
import com.scythebill.birdlist.ui.actions.locationapi.GeocoderResults;
import com.scythebill.birdlist.ui.actions.locationapi.ReverseGeocoder;
import com.scythebill.birdlist.ui.actions.locationapi.combined.CombinedGeocoder;
import com.scythebill.birdlist.ui.actions.locationapi.combined.CombinedReverseGeocoder;
import com.scythebill.birdlist.ui.components.table.DeleteColumn;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.location.LocationPreviewPanel;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FullWidthCombboxPopups;
import com.scythebill.birdlist.ui.util.LocationIdToString;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Panel to add a new location (or new hierarchy of locations).
 * <p>
 * ... and occasionally to edit an existing location.
 */
public class NewLocationPanel extends JPanel implements FontsUpdatedListener {
  private static final int LOCATION_PREVIEW_DELAY_MILLIS = 500;
  private static final int REVERSE_GEOCODE_DELAY_MILLIS = 300;
  private static final int GEOCODE_DELAY_MILLIS = 500;
  
  private static final Logger logger = Logger.getLogger(NewLocationPanel.class.getName());
  private static final ImmutableBiMap<Integer, Location.Type> TYPE_MAP =
      ImmutableBiMap.<Integer, Location.Type>builder()
      .put(1, Location.Type.park)
      .put(2, Location.Type.city)
      .put(3, Location.Type.town)
      .put(4, Location.Type.county)
      .put(5, Location.Type.state)
      .put(6, Location.Type.country)
      .build();
  
  // intentionally non-static, to support reloading with a different locale
  private final List<String> types;
  {
    ImmutableList.Builder<String> builder = ImmutableList.builder();
    builder.add(Messages.getMessage(Name.LOCATION_TYPE_NO_TYPE));
    for (Location.Type type : TYPE_MAP.values()) {
      builder.add(Messages.getText(type));
    }
    types = builder.build();
  }

  private JComboBox<String> ofTypeChoice;
  private IndexerPanel<Object> inIndexer;
  private RequiredLabel inIsRequired = new RequiredLabel();
  private JLabel summaryLabel;
  private final LocationSet locations;
  private final PredefinedLocations predefinedLocations;
  private boolean complete;
  private Location lastLocation = null;
  private String name;
  private final Alerts alerts;
  private final DocumentListener updateComplete = new DocumentListener() {
    @Override
    public void changedUpdate(DocumentEvent e) {
      updateCompleteAndValue();
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
      updateCompleteAndValue();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
      updateCompleteAndValue();
    }
  };
  private JPanel moreSpecificPanel;
  private TextLink lessSpecificRow;
  private TextLink moreSpecificRow;
  private final FontManager fontManager;
  private JPanel inBox;
  private WherePanelIndexers whereIndexers;
  private JComponent focusComponent;
  private JLabel inHelp;
  private JLabel latitudeLabel;
  private JLabel longitudeLabel;
  private JFormattedTextField latitudeField;
  private JFormattedTextField longitudeField;
  private JLabel ofTypeLabel;
  private final CombinedGeocoder geocoder;
  private final ReverseGeocoder reverseGeocoder;
  private final LocationPreviewPanel locationPreviewPanel;
  private JLabel descriptionLabel;
  private JScrollPane descriptionScrollPane;
  private JTextArea description;
  private JCheckBox privateLocation;
  
  /** The explicitly geocoded latlong.  This can freely be overwritten. */ 
  private LatLongCoordinates geocodedLatLong;
  /** An overridden, geocoded parent. */
  private Location geocodedParent;
  private Timer geocodeTimer;
  private ProgressSpinner geocodeSpinner;
  private TextLink geocodeErrors;
  private JButton ignoreGeocode;
  private JButton searchNear;
  private List<LatLongCoordinates> geocodeResultsToIgnore = Lists.newArrayList();
  
  private final PropertyChangeListener latLongChanged = new PropertyChangeListener() {
    @Override public void propertyChange(PropertyChangeEvent event) {
      // Component.java has a silly bug where "changing" the value from null to null
      // doesn't suppress the event.
      if (event.getNewValue() == null && event.getOldValue() == null) {
        return;
      }
      // If manually edited, hide the geocode
      ignoreGeocode.setVisible(false);
      updateCompleteAndValue();
    }
  };
  private Timer locationPreviewTimer;
  private JTextField nameField;
  private Location valueWithoutGeocoding;
  private JComboBox<UIGeocoderResults> multipleGeocodeResults;
  private JLabel multipleGeocodeResultsLabel;
  private Location existingLocationBeingEdited;
  private Timer reverseGeocodeTimer;
  private boolean latLongExplicitlyEntered;
  private ListenableFuture<?> currentGeocodeFuture;

  @Inject
  public NewLocationPanel(
      LocationSet locations,
      PredefinedLocations predefinedLocations,
      FontManager fontManager,
      Alerts alerts,
      CombinedGeocoder geocoder,
      CombinedReverseGeocoder reverseGeocoder,
      LocationPreviewPanel locationPreviewPanel) {
    super();
    this.locations = locations;
    this.predefinedLocations = predefinedLocations;
    this.fontManager = fontManager;
    this.alerts = alerts;
    this.geocoder = geocoder;
    this.reverseGeocoder = reverseGeocoder;
    this.locationPreviewPanel = locationPreviewPanel;
    initGUI();
    hookUpContents();
  }
  
  public String getLocationName() {
    return name;
  }

  public void setLocationName(String initialName) {
    nameField.setText(initialName);
    name = initialName;
    moreSpecificPanel.removeAll();
    moreSpecificRow.setVisible(false);
    lessSpecificRow.setVisible(false);
    inIndexer.clearValue();
    updateCompleteAndValue();
  }
  
  @Override
  public void setEnabled(boolean enabled) {
    super.setEnabled(enabled);
    nameField.setEnabled(enabled);
    inIndexer.setEnabled(enabled);
    moreSpecificRow.setEnabled(enabled);
    lessSpecificRow.setEnabled(enabled);
    latitudeField.setEnabled(enabled);
    latitudeLabel.setEnabled(enabled);
    longitudeField.setEnabled(enabled);
    longitudeLabel.setEnabled(enabled);
    multipleGeocodeResults.setEnabled(enabled);
    multipleGeocodeResultsLabel.setEnabled(enabled);
    inHelp.setEnabled(enabled);
    ofTypeLabel.setEnabled(enabled);
    ofTypeChoice.setEnabled(enabled);
    descriptionLabel.setEnabled(enabled);
    description.setEnabled(enabled);
    privateLocation.setEnabled(enabled);
    summaryLabel.setEnabled(enabled);
    for (Component component : moreSpecificPanel.getComponents()) {
      component.setEnabled(enabled);
    }
  }
  
  public void setLocationLatLong(@Nullable LatLongCoordinates latLong) {
    latitudeField.removePropertyChangeListener("value", latLongChanged);
    longitudeField.removePropertyChangeListener("value", latLongChanged);
    if (latLong == null) {
      latitudeField.setText("");
      longitudeField.setText("");
    } else {
      latitudeField.setValue(latLong.latitudeAsDouble());
      longitudeField.setValue(latLong.longitudeAsDouble());
    }
    latitudeField.addPropertyChangeListener("value", latLongChanged);
    longitudeField.addPropertyChangeListener("value", latLongChanged);
    latLongExplicitlyEntered = latLong != null;
    updateCompleteAndValue();
  }

  /**
   * Attach an explicit parent location.  When set, the parent location cannot 
   * be edited (but "more detail" can be added).  Useful for directly adding a location
   * to an existing parent.
   */
  public void setParentLocation(Location location) {
    if (location == null) {
      inIndexer.setTextValue("");
      inIndexer.setEditable(true);
      focusComponent = inIndexer;
    } else {
      inIndexer.setValue(location.getId());
      inIndexer.setEditable(false);
      focusComponent = nameField;
    }
  }

  public void setGeocodedParentLocation(Location location) {
    setParentLocation(location);
    geocodedParent = location;
  }

  /**
   * Set for editing an existing location.  All fields will be populated, and detail
   * cannot be added.  This also disabled the "preferred parent" geocoding, unfortunately.
   */
  public void setExistingLocation(Location location) {
    existingLocationBeingEdited = location;
    
    setLocationName(location.getModelName());
    setParentLocation(location.getParent());
    latitudeField.setText(location.getLatLong().isPresent()
        ? location.getLatLong().get().latitude() : "");
    longitudeField.setText(location.getLatLong().isPresent()
        ? location.getLatLong().get().longitude() : "");
    description.setText(location.getDescription());
    privateLocation.setSelected(location.isPrivate());
    if (location.getType() == null) {
      ofTypeChoice.setSelectedIndex(0);
    } else {
      Integer index = TYPE_MAP.inverse().get(location.getType());
      if (index != null) {
        ofTypeChoice.setSelectedIndex(index);
      } else {
        logger.severe("Editing of location type " + location.getType() + " not supported");
      }
    }
    
    // ... and hide the more-specific/less-specific rows
    moreSpecificRow.setEnabled(false);
    moreSpecificRow.setVisible(false);
    lessSpecificRow.setEnabled(false);
    lessSpecificRow.setVisible(false);
  }

  /**
   * Force a recalculation of the "where" indexer.  Generally not relevant, but if this same
   * instance is reused while locations are being added, important. 
   */
  public void reindexLocations() {
    whereIndexers.reindex(/*addNullName=*/false, Predicates.alwaysTrue());
    whereIndexers.configureIndexer(inIndexer);
  }
  
  private void hookUpContents() {
    whereIndexers = new WherePanelIndexers(
        locations, predefinedLocations, /*addNullName=*/false, null, Predicates.alwaysTrue(),
        /* syntheticLocations= */ null);
    whereIndexers.configureIndexer(inIndexer);
    
    TextPrompt.addToField(Name.CHOOSE_A_LOCATION_NAME, nameField).changeAlpha(0.5f);
    nameField.getDocument().addDocumentListener(new DocumentListener() {
      @Override public void removeUpdate(DocumentEvent e) {
        // Delay invocation until later - otherwise, setting the text, which
        // may send remove (with the value as empty string) followed by update,
        // goes through an intermediate phase with a null location, which resets
        // some of the UI (or, more specifically, the subsequent transition from null
        // back to non-null - the UI handles null->non-null differently than
        // non-null to a different non-null).
        SwingUtilities.invokeLater(this::updateValue);
      }
      
      @Override public void insertUpdate(DocumentEvent e) {
        SwingUtilities.invokeLater(this::updateValue);
      }
      
      @Override public void changedUpdate(DocumentEvent e) {
        SwingUtilities.invokeLater(this::updateValue);
      }
      
      private void updateValue() {
        name = CharMatcher.whitespace().trimFrom(nameField.getText());
        nameField.setColumns(Math.min(30,
                Math.max(15, (name.length() / 5) * 5 + 5)));
        revalidate();
        updateCompleteAndValue();
      }
    });

    inIndexer.addPropertyChangeListener("value", new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent e) {
        Location location = WherePanelIndexers.toLocation(locations, e.getNewValue());
        if (location != null && !Objects.equal(location, geocodedParent)) {
          // Erase the geocoded parent;  it's no longer relevant
          geocodedParent = null;
          // Hide the multi-geocoder region, ditto
          multipleGeocodeResults.setVisible(false);
          multipleGeocodeResultsLabel.setVisible(false);
          updateCompleteAndValue();
          updateInHelp(location);
        }
        
        moreSpecificRow.setVisible(location != null);
      }
    });
    
    ofTypeChoice.addActionListener(e ->
        updateValue(
            false /* load image on a timer */,
            true /* re-geocode */));
    
    moreSpecificRow.addActionListener(e -> {
      // Erase the geocoded parent;  it's no longer relevant
      geocodedParent = null;
      
      moreSpecificPanel.add(new NewLocationRow());
      lessSpecificRow.setVisible(true);
      revalidate();
      updateCompleteAndValue();
    });

    lessSpecificRow.addActionListener(e -> {
      // Erase the geocoded parent;  it's no longer relevant
      geocodedParent = null;
      
      moreSpecificPanel.remove(moreSpecificPanel.getComponentCount() - 1);
      if (moreSpecificPanel.getComponentCount() == 0) {
        lessSpecificRow.setVisible(false);
      }
      revalidate();
      updateCompleteAndValue();
    });
    
    description.getDocument().addDocumentListener(new DocumentListener() {
      @Override public void removeUpdate(DocumentEvent event) {
        updateValue(false, false);
      }
      
      @Override public void insertUpdate(DocumentEvent event) {
        updateValue(false, false);
      }
      
      @Override public void changedUpdate(DocumentEvent event) {
        updateValue(false, false);
      }
    });
    
    privateLocation.addActionListener(e -> updateValue(false, false));
    
    latitudeField.addPropertyChangeListener("value", latLongChanged);
    longitudeField.addPropertyChangeListener("value", latLongChanged);
    
    ignoreGeocode.addActionListener(e -> {
      ignoreGeocode.setVisible(false);
      if (geocodedLatLong != null) {
        geocodeResultsToIgnore.add(geocodedLatLong);
        // Drop all geocoding-derived fields
        valueWithoutGeocoding = null;
        geocodedParent = null;
        geocodedLatLong = null;
        nameField.requestFocusInWindow();
        // Clear the lat/long fields and the preview panel
        latitudeField.removePropertyChangeListener("value", latLongChanged);
        longitudeField.removePropertyChangeListener("value", latLongChanged);
        latitudeField.setText("");
        longitudeField.setText("");
        locationPreviewPanel.setLatLong(null);
        latitudeField.addPropertyChangeListener("value", latLongChanged);
        longitudeField.addPropertyChangeListener("value", latLongChanged);
        updateCompleteAndValue();
      }
    });
    
    searchNear.addActionListener(e -> runReverseGeocoding());
    
    multipleGeocodeResults.addActionListener(e -> {
      UIGeocoderResults results = (UIGeocoderResults) multipleGeocodeResults.getSelectedItem();
      geocodeResultsAvailable(results.results, false /* don't show ignore */, true /* take the name when set */);
    });
    
    locationPreviewPanel.addMapClickedListener(latLong -> {
      // Drop all geocoding-derived fields
      valueWithoutGeocoding = null;
      geocodedParent = null;
      geocodedLatLong = null;
      locationPreviewPanel.setLatLong(latLong);
      latitudeField.removePropertyChangeListener("value", latLongChanged);
      longitudeField.removePropertyChangeListener("value", latLongChanged);
      latitudeField.setText(latLong.latitude());
      longitudeField.setText(latLong.longitude());
      latitudeField.addPropertyChangeListener("value", latLongChanged);
      longitudeField.addPropertyChangeListener("value", latLongChanged);
      updateCompleteAndValue();
    });
    
    addAncestorListener(new AncestorListener() {
      @Override public void ancestorRemoved(AncestorEvent event) {
        if (geocodeTimer != null) {
          geocodeTimer.stop();
          geocodeTimer = null;
        }
        if (reverseGeocodeTimer != null) {
          reverseGeocodeTimer.stop();
          reverseGeocodeTimer = null;
        }
        if (locationPreviewTimer != null) {
          locationPreviewTimer.stop();
          locationPreviewTimer = null;
        }
      }
      
      @Override public void ancestorMoved(AncestorEvent event) {}
      @Override public void ancestorAdded(AncestorEvent event) {}
    });
  }
  
  private void updateValue(
      boolean immediatelyLoadImage,
      boolean geocode) {
    Location oldValue = this.lastLocation;
    Location newValue = getValue();
    this.lastLocation = newValue;
    
    searchNear.setVisible(getCurrentLatLong() != null);
    
    // Save the value *without* the geocoding parent hint.  This will be used for subsequent
    // geocoding.  Rationale is that if you don't do this, then you'll end up re-geocoding with
    // that hint - which might succeed and might give you a different result - and if you get a
    // different result, then things go very poorly (infinite loops result)
    Location oldValueWithoutGeocoding = valueWithoutGeocoding;
    valueWithoutGeocoding = getValuePerhapsIgnoringGeocoding(true);
    
    firePropertyChange("value", oldValue, newValue);
    if (newValue == null) {
      summaryLabel.setText("");
    } else {
      int alreadyPresentParents = 0;
      String summary = "<html>";
      Location findingAParent = newValue;

      // Find the parent that's already added;  this should be the parentId.
      Location parentWithId = newValue.getParent();
      while (parentWithId != null && parentWithId.getId() == null) {
        parentWithId = parentWithId.getParent();
      }

      Object parentId = parentWithId == null ? null : whereIndexers.toIndexerObject(parentWithId);
      inIndexer.setValue(parentId);
      
      // TODO: this is clearly wrong for RTL languages
      while (findingAParent != null) {
        summary += HtmlResponseWriter.htmlEscape(
            LocationIdToString.getStringWithType(findingAParent));
        if (findingAParent.getId() != null) {
          alreadyPresentParents++;
          if (alreadyPresentParents >= 2) {
            if (findingAParent.getParent() != null) {
              // Left arrow, ellipsis
              summary += " \u2190 \u2026";
            }
            break;
          }
        }
        findingAParent = findingAParent.getParent();
        // Left arrow
        summary += " \u2190<wbr> ";
      }
      summaryLabel.setText(summary);
    }
    
    if (geocode) {
      // Hide any errors from the last location
      geocodeErrors.setVisible(false);
      
      final LatLongCoordinates newLatLong;
      if (newValue == null) {
        newLatLong = getCurrentLatLong();
      } else {
        newLatLong = newValue.getLatLong().orNull();
      }
      // Update the location preview panel if it doesn't match the current lat long
      if (!Objects.equal(locationPreviewPanel.getLatLong(), newLatLong)) {
        // If image loading should happen immediately (or there's no lat-long), update now
        if (locationPreviewTimer != null) {
          locationPreviewTimer.stop();
        }
        
        if (immediatelyLoadImage || newLatLong == null) {
          locationPreviewPanel.setLatLong(newLatLong);
        } else {
          // Otherwise, wait for a few hundred millis for the user to stop typing
          if (locationPreviewTimer != null) {
            locationPreviewTimer.stop();
          }
          locationPreviewTimer = new Timer(
              LOCATION_PREVIEW_DELAY_MILLIS,
              (e) -> {
                // If the lat-long has changed out from underneath us while waiting, drop this. 
                if (Objects.equal(getCurrentLatLong(), newLatLong)) {
                  locationPreviewPanel.setLatLong(newLatLong);
                }
              });
          locationPreviewTimer.setRepeats(false);
          locationPreviewTimer.start();
        }
      }
      if (newValue == null) {
        // We don't have a valid value,but we *do* have lat/long, time to try a reverse geocoding
        if (newLatLong != null) {
          if (reverseGeocodeTimer == null) {
            reverseGeocodeTimer = new Timer(REVERSE_GEOCODE_DELAY_MILLIS, e -> runReverseGeocoding());
          }
          reverseGeocodeTimer.setRepeats(false);
          reverseGeocodeTimer.setCoalesce(true);
          reverseGeocodeTimer.start();
        }
      } else if (!latLongExplicitlyEntered) { 
        // Lat-long isn't explicitly entered, so use standard geocoding.
        if (geocodeTimer == null) {
          geocodeTimer = new Timer(GEOCODE_DELAY_MILLIS, e -> runGeocoding());
          geocodeTimer.setRepeats(false);
          geocodeTimer.setCoalesce(true);
          geocodeTimer.start();
        } else {
          // May need to re-geocode.  Skip if:
          //   - the previously pre-geocoded value has the same name and parent as the current value
          //     (that is, don't re-geocode just because geocoding grabbed a latitude and longitude,
          //      or because goecoding identified a more precise parent) 
          if (valueWithoutGeocoding != null) {
            if (oldValueWithoutGeocoding == null
                || !oldValueWithoutGeocoding.getModelName().equals(valueWithoutGeocoding.getModelName())
                || oldValueWithoutGeocoding.getType() != valueWithoutGeocoding.getType()
                || !oldValueWithoutGeocoding.getParent().equals(valueWithoutGeocoding.getParent())) {
              GeocoderResults currentMultiResults = null;
              if (multipleGeocodeResults.isVisible()
                  && multipleGeocodeResults.getSelectedItem() instanceof UIGeocoderResults) {
                UIGeocoderResults selectedItem = (UIGeocoderResults) multipleGeocodeResults.getSelectedItem();
                currentMultiResults = selectedItem.results;
              }
              
              // ... and also don't geocode again if the currently entered value is exactly
              if (currentMultiResults == null
                  || !valueWithoutGeocoding.getModelName().equals(currentMultiResults.name())) {
                geocodeTimer.restart();
              }
            }
          }
        }
      }
    }
  }
  
  private void geocodeResultsAvailable(
      GeocoderResults results,
      boolean showIgnoreGeocode,
      boolean takeNameFromGeocode) {
    geocodedLatLong = results == null ? null : results.coordinates();
    geocodedParent = results == null ? null : results.preferredParent().orNull();
    // When the geocoding happens, it'll create the same predefined locations again-and-again.
    // If one gets added, and *then* we try to reselect it... things go poorly.
    if (geocodedParent != null) {
      geocodedParent = Locations.normalizeLocation(geocodedParent, locations);
    }
    
    // Don't override an explicit lat/long
    if (!latLongExplicitlyEntered) {
      String newLatitudeText = geocodedLatLong == null ? "" : geocodedLatLong.latitude();
      String newLongitudeText = geocodedLatLong == null ? "" : geocodedLatLong.longitude();
  
      if (newLatitudeText.equals(latitudeField.getText())
          && newLongitudeText.equals(longitudeField.getText())) {
        return;
      }
    
      // Remove the listeners on latLong so updateValue doesn't fire twice,
      // and so we can explicitly request that update value immediately loads an image
      latitudeField.removePropertyChangeListener("value", latLongChanged);
      longitudeField.removePropertyChangeListener("value", latLongChanged);
      if (geocodedLatLong != null) {
        latitudeField.setText(geocodedLatLong.latitude());
        longitudeField.setText(geocodedLatLong.longitude());
        // Allow users to remove the geocode value
        if (showIgnoreGeocode) {
          ignoreGeocode.setText(Messages.getFormattedMessage(Name.IGNORE_RESULT_FORMAT, results.source()));
          ignoreGeocode.setVisible(true);
        }
      } else {
        latitudeField.setText("");
        longitudeField.setText("");
        ignoreGeocode.setVisible(false);
      }
      
      // Restore property change listeners, so subsequent manual edits will take
      latitudeField.addPropertyChangeListener("value", latLongChanged);
      longitudeField.addPropertyChangeListener("value", latLongChanged);
    }

    if (takeNameFromGeocode && results != null) {
      nameField.setText(results.name());
    }
    // 
    SwingUtilities.invokeLater(() -> {
      updateValue(
          true /* immediately load the image */,
          true /* geocoding path */);                        
    });
  }

  private void updateCompleteAndValue() {
    boolean isComplete = false;
    
    if (!Strings.isNullOrEmpty(name) && inIndexer.getValue() != null) {
      isComplete = true;
      for (Component c : moreSpecificPanel.getComponents()) {
        NewLocationRow row = (NewLocationRow) c;
        if (row.getLocationBuilder() == null) {
          isComplete = false;
          break;
        }
      }
    }
    
    Object latitude = latitudeField.getValue();
    Object longitude = longitudeField.getValue();
    // If lat/long are totally unspecified;  or they're both specified, that's fine.
    // But only specifying one isn't something we can save. 
    if ((latitude == null) != (longitude == null)) {
      isComplete = false;
    }
    
    setComplete(isComplete);
    updateValue(
        false /* load image on a timer */,
        true /* consider geocoding */);
  }
  
  private void initGUI() {
    ofTypeLabel = new JLabel();
    ofTypeLabel.setText(Messages.getMessage(Name.LOCATION_TYPE_LABEL));

    ofTypeChoice = new JComboBox<String>();
    ofTypeChoice.setModel(new DefaultComboBoxModel<String>(types.toArray(new String[0])));

    inBox = new JPanel();
    inBox.setLayout(new BoxLayout(inBox, BoxLayout.LINE_AXIS));

    nameField = new JTextField("", 15);
    // Default focus belongs on the name field if it's editable
    focusComponent = nameField;
    inBox.add(nameField);
    JLabel inLabel = new JLabel(Messages.getMessage(Name.LOCATION_IS_IN_LABEL));
    inBox.add(inLabel);

    inIndexer = new IndexerPanel<Object>();
    inIndexer.setColumns(20);
    inIndexer.setLayoutStrategy(IndexerPanel.LayoutStrategy.BELOW);
    inIndexer.setPreviewText(Name.START_TYPING_A_LOCATION);
    focusComponent = inIndexer;
    
    inHelp = new JLabel();
    updateInHelp(null);

    moreSpecificPanel = new JPanel();
    moreSpecificPanel.setLayout(new BoxLayout(moreSpecificPanel, BoxLayout.PAGE_AXIS));
    
    moreSpecificRow = new TextLink(Messages.getMessage(Name.EVEN_MORE_SPECIFIC));
    moreSpecificRow.setVisible(false);
    lessSpecificRow = new TextLink(Messages.getMessage(Name.LESS_SPECIFIC));
    lessSpecificRow.setVisible(false);
    
    summaryLabel = new JLabel();
    summaryLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    summaryLabel.setVerticalAlignment(SwingConstants.TOP);
    
    latitudeLabel = new JLabel(Messages.getMessage(Name.LATITUDE_LABEL));
    longitudeLabel = new JLabel(Messages.getMessage(Name.LONGITUDE_LABEL));

    latitudeField = new AutoSelectJFormattedTextField();
    latitudeField.setColumns(11);
    NumberFormatter latitudeFormatter = allowNullDecimalFormatter();
    latitudeFormatter.setValueClass(Double.class);
    latitudeFormatter.setMinimum(-90.0d);
    latitudeFormatter.setMaximum(90.0d);
    latitudeFormatter.setCommitsOnValidEdit(true);
    latitudeField.setFormatterFactory(new DefaultFormatterFactory(latitudeFormatter));
    
    longitudeField = new AutoSelectJFormattedTextField();
    longitudeField.setColumns(11);
    NumberFormatter longitudeFormatter = allowNullDecimalFormatter();
    longitudeFormatter.setValueClass(Double.class);
    longitudeFormatter.setMinimum(-180.0d);
    longitudeFormatter.setMaximum(180.0d);
    longitudeFormatter.setCommitsOnValidEdit(true);
    longitudeField.setFormatterFactory(new DefaultFormatterFactory(longitudeFormatter));

    geocodeSpinner = new ProgressSpinner();
    geocodeSpinner.setVisible(false);
    geocodeErrors = new TextLink("");
    geocodeErrors.putClientProperty(FontManager.PLAIN_LABEL, true);
    geocodeErrors.setVisible(false);
    geocodeErrors.setForeground(new Color(192, 0, 0));
    ignoreGeocode = new JButton(Messages.getMessage(Name.IGNORE_THIS_RESULT));
    ignoreGeocode.setIcon(DeleteColumn.deleteIcon());
    ignoreGeocode.setVisible(false);
    
    searchNear = new JButton(Messages.getMessage(Name.SEARCH_NEARBY));
    searchNear.setVisible(false);
    
    multipleGeocodeResults = new JComboBox<UIGeocoderResults>();
    FullWidthCombboxPopups.install(multipleGeocodeResults);
    multipleGeocodeResultsLabel = new JLabel(Messages.getMessage(Name.MULTIPLE_SEARCH_RESULTS));
    multipleGeocodeResults.setVisible(false);
    multipleGeocodeResultsLabel.setVisible(false);
    
    descriptionLabel = new JLabel(Messages.getMessage(Name.DESCRIPTION_OF_THIS_LOCATION));
    descriptionScrollPane = new JScrollPane();

    description = new JTextArea();
    descriptionScrollPane.setViewportView(description);
    description.setColumns(40);
    description.setRows(6);
    description.setWrapStyleWord(true);
    description.setLineWrap(true);
    UIUtils.fixTabOnTextArea(description);

    privateLocation = new JCheckBox(Messages.getMessage(Name.PRIVATE_LOCATION_CHECKBOX));

    fontManager.applyTo(this);
  }
  
  /** A row with a single, new location. */
  private class NewLocationRow extends JPanel {
    private JComboBox<String> ofTypeChoice;
    private JTextField field;

    NewLocationRow() {
      initGUI();
      hookUpContents();
    }

    @Override
    public void setEnabled(boolean enabled) {
      super.setEnabled(enabled);
      ofTypeChoice.setEnabled(enabled);
      field.setEnabled(enabled);
    }

    private void initGUI() {
      setAlignmentX(0.0f);
      GroupLayout thisLayout = new GroupLayout(this);

      ofTypeChoice = new JComboBox<String>();
      ofTypeChoice.setModel(new DefaultComboBoxModel<String>(types.toArray(new String[0])));

      JLabel inLabel = new JLabel();
      inLabel.setText(Messages.getMessage(Name.LOCATION_IS_IN_LABEL));
      field = new JTextField();
      field.setColumns(20);
      
      setBorder(new EmptyBorder(0, 15, 0, 0));
      setLayout(thisLayout);
      thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
          .addGroup(thisLayout.createParallelGroup(Alignment.BASELINE)
              .addComponent(field, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
              .addComponent(ofTypeChoice, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
          .addComponent(inLabel));
      thisLayout.setHorizontalGroup(thisLayout.createParallelGroup()
          .addGroup(thisLayout.createSequentialGroup()
              .addComponent(field, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
              .addPreferredGap(ComponentPlacement.RELATED)
              .addComponent(ofTypeChoice, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
          .addComponent(inLabel));
      fontManager.applyTo(this);
    }

    private void hookUpContents() {
      field.getDocument().addDocumentListener(updateComplete);      
      ofTypeChoice.addActionListener(e -> {
        updateValue(
            false /* load image on a timer */,
            false /* no reason to geocode */);
      });
    }
    
    private Location.Builder getLocationBuilder() {
      if (field.getText().isEmpty()) {
        return null;
      }
      
      return Location.builder()
          .setName(field.getText())
          .setType(TYPE_MAP.get(ofTypeChoice.getSelectedIndex()));
    }
  }
  
  public boolean isComplete() {
    return complete;
  }
  
  private void setComplete(boolean newComplete) {
    if (newComplete != complete) {
      complete = newComplete;
      
      firePropertyChange("complete", !newComplete, newComplete);
    }
  }
  
  public Location getValue() {
    return getValuePerhapsIgnoringGeocoding(false);
  }
  
  /**
   * Compute the value, perhaps ignoring the geocoding parent hint.
   */
  private Location getValuePerhapsIgnoringGeocoding(boolean ignoreGeocoding) {
    if (Strings.isNullOrEmpty(name)) {
      return null;
    }
    
    if (ignoreGeocoding) {
       if (inIndexer.getValue() == null) {
         return null;
       }
    } else {
      if (inIndexer.getValue() == null && geocodedParent == null) {
        return null;
      }
    }
    
    Location parent = WherePanelIndexers.toLocation(locations, inIndexer.getValue());
    // Use the geocoded parent as an alternate, if present.  This is turned off in two scenarios:
    // (1) The code needs to get the value *ignoring* geocoding, to try to avoid re-geocoding a value that
    //     just got geocoded!
    // (2) We're editing an existing location, and swapping out the parent induces lots of complexity
    //     that I'm not ready to handle.  In some simple cases, it's, well, simple, but if you're geocoding
    //     Ahwahnee Lodge in Yosemite Park, and then discover that that's in Mariposa County, do you
    //     move Ahwanee Lodge *out* of Yosemite and into Mariposa?  Or move Yosemite into Mariposa?  Both
    //     are wrong.  You could make a copy of Yosemite that's in Mariposa - which is fine for something
    //     like that which actually *is* in multiple counties, but what if the existing parent is only in
    //     a single county?  Now that's wrong too...
    // (2A)  But if the existing location being edited is parented *at* a built-in location, then this actually
    //       all works out, because the none of the problematic cases are relevant
    if (!ignoreGeocoding
        && geocodedParent != null) {
      // TODO: make sure this isn't sticky in unpleasant ways
      if (parent == null
          || existingLocationBeingEdited == null
          || existingLocationBeingEdited.getParent().isBuiltInLocation()) {
        parent = geocodedParent;
      }
    }
    
    // For each row, create a child.
    List<Component> rowsLastToFirst =
        Lists.reverse(Arrays.asList(moreSpecificPanel.getComponents()));
    for (Component c : rowsLastToFirst) {
      NewLocationRow row = (NewLocationRow) c;
      // TODO: what happens if an intermediate level already has a name?!?
      Location.Builder builder = row.getLocationBuilder();
      if (builder == null) {
        return null;
      }
      
      // Create the intermediate location (swapping into a predefined
      // location where necessary)
      parent = possiblyReplaceWithPredefined(builder.setParent(parent).build());
    }

    LatLongCoordinates latLong = getCurrentLatLong();

    // Create the final location (swapping into a predefined location where necessary)
    // TODO: swapping into a predefined location means that the description etc.
    // is *dropped*.  That will be very rare, but a bit worrisome.
    // Especially since a user *might* think this was a description
    // field for the whole day's outing?
    Location.Type type = TYPE_MAP.get(ofTypeChoice.getSelectedIndex());
    return possiblyReplaceWithPredefined(Location.builder()
        .setName(name)
        .setParent(parent)
        .setType(type)
        .setLatLong(latLong)
        .setDescription(description.getText())
        .setPrivate(privateLocation.isSelected())
        .build());
  }

  private LatLongCoordinates getCurrentLatLong() {
    Double latitude = (Double) latitudeField.getValue();
    Double longitude = (Double) longitudeField.getValue();
    LatLongCoordinates latLong = null;
    if (latitude != null && longitude != null) {
      latLong = LatLongCoordinates.withLatAndLong(latitude, longitude);
    }
    return latLong;
  }
  
  /**
   * Swap out a predefined location for a manually entered one.
   */
  private Location possiblyReplaceWithPredefined(Location location) {
    PredefinedLocation predefinedLocation = predefinedLocations.getPredefinedLocationChild(
        location.getParent(), location.getModelName());
    if (predefinedLocation == null) {
      return location;
    }
    
    return predefinedLocation.create(locations, location.getParent());
  }
  
  /**
   * Get the value, making sure that no duplicates are created.
   */
  public Location getValueAfterValidating() {
    Location location = getValue();
    if (location == null) {
      return null;
    }

    // Find the first parent without an ID (that is, the root-most new location)
    Location firstParentWithoutAnId = location;
    while (firstParentWithoutAnId.getParent().getId() == null) {
      firstParentWithoutAnId = firstParentWithoutAnId.getParent();
    }
    Location existingLocation = firstParentWithoutAnId.getParent().getContent(
        firstParentWithoutAnId.getModelName());
    // Make sure this won't be a duplicated name in its parent (unless it's in fact
    // replacing an existing location)
    if (existingLocation != null
        && existingLocation != existingLocationBeingEdited) {
      alerts.showError(this, Name.DUPLICATE_LOCATION_TITLE,
          Name.DUPLICATE_LOCATION_FORMAT,
          HtmlResponseWriter.htmlEscape(firstParentWithoutAnId.getDisplayName()), 
          HtmlResponseWriter.htmlEscape(firstParentWithoutAnId.getParent().getDisplayName()));
      return null;
    }
    
    return location;    
  }

  /**
   * Return a location, perhaps reusing an existing location that is already added.
   */
  public Location getValuePossiblyReusing() {
    Location location = getValue();
    if (location == null) {
      return null;
    }

    return Locations.normalizeLocation(location, locations);
  }

  public void clearValue() {
    inIndexer.clearValue();
  }

  @Override
  public boolean requestFocusInWindow() {
    return focusComponent.requestFocusInWindow();
  }

  private NumberFormatter allowNullDecimalFormatter() {
    NumberFormat numberFormat = NumberFormat.getNumberInstance();
    numberFormat.setMaximumIntegerDigits(3);
    numberFormat.setMaximumFractionDigits(7);
    return new NumberFormatter(numberFormat) {
      @Override
      public Object stringToValue(String string) throws ParseException {
        if ("".equals(string)) {
          return null;
        }
        return super.stringToValue(string);
      }
    };
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    GroupLayout thisLayout = new GroupLayout(this);
    setMinimumSize(fontManager.scale(new Dimension(300, 400)));
    setBorder(new EmptyBorder(
        fontManager.scale(20),
        fontManager.scale(20),
        fontManager.scale(20),
        fontManager.scale(20)));
        
    locationPreviewPanel.setPreferredSize(fontManager.scale(new Dimension(400, 400)));
    setLayout(thisLayout);
    
    int geocodeSpinnerSize = fontManager.scale(50);
    thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
        .addComponent(inBox, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
        .addGroup(thisLayout.createParallelGroup()
            .addComponent(locationPreviewPanel)
            .addGroup(thisLayout.createSequentialGroup()
                .addComponent(lessSpecificRow, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                .addComponent(moreSpecificPanel)
                .addComponent(moreSpecificRow, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                .addGroup(thisLayout.createBaselineGroup(false, false)
                    .addComponent(inIsRequired)
                    .addComponent(inIndexer)
                    .addComponent(inHelp))
                .addPreferredGap(ComponentPlacement.UNRELATED)
                .addGroup(thisLayout.createBaselineGroup(false, false)
                    .addComponent(ofTypeLabel)
                    .addComponent(ofTypeChoice))
                .addPreferredGap(ComponentPlacement.UNRELATED)
                .addGroup(thisLayout.createParallelGroup(Alignment.CENTER)
                    .addComponent(geocodeSpinner, geocodeSpinnerSize, geocodeSpinnerSize, geocodeSpinnerSize)
                    .addGroup(thisLayout.createSequentialGroup()
                        .addComponent(ignoreGeocode, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                        .addComponent(searchNear, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                        .addComponent(multipleGeocodeResultsLabel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                        .addComponent(multipleGeocodeResults, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
                    .addComponent(geocodeErrors, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                    .addGroup(thisLayout.createSequentialGroup()
                      .addGroup(thisLayout.createBaselineGroup(false, false)
                          .addComponent(latitudeLabel)
                          .addComponent(latitudeField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
                      .addGroup(thisLayout.createBaselineGroup(false, false)
                          .addComponent(longitudeLabel)
                          .addComponent(longitudeField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))))
                .addPreferredGap(ComponentPlacement.UNRELATED)
                // Force a minimum vertical size on the summary label so that the page doesn't reflow
                // when the name changes
                .addComponent(summaryLabel, fontManager.scale(60), PREFERRED_SIZE, PREFERRED_SIZE)
                .addPreferredGap(ComponentPlacement.UNRELATED)
                .addComponent(descriptionLabel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                .addComponent(descriptionScrollPane, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                .addPreferredGap(ComponentPlacement.UNRELATED)
                .addComponent(privateLocation, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))));
    thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
        .addGroup(thisLayout.createParallelGroup()
            .addGroup(thisLayout.createSequentialGroup()
                .addGap(20)
                .addComponent(summaryLabel,
                    fontManager.scale(200), fontManager.scale(200), fontManager.scale(400)))
            .addComponent(inBox, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addGroup(thisLayout.createSequentialGroup()
                .addComponent(inIsRequired)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(inIndexer)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(inHelp))
            .addComponent(lessSpecificRow, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addComponent(moreSpecificPanel)
            .addComponent(moreSpecificRow, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addComponent(descriptionLabel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addComponent(descriptionScrollPane, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addComponent(privateLocation, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addGroup(thisLayout.createSequentialGroup()
                .addGroup(thisLayout.createParallelGroup()
                    .addComponent(ofTypeLabel)
                    .addComponent(latitudeLabel)
                    .addComponent(longitudeLabel))
                .addPreferredGap(ComponentPlacement.RELATED)
                .addGroup(thisLayout.createParallelGroup()
                    .addComponent(ofTypeChoice, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                    .addComponent(latitudeField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                    .addComponent(longitudeField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
                .addPreferredGap(ComponentPlacement.UNRELATED)
                .addComponent(geocodeErrors, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                .addGroup(thisLayout.createParallelGroup()
                    .addComponent(ignoreGeocode, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                    .addComponent(searchNear, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                    .addComponent(multipleGeocodeResultsLabel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                    .addComponent(multipleGeocodeResults, PREFERRED_SIZE, fontManager.scale(150), fontManager.scale(190)))
                .addComponent(geocodeSpinner, geocodeSpinnerSize, geocodeSpinnerSize, geocodeSpinnerSize)))
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(locationPreviewPanel));
  }  
  
  private void updateInHelp(Location location) {
    if (location == null) {
      inHelp.setText(Messages.getMessage(Name.SELECT_AN_EXISTING_LOCATION));
    } else if (location.getType() == null) {
      inHelp.setText("");
    } else {
      inHelp.setText(String.format("(%s)", location.getType().toString()));
    }
  }

  static class UIGeocoderResults {
    private final GeocoderResults results;
    private final String uiName;

    UIGeocoderResults(GeocoderResults results, Collection<GeocoderResults> allResults) {
      this.results = results;
      if (results == null) {
        uiName = "";
      } else {
        String name = results.nameWithSource();
        // Yeah, this O(N^2).  N is tiny (or had better be)
        for (GeocoderResults otherResults : allResults) {
          if (otherResults == results) {
            continue;
          }
          if (otherResults.nameWithSource().equals(name)) {
            if (results.preferredParent().isPresent()) {
              name = String.format("%s (%s)",
                  name,
                  results.preferredParent().get().getDisplayName());
            }
            break;
          }
        }
        uiName = name;
      }
    }
    
    public UIGeocoderResults(
        GeocoderResults results,
        ImmutableList<GeocoderResults> allResults,
        LatLongCoordinates center) {
      this.results = results;
      if (results == null) {
        uiName = "";
      } else {
        String name = results.nameWithSource();
        // Yeah, this O(N^2).  N is tiny (or had better be)
        for (GeocoderResults otherResults : allResults) {
          if (otherResults == results) {
            continue;
          }
          if (otherResults.nameWithSource().equals(name)) {
            if (results.preferredParent().isPresent()) {
              name = String.format("%s (%s)",
                  name,
                  results.preferredParent().get().getDisplayName());
            }
            break;
          }
        }
        
        if (results.coordinates() != null) {
          name = String.format("%s (%.1f km)", name, results.coordinates().kmDistance(center));
        }
        uiName = name;
      }
    }

    @Override
    public String toString() {
      return uiName;
    }
  }

  /**
   * Finds the common ancestor of all of the "preferred parents".
   */
  private Location getCommonPredefinedLocation(ImmutableList<GeocoderResults> resultsList) {
    Location commonParent = null;
    for (GeocoderResults results : resultsList) {
      Location parent = getPredefinedParent(results);
      if (parent != null) {
        if (commonParent == null) {
          commonParent = parent;
        } else {
          commonParent = Locations.getCommonAncestor(commonParent, parent);
        }
      }
    }
    return commonParent;
  }

  private Location getPredefinedParent(GeocoderResults geocoderResults) {
    if (geocoderResults.preferredParent().isPresent()) {
      Location location = geocoderResults.preferredParent().get();
      while (location != null) {
        if (location.isBuiltInLocation()) {
          return location;
        }
        location = location.getParent();
      }
    }
    
    return null;
  }

  private void showGeocodeError(
      String text,
      @Nullable Throwable t) {
    
    if (t != null) {
      if (t.getCause() instanceof UnknownHostException
          || t.getCause() instanceof SocketException
          || t.getCause() instanceof SocketTimeoutException) {
        // For a few common types of errors, just tell the user there
        // were generic problems, and don't offer "click for details"
        text += " " + Messages.getMessage(Name.NETWORK_FAILED_ERROR);
        t = null;
      } else { 
        text += " " + Messages.getMessage(Name.CLICK_FOR_DETAILS);
      }
    }
    geocodeErrors.setText(text);
    geocodeErrors.setEnabled(true);
    geocodeErrors.setVisible(true);
    if (t == null) {
      for (ActionListener listener : geocodeErrors.getActionListeners()) {
        geocodeErrors.removeActionListener(listener);
      }
      geocodeErrors.setFocusable(false);
      geocodeErrors.setCursor(null);
    } else {
      final Throwable errorToShow = t;
      geocodeErrors.addActionListener(e -> 
          alerts.showError(geocodeErrors, errorToShow, geocodeErrors.getText(),
              Messages.getMessage(Name.LOCATION_DETECTION_FAILED)));
      geocodeErrors.setFocusable(true);
      geocodeErrors.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }
  }
  
  private void runReverseGeocoding() {
    final LatLongCoordinates newLatLong = getCurrentLatLong();
    // Shouldn't get called at all with this, but race conditions suggest it does.
    if (newLatLong == null) {
      return;
    }
    ListenableFuture<ImmutableList<GeocoderResults>> reverseGeocode =
        reverseGeocoder.reverseGeocode(locations, newLatLong);
    geocodeSpinner.start();
    geocodeSpinner.setVisible(true);
    geocodeErrors.setVisible(false);
    ignoreGeocode.setVisible(false);
    multipleGeocodeResultsLabel.setVisible(false);
    multipleGeocodeResults.setVisible(false);
    Futures.addCallback(reverseGeocode, new FutureCallback<ImmutableList<GeocoderResults>>() {
      @Override
      public void onFailure(final Throwable t) {
        SwingUtilities.invokeLater(() -> {
          geocodeSpinner.stop();
          geocodeSpinner.setVisible(false);
          if (!(Throwables.getRootCause(t) instanceof CancellationException)) {
            showGeocodeError(Messages.getMessage(Name.ERROR_FETCHING_LOCATION), t);
          }
        });
      }
    
      @Override
      public void onSuccess(final ImmutableList<GeocoderResults> resultsList) {
        SwingUtilities.invokeLater(new Runnable() {
          @Override public void run() {
            geocodeSpinner.stop();
            geocodeSpinner.setVisible(false);
            if (!resultsList.isEmpty()) {
              // Find the agreed-upon parent of this lat/long, and set that immediately as the "in" location,
              // unless the inIndexer value has been edited.
              if (inIndexerIsEdited()) {
                Location commonPredefinedLocation = getCommonPredefinedLocation(resultsList);
                if (commonPredefinedLocation != null) {
                  locations.ensureAdded(commonPredefinedLocation);
                  inIndexer.setValue(commonPredefinedLocation.getId());
                }
              }
              
              // Build a list that starts with an empty string
              List<UIGeocoderResults> resultsStartingWithEmptyString = Lists.newArrayList();
              resultsStartingWithEmptyString.add(new UIGeocoderResults(null, resultsList));
              // ... and a set of all the sources found
              Set<GeocoderResults.Source> sources = Sets.newHashSet();
              for (GeocoderResults results : resultsList) {
                resultsStartingWithEmptyString.add(new UIGeocoderResults(results, resultsList, newLatLong));
                sources.add(results.source());
              }
              multipleGeocodeResults.setModel(new DefaultComboBoxModel<UIGeocoderResults>(
                  resultsStartingWithEmptyString.toArray(new UIGeocoderResults[0])));
              multipleGeocodeResultsLabel.setText(Messages.getFormattedMessage(
                  Name.MULTIPLE_RESULTS_FROM_FORMAT, Joiner.on(", ").join(sources)));
              multipleGeocodeResultsLabel.setVisible(true);
              multipleGeocodeResults.setVisible(true);
            } else {
              showGeocodeError(Messages.getMessage(Name.COULDNT_FIND_RESULTS_FOR_LAT_LONG), null);
            }
          }
        });
      }
    }, MoreExecutors.directExecutor());
  }

  private void runGeocoding() {
    if (lastLocation == null) {
      return;
    }
    // If the currently entered lat long *is not* a manually entered lat long,
    // *and* the lat long isn't manually entered, then hit the geocoder service
    if (!valueWithoutGeocoding.getLatLong().isPresent()
        || Objects.equal(valueWithoutGeocoding.getLatLong().get(), geocodedLatLong)) {
      // Also, skip if the focus is in the latitude/longitude fields, because
      // the user might be trying to manually enter a latitude/longitude.
      // TODO: verify this won't get hit too often
      final String lastLocationName = valueWithoutGeocoding.getDisplayName();
      if (!latitudeField.hasFocus() && !longitudeField.hasFocus()) {
        // Geocode, but - and this is important! - use a location generated ignoring any geocoding hints.
        // If you don't do this, then you'll end up potentially infinite looping.
        ListenableFuture<ImmutableList<GeocoderResults>> geocode = geocoder.geocode(
            locations, valueWithoutGeocoding, geocodeResultsToIgnore);
        currentGeocodeFuture = geocode;
        geocodeSpinner.start();
        geocodeSpinner.setVisible(true);
        geocodeErrors.setVisible(false);
        ignoreGeocode.setVisible(false);
        multipleGeocodeResultsLabel.setVisible(false);
        multipleGeocodeResults.setVisible(false);
        Futures.addCallback(geocode, new FutureCallback<ImmutableList<GeocoderResults>>() {
          @Override
          public void onFailure(final Throwable t) {
            SwingUtilities.invokeLater(() -> {
              geocodeSpinner.stop();
              geocodeSpinner.setVisible(false);
              if (!(Throwables.getRootCause(t) instanceof CancellationException)) {
                showGeocodeError(Messages.getMessage(Name.ERROR_FETCHING_LOCATION), t);
              }
            });
          }
     
          @Override
          public void onSuccess(final ImmutableList<GeocoderResults> resultsList) {
            SwingUtilities.invokeLater(() -> {
              geocodeSpinner.stop();
              geocodeSpinner.setVisible(false);
              
              if (resultsList.size() > 1) {
                // Build a list that starts with an empty string
                List<UIGeocoderResults> resultsStartingWithEmptyString = Lists.newArrayList();
                resultsStartingWithEmptyString.add(new UIGeocoderResults(null, resultsList));
                // ... and a set of all the sources found
                Set<GeocoderResults.Source> sources = Sets.newHashSet();
                for (GeocoderResults results : resultsList) {
                  resultsStartingWithEmptyString.add(new UIGeocoderResults(results, resultsList));
                  sources.add(results.source());
                }
                multipleGeocodeResults.setModel(
                    new DefaultComboBoxModel<UIGeocoderResults>(
                        resultsStartingWithEmptyString.toArray(new UIGeocoderResults[0])));
                multipleGeocodeResultsLabel.setText(
                    Messages.getFormattedMessage(Name.MULTIPLE_RESULTS_FROM_FORMAT, Joiner.on(", ").join(sources)));
                multipleGeocodeResultsLabel.setVisible(true);
                multipleGeocodeResults.setVisible(true);
              } else {
                GeocoderResults results = resultsList.isEmpty() ? null : resultsList.get(0);

                if (results == null) {
                  showGeocodeError(
                      Messages.getFormattedMessage(Name.COULDNT_FIND_LOCATION_FORMAT, lastLocationName), null);
                }
                geocodeResultsAvailable(results, true /* show ignore geocode */, false /* don't take name */);
              }
            });
          }
        }, MoreExecutors.directExecutor());
      }
    }
  }
  
  /**
   * Cancel any outstanding operations in the NewLocationPanel.  Use this when reusing a NewLocationPanel
   * for multiple locations, and do so *before* setting any values.
   */
  public void cancelAsyncOperations() {
    if (currentGeocodeFuture != null) {
      currentGeocodeFuture.cancel(true);
    }
    if (geocodeTimer != null) {
      geocodeTimer.stop();
    }
    if (locationPreviewTimer != null) {
      locationPreviewTimer.stop();
    }
    if (reverseGeocodeTimer != null) {
      reverseGeocodeTimer.stop();
    }
  }

  private boolean inIndexerIsEdited() {
    return inIndexer.getTextValue().isEmpty()
        || (inIndexer.getValue() != null
            && WherePanelIndexers.toLocation(locations, inIndexer.getValue()) == geocodedParent);
  }
}
