/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.joda.time.DateTimeFieldType;
import org.joda.time.LocalTime;
import org.joda.time.ReadablePartial;

import com.google.common.base.Objects;
import com.google.common.base.Predicates;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.user.User;
import com.scythebill.birdlist.model.util.Dirty;
import com.scythebill.birdlist.model.util.DirtyImpl;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.UserChipsUi;
import com.scythebill.birdlist.ui.util.LocationIdToString;

/**
 * Panel for entering a date and place.
 * <p>
 * This panel <em>does not</em> require that the location be complete.  An
 * unknown location can still be entered, and will be completed elsewhere.
 */
public class NewWhenAndWherePanel extends JPanel implements FontsUpdatedListener {
  private IndexerPanel<Object> whereIndexer;
  private JLabel whereLabel;
  private RequiredLabel whereIsRequired = new RequiredLabel();
  private DatePanel whenPanel;
  private JLabel whenLabel;
  private TimePanel timePanel;
  private JLabel timeLabel;
  private final ReportSet reportSet;
  private final PredefinedLocations predefinedLocations;
  private Location where;
  private String cachedWhereText = "";
  private DirtyImpl dirty = new DirtyImpl(false);
  private JLabel whereLabelHelp;
  private JLabel locationDetailLabel;
  private WherePanelIndexers whereIndexers;
  private FontManager fontManager;
  private UserChipsUi userChipsUi;
  private ImmutableSet<User> who = ImmutableSet.of();

  @Inject
  public NewWhenAndWherePanel(ReportSet reportSet, PredefinedLocations predefinedLocations, FontManager fontManager) {
    this.reportSet = reportSet;
    this.predefinedLocations = predefinedLocations;
    this.fontManager = fontManager;

    initGUI();
    hookUpContents();
  }

  public void setWhereLayoutStrategy(IndexerPanel.LayoutStrategy layoutStrategy) {
    whereIndexer.setLayoutStrategy(layoutStrategy);
  }

  private void hookUpContents() {
    whereIndexers = new WherePanelIndexers(
        reportSet.getLocations(), predefinedLocations, /*addNullName=*/false, null, Predicates.alwaysTrue(),
        /* syntheticLocations= */ null);
    whereIndexers.configureIndexer(whereIndexer);
    // Do not allow showing the popup on focus gained;  this causes problems
    // when returning to this field (temporarily) after an invalid value.
    whereIndexer.setCanShowPopupOnFocusGained(false);
    whereIndexer.setSupportsNewValues(true);
    whereIndexer.setPreviewText(Name.START_TYPING_A_LOCATION);
    whereIndexer.addPropertyChangeListener("value",
        new PropertyChangeListener() {
          @Override
          public void propertyChange(PropertyChangeEvent event) {
            Location location = WherePanelIndexers.toLocation(reportSet.getLocations(), event.getNewValue());
            if (location == null) {
              setWhereImpl(null);
            } else {
              setWhereImpl(location);
            }
          }
        });
    whereIndexer.addPropertyChangeListener("popupVisible",
        new PropertyChangeListener() {
          @Override public void propertyChange(PropertyChangeEvent e) {
            updateLocationText();
            firePropertyChange("wherePopupVisible", e.getOldValue(), e.getNewValue());
          }
        });
    // Notify parents whenever the text of the where component changes
    // (fire a "whereText" update)
    whereIndexer.getTextComponent().getDocument().addDocumentListener(
        new DocumentListener() {
          @Override public void removeUpdate(DocumentEvent e) {
            fireWhereTextChanged();
          }
          
          @Override public void insertUpdate(DocumentEvent e) {
            fireWhereTextChanged();
          }
          
          @Override public void changedUpdate(DocumentEvent e) {
            fireWhereTextChanged();
          }
          
          private void fireWhereTextChanged() {
            String newValue = getWhereText();
            firePropertyChange("whereText", cachedWhereText, newValue);
            cachedWhereText = newValue;
            
            updateLocationText();
          }
        });

    whenPanel.addPropertyChangeListener("value", new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        firePropertyChange("when", evt.getOldValue(), evt.getNewValue());
        boolean hasTime = isTimeSupported((ReadablePartial) evt.getNewValue());
        timeLabel.setVisible(hasTime);
        timePanel.setVisible(hasTime);
        
        dirty.setDirty(true);
      }
    });

    timePanel.addPropertyChangeListener("value", new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        firePropertyChange("time", evt.getOldValue(), evt.getNewValue());
        dirty.setDirty(true);
      }
    });
    
    if (userChipsUi != null) {
      userChipsUi.userChips.addChipsChangedListener(() -> {
        setWhoImpl(userChipsUi.userChips.getChips());
        dirty.setDirty(true);
      });
    }
  }

  private boolean isTimeSupported(ReadablePartial when) {
    if (when == null) {
      return false;
    }
    
    return when.isSupported(DateTimeFieldType.dayOfMonth())
        && when.isSupported(DateTimeFieldType.monthOfYear())
        && when.isSupported(DateTimeFieldType.year());
  }

  public Dirty getDirty() {
    return dirty;
  }
  
  public ReadablePartial getWhen() {
    return whenPanel.getValue();
  }

  public void setWhen(ReadablePartial partial) {
    whenPanel.setValue(partial);
  }

  public LocalTime getTime() {
    // Don't return a time unless all date fields are specified
    if (!isTimeSupported(getWhen())) {
      return null;
    }
    
    return timePanel.getValue();
  }

  public void setTime(LocalTime time) {
    timePanel.setValue(time);
  }

  public void setWhere(Location location) {
    // This class isn't particularly happy with locations that don't yet have
    // IDs;  the indexer panel doesn't support them, since it deals entirely
    // in location IDs.
    if (location != null && location.getId() == null) {
      setWhereImpl(null);
      setWhereText(location.getDisplayName());
    } else {
      setWhereImpl(location);
      if (location != null && location.getId() == null) {
        setWhereText(location.getDisplayName());
      } else {
        whereIndexer.setValue(location == null ? null : location.getId());
      }
    }
  }

  public Location getWhere() {
    return where;
  }

  public String getWhereText() {
    return whereIndexer.getTextValue();
  }

  public void setWhereText(String whereText) {
    whereIndexer.setValue(null);
    whereIndexer.setTextValue(whereText);
  }

  protected void setWhereImpl(Location location) {
    if (this.where != location) {
      Location oldWhere = where;
      where = location;
      firePropertyChange("where", oldWhere, where);
      dirty.setDirty(true);
    }
  }

  public void shown() {
    whereIndexer.requestFocusInWindow();
    updateLocationText();
  }

  public ImmutableSet<User> getWho() {
    return who;
  }
  
  public void setWho(Collection<User> users) {
    setWhoImpl(users);
    if (userChipsUi != null) {
      userChipsUi.userChips.clear();
      if (users != null) {
        userChipsUi.userChips.addAllChips(users);
      }
    }
  }
  
  private void setWhoImpl(Collection<User> users) {
    Collection<User> oldWho = this.who;
    ImmutableSet<User> newWho = ImmutableSet.copyOf(users);
    if (!Objects.equal(oldWho, newWho)) {
      this.who = newWho;
      firePropertyChange("who", oldWho, newWho);
    }      
  }

  private void initGUI() {
    if (reportSet.getUserSet() != null) {
      userChipsUi = new UserChipsUi(reportSet.getUserSet(), fontManager);
      userChipsUi.userLabel.setText(Messages.getMessage(Name.WHO_QUESTION));
      userChipsUi.userChips.setScaledWidth(400);
    }
    
    whereIndexer = new IndexerPanel<Object>();
    whereLabel = new JLabel();
    whereLabel.setText(Messages.getMessage(Name.WHERE_QUESTION));
    
    whereLabelHelp = new JLabel();
    whereLabelHelp.putClientProperty(FontManager.PLAIN_LABEL, true);

    locationDetailLabel = new JLabel();
    locationDetailLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    
    whenLabel = new JLabel();
    whenLabel.setText(Messages.getMessage(Name.WHEN_QUESTION));
    whenPanel = new DatePanel();
    
    timeLabel = new JLabel(Messages.getMessage(Name.START_TIME_QUESTION));
    timePanel = new TimePanel();
    timeLabel.setVisible(false);
    timePanel.setVisible(false);
  }
  
  public void setEditable(boolean editable) {
    whenPanel.setEditable(editable);
    whereIndexer.setEditable(editable);
  }

  public boolean isWherePopupVisible() {
    return whereIndexer.isPopupVisible();
  }

  public boolean hasWhere() {
    return whereIndexer.getValue() != null;
  }

  public boolean hasWhen() {
    return whenPanel.getValue() != null;
  }

  private String locationHierarchyAsString(Location location) {
    if (location == null) {
      return "";
    }
    
    int count = 0;
    String text = "";
    // TODO: clearly wrong for RTL languages
    while (location != null && count < 4) {
      // Add a left arrow
      text += "\u2190 ";
      text += LocationIdToString.getStringWithType(location)
          + " ";
      location = location.getParent();
      count++;
    }
    
    if (location != null) {
      // Add an ellipsis
      text += "\u2190 \u2026";
    }
    return text;
  }
  
  
  private void updateLocationText() {
    SwingUtilities.invokeLater(new Runnable() {
      @Override public void run() {
        // Location is set;  show the parent location
        Location location = getWhere();
        if (location != null) {
          whereLabelHelp.setText("");
          locationDetailLabel.setText(locationHierarchyAsString(location.getParent()));
        } else if (!Strings.isNullOrEmpty(getWhereText())) {
          locationDetailLabel.setText("");
          if (isWherePopupVisible()) {
            whereLabelHelp.setText(
                Messages.getMessage(Name.PICK_VALUE_OR_ESCAPE_TO_CLOSE));
          } else {
            whereLabelHelp.setText(
                Messages.getFormattedMessage(Name.LOCATION_IS_NEW_FORMAT, getWhereText()));
          }
        } else {
          locationDetailLabel.setText("");
          whereLabelHelp.setText(Messages.getMessage(Name.START_TYPING_A_LOCATION));
        }
      }
    });      
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    GroupLayout layout = new GroupLayout(this);
    this.setLayout(layout);
    layout.setAutoCreateContainerGaps(true);
    setPreferredSize(fontManager.scale(new Dimension(400, 300)));

    ParallelGroup horizontalGroup = layout.createParallelGroup();
    
    if (userChipsUi != null) {
      horizontalGroup
          .addComponent(userChipsUi.userLabel)
          .addGroup(layout.createSequentialGroup()
              .addComponent(userChipsUi.userIndexer)
              .addPreferredGap(ComponentPlacement.RELATED)
              .addComponent(userChipsUi.addUserButton))
          .addGroup(layout.createSequentialGroup()
              .addGap(10)
              .addComponent(userChipsUi.userChipsScrollPane, fontManager.scale(400), fontManager.scale(400), fontManager.scale(400)));
    }
    
    horizontalGroup
        .addGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup()
                .addComponent(whenLabel)
                .addComponent(whenPanel))
            .addGap(50)
            .addGroup(layout.createParallelGroup()
                .addComponent(timeLabel)
                .addComponent(timePanel)))
        .addGroup(layout.createSequentialGroup()
            .addComponent(whereLabel)
            .addPreferredGap(ComponentPlacement.UNRELATED)
            .addComponent(whereLabelHelp))
        .addGroup(layout.createSequentialGroup()
            .addComponent(whereIsRequired)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(whereIndexer, PREFERRED_SIZE, PREFERRED_SIZE, fontManager.scale(300)))
        .addGroup(layout.createSequentialGroup()
            .addGap(20)
            .addComponent(locationDetailLabel));
    layout.setHorizontalGroup(horizontalGroup);

    SequentialGroup verticalGroup = layout.createSequentialGroup();
    if (userChipsUi != null) {
      verticalGroup
         .addComponent(userChipsUi.userLabel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
         .addGroup(layout.createBaselineGroup(false, false)
             .addComponent(userChipsUi.userIndexer, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
             .addComponent(userChipsUi.addUserButton, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
         .addComponent(userChipsUi.userChipsScrollPane, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
         .addPreferredGap(ComponentPlacement.UNRELATED);
    }
    verticalGroup
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(whenLabel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addComponent(timeLabel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
        .addPreferredGap(ComponentPlacement.RELATED)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(whenPanel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addComponent(timePanel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
        .addGap(fontManager.scale(17))
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(whereLabel,PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addComponent(whereLabelHelp,PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
        .addPreferredGap(ComponentPlacement.RELATED)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(whereIsRequired)
            .addComponent(whereIndexer, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))            
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(locationDetailLabel);
    layout.setVerticalGroup(verticalGroup);
  }
}
