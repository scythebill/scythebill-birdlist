/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import java.awt.Component;
import java.awt.Dialog.ModalityType;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.annotation.Nullable;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Simple panel for displaying a component above a message and an OK button
 * and (optionally) a cancel button.
 */
public class OkCancelPanel extends JPanel {
  public static Action CLOSE_WINDOW_ACTION = new AbstractAction() {
    @Override
    public void actionPerformed(ActionEvent e) {
      SwingUtilities.getWindowAncestor((Component) e.getSource()).dispose();
    }
  }; 
  
  protected JButton okButton;

  public OkCancelPanel(
       Action okAction,
       @Nullable Action cancelOrOtherAction,
       Component content,
       @Nullable Component focusOnEntry) {
    setBorder(new EmptyBorder(20, 20, 15, 20));
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    JSeparator separator = new JSeparator();
    
    JButton cancelButton = new JButton();
    if (cancelOrOtherAction == null) {
      cancelButton.setVisible(false);
    } else {
      cancelButton.setAction(cancelOrOtherAction);
      if (cancelOrOtherAction.getValue(Action.NAME) == null) {
        cancelButton.setText(Messages.getMessage(Name.CANCEL_BUTTON));
      }
    }
    
    okButton = new JButton(okAction);
    okButton.setText(Messages.getMessage(Name.OK_BUTTON));
    
    JPanel spacer = new JPanel();
    
    layout.setAutoCreateContainerGaps(true);

    layout.setHorizontalGroup(layout.createParallelGroup()
        .addComponent(content)
        .addComponent(spacer)
        .addComponent(separator)
        .addGroup(Alignment.LEADING, layout.createSequentialGroup()
            .addComponent(cancelButton)
            .addGap(0, 75, Short.MAX_VALUE)
            .addComponent(okButton)));
    layout.setVerticalGroup(layout.createSequentialGroup()
        .addComponent(content)
        .addGap(18)
        .addComponent(spacer, 0, 0, Short.MAX_VALUE)
        .addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addGroup(layout.createParallelGroup()
            .addComponent(cancelButton)
            .addComponent(okButton)));
    if (cancelButton.isVisible()) {
      layout.linkSize(cancelButton, okButton);
    }
    
    UIUtils.focusOnEntry(this, focusOnEntry == null ? okButton : focusOnEntry);
    
    KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
        .put(escape, "cancelPanel");
    getActionMap().put("cancelPanel", cancelOrOtherAction);
  }

  public void showInDialog(
      Window parent,
      FontManager fontManager) {
    ModalityType modality =
        Toolkit.getDefaultToolkit().isModalityTypeSupported(ModalityType.DOCUMENT_MODAL)
        ? ModalityType.DOCUMENT_MODAL : ModalityType.APPLICATION_MODAL;
    final JDialog dialog = new JDialog(parent, modality);
    dialog.setContentPane(this);
    fontManager.applyTo(dialog);
    dialog.pack();
    dialog.setVisible(true);
  }
  
  public void focusOnOk() {
    UIUtils.focusOnEntry(this, okButton);
  }
}
