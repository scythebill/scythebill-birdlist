/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.scythebill.birdlist.ui.fonts.FontManager;

/**
 * Label that optionally indicates a field is required.
 */
public class PossiblyRequiredLabel extends JPanel {
  private String text;
  private boolean required;
  private final JLabel mainLabel = new JLabel();
  private final JLabel requiredLabel = new RequiredLabel();
  
  public PossiblyRequiredLabel(String text) {
    this.text = text;
    this.required = false;
    
    setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
    add(requiredLabel);
    add(mainLabel);
    
    updateContents();
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
    updateContents();
  }

  public boolean isRequired() {
    return required;
  }

  public void setRequired(boolean required) {
    this.required = required;
    updateContents();
  }
  
  
  private void updateContents() {
    requiredLabel.setVisible(required);
    mainLabel.setText(text);
    mainLabel.putClientProperty(FontManager.PLAIN_LABEL, !required);
  }

  @Override
  public int getBaseline(int width, int height) {
    return mainLabel.getBaseline(width, height);
  }
}
