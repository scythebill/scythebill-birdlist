/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.Timer;

/**
 * A simple progress-spinner, for indicating long-running tasks of uncertain
 * length in a compact space.
 */
public class ProgressSpinner extends JComponent {
  public static void main(String[] args) {
    JFrame frame = new JFrame();
    Box box = Box.createVerticalBox();
    final ProgressSpinner spinner = new ProgressSpinner();
    final JButton start = new JButton("Start");
    box.add(spinner);
    box.add(start);
    frame.setContentPane(box);
    frame.setVisible(true);
    frame.pack();
    start.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        if (spinner.isStarted()) {
          spinner.stop();
          start.setText("Start");
        } else {
          spinner.start();
          start.setText("Stop");
        }
      }
    });
  }

  private boolean started;
  private int progress;
  private final static int INCREMENTS = 20;
  private final static double RADIANS_DELTA = 2.0 * Math.PI / INCREMENTS;
  private ActionListener advance = new ActionListener() {
    @Override public void actionPerformed(ActionEvent e) {
      advance();
    }
  };
  private Timer timer;
  
  public ProgressSpinner() {
    setPreferredSize(new Dimension(100, 100));
  }
  
  public void start() {
    if (!started) {
      progress = 0;
      started = true;
      repaint();
      
      if (timer == null) {
        timer = new Timer(40, advance);
      }
      timer.start();
    }
  }
  
  public void stop() {
    if (started) {
      started = false;
      timer.stop();
      repaint();
    }
  }
  
  public boolean isStarted() {
    return started;
  }

  private void advance() {
    progress++;
    repaint();
  }

  @Override
  protected void paintComponent(Graphics g) {
    if (isStarted()) {
      int radius = Math.min(getWidth() / 2, getHeight() / 2);
      int centerX = getWidth() / 2;
      int centerY = getHeight() / 2;
      // Start the lines halfway out
      int centerOmitted = radius / 2;
  
      double radiansStart = (progress % INCREMENTS) * RADIANS_DELTA;
      
      for (int i = 0; i < INCREMENTS; i++) {
        double radians = radiansStart + (i * RADIANS_DELTA);
        double sine = Math.sin(radians);
        double cosine = Math.cos(radians);
        
        int startX = (int) (centerX + centerOmitted * cosine);
        int startY = (int) (centerY + centerOmitted * sine);
        int endX = (int) (centerX + radius * cosine);  
        int endY = (int) (centerY + radius * sine);
        Color foreground;
        int halfway = INCREMENTS / 2;
        if (i < halfway) {
          // For the first half of the spinner, draw in white.
          // Gives it a strong white look, which looks good
          foreground = Color.WHITE;
        } else {
          float grayscale = (1.0f / halfway) * (INCREMENTS - i - 1);
          foreground = new Color(grayscale, grayscale, grayscale);
        }
        g.setColor(foreground);
        g.drawLine(startX, startY, endX, endY);
      }
    }
  }

}
