/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.ReportSetMutator;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.AndDirty;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.app.NavigablePanel;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.SpHybridDialog;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;
import com.scythebill.birdlist.ui.util.OpenMapUrl;

/**
 * Panel for displaying/updating sighting information from within the TaxonBrowser
 * (or anywhere else).
 */
public class SightingBrowsePanel extends JPanel implements FontsUpdatedListener {
  /** Listener for notifying of updates. */
  interface UpdatedListener {
    void sightingsUpdated(Sighting sighting);
    void sightingsSwapped(Sighting oldSighting, Sighting newSighting);
  }

  private Sighting sighting;
  private SightingInfoPanel infoPanel;
  private JButton saveButton;
  private JButton revertButton;
  private JButton editVisitButton;
  private final ReportSet reportSet;
  private WhenPanel whenPanel;
  private WherePanel wherePanel;
  private final UpdatedListener saved;
  private final Taxonomy taxonomy;
  private final PredefinedLocations predefinedLocations;
  private final NewLocationDialog newLocationDialog;
  private SpResolverComboBox spResolver;
  private JLabel species;
  private int lastSavedSpIndex;
  private SpResolverUi spResolverUi;
  private FontManager fontManager;
  private AndDirty dirty;
  private final FileDialogs fileDialogs;
  private final Alerts alerts;
  private SpHybridDialog spHybridDialog;
  private final OpenMapUrl openMapUrl;
  private final Action returnAction;
  private final NavigableFrame navigableFrame;

  public SightingBrowsePanel(
      Taxonomy taxonomy,
      ReportSet reportSet,
      Sighting sighting,
      NewLocationDialog newLocationDialog,
      NavigableFrame navigableFrame,
      Action returnAction,
      SpHybridDialog spHybridDialog,
      PredefinedLocations predefinedLocations,
      OpenMapUrl openMapUrl,
      FontManager fontManager,
      FileDialogs fileDialogs,
      Alerts alerts,
      UpdatedListener onSaved) {
    this.taxonomy = taxonomy;
    this.reportSet = reportSet;
    this.sighting = sighting;
    this.navigableFrame = navigableFrame;
    this.returnAction = returnAction;
    this.spHybridDialog = spHybridDialog;
    this.predefinedLocations = predefinedLocations;
    this.openMapUrl = openMapUrl;
    this.alerts = alerts;
    this.saved = onSaved;
    this.newLocationDialog = newLocationDialog;
    this.fontManager = fontManager;
    this.fileDialogs = fileDialogs;
    initComponents();
    hookUpContents();
  }

  private void hookUpContents() {
    infoPanel.setSighting(sighting.getSightingInfo());
    whenPanel.setWhen(sighting.getDateAsPartial());
    wherePanel.setWhere(reportSet.getLocations().getLocation(sighting.getLocationId()));
    saveButton.setEnabled(false);
    revertButton.setEnabled(false);
    editVisitButton.setEnabled(VisitInfoKey.forSighting(sighting) != null);
    infoPanel.getDirty().setDirty(false);
    whenPanel.getDirty().setDirty(false);
    wherePanel.getDirty().setDirty(false);
    
    dirty = new AndDirty(infoPanel.getDirty(), whenPanel.getDirty(), wherePanel.getDirty());
    if (spResolverUi != null) {
      dirty = new AndDirty(spResolverUi.getDirty(), dirty);
      spResolverUi.clearDirty();
    }
    
    dirty.addDirtyListener(e -> updateButtons());
    wherePanel.addPropertyChangeListener("where", e -> updateButtons());
    
    addAncestorListener(new AncestorListener() {      
      @Override
      public void ancestorRemoved(AncestorEvent e) {
        // Auto-save when the user navigates away.  But *don't* do that when the "sp" resolver has
        // been triggered, as it results in lots of UI nastiness (you can't really revert in a simple
        // way in the resolution UI, it screws up multi-selection, etc.)
        if (spResolverUi.getDirty().isDirty()) {
          return;
        }
        if (dirty.isDirty()) {
          save();
        }
      }
      
      @Override
      public void ancestorMoved(AncestorEvent e) {
      }
      
      @Override
      public void ancestorAdded(AncestorEvent e) {
      }
    });
  }

  /**
   * Save, possibly with an explicit taxon to set for the sighting (for the sp/hybrid path). 
   */
  public void save() {
    Sighting original = sighting;
    Sighting added = null;
    // Invalid location - ignore the fact that it's dirty.
    Location location = wherePanel.getWhere();
    if (location == null && wherePanel.getDirty().isDirty()) {
      // TODO: not ideal that this ignores a non-null forcedSightingTaxon
      return;
    }
    
    // Changing the taxa
    if (spResolverUi != null) {
      SightingTaxon sightingTaxon = spResolverUi.getNewSightingTaxon();
      if (sightingTaxon != null && !sighting.getTaxon().equals(sightingTaxon)) {
        // Keep track of the last-saved index so that "revert" can be properly disabled
        if (spResolver != null) {
          lastSavedSpIndex = spResolver.getSelectedIndex();
        }
        added = sighting.asBuilder().setTaxon(sightingTaxon).build();
        sighting = added;
        
        spResolverUi.clearDirty();
      }
    }
    
    infoPanel.save(sighting.getSightingInfo());
    
    ReportSetMutator mutator = reportSet.mutator();
    if (!Objects.equal(whenPanel.getWhen(), sighting.getDateAsPartial())) {
      added = sighting.asBuilder().setDate(whenPanel.getWhen()).build();
      sighting = added;
      
      mutator.withChangedDate(sighting.getDateAsPartial());
    }
    
    if (location != null) {
      // If there's no ID, it's a new location - register it
      reportSet.getLocations().ensureAdded(location);
      
      if (!Objects.equal(location.getId(), sighting.getLocationId())) {
        // ... and store the location.
        added = sighting.asBuilder().setLocation(location).build();
        sighting = added;
        
        mutator.withChangedLocation(location.getId());
      }
    }
    
    if (added != null) {
      mutator
          .removing(ImmutableList.of(original))
          .adding(ImmutableList.of(added))
          .mutate();
      saved.sightingsSwapped(original, added);
    } else {
      // Sightings changed inline
      reportSet.markDirty();
      saved.sightingsUpdated(sighting);
    }
    
    infoPanel.getDirty().setDirty(false);
    whenPanel.getDirty().setDirty(false);
    wherePanel.getDirty().setDirty(false);
  }
  
  public void revert() {
    infoPanel.revert();
    
    whenPanel.setWhen(sighting.getDateAsPartial());
    wherePanel.setWhere(reportSet.getLocations().getLocation(sighting.getLocationId()));
    whenPanel.getDirty().setDirty(false);
    wherePanel.getDirty().setDirty(false);

    // And reset the saved index of the spResolver, if any 
    if (spResolver != null) {
      spResolver.setSelectedIndex(lastSavedSpIndex);
    }
    
    // Revert the spResolverUI
    spResolverUi.revert();
  }

  private void initComponents() {
    species = new JLabel();
    String speciesText = getSpeciesText();
    if (Strings.isNullOrEmpty(speciesText)) {
      species.setVisible(false);
    } else {
      // Use <html> so that it'll wrap at all;  and add <wbr>'s for extra
      // wrapping with long subspecies names
      species.setText("<html>" + speciesText.replace("/", "/<wbr>"));
    }
    
    spResolver = getSpResolver();
    spResolverUi = new SpResolverUi(spResolver, alerts, spHybridDialog);
    if (spResolver != null) {
      spResolver.addActionListener(e -> updateButtons());
      lastSavedSpIndex = spResolver.getSelectedIndex();
    }
    
    if (spResolverUi.spHybridButton != null) {
      Resolved resolved = sighting.getTaxon().resolve(taxonomy);
      if (resolved.getType() != SightingTaxon.Type.SINGLE) {
        spResolverUi.configureSpHybridButtonForExistingSpOrHybrid(resolved.getSightingTaxon());
      } else {
        spResolverUi.configureSpHybridButton(resolved.getTaxon());
      }
    }

    whenPanel = new WhenPanel(fontManager);
    wherePanel = new WherePanel(reportSet, newLocationDialog, predefinedLocations, openMapUrl, "(None)");
    wherePanel.setWhereLayoutStrategy(IndexerPanel.LayoutStrategy.BELOW);
    
    infoPanel = new SightingInfoPanel(reportSet, SightingInfoPanel.Layout.VERTICAL,
        fontManager, fileDialogs, alerts);
    
    saveButton = new JButton();
    revertButton = new JButton();
    editVisitButton = new JButton();
    
    saveButton.setAction(new AbstractAction(Messages.getMessage(Name.SAVE_BUTTON)) {
      @Override public void actionPerformed(ActionEvent e) {
        save();
      }
    });
    
    revertButton.setAction(new AbstractAction(Messages.getMessage(Name.REVERT_BUTTON)) {
      @Override public void actionPerformed(ActionEvent event) {
        revert();
      }
    });
    
    editVisitButton.setAction(new AbstractAction(Messages.getMessage(Name.VISIT_SIGHTINGS)) {
      @Override public void actionPerformed(ActionEvent event) {
        save();
        VisitInfoKey visitInfoKey = VisitInfoKey.forSighting(sighting);
        if (visitInfoKey != null) {
          JPanel sightingsPanel = navigableFrame.navigateToAndPush("sightings", returnAction);
          if (sightingsPanel != null) {
            ((NavigablePanel) sightingsPanel).navigateTo(visitInfoKey);
          }
        }
      }
    });
  }

  private String getSpeciesText() {
    Resolved resolved = sighting.getTaxon().resolve(taxonomy);
    return resolved.getPreferredSingleName();
  }

  private void updateButtons() {
    boolean isDirty = infoPanel.getDirty().isDirty()
        || whenPanel.getDirty().isDirty()
        || wherePanel.getDirty().isDirty()
        || spResolverUi.getDirty().isDirty();
    // The where panel is valid if it has a value, or if it's not dirty -
    // the latter explicitly handles cases where it started empty, and is still
    // empty.
    boolean whereIsValid = (wherePanel.getWhere() != null) || !wherePanel.getDirty().isDirty();
    saveButton.setEnabled(isDirty && whereIsValid);
    revertButton.setEnabled(isDirty);
    editVisitButton.setEnabled(
        wherePanel.getWhere() != null && whenPanel.getWhen() != null);
  }

  public void setWhereVisible(boolean whereVisible) {
    wherePanel.setVisible(whereVisible);
  }

  public void setEditable(boolean editable) {
    saveButton.setVisible(editable);
    revertButton.setVisible(editable);
    whenPanel.setEditable(editable);
    wherePanel.setEditable(editable);
    infoPanel.setEditable(editable);
    spResolverUi.spLabel.setVisible(editable);
    spResolverUi.spInfoPanel.setVisible(editable);
    if (spResolverUi.spHybridButton != null) {
      spResolverUi.spHybridButton.setVisible(editable);
    }
    if (spResolver != null) {
      spResolver.setVisible(editable);
    }
  }

  private SpResolverComboBox getSpResolver() {
    Resolved resolved = sighting.getTaxon().resolve(taxonomy);
    return SpResolverComboBox.forResolved(resolved);
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHonorsVisibility(true);
    
    SequentialGroup verticalGroup = layout.createSequentialGroup();
    verticalGroup
        .addComponent(species)
        .addComponent(spResolverUi.spLabel)
        .addComponent(spResolverUi.spResolverPanel, GroupLayout.PREFERRED_SIZE,
            GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE);
    // Not a lot of room once users are added; trim out the range info
    if (reportSet.getUserSet() == null) {
      verticalGroup.addComponent(spResolverUi.spInfoPanel, GroupLayout.PREFERRED_SIZE,
          GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE);
    }
    verticalGroup
        .addComponent(spResolverUi.separator, 5, 5, GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(whenPanel)
        .addComponent(wherePanel)
        .addComponent(infoPanel)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(saveButton)
            .addComponent(revertButton)
            .addComponent(editVisitButton))
        .addGap(0, fontManager.scale(20), fontManager.scale(20));
    layout.setVerticalGroup(verticalGroup);
    
    ParallelGroup horizontalGroup = layout.createParallelGroup(Alignment.LEADING);
    horizontalGroup
        .addComponent(species)
        .addComponent(spResolverUi.spLabel)
        .addComponent(spResolverUi.spResolverPanel, GroupLayout.PREFERRED_SIZE,
            fontManager.scale(300), fontManager.scale(300));
    // Not a lot of room once users are added; trim out the range info
    if (reportSet.getUserSet() == null) {
      horizontalGroup
          .addGroup(layout.createSequentialGroup()
          .addGap(5)
          .addComponent(spResolverUi.spInfoPanel, GroupLayout.PREFERRED_SIZE,
              fontManager.scale(245), fontManager.scale(245)));
    }
    horizontalGroup
        .addComponent(spResolverUi.separator)
        .addComponent(whenPanel)
        .addComponent(wherePanel)
        .addComponent(infoPanel)
        .addGroup(layout.createParallelGroup(Alignment.CENTER)
            .addGroup(layout.createSequentialGroup()
                .addComponent(saveButton)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(revertButton)
                .addPreferredGap(ComponentPlacement.UNRELATED)
                .addComponent(editVisitButton)));
    layout.setHorizontalGroup(horizontalGroup);
  }
}
