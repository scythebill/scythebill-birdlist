/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import java.awt.Component;
import java.awt.Dialog.ModalityType;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.ReportSetMutator;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.components.SightingBulkEditPanel.UpdatedListener;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.OpenMapUrl;
import com.scythebill.birdlist.ui.util.UIUtils;

/** Dialog for triggering sighting bulk editing. */
public class SightingBulkEditDialog {
  private final FontManager fontManager;
  private final Alerts alerts;
  private final PredefinedLocations predefinedLocations;
  private final NewLocationDialog newLocationDialog;
  private final OpenMapUrl openMapUrl;

  @Inject
  public SightingBulkEditDialog(
      FontManager fontManager,
      Alerts alerts,
      NewLocationDialog newLocationDialog,
      PredefinedLocations predefinedLocations,
      OpenMapUrl openMapUrl) {
    this.fontManager = fontManager;
    this.alerts = alerts;
    this.newLocationDialog = newLocationDialog;
    this.predefinedLocations = predefinedLocations;
    this.openMapUrl = openMapUrl;
  }
  
  /**
   * Request bulk editing of sightings.
   */
  public void bulkEditSightings(
      Window parent,
      SightingBulkEditPanel.UpdatedListener onSaved,
      Taxonomy taxonomy,
      ReportSet reportSet,
      List<Sighting> sightings) {
    
    SightingBulkEditPanel sightingBulkEditPanel = new SightingBulkEditPanel(
        taxonomy, reportSet, sightings, onSaved, alerts,
        fontManager,
        new WherePanel(reportSet, newLocationDialog, predefinedLocations, openMapUrl, ""),
        null);
    sightingBulkEditPanel.setInlineButtonsVisible(false);
    showBulkEditDialog(parent, sightingBulkEditPanel, sightings, reportSet, onSaved);
  }


  private void showBulkEditDialog(
      final Window parent,
      final SightingBulkEditPanel sightingBulkEditPanel,
      final List<Sighting> sightings,
      final ReportSet reportSet,
      final UpdatedListener onSaved) {
    final int size = sightings.size();
    ModalityType modality =
        Toolkit.getDefaultToolkit().isModalityTypeSupported(ModalityType.DOCUMENT_MODAL)
        ? ModalityType.DOCUMENT_MODAL : ModalityType.APPLICATION_MODAL;
    final JDialog dialog = new JDialog(
        parent, Messages.getMessage(Name.BULK_EDIT_TITLE), modality);
    Action cancelAction = new AbstractAction() {
      @Override public void actionPerformed(ActionEvent e) {
        dialog.dispose();
      }
    }; 
    final Action okAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent event) {
        int okCancel = JOptionPane.OK_OPTION;
        // If this is a large update, ask the user before proceeding.
        if (size > 10) {
          okCancel = alerts.showOkCancel(parent,
              Name.ARE_YOU_SURE,
              Name.VERIFY_CHANGE_ALL_SIGHTINGS_FORMAT,
              size);
        }
        if (okCancel == JOptionPane.OK_OPTION) {
          sightingBulkEditPanel.save();
        }          
        dialog.dispose();
      }
    };
    final Action deleteAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent event) {
        int okCancel = alerts.showOkCancel(parent,
            Name.ARE_YOU_SURE,
            Name.VERIFY_DELETE_ALL_SIGHTINGS_FORMAT,
            size);
        if (okCancel == JOptionPane.OK_OPTION) {
          ReportSetMutator mutator = reportSet.mutator();
          mutator.removing(sightings);
          mutator.mutate();
          onSaved.sightingsRemoved(sightings);
        }          
        dialog.dispose();
      }
    };
    
    sightingBulkEditPanel.getDirty().addDirtyListener(new PropertyChangeListener() {
      @Override public void propertyChange(PropertyChangeEvent evt) {
        boolean isDirty = (Boolean) evt.getNewValue();
        okAction.setEnabled(isDirty);
      }
    });
    okAction.setEnabled(false);
    
    fontManager.applyTo(sightingBulkEditPanel);
    
    JPanel bulkEditWithDescription = new JPanel();
    bulkEditWithDescription.setLayout(new BoxLayout(bulkEditWithDescription, BoxLayout.PAGE_AXIS));
    JLabel label = new JLabel(
        Messages.getFormattedMessage(Name.EDIT_ALL_SIGHTINGS_FORMAT, sightings.size()));
    bulkEditWithDescription.add(label);
    bulkEditWithDescription.add(Box.createVerticalStrut(10));
    bulkEditWithDescription.add(sightingBulkEditPanel);
    label.setAlignmentX(0.0f);
    sightingBulkEditPanel.setAlignmentX(0.0f);
    
    BulkEditActionsPanel okCancel = new BulkEditActionsPanel(
        okAction, deleteAction, cancelAction, bulkEditWithDescription);
    fontManager.applyTo(okCancel);
    dialog.setContentPane(okCancel);
    dialog.pack();
    dialog.setVisible(true);
  }

  static class BulkEditActionsPanel extends JPanel {
    private JButton okButton;

    public BulkEditActionsPanel(
         Action okAction,
         Action deleteAction,
         Action cancelAction,
         Component content) {
      setBorder(new EmptyBorder(20, 20, 15, 20));
      GroupLayout layout = new GroupLayout(this);
      setLayout(layout);
      JSeparator separator = new JSeparator();
      
      JButton cancelButton = new JButton();
      cancelButton.setAction(cancelAction);
      cancelButton.setText(Messages.getMessage(Name.CANCEL_BUTTON));
      
      JButton deleteButton = new JButton();
      deleteButton.setAction(deleteAction);
      deleteButton.setText(Messages.getMessage(Name.DELETE_ALL));
      
      okButton = new JButton(okAction);
      okButton.setText(Messages.getMessage(Name.OK_BUTTON));
      
      JPanel spacer = new JPanel();
      
      layout.setAutoCreateContainerGaps(true);

      layout.setHorizontalGroup(layout.createParallelGroup()
          .addComponent(content)
          .addComponent(spacer)
          .addComponent(separator)
          .addGroup(Alignment.LEADING, layout.createSequentialGroup()
              .addComponent(cancelButton)
              .addGap(0, 75, Short.MAX_VALUE)
              .addComponent(deleteButton)
              .addPreferredGap(ComponentPlacement.UNRELATED)
              .addComponent(okButton)));
      layout.setVerticalGroup(layout.createSequentialGroup()
          .addComponent(content)
          .addGap(18)
          .addComponent(spacer, 0, 0, Short.MAX_VALUE)
          .addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGroup(layout.createParallelGroup()
              .addComponent(cancelButton)
              .addComponent(deleteButton)
              .addComponent(okButton)));
      layout.linkSize(cancelButton, okButton);
      
      UIUtils.focusOnEntry(this, okButton);
      
      KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
      getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
          .put(escape, "cancelPanel");
      getActionMap().put("cancelPanel", cancelAction);
      
    }
  }
}
