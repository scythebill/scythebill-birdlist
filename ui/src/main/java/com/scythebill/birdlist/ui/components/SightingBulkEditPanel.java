/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;
import javax.swing.AbstractAction;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.joda.time.DateTimeFieldType;
import org.joda.time.Partial;
import org.joda.time.ReadablePartial;
import org.joda.time.chrono.GJChronology;

import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.ReportSetMutator;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.user.User;
import com.scythebill.birdlist.model.util.AndDirty;
import com.scythebill.birdlist.model.util.Dirty;
import com.scythebill.birdlist.model.util.DirtyImpl;
import com.scythebill.birdlist.ui.components.IndexerPanel.LayoutStrategy;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.SpHybridDialog;
import com.scythebill.birdlist.ui.panels.UserChipsUi;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * Panel for displaying/updating sighting information from within the TaxonBrowser
 * (or anywhere else).
 * 
 * NOTE: before allowing the date to be updated, consider all recipients
 * of sightingsUpdated/sightingsSwapped!
 */
public class SightingBulkEditPanel extends JPanel implements FontsUpdatedListener {
  /** Listener for notifying of bulk updates. */
  public interface UpdatedListener {
    void sightingsUpdated(List<Sighting> sightings);
    void sightingsSwapped(List<Sighting> oldSightings, List<Sighting> newSightings);
    void sightingsRemoved(List<Sighting> sightings);
  }
  
  private WhenPanel whenPanel;
  private SightingBulkInfoPanel infoPanel;
  private JButton saveButton;
  private JButton revertButton;
  private final ReportSet reportSet;
  private final UpdatedListener saved;
  private final Taxonomy taxonomy;
  private final FontManager fontManager;
  private JLabel subspecies;
  private List<Sighting> sightings;
  private SpResolverComboBox spResolver;
  private int lastSavedSpIndex;
  private SpResolverUi spResolverUi;
  private UserChipsUi addedUsersChipsUi;
  private UserChipsUi removedUsersChipsUi;
  private Dirty dirty;
  @Nullable
  private WherePanel wherePanel;
  private final Component wherePanelOrPlaceholder;
  private final Alerts alerts;
  @Nullable
  private final SpHybridDialog spHybridDialog;

  public SightingBulkEditPanel(
      Taxonomy taxonomy,
      ReportSet reportSet,
      List<Sighting> sightings,
      UpdatedListener onSaved,
      Alerts alerts,
      FontManager fontManager,
      WherePanel wherePanel,
      @Nullable SpHybridDialog spHybridDialog) {
    this.alerts = alerts;
    this.fontManager = fontManager;
    this.wherePanel = wherePanel;
    this.spHybridDialog = spHybridDialog;
    if (wherePanel == null) {
      wherePanelOrPlaceholder = new JPanel();
      wherePanelOrPlaceholder.setPreferredSize(new Dimension(0, 0));
    } else {
      wherePanelOrPlaceholder = wherePanel;
    }
    Preconditions.checkArgument(sightings.size() > 1);
    this.taxonomy = taxonomy;
    this.reportSet = reportSet;
    this.sightings = sightings;
    this.saved = onSaved;
    initComponents();
    hookUpContents();
  }
  
  /** Returns the dirty status of the panel. */
  public Dirty getDirty() {
    return dirty;
  }

  private void hookUpContents() {
    saveButton.setEnabled(false);
    revertButton.setEnabled(false);
    infoPanel.getDirty().setDirty(false);
    whenPanel.getDirty().setDirty(false);
    dirty = new AndDirty(
        infoPanel.getDirty(), whenPanel.getDirty());
    if (wherePanel != null) {
      wherePanel.getDirty().setDirty(false);
      dirty = new AndDirty(dirty, wherePanel.getDirty());
    }
    spResolverUi.clearDirty();
    dirty = new AndDirty(dirty, spResolverUi.getDirty());
    dirty.addDirtyListener(e -> updateSaveAndRevertButton());
    if (addedUsersChipsUi != null) {
      DirtyImpl addedDirty = new DirtyImpl(false);
      addedUsersChipsUi.userChips.addChipsChangedListener(() -> {
        addedDirty.setDirty(!addedUsersChipsUi.userChips.getChips().isEmpty());
        updateSaveAndRevertButton();
      });
      dirty = new AndDirty(dirty, addedDirty);
    }
    if (removedUsersChipsUi != null) {
      DirtyImpl removedDirty = new DirtyImpl(false);
      removedUsersChipsUi.userChips.addChipsChangedListener(() -> {
        removedDirty.setDirty(!removedUsersChipsUi.userChips.getChips().isEmpty());
        updateSaveAndRevertButton();
      });
      dirty = new AndDirty(dirty, removedDirty);
    }
  }

  /**
   * Shows or hides the save and revert buttons.
   */
  public void setInlineButtonsVisible(boolean inlineButtonsVisible) {
    saveButton.setVisible(inlineButtonsVisible);
    revertButton.setVisible(inlineButtonsVisible);
  }
  
  /**
   * Saves the current update.
   */
  public void save() {
    List<Sighting> original = Lists.newArrayList(sightings);
    List<Sighting> added = null;
    
    // Changing the taxa
    SightingTaxon newTaxon = spResolverUi.getNewSightingTaxon();
    if (newTaxon != null
        && sightings.stream().anyMatch(s -> !s.getTaxon().equals(newTaxon))) {
      if (spResolver != null) {
        // Keep track of the last-saved index so that "revert" can be properly disabled
        lastSavedSpIndex = spResolver.getSelectedIndex();        
      }
      
      added = Lists.newArrayList();
      for (Sighting sighting : sightings) {
        added.add(sighting.asBuilder().setTaxon(newTaxon).build());
      }
      sightings = added;
      spResolverUi.clearDirty();
    }
    
    infoPanel.save(sightings);
    
    // Modify users, if necessary
    if (reportSet.getUserSet() != null) {
      if (!addedUsersChipsUi.userChips.getChips().isEmpty()
          || !removedUsersChipsUi.userChips.getChips().isEmpty()) {
        Collection<User> addedUsers = addedUsersChipsUi.userChips.getChips();
        Collection<User> removedUsers = removedUsersChipsUi.userChips.getChips();
        // Use a cache, since user state has a limited set of possibilities.
        LoadingCache<ImmutableSet<User>, ImmutableSet<User>> modifiedUsersCache = CacheBuilder.newBuilder()
            .build(new CacheLoader<ImmutableSet<User>, ImmutableSet<User>>() {
              @Override
              public ImmutableSet<User> load(ImmutableSet<User> original) {
                LinkedHashSet<User> users = new LinkedHashSet<>(original);
                users.addAll(addedUsers);
                users.removeAll(removedUsers);
                return ImmutableSet.copyOf(users);
              }
            });

        for (Sighting sighting : sightings) {
          sighting.getSightingInfo()
              .setUsers(modifiedUsersCache.getUnchecked(sighting.getSightingInfo().getUsers()));
        }
      }

      addedUsersChipsUi.userChips.clear();
      removedUsersChipsUi.userChips.clear();
    }
    
    ReportSetMutator mutator = reportSet.mutator();
    
    if (whenPanel.getDirty().isDirty()) {
      ReadablePartial updatedWhen = whenPanel.getWhen();
      if (updatedWhen != null) {
        added = Lists.newArrayList();
        for (Sighting sighting : sightings) {
          Partial sightingWhen;
          if (sighting.getDateAsPartial() == null) {
            sightingWhen = new Partial(GJChronology.getInstance());
          } else {
            sightingWhen = new Partial(sighting.getDateAsPartial());
          }
          
          if (updatedWhen.isSupported(DateTimeFieldType.year())) {
            sightingWhen = sightingWhen.with(
                DateTimeFieldType.year(), updatedWhen.get(DateTimeFieldType.year()));
          }
          if (updatedWhen.isSupported(DateTimeFieldType.monthOfYear())) {
            sightingWhen = sightingWhen.with(
                DateTimeFieldType.monthOfYear(), updatedWhen.get(DateTimeFieldType.monthOfYear()));
          }
          if (updatedWhen.isSupported(DateTimeFieldType.dayOfMonth())) {
            sightingWhen = sightingWhen.with(
                DateTimeFieldType.dayOfMonth(), updatedWhen.get(DateTimeFieldType.dayOfMonth()));
          }
        
          added.add(sighting.asBuilder().setDate(sightingWhen).build());
        }
        sightings = added;
        
        mutator.withChangedDate(updatedWhen);
      }
      whenPanel.getDirty().setDirty(false);      
    }
    
    if (wherePanel != null && wherePanel.getDirty().isDirty()) {
      Location location = wherePanel.getWhere();
      if (location != null) {
        // Make sure the location has an ID
        reportSet.getLocations().ensureAdded(location);

        // Update each sighting's location
        added = Lists.newArrayList();
        for (Sighting sighting : sightings) {
          added.add(sighting.asBuilder().setLocation(location).build());
        }
        sightings = added;
        
        mutator.withChangedLocation(location.getId());
      }      
    }
    
    if (added != null) {
      mutator.removing(original).adding(added).mutate();
      saved.sightingsSwapped(original, added);
    } else {
      saved.sightingsUpdated(sightings);
      reportSet.markDirty();
    }
    
    updateSaveAndRevertButton();
  }
  
  public void revert() {
    infoPanel.revert(sightings);
    whenPanel.setWhen(mergePartials(sightings));
    whenPanel.getDirty().setDirty(false);
    
    if (wherePanel != null) {
      Location location = mergeLocations(sightings);
      wherePanel.setWhere(location);
      wherePanel.getDirty().setDirty(false);
    }

    if (spResolver != null) {
      spResolver.setSelectedIndex(lastSavedSpIndex);
    }
    
    // Revert the spResolverUI
    spResolverUi.revert();
    // And re-disable the sp-hybrid button, if necessary
    if (spResolverUi.spHybridButton != null) {
      Taxon species = getCommonSpecies();
      if (species == null) {
        spResolverUi.spHybridButton.setEnabled(false);
      }
    }

    if (addedUsersChipsUi != null) {
      addedUsersChipsUi.userChips.clear();
    }
    if (removedUsersChipsUi != null) {
      removedUsersChipsUi.userChips.clear();
    }
    updateSaveAndRevertButton();
  }

  private void initComponents() {
    subspecies = new JLabel();
    String subspeciesText = getSubspeciesText();
    if (Strings.isNullOrEmpty(subspeciesText)) {
      subspecies.setVisible(false);
    } else {
      // Use <html> so that it'll wrap at all;  and add <wbr>'s for extra
      // wrapping with long subspecies names
      subspecies.setText("<html>" + subspeciesText.replace("/", "/<wbr>"));
    }

    spResolver = getSpResolver();
    spResolverUi = new SpResolverUi(spResolver, alerts, spHybridDialog);
    if (spResolver != null) {
      spResolver.addActionListener(e -> updateSaveAndRevertButton());
      lastSavedSpIndex = spResolver.getSelectedIndex();
    }
    
    if (spResolverUi.spHybridButton != null) {
      Taxon species = getCommonSpecies();
      if (species == null) {
        spResolverUi.spHybridButton.setEnabled(false);      
      } else {
        spResolverUi.configureSpHybridButton(species);
      }
    }
    
    infoPanel = new SightingBulkInfoPanel();
    infoPanel.setSightings(sightings);
    
    whenPanel = new WhenPanel(fontManager);

    Partial bulkPartial = mergePartials(sightings);
    whenPanel.setWhen(bulkPartial);
    
    if (wherePanel != null) {
      wherePanel.setLayoutStrategy(LayoutStrategy.BELOW);
      wherePanel.setWhere(mergeLocations(sightings));
    }
    
    if (reportSet.getUserSet() != null) {
      addedUsersChipsUi = new UserChipsUi(reportSet.getUserSet(), fontManager);
      removedUsersChipsUi = new UserChipsUi(reportSet.getUserSet(), fontManager);
      removedUsersChipsUi.addUserButton.setText(
          Messages.getMessage(Name.REMOVE_BUTTON));
    }
    
    saveButton = new JButton();
    revertButton = new JButton();
    
    saveButton.setAction(new AbstractAction(Messages.getMessage(Name.SAVE_BUTTON)) {
      @Override public void actionPerformed(ActionEvent e) {
        save();
      }
    });
    
    revertButton.setAction(new AbstractAction(Messages.getMessage(Name.REVERT_BUTTON)) {
      @Override public void actionPerformed(ActionEvent event) {
        revert();
      }
    });    
  }

  private Location mergeLocations(List<Sighting> sightingsToMerge) {
    String locationId = sightingsToMerge.get(0).getLocationId();
    for (Sighting sighting : Iterables.skip(sightingsToMerge, 1)) {
      if (!Objects.equal(locationId, sighting.getLocationId())) {
        return null;
      }
    }
    if (locationId == null) {
      return null;
    }
    
    return reportSet.getLocations().getLocation(locationId);
  }
  
  private static Partial mergePartials(List<Sighting> sightingsToMerge) {
    ReadablePartial first = sightingsToMerge.get(0).getDateAsPartial();
    Optional<Integer> day = first != null && first.isSupported(DateTimeFieldType.dayOfMonth())
        ? Optional.of(first.get(DateTimeFieldType.dayOfMonth())) : Optional.<Integer>absent();
    Optional<Integer> month = first != null && first.isSupported(DateTimeFieldType.monthOfYear())
        ? Optional.of(first.get(DateTimeFieldType.monthOfYear())) : Optional.<Integer>absent();
    Optional<Integer> year = first != null && first.isSupported(DateTimeFieldType.year())
        ? Optional.of(first.get(DateTimeFieldType.year())) : Optional.<Integer>absent();
    
    for (Sighting sighting : Iterables.skip(sightingsToMerge, 1)) {
      ReadablePartial partial = sighting.getDateAsPartial();
      if (partial == null) {
        continue;
      }
      
      if (day.isPresent()) {
        if (!partial.isSupported(DateTimeFieldType.dayOfMonth())
            || partial.get(DateTimeFieldType.dayOfMonth()) != day.get()) {
          day = Optional.absent();
        }
      }
      if (month.isPresent()) {
        if (!partial.isSupported(DateTimeFieldType.monthOfYear())
            || partial.get(DateTimeFieldType.monthOfYear()) != month.get()) {
          month = Optional.absent();
        }
      }
      if (year.isPresent()) {
        if (!partial.isSupported(DateTimeFieldType.year())
            || partial.get(DateTimeFieldType.year()) != year.get()) {
          year = Optional.absent();
        }
      }
    }
    
    Partial bulkPartial = new Partial(GJChronology.getInstance());
    if (year.isPresent()) {
      bulkPartial = bulkPartial.with(DateTimeFieldType.year(), year.get());
    }
    if (month.isPresent()) {
      bulkPartial = bulkPartial.with(DateTimeFieldType.monthOfYear(), month.get());
    }
    if (day.isPresent()) {
      bulkPartial = bulkPartial.with(DateTimeFieldType.dayOfMonth(), day.get());
    }
    return bulkPartial;
  }

  private SpResolverComboBox getSpResolver() {
    Set<Resolved> resolveds = Sets.newHashSet();
    for (Sighting sighting : sightings) {
      resolveds.add(sighting.getTaxon().resolve(taxonomy));
    }
    if (resolveds.size() > 1) {
      return null;
    }
    
    Resolved resolved = Iterables.getOnlyElement(resolveds);
    return SpResolverComboBox.forResolved(resolved);
  }
  
  /** If all sightings have a common substring, show it. */
  private String getSubspeciesText() {
    String subspeciesText = getSubspeciesText(sightings.get(0));
    if (subspeciesText == null) {
      return null;
    }
    for (Sighting sighting : Iterables.skip(sightings, 1)) {
      if (!subspeciesText.equals(getSubspeciesText(sighting))) {
        return null;
      }
    }
    return subspeciesText;
  }
  
  private String getSubspeciesText(Sighting sighting) {
    Resolved resolved = sighting.getTaxon().resolve(taxonomy);
    if (resolved.getSmallestTaxonType() != Taxon.Type.species
        || resolved.getSightingTaxon().getType() != SightingTaxon.Type.SINGLE) {
      return resolved.getCommonName();
    }
    
    return null;    
  }

  /**
   * Returns the shared species among all sightings, or null if
   * there is no common species.
   */
  private Taxon getCommonSpecies() {
    Set<String> taxa = new LinkedHashSet<>();
    for (Sighting sighting : sightings) {
      taxa.addAll(sighting.getTaxon().getIds());
    }
    
    Resolved resolved = SightingTaxons.newPossiblySpTaxon(taxa).resolve(taxonomy);
    SightingTaxon speciesTaxon = resolved.getParentOfAtLeastType(Taxon.Type.species);
    if (speciesTaxon.getType() != SightingTaxon.Type.SINGLE) {
      return null;
    }
    
    return taxonomy.getTaxon(speciesTaxon.getId());
  }

  private void updateSaveAndRevertButton() {
    boolean isDirty = infoPanel.getDirty().isDirty()
        || whenPanel.getDirty().isDirty()
        || (wherePanel != null && wherePanel.getDirty().isDirty())
        || spResolverUi.getDirty().isDirty();
    if (addedUsersChipsUi != null) {
      isDirty = isDirty || !addedUsersChipsUi.userChips.getChips().isEmpty();
    }
    if (removedUsersChipsUi != null) {
      isDirty = isDirty || !removedUsersChipsUi.userChips.getChips().isEmpty();
    }
    saveButton.setEnabled(isDirty);
    revertButton.setEnabled(isDirty);
  }

  public void setEditable(boolean editable) {
    saveButton.setVisible(editable);
    revertButton.setVisible(editable);
    infoPanel.setEditable(editable);
    whenPanel.setEditable(editable);
    if (wherePanel != null) {
      wherePanel.setEditable(editable);
    }
    spResolverUi.spLabel.setVisible(editable);
    spResolverUi.spInfoPanel.setVisible(editable);
    if (spResolverUi.spHybridButton != null) {
      spResolverUi.spHybridButton.setVisible(editable);
    }
    if (spResolver != null) {
      spResolver.setVisible(editable);
    }
    
    if (addedUsersChipsUi != null) {
      addedUsersChipsUi.addUserButton.setVisible(editable);
      addedUsersChipsUi.userIndexer.setVisible(editable);
      addedUsersChipsUi.userChips.setEnabled(editable);
    }

    if (removedUsersChipsUi != null) {
      removedUsersChipsUi.addUserButton.setVisible(editable);
      removedUsersChipsUi.userIndexer.setVisible(editable);
      removedUsersChipsUi.userChips.setEnabled(editable);
    }
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHonorsVisibility(true);
    
    if (reportSet.getUserSet() == null) {
      SequentialGroup verticalGroup = layout.createSequentialGroup()
          .addComponent(subspecies)
          .addComponent(spResolverUi.spLabel)
          .addComponent(spResolverUi.spResolverPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
          .addComponent(spResolverUi.spInfoPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
          .addComponent(spResolverUi.separator, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addComponent(whenPanel);
      if (wherePanel == null) {
        verticalGroup.addComponent(wherePanelOrPlaceholder, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE);
      } else {
        verticalGroup.addComponent(wherePanelOrPlaceholder);
      }
      verticalGroup
          .addComponent(infoPanel)
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addGroup(layout.createBaselineGroup(false, false)
              .addComponent(saveButton)
              .addComponent(revertButton));
      layout.setVerticalGroup(verticalGroup);

      layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
          .addComponent(subspecies)
          .addComponent(spResolverUi.spLabel)
          .addComponent(spResolverUi.spResolverPanel, GroupLayout.PREFERRED_SIZE,
              fontManager.scale(300), fontManager.scale(300))
          .addComponent(spResolverUi.spInfoPanel, GroupLayout.PREFERRED_SIZE,
              fontManager.scale(250), fontManager.scale(250))
          .addComponent(spResolverUi.separator)
          .addComponent(whenPanel)
          .addComponent(wherePanelOrPlaceholder)
          .addComponent(infoPanel)
          .addGroup(layout.createSequentialGroup()
              .addComponent(saveButton)
              .addComponent(revertButton)));
    } else { // version with users
      SequentialGroup verticalGroup = layout.createSequentialGroup()
          .addComponent(subspecies)
          .addComponent(spResolverUi.spLabel)
          .addComponent(spResolverUi.spResolverPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
          // No spInfoPanel
          .addComponent(spResolverUi.separator, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addComponent(whenPanel);
      if (wherePanel == null) {
        verticalGroup.addComponent(wherePanelOrPlaceholder, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE);
      } else {
        verticalGroup.addComponent(wherePanelOrPlaceholder);
      }
      verticalGroup
          .addComponent(infoPanel)
          .addGroup(layout.createBaselineGroup(false, false)
              .addComponent(addedUsersChipsUi.userIndexer, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
              .addComponent(addedUsersChipsUi.addUserButton, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
          .addComponent(addedUsersChipsUi.userChipsScrollPane, PREFERRED_SIZE, PREFERRED_SIZE, Short.MAX_VALUE)
          .addGroup(layout.createBaselineGroup(false, false)
              .addComponent(removedUsersChipsUi.userIndexer, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
              .addComponent(removedUsersChipsUi.addUserButton, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
          .addComponent(removedUsersChipsUi.userChipsScrollPane, PREFERRED_SIZE, PREFERRED_SIZE, Short.MAX_VALUE)
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addGroup(layout.createBaselineGroup(false, false)
              .addComponent(saveButton)
              .addComponent(revertButton));
      layout.setVerticalGroup(verticalGroup);

      layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
          .addComponent(subspecies)
          .addComponent(spResolverUi.spLabel)
          .addComponent(spResolverUi.spResolverPanel, GroupLayout.PREFERRED_SIZE,
              fontManager.scale(250), fontManager.scale(250))
          // No spInfoPanel
          .addComponent(spResolverUi.separator)
          .addComponent(whenPanel)
          .addComponent(wherePanelOrPlaceholder)
          .addComponent(infoPanel)
          .addGroup(layout.createSequentialGroup()
              .addComponent(addedUsersChipsUi.userIndexer, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
              .addComponent(addedUsersChipsUi.addUserButton, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
          .addComponent(addedUsersChipsUi.userChipsScrollPane)
          .addGroup(layout.createSequentialGroup()
              .addComponent(removedUsersChipsUi.userIndexer, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
              .addComponent(removedUsersChipsUi.addUserButton, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
          .addComponent(removedUsersChipsUi.userChipsScrollPane)
          .addGroup(layout.createSequentialGroup()
              .addComponent(saveButton)
              .addComponent(revertButton)));
    }       
  }
}
