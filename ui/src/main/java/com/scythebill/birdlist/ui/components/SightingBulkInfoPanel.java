/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.annotation.Nullable;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo;
import com.scythebill.birdlist.model.sighting.SightingInfo.BreedingBirdCode;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.model.util.DirtyImpl;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Panel for displaying bulk sighting information (across multiple 
 */
public class SightingBulkInfoPanel extends JPanel implements FontsUpdatedListener {
  private static final List<SightingStatus> SIGHTING_STATUS_VALUES;
  static {
    SIGHTING_STATUS_VALUES = Lists.newArrayList();
    // Add a null at the start (indicating "leave the bulk value alone")
    SIGHTING_STATUS_VALUES.add(null);
    SIGHTING_STATUS_VALUES.addAll(Arrays.asList(SightingStatus.values()));
  }
  private static final List<String> SIGHTING_STATUS_TEXT = Lists.transform(SIGHTING_STATUS_VALUES,
      status -> status == null ? "" : Messages.getText(status));
  
  private static final List<BreedingBirdCode> BREEDING_VALUES;
  static {
    BREEDING_VALUES = Lists.newArrayList();
    // Add a null at the start (indicating "leave the bulk value alone")
    BREEDING_VALUES.add(null);
    BREEDING_VALUES.addAll(Arrays.asList(BreedingBirdCode.values()));
  }
  private static final List<String> BREEDING_TEXT = Lists.transform(BREEDING_VALUES,
        new Function<BreedingBirdCode, String>() {
          @Override public String apply(@Nullable BreedingBirdCode input) {
            if (input == null) {
              return "";
            }
            if (input == BreedingBirdCode.NONE) {
              return Messages.getText(input);
            }
            return input.getId() + " - " + Messages.getText(input);
          }
  });
  
  private OptionalBooleanCombobox female;
  private OptionalBooleanCombobox heardOnly;
  private OptionalBooleanCombobox immature;
  private OptionalBooleanCombobox adult;
  private OptionalBooleanCombobox male;
  private OptionalBooleanCombobox photographed;
  private DirtyImpl dirty = new DirtyImpl(false);
  private JComboBox<String> statusCombo;
  private JComboBox<String> breedingCombo;
  private JLabel femaleLabel;
  private JLabel maleLabel;
  private JLabel immatureLabel;
  private JLabel adultLabel;
  private JLabel photographedLabel;
  private JLabel heardOnlyLabel;
  private JLabel statusLabel;
  private JLabel breedingLabel;
  private boolean editable;

  public SightingBulkInfoPanel() {
    initComponents();
    attachListeners();
  }

  public DirtyImpl getDirty() {
    return dirty;
  }

  @Override
  public void setEnabled(boolean enabled) {
    super.setEnabled(enabled);

    // And set the enabled state of all of our children too
    statusLabel.setEnabled(enabled);
    
    setEditableOrEnabled();    
  }

  public void setEditable(boolean editable) {
    this.editable = editable;
    setEditableOrEnabled();
  }

  private void setEditableOrEnabled() {
    female.setEnabled(editable && isEnabled());
    male.setEnabled(editable && isEnabled());
    immature.setEnabled(editable && isEnabled());
    adult.setEnabled(editable && isEnabled());
    heardOnly.setEnabled(editable && isEnabled());
    photographed.setEnabled(editable && isEnabled());
    statusCombo.setEnabled(editable && isEnabled());
    breedingCombo.setEnabled(editable && isEnabled());
  }
  
  private void attachListeners() {
    ActionListener dirtyAction = e -> dirty.setDirty(true);
    female.addActionListener(dirtyAction);
    male.addActionListener(dirtyAction);
    immature.addActionListener(dirtyAction);
    adult.addActionListener(dirtyAction);
    heardOnly.addActionListener(dirtyAction);
    photographed.addActionListener(dirtyAction);
    statusCombo.addActionListener(dirtyAction);
    breedingCombo.addActionListener(dirtyAction);
  }

  /**
   * Save into a list of sightings.
   */
  public void save(Iterable<Sighting> sightings) {
    for (Sighting sighting : sightings) {
      if (female.getValue().isPresent()) {
        sighting.getSightingInfo().setFemale(female.getValue().get());
      }
      
      if (male.getValue().isPresent()) {
        sighting.getSightingInfo().setMale(male.getValue().get());
      }

      if (immature.getValue().isPresent()) {
        sighting.getSightingInfo().setImmature(immature.getValue().get());
      }

      if (adult.getValue().isPresent()) {
        sighting.getSightingInfo().setAdult(adult.getValue().get());
      }

      if (heardOnly.getValue().isPresent()) {
        sighting.getSightingInfo().setHeardOnly(heardOnly.getValue().get());
      }

      if (photographed.getValue().isPresent()) {
        sighting.getSightingInfo().setPhotographed(photographed.getValue().get());
      }
      
      SightingStatus status = SIGHTING_STATUS_VALUES.get(statusCombo.getSelectedIndex());
      if (status != null) {
        sighting.getSightingInfo().setSightingStatus(status);
      }

      BreedingBirdCode breeding = BREEDING_VALUES.get(breedingCombo.getSelectedIndex());
      if (breeding != null) {
        sighting.getSightingInfo().setBreedingBirdCode(breeding);
      }
    }

    dirty.setDirty(false);
  }

  public void revert(Collection<Sighting> sightings) {
    setSightings(sightings);
  }

  public void setSightings(Collection<Sighting> sightings) {
    // When disconnecting from any sighting, erase all data
    if (sightings.isEmpty()) {
      female.unselect();
      male.unselect();
      immature.unselect();
      adult.unselect();
      heardOnly.unselect();
      photographed.unselect();
      statusCombo.setSelectedIndex(0);
      breedingCombo.setSelectedIndex(0);
      return;
    }

    SightingInfo first = sightings.iterator().next().getSightingInfo();
    Optional<Boolean> isFemale = Optional.of(first.isFemale());   
    Optional<Boolean> isMale = Optional.of(first.isMale());   
    Optional<Boolean> isImmature = Optional.of(first.isImmature());   
    Optional<Boolean> isAdult = Optional.of(first.isAdult());   
    Optional<Boolean> isHeardOnly = Optional.of(first.isHeardOnly());   
    Optional<Boolean> isPhotographed = Optional.of(first.isPhotographed());
    Optional<SightingStatus> status = Optional.of(first.getSightingStatus());
    Optional<BreedingBirdCode> breeding = Optional.of(first.getBreedingBirdCode());
    
    for (Sighting sighting : Iterables.skip(sightings, 1)) {
      SightingInfo info = sighting.getSightingInfo();
      if (isFemale.isPresent() && isFemale.get() != info.isFemale()) {
        isFemale = Optional.absent();
      }
      if (isMale.isPresent() && isMale.get() != info.isMale()) {
        isMale = Optional.absent();
      }
      if (isImmature.isPresent() && isImmature.get() != info.isImmature()) {
        isImmature = Optional.absent();
      }
      if (isAdult.isPresent() && isAdult.get() != info.isAdult()) {
        isAdult= Optional.absent();
      }
      if (isHeardOnly.isPresent() && isHeardOnly.get() != info.isHeardOnly()) {
        isHeardOnly = Optional.absent();
      }
      if (isPhotographed.isPresent() && isPhotographed.get() != info.isPhotographed()) {
        isPhotographed = Optional.absent();
      }
      if (status.isPresent() && status.get() != info.getSightingStatus()) {
        status = Optional.absent();
      }
      if (breeding.isPresent() && breeding.get() != info.getBreedingBirdCode()) {
        breeding = Optional.absent();
      }
    }
    
    female.setValue(isFemale);
    male.setValue(isMale);
    immature.setValue(isImmature);
    adult.setValue(isImmature);
    heardOnly.setValue(isHeardOnly);
    photographed.setValue(isPhotographed);
    
    if (status.isPresent()) {
      statusCombo.setSelectedIndex(SIGHTING_STATUS_VALUES.indexOf(status.get()));
    } else {
      statusCombo.setSelectedIndex(0);
    }

    if (breeding.isPresent()) {
      breedingCombo.setSelectedIndex(BREEDING_VALUES.indexOf(breeding.get()));
    } else {
      breedingCombo.setSelectedIndex(0);
    }
    
    dirty.setDirty(false);
  }

  private void initComponents() {

    female = new OptionalBooleanCombobox();
    male = new OptionalBooleanCombobox();
    immature = new OptionalBooleanCombobox();
    adult = new OptionalBooleanCombobox();
    heardOnly = new OptionalBooleanCombobox();
    photographed = new OptionalBooleanCombobox();

    maleLabel = new JLabel(Messages.getMessage(Name.MALE_LABEL));
    femaleLabel = new JLabel(Messages.getMessage(Name.FEMALE_LABEL));
    immatureLabel = new JLabel(Messages.getMessage(Name.IMMATURE_LABEL));
    adultLabel = new JLabel(Messages.getMessage(Name.ADULT_LABEL));
    heardOnlyLabel = new JLabel(Messages.getMessage(Name.HEARD_ONLY_LABEL));
    photographedLabel = new JLabel(Messages.getMessage(Name.PHOTOGRAPHED_LABEL));

    statusCombo = new JComboBox<>(SIGHTING_STATUS_TEXT.toArray(new String[]{}));
    statusLabel = new JLabel(Messages.getMessage(Name.STATUS_LABEL));

    breedingCombo = new JComboBox<>(BREEDING_TEXT.toArray(new String[]{}));
    breedingLabel = new JLabel(Messages.getMessage(Name.BREEDING_CODE_LABEL));
    breedingCombo.setMaximumRowCount(BREEDING_TEXT.size());
  }

  static class OptionalBooleanCombobox extends JComboBox<Object> {
    private static final ImmutableList<Optional<Boolean>> VALUES =
        ImmutableList.of(Optional.<Boolean>absent(), Optional.of(true), Optional.of(false));
    public OptionalBooleanCombobox() {
      super(new Object[]{"", Messages.getMessage(Name.YES_TEXT), Messages.getMessage(Name.NO_TEXT)});
    }
    
    public void setValue(Optional<Boolean> selected) {
      if (selected.isPresent()) {
        setSelectedIndex(selected.get() ? 1 : 2);
      } else {
        unselect();
      }
    }
    
    public void unselect() {
      setSelectedIndex(0);
    }
    
    public Optional<Boolean> getValue() {
      return VALUES.get(getSelectedIndex());
    }
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHonorsVisibility(true);
    
    layout.setAutoCreateGaps(false);
    
    layout.setVerticalGroup(layout.createSequentialGroup()
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(heardOnlyLabel)
            .addComponent(heardOnly))
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(maleLabel)
            .addComponent(male))
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(femaleLabel)
            .addComponent(female))
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(adultLabel)
            .addComponent(adult))
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(immatureLabel)
            .addComponent(immature))
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(photographedLabel)
            .addComponent(photographed))
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(statusLabel)
            .addComponent(statusCombo))
        .addComponent(breedingLabel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
        .addComponent(breedingCombo, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE));
    layout.setHorizontalGroup(layout.createParallelGroup()
        .addGroup(
            layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup()
                    .addComponent(heardOnlyLabel)
                    .addComponent(maleLabel)
                    .addComponent(femaleLabel)
                    .addComponent(adultLabel)
                    .addComponent(immatureLabel)
                    .addComponent(photographedLabel)
                    .addComponent(statusLabel))
                .addPreferredGap(ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup()
                    .addComponent(heardOnly, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                    .addComponent(male, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                    .addComponent(female, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                    .addComponent(adult, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                    .addComponent(immature, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                    .addComponent(photographed, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                    .addComponent(statusCombo, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)))
        .addComponent(breedingLabel)
        .addComponent(breedingCombo, fontManager.scale(200), fontManager.scale(200), fontManager.scale(200)));
  }  
}
