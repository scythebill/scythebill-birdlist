/*
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.scythebill.birdlist.ui.components;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultFormatterFactory;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.sighting.ApproximateNumber;
import com.scythebill.birdlist.model.sighting.Photo;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo;
import com.scythebill.birdlist.model.sighting.SightingInfo.BreedingBirdCode;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.model.user.UserSet;
import com.scythebill.birdlist.model.util.DirtyImpl;
import com.scythebill.birdlist.ui.components.photos.DefaultPhotoClickedListener;
import com.scythebill.birdlist.ui.components.photos.PhotosListPanel;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.UserChipsUi;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;
import com.scythebill.birdlist.ui.util.MoreFiles;
import com.scythebill.birdlist.ui.util.MoreFiles.CommonSuffixResults;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Panel for displaying sighting information.
 */
public class SightingInfoPanel extends JPanel implements FontsUpdatedListener {
  public enum Layout {
    VERTICAL, HORIZONTAL
  }

  private static final ImmutableList<SightingStatus> SIGHTING_STATUS_VALUES =
      ImmutableList.copyOf(SightingStatus.values());
  private static final List<String> SIGHTING_STATUS_TEXT =
      Lists.transform(SIGHTING_STATUS_VALUES, Messages::getText);
  private static final ImmutableList<BreedingBirdCode> BREEDING_VALUES =
      ImmutableList.copyOf(BreedingBirdCode.values());
  private static final List<String> BREEDING_TEXT = Lists.transform(BREEDING_VALUES, (input) -> {
    if (input == BreedingBirdCode.NONE) {
      return Messages.getText(input);
    }
    return input.getId() + " - " + Messages.getText(input);
  });

  private JTextArea description;
  private JCheckBox female;
  private JCheckBox heardOnly;
  private JCheckBox immature;
  private JCheckBox adult;
  private JCheckBox male;
  private JCheckBox photographed;
  private JFormattedTextField numberField;
  private JLabel numberLabel;

  private DirtyImpl dirty = new DirtyImpl(false);
  private SightingInfo sighting;
  private JLabel statusLabel;
  private JComboBox<String> statusCombo;
  private JLabel breedingLabel;
  private JComboBox<String> breedingCombo;
  private boolean editable;
  private JLabel notesLabel;
  private JScrollPane descriptionScrollPane;
  private PhotosListPanel photosListPanel;

  private Layout layoutType;
  private final DocumentListener dirtyDocument = new DocumentListener() {
    @Override
    public void insertUpdate(DocumentEvent event) {
      dirty.setDirty(true);
      firePropertyChange("value", null, null);
    }

    @Override
    public void removeUpdate(DocumentEvent event) {
      dirty.setDirty(true);
      firePropertyChange("value", null, null);
    }

    @Override
    public void changedUpdate(DocumentEvent event) {
      dirty.setDirty(true);
      firePropertyChange("value", null, null);
    }
  };
  private final Alerts alerts;
  private final UserSet userSet;
  private final ReportSet reportSet;
  private UserChipsUi userChipsUi;

  public SightingInfoPanel(ReportSet reportSet, Layout layout, FontManager fontManager,
      FileDialogs fileDialogs, Alerts alerts) {
    this.reportSet = reportSet;
    this.layoutType = layout;
    this.userSet = reportSet.getUserSet();
    this.alerts = alerts;
    initComponents(fontManager, fileDialogs);
    attachListeners();
  }

  public DirtyImpl getDirty() {
    return dirty;
  }

  @Override
  public void setEnabled(boolean enabled) {
    super.setEnabled(enabled);

    // And set the enabled state of all of our children too
    description.setEnabled(enabled);
    numberField.setEnabled(enabled);

    numberLabel.setEnabled(enabled);
    statusLabel.setEnabled(enabled);

    setEditableOrEnabled();
  }

  public void setEditable(boolean editable) {
    this.editable = editable;
    description.setEditable(editable);
    numberField.setEditable(editable);
    setEditableOrEnabled();
  }

  private void setEditableOrEnabled() {
    female.setEnabled(editable && isEnabled());
    male.setEnabled(editable && isEnabled());
    immature.setEnabled(editable && isEnabled());
    adult.setEnabled(editable && isEnabled());
    heardOnly.setEnabled(editable && isEnabled());
    photographed.setEnabled(editable && isEnabled());
    statusCombo.setEnabled(editable && isEnabled());
    breedingCombo.setEnabled(editable && isEnabled());
    photosListPanel.setEnabled(editable && isEnabled());
    if (userChipsUi != null) {
      userChipsUi.setEnabled(editable && isEnabled());
    }
  }

  private void attachListeners() {
    PropertyChangeListener dirtyProperty = (e) -> {
      if (!Objects.equal(e.getOldValue(), e.getNewValue())) {
        dirty.setDirty(true);
        firePropertyChange("value", null, null);
      }
    };

    ActionListener dirtyAction = (e) -> {
      dirty.setDirty(true);
      firePropertyChange("value", null, null);
    };

    description.getDocument().addDocumentListener(dirtyDocument);
    numberField.addPropertyChangeListener("value", dirtyProperty);
    numberField.addKeyListener(new KeyAdapter() {
      @Override
      public void keyTyped(KeyEvent e) {
        if (e.getKeyChar() == 'h') {
          heardOnly.doClick();
          e.consume();
        }
        if (e.getKeyChar() == 'a') {
          adult.doClick();
          e.consume();
        }
        if (e.getKeyChar() == 'i') {
          immature.doClick();
          e.consume();
        }
        if (e.getKeyChar() == 'm') {
          male.doClick();
          e.consume();
        }
        if (e.getKeyChar() == 'f') {
          female.doClick();
          e.consume();
        }
        if (e.getKeyChar() == 'p') {
          photographed.doClick();
          e.consume();
        }
      }
    });
    photosListPanel.getDirty().addDirtyListener(dirtyProperty);
    photosListPanel.addPropertyChangeListener("photos", e -> {
      photographed.setSelected(!photosListPanel.getPhotos().isEmpty());
      dirty.setDirty(true);
      firePropertyChange("value", null, null);
    });
    female.addActionListener(dirtyAction);
    male.addActionListener(dirtyAction);
    immature.addActionListener(dirtyAction);
    adult.addActionListener(dirtyAction);
    heardOnly.addActionListener(dirtyAction);
    photographed.addActionListener(dirtyAction);
    statusCombo.addActionListener(dirtyAction);
    breedingCombo.addActionListener(dirtyAction);
  }

  /**
   * Save into the last-used sighting.
   */
  public void save() {
    save(sighting);
  }

  /**
   * Save into a sighting info, and clear the dirty status.
   */
  public void save(SightingInfo sightingInfo) {
    writeIntoSightingInfo(sightingInfo);
    dirty.setDirty(false);
  }

  /** Get the current value, not updating any UI state. */
  public SightingInfo getValue() {
    SightingInfo sightingInfo = new SightingInfo();
    writeIntoSightingInfo(sightingInfo);
    return sightingInfo;
  }

  private void writeIntoSightingInfo(SightingInfo sightingInfo) {
    sightingInfo.setDescription(description.getText());

    try {
      numberField.commitEdit();
    } catch (ParseException e) {
      // Ignore, it's too late...
    }

    ApproximateNumber number = (ApproximateNumber) numberField.getValue();
    sightingInfo.setNumber(number);

    sightingInfo.setFemale(female.isSelected());
    sightingInfo.setMale(male.isSelected());
    sightingInfo.setImmature(immature.isSelected());
    sightingInfo.setAdult(adult.isSelected());
    sightingInfo.setHeardOnly(heardOnly.isSelected());
    sightingInfo.setPhotographed(photographed.isSelected());
    sightingInfo.setSightingStatus(SIGHTING_STATUS_VALUES.get(statusCombo.getSelectedIndex()));
    sightingInfo.setBreedingBirdCode(BREEDING_VALUES.get(breedingCombo.getSelectedIndex()));
    sightingInfo.setPhotos(photosListPanel.getPhotos());
    if (userChipsUi != null) {
      sightingInfo.setUsers(userChipsUi.userChips.getChips());
    }
  }

  public void revert() {
    setSighting(sighting);
  }

  public void setSighting(SightingInfo newSighting) {
    // When disconnecting from any sighting, erase all data
    if (newSighting == null) {
      description.setText(null);
      numberField.setValue(null);
      female.setSelected(false);
      male.setSelected(false);
      immature.setSelected(false);
      adult.setSelected(false);
      heardOnly.setSelected(false);
      photographed.setSelected(false);
      statusCombo.setSelectedIndex(0);
      breedingCombo.setSelectedIndex(0);
      photosListPanel.setPhotos(ImmutableList.<Photo>of());
      if (userChipsUi != null) {
        userChipsUi.userChips.clear();
      }
      return;
    }

    description.setText(newSighting.getDescription());
    // ... move the caret back to the start
    description.setCaretPosition(0);

    female.setSelected(newSighting.isFemale());
    male.setSelected(newSighting.isMale());
    immature.setSelected(newSighting.isImmature());
    adult.setSelected(newSighting.isAdult());
    heardOnly.setSelected(newSighting.isHeardOnly());
    photographed.setSelected(newSighting.isPhotographed());

    numberField.setValue(newSighting.getNumber());

    statusCombo.setSelectedIndex(SIGHTING_STATUS_VALUES.indexOf(newSighting.getSightingStatus()));
    breedingCombo.setSelectedIndex(BREEDING_VALUES.indexOf(newSighting.getBreedingBirdCode()));
    photosListPanel.setPhotos(newSighting.getPhotos());
    if (userChipsUi != null) {
      userChipsUi.userChips.clear();
      userChipsUi.userChips.addAllChips(newSighting.getUsers());
    }
    sighting = newSighting;

    dirty.setDirty(false);
  }

  private void initComponents(FontManager fontManager, FileDialogs fileDialogs) {

    female = new JCheckBox();
    male = new JCheckBox();
    immature = new JCheckBox();
    adult = new JCheckBox();
    heardOnly = new JCheckBox();
    photographed = new JCheckBox();

    descriptionScrollPane = new JScrollPane();

    description = new JTextArea();
    descriptionScrollPane.setViewportView(description);
    description.setColumns(layoutType == Layout.HORIZONTAL ? 40 : 20);
    description.setRows(3);
    description.setWrapStyleWord(true);
    description.setLineWrap(true);

    numberField = new AutoSelectJFormattedTextField() {
      @Override
      protected void processFocusEvent(FocusEvent event) {
        // Add and remove the document listener as the field gains and loses focus.
        // Keeping the listener attached all the time cause the field to be marked
        // dirty every time focus enters and leaves, as JFormattedTextField runs
        // the formatter and uses that to set the text on focus changes.
        if (event.getID() == FocusEvent.FOCUS_LOST) {
          numberField.getDocument().removeDocumentListener(dirtyDocument);
        }
        super.processFocusEvent(event);
        if (event.getID() == FocusEvent.FOCUS_GAINED) {
          numberField.getDocument().addDocumentListener(dirtyDocument);
        }
      }

    };

    numberLabel = new JLabel();

    male.setText(Messages.getMessage(Name.MALE_TEXT));
    male.setMnemonic('m');
    female.setText(Messages.getMessage(Name.FEMALE_TEXT));
    female.setMnemonic('f');
    immature.setText(Messages.getMessage(Name.IMMATURE_TEXT));
    immature.setMnemonic('i');
    adult.setText(Messages.getMessage(Name.ADULT_TEXT));
    adult.setMnemonic('a');
    heardOnly.setText(Messages.getMessage(Name.HEARD_ONLY_TEXT));
    heardOnly.setMnemonic('h');
    photographed.setText(Messages.getMessage(Name.PHOTOGRAPHED_TEXT));
    photographed.setMnemonic('p');

    UIUtils.fixTabOnTextArea(description);

    numberField.setColumns(8);
    numberField.setFormatterFactory(
        new DefaultFormatterFactory(new JFormattedTextField.AbstractFormatter() {
          @Override
          public String valueToString(Object value) throws ParseException {
            if (value == null) {
              return "";
            }

            return value.toString();
          }

          @Override
          public Object stringToValue(String text) throws ParseException {
            return ApproximateNumber.tryParse(text);
          }
        }));
    numberLabel.setText(Messages.getMessage(Name.NUMBER_LABEL));

    statusCombo = new JComboBox<String>(SIGHTING_STATUS_TEXT.toArray(new String[] {}));
    statusCombo.setMaximumRowCount(SIGHTING_STATUS_TEXT.size());
    statusLabel = new JLabel(Messages.getMessage(Name.STATUS_LABEL));

    breedingCombo = new JComboBox<String>(BREEDING_TEXT.toArray(new String[] {}));
    breedingCombo.setMaximumRowCount(BREEDING_VALUES.size());
    breedingLabel = new JLabel(Messages.getMessage(Name.BREEDING_CODE_LABEL));

    notesLabel = new JLabel(Messages.getMessage(Name.NOTES_LABEL));

    photosListPanel = new PhotosListPanel(fontManager, fileDialogs, 
        new PhotoClicked(fileDialogs, alerts));

    if (userSet != null) {
      userChipsUi = new UserChipsUi(userSet, fontManager);
      userChipsUi.userChips.addChipsChangedListener(() -> {
        dirty.setDirty(true);
      });
    }
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHonorsVisibility(true);

    layout.setAutoCreateGaps(false);

    if (layoutType == Layout.VERTICAL) {
      SequentialGroup verticalGroup = layout.createSequentialGroup();
      verticalGroup
          .addGroup(layout.createParallelGroup().addComponent(heardOnly).addComponent(photographed))
          .addGroup(layout.createParallelGroup().addComponent(male).addComponent(female))
          .addGroup(layout.createParallelGroup().addComponent(adult).addComponent(immature))
          .addGroup(layout.createBaselineGroup(false, false).addComponent(statusLabel)
              .addComponent(statusCombo))
          .addGroup(layout.createBaselineGroup(false, false).addComponent(breedingLabel)
              .addComponent(breedingCombo))
          .addGroup(layout.createBaselineGroup(false, false).addComponent(numberLabel)
              .addComponent(numberField))
          .addComponent(photosListPanel, fontManager.scale(60), fontManager.scale(70),
              PREFERRED_SIZE);
      if (userChipsUi != null) {
        verticalGroup
            .addGroup(layout.createBaselineGroup(false, false).addComponent(userChipsUi.userLabel)
                .addComponent(userChipsUi.userIndexer).addComponent(userChipsUi.addUserButton));
        verticalGroup.addComponent(userChipsUi.userChipsScrollPane, fontManager.scale(40),
            fontManager.scale(60), PREFERRED_SIZE);
      }
      verticalGroup.addComponent(notesLabel).addComponent(descriptionScrollPane, PREFERRED_SIZE,
          PREFERRED_SIZE, 150);
      layout.setVerticalGroup(verticalGroup);
      ParallelGroup horizontalGroup = layout.createParallelGroup();
      horizontalGroup
          .addGroup(layout.createSequentialGroup()
              .addGroup(layout.createParallelGroup().addComponent(numberLabel)
                  .addComponent(statusLabel).addComponent(breedingLabel))
              .addGroup(layout.createParallelGroup()
                  .addComponent(numberField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                  .addComponent(statusCombo, fontManager.scale(150), fontManager.scale(150),
                      fontManager.scale(150))
                  .addComponent(breedingCombo, fontManager.scale(150), fontManager.scale(150),
                      fontManager.scale(150))))
          .addGroup(layout.createSequentialGroup().addComponent(male).addComponent(female))
          .addGroup(
              layout.createSequentialGroup().addComponent(heardOnly).addComponent(photographed))
          .addGroup(layout.createSequentialGroup().addComponent(adult).addComponent(immature))
          .addComponent(notesLabel).addComponent(photosListPanel).addComponent(
              descriptionScrollPane, PREFERRED_SIZE, PREFERRED_SIZE, fontManager.scale(250));
      if (userChipsUi != null) {
        horizontalGroup
            .addGroup(layout.createSequentialGroup().addComponent(userChipsUi.userLabel)
                .addComponent(userChipsUi.userIndexer).addComponent(userChipsUi.addUserButton))
            .addComponent(userChipsUi.userChipsScrollPane, fontManager.scale(250),
                fontManager.scale(250), fontManager.scale(250));
        userChipsUi.userChips.setScaledWidth(250);
      }
      layout.setHorizontalGroup(horizontalGroup);
      layout.linkSize(new Component[] {heardOnly, male, adult});
    } else { // HORIZONTAL layout
      SequentialGroup leftVerticalGroup = layout.createSequentialGroup();
      leftVerticalGroup
          .addGroup(layout.createBaselineGroup(false, true).addComponent(numberLabel)
              .addComponent(numberField).addComponent(statusLabel).addComponent(statusCombo))
          .addGroup(layout.createBaselineGroup(false, true).addComponent(breedingLabel)
              .addComponent(breedingCombo))
          .addGroup(layout.createBaselineGroup(false, true).addComponent(notesLabel).addComponent(
              descriptionScrollPane, PREFERRED_SIZE, PREFERRED_SIZE, fontManager.scale(200)));
      if (userChipsUi != null) {
        leftVerticalGroup
            .addGroup(layout.createBaselineGroup(false, false)
                .addComponent(userChipsUi.userLabel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                .addComponent(userChipsUi.userIndexer, PREFERRED_SIZE, PREFERRED_SIZE,
                    PREFERRED_SIZE)
                .addComponent(userChipsUi.addUserButton, PREFERRED_SIZE, PREFERRED_SIZE,
                    PREFERRED_SIZE))
            .addPreferredGap(ComponentPlacement.RELATED).addComponent(
                userChipsUi.userChipsScrollPane, PREFERRED_SIZE, PREFERRED_SIZE, Short.MAX_VALUE);
      }

      layout.setVerticalGroup(
          layout.createParallelGroup(Alignment.LEADING).addGroup(leftVerticalGroup)
              .addGroup(layout.createSequentialGroup()
                  .addGroup(layout.createBaselineGroup(false, false)
                      .addComponent(heardOnly)
                      .addComponent(male)
                      .addComponent(adult))
                  .addGroup(layout.createBaselineGroup(false, false)
                      .addComponent(photographed)
                      .addComponent(female)
                      .addComponent(immature))
                  .addPreferredGap(ComponentPlacement.RELATED).addComponent(photosListPanel,
                      fontManager.scale(60), fontManager.scale(70), PREFERRED_SIZE)));

      ParallelGroup leftHorizontalGroup = layout.createParallelGroup();
      leftHorizontalGroup
          .addGroup(layout.createSequentialGroup().addComponent(breedingLabel)
              .addComponent(breedingCombo, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
          .addGroup(layout.createSequentialGroup().addComponent(numberLabel)
              .addComponent(numberField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
              .addPreferredGap(ComponentPlacement.RELATED).addComponent(statusLabel)
              .addComponent(statusCombo, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
          .addGroup(layout.createSequentialGroup().addComponent(notesLabel).addComponent(
              descriptionScrollPane, PREFERRED_SIZE, PREFERRED_SIZE, fontManager.scale(270)));
      if (userChipsUi != null) {
        leftHorizontalGroup
            .addGroup(layout.createSequentialGroup()
                .addComponent(userChipsUi.userLabel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                .addComponent(userChipsUi.userIndexer, PREFERRED_SIZE, PREFERRED_SIZE,
                    PREFERRED_SIZE)
                .addComponent(userChipsUi.addUserButton, PREFERRED_SIZE, PREFERRED_SIZE,
                    PREFERRED_SIZE))
            .addComponent(userChipsUi.userChipsScrollPane);
      }
      layout
          .setHorizontalGroup(
              layout.createSequentialGroup().addGroup(leftHorizontalGroup)
                  .addPreferredGap(ComponentPlacement.UNRELATED).addGroup(
                      layout.createParallelGroup()
                          .addGroup(layout.createSequentialGroup()
                              .addComponent(heardOnly)
                              .addComponent(male)
                              .addComponent(adult))
                          .addGroup(layout.createSequentialGroup()
                              .addComponent(photographed)
                              .addComponent(female)
                              .addComponent(immature))
                          .addComponent(photosListPanel)));
      layout.linkSize(new Component[] {photographed, heardOnly, male, female, immature, adult});
      if (userChipsUi != null) {
        layout.linkSize(
            new Component[] {notesLabel, numberLabel, breedingLabel, userChipsUi.userLabel});
      } else {
        layout.linkSize(new Component[] {notesLabel, numberLabel, breedingLabel});
      }
    }
  }

  public JFormattedTextField getNumberField() {
    return numberField;
  }
  
  private class PhotoClicked extends DefaultPhotoClickedListener {
    public PhotoClicked(FileDialogs fileDialogs, Alerts alerts) {
      super(SightingInfoPanel.this, fileDialogs, alerts);
    }

    @Override
    protected void replacePhoto(Photo oldPhoto, Photo newPhoto) {
      int index = sighting.getPhotos().indexOf(oldPhoto);
      if (index >= 0) {
        List<Photo> newPhotos = new ArrayList<>(sighting.getPhotos());
        newPhotos.set(index,newPhoto);
        sighting.setPhotos(newPhotos);
        photosListPanel.setPhotos(newPhotos);
        reportSet.markDirty();

        // If the new and old photos are both files, and have the same name,
        // then look for other photos to fix.
        if (newPhoto.isFileBased() && oldPhoto.isFileBased()
            && newPhoto.toFile().getName().equals(oldPhoto.toFile().getName())) {
          lookForOtherReplacements(oldPhoto.toFile().getAbsoluteFile(),
              newPhoto.toFile().getAbsoluteFile());
          
        }
      }
    }    
  }

  private void lookForOtherReplacements(File oldDirectory, File newDirectory) {
    // Look for the common path suffix
    CommonSuffixResults results = MoreFiles.commonSuffix(oldDirectory, newDirectory);
    // FIXME: this isn't right... feels like there might be cases where someone 
    // /a/b/c/ moves to /d/a/b/c, or /d/a/b/c/ to /a/b/c/ (especially the former -
    // e.g. copying all of /photos/ in a previous drive to /Users/me/photos/,
    //     
    // Now, look for any photo that is a file and starts with "firstRemnant()".
    // (or if firstRemnant() is old)
    record SignificantEntry(Sighting sighting, Photo photo) {      
    }
    
    List<SignificantEntry> significantEntries = new ArrayList<>();
    
    for (Sighting sighting : reportSet.getSightings()) {
      if (sighting.hasSightingInfo()) {
        SightingInfo info = sighting.getSightingInfo();
        for (Photo photo : info.getPhotos()) {
          if (photo.isFileBased()) {
            File photoFile = photo.toFile();
            if (results.oldDirectory() == null
                || MoreFiles.isChildOf(results.oldDirectory(), photoFile)) {
              significantEntries.add(new SignificantEntry(sighting, photo));
            }
          }
        }
      }
    }
    
    int investigated = 0;
    int successful = 0;
    for (SignificantEntry entry : significantEntries) {
      File newFile = results.replacePrefix(
          entry.photo.toFile());
      investigated++;
      if (newFile.exists()) {
        successful++;
      }
      
      // Give up if the first thousand haven't found much success.
      if (investigated > 1000 || successful > 3) {
        break;
      }
    }
    
    if (successful > 0) {
      if (alerts.showYesNo(this,
          Messages.Name.FIX_OTHER_PHOTOS_TITLE,
          Messages.Name.FIX_OTHER_PHOTOS_FORMAT,
          results.oldDirectory().toPath(), results.newDirectory().toPath()) == JOptionPane.YES_OPTION) {
        int updatedCount = 0;
        for (SignificantEntry entry : significantEntries) {
          File newFile = results.replacePrefix(
              entry.photo.toFile());
          if (newFile.exists()) {
            List<Photo> photos = new ArrayList<>(entry.sighting().getSightingInfo().getPhotos());
            int index = photos.indexOf(entry.photo());
            if (index >= 0) {
              updatedCount++;
              Photo newPhoto = new Photo(newFile);
              photos.set(index, newPhoto);
              entry.sighting().getSightingInfo().setPhotos(photos);
            }
          }
        }
        if (updatedCount > 0) {
          // Might have been other photos on this sighting which
          // got updated; explicitly push them.
          photosListPanel.setPhotos(sighting.getPhotos());
          // Mark the overall reportSet dirty
          reportSet.markDirty();
          alerts.showMessage(this,
              Messages.Name.OTHER_PHOTOS_FIXED_TITLE,
              Messages.Name.OTHER_PHOTOS_FIXED_FORMAT,
              // Add 1 to include the first photo fixed, which is already handled
              // before the bulk update code runs.
              updatedCount + 1); 
        }
      }
    }
    
    
  }
}
