/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Type;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.Dirty;
import com.scythebill.birdlist.model.util.DirtyImpl;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Simple combobox for choosing one species of a Sp., or a ssp/group inside a species.
 */
class SpResolverComboBox extends JComboBox<SpResolverComboBox.ResolvedAndText> {
  private final Resolved resolved;
  private final DirtyImpl dirty;

  /**
   * Creates a SpResolverComboBox given a Resolved, or returns null if this UI
   * doesn't support anything.
   */
  public static SpResolverComboBox forResolved(Resolved resolved) {
    if (resolved.getType() == SightingTaxon.Type.SP
        || resolved.getType() == SightingTaxon.Type.HYBRID) {
      return new SpResolverComboBox(resolved);
    }
    
    if (resolved.getSmallestTaxonType() == Taxon.Type.species) {
      Taxon taxon = resolved.getTaxon();
      if (taxon.getContents().isEmpty()) {
        // No point for a monotypic species
        return null;
      }
    }
    
    return new SpResolverComboBox(resolved);
  }
  
  private SpResolverComboBox(Resolved resolved) {
    this.resolved = resolved;
    setModel(newModel());
    setMaximumRowCount(Math.min(25, getModel().getSize()));
    // Select the appropriate index
    for (int i = 0; i < getModel().getSize(); i++) {
      ResolvedAndText value = (ResolvedAndText) getModel().getElementAt(i);
      if (value.resolved.equals(resolved)) {
        setSelectedIndex(i);
        break;
      }
    }
    dirty = new DirtyImpl(false);
    addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        dirty.setDirty(true);
      }
    });
  }
  
  public Dirty getDirty() {
    return dirty;
  }
  
  public void clearDirty() {
    dirty.setDirty(false);
  }

  private ComboBoxModel<SpResolverComboBox.ResolvedAndText> newModel() {
    final List<ResolvedAndText> values = Lists.newArrayList();
    if (resolved.getType() == Type.SP) {
      values.add(new ResolvedAndText(
          Messages.getMessage(Name.KEEP_AS_SP),
          resolved));
      for (Taxon taxon : resolved.getTaxa()) {
        Resolved possibleResolved = SightingTaxons.newResolved(taxon);
        values.add(new ResolvedAndText(
            possibleResolved.getCommonName(), possibleResolved));
      }
    } else if (resolved.getType() == Type.HYBRID) {
        values.add(new ResolvedAndText(
            Messages.getMessage(Name.KEEP_AS_HYBRID),
            resolved));
        for (Taxon taxon : resolved.getTaxa()) {
          Resolved possibleResolved = SightingTaxons.newResolved(taxon);
          values.add(new ResolvedAndText(
              possibleResolved.getCommonName(), possibleResolved));
        }
    } else {
      final Taxon taxon = resolved.getTaxon();
      Taxon species;
      // Add the current choice as a first item
      if (taxon.getType() == Taxon.Type.species) {
        species = taxon;
      } else {
        species = TaxonUtils.getParentOfType(taxon, Taxon.Type.species);
      }
      
      // Visit everything within the species
      TaxonUtils.visitTaxa(species, new TaxonVisitor() {
        @Override
        public boolean visitTaxon(Taxon visitedTaxon) {
          Resolved resolvedTaxon = SightingTaxons.newResolved(visitedTaxon);
          if (visitedTaxon.getType() == Taxon.Type.species) {
            values.add(new ResolvedAndText(Messages.getMessage(Name.NONE_IN_BRACKETS), resolvedTaxon));
          } else if (visitedTaxon.getType() == Taxon.Type.group) {
            values.add(new ResolvedAndText(resolvedTaxon.getCommonName(), resolvedTaxon));
          } else { 
            if (visitedTaxon.getParent().getType() == Taxon.Type.group) {
              values.add(new ResolvedAndText("  " + resolvedTaxon.getName(), resolvedTaxon));
            } else {
              values.add(new ResolvedAndText(resolvedTaxon.getName(), resolvedTaxon));
            }
          }
          return true;
        }
      });
    }
    return new DefaultComboBoxModel<ResolvedAndText>(values.toArray(new ResolvedAndText[0]));
  }
  
  /**
   * Returns the currently selected Resolved.
   */
  public Resolved getResolved() {
    return ((ResolvedAndText) getSelectedItem()).resolved;
  }
  
  /**
   * Returns the currently selected SightingTaxon, pre-resolved to the
   * base taxonomy (that is, ready to store on a Sighting).
   */
  public SightingTaxon getSightingTaxonInBaseTaxonomy() {
    SightingTaxon sightingTaxon = getResolved().getSightingTaxon();
    Taxonomy taxonomy = getResolved().getTaxa()
        .iterator().next().getTaxonomy();
    if (taxonomy instanceof MappedTaxonomy) {
      MappedTaxonomy mappedTaxonomy = (MappedTaxonomy) taxonomy;
      sightingTaxon = mappedTaxonomy.getMapping(sightingTaxon);
    }
    return sightingTaxon;
  }
  
  static class ResolvedAndText {
    final String text;
    final Resolved resolved;
    ResolvedAndText(String text, Resolved resolved) {
      this.text = text;
      this.resolved = resolved;
    }
    
    @Override public String toString() {
      return text;
    }
  }

  public String getRangeSummary() {
    List<String> ranges = Lists.newArrayList();
    for (int i = 0; i < getModel().getSize(); i++) {
      Resolved resolved = getModel().getElementAt(i).resolved;
      if (resolved.getType() == Type.SP || resolved.getType() == Type.HYBRID) {
        continue;
      }
      
      Taxon taxon = resolved.getTaxon();
      if (!(taxon instanceof Species)) {
        continue;
      }
      String range = TaxonUtils.getRange((Species) taxon);
      if (range != null) {
        if (taxon.getType() == Taxon.Type.subspecies) {
          ranges.add(String.format("%s: %s", resolved.getName(), range));
        } else {
          ranges.add(String.format("%s: %s", resolved.getCommonName(), range));
        }
      }
    }
    return Joiner.on("\n\n").join(ranges);
  }
}
