package com.scythebill.birdlist.ui.components;

import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.annotation.Nullable;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Type;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.util.AndDirty;
import com.scythebill.birdlist.model.util.Dirty;
import com.scythebill.birdlist.model.util.DirtyImpl;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.SpHybridDialog;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * Small helper class for sharing SpResolver UI between the single
 * sighting editor and the bulk editor.
 */
final class SpResolverUi {
  final JComponent spLabel;
  final JComponent spInfoPanel;
  final JComponent spResolverPanel;
  final JComponent separator;
  final JButton spHybridButton;
  final DirtyImpl dirty = new DirtyImpl(false);
  final Dirty combinedDirty;

  private final SpResolverComboBox spResolver;
  private JTextArea speciesInfo;
  private SightingTaxon spHybridTaxon;
  private final SpHybridDialog spHybridDialog;
  private SightingTaxon originalSightingTaxon;
  private Taxon originalTaxon;

  public SpResolverUi(
      SpResolverComboBox spResolver,
      Alerts alerts,
      @Nullable SpHybridDialog spHybridDialog) {
    this.spResolver = spResolver;
    this.spHybridDialog = spHybridDialog;
    
    spHybridButton =
        spHybridDialog == null ? null : new JButton(Messages.getMessage(Name.SP_HYBRID_BUTTON));
    if (spResolver != null) {
      spResolverPanel = Box.createHorizontalBox();
      spResolverPanel.add(spResolver);
      if (spHybridButton != null) {
        spResolverPanel.add(Box.createHorizontalStrut(3));
        spResolverPanel.add(spHybridButton);
        spResolverPanel.add(Box.createHorizontalGlue());
      }
      spLabel = Box.createHorizontalBox();
      spLabel.add(new JLabel(Messages.getMessage(Name.CHANGE_TO_LABEL)));
      spLabel.add(Box.createHorizontalGlue());
      TextLink rangeSummary = new TextLink(
          Messages.getMessage(Name.RANGE_SUMMARY_LINK));
      spLabel.add(rangeSummary);
      rangeSummary.addActionListener(e -> {
        String formattedMessage = alerts.getFormattedDialogMessage(
            Name.RANGE_SUMMARY,
            Name.RANGE_SUMMARY_MESSAGE);
        alerts.showScrollableInfo(rangeSummary, formattedMessage, spResolver.getRangeSummary());
      });
      spLabel.add(Box.createHorizontalGlue());
      
      speciesInfo = new JTextArea();
      speciesInfo.setEditable(false);
      speciesInfo.setLineWrap(true);
      speciesInfo.setWrapStyleWord(true);
      speciesInfo.setRows(2);
      speciesInfo.setOpaque(false);
      
      JScrollPane speciesInfoScrollPane = new JScrollPane(speciesInfo);
      speciesInfoScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
      speciesInfoScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      speciesInfoScrollPane.putClientProperty("Quaqua.Component.visualMargin", new Insets(0, 0, 0, 0));
      speciesInfoScrollPane.setOpaque(false);
      speciesInfoScrollPane.getViewport().setOpaque(false);
      speciesInfoScrollPane.setBorder(null);
      spInfoPanel = speciesInfoScrollPane;
      
      spResolver.addActionListener(e -> {
        updateSpeciesInfo(speciesInfo);
      });
      updateSpeciesInfo(speciesInfo);
      separator = new JSeparator();
      combinedDirty = new AndDirty(dirty, spResolver.getDirty());
    } else {
      spLabel = new JLabel();
      spInfoPanel = new JLabel();
      separator = new JLabel();
      if (spHybridButton == null) {
        spResolverPanel = new JLabel();
      } else {
        spResolverPanel = Box.createHorizontalBox();
        JLabel changeToLabel = new JLabel(Messages.getMessage(Name.CHANGE_TO_LABEL));
        spResolverPanel.add(changeToLabel);
        spResolverPanel.add(Box.createHorizontalStrut(5));
        spResolverPanel.add(spHybridButton);
        spHybridButton.addPropertyChangeListener("enabled", e -> {
          changeToLabel.setEnabled(spHybridButton.isEnabled());
        });
      }
      combinedDirty = dirty;
    }
  }
  
  public void revert() {
    clearDirty();
    this.spHybridTaxon = null;
    if (spHybridButton != null) {
      spHybridButton.setEnabled(true);
    }
    if (spResolver != null) {
      spResolver.setEnabled(true);
    }
    
    if (originalTaxon != null) {
      reconfigureSpHybridButton(originalTaxon);
    } else if (originalSightingTaxon != null) {
      reconfigureSpHybridButtonForExistingSpOrHybrid(originalSightingTaxon);
    }
    // TODO: this should really own restore the last-saved index on the sp resolver
  }

  public void configureSpHybridButton(Taxon taxon) {
    originalTaxon = taxon;
    originalSightingTaxon = null;
    reconfigureSpHybridButton(taxon);
  }
    
  private void reconfigureSpHybridButton(Taxon taxon) {
    spHybridButton.setText(Messages.getMessage(Name.SP_HYBRID_BUTTON));
    for (ActionListener l : spHybridButton.getActionListeners()) {
      spHybridButton.removeActionListener(l);
    }
    spHybridButton.addActionListener(e -> {
      spHybridDialog.showDialog(
          spResolverPanel,
          taxon,
          this::rememberSpHybrid);
    });
  }
  
  public void configureSpHybridButtonForExistingSpOrHybrid(SightingTaxon sightingTaxon) {
    originalTaxon = null;
    originalSightingTaxon = sightingTaxon;
    reconfigureSpHybridButtonForExistingSpOrHybrid(sightingTaxon);
  }
        
  private void reconfigureSpHybridButtonForExistingSpOrHybrid(SightingTaxon sightingTaxon) {
    for (ActionListener l : spHybridButton.getActionListeners()) {
      spHybridButton.removeActionListener(l);
    }
    if (sightingTaxon.getType() == Type.HYBRID) {
      spHybridButton.setText(Messages.getMessage(Name.SP_BUTTON));
      SightingTaxon sp = SightingTaxons.newSpTaxon(sightingTaxon.getIds());          
      spHybridButton.addActionListener(e -> {
        rememberSpHybrid(sp);
      });          
    } else if (sightingTaxon.getType() == Type.SP) {
      spHybridButton.setText(Messages.getMessage(Name.HYBRID_BUTTON));
      SightingTaxon hybrid = SightingTaxons.newHybridTaxon(sightingTaxon.getIds());          
      spHybridButton.addActionListener(e -> {
        rememberSpHybrid(hybrid);
      });
    } else {
      throw new IllegalArgumentException("Not supported for " + sightingTaxon);
    }
  }
  
  public SightingTaxon getNewSightingTaxon() {
    if (spHybridTaxon != null) {
      return spHybridTaxon;
    }
    
    if (spResolver == null) {
      return null;
    }

    return spResolver.getSightingTaxonInBaseTaxonomy();
  }

  private void rememberSpHybrid(SightingTaxon spHybridTaxon) {
    dirty.setDirty(true);
    this.spHybridTaxon = spHybridTaxon;
    reconfigureSpHybridButtonForExistingSpOrHybrid(spHybridTaxon);
    if (spResolver != null) {
      spResolver.setEnabled(false);
    }
  }
  
  private void updateSpeciesInfo(JTextArea speciesInfo) {
    Resolved resolved = spResolver.getResolved();
    if (resolved.getType() == SightingTaxon.Type.SINGLE
        || resolved.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
      Taxon species = resolved.getTaxon();
      if (species instanceof Species) {
        String info = TaxonUtils.getRange((Species) species);
        speciesInfo.setText(info == null ? "" : info);
        speciesInfo.select(0, 0);
        speciesInfo.setEnabled(true);
        return;
      }
    }
    speciesInfo.setText(
        Messages.getMessage(Name.SPECIES_INFORMATION_WILL_APPEAR_HERE));
    speciesInfo.setEnabled(false);
  }

  public void clearDirty() {
    if (spResolver != null) {
      spResolver.clearDirty();
    }
    dirty.setDirty(false);
  }

  public Dirty getDirty() {
    return combinedDirty;
  }
}
