/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import java.util.function.Function;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences;
import com.scythebill.birdlist.model.util.Indexer;
import com.scythebill.birdlist.model.util.ToString;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Creates IndexerPanels for entry of species.
 */
public class SpeciesIndexerPanelConfigurer {
  private final NamesPreferences namesPreferences;

  public static SpeciesIndexerPanelConfigurer unconfigured() {
    return new SpeciesIndexerPanelConfigurer(new NamesPreferences());
  }
  
  @Inject public SpeciesIndexerPanelConfigurer(NamesPreferences namesPreferences) {
    this.namesPreferences = namesPreferences;
  }

  public void configure(IndexerPanel<String> indexer, final Taxonomy taxonomy) {
    indexer.setPreviewText(Name.TYPE_A_SPECIES_NAME);
    indexer.removeAllIndexerGroups();
    switch (namesPreferences.scientificOrCommon) {
      case COMMON_FIRST:
        addCommonNameIndices(indexer, taxonomy);
        addScientificIndices(indexer, taxonomy);
        break;
      case SCIENTIFIC_FIRST:
        addScientificIndices(indexer, taxonomy);
        addCommonNameIndices(indexer, taxonomy);
        break;
      case SCIENTIFIC_ONLY:
        addScientificIndices(indexer, taxonomy);
        break;
      case COMMON_ONLY:
        addCommonNameIndices(indexer, taxonomy);
        break;
    }
  }

  private void addScientificIndices(IndexerPanel<String> indexer, final Taxonomy taxonomy) {
    ToString<String> sciToString = new ToString<String>() {
      @Override public String getString(String taxonId) {
        Taxon taxon = taxonomy.getTaxon(taxonId);

        return taxon == null ? null : TaxonUtils.getFullName(taxon);
      }

      @Override public String getPreviewString(String taxonId) {
        return getString(taxonId);
      }
    };
    indexer.addIndexerGroup(sciToString, taxonomy.getScientificIndexer());
    if (taxonomy.getAlternateScientificIndexer() != null) {
      indexer.addAlternateIndexerGroup(sciToString, taxonomy.getAlternateScientificIndexer());
    }
  }

  private void addCommonNameIndices(IndexerPanel<String> indexer, final Taxonomy taxonomy) {
    // First, add the localized index, if it exists
    Indexer<String> localized = taxonomy.getLocalizedCommonIndexer();
    if (localized != null) {
      ToString<String> commonToString = new ToString<String>() {
        @Override public String getString(String taxonId) {
          Taxon taxon = taxonomy.getTaxon(taxonId);
          if (taxon == null) {
            return null;
          }
          
          // If there's no common name, *and* the species has no common name, then don't bother 
          if (taxon.getCommonName() == null
              && TaxonUtils.getParentOfType(taxon, Taxon.Type.species).getCommonName() == null) {
            return null;
          }
          
          return TaxonUtils.getCommonName(taxon);
        }

        @Override public String getPreviewString(String taxonId) {
          return getString(taxonId);
        }
      };
      indexer.addIndexerGroup(commonToString, localized);
    }
    
    // Now, add the English name indexer
    ToString<String> commonToString = new ToString<String>() {
      @Override public String getString(String taxonId) {
        Taxon taxon = taxonomy.getTaxon(taxonId);
        if (taxon == null) {
          return null;
        }
        
        // If there's no common name, *and* the species has no common name, then don't bother 
        if (taxon.getCommonName() == null
            && TaxonUtils.getParentOfType(taxon, Taxon.Type.species).getCommonName() == null) {
          return null;
        }

        return TaxonUtils.getEnglishCommonName(taxon);
      }

      @Override public String getPreviewString(String taxonId) {
        return getString(taxonId);
      }
    };
    indexer.addIndexerGroup(commonToString, taxonomy.getCommonIndexer());
    if (taxonomy.getAlternateCommonIndexer() != null) {
      indexer.addAlternateIndexerGroup(commonToString, taxonomy.getAlternateCommonIndexer());
    }
  }
  
  /**
   * Add additional taxa to the indexer, only adding trivial common and scientific names.
   * In no case are alternate names, translated names, etc. provided.
   */
  public void addSimpleTaxa(IndexerPanel<String> indexer, Taxonomy taxonomy, Iterable<Taxon> taxa) {
    switch (namesPreferences.scientificOrCommon) {
      case COMMON_FIRST:
        addSimpleIndices(indexer, taxonomy, taxa, Taxon::getCommonName);
        addSimpleIndices(indexer, taxonomy, taxa, Taxon::getName);
        break;
      case SCIENTIFIC_FIRST:
        addSimpleIndices(indexer, taxonomy, taxa, Taxon::getName);
        addSimpleIndices(indexer, taxonomy, taxa, Taxon::getCommonName);
        break;
      case SCIENTIFIC_ONLY:
        addSimpleIndices(indexer, taxonomy, taxa, Taxon::getName);
        break;
      case COMMON_ONLY:
        addSimpleIndices(indexer, taxonomy, taxa, Taxon::getCommonName);
        break;
    }
  }

  private void addSimpleIndices(IndexerPanel<String> indexerPanel, Taxonomy taxonomy,
      Iterable<Taxon> taxa, Function<Taxon, String> toString) {
    Indexer<String> indexer = new Indexer<>();
    boolean foundOne = false;
    for (Taxon taxon : taxa) {
      String name = toString.apply(taxon);
      if (name != null) {
        foundOne = true;
        indexer.add(name, taxon.getId());
      }
    }
    
    if (foundOne) {
      indexerPanel.addIndexerGroup(new ToString<String>() {
        @Override
        public String getString(String id) {
          Taxon taxon = taxonomy.getTaxon(id);
          return taxon == null ? null : toString.apply(taxon);
        }      
      }, indexer);
    }
  }
}
