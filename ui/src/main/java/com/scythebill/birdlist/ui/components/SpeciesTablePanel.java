/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.EventListener;
import java.util.List;
import java.util.Set;

import javax.swing.ActionMap;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.components.table.ExpandColumn;
import com.scythebill.birdlist.ui.components.table.ExpandableTable;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.ColumnLayout;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.DetailView;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.RowState;
import com.scythebill.birdlist.ui.components.table.LabelColumn;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.ListListModel;

/**
 * Base class supporting display of a species ExpandableTable.  Subclasses override
 * hooks to support checklist mode or "direct" mode.
 */
public abstract class SpeciesTablePanel extends JPanel implements FontsUpdatedListener {
  private ExpandableTable<Resolved> speciesTable;
  private boolean editable;
  private List<Resolved> oldTaxonList = ImmutableList.of();
  private Resolved selectedTaxon;
  private Predicate<Resolved> isImportantPredicate = Predicates.alwaysFalse();
  private JScrollPane scrollPane;
  private FontManager fontManager;
  private DetailView<Resolved> expandedRowView;
  private String detailTitle;
  
  /** Creates new form SpeciesListPanel */
  protected SpeciesTablePanel(DetailView<Resolved> expandedRowView, String detailTitle,
      FontManager fontManager) {
    this.expandedRowView = expandedRowView;
    this.detailTitle = detailTitle;
    this.fontManager = fontManager;
  }
  
  protected void init() {
    initComponents(expandedRowView, detailTitle);
    hookUpContents();
    fontsUpdated(fontManager);
  }
  
  /**
   * A listener to explicitly notify that the value changed because of a direct user action, 
   * rather than just changing the taxonomy.
   */
  public interface UserModificationListener extends EventListener {
    void userModifiedValue();
  }

  public void addUserModificationListener(UserModificationListener l) {
    listenerList.add(UserModificationListener.class, l);
  }

  protected void notifyUserModifiedValue() {
    for (UserModificationListener l : getListeners(UserModificationListener.class)) {
      l.userModifiedValue();
    }
  }
  
  public void addColumn(ExpandableTable.Column<Resolved> column) {
    speciesTable.addColumn(column);
  }

  /** Make sure that all pending edits are committed. */
  public void flushEdits() {
    speciesTable.setExpandedIndex(ExpandableTable.NO_ROW);
  }

  public void setEditable(boolean editable) {
    this.editable = editable;
  }

  public boolean isEditable() {
    return editable;
  }

  private void hookUpContents() {
    speciesTable.addPropertyChangeListener("selectedIndex", new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        int newSelectedIndex = speciesTable.getSelectedIndex();
        setSelectedTaxon(newSelectedIndex >= 0
            ? getListModel().asList().get(newSelectedIndex)
            : null);
      }
    });

    // TODO: should this be in the base class?
    speciesTable.addPropertyChangeListener("selectedIndex", new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        if (speciesTable.getSelectedIndex() != ExpandableTable.NO_ROW
            && speciesTable.getSelectedIndex() != speciesTable.getExpandedIndex()) {
          speciesTable.setExpandedIndex(ExpandableTable.NO_ROW);
        }
      }
    });
  }

  /** Selects a species;  returns false if the species could not be selected. */
  public boolean selectSpecies(Resolved resolved, boolean scrollWithVisibleContext) {
    int index = findTaxon(resolved);
    if (index >= 0) {
      if (scrollWithVisibleContext) {
        speciesTable.setSelectedIndexWithVisibleContext(index);
      } else {
        speciesTable.setSelectedIndex(index);
      }
      return true;
    } else {
      speciesTable.setSelectedIndex(ExpandableTable.NO_ROW);
      return false;
    }
  }

  /** Find the index of a taxon, or -1 if it is not present. */
  public int findTaxon(Resolved resolved) {
    int index = resolved == null ? -1 : getListModel().asList().indexOf(resolved);
    return index;
  }

  public void resolvedUpdated(Resolved resolved) {
    int row = findTaxon(resolved);
    if (row >= 0) {
      getListModel().asList().set(row, resolved);
    }
  }

  /**
   * Expand the detail row of a species. 
   * @return false if no row could be found.
   */
  public boolean expandSpeciesDetail(Resolved selectedSpecies) {
    int index = findTaxon(selectedSpecies);
    if (index >= 0) {
      // Ignore expanding rows that are not enabled
      if (speciesTable.getEnabledRowsModel().isIncluded(index)) {
        speciesTable.setExpandedIndex(index);
      }
      return true;
    } else {
      speciesTable.setExpandedIndex(ExpandableTable.NO_ROW);
      return false;
    }
  }

  /** Transfer focus to the expanded species detail. */
  public void focusOnSpeciesDetail() {
    speciesTable.focusOnExpandedIndex();
  }

  public Resolved getSelectedSpecies() {
    return (Resolved) speciesTable.getSelectedValue();
  }

  public Resolved getSpecies(int index) {
    return (Resolved) speciesTable.getModel().getElementAt(index);
  }

  public void setIsImportant(Predicate<Resolved> isImportantPredicate) {
    this.isImportantPredicate = isImportantPredicate;
  }

  /**
   * Sets a list of taxa (which must already be sorted);
   */
  abstract public void setSortedContents(Taxonomy taxonomy, List<Resolved> taxa);

  abstract protected ListListModel<Resolved> getListModel();

  abstract public void addSpecies(Resolved resolved);

  abstract public void removeSpecies(Resolved taxon);

  abstract public void swapSpecies(
      Resolved currentTaxon, Resolved newTaxon);
  
 /**
   * Returns a copy of the current species list.
   */
  abstract public ImmutableList<Resolved> getValue();

  protected void fireValueChanged() {
    List<Resolved> newTaxonList = getValue();
    firePropertyChange("value", oldTaxonList, newTaxonList);

    oldTaxonList = newTaxonList;
  }

  @Override
  public boolean requestFocusInWindow() {
    return speciesTable.requestFocusInWindow();
  }

  private void setSelectedTaxon(Resolved resolved) {
    firePropertyChange("selectedValue", selectedTaxon, resolved);
    selectedTaxon = resolved;
  }

  /** Notification that the species table is now available (and empty). */
  protected void speciesTableCreated() {
  }
  
  private void initComponents(DetailView<Resolved> expandedRowView, String detailTitle) {
    scrollPane = new JScrollPane();
    setFocusScrollPaneBorder(false);
    speciesTable = new ExpandableTable<Resolved>(Resolved.class, fontManager);
    speciesTable.addFocusListener(new FocusListener() {
      @Override public void focusLost(FocusEvent e) {
        setFocusScrollPaneBorder(false);
      }
      
      @Override public void focusGained(FocusEvent e) {
        setFocusScrollPaneBorder(true);
      }
    });
    speciesTableCreated();
    
    if (expandedRowView != null) {
      speciesTable.addColumn(new ExpandColumn<Resolved>(speciesTable, detailTitle));
      speciesTable.setExpandedRowView(expandedRowView);
    }
    speciesTable.addColumn(new TaxonColumn(ColumnLayout.weightedWidth(1.0f)));
    // Let copy be handled elsewhere
    speciesTable.setActionMap(new ActionMap());

    speciesTable.setModel(getListModel());
    scrollPane.setViewportView(speciesTable);
    scrollPane.setColumnHeaderView(speciesTable.getColumnHeader());
  }

  /** Attach a focus border to the scrollpane, to make it obvious when the table has focus. */
  private void setFocusScrollPaneBorder(boolean isFocused) {
    if (isFocused) {
      Color focusColor = UIManager.getColor("Focus.color");
      if (focusColor == null) {
        focusColor = UIManager.getColor("Button.focus");
        if (focusColor == null) {
          focusColor = UIManager.getColor("TextField.selectionBackground");
        }
      }
      
      if (focusColor != null) {
        scrollPane.setBorder(new LineBorder(focusColor, 2, true));
        return;
      }
      // else fall through, and set the unfocused, empty border
    } 
    
    scrollPane.setBorder(new EmptyBorder(2, 2, 2, 2));
  }

  /** Column displaying the species name. */
  private class TaxonColumn extends LabelColumn<Resolved> {
    public TaxonColumn(ColumnLayout width) {
      super(width, Messages.getMessage(Name.SPECIES_TEXT));
    }

    @Override
    public JComponent createComponent(final Resolved value, Set<RowState> states) {
      JLabel label = new JLabel(getText(value)) {

        @Override
        protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
          if (!"parent".equals(propertyName) && !"ancestor".equals(propertyName)) {
            // Stop Swing from adding a bazillion AncestorListeners to the table via a vile hack.
            super.firePropertyChange(propertyName, oldValue, newValue);
          }
        }

        /**
         * When drawing the label, force the font to BOLD if it's "important".
         * This is more effective than setting a font on creation because the seen-taxa
         * list might not be fully loaded (especially when switching taxonomies).
         */
        @Override
        protected void paintComponent(Graphics g) {
          if (isImportantPredicate.apply(value)) {
            g.setFont(g.getFont().deriveFont(Font.BOLD));
            // With Quaqua, merely setting the Graphics font doesn't seem to be enough;
            // set the component as well.  But, hey, just do so once.  'Cause if you
            // do that every time you paint...  watch the CPU soar!
            if ((getFont().getStyle() & Font.BOLD) == 0) {
              setFont(g.getFont());
            }
          } else if ((getFont().getStyle() & Font.BOLD) != 0) {
            // ... and clear the font on the component 
            setFont(getFont().deriveFont(Font.PLAIN));
          }
          
          super.paintComponent(g);
        }
        
        /**
         * And since the font weight is getting hacked above, cheat on preferred size
         * by setting width to MAX_VALUE.  In effect, this forces the label to stretch
         * the full width of the column.
         */
        @Override
        public Dimension getPreferredSize() {
          Dimension preferredSize = super.getPreferredSize();
          preferredSize.width = Short.MAX_VALUE;
          return preferredSize;
        }
      };
      label.setOpaque(false);
      label.setFocusable(false);
      label.setBorder(new EmptyBorder(0, 3, 0, 0));
      label.putClientProperty(FontManager.PLAIN_LABEL, true);
      customizeSpeciesLabel(label);
      updateComponentState(label, value, states);

      return label;
    }

    @Override
    protected String getText(Resolved value) {
      if (value.getSmallestTaxonType() != Taxon.Type.species) {
        value = value.resolveParentOfType(Taxon.Type.species);
      }
      return value.getPreferredSingleName();
    }
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    GroupLayout layout = new GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup().addComponent(scrollPane,
        GroupLayout.DEFAULT_SIZE, fontManager.scale(395), Short.MAX_VALUE));
    layout.setVerticalGroup(layout.createParallelGroup().addComponent(scrollPane,
        GroupLayout.DEFAULT_SIZE, fontManager.scale(340), Short.MAX_VALUE));

    speciesTable.setLineHeight(fontManager.scale(20));
  }

  /** Return the species table. */
  public ExpandableTable<Resolved> getSpeciesTable() {
    return speciesTable;
  }
  
  protected void customizeSpeciesLabel(JLabel label) {
  }

}
