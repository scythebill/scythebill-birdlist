/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import java.awt.Component;
import java.awt.Font;
import java.awt.Rectangle;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.TransferHandler.TransferSupport;
import javax.swing.tree.TreePath;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.actions.ActionBroker;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.datatransfer.SightingsGroup;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.panels.SpHybridDialog;
import com.scythebill.birdlist.ui.panels.SpeciesInfoDescriber;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;
import com.scythebill.birdlist.ui.util.FocusTracker;
import com.scythebill.birdlist.ui.util.OpenMapUrl;
import com.scythebill.birdlist.ui.util.TaxonTreeModel;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Panel for browsing taxa.
 * 
 * TODO: make species without sightings leaves, but without a preview pane
 */
public final class TaxonBrowsePanel extends BaseTaxonBrowserPanel {
  private final static Logger logger = Logger.getLogger(TaxonBrowsePanel.class.getName());
  private TaxonTreeModel taxonTreeModel;
  private final ReportSet reportSet;
  private final ActionBroker actionBroker;
  private Type depth;
  private TaxonFocusTracker taxonFocusTracker;
  private Predicate<Sighting> sightingPredicate;
  private Predicate<Taxon> taxonFilter;
  private boolean styledSpeciesWithSightings = true;
    
  @Inject
  public TaxonBrowsePanel(
      ReportSet reportSet,
      FontManager fontManager,
      NewLocationDialog newLocationDialog,
      SpHybridDialog spHybridDialog,
      PredefinedLocations predefinedLocations,
      ActionBroker actionBroker,
      SpeciesInfoDescriber speciesInfoDescriber,
      OpenMapUrl openMapUrl,
      FileDialogs fileDialogs,
      Alerts alerts,
      NavigableFrame navigableFrame) {
    super(fontManager, reportSet, newLocationDialog, spHybridDialog,
        predefinedLocations, speciesInfoDescriber, openMapUrl, fileDialogs, alerts, navigableFrame);
    this.reportSet = reportSet;
    this.actionBroker = actionBroker;
        
    // Instantiate a focus tracker to set up actions appropriately when the taxon panel
    // has focus
    taxonFocusTracker = new TaxonFocusTracker();

    setDragEnabled(true);
    setTransferHandler(new TaxonTransferHandler());
  }
  
  public void setTaxonomy(Taxonomy taxonomy, Predicate<Taxon> taxonFilter) {
    this.taxonFilter = taxonFilter == null ? Predicates.alwaysTrue() : taxonFilter;
    setTaxonomy(taxonomy);
  }

  @Override
  public void setTaxonomy(Taxonomy taxonomy) {
    if (taxonomy == getTaxonomy()) {
      return;
    }
    
    boolean compatibleTaxonomies = TaxonUtils.areCompatible(getTaxonomy(), taxonomy);
    
    // Load the existing selection paths.  First, extract all taxa from those paths
    TreePath[] paths = getSelectionPaths();
    List<Taxon> taxa = Lists.newArrayList();
    if (paths != null) {
      for (TreePath path : paths) {
        while (path != null) {
          if (path.getLastPathComponent() instanceof Taxon) {
            taxa.add((Taxon) path.getLastPathComponent());
            break;
          }
          path = path.getParentPath();
        }
      }
    }

    // Clear the selection before setting the taxonomy
    clearSelection();
    super.setTaxonomy(taxonomy);
    
    // Now map each taxon to the new taxonomy
    List<TreePath> newPaths = Lists.newArrayList();
    // Only restore the selection when going between compatible taxonomies
    if (compatibleTaxonomies) {
      Set<String> taxonIds = Sets.newLinkedHashSet();
      for (Taxon taxon : taxa) {
        if (taxon.getTaxonomy() instanceof MappedTaxonomy) {
          // Mapped Taxonomy back to base
          MappedTaxonomy mappedTaxonomy = (MappedTaxonomy) taxon.getTaxonomy();
          // Haven't written code for MappedTaxonomy -> MappedTaxonomy
          Preconditions.checkState(taxonomy == mappedTaxonomy.getBaseTaxonomy());
          
          SightingTaxon mapping = mappedTaxonomy.getMapping(taxon);
          if (mapping != null) {
            taxonIds.addAll(mapping.getIds());
          }
        } else {
          // Base taxonomy to Mapped
          Preconditions.checkState(taxonomy instanceof MappedTaxonomy);
          MappedTaxonomy mappedTaxonomy = (MappedTaxonomy) taxonomy;
          Resolved resolved = mappedTaxonomy.resolveInto(
                  SightingTaxons.newSightingTaxon(taxon.getId()));
          if (resolved != null) {
            for (Taxon mappedTaxon : resolved.getTaxa()) {
              taxonIds.add(mappedTaxon.getId());
            }
          }
        }
      }
      
      for (String id : TaxonUtils.mapToCommonLevel(taxonomy, taxonIds)) {
        Taxon mappedTaxon = taxonomy.getTaxon(id);
        if (mappedTaxon != null) {
          newPaths.add(toTreePath(mappedTaxon));
        }
      }
    }
      
    if (depth != null) {
      setDepth(depth, newPaths.toArray(new TreePath[0]), taxonFilter);
    }
  }

  public void setDepth(Type depth) {
    TreePath[] paths = getSelectionPaths();
    clearSelection();
    setDepth(depth, paths, taxonFilter);    
  }

  public void setTaxonFilter(Predicate<Taxon> taxonFilter) {
    this.taxonFilter = taxonFilter == null ? Predicates.alwaysTrue() : taxonFilter;
    setDepth(depth);
  }

  public void setSightingPredicate(Predicate<Sighting> sightingPredicate) {
    this.sightingPredicate = sightingPredicate;
    if (taxonTreeModel != null) {
      taxonTreeModel.setSightingPredicate(sightingPredicate);
      
      TreePath[] selectedPaths = getSelectionPaths();
      if (selectedPaths.length > 0) {
        // Sightings are selected - move the selection back to the containing taxon,
        // which is the much much much lazier way of dealing with sightings
        // being added or removed.
        if (selectedPaths[0].getLastPathComponent() instanceof Sighting) {
          setSelectionPath(selectedPaths[0].getParentPath());
          taxonTreeModel.structureChanged(selectedPaths[0].getParentPath());
        } else {
          for (TreePath selectedPath : selectedPaths) {
            taxonTreeModel.structureChanged(selectedPath);
          }
        }
      }
      
      // Force a repaint, to deal with toggling between having children or not
      repaint();
    }
  }
  
  private void setDepth(Type depth, TreePath[] paths, Predicate<Taxon> taxonFilter) {
    this.depth = depth;
    ImmutableList.Builder<Type> levelsBuilder = ImmutableList.builder();
    if (depth == Type.subspecies) {
      levelsBuilder.add(Type.subspecies, Type.group);
    } else if (depth == Type.group) {
      levelsBuilder.add(Type.group);
    }
    
    levelsBuilder.add(Taxon.Type.species, Taxon.Type.family, Taxon.Type.order);
    List<Type> levels = levelsBuilder.build();
    taxonTreeModel = new TaxonTreeModel(getTaxonomy(), levels, taxonFilter);
    taxonTreeModel.setReportSet(reportSet);
    taxonTreeModel.setSightingPredicate(sightingPredicate);
    setModel(taxonTreeModel);
    
    if (paths != null) {
      final List<TreePath> newPaths = Lists.newArrayList();
      // Skip up past the sightings, and up to taxa
      for (TreePath path : paths) {
        while (!(path.getLastPathComponent() instanceof Taxon) ||
            !levels.contains(((Taxon) path.getLastPathComponent()).getType())) {
          path = path.getParentPath();
          if (path == null) {
            break;
          }
        }
        
        if (path != null) {
          newPaths.add(path);
        }
      }
      
      // Run later, so as to occur when layout is visible
      SwingUtilities.invokeLater(new Runnable() {
        @Override public void run() {
          if (newPaths.size() > 0) {
            getSelectionModel().addSelectionPaths(newPaths.toArray(new TreePath[newPaths.size()]));            
            ensurePathIsVisible(newPaths.get(0));
          }
        }
      });
    }
  }

  public void selectTaxon(String taxonId) {
    Taxon taxon = getTaxonomy().getTaxon(taxonId);
    if (taxon != null) {
      TreePath path = toTreePath(taxon);
      getSelectionModel().setSelectionPath(path);
      ensurePathIsVisible(path);
      requestFocus();
    } else {
      // User reported seeing this exception once.  Haven't reproduced so
      // (for now) hacking away.  This would mean that the taxon selection
      // would appear to be ignored, which would be quite bad if this was a
      // consistent problem.
      logger.warning("Could not find taxon to select: " + taxonId);
    }
  }
  
  private TreePath toTreePath(Taxon taxon) {
    if (taxon.getType() == Taxon.Type.order) {
      return new TreePath(new Object[]{
              getTaxonomy().getRoot(),
              TaxonUtils.getParentOfType(taxon, Taxon.Type.order),
           });
    } else if (taxon.getType() == Taxon.Type.family) {
      return new TreePath(new Object[]{
              getTaxonomy().getRoot(),
              TaxonUtils.getParentOfType(taxon, Taxon.Type.order),
              TaxonUtils.getParentOfType(taxon, Taxon.Type.family)
           });
    } else if (taxon.getType() == Taxon.Type.group) {
      return new TreePath(new Object[]{
              getTaxonomy().getRoot(),
              TaxonUtils.getParentOfType(taxon, Taxon.Type.order),
              TaxonUtils.getParentOfType(taxon, Taxon.Type.family),
              TaxonUtils.getParentOfType(taxon, Taxon.Type.species),
              taxon
           });
    } else if (taxon.getType() == Taxon.Type.subspecies) {
      if (taxon.getParent().getType() == Taxon.Type.group) {
        return new TreePath(new Object[]{
                getTaxonomy().getRoot(),
                TaxonUtils.getParentOfType(taxon, Taxon.Type.order),
                TaxonUtils.getParentOfType(taxon, Taxon.Type.family),
                TaxonUtils.getParentOfType(taxon, Taxon.Type.species),
                TaxonUtils.getParentOfType(taxon, Taxon.Type.group),
                taxon
             });
      } else {
        return new TreePath(new Object[]{
                getTaxonomy().getRoot(),
                TaxonUtils.getParentOfType(taxon, Taxon.Type.order),
                TaxonUtils.getParentOfType(taxon, Taxon.Type.family),
                TaxonUtils.getParentOfType(taxon, Taxon.Type.species),
                taxon
             });
      }
    } else {
      return new TreePath(new Object[]{
       getTaxonomy().getRoot(),
       TaxonUtils.getParentOfType(taxon, Taxon.Type.order),
       TaxonUtils.getParentOfType(taxon, Taxon.Type.family),
       TaxonUtils.getParentOfType(taxon, Taxon.Type.species)
      });
    }
  }

  private boolean isFirstSpeciesInGenus(Taxon taxon) {
    Taxon genus = TaxonUtils.getParentOfType(taxon, Taxon.Type.genus);
    List<Taxon> contents = genus.getContents();
    // TODO(awiner): this will break if superspecies are added
    for (int i = 0; i < contents.size(); i++) {
      Taxon species = contents.get(i);
      if (!taxonFilter.apply(species)) {
        continue;
      }
      
      return species == taxon;
    }
    
    // Shouldn't reach here, but ah well if it does.
    return false;
  }
  
  @Override
  protected Font deriveTaxonFont(Font base, Taxon taxon, boolean leaf) {
    base = super.deriveTaxonFont(base, taxon, leaf);
    switch (taxon.getType()) {
      case species:
        if (!styledSpeciesWithSightings) {
          return base;
        }
        // else fall through
      case group:
      case subspecies:
        int style = taxonTreeModel.getSightingsOf(taxon).isEmpty() ? Font.ITALIC : Font.BOLD;
        return base.deriveFont(style);
      default:
        return base;
    }
  }

  @Override
  protected boolean abbreviateGenus(Taxon taxon) {
    return !isFirstSpeciesInGenus(taxon);
  }
  
  @Override
  protected boolean usePreviewColumn(TreePath[] paths) {
    boolean usePreviewDefault = super.usePreviewColumn(paths);
    if (!usePreviewDefault) {
      return false;
    }
    
    // Don't show preview for taxa, just sightings
    if (paths.length == 1 && !(paths[0].getLastPathComponent() instanceof Sighting)) {
      return false;
    }
    
    return true;
  }
    
  public Action getCutAction() {
    return taxonFocusTracker.cut;
  }
  
  private class TaxonFocusTracker extends FocusTracker {
    private final Action cut = new CutActionWrapper(TransferHandler.getCutAction());
    private final Action copy = new CopyActionWrapper(TransferHandler.getCopyAction());
    private final Action paste = new PasteActionWrapper(TransferHandler.getPasteAction());
    
    public TaxonFocusTracker() {
      super(TaxonBrowsePanel.this);
    }

    @Override protected void focusGained(Component child) {
      actionBroker.publishAction("cut", cut);
      actionBroker.publishAction("copy", copy);
      actionBroker.publishAction("paste", paste);
    }

    @Override protected void focusLost(Component child) {
      actionBroker.unpublishAction("cut", cut);
      actionBroker.unpublishAction("copy", copy);
      actionBroker.unpublishAction("paste", paste);
    }
  }
  
  /** Handle a move - delete the sightings group. */
  @Override protected void doMove(SightingsGroup sightings) {
    // Remove all the sightings, and associated state.
    reportSet.mutator().removing(sightings.getSightings()).mutate();

    taxonTreeModel.sightingsRemoved(sightings.getParentPath(),
        sightings.getSightings(), sightings.getOriginalIndices());

    // Redraw this node, since it may have flipped from non-empty to empty.
    Rectangle pathBounds = getPathBounds(sightings.getParentPath());
    if (pathBounds != null) {
      repaint(pathBounds);
    }
    TreePath selectionPath = getSelectionPath();
    while (selectionPath != null && selectionPath.getLastPathComponent() instanceof Sighting) {
      selectionPath = selectionPath.getParentPath();
    }
    if (selectionPath != null) {
      setSelectionPath(selectionPath);
    } else {
      setSelectionPath(sightings.getParentPath());
    }
  }
  
  /** Add a single sighting. */
  public void addSighting(TreePath treePath, Sighting sighting) {
    addSightings(treePath, ImmutableList.of(sighting));
  }
  
  @Override
  protected boolean doImport(
      TransferSupport transferSupportUnused,
      SightingsGroup sightings,
      TreePath target) {
    Taxon taxon;
    if (target.getLastPathComponent() instanceof Taxon) {
      taxon = (Taxon) target.getLastPathComponent();
    } else if (target.getLastPathComponent() instanceof Sighting) {
      target = target.getParentPath();
      Object penultimate = target.getLastPathComponent();
      if (!(penultimate instanceof Taxon)) {
        return false;
      }
      taxon = (Taxon) penultimate;
    } else {
      return false;
    }
    
    if ((taxon.getType().compareTo(Taxon.Type.species) > 0)
        || taxon.isDisabled()) {
      return false;
    }
    
    ImmutableList.Builder<Sighting> newSightingsBuilder = ImmutableList.builder();
    for (Sighting.Builder sighting : sightings.sightingBuilders()) {
      Sighting newSighting = sighting.setTaxon(taxon).build();
      newSightingsBuilder.add(newSighting);
    }
    
    ImmutableList<Sighting> newSightings = newSightingsBuilder.build();
    addSightings(target, newSightings);
    
    // Restore any visit info that may have been removed when the sightings were
    for (Map.Entry<VisitInfoKey, VisitInfo> entry : sightings.getVisitInfoMap().entrySet()) {
      if (reportSet.getVisitInfo(entry.getKey()) == null) {
        reportSet.putVisitInfo(entry.getKey(), entry.getValue());
      }
    }
    
    return true;
  }
  
  /** Add sightings to a target. */
  private void addSightings(TreePath target,
      ImmutableList<Sighting> newSightings) {
    reportSet.mutator().adding(newSightings).mutate();

    taxonTreeModel.sightingsAdded(target, newSightings);

    TreePath[] paths = new TreePath[newSightings.size()];
    for (int i = 0; i < paths.length; i++) {
      paths[i] = target.pathByAddingChild(newSightings.get(i));
    }

    SwingUtilities.invokeLater(() -> {
      if (paths.length > 1) {
        setSelectionPath(paths[0]);
      }

      setSelectionPaths(paths);
      requestFocus();
    });

    // Redraw this node, since it may have flipped from empty to not-empty.
    Rectangle pathBounds = getPathBounds(target);
    if (pathBounds != null) {
      repaint(pathBounds);
    }
  }

  @Override
  protected void sightingsReplaced(
      TreePath parent,
      List<Sighting> oldSightings,
      List<Sighting> newSightings,
      int[] oldIndices) {
    Taxon taxon = (Taxon) parent.getLastPathComponent();
    
    // Get the sighting taxon of the first species.  NOTE: this assumes that transformations
    // here map all species to the same taxon
    SightingTaxon sightingTaxon = newSightings.get(0).getTaxon();
    // ... and resolve into the local taxonomy 
    sightingTaxon = sightingTaxon.resolve(taxon.getTaxonomy()).getSightingTaxon();
    // If this sighting still belongs to the current taxon, then just update current sightings.
    if (sightingTaxon.shouldBeDisplayedWith(taxon.getId())) {
      taxonTreeModel.sightingsReplaced(parent, oldSightings, newSightings, oldIndices);
    } else {
      taxonTreeModel.sightingsRemoved(parent, oldSightings, oldIndices);
    }
  }

  @Override
  protected TreePath pathToSighting(TreePath oldParentPath, Sighting sighting) {
    Resolved resolved = sighting.getTaxon().resolve(getTaxonomy());
    // Somewhat arbitrarily, pick the first taxon resolved to
    // TODO: this is destabilizing if the sighting had been found from a different path
    Taxon taxon = resolved.getTaxa().iterator().next();
    taxon = TaxonUtils.getParentOfAtLeastType(taxon, depth);
    return toTreePath(taxon).pathByAddingChild(sighting);
  }

  public void setStyledSpeciesWithSightings(boolean styledSpeciesWithSightings) {
    this.styledSpeciesWithSightings = styledSpeciesWithSightings;    
  }

  @Override
  protected String getReturnPanelName() {
    return "browse";
  }

  static class ReturnToSighting implements PostNavigateAction {
    private final Sighting sighting;

    public ReturnToSighting(Sighting sighting) {
      this.sighting = sighting;
    }

    @Override
    public void postNavigate(JPanel panel) {
      TaxonBrowsePanel taxonBrowsePanel =
          UIUtils.findChildOfType(panel, TaxonBrowsePanel.class);
      if (taxonBrowsePanel != null) {
        Taxonomy taxonomy = taxonBrowsePanel.getTaxonomy();
        if (TaxonUtils.areCompatible(sighting.getTaxonomy(), taxonomy)) {
          Taxon taxon = sighting.getTaxon().resolve(taxonomy).getTaxa().iterator().next();
          taxonBrowsePanel.selectTaxon(taxon.getId());
        }
        return;
      }
    }
    
  }
  
  @Override
  protected PostNavigateAction returnToSighting(Sighting sighting) {
    return new ReturnToSighting(sighting);
  }
}
