/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.font.TextAttribute;

import javax.swing.JButton;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import com.google.common.collect.ImmutableMap;

/**
 * A simple "button" that looks like a text link.
 */
public class TextLink extends JButton {
  public TextLink(String text) {
    setText(text);
    setOpaque(false);
    setDefaultBorder();
    setForeground(new Color(0, 0, 0x99));
    setCursor(new Cursor(Cursor.HAND_CURSOR));
    setContentAreaFilled(false);
    addFocusListener(new FocusListener() {
      @Override
      public void focusLost(FocusEvent e) {
        setDefaultBorder();
      }
      
      @Override
      public void focusGained(FocusEvent e) {
        setFocusedBorder();
      }
    });
    addMouseListener(new MouseAdapter() {
      @Override public void mouseEntered(MouseEvent e) {
        if (shouldShowLink()) {
          setFont(getFont()
              .deriveFont(ImmutableMap.of(
                  TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON)));
        }
      }

      private boolean shouldShowLink() {
        return isEnabled() && getActionListeners().length > 0;
      }

      @Override public void mouseExited(MouseEvent e) {
        if (shouldShowLink()) {
          setFont(getFont()
              .deriveFont(ImmutableMap.of(
                  TextAttribute.UNDERLINE, -1)));
        }
      }
      
    });
  }
  
  protected void setFocusedBorder() {
    Color color = UIManager.getColor("Focus.color");
    if (color != null) {
      setBorder(new CompoundBorder(new LineBorder(color, 2, false), new EmptyBorder(2, 2, 2, 2)));
    }
  }
  
  protected void setDefaultBorder() {
    setBorder(new EmptyBorder(4, 4, 4, 4));
  }
}
