/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Cloned from http://www.camick.com/java/source/TextPrompt.java
 */
package com.scythebill.birdlist.ui.components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JLabel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;

import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.UIUtils;

/** Utility for adding a text prompt to a field. */
public class TextPrompt {
  private TextPrompt() {}
  
  private static final String MACOS_PROMPT_KEY = "JTextField.Search.Prompt";
  
  public static Prompt addToField(Messages.Name name, JTextComponent component) {
    if (useMacOsSearchPrompts(component)) {
      component.putClientProperty(MACOS_PROMPT_KEY, Messages.getMessage(name));
      return new MacOSPrompt(component);
    }
    return new PromptImpl(Messages.getMessage(name), component);
  }

  private static boolean useMacOsSearchPrompts(JTextComponent component) {
    // In theory, this feature would work really nicely - you get a search
    // icon and a gray x to delete everything.  Buuut - the prompt only shows
    // when the field is missing focus, which is awful in places where the search
    // field gets default focus.  So, no thanks.  If we want that search icon and
    // gray x, we should build them from scratch and use them on all platforms.
    return false
        && UIUtils.isMacOS()
        && "search".equals(component.getClientProperty("JTextField.variant"));
  }

  @Deprecated
  public static Prompt addToField(String text, JTextComponent component) {
    if (useMacOsSearchPrompts(component)) {
      component.putClientProperty(MACOS_PROMPT_KEY, text);
      return new MacOSPrompt(component);
    }
    return new PromptImpl(text, component);
  }

  public interface Prompt {
    Prompt changeAlpha(float alpha);
    Prompt showPromptOnce();
    String getText();
    @Deprecated
    void setText(String text);
    void setText(Messages.Name text);
  }
  
  private static class MacOSPrompt implements Prompt {

    private JTextComponent component;

    MacOSPrompt(JTextComponent component) {
      this.component = component;
    }

    @Override
    public Prompt changeAlpha(float alpha) {
      return this;
    }

    @Override
    public Prompt showPromptOnce() {
      return this;
    }

    @Override
    public String getText() {
      return (String) component.getClientProperty(MACOS_PROMPT_KEY);
    }

    @Override
    public void setText(String text) {
      component.putClientProperty(MACOS_PROMPT_KEY, text);
    }

    @Override
    public void setText(Name text) {
      component.putClientProperty(MACOS_PROMPT_KEY, Messages.getMessage(text));
    }   
  }
  
  private static class PromptImpl extends JLabel
      implements Prompt, FocusListener, DocumentListener, PropertyChangeListener {
    private final Document document;
    private boolean showPromptOnce;
    private int focusLost;

    public PromptImpl(String text, JTextComponent component) {
      document = component.getDocument();

      putClientProperty(FontManager.PLAIN_LABEL, true);
      setText(text);
      setFont(component.getFont());
      setForeground(component.getForeground());
      setHorizontalAlignment(JLabel.LEADING);

      component.addFocusListener(this);
      document.addDocumentListener(this);

      component.setLayout(new BorderLayout());
      component.add(BorderLayout.CENTER, this);
      checkForPrompt();
    }
    
    @Override
    public Prompt changeAlpha(float alpha) {
      changeAlpha((int) (alpha * 255));
      return this;
    }

    private void changeAlpha(int alpha) {
      alpha = alpha > 255 ? 255 : alpha < 0 ? 0 : alpha;

      Color foreground = getForeground();
      int red = foreground.getRed();
      int green = foreground.getGreen();
      int blue = foreground.getBlue();

      Color withAlpha = new Color(red, green, blue, alpha);
      super.setForeground(withAlpha);
    }

    @Override
    public Prompt showPromptOnce() {
      this.showPromptOnce = true;
      return this;
    }

    /**
     * Check whether the prompt should be visible or not. The visibility will
     * change on updates to the Document and on focus changes.
     */
    private void checkForPrompt() {
      // Text has been entered, remove the prompt
      if (document.getLength() > 0) {
        setVisible(false);
        return;
      }

      // Prompt has already been shown once, remove it
      if (showPromptOnce && focusLost > 0) {
        setVisible(false);
        return;
      }

      // Otherwise always visible
      setVisible(true);
    }
    
    @Override
    public void focusGained(FocusEvent e) {
      checkForPrompt();
    }

    @Override
    public void focusLost(FocusEvent e) {
      focusLost++;
      checkForPrompt();
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
      checkForPrompt();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
      checkForPrompt();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    @Override
    public void propertyChange(PropertyChangeEvent e) {
      if ("font".equals(e.getPropertyName())) {
        setFont((Font) e.getNewValue());
      }
    }

    @Override
    public void setText(Name text) {
      setText(Messages.getMessage(text));
    }
  }
}
