/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Locale;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultCaret;
import javax.swing.text.NumberFormatter;

import org.joda.time.Chronology;
import org.joda.time.LocalTime;
import org.joda.time.ReadablePartial;
import org.joda.time.chrono.GJChronology;
import org.joda.time.format.DateTimeFormat;

import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.scythebill.birdlist.model.util.DirtyImpl;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;

/**
 * Panel for inputting times.  TimePanel must be used in a FontManager-managed
 * UI.
 */
public class TimePanel extends JPanel implements FontsUpdatedListener {
  private JFormattedTextField hourField;
  private JFormattedTextField minuteField;
  private JComboBox<String> amPmCombo;
  // Chronology used by TimePanel (so far)
  private final Chronology chrono = GJChronology.getInstance();
  private LocalTime value;
  private NumberFormatter minuteFormatter;
  private NumberFormatter hourFormatter;
  private DirtyImpl dirty = new DirtyImpl(false);
  private Timer timer;
  
  // If true, do not send value change events
  private boolean dontUpdateValue;
  private JLabel hourMinuteSeparatorLabel;

  public TimePanel() {
    this(Locale.getDefault());
  }

  public TimePanel(Locale locale) {
    setLocale(locale);
    initGUI();
  }

  public LocalTime getValue() {
    return value;
  }

  public DirtyImpl getDirty() {
    return dirty;
  }

  @Override
  public void setFont(Font font) {
    super.setFont(font);
    if (hourField != null) {
      hourField.setFont(font);
      minuteField.setFont(font);
    }
    if (amPmCombo != null) {
      amPmCombo.setFont(font);
    }
  }
  
  public void setValue(LocalTime newValue) {
    dontUpdateValue = true;
    if (newValue != null && newValue.size() == 0) {
      newValue = null;
    }
    
    // No-op - do nothing
    if (!Objects.equal(newValue, getValue())) {
      if (newValue == null) {
        hourField.setText("");
        minuteField.setText("");
        if (amPmCombo != null) {
          amPmCombo.setSelectedIndex(0);
        }
      } else {
        int hourOfDay = newValue.getHourOfDay();
        if (amPmCombo != null) {
          // AM/PM hour
          amPmCombo.setSelectedIndex(hourOfDay < 12 ? 0 : 1);
          int halfDayHour = hourOfDay % 12;
          hourField.setValue(halfDayHour == 0 ? 12 : halfDayHour);
        } else {
          // 24-hour hour
          hourField.setValue(hourOfDay);
        }
  
        int minute = newValue.getMinuteOfHour();
        minuteField.setValue(minute);
      }
    }
      
    dontUpdateValue = false;
    
    updateValue(newValue);
  }

  private void initGUI() {
    // See if AM/PM is needed.  If "a" is in the time pattern ("-S"), then
    // it's 12-hour, otherwise it's 24-hour
    String pattern = DateTimeFormat.patternForStyle("-S", getLocale());
    boolean isTwentyFourHour = !pattern.contains("a");
    hourMinuteSeparatorLabel = new JLabel(hourMinuteSeparator(pattern));
    
    hourFormatter = new NumberFormatter(new DecimalFormat("#")) {
      @Override
      public Object stringToValue(String text) throws ParseException {
        if (Strings.isNullOrEmpty(text)) {
          return null;
        }

        return super.stringToValue(text);
      }
    };
    hourFormatter.setValueClass(Integer.class);
    hourFormatter.setMinimum(isTwentyFourHour ? 0 : 1);
    hourFormatter.setMaximum(isTwentyFourHour ? 23 : 12);

    minuteFormatter = new NumberFormatter(new DecimalFormat("00")) {
      @Override
      public Object stringToValue(String text) throws ParseException {
        if (Strings.isNullOrEmpty(text)) {
          return null;
        }

        return super.stringToValue(text);
      }
    };
    minuteFormatter.setValueClass(Integer.class);
    minuteFormatter.setMinimum(0);
    minuteFormatter.setMaximum(59);

    hourField = new AutoSelectJFormattedTextField(hourFormatter);
    MinuteOrHourChanged dayListener = new MinuteOrHourChanged(hourField);
    hourField.addPropertyChangeListener("value", dayListener);
    hourField.getDocument().addDocumentListener(dayListener);
    hourField.setCaret(new DefaultCaret());
    attachAquaCaret(hourField);
    
    minuteField = new AutoSelectJFormattedTextField(minuteFormatter);
    MinuteOrHourChanged yearListener = new MinuteOrHourChanged(minuteField);
    minuteField.addPropertyChangeListener("value", yearListener);
    minuteField.getDocument().addDocumentListener(yearListener);
    attachAquaCaret(minuteField);
    
    if (!isTwentyFourHour) {
      ComboBoxModel<String> amPmModel = new DefaultComboBoxModel<>(new String[]{"AM", "PM"});
      amPmCombo = new JComboBox<>(amPmModel);
      amPmCombo.setMaximumRowCount(2);
      amPmCombo.addActionListener(new AmPmChanged());
      amPmCombo.setEditable(false);
    }    
  }

  private class AmPmChanged implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      calculateNewValue();
    }
  }

  private class MinuteOrHourChanged implements PropertyChangeListener, DocumentListener {
    private JFormattedTextField field;

    public MinuteOrHourChanged(JFormattedTextField field) {
      this.field = field;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      calculateNewValue();
    }

    @Override
    public void changedUpdate(DocumentEvent event) {
      startTimerToCalculateNewValue(field);
    }

    @Override
    public void insertUpdate(DocumentEvent event) {
      startTimerToCalculateNewValue(field);
    }

    @Override
    public void removeUpdate(DocumentEvent event) {
      startTimerToCalculateNewValue(field);
    }
  }

  private void startTimerToCalculateNewValue(final JFormattedTextField field) {
    if (timer != null) {
      timer.stop();
    }
    timer = new Timer(100, new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent event) {
        try {
          field.commitEdit();
          calculateNewValue();
        } catch (ParseException e) {
          // OK
        }
      }
    });
    timer.setRepeats(false);
    timer.start();
  }

  private void calculateNewValue() {
    if (dontUpdateValue) {
      return;
    }
    
    LocalTime newValue;
    if (Strings.isNullOrEmpty(hourField.getText())) {
      newValue = null;
    } else {
      int hour = (Integer) hourField.getValue();
      // Convert 12-hour times to 0-23 if necessary
      if (amPmCombo != null) {
        if (hour == 12) {
          hour = 0;
        }
        boolean isPm = amPmCombo.getSelectedIndex() == 1;
        if (isPm) {
          hour = hour + 12;
        }
      }
  
      int minute;
      if (!Strings.isNullOrEmpty(minuteField.getText())) {
        minute = (Integer) minuteField.getValue();
      } else {
        minute = 0;
      }
      
      newValue = new LocalTime(hour, minute, 0, 0, chrono);
    }
    
    updateValue(newValue);
  }

  private void updateValue(LocalTime newValue) {
    ReadablePartial oldValue = value;
    value = newValue;
    if (!Objects.equal(oldValue, newValue)) {
      firePropertyChange("value", oldValue, newValue);
      dirty.setDirty(true);
    }
  }

  // Force a formatted-field caret to the usual text field caret
  // TODO: still needed?
  private void attachAquaCaret(JFormattedTextField field) {
    JTextField tempField = new JTextField();
    field.setCaret(tempField.getCaret());    
  }

  public void setEditable(boolean editable) {
    minuteField.setEditable(editable);
    if (amPmCombo != null) {
      // Enabled, not editable, for AM/PM - since it's a combobox
      amPmCombo.setEnabled(editable);
    }
    hourField.setEditable(editable);
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    GroupLayout layout = new GroupLayout(this);
    this.setLayout(layout);
    
    if (amPmCombo != null) {
      layout.setVerticalGroup(layout.createBaselineGroup(false, false)
          .addComponent(hourField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
          .addComponent(hourMinuteSeparatorLabel)
          .addComponent(minuteField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
          .addComponent(amPmCombo, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE));
      layout.setHorizontalGroup(layout.createSequentialGroup()
          .addComponent(hourField, PREFERRED_SIZE, fontManager.scale(35), PREFERRED_SIZE)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(hourMinuteSeparatorLabel)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(minuteField, PREFERRED_SIZE, fontManager.scale(35), PREFERRED_SIZE)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(amPmCombo, PREFERRED_SIZE, fontManager.scale(80), PREFERRED_SIZE));
        
      layout.linkSize(SwingConstants.VERTICAL, hourField, minuteField, amPmCombo, hourMinuteSeparatorLabel);
    } else {
      layout.setVerticalGroup(layout.createBaselineGroup(false, false)
          .addComponent(hourField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
          .addComponent(hourMinuteSeparatorLabel)
          .addComponent(minuteField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE));
      layout.setHorizontalGroup(layout.createSequentialGroup()
          .addComponent(hourField, PREFERRED_SIZE, fontManager.scale(40), PREFERRED_SIZE)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(hourMinuteSeparatorLabel)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(minuteField, PREFERRED_SIZE, fontManager.scale(40), PREFERRED_SIZE));
        
      layout.linkSize(SwingConstants.VERTICAL, hourField, minuteField, hourMinuteSeparatorLabel);
    }
  }
  
  @Override
  public int getBaseline(int width, int height) {
    return hourField.getBaseline(width, height);
  }

  /** Determine the character - if any - that separates hours from minutes in the current locale. */
  private static String hourMinuteSeparator(String format) {
    int firstMinute = format.indexOf('m');
    if (firstMinute > 0) {
      char charBeforeMinute = format.charAt(firstMinute - 1);
      if (charBeforeMinute != 'h' && charBeforeMinute != 'H') {
        return "" + charBeforeMinute;
      }
    }
    
    return "";
  }
}
