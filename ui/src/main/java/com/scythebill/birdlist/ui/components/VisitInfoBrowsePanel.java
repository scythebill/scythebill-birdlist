/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import org.joda.time.LocalTime;
import org.joda.time.ReadablePartial;

import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.ReportSetMutator;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfo.ObservationType;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.app.NavigablePanel;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.OpenMapUrl;

class VisitInfoBrowsePanel extends JPanel implements FontsUpdatedListener {
  interface UpdatedListener {
    void visitInfoUpdated(VisitInfoKey key);
    void visitInfoDeleted(VisitInfoKey key);
  }
  
  private final UpdatedListener updatedListener;
  private final VisitInfoPanel visitInfoPanel;
  private final ReportSet reportSet;
  private VisitInfoKey visitInfoKey;
  private VisitInfo visitInfo;
  private JButton saveButton;
  private JButton revertButton;
  private JButton deleteButton;
  private JButton editButton;
  private JSeparator separator;
  private LocalTime originalStartTime;
  private final NavigableFrame navigableFrame;
  private DatePanel whenPanel;
  private JLabel whenLabel;
  private ReadablePartial originalDate;
  private NewLocationDialog newLocationDialog;
  private PredefinedLocations predefinedLocations;
  private JLabel whereLabel;
  private WherePanel wherePanel;
  private String originalLocation;
  private final Action returnAction;
  private Alerts alerts;
  private final OpenMapUrl openMapUrl;

  VisitInfoBrowsePanel(
      ReportSet reportSet,
      VisitInfoKey visitInfoKey,
      VisitInfo visitInfo,
      VisitInfoPreferences visitInfoPreferences,
      Alerts alerts,
      FontManager fontManager,
      UpdatedListener updatedListener,
      NavigableFrame navigableFrame,
      NewLocationDialog newLocationDialog,
      PredefinedLocations predefinedLocations,
      OpenMapUrl openMapUrl,
      Action returnAction) {
    this.reportSet = reportSet;
    this.visitInfoKey = visitInfoKey;
    this.alerts = alerts;
    this.updatedListener = updatedListener;
    this.navigableFrame = navigableFrame;
    this.newLocationDialog = newLocationDialog;
    this.predefinedLocations = predefinedLocations;
    this.openMapUrl = openMapUrl;
    this.returnAction = returnAction;
    this.visitInfo = visitInfo != null
        ? visitInfo
        : VisitInfo.builder().withObservationType(ObservationType.HISTORICAL).build();
    this.visitInfoPanel = new VisitInfoPanel(
        visitInfoKey.startTime().orNull(), fontManager, visitInfoPreferences);
    this.originalStartTime = visitInfoKey.startTime().orNull();
    this.originalDate = visitInfoKey.date();
    this.originalLocation = visitInfoKey.locationId();
    visitInfoPanel.setValue(this.visitInfo);
    visitInfoPanel.getDirty().setDirty(false);
    visitInfoPanel.setPanelLayout(VisitInfoPanel.PanelLayout.NARROW);
    visitInfoPanel.setStartTimeEditable(true);
    initComponents();
  }

  private void initComponents() {
    whenLabel = new JLabel();
    whenLabel.setText(Messages.getMessage(Name.WHEN_QUESTION));
    whenPanel = new DatePanel();
    whenPanel.setValue(visitInfoKey.date());
    
    whereLabel = new JLabel();
    whereLabel.setText(Messages.getMessage(Name.WHERE_QUESTION));
    wherePanel = new WherePanel(reportSet, newLocationDialog, predefinedLocations, openMapUrl);
    wherePanel.setWhereLayoutStrategy(IndexerPanel.LayoutStrategy.BELOW);
    wherePanel.setWhereLayoutStrategy(IndexerPanel.LayoutStrategy.BELOW);
    wherePanel.setWhere(reportSet.getLocations().getLocation(visitInfoKey.locationId()));

    saveButton = new JButton();
    revertButton = new JButton();
    editButton = new JButton();
    deleteButton = new JButton();
    
    saveButton.setAction(new AbstractAction(Messages.getMessage(Name.SAVE_BUTTON)) {
      @Override public void actionPerformed(ActionEvent e) {
        save();
      }
    });
    editButton.setAction(new AbstractAction(Messages.getMessage(Name.EDIT_SIGHTINGS)) {
      @Override public void actionPerformed(ActionEvent e) {
        edit();
      }
    });
    
    revertButton.setAction(new AbstractAction(Messages.getMessage(Name.REVERT_BUTTON)) {
      @Override public void actionPerformed(ActionEvent event) {
        revert();
      }
    });
    
    deleteButton.setAction(new AbstractAction(Messages.getMessage(Name.DELETE_ALL)) {
      @Override public void actionPerformed(ActionEvent event) {
        delete();
      }
    });
    
    separator = new JSeparator();
    
    visitInfoPanel.addPropertyChangeListener("validValue", e -> updateButtonEnabledState());
    visitInfoPanel.addPropertyChangeListener("startTime", e -> updateButtonEnabledState());
    visitInfoPanel.getDirty().addDirtyListener(e -> updateButtonEnabledState());
    whenPanel.addPropertyChangeListener("value", e -> updateButtonEnabledState());
    wherePanel.addPropertyChangeListener("where", e -> updateButtonEnabledState());

    // Disable: since start-time editing was added, I'm too worried about a user
    // accidentally deleting the start time (and losing data), then clicking away.
//    addAncestorListener(new AncestorListener() {      
//      @Override
//      public void ancestorRemoved(AncestorEvent e) {
//        // Auto-save when the user navigates away, as long
//        // as the panel is in a valid state.
//        if (visitInfoPanel.isValidValue()
//            && visitInfoPanel.getDirty().isDirty()) {
//          save();
//        }
//      }
//      
//      @Override
//      public void ancestorMoved(AncestorEvent e) {
//      }
//      
//      @Override
//      public void ancestorAdded(AncestorEvent e) {
//      }
//    });
    updateButtonEnabledState();
  }
  
  private void updateButtonEnabledState() {
    boolean startTimeModified = !Objects.equal(originalStartTime, visitInfoPanel.getStartTime());
    boolean dateModified = !Objects.equal(originalDate, whenPanel.getValue());
    Location updatedWhere = wherePanel.getWhere();
    boolean locationModified = !Objects.equal(
        originalLocation, updatedWhere == null ? null : updatedWhere.getId());
    saveButton.setEnabled(
        startTimeModified
        || (dateModified && (whenPanel.getValue() != null))
        || (locationModified && updatedWhere != null)
        || (visitInfoPanel.isValidValue() && visitInfoPanel.getDirty().isDirty()));
    revertButton.setEnabled(
        startTimeModified
        || dateModified
        || locationModified
        || !visitInfoPanel.isValidValue()
        || visitInfoPanel.getDirty().isDirty());
    // Don't allow editing if there's any data updated - make the
    // user click Save first.
    editButton.setEnabled(!revertButton.isEnabled());
  }

  private void delete() {
    List<Sighting> sightingsToRemove = Lists.newArrayList();
    for (Sighting sighting : reportSet.getSightings()) {
      if (visitInfoKey.matches(sighting)) {
        sightingsToRemove.add(sighting);
      }
    }
    
    int okCancel = alerts.showOkCancel(this,
        Name.ARE_YOU_SURE,
        Name.VERIFY_DELETE_ALL_SIGHTINGS_FORMAT,
        sightingsToRemove.size());
    if (okCancel == JOptionPane.OK_OPTION) {
      reportSet.mutator()
          .removing(sightingsToRemove)
          .mutate();
      updatedListener.visitInfoDeleted(visitInfoKey);
    }
  }
  
  private void save() {
    if (visitInfoPanel.isValidValue()
        && wherePanel.getWhere() != null
        && whenPanel.getValue() != null) {
      VisitInfo edited = visitInfoPanel.getValue();
      
      if (!edited.equals(visitInfo)) {
        reportSet.putVisitInfo(visitInfoKey, edited);
        reportSet.markDirty();
        visitInfo = edited;
      }
      
      // If the start time or date has been edited, update all start times of all
      // associated sightings, and send through the mutator.  NOTE:  this *must* happen
      // *after* the edited visit info itself was stored in the report set, because the 
      // mutator here will take that visit info and move it over.
      LocalTime newStartTime = visitInfoPanel.getStartTime();
      ReadablePartial newDate = whenPanel.getValue();
      Location newLocation = wherePanel.getWhere();
      // Make sure (if it's a brand-new location) that it's added, so there's an ID
      reportSet.getLocations().ensureAdded(newLocation);
      
      if (!Objects.equal(newStartTime, originalStartTime)
          || !Objects.equal(newDate, originalDate)
          || !Objects.equal(newLocation.getId(), originalLocation)) {
        List<Sighting> sightingsToRemove = Lists.newArrayList();
        List<Sighting> sightingsToAdd = Lists.newArrayList();
        for (Sighting sighting : reportSet.getSightings()) {
          if (visitInfoKey.matches(sighting)) {
            sightingsToRemove.add(sighting);
            sightingsToAdd.add(sighting.asBuilder()
                .setTime(newStartTime)
                .setDate(newDate)
                .setLocation(newLocation)
                .build());
          }
        }
        
        if (!sightingsToRemove.isEmpty()) {
          ReportSetMutator mutator = reportSet.mutator()
              .adding(sightingsToAdd)
              .removing(sightingsToRemove);
          if (!Objects.equal(newStartTime, originalStartTime)) {
            mutator.withChangedStartTime(Optional.fromNullable(newStartTime));
          }
          if (!Objects.equal(newDate, originalDate)) {
            mutator.withChangedDate(newDate);
          }
          if (!Objects.equal(newLocation.getId(), originalLocation)) {
            mutator.withChangedLocation(newLocation.getId());
          }
          mutator.mutate();
        }
        
        visitInfoKey = visitInfoKey
            .withStartTime(Optional.fromNullable(newStartTime))
            .withDate(newDate)
            .withLocationId(newLocation.getId());
        originalStartTime = newStartTime;
        originalDate = newDate;
        originalLocation = newLocation.getId();
      }
      
      updatedListener.visitInfoUpdated(visitInfoKey);
      visitInfoPanel.getDirty().setDirty(false);
    }
    updateButtonEnabledState();
  }
  
  private void revert() {
    visitInfoPanel.setStartTime(originalStartTime);
    visitInfoPanel.setValue(visitInfo);
    whenPanel.setValue(originalDate);
    wherePanel.setWhere(reportSet.getLocations().getLocation(originalLocation));
    visitInfoPanel.getDirty().setDirty(false);
    updateButtonEnabledState();
  }
  
  private void edit() {
    if (visitInfoPanel.getDirty().isDirty()) {
      save();
    }
    JPanel sightingsPanel = navigableFrame.navigateToAndPush("sightings", returnAction);
    if (sightingsPanel != null) {
      ((NavigablePanel) sightingsPanel).navigateTo(visitInfoKey);
    }
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHonorsVisibility(true);
    
    whenLabel.setAlignmentX(0.0f);
    whenPanel.setAlignmentX(0.0f);
    setBorder(new EmptyBorder(0, 2, 0, 2));
    layout.setVerticalGroup(layout.createSequentialGroup()
        .addComponent(whenLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addComponent(whenPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(wherePanel)
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(visitInfoPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addGap(20, 20, Short.MAX_VALUE)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(editButton)
            .addComponent(deleteButton))
        .addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(saveButton)
            .addComponent(revertButton))
        .addGap(fontManager.scale(20)));
    
    layout.setHorizontalGroup(layout.createParallelGroup(Alignment.CENTER)
        .addComponent(visitInfoPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
        .addComponent(whenLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
        .addComponent(whenPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
        .addComponent(wherePanel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
        .addGroup(Alignment.LEADING, layout.createSequentialGroup()
            .addComponent(editButton)
            .addGap(fontManager.scale(2))
            .addComponent(deleteButton))
        .addComponent(separator)
        .addGroup(Alignment.LEADING, layout.createSequentialGroup()
            .addComponent(saveButton)
            .addGap(fontManager.scale(2))
            .addComponent(revertButton)));
    
    layout.linkSize(editButton, saveButton);
    layout.linkSize(deleteButton, revertButton);
  }
}

