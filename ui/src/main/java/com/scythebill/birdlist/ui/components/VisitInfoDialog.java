/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JDialog;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Dialog for showing visit info.
 */
public class VisitInfoDialog {
  private final FontManager fontManager;
  private final VisitInfoPreferences visitInfoPreferences;

  @Inject
  public VisitInfoDialog(
      FontManager fontManager,
      VisitInfoPreferences visitInfoPreferences) {
    this.fontManager = fontManager;
    this.visitInfoPreferences = visitInfoPreferences;
  }
  
  public void showVisitInfo(
      Window parent,
      final ReportSet reportSet,
      final VisitInfoKey visitInfoKey) {
    ModalityType modality =
        Toolkit.getDefaultToolkit().isModalityTypeSupported(ModalityType.DOCUMENT_MODAL)
        ? ModalityType.DOCUMENT_MODAL : ModalityType.APPLICATION_MODAL;
    final JDialog dialog = new JDialog(parent,
        Messages.getMessage(Name.VISIT_DATA_TITLE), modality);
    
    final VisitInfoPanel visitInfoPanel = new VisitInfoPanel(
        visitInfoKey.startTime().orNull(),
        fontManager, visitInfoPreferences); 
    Action cancelAction = new AbstractAction() {
      @Override public void actionPerformed(ActionEvent e) {
        dialog.dispose();
      }
    };
    final Action okAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent event) {
        if (visitInfoPanel.isValidValue()) {
          VisitInfo visitInfo = visitInfoPanel.getValue();
          reportSet.putVisitInfo(visitInfoKey, visitInfo);
          reportSet.markDirty();
          dialog.dispose();
        }
      }
    };
    // OK can be enabled only when it's dirty *and* valid
    PropertyChangeListener updateOk = new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        boolean isDirty = visitInfoPanel.getDirty().isDirty();
        boolean isValid = visitInfoPanel.isValidValue();
        okAction.setEnabled(isDirty && isValid);
      }
    };
    // ... but it starts out disabled
    okAction.setEnabled(false);
    visitInfoPanel.getDirty().addDirtyListener(updateOk);
    visitInfoPanel.addPropertyChangeListener("validValue", updateOk);
    OkCancelPanel okCancel = new OkCancelPanel(
        okAction, cancelAction, visitInfoPanel, null);
    visitInfoPanel.setValue(reportSet.getVisitInfo(visitInfoKey));
    visitInfoPanel.getDirty().setDirty(false);
    fontManager.applyTo(okCancel);
    dialog.setContentPane(okCancel);
    dialog.setSize(fontManager.scale(new Dimension(750, 500)));
    dialog.setVisible(true);
  }
}
