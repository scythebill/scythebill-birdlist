/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;

import javax.annotation.Nullable;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListCellRenderer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import org.joda.time.Duration;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;
import com.scythebill.birdlist.model.io.TimeIO;
import com.scythebill.birdlist.model.sighting.Area;
import com.scythebill.birdlist.model.sighting.Distance;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfo.ObservationType;
import com.scythebill.birdlist.model.sighting.VisitInfo.VisitField;
import com.scythebill.birdlist.model.util.DirtyImpl;
import com.scythebill.birdlist.model.util.ToString;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.fonts.FontPreferences;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.ToStringListCellRenderer;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Panel for displaying and editing visit information.
 */
public class VisitInfoPanel extends JPanel implements FontsUpdatedListener {
  public static void main(String[] args) {
     JFrame frame = new JFrame();
     frame.getContentPane().setLayout(new BorderLayout());
     FontManager fontManager = new FontManager(new FontPreferences());
     LocalTime startTime = TimeIO.fromString("15:54");
     VisitInfoPanel visitInfoPanel = new VisitInfoPanel(startTime, fontManager, new VisitInfoPreferences());
     visitInfoPanel.setPanelLayout(PanelLayout.NORMAL);
     visitInfoPanel.setStartTimeEditable(true);
     frame.getContentPane().add(visitInfoPanel, BorderLayout.CENTER);
     
     visitInfoPanel.addPropertyChangeListener("validValue", new PropertyChangeListener() {
       @Override
       public void propertyChange(PropertyChangeEvent e) {
         System.err.printf("%s : %s -> %s\n", e.getPropertyName(), e.getOldValue(), e.getNewValue());
       }
     });
     visitInfoPanel.addPropertyChangeListener("value", new PropertyChangeListener() {
       @Override
       public void propertyChange(PropertyChangeEvent e) {
         System.err.printf("%s : \n   %s \n-> %s\n", e.getPropertyName(), e.getOldValue(), e.getNewValue());
       }
     });
     visitInfoPanel.addPropertyChangeListener("startTime", new PropertyChangeListener() {
       @Override
       public void propertyChange(PropertyChangeEvent e) {
         System.err.printf("%s : \n   %s \n-> %s\n", e.getPropertyName(), e.getOldValue(), e.getNewValue());
       }
     });
     fontManager.applyTo(frame);
     frame.pack();
     frame.setVisible(true);
  }
  
  public enum PanelLayout {
    NORMAL,
    NARROW
  }
  
  public enum MilesOrKilometers {
    MILES(Name.MILES_TEXT),
    KILOMETERS(Name.KILOMETERS_TEXT);
    
    private final Name label;
    
    MilesOrKilometers(Messages.Name label) {
      this.label = label;
    }
    
    @Override public String toString() {
      return Messages.getMessage(label);
    }
  }

  public enum AcresOrHectares {
    ACRES(Name.ACRES_TEXT),
    HECTARES(Name.HECTARES_TEXT);
    
    private final Name label;
    
    AcresOrHectares(Name label) {
      this.label = label;
    }
    
    @Override public String toString() {
      return Messages.getMessage(label);
    }
  }
  
  private DirtyImpl dirty = new DirtyImpl(false);
  private JScrollPane commentsScrollPane;
  private boolean startTimeEditable = false;
  private boolean lastValidValue;
  private PanelLayout layout = PanelLayout.NORMAL;
  private VisitInfo lastValue;
  
  private final DocumentListener dirtyDocument = new DocumentListener() {
    @Override
    public void insertUpdate(DocumentEvent event) {
      dirty.setDirty(true);
      updateValueAndValid();
    }

    @Override
    public void removeUpdate(DocumentEvent event) {
      dirty.setDirty(true);
      updateValueAndValid();
    }

    @Override
    public void changedUpdate(DocumentEvent event) {
      dirty.setDirty(true);
      updateValueAndValid();
    }
  };
  private PossiblyRequiredLabel observationTypeLabel;
  private JComboBox<ObservationType> observationType;
  private PossiblyRequiredLabel durationLabel;
  private JFormattedTextField durationHoursField;
  private JFormattedTextField durationMinutesField;
  private JLabel durationHoursLabel;
  private JLabel durationMinutesLabel;
  private JTextArea observationTypeDocumentation;
  private PossiblyRequiredLabel areaLabel;
  private JFormattedTextField areaField;
  private JComboBox<AcresOrHectares> acresOrHectares;
  private PossiblyRequiredLabel distanceLabel;
  private JFormattedTextField distanceField;
  private PossiblyRequiredLabel partySizeLabel;
  private JFormattedTextField partySizeField;
  private PossiblyRequiredLabel commentsLabel;
  private JTextArea comments;
  private JComboBox<MilesOrKilometers> milesOrKilometers;
  private boolean editable;
  private FontManager fontManager;
  private LocalTime startTime;
  private PossiblyRequiredLabel startTimeLabel;
  private JLabel startTimeValue;
  private final VisitInfoPreferences visitInfoPreferences;
  private TimePanel startTimeField;
  private JCheckBox completeChecklist;
  

  public VisitInfoPanel(
      @Nullable LocalTime startTime,
      FontManager fontManager,
      VisitInfoPreferences visitInfoPreferences) {
    this.startTime = startTime;
    this.fontManager = fontManager;
    this.visitInfoPreferences = visitInfoPreferences;
    initComponents();
    attachListeners();
    updateValueAndValid();
  }

  public DirtyImpl getDirty() {
    return dirty;
  }

  @Override
  public void setEnabled(boolean enabled) {
    super.setEnabled(enabled);

    // And set the enabled state of all of our children too
    durationHoursField.setEnabled(enabled);
    durationMinutesField.setEnabled(enabled);
    distanceField.setEnabled(enabled);
    areaField.setEnabled(enabled);
    partySizeField.setEnabled(enabled);
    comments.setEnabled(enabled);

    setEditableOrEnabled();    
  }

  public void setEditable(boolean editable) {
    this.editable = editable;
    durationHoursField.setEditable(editable);
    durationMinutesField.setEditable(editable);
    distanceField.setEditable(editable);
    areaField.setEditable(editable);
    partySizeField.setEditable(editable);
    comments.setEditable(editable);
    
    setEditableOrEnabled();
  }

  private void setEditableOrEnabled() {
    acresOrHectares.setEnabled(editable && isEnabled());
    milesOrKilometers.setEnabled(editable && isEnabled());
    observationType.setEnabled(editable && isEnabled());
    completeChecklist.setEnabled(editable && isEnabled());
  }
  
  private void attachListeners() {
    PropertyChangeListener dirtyProperty = new PropertyChangeListener() {
      @Override public void propertyChange(PropertyChangeEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
        if (oldValue == null) {
          if (newValue == null)
            return;
        } else if (oldValue.equals(newValue)) {
          return;
        }

        dirty.setDirty(true);
        updateValueAndValid();
      }
    };
    ActionListener dirtyAction = new ActionListener() {
      @Override public void actionPerformed(ActionEvent event) {
        dirty.setDirty(true);
        updateValueAndValid();
      }
    };

    // Report edits to the start time field - and update the UI accordingly
    startTimeField.addPropertyChangeListener("value", new PropertyChangeListener() {
      @Override public void propertyChange(PropertyChangeEvent event) {
        if (startTimeEditable) {
          LocalTime newStartTime = startTimeField.getValue();
          LocalTime oldStartTime = startTime;
          if (!Objects.equal(oldStartTime, newStartTime)) {
            setStartTime(newStartTime);
            firePropertyChange("startTime", oldStartTime, newStartTime);
          }
        }
      }
    });
    
    durationHoursField.addPropertyChangeListener("value", dirtyProperty);
    durationMinutesField.addPropertyChangeListener("value", dirtyProperty);
    distanceField.addPropertyChangeListener("value", dirtyProperty);
    observationType.addActionListener(dirtyAction);
    milesOrKilometers.addActionListener(dirtyAction);
    areaField.addPropertyChangeListener("value", dirtyProperty);
    acresOrHectares.addActionListener(dirtyAction);
    partySizeField.addPropertyChangeListener("value", dirtyProperty);
    comments.getDocument().addDocumentListener(dirtyDocument);
    completeChecklist.addActionListener(dirtyAction);
    
    observationType.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent event) {
        updateObservationType();
        fontManager.applyTo(VisitInfoPanel.this);
      }
    });
    
    updateObservationType();
  }

  /**
   * Returns true if the current state of the panel is valid.
   */
  public boolean isValidValue() {
    ObservationType type = (ObservationType) observationType.getSelectedItem();
    ImmutableSet<VisitField> requiredFields = type.getRequiredFields();
    if (requiredFields.contains(VisitField.AREA)) {
      if (areaField.getValue() == null) {
        return false;
      }
    }

    if (requiredFields.contains(VisitField.DISTANCE)) {
      if (distanceField.getValue() == null) {
        return false;
      }
    }

    if (requiredFields.contains(VisitField.DURATION)) {
      if (durationHoursField.getValue() == null
          && durationMinutesField.getValue() == null) {
        return false;
      }
    }

    if (requiredFields.contains(VisitField.PARTY_SIZE)) {
      if (partySizeField.getValue() == null) {
        return false;
      }
    }
    
    return true;
  }
  
  /**
   * Get the value of the panel.
   */
  public VisitInfo getValue() {
    VisitInfo.Builder builder = VisitInfo.builder();
    builder.withObservationType((ObservationType) observationType.getSelectedItem());
    
    Integer hours = (Integer) durationHoursField.getValue();
    Integer minutes = (Integer) durationMinutesField.getValue();
    if (hours != null || minutes != null) {
      Duration duration = Duration.ZERO;
      if (hours != null) {
        duration = duration.plus(Duration.standardHours(hours));
      }
      if (minutes != null) {
        duration = duration.plus(Duration.standardMinutes(minutes));
      }
      builder.withDuration(duration);
    }
    
    Float distance = (Float) distanceField.getValue();
    if (distance != null) {
      if (milesOrKilometers.getSelectedItem() == MilesOrKilometers.MILES) {
        builder.withDistance(Distance.inMiles(distance));
      } else {
        builder.withDistance(Distance.inKilometers(distance));
      }
      visitInfoPreferences.milesOrKilometers = (MilesOrKilometers) milesOrKilometers.getSelectedItem();
    }
    
    Float area = (Float) areaField.getValue();
    if (area != null) {
      if (acresOrHectares.getSelectedItem() == AcresOrHectares.ACRES) {
        builder.withArea(Area.inAcres(area));
      } else {
        builder.withArea(Area.inHectares(area));
      }
      visitInfoPreferences.acresOrHectares = (AcresOrHectares) acresOrHectares.getSelectedItem();
    }
    
    Integer partySize = (Integer) partySizeField.getValue();
    if (partySize != null) {
      builder.withPartySize(partySize);
    }
    
    String comments = this.comments.getText();
    if (!comments.isEmpty()) {
      builder.withComments(comments);
    }
    
    builder.withCompleteChecklist(completeChecklist.isSelected() && completeChecklist.isVisible());
    
    return builder.build();
  }

  /** Update the value. */
  public void setValue(VisitInfo visitInfo) {
    // If null, clear all fields
    if (visitInfo == null) {
      areaField.setText("");
      comments.setText("");
      distanceField.setText("");
      durationHoursField.setText("");
      durationMinutesField.setText("");
      partySizeField.setText("");
      completeChecklist.setSelected(false);
      return;
    }
    
    if (visitInfo.observationType().getRequiredFields().contains(VisitField.START_TIME)
        && startTime == null) {
      throw new IllegalArgumentException(String.format(
          "Observation type %s requires a start time, but none given",
          visitInfo.observationType()));
    }
    
    observationType.setSelectedItem(visitInfo.observationType());
    if (visitInfo.area().isPresent()) {
      AcresOrHectares areaType = (AcresOrHectares) acresOrHectares.getSelectedItem();
      switch (areaType) {
        case ACRES:
          areaField.setValue(visitInfo.area().get().acres());
          break;
        case HECTARES:
          areaField.setValue(visitInfo.area().get().hectares());
          break;
        default:
          throw new AssertionError();
      }
    } else {
      areaField.setText("");
    }
    
    if (visitInfo.comments().isPresent()) {
      comments.setText(visitInfo.comments().get());
      // ... move the caret back to the start
      comments.setCaretPosition(0);
    } else {
      comments.setText("");
    }
    
    if (visitInfo.distance().isPresent()) {
      MilesOrKilometers distanceType = (MilesOrKilometers) milesOrKilometers.getSelectedItem();
      switch (distanceType) {
        case MILES:
          distanceField.setValue(visitInfo.distance().get().miles());
          break;
        case KILOMETERS:
          distanceField.setValue(visitInfo.distance().get().kilometers());
          break;
        default:
          throw new AssertionError();
      }
    } else {
      distanceField.setText("");
    }
    
    if (visitInfo.duration().isPresent()) {
      Duration duration = visitInfo.duration().get();
      int standardMinutes = Math.min( 
          Ints.saturatedCast(duration.getStandardMinutes()),
          1439);
      durationHoursField.setValue(standardMinutes / 60);
      durationMinutesField.setValue(standardMinutes % 60);
    } else {
      durationHoursField.setText("");
      durationMinutesField.setText("");
    }
    
    if (visitInfo.partySize().isPresent()) {
      partySizeField.setValue(visitInfo.partySize().get());
    } else {
      partySizeField.setText("");
    }
    
    completeChecklist.setSelected(visitInfo.completeChecklist());
  }

  public LocalTime getStartTime() {
    return startTimeField.getValue();
  }
  
  /** Update the start time.  This may require rebuilding the set of allowed observation types. */
  public void setStartTime(LocalTime startTime) {
    this.startTime = startTime;
    ObservationType observationType = (ObservationType) this.observationType.getSelectedItem();
    updateUiForStartTime();
    
    DefaultComboBoxModel<ObservationType> model = (DefaultComboBoxModel<ObservationType>) this.observationType.getModel();
    int indexOf = model.getIndexOf(observationType);
    if (indexOf >= 0) {
      model.setSelectedItem(observationType);
    }
    
    updateObservationType();
  }

  public void tryDefaultObservationType(ObservationType defaultObservationType) {
    DefaultComboBoxModel<ObservationType> model = (DefaultComboBoxModel<ObservationType>) this.observationType.getModel();
    int indexOf = model.getIndexOf(defaultObservationType);
    if (indexOf >= 0) {
      model.setSelectedItem(defaultObservationType);
    }
  }

  private void updateUiForStartTime() {
    ObservationType[] validTypes;
    
    if (startTime == null) {
      startTimeValue.setText(Messages.getMessage(Name.NONE_IN_PARENTHESES));
      startTimeField.setValue(null);
      // Only observation types that don't require a start time
      List<ObservationType> typesList = Lists.newArrayList();
      for (ObservationType type : ObservationType.values()) {
        if (!type.getRequiredFields().contains(VisitField.START_TIME)) {
          typesList.add(type);
        }
      }
      validTypes = typesList.toArray(new ObservationType[typesList.size()]);
    } else {
      startTimeValue.setText(DateTimeFormat.shortTime().print(startTime));
      startTimeField.setValue(startTime);
      // All observation types are OK if you have a start time 
      validTypes = ObservationType.values();
    }
    startTimeField.getDirty().setDirty(false);

    observationType.setModel(new DefaultComboBoxModel<ObservationType>(validTypes));
    observationType.setMaximumRowCount(observationType.getItemCount());
  }
  
  private void initComponents() {
    startTimeLabel = new PossiblyRequiredLabel(Messages.getMessage(Name.START_TIME_LABEL));
    startTimeValue = new JLabel();
    startTimeValue.putClientProperty(FontManager.PLAIN_LABEL, true);
    startTimeField = new TimePanel();
    
    observationTypeLabel = new PossiblyRequiredLabel(
        Messages.getMessage(Name.EBIRD_OBSERVATION_TYPE_LABEL));
    observationTypeLabel.setRequired(true);
    observationType = new JComboBox<>();
    @SuppressWarnings("unchecked")
    ListCellRenderer<String> base = (ListCellRenderer<String>) new JComboBox<String>().getRenderer();
    ToString<ObservationType> toString = new ToString<ObservationType>() {
      @Override
      public String getString(ObservationType o) {
        return Messages.getText(o);
      }
    };
    observationType.setRenderer(new ToStringListCellRenderer<ObservationType>(
        toString, ObservationType.class, base));
    updateUiForStartTime();
    observationTypeDocumentation = new JTextArea();
    observationTypeDocumentation.setOpaque(false);
    observationTypeDocumentation.setColumns(40);
    observationTypeDocumentation.setRows(3);
    observationTypeDocumentation.setFocusable(false);
    observationTypeDocumentation.setEditable(false);

    durationLabel = new PossiblyRequiredLabel(Messages.getMessage(Name.DURATION_LABEL));
    durationHoursField = dirtySafeField();
    durationHoursLabel = new JLabel(Messages.getMessage(Name.HOURS_ABBREVIATION));
    durationHoursLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    durationMinutesField = dirtySafeField();
    durationMinutesLabel = new JLabel(Messages.getMessage(Name.MINUTES_ABBREVIATION));
    durationMinutesLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    distanceLabel = new PossiblyRequiredLabel(Messages.getMessage(Name.DISTANCE_LABEL));
    distanceField = dirtySafeField();
    
    areaLabel = new PossiblyRequiredLabel(Messages.getMessage(Name.AREA_LABEL));
    areaField = dirtySafeField();
    milesOrKilometers = new JComboBox<>(MilesOrKilometers.values());
    milesOrKilometers.setSelectedItem(visitInfoPreferences.milesOrKilometers);
    acresOrHectares = new JComboBox<>(AcresOrHectares.values());
    acresOrHectares.setSelectedItem(visitInfoPreferences.acresOrHectares);
    partySizeLabel = new PossiblyRequiredLabel(Messages.getMessage(Name.PARTY_SIZE_LABEL));
    partySizeField = dirtySafeField();
    
    commentsLabel = new PossiblyRequiredLabel(Messages.getMessage(Name.COMMENTS_LABEL));
    commentsScrollPane = new JScrollPane();

    comments = new JTextArea();
    commentsScrollPane.setViewportView(comments);
    comments.setWrapStyleWord(true);
    comments.setLineWrap(true);

    UIUtils.fixTabOnTextArea(comments);

    durationHoursField.setColumns(4);
    NumberFormatter hoursFormatter = allowNullIntegerFormatter();
    hoursFormatter.setMaximum(23);
    hoursFormatter.setMinimum(0);
    hoursFormatter.setCommitsOnValidEdit(true);
    durationHoursField.setFormatterFactory(new DefaultFormatterFactory(hoursFormatter));

    durationMinutesField.setColumns(4);
    NumberFormatter minutesFormatter = allowNullIntegerFormatter();
    minutesFormatter.setMaximum(59);
    minutesFormatter.setMinimum(0);
    minutesFormatter.setCommitsOnValidEdit(true);
    durationMinutesField.setFormatterFactory(new DefaultFormatterFactory(minutesFormatter));
    
    partySizeField.setColumns(4);
    NumberFormatter partySizeFormatter = allowNullIntegerFormatter();
    partySizeFormatter.setMinimum(1);
    partySizeFormatter.setCommitsOnValidEdit(true);
    partySizeField.setFormatterFactory(new DefaultFormatterFactory(partySizeFormatter));

    distanceField.setColumns(6);
    NumberFormatter distanceFormatter = allowNullDecimalFormatter();
    distanceFormatter.setValueClass(Float.class);
    distanceFormatter.setMinimum(0f);
    distanceFormatter.setMaximum(999.999f);
    distanceFormatter.setCommitsOnValidEdit(true);
    distanceField.setFormatterFactory(new DefaultFormatterFactory(distanceFormatter));

    areaField.setColumns(6);
    NumberFormatter areaFormatter = allowNullDecimalFormatter();
    areaFormatter.setValueClass(Float.class);
    areaFormatter.setMinimum(0f);
    areaFormatter.setMaximum(999.999f);
    areaFormatter.setCommitsOnValidEdit(true);
    areaField.setFormatterFactory(new DefaultFormatterFactory(areaFormatter));

    completeChecklist = new JCheckBox(
        Messages.getMessage(Name.COMPLETE_LIST_OF_SIGHTINGS_QUESTION));
  }

  private NumberFormatter allowNullIntegerFormatter() {
    return new NumberFormatter(NumberFormat.getIntegerInstance()) {
      @Override
      public Object stringToValue(String string) throws ParseException {
        if ("".equals(string)) {
          return null;
        }
        return super.stringToValue(string);
      }
    };
  }

  private NumberFormatter allowNullDecimalFormatter() {
    NumberFormat numberFormat = NumberFormat.getNumberInstance();
    numberFormat.setMaximumIntegerDigits(3);
    numberFormat.setMaximumFractionDigits(3);
    return new NumberFormatter(numberFormat) {
      @Override
      public Object stringToValue(String string) throws ParseException {
        if ("".equals(string)) {
          return null;
        }
        return super.stringToValue(string);
      }
    };
  }
  
  @Override
  public void fontsUpdated(FontManager fontManager) {
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHonorsVisibility(true);
    layout.setAutoCreateGaps(true);
    
    JComponent startTimeComponent = startTimeEditable
        ? startTimeField : startTimeValue;
        
    if (getPanelLayout() == PanelLayout.NORMAL) {
      comments.setRows(6);
      comments.setColumns(40);
      layout.setHorizontalGroup(layout.createSequentialGroup()
          .addGroup(layout.createParallelGroup(Alignment.TRAILING)
              .addComponent(startTimeLabel)
              .addComponent(observationTypeLabel)
              .addComponent(durationLabel)
              .addComponent(distanceLabel)
              .addComponent(areaLabel)
              .addComponent(partySizeLabel)
              .addComponent(commentsLabel))
          .addPreferredGap(ComponentPlacement.RELATED)
          .addGroup(layout.createParallelGroup(Alignment.LEADING)
              .addComponent(startTimeComponent)
              .addComponent(observationType, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
              .addComponent(observationTypeDocumentation, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
              .addGroup(layout.createSequentialGroup()
                  .addComponent(durationHoursField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                  .addComponent(durationHoursLabel)
                  .addPreferredGap(ComponentPlacement.RELATED)
                  .addComponent(durationMinutesField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                  .addComponent(durationMinutesLabel))
              .addGroup(layout.createSequentialGroup()
                  .addComponent(distanceField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                  .addComponent(milesOrKilometers, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
              .addGroup(layout.createSequentialGroup()
                  .addComponent(areaField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                  .addComponent(acresOrHectares, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
              .addComponent(partySizeField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
              .addComponent(commentsScrollPane, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
              .addComponent(completeChecklist, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)));
      layout.setVerticalGroup(layout.createSequentialGroup()
          .addGroup(layout.createBaselineGroup(false, false)
              .addComponent(commentsLabel)
              .addComponent(commentsScrollPane))
          .addGroup(layout.createBaselineGroup(false, false)
              .addComponent(observationTypeLabel)
              .addComponent(observationType))
          .addComponent(observationTypeDocumentation, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
          .addGroup(layout.createBaselineGroup(false, false)
              .addComponent(startTimeLabel)
              .addComponent(startTimeComponent))
          .addGroup(layout.createBaselineGroup(false, false)
              .addComponent(durationLabel)
              .addComponent(durationHoursField)
              .addComponent(durationHoursLabel)
              .addComponent(durationMinutesField)
              .addComponent(durationMinutesLabel))
          .addGroup(layout.createBaselineGroup(false, false)
              .addComponent(distanceLabel)
              .addComponent(distanceField)
              .addComponent(milesOrKilometers))
          .addGroup(layout.createBaselineGroup(false, false)
              .addComponent(areaLabel)
              .addComponent(areaField)
              .addComponent(acresOrHectares))
          .addGroup(layout.createBaselineGroup(false, false)
              .addComponent(partySizeLabel)
              .addComponent(partySizeField))
          .addComponent(completeChecklist, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE));
    } else {
      comments.setRows(4);
      comments.setColumns(28);
      layout.setAutoCreateGaps(true);
      layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
          .addComponent(observationTypeLabel)
          .addComponent(observationType, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
          .addComponent(startTimeLabel)
          .addComponent(startTimeComponent, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
          .addComponent(durationLabel)
          .addGroup(layout.createSequentialGroup()
              .addComponent(durationHoursField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
              .addComponent(durationHoursLabel)
              .addPreferredGap(ComponentPlacement.RELATED)
              .addComponent(durationMinutesField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
              .addComponent(durationMinutesLabel))
          .addComponent(distanceLabel)
          .addGroup(layout.createSequentialGroup()
              .addComponent(distanceField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
              .addComponent(milesOrKilometers, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
          .addComponent(areaLabel)
          .addGroup(layout.createSequentialGroup()
              .addComponent(areaField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
              .addComponent(acresOrHectares, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
          .addComponent(partySizeLabel)
          .addComponent(partySizeField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
          .addComponent(commentsLabel)
          .addComponent(commentsScrollPane, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
          .addComponent(completeChecklist, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE));
      layout.setVerticalGroup(layout.createSequentialGroup()
          .addComponent(commentsLabel)
          .addComponent(commentsScrollPane, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
          .addComponent(observationTypeLabel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
          .addComponent(observationType, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
          .addComponent(startTimeLabel)
          .addComponent(startTimeComponent, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
          .addComponent(durationLabel)
          .addGroup(layout.createBaselineGroup(false, false)
              .addComponent(durationHoursField)
              .addComponent(durationHoursLabel)
              .addComponent(durationMinutesField)
              .addComponent(durationMinutesLabel))
          .addComponent(distanceLabel)
          .addGroup(layout.createBaselineGroup(false, false)
              .addComponent(distanceField)
              .addComponent(milesOrKilometers))
          .addComponent(areaLabel)
          .addGroup(layout.createBaselineGroup(false, false)
              .addComponent(areaField)
              .addComponent(acresOrHectares))
          .addComponent(partySizeLabel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
          .addComponent(partySizeField, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
          .addComponent(completeChecklist, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE));
    }
  }

  private JFormattedTextField dirtySafeField() {
    return new AutoSelectJFormattedTextField() {
      @Override
      protected void processFocusEvent(FocusEvent event) {
        // Add and remove the document listener as the field gains and loses focus.
        // Keeping the listener attached all the time cause the field to be marked
        // dirty every time focus enters and leaves, as JFormattedTextField runs
        // the formatter and uses that to set the text on focus changes.
        if (event.getID() == FocusEvent.FOCUS_LOST) {
          getDocument().removeDocumentListener(dirtyDocument);
        }
        super.processFocusEvent(event);
        if (event.getID() == FocusEvent.FOCUS_GAINED) {
          getDocument().addDocumentListener(dirtyDocument);
        }
      }
    };
  }

  private void updateObservationType() {
    ObservationType type = (ObservationType) observationType.getSelectedItem();
    ImmutableSet<VisitField> hiddenFields = type.getHiddenFields();
    
    durationLabel.setVisible(!hiddenFields.contains(VisitField.DURATION));
    durationHoursField.setVisible(!hiddenFields.contains(VisitField.DURATION));
    durationHoursLabel.setVisible(!hiddenFields.contains(VisitField.DURATION));
    durationMinutesField.setVisible(!hiddenFields.contains(VisitField.DURATION));
    durationMinutesLabel.setVisible(!hiddenFields.contains(VisitField.DURATION));
    
    distanceLabel.setVisible(!hiddenFields.contains(VisitField.DISTANCE));
    distanceField.setVisible(!hiddenFields.contains(VisitField.DISTANCE));
    milesOrKilometers.setVisible(!hiddenFields.contains(VisitField.DISTANCE));

    areaLabel.setVisible(!hiddenFields.contains(VisitField.AREA));
    areaField.setVisible(!hiddenFields.contains(VisitField.AREA));
    acresOrHectares.setVisible(!hiddenFields.contains(VisitField.AREA));
    
    partySizeField.setVisible(!hiddenFields.contains(VisitField.PARTY_SIZE));
    partySizeLabel.setVisible(!hiddenFields.contains(VisitField.PARTY_SIZE));

    ImmutableSet<VisitField> requiredFields = type.getRequiredFields();
    durationLabel.setRequired(requiredFields.contains(VisitField.DURATION));
    distanceLabel.setRequired(requiredFields.contains(VisitField.DISTANCE));
    areaLabel.setRequired(requiredFields.contains(VisitField.AREA));
    partySizeLabel.setRequired(requiredFields.contains(VisitField.PARTY_SIZE));
    startTimeLabel.setRequired(requiredFields.contains(VisitField.START_TIME));
    
    completeChecklist.setVisible(!hiddenFields.contains(VisitField.COMPLETE_CHECKLIST));
    
    if (partySizeLabel.isRequired()
        && "".equals(partySizeField.getText())) {
      partySizeField.setValue(1);
    }
    
    String documentation = Messages.getDescription(type);
    if (startTime == null) {
      documentation += "\n\n" + Messages.getMessage(Name.START_TIME_ISNT_SET);
    }
    observationTypeDocumentation.setText(documentation);
  }
  
  private void updateValueAndValid() {
    boolean validValue = isValidValue();
    if (validValue != lastValidValue) {
      firePropertyChange("validValue", lastValidValue, validValue);
      lastValidValue = validValue;
    }
    
    if (validValue) {
      VisitInfo value = getValue();
      if (!Objects.equal(value, lastValue)) {
        firePropertyChange("value", lastValue, value);
        lastValue = value;
      }
    }
  }

  public PanelLayout getPanelLayout() {
    return layout;
  }

  public void setPanelLayout(PanelLayout layout) {
    this.layout = layout;
  }


  public boolean isStartTimeEditable() {
    return startTimeEditable;
  }

  public void setStartTimeEditable(boolean startTimeEditable) {
    this.startTimeEditable = startTimeEditable;
  }

  public void setPartySize(int size) {
    partySizeField.setValue(size);
  }
}