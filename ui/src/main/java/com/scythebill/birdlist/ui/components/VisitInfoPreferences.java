/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import java.util.Locale;

import com.google.common.collect.ImmutableSet;
import com.scythebill.birdlist.model.annotations.Preference;
import com.scythebill.birdlist.ui.components.VisitInfoPanel.AcresOrHectares;
import com.scythebill.birdlist.ui.components.VisitInfoPanel.MilesOrKilometers;

/** Preferences used for visit info data. */
public final class VisitInfoPreferences {
  @Preference public MilesOrKilometers milesOrKilometers = defaultToMetric()
      ? MilesOrKilometers.KILOMETERS : MilesOrKilometers.MILES;
  @Preference public AcresOrHectares acresOrHectares = defaultToMetric()
      ? AcresOrHectares.HECTARES : AcresOrHectares.ACRES;

  private static final ImmutableSet<String> NON_METRIC_COUNTRIES =
      ImmutableSet.of("US", "UK", "GB");
  private static boolean defaultToMetric() {
    String country = Locale.getDefault().getCountry().toUpperCase();
    return !NON_METRIC_COUNTRIES.contains(country);
  }
}