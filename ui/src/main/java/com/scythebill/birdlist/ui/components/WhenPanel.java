/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.Dimension;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.joda.time.ReadablePartial;

import com.scythebill.birdlist.model.util.DirtyImpl;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Panel for entering a date.
 */
public class WhenPanel extends JPanel implements FontsUpdatedListener {
  private DatePanel whenPanel;
  private JLabel whenLabel;
  private DirtyImpl dirty = new DirtyImpl(false);

  public WhenPanel(FontManager fontManager) {
    initGUI();
    hookUpContents();
    fontManager.applyTo(this);
  }

  private void hookUpContents() {
    whenPanel.addPropertyChangeListener("value",
        evt -> {
          firePropertyChange("when", evt.getOldValue(), evt.getNewValue());
          dirty.setDirty(true);
        });

  }

  public DirtyImpl getDirty() {
    return dirty;
  }

  public ReadablePartial getWhen() {
    return whenPanel.getValue();
  }

  public void setWhen(ReadablePartial partial) {
    whenPanel.setValue(partial);
  }

  private void initGUI() {
    whenLabel = new JLabel();
    whenLabel.setText(Messages.getMessage(Name.WHEN_QUESTION));
    whenPanel = new DatePanel();
  }

  public void setEditable(boolean editable) {
    whenPanel.setEditable(editable);
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    GroupLayout layout = new GroupLayout(this);
    this.setLayout(layout);
    setPreferredSize(fontManager.scale(new Dimension(400, 300)));

    layout.setHorizontalGroup(layout.createParallelGroup()
        .addComponent(whenLabel)
        .addComponent(whenPanel));

    layout.setVerticalGroup(layout.createSequentialGroup()
        .addComponent(whenLabel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
        .addComponent(whenPanel, PREFERRED_SIZE, fontManager.scale(35), PREFERRED_SIZE));    
  }
}
