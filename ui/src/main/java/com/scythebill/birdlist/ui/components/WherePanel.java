/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;

import javax.annotation.Nullable;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicates;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.util.DirtyImpl;
import com.scythebill.birdlist.ui.components.IndexerPanel.LayoutStrategy;
import com.scythebill.birdlist.ui.components.NewLocationDialog.LocationReceiver;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.OpenMapUrl;

/**
 * Panel for entering a place.
 */
public class WherePanel extends JPanel {
  private IndexerPanel<Object> whereIndexerPanel;
  private JLabel whereLabel;
  private final ReportSet reportSet;
  private final PredefinedLocations predefinedLocations;
  private final NewLocationDialog newLocationDialog;
  private JButton newLocation;
  private Location where;
  private DirtyImpl dirty = new DirtyImpl(false);
  private String nullName;
  private WherePanelIndexers whereIndexers;
  private JLabel latLongIcon;

  private static final ImageIcon latLongOriginalSize = new ImageIcon(WherePanel.class.getResource("latlong.png"));
  private final OpenMapUrl openMapUrl;

  @Inject
  public WherePanel(
      ReportSet reportSet, NewLocationDialog newLocationDialog, PredefinedLocations predefinedLocations,
      OpenMapUrl openMapUrl) {
    this(reportSet, newLocationDialog, predefinedLocations, openMapUrl, null);
  }

  public WherePanel(
      ReportSet reportSet, NewLocationDialog newLocationDialog, PredefinedLocations predefinedLocations, OpenMapUrl openMapUrl, String nullName) {
    this.reportSet = reportSet;
    this.newLocationDialog = newLocationDialog;
    this.predefinedLocations = predefinedLocations;
    this.nullName = nullName;
    this.openMapUrl = openMapUrl;

    initGUI();
    hookUpContents();
  }
  
  public void setWhereLayoutStrategy(IndexerPanel.LayoutStrategy layoutStrategy) {
    whereIndexerPanel.setLayoutStrategy(layoutStrategy);
  }

  private void hookUpContents() {
    updateIdToString(nullName);
    whereIndexerPanel.addPropertyChangeListener("value",
        new PropertyChangeListener() {
          @Override
          public void propertyChange(PropertyChangeEvent event) {
            Location location = WherePanelIndexers.toLocation(
                reportSet.getLocations(), event.getNewValue());
            if (location == null) {
              setWhereImpl(null);
              newLocation.setEnabled(!whereIndexerPanel.getTextValue().isEmpty());
            } else {
              setWhereImpl(location);
              newLocation.setEnabled(false);
            }
          }
        });
    whereIndexerPanel.getTextComponent().getDocument().addDocumentListener(
        new DocumentListener() {
          @Override public void removeUpdate(DocumentEvent e) {
            fireWhereTextChanged();
          }
          
          @Override public void insertUpdate(DocumentEvent e) {
            fireWhereTextChanged();
          }
          
          @Override public void changedUpdate(DocumentEvent e) {
            fireWhereTextChanged();
          }              
        });
    

    newLocation.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        LocationReceiver locationReceiver = new LocationReceiver() {
          @Override public void locationAvailable(Location location) {
            reportSet.getLocations().ensureAdded(location);
            setWhere(location);
          }
        };
        newLocationDialog.newLocation(
            whereIndexerPanel.getTextValue(),
            SwingUtilities.getWindowAncestor(WherePanel.this),
            locationReceiver);
      }
    });

    latLongIcon.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        Location location = getWhere();
        if (location == null || !location.getLatLong().isPresent()) {
          return;
        }
        
        try {
          openMapUrl.openLatLong(location.getLatLong().get());
          e.consume();
        } catch (IOException e1) {
        }
      }
    });
    fireWhereTextChanged();
  }

  private void updateIdToString(@Nullable String nullName) {
    whereIndexers = new WherePanelIndexers(
        reportSet.getLocations(), predefinedLocations, /*addNullName=*/true, nullName, Predicates.alwaysTrue(),
        /* syntheticLocations= */ null);

    whereIndexers.configureIndexer(whereIndexerPanel);
  }

  public DirtyImpl getDirty() {
    return dirty;
  }

  public void setWhere(Location location) {
    Preconditions.checkArgument(location == null || location.getId() != null);
    setWhereImpl(location);
    whereIndexerPanel.setValue(location == null ? null : location.getId());
  }

  /**
   * Returns the location.  If this is a new location, it may not have an ID
   * and must be explicitly added to a LocationSet.
   */
  public Location getWhere() {
    return where;
  }
  
  protected void setWhereImpl(Location location) {
    if (this.where != location) {
      Location oldWhere = where;
      String name = location == null ? "" : location.getDisplayName();
      // Don't try to force the name back to the empty string, as a user
      // may be typing
      if (!whereIndexerPanel.getTextValue().equals(name) && location != null && location.getId() != null) {
        whereIndexerPanel.setValue(location.getId());
      }
      where = location;
      
      latLongIcon.setVisible(location != null && location.getLatLong().isPresent());

      firePropertyChange("where", oldWhere, where);
      dirty.setDirty(true);
    }
  }

  public void shown() {
    whereIndexerPanel.requestFocusInWindow();
  }

  private void initGUI() {
    GroupLayout layout = new GroupLayout(this);
    this.setLayout(layout);
    setPreferredSize(new Dimension(400, 300));

    whereIndexerPanel = new IndexerPanel<Object>();
    whereIndexerPanel.setPreviewText(Name.START_TYPING_A_LOCATION);
    whereLabel = new JLabel();
    whereLabel.setText(Messages.getMessage(Name.WHERE_QUESTION));

    newLocation = new JButton();
    newLocation.setText(Messages.getMessage(Name.CHOOSE_NEW_LOCATION));
    
    latLongIcon = new JLabel(
        new ImageIcon(latLongOriginalSize.getImage()
            .getScaledInstance(18, 18,  Image.SCALE_SMOOTH)));
    latLongIcon.setVisible(false);
    latLongIcon.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

    layout.setHorizontalGroup(layout.createParallelGroup()
        .addComponent(whereLabel)
        .addGroup(layout.createSequentialGroup()
            .addComponent(whereIndexerPanel)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(latLongIcon, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
            .addGap(20)
            .addComponent(newLocation)));

    layout.setVerticalGroup(layout.createSequentialGroup()
        .addComponent(whereLabel,PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
        .addGroup(
            layout.createBaselineGroup(false, false)
              .addComponent(whereIndexerPanel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
              .addComponent(latLongIcon, GroupLayout.Alignment.CENTER, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
        .addComponent(newLocation, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE));
  }

  public void setEditable(boolean editable) {
    whereIndexerPanel.setEditable(editable);
    newLocation.setVisible(editable);
  }

  public void setLayoutStrategy(LayoutStrategy layout) {
    whereIndexerPanel.setLayoutStrategy(layout);
  }
  
  private void fireWhereTextChanged() {
    if (whereIndexerPanel.getValue() == null) {
      newLocation.setEnabled(!whereIndexerPanel.getTextValue().isEmpty());
    }
  }
}
