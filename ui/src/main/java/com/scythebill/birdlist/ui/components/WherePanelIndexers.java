/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import static com.scythebill.birdlist.ui.util.LocationIdToString.ALTERNATE_INDEX_ENTRIES;

import java.util.Collection;
import java.util.Objects;

import javax.annotation.Nullable;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.scythebill.birdlist.model.query.SyntheticLocation;
import com.scythebill.birdlist.model.query.SyntheticLocations;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Location.Type;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;
import com.scythebill.birdlist.model.util.Indexer;
import com.scythebill.birdlist.model.util.ToString;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.LocationIdToString;

/**
 * Code associated with Location indexers.
 */
public class WherePanelIndexers implements ToString<Object> {
  private final LocationSet locations;
  private final PredefinedLocations predefinedLocations;
  private String nullName;
  private Indexer<Object> primaryIndexer;
  private Indexer<Object> secondaryIndexer;
  private SyntheticLocations syntheticLocations;

  public WherePanelIndexers(
      LocationSet locations,
      @Nullable PredefinedLocations predefinedLocations,
      boolean addNullName,
      @Nullable String nullName,
      Predicate<Location> locationFilter,
      @Nullable SyntheticLocations syntheticLocations) {
    this.locations = locations;
    this.predefinedLocations = predefinedLocations;
    this.syntheticLocations = syntheticLocations;
    this.nullName = MoreObjects.firstNonNull(nullName, Messages.getMessage(Name.WORLD_TEXT));;
    this.primaryIndexer = createIndexer(addNullName, locationFilter);
    this.secondaryIndexer = createSecondaryIndexer(locationFilter);
  }
  
  public void reindex(boolean addNullName, Predicate<Location> locationFilter) {
    this.primaryIndexer = createIndexer(addNullName, locationFilter);
    this.secondaryIndexer = createSecondaryIndexer(locationFilter);
  }

  public void configureIndexer(IndexerPanel<Object> indexerPanel) {
    indexerPanel.removeAllIndexerGroups();
    indexerPanel.addIndexerGroup(this, primaryIndexer);
    indexerPanel.addIndexerGroup(this, secondaryIndexer);
  }
  
  private Indexer<Object> createIndexer(boolean addNullName, Predicate<Location> locationFilter) {
    Indexer<Object> index = new Indexer<Object>();
    index.setAlternateIndexEntries(ALTERNATE_INDEX_ENTRIES);
    
    for (Location loc : locations.rootLocations()) {
      addToLocationIndex(index, loc, locationFilter);
    }

    if (syntheticLocations != null) {
      for (SyntheticLocation location : syntheticLocations.locations()) {
        if (locationFilter.apply(location)) {
          index.add(location.getDisplayName(), location);
        }
      }      
    }
    
    if (addNullName) {
      index.add(nullName, null);
    }

    return index;
  }

  private Indexer<Object> createSecondaryIndexer(Predicate<Location> locationFilter) {
    // Use a longer number of names for the secondary indexer, since we append the parent name, and
    // can run into limits.
    Indexer<Object> index = new Indexer<Object>(8);
    index.setAlternateIndexEntries(ALTERNATE_INDEX_ENTRIES);
    for (Location loc : locations.rootLocations()) {
      addSecondaryToLocationIndex(index, loc, locationFilter);
    }
    
    return index;
  }


  @Override public String getString(Object obj) {
    if (obj instanceof IndexedPredefinedLocation indexed) {
      return nameOfPredefinedLocation(indexed);
    } else if (obj instanceof SyntheticLocation synthetic) {
      return synthetic.getDisplayName();
    } else {
      String id = (String) obj;
      String syntheticName = getSyntheticName(id);
      if (syntheticName != null) {
        return syntheticName;
      }
      
      Location loc = locations.getLocation(id);
      return LocationIdToString.getString(locations, id, true, loc);
    }
  }

  private String nameOfPredefinedLocation(IndexedPredefinedLocation indexed) {
    if (indexed.predefinedLocation.getType() == Type.county) {
      return String.format(
          "%s (%s)", indexed.predefinedLocation.getName(), Messages.getMessage(Name.LOCATION_TYPE_COUNTY));
    }
    return indexed.predefinedLocation.getName();
  }
  

  @Override public String getPreviewString(Object obj) {
    if (obj instanceof IndexedPredefinedLocation indexed) {
      return String.format("%s, %s",
          nameOfPredefinedLocation(indexed),
          LocationIdToString.getString(
              locations, indexed.parent.getId(), true, null));
    } else if (obj instanceof SyntheticLocation synthetic) {
      return synthetic.getDisplayName();
    } else {
      String id = (String) obj;
      String syntheticName = getSyntheticName(id);
      if (syntheticName != null) {
        return syntheticName;
      }
      
      return LocationIdToString.getString(locations, id, true, null);
    }
  }

  /** Return the name of the location if synthetic, null otherwise. */
  @Nullable
  private String getSyntheticName(String id) {
    if (id == null) {
      return nullName;
    }
    
    return null;
  }

  private void addToLocationIndex(Indexer<Object> index, Location loc, Predicate<Location> locationFilter) {
    if (locationFilter.apply(loc)) {
      index.add(loc.getDisplayName(), loc.getId());
    }

    for (Location child : loc.contents()) {
      addToLocationIndex(index, child, locationFilter);
    }    
  }
  
  private void addSecondaryToLocationIndex(Indexer<Object> index, Location loc, Predicate<Location> locationFilter) {
    if (locationFilter.apply(loc)) {
      if (!loc.getModelName().equals(loc.getDisplayName())) {
        index.add(loc.getModelName(), loc.getId());
        if (loc.getParent() != null && loc.getType() != Location.Type.country) {
          index.add(loc.getModelName() + " " + loc.getParent().getModelName(), loc.getId());
        }
      }
      if (loc.getParent() != null && loc.getType() != Location.Type.country) {
        index.add(loc.getDisplayName() + " " + loc.getParent().getDisplayName(), loc.getId());
      }
    }
    
    for (Location child : loc.contents()) {
      addSecondaryToLocationIndex(index, child, locationFilter);
    }    
    
    // Add all predefined locations that aren't already present
    if (predefinedLocations != null) {
      for (PredefinedLocation predefinedLocation : predefinedLocations.getPredefinedLocations(loc)) {
        if (locations.getLocationByCode(predefinedLocation.getCode()) == null) {
          if (locationFilter.apply(predefinedLocation.create(locations, loc))) {
            IndexedPredefinedLocation indexed =
                new IndexedPredefinedLocation(loc, predefinedLocation);
            String name = predefinedLocation.getName();
            index.add(name, indexed);
            index.add(name + " " + loc.getDisplayName(), indexed);
          }
        }
      }
    }
  }
  
  static class IndexedPredefinedLocation {
    final Location parent;
    final PredefinedLocation predefinedLocation;

    public IndexedPredefinedLocation(
        Location parent, PredefinedLocation predefinedLocation) {
      this.parent = parent;
      this.predefinedLocation = predefinedLocation;
    }
    
    @Override
    public int hashCode() {
      return Objects.hash(parent, predefinedLocation);
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      
      if (!(o instanceof IndexedPredefinedLocation)) {
        return false;
      }
      IndexedPredefinedLocation that = (IndexedPredefinedLocation) o;
      return Objects.equals(parent, that.parent)
          && Objects.equals(predefinedLocation, that.predefinedLocation);
    }



    @Override
    public String toString() {
      return MoreObjects.toStringHelper(this)
          .omitNullValues()
          .add("parent", parent)
          .add("predefinedLocation", predefinedLocation)
          .toString();
    }
  }

  /** Remove a location from the indexers. */
  public void remove(Location oldLocation) {
    // Not supported for not-yet-added locations
    Preconditions.checkState(oldLocation.getId() != null);
    primaryIndexer.remove(oldLocation.getDisplayName(), oldLocation.getId());
    if (oldLocation.getParent() != null && oldLocation.getType() != Location.Type.country) {
      secondaryIndexer.remove(oldLocation.getDisplayName() + " " + oldLocation.getParent().getDisplayName(), oldLocation.getId());
    }
    
    if (!oldLocation.getModelName().equals(oldLocation.getDisplayName())) {
      secondaryIndexer.remove(oldLocation.getModelName(), oldLocation.getId());
      if (oldLocation.getParent() != null && oldLocation.getType() != Location.Type.country) {
        secondaryIndexer.remove(oldLocation.getModelName() + " " + oldLocation.getParent().getModelName(), oldLocation.getId());
      }
    }
  }

  /** Add a location to the indexers. */
  public void add(Location newLocation) {
    primaryIndexer.add(newLocation.getDisplayName(), newLocation.getId());
    if (newLocation.getParent() != null && newLocation.getType() != Location.Type.country) {
      secondaryIndexer.add(newLocation.getDisplayName() + " " + newLocation.getParent().getDisplayName(), newLocation.getId());
    }

    if (!newLocation.getModelName().equals(newLocation.getDisplayName())) {
      secondaryIndexer.add(newLocation.getModelName(), newLocation.getId());
      if (newLocation.getParent() != null && newLocation.getType() != Location.Type.country) {
        secondaryIndexer.add(newLocation.getModelName() + " " + newLocation.getParent().getModelName(), newLocation.getId());
      }
    }
  }

  /** Convert an object back to a Location. */
  public static Location toLocation(LocationSet locations, Object obj) {
    if (obj == null) {
      return null;
    }
    
    if (obj instanceof IndexedPredefinedLocation) {
      // A PredefinedLocation *might* have been added by now.
      IndexedPredefinedLocation indexed = (IndexedPredefinedLocation) obj;
      Location maybeAddedByNow = locations.getLocationByCode(indexed.predefinedLocation.getCode());
      if (maybeAddedByNow != null) {
        return maybeAddedByNow;
      }
      return indexed.predefinedLocation.create(locations, indexed.parent);
    } else if (obj instanceof SyntheticLocation synthetic) {
      return synthetic;
    } else {
      String id = (String) obj;
      return locations.getLocation(id);
    }
  }
  
  public Object toIndexerObject(Location location) {
    // If it has an ID, done.
    if (location.getId() != null) {
      return location.getId();
    }
    
    // Otherwise, see if it's a predefined location
    Location parent = location.getParent();
    if (parent == null) {
      return null;
    }
    
    PredefinedLocation predefinedLocation =
        predefinedLocations.getPredefinedLocationChildThatHasNotBeenCreatedYet(locations, parent, location.getModelName());
    if (predefinedLocation == null) {
      return null;
    }
    
    return new IndexedPredefinedLocation(parent, predefinedLocation);
  }
  
  /**
   * Returns matches for a given name.  Matches are opaque objects, and must be turned into
   * locations or strings with {@link #toLocation(LocationSet, Object)} and {@link #getString(Object)}. 
   */
  public Collection<Object> getMatches(String name) {
    return primaryIndexer.find(name);
  }

  /**
   * Converts a location ID into an Object as used by the indexer. 
   */
  public Object toIndexerObject(String locationId) {
    // Return the ID as is;  but at least preserving the abstraction of this class.
    return locationId;
  }
}
