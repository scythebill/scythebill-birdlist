/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components;

import java.text.DecimalFormat;
import java.text.ParseException;

import javax.swing.text.NumberFormatter;

import com.google.common.base.Strings;

/**
 * Formatter that handles year two- and four-digit years, as well
 * as unspecified years.
 */
public final class YearFormatter extends NumberFormatter {
  public YearFormatter() {
    super(new DecimalFormat("00"));
    setValueClass(Integer.class);
    setMinimum(0);
    setMaximum(2100);
  }

  @Override
  public Object stringToValue(String text) throws ParseException {
    if (Strings.isNullOrEmpty(text)) {
      return null;
    }

    Integer value = (Integer) super.stringToValue(text);
    if (value != null) {
      if (value >= 0 && value < 100) {
        // Assume all values from 0-49 are 2000-2049, and
        // 50-99 are 1950-1999.
        if (value < 50) {
          value += 2000;
        } else {
          value += 1900;
        }
      }
    }

    return value;
  }
}