/*
 * @(#)BasicBrowserUI.java  
 *
 * Copyright (c) 2005-2010 Werner Randelshofer
 * Hausmatt 10, Immensee, CH-6405, Switzerland.
 * All rights reserved.
 *
 * The copyright of this software is owned by Werner Randelshofer. 
 * You may not use, copy or modify this software, except in  
 * accordance with the license agreement you entered into with  
 * Werner Randelshofer. For details see accompanying license terms. 
 */
package com.scythebill.birdlist.ui.components.browser;

import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.plaf.UIResource;

/**
 * NOTE: this is a forked version of Quaqua code.  The original copyright must
 * be maintained. 
 */
class BasicBrowserUI extends BrowserUI {

    protected JBrowser browser;
    public Icon sizeHandleIcon;

    /**
     * Creates a new instance.
     */
    public BasicBrowserUI() {
    }

    //
    // Install methods
    //
    @Override
    public void installUI(JComponent c) {
        c.setBackground(UIManager.getColor("List.background"));
        // Try to load the icon from Quaqua, if it's on the classpath
        sizeHandleIcon = UIManager.getIcon("Browser.sizeHandleIcon");
        if (sizeHandleIcon == null) {
          URL quaquaIcon = getClass().getClassLoader()
              .getResource("ch/randelshofer/quaqua/images/Browser.sizeHandleIcon.png");
          if (quaquaIcon != null) {
            sizeHandleIcon = new ImageIcon(quaquaIcon);
          }
        }

        browser = (JBrowser) c;
        installDefaults();
    }

    protected void installDefaults() {
        if (browser.getColumnCellRenderer() == null ||
                (browser.getColumnCellRenderer() instanceof UIResource)) {
            browser.setColumnCellRenderer(createCellRenderer());
        }
    }
    //
    // Uninstall methods
    //

    @Override
    public void uninstallUI(JComponent c) {
	uninstallDefaults();
    }
    protected void uninstallDefaults() {
	if (browser.getTransferHandler() instanceof UIResource) {
	    browser.setTransferHandler(null);
	}
    }
    
    @Override
    public Icon getSizeHandleIcon() {
        return sizeHandleIcon;
    }

    @SuppressWarnings("rawtypes")
    protected ListCellRenderer createCellRenderer() {
        return new DefaultColumnCellRenderer.UIResource(browser);
    }
}
