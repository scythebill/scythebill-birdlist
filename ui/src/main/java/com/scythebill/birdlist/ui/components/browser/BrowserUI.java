/*
 * @(#)BrowserUI.java  
 *
 * Copyright (c) 2005-2010 Werner Randelshofer
 * Hausmatt 10, Immensee, CH-6405, Switzerland.
 * All rights reserved.
 *
 * The copyright of this software is owned by Werner Randelshofer. 
 * You may not use, copy or modify this software, except in  
 * accordance with the license agreement you entered into with  
 * Werner Randelshofer. For details see accompanying license terms. 
 */

package com.scythebill.birdlist.ui.components.browser;

import javax.swing.*;
import javax.swing.plaf.*;

/**
 * NOTE: this is a forked version of Quaqua code.  The original copyright must
 * be maintained. 
 * 
 * BrowserUI.
 */
class BrowserUI extends ComponentUI {
    
    /**
     * Creates a new instance.
     */
    public BrowserUI() {
    }
    
    public Icon getSizeHandleIcon() {
        return null;
    }

}
