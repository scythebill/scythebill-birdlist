/*
 * @(#)DefaultColumnCellRenderer.java 
 *
 * Copyright (c) 2003-2010 Werner Randelshofer
 * Hausmatt 10, Immensee, CH-6405, Switzerland.
 * http://www.randelshofer.ch
 * All rights reserved.
 *
 * The copyright of this software is owned by Werner Randelshofer. 
 * You may not use, copy or modify this software, except in  
 * accordance with the license agreement you entered into with  
 * Werner Randelshofer. For details see accompanying license terms. 
 */
package com.scythebill.birdlist.ui.components.browser;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Insets;
import java.awt.Rectangle;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

/**
 * NOTE: this is a forked version of Quaqua code.  The original copyright must
 * be maintained. 
 * 
 * DefaultColumnCellRenderer.
 */
@SuppressWarnings("rawtypes")
public class DefaultColumnCellRenderer implements ListCellRenderer {

    private JPanel panel;
    private JLabel textLabel;
    private JLabel arrowLabel;
    private EmptyBorder noFocusBorder;
    private JBrowser browser;
    protected Icon expandedIcon = null;
    protected Icon selectedExpandedIcon = null;
    protected Icon focusedSelectedExpandedIcon = null;
    protected Icon expandingIcon = null;

    public DefaultColumnCellRenderer(JBrowser browser) {
        this.browser = browser;

        expandedIcon = UIManager.getIcon("Browser.expandedIcon");
        selectedExpandedIcon = UIManager.getIcon("Browser.selectedExpandedIcon");
        focusedSelectedExpandedIcon = UIManager.getIcon("Browser.focusedSelectedExpandedIcon");

        if (expandedIcon == null) {
//            BufferedImage iconImages[] = Images.split(
//                    Toolkit.getDefaultToolkit().createImage(
//                    DefaultColumnCellRenderer.class.getResource("snow_leopard/images/Browser.disclosureIcons.png")),
//                    6, true);
//
//            expandedIcon = new ImageIcon(iconImages[0]);
//            selectedExpandedIcon = new ImageIcon(iconImages[4]);
//            focusedSelectedExpandedIcon = new ImageIcon(iconImages[2]);
        }

        noFocusBorder = new EmptyBorder(1, 1, 1, 1);
        panel = new JPanel(new BorderLayout()) {
            // Overridden for performance reasons.
            //public void validate() {}

            @Override
            public void revalidate() {
            }

            @Override
            public void repaint(long tm, int x, int y, int width, int height) {
            }

            @Override
            public void repaint(Rectangle r) {
            }

            @Override
            protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
            }

            @Override
            public void firePropertyChange(String propertyName, short oldValue, short newValue) {
            }

            @Override
            public void firePropertyChange(String propertyName, int oldValue, int newValue) {
            }

            @Override
            public void firePropertyChange(String propertyName, long oldValue, long newValue) {
            }

            @Override
            public void firePropertyChange(String propertyName, float oldValue, float newValue) {
            }

            @Override
            public void firePropertyChange(String propertyName, double oldValue, double newValue) {
            }

            @Override
            public void firePropertyChange(String propertyName, boolean oldValue, boolean newValue) {
            }
        };

        textLabel = new JLabel() {
            // Overridden for performance reasons.

            @Override
            public void validate() {
            }

            @Override
            public void revalidate() {
            }

            @Override
            public void repaint(long tm, int x, int y, int width, int height) {
            }

            @Override
            public void repaint(Rectangle r) {
            }

            @Override
            protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
                // Strings get interned...
                if ("text".equals(propertyName)) {
                    super.firePropertyChange(propertyName, oldValue, newValue);
                }
            }

            @Override
            public void firePropertyChange(String propertyName, short oldValue, short newValue) {
            }

            @Override
            public void firePropertyChange(String propertyName, int oldValue, int newValue) {
            }

            @Override
            public void firePropertyChange(String propertyName, long oldValue, long newValue) {
            }

            @Override
            public void firePropertyChange(String propertyName, float oldValue, float newValue) {
            }

            @Override
            public void firePropertyChange(String propertyName, double oldValue, double newValue) {
            }

            @Override
            public void firePropertyChange(String propertyName, boolean oldValue, boolean newValue) {
            }
        };
        arrowLabel = new JLabel() {
            // Overridden for performance reasons.

            @Override
            public void validate() {
            }

            @Override
            public void revalidate() {
            }

            @Override
            public void repaint(long tm, int x, int y, int width, int height) {
            }

            @Override
            public void repaint(Rectangle r) {
            }

            @Override
            protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
                if ("text".equals(propertyName)) {
                    super.firePropertyChange(propertyName, oldValue, newValue);
                }
            }

            @Override
            public void firePropertyChange(String propertyName, short oldValue, short newValue) {
            }

            @Override
            public void firePropertyChange(String propertyName, int oldValue, int newValue) {
            }

            @Override
            public void firePropertyChange(String propertyName, long oldValue, long newValue) {
            }

            @Override
            public void firePropertyChange(String propertyName, float oldValue, float newValue) {
            }

            @Override
            public void firePropertyChange(String propertyName, double oldValue, double newValue) {
            }

            @Override
            public void firePropertyChange(String propertyName, boolean oldValue, boolean newValue) {
            }
        };

        panel.setOpaque(true);
        panel.setBorder(noFocusBorder);

        textLabel.putClientProperty("Quaqua.Component.visualMargin", new Insets(0, 0, 0, 0));
        arrowLabel.putClientProperty("Quaqua.Component.visualMargin", new Insets(0, 0, 0, 0));

        panel.add(textLabel, BorderLayout.CENTER);
        arrowLabel.setIcon(expandedIcon);
        panel.add(arrowLabel, BorderLayout.EAST);
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value,
            int index, boolean isSelected,
            boolean cellHasFocus) {
        //setComponentOrientation(list.getComponentOrientation());
        // TODO(awiner): original code checked a variety of conditions
        boolean isFocused = list.isFocusOwner();

        if (isSelected) {
            panel.setBackground(list.getSelectionBackground());
            Color foreground = (!isFocused && UIManager.getColor("List.inactiveSelectionForeground") != null) ? UIManager.getColor("List.inactiveSelectionForeground") : list.getSelectionForeground();
            textLabel.setForeground(foreground);
            arrowLabel.setForeground(foreground);
            arrowLabel.setIcon(isFocused ? focusedSelectedExpandedIcon : selectedExpandedIcon);
        } else {
            panel.setBackground(list.getBackground());
            Color foreground = list.getForeground();
            textLabel.setForeground(foreground);
            arrowLabel.setForeground(foreground);
            arrowLabel.setIcon(expandedIcon);
        }

        textLabel.setText((value == null) ? "null" : value.toString());
        //textLabel.setIcon(getFileChooser().getIcon(file));

        arrowLabel.setVisible(!browser.getModel().isLeaf(value));


        textLabel.setEnabled(list.isEnabled());
        textLabel.setFont(list.getFont());
        panel.setBorder((cellHasFocus) ? UIManager.getBorder("List.focusCellHighlightBorder") : noFocusBorder);

        return panel;
    }

    public static class UIResource extends DefaultColumnCellRenderer implements javax.swing.plaf.UIResource {

        public UIResource(JBrowser browser) {
            super(browser);
        }
    }
}

