/*   
 * Copyright 2023 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components.photos;

import java.awt.Component;
import java.awt.Desktop;
import java.awt.Desktop.Action;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import com.scythebill.birdlist.model.sighting.Photo;
import com.scythebill.birdlist.ui.components.photos.PhotosListPanel.PhotoClickedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.DesktopUtils;
import com.scythebill.birdlist.ui.util.FileDialogs;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Default implementation of a PhotoClickedListener.
 */
public abstract class DefaultPhotoClickedListener implements PhotoClickedListener {

  private static final Logger logger =
      Logger.getLogger(DefaultPhotoClickedListener.class.getName());
  private final Alerts alerts;
  private final Component parentObject;
  private final FileDialogs fileDialogs;

  public DefaultPhotoClickedListener(Component parentObject, FileDialogs fileDialogs,
      Alerts alerts) {
    this.parentObject = parentObject;
    this.fileDialogs = fileDialogs;
    this.alerts = alerts;
  }

  @Override
  public void photoClicked(Photo photo) {
    if (photo.isFileBased()) {
      if (Desktop.isDesktopSupported() && (Desktop.getDesktop().isSupported(Action.EDIT)
          || Desktop.getDesktop().isSupported(Action.OPEN))) {
        try {
          openFile(photo);
        } catch (IOException e) {
          logger.log(Level.WARNING, "Could not open " + photo.toFile(), e);
        }
      }
    } else {
      openUrl(photo);
    }
  }

  @Override
  public boolean isSupported(Photo photo) {
    if (photo.isFileBased()) {
      return isFileSupported();
    } else {
      return isUrlSupported();
    }
  }

  private boolean isFileSupported() {
    if (!Desktop.isDesktopSupported()) {
      return false;
    }

    return Desktop.getDesktop().isSupported(Action.EDIT)
        || Desktop.getDesktop().isSupported(Action.OPEN);
  }

  private boolean isUrlSupported() {
    return Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Action.BROWSE);
  }

  private void openFile(Photo photo) throws IOException {
    if (!isFileSupported()) { 
      return;
    }

    try {
      Desktop.getDesktop().open(photo.toFile().getAbsoluteFile());
    } catch (IllegalArgumentException e) {
      tryToFixFile(photo);
    }
  }

  private void tryToFixFile(Photo photo) {
    if (alerts.showOkCancel(parentObject, Name.FILE_NOT_FOUND, Name.PHOTO_NOT_THERE_ANYMORE,
        photo.getName()) != JOptionPane.OK_OPTION) {
      return;
    }

    File file = fileDialogs.openFile(UIUtils.findFrame(parentObject),
        Messages.getFormattedMessage(Name.LOOKING_FOR_PHOTO_FORMAT, photo.getName()),
        new FileFilter() {
          @Override
          public String getDescription() {
            return "Image file";
          }

          @Override
          public boolean accept(File f) {
            return true;
          }
        }, FileType.OTHER);

    if (file == null) {
      return;
    }

    Photo newPhoto = new Photo(file);
    replacePhoto(photo, newPhoto);

  }

  protected abstract void replacePhoto(Photo oldPhoto, Photo newPhoto);

  private void openUrl(Photo photo) {
    if (!isUrlSupported()) {
      return;
    }

    try {
      DesktopUtils.openUrlInBrowser(photo.getUri(), alerts);
    } catch (IOException e) {
      logger.log(Level.WARNING, "Could not browse " + photo.getUri(), e);
    }
  }
}
