/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components.photos;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.dnd.InvalidDnDOperationException;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JComponent;
import javax.swing.TransferHandler;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.io.CharStreams;
import com.scythebill.birdlist.model.sighting.Photo;

/**
 * Abstract base class for components receiving photo drag-and-drop.
 */
public abstract class PhotoDrops {
  private final static DataFlavor URL_FLAVOR = newDataFlavor("application/x-java-url; class=java.net.URL");
  private final static DataFlavor URI_LIST_FLAVOR = newDataFlavor("text/uri-list; class=java.lang.String");
  private final static Logger logger = Logger.getLogger(PhotoDrops.class.getName());

  public PhotoDrops() {}
  
  public void activate(JComponent component) {
    Listener listener = new Listener();
    DropTarget dropTarget = new DropTarget(
        component,
        TransferHandler.COPY,
        listener,
        true);
    component.setDropTarget(dropTarget);
    dropTarget.setActive(true);
  }
  
  protected abstract void setDragOverBorder();
  protected abstract void setDefaultBorder();
  protected abstract void addPhotos(List<Photo> photos);
  
  private class Listener implements DropTargetListener {
    @Override
    public void dragEnter(DropTargetDragEvent dtde) {
      if (getSupportedDataFlavor(dtde.getCurrentDataFlavors()) != null) {
        if ((dtde.getDropAction() & DnDConstants.ACTION_MOVE) != 0) {
          dtde.acceptDrag(DnDConstants.ACTION_MOVE);
          setDragOverBorder();
        }
      } else {
        dtde.rejectDrag();
      }
    }

    private DataFlavor getSupportedDataFlavor(DataFlavor[] currentDataFlavors) {
      DataFlavor urlFlavor = null;
      
      for (DataFlavor flavor : currentDataFlavors) {
        if (flavor.isFlavorJavaFileListType()) {
          // Prefer file flavor if at all possible
          return flavor;
        }
        
        // Second best
        if (flavor.equals(URI_LIST_FLAVOR)) {
          urlFlavor = flavor;;
        }
        
        if (urlFlavor == null && flavor.equals(URL_FLAVOR)) {
          urlFlavor = flavor;
        }        
      }
      
      return urlFlavor;
    }

    @Override
    public void dragExit(DropTargetEvent dte) {
      setDefaultBorder();
    }

    @Override
    public void dragOver(DropTargetDragEvent dtde) {
    }

    @Override
    public void drop(DropTargetDropEvent dtde) {
      setDefaultBorder();
      
      DataFlavor supportedDataFlavor = getSupportedDataFlavor(dtde.getCurrentDataFlavors());
      if (supportedDataFlavor == null) {
        dtde.rejectDrop();
        return;
      }

      dtde.acceptDrop(DnDConstants.ACTION_COPY);
      Object data;
      try {
        data = dtde.getTransferable().getTransferData(supportedDataFlavor);
      } catch (UnsupportedFlavorException | IOException e) {
        throw new RuntimeException(e);
      } catch (InvalidDnDOperationException e) {
        // Workaround for a Java 10? bug where URLs aren't being retrieved - there's
        // an InvalidDnDOperationException, which appears to be triggered by a MalformedURLException
        // itself wrapping a NullPointerException?
        data = null;
      }
      
      // Workaround for https://bugs.openjdk.java.net/browse/JDK-8156099, where URL lists return null.
      // Grab the URL as text, then convert into a URL
      if (data == null) {
        if (supportedDataFlavor.equals(URL_FLAVOR) || supportedDataFlavor.equals(URI_LIST_FLAVOR)) {
          DataFlavor flavor = DataFlavor.getTextPlainUnicodeFlavor();
          if (dtde.getTransferable().isDataFlavorSupported(flavor)) {
            try {
              String text = CharStreams.toString(flavor.getReaderForText(dtde.getTransferable()));
              data = new URL(text);
            } catch (IOException | UnsupportedFlavorException e) {
              throw new RuntimeException(e);
            }
          }          
        }
        
        // Still failed!
        if (data == null) {
          throw new IllegalStateException("Could not get data of type " + supportedDataFlavor.getMimeType());
        }
      } else if (supportedDataFlavor.isFlavorJavaFileListType()) {
        @SuppressWarnings("unchecked")
        List<File> files = (List<File>) data;
        addPhotos(Lists.transform(files, Photo::new));
      } else if (supportedDataFlavor.equals(URI_LIST_FLAVOR)) {
        // Format defined at:
        // https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API/Recommended_drag_types#link
        List<String> uris =
            Splitter.on(CharMatcher.anyOf("\r\n")).omitEmptyStrings().splitToList(((String) data));
        List<Photo> photos = new ArrayList<>();
        for (String uri : uris) {
          // Skip comments: 
          if (uri.startsWith("#")) {
            continue;
          }
          
          try {
            photos.add(new Photo(new URI(uri))); 
          } catch (URISyntaxException e) {
            logger.log(Level.WARNING, String.format("Could not parse %s", uri), e);
          }
          
          if (!photos.isEmpty()) {
            addPhotos(photos);
          }
        }
      } else if (supportedDataFlavor.equals(URL_FLAVOR)) {
        URL url = (URL) data;
        try {
          addPhotos(ImmutableList.of(new Photo(url.toURI())));
        } catch (URISyntaxException e) {
          logger.log(Level.WARNING, String.format("Could not parse %s", url), e);
        }
      }
    }

    @Override
    public void dropActionChanged(DropTargetDragEvent dtde) {
    }
  }

  private static DataFlavor newDataFlavor(String mimeType) {
    try {
      return new DataFlavor(mimeType);
    } catch (ClassNotFoundException e) {
      throw new RuntimeException(e);
    }
  }  
}

