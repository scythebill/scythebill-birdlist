/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components.photos;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.Component;
import java.io.File;
import java.net.URI;
import java.util.Collections;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.io.Resources;
import com.scythebill.birdlist.model.sighting.Photo;
import com.scythebill.birdlist.model.util.DirtyImpl;
import com.scythebill.birdlist.ui.components.ChipsTextPanel;
import com.scythebill.birdlist.ui.components.ChipsTextPanel.ChipToggledListener;
import com.scythebill.birdlist.ui.components.TextLink;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.fonts.FontPreferences;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.FilePreferences;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.util.FileDialogs;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Supports adding and removing photos.
 */
public class PhotosListPanel extends JPanel implements FontsUpdatedListener {
  public static void main(String[] args) {
    final JFrame frame = new JFrame();
    FontManager fontManager = new FontManager(new FontPreferences());
    FileDialogs fileDialogs = new FileDialogs(new FilePreferences(), new FileDialogs.Flags() {
      @Override public boolean useNativeSaveDialog() { return true; }
      @Override public boolean useNativeOpenDialog() { return true; }
    }, null);
    
    PhotosListPanel photosListPanel = new PhotosListPanel(fontManager, fileDialogs, null);
    photosListPanel.setPhotos(ImmutableList.of(
        new Photo(new File("/Users/awiner/Desktop/SokokeScopsOwl.jpg")),
        new Photo(URI.create("http://ibc.lynxeds.com/files/imagecache/photo_940/pictures/DSC_8592__675x900_Sokoke_Scops-owl_z_podpisem.jpg"))));
    photosListPanel.getDirty()
        .addDirtyListener(e -> SwingUtilities.invokeLater(() -> frame.pack()));
    fontManager.applyTo(photosListPanel);
    frame.setContentPane(photosListPanel);
    frame.setVisible(true);
    frame.pack();
    frame.setSize(250, 500);
  }
  
  private final DirtyImpl dirty;
  private final FontManager fontManager;
  private final FileDialogs fileDialogs;
  private final PhotosDropPanel dropPanel;
  private ChipsTextPanel<Photo> photoChipsPanel;
  private List<Photo> photos = Lists.newArrayList();
  private JScrollPane photoChipsScrollPane;
  private final PhotoClickedListener photoClickedListener;

  public interface PhotoClickedListener {
    void photoClicked(Photo photo);
    boolean isSupported(Photo photo);
  }

  public PhotosListPanel(
      FontManager fontManager, FileDialogs fileDialogs,
      PhotoClickedListener photoClickedListener) {
    this.fontManager = fontManager;
    this.fileDialogs = fileDialogs;
    this.photoClickedListener = photoClickedListener;
    this.dirty = new DirtyImpl(false);
    dropPanel = new PhotosDropPanel();
    dropPanel.addActionListener(e -> openFile());
    
    rebuildUI(photos);
  }
  
  public void setPhotos(List<Photo> photos) {
    this.photos = Lists.newArrayList(photos);
    rebuildUI(photos);
    fontManager.applyTo(this);
    dirty.setDirty(false);
  }
  
  public List<Photo> getPhotos() {
    return Collections.unmodifiableList(photos); 
  }

  private void addPhotos(List<Photo> newPhotos) {
    List<Photo> oldPhotos = ImmutableList.copyOf(photos);
    photos.addAll(newPhotos);
    dirty.setDirty(true);
    firePropertyChange("photos", oldPhotos, getPhotos());
    
    // Rebuild the UI
    rebuildUI(photos);
    fontManager.applyTo(this);
  }

  public DirtyImpl getDirty() {
    return dirty;
  }
  
  @Override
  public void fontsUpdated(FontManager fontManager) {
    GroupLayout layout = new GroupLayout(this);
    if (photoChipsPanel.getChips().isEmpty()) {
      layout.setVerticalGroup(layout.createSequentialGroup()
          .addComponent(dropPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE));
      layout.setHorizontalGroup(layout.createParallelGroup(Alignment.CENTER)
          .addComponent(dropPanel, Alignment.CENTER, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE));
    } else {
      GroupLayout.SequentialGroup verticalGroup = layout.createSequentialGroup();
      verticalGroup.addComponent(photoChipsScrollPane, fontManager.scale(40), fontManager.scale(60), PREFERRED_SIZE);
      verticalGroup.addComponent(dropPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE);
      layout.setVerticalGroup(verticalGroup);
      
      layout.setHorizontalGroup(layout.createParallelGroup(Alignment.CENTER)
          .addComponent(dropPanel, Alignment.CENTER, GroupLayout.PREFERRED_SIZE,
              GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
          .addComponent(photoChipsScrollPane, Alignment.CENTER, GroupLayout.PREFERRED_SIZE,
              GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE));
    }
    
    setLayout(layout);
  }

  class PhotosDropPanel extends TextLink {
    PhotosDropPanel() {
      super(Messages.getMessage(Name.DRAG_PHOTO_OR_CLICK));
      setVerticalTextPosition(SwingConstants.BOTTOM);
      setVerticalAlignment(SwingConstants.BOTTOM);
      new PhotoDrops() {
        @Override
        protected void setDragOverBorder() {
          PhotosDropPanel.this.setFocusedBorder();
        }
        
        @Override
        protected void setDefaultBorder() {
          PhotosDropPanel.this.setDefaultBorder();
        }
        
        @Override
        protected void addPhotos(List<Photo> photos) {
          PhotosListPanel.this.addPhotos(photos);
        }
      }.activate(this);
    }
  }

  private void openFile() {
    File file = fileDialogs.openFile(UIUtils.findFrame(this),
        Messages.getMessage(Name.CHOOSE_A_PHOTO),
        new FileFilter() {
          @Override public String getDescription() {
            return Messages.getMessage(Name.IMAGE_FILE);
          }
          
          @Override public boolean accept(File f) {
            return true;
          }
    }, FileType.OTHER );
    if (file != null) {
      addPhotos(ImmutableList.of(new Photo(file)));
    }
  }

  private TextLink newPhotoTextLink(Photo photo) {
    String name = photo.getName();
    if (name.length() > 30) {
      name = name.substring(0, 29) + '\u2026';
    }
    TextLink label = new TextLink(name);
    if (photo.isFileBased()) {
      label.setToolTipText(photo.toFile().getAbsolutePath());
    }
    if (photoClickedListener != null && photoClickedListener.isSupported(photo)) {
      label.addActionListener(e -> photoClickedListener.photoClicked(photo));
    } else {
      label.setEnabled(false);
    }
    
    return label;
  }

  private void rebuildUI(List<Photo> newPhotos) {
    photoChipsPanel = new ChipsTextPanel<Photo>(Photo::getAbbreviatedName, fontManager) {
      @Override
      protected Component toComponent(Photo photo, String chipText) {
        return newPhotoTextLink(photo);
      }
    };
    photoChipsPanel.setScaledWidth(250);
    Icon favoriteOff = new ImageIcon(
        Resources.getResource("com/scythebill/birdlist/ui/icons/unfilled_star.png"));
    Icon favoriteOn = new ImageIcon(
         Resources.getResource("com/scythebill/birdlist/ui/icons/filled_star.png"));
    photoChipsPanel.addToggleFeature(
        favoriteOff, favoriteOn, Photo::isFavorite,
        Name.SET_AS_FAVORITE_FORMAT, Name.REMOVE_AS_FAVORITE_FORMAT);
    photoChipsPanel.addChipToggledListener(new ChipToggledListener<Photo>() {
      @Override
      public void chipToggled(Photo photo, boolean newValue) {
        photo.setFavorite(newValue);
        dirty.setDirty(true);
      }
    });
    
    photoChipsPanel.addAllChips(newPhotos);
    // Handle photo deletion.
    photoChipsPanel.addChipsChangedListener(() -> {
      List<Photo> oldPhotos = ImmutableList.copyOf(photos);
      photos = Lists.newArrayList(photoChipsPanel.getChips());
      dirty.setDirty(true);
      firePropertyChange("photos", oldPhotos, getPhotos());
      fontManager.applyTo(this);
    });
    photoChipsScrollPane = new JScrollPane(photoChipsPanel,
        JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    
    removeAll();
  }
}
