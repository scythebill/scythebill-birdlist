/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components.table;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.JComponent;
import javax.swing.Timer;

/**
 * Delayed creation of UI. Lets ExpandableTable create a few thousand components
 * up front, and have the actual Swing UI created (a bit) later, so the screen
 * continues to be responsive.
 */
public class DelayedUI {
  private static final int COMPONENTS_TO_CREATE_AT_A_TIME = 100;
  private static final int UI_CREATION_DELAY = 100;
  private static final DelayedUI instance = new DelayedUI();
  private final AtomicBoolean delay = new AtomicBoolean();
  private final AtomicInteger initialAllowance = new AtomicInteger();
  private final BlockingQueue<JComponent> delayedComponents = new LinkedBlockingQueue<JComponent>();
  private Timer timer; 

  public static DelayedUI instance() {
    return instance;
  }
  
  private DelayedUI() {
  }
  
  public void enableDelay() {
    if (!delay.compareAndSet(false, true)) {
      throw new IllegalStateException("Already delayed");
    }
    // Allow 100 components to be created up front, then delay
    initialAllowance.set(COMPONENTS_TO_CREATE_AT_A_TIME);
  }
  
  public void liftDelay() {
    if (!delay.compareAndSet(true, false)) {
      throw new IllegalStateException("Was not delayed");
    }
  }

  public boolean requestUICreation(JComponent component) {
    if (delay.get()) {
      if (initialAllowance.getAndDecrement() > 0) {
        return true;
      }
      delayedComponents.add(component);
      startTimer();
      return false;
    } else {
      return true;
    }
  }

  private void startTimer() {
    if (timer == null) {
      timer = new Timer(UI_CREATION_DELAY, new ActionListener() {
        @Override public void actionPerformed(ActionEvent e) {
          ArrayList<JComponent> componentsToCreate = new ArrayList<JComponent>(100);
          delayedComponents.drainTo(componentsToCreate, COMPONENTS_TO_CREATE_AT_A_TIME);
          if (delayedComponents.isEmpty()) {
            timer.stop();
            timer = null;
          }
          
          for (JComponent component : componentsToCreate) {
            component.updateUI();
          }
        }
      });
      timer.setDelay(0);
      timer.setRepeats(true);
      timer.start();
    }    
  }
  
}
