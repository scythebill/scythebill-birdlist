/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components.table;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import com.scythebill.birdlist.ui.components.table.ExpandableTable.Column;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.ColumnLayout;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.RowState;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Column for deleting entries from an {@link ExpandableTable}.
 */
public abstract class DeleteColumn<T> implements Column<T> {
  private final MouseListener listener;
  private ColumnLayout width;
  private final ExpandableTable<T> table;
  
  /**
   * Returns a simple label with the delete appearance (but no behavior).
   */
  public static JLabel createDeleteLabel() {
    JLabel label = new JLabel();
    label.setIcon(deleteIcon());
    label.setOpaque(false);
    label.setToolTipText(Messages.getMessage(Name.DELETE_ACTION));
    return label;
  }

  public static ImageIcon deleteIcon() {
    return new ImageIcon(DeleteColumn.class.getResource("action_delete.png"));
  }

  public DeleteColumn(ColumnLayout width, ExpandableTable<T> table) {
    this.width = width;
    this.table = table;
    listener = new MouseAdapter() {
      @Override public void mouseClicked(MouseEvent e) {
        Point tablePoint = SwingUtilities.convertPoint(
            e.getComponent(), e.getPoint(), DeleteColumn.this.table);
        int row = DeleteColumn.this.table.getRow(tablePoint.y);
        deleteRow(row);
        e.consume();
      }
    };
  }

  @Override public JComponent createComponent(T value, Set<RowState> states) {
    JLabel label = createDeleteLabel();
    label.addMouseListener(listener);
    return label;
  }

  @Override public ColumnLayout getWidth() {
    return width;
  }

  @Override
  public boolean sizeComponentsToFit() {
    return false;
  }

  @Override public void updateComponentValue(Component component, T value) {
  }
  
  @Override public void updateComponentState(Component component,
      T value,
      Set<RowState> states) {
  }
  
  abstract protected void deleteRow(int index);
}
