/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components.table;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Set;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import com.scythebill.birdlist.ui.components.table.ExpandableTable.Column;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.ColumnLayout;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.RowState;

/**
 * Column providing expand/collapse.
 */
public class ExpandColumn<T> implements Column<T> {
  private final MouseListener listener;
  private ColumnLayout width;
  private final ExpandableTable<T> table;
  private final String title;

  public ExpandColumn(ExpandableTable<T> table, String title) {
    this.width = ColumnLayout.fixedWidth(UIManager.getIcon("Tree.expandedIcon").getIconWidth());
    this.table = table;
    this.title = title;
    listener = new MouseAdapter() {
      @Override public void mouseClicked(MouseEvent e) {
        Point tablePoint = SwingUtilities.convertPoint(
            e.getComponent(), e.getPoint(), ExpandColumn.this.table);
        int row = ExpandColumn.this.table.getRow(tablePoint.y);
        if (ExpandColumn.this.table.getExpandedIndex() == row) {
          row = ExpandableTable.NO_ROW;
        }
        ExpandColumn.this.table.setExpandedIndex(row);
        e.consume();
      }
    };
  }

  @Override public JComponent createComponent(T value, Set<RowState> states) {
    JLabel label = new JLabel();
    label.setOpaque(false);
    label.addMouseListener(listener);
    updateComponentState(label, value, states);
    if (title != null) {
      label.setToolTipText(title);
    }
    return label;
  }

  @Override public ColumnLayout getWidth() {
    return width;
  }

  @Override
  public boolean sizeComponentsToFit() {
    return false;
  }

  @Override public String getName() {
    return "";
  }

  @Override public void updateComponentValue(Component component, T value) {
  }

  @Override public void updateComponentState(Component component,
      T value, Set<RowState> states) {
    Icon icon = UIManager.getIcon(states.contains(RowState.EXPANDED)
        ? "Tree.expandedIcon" : "Tree.collapsedIcon");
    ((JLabel) component).setIcon(icon);
    
    // Hide the expand toggle if the row is not enabled.
    component.setVisible(states.contains(RowState.ENABLED));
  }
}
