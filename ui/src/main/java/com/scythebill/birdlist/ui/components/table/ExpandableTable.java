/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.ContainerOrderFocusTraversalPolicy;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.KeyboardFocusManager;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.FocusManager;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListModel;
import javax.swing.Scrollable;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.util.FocusTracker;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Table component supporting a single expandable row.
 * ExpandableTables do not use stamping.  This makes it simpler to code and provide for
 * better editing UIs;  it also means that it can't support very large data sets, but that's
 * an explicit non-goal of this component.
 * 
 * DONE:
 * - Listen to the table model
 * - Test deletion
 * - Selection
 * - Key events
 * - Column dividers
 * - Better layout model (allow absolute widths)
 * - Implement expansion (off selection?)
 * TODOs:
 * - Tooltip if truncated
 * 
 * @author Adam Winer
 */
public class ExpandableTable<T> extends JComponent implements Scrollable {
  
  /** Constant indicating no row is/should be selected/expanded */
  static public final int NO_ROW = -1;

  private static final int EXPANDED_ROW_INDENT = 35;

  private ListModel<T> model;
  private int lineHeight = 20;
  private int visibleRowCount = 10;
  private boolean componentsCreated = false;
  private boolean showVerticalGridLines = false;
  private final List<Column<T>> columns = Lists.newArrayList();
  private final ListDataListener dataListener = new ModelEvents();
  private final MouseListener mouseListener = new MouseEvents();
  private final KeyListener keyListener = new KeyEvents();
  private int selectedIndex = NO_ROW;
  private int[] columnWidths;
  private int expandedIndex = NO_ROW;
  private DetailView<T> expandedRowView;
  private JComponent expandedRowComponent;
  private final Class<T> type;
  private JTableHeader tableHeader;
  /** True if one of the columns is an ExpandColumn */
  private boolean hasExpandColumn;

  private FontManager fontManager;

  private TableRowsModel enabledRowsModel;

  private boolean highlightEnabledRows;
  
  // Honeypot component to detect when focus has reached the start (and appropriately redirect)
  private final JButton focusStartHoneypot = new JButton("");
  // Honeypot component to detect when focus has reached the end(and appropriately redirect)
  private final JButton focusEndHoneypot = new JButton("");

  private Component focusableComponentAfterTable;

  private Component focusableComponentBeforeTable;

  /** Description of how a column should lay out itself. */
  static public class ColumnLayout {
    /** Returns a ColumnLayout requesting a fixed width. */
    static public ColumnLayout fixedWidth(int pixels) {
      return new ColumnLayout(pixels, 0.0f, true, false);
    }
    
    /** Returns a ColumnLayout requesting a fixed width, scaling based on FontManager. */
    static public ColumnLayout scalableFixedWidth(int pixels) {
      return new ColumnLayout(pixels, 0.0f, true, true);
    }

    /** return a column layout requesting a fractional width. */
    static public ColumnLayout weightedWidth(float weight) {
      return new ColumnLayout(0, weight, false, false);
    }
    
    // TOOD - polymorphism, anyone?
    private final boolean isFixed;
    private final int widthFixed;
    private final float widthWeight;
    private final boolean scaling;

    int fixedWidth(FontManager fontManager) {
      return scaling ? fontManager.scale(widthFixed) : widthFixed;
    }
    
    private ColumnLayout(int widthAbsolute, float widthWeight, boolean isAbsolute, boolean scaling) {
      this.widthFixed = widthAbsolute;
      this.widthWeight = widthWeight;
      this.isFixed = isAbsolute;
      this.scaling = scaling;
    }
  }
  
  public enum RowState {
    ENABLED,
    EXPANDED,
    SELECTED
  }
  
  private static final Set<RowState> STATE_NONE = ImmutableSet.of();
  private static final Set<RowState> STATE_SELECTED = ImmutableSet.of(RowState.SELECTED);
  private static final Set<RowState> STATE_EXPANDED = ImmutableSet.of(RowState.EXPANDED);
  private static final Set<RowState> STATE_SELECTED_AND_EXPANDED = ImmutableSet.of(
      RowState.SELECTED, RowState.EXPANDED);
  private static final Set<RowState> STATE_ENABLED = ImmutableSet.of(RowState.ENABLED);
  private static final Set<RowState> STATE_ENABLED_SELECTED = ImmutableSet.of(
      RowState.ENABLED, RowState.SELECTED);
  private static final Set<RowState> STATE_ENABLED_EXPANDED =
      ImmutableSet.of(RowState.ENABLED, RowState.EXPANDED);
  private static final Set<RowState> STATE_ENABLED_SELECTED_AND_EXPANDED = ImmutableSet.of(
      RowState.ENABLED, RowState.SELECTED, RowState.EXPANDED);
  
  /** A single column in the table. */
  public interface Column<T> {
    JComponent createComponent(T value, Set<RowState> states);
    ColumnLayout getWidth();
    /**
     * If true, components in the column will individually be sized to match the preferred with.
     * If false, components will occupy the entire column width.
     */
    boolean sizeComponentsToFit();
    void updateComponentValue(Component component, T value);
    void updateComponentState(Component component, T value, Set<RowState> states);
    String getName();
  }
  
  /** Interface for the detailed view of a table. */
  public interface DetailView<T> {
    JComponent createComponent(T value);
    void onClose(JComponent component);
    int getHeight();
  }
  
  public ExpandableTable(Class<T> type, FontManager fontManager) {
    this.type = type;
    this.fontManager = fontManager;
    this.enabledRowsModel = new AllRowsTableRowsModel();
    setLayout(new LayoutImpl());
    setBackground(UIManager.getColor("Table.background"));
    setForeground(UIManager.getColor("Table.foreground"));
    addMouseListener(mouseListener);
    addKeyListener(keyListener);
    
    setFocusable(true);
    
    // Force the table to be a focus traversal policy provider;  without this,
    // tab navigation gets outrageously slow when there's lots of rows.
    setFocusCycleRoot(true);
    setFocusTraversalPolicy(new ExpandableTableFocusTraversalPolicy());
    
    new TableFocusTracker();
  }
  
  public TableRowsModel getEnabledRowsModel() {
    return enabledRowsModel;
  }

  /**
   * Attach an "EnabledRowsModel", which controls which rows are enabled.
   * Disabled rows may not be expanded (but can be selected), and other content
   * may become disabled.
   */
  public void setEnabledRowsModel(TableRowsModel rowsModel) {
    enabledRowsModel = rowsModel;
    rowsModel.addListener(new TableRowsModel.Listener() {
      
      @Override public void rowRemoved(int row) {
        // If the row is expanded, collapse it (and let that
        // retrigger the changed state).
        if (getExpandedIndex() == row) {
          setExpandedIndex(NO_ROW);
        } else {
          updateRowState(row);
        }
      }
      
      @Override public void rowIncluded(int row) {
        updateRowState(row);
      }
    });
  }
  
  /** Add a column to the ExpandableTable. */
  public void addColumn(Column<T> column) {
    if (column instanceof ExpandColumn) {
      hasExpandColumn = true;
    }
    
    columns.add(column);
    if (tableHeader != null) {
      addHeaderColumn(column);
    }
  }
  
  /** Reset the model of the list. */
  public void setModel(ListModel<T> model) {
    if (this.model != null) {
      this.model.removeListDataListener(dataListener);
    }
    
    this.model = model;
    if (model != null) {
      model.addListDataListener(dataListener);
    }
    
    // Remove all existing component state
    componentsCreated = false;
    removeAll();
    
    setSelectedIndex(NO_ROW);
    
    revalidate();
  }

  public ListModel<T> getModel() {
    return model;
  }

  /** Get the column header kept in sync with the table. */
  public JTableHeader getColumnHeader() {
    if (tableHeader == null) {
      tableHeader = new JTableHeader();
      // Dummy up an associated JTable to stop synth UIs from NPEing
      JTable dummyTable = new JTable();
      tableHeader.setTable(dummyTable);
      // TODO: support resizing
      tableHeader.setResizingAllowed(false);
      tableHeader.setReorderingAllowed(false);
      for (Column<?> column : columns) {
        addHeaderColumn(column);
      }      
    }

    return tableHeader;
  }

  private void addHeaderColumn(Column<?> column) {
    TableColumn headerColumn = new TableColumn();
    headerColumn.setHeaderValue(column.getName());
    headerColumn.setHeaderRenderer(new TableCellRenderer() {
        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int column) {
          Component c = tableHeader.getDefaultRenderer()
              .getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
          c.setFont(fontManager.getTableFont());
          return c;
        }
      });
    tableHeader.getColumnModel().addColumn(headerColumn);
  }
  
  public int getLineHeight() {
    return lineHeight;
  }

  public void setLineHeight(int lineHeight) {
    this.lineHeight = lineHeight;
    revalidate();
  }

  public int getVisibleRowCount() {
    return visibleRowCount;
  }

  public void setVisibleRowCount(int visibleRowCount) {
    this.visibleRowCount = visibleRowCount;
    revalidate();
  }

  public int getSelectedIndex() {
    return this.selectedIndex;
  }
  
  /** Set the selected row. */
  public void setSelectedIndex(int selectedIndex) {
    setSelectedIndex(selectedIndex, true, true);
  }
  
  /** Set the selected row, and scrolls to ensure extra context is visible. */
  public void setSelectedIndexWithVisibleContext(int selectedIndex) {
    setSelectedIndex(selectedIndex, true, false);
    int startRow = Math.max(0, selectedIndex - getContextRows());
    int endRow = Math.min(getModel().getSize() - 1, selectedIndex + getContextRows());
    scrollRectToVisible(getRowBounds(startRow, endRow));
  }
  /**
   * Set the selected row, conditionally not updating row state.
   * Row state should not be updated when components have been added/removed,
   * as the existing components are all in the correct state.
   */
  private void setSelectedIndex(
      int selectedIndex, boolean updateRowState, boolean scrollToVisible) {
    if (this.selectedIndex == selectedIndex) {
      return;
    }
    
    if (this.selectedIndex != NO_ROW) {
      repaint(getRowBounds(this.selectedIndex));

      if (updateRowState) {
        // Update the old row to remove the selected state;  explicitly remove
        // STATE_SELECTED, since the selected index hasn't been updated yet
        Set<RowState> rowState = Sets.difference(
            getRowState(this.selectedIndex), STATE_SELECTED);
        updateRowState(this.selectedIndex, rowState);
      }
    }
    
    int oldSelectedIndex = this.selectedIndex;
    this.selectedIndex = selectedIndex;
    
    if (selectedIndex != NO_ROW) {
      Rectangle rowBounds = getRowBounds(selectedIndex);
      repaint(rowBounds);
      if (scrollToVisible) {
        scrollRectToVisible(rowBounds);
      }
      if (updateRowState) {
        updateRowState(selectedIndex);
      }
    }
    
    firePropertyChange("selectedIndex", oldSelectedIndex, selectedIndex);
  }
  
  public Object getSelectedValue() {
    if (this.selectedIndex == NO_ROW) {
      return null;
    }
    
    return model.getElementAt(this.selectedIndex);
  }
  
  public int getExpandedIndex() {
    return expandedIndex;
  }
  
  /** Sets the expanded row */
  public void setExpandedIndex(int expandedIndex) {
    setExpandedIndex(expandedIndex, true);
  }
  
  /**
   * Set the expanded row, conditionally not updating row state.
   * Row state should not be updated when components have been added/removed,
   * as the existing components are all in the correct state.
   */
  private void setExpandedIndex(int expandedIndex, boolean updateRowState) {
    if (this.expandedIndex == expandedIndex) {
      return;
    }
    
    if (this.expandedIndex != NO_ROW) {
      expandedRowView.onClose(expandedRowComponent);
      // If focus is in the expanded component, explicitly move the focus back to the table
      Component focusOwner = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
      if (focusOwner != null
          && SwingUtilities.isDescendingFrom(focusOwner, expandedRowComponent)) {
        SwingUtilities.invokeLater(this::requestFocusInWindow);
      }

      if (updateRowState) {
        // Update the old row to remove the expanded state.  This must happen *before* the row is 
        // removed (and before the expanded row index is set), or indices won't line up.
        // 
        // Later note: http://bitbucket.org/scythebill/scythebill-birdlist/issues/497 seems
        // to have been triggered by this decision, but this was cleared up by avoiding use
        // of expandedIndex in SingleLocationSpeciesListPanel.getCurrentSightingInfo()
        Set<RowState> rowState = Sets.difference(getRowState(this.expandedIndex), STATE_EXPANDED);
        updateRowState(this.expandedIndex, rowState);
      }

      remove(expandedRowComponent);
      expandedRowComponent = null;
    }
    
    int oldExpandedIndex = this.expandedIndex;
    this.expandedIndex = expandedIndex;
    
    if (expandedIndex != NO_ROW) {
      expandedRowComponent = Preconditions.checkNotNull(
          expandedRowView.createComponent(type.cast(model.getElementAt(expandedIndex))));
      // Attach expanded row view listeners
      
      int mask = UIUtils.isMacOS()
          ? InputEvent.ALT_MASK | InputEvent.META_MASK
          : InputEvent.ALT_MASK | InputEvent.SHIFT_MASK;

      KeyStroke nextExpandedRowKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, mask);
      Object nextExpandedRowKey = new Object();
      expandedRowComponent.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
          .put(nextExpandedRowKeyStroke, nextExpandedRowKey);
      expandedRowComponent.getActionMap().put(nextExpandedRowKey, new MoveToNextExpandedRow(true));
      
      KeyStroke previousExpandedRowKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_UP, mask);
      Object previousExpandedRowKey = new Object();
      expandedRowComponent.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
          .put(previousExpandedRowKeyStroke, previousExpandedRowKey);
      expandedRowComponent.getActionMap().put(previousExpandedRowKey, new MoveToNextExpandedRow(false));
      
      expandedRowComponent.setBorder(new CompoundBorder(
          // TODO: this is super-ugly.  Pick a non-ugly border.
          BorderFactory.createLoweredBevelBorder(),
          new EmptyBorder(2, EXPANDED_ROW_INDENT + 2, 2, 2)));
      // Add the expanded component at index 1 (so it has immediate focus)
      add(expandedRowComponent, 1);
      if (updateRowState) {
        // ... and this has to happen *after* the expanded component is added.
        updateRowState(expandedIndex);
      }
    }
    
    // TODO: this could be highly optimized - specifically, we only really need to
    // move up/down all the components after the former/current expanded row, and repaint
    // accordingly.  But this will get things going.
    int topOfOldAndNewIndices = (oldExpandedIndex == NO_ROW || expandedIndex < oldExpandedIndex)
        ? expandedIndex
        : oldExpandedIndex;
    repaintStartingAt(topOfOldAndNewIndices);
    
    if (expandedIndex != NO_ROW) {
      scrollRectToVisible(getExpandedRowBounds());
    }
  }
  
  public boolean isShowVerticalGridLines() {
    return showVerticalGridLines;
  }

  public void setShowVerticalGridLines(boolean showVerticalGridLines) {
    if (showVerticalGridLines != this.showVerticalGridLines) {
      this.showVerticalGridLines = showVerticalGridLines;
      revalidate();
    }
  }

  public boolean isHighlightEnabledRows() {
    return highlightEnabledRows;
  }

  /**
   * By default, ExpandableTable highlights alternating rows.  When this is
   * selected, only enabled rows are highlighted.
   */
  public void setHighlightEnabledRows(boolean highlightEnabledRows) {
    this.highlightEnabledRows = highlightEnabledRows;
  }

  public void setExpandedRowView(DetailView<T> expandedRowView) {
    if (this.expandedRowView != null && this.expandedRowComponent != null) {
      setExpandedIndex(NO_ROW);
    }
    this.expandedRowView = expandedRowView;
  }
  
  public DetailView<T> getExpandedRowView() {
    return expandedRowView;
  }
  
  /** Transfer focus to the expanded row. */
  public void focusOnExpandedIndex() {
    if (expandedRowComponent != null) {
      expandedRowComponent.transferFocus();
    } else {
      // No expanded row - just grab focus for the table
      requestFocusInWindow();
    }
  }
  
  @Override protected void paintComponent(Graphics g) {
    Rectangle clipBounds = g.getClipBounds();
    
    int row = getBoundedRow(clipBounds.y);
    
    while (true) {
      Rectangle rowBounds = getRowBounds(row);
      if (!rowBounds.intersects(clipBounds)) {
        break;
      }
      
      Color color;
      if (row == selectedIndex) {
        if (UIUtils.isMacOS() || hasFocus()) {
          color = UIManager.getColor("TextPane.selectionBackground");
        } else {
          color = UIManager.getColor("Table.selectionBackground");
        }
      } else {
        boolean useAlternateBackground;
        if (isHighlightEnabledRows()) {
          useAlternateBackground = getEnabledRowsModel().isIncluded(row);
        } else {
          useAlternateBackground = row % 2 != 0;
        }
        color = UIManager.getColor(useAlternateBackground
            ? "Table.alternateBackground.0"
            : "Table.alternateBackground.1");
      }
      
      if (color == null) {
        color = UIManager.getColor("Table.background");
      }
      
      g.setColor(color);
      g.fillRect(rowBounds.x, rowBounds.y, rowBounds.width, rowBounds.height);
      row++;
    }
    
    if (showVerticalGridLines && columnWidths != null) {
      Rectangle expandedRowBounds = getExpandedRowBounds();
      g.setColor(UIManager.getColor("Table.gridColor"));
      int gridX = 0;
      for (int columnWidth : columnWidths) {
        if (gridX != 0) {
          
          if (expandedRowBounds == null
              || !clipBounds.intersects(expandedRowBounds)) {
            g.drawLine(gridX, clipBounds.y, gridX, clipBounds.y + clipBounds.height);
          } else {
            g.drawLine(gridX, clipBounds.y, gridX, expandedRowBounds.y);
            g.drawLine(gridX, expandedRowBounds.y + expandedRowBounds.height,
                gridX, clipBounds.y + clipBounds.height);
          }
        }
        gridX += (columnWidth + 1);
      }
    }
  }

  // Scrollable implementation
  
  @Override public Dimension getPreferredScrollableViewportSize() {
    return new Dimension(200, visibleRowCount * lineHeight);
  }

  @Override public int getScrollableBlockIncrement(Rectangle visibleRect,
      int orientation, int direction) {
    if (orientation == SwingConstants.HORIZONTAL) {
      return visibleRect.width;
    } else {
      return visibleRect.height;
    }
  }

  @Override public boolean getScrollableTracksViewportHeight() {
    return getParent() instanceof JViewport
        && getPreferredSize().height < getParent().getHeight();
  }

  @Override public boolean getScrollableTracksViewportWidth() {
    return true;
  }

  @Override public int getScrollableUnitIncrement(Rectangle visibleRect,
      int orientation, int direction) {
    if (orientation == SwingConstants.HORIZONTAL) {
      return 1;
    } else {
      return lineHeight;
    }
  }
  
  private int getVerticalBlockIncrement() {
    if (!(getParent() instanceof JViewport)) {
      return 1;
    }
    
    JViewport viewport = (JViewport) getParent();
    return viewport.getSize().height / lineHeight;
  }
  
  /** Repaint all content from a given row down. */
  private void repaintStartingAt(int row) {
    revalidate();
    Rectangle bounds = getRowBounds(row);
    bounds.height = getHeight() - bounds.y;
    repaint(bounds);
  }
  
  private Rectangle getRowBounds(int row) {
    int y = getRowY(row);
    return new Rectangle(0, y, getWidth(), lineHeight);
  }

  private int getRowY(int row) {
    int y = lineHeight * row;
    if (expandedIndex != NO_ROW && row > expandedIndex) {
      y += expandedRowView.getHeight();
    }
    return y;
  }

  private Rectangle getRowBounds(int startRow, int endRow) {
    int startY = getRowY(startRow);
    int endY = getRowY(endRow) + lineHeight;
    return new Rectangle(0, startY, getWidth(), endY - startY);
  }

  private Rectangle getExpandedRowBounds() {
    if (expandedIndex == NO_ROW) {
      return null;
    }
    
    return new Rectangle(0, lineHeight * (expandedIndex + 1),
        getWidth(), expandedRowView.getHeight());
  }
  
  /** Return the row, bounding to 0 and the size. */
  private int getBoundedRow(int y) {
    Rectangle expandedRowBounds = getExpandedRowBounds();
    if (expandedRowBounds != null && y >= expandedRowBounds.y) {
      if (y < expandedRowBounds.y + expandedRowBounds.height) {
        return expandedIndex;
      }
      
      y -= expandedRowBounds.height;
    }
    
    int row = y / lineHeight;
    if (row < 0) {
      row = 0;
    } else {
      row = Math.min(row, model.getSize() - 1);
    }
    
    return row;
  }
  
  /** Get the row, returning -1 if it misses all rows. */ 
  public int getRow(int y) {
    Rectangle expandedRowBounds = getExpandedRowBounds();
    if (expandedRowBounds != null && y >= expandedRowBounds.y) {
      // Inside the expanded row - return it (perhaps dubious in some cases)
      if (y < expandedRowBounds.y + expandedRowBounds.height) {
        return getExpandedIndex();
      }
      
      y -= expandedRowBounds.height;
    }
    
    int row = y / lineHeight;
    if (row < 0) {
      return NO_ROW;
    } else if (row >= model.getSize()) {
      return NO_ROW;
    }
    
    return row;
  }
  
  /**
   * Hack API to force component creation.  For large tables, it's useful to
   * add all the components before the table is attached and visible.
   */
  public void forceComponentCreation() {
    createComponents();
  }
  
  private void createComponents() {
    if (componentsCreated) {
      return;
    }
    
    componentsCreated = true;
    DelayedUI.instance().enableDelay();
    try {
      removeAll();

      add(focusStartHoneypot);
      
      // Since we just removed all components, re-add the expanded-row-compoennt
      if (expandedRowComponent != null) {
        add(expandedRowComponent);
        fontManager.applyTo(expandedRowComponent);
      }
      
      if (model != null) {
        int modelSize = model.getSize();
        for (int row = 0; row < modelSize; row++) {
          T value = type.cast(model.getElementAt(row));
          Set<RowState> rowState = getRowState(row);
          for (Column<T> column : columns) {
            JComponent created = column.createComponent(value, rowState);
            fontManager.applyTo(created);
            add(created);
          }
        }

        add(focusEndHoneypot);
      }
    } finally {
      DelayedUI.instance().liftDelay();
    }
  }
  
  /** Return the state of a row. */
  private Set<RowState> getRowState(int row) {
    if (getEnabledRowsModel().isIncluded(row)) {
      if (row == getSelectedIndex()) {
        if (row == getExpandedIndex()) {
          return STATE_ENABLED_SELECTED_AND_EXPANDED;
        } else {
          return STATE_ENABLED_SELECTED;
        }
      } else if (row == getExpandedIndex()) {
        return STATE_ENABLED_EXPANDED;
      } else {
        return STATE_ENABLED;
      }
    } else {
      if (row == getSelectedIndex()) {
        if (row == getExpandedIndex()) {
          return STATE_SELECTED_AND_EXPANDED;
        } else {
          return STATE_SELECTED;
        }
      } else if (row == getExpandedIndex()) {
        return STATE_EXPANDED;
      } else {
        return STATE_NONE;
      }
    }
  }

  /** Set the state of all columns in a row. */
  private void updateRowState(int row) {
    updateRowState(row, getRowState(row));
  }

  /** Set the state of all columns in a row. */
  private void updateRowState(int row, Set<RowState> rowState) {
    if (componentsCreated) {
      // Offset of 1 when there's no expanded component, and 2 when there is
      // (the expanded component, plus the focus start honeypot)
      int offset = expandedIndex == NO_ROW ? 1 : 2;
      int componentIndex = offset + (row * columns.size());
      for (Column<T> column : columns) {
        T value = type.cast(model.getElementAt(row));
        column.updateComponentState(getComponent(componentIndex++), value, rowState);
      }
    }
  }

  private class LayoutImpl implements LayoutManager {
    @Override public void addLayoutComponent(String name, Component comp) {
    }

    @Override public void layoutContainer(Container container) {
      createComponents();
      
      // Move the honeypots offscreen
      focusStartHoneypot.setBounds(-100, -100, 1, 1);
      focusEndHoneypot.setBounds(-100, -100, 1, 1);
      
      int expandedRowViewCount = expandedRowComponent == null ? 0 : 1;
      int focusHoneypotCount = 2;
      int expectedComponentCount = ((model == null ? 0 : model.getSize()) * columns.size()) + expandedRowViewCount + focusHoneypotCount;
      Preconditions.checkState(getComponentCount() == expectedComponentCount,
          "Invalid component count.  Expected %s, was %s",
          expectedComponentCount,
          getComponentCount());
      
      // Lay out columns
      columnWidths = new int[columns.size()];
      // Figure out how much space is used by fixed width columns
      int totalFixedWidth = 0;
      // Add one pixel per column boundary if grid lines are shown
      if (showVerticalGridLines) {
        totalFixedWidth += (columns.size() - 1);
      }
      float totalWeightedWidth = 0.0f;
      
      for (Column<T> column : columns) {
        ColumnLayout width = column.getWidth();
        if (width.isFixed) {
          totalFixedWidth += width.fixedWidth(fontManager);
        } else {
          totalWeightedWidth += width.widthWeight;
        }
      }
      
      int columnIndex = 0;
      for (Column<T> column : columns) {
        ColumnLayout width = column.getWidth();
        int pixels;
        if (width.isFixed) {
          pixels = width.fixedWidth(fontManager);
        } else {
          pixels = (int) ((container.getWidth() - totalFixedWidth)
              * (width.widthWeight / totalWeightedWidth));
        }
        
        if (tableHeader != null) {
          // Set the width of each column;  append 1, as JTableHeader includes border
          // in the size of each column, and ExpandableTable doesn't.  And then another
          // 1 pixel for the first column - brutal hack
          // TODO: undo this;  it certainly isn't working on current MacOS.
          // Test on other OS as well - this might have only been a Quaqua bug.
          int columnHeaderWidth = pixels;// + (columnIndex == 0 ? 2 : 1);
          tableHeader.getColumnModel().getColumn(columnIndex).setWidth(columnHeaderWidth);
        }
        columnWidths[columnIndex++] = pixels;
      }
      
      if (model != null) {
        int yPosition = 0;
        int row = 0;
        for (int componentOffset = expandedRowViewCount +  1; // expanded count, + 1 for the start focus honeypot
            componentOffset < getComponentCount() - 1; // -1 for the end focus honeypot
            componentOffset += columns.size()) {
          int xPosition = 0;
          for (int column = 0; column < columns.size(); column++) {
            int width = columnWidths[column];
            // Count out the overall offset, 
            Component cellComponent = getComponent(componentOffset + column);
            if (columns.get(column).sizeComponentsToFit()) {
              Dimension maxSize = cellComponent.getPreferredSize();
              cellComponent.setBounds(xPosition, yPosition, Math.min(width, maxSize.width), lineHeight);
            } else {
              cellComponent.setBounds(xPosition, yPosition, width, lineHeight);
            }
            xPosition += width;
            if (showVerticalGridLines) {
              xPosition += 1;
            }
          }
          yPosition += lineHeight;
          if (expandedRowComponent != null && row == expandedIndex) {
            yPosition += expandedRowView.getHeight();
          }
          row++;
        }
      }
      
      if (expandedRowComponent != null) {
        if (expandedIndex == NO_ROW) {
          expandedRowComponent.setVisible(false);
        } else {
          expandedRowComponent.setBounds(0, lineHeight * (expandedIndex + 1),
              container.getWidth(), expandedRowView.getHeight());
          expandedRowComponent.setVisible(true);
        }
      }
    }

    @Override public Dimension minimumLayoutSize(Container container) {
      return new Dimension(10, 10);
    }

    @Override public Dimension preferredLayoutSize(Container container) {
      int rowCount = model == null ? 0 : model.getSize();
      int height = rowCount * lineHeight;
      if (expandedRowComponent != null && expandedIndex != NO_ROW) {
        height += expandedRowView.getHeight();
      }
      
      return new Dimension(200, height);
    }

    @Override public void removeLayoutComponent(Component comp) {
    }    
  }
  
  private class ModelEvents implements ListDataListener {
    @Override public void contentsChanged(ListDataEvent event) {
      createComponents();
      int offset = getComponentOffset();
      int componentIndex = offset + (event.getIndex0() * columns.size());
      for (int row = event.getIndex0(); row <= event.getIndex1(); row++) {
        T value = type.cast(model.getElementAt(row));
        for (Column<T> column : columns) {
          column.updateComponentValue(getComponent(componentIndex++), value);
        }
      }
      
      Rectangle bounds0 = getRowBounds(event.getIndex0());
      Rectangle bounds1 = getRowBounds(event.getIndex1());
      repaint(bounds0.x, bounds0.y, bounds0.width, bounds1.y + bounds1.height - bounds0.y);
    }

    @Override public void intervalAdded(ListDataEvent event) {
      // UGH.  New rows might be enabled, but the enabled rows model really shouldn't
      // be updated until the rows are already added.  So hack and check the type
      // of the rows model.
      boolean allRowsAreEnabled = getEnabledRowsModel() instanceof AllRowsTableRowsModel;
      Set<RowState> rowState = allRowsAreEnabled ? STATE_ENABLED : STATE_NONE;
      // Add needed components (but only if nothing has been added yet)
      if (componentsCreated) {
        int offset = getComponentOffset();
        int componentIndex = offset + (event.getIndex0() * columns.size());
        for (int row = event.getIndex0(); row <= event.getIndex1(); row++) {
          T value = type.cast(model.getElementAt(row));
          for (Column<T> column : columns) {
            JComponent component = column.createComponent(value, rowState);
            fontManager.applyTo(component);
            add(component, componentIndex++);
          }
        }
      }

      // Update the enabled rows model immediately;  this ensures that changes to selection or expanded index
      // below, which might trigger refreshing component state, will get the correct "enabled" state value.
      getEnabledRowsModel().intervalAdded(event.getIndex0(), event.getIndex1());

      // Advance the current selection by N rows if it comes after the new block
      if (selectedIndex != NO_ROW && selectedIndex >= event.getIndex0()) {
        setSelectedIndex(selectedIndex + (event.getIndex1() - event.getIndex0() + 1), false, true);
      }
      if (expandedIndex != NO_ROW && expandedIndex >= event.getIndex0()) {
        setExpandedIndex(expandedIndex + (event.getIndex1() - event.getIndex0() + 1), false);
      }
      
      redrawFromStartOfEvent(event);
    }

    @Override public void intervalRemoved(ListDataEvent event) {
      if (componentsCreated) {
        int offset = getComponentOffset();
        int componentIndex = offset + (event.getIndex0() * columns.size());
        for (int row = event.getIndex0(); row <= event.getIndex1(); row++) {
          for (@SuppressWarnings("unused") Column<?> ignored : columns) {
            remove(componentIndex);
          }
        }
      }
      
      // Remove the current expansion and selection if it's inside the deleted block;
      // otherwise if it comes after adjust the index
      if (expandedIndex != NO_ROW && expandedIndex >= event.getIndex0()) {
          setExpandedIndex(adjustRowForDeletion(event, expandedIndex), false);
        }
      if (selectedIndex != NO_ROW && selectedIndex >= event.getIndex0()) {
        setSelectedIndex(adjustRowForDeletion(event, selectedIndex), false, true);
      }

      getEnabledRowsModel().intervalRemoved(event.getIndex0(), event.getIndex1());
      redrawFromStartOfEvent(event);
    }

    private void redrawFromStartOfEvent(ListDataEvent event) {
      int row = event.getIndex0();
      repaintStartingAt(row);
    }

    private int adjustRowForDeletion(ListDataEvent event, int row) {
      if (row <= event.getIndex1()) {
        return NO_ROW;
      } else {
        return row - (event.getIndex1() - event.getIndex0() + 1);
      } 
    }
  }
  
  private class MouseEvents extends MouseAdapter {
    @Override public void mouseClicked(MouseEvent e) {
      int row = getRow(e.getY());
      // TODO: platform-agnostic way of detecting this?
      if (((e.getModifiers() & InputEvent.META_MASK) != 0) && row == selectedIndex) {
        setSelectedIndex(NO_ROW);
      } else {
        setSelectedIndex(row);
      }
      
      requestFocusInWindow();
    }
  }
  
  private class KeyEvents extends KeyAdapter {
    @Override public void keyPressed(KeyEvent e) {
      switch (e.getKeyCode()) {
        case KeyEvent.VK_UP:
          if (selectedIndex > 0) {
            setSelectedIndex(selectedIndex - 1, true, false);
            // Ensure that sufficient context rows are visible
            scrollRectToVisible(getRowBounds(Math.max(0, selectedIndex - getContextRows()), selectedIndex - 1));
          } else {
            setSelectedIndex(0);
          }
          e.consume();
          break;
        case KeyEvent.VK_DOWN:
          if (selectedIndex != NO_ROW) {
            int maxRow = model.getSize() - 1;
            int newRow = Math.min(maxRow, selectedIndex + 1);
            setSelectedIndex(newRow, true, false);
            // Ensure that sufficient context rows are visible
            scrollRectToVisible(getRowBounds(newRow, Math.min(maxRow, selectedIndex + getContextRows())));
          } else {
            setSelectedIndex(0);
          }
          e.consume();
          break;

        case KeyEvent.VK_PAGE_UP:
          setSelectedIndex(Math.max(0, selectedIndex - getVerticalBlockIncrement()));
          e.consume();
          break;

        case KeyEvent.VK_PAGE_DOWN:
          setSelectedIndex(Math.min(model.getSize() - 1, selectedIndex + getVerticalBlockIncrement()));
          e.consume();
          break;

        case KeyEvent.VK_HOME:
          setSelectedIndex(0);
          e.consume();
          break;

        case KeyEvent.VK_END:
          setSelectedIndex(model.getSize() - 1);
          e.consume();
          break;

        case KeyEvent.VK_RIGHT:
          if (selectedIndex != NO_ROW
              && hasExpandColumn
              && getEnabledRowsModel().isIncluded(selectedIndex)) {
            setExpandedIndex(selectedIndex);
            e.consume();
          }
          break;
        case KeyEvent.VK_LEFT:
          if (selectedIndex != NO_ROW && expandedIndex == selectedIndex && hasExpandColumn) {
            setExpandedIndex(NO_ROW);
            e.consume();
          }
          break;
      }
    }
  }

  @Override
  public synchronized void addComponentListener(ComponentListener l) {
    // Break AncestorNotifiers on ExpandableTables - the table ends up getting hugely loaded
    // down with them, which is problematic.
    if (l.getClass().getName().contains("AncestorNotifier")) {
      return;
    }
    
    super.addComponentListener(l);
  }
  
  /**
   * Custom FocusTraversalPolicy.  Without this, tabbing gets incredibly slow as the
   * "layout order" policy constantly tries to sort thousands of components.
   */
  class ExpandableTableFocusTraversalPolicy extends ContainerOrderFocusTraversalPolicy {
    @Override
    public Component getComponentAfter(Container container, Component component) {
      Component componentAfter = super.getComponentAfter(container, component);
      // If we're about to focus on the start honeypot, move one forward
      if (componentAfter == focusStartHoneypot) {
        componentAfter = super.getComponentAfter(container, focusStartHoneypot);
      }
      
      // If we're about to focus on the end honeypot, move out of the table.
      // (This includes cases where the end honeypot is immediately after the
      // start honeypot.
      if (componentAfter == focusEndHoneypot) {
        componentAfter = findComponentAfterTable(container);
      }
      
      return componentAfter;
    }

    @Override
    public Component getComponentBefore(Container container, Component component) {
      // Move focus out of the table (even though it's a focus root)
      if (component == container) {
        return findComponentBeforeTable(container);
      }
      
      Component componentBefore = super.getComponentBefore(container, component);
      // If focus moved onto the start honeypot, move it up to the table
      if (componentBefore == focusStartHoneypot) {
        componentBefore = container;
      }
      return componentBefore;
    }

    @Override
    public Component getDefaultComponent(Container container) {
      
      // EPIC HACK!  Because ExpandableTable is a focus root, every time you tab into it, this method gets hit.
      // Unfortunately, this method also gets hit when you SHIFT-tab into it.  We'd like to return a different
      // component for those two cases, but the API doesn't tell us...  So grab the stack!  The two entries above
      // this will typically be either:
      // TAB:
      //  at javax.swing.SortingFocusTraversalPolicy.getComponentDownCycle(SortingFocusTraversalPolicy.java:194)
      //  at javax.swing.SortingFocusTraversalPolicy.getComponentAfter(SortingFocusTraversalPolicy.java:255)
      // SHIFT-TAB:
      //  at javax.swing.SortingFocusTraversalPolicy.getComponentDownCycle(SortingFocusTraversalPolicy.java:194)
      //  at javax.swing.SortingFocusTraversalPolicy.getComponentBefore(SortingFocusTraversalPolicy.java:392)
      // Of course, this is not guaranteed in the least - but if it fails, the end result *should* just be that
      // we always tab back to the first element, which isn't the worst outcome.
      boolean isBackwards = false;
      StackTraceElement[] stackTrace = new Throwable().getStackTrace();
      if (stackTrace.length > 3
          && "getComponentBefore".equals(stackTrace[2].getMethodName())) {
        isBackwards = true;
      }

      Component defaultComponent = isBackwards
          ? getLastComponent(container)
          : super.getDefaultComponent(container);
      // Pick a default component, but avoid the honeypots (and as a last resort,
      // focus on the table itself)
      if (defaultComponent == focusStartHoneypot
          || defaultComponent == focusEndHoneypot
          || !SwingUtilities.isDescendingFrom(defaultComponent, ExpandableTable.this)) {
        if (isBackwards) {
          defaultComponent = getComponentBefore(container, focusEndHoneypot);
          if (defaultComponent == focusStartHoneypot) {
            defaultComponent = container;
          }
        } else {
          defaultComponent = getComponentAfter(container, focusStartHoneypot);
          if (defaultComponent == focusEndHoneypot) {
            defaultComponent = container;
          }
        }
      }
      
      return defaultComponent;
    }

    private Component nextComponent;
    private Component previousComponent;
    
    @Override
    public Component getFirstComponent(Container container) {
      // By default, focus after the start honeypot
      Component firstComponent = getComponentAfter(container, focusStartHoneypot);
      
      // But if that doesn't work, and focus would go back to the table, then
      // leave the table altogether
      if (firstComponent == container) {
        firstComponent = findComponentAfterTable(container);
      }
      return firstComponent;
    }

    /** Find the component immediately after the table. */
    Component findComponentAfterTable(Container container) {
      if (getFocusableComponentAfterTable() != null) {
        return getFocusableComponentAfterTable();
      }
      Component afterComponent;
      if (nextComponent == null) {
        // HACK: turn everything off, and let the default Swing algorithm find the component
        // after the table.  This is slow, so typically you want getFocusableComponentAfterTable()
        Container rootAncestor = container.getParent().getFocusCycleRootAncestor();
        container.setFocusCycleRoot(false);
        focusEndHoneypot.setFocusable(false);
        focusStartHoneypot.setFocusable(false);
        nextComponent = rootAncestor.getFocusTraversalPolicy().getComponentAfter(rootAncestor, container);
        focusEndHoneypot.setFocusable(true);
        focusStartHoneypot.setFocusable(true);
        container.setFocusCycleRoot(true);
      }
      afterComponent = nextComponent;
      return afterComponent;
    }

    /** Find the component immediately before the table. */
    Component findComponentBeforeTable(Container container) {
      if (getFocusableComponentBeforeTable() != null) {
        return getFocusableComponentBeforeTable();
      }

      Component beforeComponent;
      if (previousComponent == null) {
        // HACK: turn everything off, and let the default Swing algorithm find the component
        // before the table.  This is slow, so typically you want getFocusableComponentBeforeTable()
        Container rootAncestor = container.getParent().getFocusCycleRootAncestor();
        container.setFocusCycleRoot(false);
        focusEndHoneypot.setFocusable(false);
        focusStartHoneypot.setFocusable(false);
        previousComponent = rootAncestor.getFocusTraversalPolicy().getComponentBefore(rootAncestor, container);
        focusEndHoneypot.setFocusable(true);
        focusStartHoneypot.setFocusable(true);
        container.setFocusCycleRoot(true);
      }
      beforeComponent = previousComponent;
      return beforeComponent;
    }

    @Override
    public Component getLastComponent(Container container) {
      Component lastComponent = getComponentBefore(container, focusEndHoneypot);
      return lastComponent;
    }

    @Override
    protected boolean accept(final Component candidate) {
      if (!candidate.isEnabled() || !candidate.isFocusable()) {
        return false;
      }
      
      if (candidate instanceof JComboBox) {
        return ((JComboBox<?>) candidate).getUI().isFocusTraversable((JComboBox<?>) candidate);
      }

      if (candidate instanceof JComponent) {
        // Blargh;  no access to the override that doesn't force-create, so this will be expensive...
        InputMap whenFocusedMap = ((JComponent) candidate).getInputMap(JComponent.WHEN_FOCUSED);
        if (whenFocusedMap == null
            || whenFocusedMap.allKeys() == null
            || whenFocusedMap.allKeys().length == 0) {
          return false;
        }
      }

      return super.accept(candidate);
    }
  }
  
  /**
   * FocusTracker for ExpandableTable.  Responsible for ensuring the focused areas are visible.
   */
  class TableFocusTracker extends FocusTracker {
    protected TableFocusTracker() {
      super(ExpandableTable.this);
    }

    @Override
    protected boolean includeTextComponents() {
      // This tracker only adjusts visibility, so it does want to hear about focus entering
      // text components.
      return true;
    }

    @Override
    protected void focusGained(Component child) {
      if (expandedRowComponent != null
          && SwingUtilities.isDescendingFrom(child, expandedRowComponent)) {
        // If the focus went inside of the expanded row component, just make the whole
        // expanded row visible.
        Rectangle expandedRowBounds = expandedRowComponent.getBounds();
        // ... and try to keep the selected row visible too (assuming it's the same);
        if (selectedIndex == expandedIndex) {
          expandedRowBounds = expandedRowBounds.union(getRowBounds(selectedIndex));
        }
        scrollRectToVisible(expandedRowBounds);
      } else if (child == ExpandableTable.this) {
        // If the focus went to the table as a whole, focus on the selected row
        if (selectedIndex != NO_ROW) {
          Rectangle rowBounds = getRowBounds(selectedIndex);
          // ... and make the selected row and the expanded row visible if they're the same
          if (selectedIndex == expandedIndex) {
            Rectangle expandedRowBounds = expandedRowComponent.getBounds();
            rowBounds = rowBounds.union(expandedRowBounds);
          }
          scrollRectToVisible(rowBounds);
        }
      } else {
        // Otherwise, the focus has gone to some other child of the table.
        // Make sure that component is visible.
        Rectangle bounds = child.getBounds();
        if (child.getParent() != ExpandableTable.this) {
          bounds = SwingUtilities.convertRectangle(child.getParent(), bounds, ExpandableTable.this);
        }
        scrollRectToVisible(bounds);
      }
    }

    @Override
    protected void focusLost(Component child) {
    }
    
  }

  public Component getComponent(int row, Column<T> column) {
    int columnIndex = columns.indexOf(column);
    Preconditions.checkArgument(columnIndex >= 0);
    
    int componentIndex = getComponentOffset()
        + (row * columns.size())
        + columnIndex;
    return getComponent(componentIndex);
  }

  public void resetColumnValues(Column<T> column) {
    if (!componentsCreated) {
      return;
    }

    int columnIndex = columns.indexOf(column);
    Preconditions.checkArgument(columnIndex >= 0);

    DelayedUI.instance().enableDelay();
    try {
      for (int i = 0; i < model.getSize(); i++) {      
        int componentIndex = getComponentOffset()
            + (i* columns.size())
            + columnIndex;
        Component component = getComponent(componentIndex);
        T value = type.cast(model.getElementAt(i));
        column.updateComponentValue(component, value);
      }
    } finally {
      DelayedUI.instance().liftDelay();
    }
  }
  
  private int getComponentOffset() {
    // Offset of 1 when there's no expanded component, and 2 when there is
    // (the expanded component, plus the focus start honeypot)
    int offset = expandedIndex == NO_ROW ? 1 : 2;
    return offset;
  }

  public Component getFocusableComponentAfterTable() {
    return focusableComponentAfterTable;
  }

  public void setFocusableComponentAfterTable(Component focusableComponentAfterTable) {
    this.focusableComponentAfterTable = focusableComponentAfterTable;
  }

  public Component getFocusableComponentBeforeTable() {
    return focusableComponentBeforeTable;
  }

  public void setFocusableComponentBeforeTable(Component focusableComponentBeforeTable) {
    this.focusableComponentBeforeTable = focusableComponentBeforeTable;
  }
  
  public JComponent getExpandedRowComponent() {
    return expandedRowComponent;
  }

  /** Number of extra rows to ensure visible when scrolling via the keyboard. */ 
  private int getContextRows() {
    return 3;
  }
  
  class MoveToNextExpandedRow extends AbstractAction {
    private final boolean next;
    MoveToNextExpandedRow(boolean next) {
      this.next = next;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
      if (expandedIndex == NO_ROW || expandedRowComponent == null) {
        return;
      }
      
      int newExpandedIndex = expandedIndex + (next ? 1 : -1);
      if (newExpandedIndex < 0
          || newExpandedIndex >= getModel().getSize()) {
        return;
      }
      
      Component focusOwner = FocusManager.getCurrentManager().getFocusOwner();
      if (focusOwner == null) {
        return;
      }
      
      List<Integer> childIndices = new LinkedList<>();
      while (focusOwner != expandedRowComponent) {
        // Focus owner somehow wasn't inside the expanded row?  Bail.
        if (focusOwner == null) {
          return;
        }
        
        // Find the focus owner in its parent, and insert that index at the front of the list
        Container parent = focusOwner.getParent();
        for (int i = 0; i < parent.getComponentCount(); i++) {
          if (focusOwner == parent.getComponent(i)) {
            childIndices.add(0, i);
            break;
          }
        }
        
        focusOwner = parent;
      }
      
      if (childIndices.isEmpty()) {
        return;
      }
      
      // Expand that next row
      setExpandedIndex(newExpandedIndex);
      Component component = expandedRowComponent; 
      for (int childIndex : childIndices) {
        if (!(component instanceof Container)) {
          return;
        }
        
        Container parent = (Container) component;
        if (childIndex >= parent.getComponentCount()) {
          return;
        }
        
        component = parent.getComponent(childIndex);
      }
      
      Component finalComponent = component;
      // And focus on the identified component
      SwingUtilities.invokeLater(() -> finalComponent.requestFocusInWindow());
    }
    
  }
}
