/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components.table;

import java.awt.Component;
import java.util.Set;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import com.scythebill.birdlist.ui.components.table.ExpandableTable.Column;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.ColumnLayout;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.RowState;
import com.scythebill.birdlist.ui.fonts.FontManager;

/**
 * Column providing a single label.
 */
public class LabelColumn<T> implements Column<T> {
  private ColumnLayout width;
  private final String name;

  public LabelColumn(ColumnLayout width, String name) {
    this.width = width;
    this.name = name;
  }

  @Override public JComponent createComponent(T value, Set<RowState> states) {
    JLabel label = new JLabel() {
      @Override
      protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        // Stop Swing from adding a bazillion AncestorListeners to the table via a vile hack.
        if (!"parent".equals(propertyName) && !"ancestor".equals(propertyName)) {
          super.firePropertyChange(propertyName, oldValue, newValue);
        }
      }
    };
    label.setOpaque(false);
    label.setFocusable(false);
    label.setBorder(new EmptyBorder(0, 3, 0, 0));
    label.putClientProperty(FontManager.PLAIN_LABEL, true);
    updateComponentValue(label, value);
    updateComponentState(label, value, states);
    return label;
  }

  @Override public ColumnLayout getWidth() {
    return width;
  }

  @Override
  public boolean sizeComponentsToFit() {
    return false;
  }

  protected String getText(T value) {
    return value == null ? "" : value.toString();
  }

  @Override public void updateComponentValue(Component component, T value) {
    ((JLabel) component).setText(getText(value));
  }

  @Override public void updateComponentState(Component component,
      T value, Set<RowState> states) {
    // Set the component foreground color
    component.setForeground(UIManager.getColor(states.contains(RowState.SELECTED)
        ? "Table.selectionForeground"
        : "Table.foreground"));
  }

  @Override public String getName() {
    return name;
  }
}
