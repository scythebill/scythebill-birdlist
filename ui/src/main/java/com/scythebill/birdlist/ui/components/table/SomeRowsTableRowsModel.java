/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.components.table;

import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * TableRowsModel supporting an explicitly specified set of rows.
 */
public class SomeRowsTableRowsModel implements TableRowsModel {
  private Set<Integer> rows = Sets.newHashSet();
  private final List<Listener> listeners = Lists.newArrayList();
  
  @Override
  public boolean isIncluded(int row) {
    return rows.contains(row);
  }

  @Override
  public void intervalAdded(int rowStart, int rowEndInclusive) {
    Set<Integer> newRows = Sets.newHashSet();
    int rowCount = (rowEndInclusive - rowStart) + 1; 
    for (int row : rows) {
      if (row < rowStart) {
        newRows.add(row);
      } else {
        newRows.add(row + rowCount);
      }
    }
    
    rows = newRows;
  }

  @Override
  public boolean intervalRemoved(int rowStart, int rowEndInclusive) {
    Set<Integer> newRows = Sets.newHashSet();
    int rowCount = (rowEndInclusive - rowStart) + 1;
    boolean foundAny = false;
    for (int row : rows) {
      if (row < rowStart) {
        newRows.add(row);
      } else if (row <= rowEndInclusive) {
        foundAny = true;
      } else {
        newRows.add(row - rowCount);
      }
    }
    
    rows = newRows;
    return foundAny;
  }

  @Override
  public void addListener(Listener listener) {
    listeners.add(listener);
  }

  @Override
  public void includeRow(int row) {
    if (rows.add(row)) {
      for (Listener listener : listeners) {
        listener.rowIncluded(row);
      }
    }
  }

  @Override
  public void removeRow(int row) {
    if (rows.remove(row)) {
      for (Listener listener : listeners) {
        listener.rowRemoved(row);
      }
    }
  }

}
