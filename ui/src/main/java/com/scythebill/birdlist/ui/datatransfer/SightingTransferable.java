/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.datatransfer;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;


/**
 * Transferable for groups of sightings.
 */
public class SightingTransferable implements Transferable {

  private final SightingsGroup sightings;
  private final Transferable additional;

  public SightingTransferable(SightingsGroup sightings, @Nullable Transferable additional) {
    this.sightings = sightings;
    this.additional = additional;
  }

  @Override
  public Object getTransferData(DataFlavor flavor)
      throws UnsupportedFlavorException, IOException {
    if (SightingsGroup.FLAVOR.equals(flavor)) {
      return sightings;
    }
    
    if (additional != null) {
      return additional.getTransferData(flavor);
    }
    
    throw new UnsupportedFlavorException(flavor);
  }

  @Override
  public DataFlavor[] getTransferDataFlavors() {
    if (additional == null) {
      return new DataFlavor[] { SightingsGroup.FLAVOR };
    }
    
    List<DataFlavor> flavors = new ArrayList<>();
    flavors.add(SightingsGroup.FLAVOR);
    flavors.addAll(Arrays.asList(additional.getTransferDataFlavors()));
    return flavors.toArray(new DataFlavor[0]);
  }

  @Override
  public boolean isDataFlavorSupported(DataFlavor flavor) {
    if (SightingsGroup.FLAVOR.equals(flavor)) {
      return true;
    }
    
    if (additional == null) {
      return false;
    }
    
    return additional.isDataFlavorSupported(flavor);
  }
}