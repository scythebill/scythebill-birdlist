/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.datatransfer;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import javax.swing.tree.TreePath;

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;

/**
 * Object used for drag-and-drop of sightings.
 */  
public class SightingsGroup {
  /** DataFlavor of a SightingsGroup */
  public static final DataFlavor FLAVOR =
      new DataFlavor(SightingsGroup.class, "Sighting");

  /** Extract a SightingsGroup from a Transferable. */
  public static SightingsGroup fromTransferable(Transferable data) {
    try {
      return (SightingsGroup) data.getTransferData(FLAVOR);
    } catch (UnsupportedFlavorException e) {
      throw new RuntimeException(e);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private final ImmutableList<Sighting> sightings;
  private final TreePath commonParent;
  private final Taxon sourceTaxon;
  private final ImmutableList<Integer> originalIndices;
  private final ImmutableMap<VisitInfoKey, VisitInfo> visitInfoMap;

  public SightingsGroup(
      ImmutableList<Sighting> sightings,
      ImmutableMap<VisitInfoKey, VisitInfo> visitInfoMap,
      ImmutableList<Integer> originalIndices, TreePath commonParent) {
    this.sightings = sightings;
    this.visitInfoMap = visitInfoMap;
    this.originalIndices = originalIndices;
    this.commonParent = commonParent;
    this.sourceTaxon = commonParent.getLastPathComponent() instanceof Taxon
        ? (Taxon) commonParent.getLastPathComponent()
        : null;
  }
  
  /**
   * Builders for the current sightings;  retrieve when pasting/dropping the sightings.
   */
  public ImmutableList<Sighting.Builder> sightingBuilders() {
    ImmutableList.Builder<Sighting.Builder> sightingBuilders = ImmutableList.builder();
    for (Sighting sighting : sightings) {
      sightingBuilders.add(sighting.asBuilder());
    }
    return sightingBuilders.build();
  }
  
  /**
   * Returns the Sightings.  In general DO NOT mutate these sightings.
   */
  public ImmutableList<Sighting> getSightings() {
    return sightings;
  }
  
  /**
   * Returns the visit info associated with those sightings when they were removed.
   */
  public ImmutableMap<VisitInfoKey, VisitInfo> getVisitInfoMap() {
    return visitInfoMap;
  }

  public ImmutableList<Integer> getOriginalIndices() {
    return originalIndices;
  }
  
  public TreePath getParentPath() {
    return commonParent;
  }

  public Taxon getSourceTaxon() {
    return sourceTaxon;
  }
  
  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("sightings", sightings)
        .add("visitInfo", visitInfoMap)
        .add("parentPath", commonParent)
        .add("sourceTaxon",
            sourceTaxon == null ? "null" : TaxonUtils.getCommonName(sourceTaxon))
        .toString();
  }
}