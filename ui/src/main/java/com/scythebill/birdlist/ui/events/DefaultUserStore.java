/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.events;

import com.google.common.base.Strings;
import com.google.common.eventbus.EventBus;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.user.User;
import com.scythebill.birdlist.ui.events.UserPreferences.PerReportSet;
import com.scythebill.birdlist.ui.prefs.ReportSetPreference;

/**
 * Stores the current default user, and publishes updates to it.
 */
@Singleton
public class DefaultUserStore {
  private final EventBus eventBus;
  private final ReportSetPreference<PerReportSet> perReportSetPreferences;
  private volatile User user;

  @Inject
  public DefaultUserStore(
      EventBus eventBus,
      ReportSet reportSet,
      ReportSetPreference<UserPreferences.PerReportSet> perReportSetPreferences) {
    this.eventBus = eventBus;
    this.perReportSetPreferences = perReportSetPreferences;
    String userId = perReportSetPreferences.get().userId;
    if (!Strings.isNullOrEmpty(userId) && reportSet.getUserSet() != null) {
      try {
        user = reportSet.getUserSet().userById(userId);
      } catch (IllegalArgumentException e) {
        // Ignore
      }
    }
    
  }
  
  public void setUser(User user) {
    if (this.user != user) {
      this.user = user;
      eventBus.post(new DefaultUserChangedEvent(user));
      
      // But write the local taxonomy in the per-reportset preferences
      updatePerReportSetPreferences();
    }
  }

  void updatePerReportSetPreferences() {
    perReportSetPreferences.get().userId = user == null ? null : user.id();
    perReportSetPreferences.save(false /* don't mark dirty, a compromise to avoid constant dirtying */);
  }
  
  public User getUser() {
    return user;
  }
  
  /**
   * Notify that some unspecified change has happened to the user set.
   */
  public void userSetChanged() {
    // Just refire an event, which is a hack but clears up the chooser state.
    eventBus.post(new DefaultUserChangedEvent(user));
  }
}
