/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.events;

import javax.swing.JComponent;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import com.google.common.eventbus.EventBus;
import com.google.inject.Inject;

/**
 * Supports automatically registering and unregistering objects as their
 * container is added to or removed from the visible hierarchy.
 */
public class EventBusRegistrar {
  private EventBus eventBus;

  @Inject
  public EventBusRegistrar(EventBus eventBus) {
    this.eventBus = eventBus;
  }
  
  /**
   * Register a component that is itself an EventBus subscriber.
   */
  public void registerWhenInHierarchy(JComponent componentSubscriber) {
    registerWhenInHierarchy(componentSubscriber, componentSubscriber);
  }
  
  public void registerWhenInHierarchy(
      final Object subscriber, final JComponent component) {
    component.addAncestorListener(new AncestorListener() {      
      @Override
      public void ancestorRemoved(AncestorEvent event) {
        eventBus.unregister(subscriber);
      }
      
      @Override
      public void ancestorMoved(AncestorEvent event) {
      }
      
      @Override
      public void ancestorAdded(AncestorEvent event) {
        eventBus.register(subscriber);
      }
    });
  }
}
