package com.scythebill.birdlist.ui.events;

import javax.swing.SwingUtilities;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.SubscriberExceptionContext;
import com.google.common.eventbus.SubscriberExceptionHandler;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.scythebill.birdlist.ui.prefs.PreferencesManager;
import com.scythebill.birdlist.ui.prefs.ReportSetPreferencesModule;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * Module for bindings in this package 
 */
public class EventsModule extends AbstractModule {
  @Override
  protected void configure() {
    bind(TaxonomyStore.class);
    bind(DefaultUserStore.class);
    install(ReportSetPreferencesModule.forType(TaxonomyPreferences.PerReportSet.class));
    install(ReportSetPreferencesModule.forType(UserPreferences.PerReportSet.class));
  }

  @Provides @Singleton
  EventBus provideEventBus(Alerts alerts) {
    return new AsyncEventBus(
        SwingUtilities::invokeLater,
        new SubscriberExceptionHandler() {
          @Override
          public void handleException(Throwable exception, SubscriberExceptionContext context) {
            alerts.reportError(exception);
          }
        });
  }
  
  @Provides
  public TaxonomyPreferences provideTaxonomyPreferences(PreferencesManager manager) {
    return manager.getPreference(TaxonomyPreferences.class);
  }
}
