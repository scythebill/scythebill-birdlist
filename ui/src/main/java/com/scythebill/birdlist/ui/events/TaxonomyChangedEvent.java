package com.scythebill.birdlist.ui.events;

import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Event notifying listeners that the taxonomy has changed.
 */
public class TaxonomyChangedEvent {
  private final Taxonomy taxonomy;

  public TaxonomyChangedEvent(Taxonomy taxonomy) {
    this.taxonomy = taxonomy;
  }

  public Taxonomy getTaxonomy() {
    return taxonomy;
  }
}
