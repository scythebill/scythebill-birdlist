/*   
 * Copyright 2012 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.events;

import com.scythebill.birdlist.model.annotations.Preference;
import com.scythebill.birdlist.model.annotations.SerializeAsJson;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/** List of preferences used by the taxonomy store. */
public class TaxonomyPreferences {
  public enum TaxonomyType {
    clements,
    ioc
  }
  
  @Preference TaxonomyType taxonomyType = TaxonomyType.clements;
  
  @SerializeAsJson
  public static class PerReportSet {
    private String lastTaxonomyId;
    private String lastIocVersion;
    
    public void updateForLatestTaxonomy(Taxonomy taxonomy) {
      lastTaxonomyId = taxonomy.getId();
      if (taxonomy.isBuiltIn() && taxonomy.getId().startsWith("ioc")) {
        lastIocVersion = taxonomy.getId();
      }
    }

    public String lastTaxonomyId() {
      return lastTaxonomyId;
    }
    
    public String lastIocVersion() {
      return lastIocVersion;
    }

    /** Do not use this function, typically;  use updateForLastTaxonomy(). See caller sites for explanations. */
    public void setLastIocVersion(MappedTaxonomy iocTaxonomy) {
      lastIocVersion = iocTaxonomy.getId();
    }
  }
}
