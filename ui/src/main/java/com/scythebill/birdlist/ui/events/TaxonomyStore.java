/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.events;

import com.google.common.eventbus.EventBus;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.events.TaxonomyPreferences.TaxonomyType;
import com.scythebill.birdlist.ui.guice.Clements;
import com.scythebill.birdlist.ui.guice.IOC;
import com.scythebill.birdlist.ui.prefs.ReportSetPreference;

/**
 * Stores the current taxonomy, and publishes updates to it.
 */
@Singleton
public class TaxonomyStore {
  private final EventBus eventBus;
  private volatile Taxonomy taxonomy;
  private final TaxonomyPreferences taxonomyPreferences;
  private final Taxonomy clements;
  private final ReportSetPreference<TaxonomyPreferences.PerReportSet> perReportSetPreferences;
  private MappedTaxonomy ioc;
  private Taxonomy rememberedTaxonomy;

  @Inject
  public TaxonomyStore(
      EventBus eventBus,
      @Clements Taxonomy clements,
      @IOC MappedTaxonomy ioc,
      TaxonomyPreferences taxonomyPreferences,
      ReportSet reportSet,
      ReportSetPreference<TaxonomyPreferences.PerReportSet> perReportSetPreferences) {
    this.eventBus = eventBus;
    this.clements = clements;
    this.ioc = ioc;
    this.taxonomyPreferences = taxonomyPreferences;
    this.perReportSetPreferences = perReportSetPreferences;
    
    String taxonomyId = perReportSetPreferences.get().lastTaxonomyId();
    if (taxonomyId != null && reportSet.getExtendedTaxonomy(taxonomyId) != null) {
      this.taxonomy = reportSet.getExtendedTaxonomy(taxonomyId);
    } else {
      this.taxonomy = taxonomyPreferences.taxonomyType == TaxonomyType.clements
          ? clements : ioc;
    }
    // Force the reportset preferences to store the current state.  (If nothing else,
    // this forces the IOC state to be up-to-date)
    updatePerReportSetPreferences();
  }
  
  public void setTaxonomy(Taxonomy taxonomy) {
    if (this.taxonomy != taxonomy) {
      this.taxonomy = taxonomy;
      eventBus.post(new TaxonomyChangedEvent(taxonomy));
      // Only write clements vs. ioc globally
      if (taxonomy == clements || taxonomy == ioc) {
        taxonomyPreferences.taxonomyType = taxonomy == clements
            ? TaxonomyType.clements : TaxonomyType.ioc;
      }
      
      // But write the local taxonomy in the per-reportset preferences
      updatePerReportSetPreferences();
    }
  }

  void updatePerReportSetPreferences() {
    perReportSetPreferences.get().updateForLatestTaxonomy(this.taxonomy);
    perReportSetPreferences.save(false /* don't mark dirty, a compromise to avoid constant dirtying */);
  }
  
  public Taxonomy getTaxonomy() {
    return taxonomy;
  }

  /** Returns the Clements taxonomy, no matter what the current state is. */ 
  public Taxonomy getClements() {
    return clements;
  }

  /** Returns the IOC taxonomy, no matter what the current state is. */ 
  public Taxonomy getIoc() {
    return ioc;
  }

  /** Returns true if the current taxonomy is a bird taxonomy. */
  public boolean isBirdTaxonomy() {
    return taxonomy == clements || taxonomy == ioc;
  }

  /**
   * If the current taxonomy is not a Bird taxonomy, make it one!
   */
  public TaxonomyStore switchToPreferredBirdTaxonomy() {
    if (!isBirdTaxonomy()) {
      // Revert to either clements or IOC depending on what's preferred
      setTaxonomy(taxonomyPreferences.taxonomyType == TaxonomyType.clements
          ? clements : ioc);
    }
    return this;
  }

  /**
   * Remember the current taxonomy, which can be restored with a call to
   * {@link #restoreRememberedTaxonomy()}.
   */
  public void rememberCurrentTaxonomy() {
    rememberedTaxonomy = taxonomy;
  }
  
  /**
   * Restore the taxonomy as remembered by {@link #rememberCurrentTaxonomy()}.
   * 
   * @return null if the restoration is a no-op, or the new taxonomy if restoration did anything.
   */
  public Taxonomy restoreRememberedTaxonomy() {
    if (rememberedTaxonomy == null) {
      return null;
    } else if (taxonomy == rememberedTaxonomy) {
      rememberedTaxonomy = null;
      return null;
    }
    
    setTaxonomy(rememberedTaxonomy);
    rememberedTaxonomy = null;
    return taxonomy;
  }
  
  /**
   * Notify that some unspecified change has happened to extended taxonomies.
   */
  public void extendedTaxonomiesChanged() {
    // Just refire an event, which is a hack but clears up the chooser state.
    eventBus.post(new TaxonomyChangedEvent(taxonomy));
  }

  public void extendedTaxonomyRemoved() {
    // Ideally, would find the next-most-recently used taxonomy;  this is a shortcut. 
    switchToPreferredBirdTaxonomy();
  }
}
