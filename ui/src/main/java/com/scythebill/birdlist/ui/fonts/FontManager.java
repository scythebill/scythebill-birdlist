/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.fonts;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.table.JTableHeader;
import javax.swing.text.JTextComponent;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Class for managing fonts.
 */
@Singleton
public class FontManager {
  private static final String FONT_NAME = UIUtils.isMacOSYosemite()
      ? pickFont("Helvetica Neue", "Lucida Grande") // On Yosemite and *only* on that, use Helvetica Neue
      : pickFont(
    /* Stop using anything but Sans Serif, in general. Using a physical
       font means that only characters supported by the Font work.
       In practice, this means that, for example, CJK characters were
       not working on Windows, since Segoe UI doesn't support them.
       (Though Lucida Grande worked fine on MacOS.)
          // Windows 10
          "Segoe UI",
          // A MacOS font
          "Lucida Grande",
          // Some Linux fonts
          "Liberation Sans",
          "Roboto",
          "Oxygen",
          "Ubuntu",
          "Cantarell",
          "Garuda",
          "Sans Regular"
    */);

  private static final String pickFont(String... preferredFamilies) {
    Set<String> availableFamilies = Sets.newHashSet(
        GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames());
    for (String preferredFamily : preferredFamilies) {
      if (availableFamilies.contains(preferredFamily)) {
        return preferredFamily;
      }
    }
    
    // No preferred fonts font - use a generic Sans Serif font.
    return Font.SANS_SERIF;
  }
  
  /**
   * Listener for fonts updated.  Also automatically invoked on any components when explicitly
   * called by the font manager.
   */
  public interface FontsUpdatedListener {
    void fontsUpdated(FontManager fontManager);  
  }

  public static final String TEXT_SIZE_PROPERTY = "birdlist.textSize";
  public static final String PLAIN_LABEL = "birdlist.plainLabel";
  public static final String BUTTON_SIZE_PROPERTY = "birdlist.buttonSize";
  public static final Object BUTTON_TYPE = "birdlist.buttonType";
  
  public enum ButtonSize {
    SMALL,
    MEDIUM,
    MEDIUM_BOLD,
    LARGE
  }

  public enum TextSize {
    NORMAL,
    LARGE,
    VERY_LARGE
  }

  public enum ButtonType {
    MENU
  }
  
  private final static int[] fontSizes = new int[] {
    9,
    10,
    11,
    12,
    13,
    15,
    18
  };
  static final int DEFAULT_FONT_SIZE = 11;
  
  private final List<FontsUpdatedListener> listeners = Lists.newCopyOnWriteArrayList();
  private FontPreferences preferences;
  
  @Inject
  public FontManager(FontPreferences preferences) {
    this.preferences = preferences;
    // Bound the saved font size to the min and max allowed font sizes
    preferences.fontSize = Math.max(preferences.fontSize, fontSizes[0]);
    preferences.fontSize = Math.min(preferences.fontSize, fontSizes[fontSizes.length - 1]);
  }
  
  public void addListener(FontsUpdatedListener listener) {
    listeners.add(listener);
  }

  public void removeListener(FontsUpdatedListener listener) {
    listeners.remove(listener);
  }
  
  public void incrementFontSize() {
    boolean changed = false;
    synchronized (this) {
      for (int i = 0; i < fontSizes.length; i++) {
        if (fontSizes[i] > preferences.fontSize) {
          preferences.fontSize = fontSizes[i];
          changed = true;
          break;
        }
      }
    }
    if (changed) {
      notifyListeners();
    }
  }

  public void decrementFontSize() {
    boolean changed = false;
    synchronized (this) {
      for (int i = fontSizes.length - 1; i >=0; i--) {
        if (fontSizes[i] < preferences.fontSize) {
          preferences.fontSize = fontSizes[i];
          changed = true;
          break;
        }
      }
    }
    if (changed) {
      UIManager.put("TableHeader.font", getTableFont());
      UIManager.put("Table.font", getTableFont());
      notifyListeners();
    }
  }
  
  public Font getTextFont() {   
    return new Font(FONT_NAME, Font.PLAIN, preferences.fontSize);
  }
  
  public Font getLabelFont() {
    return new Font(FONT_NAME, Font.BOLD, preferences.fontSize);
  }

  public Font getLargeLabelFont() {
    return new Font(FONT_NAME, Font.BOLD, preferences.fontSize + 2);
  }

  public Font getVeryLargeLabelFont() {
    return new Font(FONT_NAME, Font.BOLD, preferences.fontSize + 5);
  }

  public Font getSmallButtonFont() {
    return new Font(FONT_NAME, Font.PLAIN, preferences.fontSize);
  }
  
  public Font getMediumButtonFont() {
    return new Font(FONT_NAME, Font.PLAIN, preferences.fontSize + 2);
  }

  
  public Font getMediumBoldButtonFont() {
    // Return the ordinary medium button font for now
    return getMediumButtonFont();
  }

  public Font getLargeButtonFont() {
    return new Font(FONT_NAME, Font.PLAIN, preferences.fontSize + 5).deriveFont(Font.BOLD);
  }

  public Font getLargeTextFont() {
    return new Font(FONT_NAME, Font.PLAIN, preferences.fontSize + 2);
  }

  public Font getVeryLargeTextFont() {
    return new Font(FONT_NAME, Font.PLAIN, preferences.fontSize + 5);
  }

  public int scale(int size) {
    return (int) ((((double) size) * preferences.fontSize) / DEFAULT_FONT_SIZE); 
    
  }
  
  public Dimension scale(Dimension dimension) {
    return new Dimension(scale(dimension.width), scale(dimension.height));
  }
  
  /**
   * Set the font of this component (and its children) to appropriate
   * defaults.
   */
  public void applyTo(Component component) {
    if (component instanceof FontsUpdatedListener) {
      ((FontsUpdatedListener) component).fontsUpdated(this);
    }
    TextSize textSize;
    if (component instanceof JComponent) {
      textSize = (TextSize) ((JComponent) component)
          .getClientProperty(TEXT_SIZE_PROPERTY);
      if (textSize == null) {
        textSize = TextSize.NORMAL;
      }
    } else {
      textSize = TextSize.NORMAL;
    }
    
    if (component instanceof JTextComponent ||
        component instanceof JComboBox ||
        component instanceof JCheckBox ||
        component instanceof JRadioButton) {
      switch (textSize) {
        case NORMAL:
          component.setFont(getTextFont());
          break;
        case LARGE:
          component.setFont(getLargeTextFont());
          break;
        case VERY_LARGE:
          component.setFont(getVeryLargeTextFont());
          break;
      }
    } else if (component instanceof JLabel) {
      if (Boolean.TRUE.equals(((JLabel) component).getClientProperty(PLAIN_LABEL))) {
        component.setFont(getTextFont());
      } else {
        switch (textSize) {
          case NORMAL:
            component.setFont(getLabelFont());
            break;
          case LARGE:
            component.setFont(getLargeLabelFont());
            break;
          case VERY_LARGE:
            component.setFont(getVeryLargeLabelFont());
            break;
        }
      }
    } else if (component instanceof JButton) {
      ButtonSize buttonSize = (ButtonSize) ((JButton) component)
          .getClientProperty(BUTTON_SIZE_PROPERTY);
      if (buttonSize == null) {
        buttonSize = ButtonSize.SMALL;
      }
      switch (buttonSize) {
        case SMALL:
          component.setFont(getSmallButtonFont());
          break;
        case MEDIUM:
          component.setFont(getMediumButtonFont());
          break;
        case MEDIUM_BOLD:
          component.setFont(getMediumBoldButtonFont());
          break;
        case LARGE:
          component.setFont(getLargeButtonFont());
          break;
      }
    } else if (component instanceof JTableHeader
        || component instanceof JTable
        || component instanceof JTree
        || component instanceof JList) {
      component.setFont(getTableFont());
    }
    
    if (component instanceof Container) {
      Container container = (Container) component; 
      if (container.getComponentCount() > 0) {
        for (Component child : container.getComponents()) {
          applyTo(child);
        }
      }
    }
  }
  
  public Font getTableFont() {
    return getTextFont();
  }

  boolean sizeIsAtMaximum() {
    return preferences.fontSize >= fontSizes[fontSizes.length - 1];
  }

  boolean sizeIsAtMinimum() {
    return preferences.fontSize <= fontSizes[0];
  }
  
  private void notifyListeners() {
    for (FontsUpdatedListener listener : listeners) {
      listener.fontsUpdated(this);
    }
  }

  public static String getFontName() {
    return FONT_NAME;
  }
}
