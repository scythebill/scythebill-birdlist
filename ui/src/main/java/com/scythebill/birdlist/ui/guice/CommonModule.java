/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.guice;

import java.awt.Image;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.ListeningScheduledExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.upgrades.UpgraderModule;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences;
import com.scythebill.birdlist.ui.app.FrameRegistry;
import com.scythebill.birdlist.ui.app.OtherFileLoaderRegistry;
import com.scythebill.birdlist.ui.fonts.FontModule;
import com.scythebill.birdlist.ui.imports.ImportPreferences;
import com.scythebill.birdlist.ui.panels.FilePreferences;
import com.scythebill.birdlist.ui.panels.reports.QueryPreferences;
import com.scythebill.birdlist.ui.panels.reports.UndescribedTaxa;
import com.scythebill.birdlist.ui.prefs.PreferencesManager;
import com.scythebill.birdlist.ui.uptodate.UpToDatePreferences;

/**
 * Binds common, global framework objects.
 */
public class CommonModule extends AbstractModule {
  private static final Logger logger = Logger.getLogger(CommonModule.class.getName()); 

  @Override protected void configure() {
    install(new FontModule());
    install(new UpgraderModule());
    ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(5);
    bind(ScheduledExecutorService.class).toInstance(scheduledExecutorService);
    bind(ExecutorService.class).to(ScheduledExecutorService.class);
    bind(ListeningExecutorService.class).to(ListeningScheduledExecutorService.class);
    // Bind Singletons that had better be global singletons, not per-window
    bind(PreferencesManager.class).in(Singleton.class);
    bind(FrameRegistry.class).in(Singleton.class);
    bind(OtherFileLoaderRegistry.class).in(Singleton.class);
    bind(Checklists.class).in(Singleton.class);
  }

  @Provides
  FilePreferences provideFilePreferences(PreferencesManager manager) {
    // Bound at global level so it can be used by FrameRegistry
    return manager.getPreference(FilePreferences.class);
  }

  @Provides
  public NamesPreferences provideNamesPreferences(PreferencesManager manager) {
    return manager.getPreference(NamesPreferences.class);
  }

  @Provides
  public UpToDatePreferences provideUpToDatePreferences(PreferencesManager manager) {
    return manager.getPreference(UpToDatePreferences.class);
  }

  @Provides
  public ImportPreferences provideImportPreferences(PreferencesManager manager) {
    return manager.getPreference(ImportPreferences.class);
  }

  @Provides
  public QueryPreferences provideQueryPreferences(PreferencesManager manager, UndescribedTaxa undescribedTaxa) {
    return manager.getPreference(QueryPreferences.class, () -> {
      return new QueryPreferences(undescribedTaxa);
    });
  }

  @Provides
  @Singleton
  Gson provideGson() {
    return new GsonBuilder().setPrettyPrinting().create(); 
  }
  
  
  @Provides
  @Singleton
  PredefinedLocations providePredefinedLocations() {
    return PredefinedLocations.loadAndParse();
  }
  
  @Provides
  @Singleton
  ListeningScheduledExecutorService provideListeningExecutorService(ScheduledExecutorService executorService) {
    return MoreExecutors.listeningDecorator(executorService);
  }


  @Provides
  @CurrentVersion
  String provideVersionInfo() {
    try {
      String version = Resources.toString(Resources.getResource("version.txt"), Charsets.UTF_8);
      return CharMatcher.whitespace().trimFrom(version);
    } catch (IOException e) {
      logger.log(Level.SEVERE, "Could not get version", e);
      return null;
    }
  }
  
  @Provides
  @Singleton
  @Scythebill
  Icon provideIcon(@Scythebill Image image) {
    Image scaled = image.getScaledInstance(64, 64, Image.SCALE_AREA_AVERAGING);
    return new ImageIcon(scaled);
  }
  
  @Provides
  @Singleton
  CloseableHttpClient provideHttpClient() {
    return HttpClients.custom()
        .disableCookieManagement()
        .setMaxConnPerRoute(3)
        .setMaxConnTotal(20)
        .setConnectionManager(new PoolingHttpClientConnectionManager())
        .build();
  }
  
  @Provides
  @Singleton
  @GoogleApiKey
  String provideGoogleApiKey() {
    try {
      String googleApiKey = Resources.toString(Resources.getResource("googleapikey.txt"), Charsets.UTF_8);
      return CharMatcher.whitespace().trimFrom(googleApiKey);
    } catch (IOException | IllegalArgumentException e) {
      logger.log(Level.SEVERE, "Could not get Google API Key; add googleapikey.txt to ui/src/main/resources", e);
      return null;
    }
  }

  @Provides
  @Singleton
  @EBirdApiKey
  String provideEBirdApiKey() {
    try {
      String googleApiKey = Resources.toString(Resources.getResource("ebirdapikey.txt"), Charsets.UTF_8);
      return CharMatcher.whitespace().trimFrom(googleApiKey);
    } catch (IOException | IllegalArgumentException e) {
      logger.log(Level.SEVERE, "Could not get eBird API Key; add ebirdapikey.txt to ui/src/main/resources", e);
      return null;
    }
  }

  @Provides
  @Singleton
  @FlickrApiKey
  String provideFlickrApiKey() {
    try {
      String googleApiKey = Resources.toString(Resources.getResource("flickrapikey.txt"), Charsets.UTF_8);
      return CharMatcher.whitespace().trimFrom(googleApiKey);
    } catch (IOException | IllegalArgumentException e) {
      logger.log(Level.SEVERE, "Could not get Flickr API Key; add flickrapikey.txt to ui/src/main/resources", e);
      return null;
    }
  }
}
