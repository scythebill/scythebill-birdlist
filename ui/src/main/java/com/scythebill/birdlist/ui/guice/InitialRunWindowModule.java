/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.guice;

import java.io.File;

import javax.swing.Action;
import javax.swing.JPanel;

import com.google.inject.AbstractModule;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.Scopes;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.sighting.edits.ChosenUsers;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.actions.FinishImportToNewReportSetAction;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.imports.FinishedImport;
import com.scythebill.birdlist.ui.imports.ImportMenuPanel;
import com.scythebill.birdlist.ui.panels.OpenOrNewListFrame;
import com.scythebill.birdlist.ui.panels.OpenOrNewListPanel;
import com.scythebill.birdlist.ui.prefs.ReportSetPreference;

/**
 * Module for bindings specific to the initial run of Scythebill.
 */
public class InitialRunWindowModule extends AbstractModule {

  @Override
  protected void configure() {
    bind(OpenOrNewListFrame.class).in(Scopes.SINGLETON);
    bind(NavigableFrame.class).to(OpenOrNewListFrame.class);
    bind(Key.get(JPanel.class, Names.named("main")))
        .to(OpenOrNewListPanel.class);
    bind(Key.get(JPanel.class, Names.named("importMenu")))
        .to(ImportMenuPanel.class);
    bind(Action.class).annotatedWith(FinishedImport.class)
        .to(FinishImportToNewReportSetAction.class);
    bindConstant().annotatedWith(InitialRun.class).to(true);
  }
  
  @Provides
  File provideFile() {
    // Ugggghhhh..  needed for Flickr imports during startup.  Return null
    // as a signal that there's no real place.
    return null;
  }
  
  @Provides @Singleton
  ReportSet provideReportSet(@Clements Taxonomy taxonomy) {
    return ReportSets.newReportSet(taxonomy);
  }

  @Provides @Singleton
  LocationSet providesLocations(ReportSet reportSet) {
    return reportSet.getLocations();
  }
  
  @Provides
  ReportSetPreference<ChosenUsers> provideChosenUsers() {
    return ReportSetPreference.staticValue(new ChosenUsers());
  }
}
