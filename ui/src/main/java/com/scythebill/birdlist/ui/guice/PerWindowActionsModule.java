/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.guice;

import javax.swing.Action;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.scythebill.birdlist.ui.actions.ActionBroker;
import com.scythebill.birdlist.ui.actions.ExportAction;
import com.scythebill.birdlist.ui.actions.FullExportAction;
import com.scythebill.birdlist.ui.actions.IdentifyRaritySightingsAction;
import com.scythebill.birdlist.ui.actions.IdentifySpeciesMissingFromChecklistsAction;
import com.scythebill.birdlist.ui.actions.ImportChecklistsAction;
import com.scythebill.birdlist.ui.actions.OpenContainingFolderAction;
import com.scythebill.birdlist.ui.actions.ReconcileAgainstChecklistsAction;
import com.scythebill.birdlist.ui.actions.ReturnAction;
import com.scythebill.birdlist.ui.actions.SaveACopyAsAction;
import com.scythebill.birdlist.ui.actions.SaveAction;
import com.scythebill.birdlist.ui.imports.FinishedImport;
import com.scythebill.birdlist.ui.imports.ImportAction;
import com.scythebill.birdlist.ui.panels.PreferencesAction;
import com.scythebill.birdlist.ui.taxonomy.ManageTaxonomiesAction;

/**
 * Bindings for per-window action instances.
 */
public class PerWindowActionsModule extends AbstractModule {

  @Override protected void configure() {
    bind(Action.class).annotatedWith(
        Names.named(ActionBroker.SAVE)).to(SaveAction.class);
    bind(Action.class).annotatedWith(
        Names.named(ActionBroker.SAVE_A_COPY_AS)).to(SaveACopyAsAction.class);
    bind(Action.class).annotatedWith(
        Names.named(ActionBroker.EXPORT)).to(ExportAction.class);
    bind(Action.class).annotatedWith(
        Names.named(ActionBroker.PREFERENCES)).to(PreferencesAction.class);
    bind(Action.class).annotatedWith(
        Names.named(ActionBroker.FULL_EXPORT)).to(FullExportAction.class);
    bind(Action.class).annotatedWith(
        Names.named(ActionBroker.IMPORT)).to(ImportAction.class);
    bind(Action.class).annotatedWith(
        Names.named(ActionBroker.MANAGE_TAXONOMIES)).to(ManageTaxonomiesAction.class);
    bind(Action.class).annotatedWith(
        Names.named(ActionBroker.IMPORT_CHECKLISTS)).to(ImportChecklistsAction.class);
    bind(Action.class).annotatedWith(
        Names.named(ActionBroker.OPEN_CONTAINING)).to(OpenContainingFolderAction.class);
    bind(Action.class).annotatedWith(
        Names.named(ActionBroker.IDENTIFY_CHECKLIST_ERRORS)).to(IdentifySpeciesMissingFromChecklistsAction.class);
    bind(Action.class).annotatedWith(
        Names.named(ActionBroker.IDENTIFY_CHECKLIST_RARITIES)).to(IdentifyRaritySightingsAction.class);
    bind(Action.class).annotatedWith(
        Names.named(ActionBroker.RECONCILE_AGAINST_CHECKLISTS)).to(ReconcileAgainstChecklistsAction.class);
        
    bind(Action.class).annotatedWith(FinishedImport.class).to(ReturnAction.class);
  }

}
