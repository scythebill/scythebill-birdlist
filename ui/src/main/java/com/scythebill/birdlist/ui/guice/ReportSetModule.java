/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.guice;

import java.io.File;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.edits.ChosenUsers;
import com.scythebill.birdlist.model.sighting.edits.RecentEdits;
import com.scythebill.birdlist.ui.app.ReportSetSaver;
import com.scythebill.birdlist.ui.panels.location.LocationBrowsePreferences;
import com.scythebill.birdlist.ui.panels.reports.StoredReportSetQueriesPreferences;
import com.scythebill.birdlist.ui.prefs.ReportPreferencesManager;
import com.scythebill.birdlist.ui.prefs.ReportSetPreferencesModule;

/**
 * Bindings for ReportSet instances and associated objects.
 */
public class ReportSetModule extends AbstractModule {
  public ReportSetModule() {
  }

  @Override protected void configure() {
    bind(ReportSetSaver.class).in(Singleton.class);
    bind(ReportPreferencesManager.class).in(Singleton.class);
    bindConstant().annotatedWith(InitialRun.class).to(false);
    install(ReportSetPreferencesModule.forType(RecentEdits.class));
    install(ReportSetPreferencesModule.forType(ChosenUsers.class));
    install(ReportSetPreferencesModule.forType(StoredReportSetQueriesPreferences.class));
    install(ReportSetPreferencesModule.forType(LocationBrowsePreferences.class));
  }
  
  @Provides
  // TODO: bad idea reserving the global "File" binding? 
  public File getReportSetFile(@Named("com.adamwiner.birdlist.ui.reportFile") String filename) {
    return new File(filename);  
  }
  
  @Provides @Singleton
  public LocationSet providesLocations(ReportSet reportSet) {
    return reportSet.getLocations();
  }
}
