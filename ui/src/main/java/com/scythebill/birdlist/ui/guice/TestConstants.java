/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.guice;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Bindings used for bootstrapping test instances.
 */
public class TestConstants extends AbstractModule {
  static private final Logger logger = Logger.getLogger(TestConstants.class.getName());

  @Override
  protected void configure() {
    try {
      Properties properties = new Properties();
      InputStream propertyStream = getClass().getResourceAsStream("test.properties");
      properties.load(propertyStream);
      Names.bindProperties(binder(), properties);
    } catch (IOException ioe) {
      logger.log(Level.SEVERE, "Couldn't load test properties", ioe);
    }
  }
}
