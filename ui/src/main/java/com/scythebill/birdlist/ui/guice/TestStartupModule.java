/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.guice;

import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import com.google.common.io.Files;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.Progress;
import com.scythebill.birdlist.ui.app.Startup;
import com.scythebill.birdlist.ui.app.StartupView;
import com.scythebill.birdlist.ui.app.TaxonomyLoader;
import com.scythebill.birdlist.ui.app.TaxonomyReference;

/**
 * Module used for bootstrapping test classes.
 */
public class TestStartupModule extends AbstractModule
{
  private static final Logger logger = Logger.getLogger(TestStartupModule.class.getName());
    
  @Override
  protected void configure() {
    // Dummy startup view, to avoid showing progress UI while bootstrapping a small test
    bind(StartupView.class).toInstance(new StartupView() {
      @Override public void addProgress(Progress progress, double fraction) {
      }

      @Override public void start() {
      }

      @Override public void stop() {
      }      
    });
  }
  
  @Provides @Singleton
  TaxonomyLoader providesTaxonomyLoader(
      @Named("com.adamwiner.birdlist.ui.taxonFile") String filename) {
    File file = new File(filename);
    TaxonomyReference reference = new TaxonomyReference(
        Files.asByteSource(file), file.length());
    return new TaxonomyLoader(reference);
  }
  
  @Singleton
  public Taxonomy providesTaxonomy(Startup startup) {
    logger.info("Reading taxonomy");
    try {
      return startup.getTaxonomyFuture().get();
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    } catch (ExecutionException e) {
      throw new RuntimeException(e);
    }
  }
}