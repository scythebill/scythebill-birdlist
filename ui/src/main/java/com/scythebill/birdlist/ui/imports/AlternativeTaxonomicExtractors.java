/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

import com.google.common.base.Strings;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;

/**
 * Class for hardcoding taxonomic mappings for certain importers. 
 */
class AlternativeTaxonomicExtractors<T> {
  private RowExtractor<T, String> speciesIn;
  private RowExtractor<T, String> subspeciesIn;
  private RowExtractor<T, String> commonExtractor;
  private Multimap<String, Taxon> splits = ArrayListMultimap.create();

  public AlternativeTaxonomicExtractors(
      RowExtractor<T, String> speciesExtractor,
      RowExtractor<T, String> subspeciesExtractor,
      RowExtractor<T, String> commonExtractor) {
    this.speciesIn = speciesExtractor;
    this.subspeciesIn = subspeciesExtractor;
    this.commonExtractor = commonExtractor;
  }
  
  public AlternativeTaxonomicExtractors<T> splitting(
      String species,
      Taxon... taxa) {
    splits.putAll(species, Arrays.asList(taxa));
    return this;
  }
  
  public RowExtractor<T, String> common() {
    return line -> {
      String species = speciesIn.extract(line);
      // If this species is explicitly handled, null out the common name
      return splits.containsKey(species)
          ? null
          : commonExtractor.extract(line);
    };
  }
  
  public RowExtractor<T, String> species() {
    return line -> {
      String extracted = speciesIn.extract(line);
      if (!splits.containsKey(extracted)) {
        return extracted;
      }
      
      Collection<Taxon> splitTaxa = splits.get(extracted);
      // Only one mapping!  Short-circuit.
      if (splitTaxa.size() == 1) {
        return TaxonUtils.getFullName(splitTaxa.iterator().next());
      }
      
      // If subspecies is set, look for that subspecies in any of the taxa
      String subspecies = subspeciesIn.extract(line);
      if (!Strings.isNullOrEmpty(subspecies)) {
        Optional<Taxon> taxon = splitTaxa.stream()
            .filter(t -> hasOrIsSubspecies(t, subspecies))
            .findAny();
        if (taxon.isPresent()) {
          return TaxonUtils.getFullName(taxon.get());
        }
      }
      
      // Otherwise, return a "spuh"
      return SightingTaxons.newSpResolved(ImmutableSet.copyOf(splitTaxa)).getFullName();
    };
  }

  private boolean hasOrIsSubspecies(Taxon taxon, String subspecies) {
    boolean[] found = new boolean[1];
    TaxonUtils.visitTaxa(taxon, t -> {
      if (t.getName().equals(subspecies)) {
        found[0] = true;
        return false;
      }
      return true;
    });
    return found[0];
  }
  
  
}
