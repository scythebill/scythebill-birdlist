/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.IOException;

import com.google.common.base.Optional;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/** An importer that will always fail (and report an error. */
final class AlwaysFailImporter extends CsvSightingsImporter {
  private final String errorMessage;
  private final File file;

  AlwaysFailImporter(ReportSet reportSet, Taxonomy taxonomy, Checklists checklists,
      PredefinedLocations predefinedLocations, File file, String errorMessage) {
    super(reportSet, taxonomy, checklists, predefinedLocations, file, file);
    this.file = file;
    this.errorMessage = errorMessage;
  }

  @Override
  public Optional<String> initialCheck() throws IOException {
    return Optional.of(
        Messages.getFormattedMessage(Name.DOES_NOT_APPEAR_TO_BE_AN_ACTUAL_EBIRD_FILE,
            file.getName(), errorMessage));
  }

  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    throw new UnsupportedOperationException();
  }

  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    throw new UnsupportedOperationException();
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    throw new UnsupportedOperationException();
  }

  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    throw new UnsupportedOperationException();
  }
}