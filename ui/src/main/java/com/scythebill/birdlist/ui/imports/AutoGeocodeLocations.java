/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

import org.apache.commons.text.similarity.LevenshteinDistance;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.SettableFuture;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.ClosestLocations;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.ui.actions.locationapi.GeocoderResults;
import com.scythebill.birdlist.ui.actions.locationapi.ebird.EBirdGeocoder;
import com.scythebill.birdlist.ui.imports.ParsedLocationIds.ToBeDecided;

/**
 * For imports, automatically resolves locations (or at least attempts to) locations that have a
 * lat/long and a location name.
 */
class AutoGeocodeLocations {
  private static final double KM_CLOSE_ENOUGH_FOR_EXISTING_LOCATION = 1.0;
  private static final Logger logger = Logger.getLogger(AutoGeocodeLocations.class.getName());
  private static final double KM_CLOSE_ENOUGH_FOR_EXACT_NAME_MATCH = 2.0;
  private static final double KM_CLOSE_ENOUGH_FOR_LESS_PRECISE_NAME_MATCH = 0.5;
  /** If we're less than this distance, assume that we've got the right geocoded parent. */
  private static final double KM_CLOSE_ENOUGH_TO_TRUST_PARENT = 10.0;
  /** If we're less than this distance, assume that we've got the right geocoded parent. */
  private static final double KM_CLOSE_ENOUGH_TO_PREFILL_PARENT = 50.0;
  private final EBirdGeocoder eBirdGeocoder;

  @Inject
  AutoGeocodeLocations(EBirdGeocoder eBirdGeocoder) {
    this.eBirdGeocoder = eBirdGeocoder;    
  }
  
  public ListenableFuture<Void> autoGeocodeLocations(ParsedLocationIds parsedLocationIds, ReportSet reportSet) {
    ImmutableMap<Object, ToBeDecided> toBeResolvedNames = parsedLocationIds.toBeResolvedNames();    
    
    List<ListenableFuture<Void>> futures = new ArrayList<>();
    for (Map.Entry<Object, ToBeDecided> entry : toBeResolvedNames.entrySet()) {
      ToBeDecided toBeDecided = entry.getValue();
      Object key = entry.getKey();
      
      // Ignore anything that doesn't have both a lat/long and a name 
      if (toBeDecided.latLong != null && toBeDecided.name != null && !toBeDecided.name.isEmpty()) {
        // First, see if there's an already-entered name that matches. 
        Location nearestLocation = ClosestLocations.nearestLocation(toBeDecided.latLong, reportSet.getLocations());
        if (nearestLocation != null) {
          double distanceToNearestLocation = nearestLocation.getLatLong().get().kmDistance(toBeDecided.latLong);
          // If the location is quite close, and the name is about the same, then just call it the same and don't
          // ask for re-resolution
          if (distanceToNearestLocation < KM_CLOSE_ENOUGH_FOR_EXISTING_LOCATION
              // TODO: include getModelName() too?  But this is not relevant today, as display name
              // and model name should be the same for any location with a lat/long.
              && LevenshteinDistance.getDefaultInstance().apply(nearestLocation.getDisplayName(), toBeDecided.name) <= 2) {
            parsedLocationIds.put(key, nearestLocation.getId());
            continue;
          }

          if (distanceToNearestLocation < KM_CLOSE_ENOUGH_TO_TRUST_PARENT) {
            Location nearestBuiltInParent = Locations.getBuiltInAncestor(nearestLocation);
            fullyResolveLocationUsingGeocodedParent(parsedLocationIds, key, nearestBuiltInParent,
                toBeDecided, reportSet.getLocations());
            continue;
          }
        }
        
        // TODO: check if nearestLocation is non-null, and distance is < N km, then at least pick a state or country?
        
        // Nothing in our location database that was good enough.  Hit up eBird.
        // Visiting Google is a bit more problematic;  that's one lookup per lat/long, whereas with eBird, we
        // can lookup (and cache) a lat/long "square" 
        // Use one-degree squares (centered on the nearest half degree of lat/long).
        // These are at most ~110km on a side.  Technically, 80km would be sufficient,
        // since we just need 110km/sqrt(2).
        double searchCenterLat = Math.round(toBeDecided.latLong.latitudeAsDouble());  
        double searchCenterLong = Math.round(toBeDecided.latLong.longitudeAsDouble());  
        ListenableFuture<ImmutableList<GeocoderResults>> resultsFutures =
            eBirdGeocoder.reverseGeocode(
                reportSet.getLocations(), toBeDecided.latLong, LatLongCoordinates.withLatAndLong(searchCenterLat, searchCenterLong), 100 /* km */);
        SettableFuture<Void> done = SettableFuture.create();
        resultsFutures.addListener(() -> {
          geocodeResultsAvailable(parsedLocationIds, reportSet, toBeDecided, key, resultsFutures,
              done);
        }, MoreExecutors.directExecutor());
        
        futures.add(done);
      }
    }
    
    if (futures.isEmpty()) {
      return Futures.immediateFuture(null);
    } else {
      return Futures.whenAllComplete(futures).call(() -> (Void) null, MoreExecutors.directExecutor());
    }
  }

  /** Run when geocode results are available. */
  private void geocodeResultsAvailable(ParsedLocationIds parsedLocationIds, ReportSet reportSet,
      ToBeDecided toBeDecided, Object key,
      ListenableFuture<ImmutableList<GeocoderResults>> resultsFutures, SettableFuture<Void> done) {
    try {
      ImmutableList<GeocoderResults> results = resultsFutures.get();
      if (!results.isEmpty()) {
        // Found some results.  First, see if any locations have an exact location name, and are close.
        for (GeocoderResults result : results) {
          if (result.preferredParent().isPresent()
              && result.name().equals(toBeDecided.name)
              && result.coordinates().kmDistance(toBeDecided.latLong) < KM_CLOSE_ENOUGH_FOR_EXACT_NAME_MATCH) {
            SwingUtilities.invokeLater(() -> {
              // resolve the location on the UI thread;  it's not threadsafe
              fullyResolveLocationToGeocoderResult(parsedLocationIds, key, result, toBeDecided, reportSet.getLocations());
              done.set(null);
            });
            return;
          }
        }

        // Now, see if the closest location is at least similar (and is very close)
        GeocoderResults closestResult = results.get(0);
        // If there's a preferred parent...
        if (closestResult.preferredParent().isPresent()) {
          double distanceInKm = closestResult.coordinates().kmDistance(toBeDecided.latLong);
          
          // If the distance is very close, and the names are similar, then again, use that location.
          if (distanceInKm < KM_CLOSE_ENOUGH_FOR_LESS_PRECISE_NAME_MATCH
              && (closestResult.name().contains(toBeDecided.name)
                  || toBeDecided.name.contains(closestResult.name()))) {
            SwingUtilities.invokeLater(() -> {
              // resolve the location on the UI thread; it's not threadsafe
              fullyResolveLocationToGeocoderResult(parsedLocationIds, key, closestResult,
                  toBeDecided, reportSet.getLocations());
              done.set(null);
            });
            return;
          }

          // If the distance is fairly close, then just take the name given by the user, stick it in the geocoded parent, and be done 
          if (distanceInKm < KM_CLOSE_ENOUGH_TO_TRUST_PARENT) {
            SwingUtilities.invokeLater(() -> {
              // resolve the location on the UI thread; it's not threadsafe
              fullyResolveLocationUsingGeocodedParent(parsedLocationIds, key,
                  closestResult.preferredParent().get(), toBeDecided, reportSet.getLocations());
              done.set(null);
            });
            return;
          }

          // If the distance is at all close, then at least prefill the parent 
          if (distanceInKm < KM_CLOSE_ENOUGH_TO_PREFILL_PARENT) {
            SwingUtilities.invokeLater(() -> {
              // resolve the location on the UI thread; it's not threadsafe
              prefillParentLocation(parsedLocationIds, key, closestResult,
                  toBeDecided, reportSet.getLocations());
              done.set(null);
            });
            return;
          }
        }
      }
    } catch (Exception e) {
      logger.log(Level.WARNING, "Failed to reverse geocode", e);
    }
    
    done.set(null);
  }

  /**
   * Indicates that an unresolved location should use the preferred parent, *but* the name of "toBeDecided" 
   */
  private void fullyResolveLocationUsingGeocodedParent(
      ParsedLocationIds parsedLocationIds,
      Object key,
      Location preferredParent,
      ToBeDecided toBeDecided,
      LocationSet locationSet) {
    // Normalize the location!  We geocode a bunch in parallel.  Each one of these might trigger the
    // same "preferred parent" to be created, but only the first can legitimately be added.
    Location parent = Locations.normalizeLocation(preferredParent, locationSet);
    Location existingChild = parent.getContent(toBeDecided.name);
    if (existingChild != null) {
      // TODO: add the lat/long if none is present?
      parsedLocationIds.put(key, existingChild.getId());
    } else {
      Location child = new Location.Builder()
          .setParent(parent)
          .setName(toBeDecided.name)
          .setLatLong(toBeDecided.latLong)
          .build();
      locationSet.ensureAdded(child);
      parsedLocationIds.put(key, child.getId());
    }
  }

  /**
   * Indicates that an unresolved location should be entirely allocated to a given geocoded result. 
   */
  private void fullyResolveLocationToGeocoderResult(
      ParsedLocationIds parsedLocationIds,
      Object key,
      GeocoderResults result,
      ToBeDecided toBeDecided,
      LocationSet locationSet) {
    // Normalize the location!  We geocode a bunch in parallel.  Each one of these might trigger the
    // same "preferred parent" to be created, but only the first can legitimately be added.
    Location parent = Locations.normalizeLocation(result.preferredParent().get(), locationSet);
    Location existingChild = parent.getContent(result.name());
    if (existingChild != null) {
      // TODO: add the lat/long if none is present?
      parsedLocationIds.put(key, existingChild.getId());
    } else {
      Location child = new Location.Builder()
          .setParent(parent)
          .setName(result.name())
          .setLatLong(toBeDecided.latLong)
          .build();
      locationSet.ensureAdded(child);
      parsedLocationIds.put(key, child.getId());
    }
  }

  /**
   * Indicates that an unresolved location should use the preferred parent, *but* the name of "toBeDecided" 
   */
  private void prefillParentLocation(
      ParsedLocationIds parsedLocationIds,
      Object key,
      GeocoderResults result,
      ToBeDecided toBeDecided,
      LocationSet locationSet) {
    // Normalize the location!  We geocode a bunch in parallel.  Each one of these might trigger the
    // same "preferred parent" to be created, but only the first can legitimately be added.
    Location parent = Locations.normalizeLocation(result.preferredParent().get(), locationSet);
    Location existingChild = parent.getContent(toBeDecided.name);
    if (existingChild != null) {
      // If we found an existing child *now*, then just use it.  
      // TODO: add the lat/long if none is present?
      parsedLocationIds.put(key, existingChild.getId());
    } else {
      // Otherwise, just mark "preferredParent" down in ToBeDecided to streamline things
      locationSet.ensureAdded(parent);
      toBeDecided.preferredParent = parent;
    }
  }

}
