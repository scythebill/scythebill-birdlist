/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import com.google.common.io.LineReader;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.checklist.TransposedChecklistSynthesizer;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Imports sightings from Avisys files.
 */
public class AvisysImporter extends CsvSightingsImporter {
  private static final int DESCRIPTION_FIELD_INDEX = 6;

  private final LineExtractor<String> idExtractor = LineExtractors.joined(
      Joiner.on('|').useForNull(""),
      LineExtractors.stringFromIndex(3),
      LineExtractors.stringFromIndex(11),
      LineExtractors.stringFromIndex(12),
      LineExtractors.stringFromIndex(13),
      LineExtractors.stringFromIndex(14),
      LineExtractors.stringFromIndex(15));

  private final LineExtractor<String> taxonomyIdExtractor = LineExtractors.joined(
      Joiner.on('|').useForNull(""),
      LineExtractors.stringFromIndex(0),
      LineExtractors.stringFromIndex(1),
      LineExtractors.stringFromIndex(2));

  private final TransposedChecklistSynthesizer transposedChecklistSynthesizer;
  private final Checklists checklists;
  private File fieldNotesFile;
  
  public AvisysImporter(ReportSet reportSet, Taxonomy taxonomy, Checklists checklists,
      TransposedChecklistSynthesizer transposedChecklistSynthesizer, PredefinedLocations predefinedLocations, File file) {
    super(reportSet, taxonomy, checklists, predefinedLocations, file, file);
    this.checklists = checklists;
    this.transposedChecklistSynthesizer = transposedChecklistSynthesizer;
  }

  public void attachFieldNotes(File file) {
    fieldNotesFile = file;
  }
  
  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return taxonomyIdExtractor;
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    return new FieldTaxonImporter<>(taxonomy,
        LineExtractors.stringFromIndex(0),
        LineExtractors.joined(Joiner.on(' ').useForNull(""),
            LineExtractors.stringFromIndex(1),
            LineExtractors.stringFromIndex(2)),
        null);
  }
       
  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    ImportLines lines = importLines(locationsFile);
    try {
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }
        
        String id = idExtractor.extract(line);
        if (locationIds.hasBeenParsed(id)) {
          continue;
        }

        ImportedLocation imported = new ImportedLocation();
        imported.state = safeGet(line, 14);
        imported.stateCode = Strings.emptyToNull(line[7]);
        imported.county = safeGet(line, 13);
        String place = Strings.emptyToNull(line[3]);
        // Find the place name;  if it differs from the country (col. 15), add it.
        if (place != null
            && !place.equals(safeGet(line, 15))) {
          imported.locationNames.add(place);
        }
        // Then add the "site" as a further detail, if available
        imported.locationNames.add(safeGet(line, 11));
        imported.city = safeGet(line, 12);
        String countryCode = Strings.emptyToNull(line[8]);
        if (countryCode != null) {
          BirdbaseAndAvisysLocations.updateImportedLocation(imported, countryCode);
        }
        // "Country" field.  Note that this is often not really a country, but instead is
        // a biogeographical region like "Java and Bali"
        if (imported.country == null) {
          imported.country = safeGet(line, 15);
          if ("ID".equals(countryCode)) {
            if ("Java and Bali".equals(place)
                || "Java and Bali".equals(imported.country)) {
              // Make the dubious assumption that Java and Bali applies to Java, not Bali,
              // because those are two different states.
              imported.state = "Jawa";
              imported.stateCode = "JW";
            }
          }
        }
        // Tweak an Avisys oddity with treatment of Hawaii
        if ("US-Hawaii".equalsIgnoreCase(imported.country)) {
          imported.country = null;
          imported.countryCode = "US";
        }
        if (imported.region == null) {
          imported.region = safeGet(line, 16);
        }

        String locationId = imported.addToLocationSet(reportSet, locations, locationShortcuts, predefinedLocations);
        locationIds.put(id, locationId);
      }
    } catch (IOException | RuntimeException e) {
      throw new ImportException(
          Messages.getFormattedMessage(Name.FAILED_IMPORT_ON_LINE_FORMAT, lines.lineNumber()),
          e);
    } finally {
      lines.close();
    }
  }

  /** Get a field from the line, when unsure if this index might be off the end of the line. */
  private static String safeGet(String[] line, int index) {
    if (index >= line.length) {
      return null;
    }
    
    return Strings.emptyToNull(line[index]);
  }
  
  static class ExtractAvisysFlag implements LineExtractor<Boolean> {
    private final LineExtractor<String> descriptionField;
    private final String flagName;

    public ExtractAvisysFlag(
        LineExtractor<String> descriptionField,
        String flag) {
      this.descriptionField = descriptionField;
      this.flagName = "/" +  flag;
      
    }

    @Override
    public Boolean extract(String[] line) {
      String extract = descriptionField.extract(line);
      if (extract == null) {
        return false;
      }
      
      return extract.contains(flagName);
    }    
  }
  
  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
        
    mappers.add(new DateFromStringFieldMapper<>(
        // Two *or* four-digit imports
        "MM/dd/yy",
        LineExtractors.stringFromIndex(4)));    
    mappers.add(new CountFieldMapper<>(LineExtractors.stringFromIndex(5)));
    LineExtractor<String> descriptionField = LineExtractors.stringFromIndex(DESCRIPTION_FIELD_INDEX);
    mappers.add(new DescriptionFieldMapper<>(descriptionField));
    mappers.add(new FemaleFieldMapper<>(new ExtractAvisysFlag(descriptionField, "f")));
    mappers.add(new MaleFieldMapper<>(new ExtractAvisysFlag(descriptionField, "m")));
    mappers.add(new ImmatureFieldMapper<>(new ExtractAvisysFlag(descriptionField, "i")));
    mappers.add(new HeardOnlyFieldMapper<>(new ExtractAvisysFlag(descriptionField, "h")));
    mappers.add(new PhotographedFieldMapper<>(new ExtractAvisysFlag(descriptionField, "p")));
    return new ComputedMappings<>(
        new TaxonFieldMapper(taxonomyIdExtractor),
        new LocationMapper(idExtractor),
        mappers);
  }


  @Override
  protected ImportLines importLines(File file) throws IOException {
    // Don't trust Avisys to perform proper in-line escaping
    final ImportLines mainFile = CsvImportLines.fromFileNoMulti(file, getCharset());
    if (fieldNotesFile == null) {
      return mainFile;
    }
    
    final BufferedReader fieldNotesReader = Files.newReader(fieldNotesFile, getCharset());
    final LineReader fieldNotesLines = new LineReader(fieldNotesReader);
    
    // Create a merged ImportLines that incorporates the field notes.
    return new ImportLines() {
      private boolean foundFirstLine = false;
      private String pushBackFieldNotesLine = null;
      
      @Override
      public void close() throws IOException {
        // Close both
        try {
          mainFile.close();
        } finally {
          fieldNotesReader.close();
        }
      }
      
      @Override
      public ImportLines withoutTrimming() {
        mainFile.withoutTrimming();
        return this;
      }
      
      @Override
      public int lineNumber() {
        // Not exactly right;  should acount for read-ahead.
        return mainFile.lineNumber();
      }

      @Override
      public String[] nextLine() throws IOException {
        String[] nextLine = mainFile.nextLine();
        // Read the next line - if empty, we're done
        if (nextLine == null || nextLine.length == 0) {
          return nextLine;
        }
        
        // Grab the common name out of the imported field - to make sure everything lines up.
        String commonName = nextLine[0];
        String speciesLine;
        // For the first species, skip entries until we find a line that matches the first common name
        // (effectively skipping the header entries like "25755 records", etc.)
        if (foundFirstLine) {
          speciesLine = nextFieldNotesLine();
        } else {
          do {
            speciesLine = nextFieldNotesLine();
            if (speciesLine == null) {
              break;
            }
            foundFirstLine = speciesLine.startsWith(commonName);
          } while (!foundFirstLine);
        }
        
        // Uh-oh:  did not find a line with this common name
        if (speciesLine == null || !speciesLine.startsWith(commonName)) {
          throw new ImportException(
              Messages.getMessage(Name.AVISYS_FIELD_NOTES_FILE_DOES_NOT_MATCH));
        }
        
        // Next line should have metadata (like count), and start with two spaces and an equals sign
        String metadataNotesLine = nextFieldNotesLine();
        if (metadataNotesLine == null || !metadataNotesLine.startsWith("  ")) {
          throw new ImportException(
              Messages.getMessage(Name.AVISYS_FIELD_NOTES_FILE_NOT_EXPECTED_FORM));
        }
        
        // Now, look for field notes lines prior to the next species.  Field note lines
        // are indented by four spaces.
        while (true) {
          String maybeFieldNotesLine = nextFieldNotesLine();
          if (maybeFieldNotesLine != null && maybeFieldNotesLine.startsWith("    ")) {
            String fieldNotesLine = maybeFieldNotesLine.substring(4);
            // Append to the description field, if one exists.
            if (nextLine[DESCRIPTION_FIELD_INDEX] == null
                || nextLine[DESCRIPTION_FIELD_INDEX].isEmpty()) {
              nextLine[DESCRIPTION_FIELD_INDEX] = fieldNotesLine;
            } else {
              nextLine[DESCRIPTION_FIELD_INDEX] += "\n" + fieldNotesLine;
            }
          } else {
            // Went too far - push this line back to the 1-line buffer, and stop
            pushBackFieldNotesLine = maybeFieldNotesLine;
            break;
          }
        }
        
        return nextLine;
      }
      
      /** Grab the next line of field notes - possibly reading from a pushed-back buffer line */
      private String nextFieldNotesLine() throws IOException {
        if (pushBackFieldNotesLine != null) {
          String nextLine = pushBackFieldNotesLine;
          pushBackFieldNotesLine = null;
          return nextLine;
        }
        
        return fieldNotesLines.readLine();
      }
    };
  }

  @Override
  protected Checklist getBuiltInChecklist(Location location) {
    String locationCode = checklists.getLocationWithNearestBuiltInChecklist(location);
    if (locationCode != null) {
      ImmutableCollection<String> locations = BirdbaseAndAvisysLocations.getMergedLocations(locationCode);
      if (locations != null && !locations.isEmpty()) {
        return transposedChecklistSynthesizer.synthesizeChecklistInternal(reportSet, locations);
      }
    }
    return super.getBuiltInChecklist(location);
  }

  @Override
  protected Charset getCharset() {
    // Don't know for sure that this is correct... but I'm hard-pressed to imagine Avisys using anything else.
    return Charsets.ISO_8859_1;
  }
}
