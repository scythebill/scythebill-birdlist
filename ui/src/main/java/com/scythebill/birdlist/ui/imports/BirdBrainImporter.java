/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Locale;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Imports sightings from BirdBrain CSV files.
 */
public class BirdBrainImporter extends CsvSightingsImporter {
  private final LineExtractor<String> locationIdExtractor = LineExtractors.joined(
      Joiner.on('|').useForNull(""),
      LineExtractors.stringFromIndex(5),
      LineExtractors.stringFromIndex(6),
      LineExtractors.stringFromIndex(7),
      LineExtractors.stringFromIndex(8),
      LineExtractors.stringFromIndex(9));

  private final LineExtractor<String> taxonomyIdExtractor = LineExtractors.stringFromIndex(1);
  
  public BirdBrainImporter(ReportSet reportSet, Taxonomy taxonomy, Checklists checklists, PredefinedLocations predefinedLocations, File file) {
    super(reportSet, taxonomy, checklists, predefinedLocations, file, file);
  }
  
  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return taxonomyIdExtractor;
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    TaxonResolver taxonResolver = new TaxonResolver(taxonomy);
    return new FieldTaxonImporter<>(taxonResolver,
        LineExtractors.stringFromIndex(1),
        LineExtractors.stringFromIndex(2));
  }

  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    ImportLines lines = CsvImportLines.fromFile(locationsFile, getCharset());
    try {
      computeMappings(lines);
      
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }
        
        String id = locationIdExtractor.extract(line);
        if (locationIds.hasBeenParsed(id)) {
          continue;
        }
        ImportedLocation imported = new ImportedLocation();
        imported.country = Strings.emptyToNull(line[5]);
        if (line[6] != null && line[6].length() == 2) {
          imported.stateCode = line[6];
        } else {
          imported.state = Strings.emptyToNull(line[6]);
        }
        
        imported.county = Strings.emptyToNull(line[7]);
        imported.city = Strings.emptyToNull(line[8]);
        if (!Strings.isNullOrEmpty(line[9])) {
          imported.locationNames.add(line[9]);
        }

        // Habitat
        imported.description = Strings.emptyToNull(line[10]);
        
        // Lat-long together
        if (line.length > 16 && !Strings.isNullOrEmpty(line[16])) {
          String latLong = line[16];
          int commaIndex = latLong.indexOf(',');
          if (commaIndex > 0) {
            imported.latitude = latLong.substring(0, commaIndex).trim();
            imported.longitude = latLong.substring(commaIndex + 1).trim();                
          }
        }
        
        String locationId = imported.addToLocationSet(reportSet, locations, locationShortcuts, predefinedLocations);
        locationIds.put(id, locationId);
      }
    } catch (IOException | RuntimeException e) {
      throw new ImportException(
          Messages.getFormattedMessage(Name.FAILED_IMPORT_ON_LINE_FORMAT, lines.lineNumber()),
          e);
    } finally {
      lines.close();
    }
  }
  
  private static boolean isHeardOnly(String tag) {
    if (Strings.isNullOrEmpty(tag)) {
      return false;
    }
    
    return tag.toLowerCase().equals("heard only");
  }
  
  static class DescriptionExtractor implements LineExtractor<String> {
    @Override
    public String extract(String[] line) {
      StringBuilder builder = new StringBuilder();
      
      
      String tag = entry(line, 11);
      if (!Strings.isNullOrEmpty(tag) && !isHeardOnly(tag)) {
        append(builder, "/" + tag, ' ');
      }
      String flag1 = entry(line, 12);
      if ("Yes".equals(flag1)) {
        append(builder, "/f1", ' ');
      }
      String flag2 = entry(line, 13);
      if ("Yes".equals(flag2)) {
        append(builder, "/f2", ' ');
      }
      
      String notes = entry(line, 14);
      append(builder, notes, ' ');
      String moreNotes = entry(line, 15);
      append(builder, moreNotes, '\n');
      return Strings.emptyToNull(builder.toString());
    }
    
    private void append(StringBuilder builder, String text, char separator) {
      if (!Strings.isNullOrEmpty(text)) {
        if (builder.length() > 0) {
          builder.append(separator);
        }
        builder.append(text);
      }
    }
    
    private String entry(String[] line, int index) {
      if (line.length <= index) {
        return null;
      }
      
      return line[index];
    }
    
  }

  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
    mappers.add(new MultiFormatDateFromStringFieldMapper<>(
        LineExtractors.stringFromIndex(4),
        Locale.US,
        "MM/dd/yy",
        "MM-dd-yy"));
    mappers.add(new DescriptionFieldMapper<>(new DescriptionExtractor()));
    mappers.add(new CountFieldMapper<>(LineExtractors.stringFromIndex(3)));
    LineExtractor<String> tagExtractor =  LineExtractors.stringFromIndex(11);
    mappers.add(new HeardOnlyFieldMapper<>(line -> isHeardOnly(tagExtractor.extract(line))));
    
    return new ComputedMappings<>(
        new TaxonFieldMapper(taxonomyIdExtractor),
        new LocationMapper(locationIdExtractor),
        mappers);
  }

  @Override
  protected Charset getCharset() {
    if (Charset.isSupported("x-MacRoman")) {
      return Charset.forName("x-MacRoman");
    } else if (Charset.isSupported("MacRoman")) {
      return Charset.forName("MacRoman");
    }
    return StandardCharsets.ISO_8859_1;
  }
  
  
}
