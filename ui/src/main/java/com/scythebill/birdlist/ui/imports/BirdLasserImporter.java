/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.joda.time.Duration;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;
import org.joda.time.ReadableDuration;
import org.joda.time.ReadablePartial;
import org.joda.time.chrono.GJChronology;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import com.google.common.collect.TreeMultimap;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.io.TimeIO;
import com.scythebill.birdlist.model.sighting.Distance;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo.BreedingBirdCode;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfo.ObservationType;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.imports.ParsedLocationIds.ToBeDecided;
import com.scythebill.birdlist.ui.imports.ParsedLocationIds.ToBeDecided.HintType;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Imports sightings from BirdLasser export files.
 * 
 * TODO:
 * - Activity
 * - Male/female/juvenile?
 */
public class BirdLasserImporter extends CsvSightingsImporter {
  private static final DateTimeFormatter ISO_DATE_TIME_FORMATTER =
      ISODateTimeFormat.dateTime().withChronology(GJChronology.getInstance());
  private static final double ASSUME_SAME_LOCATION_DISTANCE_KM = 2.0;
  private static final ReadableDuration ASSUME_SAME_LOCATION_DURATION = Duration.standardMinutes(30);
  private LineExtractor<String> commonNameExtractor;
  private LineExtractor<String> sciNameExtractor;
  private LineExtractor<String> dateExtractor;
  private LineExtractor<String> timeExtractor;
  private LineExtractor<String> isoDateExtractor;
  private LineExtractor<String> latitudeExtractor;
  private LineExtractor<String> longitudeExtractor;
  private LineExtractor<String> locationExtractor;
  private LineExtractor<String> locationIdExtractor;
  private Map<String, String> directLocationIdToCanonicalLocationId;
  private Map<VisitInfoKey, VisitInfo> visitInfoMap;
  
  /**
   * Mapping of BirdLasser strings to various Breeding Bird Codes.
   */
  private static final ImmutableMap<String, BreedingBirdCode> BREEDING_BIRD_CODES;
  static {
    ImmutableMap.Builder<String, BreedingBirdCode> builder = ImmutableMap.builder();
    builder.put("Courting display", BreedingBirdCode.COURTSHIP_DISPLAY_OR_COPULATION);
    builder.put("Copulation", BreedingBirdCode.COURTSHIP_DISPLAY_OR_COPULATION);
    builder.put("Carrying nesting material", BreedingBirdCode.CARRYING_NESTING_MATERIAL);
    builder.put("Active nest building", BreedingBirdCode.NEST_BUILDING);
    builder.put("Active nest building - excavating cavity", BreedingBirdCode.NEST_BUILDING);
    builder.put("Newly completed nest", BreedingBirdCode.OCCUPIED_NEST);
    builder.put("Nest with chicks", BreedingBirdCode.NEST_WITH_YOUNG);
    builder.put("Nest with eggs", BreedingBirdCode.NEST_WITH_EGGS);
    builder.put("Parents feeding young in nest", BreedingBirdCode.FEEDING_YOUNG);
    builder.put("Parent with faecal sac", BreedingBirdCode.CARRYING_FECAL_SAC);
    builder.put("Parents and young, not in nest", BreedingBirdCode.RECENTLY_FLEDGED);
    builder.put("Nest present - status uncertain", BreedingBirdCode.OCCUPIED_NEST);
    builder.put("In cavity", BreedingBirdCode.OCCUPIED_NEST);
    BREEDING_BIRD_CODES = builder.build();
  }
  
  public BirdLasserImporter(ReportSet reportSet, Taxonomy taxonomy, Checklists checklists, PredefinedLocations predefinedLocations, File file) {
    super(reportSet, taxonomy, checklists, predefinedLocations, file, file);
  }
  
  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return LineExtractors.joined(Joiner.on('|'), commonNameExtractor, sciNameExtractor);
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    return new FieldTaxonImporter<>(taxonomy, commonNameExtractor, sciNameExtractor);
  }
  
  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    directLocationIdToCanonicalLocationId = Maps.newHashMap();
    
    ImportLines lines = importLines(locationsFile);
    try {
      computeMappings(lines);
      
      LocalDateTime previousDateTime = null;
      String previousLocationId = null;
      String previousLocationName = null;
      LatLongCoordinates previousLatLong = null;
      // Now run through those lines in ascending order.
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }
        String id = locationIdExtractor.extract(line);
        if (locationIds.hasBeenParsed(id)) {
          continue;
        }
        
        String isoDate = isoDateExtractor.extract(line);
        LocalDateTime dateTime;
        try {
          dateTime = ISO_DATE_TIME_FORMATTER.parseLocalDateTime(isoDate);
        } catch (IllegalArgumentException e) {
          // Invalid date-time will fail that line anyway, move along.  Otherwise the entire import will fail.
          continue;
        }
        
        String locationName = locationExtractor.extract(line);
        String latitude = latitudeExtractor.extract(line);
        String longitude = longitudeExtractor.extract(line);
        LatLongCoordinates latLong;
        if (Strings.isNullOrEmpty(latitude) || Strings.isNullOrEmpty(longitude)) {
          latLong = null;
        } else {
          latLong = LatLongCoordinates.withLatAndLong(latitude, longitude);
        }

        // If the name is empty - or the same as the previous name - consider if 
        // we should skip this location line and map it to the previous one
        if (Strings.isNullOrEmpty(locationName)
            || Objects.equal(locationName, previousLocationName)) {
          // If the previous entry was < N minutes ago, then lump it with this location;
          // TODO: maybe consider putting a location delta on it too?
          if (previousDateTime != null
              && dateTime.isBefore(previousDateTime.plus(ASSUME_SAME_LOCATION_DURATION))) {
            directLocationIdToCanonicalLocationId.put(id, previousLocationId);
            continue;
          }
          
          if (previousLatLong != null && latLong != null) {
            double kmDistance = previousLatLong.kmDistance(latLong);
            if (kmDistance < ASSUME_SAME_LOCATION_DISTANCE_KM) {
              directLocationIdToCanonicalLocationId.put(id, previousLocationId);
              continue;
            }
          }
        }
        
        // This location will use its own location ID
        directLocationIdToCanonicalLocationId.put(id, id);
        // ... and others may be attached to it.
        previousDateTime = dateTime;
        previousLocationId = id;
        previousLocationName = locationName;
        previousLatLong = latLong; 
        
        String date = dateExtractor.extract(line);
        String time = timeExtractor.extract(line);
        // TODO: parse to a date and time, then back to a locale-specific string
        String hint = date + " " + time;
        ToBeDecided tbd = new ToBeDecided(locationName, latLong, hint, HintType.other);
        
        locationIds.addToBeResolvedLocationName(id, tbd);
      }
    } catch (IOException | RuntimeException e) {
      throw new ImportException(
          Messages.getFormattedMessage(Name.FAILED_IMPORT_ON_LINE_FORMAT, lines.lineNumber()),
          e);
    } finally {
      lines.close();
    }
  }

  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    String[] header = lines.nextLine();
    Map<String, Integer> headersByIndex = Maps.newHashMap();
    Function<String, String> headerFunc = s -> CharMatcher.whitespace().removeFrom(s).toLowerCase();
    
    for (int i = 0; i < header.length; i++) {
      headersByIndex.put(headerFunc.apply(header[i]), i);
    }
    
    int commonIndex;
    int sciIndex;
    // If a sciNameColumnIndex has been explicitly found, then use that
    if (sciNameColumnIndex != null) {
     sciIndex = sciNameColumnIndex;
     commonIndex = getRequiredHeader(headersByIndex, headerFunc,
         "Primary Language", "Species Primary Name");
     // If someone chose scientific names as their primary, use the secondary name
     // as common.
     if (commonIndex == sciIndex) {
       commonIndex = getRequiredHeader(headersByIndex, headerFunc,
           "Secondary Language", "Species Secondary Name");
     }     
    } else {
      // Primary/Tertiary appears to be the default
      commonIndex = getRequiredHeader(headersByIndex, headerFunc,
          "Primary Language", "Species Primary Name");
      sciIndex = getRequiredHeader(headersByIndex, headerFunc,
          "Tertiary Language", "Species Tertiary Name");
    }

    int dateIndex = getRequiredHeader(headersByIndex, headerFunc, "Date");
    int timeIndex = getRequiredHeader(headersByIndex, headerFunc, "Time");
    Integer seenHeardIndex = headersByIndex.get("seen/heard");
    int latitudeIndex = getRequiredHeader(headersByIndex, headerFunc, "Latitude");
    int longitudeIndex = getRequiredHeader(headersByIndex, headerFunc, "Longitude");
    Integer countIndex = headersByIndex.get("count");
    Integer locationNameIndex = headersByIndex.get("locationname"); 
    final Integer commentIndex = headersByIndex.get("notes");
    int isoDateIndex = getRequiredHeader(headersByIndex, headerFunc, "ISO date");
    Integer breedingStatusIndex = headersByIndex.get("breedingstatus");
    Integer aliveOrDeadIndex = headersByIndex.get("dead/alive");
    Integer photographedIndex = headersByIndex.get("photographed");

    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
    commonNameExtractor = LineExtractors.stringFromIndex(commonIndex);
    sciNameExtractor = LineExtractors.stringFromIndex(sciIndex);
    
    dateExtractor = LineExtractors.stringFromIndex(dateIndex);
    timeExtractor = LineExtractors.stringFromIndex(timeIndex);
    // Location name was once present, but has been removed
    locationExtractor = locationNameIndex == null
        ? LineExtractors.constant("")
        : LineExtractors.stringFromIndex(locationNameIndex);
        
    latitudeExtractor = LineExtractors.stringFromIndex(latitudeIndex);
    longitudeExtractor = LineExtractors.stringFromIndex(longitudeIndex);
 
    isoDateExtractor = LineExtractors.stringFromIndex(isoDateIndex);
    
    locationIdExtractor = new LineExtractor<String>() {
      @Override
      public String extract(String[] line) {
        String name = locationExtractor.extract(line);
        // If name is set, just use that as the key (don't ask for any re-mappings based
        // on lat-long)
        if (!Strings.isNullOrEmpty(name)) {
          return name;
        }
        return String.format("%s|%s",
            latitudeExtractor.extract(line), longitudeExtractor.extract(line));
      }
    };
    
    mappers.add(new MultiFormatDateFromStringFieldMapper<>(
        dateExtractor,
        "yyyy-MM-dd",
        "yyyy/MM/dd"));
    if (countIndex != null) {
      mappers.add(new CountFieldMapper<>(
          LineExtractors.stringFromIndex(countIndex)));
    }
    LineExtractor<String> commentExtractor = commentIndex == null
        ? LineExtractors.constant("")
        : LineExtractors.stringFromIndex(commentIndex);
    mappers.add(new DescriptionFieldMapper<>(
        LineExtractors.appendingLatitudeAndLongitude(commentExtractor, latitudeExtractor, longitudeExtractor)));
    
    if (seenHeardIndex != null) {
      final LineExtractor<String> seenHeardExtractor = LineExtractors.stringFromIndex(seenHeardIndex);
      mappers.add(new HeardOnlyFieldMapper<>(new LineExtractor<Boolean>() {
        @Override
        public Boolean extract(String[] line) {
          String extracted = seenHeardExtractor.extract(line);
          return "heard".equalsIgnoreCase(extracted);
        }
      }));
    }
    
    // If present, map Breeding Bird Codes appropriately
    if (breedingStatusIndex != null) {
      final LineExtractor<String> breedingStatusExtractor = LineExtractors.stringFromIndex(breedingStatusIndex);
      mappers.add(new FieldMapper<>() {
        @Override
        public void map(String[] line, Sighting.Builder sighting) {
          String extracted = breedingStatusExtractor.extract(line);
          BreedingBirdCode code = BREEDING_BIRD_CODES.get(extracted);
          if (code != null) {
            sighting.getSightingInfo().setBreedingBirdCode(code);
          }
        }
      });
    }
    
    if (aliveOrDeadIndex != null) {
      final LineExtractor<String> aliveOrDeadExtractor = LineExtractors.stringFromIndex(aliveOrDeadIndex);
      mappers.add(new FieldMapper<>() {
        @Override
        public void map(String[] line, Sighting.Builder sighting) {
          String extracted = aliveOrDeadExtractor.extract(line);
          if (extracted != null
              && extracted.toLowerCase().startsWith("dead")) {
            sighting.getSightingInfo().setSightingStatus(SightingStatus.DEAD);
          }
        }
      });
    }
    
    if (photographedIndex != null) {
      LineExtractor<Boolean> photographedExtractor = LineExtractors.booleanFromIndex(photographedIndex);
      mappers.add(new PhotographedFieldMapper<>(photographedExtractor));
    }
    
    visitInfoMap = Maps.newLinkedHashMap();
        
    return new ComputedMappings<>(
        new TaxonFieldMapper(taxonomyIdExtractor()),
        new LocationMapper(new LineExtractor<Object>() {
          @Override
          public Object extract(String[] line) {
            // Find the location ID implied by the line
            String extracted = locationIdExtractor.extract(line);
            // ... and map that to a "canonical" ID extracted directly
            String canonicalId = directLocationIdToCanonicalLocationId.get(extracted);
            return canonicalId;
          }
        }),
        mappers);
  }

  @Override
  protected ImportLines importLines(File file) throws IOException {
    return new SortedByTimeImporter(CsvImportLines.fromFile(file, getCharset()));
  }  
  
  /**
   * ImportLines proxy which sorts all rows by the ISO Date field.  This simplifies
   * importing logic greatly, since it means that all entries for a given visit are
   * consecutive - it's trivial to find the start time and computing the distance 
   * traveled is fairly simple
   */
  static class SortedByTimeImporter implements ImportLines {
    private final ImportLines root;
    private Iterator<LineAndRowNumber> remainingLines;
    private int currentRow; 

    SortedByTimeImporter(ImportLines root) {
      this.root = root;
    }

    @Override
    public void close() throws IOException {
      root.close();
    }

    @Override
    public ImportLines withoutTrimming() {
      root.withoutTrimming();
      return this;
    }
    
    @Override
    public String[] nextLine() throws IOException {
      if (remainingLines == null) {
        String[] header = root.nextLine();
        if (header == null) {
          throw new IllegalStateException("File is empty");
        }
        
        int foundIndex = -1;
        
        // In the header row, find 
        for (int i = 0; i < header.length; i++) {
          if ("isodate".equals(CharMatcher.whitespace().removeFrom(header[i]).toLowerCase())) {
            foundIndex = i;
            break;
          }
        }
        if (foundIndex < 0) {
          throw new IllegalStateException("Could not find ISO Date column in BirdLasser export.");
        }
        
        TreeMultimap<String, LineAndRowNumber> sortedLines = TreeMultimap.create();
        // Load all lines, sorting by the ISO date field
        int row = 0;
        while (true) {
          String[] line = root.nextLine();
          if (line == null) {
            break;
          }
          
          // Skip any line that doesn't have the ISO Date column.
          if (line.length <= foundIndex) {
            continue;
          }
          sortedLines.put(line[foundIndex], new LineAndRowNumber(line, ++row));
        }
        remainingLines = sortedLines.values().iterator();
        
        return header;
      } else {
        if (!remainingLines.hasNext()) {
          return null;
        }
        LineAndRowNumber next = remainingLines.next();
        currentRow = next.row;
        return next.line;
      }
    }

    @Override
    public int lineNumber() {
      return currentRow;
    }
  }
  
  static class LineAndRowNumber implements Comparable<LineAndRowNumber>{
    final String[] line;
    final int row;
    
    LineAndRowNumber(String[] line, int row) {
      this.line = line;
      this.row = row;
    }

    @Override
    public int compareTo(LineAndRowNumber that) {
      return row - that.row;
    }
  }

  // Instance variables used by finishSighting
  
  private String lastLocationId;
  private Integer sciNameColumnIndex;
  private LocalTime lastStartTime;
  private ReadablePartial lastDate;

  @Override
  protected void finishSighting(
      Sighting.Builder newSighting,
      String[] line) {
    String locationId = newSighting.getLocationId();
    if (locationId == null) {
      return;
    }

    try {
      LocalTime localTime = TimeIO.fromExternalString(timeExtractor.extract(line));
    
      LocalTime startTimeToSet = localTime;
      boolean resetVisit = false;
      if (lastLocationId == null) {
        // First row, reset the visit
        resetVisit = true;
      } else {
        // If we're still at the same location on the same day, then switch to
        // the start time.
        if (lastLocationId.equals(newSighting.getLocationId())
            && lastDate.equals(newSighting.getDate())) {
          startTimeToSet = lastStartTime;
        } else {
          resetVisit = true;
        }
      }

      newSighting.setTime(TimeIO.normalize(startTimeToSet));
      
      if (resetVisit) {
        lastLocationId = locationId;
        lastStartTime = startTimeToSet;
        lastDate = newSighting.getDate();
      }
    } catch (IllegalArgumentException e) {
      // ignore
    }
  }

  @Override
  public void beforeParseTaxonomyIds() throws IOException {
    try (ImportLines lines = importLines(sightingsFile)) {
      String[] header = lines.nextLine();      
      Map<String, Integer> headersByIndex = Maps.newHashMap();
      Function<String, String> headerFunc = s -> CharMatcher.whitespace().removeFrom(s).toLowerCase();
      
      for (int i = 0; i < header.length; i++) {
        headersByIndex.put(headerFunc.apply(header[i]), i);
      }
      
      TaxonResolver resolver = new TaxonResolver(getTaxonomy());
      Multiset<Integer> headersWithSciNames = HashMultiset.create();
      while (headersWithSciNames.size() <= 500) {
        String[] line = lines.nextLine();
        if (line == null) break;
        
        if (skipLine(line)) {
          continue;
        }
        
        for (int i = 0; i < line.length; i++) {
          if (resolver.map(null, line[i], null) != null) {
            headersWithSciNames.add(i);
            break;
          }
        }
      }
      
      if (!headersWithSciNames.isEmpty()) {
        sciNameColumnIndex = Multisets.copyHighestCountFirst(headersWithSciNames).iterator().next();
      }
    }
  }
  
  // Instance variables used by lookForVisitInfo and allSightingsFinished
  
  private VisitInfoKey lastVisitInfoKey = null;
  private LocalDateTime firstDateTime;
  private LocalDateTime lastDateTime;
  private LatLongCoordinates lastLatLong;
  private float accumulatedDistanceKm;

  @Override
  protected void lookForVisitInfo(
      ReportSet reportSet,
      String[] line,
      Sighting newSighting,
      VisitInfoKey visitInfoKey) {
    LatLongCoordinates latLong = LatLongCoordinates.withLatAndLong(
        latitudeExtractor.extract(line),
        longitudeExtractor.extract(line));
    String isoDate = isoDateExtractor.extract(line);
    // Use the ISO date time here - the 
    LocalDateTime dateTime = ISO_DATE_TIME_FORMATTER.parseLocalDateTime(isoDate);
    
    boolean resetVisitInfo = false;
    if (lastVisitInfoKey == null) {
      resetVisitInfo = true;
    } else if (lastVisitInfoKey.equals(visitInfoKey)) {
      // Still on the same visit;  increment the total distance
      accumulatedDistanceKm += latLong.kmDistance(lastLatLong);
    } else {
      // Call all-sightings-finished, which finishes populating the visit info
      allSightingsFinished();
      resetVisitInfo = true;
    }

    lastLatLong = latLong;
    lastDateTime = dateTime;

    if (resetVisitInfo) {
      lastVisitInfoKey = visitInfoKey;
      accumulatedDistanceKm = 0;
      firstDateTime = dateTime;
    }
  }
  
  @Override
  protected void allSightingsFinished() {
    // Record the accumulated distance and time, and finalize the visit info
    VisitInfo.Builder visitInfo = VisitInfo.builder().withObservationType(ObservationType.HISTORICAL);
    if (accumulatedDistanceKm > 0.010) {
      visitInfo.withDistance(Distance.inKilometers(accumulatedDistanceKm));
    }
    
    Duration duration = Minutes.minutesBetween(firstDateTime, lastDateTime).toStandardDuration();
    if (duration.isLongerThan(Duration.standardMinutes(5))) {
      visitInfo.withDuration(duration);
    }
    visitInfoMap.put(lastVisitInfoKey, visitInfo.build());
  }

  @Override
  public Map<VisitInfoKey, VisitInfo> getAccumulatedVisitInfoMap() {
    return visitInfoMap;
  }
}
