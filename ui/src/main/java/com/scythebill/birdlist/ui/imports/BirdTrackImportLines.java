/*   
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.xml.sax.SAXParseException;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.io.ByteSource;
import com.scythebill.birdlist.model.io.ImportLines;

/**
 * An ImportLines implementation for the BirdTrack XLSX.
 */
class BirdTrackImportLines {  
  static class BirdTrackXlsxImportLines implements ImportLines {
    private final boolean trim;
    private final DataFormatter df = new DataFormatter();
    private final ImmutableList<XSSFSheet> sheets;
    private XSSFSheet sheet;
    private int sheetIndex = 0;
    private int rowNum;

    BirdTrackXlsxImportLines(List<XSSFSheet> sheets) throws SAXParseException {
      this(sheets, true);

    }

    public BirdTrackXlsxImportLines(List<XSSFSheet> sheets, boolean trim) {
      this.sheets = ImmutableList.copyOf(sheets);
      if (!this.sheets.isEmpty()) {
        sheet = this.sheets.get(0);
        rowNum = sheet.getFirstRowNum();
      }
      this.trim = trim;
    }

    @Override
    public void close() throws IOException {}

    @Override
    public String[] nextLine() throws IOException {
      if (sheet == null) {
        return null;
      }

      XSSFRow row = null;      
      while (row == null) {
        if (rowNum > sheet.getLastRowNum()) {
          if (!nextSheet()) {
            return null;          
          }
        }
  
        row = sheet.getRow(rowNum++);
        // Skip any blank rows
        if (row == null) {
          continue;
        }
      }
      
      String[] line = new String[row.getLastCellNum() - row.getFirstCellNum()];
      for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
        XSSFCell cell = row.getCell(i);
        if (cell != null) {
          String cellValue = df.formatCellValue(cell);
          if (trim) {
            cellValue = cellValue.trim();
          }
          line[i - row.getFirstCellNum()] = cellValue;
        }
      }
      
      for (String cell : line) {
        if (!Strings.isNullOrEmpty(cell)) {
          return line;
        }
      }
      
      return null;
    }

    private boolean nextSheet() {
      // Advances to the next sheet, and returns false if there are none
      sheetIndex++;
      if (sheetIndex >= sheets.size()) {
        return false;
      }
      
      sheet = sheets.get(sheetIndex);
      // Skip the header row
      rowNum = sheet.getFirstRowNum() + 1;
      return true;
    }

    @Override
    public int lineNumber() {
      return rowNum;
    }

    @Override
    public ImportLines withoutTrimming() {
      return new BirdTrackXlsxImportLines(sheets, false);
    }
  }

  public static ImportLines importBirdTrackXlsx(ByteSource byteSource) throws IOException {
    try (InputStream inputStream = byteSource.openBufferedStream()) {
      try (XSSFWorkbook workbook = new XSSFWorkbook(byteSource.openStream())) {
        List<XSSFSheet> sheets = new ArrayList<>();
        for (int i = 1; i < workbook.getNumberOfSheets(); i++) {
          sheets.add(workbook.getSheetAt(i));
        }
        return new BirdTrackXlsxImportLines(sheets);
      } catch (SAXParseException e) {
        throw new IOException("Could not parse BirdTrack XSLX file", e);
      }
    }
  }
  
}
