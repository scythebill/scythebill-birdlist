/*   
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.joda.time.Duration;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.io.TimeIO;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.Sighting.Builder;
import com.scythebill.birdlist.model.sighting.SightingInfo.BreedingBirdCode;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfo.ObservationType;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.imports.ParsedLocationIds.ToBeDecided;

/** Imports BirdTrack xlsx files. */
class BirdTrackImporter extends CsvSightingsImporter {
  private final static Logger logger = Logger.getLogger(BirdTrackImporter.class.getName());

  private Map<VisitInfoKey, VisitInfo.Builder> visitInfoMap;
  private LineExtractor<String> taxonomyIdExtractor;
  private LineExtractor<String> commonNameExtractor;
  private LineExtractor<String> scientificExtractor;
  private LineExtractor<String> placeExtractor;
  private LineExtractor<String> longitudeExtractor;
  private LineExtractor<String> latitudeExtractor;
  private LineExtractor<String> locationIdExtractor;
  private LineExtractor<String> dateExtractor;
  private LineExtractor<String> startTimeExtractor;
  private LineExtractor<String> endTimeExtractor;
  private LineExtractor<String> countExtractor;
  private LineExtractor<String> commentsExtractor;
  private LineExtractor<String> habitatNotesExtractor;
  private LineExtractor<String> visitCommentExtractor;
  private LineExtractor<Boolean> completeListExtractor;

  protected BirdTrackImporter(ReportSet reportSet, Taxonomy taxonomy, Checklists checklists,
      PredefinedLocations predefinedLocations, File file) {
    super(reportSet, taxonomy, checklists, predefinedLocations, file, file);
  }

  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return taxonomyIdExtractor;
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    return new FieldTaxonImporter<>(taxonomy, commonNameExtractor, scientificExtractor);
  }
  
  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    String[] header = lines.nextLine();
    Map<String, Integer> headersByIndex = Maps.newHashMap();
    CharMatcher trimmer = CharMatcher.whitespace().or(CharMatcher.anyOf("-_?"));
    Function<String, String> transform = s -> trimmer.removeFrom(s).toLowerCase(); 
    for (int i = 0; i < header.length; i++) {
      headersByIndex.put(transform.apply(header[i]), i);
    }

    visitInfoMap = new LinkedHashMap<>();
    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
    int commonIndex = getRequiredHeader(headersByIndex, "Species", transform);
    int sciIndex = getRequiredHeader(headersByIndex, "Scientific name", transform);
    int placeIndex = getRequiredHeader(headersByIndex, "Place", transform);
    int dateIndex = getRequiredHeader(headersByIndex, "Date", transform);
    Integer latitudeIndex = headersByIndex.get("lat");
    Integer longitudeIndex = headersByIndex.get("long");
    Integer startTimeIndex = headersByIndex.get("starttime");
    Integer endTimeIndex = headersByIndex.get("endtime");
    Integer countIndex = headersByIndex.get("count");
    Integer commentsIndex = headersByIndex.get("comment");
    Integer habitatNotesIndex = headersByIndex.get("habitatnotes");
    Integer visitCommentIndex = headersByIndex.get("visitcomments");
    Integer visitWeatherCommentIndex = headersByIndex.get("visitweathercomments");
    Integer completeListIndex = headersByIndex.get("partofcompletelist");
    Integer breedingStatusIndex = headersByIndex.get("breedingstatus");
    Integer plumageIndex = headersByIndex.get("plumage");

    // Create a taxonomy extractor from the located common/scientific/ssp.
    // columns
    LineExtractor<String> rawCommonExtractor = LineExtractors.stringFromIndex(commonIndex);
    commonNameExtractor = row -> {
      String rawCommon = rawCommonExtractor.extract(row);
      // Common names in BirdTrack have a parenthetical suffix when there's
      // subspecies.  Drop this, and use subspecies from the scientific name
      int parenthesisIndex = rawCommon.indexOf(" (");
      if (parenthesisIndex > 0) {
        rawCommon = rawCommon.substring(0, parenthesisIndex);
      }
      return rawCommon;
    };
    scientificExtractor = LineExtractors.stringFromIndex(sciIndex);
    taxonomyIdExtractor = LineExtractors.joined(Joiner.on('|').useForNull(""),
        ImmutableList.of(commonNameExtractor, scientificExtractor));
    
    placeExtractor = LineExtractors.stringFromIndex(placeIndex);
    locationIdExtractor = LineExtractors.joined(Joiner.on('|').useForNull(""), placeExtractor);
    dateExtractor = LineExtractors.stringFromIndex(dateIndex);
    mappers.add(new DateFromStringFieldMapper<>("dd/MM/yyyy", dateExtractor));
    latitudeExtractor = (latitudeIndex == null)
        ?  LineExtractors.<String> alwaysNull()
        : LineExtractors.stringFromIndex(latitudeIndex);
    longitudeExtractor = (longitudeIndex == null)
        ?  LineExtractors.<String> alwaysNull()
        : LineExtractors.stringFromIndex(longitudeIndex);
    
    
    commentsExtractor = commentsIndex == null
        ? LineExtractors.alwaysNull()
        : LineExtractors.stringFromIndex(commentsIndex);
    habitatNotesExtractor = habitatNotesIndex == null
        ? LineExtractors.alwaysNull()
        : LineExtractors.stringFromIndex(habitatNotesIndex);
    mappers.add(new DescriptionFieldMapper<>(
        LineExtractors.joined(Joiner.on('\n').skipNulls(), commentsExtractor, habitatNotesExtractor)));

    if (breedingStatusIndex != null) {
      mappers.add(new BreedingStatusMapper(LineExtractors.stringFromIndex(breedingStatusIndex)));
    }
    
    if (plumageIndex != null) {
      mappers.add(new PlumageMapper(LineExtractors.stringFromIndex(plumageIndex)));
    }
    
    if (countIndex != null) {
      countExtractor = LineExtractors.stringFromIndex(countIndex);
      // Support a character at the start or end
      Pattern characterAtStart = Pattern.compile(".?[0-9]+");
      Pattern characterAtEnd = Pattern.compile("[0-9]+.");
      mappers.add(new CountFieldMapper<>(
          line -> {
            String count = countExtractor.extract(line);
            // Ignore anything that is missing or doesn't match the "optional single character and number"
            // format
            if (count == null) {
              return null;
            }
            
            // Character at start is accepted natively
            if (characterAtStart.matcher(count).matches()) {
              return count;
            }
            
            // But move the "character at end" to the start 
            if (characterAtEnd.matcher(count).matches()) {
              char charAtEnd = count.charAt(count.length() - 1);
              return charAtEnd + count.substring(0, count.length() - 1); 
            }
            
            return count;
          }));
    }
    
    completeListExtractor = completeListIndex == null
        ? LineExtractors.constant(false)
        : LineExtractors.booleanFromIndex(completeListIndex);
    startTimeExtractor = startTimeIndex == null
        ? LineExtractors.alwaysNull()
        : LineExtractors.stringFromIndex(startTimeIndex);
    
    mappers.add(new TimeMapper<>(startTimeExtractor));
    
    endTimeExtractor = endTimeIndex == null
        ? LineExtractors.alwaysNull()
        : LineExtractors.stringFromIndex(endTimeIndex);
    visitCommentExtractor = LineExtractors.joined(Joiner.on('\n').skipNulls(),
        (visitCommentIndex == null
            ? LineExtractors.alwaysNull()
            : LineExtractors.stringFromIndex(visitCommentIndex)),
        (visitWeatherCommentIndex == null
            ? LineExtractors.alwaysNull()
            : LineExtractors.stringFromIndex(visitWeatherCommentIndex)));
    
    return new ComputedMappings<>(
        new TaxonFieldMapper(taxonomyIdExtractor),
        new LocationMapper(locationIdExtractor),
        mappers);
  }
  
  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    try (ImportLines lines = importLines(locationsFile)) {
      computeMappings(lines);
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }

        if (skipLine(line)) {
          continue;
        }
        
        String id = locationIdExtractor.extract(line);
        if (locationIds.hasBeenParsed(id)) {
          continue;
        }

        String place = placeExtractor.extract(line);
        ImportedLocation importedLocation = new ImportedLocation();
        importedLocation.locationNames.add(place);
        
        importedLocation.longitude = Strings.emptyToNull(longitudeExtractor.extract(line));
        importedLocation.latitude = Strings.emptyToNull(latitudeExtractor.extract(line));
        
        String locationId = importedLocation.tryAddToLocationSetWithLatLong(reportSet, locations, locationShortcuts, predefinedLocations);
        if (locationId != null) {
          locationIds.put(id, locationId);
        } else {
          locationIds.addToBeResolvedLocationName(id,
              new ToBeDecided(place, importedLocation.getLatLong()));
        }
      }
    }
  }

  @Override
  protected void lookForVisitInfo(
      ReportSet reportSet,
      String[] line,
      Sighting newSighting,
      VisitInfoKey visitInfoKey) {
    // Merge results in with any other data gathered for this visit (e.g.,
    // do not assume that all visit info data is set on all lines)
    VisitInfo.Builder builder = visitInfoMap.get(visitInfoKey);
    if (builder == null) {
      // Default to HISTORICAL, which is fairly safe
      builder = VisitInfo.builder().withObservationType(ObservationType.HISTORICAL);
      visitInfoMap.put(visitInfoKey, builder);
    }
    
    if (visitInfoKey.startTime().isPresent()) {
      String endTimeString = endTimeExtractor.extract(line);
      if (!Strings.isNullOrEmpty(endTimeString)) {
        try {
          LocalTime endTime = TimeIO.fromExternalString(endTimeString);
          Duration duration = Minutes.minutesBetween(visitInfoKey.startTime().get(), endTime).toStandardDuration();
          if (duration.isLongerThan(Duration.standardMinutes(5))) {
            builder = builder.withDuration(duration);
          }
        } catch (IllegalArgumentException e) {
          logger.log(Level.WARNING, "Failed to parse time " + endTimeString, e);
        }
      }
      
    }
    
    String visitComments = visitCommentExtractor.extract(line);
    if (!Strings.isNullOrEmpty(visitComments)) {
      builder.withComments(visitComments);
    }
    
    if (completeListExtractor.extract(line)) {
      builder.withCompleteChecklist(true);
    }      
  }

  @Override
  public Map<VisitInfoKey, VisitInfo> getAccumulatedVisitInfoMap() {
    Map<VisitInfoKey, VisitInfo> accumulated = new LinkedHashMap<>();
    // Convert the assembled data into final VisitInfo objects
    for (Map.Entry<VisitInfoKey, VisitInfo.Builder> entry : visitInfoMap.entrySet()) {
      VisitInfo.Builder builder = entry.getValue();
      VisitInfo visitInfo = builder.build();

      // If there's any data worth storing, do so.
      if (visitInfo.hasData()) {
        accumulated.put(entry.getKey(), visitInfo);
      }

    }
    return accumulated;
  }

  @Override
  protected ImportLines importLines(File file) throws IOException {
    return BirdTrackImportLines.importBirdTrackXlsx(Files.asByteSource(file));
  }

  static class BreedingStatusMapper implements FieldMapper<String[]> {

    private final static ImmutableMap<String, BreedingBirdCode> STATUS_TO_BREEDING_BIRD_CODE = 
        ImmutableMap.<String, BreedingBirdCode>builder()
        .put("01", BreedingBirdCode.IN_APPROPRIATE_HABITAT)
        .put("02", BreedingBirdCode.SINGING_BIRD)
        .put("03", BreedingBirdCode.PAIR_IN_SUITABLE_HABITAT)
        // 4 is "Permanent territory" - no obvious mapping
        .put("05", BreedingBirdCode.COURTSHIP_DISPLAY_OR_COPULATION)
        .put("06", BreedingBirdCode.VISITING_PROBABLE_NEST_SITE)
        .put("07", BreedingBirdCode.AGITATED_BEHAVIOR)
        .put("08", BreedingBirdCode.PHYSIOLOGICAL_EVIDENCE)
        // Technically wrong if it's a woodpecker or wren
        .put("09", BreedingBirdCode.NEST_BUILDING)
        .put("10", BreedingBirdCode.DISTRACTION_DISPLAY)
        .put("11", BreedingBirdCode.USED_NEST)
        .put("12", BreedingBirdCode.RECENTLY_FLEDGED)
        .put("13", BreedingBirdCode.OCCUPIED_NEST)
        // This could also be "carrying a fecal sac";  BirdTrack doesn't distinguish, eBird does.
        .put("14", BreedingBirdCode.CARRYING_FOOD)
        .put("15", BreedingBirdCode.NEST_WITH_EGGS)
        .put("16", BreedingBirdCode.NEST_WITH_YOUNG)
        .build();
    
    private final LineExtractor<String> extractor;

    public BreedingStatusMapper(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public void map(String[] line, Builder sighting) {
      BreedingBirdCode code = STATUS_TO_BREEDING_BIRD_CODE.get(extractor.extract(line));
      if (code != null) {
        sighting.getSightingInfo().setBreedingBirdCode(code);
      }
    }
  }

  static class PlumageMapper implements FieldMapper<String[]> {
    private final LineExtractor<String> extractor;

    public PlumageMapper(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public void map(String[] line, Builder sighting) {
      String plumages = extractor.extract(line);
      if (Strings.isNullOrEmpty(plumages)) {
        return;
      }
      
      // I'd thought this might be in the funky import format for Age and Plumage
      // but it appears to come out in a form like "1 Male, 2 Female"
      plumages = plumages.toLowerCase();
      if (plumages.contains("male")) {
        sighting.getSightingInfo().setMale(true);        
      }
      if (plumages.contains("female")) {
        sighting.getSightingInfo().setFemale(true);        
      }
    }
  }
}
