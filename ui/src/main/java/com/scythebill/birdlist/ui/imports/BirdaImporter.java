/*   
 * Copyright 2024 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;
import org.joda.time.ReadablePartial;
import org.joda.time.chrono.GJChronology;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.TreeMultimap;
import com.google.common.primitives.Doubles;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.io.TimeIO;
import com.scythebill.birdlist.model.mapdata.TimezoneMapper;
import com.scythebill.birdlist.model.sighting.Distance;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfo.ObservationType;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.imports.ParsedLocationIds.ToBeDecided;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Imports sightings from Birda export files.
 */
public class BirdaImporter extends CsvSightingsImporter {
  private static final DateTimeFormatter ISO_DATE_TIME_FORMATTER =
      ISODateTimeFormat.dateTime().withChronology(GJChronology.getInstance());
  private static final double ASSUME_SAME_LOCATION_DISTANCE_KM = 2.0;
  private LineExtractor<String> commonNameExtractor;
  private LineExtractor<String> sciNameExtractor;
  private LineExtractor<String> isoDateExtractor;
  private LineExtractor<String> latitudeExtractor;
  private LineExtractor<String> longitudeExtractor;
  private LineExtractor<String> locationIdExtractor;
  private LineExtractor<String> sessionIdExtractor;
  private LineExtractor<String> sessionTitleExtractor;
  private Map<String, String> directLocationIdToCanonicalLocationId;
  private Map<VisitInfoKey, VisitInfo> visitInfoMap;
  private LineExtractor<LocalTime> timeExtractor;
  private LineExtractor<LocalDateTime> localDateTimeExtractor;
  
  public BirdaImporter(ReportSet reportSet, Taxonomy taxonomy, Checklists checklists, PredefinedLocations predefinedLocations, File file) {
    super(reportSet, taxonomy, checklists, predefinedLocations, file, file);
  }
  
  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return LineExtractors.joined(Joiner.on('|'), commonNameExtractor, sciNameExtractor);
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    return new FieldTaxonImporter<>(taxonomy, commonNameExtractor, sciNameExtractor);
  }
  
  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    directLocationIdToCanonicalLocationId = Maps.newHashMap();
    
    ImportLines lines = importLines(locationsFile);
    try {
      computeMappings(lines);
      
      String previousLocationId = null;
      String previousSessionId = null;
      List<String> allLocationIds = new ArrayList<>();
      List<LatLongCoordinates> allLatLongs = new ArrayList<>();
      
      // Now run through those lines in ascending order.
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }
        String id = locationIdExtractor.extract(line);
        if (locationIds.hasBeenParsed(id)) {
          continue;
        }
        
        String sessionId = sessionIdExtractor.extract(line);
        String latitude = latitudeExtractor.extract(line);
        String longitude = longitudeExtractor.extract(line);
        LatLongCoordinates latLong;
        if (Strings.isNullOrEmpty(latitude) || Strings.isNullOrEmpty(longitude)) {
          latLong = null;
        } else {
          latLong = LatLongCoordinates.withLatAndLong(latitude, longitude);
        }

        if (Objects.equal(sessionId, previousSessionId)) {
          directLocationIdToCanonicalLocationId.put(id, previousLocationId);
          continue;
        }
        
        if (latLong != null) {
          String closeEnoughLocationId = null;
          for (int i = allLatLongs.size() - 1; i >= 0; i--) {
            LatLongCoordinates previousLatLong = allLatLongs.get(i);
            double kmDistance = previousLatLong.kmDistance(latLong);
            if (kmDistance < ASSUME_SAME_LOCATION_DISTANCE_KM) {
              closeEnoughLocationId = allLocationIds.get(i);
              break;
            }
          }
          
          if (closeEnoughLocationId != null) {
            directLocationIdToCanonicalLocationId.put(id, closeEnoughLocationId);
            continue;
          }
        }
        
        // This location will use its own location ID
        directLocationIdToCanonicalLocationId.put(id, id);
        // ... and others may be attached to it.
        previousLocationId = id;
        previousSessionId = sessionId;
        allLocationIds.add(id);
        allLatLongs.add(latLong);
        
        ToBeDecided tbd = new ToBeDecided("", latLong, sessionTitleExtractor.extract(line),
            ToBeDecided.HintType.other);
        
        locationIds.addToBeResolvedLocationName(id, tbd);
      }
    } catch (IOException | RuntimeException e) {
      throw new ImportException(
          Messages.getFormattedMessage(Name.FAILED_IMPORT_ON_LINE_FORMAT, lines.lineNumber()),
          e);
    } finally {
      lines.close();
    }
  }

  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    String[] header = lines.nextLine();
    Map<String, Integer> headersByIndex = Maps.newHashMap();
    Function<String, String> headerFunc = s -> CharMatcher.whitespace().removeFrom(s).toLowerCase();
    
    for (int i = 0; i < header.length; i++) {
      headersByIndex.put(headerFunc.apply(header[i]), i);
    }
    
    int commonIndex = getRequiredHeader(headersByIndex, headerFunc,
          "commonName");
    int sciIndex = getRequiredHeader(headersByIndex, headerFunc,
        "scientificName");

    int isoDateIndex = getRequiredHeader(headersByIndex, headerFunc, "date");
    int latitudeIndex = getRequiredHeader(headersByIndex, headerFunc, "latitude");
    int longitudeIndex = getRequiredHeader(headersByIndex, headerFunc, "longitude");
    int sessionIdIndex = getRequiredHeader(headersByIndex, headerFunc, "sessionId");
    int sessionTitleIndex = getRequiredHeader(headersByIndex, headerFunc, "sessionTitle");
    Integer countIndex = headersByIndex.get("count");
    Integer countTypeIndex = headersByIndex.get("countType");
    final Integer commentIndex = headersByIndex.get("note");

    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
    commonNameExtractor = LineExtractors.stringFromIndex(commonIndex);
    sciNameExtractor = LineExtractors.stringFromIndex(sciIndex);
    
    isoDateExtractor = LineExtractors.stringFromIndex(isoDateIndex);
        
    latitudeExtractor = LineExtractors.stringFromIndex(latitudeIndex);
    longitudeExtractor = LineExtractors.stringFromIndex(longitudeIndex);
 
    isoDateExtractor = LineExtractors.stringFromIndex(isoDateIndex);
    
    sessionIdExtractor = LineExtractors.stringFromIndex(sessionIdIndex);
    sessionTitleExtractor = LineExtractors.stringFromIndex(sessionTitleIndex);
    locationIdExtractor = sessionIdExtractor;
    
    localDateTimeExtractor = new LineExtractor<LocalDateTime>() {
      @Override
      public LocalDateTime extract(String[] row) {
        String isoDateTime = isoDateExtractor.extract(row);
        DateTime dateTime = ISO_DATE_TIME_FORMATTER.parseDateTime(isoDateTime);
        Double longitude = Doubles.tryParse(longitudeExtractor.extract(row));
        Double latitude = Doubles.tryParse(latitudeExtractor.extract(row));
        if (longitude == null || latitude == null) {
          return dateTime.toLocalDateTime();
        }
        
        String timezoneId = TimezoneMapper.latLngToTimezoneString(latitude.doubleValue(), longitude.doubleValue());
        if (timezoneId == null) {
          return dateTime.toLocalDateTime();
        }
        
        try {
          DateTimeZone zone = DateTimeZone.forID(timezoneId);
          return dateTime.toDateTime(zone).toLocalDateTime();
        } catch (IllegalArgumentException e) {
          return dateTime.toLocalDateTime();
        }
      }
    };

    mappers.add(new FieldMapper<String[]>() {
      @Override
      public void map(String[] row, Sighting.Builder builder) {
        LocalDateTime localDateTime = localDateTimeExtractor.extract(row);
        if (localDateTime != null) {
          builder.setDate(localDateTime.toLocalDate());
        }
      }
    });
    
    timeExtractor = new LineExtractor<LocalTime>() {
      @Override
      public LocalTime extract(String[] row) {
        LocalDateTime localDateTime = localDateTimeExtractor.extract(row);
        if (localDateTime == null) {
          return null;
        }
        
        return localDateTime.toLocalTime();
      }
    };
    
    if (countIndex != null) {
      LineExtractor<String> countExtractor = LineExtractors.stringFromIndex(countIndex);
      LineExtractor<String> modifiedCountExtractor;
      if (countTypeIndex != null) {
        LineExtractor<String> countTypeExtractor = LineExtractors.stringFromIndex(countTypeIndex);
        modifiedCountExtractor = new LineExtractor<String>() {
          @Override
          public String extract(String[] row) {
            if ("UNKNOWN".equalsIgnoreCase(countTypeExtractor.extract(row))) {
              return null;
            }
            return countExtractor.extract(row);
          }
        };
      } else {
        modifiedCountExtractor = countExtractor;
      }
      mappers.add(new CountFieldMapper<>(modifiedCountExtractor));
    }
    LineExtractor<String> commentExtractor = commentIndex == null
        ? LineExtractors.constant("")
        : LineExtractors.stringFromIndex(commentIndex);
    mappers.add(new DescriptionFieldMapper<>(
        LineExtractors.appendingLatitudeAndLongitude(commentExtractor, latitudeExtractor, longitudeExtractor)));
    
    visitInfoMap = Maps.newLinkedHashMap();
        
    return new ComputedMappings<>(
        new TaxonFieldMapper(taxonomyIdExtractor()),
        new LocationMapper(new LineExtractor<Object>() {
          @Override
          public Object extract(String[] line) {
            // Find the location ID implied by the line
            String extracted = locationIdExtractor.extract(line);
            // ... and map that to a "canonical" ID extracted directly
            String canonicalId = directLocationIdToCanonicalLocationId.get(extracted);
            return canonicalId;
          }
        }),
        mappers);
  }

  @Override
  protected ImportLines importLines(File file) throws IOException {
    ImportLines importLines = new SortedByTimeImporter(CsvImportLines.fromFile(file, getCharset()));
    if (onlyImportDatesIn == null) {
      return importLines;
    }
    
    LineExtractor<ReadablePartial> dateExtractor = new LineExtractor<ReadablePartial>() {
      @Override
      public ReadablePartial extract(String[] row) {
        LocalDateTime localDateTime = localDateTimeExtractor.extract(row);
        if (localDateTime == null) {
          return null;
        }
        
        return localDateTime.toLocalDate();
      }
    };
    
    return new OnlyIncludeRowsMatchingDates(importLines, onlyImportDatesIn, dateExtractor);
  }
  
  /**
   * ImportLines proxy which sorts all rows by the ISO Date field.  This simplifies
   * importing logic greatly, since it means that all entries for a given visit are
   * consecutive - it's trivial to find the start time and computing the distance 
   * traveled is fairly simple
   */
  static class SortedByTimeImporter implements ImportLines {
    private ImportLines root;
    private Iterator<LineAndRowNumber> remainingLines;
    private int currentRow; 

    SortedByTimeImporter(ImportLines root) {
      this.root = root;
    }

    @Override
    public void close() throws IOException {
      root.close();
    }

    @Override
    public ImportLines withoutTrimming() {
      root = root.withoutTrimming();
      return this;
    }
    
    @Override
    public String[] nextLine() throws IOException {
      if (remainingLines == null) {
        String[] header = root.nextLine();
        if (header == null) {
          throw new IllegalStateException("File is empty");
        }
        
        int foundIndex = -1;
        
        // In the header row, find 
        for (int i = 0; i < header.length; i++) {
          if ("date".equals(CharMatcher.whitespace().removeFrom(header[i]).toLowerCase())) {
            foundIndex = i;
            break;
          }
        }
        if (foundIndex < 0) {
          throw new IllegalStateException("Could not find date column in Birda export.");
        }
        
        TreeMultimap<String, LineAndRowNumber> sortedLines = TreeMultimap.create();
        // Load all lines, sorting by the ISO date field
        int row = 0;
        while (true) {
          String[] line = root.nextLine();
          if (line == null) {
            break;
          }
          
          // Skip any line that doesn't have the ISO Date column.
          if (line.length <= foundIndex) {
            continue;
          }
          sortedLines.put(line[foundIndex], new LineAndRowNumber(line, ++row));
        }
        remainingLines = sortedLines.values().iterator();
        
        return header;
      } else {
        if (!remainingLines.hasNext()) {
          return null;
        }
        LineAndRowNumber next = remainingLines.next();
        currentRow = next.row;
        return next.line;
      }
    }

    @Override
    public int lineNumber() {
      return currentRow;
    }
  }
  
  static class LineAndRowNumber implements Comparable<LineAndRowNumber>{
    final String[] line;
    final int row;
    
    LineAndRowNumber(String[] line, int row) {
      this.line = line;
      this.row = row;
    }

    @Override
    public int compareTo(LineAndRowNumber that) {
      return row - that.row;
    }
  }

  // Instance variables used by finishSighting
  
  private String lastLocationId;
  private LocalTime lastStartTime;
  private ReadablePartial lastDate;

  @Override
  protected void finishSighting(
      Sighting.Builder newSighting,
      String[] line) {
    String locationId = newSighting.getLocationId();
    if (locationId == null) {
      return;
    }

    try {
      
      LocalTime localTime = timeExtractor.extract(line);
    
      LocalTime startTimeToSet = localTime;
      boolean resetVisit = false;
      if (lastLocationId == null) {
        // First row, reset the visit
        resetVisit = true;
      } else {
        // If we're still at the same location on the same day, then switch to
        // the start time.
        if (lastLocationId.equals(newSighting.getLocationId())
            && lastDate.equals(newSighting.getDate())) {
          startTimeToSet = lastStartTime;
        } else {
          resetVisit = true;
        }
      }

      newSighting.setTime(TimeIO.normalize(startTimeToSet));
      
      if (resetVisit) {
        lastLocationId = locationId;
        lastStartTime = startTimeToSet;
        lastDate = newSighting.getDate();
      }
    } catch (IllegalArgumentException e) {
      // ignore
    }
  }
  
  // Instance variables used by lookForVisitInfo and allSightingsFinished
  
  private VisitInfoKey lastVisitInfoKey = null;
  private LocalDateTime firstDateTime;
  private LocalDateTime lastDateTime;
  private LatLongCoordinates lastLatLong;
  private float accumulatedDistanceKm;
  private String sessionTitle;
  private ImmutableSet<ReadablePartial> onlyImportDatesIn = null;

  @Override
  protected void lookForVisitInfo(
      ReportSet reportSet,
      String[] line,
      Sighting newSighting,
      VisitInfoKey visitInfoKey) {
    LatLongCoordinates latLong = LatLongCoordinates.withLatAndLong(
        latitudeExtractor.extract(line),
        longitudeExtractor.extract(line));
    String isoDate = isoDateExtractor.extract(line);
    // Use the ISO date time here - the 
    LocalDateTime dateTime = ISO_DATE_TIME_FORMATTER.parseLocalDateTime(isoDate);
    
    boolean resetVisitInfo = false;
    if (lastVisitInfoKey == null) {
      resetVisitInfo = true;
    } else if (lastVisitInfoKey.equals(visitInfoKey)) {
      // Still on the same visit;  increment the total distance
      accumulatedDistanceKm += latLong.kmDistance(lastLatLong);
    } else {
      // Call all-sightings-finished, which finishes populating the visit info
      allSightingsFinished();
      resetVisitInfo = true;
    }

    lastLatLong = latLong;
    lastDateTime = dateTime;

    if (resetVisitInfo) {
      lastVisitInfoKey = visitInfoKey;
      accumulatedDistanceKm = 0;
      firstDateTime = dateTime;
      sessionTitle = sessionTitleExtractor.extract(line);
    }
  }
  
  @Override
  protected void allSightingsFinished() {
    // Record the accumulated distance and time, and finalize the visit info
    VisitInfo.Builder visitInfo = VisitInfo.builder().withObservationType(ObservationType.HISTORICAL);
    if (accumulatedDistanceKm > 0.010) {
      visitInfo.withDistance(Distance.inKilometers(accumulatedDistanceKm));
    }
    
    Duration duration = Minutes.minutesBetween(firstDateTime, lastDateTime).toStandardDuration();
    if (duration.isLongerThan(Duration.standardMinutes(5))) {
      visitInfo.withDuration(duration);
    }
    if (!Strings.isNullOrEmpty(sessionTitle)) {
      visitInfo.withComments(sessionTitle);
    }
    visitInfoMap.put(lastVisitInfoKey, visitInfo.build());
  }

  @Override
  public Map<VisitInfoKey, VisitInfo> getAccumulatedVisitInfoMap() {
    return visitInfoMap;
  }

  @Override
  public boolean isMassExport() {
    // Birda only supports mass exports; let Scythebill import more carefully.
    return true;
  }

  /**
   * Must be overridden if isMassExport() returns true.
   */
  @Override
  public Collection<ReadablePartial> getAllDates() throws IOException {
    try (ImportLines lines = importLines(locationsFile)) {
      computeMappings(lines);
      
      Set<ReadablePartial> allDates = new LinkedHashSet<>();
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }
        LocalDateTime localDateTime = localDateTimeExtractor.extract(line);
        if (localDateTime != null) {
          allDates.add(localDateTime.toLocalDate());
        }
      }
      return allDates;
    }
  }

  /** Must be overridden and honored if isMassExport() returns true. */
  @Override
  public void onlyImportDatesIn(Set<ReadablePartial> newDates) {
    this.onlyImportDatesIn = ImmutableSet.copyOf(newDates);
  }  
}
