/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.util.List;

import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;

/**
 * Location codes;  these are known to be accurate for Birdbase, and are suspected to be accurate for Avisys.
 * At a minimum, it's known that Avisys used "GS" for the Galapagos, and "MS" for "Leeward Islands", which
 * match Birdbase.
 */
class BirdbaseAndAvisysLocations {
  private final static ImmutableMap<String, String> BIRDBASE_LOCATION_ABBREVIATIONS = ImmutableMap.<String, String>builder()
      .put("BU", "Myanmar") // Myanmar
      .put("EN", "Estonia") // Estonia
      .put("CS", "Indonesia-Maluku") // Maluku
      .put("IC", "Canary Islands") // Canary Islands
      .put("SW", "Indonesia-Sulawesi") // Sulawesi
      .put("YU", "Serbia") // Any of Serbia, Montenegro, or Kosovo
      .put("GS", "Galapagos Islands")
      .put("RS", "Russia")
      .put("RSE", "Russia") // East
      .put("RSW", "Russia") // West
      .put("RA", "Socotra")
      .put("HV", "Burkina Faso")
      .put("ZR", "Democratic Republic of the Congo")
      .put("TS", "Australia-Tasmania")
      .put("TP", "East Timor") // Not necessarily "East"
      .put("DB", "Seychelles-Aldabra")
      .put("RZ", "Mauritius-Rodrigues Island")
      .put("MXN", "Mexico")
      .put("MXS", "Mexico")
      .put("USM", "United States")
      .put("RL", "Papua New Guinea-Manus")
      // Really, the whole Bismarcks... so this is poor, but seems to get things at least *moderately* close,
      // and West New Britain is probably the first place you'd bird in the Bismarcks?
      .put("MK", "Papua New Guinea-West New Britain")
      .put("CT", "Kiribati-Phoenix Islands")
      .put("RM", "Marshall Islands")
      .put("IP", "Easter Island")
      .put("JT", "United States Minor Outlying Islands-Johnston Atoll")
      .put("MI", "United States Minor Outlying Islands-Midway Islands")
      .put("WK", "United States Minor Outlying Islands-Wake Island")
      .put("PU", "Palau") // ... and the Carolines, so not technically correct
      .put("AI", "India-Andaman and Nicobar Islands")
      .put("IB", "Malaysia-Sabah") // Could be Sarawak too, ah well
      .put("CNS", "China") // Southern
      .put("CNN", "China") // Northern
      .put("SS", "Indonesia-Nusa Tenggara")
      .put("ID", "Indonesia-Jawa")
      .put("TA", "Indonesia-Sumatera")
      .put("BL", "Belarus")
      .put("RT", "Croatia")
      .put("GG", "Georgia")
      .put("GZ", "Kyrgyzstan")
      .put("LN", "Lithuania")
      .put("ME", "Macedonia")
      .put("AZ", "Azores")
      .put("MD", "Madeira")
      .put("DV", "Moldova")
      .put("ZS", "Tajikistan")
      .put("UR", "Ukraine")
      .put("BK", "Uzbekistan")
      .put("UH", "United States") // Avisys abbreviation for "US-Hawaii"
      .put("ZH", "Saint Helena, Ascension, and Tristan da Cunha-Saint Helena") // Could also be Ascension Island
      .put("ZC", "Saint Helena, Ascension, and Tristan da Cunha-Tristan da Cunha")
      .put("AA", "Antarctica")
      .put("AN", "Caribbean Netherlands")
      .build();
  
  /**
   * A map from a location ID to a set of location IDs whose checklists should be assembled.
   * When judging taxonomic uncertainy, the checklist for the sum of the locations should
   * be considered to produce the best results.  
   */
  private final static ImmutableMultimap<String, String> LOCATION_TO_CHECKLIST_LOCATIONS = ImmutableMultimap.<String, String>builder()
      // Montserrat could be Antigua and Barbuda, St. Kitts and Nevis, Anguilla, Saint Barthelemy, or the Caribbean Netherlands (West Indies)
      .putAll("MS", "MS", "AG", "KN", "AI", "BL", "BQ")
      // Papua New Guinea could be Indonesian Papua
      .putAll("PG", "PG", "ID-IJ")
      // Serbia could be Montenegro or Kosovo
      .putAll("RS", "RS", "ME", "XK")
      // Saint Helena could be Ascension Islands
      .putAll("SH-SH", "SH-SH", "SH-AC")
      // Palau could be Micronesia
      .putAll("PU", "PU", "FM")
      // East Timor could be Nusa Tenggara in Indonesia
      .putAll("TP", "TP", "ID-NU")
      // Java could also be Nusa Tenggara
      .putAll("ID-JW", "ID-JW", "ID-NU")
      // Nusa Tenggara could also be Maluku
      .putAll("ID-NU", "ID-NU", "ID-MA")
      .build();
  
  private final static ImmutableMap<String, String> BIRDBASE_REGION_ABBREVIATIONS = ImmutableMap.<String, String>builder()
      .put("ZT", "Atlantic Ocean") // Could also be Arctic Ocean
      .put("ZI", "Indian Ocean")
      .put("ZP", "Pacific Ocean")
      .put("ZO", "South Polar Region")
      .put("YF", "Africa") // Afrotropical
      .put("YA", "Australasia") // Australasian
      .put("YM", "Indian Ocean") // Malagasy Zone
      .put("YN", "North America") // Nearctic
      .put("YT", "South America") // Neotropical - could also be Central America
      .put("YI", "Pacific Ocean") // Oceania
      .put("YO", "Asia") // Oriental Zone
      .put("YP", "Eurasia") // Palearctic 
      .build();
  
  static ImmutableCollection<String> getMergedLocations(String locationCode) {
    return LOCATION_TO_CHECKLIST_LOCATIONS.get(locationCode);
  }
  
  static void updateImportedLocation(ImportedLocation imported, String countryCode) {
    if (BIRDBASE_REGION_ABBREVIATIONS.containsKey(countryCode)) {
      imported.region = BIRDBASE_REGION_ABBREVIATIONS.get(countryCode);
    } else if (BIRDBASE_LOCATION_ABBREVIATIONS.containsKey(countryCode)) {
      String countryAndMaybeState = BIRDBASE_LOCATION_ABBREVIATIONS.get(countryCode);
      List<String> split = Splitter.on('-').splitToList(countryAndMaybeState);
      imported.country = split.get(0);
      if (split.size() > 1) {
        imported.state = split.get(1);
      }
      if ("CS".equals(countryCode)) {
        imported.region = "Australasia";
      } else if ("RSW".equals(countryCode)) {
        imported.region = "Europe";
      } else if ("RSE".equals(countryCode)) {
        imported.region = "Asia";
      } else if ("UH".equals(countryCode)) {
        imported.region = "Pacific Ocean";
      }
    } else {
      imported.countryCode = countryCode;
    }
    
  }
}
