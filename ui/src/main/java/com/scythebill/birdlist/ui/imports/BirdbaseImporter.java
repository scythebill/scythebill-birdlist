/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.checklist.TransposedChecklistSynthesizer;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.io.MergingImportLines;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Imports sightings from BirdBase files.
 */
public class BirdbaseImporter extends CsvSightingsImporter {
  private final static int DESCRIPTION_COLUMN_INDEX = 23;
  private final static int NUMBER_COLUMN_INDEX = 24;
  private final static CharMatcher NULL = CharMatcher.is((char) 0);

  private final LineExtractor<String> idExtractor = LineExtractors.joined(
      Joiner.on('|').useForNull(""),
      LineExtractors.stringFromIndex(8),
      LineExtractors.stringFromIndex(9),
      LineExtractors.stringFromIndex(16),
      LineExtractors.stringFromIndex(17),
      LineExtractors.stringFromIndex(18));

  private final LineExtractor<String> taxonomyIdExtractor = LineExtractors.joined(
      Joiner.on('|').useForNull(""),
      new TrimCommonName(LineExtractors.stringFromIndex(0)),
      LineExtractors.stringFromIndex(1),
      LineExtractors.stringFromIndex(2),
      new SubspeciesExtractor(LineExtractors.stringFromIndex(DESCRIPTION_COLUMN_INDEX)));
  private final TransposedChecklistSynthesizer transposedChecklistSynthesizer;
  private final Checklists checklists;
  
  /**
   * Returns a custom string, if and only if a field contains a "1".  Used for
   * custom BirdBase fields.
   */
  static class CustomBitExtractor implements LineExtractor<String> {
    private final int index;
    private final String ifPresent;

    CustomBitExtractor(int index, String ifPresent) {
      this.index = index;
      this.ifPresent = ifPresent;
    }
    
    @Override
    public String extract(String[] line) {
      if (line.length < index + 1) {
        return null;
      }
      
      if ("1".equals(line[index])) {
        return ifPresent;
      }
      
      return null;
    }
  }
  
  static class StatusExtractor implements LineExtractor<String> {
    private final LineExtractor<Integer> uncertainExtractor;

    StatusExtractor(LineExtractor<Integer> uncertainExtractor) {
      this.uncertainExtractor = uncertainExtractor;
    }
    
    @Override
    public String extract(String[] line) {
      Integer uncertain = uncertainExtractor.extract(line);
      if (uncertain != null) {
        // "2" means an uncertain sighting.  Scythebill supports a richer status of statuses,
        // but this'll have to do.
        if (uncertain == 2) {
          return "uncertain";
        }
      }
      return null;
    }
    
  }
  
  static class DropIfMatchesCount implements LineExtractor<String> {

    private final LineExtractor<String> mainExtractor;
    private final LineExtractor<String> countExtractor;

    DropIfMatchesCount(
        LineExtractor<String> main,
        LineExtractor<String> count) {
      this.mainExtractor = main;
      this.countExtractor = count;
    }

    @Override
    public String extract(String[] line) {
      String main = mainExtractor.extract(line);
      if (main == null) {
        return null;
      }
      
      String count = countExtractor.extract(line);
      if (main.equals(count)) {
        return null;
      }
      
      return main;
    }

  }

  static class TrimCommonName implements LineExtractor<String> {
    private final CharMatcher ASTERISK = CharMatcher.is('*');
    private final LineExtractor<String> extractor;

    TrimCommonName(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public String extract(String[] line) {
      String extract = extractor.extract(line);
      if (Strings.isNullOrEmpty(extract)) {
        return extract;
      }
      // Remove trailing asterisks
      extract = ASTERISK.removeFrom(extract);
      // Remove parenthetical phrases
      while (extract.indexOf('(') >= 0) {
        int leftParen = extract.indexOf('(');
        int rightParen = extract.indexOf(')', leftParen);
        extract = extract.substring(0,  leftParen) + extract.substring(rightParen + 1);
      }
      
      // ... and collapse any consecutive whitespace
      extract = CharMatcher.whitespace().collapseFrom(extract, ' ');
      return extract;
    }
  }
  
  static class SubspeciesExtractor implements LineExtractor<String> {
    private final LineExtractor<String> extractor;

    SubspeciesExtractor(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public String extract(String[] line) {
      String extract = extractor.extract(line);
      if (Strings.isNullOrEmpty(extract)) {
        return null;
      }
      
      int period = extract.indexOf('.');
      if (period < 0) {
        return null;
      }
      
      String maybeSubspecies = extract.substring(0, period);
      if (!CharMatcher.inRange('a', 'z').matchesAllOf(maybeSubspecies)) {
        return null;
      }
      
      return maybeSubspecies;
    }
  }

  public BirdbaseImporter(ReportSet reportSet, Taxonomy taxonomy, Checklists checklists,
      PredefinedLocations predefinedLocations,
      TransposedChecklistSynthesizer transposedChecklistSynthesizer, File file) {
    super(reportSet, taxonomy, checklists, predefinedLocations, file, file);
    this.checklists = checklists;
    this.transposedChecklistSynthesizer = transposedChecklistSynthesizer;
  }
  
  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return taxonomyIdExtractor;
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    return new FieldTaxonImporter<>(taxonomy,
        new TrimCommonName(LineExtractors.stringFromIndex(0)),
        LineExtractors.joined(Joiner.on(' ').useForNull(""),
            LineExtractors.stringFromIndex(1),
            LineExtractors.stringFromIndex(2)),
            new SubspeciesExtractor(LineExtractors.stringFromIndex(DESCRIPTION_COLUMN_INDEX)));
  }
       
  /** Canadian provinces, in lowercase. */
  private final static ImmutableSet<String> CANADA_PROVINCES = ImmutableSet.of(
      "ab",
      "bc",
      "mb",
      "nb",
      "nf",
      "nl",
      "nn",
      "nt",
      "ns",
      "nu",
      "nw",
      "on",
      "pe",
      "qc",
      "qu",
      "sk",
      "yk");
    
  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    ImportLines lines = importLines(locationsFile);
    
    LineExtractor<String> locationExtractor = LineExtractors.stringFromIndex(9);
    LineExtractor<String> birdbaseHomeExtractor = LineExtractors.stringFromIndex(18); 
    LineExtractor<String> locationNamesExtractor = LineExtractors.stringFromIndex(8); 
    LineExtractor<String> birdbaseRegionExtractor = LineExtractors.stringFromIndex(16); 
    LineExtractor<String> birdbaseLocalExtractor = LineExtractors.stringFromIndex(17); 
    try {
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }
        
        String id = idExtractor.extract(line);
        if (locationIds.hasBeenParsed(id)) {
          continue;
        }

        // Location has a country code or a lowercase state/province
        ImportedLocation imported = new ImportedLocation();
        String location = Strings.emptyToNull(locationExtractor.extract(line));
        if (location != null) {
          if (CharMatcher.inRange('a', 'z').matchesAllOf(location)) {
            if (CANADA_PROVINCES.contains(location)) {
              imported.countryCode = "CA";
              // A few canadian provinces have incorrect codes in BirdBase
              if (location.equals("nn")) {
                imported.stateCode = "NU";
              } else if (location.equals("nf")) {
                imported.stateCode = "NL";
              } else if (location.equals("nw")) {
                imported.stateCode = "NT";
              } else if (location.equals("qu")) {
                imported.stateCode = "QC";
              } else {
                imported.stateCode = location.toUpperCase();
              }
            } else {
              imported.countryCode = "US";
              imported.stateCode = location.toUpperCase();
            }
          } else {
            BirdbaseAndAvisysLocations.updateImportedLocation(imported, location);
          }
        }
        
        if ("1".equals(birdbaseHomeExtractor.extract(line))) {
          imported.locationNames.add("BirdBase Home");
        }
        
        if (!Strings.isNullOrEmpty(locationNamesExtractor.extract(line))) {
          imported.locationNames.add(locationNamesExtractor.extract(line));
        }

        // These seem more of a mess than useful, unless there's no other data.
        if (imported.locationNames.isEmpty()) {
          if ("1".equals(birdbaseRegionExtractor.extract(line))) {
            imported.locationNames.add("BirdBase Region");
          } else if ("1".equals(birdbaseLocalExtractor.extract(line))) {
            imported.locationNames.add("BirdBase Local");
          }
        }
        
        String locationId = imported.addToLocationSet(reportSet, locations, locationShortcuts, predefinedLocations);
        locationIds.put(id, locationId);
      }
    } catch (IOException | RuntimeException e) {
      throw new ImportException(
          Messages.getFormattedMessage(Name.FAILED_IMPORT_ON_LINE_FORMAT, lines.lineNumber()),
          e);
    } finally {
      lines.close();
    }
  }
  
  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
        
    mappers.add(new DateFromYearMonthDayFieldMapper<>(
        LineExtractors.intFromIndex(11),
        LineExtractors.intFromIndex(12),
        LineExtractors.intFromIndex(13)));
    // Only needed if S1-S4 aren't automatically included in the text.
    mappers.add(new DescriptionFieldMapper<>(
          LineExtractors.joined(Joiner.on(' ').skipNulls(),
              // And don't bother including a description which just 
              // repeats the number.
              new DropIfMatchesCount(
                  LineExtractors.stringFromIndex(DESCRIPTION_COLUMN_INDEX),
                  LineExtractors.stringFromIndex(NUMBER_COLUMN_INDEX)),
              // Append "S1" - "S4"
              new CustomBitExtractor(19, "S1"),
              new CustomBitExtractor(20, "S2"),
              new CustomBitExtractor(21, "S3"),
              new CustomBitExtractor(22, "S4"))));
    // Column 4 is "1" if a first sighting (we don't care here), "2" if uncertain, and "0" otherwise
    mappers.add(new StatusFieldMapper<>(new StatusExtractor(LineExtractors.intFromIndex(3))));
    mappers.add(new CountFieldMapper<>(LineExtractors.stringFromIndex(NUMBER_COLUMN_INDEX)));
    return new ComputedMappings<>(
        new TaxonFieldMapper(taxonomyIdExtractor),
        new LocationMapper(idExtractor),
        mappers);
  }

  @Override
  protected Charset getCharset() {
    return Charsets.ISO_8859_1;
  }

  
  @Override
  protected Checklist getBuiltInChecklist(Location location) {
    String locationCode = checklists.getLocationWithNearestBuiltInChecklist(location);
    if (locationCode != null) {
      ImmutableCollection<String> locations = BirdbaseAndAvisysLocations.getMergedLocations(locationCode);
      if (locations != null && !locations.isEmpty()) {
        return transposedChecklistSynthesizer.synthesizeChecklistInternal(reportSet, locations);
      }
    }
    return super.getBuiltInChecklist(location);
  }

  @Override
  protected ImportLines importLines(File file) throws IOException {
    // The BirdBase format splits long descriptions across multiple lines.
    // Identify and merge those here, so that most of the code doesn't care.
    // .. and don't trust BirdBase to properly escape quotes 
    // Also, ignore quotes that come in the middle of quotes (and aren't followed by a separator),
    // because BirdBase totally botches these!
    // (E.g. "There were 8" of snow", where BirdBase *should have 8\" but doesn't.  Argh.
    ImportLines importLines = CsvImportLines.fromFileNoMultiIgnoringQuotesNotFollowedBySeparators(file, getCharset());
    return new MergeDescriptionLines(importLines);
  }
  
  static class MergeDescriptionLines extends MergingImportLines {
    MergeDescriptionLines(ImportLines importLines) {
      super(importLines);
    }

    @Override
    protected boolean merge(String[] mergedLine, String[] nextLine) {
      if (!containsOnlyADescription(nextLine)) {
        return false;
      }
      
      // Merge the description from the next line.
      mergedLine[DESCRIPTION_COLUMN_INDEX] =
          mergedLine[DESCRIPTION_COLUMN_INDEX]
          + " "
          + nextLine[DESCRIPTION_COLUMN_INDEX];
      return true;
    }
    
    /** Anything that is blank until the description column is empty. */
    private boolean containsOnlyADescription(String[] line) {
      if (line.length <= DESCRIPTION_COLUMN_INDEX) {
        return false;
      }
      
      for (int i = 0; i < DESCRIPTION_COLUMN_INDEX; i++) {
        if (!Strings.isNullOrEmpty(line[i])) {
          return false;
        }
      }
      
      // HACK: if there is trailing text after the description column, then
      // BirdBase munged the encoding;  merge text together.
      String description = line[DESCRIPTION_COLUMN_INDEX];
      for (int i = DESCRIPTION_COLUMN_INDEX + 1; i < line.length; i++) {
        if (!Strings.isNullOrEmpty(line[i])) {
          description += "," + line[1];
        }
      }
      line[DESCRIPTION_COLUMN_INDEX] = description;
      
      return true;
    }
    
    @Override
    protected String[] nextLineInternal() throws IOException {
      String[] line = super.nextLineInternal();
      if (line != null) {
        for (int i = 0; i < line.length; i++) {
          // Birdbase can produce null characters:
          // https://bitbucket.org/scythebill/scythebill-birdlist/issues/292/empty-locations-sometimes-duplicated
          // Strip all nulls from the output
          line[i] = NULL.removeFrom(line[i]);
        }
      }
      return line;      
    }

    @Override
    protected boolean neverMergeFirstLine() {
      // Merging the first line is fine;  there's no headers.
      return false;
    }
    
    
  }
}
