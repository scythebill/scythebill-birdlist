/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.google.common.base.Charsets;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.sigpwned.chardet4j.Chardet;

/**
 * Utilities shared among Birder's Diary.
 */
final class BirdersDiary {
  /**
   * Choose an importer for a specific file.
   */
  public static CsvSightingsImporter chooseImporter(File openFile, ReportSet reportSet,
      Taxonomy taxonomy, Checklists checklists, PredefinedLocations predefinedLocations) {
    
    try (ImportLines importLines = CsvImportLines.fromReader(Chardet.decode(new FileInputStream(openFile), Charsets.ISO_8859_1))) {
      List<String> nextLine = Arrays.asList(importLines.nextLine());
      if (!nextLine.isEmpty() && nextLine.contains("Name")) {
        return new BirdersDiaryImporter(reportSet, taxonomy, checklists, predefinedLocations, openFile);
      } else {
        return new BirdersDiary5Importer(reportSet, taxonomy, checklists, predefinedLocations, openFile);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

}
