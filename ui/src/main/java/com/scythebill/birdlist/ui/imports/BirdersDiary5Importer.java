/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.joda.time.chrono.GJChronology;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.io.MergingImportLines;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.imports.ParsedLocationIds.ToBeDecided;
import com.scythebill.birdlist.ui.imports.ParsedLocationIds.ToBeDecided.HintType;
import com.sigpwned.chardet4j.Chardet;

/**
 * Imports from Birder's Diary 5 format imports.  Not *really* sure when this importer
 * is used vs. the other one, to be honest - it's very different!
 */
public class BirdersDiary5Importer extends CsvSightingsImporter {

  private LineExtractor<String> commonOrSciExtractor;
  private LineExtractor<String> dateExtractor;
  private LineExtractor<String> locationExtractor;
  private LineExtractor<String> parentLocationExtractor;
  
  // Observer name handling, an ugly (but documented) hack:
  // Birder's Diary will output the same record, repeatedly, once for each observer.
  // To make processing easier, Scythebill will merge these into a single line containing
  // all the observer data.  But Birder's Diary *also* separates the name into three fields.
  // So merging that into a single row would mean that there'd be merged first names,
  // merged last names, merged  middle initials.  Which is ugly.
  //
  // So instead we do a first pass - merging the name into a single field immediately
  // after reading (where the first name would go).
  //
  // Then the second pass is merging those names if they're for the same species/etc.
  //
  // Then the actual FieldMapper receives the merged names.
  private LineExtractor<String> observerNameExtractor;
  private LineExtractor<String> mergedObserverNameExtractor;
  private Integer firstNameIndex;
  
  private LineExtractor<String> observerInitialsExtractor;
  private LineExtractor<String> locationIdExtractor;
  private LineExtractor<String> taxonomyIdExtractor;
  private Set<String> observerSet = new HashSet<>();
  private boolean multipleObservers = false;
  private boolean hadObservers;
  private Integer observerInitialsIndex;
  private LineExtractor<String> tripExtractor;
  private List<DateTimeFormatter> chosenFormatters;
  
  public BirdersDiary5Importer(ReportSet reportSet, Taxonomy taxonomy, Checklists checklists,
      PredefinedLocations predefinedLocations, File file) {
    super(reportSet, taxonomy, checklists, predefinedLocations, file, file);
  }

  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return taxonomyIdExtractor;
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    return new FieldTaxonImporter<>(taxonomy, commonOrSciExtractor);
  }

  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    String[] header = lines.nextLine();
    Map<String, Integer> headersByIndex = new LinkedHashMap<>();
    for (int i = 0; i < header.length; i++) {
      headersByIndex.put(header[i].toLowerCase(), i);
    }

    int commonOrSciIndex = getRequiredHeader(headersByIndex, "SciOrCommonName");
    int dateIndex = getRequiredHeader(headersByIndex, "DateAndTime");
    int locationIndex = getRequiredHeader(headersByIndex, "Location");
    int parentLocationIndex = getRequiredHeader(headersByIndex, "ParentLocation");
    firstNameIndex =  headersByIndex.get("firstname");
    Integer middleInitialIndex =  headersByIndex.get("mi");
    Integer lastNameIndex = headersByIndex.get("lastname");
    observerInitialsIndex = headersByIndex.get("initials");
    Integer commentsIndex = headersByIndex.get("sightingcomments");
    Integer tripIndex = headersByIndex.get("trip");
    Integer countIndex = headersByIndex.get("maxobserved");
    
    commonOrSciExtractor = LineExtractors.stringFromIndex(commonOrSciIndex);
    dateExtractor = LineExtractors.stringFromIndex(dateIndex);
    locationExtractor = LineExtractors.stringFromIndex(locationIndex);
    parentLocationExtractor = LineExtractors.stringFromIndex(parentLocationIndex);
    tripExtractor = tripIndex == null
        ? LineExtractors.<String>alwaysNull() : LineExtractors.stringFromIndex(tripIndex);
    LineExtractor<String> commentsExtractor = commentsIndex == null 
        ? LineExtractors.<String>alwaysNull() : LineExtractors.stringFromIndex(commentsIndex);
        
    // See comments above observerNameExtractor field
    if (firstNameIndex == null || lastNameIndex == null) {
      mergedObserverNameExtractor = LineExtractors.<String>alwaysNull();
    } else {
      observerNameExtractor = LineExtractors.joined(
          Joiner.on(' ').skipNulls(),
          LineExtractors.stringFromIndex(firstNameIndex),
          middleInitialIndex == null ? LineExtractors.<String>alwaysNull() : LineExtractors.stringFromIndex(middleInitialIndex),
          LineExtractors.stringFromIndex(lastNameIndex));
      mergedObserverNameExtractor = LineExtractors.stringFromIndex(firstNameIndex);
    }
    observerInitialsExtractor = observerInitialsIndex == null ? LineExtractors.<String>alwaysNull()
        : LineExtractors.stringFromIndex(observerInitialsIndex);

    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
    // TODO: would be Nice-To-Have to extract the times, look for a range of times at a location,
    // convert it into a start-time and a duration, etc.  But that'd only affect users who set
    // times and really care about having that preserved.
    // (Another possibility is looking for the time and adding it to the sighting description.)
    mappers.add(
        new MultiFormatDateFromStringFieldMapper<>(
            dateExtractor,
            () -> this.chosenFormatters));
    LineExtractor<String> descriptionExtractor = new LineExtractor<String>() {
      @Override
      public String extract(String[] line) {
        StringBuilder builder = new StringBuilder();
        String trip = tripExtractor.extract(line);
        // Include the trip, which doesn't have a different use here
        if (!Strings.isNullOrEmpty(trip)) {
          builder.append(trip);
        }
        
        String comment = commentsExtractor.extract(line);
        if (!Strings.isNullOrEmpty(comment)
            // Ignore RTF
            && !comment.startsWith("{\\rtf1")) {
          if (builder.length() > 0) {
            builder.append('\n');
          }
          builder.append(comment);
        }
        return builder.toString();
      }
    };
    mappers.add(new DescriptionFieldMapper<>(descriptionExtractor));
    locationIdExtractor = LineExtractors.joined(
        Joiner.on('|'),
        locationExtractor, parentLocationExtractor);
    taxonomyIdExtractor = commonOrSciExtractor;
    
    // If preSightingAnalysis() found observers, and:
    // - It never found multiple observers in a single sighting
    // - It didn't find different observers for different sightings
    // - The user hasn't already enabled multiple observers
    // ... then skip the UserFieldMapper altogether.
    if (observerSet.size() != 1 || multipleObservers || reportSet.getUserSet() != null) {
      hadObservers = true;
      // See comments above observerNameExtractor field
      mappers.add(new UserFieldMapper<>(reportSet, observerInitialsExtractor, mergedObserverNameExtractor));
    }
    
    if (countIndex > 0) {
      mappers.add(new CountFieldMapper<>(LineExtractors.stringFromIndex(countIndex)));
    }

    return new ComputedMappings<>(
        new TaxonFieldMapper(taxonomyIdExtractor),
        new LocationMapper(locationIdExtractor),
        mappers);
  }

  private static final Splitter PARENT_LOCATION_SPLITTER = Splitter.on('>');
  
  private static final ImmutableMap<String, String> PARENT_LOCATION_REPLACE = ImmutableMap.<String, String>builder()
      .put("CONTINENTAL UNITED STATES>LOWER 48 UNITED STATES", "United States")
      .put("CONTINENTAL UNITED STATES", "United States")
      .put("UNITED STATES OF AMERICA", "United States")
      .put("SOUTH POLAR", "South Polar Region")
      .put("ABA LOCATIONS>ABA NORTH AMERICAN REGION>ABA WEST INDIES AND CARIBBEAN", "North America>West Indies")
      .build();
      
  private static final ImmutableMap<String, String> ANY_LOCATION_REPLACE = ImmutableMap.<String, String>builder()
      .put("United States>HAWAII", "Pacific Ocean>United States>Hawaii")
      .put("NETHERLANDS ANTILLES", "Caribbean Netherlands")
      .put("DEMOCRATIC REP. OF THE CONGO", "Democratic Republic of the Congo")
      .put("SPANISH NORTH AFRICA", "Ceuta and Melilla")
      .put("UNITED REPUBLIC OF TANZANIA", "Tanzania")
      .put("SVALBARD AND JAN MAYEN ISLANDS", "Svalbard")
      .put("CHINA>HONG KONG", "Hong Kong")
      .put("SOUTH AMERICA>ECUADOR>GALAPAGOS ISLANDS", "Pacific Ocean>Galapagos Islands")
      .put("REPUBLIC OF GEORGIA", "Georgia")
      .put("REPUBLIC OF KOREA (SOUTH KOREA)", "South Korea")
      .put("DEMOCRATIC PEOPLE'S REPUBLIC OF KOREA (NORTH KOREA)", "North Korea")
      .put("BOUVET", "Bouvet Island")
      .put("REPUBLIC OF MOLDOVA", "Moldova")
      .put("KOSOVA", "Kosovo")
      .put("COMORO ISLANDS", "Comoros")
      .put("RODRIGUES ISLAND", "Mauritius>Rodrigues Island")
      .put("VIRGIN ISLANDS, BRITISH", "British Virgin Islands")
      .put("VIRGIN ISLANDS, USA", "United States>Virgin Islands (U.S.)")
      .put("PUERTO RICO", "United States>Puerto Rico")
      .put("FED. STATES OF MICRONESIA", "Micronesia")
      .put("NORTHERN MARINA IS.", "Northern Mariana Islands")
      .put("IRIAN JAYA", "Papua")
      .put("MALUKU ISLANDS", "Maluku")
      .put("JAVA", "Jawa")
      .put("SUMATRA", "Sumatera")
      .put("LESSER SUNDAS", "Nusa Tenggara")
      .put("SULAWESI (CELEBES)", "Sulawesi")
      .put("RUSSIA (Europe)", "Russia")      
      .put("RUSSIA (Asia)", "Russia")      
      .put("TURKEY (Europe)", "Turkey")      
      .put("TURKEY (Asia)", "Turkey")      
      .put("EGYPT (Africa)", "Egypt")      
      .put("EGYPT (Asia)", "Egypt")      
      .put("INDONESIA (Asia)", "Indonesia")      
      .put("INDONESIA (Australasia)", "Indonesia")      
      .put("MYANMAR (Burma)", "Myanmar")      
      .put("SOUTH GEORGIA ISLAND", "South Georgia and South Sandwich Islands>South Georgia Island")      
      .put("SOUTH SANDWICH ISLANDS", "South Georgia and South Sandwich Islands>South Sandwich Islands")
      .put("ANDAMAN ISLANDS", "India>Andaman and Nicobar Islands>Andaman Islands")      
      .put("NICOBAR ISLANDS", "India>Andaman and Nicobar Islands>Nicobar Islands")      
      .put("IRELAND>NORTHERN IRELAND", "United Kingdom>Northern Ireland")      
      .put("IRELAND>REPUBLIC OF IRELAND", "Ireland")      
      .build();

  private final static CharMatcher CAPS = CharMatcher.inRange('A', 'Z');
  private final static CharMatcher ALL_CAPS_NAMES = CAPS.or(CharMatcher.anyOf(" .,()"));

  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    
    try (ImportLines lines = importLines(locationsFile)) {
      // Skip the header
      lines.nextLine();
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }
        String id = locationIdExtractor.extract(line);
        if (locationIds.hasBeenParsed(id)) {
          continue;
        }

        String leafLocation = locationExtractor.extract(line);
        String parentLocation = parentLocationExtractor.extract(line);
        
        if (Strings.isNullOrEmpty(parentLocation)) {
          ToBeDecided toBeDecided = new ToBeDecided(leafLocation, null, tripExtractor.extract(line), HintType.location);
          locationIds.addToBeResolvedLocationName(id, toBeDecided);
        } else {
          // Do some parent location normalization
          for (Map.Entry<String, String> entry : PARENT_LOCATION_REPLACE.entrySet()) {
            parentLocation = parentLocation.replace(entry.getKey(), entry.getValue());
          }
          
          // Then do some more normalization after glueing the leaf location in,
          // so we can tweak the whole thing as one.
          String wholeLocation = parentLocation + ">" + leafLocation;          
          for (Map.Entry<String, String> entry : ANY_LOCATION_REPLACE.entrySet()) {
            wholeLocation = wholeLocation.replace(entry.getKey(), entry.getValue());
          }
          
          // Test Northern Ireland
          // Test Ireland
          
          List<String> locationNames = PARENT_LOCATION_SPLITTER.splitToList(wholeLocation);
          ImportedLocation imported = new ImportedLocation();
          
          boolean foundNonRegion = false;
          for (String location : locationNames) {
            location = BirdersDiary5Importer.toInitialCaps(location);
            if (!foundNonRegion) {
              if (locationShortcuts.getRegion(location) == null) {
                foundNonRegion = true;
              } else {
                imported.region = location;
                continue;
              }
            }
            
            if (imported.country == null) {
              imported.country = location;
              continue;
            }
            
            imported.locationNames.add(location);
          }

          String locationId = imported.addToLocationSet(reportSet, reportSet.getLocations(), locationShortcuts,
              predefinedLocations);
          locationIds.put(id, locationId);
        }        
      }
    }
    
  }

  @Override
  protected String[] getHeaderRow(ImportLines lines) throws IOException {
    return lines.nextLine();
  }

  @Override
  protected Charset getCharset() {
    throw new UnsupportedOperationException("Need to use per-file detection");
  }
  
  class MergeObserversImportLines extends MergingImportLines {
    MergeObserversImportLines(ImportLines importLines) {
      super(importLines);
    }

    @Override
    protected String[] nextLineInternal() throws IOException {
      String[] line = super.nextLineInternal();
      // See comments above observerNameExtractor field
      if (line != null && firstNameIndex != null && observerNameExtractor != null) {
        line[firstNameIndex] = observerNameExtractor.extract(line);
      }
      return line;
      
    }
    
    @Override
    protected boolean merge(String[] mergedLine, String[] nextLine) {
      // Don't bother merging until the observer indices have been found
      if (firstNameIndex == null || mergedObserverNameExtractor == null) {
        return false;
      }
      
      // And skip any short lines that don't have the first name index present
      // (where we store the first name)
      if (nextLine.length <= firstNameIndex || mergedLine.length <= firstNameIndex) {
        return false;
      }
      
      // Also need these to see if it's "the same"
      if (commonOrSciExtractor == null || dateExtractor == null || locationIdExtractor == null) {
        return false;
      }
      // If anything about those two lines is different, they shouldn't be merged. 
      // (Ignoring "trip" here, which is maybe wrong, but hardly seems relevant?  Ignoring
      // comment is very intentional.)
      if (!Objects.equals(commonOrSciExtractor.extract(nextLine), commonOrSciExtractor.extract(mergedLine))
          || !Objects.equals(dateExtractor.extract(nextLine), dateExtractor.extract(mergedLine))
          || !Objects.equals(locationIdExtractor.extract(nextLine), locationIdExtractor.extract(mergedLine))) {
        return false;
      }

      // Merge in the next "who" if necessary.
      String nextName = mergedObserverNameExtractor.extract(nextLine);
      if (!Strings.isNullOrEmpty(nextName)) {
        mergedLine[firstNameIndex] = mergedLine[firstNameIndex] + "\n" + nextName;
      }
      
      if (observerInitialsExtractor != null && observerInitialsIndex != null) {
        String nextInitials = observerInitialsExtractor.extract(nextLine);
        if (!Strings.isNullOrEmpty(nextInitials)) {
          mergedLine[observerInitialsIndex] = mergedLine[observerInitialsIndex] + " " + nextInitials;
        }
      }
      
      return true;
    }

  }

  @Override
  protected ImportLines importLines(File file) throws IOException {
    ImportLines importLines = CsvImportLines.fromReader(Chardet.decode(new FileInputStream(file), Charsets.ISO_8859_1));
    return new MergeObserversImportLines(importLines);
    
  }

  @Override
  protected void preSightingAnalysis(String[] line) {
    // In a pass prior to performing sighting import, check what the observer
    // extractor will say:
    // - Are there any lines containing multiple observers?
    // - Is there different observer information on different lines?
    // ... as these say whether it's useful to add an observer mapping
    // and force enabling observer support in Scythebill.
    if (observerNameExtractor != null) {
      String observers = observerNameExtractor.extract(line);
      if (!Strings.isNullOrEmpty(observers)) {
        observerSet.add(observers);
        if (observers.contains("\n")) {
          multipleObservers = true;
        }
      }
    }
  }
  
  @Override
  public boolean importContainedUserInformation() {
    return hadObservers;
  }
  
  @Override
  protected void computeExtendedMappings() throws IOException {
    // Only need to choose formatters once.
    if (chosenFormatters != null) {
      return;
    }
    
    /** Store a format and pattern and how often it's worked. */
    class PossibleFormat {
      PossibleFormat(String pattern) {
        this.formatter = DateTimeFormat.forPattern(pattern)
            .withPivotYear(2000)
            .withLocale(Locale.getDefault())
            .withChronology(GJChronology.getInstance());
      }
      
      /** Try a single date, if not already disabled. */
      void tryDate(String date) {
        try {
          formatter.parseLocalDate(date);
          successCount++;
        } catch (IllegalArgumentException e) {
        }
      }
    
      int successCount;
      final DateTimeFormatter formatter;
    }
    
    Set<String> patterns = new LinkedHashSet<>();
    // Prefer the local short (just date) and short (with time), whatever
    // they are.
    patterns.add(DateTimeFormat.patternForStyle("S-", Locale.getDefault()));
    patterns.add(DateTimeFormat.patternForStyle("SS", Locale.getDefault()));
    // Then add some other date and date/time patterns.
    // The time gets thrown away, but is necessary to ensure parsing succeeds.
    patterns.add("M/d/yy");
    patterns.add("M/d/yy h:mm:ss a");
    patterns.add("M/d/yy H:mm:ss");
    patterns.add("d/M/yy");
    patterns.add("d/M/yy h:mm:ss a");
    patterns.add("d/M/yy H:mm:ss");
    
    List<PossibleFormat> formats = new ArrayList<>();    
    patterns.forEach(p -> formats.add(new PossibleFormat(p)));
    
    try (ImportLines lines = importLines(sightingsFile)) {
      lines.nextLine();
      
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }
        
        String date = dateExtractor.extract(line);
        if (Strings.isNullOrEmpty(date)) {
          continue;
        }
        
        for (int i = 0; i < formats.size(); i++) {
          formats.get(i).tryDate(date);
        }        
      }
    }
  
    chosenFormatters = formats.stream()
        .sorted(Comparator.<PossibleFormat>comparingInt(f -> f.successCount).reversed())
        .map(f -> f.formatter)
        .collect(ImmutableList.toImmutableList());
  }
  
  static private String toInitialCaps(String in) {
    if (!ALL_CAPS_NAMES.matchesAllOf(in) || in.length() <= 2) {
      return in;
    }
    StringBuilder builder = new StringBuilder(in);
    for (int i = builder.length() - 1; i > 0 ; i--) {
      if (CAPS.matches(builder.charAt(i)) && CAPS.matches(builder.charAt(i - 1))) {
        builder.setCharAt(i, Character.toLowerCase(builder.charAt(i)));
      }
    }
    return builder.toString();
  }
}
