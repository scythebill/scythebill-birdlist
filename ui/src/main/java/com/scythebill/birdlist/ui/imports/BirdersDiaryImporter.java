/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.io.MergingImportLines;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.imports.ParsedLocationIds.ToBeDecided;
import com.scythebill.birdlist.ui.imports.ParsedLocationIds.ToBeDecided.HintType;

/**
 * Imports from Birder's Diary format imports.
 */
public class BirdersDiaryImporter extends CsvSightingsImporter {

  private LineExtractor<String> commonExtractor;
  private LineExtractor<String> sciExtractor;
  private LineExtractor<String> dateExtractor;
  private LineExtractor<String> locationExtractor;
  private LineExtractor<String> commentsExtractor;
  private LineExtractor<String> tripExtractor;
  private Integer observerIndex;
  private LineExtractor<String> observerExtractor;
  private LineExtractor<String> locationIdExtractor;
  private LineExtractor<String> taxonomyIdExtractor;
  private Set<String> observerSet = new HashSet<>();
  private boolean multipleObservers = false;
  
  public BirdersDiaryImporter(ReportSet reportSet, Taxonomy taxonomy, Checklists checklists,
      PredefinedLocations predefinedLocations, File file) {
    super(reportSet, taxonomy, checklists, predefinedLocations, file, file);
  }

  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return taxonomyIdExtractor;
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    // Subspecies are appended to both the sci and common names in Birder's Diary, so
    // remove from both and report correctly.
    RowExtractor<String[], String> species = new FieldTaxonImporter.JustGenusAndSpecies<String[]>(sciExtractor);
    RowExtractor<String[], String> subspecies = new FieldTaxonImporter.SubspeciesIfPresent<String[]>(sciExtractor);
    RowExtractor<String[], String> commonNameMinusSubspecies = new LineExtractor<String>() {
      
      @Override
      public String extract(String[] line) {
        String common = commonExtractor.extract(line);
        String ssp = subspecies.extract(line);
        if (Strings.isNullOrEmpty(common) || Strings.isNullOrEmpty(ssp)) {
          return common;
        }
        
        if (common.endsWith(ssp)) {
          return common.substring(0, common.length() - (ssp.length() + 1));
        }
        
        return common;
      }
    };
    
    return new FieldTaxonImporter<String[]>(taxonomy, commonNameMinusSubspecies, species, subspecies);
  }

  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    String[] header = lines.nextLine();
    Map<String, Integer> headersByIndex = new LinkedHashMap<>();
    for (int i = 0; i < header.length; i++) {
      headersByIndex.put(header[i].toLowerCase(), i);
    }

    int commonIndex = getRequiredHeader(headersByIndex, "Name");
    int scientificIndex = getRequiredHeader(headersByIndex, "Scientific Name");
    int dateIndex = getRequiredHeader(headersByIndex, "When Seen");
    int locationIndex = getRequiredHeader(headersByIndex, "Location");
    Integer tripIndex = headersByIndex.get("trip");
    observerIndex = headersByIndex.get("observer");
    Integer commentsIndex = headersByIndex.get("comments");
    
    commonExtractor = LineExtractors.stringFromIndex(commonIndex);
    sciExtractor = LineExtractors.stringFromIndex(scientificIndex);
    dateExtractor = LineExtractors.stringFromIndex(dateIndex);
    locationExtractor = LineExtractors.stringFromIndex(locationIndex);
    tripExtractor = tripIndex == null 
        ? LineExtractors.<String>alwaysNull() : LineExtractors.stringFromIndex(tripIndex);
    commentsExtractor = commentsIndex == null 
        ? LineExtractors.<String>alwaysNull() : LineExtractors.stringFromIndex(commentsIndex);
    observerExtractor = (observerIndex == null)
        ? LineExtractors.<String>alwaysNull() : LineExtractors.stringFromIndex(observerIndex);

    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
    mappers.add(new DateFromStringFieldMapper<>("dd/MM/yy", dateExtractor));
    mappers.add(new DescriptionFieldMapper<>(commentsExtractor));
    locationIdExtractor = LineExtractors.joined(
        Joiner.on('|'),
        locationExtractor, tripExtractor);
    taxonomyIdExtractor = LineExtractors.joined(Joiner.on('|'),
        commonExtractor, sciExtractor);
    
    // If preSightingAnalysis() found observers, and:
    // - It never found multiple observers in a single sighting
    // - It didn't find different observers for different sightings
    // - The user hasn't already enabled multiple observers
    // ... then skip the UserFieldMapper altogether.
    if (observerSet.size() != 1 || multipleObservers || reportSet.getUserSet() != null) {
      mappers.add(new UserFieldMapper<>(reportSet, LineExtractors.alwaysNull(), observerExtractor));
    }

    return new ComputedMappings<>(
        new TaxonFieldMapper(taxonomyIdExtractor),
        new LocationMapper(locationIdExtractor),
        mappers);
  }

  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    try (ImportLines lines = importLines(locationsFile)) {
      // Skip the header
      lines.nextLine();
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }
        String id = locationIdExtractor.extract(line);
        if (locationIds.hasBeenParsed(id)) {
          continue;
        }

        String location = locationExtractor.extract(line);
        String trip = tripExtractor.extract(line);
        ToBeDecided toBeDecided = new ToBeDecided(location, null, trip, HintType.location);
        locationIds.addToBeResolvedLocationName(id, toBeDecided);
      }
    }
    
  }

  @Override
  protected String[] getHeaderRow(ImportLines lines) throws IOException {
    return lines.nextLine();
  }

  @Override
  protected Charset getCharset() {
    // Check - maybe it's IOS_8859_1 instead?
    //return Charset.forName("Windows-1252");
    return StandardCharsets.ISO_8859_1;
  }
  
  class MergeObserversImportLines extends MergingImportLines {
    MergeObserversImportLines(ImportLines importLines) {
      super(importLines);
    }

    @Override
    protected boolean merge(String[] mergedLine, String[] nextLine) {
      if (nextLine.length < 5 || mergedLine.length < 5) {
        return false;
      }
      
      // Don't bother merging until an Observer index has been
      // found.
      if (observerIndex == null
          || commonExtractor == null
          || sciExtractor == null
          || dateExtractor == null
          || locationExtractor == null) {
        return false;  
      }

      // If anything about those two lines is different, they shouldn't be merged. 
      // (Ignoring "trip" here, which is maybe wrong, but hardly seems relevant?  Ignoring
      // comment is very intentional.)
      if (!Objects.equals(commonExtractor.extract(nextLine), commonExtractor.extract(mergedLine))
          || !Objects.equals(sciExtractor.extract(nextLine), sciExtractor.extract(mergedLine))
          || !Objects.equals(dateExtractor.extract(nextLine), dateExtractor.extract(mergedLine))
          || !Objects.equals(locationExtractor.extract(nextLine), locationExtractor.extract(mergedLine))) {
        return false;
      }

      // Merge in the next "who" if necessary.
      String nextWho = observerExtractor.extract(nextLine);
      if (!Strings.isNullOrEmpty(nextWho)) {
        mergedLine[observerIndex] = mergedLine[observerIndex] + "\n" + nextWho;
      }
      
      return true;
    }

  }

  @Override
  protected ImportLines importLines(File file) throws IOException {
    ImportLines importLines = super.importLines(file);
    return new MergeObserversImportLines(importLines);
    
  }

  @Override
  protected void preSightingAnalysis(String[] line) {
    // In a pass prior to performing sighting import, check what the observer
    // extractor will say:
    // - Are there any lines containing multiple observers?
    // - Is there different observer information on different lines?
    // ... as these say whether it's useful to add an observer mapping
    // and force enabling observer support in Scythebill.
    if (observerExtractor != null) {
      String observers = observerExtractor.extract(line);
      if (!Strings.isNullOrEmpty(observers)) {
        observerSet.add(observers);
        if (observers.contains("\n")) {
          multipleObservers = true;
        }
      }
    }
  }
  
  @Override
  public boolean importContainedUserInformation() {
    return observerIndex != null;
  }
}
