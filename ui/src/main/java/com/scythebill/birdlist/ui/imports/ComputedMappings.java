/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import javax.annotation.Nullable;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.scythebill.birdlist.model.sighting.Sighting;

/**
 * A full set of computed field mappings for an import.
 */
public class ComputedMappings<T> {
  private final SightingsImporter<T>.TaxonFieldMapper taxonFieldMapper;
  private final SightingsImporter<T>.LocationMapper locationMapper;
  private final ImmutableList<FieldMapper<T>> otherMappers;

  public ComputedMappings(
      SightingsImporter<T>.TaxonFieldMapper taxonFieldMapper,
      @Nullable SightingsImporter<T>.LocationMapper locationMapper,
      Iterable<FieldMapper<T>> otherMappers) {
    this.taxonFieldMapper = Preconditions.checkNotNull(taxonFieldMapper);
    this.locationMapper = locationMapper;
    this.otherMappers = ImmutableList.copyOf(otherMappers);
  }
  
  public void mapAll(T importRow, Sighting.Builder sighting) {
    if (locationMapper != null) {
      locationMapper.map(importRow, sighting);
    }
    
    // Run the taxonomy mapper specifically after the location mapping, so
    // we can take advantage of checklists
    taxonFieldMapper.map(importRow, sighting);

    for (FieldMapper<T> fieldMapper : otherMappers) {
      fieldMapper.map(importRow, sighting);
    } 
  }

  // Skip any mappings 
  public void skipLocationAndTaxonAndUser(T importRow, Sighting.Builder sighting) {
    for (FieldMapper<T> fieldMapper : otherMappers) {
      if (!(fieldMapper instanceof UserFieldMapper)) {
        fieldMapper.map(importRow, sighting);
      }
    }
  }
}
