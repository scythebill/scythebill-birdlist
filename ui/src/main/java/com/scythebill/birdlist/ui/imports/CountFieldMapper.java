/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import com.scythebill.birdlist.model.sighting.ApproximateNumber;
import com.scythebill.birdlist.model.sighting.Sighting.Builder;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;

/**
 * Extracts the count field of a sighting.
 */
public class CountFieldMapper<T> implements FieldMapper<T> {
  private final RowExtractor<T, String> countExtractor;

  public CountFieldMapper(RowExtractor<T, String> countExtractor) {
    this.countExtractor = countExtractor;
  }

  @Override public void map(T line, Builder sighting) {
    String count = countExtractor.extract(line);
    if (count != null) {
      ApproximateNumber number = ApproximateNumber.tryParse(count);
      if (number != null) {
        // Treat zero as a "not by me" status;  don't set directly.  This seems to be the
        // eBird convention, for one.
        if (number.getNumber() == 0) {
          if (sighting.getSightingInfo().getSightingStatus() == SightingStatus.NONE) {
           sighting.getSightingInfo().setSightingStatus(SightingStatus.NOT_BY_ME);
          }
        } else {
          sighting.getSightingInfo().setNumber(number);
        }
      }
    }
  }
}