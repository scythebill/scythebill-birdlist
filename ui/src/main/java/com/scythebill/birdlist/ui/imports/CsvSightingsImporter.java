/*
 * Copyright 2012 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.common.base.Charsets;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.Sighting.Builder;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Imports sightings from CSV files. The procedure for an import requires multiple steps.
 * <p>
 * <ol>
 * <li>Run a quick sanity check, looking for a single importable line (ignoring taxa/locations).
 * <li>Parse all taxa, via a call to {@link #parseTaxonomyIds}. This returns a map of taxa that
 * could not be automatically resolved.
 * <li>An independent UI calls {@link #registerTaxon} with manually registered taxa.
 * <li>Location resolution is triggered with {@link #parseLocationIds}. This too returns a map of
 * unresolved locations.
 * <li>An independent UI calls {@link ParsedLocationIds#put} to resolve the remaining locations.
 * <li>Finally, {@link #parseSightings} finishes the process.
 * </ol>
 */
public abstract class CsvSightingsImporter extends SightingsImporter<String[]> {
  private final static Logger logger = Logger.getLogger(CsvSightingsImporter.class.getName());

  private final List<String[]> failedLines = Lists.newArrayList();

  protected final File sightingsFile;
  protected final File locationsFile;

  protected CsvSightingsImporter(ReportSet reportSet, Taxonomy taxonomy, Checklists checklists,
      PredefinedLocations predefinedLocations, File sightingsFile, File locationsFile) {
    super(reportSet, taxonomy, checklists, predefinedLocations);
    this.sightingsFile = sightingsFile;
    this.locationsFile = locationsFile;
  }

  /**
   * Parses locations and sightings. Not used by the main UI, which parsed locations and sightings
   * separately.
   */
  public List<Sighting> runImport() throws IOException {
    parseLocationIds();
    return parseSightings();
  }

  /** Write all failed lines to CSV output. */
  public void writeFailedLines(ExportLines writer) throws IOException {
    for (String[] failedLine : failedLines) {
      writer.nextLine(failedLine);
    }
  }

  protected Charset getCharset() {
    return Charsets.UTF_8;
  }

  /**
   * Run an initial check before doing any taxon or location mapping to see if the file has any
   * valid data.
   * 
   * @return an absent optional if at least one row looked good, an error message string otherwise
   */
  @Override
  public Optional<String> initialCheck() throws IOException {
    ImportLines lines = importLines(sightingsFile);
    int i = 0;
    int dateFailures = 0;

    try {
      beforeParseTaxonomyIds();
      ComputedMappings<String[]> computedMappings = computeMappings(lines);
      computeExtendedMappings();
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }

        if (skipLine(line)) {
          continue;
        }

        try {
          Builder newBuilder = Sighting.newBuilder();
          // Skip over the taxon mapper or location mapper during
          // the sanity check (since no taxa have been resolved).
          // And skip user mappings too, just to avoid forcing UserSet creation.
          computedMappings.skipLocationAndTaxonAndUser(line, newBuilder);

          // Found a good line - return true
          return Optional.absent();
        } catch (RuntimeException e) {
          // Ignore. (But log the first ten)
          if (i++ < 10) {
            logger.log(Level.WARNING, "Failed to parse line", e);
          }
          if (e instanceof DateParseException) {
            dateFailures++;
          }
        }
      }
    } finally {
      lines.close();
    }

    return Optional.of(getFailedParseFormat(dateFailures == i));
  }

  protected String getFailedParseFormat(boolean allDateFailures) {
    return Messages.getMessage(Name.COULD_NOT_EXTRACT_ANY_DATA_FORMAT);
  }

  /**
   * Compute any additional mappings (potentially reparsing the entire file). Will always be called
   * after computeMappings(). Currently relied on only for more intelligent date mappings.
   */
  // TODO: this is an uuuugly API; come up with a more elegant way to represent this.
  protected void computeExtendedMappings() throws IOException {}

  @Override
  protected void operateOnAllRows(RowOperator<String[]> operator) throws IOException {
    ImportLines lines = importLines(sightingsFile);
    try {
      // Skip over the header (or compute the taxonomyIdExtractor) if necessary
      computeMappings(lines);
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }
  
        if (skipLine(line)) {
          continue;
        }
        
        operator.operate(line);
      }
    } finally {
      lines.close();
    }
  }
  

  @Override
  public List<Sighting> parseSightings() throws IOException {
    beforeParseSightings();
    ImportLines lines = importLines(sightingsFile);
    try {
      ComputedMappings<String[]> computedMappings = computeMappings(lines);
      computeExtendedMappings();

      RowExtractor<String[], ? extends Object> taxonomyIdExtractor = taxonomyIdExtractor();

      List<Sighting> sightings = Lists.newArrayList();
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }

        if (skipLine(line)) {
          continue;
        }


        parseSighting(line, computedMappings::mapAll, taxonomyIdExtractor::extract, sightings, computedMappings::skipLocationAndTaxonAndUser);
      }

      // Notify the importer that all sightings have completed
      allSightingsFinished();

      return sightings;
    } finally {
      lines.close();
    }
  }

  protected void beforeParseSightings() throws IOException {}
  
  protected void allSightingsFinished() {}

  protected abstract ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException;

  protected ImportLines importLines(File file) throws IOException {
    return CsvImportLines.fromFile(file, getCharset());
  }

  protected boolean skipLine(String[] line) {
    return false;
  }

  protected int getRequiredHeader(Map<String, Integer> indexMap, String key) throws IOException {
    return getRequiredHeader(indexMap, key, String::toLowerCase);
  }

  protected int getRequiredHeader(Map<String, Integer> indexMap, String key,
      Function<String, String> transform) throws IOException {
    Integer index = indexMap.get(transform.apply(key));
    if (index == null) {
      throw new ImportException(
          Messages.getFormattedMessage(Name.IMPORT_REQUIRED_HEADER_MISSING_FORMAT, key));
    }

    return index;
  }

  protected int getRequiredHeader(Map<String, Integer> indexMap,
      Function<String, String> transform, String...keys) throws IOException {
    Preconditions.checkArgument(keys.length > 0);
    for (String key : keys) {
      Integer index = indexMap.get(transform.apply(key));
      if (index != null) {
        return index;
      }
    }
    throw new ImportException(
        Messages.getFormattedMessage(Name.IMPORT_REQUIRED_HEADER_MISSING_FORMAT, keys[0]));
  }

  protected int getRequiredHeader(Map<String, Integer> indexMap,
      Function<String, String> transform, Iterable<String> keys) throws IOException {
    Preconditions.checkArgument(!Iterables.isEmpty(keys));
    for (String key : keys) {
      Integer index = indexMap.get(transform.apply(key));
      if (index != null) {
        return index;
      }
    }
    throw new ImportException(
        Messages.getFormattedMessage(Name.IMPORT_REQUIRED_HEADER_MISSING_FORMAT, keys.iterator().next()));
  }

  protected String[] getHeaderRow(ImportLines lines) throws IOException {
    return null;
  }

  /** Record that a particular row failed. */
  @Override
  protected void importRowFailed(String[] line) {
    failedLines.add(line);
  }

  @Override
  public File writeFailedLines() {
    // Loop through (up to) 50 file names to ensure we don't overwrite an existing file.
    File failedLinesFile = null;
    for (int i = 0; i < 50; i++) {
      failedLinesFile = failedFile(sightingsFile, i);
      if (!failedLinesFile.exists()) {
        break;
      }
    }
    // All 50 occupied? Give up.
    if (failedLinesFile == null || failedLinesFile.exists()) {
      return null;
    }

    try {
      if (!failedLinesFile.createNewFile()) {
        return null;
      }

      ExportLines writer = CsvExportLines.fromWriter(new BufferedWriter(
          new OutputStreamWriter(new FileOutputStream(failedLinesFile), Charsets.UTF_8)));
      try {
        // Clone the header row, if present, so that the file looks in reasonable shape for tweaking
        // and re-importing
        try (ImportLines importLines = importLines(sightingsFile)) {
          String[] headerRow = getHeaderRow(importLines);
          if (headerRow != null) {
            writer.nextLine(headerRow);
          }
        }

        writeFailedLines(writer);
        return failedLinesFile;
      } finally {
        writer.close();
      }
    } catch (Exception e) {
      return null;
    }
  }

  private File failedFile(File openFile, int index) {
    String failedLinesFileName = openFile.getName();
    if (failedLinesFileName.indexOf('.') >= 0) {
      failedLinesFileName = failedLinesFileName.substring(0, failedLinesFileName.lastIndexOf('.'));
    }

    if (index == 0) {
      failedLinesFileName += "-failed.csv";
    } else {
      failedLinesFileName += "-" + index + "-failed.csv";
    }
    return new File(openFile.getParentFile(), failedLinesFileName);
  }

  @Override
  public String importFileName() {
    return sightingsFile.getName();
  }
}
