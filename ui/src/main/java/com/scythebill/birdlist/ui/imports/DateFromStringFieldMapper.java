/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import org.joda.time.chrono.GJChronology;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.common.base.Strings;
import com.scythebill.birdlist.model.sighting.Sighting;

/**
 * Extracts a date from a single string. 
 */
public class DateFromStringFieldMapper<T> implements FieldMapper<T> {

  private DateTimeFormatter dateTimeFormatter;
  private RowExtractor<T, String> extractor;

  public DateFromStringFieldMapper(String pattern, RowExtractor<T, String> extractor) {
    this.extractor = extractor;
    this.dateTimeFormatter = DateTimeFormat.forPattern(pattern)
        .withChronology(GJChronology.getInstance())
        .withPivotYear(2000);
  }

  @Override
  public void map(T line, Sighting.Builder sighting) {
    String date = Strings.emptyToNull(extractor.extract(line));
    if (date != null) {
      try {
        sighting.setDate(dateTimeFormatter.parseLocalDate(date));
      } catch (IllegalArgumentException e) {
        throw new DateParseException(e);
      }
    }
  }

}
