/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.joda.time.DateTimeFieldType;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.Partial;
import org.joda.time.chrono.GJChronology;

import com.scythebill.birdlist.model.sighting.Sighting.Builder;

public class DateFromYearMonthDayFieldMapper<T> implements FieldMapper<T> {
  private final RowExtractor<T, Integer> yearExtractor;
  private final RowExtractor<T, Integer> monthExtractor;
  private final RowExtractor<T, Integer> dayExtractor;

  public DateFromYearMonthDayFieldMapper(RowExtractor<T, Integer> yearExtractor,
      RowExtractor<T, Integer> monthExtractor,
      RowExtractor<T, Integer> dayExtractor) {
    this.yearExtractor = yearExtractor;
    this.monthExtractor = monthExtractor;
    this.dayExtractor = dayExtractor;
  }

  @Override public void map(T line, Builder sighting) {
    Partial p = new Partial(GJChronology.getInstance());
    p = with(p, DateTimeFieldType.year(), yearExtractor.extract(line));
    p = with(p, DateTimeFieldType.monthOfYear(), monthExtractor.extract(line));
    p = with(p, DateTimeFieldType.dayOfMonth(), dayExtractor.extract(line));
    sighting.setDate(p);
  }
  
  private Partial with(Partial p, DateTimeFieldType type, Integer extracted) {
    if (extracted == null || extracted == 0) {
      return p;
    }
    
    try {
      return p.with(type, extracted);
    } catch (IllegalFieldValueException e) {
      Logger.getLogger(getClass().getName())
          .log(Level.WARNING, "Failed to parse field {0} with value {1}", new Object[]{type, extracted});
      return p;
    }
  }
}