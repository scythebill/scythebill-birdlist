/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.joda.time.Duration;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.Area;
import com.scythebill.birdlist.model.sighting.Distance;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.VisitInfo.ObservationType;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * EBird import utilities.
 */
class EBird {
  private final static Logger logger = Logger.getLogger(EBird.class.getName());
  private final EBirdChecklistApi ebirdChecklistApi;
  
  @Inject
  EBird(EBirdChecklistApi ebirdChecklistApi) {
    this.ebirdChecklistApi = ebirdChecklistApi;
  }
  
  /**
   * Choose an importer for a specific file.  Looks at the first line and heuristically
   * picks whether it's a "My data" export or a checklist export, etc.
   */
  public CsvSightingsImporter chooseImporter(File openFile, ReportSet reportSet,
      Taxonomy clements, Checklists checklists, PredefinedLocations predefinedLocations) {
    ImportLines importLines = null;
    try {
      importLines = CsvImportLines.fromFile(openFile, Charsets.UTF_8);
      List<String> nextLine = Arrays.asList(importLines.nextLine());
      if (!nextLine.isEmpty()
          && EBirdChecklistImporter.matchesLocalizedHeader("Species", nextLine.get(0))) {
        return new EBirdChecklistImporter(reportSet, clements, checklists, predefinedLocations, openFile,
            ebirdChecklistApi);
      } else if (nextLine.contains("Checklist Comments")) {
        return new EBirdMyDataImporter(reportSet, clements, checklists, predefinedLocations, openFile);
      } else if (nextLine.get(0).equalsIgnoreCase("Row #")
          && nextLine.contains("S/P")
          && nextLine.contains("LocID")) {
        if (nextLine.contains("Countable")) {
          return new EBird2023LifeListImporter(reportSet, clements, checklists, predefinedLocations, openFile);
        } else {
          return new EBirdLifeListImporter(reportSet, clements, checklists, predefinedLocations, openFile);        
        }
      } else {
        return new EBirdImportImporter(reportSet, clements, checklists, predefinedLocations, openFile);
      }
    } catch (IOException e) {
      return new AlwaysFailImporter(reportSet, clements, checklists, predefinedLocations, openFile, e.getMessage());
    } finally {
      try {
        // importLines will be null if CsvImportLines failed to open the file
        if (importLines != null) {
          importLines.close();
        }
      } catch (IOException e) {
        logger.log(Level.WARNING, "Could not close", e);
      }
    }
  }

  // Two patterns which eBird uses when preferences are set to
  // "Species name display - both". 
  static private Pattern COMMON_AND_SCIENTIFIC_IN_PARENS = Pattern.compile(
      "^(.+) \\((.+)\\)$");
  static private Pattern COMMON_AND_SCIENTIFIC_HYPHENATED = Pattern.compile(
      "^(.+) - (.+)$");
  
  // This intentionally doesn't match to the end of lines, to deal
  // with sp., slash, and subspecies
  private static final Pattern SCIENTIFIC = Pattern.compile("^\\(?[A-Z][a-z]+ [a-z]+\\)?");
  private static final CharMatcher PARENS = CharMatcher.anyOf("()"); 
  
  static class CommonNameExtractor implements LineExtractor<String> {
    private final LineExtractor<String> extractor;
    private final TaxonResolver taxonResolver;

    CommonNameExtractor(LineExtractor<String> extractor, TaxonResolver taxonResolver) {
      this.extractor = Preconditions.checkNotNull(extractor);
      this.taxonResolver = taxonResolver;
    }

    @Override
    public String extract(String[] line) {
      String name = extractor.extract(line);
      if (name == null) {
        return null;
      }
      
      // If this looks like common + scientific, return just the common name
      Matcher matcher = COMMON_AND_SCIENTIFIC_IN_PARENS.matcher(name);
      if (matcher.matches()) {
        // An annoying special case for the monstrosity:
        //   Rock Pigeon (Feral Pigeon) (Columba livia (Domestic type))
        if (name.endsWith("))")) {
          int firstRightParen = name.indexOf(')');
          if (firstRightParen > 0) {
            return name.substring(0, firstRightParen + 1);
          }
        }

        if (mightBeScientificName(matcher.group(2), taxonResolver)) {
          return matcher.group(1);
        }
      }
      
      matcher = COMMON_AND_SCIENTIFIC_HYPHENATED.matcher(name);
      if (matcher.matches()) {
        if (mightBeScientificName(matcher.group(2), taxonResolver)) {
          return matcher.group(1);
        }
      }
      
      // If it looks like just a scientific name, then bail 
      if (mightBeScientificName(name, taxonResolver)) {
        return null;
      }
      
      return name;
    }
  }

  private static boolean mightBeScientificName(String name, TaxonResolver taxonResolver) {
    if (!SCIENTIFIC.matcher(name).find()) {
      return false;
    }
    
    TaxonPossibilities mapCommonName = taxonResolver.map(name, null, null);
    TaxonPossibilities mapScientificName = taxonResolver.map(null, name, null);
    return mapCommonName == null && mapScientificName != null;
  }
  
  static class ScientificNameExtractor implements LineExtractor<String> {
    private final LineExtractor<String> extractor;
    private final TaxonResolver taxonResolver;

    ScientificNameExtractor(LineExtractor<String> extractor, TaxonResolver taxonResolver) {
      this.extractor = Preconditions.checkNotNull(extractor);
      this.taxonResolver = taxonResolver;
    }

    @Override
    public String extract(String[] line) {
      String name = extractor.extract(line);
      if (name == null) {
        return null;
      }
      
      // If this looks like common + scientific, return just the common name
      Matcher matcher = COMMON_AND_SCIENTIFIC_IN_PARENS.matcher(name);
      if (matcher.matches()) {
        // An annoying special case for the monstrosity:
        //   Rock Pigeon (Feral Pigeon) (Columba livia (Domestic type))
        if (name.endsWith("))")) {
          int middle = name.indexOf(") (");
          if (middle > 0) {
            return name.substring(middle + 3, name.length() - 1);
          }
        }

        if (mightBeScientificName(matcher.group(2), taxonResolver)) {
          return matcher.group(2);
        }
      }
      
      matcher = COMMON_AND_SCIENTIFIC_HYPHENATED.matcher(name);
      if (matcher.matches()) {
        if (mightBeScientificName(matcher.group(2), taxonResolver)) {
          return matcher.group(2);
        }
      }
      
      // If it looks like just a scientific name, then return it 
      if (mightBeScientificName(name, taxonResolver)) {
        return PARENS.trimFrom(name);
      }
      
      return null;
    }
  }

  public static ObservationType observationType(String extracted) {
    if (Strings.isNullOrEmpty(extracted)) {
      return ObservationType.HISTORICAL;
    }
    
    ObservationType type = ObservationType.fromName(extracted);
    if (type == null) {
      type = ObservationType.HISTORICAL;
    }
    return type;
  }
  
  
  // Patterns for various eBird patterns 
  
  private static final List<Pattern> HOURS_AND_MINUTES = ImmutableList.of(
      Pattern.compile("(\\d+) hour.*?(\\d+) minute.*"),
      Pattern.compile("(\\d+) hodin.*?(\\d+) minut.*"),
      Pattern.compile("(\\d+) Stunde.*?(\\d+) Minute.*"),
      Pattern.compile("(\\d+) ordu.*?(\\d+) minutu.*"),
      Pattern.compile("(\\d+) heure.*?(\\d+) minute.*"),
      Pattern.compile("(\\d+) ora.*?(\\d+) minute.*"),
      Pattern.compile("(\\d+) שעות, (\\d+) דקות"),
      Pattern.compile("(\\d+) 時間.*?(\\d+) 分"),
      Pattern.compile("(\\d+) time.*?(\\d+) minutt.*"),
      Pattern.compile("(\\d+) hora.*?(\\d+) minuto.*"),
      Pattern.compile("(\\d+) час.*?(\\d+) минута.*"),
      Pattern.compile("(\\d+) saat, (\\d+) dakika"),
      Pattern.compile("(\\d+) годин.*?(\\d+) хвилин.*"),
      Pattern.compile("(\\d+) 小时, (\\d+) 分钟"),
      Pattern.compile("(\\d+) 小時.*?(\\d+) 分.*"));
  private static final List<Pattern> HOURS = ImmutableList.of(
      Pattern.compile("(\\d+) hour.*"),
      Pattern.compile("(\\d+) hodin.*"),
      Pattern.compile("(\\d+) Stunde.*"),
      Pattern.compile("(\\d+) ordu.*"),
      Pattern.compile("(\\d+) heure.*"),
      Pattern.compile("(\\d+) ora.*"),
      Pattern.compile("(\\d+) שעות"),
      Pattern.compile("(\\d+) 時間.*?"),
      Pattern.compile("(\\d+) time.*"),
      Pattern.compile("(\\d+) hora.*"),
      Pattern.compile("(\\d+) час.*"),
      Pattern.compile("(\\d+) saat"),
      Pattern.compile("(\\d+) годин.*"),
      Pattern.compile("(\\d+) 小時.*"),
      Pattern.compile("(\\d+) 小时"));
  private static final List<Pattern> MINUTES = ImmutableList.of(
      Pattern.compile("(\\d+) minut.*"),
      Pattern.compile("(\\d+) Minute.*"),
      Pattern.compile("(\\d+) דקות"),
      Pattern.compile("(\\d+) 分"),
      Pattern.compile("(\\d+) минута.*"),
      Pattern.compile("(\\d+) dakika"),
      Pattern.compile("(\\d+) хвилин.*"),
      Pattern.compile("(\\d+) 分.*"),
      Pattern.compile("(\\d+) 分钟"));

  
  /**
   * Return a duration from a a string of the form:
   * <pre>
   * 1 hour(s), 30 minute(s)
   * </pre>
   */
  public static Duration durationHrsMins(String extracted) {
    if (Strings.isNullOrEmpty(extracted)) {
      return null;
    }

    for (Pattern hoursAndMinutes : HOURS_AND_MINUTES) {
      Matcher matcher = hoursAndMinutes.matcher(extracted);
      if (matcher.matches()) {
        String hours = matcher.group(1);
        String minutes = matcher.group(2);
        return Duration.standardHours(Integer.parseInt(hours))
            .plus(Duration.standardMinutes(Integer.parseInt(minutes)));
      }
    }

    for (Pattern hoursPattern : HOURS) {
      Matcher matcher = hoursPattern.matcher(extracted);
      if (matcher.matches()) {
        String hours = matcher.group(1);
        return Duration.standardHours(Integer.parseInt(hours));
      }
    }
    
    for (Pattern minutesPattern : MINUTES) {
      Matcher matcher = minutesPattern.matcher(extracted);
      if (matcher.matches()) {
        String minutes = matcher.group(1);
        return Duration.standardMinutes(Integer.parseInt(minutes));
      }
    }
    
    return null;
  }
  
  private static CharMatcher ONLY_DECIMAL = CharMatcher.inRange('0', '9').or(CharMatcher.is('.'));

  private static final ImmutableList<String> MILES_STRINGS = ImmutableList.of(
      "mil",
      "Meile",
      "miglio",
      "מייל",
      "マイル",
      "миля",
      "миль",
      "英里");
  private static final ImmutableList<String> KILOMETER_STRINGS = ImmutableList.of(
      "kilo",
      "Kilo",
      "kiló",
      "chilo",
      "quiló",
      "km",
      "кило",
      "кіло",
      "千米",
      "公里",
      "קילו",
      "キロメートル");

  /**
   * Extract distance when of the form:
   * <pre>
   * 2.0 mile(s)
   * 3.0 kilometer(s)
   * </pre>
   */
  public static Distance distanceMilesOrKilometers(String extracted) {
    if (extracted == null) {
      return null;
    }
    String justDecimal = ONLY_DECIMAL.retainFrom(extracted);
    if (justDecimal.isEmpty()) {
      return null;
    }
    
    for (String milesString : MILES_STRINGS) {
      if (extracted.contains(milesString)) {
        return Distance.inMiles(Float.parseFloat(justDecimal));
      }
    }

    for (String milesString : KILOMETER_STRINGS) {
      if (extracted.contains(milesString)) {
        return Distance.inKilometers(Float.parseFloat(justDecimal));      
      }
    }

    return null; 
  }

  /**
   * Extract area when of the form:
   * <pre>
   * 2.0 ac
   * 5.0 ha
   * </pre>
   */
  public static Area areaHectaresOrAcres(String extracted) {
    if (extracted == null) {
      return null;
    }
    String justDecimal = ONLY_DECIMAL.retainFrom(extracted);
    if (justDecimal.isEmpty()) {
      return null;
    }
    
    if (extracted.contains("ac")) {
      return Area.inAcres(Float.parseFloat(justDecimal));
    } else if (extracted.contains("ha")) {
      return Area.inHectares(Float.parseFloat(justDecimal));      
    }

    return null; 
  }
  
  static class NameAndLatLong {
    String name;
    LatLongCoordinates latLong;
  }

  // Patterns below use lazy mode for the first group to ensure that none
  // of the lat-long gets pulled into the name, though in practice this
  // is only an issue for the COMMA_PATTERN
  
  /** Matches "MyName (123.45, -67.89)" */
  private static final Pattern PAREN_PATTERN = Pattern.compile(
      "(.*?) \\((-?\\d+\\.\\d+), (-?\\d+\\.\\d+)\\)");
  
  /** Matches "MyName - 123.45x-67.89" */
  private static final Pattern X_PATTERN = Pattern.compile(
      "(.*?) - (-?\\d+\\.\\d+)x(-?\\d+\\.\\d+)");
  
  /** Matches "MyName 123.45, -67.89" */
  private static final Pattern COMMA_PATTERN = Pattern.compile(
      "(.*?)\\s*(-?\\d+\\.\\d+),\\s*(-?\\d+\\.\\d+)");
  
  
  /**
   * Looks for an embedded lat-long in a location name,
   * as sometimes occurs in eBird checklists.
   */
  public static NameAndLatLong extractLatLongFromName(String name) {
    NameAndLatLong nameAndLatLong = new NameAndLatLong();
    nameAndLatLong.name = name;
    
    Matcher matcher = PAREN_PATTERN.matcher(name);
    if (!matcher.matches()) {
      matcher = X_PATTERN.matcher(name);
    }
    if (!matcher.matches()) {
      matcher = COMMA_PATTERN.matcher(name);
    }
    
    if (matcher.matches()) {
      try {
        nameAndLatLong.latLong = LatLongCoordinates.withLatAndLong(
            matcher.group(2), matcher.group(3));
        nameAndLatLong.name = matcher.group(1);
      } catch (IllegalArgumentException e) {
        // Fall through
      }
    }
    return nameAndLatLong;
  }
}
