/*
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;

import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.inject.Inject;
import com.scythebill.birdlist.ui.guice.EBirdApiKey;

/**
 * Reads information about an ebird checklist.
 */
class EBirdChecklistApi {
  private final String eBirdApiKey;
  private final CloseableHttpClient httpClient;
  private Gson gson;

  public static class ChecklistInfo {
    public String countryCode;
    public String stateCode;
    public String countyName;
  }
  
  @Inject
  public EBirdChecklistApi(Gson gson, CloseableHttpClient httpClient,
      @EBirdApiKey String eBirdApiKey) {
    this.gson = gson;
    this.httpClient = httpClient;
    this.eBirdApiKey = eBirdApiKey;
  }

  public ChecklistInfo resolveChecklist(String checklistId) throws IOException {
    URI uri;
    try {
      uri =
          new URIBuilder("https://api.ebird.org/v2/product/checklist/view/" + checklistId).build();
    } catch (URISyntaxException e) {
      throw new RuntimeException(e);
    }

    ChecklistApiResult result;

    try (CloseableHttpResponse get = httpClient.execute(newHttpGet(uri))) {
      HttpEntity entity = get.getEntity();
  
      try (Reader reader = new InputStreamReader(new BufferedInputStream(entity.getContent()),
          // eBird API now appears to use UTF-8
          Charsets.UTF_8)) {
        result = gson.fromJson(reader, ChecklistApiResult.class);
      }
    }
    
    ChecklistInfo checklistInfo = new ChecklistInfo();
    if (!Strings.isNullOrEmpty(result.subnational1Code)) {
      List<String> codes = Splitter.on('-').omitEmptyStrings().splitToList(result.subnational1Code);
      if (codes.size() >= 1) {
        checklistInfo.countryCode = codes.get(0);
        if (codes.size() >= 2) {
          checklistInfo.stateCode = codes.get(1); 
        }
      }
    }

    if (!Strings.isNullOrEmpty(result.locId)) {
      URI hotspotApiUri;
      try {
        hotspotApiUri =
            new URIBuilder("https://api.ebird.org/v2/ref/hotspot/info/" + result.locId).build();
      } catch (URISyntaxException e) {
        throw new RuntimeException(e);
      }
      
      
      try (CloseableHttpResponse get = httpClient.execute(newHttpGet(hotspotApiUri))) {
        if (get.getStatusLine().getStatusCode() == 200) {
          HttpEntity entity = get.getEntity();
      
          try (Reader reader = new InputStreamReader(new BufferedInputStream(entity.getContent()),
              // eBird API now appears to use UTF-8
              Charsets.UTF_8)) {
            HotspotApiResult hotspot = gson.fromJson(reader, HotspotApiResult.class);
            if (!Strings.isNullOrEmpty(hotspot.subnational2Name)) {
              checklistInfo.countyName = hotspot.subnational2Name;
            }
            if (!Strings.isNullOrEmpty(hotspot.subnational1Code)) {
              List<String> codes = Splitter.on('-').omitEmptyStrings().splitToList(hotspot.subnational1Code);
              if (codes.size() >= 1) {
                checklistInfo.countryCode = codes.get(0);
                if (codes.size() >= 2) {
                  checklistInfo.stateCode = codes.get(1); 
                }
              }
            }
            
          }
        }
      }
      
    }
    
    return checklistInfo;
  }

  private HttpGet newHttpGet(final URI uri) {
    HttpGet httpGet = new HttpGet(uri);
    httpGet.setConfig(RequestConfig.custom().setConnectionRequestTimeout(5000)
        .setConnectTimeout(5000).setSocketTimeout(5000).build());
    httpGet.addHeader("X-eBirdApiToken", eBirdApiKey);
    return httpGet;
  }

  static class ChecklistApiResult {
    // TODO: observer names?
    public String subnational1Code;
    public String locId;
  }
  
  static class HotspotApiResult {
    public String subnational1Code;
    public String subnational2Name;
  }
}
