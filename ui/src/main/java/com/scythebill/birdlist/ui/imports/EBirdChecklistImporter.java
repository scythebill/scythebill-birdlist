/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.joda.time.Duration;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.Resources;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.Area;
import com.scythebill.birdlist.model.sighting.Distance;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfo.ObservationType;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.imports.EBird.NameAndLatLong;
import com.scythebill.birdlist.ui.imports.EBirdChecklistApi.ChecklistInfo;
import com.scythebill.birdlist.ui.imports.ParsedLocationIds.ToBeDecided;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Imports sightings from eBird "Checklist" export files.
 */
public class EBirdChecklistImporter extends CsvSightingsImporter {
  private final static Logger logger = Logger.getLogger(EBirdChecklistImporter.class.getName());
  
  private LineExtractor<String> nameExtractor;
  private LineExtractor<String> locationIdExtractor;
  private Map<VisitInfoKey, VisitInfo> visitInfoMap;
  private LineExtractor<String> observationTypeExtractor;
  private LineExtractor<String> durationExtractor;
  private LineExtractor<String> distanceExtractor;
  private LineExtractor<String> areaExtractor;
  private LineExtractor<Integer> partySizeExtractor;
  private LineExtractor<Boolean> completeExtractor;
  private final EBirdChecklistApi ebirdChecklistApi;
  private volatile String countryCode;
  private volatile String stateCode;
  private volatile String countyName;
  
  public EBirdChecklistImporter(ReportSet reportSet, Taxonomy taxonomy, Checklists checklists,
      PredefinedLocations predefinedLocations, File file, EBirdChecklistApi ebirdChecklistApi) {
    super(reportSet, taxonomy, checklists, predefinedLocations, file, file);
    this.ebirdChecklistApi = ebirdChecklistApi;
  }
  
  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return nameExtractor;
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    TaxonResolver taxonResolver = new TaxonResolver(taxonomy);
    taxonResolver.setMaximumNameDistance(1);
    return new FieldTaxonImporter<>(taxonResolver, 
        new EBird.CommonNameExtractor(nameExtractor, taxonResolver),
        new EBird.ScientificNameExtractor(nameExtractor, taxonResolver));
  }
  
  private static final Pattern CHECKLIST_PATTERN = Pattern.compile("(S[0-9]+).*");

  
  @Override
  public Runnable longRunningTask() {
    return new Runnable() {
      @Override
      public void run() {
        Matcher matcher = CHECKLIST_PATTERN.matcher(locationsFile.getName());
        if (matcher.matches()) {
          try {
            ChecklistInfo checklistInfo = ebirdChecklistApi.resolveChecklist(matcher.group(1));
            if (checklistInfo.countryCode != null) {
              countryCode = checklistInfo.countryCode;
            }
            
            if (checklistInfo.stateCode != null) {
              stateCode = checklistInfo.stateCode;
            }
            
            if (checklistInfo.countyName != null) {
              countyName = checklistInfo.countyName;
            }
            
          } catch (IOException e) {
            logger.log(Level.WARNING, "Could not load checklist information from eBird", e);
          }
        }
      }
    };
  }

  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    ImportLines lines = CsvImportLines.fromFile(locationsFile, getCharset());
    try {
      computeMappings(lines);
      
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }
        
        String id = locationIdExtractor.extract(line);
        if (locationIds.hasBeenParsed(id)) {
          continue;
        }
        
        NameAndLatLong nameAndLatLong = EBird.extractLatLongFromName(id);
        // If the checklist API successfully got a country code (and hopefully a state code), import directly
        if (countryCode != null) {
          ImportedLocation importedLocation = new ImportedLocation();
          importedLocation.countryCode = countryCode;
          importedLocation.stateCode = stateCode;
          importedLocation.county = countyName;
          importedLocation.locationNames.add(nameAndLatLong.name);
          if (nameAndLatLong.latLong != null) {
            importedLocation.latitude = nameAndLatLong.latLong.latitude();
            importedLocation.longitude = nameAndLatLong.latLong.longitude();
          }
          String locationId = importedLocation.addToLocationSet(reportSet, locations, locationShortcuts, predefinedLocations);
          if (locationId != null) {
            locationIds.put(id, locationId);
            continue;
          }
        } else {
          // If there's a lat-long that came out of the name, then use "tryAddToLocationSetWithLatLong",
          // which will use map data to identify a country (and sometimes a state)
          if (nameAndLatLong.latLong != null && !Strings.isNullOrEmpty(nameAndLatLong.name)) {
            ImportedLocation importedLocation = new ImportedLocation();
            importedLocation.locationNames.add(nameAndLatLong.name);
            importedLocation.latitude = nameAndLatLong.latLong.latitude();
            importedLocation.longitude = nameAndLatLong.latLong.longitude();
            String locationId = importedLocation.tryAddToLocationSetWithLatLong(reportSet, locations,
                locationShortcuts, predefinedLocations);
            if (locationId != null) {
              locationIds.put(id, locationId);
              continue;
            }
          }
        } 

        // Nope, need to directly import
        locationIds.addToBeResolvedLocationName(id,
            new ToBeDecided(nameAndLatLong.name, nameAndLatLong.latLong));
      }
    } catch (IOException | RuntimeException e) {
      throw new ImportException(
          Messages.getFormattedMessage(Name.FAILED_IMPORT_ON_LINE_FORMAT, lines.lineNumber()),
          e);
    } finally {
      lines.close();
    }
  }

  private static final ImmutableMultimap<String, String> HEADER_NAMES;
  static {
    ImmutableListMultimap.Builder<String, String> builder = ImmutableListMultimap.builder();
    URL resource = Resources.getResource(EBirdChecklistImporter.class, "ebird-checklist-headers.csv");
    try (ImportLines headers = CsvImportLines.fromUrl(resource, StandardCharsets.UTF_8)) {
      String[] englishLine = headers.nextLine();
      for (String english : englishLine) {
        builder.put(english, english);
      }
      
      while (true) {
        String[] line = headers.nextLine();
        if (line == null) {
          break;
        }
        
        Preconditions.checkState(line.length == englishLine.length);
        for (int i = 0; i < line.length; i++) {
          builder.put(englishLine[i], line[i]);
        }
      }
      
      
      HEADER_NAMES = builder.build();
    } catch (IOException e) {
      throw new AssertionError("Could not load EBird headers files");
    }
  }
   
  private int getRequiredLocalizedHeader(Map<String, Integer> headersByIndex, String name) {
    for (String localizedName : HEADER_NAMES.get(name)) {
      Integer index = headersByIndex.get(localizedName);
      if (index != null) {
        return index;
      }
    }

    throw new ImportException(
        Messages.getFormattedMessage(Name.IMPORT_REQUIRED_HEADER_MISSING_FORMAT, name));
  }
  
  static boolean matchesLocalizedHeader(String englishName, String headerEntry) {
    return HEADER_NAMES.get(englishName).contains(headerEntry);
  }
  
  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    String[] header = lines.nextLine();
    Map<String, Integer> headersByIndex = Maps.newHashMap();
    for (int i = 0; i < header.length; i++) {
      headersByIndex.put(header[i], i);
    }

    int speciesIndex = getRequiredLocalizedHeader(headersByIndex, "Species");
    int countIndex = getRequiredLocalizedHeader(headersByIndex, "Count");
    int locationIndex = getRequiredLocalizedHeader(headersByIndex, "Location");
    int dateIndex = getRequiredLocalizedHeader(headersByIndex, "Observation date");
    int startTimeIndex = getRequiredLocalizedHeader(headersByIndex, "Start Time");
    // Used to be Comments, now Details
    Integer commentIndex = headersByIndex.get("Comments");
    if (commentIndex == null) {
      commentIndex = getRequiredLocalizedHeader(headersByIndex, "Details");
    }
    
    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
    nameExtractor = LineExtractors.stringFromIndex(speciesIndex);
    locationIdExtractor = LineExtractors.stringFromIndex(locationIndex);
    
    mappers.add(new MultiFormatDateFromStringFieldMapper<>(
        LineExtractors.stringFromIndex(dateIndex),
        Locale.getDefault(),
        "dd MMM, yy",
        "MMM dd, yy",
        "dd MMM yy",
        "dd-MMM-yy",
        "dd/MM/yy",
        // Really weird - see
        // https://bitbucket.org/scythebill/scythebill-birdlist/issues/493/support-ebird-checklists-with-odd-date
        "MM dd, yy",
        "yy MMM dd",
        "dd.MM.yyyy",
        "dd.MMM.yyyy",
        "dd/MMM/yy",
        "yyyy-MM-dd"));
    mappers.add(new TimeMapper<>(LineExtractors.stringFromIndex(startTimeIndex)));
    mappers.add(new CountFieldMapper<>(
        LineExtractors.stringFromIndex(countIndex)));
    mappers.add(new DescriptionFieldMapper<>(
        LineExtractors.stringFromIndex(commentIndex)));
    
    // Find the visit info data 
    int observationTypeIndex = getRequiredLocalizedHeader(headersByIndex, "Observation type");
    int durationIndex = getRequiredLocalizedHeader(headersByIndex, "Duration");
    int distanceIndex = getRequiredLocalizedHeader(headersByIndex, "Distance");
    int areaIndex = getRequiredLocalizedHeader(headersByIndex, "Area");
    int partySizeIndex = getRequiredLocalizedHeader(headersByIndex, "Party Size");
    int completeIndex = getRequiredLocalizedHeader(headersByIndex, "Complete Checklist");
    visitInfoMap = Maps.newHashMap();
    observationTypeExtractor = LineExtractors.stringFromIndex(observationTypeIndex);
    durationExtractor = LineExtractors.stringFromIndex(durationIndex);
    distanceExtractor = LineExtractors.stringFromIndex(distanceIndex);
    areaExtractor = LineExtractors.stringFromIndex(areaIndex);
    partySizeExtractor = LineExtractors.intFromIndex(partySizeIndex);
    completeExtractor = LineExtractors.booleanFromIndex(completeIndex);
    
    return new ComputedMappings<>(
        new TaxonFieldMapper(nameExtractor),
        new LocationMapper(locationIdExtractor),
        mappers);
  }
  
  @Override
  protected Charset getCharset() {
    return Charsets.UTF_8;
  }

  @Override
  protected void lookForVisitInfo(
      ReportSet reportSet,
      String[] line,
      Sighting newSighting,
      VisitInfoKey visitInfoKey) {
    VisitInfo.Builder builder = VisitInfo.builder();
    builder.withObservationType(EBird.observationType(observationTypeExtractor.extract(line)));
    Duration duration = EBird.durationHrsMins(durationExtractor.extract(line));
    if (duration != null) {
      builder.withDuration(duration);
    }
    Distance distance = EBird.distanceMilesOrKilometers(distanceExtractor.extract(line));
    if (distance != null) {
      builder.withDistance(distance);
    }
    Area area = EBird.areaHectaresOrAcres(areaExtractor.extract(line));
    if (area != null) {
      builder.withArea(area);
    }
    Integer partySize = partySizeExtractor.extract(line);
    if (partySize != null && partySize > 0) {
      builder.withPartySize(partySize);
    }
    if (completeExtractor.extract(line)) {
      builder.withCompleteChecklist(true);
    }
    VisitInfo visitInfo;
    try {
      visitInfo = builder.build();
    } catch (IllegalStateException e) {
      // If building the visit info fails - there may be data missing
      // from the export (especially a missing required field).  Swap
      // the observation type to HISTORICAL (which accepts everything),
      // and try again.
      builder.withObservationType(ObservationType.HISTORICAL);
      visitInfo = builder.build();
    }
    
    // If there's any data worth storing, do so.
    if (visitInfo.hasData()) {
      visitInfoMap.put(visitInfoKey, visitInfo);
    }
  }

  @Override
  public Map<VisitInfoKey, VisitInfo> getAccumulatedVisitInfoMap() {
    return visitInfoMap;
  }
}
