/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.Duration;
import org.joda.time.ReadablePartial;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.Area;
import com.scythebill.birdlist.model.sighting.Distance;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfo.ObservationType;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.imports.ParsedLocationIds.ToBeDecided;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Imports sightings from files meant to be imported into eBird, in the
 * "eBird Record Format".  Handy because multiple programs support exporting into this format.
 */
public class EBirdImportImporter extends CsvSightingsImporter {
  private final LineExtractor<String> locationIdExtractor = LineExtractors.joined(
      Joiner.on('|').useForNull(""),
      LineExtractors.stringFromIndex(5),
      LineExtractors.stringFromIndex(10),
      LineExtractors.stringFromIndex(11),
      new WithinBracesIfCountryIsEmpty(LineExtractors.stringFromIndex(4), LineExtractors.stringFromIndex(11)));

  private final LineExtractor<String> taxonomyIdExtractor = LineExtractors.joined(
      Joiner.on('|').useForNull(""),
      LineExtractors.stringFromIndex(0),
      LineExtractors.stringFromIndex(1),
      LineExtractors.stringFromIndex(2));

  private LineExtractor<String> protocolExtractor;
  private LineExtractor<Integer> partySizeExtractor;
  private LineExtractor<Integer> durationExtractor;
  private LineExtractor<Boolean> completeExtractor;
  private LineExtractor<Float> distanceExtractor;
  private LineExtractor<Float> areaExtractor;
  private LineExtractor<String> visitCommentsExtractor;
  
  private Map<VisitInfoKey, VisitInfo> visitInfoMap;
  private LineExtractor<ReadablePartial> dateExtractor;

  public EBirdImportImporter(ReportSet reportSet, Taxonomy taxonomy, Checklists checklists, PredefinedLocations predefinedLocations, File file) {
    super(reportSet, taxonomy, checklists, predefinedLocations, file, file);
  }
  
  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return taxonomyIdExtractor;
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    LineExtractor<String> commonNameExtractor = LineExtractors.stringFromIndex(0);
    LineExtractor<String> speciesNameExtractor = LineExtractors.stringFromIndex(2);
    // *Don't* override TaxonResolver max name distance here;  I don't have confidence
    // that these imports use recent names.
    return new FieldTaxonImporter<>(taxonomy,
        commonNameExtractor,
        LineExtractors.joined(Joiner.on(' ').useForNull(""),
            LineExtractors.stringFromIndex(1),
            line -> dropSubspeciesInSpeciesName(speciesNameExtractor.extract(line))),
            line -> {
              String ssp = lookForSubspeciesInCommonName(commonNameExtractor.extract(line));
              if (ssp != null) {
                return ssp;
              }
              ssp = lookForSubspeciesInSpeciesName(speciesNameExtractor.extract(line));
              return ssp;
            });
  }
  
  private String lookForSubspeciesInCommonName(String commonName) {
    if (commonName == null) {
      return null;
    }
    
    int lastRightParen = commonName.lastIndexOf(')');
    int lastRightBrace = commonName.lastIndexOf(']');
    if (lastRightParen >= 0 && lastRightParen > lastRightBrace) {
      int firstLeftParen = commonName.lastIndexOf('(', lastRightParen);
      if (firstLeftParen < 0) {
        return null;
      }
      
      return commonName.substring(firstLeftParen + 1, lastRightParen);
    }
    
    if (lastRightBrace >= 0) {
      int firstLeftBrace = commonName.lastIndexOf('[', lastRightBrace);
      if (firstLeftBrace < 0) {
        return null;
      }
      
      return commonName.substring(firstLeftBrace + 1, lastRightBrace);
    }
    
    return null;
  }

  private String lookForSubspeciesInSpeciesName(String sciName) {
    if (sciName == null) {
      return null;
    }
    int lastSpace = sciName.lastIndexOf(' ');
    if (lastSpace < 0) {
      return null;
    }
    
    return sciName.substring(lastSpace + 1);
  }

  private String dropSubspeciesInSpeciesName(String sciName) {
    if (sciName == null) {
      return null;
    }
    int lastSpace = sciName.lastIndexOf(' ');
    if (lastSpace < 0) {
      return sciName;
    }
    
    return sciName.substring(0, lastSpace);
  }

  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    ImportLines lines = CsvImportLines.fromFile(locationsFile, getCharset());
    try {
      computeMappings(lines);
      
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }
        
        String id = locationIdExtractor.extract(line);
        if (locationIds.hasBeenParsed(id)) {
          continue;
        }
        ImportedLocation imported = new ImportedLocation();
        imported.stateCode = Strings.emptyToNull(line[10]);
        imported.countryCode = Strings.emptyToNull(line[11]);
        imported.latitude = Strings.emptyToNull(line[6]);
        imported.longitude = Strings.emptyToNull(line[7]);
        
        String location = Strings.emptyToNull(line[5]);
        if (imported.countryCode == null) {
          // No countryCode.  This is a poor excuse for an eBird import, but run with it...
          // Try to map the location to a country.
          // If that fails, see if it's a US or CA state/province? 
          Location country = locationShortcuts.getCountryExactMatch(location, null, null);
          if (country != null) {
            if (country.getEbirdCode() != null) {
              imported.countryCode = country.getEbirdCode();
            } else {
              imported.country = country.getModelName();
            }
          } else {
            Location state = locationShortcuts.getStateByName("US", location);
            if (state != null) {
              imported.countryCode = "US";
              imported.state = state.getModelName();
            } else {
              state = locationShortcuts.getStateByName("CA", location);
              if (state != null) {
                imported.countryCode = "CA";
                imported.state = state.getModelName();
              }
            }
          }
          
          if (imported.country == null && imported.countryCode == null) {
            imported.locationNames.add(location);
          }
        } else {
          imported.locationNames.add(location);
        }
        
        // Further hacking - look for a location embedded within square braces, at least 
        // if there's no country field (for ancient Birder's Diary imports).  If it's there grab it.
        if (Strings.isNullOrEmpty(line[11])) {
          String withinBraces = Strings.emptyToNull(WithinBracesIfCountryIsEmpty.withinBrace(line[4]));
          if (withinBraces != null) {
            imported.locationNames.add(withinBraces);
          }
        }

        //  No country or countryCode, but 
        if (imported.country == null && imported.countryCode == null && !imported.locationNames.isEmpty()) {
          locationIds.addToBeResolvedLocationName(id,
              new ToBeDecided(Joiner.on(", ").join(imported.locationNames)));
        } else {
          String locationId = imported.addToLocationSet(reportSet, locations, locationShortcuts, predefinedLocations);
          locationIds.put(id, locationId);
        }
      }
    } catch (IOException | RuntimeException e) {
      throw new ImportException(
          Messages.getFormattedMessage(Name.FAILED_IMPORT_ON_LINE_FORMAT, lines.lineNumber()),
          e);
    } finally {
      lines.close();
    }
  }

  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
    
    dateExtractor = new MultiFormatDateFromStringLineExtractor(
        LineExtractors.stringFromIndex(8),
        "MM/dd/yyyy",
        "MM-dd-yy");
        
    mappers.add(new MultiFormatDateFromStringFieldMapper<>(
        LineExtractors.stringFromIndex(8),
        "MM/dd/yyyy",
        "MM-dd-yy"));    
    mappers.add(new CountFieldMapper<>(LineExtractors.stringFromIndex(3)));
    mappers.add(new DescriptionFieldMapper<>(LineExtractors.stringFromIndex(4)));
    mappers.add(new TimeMapper<>(LineExtractors.stringFromIndex(9)));
    
    visitInfoMap = Maps.newHashMap();
    protocolExtractor = LineExtractors.stringFromIndex(12);
    partySizeExtractor = LineExtractors.intFromIndex(13);
    durationExtractor = LineExtractors.intFromIndex(14);
    completeExtractor = LineExtractors.booleanFromIndex(15);
    distanceExtractor = LineExtractors.floatFromIndex(16);
    areaExtractor = LineExtractors.floatFromIndex(17);
    visitCommentsExtractor = LineExtractors.stringFromIndex(18);
    
    return new ComputedMappings<>(
        new TaxonFieldMapper(taxonomyIdExtractor),
        new LocationMapper(locationIdExtractor),
        mappers);
  }

  
  /**
   * A hack for some old Birder's Diary "eBird" export files.
   * 
   * Back in 2013, it seemed that Birder's Diary didn't include country/state, and also 
   * put some extra location information within braces in the notes field?!?  Do this hack,
   * but only if another field (the country extractor) can't find any country data. 
   */
  static class WithinBracesIfCountryIsEmpty implements LineExtractor<String> {

    private final LineExtractor<String> extractor;
    private final LineExtractor<String> countryExtractor;

    WithinBracesIfCountryIsEmpty(LineExtractor<String> extractor, LineExtractor<String> countryExtractor) {
      this.extractor = extractor;
      this.countryExtractor = countryExtractor;
    }

    @Override
    public String extract(String[] line) {
      String country = countryExtractor.extract(line);
      if (!Strings.isNullOrEmpty(country)) {
        return null;
      }
      
      String extracted = extractor.extract(line);
      return withinBrace(extracted);
    }

    static String withinBrace(String extracted) {
      if (extracted == null) {
        return null;
      }
      
      int openBrace = extracted.indexOf('[');
      if (openBrace < 0) {
        return "";
      }
      int closeBrace = extracted.indexOf(']', openBrace + 1);
      if (closeBrace < 0) {
        return "";
      }
      return extracted.substring(openBrace + 1, closeBrace);
    }
  }

  @Override
  protected void lookForVisitInfo(
      ReportSet reportSet,
      String[] line,
      Sighting newSighting,
      VisitInfoKey visitInfoKey) {
    VisitInfo.Builder builder = VisitInfo.builder();
    ObservationType observationType = ObservationType.fromEBirdId(protocolExtractor.extract(line));
    if (observationType == null) {
      observationType = ObservationType.HISTORICAL;
    }
    
    Integer duration = durationExtractor.extract(line);
    if (duration != null) {
      builder.withDuration(Duration.standardMinutes(duration));
    }
    
    Float distance = distanceExtractor.extract(line);
    if (distance != null) {
      builder.withDistance(Distance.inMiles(distance));
    }
    Float area = areaExtractor.extract(line);
    if (area != null) {
      builder.withArea(Area.inAcres(area));
    }
    Integer partySize = partySizeExtractor.extract(line);
    if (partySize != null && partySize > 0) {
      builder.withPartySize(partySize);
    }
    
    String visitComments = visitCommentsExtractor.extract(line);
    if (!Strings.isNullOrEmpty(visitComments)) {
      builder.withComments(visitComments);
    }
    
    if (completeExtractor.extract(line)) {
      builder.withCompleteChecklist(true);
    }
    VisitInfo visitInfo;
    try {
      visitInfo = builder.build();
    } catch (IllegalStateException e) {
      // If building the visit info fails - there may be data missing
      // from the export (especially a missing required field).  Swap
      // the observation type to HISTORICAL (which accepts everything),
      // and try again.
      builder.withObservationType(ObservationType.HISTORICAL);
      visitInfo = builder.build();
    }
    
    // If there's any data worth storing, do so.
    if (visitInfo.hasData()) {
      visitInfoMap.put(visitInfoKey, visitInfo);
    }
  }

  @Override
  public Map<VisitInfoKey, VisitInfo> getAccumulatedVisitInfoMap() {
    return visitInfoMap;
  }

  @Override
  public boolean isMassExport() {
    // "Imports" *might* be a mass export;  let Scythebill import more carefully.
    return true;
  }

  @Override
  public Collection<ReadablePartial> getAllDates() throws IOException {
    try (ImportLines lines = importLines(locationsFile)) {
      computeMappings(lines);
      
      Set<ReadablePartial> allDates = new LinkedHashSet<>();
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }
        ReadablePartial date = dateExtractor.extract(line);
        if (date != null) {
          allDates.add(date);
        }
      }
      return allDates;
    }
  }

}
