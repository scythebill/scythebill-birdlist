/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.imports.EBird.NameAndLatLong;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Imports sightings from eBird "Life List" export files.
 */
public class EBirdLifeListImporter extends CsvSightingsImporter {
  private final LineExtractor<String> locationIdExtractor = LineExtractors.joined(
      Joiner.on('|').useForNull(""),
      LineExtractors.stringFromIndex(3),
      LineExtractors.stringFromIndex(4));

  private final LineExtractor<String> taxonomyIdExtractor = LineExtractors.stringFromIndex(1);
  
  public EBirdLifeListImporter(ReportSet reportSet, Taxonomy taxonomy, Checklists checklists, PredefinedLocations predefinedLocations, File file) {
    super(reportSet, taxonomy, checklists, predefinedLocations, file, file);
  }
  
  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return taxonomyIdExtractor;
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    TaxonResolver taxonResolver = new TaxonResolver(taxonomy);
    taxonResolver.setMaximumNameDistance(1);
    return new FieldTaxonImporter<>(taxonResolver,
        new EBird.CommonNameExtractor(LineExtractors.stringFromIndex(1), taxonResolver),
        new EBird.ScientificNameExtractor(LineExtractors.stringFromIndex(1), taxonResolver));
  }

  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    ImportLines lines = CsvImportLines.fromFile(locationsFile, getCharset());
    try {
      computeMappings(lines);
      
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }
        
        String id = locationIdExtractor.extract(line);
        if (locationIds.hasBeenParsed(id)) {
          continue;
        }
        ImportedLocation imported = new ImportedLocation();
        List<String> countryAndState = Lists.newArrayList(
                Splitter.on('-').split(line[4]));
        if (countryAndState.size() != 2) {
          throw new IllegalArgumentException("Doesn't look like an EBird file.  Location contains " + line[5]);
        }
        imported.countryCode = countryAndState.get(0);
        imported.stateCode = countryAndState.get(1);
        
        if (!Strings.isNullOrEmpty(line[3])) {
          NameAndLatLong nameAndLatLong = EBird.extractLatLongFromName(line[3]);
          imported.locationNames.add(nameAndLatLong.name);
          if (nameAndLatLong.latLong != null) {
            imported.latitude = nameAndLatLong.latLong.latitudeAsCanonicalString();
            imported.longitude = nameAndLatLong.latLong.longitudeAsCanonicalString();
          }
        }

        String locationId = imported.addToLocationSet(reportSet, locations, locationShortcuts, predefinedLocations);
        locationIds.put(id, locationId);
      }
    } catch (IOException | RuntimeException e) {
      throw new ImportException(
          Messages.getFormattedMessage(Name.FAILED_IMPORT_ON_LINE_FORMAT, lines.lineNumber()),
          e);
    } finally {
      lines.close();
    }
  }

  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    lines.nextLine();
    
    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
        
    mappers.add(new MultiFormatDateFromStringFieldMapper<>(
        LineExtractors.stringFromIndex(5),
        Locale.US,
        "dd MMM yy",
        "dd-MMM-yy"));
    mappers.add(new CountFieldMapper<>(LineExtractors.stringFromIndex(2)));
    return new ComputedMappings<>(
        new TaxonFieldMapper(taxonomyIdExtractor),
        new LocationMapper(locationIdExtractor),
        mappers);
  }
}
