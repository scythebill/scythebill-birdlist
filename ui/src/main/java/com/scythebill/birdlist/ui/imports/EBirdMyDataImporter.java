/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.joda.time.Duration;
import org.joda.time.ReadablePartial;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.Area;
import com.scythebill.birdlist.model.sighting.Distance;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Photo;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfo.ObservationType;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Imports sightings from eBird "My Data" export files.
 */
public class EBirdMyDataImporter extends CsvSightingsImporter {
  private static final Logger logger = Logger.getLogger(EBirdMyDataImporter.class.getName());
  private LineExtractor<String> locationIdExtractor;
  
  private LineExtractor<String> taxonomyIdExtractor;
  private Map<VisitInfoKey, VisitInfo> visitInfoMap;

  private LineExtractor<String> observationTypeExtractor;

  private LineExtractor<Integer> durationExtractor;

  private LineExtractor<Float> distanceKmExtractor;

  private LineExtractor<Float> areaHectaresExtractor;

  private LineExtractor<Integer> partySizeExtractor;

  private LineExtractor<Boolean> completeExtractor;

  private LineExtractor<String> visitCommentsExtractor;

  private LineExtractor<String> commonExtractor;

  private LineExtractor<String> scientificExtractor;
  private LineExtractor<String> countryAndStateExtractor;
  private LineExtractor<String> countyExtractor;
  private LineExtractor<String> locationExtractor;
  private LineExtractor<String> latitudeExtractor;
  private LineExtractor<String> longitudeExtractor;
  private LineExtractor<ReadablePartial> dateExtractor;
  private ImmutableSet<ReadablePartial> onlyImportDatesIn;
  
  public EBirdMyDataImporter(ReportSet reportSet, Taxonomy taxonomy, Checklists checklists, PredefinedLocations predefinedLocations, File file) {
    super(reportSet, taxonomy, checklists, predefinedLocations, file, file);
  }
  
  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return taxonomyIdExtractor;
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    TaxonResolver taxonResolver = new TaxonResolver(taxonomy);
    taxonResolver.setMaximumNameDistance(1);
    return new FieldTaxonImporter<>(taxonResolver,
        commonExtractor,
        new JustBinomial(scientificExtractor),
        new AfterBinomial(scientificExtractor));
  }
  
  static class JustBinomial implements LineExtractor<String> {
    private LineExtractor<String> stringFromIndex;

    public JustBinomial(LineExtractor<String> stringFromIndex) {
      this.stringFromIndex = stringFromIndex;
    }

    @Override
    public String extract(String[] line) {
      String text = stringFromIndex.extract(line);
      if (hasSubspecies(text)) {
        int secondSpace = secondSpace(text);
        text = text.substring(0, secondSpace);
      }
      return text;
    }
  }

  static class AfterBinomial implements LineExtractor<String> {
    private LineExtractor<String> stringFromIndex;

    public AfterBinomial(LineExtractor<String> stringFromIndex) {
      this.stringFromIndex = stringFromIndex;
    }

    @Override
    public String extract(String[] line) {
      String text = stringFromIndex.extract(line);
      if (hasSubspecies(text)) {
        int secondSpace = secondSpace(text);
        return text.substring(secondSpace + 1);
      }
      return null;
    }
  }
  
  static class MLCatalogNumbersFieldMapper implements FieldMapper<String[]> {

    private static final String MACAULAY_LIBRARY_URL_FORMAT = "https://macaulaylibrary.org/asset/%s";
    private static final Splitter CATALOG_NUMBERS_SPLITTER = Splitter.on(CharMatcher.whitespace());
    private final LineExtractor<String> extractor;

    public MLCatalogNumbersFieldMapper(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public void map(String[] line, Sighting.Builder sighting) {
      String catalogNumbers = extractor.extract(line);
      if (!Strings.isNullOrEmpty(catalogNumbers)) {
        List<Photo> photos = new ArrayList<>();
        for (String catalogNumber : CATALOG_NUMBERS_SPLITTER.split(catalogNumbers)) {
          String catalogUri = String.format(MACAULAY_LIBRARY_URL_FORMAT, catalogNumber);
          try {
            URI uri = new URI(String.format(MACAULAY_LIBRARY_URL_FORMAT, catalogNumber));
            photos.add(new Photo(uri));
          } catch (URISyntaxException e) {
            logger.warning("Invalid library URI: " + catalogUri);
          }
        }
        
        if (!photos.isEmpty()) {
          sighting.getSightingInfo().setPhotographed(true);
        }
        sighting.getSightingInfo().setPhotos(photos);
      }
    }
    
  }
  
  static boolean hasSubspecies(String s) {
    return (CharMatcher.whitespace().countIn(s) > 1)
        && !s.contains(" x "); // " x " marks a hybrid
  }

  static int secondSpace(CharSequence s) {
    int first = CharMatcher.whitespace().indexIn(s);
    if (first < 0) {
      return first;
    }
    return CharMatcher.whitespace().indexIn(s, first + 1);
  }
  
  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    ImportLines lines = CsvImportLines.fromFile(locationsFile, getCharset());
    try {
      computeMappings(lines);
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }
        
        String id = locationIdExtractor.extract(line);
        if (locationIds.hasBeenParsed(id)) {
          continue;
        }
        ImportedLocation imported = new ImportedLocation();
        List<String> countryAndState = Lists.newArrayList(
                Splitter.on('-').split(countryAndStateExtractor.extract(line)));
        if (countryAndState.size() != 2) {
          throw new IllegalArgumentException("Doesn't look like an EBird file.  Location contains " + line[5]);
        }
        imported.countryCode = countryAndState.get(0);
        imported.stateCode = countryAndState.get(1);
        imported.county = Strings.emptyToNull(countyExtractor.extract(line));
        imported.locationNames.add(Strings.emptyToNull(locationExtractor.extract(line)));
        
        imported.latitude = Strings.emptyToNull(latitudeExtractor.extract(line));
        imported.longitude = Strings.emptyToNull(longitudeExtractor.extract(line));

        String locationId = imported.addToLocationSet(reportSet, locations, locationShortcuts, predefinedLocations);
        locationIds.put(id, locationId);
      }
    } catch (IOException | RuntimeException e) {
      throw new ImportException(
          Messages.getFormattedMessage(Name.FAILED_IMPORT_ON_LINE_FORMAT, lines.lineNumber()),
          e);
    } finally {
      lines.close();
    }
  }

  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    String[] header = lines.nextLine();
    
    Map<String, Integer> headersByIndex = Maps.newHashMap();
    for (int i = 0; i < header.length; i++) {
      headersByIndex.put(header[i].toLowerCase(), i);
    }

    // TODO: convert everything to find-in-the-header
    Integer timeIndex = headersByIndex.get("time");
    Integer breedingCodeIndex = headersByIndex.get("breeding code"); 
    Integer speciesCommentsIndex = headersByIndex.get("species comments");
    Integer mlCatalogNumbersIndex = headersByIndex.get("ml catalog numbers");
    // Changed from Species Comments to Observation Details in Oct. 2019
    if (speciesCommentsIndex == null) {
      speciesCommentsIndex = headersByIndex.get("observation details");
    }
    Integer countIndex = headersByIndex.get("count");
    int dateIndex = getRequiredHeader(headersByIndex, "Date");

    int countryAndStateIndex = getRequiredHeader(headersByIndex, "State/Province");
    int countyIndex = getRequiredHeader(headersByIndex, "County");
    int locationIndex = getRequiredHeader(headersByIndex, "Location");
    
    countryAndStateExtractor = LineExtractors.stringFromIndex(countryAndStateIndex);
    countyExtractor = LineExtractors.stringFromIndex(countyIndex);
    locationExtractor = LineExtractors.stringFromIndex(locationIndex);
    
    LineExtractor<String> joined = LineExtractors.joined(
        Joiner.on('|').useForNull(""),
        countryAndStateExtractor,
        countyExtractor,
        locationExtractor);
    locationIdExtractor = joined;
        
    Integer latitudeIndex = headersByIndex.get("latitude");
    Integer longitudeIndex = headersByIndex.get("longitude");
    latitudeExtractor = latitudeIndex == null
        ? LineExtractors.<String>alwaysNull()
        : LineExtractors.stringFromIndex(latitudeIndex);
    longitudeExtractor = longitudeIndex == null
        ? LineExtractors.<String>alwaysNull()
        : LineExtractors.stringFromIndex(longitudeIndex);
    int commonIndex = getRequiredHeader(headersByIndex, "Common Name");
    commonExtractor = LineExtractors.stringFromIndex(commonIndex);
    int sciIndex = getRequiredHeader(headersByIndex, "Scientific Name");
    scientificExtractor = LineExtractors.stringFromIndex(sciIndex);
    taxonomyIdExtractor = LineExtractors.joined(
        Joiner.on('|').useForNull(""),
        commonExtractor,
        scientificExtractor);
    dateExtractor = new MultiFormatDateFromStringLineExtractor(
        LineExtractors.stringFromIndex(dateIndex),
        "yy-MM-dd",
        "yy/MM/dd");
        
    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
    mappers.add(
        new FieldMapper<String[]>() {
          @Override
          public void map(String[] row, Sighting.Builder sighting) {
            ReadablePartial date = dateExtractor.extract(row);
            if (date != null) {
              sighting.setDate(date);
            }
          }
        });
    if (countIndex != null) {
      mappers.add(new CountFieldMapper<>(LineExtractors.stringFromIndex(4)));
    }
    if (speciesCommentsIndex != null) {
      mappers.add(new DescriptionFieldMapper<>(LineExtractors.stringFromIndex(speciesCommentsIndex)));
    }
    if (timeIndex != null) {
      mappers.add(new TimeMapper<>(LineExtractors.stringFromIndex(timeIndex)));
    }
    if (breedingCodeIndex != null) {
      mappers.add(new BreedingBirdCodeFieldMapper<>(LineExtractors.stringFromIndex(breedingCodeIndex)));
    }
    if (mlCatalogNumbersIndex != null) {
      mappers.add(new MLCatalogNumbersFieldMapper(LineExtractors.stringFromIndex(mlCatalogNumbersIndex)));
    }

    // Find the visit info data 
    int observationTypeIndex = getRequiredHeader(headersByIndex, "Protocol");
    Integer durationIndexAsMinutes = headersByIndex.get("duration (min)");
    Integer durationIndexAsHours = headersByIndex.get("duration (hrs)");
    Integer completeIndex = headersByIndex.get("all obs reported");
    Integer distanceKmIndex = headersByIndex.get("distance traveled (km)");
    Integer areaHectaresIndex = headersByIndex.get("area covered (ha)");
    Integer partySizeIndex = headersByIndex.get("number of observers");
    Integer visitCommentsIndex = headersByIndex.get("checklist comments");
    visitInfoMap = Maps.newHashMap();
    observationTypeExtractor = LineExtractors.stringFromIndex(observationTypeIndex);
    if (durationIndexAsHours != null) {
      final LineExtractor<Float> durationInHours = LineExtractors.floatFromIndex(durationIndexAsHours);
      durationExtractor = new LineExtractor<Integer>() {
        @Override public Integer extract(String[] line) {
          Float extracted = durationInHours.extract(line);
          if (extracted == null) {
            return null;
          }
          return (int) (extracted * 60);
        }        
      };
    } else if (durationIndexAsMinutes != null) {
      durationExtractor = LineExtractors.intFromIndex(durationIndexAsMinutes);
    } else {
      durationExtractor = LineExtractors.alwaysNull();
    }
    
    distanceKmExtractor = distanceKmIndex != null ? LineExtractors.floatFromIndex(distanceKmIndex) : LineExtractors.alwaysNull();
    areaHectaresExtractor = areaHectaresIndex != null ? LineExtractors.floatFromIndex(areaHectaresIndex) : LineExtractors.alwaysNull();
    partySizeExtractor = partySizeExtractor != null ? LineExtractors.intFromIndex(partySizeIndex) : LineExtractors.alwaysNull();
    completeExtractor = completeIndex == null
        ? LineExtractors.constant(false)
        : LineExtractors.booleanFromIndex(completeIndex);
    visitCommentsExtractor = visitCommentsIndex != null ? LineExtractors.stringFromIndex(visitCommentsIndex) : LineExtractors.alwaysNull();
    return new ComputedMappings<>(
        new TaxonFieldMapper(taxonomyIdExtractor),
        new LocationMapper(locationIdExtractor),
        mappers);
  }


  @Override
  protected void lookForVisitInfo(
      ReportSet reportSet,
      String[] line,
      Sighting newSighting,
      VisitInfoKey visitInfoKey) {
    VisitInfo.Builder builder = VisitInfo.builder();
    builder.withObservationType(EBird.observationType(observationTypeExtractor.extract(line)));
    Integer duration = durationExtractor.extract(line);
    if (duration != null && duration > 0) {
      builder.withDuration(Duration.standardMinutes(duration));
    }
    Float kms = distanceKmExtractor.extract(line);
    if (kms != null && kms > 0.0f) {
      builder.withDistance(Distance.inKilometers(kms));
    }
    
    Float hectares = areaHectaresExtractor.extract(line);
    if (hectares != null && hectares > 0.0f) {
      builder.withArea(Area.inHectares(hectares));
    }
    Integer partySize = partySizeExtractor.extract(line);
    if (partySize != null && partySize > 0) {
      builder.withPartySize(partySize);
    }
    if (completeExtractor.extract(line)) {
      builder.withCompleteChecklist(true);
    }
    String visitComments = visitCommentsExtractor.extract(line);
    if (!Strings.isNullOrEmpty(visitComments)) {
      builder.withComments(visitComments);
    }
    VisitInfo visitInfo;
    try {
      visitInfo = builder.build();
    } catch (IllegalStateException e) {
      // If building the visit info fails - there may be data missing
      // from the export (especially a missing required field).  Swap
      // the observation type to HISTORICAL (which accepts everything),
      // and try again.
      builder.withObservationType(ObservationType.HISTORICAL);
      visitInfo = builder.build();
    }
    
    // If there's any data worth storing, do so.
    if (visitInfo.hasData()) {
      visitInfoMap.put(visitInfoKey, visitInfo);
    }
  }

  @Override
  public Map<VisitInfoKey, VisitInfo> getAccumulatedVisitInfoMap() {
    return visitInfoMap;
  }

  @Override
  public boolean isMassExport() {
    // "My Data" is a mass export;  let Scythebill import more carefully.
    return true;
  }

  /**
   * Must be overridden if isMassExport() returns true.
   */
  @Override
  public Collection<ReadablePartial> getAllDates() throws IOException {
    try (ImportLines lines = importLines(locationsFile)) {
      computeMappings(lines);
      
      Set<ReadablePartial> allDates = new LinkedHashSet<>();
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }
        ReadablePartial date = dateExtractor.extract(line);
        if (date != null) {
          allDates.add(date);
        }
      }
      return allDates;
    }
  }

  /** Must be overridden and honored if isMassExport() returns true. */
  @Override
  public void onlyImportDatesIn(Set<ReadablePartial> newDates) {
    this.onlyImportDatesIn = ImmutableSet.copyOf(newDates);
  }
  
  @Override
  protected String getFailedParseFormat(boolean allDateFailures) {
    // A lot of people try opening MyData in Excel to edit, then get
    // errors with date parsing.
    if (allDateFailures) {
      return Messages.getMessage(Name.EBIRD_DATES_DID_NOT_PARSE);      
    }
    
    return super.getFailedParseFormat(allDateFailures);
  }  

  @Override
  protected ImportLines importLines(File file) throws IOException {
    ImportLines importLines = super.importLines(file);
    if (onlyImportDatesIn == null) {
      return importLines;
    }
    
    return new OnlyIncludeRowsMatchingDates(importLines, onlyImportDatesIn, dateExtractor);
  }
}
