/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.util.Iterator;

import javax.annotation.Nullable;

import com.google.common.base.CharMatcher;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Maps the taxon off a set of a fields - use when the data can be 
 * analyzed to extract "this is the taxon name", as opposed to looking
 * for taxa in a blob of text.
 */
public class FieldTaxonImporter<T> extends TaxonImporter<T> {
  private final RowExtractor<T, String> commonNameExtractor;
  private final RowExtractor<T, String> sciNameExtractor;
  private final RowExtractor<T, String> subspecies;
  private final RowExtractor<T, String> commonOrSciNameExtractor;
  private final TaxonResolver taxonResolver;

  /**
   * Importer when there is a name, but it is not known if it's a common or
   * scientific name.
   */
  public FieldTaxonImporter(
      Taxonomy taxonomy,
      RowExtractor<T, String> commonOrSciNameExtractor) {
    this.commonOrSciNameExtractor = commonOrSciNameExtractor;
    this.sciNameExtractor = null;
    this.commonNameExtractor = null;
    this.subspecies = null;
    this.taxonResolver = new TaxonResolver(taxonomy);
  }

  public FieldTaxonImporter(
      Taxonomy taxonomy,
      RowExtractor<T, String> commonName,
      RowExtractor<T, String> sciName) {
    this(taxonomy, commonName, new JustGenusAndSpecies<>(sciName), new SubspeciesIfPresent<>(sciName));
  }

  public FieldTaxonImporter(
      TaxonResolver taxonResolver,
      RowExtractor<T, String> commonName,
      RowExtractor<T, String> sciName) {
    this(taxonResolver, commonName, new JustGenusAndSpecies<>(sciName), new SubspeciesIfPresent<>(sciName));
  }

  static private final Splitter SCI_SPLITTER = Splitter.on(' ').omitEmptyStrings().trimResults();
  
  static class JustGenusAndSpecies<T> implements RowExtractor<T, String> {
    private final RowExtractor<T, String> sciName;

    JustGenusAndSpecies(RowExtractor<T, String> sciName) {
      this.sciName = Preconditions.checkNotNull(sciName);
    }

    @Override
    public String extract(T line) {
      String sci = sciName.extract(line);
      if (sci == null) {
        return null;
      }
      
      if (CharMatcher.whitespace().countIn(sci) <= 1) {
        return sci;
      }
      
      Iterable<String> split = SCI_SPLITTER.split(sci);
      if (Iterables.size(split) != 3) {
        return sci;
      }
      
      Iterator<String> iterator = split.iterator();
      return iterator.next() + " " + iterator.next();
    }    
  }

  static class SubspeciesIfPresent<T> implements RowExtractor<T, String> {
    private final RowExtractor<T, String> sciName;
    
    SubspeciesIfPresent(RowExtractor<T, String> sciName) {
      this.sciName = Preconditions.checkNotNull(sciName);
    }

    @Override
    public String extract(T line) {
      String sci = sciName.extract(line);
      if (sci == null) {
        return null;
      }

      if (CharMatcher.whitespace().countIn(sci) <= 1) {
        return null;
      }
      
      Iterable<String> split = SCI_SPLITTER.split(sci);
      if (Iterables.size(split) != 3) {
        return null;
      }
      
      return Iterables.getLast(split);
    }
  }
  
  public FieldTaxonImporter(
      Taxonomy taxonomy,
      RowExtractor<T, String> commonName,
      @Nullable RowExtractor<T, String> sciName,
      @Nullable RowExtractor<T, String> subspecies) {
    this(new TaxonResolver(taxonomy), commonName, sciName, subspecies);
  }

  public FieldTaxonImporter(
      TaxonResolver taxonResolver,
      RowExtractor<T, String> commonName,
      @Nullable RowExtractor<T, String> sciName,
      @Nullable RowExtractor<T, String> subspecies) {
    this.taxonResolver = Preconditions.checkNotNull(taxonResolver);
    this.commonNameExtractor = Preconditions.checkNotNull(commonName);
    if (sciName == null) {
      sciName = RowExtractors.alwaysNull();
    }
    this.sciNameExtractor = sciName;
    if (subspecies == null) {
      subspecies = RowExtractors.alwaysNull();
    }
    this.subspecies = subspecies;
    this.commonOrSciNameExtractor = null;
  }
  
  @Override
  public ToBeDecided decideLater(T line) {
    ToBeDecided toBeDecided = new ToBeDecided();
    if (commonOrSciNameExtractor != null) {
      toBeDecided.commonName = commonOrSciNameExtractor.extract(line);
    } else {
      String commonName = commonNameExtractor.extract(line);
      String sciName = sciNameExtractor.extract(line);
      String subspeciesName = subspecies.extract(line);
      
      toBeDecided.commonName = Strings.emptyToNull(commonName);
      toBeDecided.scientificName = Strings.emptyToNull(sciName);
      toBeDecided.subspecies = Strings.emptyToNull(subspeciesName);
    }
    return toBeDecided;
  }
  
  @Override
  public TaxonPossibilities map(T line) {
    if (commonOrSciNameExtractor != null) {
      String commonOrSci = Strings.emptyToNull(commonOrSciNameExtractor.extract(line));
      if (commonOrSci == null) {
        return null;
      }
      
      return taxonResolver.mapUnknown(commonOrSci);
    } else {
      String commonName = Strings.emptyToNull(commonNameExtractor.extract(line));
      String subspeciesName = subspecies.extract(line);
      String sciName = Strings.emptyToNull(sciNameExtractor.extract(line));
      return taxonResolver.map(commonName, sciName, subspeciesName);
    }
  }

}