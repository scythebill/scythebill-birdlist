/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.google.common.base.Strings;
import com.google.common.io.ByteSource;
import com.google.common.io.Files;
import com.scythebill.birdlist.model.io.ImportLines;

/**
 * An ImportLines implementation for the HBW "XLS" format.  It's named ".xls", but it's not
 * an Excel sheet at all - it's an HTML file containing a table!
 */
class HbwImportLines {
  public static void main(String[] args) throws Exception {
    try (ImportLines importLines =
        importHbwXls(Files.asByteSource(new File("/Users/awiner/Downloads/HBW_Alive-sightings-20190225.xls")))) {
      while (true) {
        String[] line = importLines.nextLine();
        if (line == null) {
          break;
        }
        
        System.out.println(importLines.lineNumber() + ": " + Arrays.asList(line));
      }
    }
  }
  
  public static ImportLines importHbwXls(ByteSource byteSource) throws IOException {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    factory.setIgnoringElementContentWhitespace(true);
    try (InputStream inputStream = byteSource.openBufferedStream()) {
      try {
        // Force the charset to UTF-8, instead of relying on the default.
        InputSource source = new InputSource(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
        Document document = factory.newDocumentBuilder().parse(source);
        return new HbwXlsImportLines(document);
      } catch (SAXException e) {
        throw new IOException("Unexpected file contents", e);
      } catch (ParserConfigurationException e) {
        throw new AssertionError(e);
      }
    }  
  }
  
  static class HbwXlsImportLines implements ImportLines {
    private int rowNum;
    private final List<String[]> lines;
    private final boolean trim;

    HbwXlsImportLines(Document document) throws SAXParseException {
      this.lines = parseDocument(document);
      this.rowNum = 0;
      this.trim = true;

    }

    public HbwXlsImportLines(List<String[]> lines, boolean trim) {
      this.lines = lines;
      this.rowNum = 0;
      this.trim = trim;
    }

    private List<String[]> parseDocument(Document document) throws SAXParseException {
      List<String[]> lines = new ArrayList<>();
      NodeList rowElements = document.getElementsByTagName("tr");
      for (int i = 0; i < rowElements.getLength(); i++) {
        Node node = rowElements.item(i);
        lines.add(parseRow(node));
      }

      return lines;
    }

    private String[] parseRow(Node node) throws SAXParseException {
      List<String> text = new ArrayList<String>();
      NodeList children = node.getChildNodes();
      for (int i = 0; i < children.getLength(); i++) {
        Node child = children.item(i);
        if (child.getNodeType() == Node.ELEMENT_NODE) {
          text.add(getTextContent(child));
        }
      }
      return text.toArray(new String[text.size()]);
    }

    private String getTextContent(Node child) {
      String textContent = child.getTextContent();
      if (trim) {
        textContent = textContent.trim();
      }

      return textContent;
    }

    @Override
    public void close() throws IOException {}

    @Override
    public String[] nextLine() throws IOException {
      if (rowNum >= lines.size()) {
        return null;
      }

      return lines.get(rowNum++);
    }

    @Override
    public int lineNumber() {
      return rowNum;
    }

    @Override
    public ImportLines withoutTrimming() {
      return new HbwXlsImportLines(lines, false);
    }
  }

  public static ImportLines importHbwXlsx(ByteSource byteSource) throws IOException {
    try (InputStream inputStream = byteSource.openBufferedStream()) {
      try (XSSFWorkbook workbook = new XSSFWorkbook(byteSource.openStream())) {
        XSSFSheet sheet = workbook.getSheetAt(0);
        return new HbwXlsxImportLines(sheet);
      } catch (SAXParseException e) {
        throw new IOException("Could not parse HBW XSLX file", e);
      }
    }
  }
  
  static class HbwXlsxImportLines implements ImportLines {
    private int rowNum;
    private final boolean trim;
    private final DataFormatter df = new DataFormatter();

    private XSSFSheet sheet;

    HbwXlsxImportLines(XSSFSheet sheet) throws SAXParseException {
      this(sheet, true);

    }

    public HbwXlsxImportLines(XSSFSheet sheet, boolean trim) {
      this.sheet = sheet;
      this.rowNum = sheet.getFirstRowNum();
      this.trim = trim;
    }

    @Override
    public void close() throws IOException {}

    @Override
    public String[] nextLine() throws IOException {
      if (rowNum > sheet.getLastRowNum()) {
        return null;
      }

      XSSFRow row = sheet.getRow(rowNum++);
      // Stop at the first blank row
      if (row == null) {
        return null;
      }
      
      String[] line = new String[row.getLastCellNum() - row.getFirstCellNum()];
      for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
        XSSFCell cell = row.getCell(i);
        if (cell != null) {
          String cellValue = df.formatCellValue(cell);
          if (trim) {
            cellValue = cellValue.trim();
          }
          line[i - row.getFirstCellNum()] = cellValue;
        }
      }
      
      for (String cell : line) {
        if (!Strings.isNullOrEmpty(cell)) {
          return line;
        }
      }
      
      return null;
    }

    @Override
    public int lineNumber() {
      return rowNum;
    }

    @Override
    public ImportLines withoutTrimming() {
      return new HbwXlsxImportLines(sheet, false);
    }
  }

}
