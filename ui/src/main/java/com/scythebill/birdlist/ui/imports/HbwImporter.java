/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.joda.time.chrono.GJChronology;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;
import com.google.common.io.Files;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting.Builder;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.imports.ParsedLocationIds.ToBeDecided;

/** Imports HBW Alive "xls" files. */
class HbwImporter extends CsvSightingsImporter {

  private LineExtractor<String> taxonomyIdExtractor;
  private LineExtractor<String> commonNameExtractor;
  private LineExtractor<String> scientificExtractor;
  private LineExtractor<String> subspeciesExtractor;
  private LineExtractor<String> territoryExtractor;
  private LineExtractor<String> birdlistExtractor;
  private LineExtractor<String> longitudeExtractor;
  private LineExtractor<String> latitudeExtractor;
  private LineExtractor<String> locationIdExtractor;
  private LineExtractor<String> dateExtractor;
  // FieldMapper computed after a second, bonus pass through the file to
  // determine a date format.
  private FieldMapper<String[]> dateMapper;

  protected HbwImporter(ReportSet reportSet, Taxonomy taxonomy, Checklists checklists,
      PredefinedLocations predefinedLocations, File file) {
    super(reportSet, taxonomy, checklists, predefinedLocations, file, file);
  }

  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return taxonomyIdExtractor;
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    return new FieldTaxonImporter<>(taxonomy, commonNameExtractor, scientificExtractor,
        subspeciesExtractor);
  }
  
  class DelayedDateMapper implements FieldMapper<String[]> {
    @Override
    public void map(String[] line, Builder sighting) {
      dateMapper.map(line, sighting);
    }
  }

  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    String[] header = lines.nextLine();
    Map<String, Integer> headersByIndex = Maps.newHashMap();
    CharMatcher trimmer = CharMatcher.whitespace().or(CharMatcher.is('-'));
    Function<String, String> transform = s -> trimmer.removeFrom(s).toLowerCase(); 
    for (int i = 0; i < header.length; i++) {
      headersByIndex.put(transform.apply(header[i]), i);
    }

    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
    int commonIndex = getRequiredHeader(headersByIndex, "Common Name", transform);
    int sciIndex = getRequiredHeader(headersByIndex, "Scientific Name", transform);
    Integer sspIndex = headersByIndex.get("ssp");
    Integer territoryIndex = headersByIndex.get("territory");
    Integer birdlistIndex = headersByIndex.get("birdlist");
    int dateIndex = getRequiredHeader(headersByIndex, "Date", transform);
    Integer latitudeIndex = headersByIndex.get("latitude");
    Integer longitudeIndex = headersByIndex.get("longitude");
    Integer heardOnlyIndex = headersByIndex.get("heardonly");
    Integer photoIndex = headersByIndex.get("photo");
    Integer videoIndex = headersByIndex.get("video");
    Integer escapedIndex = headersByIndex.get("escaped");
    Integer captiveIndex = headersByIndex.get("captive");
    Integer countIndex = headersByIndex.get("#");
    Integer notesIndex = headersByIndex.get("notes");

    // Create a taxonomy extractor from the located common/scientific/ssp.
    // columns
    commonNameExtractor = LineExtractors.stringFromIndex(commonIndex);
    scientificExtractor = LineExtractors.stringFromIndex(sciIndex);
    subspeciesExtractor = (sspIndex == null) ? LineExtractors.<String> alwaysNull()
        : LineExtractors.stringFromIndex(sspIndex);
    taxonomyIdExtractor = LineExtractors.joined(Joiner.on('|').useForNull(""),
        ImmutableList.of(commonNameExtractor, scientificExtractor, subspeciesExtractor));
    
    territoryExtractor = (territoryIndex == null)
        ?  LineExtractors.<String> alwaysNull()
        : LineExtractors.stringFromIndex(territoryIndex);
    birdlistExtractor = (birdlistIndex == null)
        ?  LineExtractors.<String> alwaysNull()
        : LineExtractors.stringFromIndex(birdlistIndex);
    locationIdExtractor = LineExtractors.joined(Joiner.on('|').useForNull(""), territoryExtractor, birdlistExtractor);
    dateExtractor = LineExtractors.stringFromIndex(dateIndex);
    // Don't know whether the format is "dd/MM/yy" or "MM/dd/yy" yet.
    mappers.add(new DelayedDateMapper());
    latitudeExtractor = (latitudeIndex == null)
        ?  LineExtractors.<String> alwaysNull()
        : LineExtractors.stringFromIndex(latitudeIndex);
    longitudeExtractor = (longitudeIndex == null)
        ?  LineExtractors.<String> alwaysNull()
        : LineExtractors.stringFromIndex(longitudeIndex);
    
    if (notesIndex != null) {
      mappers.add(new DescriptionFieldMapper<String[]>(LineExtractors.stringFromIndex(notesIndex)));
    }
    
    if (heardOnlyIndex != null) {
      mappers.add(new HeardOnlyFieldMapper<String[]>(LineExtractors.booleanFromIndex(heardOnlyIndex)));
    }

    if (photoIndex != null || videoIndex != null) {
      LineExtractor<Boolean> photoExtractor = (photoIndex == null)
          ?  LineExtractors.constant(false)
          : LineExtractors.booleanFromIndex(photoIndex);
      LineExtractor<Boolean> videoExtractor = (videoIndex == null)
          ?  LineExtractors.constant(false)
          : LineExtractors.booleanFromIndex(videoIndex);
      mappers.add(new FieldMapper<String[]>() {
        @Override
        public void map(String[] line, Builder sighting) {
          boolean photo = photoExtractor.extract(line);
          boolean video = videoExtractor.extract(line);
          if (photo || video) {
            sighting.getSightingInfo().setPhotographed(true);
          }
        }
      });
    }

    if (escapedIndex != null || captiveIndex != null) {
      LineExtractor<Boolean> escapedExtractor = (escapedIndex == null)
          ?  LineExtractors.constant(false)
          : LineExtractors.booleanFromIndex(escapedIndex);
      LineExtractor<Boolean> captiveExtractor = (captiveIndex == null)
          ?  LineExtractors.constant(false)
          : LineExtractors.booleanFromIndex(captiveIndex);
      mappers.add(new FieldMapper<>() {
        @Override
        public void map(String[] line, Builder sighting) {
          boolean escaped = escapedExtractor.extract(line);
          boolean captive = captiveExtractor.extract(line);
          if (escaped) {
            sighting.getSightingInfo().setSightingStatus(SightingStatus.INTRODUCED_NOT_ESTABLISHED);
          } else if (captive) {
            // Not a supported setting; domestic is a close as Scythebill has for now.
            sighting.getSightingInfo().setSightingStatus(SightingStatus.DOMESTIC);
          }
        }
      });
    }

    
    if (countIndex != null) {
      mappers.add(new CountFieldMapper<>(LineExtractors.stringFromIndex(countIndex)));
    }
    
    return new ComputedMappings<>(
        new TaxonFieldMapper(taxonomyIdExtractor),
        new LocationMapper(locationIdExtractor),
        mappers);
  }

  @Override
  protected void computeExtendedMappings() throws IOException {
    if (dateMapper != null) {
      return;
    }
    
    LinkedHashMap<String, DateTimeFormatter> patternToFormatter = new LinkedHashMap<>();
    for (String pattern : ImmutableList.of("dd/MM/yy",  "MM/dd/yy")) {
      patternToFormatter.put(pattern, DateTimeFormat.forPattern(pattern)
        .withChronology(GJChronology.getInstance())
        .withPivotYear(2000));
    }
    
    try (ImportLines lines = importLines(sightingsFile)) {
      // See if any of the dates are of an unknown form
      Multiset<String> successfulPatterns = HashMultiset.create();
      while (true) {
        String[] nextLine = lines.nextLine();
        if (nextLine == null) {
          break;
        }
        
        String date = dateExtractor.extract(nextLine);
        if (!Strings.isNullOrEmpty(date)) {
          for (Map.Entry<String, DateTimeFormatter> entry : patternToFormatter.entrySet()) {
            try {
              entry.getValue().parseLocalDate(date);
              successfulPatterns.add(entry.getKey());
            } catch (IllegalArgumentException e) {
              // ignore
            }
          }
        }
      }
      
      String bestPattern = Multisets.copyHighestCountFirst(successfulPatterns).iterator().next();
      dateMapper = new DateFromStringFieldMapper<>(bestPattern, dateExtractor);
    }
  }
  
  private static final Pattern PAREN_WITH_DATE_PATTERN =
      Pattern.compile("^(.+) \\(\\d\\d \\w\\w\\w \\d\\d\\d\\d\\)$"); 

  private static final Pattern PAREN_WITH_DATE_AND_TIME_PATTERN =
      Pattern.compile("^(.+) \\(\\d\\d \\w\\w\\w \\d\\d\\d\\d \\d\\d:\\d\\d\\)$"); 

  /**
   * For at least one user, the birdlist IDs had an appended date and time.  Remove these.
   * This code merely removes date-time *shaped* things, not actual dates and times.
   */
  static String maybeRemoveDateOrDateAndTime(String birdlistId) {
    if (Strings.isNullOrEmpty(birdlistId)) {
      return birdlistId;
    }
    
    Matcher dateMatcher = PAREN_WITH_DATE_PATTERN.matcher(birdlistId);
    if (dateMatcher.matches()) {
      return dateMatcher.group(1);
    }
    
    Matcher dateAndTimeMatcher = PAREN_WITH_DATE_AND_TIME_PATTERN.matcher(birdlistId);
    if (dateAndTimeMatcher.matches()) {
      return dateAndTimeMatcher.group(1);
    }
    return birdlistId;
  }

  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    try (ImportLines lines = importLines(locationsFile)) {
      computeMappings(lines);
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }

        if (skipLine(line)) {
          continue;
        }
        
        String id = locationIdExtractor.extract(line);
        if (locationIds.hasBeenParsed(id)) {
          continue;
        }

        String territory = territoryExtractor.extract(line);
        String birdlist = maybeRemoveDateOrDateAndTime(birdlistExtractor.extract(line));
        
        if (territory == null) {
          if (Strings.isNullOrEmpty(birdlist)) {
            String fileName = locationsFile.getName();
            int period = fileName.lastIndexOf('.');
            if (period >= 0) {
              fileName = fileName.substring(0, period);
            }
            if (fileName.endsWith(" HBW")) {
              fileName = fileName.substring(0, fileName.length() - 4);
            }
            birdlist = fileName;
          }
          ImportedLocation importedLocation = new ImportedLocation();
          importedLocation.locationNames.add(birdlist);
          importedLocation.latitude = latitudeExtractor.extract(line);
          importedLocation.longitude = longitudeExtractor.extract(line);
          String locationId = importedLocation.tryAddToLocationSetWithLatLong(reportSet, locations, locationShortcuts, predefinedLocations);
          if (locationId != null) {
            locationIds.put(id, locationId);
          } else {
            locationIds.addToBeResolvedLocationName(id,
                new ToBeDecided(birdlist, importedLocation.getLatLong()));
          }         
        } else {
          ImportedLocation imported = importedLocation(locationShortcuts, territory);
          if (imported == null) {
            Collection<Location> anyLocations = locationShortcuts.getAnyLocationFromName(territory);
            if (!anyLocations.isEmpty()) {
              // Territory is literally anything?
              Location parentLocation = anyLocations.iterator().next();
              // Normalize, just because we might have added the parent location on a previous iteration,
              // and LocationShortcuts hasn't been updated
              parentLocation = Locations.normalizeLocation(parentLocation, locations);
              locations.ensureAdded(parentLocation);
              
              Location location = parentLocation;
              if (!Strings.isNullOrEmpty(birdlist)) {
                // Divide up locations on there's a "--"
                for (String sublocation : Splitter.on("--").split(birdlist)) {
                  // Skip prefixes that match the territory
                  if (sublocation.equals(territory)) {
                    continue;
                  }
                  
                  Location childLocation = parentLocation.getContent(sublocation);
                  if (childLocation == null) {
                    childLocation = Location.builder().setParent(parentLocation).setName(sublocation).build();
                    locations.ensureAdded(childLocation);
                  }
                  location = childLocation;
                }
              }
              locationIds.put(id, location.getId());
              continue;
            } else {
              // Give up - just call the territory a country, and move on
              imported = new ImportedLocation();
              imported.country = territory;
            }
          }
  
          if (!Strings.isNullOrEmpty(birdlist)) {
            for (String sublocation : Splitter.on("--").split(birdlist)) {
              // Skip prefixes that match the territory
              if (sublocation.equals(territory)) {
                continue;
              }
              imported.locationNames.add(sublocation);
            }
          }
  
          imported.longitude = Strings.emptyToNull(longitudeExtractor.extract(line));
          imported.latitude = Strings.emptyToNull(latitudeExtractor.extract(line));
          
          String locationId = imported.addToLocationSet(reportSet, locations, locationShortcuts,
              predefinedLocations);
          locationIds.put(id, locationId);
        }
      }
    }
  }

  @Override
  protected ImportLines importLines(File file) throws IOException {
    if (file.getName().endsWith(".xlsx")) {
      return HbwImportLines.importHbwXlsx(Files.asByteSource(file));
    }
    return HbwImportLines.importHbwXls(Files.asByteSource(file));
  }
  
  private static final ImmutableMap<String, String> NAME_TO_COUNTRY_AND_STATE_CODE =
      ImmutableMap.<String, String>builder()
      .put("Andaman and Nicobar", "IN-AN")
      .put("Ascension Island", "SH-AC")
      .put("Bali", "ID-NU-Bali")
      .put("Bioko", "GQ-I")
      .put("Bismarck Archipelago", "PG--Bismarck Archipelago")
      // Sabah - this is, clearly, not necessarily correct
      .put("Borneo", "MY-12")
      .put("Chagos Archipelago (British Indian Ocean Territory)", "IO--Chagos Archipelago")
      .put("China (Northern)", "CN")
      .put("China (Southern)", "CN")
      .put("Comoro Islands", "KM")
      .put("Congo Republic", "CG")
      .put("Crozet Islands", "TF--Crozet Islands")
      .put("Falkland (Malvinas) Islands", "FK")
      .put("Federated States Micronesia", "FM")
      .put("Galapagos Islands", "EC-W")
      .put("Georgia (USA)", "US-GA")
      .put("Hawaiian Islands", "US-HI")
      .put("Java", "ID-JW")
      .put("Johnston Island", "UM-67")
      .put("Kalimantan", "ID-KM")
      .put("Kerguelen Islands", "TF--Kerguelen Islands")
      .put("Lesser Sundas", "ID-NU")
      .put("Madeira Islands", "PT-30")
      .put("Maldive Islands", "MV")
      .put("Maluku Islands", "ID-MA")
      .put("Mexico (Northern)", "MX")
      .put("Mexico (Southern)", "MX")
      .put("Netherlands Antilles", "BQ")
      .put("New Guinea", "PG")
      .put("North Solomons", "PG-NSA")
      .put("Papua New Guinea (mainland)", "PG")
      .put("Peninsular Malaysia", "MY")
      .put("Prince Edward Islands", "CA-PE")
      .put("Republic of Macedonia", "MK")
      .put("Rodrigues", "MU-RO")
      .put("Sabah", "MY-12")
      .put("Saint Helena Island", "SH-SH")
      .put("Saint Pierre and Miquelon", "PM")
      .put("Sarawak", "MY-13")
      .put("Solomon Archipelago", "SB") // could also be Bougainville
      .put("Solomon Islands (country)", "SB")
      .put("South Georgia", "GS--South Georgia")
      .put("South Sandwich Islands", "GS--South Sandwich Islands")
      .put("St. Paul and Amsterdam Islands", "TF--St. Paul and Amsterdam Islands")
      .put("Sulawesi", "ID-SL")
      .put("Sumatra", "ID-SM")
      .put("Svalbard Islands and Jan Mayen", "SJ")
      .put("Timor-Leste", "TL")
      .put("Tristan da Cunha Archipelago", "SH-TA")
      .put("United States Virgin Islands", "VI")
      .put("USA (United States)", "US")
      .put("Virgin Islands", "VG")
      .put("Wake Island", "UM-79")
      .put("Washington DC", "US-DC")
      .put("Washington State", "US-WA")
      .put("West Papua (Indonesia)", "ID-IJ")
      .put("West Timor", "ID-MA-West Timor")
      .put("Yukon", "CA-YT")
      .put("Zaire (Democratic Republic of the Congo)", "CD")
      .build();

  static ImportedLocation importedLocation(LocationShortcuts locationShortcuts, String territory) {
    if (territory == null) {
      return null;
    }
    
    if (locationShortcuts.getCountryInexactMatch(territory, null, null) != null) {
      // Looks like a country
      ImportedLocation imported = new ImportedLocation();
      imported.country = territory;
      return imported;
    } else if (locationShortcuts.getStateByName("US", territory) != null) {
      // Looks like a US state
      ImportedLocation imported = new ImportedLocation();
      imported.countryCode = "US";
      return imported;
    } else if (locationShortcuts.getStateByName("CA", territory) != null) {
      // Looks like a CA province
      ImportedLocation imported = new ImportedLocation();
      imported.countryCode = "CA";
      return imported;
    } else if ("Russia (Eastern)".equals(territory)) {
      ImportedLocation imported = new ImportedLocation();
      imported.region = "Asia";
      imported.countryCode = "RU";
      return imported;
    } else if ("Russia (Western)".equals(territory)) {
      ImportedLocation imported = new ImportedLocation();
      imported.region = "Europe";
      imported.countryCode = "RU";
      return imported;
    } else if (NAME_TO_COUNTRY_AND_STATE_CODE.containsKey(territory)) {
      List<String> countryAndStateCode = Splitter.on('-').splitToList(NAME_TO_COUNTRY_AND_STATE_CODE.get(territory));
      ImportedLocation imported = new ImportedLocation();
      imported.countryCode = countryAndStateCode.get(0);
      if (countryAndStateCode.size() > 1) {
        imported.stateCode = Strings.emptyToNull(countryAndStateCode.get(1));
      }
      if (countryAndStateCode.size() > 2) {
        imported.locationNames.add(countryAndStateCode.get(2));
      }
      return imported;
    }
    
    return null;
  }
}
