/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import com.scythebill.birdlist.model.sighting.Sighting.Builder;

public class HeardOnlyFieldMapper<T> implements FieldMapper<T> {

  private RowExtractor<T, Boolean> extractor;

  public HeardOnlyFieldMapper(RowExtractor<T, Boolean> extractor) {
    this.extractor = extractor;
  }

  @Override
  public void map(T line, Builder sighting) {
    if (extractor.extract(line)) {
      sighting.getSightingInfo().setHeardOnly(true);
    }
  }
}
