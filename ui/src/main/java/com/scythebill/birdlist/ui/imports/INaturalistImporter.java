/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Photo;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting.Builder;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Imports sightings from iNaturalist CSV files.
 */
public class INaturalistImporter extends CsvSightingsImporter {

  private final static Logger logger = Logger.getLogger(INaturalistImporter.class.getName());
  private LineExtractor<String> locationIdExtractor;
  private LineExtractor<String> taxonomyIdExtractor;
  private LineExtractor<String> commonNameExtractor;
  private LineExtractor<String> scientificExtractor;
  private LineExtractor<String> subspeciesExtractor;
  private LineExtractor<String> countryExtractor;
  private LineExtractor<String> stateExtractor;
  private LineExtractor<String> countyExtractor;
  private LineExtractor<String> locationExtractor;
  private LineExtractor<String> latitudeExtractor;
  private LineExtractor<String> longitudeExtractor;
  private LineExtractor<String> classExtractor;
  private String chosenClass = "Mammalia";

  public INaturalistImporter(ReportSet reportSet, Taxonomy taxonomy,
      Checklists checklists, PredefinedLocations predefinedLocations, File file) {
    super(reportSet, taxonomy, checklists, predefinedLocations, file, file);
  }

  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return taxonomyIdExtractor;
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    return new FieldTaxonImporter<>(taxonomy, commonNameExtractor, scientificExtractor,
        subspeciesExtractor);
  }

  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    ImportLines lines = CsvImportLines.fromFile(locationsFile, getCharset());
    try {
      computeMappings(lines);
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }

        try {
          String id = locationIdExtractor.extract(line);
          if (locationIds.hasBeenParsed(id)) {
            continue;
          }
          ImportedLocation imported = new ImportedLocation();
          imported.state = stateExtractor.extract(line);
          imported.county = countyExtractor.extract(line);
          imported.country = countryExtractor.extract(line);
          String location = locationExtractor.extract(line);
          if (!Strings.isNullOrEmpty(location)) {
            int commaIndex = location.indexOf(',');
            if (commaIndex > 0) {
              location = location.substring(0, commaIndex);
            }
            imported.locationNames.add(location);
          }
          
          imported.latitude = Strings.emptyToNull(latitudeExtractor.extract(line));
          imported.longitude = Strings.emptyToNull(longitudeExtractor.extract(line));

          String locationId = imported.addToLocationSet(reportSet, locations, locationShortcuts,
              predefinedLocations);
          locationIds.put(id, locationId);
        } catch (Exception e) {
          logger.log(Level.WARNING, "Failed to import location " + Joiner.on(',').join(line), e);
        }
      }
    } catch (IOException | RuntimeException e) {
      throw new ImportException(
          Messages.getFormattedMessage(Name.FAILED_IMPORT_ON_LINE_FORMAT, lines.lineNumber()),
          e);
    } finally {
      lines.close();
    }
  }

  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    String[] header = lines.nextLine();
    Map<String, Integer> headersByIndex = new LinkedHashMap<>();
    for (int i = 0; i < header.length; i++) {
      headersByIndex.put(CharMatcher.whitespace().removeFrom(header[i]).toLowerCase(), i);
    }

    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
    Integer commonIndex = headersByIndex.get("common_name");
    Integer sciIndex = headersByIndex.get("taxon_species_name");
    Integer sspIndex = headersByIndex.get("taxon_subspecies_name");
    int dateIndex = getRequiredHeader(headersByIndex, "observed_on");
    Integer photosIndex = headersByIndex.get("image_url");
    Integer descriptionIndex = headersByIndex.get("description");
    int countryIndex = getRequiredHeader(headersByIndex, "place_country_name");
    Integer stateIndex = headersByIndex.get("place_state_name");
    Integer placeAdmin1Index = headersByIndex.get("place_admin1_name");
    Integer countyIndex = headersByIndex.get("place_county_name");
    Integer placeAdmin2Index = headersByIndex.get("place_admin2_name");
    Integer latitudeIndex = headersByIndex.get("latitude");
    Integer privateLatitudeIndex = headersByIndex.get("private_latitude");
    Integer longitudeIndex = headersByIndex.get("longitude");
    Integer privateLongitudeIndex = headersByIndex.get("private_longitude");
    Integer captiveCultivatedIndex = headersByIndex.get("captive_cultivated");
    int classIndex = getRequiredHeader(headersByIndex, "taxon_class_name");
    Integer placeGuessIndex = headersByIndex.get("place_guess");
    Integer privatePlaceGuessIndex = headersByIndex.get("private_place_guess");

    if (commonIndex == null && sciIndex == null) {
      throw new ImportException(Messages.getMessage(Name.SCYTHEBILL_IMPORT_NEITHER_SCIENTIFIC_NOR_COMMON));
    }
    // Create a taxonomy extractor from the located common/scientific/ssp.
    // columns
    commonNameExtractor = commonIndex == null ? LineExtractors.<String> alwaysNull()
        : LineExtractors.stringFromIndex(commonIndex);
    scientificExtractor = sciIndex == null ? LineExtractors.<String> alwaysNull() : LineExtractors
        .stringFromIndex(sciIndex);
    LineExtractor<String> rawSspExtractor = (sspIndex == null) ? LineExtractors.<String> alwaysNull()
        : LineExtractors.stringFromIndex(sspIndex);
    // iNaturalist currently returns the subspecies as a full trinomial;  extract just the subspecies 
    subspeciesExtractor = line -> {
      String s = rawSspExtractor.extract(line);
      if (!Strings.isNullOrEmpty(s)) {
        int lastSpace = s.lastIndexOf(' ');
        if (lastSpace > 0) {
          return s.substring(lastSpace + 1);
        } else {
          return s;
        }
      }
      
      return null;
    };
    taxonomyIdExtractor = LineExtractors.joined(Joiner.on('|').useForNull(""),
        ImmutableList.of(commonNameExtractor, scientificExtractor, subspeciesExtractor));
    classExtractor = LineExtractors.stringFromIndex(classIndex);
    // Extract the specifically named location columns
    countryExtractor = LineExtractors.stringFromIndex(countryIndex);
    stateExtractor = 
        LineExtractors.firstNonEmpty(
            stateIndex == null ? LineExtractors.<String>alwaysNull()
                : LineExtractors.stringFromIndex(stateIndex),
            placeAdmin1Index == null ? LineExtractors.<String>alwaysNull()
                : LineExtractors.stringFromIndex(placeAdmin1Index));
            
    countyExtractor = 
        LineExtractors.firstNonEmpty(
            countyIndex == null ? LineExtractors.<String>alwaysNull()
                : LineExtractors.stringFromIndex(countyIndex),
            placeAdmin2Index == null ? LineExtractors.<String>alwaysNull()
                : LineExtractors.stringFromIndex(placeAdmin2Index));
    locationExtractor = 
        LineExtractors.firstNonEmpty(
            privatePlaceGuessIndex == null ? LineExtractors.<String>alwaysNull()
                : LineExtractors.stringFromIndex(privatePlaceGuessIndex),
            placeGuessIndex == null ? LineExtractors.<String>alwaysNull()
                : LineExtractors.stringFromIndex(placeGuessIndex));
 
    List<RowExtractor<String[], String>> allLocationExtractors = Lists.newArrayList();
    allLocationExtractors.add(countryExtractor);
    allLocationExtractors.add(stateExtractor);
    allLocationExtractors.add(countyExtractor);
    allLocationExtractors.add(locationExtractor);

    locationIdExtractor = LineExtractors.joined(Joiner.on('|').useForNull(""), allLocationExtractors);
    
    latitudeExtractor = LineExtractors.firstNonEmpty(
        privateLatitudeIndex == null ? LineExtractors.<String>alwaysNull()
            : LineExtractors.stringFromIndex(privateLatitudeIndex),
        latitudeIndex == null ? LineExtractors.<String>alwaysNull()
            : LineExtractors.stringFromIndex(latitudeIndex));
    longitudeExtractor = LineExtractors.firstNonEmpty(
        privateLongitudeIndex == null ? LineExtractors.<String>alwaysNull()
            : LineExtractors.stringFromIndex(privateLongitudeIndex),
        longitudeIndex == null ? LineExtractors.<String>alwaysNull()
            : LineExtractors.stringFromIndex(longitudeIndex));
    if (descriptionIndex != null) {
      mappers.add(new DescriptionFieldMapper<>(LineExtractors.stringFromIndex(descriptionIndex)));
    }
    if (photosIndex != null) {
      mappers.add(new PhotosMapper(
          LineExtractors.stringFromIndex(photosIndex)));
    }
    
    if (captiveCultivatedIndex != null) {
      LineExtractor<String> captiveCultivatedExtractor = LineExtractors.stringFromIndex(captiveCultivatedIndex);
      mappers.add(new FieldMapper<>() {
        @Override
        public void map(String[] line, Builder sighting) {
          String captiveCultivated = captiveCultivatedExtractor.extract(line);
          if (captiveCultivated != null && "true".equalsIgnoreCase(captiveCultivated)) {
            sighting.getSightingInfo().setSightingStatus(SightingStatus.DOMESTIC);
          }
        }
      });
    }

    mappers.add(new DateFromStringFieldMapper<>("yy-MM-dd", LineExtractors.stringFromIndex(dateIndex)));
    return new ComputedMappings<>(
        new TaxonFieldMapper(taxonomyIdExtractor),
        new LocationMapper(locationIdExtractor),
        mappers);
  }
  

  /** Extract photos. */
  static class PhotosMapper implements FieldMapper<String[]> {
    private final LineExtractor<String> extractor;

    PhotosMapper(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public void map(String[] line, Builder sighting) {
      String photoString = Strings.emptyToNull(extractor.extract(line));
      if (photoString != null) {
        try {
          URI uri = new URI(photoString);
          sighting.getSightingInfo().setPhotos(ImmutableList.of(new Photo(uri)));
          sighting.getSightingInfo().setPhotographed(true);
        } catch (URISyntaxException e) {
        }
      }
    }
  }

  @Override
  protected String[] getHeaderRow(ImportLines lines) throws IOException {
    return lines.nextLine();
  }

  @Override
  protected boolean skipLine(String[] line) {
    if (!chosenClass.equals(classExtractor.extract(line))) {
      return true;
    }
    
    // No point importing anything that doesn't have a scientific name (these
    // correspond to sightings not ID'd to a species)
    if (Strings.isNullOrEmpty(scientificExtractor.extract(line))) {
      return true;
    }
    
    return false;
  }

  public Set<String> findAllClasses(File sightingsFile) {
    // If importing to the bird taxonomy, stop now
    if (getTaxonomy().isBuiltIn()) {
      return ImmutableSet.of("Aves");
    }
    try {
      ImportLines lines = CsvImportLines.fromFile(sightingsFile, getCharset());
      computeMappings(lines);
      Set<String> classes = new TreeSet<>();
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          if (classes.contains("Aves")
              && classes.size() > 1) {
            classes.remove("Aves");
          }
          return classes;
        }
        
        String className = classExtractor.extract(line);
        if (!Strings.isNullOrEmpty(className)) {
          classes.add(className);
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public void chooseClass(String className) {
    chosenClass = className;
  }  
}
