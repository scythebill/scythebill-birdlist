/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.util.function.Consumer;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.LayoutStyle.ComponentPlacement;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.edits.ChosenUsers;
import com.scythebill.birdlist.model.user.User;
import com.scythebill.birdlist.model.user.UserSet;
import com.scythebill.birdlist.ui.actions.ReturnAction;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.UserChipsUi;
import com.scythebill.birdlist.ui.panels.UserDialog;
import com.scythebill.birdlist.ui.prefs.ReportSetPreference;

/**
 * Handles choosing users when importing.
 */
class ImportChooseUsersPanel extends JPanel implements FontsUpdatedListener {
  
  private JButton cancel;
  private JButton ok;
  private JButton newUser;
  private JLabel topLabel;
  private UserSet userSet;
  private UserChipsUi userChipsUi;
  private JSeparator separator;
  private Consumer<ImmutableSet<User>> resultsConsumer;
  private final ReportSetPreference<ChosenUsers> chosenUsersPreference;
  private final UserDialog userDialog;

  
  @Inject
  public ImportChooseUsersPanel(
      ReturnAction returnAction,
      ReportSet reportSet,
      ReportSetPreference<ChosenUsers> chosenUsersPreference,
      UserDialog userDialog,
      FontManager fontManager) {

    this.userDialog = userDialog;
    this.userSet = Preconditions.checkNotNull(reportSet.getUserSet());
    this.chosenUsersPreference = chosenUsersPreference;
    initGUI(fontManager);
    fontManager.applyTo(this);
    newUser.addActionListener(e -> newUser());
    cancel.addActionListener(returnAction);
    ok.addActionListener(e -> {
      ImmutableSet<User> users = ImmutableSet.copyOf(userChipsUi.userChips.getChips());
      chosenUsersPreference.get().setUsers(users);
      // Don't bother marking dirty;  if the import succeeds, it'll get saved, if not, no point
      // marking dirty.
      chosenUsersPreference.save(false /* markDirty */);
      resultsConsumer.accept(users);
    });
  }
  
  public void chooseUsers(
      Consumer<ImmutableSet<User>> results) {
    this.resultsConsumer = results;
  }

  private void newUser() {
    userDialog.showUserDialog(this, Messages.getMessage(Name.NEW_USER), userSet, null, userBuilder -> {
      User user = userSet.addUser(userBuilder);
      this.userChipsUi.userChips.addChip(user);
    });
  }
  
  private void initGUI(FontManager fontManager) {
    
    topLabel = new JLabel("<html>" + Messages.getMessage(Name.NEW_USER_IMPORT_HELP_MESSAGE));
    
    topLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    
    userChipsUi = new UserChipsUi(userSet, fontManager);
    userChipsUi.userChips.addAllChips(chosenUsersPreference.get().getUsers(userSet));
    
    separator = new JSeparator();
    
    cancel = new JButton();
    cancel.setText(Messages.getMessage(Name.CANCEL_IMPORT));

    ok = new JButton();
    ok.setText(Messages.getMessage(Name.OK_BUTTON));
    
    newUser = new JButton();
    newUser.setText(Messages.getMessage(Name.NEW_OBSERVER));
  }
  
  @Override
  public void fontsUpdated(FontManager fontManager) {
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setAutoCreateContainerGaps(true);
    layout.setHorizontalGroup(layout.createParallelGroup()
        .addComponent(topLabel)
        .addGroup(layout.createSequentialGroup()
            .addComponent(userChipsUi.userLabel)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(userChipsUi.userIndexer)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(userChipsUi.addUserButton))
        .addComponent(userChipsUi.userChipsScrollPane, fontManager.scale(400), fontManager.scale(400), fontManager.scale(400))
        .addComponent(newUser, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
        .addComponent(separator)
        .addGroup(Alignment.LEADING, layout.createSequentialGroup()
            .addComponent(cancel)
            .addGap(0, fontManager.scale(75), Short.MAX_VALUE)
            .addComponent(ok)));
    layout.setVerticalGroup(layout.createSequentialGroup()
        .addComponent(topLabel)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(userChipsUi.userLabel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addComponent(userChipsUi.userIndexer, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addComponent(userChipsUi.addUserButton, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(userChipsUi.userChipsScrollPane, fontManager.scale(80), PREFERRED_SIZE, Short.MAX_VALUE)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(newUser)
        .addGap(18)
        .addComponent(separator)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(cancel)
            .addComponent(ok)));
        
    layout.linkSize(cancel, ok, newUser);
  }
}
