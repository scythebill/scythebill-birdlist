/*   
 * Copyright 2020 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.awt.Dimension;
import java.io.InterruptedIOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Pattern;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.google.common.base.CharMatcher;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.ui.actions.ReturnAction;
import com.scythebill.birdlist.ui.components.ErrorLabel;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.components.NewLocationDialog;
import com.scythebill.birdlist.ui.components.PossiblyRequiredLabel;
import com.scythebill.birdlist.ui.components.ProgressSpinner;
import com.scythebill.birdlist.ui.components.WherePanel;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.imports.flickr.FlickrApiException;
import com.scythebill.birdlist.ui.imports.flickr.FlickrImporterFactory;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.OpenMapUrl;

/**
 * Supports setting up a Flickr import by choosing a Flickr album and location.
 */
class ImportFlickrPanel extends JPanel implements FontsUpdatedListener {

  private final FlickrImporterFactory flickrImporterFactory;
  private final ReportSet reportSet;
  private final NewLocationDialog newLocationDialog;
  private final PredefinedLocations predefinedLocations;
  private final ListeningExecutorService executorService;
  private final Alerts alerts;
  private JLabel topLabel;
  private JButton cancel;
  private JButton startImport;
  private JSeparator separator;
  private PossiblyRequiredLabel albumLabel;
  private JTextField albumField;
  private WherePanel wherePanel;
  private ImportReady importReadyCallback;
  private ProgressSpinner progressSpinner;
  private ErrorLabel urlErrorLabel;
  private final OpenMapUrl openMapUrl;
  
  private static final Pattern FLICKR_ALBUM_OR_PHOTOS_PATTERN = Pattern.compile(
      "https://www.flickr.com/photos/.*/[0-9]+");

  public interface ImportReady {
    void importReady(SightingsImporter<?> flickrImporter);  
  }
  
  @Inject
  public ImportFlickrPanel(
      FlickrImporterFactory flickrImporterFactory,
      ReturnAction returnAction,
      ReportSet reportSet,
      NewLocationDialog newLocationDialog,
      PredefinedLocations predefinedLocations,
      OpenMapUrl openMapUrl,
      Alerts alerts,
      ListeningExecutorService executorService,
      FontManager fontManager) {
    this.flickrImporterFactory = flickrImporterFactory;
    this.reportSet = reportSet;
    this.newLocationDialog = newLocationDialog;
    this.predefinedLocations = predefinedLocations;
    this.openMapUrl = openMapUrl;
    this.alerts = alerts;
    this.executorService = executorService;
    initGUI(fontManager);
    fontManager.applyTo(this);
    albumField.getDocument().addDocumentListener(new DocumentListener() {
      @Override
      public void removeUpdate(DocumentEvent e) {
        validateFields();
      }
      
      @Override
      public void insertUpdate(DocumentEvent e) {
        validateFields();
      }
      
      @Override
      public void changedUpdate(DocumentEvent e) {
        validateFields();
      }
    });
    cancel.addActionListener(returnAction);
    validateFields();
  }

  private void validateFields() {
    // TODO: block starting when the location is "red"
    urlErrorLabel.setText("");    
    String albumUrl = CharMatcher.whitespace().trimFrom(albumField.getText());
    if (albumUrl.isEmpty()) {
      startImport.setEnabled(false);
    } else {
      if (!FLICKR_ALBUM_OR_PHOTOS_PATTERN.matcher(albumUrl).matches()) {
        startImport.setEnabled(false);
        urlErrorLabel.setText(Messages.getMessage(Name.NOT_A_VALID_FLICKR_ALBUM_URL));
      } else {
        try {
          new URI(albumUrl);
          startImport.setEnabled(true);
        } catch (URISyntaxException e) {
          startImport.setEnabled(false);
          urlErrorLabel.setText(Messages.getMessage(Name.NOT_A_VALID_FLICKR_ALBUM_URL));
        }
      }
    }
  }
  
  private void initGUI(FontManager fontManager) {
    topLabel = new JLabel("<html>" + Messages.getMessage(Name.FLICKR_IMPORT_INSTRUCTIONS));
    topLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    
    albumLabel = new PossiblyRequiredLabel(
        Messages.getMessage(Name.FLICKR_ALBUM_URL_LABEL));
    albumLabel.setRequired(true);
    albumField = new JTextField(50);

    urlErrorLabel = new ErrorLabel();
    
    wherePanel = new WherePanel(reportSet, newLocationDialog, predefinedLocations, openMapUrl, "(None)");
    wherePanel.setWhereLayoutStrategy(IndexerPanel.LayoutStrategy.BELOW);

    cancel = new JButton();
    cancel.setText(Messages.getMessage(Name.CANCEL_IMPORT));

    separator = new JSeparator();
    
    progressSpinner = new ProgressSpinner();

    startImport = new JButton();
    startImport.setText(Messages.getMessage(Name.START_BUTTON));
    startImport.addActionListener(e -> tryStart());
  }

  public void start(ImportReady importReadyCallback) {
    this.importReadyCallback = importReadyCallback;
  }
  
  private void tryStart() {
    String albumUrl = CharMatcher.whitespace().trimFrom(albumField.getText());
    progressSpinner.start();
    ListenableFuture<SightingsImporter<?>> future =
        executorService.submit(() -> flickrImporterFactory.newFlickrImporter(albumUrl, wherePanel.getWhere()));
    Futures.addCallback(future,
        new FutureCallback<SightingsImporter<?>>() {
          @Override
          public void onFailure(Throwable t) {
            SwingUtilities.invokeLater(() -> {
              progressSpinner.stop();
              if (t instanceof FlickrApiException
                  && ((FlickrApiException) t).getCode() == 1) {
                alerts.showError(ImportFlickrPanel.this,
                    Name.FLICKR_ALBUM_LOAD_FAILED_TITLE,
                    Name.FLICKR_ALBUM_LOAD_FAILED_MESSAGE);
              } else if (t instanceof InterruptedIOException) {
                alerts.showError(ImportFlickrPanel.this,
                    Name.FLICKR_ALBUM_LOAD_FAILED_TITLE,
                    Name.FLICKR_ALBUM_LOAD_TIMEOUT_MESSAGE);
              } else {
                alerts.showError(ImportFlickrPanel.this, t,
                    Name.FLICKR_ALBUM_LOAD_FAILED_TITLE,
                    Name.FLICKR_ALBUM_LOAD_FAILED_UNEXPECTED_MESSAGE);
              }
            });
          }

          @Override
          public void onSuccess(SightingsImporter<?> importer) {
            SwingUtilities.invokeLater(() -> {
              progressSpinner.stop();
              importReadyCallback.importReady(importer);
            });
          }},
        executorService);
  }
  
  @Override
  public void fontsUpdated(FontManager fontManager) {
    setPreferredSize(fontManager.scale(new Dimension(900, 600)));
    int spinnerSize = fontManager.scale(30);
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setAutoCreateContainerGaps(true);
    layout.setHorizontalGroup(layout.createParallelGroup()
        .addComponent(topLabel)
        .addComponent(albumLabel)
        .addGroup(layout.createSequentialGroup()
            .addComponent(albumField, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            .addComponent(progressSpinner, spinnerSize, spinnerSize, spinnerSize))
        .addComponent(urlErrorLabel)
        .addComponent(wherePanel)
        .addComponent(separator)
        .addGroup(Alignment.LEADING, layout.createSequentialGroup()
            .addComponent(cancel)
            .addGap(0, fontManager.scale(75), Short.MAX_VALUE)
            .addComponent(startImport)));
    layout.setVerticalGroup(layout.createSequentialGroup()
        .addComponent(topLabel)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(albumLabel)
        .addPreferredGap(ComponentPlacement.RELATED)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(albumField, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            .addComponent(progressSpinner, spinnerSize, spinnerSize, spinnerSize))
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(urlErrorLabel)
        .addGap(fontManager.scale(30))
        .addComponent(wherePanel)
        .addGap(18, 18, Short.MAX_VALUE)
        .addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(cancel)
            .addComponent(startImport)));
        
    layout.linkSize(cancel, startImport);
  }
}
