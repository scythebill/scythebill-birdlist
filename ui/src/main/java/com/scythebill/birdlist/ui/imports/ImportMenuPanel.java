/*   
 *
 * Copyright 2013 Adam Winer
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.OptionalInt;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.checkerframework.checker.nullness.qual.Nullable;
import org.joda.time.DateTimeFieldType;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.ReadablePartial;
import org.joda.time.chrono.GJChronology;

import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.base.Strings;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Sets;
import com.google.common.collect.Streams;
import com.google.common.eventbus.Subscribe;
import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnel;
import com.google.common.hash.PrimitiveSink;
import com.google.common.html.HtmlEscapers;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningScheduledExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.checklist.TransposedChecklistSynthesizer;
import com.scythebill.birdlist.model.io.HtmlResponseWriter;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.query.SightingComparators;
import com.scythebill.birdlist.model.query.SightingPredicates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.sighting.edits.RecentEdits;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.names.LocalNames;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences.AvailableLocale;
import com.scythebill.birdlist.model.user.User;
import com.scythebill.birdlist.model.util.ResolvedComparator;
import com.scythebill.birdlist.ui.actions.ReturnAction;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.app.Titled;
import com.scythebill.birdlist.ui.components.OkCancelPanel;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.events.TaxonomyChangedEvent;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.guice.Clements;
import com.scythebill.birdlist.ui.guice.InitialRun;
import com.scythebill.birdlist.ui.imports.ImportPreferences.ImportType;
import com.scythebill.birdlist.ui.imports.TaxonImporter.ToBeDecided;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.panels.ShrinkToFit;
import com.scythebill.birdlist.ui.panels.reports.QueryPreferences;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;
import com.scythebill.birdlist.ui.util.ScanSeenTaxa;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Panel running the main import flow.
 */
public class ImportMenuPanel extends JPanel implements FontsUpdatedListener, ShrinkToFit, Titled {
  private static final Logger logger =
          Logger.getLogger(ImportMenuPanel.class.getName());
  private JButton returnButton;
  private JButton importAvisys;
  private JButton importBirdbase;
  private JButton importBirdersDiary;
  private JButton importBirdBrain;
  private JButton importBirdTrack;
  private JButton importBirdLasser;
  private JButton importBirda;
  private JButton importEbird;
  private JButton importFlickr;
  private JButton importHbwAlive;
  private JButton importINaturalist;
  private JButton importScythebill;
  private JButton importObservationOrg;
  private JButton importOrnitho;
  private JButton importWildlifeRecorder;
  private JButton importWings;
  private final FontManager fontManager;
  private final ReportSet reportSet;
  private final FileDialogs fileDialogs;
  private final Taxonomy clements;
  private final Provider<ImportResolveTaxaPanel> resolveTaxaPanelProvider;
  private final Provider<ImportResolveLocationsPanel> resolveLocationsPanelProvider;
  private final Provider<ImportChooseUsersPanel> chooseUsersPanelProvider;
  private final Provider<AutoGeocodeLocations> autoGeocodeLocationsProvider;
  private final Provider<ImportFlickrPanel> importFlickrPanelProvider;
  private final NavigableFrame navigableFrame;
  private final Action importCompleteAction;
  private final ReturnAction returnAction;
  private final TaxonomyStore taxonomyStore;
  private final Alerts alerts;
  private final Checklists checklists;
  private final TransposedChecklistSynthesizer transposedChecklistsSynthesizer;
  private final boolean initialRun;
  private final PredefinedLocations predefinedLocations;
  private RecentEdits recentEdits;
  private JPanel centerBox;
  private JLabel importLabel;
  private JLabel avisysLabel;
  private JLabel ebirdLabel;
  private JLabel flickrLabel;
  private JLabel hbwAliveLabel;
  private JLabel iNaturalistLabel;
  private JLabel scythebillLabel;
  private JLabel observationOrgLabel;
  private JLabel emptyLabel;
  private JLabel birdbaseLabel;
  private JLabel birdBrainLabel;
  private JLabel birdTrackLabel;
  private JLabel birdersDiaryLabel;
  private JLabel birdLasserLabel;
  private JLabel birdaLabel;
  private JLabel ornithoLabel;
  private JLabel wildlifeRecorderLabel;
  private JLabel wingsLabel;
  private final ImportPreferences importPreferences;
  private JLabel importMenuLabel;
  private JComboBox<Object> importMenuComboBox;
  private final QueryPreferences queryPreferences;
  private final ListeningScheduledExecutorService executorService;
  private final NamesPreferences namesPreferences;
  private Set<String> locationsBeforeImport;
  private final EBird ebird;
  
  @Inject
  public ImportMenuPanel(
      @FinishedImport final Action importCompleteAction,
      ReturnAction returnAction,
      NavigableFrame navigableFrame,
      FontManager fontManager,
      ReportSet reportSet,
      @InitialRun boolean initialRun,
      FileDialogs fileDialogs,
      TaxonomyStore taxonomyStore,
      @Clements Taxonomy clements,
      Provider<ImportResolveTaxaPanel> resolveTaxaPanelProvider,
      Provider<ImportResolveLocationsPanel> resolveLocationsPanelProvider,
      Provider<ImportChooseUsersPanel> chooseUsersPanelProvider,
      Provider<AutoGeocodeLocations> autoGeocodeLocationsProvider,
      Provider<ImportFlickrPanel> importFlickrPanelProvider,
      EBird ebird,
      ImportPreferences importPreferences,
      Checklists checklists,
      TransposedChecklistSynthesizer transposedChecklistsSynthesizer,
      PredefinedLocations predefinedLocations,
      EventBusRegistrar eventBusRegistrar,
      QueryPreferences queryPreferences,
      NamesPreferences namesPreferences,
      ListeningScheduledExecutorService executorService,
      Alerts alerts) {
    this.returnAction = returnAction;
    this.fontManager = fontManager;
    this.reportSet = reportSet;
    this.initialRun = initialRun;
    this.fileDialogs = fileDialogs;
    this.taxonomyStore = taxonomyStore;
    this.clements = clements;
    this.navigableFrame = navigableFrame;
    this.importCompleteAction = importCompleteAction;
    this.resolveTaxaPanelProvider = resolveTaxaPanelProvider;
    this.resolveLocationsPanelProvider = resolveLocationsPanelProvider;
    this.chooseUsersPanelProvider = chooseUsersPanelProvider;
    this.autoGeocodeLocationsProvider = autoGeocodeLocationsProvider;
    this.importFlickrPanelProvider = importFlickrPanelProvider;
    this.ebird = ebird;
    this.importPreferences = importPreferences;
    this.checklists = checklists;
    this.transposedChecklistsSynthesizer = transposedChecklistsSynthesizer;
    this.predefinedLocations = predefinedLocations;
    this.queryPreferences = queryPreferences;
    this.namesPreferences = namesPreferences;
    this.executorService = executorService;
    this.alerts = alerts;
    initGUI();
    returnButton.setAction(returnAction);
    importAvisys.addActionListener(e -> doImportAvisys());
    importBirdbase.addActionListener(e -> doImportBirdbase());
    importBirdBrain.addActionListener(e -> doImportBirdBrain());
    importBirdTrack.addActionListener(e -> doImportBirdTrack());
    importBirdersDiary.addActionListener(e -> doImportBirdersDiary());
    importBirdLasser.addActionListener(e -> doImportBirdLasser());
    importBirda.addActionListener(e -> doImportBirda());
    importEbird.addActionListener(e -> doImportEBird());
    importFlickr.addActionListener(e -> doImportFlickr());
    importHbwAlive.addActionListener(e -> doImportHbwAlive());
    importINaturalist.addActionListener(e -> doImportINaturalist());
    importScythebill.addActionListener(e -> doImportScythebill());
    importObservationOrg.addActionListener(e -> doImportObservationOrg());
    importOrnitho.addActionListener(e -> doImportOrnitho());
    importWildlifeRecorder.addActionListener(e -> doImportWildlifeRecorder());
    importWings.addActionListener(e -> doImportWings());
    
    importMenuComboBox.addActionListener(e -> {
      Object o = importMenuComboBox.getSelectedItem();
      if (o instanceof ImportType) {
        doImport((ImportType) o);
        importMenuComboBox.setSelectedIndex(0);
      }
    });

    updateUiForTaxonomy();
    eventBusRegistrar.registerWhenInHierarchy(this);
  }

  @Inject(optional = true)
  void setRecentEdits(RecentEdits recentEdits) {
    // RecentEdits are not available for import-to-a-new-file.
    // So move this to an optional injection point
    this.recentEdits = recentEdits;
  }
  
  private void initGUI() {
    setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

    centerBox = new JPanel();
    add(centerBox);
    
    importLabel = new JLabel();
    // Label content is updated in updateUiForTaxonomy()
    importLabel.putClientProperty(FontManager.PLAIN_LABEL, true);

    importMenuComboBox = new JComboBox<Object>();
    
    importAvisys = new JButton();
    importAvisys.setText(Messages.getFormattedMessage(Name.IMPORT_FROM_FORMAT, "Avisys"));
    importAvisys.putClientProperty("Quaqua.Button.style", "bevel");
    
    importBirdbase = new JButton();
    importBirdbase.setText(Messages.getFormattedMessage(Name.IMPORT_FROM_FORMAT, "Birdbase"));
    importBirdbase.putClientProperty("Quaqua.Button.style", "bevel");
    
    importBirdBrain = new JButton();
    importBirdBrain.setText(Messages.getFormattedMessage(Name.IMPORT_FROM_FORMAT, "Bird Brain"));
    importBirdBrain.putClientProperty("Quaqua.Button.style", "bevel");
    
    importBirdTrack = new JButton();
    importBirdTrack.setText(Messages.getFormattedMessage(Name.IMPORT_FROM_FORMAT, "BirdTrack"));
    importBirdTrack.putClientProperty("Quaqua.Button.style", "bevel");
    
    importBirdersDiary = new JButton();
    importBirdersDiary.setText(Messages.getFormattedMessage(Name.IMPORT_FROM_FORMAT, "Birder's Diary"));
    importBirdersDiary.putClientProperty("Quaqua.Button.style", "bevel");
    
    importBirdLasser = new JButton();
    importBirdLasser.setText(Messages.getFormattedMessage(Name.IMPORT_FROM_FORMAT, "BirdLasser"));
    importBirdLasser.putClientProperty("Quaqua.Button.style", "bevel");
    
    importBirda = new JButton();
    importBirda.setText(Messages.getFormattedMessage(Name.IMPORT_FROM_FORMAT, "Birda"));
    importBirda.putClientProperty("Quaqua.Button.style", "bevel");
    
    importEbird = new JButton();
    importEbird.setText(Messages.getFormattedMessage(Name.IMPORT_FROM_FORMAT, "eBird"));
    importEbird.putClientProperty("Quaqua.Button.style", "bevel");
    
    importFlickr = new JButton();
    importFlickr.setText(Messages.getFormattedMessage(Name.IMPORT_FROM_FORMAT, "Flickr"));
    importFlickr.putClientProperty("Quaqua.Button.style", "bevel");
    
    importHbwAlive = new JButton();
    importHbwAlive.setText(Messages.getFormattedMessage(Name.IMPORT_FROM_FORMAT, "HBW Alive"));
    importHbwAlive.putClientProperty("Quaqua.Button.style", "bevel");
    
    importINaturalist = new JButton();
    importINaturalist.setText(Messages.getFormattedMessage(Name.IMPORT_FROM_FORMAT, "iNaturalist"));
    importINaturalist.putClientProperty("Quaqua.Button.style", "bevel");
    
    importScythebill = new JButton();
    importScythebill.setText(Messages.getFormattedMessage(Name.IMPORT_FROM_FORMAT, "Scythebill"));
    importScythebill.putClientProperty("Quaqua.Button.style", "bevel");

    importObservationOrg = new JButton();
    importObservationOrg.setText(Messages.getFormattedMessage(Name.IMPORT_FROM_FORMAT, "Observation.org"));
    importObservationOrg.putClientProperty("Quaqua.Button.style", "bevel");
    
    importOrnitho = new JButton();
    importOrnitho.setText(Messages.getFormattedMessage(Name.IMPORT_FROM_FORMAT, "Ornitho"));
    importOrnitho.putClientProperty("Quaqua.Button.style", "bevel");
    
    importWildlifeRecorder = new JButton();
    importWildlifeRecorder.setText(Messages.getFormattedMessage(Name.IMPORT_FROM_FORMAT, "Wildlife Recorder"));
    importWildlifeRecorder.putClientProperty("Quaqua.Button.style", "bevel");
    
    importWings = new JButton();
    importWings.setText(Messages.getFormattedMessage(Name.IMPORT_FROM_FORMAT, "Wings"));
    importWings.putClientProperty("Quaqua.Button.style", "bevel");
    
    returnButton = new JButton();
    returnButton.putClientProperty("Quaqua.Button.style", "bevel");

    avisysLabel = new JLabel(
        Messages.getFormattedMessage(Name.CSV_EXPORT_FROM_FORMAT, "Sighting Record Listing"));
    avisysLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    birdbaseLabel = new JLabel(
        Messages.getFormattedMessage(Name.CSV_EXPORT_FROM_FORMAT, "Birdbase"));
    birdbaseLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    birdTrackLabel = new JLabel(
        Messages.getFormattedMessage(Name.XLSX_EXPORT_FROM_FORMAT, "BirdTrack"));
    birdTrackLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    birdBrainLabel = new JLabel(
        Messages.getFormattedMessage(Name.CSV_EXPORT_FROM_FORMAT, "Bird Brain"));
    birdBrainLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    birdersDiaryLabel = new JLabel(
        Messages.getFormattedMessage(Name.CSV_EXPORT_FROM_FORMAT, "Birder's Diary"));
    birdersDiaryLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    birdLasserLabel = new JLabel(
        Messages.getFormattedMessage(Name.CSV_EXPORT_FROM_FORMAT, "BirdLasser"));
    birdLasserLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    birdaLabel = new JLabel(
        Messages.getFormattedMessage(Name.CSV_EXPORT_FROM_FORMAT, "Birda"));
    birdaLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    ebirdLabel = new JLabel(
        Messages.getFormattedMessage(Name.CSV_OR_ZIP_EXPORT_FROM_FORMAT, "eBird"));
    ebirdLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    flickrLabel = new JLabel(
        Messages.getFormattedMessage(Name.FLICKR_ALBUM, "Flickr"));
    flickrLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    hbwAliveLabel = new JLabel(
        Messages.getFormattedMessage(Name.XLS_EXPORT_FROM_FORMAT, "HBW Alive"));
    hbwAliveLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    iNaturalistLabel = new JLabel(
        Messages.getFormattedMessage(Name.CSV_EXPORT_FROM_FORMAT, "iNaturalist"));
    iNaturalistLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    scythebillLabel = new JLabel(
        Messages.getFormattedMessage(Name.CSV_EXPORT_FROM_FORMAT, "Scythebill"));
    scythebillLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    observationOrgLabel = new JLabel(
        Messages.getFormattedMessage(Name.CSV_EXPORT_FROM_FORMAT, "Observation.org/Waarnemingen"));
    observationOrgLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    ornithoLabel = new JLabel(
        Messages.getFormattedMessage(Name.TXT_EXPORT_FROM_FORMAT, "Ornitho"));
    ornithoLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    wildlifeRecorderLabel = new JLabel(
        Messages.getFormattedMessage(Name.CSV_EXPORT_FROM_FORMAT, "Wildlife Recorder"));
    wildlifeRecorderLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    wingsLabel = new JLabel(
        Messages.getFormattedMessage(Name.XML_EXPORTS_FROM_FORMAT, "Wings"));
    wingsLabel.putClientProperty(FontManager.PLAIN_LABEL, true);

    importMenuLabel = new JLabel(Messages.getMessage(Name.OTHER_KINDS_OF_EXPORTS));
    importMenuLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    
    emptyLabel = new JLabel();

    fontManager.applyTo(this);
  }

  private void doImport(ImportType importType) {
    switch (importType) {
      case AVISYS:
        doImportAvisys();
        break;
      case BIRDBASE:
        doImportBirdbase();
        break;
      case BIRD_BRAIN:
        doImportBirdBrain();
        break;
      case BIRD_TRACK:
        doImportBirdTrack();
        break;
      case BIRDERS_DIARY:
        doImportBirdersDiary();
        break;
      case BIRDLASSER:
        doImportBirdLasser();
        break;
      case BIRDA:
        doImportBirda();
        break;
      case EBIRD:
        doImportEBird();
        break;
      case FLICKR:
        doImportFlickr();
        break;
      case HBW_ALIVE:
        doImportHbwAlive();
        break;
      case INATURALIST:
        doImportINaturalist();
        break;
      case OBSERVADO:
        doImportObservationOrg();
        break;
      case ORNITHO:
        doImportOrnitho();
        break;
      case SCYTHEBILL:
        doImportScythebill();
        break;
      case WILDLIFE_RECORDER:
        doImportWildlifeRecorder();
        break;
      case WINGS:
        doImportWings();
        break;
      default:
        throw new IllegalStateException("Unimplemented import type " + importType);
      
    }
  }

  private void doImportAvisys() {
    FileFilter fileFilter = new FileNameExtensionFilter(
        Messages.getMessage(Name.AVISYS_EXPORT_TYPES), "csv", "txt");
    File openFile = fileDialogs.openFile(
        UIUtils.findFrame(this), Messages.getFormattedMessage(Name.OPEN_EXPORT_FORMAT, "Avisys"), fileFilter, FileType.OTHER);
    if (openFile != null) {
      AvisysImporter importer = new AvisysImporter(
          reportSet,
          // Import birds using the Clements taxonomy, everything else using the current taxonomy
          taxonomyStore.isBirdTaxonomy() ? taxonomyStore.getClements() : taxonomyStore.getTaxonomy(),
          checklists,
          transposedChecklistsSynthesizer,
          predefinedLocations,
          openFile);
      
      int showYesNo = alerts.showYesNo(this, Name.FIELD_NOTES_FILE_TITLE, Name.FIELD_NOTES_FILE_MESSAGE);
      if (showYesNo == JOptionPane.YES_OPTION) {
        FileFilter fieldNotesFilter = new FileNameExtensionFilter(
            Messages.getMessage(Name.FIELD_NOTES_FILE_TYPE), "txt");
        File fieldNotesFile = fileDialogs.openFile(UIUtils.findFrame(this), Messages.getMessage(Name.OPEN_FIELD_NOTES_FILE), fieldNotesFilter, FileType.OTHER);
        if (fieldNotesFile == null) {
          return;
        } else {
          importer.attachFieldNotes(fieldNotesFile);
        }
      }
      doImport(importer, importCompleteAction, ImportType.AVISYS);
    }
  }
  
  private void doImportBirdbase() {
    File openFile = fileDialogs.openFile(UIUtils.findFrame(this), Messages.getFormattedMessage(Name.OPEN_EXPORT_FORMAT, "Birdbase"), null, FileType.OTHER);
    if (openFile != null) {
      BirdbaseImporter importer = new BirdbaseImporter(
          reportSet,
          // Import birds using the Clements taxonomy, everything else using the current taxonomy
          taxonomyStore.isBirdTaxonomy() ? taxonomyStore.getClements() : taxonomyStore.getTaxonomy(),
          checklists, predefinedLocations, transposedChecklistsSynthesizer, openFile);
      doImport(importer, importCompleteAction, ImportType.BIRDBASE);
    }
  }

  private void doImportBirdBrain() {
    FileFilter fileFilter = new FileNameExtensionFilter(
        Messages.getFormattedMessage(Name.CSV_EXPORT_TYPE, "Bird Brain"), "csv");
    File openFile = fileDialogs.openFile(UIUtils.findFrame(this), Messages.getFormattedMessage(Name.OPEN_EXPORT_FORMAT, "Bird Brain"), fileFilter, FileType.OTHER);
    if (openFile != null) {
      BirdBrainImporter importer = new BirdBrainImporter(reportSet, taxonomyStore.getClements(), checklists, predefinedLocations, openFile);
      doImport(importer, importCompleteAction, ImportType.BIRD_BRAIN);
    }
  }

  private void doImportBirdTrack() {
    FileFilter fileFilter = new FileNameExtensionFilter(
        Messages.getFormattedMessage(Name.XLSX_EXPORT_TYPE, "BirdTrack"), "xlsx");
    File openFile = fileDialogs.openFile(UIUtils.findFrame(this), Messages.getFormattedMessage(Name.OPEN_EXPORT_FORMAT, "BirdTrack"), fileFilter, FileType.OTHER);
    if (openFile != null) {
      BirdTrackImporter importer = new BirdTrackImporter(reportSet, taxonomyStore.getIoc(), checklists, predefinedLocations, openFile);
      doImport(importer, importCompleteAction, ImportType.BIRD_TRACK);
    }
  }

  private void doImportBirdersDiary() {
    FileFilter fileFilter = new FileNameExtensionFilter(
        Messages.getFormattedMessage(Name.CSV_EXPORT_TYPE, "Birder's Diary"), "csv");
    File openFile = fileDialogs.openFile(UIUtils.findFrame(this), Messages.getFormattedMessage(Name.OPEN_EXPORT_FORMAT, "Birder's Diary"), fileFilter, FileType.OTHER);
    if (openFile != null) {
      doImport(
          BirdersDiary.chooseImporter(openFile, reportSet, taxonomyStore.getTaxonomy(), checklists, predefinedLocations),
          importCompleteAction, ImportType.BIRDERS_DIARY);
    }
  }

  private void doImportBirdLasser() {
    FileFilter fileFilter = new FileNameExtensionFilter(
        Messages.getFormattedMessage(Name.CSV_EXPORT_TYPE, "BirdLasser"), "csv");
    File openFile = fileDialogs.openFile(UIUtils.findFrame(this), Messages.getFormattedMessage(Name.OPEN_EXPORT_FORMAT, "BirdLasser"), fileFilter, FileType.OTHER);
    if (openFile != null) {
      BirdLasserImporter importer = new BirdLasserImporter(reportSet, taxonomyStore.getIoc(), checklists, predefinedLocations, openFile);
      doImport(importer, importCompleteAction, ImportType.BIRDLASSER);
    }
  }

  private void doImportBirda() {
    FileFilter fileFilter = new FileNameExtensionFilter(
        Messages.getFormattedMessage(Name.CSV_EXPORT_TYPE, "Birda"), "csv");
    File openFile = fileDialogs.openFile(UIUtils.findFrame(this), Messages.getFormattedMessage(Name.OPEN_EXPORT_FORMAT, "Birda"), fileFilter, FileType.OTHER);
    if (openFile != null) {
      BirdaImporter importer = new BirdaImporter(
          reportSet,
          taxonomyStore.isBirdTaxonomy() ? taxonomyStore.getTaxonomy() : taxonomyStore.getClements(),
          checklists, predefinedLocations, openFile);
      doImport(importer, importCompleteAction, ImportType.BIRDA);
    }
  }

  /** Import an eBird file. */
  public void importEBirdFile(File file, Action finishedAction) {
    SightingsImporter<?> importer = ebird.chooseImporter(file, reportSet, clements, checklists, predefinedLocations);
    doImport(importer, finishedAction, ImportType.EBIRD);
  }

  private void doImportEBird() {
    FileFilter fileFilter = new FileNameExtensionFilter(
        Messages.getFormattedMessage(Name.CSV_OR_ZIP_EXPORT_TYPE, "eBird"), "csv", "zip");
    File openFile = fileDialogs.openFile(UIUtils.findFrame(this), Messages.getFormattedMessage(Name.OPEN_EXPORT_FORMAT, "eBird"), fileFilter, FileType.OTHER);
    if (openFile != null) {
      SightingsImporter<?> importer = ebird.chooseImporter(openFile, reportSet,
          // Import birds using the Clements taxonomy, everything else using the current taxonomy.
          // Originally, this just imported using Clements, since eBird is just birds, but there's software
          // like BirdJournal that will export *non*-bird lists using the eBird format!
          taxonomyStore.isBirdTaxonomy() ? taxonomyStore.getClements() : taxonomyStore.getTaxonomy(), checklists, predefinedLocations);
      doImport(importer, importCompleteAction, ImportType.EBIRD);
    }
  }

  private void doImportFlickr() {
    ImportFlickrPanel importFlickrPanel = importFlickrPanelProvider.get();
    if (navigableFrame.navigateTo(importFlickrPanel)) {
      importFlickrPanel.start((importer) -> {
        doImport(importer, importCompleteAction, ImportType.FLICKR);
      });
    }
  }

  private void doImportHbwAlive() {
    FileFilter fileFilter = new FileNameExtensionFilter(
        Messages.getFormattedMessage(Name.XLS_EXPORT_TYPE, "HBW Alive"), "xls", "xlsx");
    File openFile = fileDialogs.openFile(UIUtils.findFrame(this), Messages.getFormattedMessage(Name.OPEN_EXPORT_FORMAT, "HBW Alive"), fileFilter, FileType.OTHER);
    if (openFile != null) {
      // Use Clements, not IOC.  IOC might be a bit closer in taxonomy, but Clements has far more
      // IOC alternate names (basically because of groups).
      SightingsImporter<?> importer =
          new HbwImporter(reportSet, taxonomyStore.getClements(), checklists, predefinedLocations, openFile);
      doImport(importer, importCompleteAction, ImportType.HBW_ALIVE);
    }
  }

  private void doImportINaturalist() {
    FileFilter fileFilter = new FileNameExtensionFilter(
        Messages.getFormattedMessage(Name.CSV_EXPORT_TYPE, "iNaturalist"), "csv");
    File openFile = fileDialogs.openFile(UIUtils.findFrame(this), Messages.getFormattedMessage(Name.OPEN_EXPORT_FORMAT, "iNaturalist"), fileFilter, FileType.OTHER);
    if (openFile != null) {
      // Use the current taxonomy ... but for birds, always Clements, since that's what iNaturalist follows.
      Taxonomy taxonomy = taxonomyStore.getTaxonomy();
      if (taxonomy.isBuiltIn()) {
        taxonomy = taxonomyStore.getClements();
      }
      INaturalistImporter importer = new INaturalistImporter(reportSet, taxonomy, checklists, predefinedLocations, openFile);
      Set<String> classes = importer.findAllClasses(openFile);
      if (classes.isEmpty()) {
        // No classes found: just give up. 
        importer.chooseClass(taxonomy.getRoot().getName());
      } else if (classes.size() == 1) {
        // Just one class found, use that
        importer.chooseClass(classes.iterator().next());
      } else {
        // See if we get lucky, and the custom taxonomy magically has a reasonable
        // class name at the root.  (They generally don't, though perhaps this is worth
        // fixing!)
        if (classes.contains(taxonomy.getRoot().getName())) {
          importer.chooseClass(taxonomy.getRoot().getName());
        } else {
          // Can't pick a class name. Make the user choose.
          JComboBox<String> classCombobox =
              new JComboBox<>(classes.toArray(new String[classes.size()]));
          classCombobox.setMaximumRowCount(classes.size());
          fontManager.applyTo(classCombobox);
          if (JOptionPane.showConfirmDialog(this,
              new Object[] {
                  alerts.getFormattedDialogMessage(Name.CHOOSE_A_TAXONOMIC_CLASS_TITLE,
                      Name.CHOOSE_A_TAXONOMIC_CLASS_FORMATTED_MESSAGE, taxonomy.getName()),
                  classCombobox},
              "",
              JOptionPane.OK_CANCEL_OPTION,
              JOptionPane.INFORMATION_MESSAGE,
              alerts.getAlertIcon()) != JOptionPane.OK_OPTION) {
            return;
          }
          importer.chooseClass((String) classCombobox.getSelectedItem());
        }
      }
      doImport(importer, importCompleteAction, ImportType.INATURALIST);
    }
  }
  
  private void doImportScythebill() {
    FileFilter fileFilter = new FileNameExtensionFilter(
        Messages.getFormattedMessage(Name.CSV_EXPORT_TYPE, "Scythebill"), "csv");
    File openFile = fileDialogs.openFile(UIUtils.findFrame(this), Messages.getFormattedMessage(Name.OPEN_EXPORT_FORMAT, "Scythebill"), fileFilter, FileType.OTHER);
    if (openFile != null) {
      // Import using whatever the current taxonomy is
      ScythebillImporter importer = new ScythebillImporter(reportSet, taxonomyStore.getTaxonomy(), checklists, predefinedLocations, openFile);
      doImport(importer, importCompleteAction, ImportType.SCYTHEBILL);
    }
  }

  private void doImportObservationOrg() {
    FileFilter fileFilter = new FileNameExtensionFilter(
        Messages.getFormattedMessage(Name.CSV_EXPORT_TYPE, "Observation.org"), "csv");
    File openFile = fileDialogs.openFile(UIUtils.findFrame(this), Messages.getFormattedMessage(Name.OPEN_EXPORT_FORMAT, "Observation.org"), fileFilter, FileType.OTHER);
    if (openFile != null) {
      // Import using whatever the current taxonomy is
      SightingsImporter<?> importer = Observado.newImporter(
          reportSet,
          // Import birds using the IOC taxonomy, everything else using the current taxonomy
          taxonomyStore.isBirdTaxonomy() ? taxonomyStore.getIoc() : taxonomyStore.getTaxonomy(),
          checklists,
          predefinedLocations,
          openFile);
      doImport(importer, importCompleteAction, ImportType.OBSERVADO);
    }
  }

  private void doImportOrnitho() {
    FileFilter fileFilter = new FileNameExtensionFilter(
        Messages.getFormattedMessage(Name.TXT_EXPORT_TYPE, "Ornitho"), "txt");
    File openFile = fileDialogs.openFile(UIUtils.findFrame(this), Messages.getFormattedMessage(Name.OPEN_EXPORT_FORMAT, "Ornitho"), fileFilter, FileType.OTHER);
    if (openFile != null) {
      OrnithoImporter importer = new OrnithoImporter(
          reportSet,
          // TODO: is IOC really closer?
          taxonomyStore.getIoc(),
          checklists,
          predefinedLocations,
          openFile);
      doImport(importer, importCompleteAction, ImportType.ORNITHO);
    }
  }

  private void doImportWildlifeRecorder() {
    FileFilter fileFilter = new FileNameExtensionFilter(
        Messages.getFormattedMessage(Name.CSV_EXPORT_TYPE, "Wildlife Recorder"), "csv");
    File openFile = fileDialogs.openFile(UIUtils.findFrame(this), Messages.getFormattedMessage(Name.OPEN_EXPORT_FORMAT, "Wildlife Recorder"), fileFilter, FileType.OTHER);
    if (openFile != null) {
      File locationsFile = null;
      int showYesNo = alerts.showYesNo(this,
          Name.WR_LOCATION_EXPORTS_FILE_TITLE,
          Name.WR_LOCATION_EXPORTS_FILE_MESSAGE);
      if (showYesNo == JOptionPane.YES_OPTION) {
        FileFilter locationsFilter = new FileNameExtensionFilter(
            Messages.getFormattedMessage(Name.CSV_EXPORT_TYPE, "Wildlife Recorder Locations"), "csv");
        locationsFile = fileDialogs.openFile(UIUtils.findFrame(this), Messages.getMessage(Name.OPEN_FIELD_NOTES_FILE), locationsFilter, FileType.OTHER);
        if (locationsFile == null) {
          return;
        }
      }
      WildlifeRecorderImporter importer = new WildlifeRecorderImporter(
          reportSet,
          taxonomyStore.getTaxonomy(),
          checklists,
          predefinedLocations,
          openFile,
          locationsFile);

      doImport(importer, importCompleteAction, ImportType.WILDLIFE_RECORDER);
    }
  }

  private void doImportWings() {
    FileFilter fileFilter = new FileNameExtensionFilter(
        Messages.getFormattedMessage(Name.XML_EXPORT_TYPE, "Wings"), "xml");
    File[] files = fileDialogs.openFiles(UIUtils.findFrame(this),
        Messages.getMessage(Name.OPEN_WINGS_FILES), fileFilter, FileType.OTHER);
    if (files == null || files.length == 0) {
      return;
    }
    
    // TODO: is it really IOC?
    WingsImporter importer = new WingsImporter(
        reportSet, taxonomyStore.getIoc(), checklists, predefinedLocations,
        Arrays.asList(files));
    doImport(importer, importCompleteAction, ImportType.WINGS);
  }

  /**
   * First step of imports:
   * <ol>
   * <li>Run an initial check on the import file.
   * <li>Parse taxa, perhaps requiring the user to manually resolve any taxa that couldn't
   *     be resolved manually.
   * </ol>
   */
  private void doImport(
      SightingsImporter<?> importer,
      Action finishedAction,
      ImportType importType) {
    importPreferences.addRecentlyUsedImport(importType);
    // Set to not-yet-gathered
    locationsBeforeImport = null;
    
    Runnable task = importer.longRunningTask();
    if (task == null) {
      beginImport(importer, finishedAction, importType);
    } else {
      setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
      // Submit the task, but cancel after 10 seconds
      ListenableFuture<?> future = 
          Futures.withTimeout(
              executorService.submit(task),
              10,
              TimeUnit.SECONDS,
              executorService);
      // And when the future is done, continue
      Futures.addCallback(future, new FutureCallback<Object>() {
        @Override
        public void onFailure(Throwable t) {
          logger.log(Level.WARNING, "Could not complete long running import task", t);
          done();
        }

        @Override
        public void onSuccess(@Nullable Object o) {
          done();
        }
        
        private void done() {
          SwingUtilities.invokeLater(() -> {
            setCursor(Cursor.getDefaultCursor());
            beginImport(importer, finishedAction, importType);
          });
        }
      }, executorService);
    }
  }

  private void beginImport(
      SightingsImporter<?> importer,
      Action finishedAction,
      ImportType importType) {
    try {
      // Do an initial check to see if the import is garbage.
      Optional<String> initialCheck = importer.initialCheck();
      if (initialCheck.isPresent()) {
        String initialCheckMessage = String.format(initialCheck.get(), HtmlResponseWriter.htmlEscape(importer.importFileName()));
        alerts.showError(this,
            Name.FAILED_IMPORT_TITLE,
            Name.FAILED_IMPORT_FORMAT,
            initialCheckMessage);
        // No actions taken, so no need to call finishFailedOrCanceledImport()
        return;
      }
      
      // Trim out unnecessary dates if needed
      if (importer.isMassExport()) {
        ReadablePartial nullDate = PartialIO.fromString("1900-01-01");
        Function<Sighting, ReadablePartial> dateAsPartialMapping = s -> {
          ReadablePartial date = s.getDateAsPartial();
          return date == null ? nullDate : date;
        };

        // Look for matching dates (in a compatible taxonomy)
        Set<ReadablePartial> existingDates = 
            Streams.stream(reportSet.getSightings(importer.getTaxonomy()))
            .filter(this::doesNotHavePrivateLocation)
            .map(dateAsPartialMapping)
                .filter(p -> p != null)
                .collect(Collectors.toCollection(LinkedHashSet::new));
        Comparator<ReadablePartial> dateComparator = SightingComparators::comparePartials;
        
        Collection<ReadablePartial> datesFromImport = importer.getAllDates();
        TreeSet<ReadablePartial> newDates = datesFromImport.stream()
            .filter(p -> p != null)
            .collect(Collectors.toCollection(() -> new TreeSet<>(dateComparator)));
        int newDatesCountPriorToRemovingExisting = newDates.size();
        newDates.removeAll(existingDates);
        int newDatesCountAfterRemovingExisting = newDates.size();
        
        // If there's some new dates, *and* it's not all new dates, then prompt the user to just
        // import the new dates.
        if (newDatesCountAfterRemovingExisting > 0
            && newDatesCountAfterRemovingExisting < newDatesCountPriorToRemovingExisting) {
          ReadablePartial first = newDates.first();
          ReadablePartial last = newDates.last();
          if (alerts.showYesNo(this,
              Name.IMPORT_ONLY_NEW_DATES_TITLE, Name.IMPORT_ONLY_NEW_DATES_FORMAT,
              newDatesCountAfterRemovingExisting,
              PartialIO.toShortUserString(first, Locale.getDefault()),
              PartialIO.toShortUserString(last, Locale.getDefault())) == JOptionPane.YES_OPTION) {
            importer.onlyImportDatesIn(newDates);
          }
        } else if (newDatesCountAfterRemovingExisting == 0) {
          // It's all duplicate dates. The user probably wants to quit.
          if (alerts.showYesNo(this,
              Name.IMPORT_DATES_ARE_ENTIRELY_DUPLICATED_TITLE,
              Name.IMPORT_DATES_ARE_ENTIRELY_DUPLICATED_MESSAGE) != JOptionPane.YES_OPTION) {
            return;
          }
        }
      }
      
      // Continue by importing taxa
      parseTaxaAndLocations(importer, finishedAction, importType);
    } catch (ImportException e) {
      alerts.showError(
          this,
          Messages.getMessage(Name.FAILED_IMPORT_TITLE),
          e.getMessage());
      logger.log(Level.WARNING, "Failed import", e);
      finishFailedOrCanceledImport();
    } catch (Exception e) {
      alerts.showError(
          this,
      e,
          Name.FAILED_IMPORT_TITLE,
          Name.IMPORTING_UNSUCCESSFUL_MESSAGE,
          HtmlEscapers.htmlEscaper().escape(importer.importFileName()));
      logger.log(Level.WARNING, "Failed import", e);
      finishFailedOrCanceledImport();
    }
  }

  /**
   * Do a combined parse of taxa and locations.  This ensures that all parse-time errors get
   * caught up front (before resolving anything).
   */
  private void parseTaxaAndLocations(
      SightingsImporter<?> importer,
      Action finishedAction,
      ImportType importType) throws IOException {
    importer.beforeParseTaxonomyIds();
    
    Map<Object, ToBeDecided> allTbdIds = importer.parseTaxonomyIds();
    locationsBeforeImport = new LinkedHashSet<>();
    Locations.visitLocations(reportSet.getLocations(), l -> locationsBeforeImport.add(l.getId()));
    
    ParsedLocationIds parsedLocationIds = importer.parseLocationIds();

    resolveImportedTaxa(importer, allTbdIds, parsedLocationIds, finishedAction, importType);
  }

  private void resolveImportedTaxa(
      SightingsImporter<?> importer,
      Map<Object, ToBeDecided> allTbdIds,
      ParsedLocationIds parsedLocationIds,
      Action finishedAction,
      ImportType importType) {
    try {
      // Only bother validating TBDs that aren't ignorable *and* have either a common or scientific name.
      Map<Object, ToBeDecided> validTbdIds =
          Maps.filterValues(allTbdIds, tbd ->
              !tbd.shouldIgnore()
              && !Strings.isNullOrEmpty(tbd.commonName)
              && !Strings.isNullOrEmpty(tbd.scientificName));
      // Ignored taxon IDs is *just* ignored entries, which really is just eBird "sp.". 
      importer.setIgnoredTaxonIds(
          allTbdIds.entrySet().stream()
              .filter(entry -> entry.getValue().shouldIgnore())
              .map(entry -> entry.getKey())
              .collect(ImmutableSet.toImmutableSet()));
      
      if (!validTbdIds.isEmpty()) {
        LocalNames localNames = importer.getTaxonomy().getLocalNames();
        // See if any alternate translations are available...
        Set<AvailableLocale> availableLocales = localNames.availableLocales(importer.getTaxonomy());
        if (!availableLocales.isEmpty()) {
          // And if there's any common names that need to be looked up
          Set<String> commonNames = validTbdIds.values().stream()
              .filter(tbd -> tbd.commonName != null)
              .map(tbd -> tbd.commonName)
              .collect(ImmutableSet.toImmutableSet());
          if (!commonNames.isEmpty()) {
            // And if there's any improved translations for this
            Multimap<AvailableLocale, String> possibleTranslations =
                localNames.possibleTranslations(availableLocales, commonNames);
            if (!possibleTranslations.isEmpty()) {
              // Pick the best possible such locale - the one with the most new names
              // that would get parsed.  TODO: this isn't perfect; it's theoretically possible that this
              // new set of names is perfect for what got missed, but a disaster for what was successfully
              // parsed.  I suspect this will rarely be the case, though.
              AvailableLocale bestLocale = possibleTranslations.keySet().stream()
                  .max(Comparator.comparing(locale -> possibleTranslations.get(locale).size()))
                  .get();
              AvailableLocale currentLocale = localNames.currentLocale(importer.getTaxonomy());
              // Make sure we don't show nonsense... not that this *should* happen (after all, we just
              // failed to translate these names!)
              if (bestLocale != currentLocale) {
                int showYesNo = alerts.showYesNo(
                    this,
                    Name.USE_DIFFERENT_INTERNATIONAL_NAMES_TITLE,
                    Name.USE_DIFFERENT_INTERNATIONAL_NAMES_FORMAT,
                    currentLocale, possibleTranslations.get(bestLocale).size(), bestLocale);
                if (showYesNo == JOptionPane.YES_OPTION) {
                  localNames.setLocale(importer.getTaxonomy(), bestLocale);
                  // Reparse!
                  allTbdIds = importer.parseTaxonomyIds();
                  validTbdIds = Maps.filterValues(allTbdIds, tbd -> !tbd.shouldIgnore());
                }
              }
            }
          } 
        }
      }
      
      if (!validTbdIds.isEmpty()) {
        int successfullyParsedTaxa = importer.successfullyResolvedTaxa();
        if (successfullyParsedTaxa * 10 < validTbdIds.size()) {
          Taxonomy taxonomy = importer.getTaxonomy();
          ToBeDecided singleToBeDecided = validTbdIds.values().iterator().next();
          String name = singleToBeDecided.commonName == null
              ? singleToBeDecided.scientificName : singleToBeDecided.commonName;
          
          if (successfullyParsedTaxa == 0) {
            // If no taxa were successfully resolved, warn that this might be the wrong taxonomy
            int showYesNo = alerts.showYesNo(
                this,
                Name.RIGHT_TAXONOMY_TITLE,
                Name.RIGHT_TAXONOMY_FORMAT_NO_TAXA,
                validTbdIds.size(),
                name,
                taxonomy.getName());
            if (showYesNo != JOptionPane.YES_OPTION) {
              return;
            }
          } else if (successfullyParsedTaxa * 10 < validTbdIds.size() 
              && importer.expectTaxonSuccess()){
            // Less than 10% were parsed successfully. Unless the importer says this is
            // no big deal, stop.
            int showYesNo = alerts.showYesNo(
                this,
                Name.RIGHT_TAXONOMY_TITLE,
                Name.RIGHT_TAXONOMY_FORMAT_FEW_TAXA,
                successfullyParsedTaxa,
                validTbdIds.size(),
                name,
                taxonomy.getName());
            if (showYesNo != JOptionPane.YES_OPTION) {
              return;
            }
          }
        }
        
        // Taxa to resolve: kick off that UI, and go.
        ImportResolveTaxaPanel importResolveTaxaPanel = resolveTaxaPanelProvider.get();
        importResolveTaxaPanel.start(validTbdIds, importer, () -> {
          resolveImportedLocations(importer, parsedLocationIds, finishedAction);
        });
        navigableFrame.navigateTo(importResolveTaxaPanel);
      } else {
        resolveImportedLocations(importer, parsedLocationIds, finishedAction);
      }
    } catch (ImportException e) {
      alerts.showError(
          this,
          Messages.getMessage(Name.FAILED_IMPORT_TITLE),
          e.getMessage());
      logger.log(Level.WARNING, "Failed import", e);
      finishFailedOrCanceledImport();
    } catch (Exception e) {
      alerts.showError(
          this,
	  e,
          Name.FAILED_IMPORT_TITLE,
          Name.IMPORTING_UNSUCCESSFUL_MESSAGE,
          HtmlEscapers.htmlEscaper().escape(importer.importFileName()));
      logger.log(Level.WARNING, "Failed import", e);
      finishFailedOrCanceledImport();
    }
  }
  
  /**
   * Second step of imports:  resolve locations, some automatically, some with manual intervention.
   */
  private void resolveImportedLocations(SightingsImporter<?> importer, ParsedLocationIds parsedLocationIds, Action finishedAction) {
    try {
      ImmutableMap<Object, com.scythebill.birdlist.ui.imports.ParsedLocationIds.ToBeDecided> toBeResolvedNames = parsedLocationIds.toBeResolvedNames();
      if (toBeResolvedNames.isEmpty()) {
        parseSightings(importer, finishedAction);
      } else {
        // Try to auto-geocode as much as possible
        AutoGeocodeLocations autoGeocodeLocations = autoGeocodeLocationsProvider.get();
        ListenableFuture<Void> autoGeocodeCompleted = autoGeocodeLocations.autoGeocodeLocations(parsedLocationIds, reportSet);

        navigableFrame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        autoGeocodeCompleted.addListener(
            () -> {
              // Don't really care if it succeeded or failed... whatever's left needs to get resolved. (But back on the
              // UI thread.)
              
              SwingUtilities.invokeLater(() -> {
                navigableFrame.setCursor(Cursor.getDefaultCursor());
                // Get a new set of "to-be-resolved-names"
                ImmutableMap<Object, com.scythebill.birdlist.ui.imports.ParsedLocationIds.ToBeDecided> toBeResolvedNames2 =
                    parsedLocationIds.toBeResolvedNames();
                if (toBeResolvedNames2.isEmpty()) {
                  parseSightings(importer, finishedAction);
                } else {
                  ImportResolveLocationsPanel importResolveLocationsPanel = resolveLocationsPanelProvider.get();
                  importResolveLocationsPanel.start(reportSet, toBeResolvedNames2, parsedLocationIds, () -> {
                    parseSightings(importer, finishedAction);
                  });
                  navigableFrame.navigateTo(importResolveLocationsPanel);                
                }
              });
            }, MoreExecutors.directExecutor());
      }
    } catch (ImportException e) {
      alerts.showError(
          this,
          Messages.getMessage(Name.FAILED_IMPORT_TITLE),
          e.getMessage());
      logger.log(Level.WARNING, "Failed import", e);
      finishFailedOrCanceledImport();
    } catch (Exception e) {
      alerts.showError(
          this,
	  e,
          Name.FAILED_IMPORT_TITLE,
          Name.IMPORTING_UNSUCCESSFUL_MESSAGE,
          HtmlEscapers.htmlEscaper().escape(importer.importFileName()));
      logger.log(Level.WARNING, "Failed import", e);
      finishFailedOrCanceledImport();
    }
  }
  
  /**
   * Third step: import the sightings, and request entry of users if necessary.
   */
  private void parseSightings(
      SightingsImporter<?> importer, Action finishedAction) {
    try {
      List<Sighting> imports = importer.parseSightings();
      if (imports.isEmpty()) {
        alerts.showError(this,
            Name.FAILED_IMPORT_TITLE,
            Name.IMPORTING_EMPTY_RESULT_MESSAGE,
            HtmlResponseWriter.htmlEscape(importer.importFileName()));
        return;
      }
      
      if (reportSet.getUserSet() != null && !importer.importContainedUserInformation()) {
        ImportChooseUsersPanel importChooseUsersPanel = chooseUsersPanelProvider.get();
        importChooseUsersPanel.chooseUsers(users -> {
          if (!users.isEmpty()) {
            for (Sighting sighting : imports) {
              sighting.getSightingInfo().setUsers(users);
            }
            checkDuplicates(importer, finishedAction, imports);
          }
        });
        navigableFrame.navigateTo(importChooseUsersPanel);
      } else {
        checkDuplicates(importer, finishedAction, imports);
      }
    } catch (ImportException e) {
      alerts.showError(
          this,
          Messages.getMessage(Name.FAILED_IMPORT_TITLE),
          e.getMessage());
      logger.log(Level.WARNING, "Failed import", e);
      finishFailedOrCanceledImport();
    } catch (Exception e) {
      alerts.showError(
          this,
	  e,
          Name.FAILED_IMPORT_TITLE,
          Name.IMPORTING_UNSUCCESSFUL_MESSAGE,
          HtmlEscapers.htmlEscaper().escape(importer.importFileName()));
      logger.log(Level.WARNING, "Failed import", e);
      finishFailedOrCanceledImport();
    }
  }

  private boolean doesNotHavePrivateLocation(Sighting sighting) {
    if (sighting.getLocationId() == null) {
      return true;
    }
    
    return !reportSet.getLocations().getLocation(sighting.getLocationId()).isPrivate();
  }
  
  /**
   * Final step in the import.
   *  
   * <ul>
   * <li>Check for duplicates, and see how the user wants to handle them.
   * <li>Apply sightings.
   * <li>Navigate either to taxon resolution or the main menu. 
   * </ul>
   */
  private void checkDuplicates(
      SightingsImporter<?> importer, Action finishedAction, List<Sighting> imports) {        
    if (reportSet.getUserSet() == null) {
      checkDuplicatesWithoutUsers(importer, finishedAction, imports);
    } else {
      checkDuplicatesWithUsers(importer, finishedAction, imports);
    }    
  }

  /**
   * Handle duplicate detection logic *without* being concerned about users.
   */
  private void checkDuplicatesWithoutUsers(SightingsImporter<?> importer, Action finishedAction,
      List<Sighting> imports) {
    int countDuplicates = countDuplicates(imports, SightingFunnel.INSTANCE);    
    int droppedDuplicates = 0;
    // Existing sightings that will need to be removed.
    List<Sighting> sightingsToRemove = ImmutableList.of();
    if (countDuplicates > 0
        && countDuplicates > (2 * (imports.size() * FALSE_POSITIVES))) {
      
      if (countDuplicates == imports.size()) {
        Object[] options = {
            Messages.getMessage(Name.IMPORT_ANYWAY),
            Messages.getMessage(Name.OVERWRITE_DUPLICATES),
            Messages.getMessage(Name.CANCEL_BUTTON)
            };

        String message = Messages.getFormattedMessage(Name.ALL_SIGHTINGS_DUPLICATES,
            imports.size());

        int chosenOption = alerts.showWithOptions(
            this,
            Messages.getMessage(Name.DUPLICATE_IMPORTS_QUESTION),
            message,
            options);
        if (chosenOption == 2 || chosenOption < 0) {
          finishFailedOrCanceledImport();
          return;
        }
        
        if (chosenOption == 1) {
          sightingsToRemove = findExistingDuplicates(imports, DuplicateSightingKey::forSighting);
          alerts.showMessage(this,
              Name.SIGHTINGS_OVERWRITTEN_TITLE,
              Name.SIGHTINGS_OVERWRITTEN_FORMAT,
              sightingsToRemove.size());
        }
      } else {
        Object[] options = {
            Messages.getMessage(Name.IMPORT_ALL),
            Messages.getMessage(Name.DROP_DUPLICATES),
            Messages.getMessage(Name.OVERWRITE_DUPLICATES),
            Messages.getMessage(Name.CANCEL_BUTTON)
        };

        String message = Messages.getFormattedMessage(
            Name.SOME_SIGHTINGS_DUPLICATES,
            imports.size(),
            countDuplicates);
        
        int chosenOption = alerts.showWithOptions(
            this,
            Messages.getMessage(Name.DUPLICATE_IMPORTS_QUESTION),
            message,
            options);
        if (chosenOption == 3 || chosenOption < 0) {
          finishFailedOrCanceledImport();
          return;
        }
        
        if (chosenOption == 1) {
          List<Sighting> removedDuplicates =
              dropDuplicates(importer, imports, DuplicateSightingKey::forSighting);
          droppedDuplicates = imports.size() - removedDuplicates.size();
          if (droppedDuplicates > 0) {
            alerts.showMessage(this, 
                Name.DUPLICATES_DROPPED_TITLE,
                Name.DUPLICATES_DROPPED_FORMAT,
                droppedDuplicates);
            imports = removedDuplicates;
          }
        } else if (chosenOption == 2) {
          sightingsToRemove = findExistingDuplicates(imports, DuplicateSightingKey::forSighting);
          alerts.showMessage(this,
              Name.SIGHTINGS_OVERWRITTEN_TITLE,
              Name.SIGHTINGS_OVERWRITTEN_FORMAT,
              sightingsToRemove.size());
        }
      }
    }
    
    finishImport(importer, finishedAction, droppedDuplicates, imports, sightingsToRemove);
  }

  /**
   * Handle duplicate detection *while* caring about users.
   */
  private void checkDuplicatesWithUsers(SightingsImporter<?> importer, Action finishedAction,
      List<Sighting> imports) {
    Preconditions.checkState(reportSet.getUserSet() != null);
    int countDuplicates = countDuplicates(imports, SightingFunnel.INSTANCE);
    boolean areThereDuplicates = countDuplicates > (2 * (imports.size() * FALSE_POSITIVES));
    // Short-circuit: no duplicates to consider, just get it done 
    if (!areThereDuplicates) {
      finishImport(importer, finishedAction, 0 /* no dropped duplicates */, imports, ImmutableList.of());
      return;
    }
    
    // If there's some duplicates (ignoring users), then gather stats for how different
    // the users are between the imported and existing sightings.
    UserOverlapStats userOverlapStats =
        getUserOverlapStats(imports, DuplicateSightingKey::forSighting);
    if (userOverlapStats.actuallyNoDuplicates()) {
      // Looked again, and nope, there really aren't any duplicates.  Skip to the end.
      finishImport(importer, finishedAction, 0 /* no dropped duplicates */, imports,
          ImmutableList.of());
      return;
    }
    
    // If all the user stats say it's the same users (or a subset), then this is basically not that
    // different from a no-observer import;  jump to that path.
    // (This does re-compute duplicates... but... meh.  It's just not that slow.)
    if (userOverlapStats.existingUsersAreTheSameOrSuperset()) {
      checkDuplicatesWithoutUsers(importer, finishedAction, imports);
      return;
    }
    
    // At this point, we're fairly sure that the set of observers is more ... interesting ... relative
    // to the existing data.  And, importantly, all of the data so far *ignores the possibility of different
    // locations*.  Which is fine when there's no observers (or it's all the same observers), since they clearly
    // were in the same location!  But it's more equivocal if there are multiple users. 
    
    // First, recalculate the overlap stats once more, but taking into account location.
    UserOverlapStats userOverlapStatsWithLocation =
        getUserOverlapStats(imports, DuplicateSightingKey::forSightingWithLocation);
    
    Function<Sighting, DuplicateSightingKey> duplicateSightingKeyStrategy;
    
    if (userOverlapStatsWithLocation.totalOverlap() != userOverlapStats.totalOverlap()) {
      // So there's some difference between overlap with location and overlap without.
      // Now the tough question;  should we throw away the location data (in case of overlap)?
      // "Some(A few/most/all???) of these sightings come at the same date as existing sightings, but look like
      // different locations.  Should Scythebill treat these as separate sightings from their own locations,
      // or merge these into the existing sightings?  (See the manual for more discussion, if needed.)
      //  <Cancel> <Separate Sightings>  <Merge Sightings>.
      float ratio = ((float) userOverlapStatsWithLocation.totalOverlap()) / userOverlapStats.totalOverlap();
      Name name;
      if (userOverlapStatsWithLocation.totalOverlap() == 0) {
        name = Name.ALL_SIGHTINGS_HAVE_DIFFERENT_LOCATIONS;
      } else if (ratio < 0.1) {
        name = Name.A_FEW_SIGHTINGS_HAVE_DIFFERENT_LOCATIONS;
      } else if (ratio < 0.5) {
        name = Name.MANY_SIGHTINGS_HAVE_DIFFERENT_LOCATIONS;
      } else if (ratio < 0.95) {
        name = Name.MOST_SIGHTINGS_HAVE_DIFFERENT_LOCATIONS;
      } else {
        name = Name.NEARLY_ALL_SIGHTINGS_HAVE_DIFFERENT_LOCATIONS;
      }
      
      int chosenOption = alerts.showWithOptions(
          this,
          Name.IMPORTED_LOCATIONS_DIFFER,
          name,
          new Object[]{
              Messages.getMessage(Name.SEPARATE_SIGHTINGS),
              Messages.getMessage(Name.MERGE_SIGHTINGS),
              Messages.getMessage(Name.CANCEL_BUTTON)
          });
      if (chosenOption == 2 || chosenOption < 0) {
        finishFailedOrCanceledImport();
        return;
      }

      // Keeping sightings separate.
      if (chosenOption == 0) {
        if (userOverlapStatsWithLocation.actuallyNoDuplicates()) {
          // If sightings are being kept separate, and there's no overlap once locations are
          // accounted for, then skip any further considerations, and just import everything.
          finishImport(importer, finishedAction, 0 /* no dropped duplicates */, imports,
              ImmutableList.of());
          return;
        } else if (userOverlapStatsWithLocation.existingUsersAreTheSameOrSuperset()) {
          // And if users are the same or a subset, then it's pointless to ask about
          // how to merge users.  Drop whatever's an exact duplicate.
          List<Sighting> remainingImports =
              dropDuplicates(importer, imports, DuplicateSightingKey::forSightingWithLocation);
          finishImport(importer, finishedAction, imports.size() - remainingImports.size(),
              remainingImports, ImmutableList.of());
          return;
        }
        
        duplicateSightingKeyStrategy = DuplicateSightingKey::forSightingWithLocation;
      } else {
        duplicateSightingKeyStrategy = DuplicateSightingKey::forSighting;
      }
    } else {      
      // If the total overlap is exactly the same even taking into account location, choose a
      // strategy that ignores location (though, really, it doesn't matter which we choose - this
      // one's just a bit cheaper?)
      duplicateSightingKeyStrategy = DuplicateSightingKey::forSighting;
      // TODO: should we consider the case where "overlap with location" is *almost* as big as "overlap without location"
      // as something like this? 
    }
    
    // Now, we need to ask the user whether to (A) merge users, (B) overwrite duplicates,
    // or (C) drop duplicates.
    // TODO: info about how much overlap?
    int chosenOption = alerts.showWithOptions(
        this,
        Name.IMPORTED_OBSERVERS_DIFFER_TITLE,
        Name.IMPORTED_OBSERVERS_DIFFER_MESSAGE,
        new Object[]{
            Messages.getMessage(Name.MERGE_USERS),
            Messages.getMessage(Name.PREFER_IMPORT),
            Messages.getMessage(Name.PREFER_EXISTING),
            Messages.getMessage(Name.CANCEL_BUTTON)
        });
    if (chosenOption == 3 || chosenOption < 0) {
      finishFailedOrCanceledImport();
      return;
    }

    
    int droppedDuplicates;
    List<Sighting> sightingsToRemove;
    if (chosenOption == 0) {
      // merge users
      droppedDuplicates = 0;
      ModifiedImportsAndSightingsToRemove mergedObservers = mergeObservers(imports, duplicateSightingKeyStrategy);
      imports = mergedObservers.modifiedImports;
      sightingsToRemove = mergedObservers.sightingsToRemove;
    } else if (chosenOption == 1) {
      // prefer import (and overwrite existing sightings)   
      sightingsToRemove = findExistingDuplicates(imports, duplicateSightingKeyStrategy);
      droppedDuplicates = 0;
    } else {
      // prefer existing
      List<Sighting> remainingImports = dropDuplicates(importer, imports, duplicateSightingKeyStrategy);
      droppedDuplicates = imports.size() - remainingImports.size();
      imports = remainingImports;
      sightingsToRemove = ImmutableList.of();
    }
    
    finishImport(importer, finishedAction, droppedDuplicates, imports, sightingsToRemove);
  }

  private void finishImport(SightingsImporter<?> importer, Action finishedAction,
      int droppedDuplicates, List<Sighting> finalImports, List<Sighting> finalSightingsToRemove) {
    // Add the sightings, and any visit info data that accumulated during the import.
    reportSet.mutator().adding(finalImports).removing(finalSightingsToRemove).mutate();
    Map<VisitInfoKey, VisitInfo> visitInfoMap = importer.getAccumulatedVisitInfoMap();
    for (Map.Entry<VisitInfoKey, VisitInfo> entry : visitInfoMap.entrySet()) {
      reportSet.putVisitInfo(entry.getKey(), entry.getValue());
    }
    // Remove any locations that weren't needed.
    cleanUnneededLocations();

    if (recentEdits != null) {
      // Attach imported visit info to recent edits
      Set<VisitInfoKey> allVisitInfoKeys = extractVisitInfoKeys(finalImports);
      recentEdits.add(allVisitInfoKeys);
    }
    
    // Figure out if there's any "spuh"s to resolve.  There'll be some if
    // - The import generated some "spuh"s
    // - They'll still be "spuh"s in the user's preferred taxonomy
    Set<SightingTaxon> spToResolve;
    Taxonomy taxonomy = taxonomyStore.getTaxonomy();
    if (taxonomy instanceof MappedTaxonomy) {
      spToResolve = Sets.newLinkedHashSet();
      for (SightingTaxon taxon : importer.getSpToResolve()) {
        if (taxon.resolve(taxonomy).getType() == SightingTaxon.Type.SP) {
          spToResolve.add(taxon);
        }
      }
    } else {
      spToResolve = importer.getSpToResolve();
    }

    // If we found spToResolve, finish up by navigating to "resolveTaxa".
    // But HACK: this is broken for a narrow case (import of multiple eBird files
    // at one time), since resolveTaxa will just finish into the main menu,
    // not run importCompleteAction.  So skip sp-resolution in this case
    // (which should be irrelevant, seeing as eBird import should ~never
    // generate sps-to-resolve?)
    if (!spToResolve.isEmpty() && finishedAction == importCompleteAction) {
      reportSet.setSpsToResolve(ImmutableSet.copyOf(spToResolve), null /* no forced-resolve taxonomy */);
      // But don't do this on an initial run, see:
      // https://bitbucket.org/scythebill/scythebill-birdlist/issues/246/exception-when-importing-avisys-as-first
      // Instead, just save the "sps to resolve" and rely on the main reportset loading code to
      // navigate to "resolve taxa".
      // Also, don't bother if it's not a bird taxonomy - as resolveTaxa only supports birds!
      if (!initialRun && taxonomy.isBuiltIn()) {
        finishedAction = new AbstractAction() {
          @Override
          public void actionPerformed(ActionEvent e) {
            navigableFrame.navigateTo("resolveTaxa");
          }
        };
      }
    }
    
    Action cancelAction = new AbstractAction() {
      @Override public void actionPerformed(ActionEvent event) {
        reportSet.mutator().removing(finalImports).adding(finalSightingsToRemove).mutate();
        // This codepath will re-trigger "cleanUnneededLocations()", which is fine. 
        finishFailedOrCanceledImport();
        returnAction.actionPerformed(event);
      }
    };
    
    String message; 

    if (importer.getFailedSightingsCount() > 0) {
      File failedLinesFile = importer.writeFailedLines();
      int success = 
          importer.getSightingsFoundCount() - importer.getFailedSightingsCount() - droppedDuplicates;
      
      if (failedLinesFile != null) {
        message = alerts.getFormattedDialogMessageUnscaled(
            Name.IMPORT_PARTIALLY_SUCCESSFUL_TITLE,
            Name.IMPORT_PARTIALLY_SUCCESSFUL_WITH_FAILED_SIGHTINGS_FORMAT,
            success,
            importer.getSightingsFoundCount() - droppedDuplicates,
            failedLinesFile.getName());
      } else {
        message = alerts.getFormattedDialogMessageUnscaled(
            Name.IMPORT_PARTIALLY_SUCCESSFUL_TITLE,
            Name.IMPORT_PARTIALLY_SUCCESSFUL_FORMAT,
            success,
            importer.getSightingsFoundCount() - droppedDuplicates);
      }
    } else {
      if (importer.getSightingsFoundCount() == droppedDuplicates) {
        message = alerts.getFormattedDialogMessageUnscaled(
            Name.ALL_DUPLICATES_TITLE,
            Name.ALL_DUPLICATES_FORMAT,
            HtmlEscapers.htmlEscaper().escape(importer.importFileName()));
      } else {
        message = alerts.getFormattedDialogMessageUnscaled(
            Name.IMPORT_SUCCEEDED_TITLE,
            Name.IMPORT_SUCCEEDED_FORMAT,
            importer.getSightingsFoundCount() - droppedDuplicates,
            HtmlEscapers.htmlEscaper().escape(importer.importFileName()));
      }
    }

    if (importer.getSightingsMergedCount() > 0) {
      message += "<br><br>"
          + Messages.getFormattedMessage(Name.IMPORT_SIGHTINGS_MERGED, importer.getSightingsMergedCount());
    }
    
    if (importer.getSightingsEBirdSpuhsDroppedCount() > 0) {
      message += "<br><br>"
          + Messages.getFormattedMessage(Name.IMPORT_EBIRD_SPUHS_DROPPED, importer.getSightingsEBirdSpuhsDroppedCount());
      
    }

    ImportScanner importScanner = new ImportScanner();
    JScrollPane scrollPane = importScanner.scanForNewSighting(finalImports, importer.getTaxonomy());

    Box box = new Box(BoxLayout.PAGE_AXIS);
    JLabel label = new JLabel(message);
    label.setAlignmentX(0.0f);
    box.add(label);
    
    box.add(Box.createVerticalStrut(fontManager.scale(15)));
    scrollPane.setAlignmentX(0.0f);
    box.add(scrollPane);
    
    OkCancelPanel messagePanel = new OkCancelPanel(
        finishedAction,
        cancelAction,
        box,
        null);
    messagePanel.setMinimumSize(fontManager.scale(new Dimension(400, 300)));
    messagePanel.focusOnOk();
        
    navigableFrame.navigateTo(messagePanel);
  }

  private void finishFailedOrCanceledImport() {
    cleanUnneededLocations();
  }
  
  private void cleanUnneededLocations() {
    if (locationsBeforeImport == null) {
      return;
    }
    
    // Loop until no locations need cleaning
    while (true) {
      Set<String> locationsWithSightings =
          reportSet.getSightings().stream().filter(s -> s.getLocationId() != null)
              .map(Sighting::getLocationId).collect(Collectors.toSet());
      List<Location> locationsToDelete = new ArrayList<>();
      Locations.visitLocations(reportSet.getLocations(), l -> {
        // Look for locations that weren't present before the import, have no sightings, aren't built-in locations,
        // and have no children.
        if (locationsBeforeImport.contains(l.getId())
            || locationsWithSightings.contains(l.getId())
            || l.isBuiltInLocation()
            // Specifically, skip locations with children because it's possible multiple levels of hierarchy
            // will be trimmed.
            || !l.contents().isEmpty()) {
          return;
        }
        
        locationsToDelete.add(l);
      });
      
      if (locationsToDelete.isEmpty()) {
        return;
      }
      
      // Delete each location, then loop again as some locations might not have any children
      for (Location locationToDelete : locationsToDelete) {
        reportSet.deleteLocation(locationToDelete);
      }
    }
  }

  enum NewFor{
    world,
    country,
    state,
    county,
    site,
    year
  };
  
  class ImportScanner {
    private OptionalInt year;
    private Location countyLocation;
    private Location stateLocation;
    private Location countryLocation;
    private JEditorPane newText;
    private JScrollPane newScrollPane;
    private Location siteLocation;

    ImportScanner() {
      newText = new JEditorPane("text/html", "");
      newScrollPane = new JScrollPane(newText);
      newScrollPane.setVisible(false);
      newText.setEditable(false);
      newText.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, true);
    }

    JScrollPane scanForNewSighting(List<Sighting> finalImports, Taxonomy taxonomy) {
      if (!finalImports.isEmpty()) {
        ImmutableSet<String> locations = finalImports
            .stream()
            .filter(s -> s.getLocationId() != null)
            .map(Sighting::getLocationId)
            .collect(ImmutableSet.toImmutableSet());
        
        // Look for the root location - we'll only scan for "new for" within that. This isn't ideal for large imports,
        // but massively reduces the computational complexity and works quite well for small imports.
        Location rootLocation;
        if (locations.isEmpty()) {
          rootLocation = null;
        } else {
          LocationSet locationSet = reportSet.getLocations();
          rootLocation = locationSet.getLocation(locations.iterator().next());
          for (String locationId : Iterables.skip(locations, 1)) {
            Location location = locationSet.getLocation(locationId);
            rootLocation = Locations.getCommonAncestor(rootLocation, location);
            if (rootLocation == null) {
              break;
            }
          }
        }
        
        // Find the county, state, and country location (if any)
        List<Location> locationsToScan = new ArrayList<>();
        countryLocation = null;
        stateLocation = null;
        countyLocation = null;
        
        Location perhapsInterestingLocation = rootLocation;
        // Add the current location unless it's already going to be 
        // treated as a country/state/county
        if (rootLocation != null
            && rootLocation.getId() != null
            && rootLocation.getType() != Location.Type.country
            && rootLocation.getType() != Location.Type.state
            && rootLocation.getType() != Location.Type.county) {
          locationsToScan.add(rootLocation);
          siteLocation = rootLocation;
          perhapsInterestingLocation = rootLocation.getParent();
        }
        
        while (perhapsInterestingLocation != null) {
          if (perhapsInterestingLocation.getType() != null) {
            switch (perhapsInterestingLocation.getType()) {
              case country:
                if (countryLocation == null) {
                  countryLocation = perhapsInterestingLocation;
                  locationsToScan.add(perhapsInterestingLocation);
                }
                break;
              case state:
                if (stateLocation == null) {
                  stateLocation = perhapsInterestingLocation;
                  locationsToScan.add(perhapsInterestingLocation);
                }
                break;
              case county:
                if (countyLocation == null) {
                  countyLocation = perhapsInterestingLocation;
                  locationsToScan.add(perhapsInterestingLocation);
                }
                break;
              default:
                break;
            }
          }
          perhapsInterestingLocation = perhapsInterestingLocation.getParent();
        }
        locationsToScan.add(null);

        // Always use *today's* year as the "new for" year.  So, yes, when importing
        // old lists, you won't find out if they were new for that year list.  But
        // if you import Dec->Jan, you'll see everything new for the current year,
        // which is more likely.
        year = OptionalInt.of(LocalDate.now(GJChronology.getInstance()).getYear());

        ImmutableSet<Sighting> importedSightingSet = ImmutableSet.copyOf(finalImports);
        Predicate<Sighting> sightingsToInclude = Predicates.and( 
            s -> !importedSightingSet.contains(s),
            queryPreferences.getCountablePredicate(taxonomy, false, null));
        
        Set<User> users = new LinkedHashSet<>();
        for (Sighting sighting : finalImports) {
          if (sighting.hasSightingInfo()) {
            users.addAll(sighting.getSightingInfo().getUsers());
          }
        }
        ImmutableSet<User> immutableUsers = ImmutableSet.copyOf(users);
        
        if (!users.isEmpty()) {
          sightingsToInclude = Predicates.and(
              sightingsToInclude,
              SightingPredicates.includesAnyOfUsers(immutableUsers));
        }
        ScanSeenTaxa.start(
            reportSet, taxonomy, executorService, 
            scanSeenTaxa -> showScanResults(scanSeenTaxa, finalImports, taxonomy), locationsToScan, sightingsToInclude,
            new ScanSeenTaxa.Options().setYearToScan(year).setUsers(immutableUsers));
      }      
      
      return newScrollPane;
    }

    private void showScanResults(ScanSeenTaxa scannedSeenTaxa, List<Sighting> finalImports, Taxonomy taxonomy) {
      ResolvedComparator resolvedComparator = new ResolvedComparator();
      Multimap<NewFor, Resolved> scanResults =
          Multimaps.newSortedSetMultimap(new TreeMap<>(), () -> new TreeSet<>(resolvedComparator));
              
      Set<String> siteSeenTaxa = null;
      if (siteLocation != null && scannedSeenTaxa.getVisitCount(siteLocation) >= 3) {
        siteSeenTaxa = scannedSeenTaxa.getSeenTaxa(taxonomy, siteLocation); 
      }
      Set<String> countySeenTaxa =
          countyLocation == null ? null : scannedSeenTaxa.getSeenTaxa(taxonomy, countyLocation);
      Set<String> stateSeenTaxa =
          stateLocation == null ? null : scannedSeenTaxa.getSeenTaxa(taxonomy, stateLocation);
      Set<String> countrySeenTaxa =
          countryLocation == null ? null : scannedSeenTaxa.getSeenTaxa(taxonomy, countryLocation);
      Set<String> worldSeenTaxa = scannedSeenTaxa.getSeenTaxa(taxonomy, null);
      Set<String> yearSeenTaxa =
          !year.isPresent() ? null : scannedSeenTaxa.getYearTaxa(taxonomy, year.getAsInt());
      
      Predicate<Sighting> countable = queryPreferences.getCountablePredicate(taxonomy, false, null);
      for (Sighting sighting : finalImports) {
        // Ignore uncountable sightings
        if (!countable.apply(sighting)) {
          continue;
        }

        Resolved resolved = sighting.getTaxon().resolve(taxonomy);
        if ((resolved.getType() == SightingTaxon.Type.SINGLE
            || resolved.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES)) {
          String taxonId;
          if (resolved.getSmallestTaxonType() != Taxon.Type.species) {
            taxonId = resolved.getParentOfAtLeastType(Taxon.Type.species).getId();
          } else {
            taxonId = resolved.getSightingTaxon().getId();
          }
          
          if (worldSeenTaxa != null && !worldSeenTaxa.contains(taxonId)) {
            scanResults.put(NewFor.world, SightingTaxons.newResolved(taxonomy.getTaxon(taxonId)));
          } else if (countrySeenTaxa != null && !countrySeenTaxa.contains(taxonId)) {
            scanResults.put(NewFor.country, SightingTaxons.newResolved(taxonomy.getTaxon(taxonId)));
          } else if (stateSeenTaxa != null && !stateSeenTaxa.contains(taxonId)) {
            scanResults.put(NewFor.state, SightingTaxons.newResolved(taxonomy.getTaxon(taxonId)));
          } else if (countySeenTaxa != null && !countySeenTaxa.contains(taxonId)) {
            scanResults.put(NewFor.county, SightingTaxons.newResolved(taxonomy.getTaxon(taxonId)));
          } else if (siteSeenTaxa != null && !siteSeenTaxa.contains(taxonId)) {
            scanResults.put(NewFor.site, SightingTaxons.newResolved(taxonomy.getTaxon(taxonId)));
          }
          
          // Always scan the year taxa
          if (yearSeenTaxa != null && !yearSeenTaxa.contains(taxonId)) {
            // But only count sightings from this year.
            if (year.isPresent()
                && sighting.getDateAsPartial() != null
                && sighting.getDateAsPartial().isSupported(DateTimeFieldType.year())
                && sighting.getDateAsPartial().get(DateTimeFieldType.year()) == year.getAsInt()) {
              scanResults.put(NewFor.year, SightingTaxons.newResolved(taxonomy.getTaxon(taxonId)));
            }
          }
        }
      }
      
      if (!scanResults.isEmpty()) {
        // If there's any new entries just for the *site*, then also include any entries for higher-level locations.
        // And ditto for the rest.
        if (!scanResults.get(NewFor.site).isEmpty()) {
          scanResults.putAll(NewFor.site, scanResults.get(NewFor.county));
          scanResults.putAll(NewFor.site, scanResults.get(NewFor.state));
          scanResults.putAll(NewFor.site, scanResults.get(NewFor.country));
          scanResults.putAll(NewFor.site, scanResults.get(NewFor.world));
        }
        if (!scanResults.get(NewFor.county).isEmpty()) {
          scanResults.putAll(NewFor.county, scanResults.get(NewFor.state));
          scanResults.putAll(NewFor.county, scanResults.get(NewFor.country));
          scanResults.putAll(NewFor.county, scanResults.get(NewFor.world));
        }
        if (!scanResults.get(NewFor.state).isEmpty()) {
          scanResults.putAll(NewFor.state, scanResults.get(NewFor.country));
          scanResults.putAll(NewFor.state, scanResults.get(NewFor.world));
        }
        if (!scanResults.get(NewFor.country).isEmpty()) {
          scanResults.putAll(NewFor.country, scanResults.get(NewFor.world));
        }
        
        StringBuilder text = new StringBuilder();
        
        text.append(newForText(scanResults, NewFor.world));
        text.append(newForText(scanResults, NewFor.country));
        text.append(newForText(scanResults, NewFor.state));
        text.append(newForText(scanResults, NewFor.county));
        text.append(newForText(scanResults, NewFor.site));
        text.append(newForText(scanResults, NewFor.year));

        newText.setText(text.toString());
        newScrollPane.setVisible(true);
        newScrollPane.getParent().revalidate();
      }
    }

    private String newForText(Multimap<NewFor, Resolved> scanResults, NewFor newFor) {
      Collection<Resolved> newForSpecies = scanResults.get(newFor);
      if (newForSpecies.isEmpty()) {
        return "";
      }
      
      Function<Resolved, String> toString;
      switch (namesPreferences.scientificOrCommon) {
        case COMMON_FIRST:
        case COMMON_ONLY:
          toString = Resolved::getCommonName;
          break;
        case SCIENTIFIC_FIRST:
        case SCIENTIFIC_ONLY:
          toString = Resolved::getName;
          break;
        default:
          throw new AssertionError("Unexpected scientificOrCommon: " + namesPreferences.scientificOrCommon);
      }

      StringBuilder text = new StringBuilder();
      if (newForSpecies.size() > 30) {
        Name format;
        String forString;
        switch (newFor) {
          case world:
            format = Name.IMPORT_NEW_FOR_WORLD_SPECIES_COUNT;
            forString = "";
            break;
          case country:
            format = Name.IMPORT_NEW_FOR_LOCATION_SPECIES_COUNT;
            forString = countryLocation.getDisplayName();
            break;
          case state:
            format = Name.IMPORT_NEW_FOR_LOCATION_SPECIES_COUNT;
            forString = stateLocation.getDisplayName();
            break;
          case county:
            format = Name.IMPORT_NEW_FOR_LOCATION_SPECIES_COUNT;
            forString = countyLocation.getDisplayName();
            break;
          case site:
            format = Name.IMPORT_NEW_FOR_LOCATION_SPECIES_COUNT;
            forString = siteLocation.getDisplayName();
            break;
          case year:
            format = Name.IMPORT_NEW_FOR_YEAR_SPECIES_COUNT;
            forString = "";
            break;
          default:
            throw new AssertionError("Unexpected newFor " + newFor);              
        }
        
        text.append(Messages.getFormattedMessage(format, newForSpecies.size(), forString));
      } else if (newForSpecies.size() == 1) {
        Name format;
        String forString;
        switch (newFor) {
          case world:
            format = Name.IMPORT_NEW_FOR_WORLD_SPECIES_NAME;
            forString = "";
            break;
          case country:
            format = Name.IMPORT_NEW_FOR_LOCATION_SPECIES_NAME;
            forString = countryLocation.getDisplayName();
            break;
          case state:
            format = Name.IMPORT_NEW_FOR_LOCATION_SPECIES_NAME;
            forString = stateLocation.getDisplayName();
            break;
          case county:
            format = Name.IMPORT_NEW_FOR_LOCATION_SPECIES_NAME;
            forString = countyLocation.getDisplayName();
            break;
          case site:
            format = Name.IMPORT_NEW_FOR_LOCATION_SPECIES_NAME;
            forString = siteLocation.getDisplayName();
            break;
          case year:
            format = Name.IMPORT_NEW_FOR_YEAR_SPECIES_NAME;
            forString = "";
            break;
          default:
            throw new AssertionError("Unexpected newFor " + newFor);              
        }
        
        text.append(
            Messages.getFormattedMessage(
                format, toString.apply(newForSpecies.iterator().next()), forString));
      } else {
        Name format;
        String forString;
        switch (newFor) {
          case world:
            format = Name.IMPORT_NEW_FOR_WORLD_SPECIES_COUNT_AND_NAMES;
            forString = "";
            break;
          case country:
            format = Name.IMPORT_NEW_FOR_LOCATION_SPECIES_COUNT_AND_NAMES;
            forString = countryLocation.getDisplayName();
            break;
          case state:
            format = Name.IMPORT_NEW_FOR_LOCATION_SPECIES_COUNT_AND_NAMES;
            forString = stateLocation.getDisplayName();
            break;
          case county:
            format = Name.IMPORT_NEW_FOR_LOCATION_SPECIES_COUNT_AND_NAMES;
            forString = countyLocation.getDisplayName();
            break;
          case site:
            format = Name.IMPORT_NEW_FOR_LOCATION_SPECIES_COUNT_AND_NAMES;
            forString = siteLocation.getDisplayName();
            break;
          case year:
            format = Name.IMPORT_NEW_FOR_YEAR_SPECIES_COUNT_AND_NAMES;
            forString = "";
            break;
          default:
            throw new AssertionError("Unexpected newFor " + newFor);              
        }
        
        text.append(
            Messages.getFormattedMessage(
                format,
                newForSpecies.size(),
                newForSpecies.stream()
                    .map(toString)
                    .collect(Collectors.joining(", ")),
                forString));
      }
      text.append("<p>");
      return text.toString();
    }
  }
  
  private Set<VisitInfoKey> extractVisitInfoKeys(List<Sighting> imports) {
    Set<VisitInfoKey> visitInfoKeys = Sets.newLinkedHashSet();
    for (Sighting sighting : imports) {
      VisitInfoKey visitInfoKey = VisitInfoKey.forSighting(sighting);
      if (visitInfoKey != null) {
        visitInfoKeys.add(visitInfoKey);
      }
    }
    return visitInfoKeys;
  }

  private enum SightingFunnel implements Funnel<Sighting> {
    INSTANCE;

    @Override
    public void funnel(Sighting from, PrimitiveSink into) {
      ReadablePartial dateAsPartial = from.getDateAsPartial();
      if (dateAsPartial != null) {
        funnelField(dateAsPartial, DateTimeFieldType.dayOfMonth(), into); 
        funnelField(dateAsPartial, DateTimeFieldType.monthOfYear(), into); 
        funnelField(dateAsPartial, DateTimeFieldType.year(), into); 
      } else {
        into.putInt(-1);
        into.putInt(-1);
        into.putInt(-1);
      }
      ReadablePartial time = from.getTimeAsPartial();
      if (time != null) {
        funnelField(time, DateTimeFieldType.hourOfDay(), into); 
        funnelField(time, DateTimeFieldType.minuteOfHour(), into);         
      } else {
        into.putInt(-1);        
        into.putInt(-1);
      }
      
      into.putUnencodedChars(from.getTaxonomy().getId());
      into.putInt(from.getTaxon().getType().ordinal());
      for (String id : from.getTaxon().getIds()) {
        into.putUnencodedChars(id);
      }
    }
    
    private void funnelField(ReadablePartial partial, DateTimeFieldType fieldType,
        PrimitiveSink into) {
      if (partial.isSupported(fieldType)) {
        into.putInt(partial.get(fieldType));
      } else {
        into.putInt(-1);
      }
    }
  }
  
  private static final double FALSE_POSITIVES = 0.0001;
  private static final long MAX_IMPORT_TYPES = 4;
  
  private int countDuplicates(List<Sighting> imports, SightingFunnel sightingFunnel) {
    if (reportSet.getSightings().isEmpty()) {
      return 0;
    }
    
    BloomFilter<Sighting> filter = BloomFilter.create(
        sightingFunnel, reportSet.getSightings().size() + imports.size(), FALSE_POSITIVES);
    for (Sighting sighting : reportSet.getSightings()) {
      filter.put(sighting);
    }
    
    int count = 0;
    for (Sighting importedSighting : imports) {
      if (filter.mightContain(importedSighting)) {
        count++;
      }
    }
    
    return count;
  }

  /**
   * A key with just sighting fields necessary to establish duplicates.
   * Specifically, not location.
   * 
   * This may seem puzzling - how can it be different if it's in a different location?
   * Well, for single-observer datasets, this makes lots of sense - you can't be in two
   * places at the same time.  Buuuuut for multiple users, it does not.  At least, not
   * necessarily.  
   */
  static final class DuplicateSightingKey {
    private final ReadablePartial date;
    private final LocalTime startTime;
    private final String taxonomyId;
    private final int hashCode;
    private final String locationId;

    private DuplicateSightingKey(
        ReadablePartial date, LocalTime startTime, String taxonomyId, String locationId) {
      this.taxonomyId = taxonomyId;
      this.date = Preconditions.checkNotNull(date);
      this.startTime = startTime;
      this.locationId = locationId;
      this.hashCode =
          ((((date.hashCode() * 31) + (startTime == null ? 0 : startTime.hashCode()) * 31)
              + (locationId == null ? 0 : locationId.hashCode())) * 31) + taxonomyId.hashCode();
    }

    @Override
    public int hashCode() {
      return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      
      if (!(obj instanceof DuplicateSightingKey)) {
        return false;
      }
     
      DuplicateSightingKey that = (DuplicateSightingKey) obj;
      return this.hashCode == that.hashCode
          && this.date.equals(that.date)
          && this.taxonomyId.equals(that.taxonomyId)
          && Objects.equal(this.locationId, that.locationId)
          && Objects.equal(this.startTime, that.startTime);
    }
    
    static DuplicateSightingKey forSighting(Sighting sighting) {
      if (sighting.getDateAsPartial() == null) {
        return null;
      }
      return new DuplicateSightingKey(
          sighting.getDateAsPartial(), sighting.getTimeAsPartial(), sighting.getTaxonomy().getId(), null);
    }

    static DuplicateSightingKey forSightingWithLocation(Sighting sighting) {
      if (sighting.getDateAsPartial() == null) {
        return null;
      }
      return new DuplicateSightingKey(
          sighting.getDateAsPartial(), sighting.getTimeAsPartial(), sighting.getTaxonomy().getId(), sighting.getLocationId());
    }
}
  
  /**
   * Remove duplicates from an import.
   */
  private List<Sighting> dropDuplicates(
      SightingsImporter<?> importer, List<Sighting> imports, Function<Sighting, DuplicateSightingKey> duplicateStrategy) {
    // Find all DateAndTimeKeys that *could* overlap, so most existing sightings can be ignored.
    Set<DuplicateSightingKey> importedDateAndTimeKeys = Sets.newHashSet();
    for (Sighting importedSighting : imports) {
      DuplicateSightingKey dateAndTimeKey = duplicateStrategy.apply(importedSighting);
      if (dateAndTimeKey != null) {
        importedDateAndTimeKeys.add(dateAndTimeKey);
      }
    }
    
    Function<SightingTaxon, SightingTaxon> taxonMapper;
    Taxonomy taxonomy = importer.getTaxonomy();
    if (taxonomy instanceof MappedTaxonomy) {
      taxonMapper = taxon -> taxon.resolve(taxonomy).getSightingTaxon();
    } else {
      taxonMapper = Function.identity();
    }

    // Now, gather all existing sightings.
    Multimap<DuplicateSightingKey, SightingTaxon> existingSightings = HashMultimap.create();
    for (Sighting sighting : reportSet.getSightings(taxonomy)) {
      DuplicateSightingKey dateAndTimeKey = duplicateStrategy.apply(sighting);
      if (dateAndTimeKey != null
          && importedDateAndTimeKeys.contains(dateAndTimeKey)) {
        existingSightings.put(dateAndTimeKey, taxonMapper.apply(sighting.getTaxon()));
      }
    }
    
    List<Sighting> notDuplicates = new ArrayList<Sighting>(imports.size());
    for (Sighting importedSighting : imports) {
      DuplicateSightingKey dateAndTimeKey = duplicateStrategy.apply(importedSighting);
      // Include anything that is missing a location
      if (dateAndTimeKey == null) {
        notDuplicates.add(importedSighting);
      } else {
        // As well as anything that doesn't match to an existing sighting
        if (!existingSightings.containsEntry(dateAndTimeKey,
            taxonMapper.apply(importedSighting.getTaxon()))) {
          notDuplicates.add(importedSighting);
        }
      }
    }
    return notDuplicates;
  }

  /**
   * Finds existing sightings that are a duplicate of the new import.
   */
  private List<Sighting> findExistingDuplicates(List<Sighting> imports, Function<Sighting, DuplicateSightingKey> duplicateStrategy) {
    // Find all DateAndTimeKeys that *could* overlap, so most existing sightings can be ignored.
    Set<DuplicateSightingKey> importedDateAndTimeKeys = Sets.newHashSet();
    // And find all sighting taxa for each key
    Multimap<DuplicateSightingKey, SightingTaxon> importedSightings = HashMultimap.create();
    for (Sighting importedSighting : imports) {
      DuplicateSightingKey dateAndTimeKey = duplicateStrategy.apply(importedSighting);
      if (dateAndTimeKey != null) {
        importedDateAndTimeKeys.add(dateAndTimeKey);
        importedSightings.put(dateAndTimeKey, importedSighting.getTaxon());
      }
    }
        
    List<Sighting> existingDuplicates = new ArrayList<>(imports.size());
    for (Sighting sighting : reportSet.getSightings()) {
      DuplicateSightingKey dateAndTimeKey = duplicateStrategy.apply(sighting);
      if (importedSightings.containsEntry(dateAndTimeKey, sighting.getTaxon())) {
        existingDuplicates.add(sighting);
      }
    }
    return existingDuplicates;
  }

  static class UserOverlapStats {
    int sameUsers;
    int existingIsSuperset;
    int existingIsSubset;
    int differentUsers;
    
    public boolean actuallyNoDuplicates() {
      return totalOverlap() == 0;
    }
    
    
    /**
     * Returns true if all duplicates had the same users or a subset of what was in the existing.
     */
    public boolean existingUsersAreTheSameOrSuperset() {
      return existingIsSubset == 0 && differentUsers == 0;
    }
    
    public int totalOverlap() {
      return sameUsers + existingIsSuperset + existingIsSubset + differentUsers;
    }
  }
  
  /**
   * Gather statistics for *how* different the user data is between the imports and existing sightings.
   * As written, this method *does* consider sightings to overlap even if they are in different locations.
   */
  private UserOverlapStats getUserOverlapStats(List<Sighting> imports, Function<Sighting, DuplicateSightingKey> keyCreator) {
    // Find all DateAndTimeKeys that *could* overlap, so most existing sightings can be ignored.
    Set<DuplicateSightingKey> importedDateAndTimeKeys = Sets.newHashSet();
    for (Sighting importedSighting : imports) {
      DuplicateSightingKey dateAndTimeKey = keyCreator.apply(importedSighting);
      if (dateAndTimeKey != null) {
        importedDateAndTimeKeys.add(dateAndTimeKey);
      }
    }
    
    // Now, gather all existing sightings that could possibly overlap.
    HashBasedTable<DuplicateSightingKey, SightingTaxon, Sighting> existingSightings = HashBasedTable.create();
    for (Sighting sighting : reportSet.getSightings()) {
      DuplicateSightingKey dateAndTimeKey = keyCreator.apply(sighting);
      if (dateAndTimeKey != null
          && importedDateAndTimeKeys.contains(dateAndTimeKey)) {
        existingSightings.put(dateAndTimeKey, sighting.getTaxon(), sighting);
      }
    }
    
    UserOverlapStats overlapStats = new UserOverlapStats();
    
    for (Sighting importedSighting : imports) {
      DuplicateSightingKey dateAndTimeKey = keyCreator.apply(importedSighting);
      if (dateAndTimeKey != null) {
        Sighting existingSighting = existingSightings.get(dateAndTimeKey, importedSighting.getTaxon());
        if (existingSighting != null) {
          ImmutableSet<User> existingUsers = existingSighting.getSightingInfo().getUsers();
          ImmutableSet<User> importedUsers = importedSighting.getSightingInfo().getUsers();
          if (existingUsers.equals(importedUsers)) {
            overlapStats.sameUsers++;
          } else if (existingUsers.containsAll(importedUsers)) {
            overlapStats.existingIsSuperset++;
          } else if (importedUsers.containsAll(existingUsers)) {
            overlapStats.existingIsSubset++;
          } else {
            overlapStats.differentUsers++;
          }
        }
      }
    }
    
    return overlapStats;
  }
  
  /** Data class to encapsulate the result of merging users. */
  static class ModifiedImportsAndSightingsToRemove {
    List<Sighting> modifiedImports;
    List<Sighting> sightingsToRemove;
  }
  
  private ModifiedImportsAndSightingsToRemove mergeObservers(
      List<Sighting> imports, Function<Sighting, DuplicateSightingKey> duplicateStrategy) {
    // Find all DateAndTimeKeys that *could* overlap, so most existing sightings can be ignored.
    Set<DuplicateSightingKey> importedDateAndTimeKeys = Sets.newHashSet();
    for (Sighting importedSighting : imports) {
      DuplicateSightingKey dateAndTimeKey = duplicateStrategy.apply(importedSighting);
      if (dateAndTimeKey != null) {
        importedDateAndTimeKeys.add(dateAndTimeKey);
      }
    }
    
    // Now, gather all existing sightings that could possibly overlap.
    HashBasedTable<DuplicateSightingKey, SightingTaxon, Sighting> existingSightings = HashBasedTable.create();
    for (Sighting sighting : reportSet.getSightings()) {
      DuplicateSightingKey dateAndTimeKey = duplicateStrategy.apply(sighting);
      if (dateAndTimeKey != null
          && importedDateAndTimeKeys.contains(dateAndTimeKey)) {
        existingSightings.put(dateAndTimeKey, sighting.getTaxon(), sighting);
      }
    }
    
    ModifiedImportsAndSightingsToRemove merged = new ModifiedImportsAndSightingsToRemove();
    merged.modifiedImports = new ArrayList<Sighting>(imports.size());
    merged.sightingsToRemove = new ArrayList<Sighting>(imports.size());
    for (Sighting importedSighting : imports) {
      DuplicateSightingKey dateAndTimeKey = duplicateStrategy.apply(importedSighting);
      // Include anything that is missing a location or date
      if (dateAndTimeKey == null) {
        merged.modifiedImports.add(importedSighting);
      } else {
        Sighting sighting = existingSightings.get(dateAndTimeKey, importedSighting.getTaxon());
        if (sighting == null) {
          // Not a duplicate: import
          merged.modifiedImports.add(importedSighting);
        } else if (!sighting.getSightingInfo().getUsers().equals(importedSighting.getSightingInfo().getUsers())) {
          // Merge the users into the original sighting, and treat that as imported.
          Sighting.Builder mergedSighting = sighting.asBuilder();
          mergedSighting.getSightingInfo().setUsers(
              Sets.union(sighting.getSightingInfo().getUsers(), importedSighting.getSightingInfo().getUsers()));
          merged.modifiedImports.add(mergedSighting.build());
          // And remove the original unmodified sighting.
          merged.sightingsToRemove.add(sighting);
        }
        // else it is a duplicate, and the users are the same: drop from the import.
      }
    }
    
    
    return merged;
    
  }
  
  @Override
  public void fontsUpdated(FontManager fontManager) {
    centerBox.setPreferredSize(fontManager.scale(new Dimension(700, 520)));
    centerBox.setMinimumSize(centerBox.getPreferredSize());
    centerBox.setBorder(new EmptyBorder(
        fontManager.scale(20),
        fontManager.scale(60),
        fontManager.scale(20),
        fontManager.scale(60)));

    GroupLayout layout = new GroupLayout(centerBox);
    centerBox.setLayout(layout);
    
    List<ImportType> usedImportTypes = preferredImportTypes();
    
    List<JComponent> buttons = new ArrayList<>(); 
    ParallelGroup horizontalImportLabels = layout.createParallelGroup();
    ParallelGroup horizontalImportButtons = layout.createParallelGroup();
    
    SequentialGroup verticalGroup = layout.createSequentialGroup();
    verticalGroup
        .addComponent(importLabel)
        .addGap(reportSet.getSightings().isEmpty() ? 0 : fontManager.scale(24));
    
    for (ImportType importType : usedImportTypes) {
      JLabel label;
      JButton button;
      switch (importType) {
        case AVISYS:
          label = avisysLabel;
          button = importAvisys;
          break;
        case BIRDBASE:
          label = birdbaseLabel;
          button = importBirdbase;
          break;
        case BIRD_BRAIN:
          label = birdBrainLabel;
          button = importBirdBrain;
          break;
        case BIRD_TRACK:
          label = birdTrackLabel;
          button = importBirdTrack;
          break;
        case BIRDERS_DIARY:
          label = birdersDiaryLabel;
          button = importBirdersDiary;
          break;
        case BIRDLASSER:
          label = birdLasserLabel;
          button = importBirdLasser;
          break;
        case BIRDA:
          label = birdaLabel;
          button = importBirda;
          break;
        case EBIRD:
          label = ebirdLabel;
          button = importEbird;
          break;
        case FLICKR:
          label = flickrLabel;
          button = importFlickr;
          break;
        case HBW_ALIVE:
          label = hbwAliveLabel;
          button = importHbwAlive;
          break;
        case INATURALIST:
          label = iNaturalistLabel;
          button = importINaturalist;
          break;
        case OBSERVADO:
          label = observationOrgLabel;
          button = importObservationOrg;
          break;
        case ORNITHO:
          label = ornithoLabel;
          button = importOrnitho;
          break;
        case SCYTHEBILL:
          label = scythebillLabel;
          button = importScythebill;
          break;
        case WILDLIFE_RECORDER:
          label = wildlifeRecorderLabel;
          button = importWildlifeRecorder;
          break;
        case WINGS:
          label = wingsLabel;
          button = importWings;
          break;
        default:
          throw new IllegalStateException("No UI added for type " + importType);
      }
      
      horizontalImportButtons.addComponent(button, Alignment.LEADING,
          PREFERRED_SIZE, fontManager.scale(224), PREFERRED_SIZE);
      horizontalImportLabels.addComponent(label);
      
      verticalGroup.addGroup(layout.createBaselineGroup(false, false)
          .addComponent(button, Alignment.BASELINE,
              PREFERRED_SIZE, fontManager.scale(52), PREFERRED_SIZE)
          .addComponent(label, Alignment.BASELINE, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE));
      verticalGroup.addPreferredGap(ComponentPlacement.RELATED);

      buttons.add(button);
    }

    horizontalImportButtons.addComponent(importMenuComboBox);
    horizontalImportLabels.addComponent(importMenuLabel);
    
    verticalGroup.addGroup(layout.createBaselineGroup(false, false)
        .addComponent(importMenuComboBox, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
        .addComponent(importMenuLabel));

    verticalGroup.addGap(24);

    horizontalImportButtons.addComponent(returnButton, Alignment.LEADING,
                    PREFERRED_SIZE, fontManager.scale(224), PREFERRED_SIZE);
    horizontalImportLabels.addComponent(emptyLabel);
    buttons.add(returnButton);
        
    verticalGroup
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(returnButton, Alignment.BASELINE,
                PREFERRED_SIZE, fontManager.scale(52), PREFERRED_SIZE)
            .addComponent(emptyLabel))
        .addContainerGap(82, 82);
    
    layout.setHorizontalGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(layout.createParallelGroup()
          .addComponent(importLabel)
          .addGroup(layout.createSequentialGroup()
            .addGroup(horizontalImportButtons)
            .addGap(63)
            .addGroup(horizontalImportLabels)))
        .addContainerGap(67, 67));

    layout.setVerticalGroup(verticalGroup);
    layout.linkSize(buttons.toArray(new JComponent[buttons.size()]));
    layout.linkSize(SwingConstants.HORIZONTAL, importMenuComboBox, buttons.get(0));
  }

  private List<ImportType> preferredImportTypes() {
    List<ImportType> usedImportTypes = importPreferences.recentlyUsedImports().stream()
        .limit(MAX_IMPORT_TYPES).collect(Collectors.toCollection(ArrayList::new));
    // Start with eBird and Scythebill in the list
    if (usedImportTypes.size() < MAX_IMPORT_TYPES && !usedImportTypes.contains(ImportType.EBIRD)) {
      usedImportTypes.add(ImportType.EBIRD);
    }
    if (usedImportTypes.size() < MAX_IMPORT_TYPES && !usedImportTypes.contains(ImportType.SCYTHEBILL)) {
      usedImportTypes.add(ImportType.SCYTHEBILL);
    }
    return usedImportTypes;
  }
  
  @Subscribe
  public void taxonomyChanged(TaxonomyChangedEvent event) {
    updateUiForTaxonomy();
  }
  
  private void updateUiForTaxonomy() {
    enableTaxonomy(importAvisys, ImportType.AVISYS);
    enableTaxonomy(importBirdBrain, ImportType.BIRD_BRAIN);
    enableTaxonomy(importBirdLasser, ImportType.BIRDLASSER);
    enableTaxonomy(importBirdTrack, ImportType.BIRD_TRACK);
    enableTaxonomy(importBirdbase, ImportType.BIRDBASE);
    enableTaxonomy(importBirda, ImportType.BIRDA);
    enableTaxonomy(importBirdersDiary, ImportType.BIRDERS_DIARY);
    enableTaxonomy(importEbird, ImportType.EBIRD);
    enableTaxonomy(importFlickr, ImportType.FLICKR);
    enableTaxonomy(importHbwAlive, ImportType.HBW_ALIVE);
    enableTaxonomy(importINaturalist, ImportType.INATURALIST);
    enableTaxonomy(importObservationOrg, ImportType.OBSERVADO);
    enableTaxonomy(importOrnitho, ImportType.ORNITHO);
    enableTaxonomy(importScythebill, ImportType.SCYTHEBILL);
    enableTaxonomy(importWildlifeRecorder, ImportType.WILDLIFE_RECORDER);
    enableTaxonomy(importWings, ImportType.WINGS);
    
    List<Object> comboBoxItems = new ArrayList<>();
    comboBoxItems.add(Messages.getMessage(Name.IMPORT_FROM));
    List<ImportType> importTypes;
    if (taxonomyStore.isBirdTaxonomy()) {
      importTypes = Lists.newArrayList(ImportType.values());
      if (!reportSet.getSightings().isEmpty()) {
        importLabel.setText("<html>" + Messages.getMessage(Name.CHOOSE_AN_IMPORT_FORMAT));
      }
    } else {
      // Only use importTypes that support non-avian taxonomies
      importTypes = Arrays.stream(ImportType.values()).filter(ImportType::supportsNonAvianTaxa)
          .collect(Collectors.toCollection(ArrayList::new));
      
      if (!reportSet.getSightings().isEmpty()) {
        importLabel.setText("<html>"
            + Messages.getMessage(Name.CHOOSE_AN_IMPORT_FORMAT_ONLY_NON_BIRD));
      } else {
        importLabel.setText("<html>"
            + Messages.getMessage(Name.CHOOSE_AN_IMPORT_FORMAT_ONLY_NON_BIRD_EMPTY_FILE));
      }
    }
    
    // Remove everything that has its own button.
    importTypes.removeAll(preferredImportTypes());
    comboBoxItems.addAll(importTypes);
    importMenuComboBox.setModel(new DefaultComboBoxModel<>(comboBoxItems.toArray()));
    importMenuComboBox.setMaximumRowCount(comboBoxItems.size());
    
    // Hide the additional content if there's no other valid import types that aren't already visible
    importMenuComboBox.setVisible(!importTypes.isEmpty());
    importMenuLabel.setVisible(!importTypes.isEmpty());
  }

  private void enableTaxonomy(JButton importButton, ImportType importType) {
    importButton.setEnabled(importType.supportsNonAvianTaxa() || taxonomyStore.isBirdTaxonomy());
    
  }

  @Override
  public String getTitle() {
    return Messages.getMessage(Name.IMPORT_SIGHTINGS);
  }
}

