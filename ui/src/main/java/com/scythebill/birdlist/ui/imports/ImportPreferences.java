/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.scythebill.birdlist.model.annotations.Preference;
import com.scythebill.birdlist.model.annotations.SerializeAsJson;
import com.scythebill.birdlist.ui.messages.Messages;

/**
 * Stores preferences about imports.
 */
@Singleton
public class ImportPreferences {
  public enum ImportType {
    EBIRD("eBird", Messages.Name.CSV_EXPORT_FROM_FORMAT, true),
    SCYTHEBILL("Scythebill", Messages.Name.CSV_EXPORT_FROM_FORMAT, true),
    AVISYS("Avisys", Messages.Name.CSV_EXPORT_FROM_FORMAT, true),
    BIRDBASE("Birdbase", Messages.Name.CSV_EXPORT_FROM_FORMAT, true),
    BIRD_BRAIN("Bird Brain", Messages.Name.CSV_EXPORT_FROM_FORMAT),
    BIRD_TRACK("BirdTrack", Messages.Name.XLSX_EXPORT_FROM_FORMAT, true),
    BIRDERS_DIARY("Birder's Diary", Messages.Name.CSV_EXPORT_FROM_FORMAT, true),
    BIRDLASSER("BirdLasser", Messages.Name.CSV_EXPORT_FROM_FORMAT),
    BIRDA("Birda", Messages.Name.CSV_EXPORT_FROM_FORMAT, false),
    FLICKR("Flickr Albums", Messages.Name.FLICKR_ALBUM, true),
    HBW_ALIVE("HBW Alive", Messages.Name.XLS_EXPORT_FROM_FORMAT),
    INATURALIST("iNaturalist", Messages.Name.CSV_EXPORT_FROM_FORMAT, true),
    OBSERVADO("Observado/Waarnemingen", Messages.Name.CSV_EXPORT_FROM_FORMAT, true),
    ORNITHO("Ornitho", Messages.Name.TXT_EXPORT_FROM_FORMAT),
    WILDLIFE_RECORDER("Wildlife Recorder", Messages.Name.CSV_EXPORT_FROM_FORMAT, true),
    WINGS("Wings", Messages.Name.XML_EXPORTS_FROM_FORMAT);
    
    private final String importName;
    private final Messages.Name documentationFormat;
    private final boolean supportsNonAvianTaxa;
    
    private ImportType(String importName, Messages.Name documentationFormat) {
      this(importName, documentationFormat, false);
    }

    private ImportType(String importName, Messages.Name documentationFormat,
        boolean supportsNonAvianTaxa) {
      this.importName = importName;
      this.documentationFormat = documentationFormat;
      this.supportsNonAvianTaxa = supportsNonAvianTaxa;
    }

    public String importName() {
      return importName;
    }

    public String documentation() {
      return Messages.getFormattedMessage(documentationFormat, importName);
    }
    
    public boolean supportsNonAvianTaxa() {
      return supportsNonAvianTaxa;
    }

    @Override
    public String toString() {
      return importName();
    }
  }
  
  private static ImmutableMap<String, ImportType> STRING_TO_TYPE = 
      Arrays.stream(ImportType.values())
      .collect(
          ImmutableMap.toImmutableMap(
              type -> type.name(),
              type -> type));
  
  @Inject
  ImportPreferences() {}
  
  /**
   * Store the imports as a string list, not an enum list, to simplify
   * version compatibility.  We want any mismatches to result in dropped data,
   * not error messages. 
   */
  @Preference @SerializeAsJson List<String> recentlyUsedImports = new ArrayList<>();

  public ImmutableSet<ImportType> recentlyUsedImports() {
    return recentlyUsedImports.stream()
        .filter(STRING_TO_TYPE::containsKey)
        .map(STRING_TO_TYPE::get)
        .collect(ImmutableSet.toImmutableSet());
  }
  
  public void addRecentlyUsedImport(ImportType importType) {
    // Don't reorder the list to always keep the most recently-used 
    // at the front.  This is just annoying...
    if (recentlyUsedImports.contains(importType.name())) {
      return;
    }
    
    // Add the import at the front.
    recentlyUsedImports.add(0, importType.name());
  }
}
