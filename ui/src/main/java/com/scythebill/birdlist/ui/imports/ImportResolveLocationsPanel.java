/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.LayoutStyle.ComponentPlacement;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicates;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.mapdata.MapData;
import com.scythebill.birdlist.model.sighting.ClosestLocations;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.ui.actions.ReturnAction;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.components.NewLocationPanel;
import com.scythebill.birdlist.ui.components.WherePanelIndexers;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.imports.ParsedLocationIds.ToBeDecided;
import com.scythebill.birdlist.ui.imports.ParsedLocationIds.ToBeDecided.HintType;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.LocationIdToString;

/**
 * Handles resolving a location when importing.
 */
class ImportResolveLocationsPanel extends JPanel implements FontsUpdatedListener {
  
  private static final int MAX_CLOSE_LOCATIONS = 10;
  private static final double MAX_CLOSE_DISTANCE_KM = 50.0;
  /**
   * Distance at which defaulting to the existing location makes sense.
   * This is arbitrary, but should be bigger than, for example, BirdLasserImporter.ASSUME_SAME_LOCATION_DISTANCE_KM.
   * *That* value is a distance at which Scythebill won't even give you a choice - this is a distance at which
   * it'll default to the same location but let you override.
   */
  private static final double SHORTCUT_CLOSE_DISTANCE_KM = 5.0;
  private final Alerts alerts;
  private JButton cancel;
  private JButton back;
  private JButton ok;
  private List<Object> keys;
  private int index;
  private Object toBeDecidedKey;
  private Runnable onFinished;
  private JSeparator separator;
  private JLabel topLabel;
  private ParsedLocationIds parsedLocationIds;
  private Map<Object, ToBeDecided> toBeResolvedNames;
  private Map<Object, Location> resolvedLocations;
  private Map<String, Location> resolvedLocationsByName = new LinkedHashMap<>();
  private Set<String> askedHints = new LinkedHashSet<>();
  private ReportSet reportSet;
  private final NewLocationPanel newLocationPanel;
  private JRadioButton existingRadio;
  private JRadioButton newRadio;
  /** Combobox for resolving a sighting to an existing location, when the name *does* match an existing location. */ 
  private JComboBox<ExistingLocation> existingCombobox;
  /** Indexer for resolving a sighting to an existing location, when the name *doesn't* match anything. */ 
  private IndexerPanel<Object> existingIndexer;
  private WherePanelIndexers wherePanelIndexers;
  private Multimap<String, String> locationsByExactName;
  private PredefinedLocations predefinedLocations;
  private JLabel countLabel;

  @Inject
  public ImportResolveLocationsPanel(
      ReturnAction returnAction,
      FontManager fontManager,
      NewLocationPanel newLocationPanel,
      PredefinedLocations predefinedLocations,
      Alerts alerts) {
    this.newLocationPanel = newLocationPanel;
    this.predefinedLocations = predefinedLocations;
    this.alerts = alerts;
    
    initGUI();
    fontManager.applyTo(this);
    
    cancel.addActionListener(returnAction);
    back.setEnabled(false);
    back.addActionListener(e -> previousLocation());

    ok.setEnabled(false);
    ok.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent e) {
        Location value = null;
        int advanceTo = index + 1;
        if (newRadio.isSelected()) {
          if (newLocationPanel.isComplete()) {
            value = newLocationPanel.getValuePossiblyReusing();
          }
          
          ToBeDecided toBeDecided = toBeResolvedNames.get(toBeDecidedKey);
          Preconditions.checkState(toBeDecided != null);
          if (!Strings.isNullOrEmpty(toBeDecided.hint)) {
            advanceTo = useHintToSkipPastEntries(advanceTo, toBeDecided.hint, value);
          }
        } else {
          // Grabbing the imported location from the indexer
          value = WherePanelIndexers.toLocation(reportSet.getLocations(), existingIndexer.getValue());
        }
        
        if (value != null) {
          markLocationResolved(toBeDecidedKey, value);
          advanceTo(advanceTo);
        }
      }
    });
    
    PropertyChangeListener updateButtonsListener = e -> updateButtons();
    newLocationPanel.addPropertyChangeListener("complete", updateButtonsListener);
    existingIndexer.addPropertyChangeListener("value", updateButtonsListener);
    
    existingCombobox.addActionListener(e -> {
      Object selectedItem = existingCombobox.getSelectedItem();
      if (selectedItem instanceof ExistingLocation) {
        existingIndexer.setValue(((ExistingLocation) selectedItem).locationId);        
      }
    });

    ActionListener buttonListener = e -> updateButtons();
    existingRadio.addActionListener(buttonListener);
    newRadio.addActionListener(buttonListener);
  }
  
  private void updateButtons() {
    if (newRadio.isSelected()) {
      newLocationPanel.setEnabled(true);
      existingCombobox.setEnabled(false);
      existingIndexer.setEnabled(false);
      ok.setEnabled(newLocationPanel.isComplete());
    } else {
      newLocationPanel.setEnabled(false);
      existingCombobox.setEnabled(true);
      existingIndexer.setEnabled(true);
      
      Object comboboxItem = existingCombobox.getSelectedItem();
      Object indexerItem = existingIndexer.getValue();
      // When there's a legit value entered in the indexer, and a value entered in the combobox, and
      // they *don't* match, then reset the combobox to null so there aren't two conflicting locations.
      // If the user picks from the combobox, then that gets pushed to the indexer, so they'll match.
      // If the user picks from the indexer, then that doesn't get pushed to the combobox, and they might not match.
      if (comboboxItem != null && (comboboxItem instanceof ExistingLocation) && indexerItem != null) {
        if (!indexerItem.equals(((ExistingLocation) comboboxItem).locationId)) {
          existingCombobox.setSelectedItem(null);
        }
      }
      ok.setEnabled(existingIndexer.getValue() != null);
    }
  }
  
  /**
   * Start iterating through the list of To Be Decideds.
   */
  public void start(
      ReportSet reportSet,
      ImmutableMap<Object, ToBeDecided> toBeResolvedNames,
      ParsedLocationIds parsedLocationIds,
      Runnable onFinished) {
    this.reportSet = reportSet;
    this.toBeResolvedNames = toBeResolvedNames;
    this.parsedLocationIds = parsedLocationIds;
    this.onFinished = onFinished;
    wherePanelIndexers = new WherePanelIndexers(
        reportSet.getLocations(), predefinedLocations, /*addNullName=*/false, null, Predicates.alwaysTrue(),
        /* syntheticLocations= */ null);
    Preconditions.checkState(!toBeResolvedNames.isEmpty());
    locationsByExactName = LinkedHashMultimap.create();
    for (Location location : reportSet.getLocations().rootLocations()) {
      populateLocationsByExactNameMap(location);
    }
    keys = Lists.newArrayList(toBeResolvedNames.keySet());
    
    // Sort the "keys" by hints, where each hint has a "value" of its first index.
    // Baaasically, we want to ensure that, no matter what weird order the input comes in,
    // that "hints" are all contiguous, so that later optimizations to treat locations
    // with the same hint "just works".
    Map<String, Integer> hintFirstPositions = new LinkedHashMap<>();
    for (int i = 0; i < keys.size(); i++) {
      Object key = keys.get(i);
      ToBeDecided tbd = toBeResolvedNames.get(key);
      String hint = Strings.nullToEmpty(tbd.hint);
      if (!hintFirstPositions.containsKey(hint)) {
        hintFirstPositions.put(hint, i);
      }
    }
    
    if (hintFirstPositions.size() > 1) {
      Collections.sort(
          keys,
          Comparator.comparing(
              key -> {
                String hint = Strings.nullToEmpty(toBeResolvedNames.get(key).hint);
                return hintFirstPositions.get(hint);
              },
              Comparator.naturalOrder()));
    }
    
    // See if the parent location of the first location can be pre-filled based on lat-long.
    // Arguably, this could prefill *all* the locations, but I'd rather have subsequent locations
    // default based on the previous item.
    ToBeDecided firstTbd = toBeResolvedNames.get(keys.get(0));
    if (firstTbd.preferredParent == null
        && firstTbd.latLong != null) {
      String code = MapData.instance().getISOCode(firstTbd.latLong);
      if (code != null) {
        LocationShortcuts locationShortcuts =
            new LocationShortcuts(reportSet.getLocations(), predefinedLocations);
        firstTbd.preferredParent = locationShortcuts.getLocationFromEBirdCode(code);
      }
    }

    resolvedLocations = Maps.newHashMap();
    setIndex(0);
  }
  
  /**
   * Builds a multimap of exact location name matches.
   */
  private void populateLocationsByExactNameMap(Location location) {
    locationsByExactName.put(location.getDisplayName(), location.getId());
    if (!location.getModelName().equals(location.getDisplayName())) {
      locationsByExactName.put(location.getModelName(), location.getId());
    }
    for (Location child : location.contents()) {
      populateLocationsByExactNameMap(child);
    }
  }

  /**
   * Move to a new index.
   */
  private void setIndex(int index) {
    this.index = index;
    this.toBeDecidedKey = keys.get(index);
    
    final ToBeDecided toBeDecided = toBeResolvedNames.get(toBeDecidedKey);
    String name = toBeDecided.name;
    // Make sure any existing async operations are done.  Would be nice if 
    // the panel could figure this out on its own, but this is a lot simpler,
    // since in most places NewLocationPanel isn't reused.  (An even easier
    // alternative might be deleting and recreating the NewLocationPanel?)
    newLocationPanel.cancelAsyncOperations();
    newLocationPanel.setLocationName(name);
    newLocationPanel.setGeocodedParentLocation(toBeDecided.preferredParent);
    newLocationPanel.setLocationLatLong(toBeDecided.latLong);
    
    // Make sure the current location state is re-indexed
    newLocationPanel.reindexLocations();
    wherePanelIndexers.configureIndexer(existingIndexer);
    
    if (keys.size() != 1) {
      String countText;
      if (Strings.isNullOrEmpty(name)) {
        countText =
            Messages.getFormattedMessage(Name.LOCATION_OF_FORMAT, index + 1, keys.size());
      } else {
        countText = Messages.getFormattedMessage(Name.LOCATION_OF_FORMAT_WITH_HINT, index + 1, keys.size(), name);
      }
      
      if (toBeDecided.hint != null) {
        countText = String.format("%s - %s", countText, toBeDecided.hint);
      }
      countLabel.setText(countText);
    }
    
    Collection<Object> matches;
    
    if (Strings.isNullOrEmpty(name)) {
      if (toBeDecided.latLong != null) {
        ClosestLocations closestLocations = new ClosestLocations(toBeDecided.latLong, reportSet.getLocations());
        matches = closestLocations.getNearestLocations(MAX_CLOSE_LOCATIONS, MAX_CLOSE_DISTANCE_KM);
      } else {
        matches = ImmutableList.of();
      }
    } else {
      matches = wherePanelIndexers.getMatches(name);
      if (matches.isEmpty()) {
        // For really long names, the indexer will fail.  Work around this by
        // checking exact matches when the indexer lookup gets nothing.
        Collection<String> exactIds = locationsByExactName.get(name);
        List<Object> indexerMatchers = Lists.newArrayList();
        for (String exactId : exactIds) {
          indexerMatchers.add(wherePanelIndexers.toIndexerObject(exactId));
        }
        matches = indexerMatchers;
      }
      // TODO: maybe sort these matches based on distance?

      if (resolvedLocationsByName.containsKey(name)) {
        Location location = resolvedLocationsByName.get(name);
        Object resolved = wherePanelIndexers.toIndexerObject(location.getId());
        List<Object> matchesWithResolved = new ArrayList<>(1 + matches.size());
        matchesWithResolved.addAll(matches);
        matchesWithResolved.remove(resolved);
        matchesWithResolved.add(0, resolved);
        matches = matchesWithResolved;
      }
    }
    
    if (matches.isEmpty()) {
      existingCombobox.setVisible(false);
      existingCombobox.setModel(new DefaultComboBoxModel<ExistingLocation>());
      existingIndexer.setVisible(true);
      newRadio.setSelected(true);
      newLocationPanel.setEnabled(true);
    } else {
      existingCombobox.setVisible(true);
      existingIndexer.setVisible(true);
      
      List<ExistingLocation> list =
          matches.stream().map(o -> new ExistingLocation(o, toBeDecided.latLong))
              .collect(Collectors.toCollection(ArrayList::new));
      list.add(0, null);
      existingCombobox.setModel(new DefaultComboBoxModel<ExistingLocation>(list.toArray(new ExistingLocation[0])));
      
      if (Strings.isNullOrEmpty(name)) {
        // Match list is lat-long based.
        ExistingLocation closestExistingLocation = list.get(1);
        if (closestExistingLocation.distanceToCenter() < SHORTCUT_CLOSE_DISTANCE_KM) {
          // If there's a fairly close existing location, default to it, and default
          // the choice to using an existing location
          newLocationPanel.setEnabled(false);
          existingRadio.setSelected(true);
          existingCombobox.setSelectedIndex(1);
        } else {
          // Otherwise, assume the user will need a new location.
          newLocationPanel.setEnabled(false);
          newRadio.setSelected(true);
        }
      } else {
        newLocationPanel.setEnabled(false);
        existingRadio.setSelected(true);

        // Find all with name matches
        ImmutableList<ExistingLocation> nameMatches =
            list.stream().filter(el -> el != null && el.nameMatches(name))
                .collect(ImmutableList.toImmutableList());
        // If there's exactly one name match, select it
        if (nameMatches.size() == 1) {
          existingCombobox.setSelectedIndex(list.indexOf(nameMatches.get(0)));
          
          // If there's one match, and it's the only entry, then there's no need for the combobox.
          if (matches.size() == 1) {
            existingCombobox.setVisible(false);
          }
        }
      }      
    }
    
    back.setEnabled(index > 0);
    ok.setText(index < (keys.size() - 1) ?
        Messages.getMessage(Name.NEXT_BUTTON) : Messages.getMessage(Name.OK_BUTTON));
    updateButtons();
  }

  private void initGUI() {
    
    topLabel = new JLabel("<html>" +
        Messages.getMessage(Name.IDENTIFY_WHERE_THE_SIGHTINGS_TOOK_PLACE));
    topLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    
    countLabel = new JLabel();
        
    separator = new JSeparator();
    
    cancel = new JButton();
    cancel.setText(Messages.getMessage(Name.CANCEL_IMPORT));

    back = new JButton();
    back.setText(Messages.getMessage(Name.PREVIOUS_BUTTON));

    ok = new JButton();
    ok.setText(Messages.getMessage(Name.OK_BUTTON));
    
    existingRadio = new JRadioButton(Messages.getMessage(Name.EXISTING_LOCATION));
    newRadio = new JRadioButton(Messages.getMessage(Name.NEW_LOCATION));
    ButtonGroup group = new ButtonGroup();
    group.add(existingRadio);
    group.add(newRadio);
    existingCombobox = new JComboBox<ExistingLocation>();
    existingCombobox.setEditable(false);
    
    existingIndexer = new IndexerPanel<Object>();
    existingIndexer.setPreviewText(Name.START_TYPING_A_LOCATION);
  }

  private void advanceTo(int toIndex) {
    if (toIndex >= keys.size()) {
      // Store all resolved locations
      for (Map.Entry<Object, Location> entry : resolvedLocations.entrySet()) {
        Location location = entry.getValue();
        // Anything that doesn't have an ID needs to be added.
        reportSet.getLocations().ensureAdded(location);
        Preconditions.checkState(location.getId() != null);
        parsedLocationIds.put(entry.getKey(), location.getId());
      }
      
      onFinished.run();
    } else {
      ToBeDecided previousTbd = toBeResolvedNames.get(toBeDecidedKey);
      String previousHint = previousTbd.hint;
      if (!Strings.isNullOrEmpty(previousHint)) {
        ToBeDecided nextTbd = toBeResolvedNames.get(keys.get(toIndex));
        if (previousTbd.hintType == HintType.location) {
          // For location hints, if the hints are equal, copy over the parent. 
          if (previousHint.equals(nextTbd.hint) && nextTbd.preferredParent == null) {
            Location previous = Preconditions.checkNotNull(resolvedLocations.get(toBeDecidedKey));
            Location country = Locations.getAncestorOfType(previous, Location.Type.country);
            if (country != null) {
              nextTbd.preferredParent = country;
            }
          }
        } else if (nextTbd.preferredParent == null) {
          // For non-location hints, always copy over the state or country as a preferred parent
          // if there isn't already one.
          // This is is basically a hack which is "if BirdLasser", since BirdLasser is the only
          // non-location hint importer.
          Location previous = Preconditions.checkNotNull(resolvedLocations.get(toBeDecidedKey));
          Location state = Locations.getAncestorOfType(previous, Location.Type.state);
          if (state != null) {
            nextTbd.preferredParent = state;
          } else {
            Location country = Locations.getAncestorOfType(previous, Location.Type.country);
            if (country != null) {
              nextTbd.preferredParent = country;
            }
          }
        }
      }
      setIndex(toIndex);
    }
  }

  private void previousLocation() {
    if (index > 0) {
      setIndex(index - 1);
    }
  }

  /**
   * Sees if the hint matches any further entries, and maps them all using the parent
   * if possible.
   */
  private int useHintToSkipPastEntries(int advanceTo, String hint, Location value) {
    if (!askedHints.contains(hint)) {
      List<ToBeDecided> toBeDecidedWithSameHint = new ArrayList<>();
      for (int i = advanceTo; i < keys.size(); i++) {
        ToBeDecided toBeDecided = toBeResolvedNames.get(keys.get(i));
        // Intersecting the "hint" feature with preferred parent would require
        // some extra code, and there are not (Oct 2018) anything that has both
        // hints-useful-for-grouping *and* preferred parents. (BirdLasser has hints,
        // and could trip preferred-parents-via geocoding, but they're just for UI
        // improvements;  BirdLasser 
        if (toBeDecided.preferredParent != null) {
          return advanceTo;
        }
        
        // Look for locations with a given name *and* with a matching hint.  They're eligible.
        if (!Strings.isNullOrEmpty(toBeDecided.name) && Objects.equals(toBeDecided.hint, hint)
            && toBeDecided.hintType == HintType.location) {
          toBeDecidedWithSameHint.add(toBeDecided);
        } else {
          // Must be contiguous for the "advanceTo" to work. Sorting above should guarantee that
          // they are.
          break;
        }
      }
      
      if (!toBeDecidedWithSameHint.isEmpty()) {
        Location country = Locations.getAncestorOfType(value, Location.Type.country);
        if (country != null) {
          int yesNo = alerts.showYesNo(this,
              Messages.getFormattedMessage(
                  Name.ASSIGN_ALL_TO_COUNTRY_TITLE_FORMAT,
                  toBeDecidedWithSameHint.size(),
                  country.getDisplayName()),
              Messages.getFormattedMessage(
                  Name.ASSIGN_ALL_TO_COUNTRY_MESSAGE_FORMAT,
                  toBeDecidedWithSameHint.size(),
                  hint,
                  country.getDisplayName()));
          
          // If yes, then assign everything to this country.
          if (yesNo == JOptionPane.YES_OPTION) {
            for (int i = advanceTo; i < (advanceTo + toBeDecidedWithSameHint.size()); i++) {
              Object key = keys.get(i);
              ToBeDecided toBeDecided = toBeResolvedNames.get(key);
              
              Location location = country.getContent(toBeDecided.name);
              if (location == null) {
                Location.Builder builder = Location.builder()
                    .setName(toBeDecided.name)
                    .setParent(country);
                if (toBeDecided.latLong != null) {
                  builder.setLatLong(toBeDecided.latLong);
                }
                location = builder.build();
              }
              
              markLocationResolved(key, location);
            }
            
            advanceTo = advanceTo + toBeDecidedWithSameHint.size();
          }
        }
      }
      
      askedHints.add(hint);
    }
    
    return advanceTo;
  }

  /**
   * Enter the (potentially temporary) resolution of a specific key to a given
   * location.  All of these will be resolved for-real (and added to ParsedLocationIds)
   * at the very end. 
   */
  private void markLocationResolved(Object key, Location location) {
    reportSet.getLocations().ensureAdded(location);
    resolvedLocations.put(key, location);
    
    // Store by name as well.  Note that this may replace another location with the same
    // name;  this is fine, as it's merely used to *default* to "existing locations" when
    // picking a location that was already set.
    resolvedLocationsByName.put(location.getModelName(), location);
    // Save by both names (typically the same)
    resolvedLocationsByName.put(location.getDisplayName(), location);
    
    // And store by the input name too - in case the user edited the name, we should
    // resolve that way too.
    ToBeDecided toBeDecided = toBeResolvedNames.get(key);
    if (!Strings.isNullOrEmpty(toBeDecided.name)) {
      resolvedLocationsByName.put(toBeDecided.name, location);
    }
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    setPreferredSize(fontManager.scale(new Dimension(900, 600)));
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setAutoCreateContainerGaps(true);
    layout.setHorizontalGroup(layout.createParallelGroup()
        .addComponent(topLabel)
        .addComponent(countLabel)
        .addGroup(Alignment.LEADING, layout.createSequentialGroup()
            .addComponent(existingRadio)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup()
                .addComponent(existingCombobox, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(existingIndexer, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)))
        .addComponent(newRadio)
        .addComponent(newLocationPanel)
        .addComponent(separator)
        .addGroup(Alignment.LEADING, layout.createSequentialGroup()
            .addComponent(cancel)
            .addGap(0, fontManager.scale(75), Short.MAX_VALUE)
            .addComponent(back)
            .addGap(fontManager.scale(20))
            .addComponent(ok)));
    layout.setVerticalGroup(layout.createSequentialGroup()
        .addComponent(topLabel)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(countLabel)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addGroup(layout.createBaselineGroup(false, true)
            .addComponent(existingRadio)
            .addGroup(layout.createSequentialGroup()
                .addComponent(existingIndexer)
                .addComponent(existingCombobox)))
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(newRadio)
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(newLocationPanel)
        .addGap(18, 18, Short.MAX_VALUE)
        .addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(cancel)
            .addComponent(back)
            .addComponent(ok)));
        
    layout.linkSize(cancel, back, ok);
  }
  
  class ExistingLocation {
    final Object locationId;
    private final LatLongCoordinates center;
    ExistingLocation(Object locationId, LatLongCoordinates center) {
      this.locationId = locationId;
      this.center = center;
    }
    
    @Override
    public String toString() {
      Location location = WherePanelIndexers.toLocation(reportSet.getLocations(), locationId);
      if (location == null) {
        return "";
      }
      
      String locationName = LocationIdToString.getStringWithType(location);
      if (center != null && location.getLatLong().isPresent()) {
        // TODO: support miles, based on VisitInfoPreferences
        locationName = String.format("%s (%.1f km)",
            locationName, center.kmDistance(location.getLatLong().get()));
      }
      return locationName;
    }
    
    public double distanceToCenter() {
      if (center == null) {
        return Double.MAX_VALUE;
      }
      Location location = WherePanelIndexers.toLocation(reportSet.getLocations(), locationId);
      if (location == null || !location.getLatLong().isPresent()) {
        return Double.MAX_VALUE;
      }
      
      return center.kmDistance(location.getLatLong().get());      
    }
    
    public boolean nameMatches(String name) {
      Location location = WherePanelIndexers.toLocation(reportSet.getLocations(), locationId);
      if (location == null) {
        return false;
      }
      
      return name.equals(location.getDisplayName()) || name.equals(location.getModelName());
    }
  } 
}
