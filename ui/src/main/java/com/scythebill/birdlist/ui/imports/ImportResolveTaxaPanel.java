/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.awt.Dimension;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;

import com.google.common.base.CharMatcher;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.actions.ReturnAction;
import com.scythebill.birdlist.ui.components.SpeciesIndexerPanelConfigurer;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.events.TaxonomyChangedEvent;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.imports.TaxonImporter.ToBeDecided;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.SpeciesSpHybridEntryPanel;
import com.scythebill.birdlist.ui.panels.SpeciesSpHybridEntryPanel.SpeciesEntryEvent;
import com.scythebill.birdlist.ui.panels.SpeciesSpHybridEntryPanel.SpeciesEntryListener;

/**
 * Handles resolving taxa when importing.
 */
class ImportResolveTaxaPanel extends JPanel implements FontsUpdatedListener {
  
  private JButton cancel;
  private JButton back;
  private JButton dropSpecies;
  private Taxonomy taxonomy;
  private List<TbdNoSsp> keys;
  private int index;
  private JLabel commonName;
  private JLabel scientificName;
  private JScrollPane descriptionScrollPane;
  private JTextArea description;
  private Map<Object, ToBeDecided> toBeDecidedMap;
  private TbdNoSsp toBeDecided;
  private SightingsImporter<?> importer;
  private Runnable onFinished;
  private Multimap<TbdNoSsp, Object> tbdNoSspToKeys;
  private SpeciesSpHybridEntryPanel speciesEntry;
  private JButton ok;
  private JLabel commonLabel;
  private JLabel scientificLabel;
  private JLabel descriptionLabel;
  private JSeparator separator;
  private JLabel topLabel;
  private TaxonResolver taxonResolver;

  @Inject
  public ImportResolveTaxaPanel(
      TaxonomyStore taxonomyStore,
      SpeciesIndexerPanelConfigurer speciesIndexerPanelConfigurer,
      EventBusRegistrar eventBusRegistrar,
      ReturnAction returnAction,
      SpeciesSpHybridEntryPanel speciesEntry,
      FontManager fontManager) {
    this.speciesEntry = speciesEntry;

    initGUI();
    fontManager.applyTo(this);
    
    eventBusRegistrar.registerWhenInHierarchy(this);
    setTaxonomy(taxonomyStore.getTaxonomy());

    cancel.addActionListener(returnAction);
    speciesEntry.setInPanelAddButtonVisibility(false);
    ok.setAction(speciesEntry.getAddAction());
    ok.setText(Messages.getMessage(Name.OK_BUTTON));
    speciesEntry.addSpeciesEntryListener(new SpeciesEntryListener() {
      @Override public void addSpecies(SpeciesEntryEvent event) {
        SightingTaxon species = event.getResolved().getSightingTaxon();
        Collection<Object> taxonKeys = tbdNoSspToKeys.get(keys.get(index));
        for (Object taxonKey : taxonKeys) {
          // For every entry with that same common name/scientific name pairing,
          // resolve the subspecies, if any
          SightingTaxon withSubspecies = resolveSubspecies(toBeDecidedMap.get(taxonKey), species);
          SightingTaxon mappedToBase = mapToBase(withSubspecies);
          importer.registerTaxon(taxonKey, new TaxonPossibilities(mappedToBase));
        }
        nextSpecies();
      }
    });
    
    back.setEnabled(false);
    back.addActionListener(e -> previousSpecies());
    
    dropSpecies.addActionListener(e -> nextSpecies());
  }
  
  final static class TbdNoSsp {
    final String commonName;
    final String sciName;
    final String longText;
    
    TbdNoSsp(ToBeDecided tbd) {
      commonName = tbd.commonName == null ? null : CharMatcher.whitespace().trimFrom(tbd.commonName);
      sciName = tbd.scientificName == null ? null : CharMatcher.whitespace().trimFrom(tbd.scientificName);
      longText = tbd.longText == null ? null : CharMatcher.whitespace().trimFrom(tbd.longText);
    }

    @Override
    public int hashCode() {
      return Objects.hashCode(commonName, sciName, longText);
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      }
      if (!(obj instanceof TbdNoSsp)) {
        return false;
      }
      
      TbdNoSsp that = (TbdNoSsp) obj;
      return Objects.equal(commonName, that.commonName)
          && Objects.equal(sciName, that.sciName)
          && Objects.equal(longText, that.longText);
    }
  }
  
  /**
   * Start iterating through the list of To Be Decideds.
   */
  public void start(
      Map<Object, ToBeDecided> toBeDecided,
      SightingsImporter<?> importer,
      Runnable onFinished) {
    this.onFinished = onFinished;
    Preconditions.checkState(!toBeDecided.isEmpty());
    
    // Strip out subspecies from the list of options presented to users.
    // The importer process will do its best to reinsert subspecies on the way out
    this.tbdNoSspToKeys = LinkedHashMultimap.create();
    for (Map.Entry<Object, ToBeDecided> tbdEntry : toBeDecided.entrySet()) {
      tbdNoSspToKeys.put(new TbdNoSsp(tbdEntry.getValue()), tbdEntry.getKey());
    }
    
    this.toBeDecidedMap = toBeDecided;
    this.importer = importer;
    keys = Lists.newArrayList(tbdNoSspToKeys.keySet());
    setIndex(0);
  }
  
  /**
   * Move to a new index.
   */
  private void setIndex(int index) {
    this.index = index;
    this.toBeDecided = keys.get(index);
    commonName.setText(toBeDecided.commonName == null
        ? Messages.getMessage(Name.NO_COMMON_NAME) : toBeDecided.commonName);
    scientificName.setText(toBeDecided.sciName == null
        ? Messages.getMessage(Name.NO_SCIENTIFIC_NAME) : toBeDecided.sciName);
    description.setText(toBeDecided.longText == null ? "" : toBeDecided.longText);
    if (toBeDecided.longText == null) {
      commonLabel.setVisible(true);
      commonName.setVisible(true);
      scientificLabel.setVisible(true);
      scientificName.setVisible(true);
      descriptionLabel.setVisible(false);
      descriptionScrollPane.setVisible(false);
    } else {
      commonLabel.setVisible(toBeDecided.commonName != null || toBeDecided.sciName != null);
      commonName.setVisible(toBeDecided.commonName != null || toBeDecided.sciName != null);
      scientificLabel.setVisible(toBeDecided.commonName != null || toBeDecided.sciName != null);
      scientificName.setVisible(toBeDecided.commonName != null || toBeDecided.sciName != null);
      descriptionLabel.setVisible(true);
      descriptionScrollPane.setVisible(true);
    }
    
    speciesEntry.setIndexerLabel(
        Messages.getFormattedMessage(Name.RESOLVE_SPECIES_OF_FORMAT, index + 1, keys.size()));
    speciesEntry.getIndexer().setValue(null);
    back.setEnabled(index > 0);
  }

  @Subscribe
  public void taxonomyChanged(TaxonomyChangedEvent event) {
    setTaxonomy(event.getTaxonomy());    
  }

  private void setTaxonomy(Taxonomy newTaxonomy) {
    if (newTaxonomy == this.taxonomy) {
      return;
    }
    
    this.taxonomy = newTaxonomy;
    speciesEntry.setTaxonomy(newTaxonomy);
    this.taxonResolver = new TaxonResolver(newTaxonomy);
  }

  private void initGUI() {
    
    topLabel = new JLabel(Messages.getMessage(Name.COULD_NOT_LOCATE_SOME_SPECIES));
    topLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    
    commonLabel = new JLabel(Messages.getMessage(Name.IMPORTED_COMMON_NAME_LABEL));
    commonName = new JLabel();
    commonName.putClientProperty(FontManager.PLAIN_LABEL, true);
    scientificLabel = new JLabel(Messages.getMessage(Name.IMPORTED_SCIENTIFIC_NAME_LABEL));
    scientificName = new JLabel();
    scientificName.putClientProperty(FontManager.PLAIN_LABEL, true);
    
    descriptionLabel = new JLabel(Messages.getMessage(Name.IMPORTED_DESCRIPTION_LABEL));
    description = new JTextArea(5, 60);
    description.setWrapStyleWord(true);
    description.setLineWrap(true);
    description.setEditable(false);
    descriptionScrollPane = new JScrollPane(description, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    

    separator = new JSeparator();
    
    cancel = new JButton();
    cancel.setText(Messages.getMessage(Name.CANCEL_IMPORT));

    dropSpecies = new JButton();
    dropSpecies.setText(Messages.getMessage(Name.DROP_SPECIES));
    
    back = new JButton();
    back.setText(Messages.getMessage(Name.PREVIOUS_BUTTON));

    ok = new JButton();
    ok.setText(Messages.getMessage(Name.OK_BUTTON));
  }

  private void nextSpecies() {
    if (index + 1 == keys.size()) {
      onFinished.run();
    } else {
      setIndex(index + 1);
    }
  }

  private void previousSpecies() {
    if (index > 0) {
      setIndex(index - 1);
    }
  }

  /** Resolve any subspecies, if necessary. */
  private SightingTaxon resolveSubspecies(ToBeDecided toBeDecided, SightingTaxon species) {
    if (Strings.isNullOrEmpty(toBeDecided.subspecies)) {
      return species;
    }
    
    // Subspecies of a hybrid or sp.?  Ignore it, moving along.
    if (species.getType() != SightingTaxon.Type.SINGLE) {
      return species;
    }
    
    Taxon speciesTaxon = taxonomy.getTaxon(species.getId());
    Taxon subspecies = taxonResolver.findMatchWithin(speciesTaxon, toBeDecided.subspecies, false /* Don't prefer groups */);
    if (subspecies != null) {
      return SightingTaxons.newSightingTaxon(subspecies.getId());
    }
    
    // Couldn't find the subspecies;  return the species
    return species;
  }

  /** Map the taxon back to the base taxonomy. */
  private SightingTaxon mapToBase(SightingTaxon taxon) {
    if (taxonomy instanceof MappedTaxonomy) {
      MappedTaxonomy mappedTaxonomy = (MappedTaxonomy) taxonomy;
      SightingTaxon mapping = mappedTaxonomy.getMapping(taxon);
      // If that taxon couldn't be mapped, try to map the parent.
      if (mapping == null) {
        taxon = taxon.resolveInternal(taxonomy).getParent();
        mapping = mappedTaxonomy.getMapping(taxon);
      }
      
      return mapping;
    } else {
      return taxon;
    }
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    setPreferredSize(fontManager.scale(new Dimension(700, 440)));
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setAutoCreateContainerGaps(true);
    layout.setHorizontalGroup(layout.createParallelGroup()
        .addComponent(topLabel)
        .addGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup()
                .addComponent(commonLabel)
                .addComponent(scientificLabel)
                .addComponent(descriptionLabel))
            .addPreferredGap(ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup()
                .addComponent(commonName)
                .addComponent(scientificName)
                .addComponent(descriptionScrollPane, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)))
        .addComponent(speciesEntry)
        .addComponent(separator)
        .addGroup(Alignment.LEADING, layout.createSequentialGroup()
            .addComponent(cancel)
            .addGap(0, fontManager.scale(75), Short.MAX_VALUE)
            .addComponent(dropSpecies)
            .addGap(fontManager.scale(20))
            .addComponent(back)
            .addGap(fontManager.scale(20))
            .addComponent(ok)));
    layout.setVerticalGroup(layout.createSequentialGroup()
        .addComponent(topLabel)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(commonLabel)
            .addComponent(commonName))
        .addPreferredGap(ComponentPlacement.RELATED)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(scientificLabel)
            .addComponent(scientificName))
        .addPreferredGap(ComponentPlacement.RELATED)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(descriptionLabel)
            .addComponent(descriptionScrollPane, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(speciesEntry)
        .addGap(18)
        .addComponent(separator)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(cancel)
            .addComponent(dropSpecies)
            .addComponent(back)
            .addComponent(ok)));
        
    layout.linkSize(commonLabel, scientificLabel, descriptionLabel);
    layout.linkSize(cancel, dropSpecies, back, ok);
  }
}
