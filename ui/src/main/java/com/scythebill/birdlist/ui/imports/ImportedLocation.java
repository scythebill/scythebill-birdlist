/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.util.List;
import java.util.logging.Logger;

import com.google.common.base.MoreObjects;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.mapdata.MapData;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Location.Type;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Utility for importing locations.
 */
public class ImportedLocation {
  private static final Logger logger = Logger.getLogger(ImportedLocation.class.getName()); 
  public String countryCode;
  public String country;
  public String stateCode;
  public String state;
  public String county;
  public List<String> locationNames = Lists.newArrayList();
  public String city;
  public String region;
  public String defaultRegion = "North America";
  public String latitude;
  public String longitude;
  public String description;
  
  public String addToLocationSet(
      ReportSet reportSet,
      LocationSet locations,
      LocationShortcuts locationShortcuts,
      PredefinedLocations predefinedLocations) {
    
    Location current = null;
    // Specifically hack "USA".  This turns out to be saner than throwing "USA"
    // into LocationShortcuts, because the match-by-increasing-distance code
    // ends up matching all abbreviations to the US!
    if ("usa".equalsIgnoreCase(country)
        || "us".equalsIgnoreCase(country)) {
      countryCode = "US";
    }
    
    // UK isn't the country code - it's GB
    if ("uk".equalsIgnoreCase(country)) {
      countryCode = "GB";
      country = null;
    }
    
    // Give all two-letter "countries" a shot as a country code
    if (country != null && country.length() == 2 && countryCode == null) {
      countryCode = country;
      // ... countryCode will turn into country below
      country = null;
    }
    
    // And if the state is 1 to 3 letters, then try it as a state code too
    if (state != null && state.length() >= 1 && state.length() <= 3 && stateCode == null) {
      stateCode = state;
      // Don't erase "state", since it doesn't fall back the way country code does
    }
    
    // Try to grab a country code using the lat-long, and prefer that to country name.
    if (Strings.isNullOrEmpty(countryCode)) {
      setCodesUsingLatLong();
    }
    
    if (!Strings.isNullOrEmpty(countryCode)) {
      // Find by country code - with state as a hint
      if (state != null) {
        current = locationShortcuts.getCountryFromCodeWithExpectedChild(countryCode, state);
      } else {
        String regionForCountryCodeLookup = (region != null) ? region : defaultRegion;
        current = locationShortcuts.getCountryFromCodeWithExpectedParent(countryCode, regionForCountryCodeLookup);
      }
      
      // Look for things like "EC-W" and "PT-30"
      if (!Strings.isNullOrEmpty(stateCode)) {
        String countryCodeIncludingState = countryCode + "-" + stateCode;
        Location countryIncludingState = locationShortcuts.getLocationFromEBirdCode(countryCodeIncludingState);
        if (countryIncludingState != null ) {
          current = countryIncludingState;
          state = null;
          stateCode = null;
        }
      }
      
      if (current == null && country == null) {
        // If the country code fails, then treat it as a name
        country = countryCode;
      }
    }
    
    if (current == null) {
      if (!Strings.isNullOrEmpty(country)) {
        Location regionLoc = null;
        if (region != null) {
          regionLoc = locationShortcuts.getRegion(region);
        }
        // First, try an exact match based on the country, using the state to pick among multiples.
        current = locationShortcuts.getCountryExactMatch(country, regionLoc, state);
        if (current == null) {
          // If that doesn't work, try inexact location name matches (again, with the state as a hint)
          current = locationShortcuts.getCountryInexactMatch(country, regionLoc, state);
        }
        if (current == null) {
          if (regionLoc == null) {
            regionLoc = locationShortcuts.getRegion(defaultRegion);
          }
          current = Location.builder()
              .setName(country)
              .setType(Type.country)
              .setParent(regionLoc)
              .build();
          current = addLocationIfNecessary(locations, predefinedLocations, current);
        } else {
          // If we grabbed a pre-built location with an ebird code, store that
          // off to let us look up the state by stateCode.
          if (current.getEbirdCode() != null) {
            countryCode = current.getEbirdCode();
          }
        }
      } else if (!Strings.isNullOrEmpty(region)) {
        current = locationShortcuts.getRegion(region);
        if (current == null) {
          throw new IllegalStateException("Couldn't find region " + region);
        }
      } else {
        // No location information - return.  This once was illegal, but now Scythebill
        // allows entering location-free data.
        return null;
      }
    }
    
    // See if anyone is trying to import "Falkland Islands" into the UK
    if (current != null
        && "GB".equals(current.getEbirdCode())) {
      if ("Falkland Islands".equals(state)) {
        // ... and swap out the Falklands
        Location falklands = locations.getLocationByCode("FK");
        if (falklands != null) {
          current = falklands;
          state = null;
        }
      }
    }

    if (!Strings.isNullOrEmpty(stateCode) || !Strings.isNullOrEmpty(state)) {
      Location stateLoc = null;
      if (stateCode != null && countryCode != null) {
        stateLoc = locationShortcuts.getStateByCode(countryCode, countryCode + "-" + stateCode);
        if (stateLoc != null) {
          stateLoc = addLocationIfNecessary(locations, predefinedLocations, stateLoc);
        }
      }

      if (stateLoc == null) {
        String stateName = MoreObjects.firstNonNull(state, stateCode);
        stateLoc = current.getContent(stateName);
        if (stateLoc == null) {
          Location.Builder stateLocBuilder = Location.builder()
              .setParent(current)
              .setName(stateName)
              .setType(Type.state);
          stateLoc = addLocationIfNecessary(locations, predefinedLocations, stateLocBuilder.build());
        }
      }
      current = stateLoc;
    }

    if (!Strings.isNullOrEmpty(county)) {
      if (county.endsWith(" Co")) {
        String originalCounty = county;
        county = county.substring(0, county.length() - 3);
        // Don't defeat the "current location == parent" in locationNames
        if (!locationNames.isEmpty() && originalCounty.equals(locationNames.get(0))) {
          locationNames.remove(0);
        }
      }
      
      Location countyLoc = current.getContent(county);
      if (countyLoc == null) {
        countyLoc = Location.builder()
            .setParent(current)
            .setName(county)
            .setType(Type.county)
            .build();
        countyLoc = addLocationIfNecessary(locations, predefinedLocations, countyLoc);
      }
      current = countyLoc;
    }

    if (!Strings.isNullOrEmpty(city)) {
      Location cityLoc = current.getContent(city);
      if (cityLoc == null) {
        cityLoc = Location.builder()
            .setParent(current)
            .setName(city)
            .setType(Type.city)
            .build();
        cityLoc = addLocationIfNecessary(locations, predefinedLocations, cityLoc);
      }
      current = cityLoc;
    }
    
    for (String locationName : locationNames) {
      if (!Strings.isNullOrEmpty(locationName)
          && !locationName.equals(current.getModelName())) {
        Location location = current.getContent(locationName);
        if (location == null) {
          Location groupedLocationParent = getGroupedLocationParent(current, locationName);
          if (groupedLocationParent != null) {
            current = groupedLocationParent;
            location = current.getContent(locationName);
          }

          if (location == null) {
            location = Location.builder()
                .setParent(current)
                .setName(locationName)
                .build();
            location = addLocationIfNecessary(locations, predefinedLocations, location);
          }
        }
        current = location;
      }
    }
    
    locations.ensureAdded(current);
    
    // Set latitude and longitude if available, but only on non-built-in locations
    if (!current.isBuiltInLocation()) {
      LatLongCoordinates latLong = getLatLong();
      if (latLong != null) {
        Location withLatLong = current.asBuilder().setLatLong(latLong).build();
        reportSet.replaceLocation(current, withLatLong);
        current = withLatLong;
      }
    }
    
    if (!Strings.isNullOrEmpty(description) && !current.isBuiltInLocation()) {
      Location withDescription = current.asBuilder().setDescription(description).build();
      reportSet.replaceLocation(current, withDescription);
      current = withDescription;
    }
    
    return current.getId();
  }

  /**
   * With a name like "Presidio--Dragonfly Creek", see if there's a Presidio
   * location to add it inside of.
   */
  private Location getGroupedLocationParent(Location current, String locationName) {
    String groupedLocationPrefix = Locations.getGroupedLocationPrefix(locationName);
    if (groupedLocationPrefix == null) {
      return null;
    }

    return findChildMatchingName(current, groupedLocationPrefix);
  }

  /**
   * Attempts to add a location based on imported data using 
   * attached lat/long. Will return null if lat/long is missing
   * or cannot be resolved to a parent location, or there are no
   * parent locations. 
   */
  public String tryAddToLocationSetWithLatLong(
      ReportSet reportSet,
      LocationSet locations,
      LocationShortcuts locationShortcuts,
      PredefinedLocations predefinedLocations) {
    // Technically, *could* just import "somewhere in this country", but
    // I don't think that's desirable.
    if (locationNames.isEmpty()) {
      return null;
    }
    
    if (!setCodesUsingLatLong()) {
      return null;
    }
    
    return addToLocationSet(reportSet, locations, locationShortcuts, predefinedLocations);
  }
  
  private boolean setCodesUsingLatLong() {
    LatLongCoordinates latLong = getLatLong();
    if (latLong == null) {
      return false;
    }
    
    String eBirdCode = MapData.instance().getISOCode(latLong);
    if (eBirdCode == null) {
      return false;
    }
    List<String> codeList = Splitter.on('-').splitToList(eBirdCode);
    if (codeList.isEmpty()) {
      return false;
    }
      
    countryCode = codeList.get(0);
    if (codeList.size() > 1) {
      stateCode = codeList.get(1);
    }
    
    return true;
  }
  
  public LatLongCoordinates getLatLong() {
    if (Strings.isNullOrEmpty(longitude) || Strings.isNullOrEmpty(latitude)) {
      return null;
    }

    try {
      LatLongCoordinates latLong = LatLongCoordinates.withLatAndLong(latitude, longitude);
      // Some import systems use zeros as "lat/long missing".  Treat anything that is
      // precisely zero as such.
      if (latLong.latitudeAsDouble() == 0.0 &&  latLong.longitudeAsDouble() == 0.0) {
        return null;
      }
      
      return latLong;
    } catch (IllegalArgumentException e) {
      logger.warning(String.format("Could not parse lat/long: %s/%s",
          latitude, longitude));
      return null;
    }
  }

  /** 
   * Add a location - but only after checking if the location already
   * exists in its putative parent.
   */
  private Location addLocationIfNecessary(
      LocationSet locations,
      PredefinedLocations predefinedLocations,
      Location newLocation) {
    // In case the parent hasn't been added - do that.
    locations.ensureAdded(newLocation.getParent());
    
    Location foundChild = findChildMatchingName(newLocation.getParent(), newLocation.getModelName());
    if (foundChild != null) {
      return foundChild;
    }
    
    // In case the location would be a predefined location, grab that!
    PredefinedLocation predefinedLocation = predefinedLocations.getPredefinedLocationChild(
        newLocation.getParent(), newLocation.getModelName());
    if (predefinedLocation != null) {
      Location fromPredefined = predefinedLocation.create(locations, newLocation.getParent());
      locations.ensureAdded(fromPredefined);
      return fromPredefined;
    }
    
    locations.addLocation(newLocation);
    return newLocation;
  }
  
  /** Search for a named location anywhere in the tree of locations. */
  private Location findChildMatchingName(Location location, String name) {
    for (Location child : location.contents()) {
      // Either name or display name is OK, since this is called from
      // addLocationIfNecessary() and could find countries!
      if (child.getModelName().equals(name) || child.getDisplayName().equals(name)) {
        return child;
      }
      
      Location foundChild = findChildMatchingName(child, name);
      if (foundChild != null) {
        return foundChild;
      }
    }
    
    return null;
  }
}
