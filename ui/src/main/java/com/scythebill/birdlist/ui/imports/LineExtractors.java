/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.primitives.Floats;
import com.google.common.primitives.Ints;

/**
 * Implementation of various LineExtractor implementations.
 */
public class LineExtractors {
  public static LineExtractor<String> stringFromIndex(int index) {
    return new DirectStringExtractor(index);
  }

  public static LineExtractor<String> nullIfEmpty(LineExtractor<String> stringExtractor) {
    return line -> Strings.emptyToNull(stringExtractor.extract(line));
  }

  public static LineExtractor<Integer> intFromIndex(int index) {
    return new DirectIntegerExtractor(index);
  }

  public static LineExtractor<Boolean> booleanFromIndex(int index) {
    return new DirectBooleanExtractor(index);
  }

  public static LineExtractor<Float> floatFromIndex(int index) {
    return new DirectFloatExtractor(index);
  }

  public static <T> LineExtractor<T> alwaysNull() {
    return line -> null;
  }
  
  public static LineExtractor<String> joined(
      Joiner joiner,
      RowExtractor<String[], String> first,
      RowExtractor<String[], String> second) {    
    return line -> joiner.join(first.extract(line), second.extract(line));
  }

  @SafeVarargs
  public static LineExtractor<String> joined(
      final Joiner joiner,
      final RowExtractor<String[], String>... extractors) {
    return new LineExtractor<String>() {
      @Override public String extract(String[] line) {
        Object[] parameters = new Object[extractors.length];
        for (int i = 0; i < extractors.length; i++) {
          parameters[i] = extractors[i].extract(line);
        }
        return joiner.join(parameters);
      }
    };
  }

  @SafeVarargs
  public static LineExtractor<String> firstNonEmpty(
      final RowExtractor<String[], String>... extractors) {
    return new LineExtractor<String>() {
      @Override public String extract(String[] line) {
        for (int i = 0; i < extractors.length; i++) {
          String s = extractors[i].extract(line);
          if (!Strings.isNullOrEmpty(s)) {
            return s;
          }
        }
        return null;
      }
    };
  }

  public static LineExtractor<String> joined(
      final Joiner joiner,
      Iterable<RowExtractor<String[], String>> extractors) {
    final ImmutableList<RowExtractor<String[], String>> extractorsList =
        ImmutableList.copyOf(extractors);
    return new LineExtractor<String>() {
      @Override public String extract(String[] line) {
        Object[] parameters = new Object[extractorsList.size()];
        int i = 0;
        for (RowExtractor<String[], String> extractor : extractorsList) {
          parameters[i++] = extractor.extract(line);
        }
        return joiner.join(parameters);
      }
    };
  }
  
  static class DirectStringExtractor implements LineExtractor<String> {
    private final int index;
    DirectStringExtractor(int index) {
      this.index = index;
    }
    
    @Override public String extract(String[] line) {
      if (line.length <= index) {
        return null;
      }
      return CharMatcher.whitespace().trimFrom(line[index]);
    }    
  }

  static class DirectBooleanExtractor implements LineExtractor<Boolean> {
    private final int index;
    private final static ImmutableSet<String> TRUE_SET = ImmutableSet.of(
        "Y",
        "y",
        "T",
        "t",
        "TRUE",
        "True",
        "true",
        "1");
    
    DirectBooleanExtractor(int index) {
      this.index = index;
    }
    
    @Override public Boolean extract(String[] line) {
      if (line.length <= index) {
        return false;
      }
      String text = CharMatcher.whitespace().trimFrom(line[index]);
      return TRUE_SET.contains(text);
    }    
  }

  static class DirectIntegerExtractor implements LineExtractor<Integer> {
    private final int index;
    DirectIntegerExtractor(int index) {
      this.index = index;
    }
    @Override public Integer extract(String[] line) {
      if (line.length <= index) {
        return null;
      }
      String entry = CharMatcher.whitespace().trimFrom(line[index]);
      // And trim everything that isn't a digit before or after the number,
      // so at fields like "20+" or "~50" are properly kept. 
      entry = CharMatcher.inRange('0', '9').negate().trimFrom(entry);
      if (entry.isEmpty()) {
        return null;
      }
      if ("X".equalsIgnoreCase(entry)) {
        return null;
      }
      return Ints.tryParse(entry);
    }    
  }

  static private final CharMatcher DIGIT_OR_PERIOD =
      CharMatcher.inRange('0', '9').or(CharMatcher.is('.'));
  
  static class DirectFloatExtractor implements LineExtractor<Float> {
    private final int index;
    DirectFloatExtractor(int index) {
      this.index = index;
    }
    @Override public Float extract(String[] line) {
      if (line.length <= index) {
        return null;
      }
      
      String entry = DIGIT_OR_PERIOD.retainFrom(line[index]);
      if (entry.isEmpty()) {
        return null;
      }
      return Floats.tryParse(entry);
    }
  }

  /** Line extractor that always returns the same value. */
  public static <T> LineExtractor<T> constant(final T value) {
    return new LineExtractor<T>() {
      @Override
      public T extract(String[] line) {
        return value;
      }
    };
  }
  
  public static LineExtractor<String> appendingLatitudeAndLongitude(
      final LineExtractor<String> primaryText,
      final LineExtractor<String> latitudeExtractor,
      final LineExtractor<String> longitudeExtractor) {
    return new LineExtractor<String>() {
      @Override
      public String extract(String[] line) {
        String description = primaryText.extract(line);
        if (description == null) {
          description = "";
        }
        // Append latitude and longitude to the description
        // Before changing this, see EBirdCsvExport, which trims this back out before sending to eBird.
        String latitude = latitudeExtractor.extract(line);
        String longitude = longitudeExtractor.extract(line);
        if (!Strings.isNullOrEmpty(latitude) && !Strings.isNullOrEmpty(longitude)) {
          String latLong = String.format(
              description.isEmpty()
                  ? "LL:%s,%s" : "\nLL:%s,%s",
              trimOverlyPreciseLatOrLong(latitude),
              trimOverlyPreciseLatOrLong(longitude));
          description = description + latLong;
        }
        
        return description;
      }
    };
  }

  private static String trimOverlyPreciseLatOrLong(String latOrLong) {
    int decimalIndex = latOrLong.indexOf('.');
    if (decimalIndex < 0) {
      return latOrLong;
    }
    
    if (decimalIndex + 7 >= latOrLong.length()) {
      return latOrLong;
    }
    
    return latOrLong.substring(0, decimalIndex + 7);
  }
}
