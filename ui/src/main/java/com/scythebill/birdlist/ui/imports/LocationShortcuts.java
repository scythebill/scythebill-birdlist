/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import com.google.common.base.Function;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Ordering;
import com.google.common.collect.Table;
import com.google.common.collect.TreeBasedTable;
import com.google.common.collect.TreeMultimap;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;
import com.scythebill.birdlist.model.util.BKTree;
import com.scythebill.birdlist.model.util.Metrics;
import com.scythebill.birdlist.model.util.Trie;

/**
 * Shortctus for location importing.
 */
public class LocationShortcuts {
  private final Multimap<String, Location> countries = LinkedHashMultimap.create();
  private final Multimap<String, Location> allLocations = LinkedHashMultimap.create();
  private final Table<String, String, Location> statesByCode = HashBasedTable.create();
  private final Table<String, String, Location> statesByName = TreeBasedTable.create(
      Ordering.natural(), String.CASE_INSENSITIVE_ORDER);
  private final Map<String, Location> locationsByCode = Maps.newHashMap();
  private final ImmutableMultimap<String, String> ALTERNATE_NAMES =
      ImmutableMultimap.<String,String>builder()
      .putAll("GB", "Great Britain")
      .putAll("BQ", "Bonaire, Sint Eustatius and Saba")
      .putAll("CI", "Côte d'Ivoire")
      .putAll("CW", "Curaçao")
      .putAll("CZ", "Czechia")
      .putAll("CY", "Northern Cyprus")
      .putAll("FM", "Federated States of Micronesia")
      .putAll("MF", "Saint-Martin")
      .putAll("MK", "Macedonia")
      .putAll("MP", "Commonwealth of the Northern Mariana Islands")
      .putAll("PS", "Palestine")
      .putAll("SH-SH", "Saint Helena")
      .putAll("SX", "Sint Maarten")
      .putAll("VI", "United States Virgin Islands")
      .putAll("TF", "French Southern Territories")
      .putAll("MP", "Saint Pierre and Miquelon")
      .putAll("US", "United States of America")
      .build();
  /**
   * Instead of randomly choosing a region, prefer one for multi-region countries.  Ideally
   * this would be done by species, but that proves difficult.
   */
  private static final ImmutableMap<String, String> PREFERRED_PARENTS = ImmutableMap.of(
      "United States", "North America",
      "Egypt", "Africa",
      "Turkey", "Asia",
      "Indonesia", "Asia");
  private static final Ordering<Location> PREFERRED_PARENT_ORDERING = new Ordering<Location>() {
    @Override
    public int compare(Location left, Location right) {
      // Do comparisons here based on model name, since PREFERRED_PARENTS does so.
      String preferredParent = PREFERRED_PARENTS.get(left.getModelName());
      if (preferredParent != null) {
        if (preferredParent.equals(left.getParent().getModelName())) {
          return -1;
        } else if (preferredParent.equals(right.getParent().getModelName())) {
          return 1;
        }
      }
      return left.getParent().getModelName().compareTo(right.getParent().getModelName());
    }
  }; 
  private final Multimap<String, Location> exactCountryNames = TreeMultimap.create(
      Ordering.natural(),
      PREFERRED_PARENT_ORDERING);
  private final BKTree<String, Location> countryNames;
  private final BKTree<String, Location> regionNames;
  private final PredefinedLocations predefinedLocations;
  private int maxDistance = 3;
  private LocationSet locations;
  private static final Function<String, String> NORMALIZE_NAMES = s -> s == null ? null : Trie.normalizeString(s.toLowerCase());
  
  public LocationShortcuts(LocationSet locations, PredefinedLocations predefinedLocations) {
    this.locations = locations;
    this.predefinedLocations = predefinedLocations;
    BKTree.Builder<String, Location> countryNames = BKTree.<String, Location>builder(Metrics.levenshteinDistance())
        .withTransform(NORMALIZE_NAMES);
    BKTree.Builder<String, Location> regionNames = BKTree.<String, Location>builder(Metrics.levenshteinDistance())
        .withTransform(NORMALIZE_NAMES);
    buildMaps(locations.rootLocations(), countryNames, regionNames);
    
    // Add specific hacks to improve importing
//    Location unitedStates = locations.getLocationByCode("US");
//    countryNames.add("USA", unitedStates);
    for (String key : ALTERNATE_NAMES.keySet()) {
      Location location = getLocationFromEBirdCode(key);
      if (location != null) {
        for (String name : ALTERNATE_NAMES.get(key)) {
          countryNames.add(name, location);
        }
      }
    }
    
    this.countryNames = countryNames.build();
    this.regionNames = regionNames.build();
  }

  private void buildMaps(Iterable<Location> collection,
      BKTree.Builder<String, Location> countryNames,
      BKTree.Builder<String, Location> regionNames) {
    for (Location location : collection) {
      boolean hasDifferentDisplayName = !location.getModelName().equals(location.getDisplayName());
      allLocations.put(location.getModelName(), location);
      if (hasDifferentDisplayName) {
        allLocations.put(location.getDisplayName(), location);
      }
      if (location.getType() == Location.Type.country) {
        countryNames.add(location.getModelName(), location);
        exactCountryNames.put(location.getModelName(), location);
        if (hasDifferentDisplayName) {
          countryNames.add(location.getDisplayName(), location);
          exactCountryNames.put(location.getDisplayName(), location);
        }
        
        String locationCode = Locations.getLocationCode(location);
        if (locationCode != null) {
          locationsByCode.put(locationCode, location);
        }
        if (location.getEbirdCode() != null) {
          countries.put(location.getEbirdCode(), location);
        }
      } else {
        String locationCode = Locations.getLocationCode(location);
        if (locationCode != null) {
          locationsByCode.put(locationCode, location);
        }
      }

      // Store any states - or "country-in-country" - as states.
      // (This allows Puerto Rico to be properly imported when it's exported as a state)
      if (isStateOrCountryInCountry(location)
          && location.getEbirdCode() != null
          && location.getParent().getEbirdCode() != null) {
        statesByCode.put(
            Locations.getLocationCode(location.getParent()),
            Locations.getLocationCode(location),
            location);
        statesByName.put(location.getParent().getEbirdCode(), location.getModelName(),
            location);
        if (hasDifferentDisplayName) {
          statesByName.put(location.getParent().getEbirdCode(), location.getDisplayName(),
              location);
        }
      }
      
      if (location.getType() == Location.Type.region
          && location.isBuiltInLocation()) {
        regionNames.add(location.getModelName(), location);
        if (hasDifferentDisplayName) {
          regionNames.add(location.getDisplayName(), location);
        }
      }
      
      List<Location> extraPredefinedLocations = Lists.newArrayList();
      for (PredefinedLocation predefinedLocation : predefinedLocations.getPredefinedLocations(location)) {
        if (locations.getLocationByCode(predefinedLocation.getCode()) == null) {
          Location notYetAdded = predefinedLocation.create(locations, location);
          // It might *actually* have been added.
         // TODO:  why?!?  Not clear why this would ever get hit...
          if (notYetAdded.getId() != null) {
            locationsByCode.put(Locations.getLocationCode(notYetAdded), notYetAdded);
          } else {
            extraPredefinedLocations.add(notYetAdded);
          }
        }
      }

      buildMaps(Iterables.concat(location.contents(), extraPredefinedLocations), countryNames, regionNames);
    }
  }

  private boolean isStateOrCountryInCountry(Location location) {
    if (location.getType() == Location.Type.state) {
      return true;
    }
    
    if (location.getType() == Location.Type.country
        && location.getParent() != null
        && location.getParent().getType() == Location.Type.country) {
      return true;
    }
    
    return false;
  }

  /**
   * Return an exact-match country ID lookup.  If there's more than one item present,
   * use an optional "expected child" string to pick one. 
   */
  public Location getCountryFromCodeWithExpectedChild(String countryCode, @Nullable String expectedChild) {
    return replaceWithCreated(chooseByExpectedChild(countries.get(countryCode), expectedChild));
  }

  /**
   * Return an exact-match country ID lookup.  If there's more than one item present,
   * use an optional "expected child" string to pick one. 
   */
  public Location getCountryFromCodeWithExpectedParent(String countryCode, @Nullable String expectedParent) {
    return replaceWithCreated(chooseByExpectedParent(countries.get(countryCode), expectedParent));
  }


  public Location getLocationFromEBirdCode(String code) {
    return replaceWithCreated(locationsByCode.get(code));
  }

  public Location getStateByCode(String countryCode, String stateCode) {
    return replaceWithCreated(statesByCode.get(countryCode, stateCode));
  }

  public Location getStateByName(String countryCode, String stateName) {
    return replaceWithCreated(statesByName.get(countryCode, stateName));
  }

  public Location getRegion(String regionName) {
    return replaceWithCreated(searchAtGrowingDistances(regionNames, regionName));
  }
  
  public Collection<Location> getAnyLocationFromName(String name) {
    return allLocations.get(name);
  }
  
  /**
   * Return an exact-match country lookup.  If there's more than one item present,
   * use an optional "expected child" string to pick one. 
   */
  public Location getCountryExactMatch(String country, @Nullable Location optionalRegion, @Nullable String expectedChild) {
    if (optionalRegion != null) {
      Location countryInRegion = optionalRegion.getContent(country);
      if (countryInRegion != null) {
        return countryInRegion;
      }
    }
    return chooseByExpectedChild(exactCountryNames.get(country), expectedChild);
  }

  public Location getCountryInexactMatch(String country, @Nullable Location optionalRegion, @Nullable String expectedChild) {
    Location location = searchAtGrowingDistances(countryNames, country);
    if (location == null) {
      return null;
    }
    
    // Double-check against the region (and switch to an alternative if possible), if provided
    if (optionalRegion != null) {
      Location countryInRegion = optionalRegion.getContent(location.getModelName());
      if (countryInRegion != null) {
        location = countryInRegion;
      }
      return location;
    }
    
    // If the expected child doesn't line up, consider switching based to another with that correct name
    if (expectedChild != null
        && !hasExpectedChild(location, expectedChild)) {
      Location exactMatchWithChild =
          getCountryExactMatch(location.getModelName(), optionalRegion, expectedChild);
      if (exactMatchWithChild != null) {
        location = exactMatchWithChild;
      }
    }
    
    return location;
  }

  private Location searchAtGrowingDistances(
      BKTree<String, Location> tree, String name) {
    int maxDistanceForName = maxDistanceForName(name);
    for (int i = 0; i < maxDistanceForName; i++) {
      List<Location> list = tree.search(name, i);
      if (!list.isEmpty()) {
        return list.get(0);
      }
    }
    
    return null;
  }

  private int maxDistanceForName(String name) {
    // Don't change more than half the letters ... and subtract 1, so that two letter
    // codes end up supporting zero distance.
    return Math.min((name.length() - 1) / 2, maxDistance);
  }
  
  public void setMaxDistance(int maxDistance) {
    this.maxDistance  = maxDistance;
  }

  /**
   * Pick the first location with an optional "expected child".
   */
  private Location chooseByExpectedChild(
      Collection<Location> collection,
      @Nullable String expectedChild) {
    if (collection.isEmpty()) {
      return null;
    }
    
    // More than one, and an expected child?  Pick the first one that has a child with that name.
    if (collection.size() > 1 && expectedChild != null) {
      for (Location location : collection) {
        if (hasExpectedChild(location, expectedChild)) {
          return location;
        }
      }
    }
        
    // Give up, and pick the first such location.
    return collection.iterator().next();
  }
  
  /**
   * Pick the first location with an optional "expected parent".
   */
  private Location chooseByExpectedParent(
      Collection<Location> collection,
      @Nullable String expectedParent) {
    if (collection.isEmpty()) {
      return null;
    }
    
    // More than one, and an expected parent?  Pick the first one that has a parent with that name.
    if (collection.size() > 1 && expectedParent != null) {
      for (Location location : collection) {
        if (location.getParent() != null
            && (location.getParent().getModelName().equals(expectedParent)
                || location.getParent().getDisplayName().equals(expectedParent))) {
          return location;
        }
      }
    }
        
    // Give up, and pick the first such location.
    return collection.iterator().next();
  }
  

  private boolean hasExpectedChild(Location location, String expectedChild) {
    if (location.getContent(expectedChild) != null) {
      return true;
    }
    
    if (predefinedLocations.getPredefinedLocationChild(location, expectedChild) != null) {
      return true;
    }
    
    return false;
  }
  
  private Location replaceWithCreated(Location location) {
    if (location == null || location.getId() != null || location.getParent() == null) {
      return location;
    }
    
    Location oldParent = location.getParent();
    Location newParent = replaceWithCreated(oldParent);
    if (oldParent != newParent) {
      location = location.asBuilder().setParent(newParent).build();
    }
    
    Location existingChild = newParent.getContent(location.getModelName());
    return existingChild == null ? location : existingChild;
  }
}
