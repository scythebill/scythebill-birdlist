/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Supplier;

import org.joda.time.chrono.GJChronology;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.common.base.Strings;
import com.scythebill.birdlist.model.sighting.Sighting;

/**
 * Extracts a date from a single string, using any of N possible date formats. 
 */
public class MultiFormatDateFromStringFieldMapper<T> implements FieldMapper<T> {
  private final RowExtractor<T, String> extractor;
  private final Supplier<List<DateTimeFormatter>> dateTimeFormatters;

  public MultiFormatDateFromStringFieldMapper(RowExtractor<T, String> extractor, String... patterns) {
    this(extractor, Locale.getDefault(), patterns);
  }

  public MultiFormatDateFromStringFieldMapper(
      RowExtractor<T, String> extractor,
      Locale locale,
      String... patterns) {
    this.extractor = extractor;
    List<DateTimeFormatter> formatters = new ArrayList<>();
    for (int i = 0; i < patterns.length; i++) {
      formatters.add(DateTimeFormat.forPattern(patterns[i])
          .withPivotYear(2000)
          .withLocale(locale)
          .withChronology(GJChronology.getInstance()));
    }
    
    this.dateTimeFormatters = () -> formatters;
  }

  public MultiFormatDateFromStringFieldMapper(
      RowExtractor<T, String> extractor,
      Supplier<List<DateTimeFormatter>> dateTimeFormatters) {
    this.extractor = extractor;
    this.dateTimeFormatters = dateTimeFormatters;
  }
  
  @Override
  public void map(T line, Sighting.Builder sighting) {
    String date = Strings.emptyToNull(extractor.extract(line));
    if (date != null) {
      List<DateTimeFormatter> formatters = dateTimeFormatters.get();
      for (int i = 0; i < formatters.size(); i++) {
        try {
          // Try each format in sequence
          sighting.setDate(formatters.get(i).parseLocalDate(date));
          // If one succeeds, we're done
          return;
        } catch (IllegalArgumentException e) {
          // ... and ignore all but the last exception
          if (i == formatters.size() - 1) {
            throw new DateParseException(e);
          }
        }
      }
    }
  }

}
