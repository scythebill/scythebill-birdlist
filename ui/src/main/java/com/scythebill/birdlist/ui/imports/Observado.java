/*   
 * Copyright 2020 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.IOException;

import com.google.common.base.Charsets;
import com.google.common.io.CharSource;
import com.google.common.io.Files;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Taxonomy;

public class Observado {
  private Observado() {}
 
  /**
   * Examines {@code file} to see which importer is needed. 
   */
  public static SightingsImporter<?> newImporter(
      ReportSet reportSet, Taxonomy taxonomy,
      Checklists checklists, PredefinedLocations predefinedLocations, File file) {
    // Old format files were in UTF16 and started with a Byte Order Mark
    CharSource charSource = Files.asCharSource(file, Charsets.UTF_16LE);
    try {
      String firstLine = charSource.readFirstLine();
      if (firstLine != null && firstLine.startsWith("\ufeff")) {
        return new ObservadoImporter(reportSet, taxonomy, checklists, predefinedLocations, file);
      }
    } catch (IOException e) {
      // ignore
    }
    
    // Otherwise assume it's the new CSV format
    return new ObservadoNewImporter(reportSet, taxonomy, checklists, predefinedLocations, file);
  }
  

}
