/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.joda.time.ReadablePartial;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Table;
import com.opencsv.CSVReader;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.mapdata.MapData;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Photo;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.Sighting.Builder;
import com.scythebill.birdlist.model.sighting.SightingInfo.BreedingBirdCode;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Imports sightings from "classic" Observado and waarnemingen files. 
 */
public class ObservadoImporter extends CsvSightingsImporter {
  private final static Logger logger = Logger.getLogger(ObservadoImporter.class.getName());
  private final static Splitter LOCATION_SPLITTER = Splitter.on("- ").trimResults(CharMatcher.whitespace());
  private LineExtractor<String> idExtractor;
  private LineExtractor<String> taxonomyIdExtractor;
  private LineExtractor<String> commonNameExtractor;
  private LineExtractor<String> scientificExtractor;
  private LineExtractor<String> subspeciesExtractor;
  private LineExtractor<String> countryExtractor;
  private LineExtractor<String> stateExtractor;
  private LineExtractor<String> locationExtractor;
  private LineExtractor<String> speciesGroupExtractor;
  private LineExtractor<String> longitudeExtractor;
  private LineExtractor<String> latitudeExtractor;
  private boolean onlyBirds;

  public ObservadoImporter(ReportSet reportSet, Taxonomy taxonomy,
      Checklists checklists, PredefinedLocations predefinedLocations, File file) {
    super(reportSet, taxonomy, checklists, predefinedLocations, file, file);
    onlyBirds = taxonomy.isBuiltIn();
  }

  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return taxonomyIdExtractor;
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    return new FieldTaxonImporter<>(taxonomy, commonNameExtractor, scientificExtractor,
        subspeciesExtractor);
  }

  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    ImportLines lines = importLines(locationsFile);
    try {
      computeMappings(lines);
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }

        if (skipLine(line)) {
          continue;
        }
        
        try {
          String id = idExtractor.extract(line);
          if (locationIds.hasBeenParsed(id)) {
            continue;
          }

          ImportedLocation imported = new ImportedLocation();
          // In even of total failure, at least put the locations in Europe, since Observado users
          // are most likely there.
          imported.defaultRegion = "Europe";
          imported.longitude = Strings.emptyToNull(longitudeExtractor.extract(line));
          imported.latitude = Strings.emptyToNull(latitudeExtractor.extract(line));
          
          String country = countryExtractor.extract(line);
          if (country.endsWith(", The")) {
            country = country.substring(0, country.length() - 5);
          }
          
          // Detect imports from waarnemingen (.be or .nl)
          CountryAndState countryAndState = getCountryAndState(country);
          if (countryAndState != null) {
            imported.country = countryAndState.country;
            imported.stateCode = countryAndState.state;
            if (countryAndState.appendCounty) {
              imported.county = country;
            }
            
            String town = stateExtractor.extract(line);
            if (!Strings.isNullOrEmpty(town)) {
              imported.city = town;
            }
            
            String area = locationExtractor.extract(line);
            if (!Strings.isNullOrEmpty(area)) {
              if (!Strings.isNullOrEmpty(town)) {
                area = stripLocationPrefix(area, town);
              }
              imported.locationNames.add(area);
            }
          } else {
            imported.country = country;
            
            imported.state = stripParentheticalAbbrevation(stripLocationPrefix(
                stateExtractor.extract(line),
                imported.country));
            // Change "Açores / Azores" into "Azores" 
            int slashSeparatedIndex = imported.state.indexOf(" / ");
            if (slashSeparatedIndex > 0) {
              imported.state = imported.state.substring(slashSeparatedIndex + 3);
            }
            
            // Ah, Luxembourg.  Both a country name and a state name (of Belgium), and Observado
            // sticks 'em both in a Province field! If we've got a lat-long, see if we can
            // use the map data to extract which one it is.
            if ("Luxembourg".equals(imported.country)
                && imported.latitude != null
                && imported.longitude != null) {
              try {
                LatLongCoordinates latLong = LatLongCoordinates.withLatAndLong(imported.latitude, imported.longitude);
                String isoCode = MapData.instance().getISOCode(latLong);
                if (isoCode != null) {
                  switch (isoCode) {
                    case "BE":
                    case "BE-WAL":
                      imported.countryCode = "BE";
                      imported.country = null;
                      if (!Strings.isNullOrEmpty(imported.state)) {
                        imported.locationNames.add(imported.state);
                      }
                      imported.stateCode = "WAL";
                      imported.state = null;
                      imported.county = "Luxembourg";
                      break;
                    case "LU":
                      // This is fine, it's already a country
                      break;
                    default:
                      logger.log(Level.WARNING,
                          "Expected one of Belgium or Luxembourg, got {0} at {1}",
                          new Object[] {isoCode, latLong});
                      break;
                  }
                }
              } catch (IllegalArgumentException e) {
                // Ah well, this did not work
              }
              
            }
              
            
            // Look for entries that Scythebill treats as special "countries"
            String countryCode = COUNTRY_AND_STATE_TO_COUNTRY_CODE.get(imported.country, imported.state);
            if (countryCode != null) {
              imported.countryCode = countryCode;
              imported.country = null;
              imported.state = null;
            }
            
            // Strip the country and the state
            String locationNames =
                stripParentheticalAbbrevation(
                    stripLocationPrefix(
                        stripLocationPrefix(
                            locationExtractor.extract(line),
                            imported.country),
                        imported.state));
            imported.locationNames.addAll(LOCATION_SPLITTER.splitToList(locationNames));
          }

          String locationId = imported.addToLocationSet(reportSet, locations, locationShortcuts,
              predefinedLocations);
          locationIds.put(id, locationId);
        } catch (Exception e) {
          logger.log(Level.WARNING, "Failed to import location " + Joiner.on(',').join(line), e);
        }
      }
    } catch (IOException | RuntimeException e) {
      throw new ImportException(
          Messages.getFormattedMessage(Name.FAILED_IMPORT_ON_LINE_FORMAT, lines.lineNumber()),
          e);
    } finally {
      lines.close();
    }
  }
  
  private final CharMatcher SLASH = CharMatcher.is('/');
  private final Splitter SLASH_SPLITTER = Splitter.on(SLASH);
  
  private final CountryAndState getCountryAndState(String possibleProvince) {
    String stripped = STRIP_FOR_BELGIUM_AND_NETHERLANDS_LOOKUP.removeFrom(possibleProvince.toLowerCase());
    CountryAndState countryAndState = BELGIUM_AND_NETHERLANDS.get(stripped);
    if (countryAndState == null) {
      if (SLASH.matchesAnyOf(possibleProvince)) {
        for (String split : SLASH_SPLITTER.split(stripped)) {
          countryAndState = BELGIUM_AND_NETHERLANDS.get(split);
          if (countryAndState != null) {
            break;
          }
        }
      }
    }
    return countryAndState;
  }
  
  private final static CharMatcher STRIP_FOR_BELGIUM_AND_NETHERLANDS_LOOKUP = CharMatcher.anyOf(" -");
  
  private final static ImmutableMap<String, CountryAndState> BELGIUM_AND_NETHERLANDS =
      ImmutableMap.<String, CountryAndState>builder()
      .put("antwerp", countryAndState("Belgium", "VLG"))
      .put("antwerpen", countryAndState("Belgium", "VLG"))
      .put("anvers", countryAndState("Belgium", "VLG"))
      .put("eastflanders", countryAndState("Belgium", "VLG"))
      .put("oostvlaanderen", countryAndState("Belgium", "VLG"))
      .put("flandreorientale", countryAndState("Belgium", "VLG"))
      .put("vlaamsbrabant", countryAndState("Belgium", "VLG"))
      .put("flemishbrabant", countryAndState("Belgium", "VLG"))
      .put("brabantflamband", countryAndState("Belgium", "VLG"))
      // TODO: Limburg could also be southernmost Netherlands.  Argh.  Use lat-long
      // and reverse-geocoding?!?
      .put("limburg", countryAndState("Belgium", "VLG"))
      .put("limbourg", countryAndState("Belgium", "VLG"))
      .put("westflanders", countryAndState("Belgium", "VLG"))
      .put("westvlaanderen", countryAndState("Belgium", "VLG"))
      .put("flandreoccidentale", countryAndState("Belgium", "VLG"))
      .put("henegouwen", countryAndState("Belgium", "WAL"))
      .put("hainaut", countryAndState("Belgium", "WAL"))
      .put("luik", countryAndState("Belgium", "WAL"))
      .put("liège", countryAndState("Belgium", "WAL"))
      .put("lüttich", countryAndState("Belgium", "WAL"))
      .put("luxemburg", countryAndState("Belgium", "WAL"))
      .put("namen", countryAndState("Belgium", "WAL"))
      .put("namur", countryAndState("Belgium", "WAL"))
      .put("walloonbrabant", countryAndState("Belgium", "WAL"))
      .put("waalsbrabant", countryAndState("Belgium", "WAL"))
      .put("brabantwallon", countryAndState("Belgium", "WAL"))
      .put("brussel", countryAndStateNoProvince("Belgium", "BRU"))
      .put("brussels", countryAndStateNoProvince("Belgium", "BRU"))
      .put("bruxelles", countryAndStateNoProvince("Belgium", "BRU"))
      .put("brusselshoofdstedelijkgewest", countryAndStateNoProvince("Belgium", "BRU"))
      .put("régiondebruxellescapitale", countryAndStateNoProvince("Belgium", "BRU"))
      .put("drenthe", countryAndStateNoProvince("Netherlands", "DR"))
      .put("flevoland", countryAndStateNoProvince("Netherlands", "FL"))
      .put("fryslân", countryAndStateNoProvince("Netherlands", "FR"))
      .put("friesland", countryAndStateNoProvince("Netherlands", "FR"))
      .put("gelderland", countryAndStateNoProvince("Netherlands", "GE"))
      .put("groningen", countryAndStateNoProvince("Netherlands", "GR"))
      .put("grönnen", countryAndStateNoProvince("Netherlands", "GR"))
      .put("grinslân", countryAndStateNoProvince("Netherlands", "GR"))
      .put("noordbrabant", countryAndStateNoProvince("Netherlands", "NB"))
      .put("northbrabant", countryAndStateNoProvince("Netherlands", "NB"))
      .put("noordholland", countryAndStateNoProvince("Netherlands", "NH"))
      .put("northholland", countryAndStateNoProvince("Netherlands", "NH"))
      .put("overijssel", countryAndStateNoProvince("Netherlands", "OV"))
      .put("utrecht", countryAndStateNoProvince("Netherlands", "UT"))
      .put("zeeland", countryAndStateNoProvince("Netherlands", "ZE"))
      .put("zuidholland", countryAndStateNoProvince("Netherlands", "ZH"))
      .put("southholland", countryAndStateNoProvince("Netherlands", "ZH"))
      .build();
  
  private final static Table<String, String, String> COUNTRY_AND_STATE_TO_COUNTRY_CODE = ImmutableTable.<String, String, String>builder()
      .put("Portugal", "Azores", "PT-20")
      .put("Portugal", "Madeira", "PT-30")
      .put("Ecuador", "Galápagos", "EC-W")
      .put("Spain", "Santa Cruz de Tenerife", "ES-CN")
      .put("Spain", "Las Palmas", "ES-CN")
      .build();
  
  static CountryAndState countryAndState(String country, String state) {
    return new CountryAndState(country, state, true);
  }
  
  static CountryAndState countryAndStateNoProvince(String country, String state) {
    return new CountryAndState(country, state, false);
  }
  
  static class CountryAndState {
    final String country;
    final String state;
    final boolean appendCounty;
    
    CountryAndState(String country, String state, boolean appendCounty) {
      this.country = country;
      this.state = state; 
      this.appendCounty = appendCounty;
    }
  }

  /**
   * Trim abbreviations - like " (CA)" - at the end of a string.
   */
  private String stripParentheticalAbbrevation(String extracted) {
    // Ends with a right-paren?
    if (extracted.endsWith(")")) {
      // Has a left-paren?
      int lastLeftParen = extracted.lastIndexOf('(');
      if (lastLeftParen > 1) {
        // Trim off one more bit of whitespace if needed
        if (CharMatcher.whitespace().matches(extracted.charAt(lastLeftParen - 1))) {
          lastLeftParen--;
        }
        
        extracted = extracted.substring(0, lastLeftParen);
      }
    }
    
    return extracted;
  }

  /** 
   * Trim off countries from examples like "United States - San Francisco (CA)",
   * to just return "San Francisco (CA)".  {@link #stripParentheticalAbbrevation} will take
   * care of the rest.
   */
  private String stripLocationPrefix(String extracted, String country) {
    if (extracted == null) {
      return null;
    }
    if (country == null) {
      return extracted;
    }
    
    if (extracted.startsWith(country)) {
      extracted = extracted.substring(country.length());
    } else if (extracted.startsWith("BRD")) {
      extracted = extracted.substring(3);
    }
    
    if (extracted.startsWith(" - ")) {
      extracted = extracted.substring(3);
    } else if (extracted.startsWith(" -") || extracted.startsWith("- ")) {
      extracted = extracted.substring(2);
    } else if (extracted.startsWith("-")) {
      extracted = extracted.substring(1);
    }
    
    return extracted;
  }

  static class DropSspFromCommon implements LineExtractor<String> {
    private final LineExtractor<String> extractor;

    public DropSspFromCommon(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public String extract(String[] line) {
      String name = extractor.extract(line);
      int indexOf = name.indexOf(" ssp ");
      if (indexOf > 0) {
        name = name.substring(0, indexOf);
      }
      return name;
    }
  }
  
  /** Take a trinomial and return the first two parts. */
  static class DropSspFromSci implements LineExtractor<String> {
    private final LineExtractor<String> extractor;

    public DropSspFromSci(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public String extract(String[] line) {
      String name = extractor.extract(line);
      if (name == null) {
        return null;
      }
      // Don't try trimming "slash" names - they've got an extra space, but that's for the sp.
      if (name.indexOf('/') >= 0) {
        return name;
      }
      
      int firstSpace = name.indexOf(' ');
      if (firstSpace >= 0) {
        int nextSpace = name.indexOf(' ', firstSpace + 1);
        if (nextSpace >= 0) {
          return name.substring(0, nextSpace);
        }
      }
      return name;
    }
  }

  /** Take a trinomial and return the first two parts. */
  static class ExtractSspFromSci implements LineExtractor<String> {
    private final LineExtractor<String> extractor;

    public ExtractSspFromSci(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public String extract(String[] line) {
      String name = extractor.extract(line);
      if (name == null) {
        return null;
      }
      int firstSpace = name.indexOf(' ');
      if (firstSpace >= 0) {
        int nextSpace = name.indexOf(' ', firstSpace + 1);
        if (nextSpace >= 0) {
          return name.substring(nextSpace + 1);
        }
      }
      return null;
    }
  }

  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    String[] header = lines.nextLine();
    Map<String, Integer> headersByIndex = Maps.newHashMap();
    for (int i = 0; i < header.length; i++) {
      headersByIndex.put(CharMatcher.whitespace().removeFrom(header[i]).toLowerCase(), i);
    }

    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
    Integer commonIndex = findIndex(headersByIndex, "name", "naam", "nom");
    Integer sciIndex = findIndex(headersByIndex, "scientificname", "wetenschappelijkenaam", "nomscientifique");
    Integer dateIndex = findIndex(headersByIndex, "date", "datum");
    Integer numberIndex = findIndex(headersByIndex, "number", "aantal", "nombre");
    Integer sexIndex = findIndex(headersByIndex, "sex", "geslacht", "sexe");
    Integer plumageIndex = findIndex(headersByIndex, "plumage", "kleed");
    Integer descriptionIndex = findIndex(headersByIndex, "remarks", "toelichting", "remarques");
    Integer countryIndex = findIndex(headersByIndex, "province", "provincie");
    Integer stateIndex = findIndex(headersByIndex, "municipal", "gemeente", "commune");
    Integer locationIndex = findIndex(headersByIndex, "area", "gebied", "site");
    Integer speciesGroupIndex = findIndex(headersByIndex, "speciesgroup", "soortgroep", "groupetaxonomique");
    Integer certainIndex = findIndex(headersByIndex, "certain", "zeker");
    Integer exoticIndex = findIndex(headersByIndex, "exotic", "escape", "échappédecaptivité");
    Integer activityIndex = findIndex(headersByIndex, "activity", "gedrag");
    Integer latitudeIndex = findIndex(headersByIndex, "latitude", "lat");
    Integer longitudeIndex = findIndex(headersByIndex, "longitude", "lon", "lng");
    Integer photosIndex = findIndex(headersByIndex, "photos");
    Integer linkIndex = findIndex(headersByIndex, "link");
    
    // Create a taxonomy extractor from the located common/scientific/ssp.
    // columns
    commonNameExtractor = commonIndex == null ? LineExtractors.<String> alwaysNull()
        : new DropSspFromCommon(LineExtractors.stringFromIndex(commonIndex));
    scientificExtractor = sciIndex == null ? LineExtractors.<String> alwaysNull()
        : new DropSspFromSci(LineExtractors.stringFromIndex(sciIndex));
    subspeciesExtractor = (sciIndex == null) ? LineExtractors.<String> alwaysNull()
        : new ExtractSspFromSci(LineExtractors.stringFromIndex(sciIndex));
    taxonomyIdExtractor = LineExtractors.joined(Joiner.on('|').useForNull(""),
        ImmutableList.of(commonNameExtractor, scientificExtractor, subspeciesExtractor));

    // Extract the specifically named location columns
    countryExtractor = LineExtractors.stringFromIndex(countryIndex);
    stateExtractor = stateIndex == null ? LineExtractors.<String> alwaysNull() : LineExtractors
        .stringFromIndex(stateIndex);
    locationExtractor = locationIndex == null ? LineExtractors.<String> alwaysNull() : LineExtractors
        .stringFromIndex(locationIndex);
    longitudeExtractor = longitudeIndex == null 
        ? LineExtractors.<String> alwaysNull()
        : LineExtractors.stringFromIndex(longitudeIndex); 
    latitudeExtractor = latitudeIndex == null 
        ? LineExtractors.<String> alwaysNull()
        : LineExtractors.stringFromIndex(latitudeIndex); 
    List<RowExtractor<String[], String>> allLocationExtractors = Lists.newArrayList();
    allLocationExtractors.add(countryExtractor);
    allLocationExtractors.add(stateExtractor);
    allLocationExtractors.add(locationExtractor);

    speciesGroupExtractor = LineExtractors.stringFromIndex(speciesGroupIndex);
    
    // Join those to form a location key. Note that not all rows
    // will have all location columns, so this Joiner must not blow
    // up on null.
    idExtractor = LineExtractors.joined(Joiner.on('|').useForNull(""), allLocationExtractors);

    mappers.add(new ScythebillDateMapper(LineExtractors.stringFromIndex(dateIndex)));
    if (numberIndex != null) {
      mappers.add(new CountFieldMapper<>(LineExtractors.stringFromIndex(numberIndex)));
    }
    LineExtractor<String> commentExtractor = descriptionIndex == null
        ? LineExtractors.constant("")
        : new StripQuotes(LineExtractors.stringFromIndex(descriptionIndex));
    mappers.add(new DescriptionFieldMapper<>(
        LineExtractors.appendingLatitudeAndLongitude(
            commentExtractor, latitudeExtractor, longitudeExtractor)));
    
    if (sexIndex != null) {
      mappers.add(new SexMapper(LineExtractors.stringFromIndex(sexIndex)));
    }
    if (plumageIndex != null) {
      mappers.add(new PlumageMapper(LineExtractors.stringFromIndex(plumageIndex)));
    }
    if (activityIndex != null) {
      mappers.add(new ActivityMapper(LineExtractors.stringFromIndex(activityIndex)));
    }
    if (photosIndex != null) {
      LineExtractor<String> linkExtractor =
          linkIndex == null
          ? LineExtractors.alwaysNull()
          : LineExtractors.stringFromIndex(linkIndex);
      mappers.add(new PhotosMapper(
          LineExtractors.intFromIndex(photosIndex),
          linkExtractor));
    }

    LineExtractor<String> exoticExtractor = exoticIndex == null
        ? LineExtractors.<String>alwaysNull()
        : LineExtractors.stringFromIndex(exoticIndex);
    LineExtractor<String> certainExtractor = certainIndex == null
        ? LineExtractors.<String>alwaysNull()
        : LineExtractors.stringFromIndex(certainIndex);
    mappers.add(new StatusMapper(certainExtractor, exoticExtractor));
    
    return new ComputedMappings<>(
        new TaxonFieldMapper(taxonomyIdExtractor),
        new LocationMapper(idExtractor),
        mappers);
  }

  private Integer findIndex(Map<String, Integer> headersByIndex, String... names) {
    for (String name : names) {
      Integer index = headersByIndex.get(name);
      if (index != null) {
        return index;
      }
    }
    return null;
  }

  /**
   * Extract Scythebill dates using its internal formatter, which supports
   * missing days and months.
   */
  static class ScythebillDateMapper implements FieldMapper<String[]> {
    private LineExtractor<String> extractor;

    ScythebillDateMapper(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public void map(String[] line, Builder sighting) {
      String date = Strings.emptyToNull(extractor.extract(line));
      if (date != null) {
        ReadablePartial datePartial = PartialIO.fromString(date);
        sighting.setDate(datePartial);
      }
    }
  }

  static class SexMapper implements FieldMapper<String[]> {
    private final LineExtractor<String> extractor;

    public SexMapper(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override public void map(String[] line, Sighting.Builder sighting) {
      String extracted = extractor.extract(line);
      if (extracted != null) {
        extracted = extracted.toLowerCase();
        if ("male".equals(extracted)
            || "man".equals(extracted)
            || "mâle".equals(extracted)) {
          sighting.getSightingInfo().setMale(true);
        } else if ("female".equals(extracted)
            || "vrouw".equals(extracted)
            || "femelle".equals(extracted)) {
          sighting.getSightingInfo().setFemale(true);
        } else if ("pair".equalsIgnoreCase(extracted)) {
          // TODO: Dutch for "pair"
          sighting.getSightingInfo().setMale(true);
          sighting.getSightingInfo().setFemale(true);
        }
      }
    }
  }

  static class PlumageMapper implements FieldMapper<String[]> {
    private final LineExtractor<String> extractor;

    public PlumageMapper(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override public void map(String[] line, Sighting.Builder sighting) {
      String extracted = extractor.extract(line);
      if (extracted != null) {
        extracted = extracted.toLowerCase();
        if (extracted.contains("adult")) {
          sighting.getSightingInfo().setAdult(true);
        } else if ("immature".equals(extracted)
            || "onvolwassen".equals(extracted)
            || "juvenile".equals(extracted)
            || "juveniel".equals(extracted)) {
          sighting.getSightingInfo().setImmature(true);
        }
      }
    }
  }

  static class ActivityMapper implements FieldMapper<String[]> {
    private final LineExtractor<String> extractor;

    public ActivityMapper(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override public void map(String[] line, Sighting.Builder sighting) {
      String extracted = extractor.extract(line);
      if (extracted != null) {
        extracted = extracted.toLowerCase();
        // TODO: Dutch for these
        if ("territorial behaviour".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.TERRITORIAL_DEFENSE);
        } else if ("occupied nest".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.OCCUPIED_NEST);
        } else if ("pair in breeding area".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.PAIR_IN_SUITABLE_HABITAT);
        } else if ("adult in breeding area".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.IN_APPROPRIATE_HABITAT);
        } else if ("occupied nest with eggs".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.NEST_WITH_EGGS);
        } else if ("occupied nest with young".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.NEST_WITH_YOUNG);
        } else if ("probable nesting location".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.VISITING_PROBABLE_NEST_SITE);
        } else if ("pair courtship and or mating".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.COURTSHIP_DISPLAY_OR_COPULATION);
        } else if ("nest building".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.NEST_BUILDING);
        } else if ("recently fletched young".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.RECENTLY_FLEDGED);
        } else if ("bird with brood patch".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.PHYSIOLOGICAL_EVIDENCE);
        } else if ("transport of food or faeces".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.CARRYING_FOOD);
        }
      }
    }
  }

  static class StatusMapper implements FieldMapper<String[]> {
    private final LineExtractor<String> certainExtractor;
    private final LineExtractor<String> exoticExtractor;

    public StatusMapper(
        LineExtractor<String> certainExtractor,
        LineExtractor<String> exoticExtractor) {
      this.certainExtractor = certainExtractor;
      this.exoticExtractor = exoticExtractor;
    }

    @Override public void map(String[] line, Sighting.Builder sighting) {
      String certain = certainExtractor.extract(line);
      if ("N".equals(certain)) {
        sighting.getSightingInfo().setSightingStatus(SightingStatus.ID_UNCERTAIN);
      } else {
        String exotic = exoticExtractor.extract(line);
        if ("Y".equals(exotic)) {
          sighting.getSightingInfo().setSightingStatus(SightingStatus.INTRODUCED_NOT_ESTABLISHED);
        }
      }
    }
  }

  static class PhotosMapper implements FieldMapper<String[]> {
    private LineExtractor<Integer> photoCountExtractor;
    private LineExtractor<String> linkExtractor;

    public PhotosMapper(
        LineExtractor<Integer> photoCountExtractor,
        LineExtractor<String> linkExtractor) {
      this.photoCountExtractor = photoCountExtractor;
      this.linkExtractor = linkExtractor;
    }

    @Override
    public void map(String[] line, Sighting.Builder sighting) {
      Integer photoCount = photoCountExtractor.extract(line);
      if (photoCount != null && photoCount.intValue() > 0) {
        // If there's > 0 photos, then set the photographed bit
        sighting.getSightingInfo().setPhotographed(true);
        String link = linkExtractor.extract(line);
        // And try to parse the link as a photo URL
        if (!Strings.isNullOrEmpty(link)) {
          try {
            URI linkUri = new URI(link);
            Photo photo = new Photo(linkUri);
            sighting.getSightingInfo().setPhotos(ImmutableList.of(photo));
          } catch (URISyntaxException e) {
            // Ignore, failed to parse the link
            logger.warning("Could not parse link " + link);
          }
        }
      }
    }
    
  }
  
  static class StripQuotes implements LineExtractor<String> {
    private final CharMatcher QUOTE = CharMatcher.is('"');
    private final LineExtractor<String> extractor;

    public StripQuotes(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public String extract(String[] line) {
      String extracted = extractor.extract(line);
      if (extracted == null) {
        return extracted;
      }
      
      return QUOTE.trimFrom(extracted);
    }

  }

  @Override
  protected ImportLines importLines(File file) throws IOException {
    final CSVReader csvReader = new CSVReader(
        new InputStreamReader(
            new BufferedInputStream(new FileInputStream(file)), Charsets.UTF_16LE),
        '\t',
        '"',
        false);
    return CsvImportLines.fromReader(csvReader);
  }
  
  @Override
  protected boolean skipLine(String[] line) {
    if (onlyBirds) {
      String extracted = speciesGroupExtractor.extract(line);
      if (!Strings.isNullOrEmpty(extracted) && !"birds".equalsIgnoreCase(extracted)) {
        // Not a bird - skip.
        return true;
      }
    }
    return false;
  }
}
