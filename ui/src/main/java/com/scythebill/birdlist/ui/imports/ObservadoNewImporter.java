/*   
 * Copyright 2020 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.joda.time.ReadablePartial;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.Sighting.Builder;
import com.scythebill.birdlist.model.sighting.SightingInfo.BreedingBirdCode;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Imports sightings from "new" Observado and waarnemingen files after the site
 * redesign. 
 */
public class ObservadoNewImporter extends CsvSightingsImporter {
  private final static Logger logger = Logger.getLogger(ObservadoNewImporter.class.getName());
  private LineExtractor<String> idExtractor;
  private LineExtractor<String> taxonomyIdExtractor;
  private LineExtractor<String> commonNameExtractor;
  private LineExtractor<String> scientificExtractor;
  private LineExtractor<String> subspeciesExtractor;
  private LineExtractor<String> locationExtractor;
  private LineExtractor<String> countryExtractor;
  private LineExtractor<String> stateExtractor;
  private LineExtractor<String> speciesGroupExtractor;
  private LineExtractor<String> longitudeExtractor;
  private LineExtractor<String> latitudeExtractor;
  private boolean onlyBirds;

  public ObservadoNewImporter(ReportSet reportSet, Taxonomy taxonomy,
      Checklists checklists, PredefinedLocations predefinedLocations, File file) {
    super(reportSet, taxonomy, checklists, predefinedLocations, file, file);
    onlyBirds = taxonomy.isBuiltIn();
  }

  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return taxonomyIdExtractor;
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    return new FieldTaxonImporter<>(taxonomy, commonNameExtractor, scientificExtractor,
        subspeciesExtractor);
  }

  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    ImportLines lines = importLines(locationsFile);
    try {
      computeMappings(lines);
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }

        if (skipLine(line)) {
          continue;
        }
        
        try {
          String id = idExtractor.extract(line);
          if (locationIds.hasBeenParsed(id)) {
            continue;
          }

          ImportedLocation imported = new ImportedLocation();
          String country = countryExtractor.extract(line);
          if (!Strings.isNullOrEmpty(country)) {
            imported.country = country;
          }

          String state = stateExtractor.extract(line);
          if (!Strings.isNullOrEmpty(state)) {
            imported.state = state;
          }
          
          String location = locationExtractor.extract(line);
          if (!Strings.isNullOrEmpty(location)) {
            Splitter locationSplitter = Splitter.on('-').trimResults(CharMatcher.whitespace());
            List<String> locationsList = locationSplitter.splitToList(location);
            // Older versions of observation.org would put the country in the location name.
            if (locationsList.size() > 1 && Strings.isNullOrEmpty(imported.country)) {
              imported.country = locationsList.get(0);
              imported.locationNames = new ArrayList<>(locationsList.subList(1, locationsList.size()));
            } else {
              imported.locationNames = new ArrayList<>(locationsList);
            }
          }
          
          if (!imported.locationNames.isEmpty()) {
            // Look for a location name that is of the form "Somewhere (CA)"; turn these
            // into a location and a state.
            String locationName = imported.locationNames.get(0);
            Pattern abbreviationPattern = Pattern.compile("(.*) \\((.*)\\)");
            Matcher matcher = abbreviationPattern.matcher(locationName);
            if (matcher.matches()) {
              imported.locationNames.set(0, matcher.group(1));
              if (Strings.isNullOrEmpty(imported.state)) {
                String stateFromLocation = matcher.group(2);
                if (CharMatcher.inRange('A', 'Z').matchesAllOf(stateFromLocation)) {
                  imported.stateCode = stateFromLocation;
                } else {
                  imported.state = stateFromLocation;
                }
              }
            }
          }
          
          imported.longitude = Strings.emptyToNull(longitudeExtractor.extract(line));
          imported.latitude = Strings.emptyToNull(latitudeExtractor.extract(line));
          
          String locationId = imported.addToLocationSet(reportSet, locations, locationShortcuts,
              predefinedLocations);
          locationIds.put(id, locationId);
        } catch (Exception e) {
          logger.log(Level.WARNING, "Failed to import location " + Joiner.on(',').join(line), e);
        }
      }
    } catch (IOException | RuntimeException e) {
      throw new ImportException(
          Messages.getFormattedMessage(Name.FAILED_IMPORT_ON_LINE_FORMAT, lines.lineNumber()),
          e);
    } finally {
      lines.close();
    }
  }
  

  static class DropSspFromCommon implements LineExtractor<String> {
    private final LineExtractor<String> extractor;

    public DropSspFromCommon(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public String extract(String[] line) {
      String name = extractor.extract(line);
      int indexOf = name.indexOf(" ssp ");
      if (indexOf > 0) {
        name = name.substring(0, indexOf);
      }
      return name;
    }
  }
  
  /** Take a trinomial and return the first two parts. */
  static class DropSspFromSci implements LineExtractor<String> {
    private final LineExtractor<String> extractor;

    public DropSspFromSci(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public String extract(String[] line) {
      String name = extractor.extract(line);
      if (name == null) {
        return null;
      }
      // Don't try trimming "slash" names - they've got an extra space, but that's for the sp.
      if (name.indexOf('/') >= 0) {
        return name;
      }
      
      int firstSpace = name.indexOf(' ');
      if (firstSpace >= 0) {
        int nextSpace = name.indexOf(' ', firstSpace + 1);
        if (nextSpace >= 0) {
          return name.substring(0, nextSpace);
        }
      }
      return name;
    }
  }

  /** Take a trinomial and return the first two parts. */
  static class ExtractSspFromSci implements LineExtractor<String> {
    private final LineExtractor<String> extractor;

    public ExtractSspFromSci(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public String extract(String[] line) {
      String name = extractor.extract(line);
      if (name == null) {
        return null;
      }
      int firstSpace = name.indexOf(' ');
      if (firstSpace >= 0) {
        int nextSpace = name.indexOf(' ', firstSpace + 1);
        if (nextSpace >= 0) {
          return name.substring(nextSpace + 1);
        }
      }
      return null;
    }
  }

  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    String[] header = lines.nextLine();
    Map<String, Integer> headersByIndex = Maps.newHashMap();
    for (int i = 0; i < header.length; i++) {
      headersByIndex.put(CharMatcher.whitespace().removeFrom(header[i]).toLowerCase(), i);
    }

    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
    Integer commonIndex = findIndex(headersByIndex, "speciesname");
    Integer sciIndex = findIndex(headersByIndex, "scientificname");
    Integer dateIndex = findIndex(headersByIndex, "date");
    Integer numberIndex = findIndex(headersByIndex, "number");
    Integer sexIndex = findIndex(headersByIndex, "sex");
    Integer lifeStageIndex = findIndex(headersByIndex, "plumage", "lifestage");
    Integer descriptionIndex = findIndex(headersByIndex, "notes");
    Integer methodIndex = findIndex(headersByIndex, "method");
    Integer countryIndex = findIndex(headersByIndex, "country");
    Integer stateIndex = findIndex(headersByIndex, "countrydivision");
    Integer locationIndex = findIndex(headersByIndex, "location");
    Integer speciesGroupIndex = findIndex(headersByIndex, "speciesgroup");
    Integer certainIndex = findIndex(headersByIndex, "iscertain");
    Integer isEscapeIndex = findIndex(headersByIndex, "isescape");
    Integer activityIndex = findIndex(headersByIndex, "activity");
    Integer latitudeIndex = findIndex(headersByIndex, "lat");
    Integer longitudeIndex = findIndex(headersByIndex, "lng");
    Integer hasphotosIndex = findIndex(headersByIndex, "hasphotos");
    
    // Create a taxonomy extractor from the located common/scientific/ssp.
    // columns
    commonNameExtractor = commonIndex == null ? LineExtractors.<String> alwaysNull()
        : new DropSspFromCommon(LineExtractors.stringFromIndex(commonIndex));
    scientificExtractor = sciIndex == null ? LineExtractors.<String> alwaysNull()
        : new DropSspFromSci(LineExtractors.stringFromIndex(sciIndex));
    subspeciesExtractor = (sciIndex == null) ? LineExtractors.<String> alwaysNull()
        : new ExtractSspFromSci(LineExtractors.stringFromIndex(sciIndex));
    taxonomyIdExtractor = LineExtractors.joined(Joiner.on('|').useForNull(""),
        ImmutableList.of(commonNameExtractor, scientificExtractor, subspeciesExtractor));

    // Extract the specifically named location columns
    locationExtractor = locationIndex == null ? LineExtractors.<String> alwaysNull() :
        LineExtractors.stringFromIndex(locationIndex);
    countryExtractor = locationIndex == null ? LineExtractors.<String>alwaysNull()
        : LineExtractors.stringFromIndex(countryIndex);
    stateExtractor = locationIndex == null ? LineExtractors.<String>alwaysNull()
        : LineExtractors.stringFromIndex(stateIndex);
    longitudeExtractor = longitudeIndex == null 
        ? LineExtractors.<String> alwaysNull()
        : LineExtractors.stringFromIndex(longitudeIndex); 
    latitudeExtractor = latitudeIndex == null 
        ? LineExtractors.<String> alwaysNull()
        : LineExtractors.stringFromIndex(latitudeIndex); 

    speciesGroupExtractor = LineExtractors.stringFromIndex(speciesGroupIndex);
    
    // Join those to form a location key. Note that not all rows
    // will have all location columns, so this Joiner must not blow
    // up on null.
    idExtractor = LineExtractors.joined(Joiner.on('|').useForNull(""),
        ImmutableList.of(locationExtractor, stateExtractor, countryExtractor));

    mappers.add(new ScythebillDateMapper(LineExtractors.stringFromIndex(dateIndex)));
    if (numberIndex != null) {
      mappers.add(new CountFieldMapper<>(LineExtractors.stringFromIndex(numberIndex)));
    }
    LineExtractor<String> commentExtractor = descriptionIndex == null
        ? LineExtractors.constant("")
        : new StripQuotes(LineExtractors.stringFromIndex(descriptionIndex));
    mappers.add(new DescriptionFieldMapper<>(
        LineExtractors.appendingLatitudeAndLongitude(
            commentExtractor, latitudeExtractor, longitudeExtractor)));
    
    if (sexIndex != null) {
      mappers.add(new SexMapper(LineExtractors.stringFromIndex(sexIndex)));
    }
    if (lifeStageIndex != null) {
      mappers.add(new LifeStageMapper(LineExtractors.stringFromIndex(lifeStageIndex)));
    }
    if (activityIndex != null) {
      mappers.add(new ActivityMapper(LineExtractors.stringFromIndex(activityIndex)));
    }
    if (hasphotosIndex != null) {
      mappers.add(new PhotographedFieldMapper<>(LineExtractors.booleanFromIndex(hasphotosIndex)));
    }

    LineExtractor<Boolean> isEscapeExtractor = isEscapeIndex == null
        ? LineExtractors.constant(false)
        : LineExtractors.booleanFromIndex(isEscapeIndex);
    // Use a String extractor instead of a boolean extractor;  the boolean extractor
    // defaults to "false" if nothing is met, which is ... not good here!
    LineExtractor<String> certainExtractor = certainIndex == null
        ? LineExtractors.constant("True")
        : LineExtractors.stringFromIndex(certainIndex);
    mappers.add(new StatusMapper(certainExtractor, isEscapeExtractor));
    
    if (methodIndex != null) {
      LineExtractor<String> methodExtractor = LineExtractors.stringFromIndex(methodIndex);
      mappers.add(new FieldMapper<String[]>() {
        @Override
        public void map(String[] line, Builder sighting) {
          if ("heard".equalsIgnoreCase(methodExtractor.extract(line))) {
            sighting.getSightingInfo().setHeardOnly(true);
          }
        }
      });
    }
    return new ComputedMappings<>(
        new TaxonFieldMapper(taxonomyIdExtractor),
        new LocationMapper(idExtractor),
        mappers);
  }

  private Integer findIndex(Map<String, Integer> headersByIndex, String... names) {
    for (String name : names) {
      Integer index = headersByIndex.get(name);
      if (index != null) {
        return index;
      }
    }
    return null;
  }

  /**
   * Extract Scythebill dates using its internal formatter, which supports
   * missing days and months.
   */
  static class ScythebillDateMapper implements FieldMapper<String[]> {
    private LineExtractor<String> extractor;

    ScythebillDateMapper(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public void map(String[] line, Builder sighting) {
      String date = Strings.emptyToNull(extractor.extract(line));
      if (date != null) {
        ReadablePartial datePartial = PartialIO.fromString(date);
        sighting.setDate(datePartial);
      }
    }
  }

  static class SexMapper implements FieldMapper<String[]> {
    private final LineExtractor<String> extractor;

    public SexMapper(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override public void map(String[] line, Sighting.Builder sighting) {
      String extracted = extractor.extract(line);
      if (extracted != null) {
        extracted = extracted.toLowerCase();
        if ("m".equals(extracted)) {
          sighting.getSightingInfo().setMale(true);
        } else if ("f".equals(extracted)) {
          sighting.getSightingInfo().setFemale(true);
        } else if ("pair".equalsIgnoreCase(extracted)) {
          // TODO: Dutch for "pair"
          sighting.getSightingInfo().setMale(true);
          sighting.getSightingInfo().setFemale(true);
        }
      }
    }
  }

  static class LifeStageMapper implements FieldMapper<String[]> {
    private final LineExtractor<String> extractor;

    public LifeStageMapper(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override public void map(String[] line, Sighting.Builder sighting) {
      String extracted = extractor.extract(line);
      if (extracted != null) {
        extracted = extracted.toLowerCase();
        if (extracted.contains("adult")) {
          sighting.getSightingInfo().setAdult(true);
        } else if ("immature".equals(extracted)
            || "onvolwassen".equals(extracted)
            || "juvenile".equals(extracted)
            || "juveniel".equals(extracted)
            || "first winter".equals(extracted)) {
          sighting.getSightingInfo().setImmature(true);
        } else if ("nestling".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.NEST_WITH_YOUNG);
        }
      }
    }
  }

  static class ActivityMapper implements FieldMapper<String[]> {
    private final LineExtractor<String> extractor;

    public ActivityMapper(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override public void map(String[] line, Sighting.Builder sighting) {
      String extracted = extractor.extract(line);
      if (extracted != null) {
        extracted = extracted.toLowerCase();
        if ("territorial behaviour".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.TERRITORIAL_DEFENSE);
        } else if ("occupied nest".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.OCCUPIED_NEST);
        } else if ("pair in breeding area".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.PAIR_IN_SUITABLE_HABITAT);
        } else if ("adult in breeding area".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.IN_APPROPRIATE_HABITAT);
        } else if ("occupied nest with eggs".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.NEST_WITH_EGGS);
        } else if ("occupied nest with young".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.NEST_WITH_YOUNG);
        } else if ("probable nesting location".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.VISITING_PROBABLE_NEST_SITE);
        } else if ("pair courtship and or mating".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.COURTSHIP_DISPLAY_OR_COPULATION);
        } else if ("courtship/singing".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.SINGING_BIRD);
        } else if ("flying over".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.FLYOVER);
        } else if ("nest building".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.NEST_BUILDING);
        } else if ("recently fletched young".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.RECENTLY_FLEDGED);
        } else if ("bird with brood patch".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.PHYSIOLOGICAL_EVIDENCE);
        } else if ("transport of food or faeces".equals(extracted)) {
          sighting.getSightingInfo().setBreedingBirdCode(BreedingBirdCode.CARRYING_FOOD);
        } else if ("road kill".equals(extracted)
            || "windfarm victim".equals(extracted)
            || "window victim".equals(extracted)
            || "prey (dead)".equals(extracted)
            || "discovery (dead)".equals(extracted)) {
          sighting.getSightingInfo().setSightingStatus(SightingStatus.DEAD);
        }
      }
    }
  }

  static class StatusMapper implements FieldMapper<String[]> {
    private LineExtractor<String> certainExtractor;
    private LineExtractor<Boolean> isEscapeExtractor;

    public StatusMapper(
        LineExtractor<String> certainExtractor,
        LineExtractor<Boolean> isEscapeExtractor) {
      this.certainExtractor = certainExtractor;
      this.isEscapeExtractor = isEscapeExtractor;
    }

    @Override public void map(String[] line, Sighting.Builder sighting) {
      if ("False".equalsIgnoreCase(certainExtractor.extract(line))) {
        sighting.getSightingInfo().setSightingStatus(SightingStatus.ID_UNCERTAIN);
      } else if (isEscapeExtractor.extract(line)) {
        sighting.getSightingInfo().setSightingStatus(SightingStatus.INTRODUCED_NOT_ESTABLISHED);
      }
    }
  }
  
  static class StripQuotes implements LineExtractor<String> {
    private final CharMatcher QUOTE = CharMatcher.is('"');
    private final LineExtractor<String> extractor;

    public StripQuotes(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public String extract(String[] line) {
      String extracted = extractor.extract(line);
      if (extracted == null) {
        return extracted;
      }
      
      return QUOTE.trimFrom(extracted);
    }
  }
  
  /** A hacky list of possible values for the species group names. */
  private static final ImmutableSet<String> BIRDS_GROUP_NAMES = ImmutableSet.of(
      "birds",
      "vogels",
      "oiseaux",
      "pájaros",
      "pajaros",
      "vögel",
      "vogel",
      "fûgels",
      "fugels",
      "uccelli");

  @Override
  protected boolean skipLine(String[] line) {
    if (onlyBirds) {
      String extracted = speciesGroupExtractor.extract(line);
      if (!Strings.isNullOrEmpty(extracted)
          && !BIRDS_GROUP_NAMES.stream().anyMatch(extracted::equalsIgnoreCase)) {
        // Not a bird - skip.
        return true;
      }
    }
    return false;
  }
}
