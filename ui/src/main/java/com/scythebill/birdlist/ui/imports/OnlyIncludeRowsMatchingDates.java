/*   
 * Copyright 2024 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.IOException;

import org.joda.time.ReadablePartial;

import com.google.common.collect.ImmutableSet;
import com.scythebill.birdlist.model.io.ImportLines;

/**
 * Proxy ImportLines which only returns rows whose date matches a given set.
 * 
 * Assumes that there is exactly one header row.
 */
class OnlyIncludeRowsMatchingDates implements ImportLines {
  private ImportLines importLines;
  boolean readHeader = false;
  private final ImmutableSet<ReadablePartial> onlyImportDatesIn;
  private final LineExtractor<ReadablePartial> dateExtractor;

  OnlyIncludeRowsMatchingDates(ImportLines importLines,
      ImmutableSet<ReadablePartial> onlyImportDatesIn,
      LineExtractor<ReadablePartial> dateExtractor) {
    this.importLines = importLines;
    this.onlyImportDatesIn = onlyImportDatesIn;
    this.dateExtractor = dateExtractor;
  }

  @Override
  public void close() throws IOException {
    importLines.close();
  }

  @Override
  public String[] nextLine() throws IOException {
    if (!readHeader) {
      String[] header = importLines.nextLine();
      readHeader = true;
      return header;
    }
    
    while (true) {
      String[] row = importLines.nextLine();
      if (row == null) {
        break;
      }
      
      if (onlyImportDatesIn.contains(dateExtractor.extract(row))) {
        return row;
      }
    }
    return null;
  }

  @Override
  public int lineNumber() {
    return importLines.lineNumber();
  }

  @Override
  public ImportLines withoutTrimming() {
    importLines = importLines.withoutTrimming();
    return this;
  }
  
}