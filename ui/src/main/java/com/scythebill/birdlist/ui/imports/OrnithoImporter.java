/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.joda.time.Duration;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;
import org.joda.time.ReadablePartial;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.opencsv.CSVReader;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.io.TimeIO;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.Sighting.Builder;
import com.scythebill.birdlist.model.sighting.SightingInfo.BreedingBirdCode;
import com.scythebill.birdlist.model.sighting.VisitInfo.ObservationType;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.BKTree;
import com.scythebill.birdlist.model.util.Metrics;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Imports sightings from Ornitho files. 
 */
public class OrnithoImporter extends CsvSightingsImporter {
  private final static Logger logger = Logger.getLogger(OrnithoImporter.class.getName());
  private LineExtractor<String> idExtractor;
  private LineExtractor<String> taxonomyIdExtractor;
  private RowExtractor<String[], String> commonNameExtractor;
  private RowExtractor<String[], String> scientificExtractor;
  private LineExtractor<String> subspeciesExtractor;
  private LineExtractor<String> stateExtractor;
  private LineExtractor<String> countyExtractor;
  private LineExtractor<String> municipalityExtractor;
  private LineExtractor<String> locationExtractor;
  private LineExtractor<String> longitudeExtractor;
  private LineExtractor<String> latitudeExtractor;
  private LineExtractor<String> timeStartExtractor;
  private LineExtractor<String> timeStopExtractor;
  private LineExtractor<String> visitCommentExtractor;
  private LineExtractor<Boolean> fullFormExtractor;
  private Map<VisitInfoKey, VisitInfo> visitInfoMap;

  
  private final ImmutableMap<String, BreedingBirdCode> ATLAS_CODES =
      ImmutableMap.<String, BreedingBirdCode>builder()
      .put("A1", BreedingBirdCode.IN_APPROPRIATE_HABITAT)
      .put("A2", BreedingBirdCode.SINGING_BIRD)
      .put("B3", BreedingBirdCode.PAIR_IN_SUITABLE_HABITAT)
      .put("B4", BreedingBirdCode.SINGING_BIRD_7_DAYS)
      .put("B5", BreedingBirdCode.COURTSHIP_DISPLAY_OR_COPULATION)
      .put("B6", BreedingBirdCode.VISITING_PROBABLE_NEST_SITE)
      .put("B7", BreedingBirdCode.AGITATED_BEHAVIOR)
      .put("B8", BreedingBirdCode.PHYSIOLOGICAL_EVIDENCE)
      .put("B9", BreedingBirdCode.NEST_BUILDING)
      .put("C10", BreedingBirdCode.DISTRACTION_DISPLAY)
      .put("C11", BreedingBirdCode.USED_NEST)
      .put("C11a", BreedingBirdCode.USED_NEST)
      .put("C11b", BreedingBirdCode.USED_NEST) // eggshells from the current season
      .put("C12", BreedingBirdCode.RECENTLY_FLEDGED)
      .put("C13", BreedingBirdCode.OCCUPIED_NEST)
      .put("C13a", BreedingBirdCode.OCCUPIED_NEST)
      .put("C13b", BreedingBirdCode.OCCUPIED_NEST)
      .put("C14a", BreedingBirdCode.CARRYING_FECAL_SAC)
      .put("C14b", BreedingBirdCode.CARRYING_FOOD)
      .put("C15", BreedingBirdCode.NEST_WITH_EGGS)
      .put("C16", BreedingBirdCode.NEST_WITH_YOUNG)
      .build();

  public OrnithoImporter(ReportSet reportSet, Taxonomy taxonomy,
      Checklists checklists, PredefinedLocations predefinedLocations, File file) {
    super(reportSet, taxonomy, checklists, predefinedLocations, file, file);
  }

  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return taxonomyIdExtractor;
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    return new FieldTaxonImporter<>(taxonomy, commonNameExtractor, scientificExtractor,
        subspeciesExtractor);
  }

  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    OrnithoStateShortcuts ornithoStateShortcuts = new OrnithoStateShortcuts(locations, predefinedLocations);

    ImportLines lines = importLines(locationsFile);
    try {
      computeMappings(lines);

      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }

        if (skipLine(line)) {
          continue;
        }
        
        try {
          String id = idExtractor.extract(line);
          if (locationIds.hasBeenParsed(id)) {
            continue;
          }

          ImportedLocation imported = new ImportedLocation();
          // In even of total failure, at least put the locations in Europe, since Observado users
          // are most likely there.
          imported.defaultRegion = "Europe";
          
          String state = stateExtractor.extract(line);
          if (state.endsWith(", The")) {
            state = state.substring(0, state.length() - 5);
          }
          
          CountryAndState countryFromState = ornithoStateShortcuts.countryFromState(state);
          imported.countryCode = countryFromState.country;
          // Use the derived state name (which may not be exactly the imported one) to keep to built-in names where possible.
          imported.state = countryFromState.state;
          
          String county = countyExtractor.extract(line);
          if (!Strings.isNullOrEmpty(county)) {
            // TODO: these are county/district codes of some sort.  If there were a
            // simple way to translate into full names, that'd be great (AUR -> Aurich, etc.)
            // but I don't know of one yet
            // imported.county = county;
          }
          String town = municipalityExtractor.extract(line);
          if (!Strings.isNullOrEmpty(town)) {
            // In Ornitho.de exports, these have province and district codes in parentheses (and not at the end)
            town = removeParentheticals(town);
            // Add as a location name;  some are towns/cities, some are not.
            imported.locationNames.add(town);
          }
            
          String area = locationExtractor.extract(line);
          if (!Strings.isNullOrEmpty(area)) {
            area = removeParentheticals(area);
            imported.locationNames.add(area);
          }

          imported.longitude = Strings.emptyToNull(longitudeExtractor.extract(line));
          imported.latitude = Strings.emptyToNull(latitudeExtractor.extract(line));
          
          String locationId = imported.addToLocationSet(reportSet, locations, locationShortcuts,
              predefinedLocations);
          locationIds.put(id, locationId);
        } catch (Exception e) {
          logger.warning("Failed to import location " + Joiner.on(',').join(line));
        }
      }
    } catch (IOException | RuntimeException e) {
      throw new ImportException(
          Messages.getFormattedMessage(Name.FAILED_IMPORT_ON_LINE_FORMAT, lines.lineNumber()),
          e);
    } finally {
      lines.close();
    }
  }
  
  private final static CharMatcher ONLY_CODES = CharMatcher.inRange('A', 'Z')
      .or(CharMatcher.anyOf(" -,*")).or(CharMatcher.inRange('0', '9'));

  /**
   * Trim parenthetical inclusions, like "Krummhörn (NI, AUR), offshore" down to "Krummhörn, offshore". 
   */
  private static String removeParentheticals(String text) {
    while (true) {
      // Look for a space followed by a parenthesis
      int leftParen = text.indexOf(" (");
      if (leftParen < 0) {
        return text;
      }
      
      int rightParen = text.indexOf(')', leftParen + 1);
      if (rightParen < 0) {
        return text;
      }
      
      // Make sure it looks like a code (uppercase and numbers, plus maybe hyphens, spaces, commas
      if (!ONLY_CODES.matchesAllOf(text.substring(leftParen + 2, rightParen))) {
        return text;
      }
      
      text = text.substring(0,  leftParen)  + text.substring(rightParen + 1);
    }
    
  }

  static class DropSspFromCommon implements LineExtractor<String> {
    private final LineExtractor<String> extractor;

    public DropSspFromCommon(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public String extract(String[] line) {
      String name = extractor.extract(line);
      int indexOf = name.indexOf(" ssp ");
      if (indexOf > 0) {
        name = name.substring(0, indexOf);
      }
      return name;
    }
  }
  
  /** Take a trinomial and return the first two parts. */
  static class DropSspFromSci implements LineExtractor<String> {
    private final LineExtractor<String> extractor;

    public DropSspFromSci(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public String extract(String[] line) {
      String name = extractor.extract(line);
      if (name == null) {
        return null;
      }
      // Don't try trimming "slash" names - they've got an extra space, but that's for the sp.
      if (name.indexOf('/') >= 0) {
        return name;
      }
      
      // And don't trim hybrid names either
      if (name.indexOf(" x ") >= 0) {
        return name;
      }
      
      int firstSpace = name.indexOf(' ');
      if (firstSpace >= 0) {
        int nextSpace = name.indexOf(' ', firstSpace + 1);
        if (nextSpace >= 0) {
          return name.substring(0, nextSpace);
        }
      }
      return name;
    }
  }

  /** Take a trinomial and return the first two parts. */
  static class ExtractSspFromSci implements LineExtractor<String> {
    private final LineExtractor<String> extractor;

    public ExtractSspFromSci(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public String extract(String[] line) {
      String name = extractor.extract(line);
      if (name == null) {
        return null;
      }
      // Don't try trimming "slash" names - they've got an extra space, but that's for the sp.
      if (name.indexOf('/') >= 0) {
        return null;
      }
      
      // And don't trim hybrid names either
      if (name.indexOf(" x ") >= 0) {
        return null;
      }
      
      int firstSpace = name.indexOf(' ');
      if (firstSpace >= 0) {
        int nextSpace = name.indexOf(' ', firstSpace + 1);
        if (nextSpace >= 0) {
          return name.substring(nextSpace + 1);
        }
      }
      return null;
    }
  }

  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    String[] header = lines.nextLine();
    Map<String, Integer> headersByIndex = Maps.newHashMap();
    for (int i = 0; i < header.length; i++) {
      headersByIndex.put(CharMatcher.whitespace().removeFrom(header[i]).toLowerCase(), i);
    }
    // Skip the next line
    lines.nextLine();

    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
    Integer commonIndex = findIndex(headersByIndex, "name_species");
    Integer sciIndex = findIndex(headersByIndex, "latin_species");
    Integer dateIndex = findIndex(headersByIndex, "date");
    Integer estimationCodeIndex = findIndex(headersByIndex, "estimation_code");
    Integer numberIndex = findIndex(headersByIndex, "total_count");
    Integer descriptionIndex = findIndex(headersByIndex, "comment");
    Integer privateDescriptionIndex = findIndex(headersByIndex, "private_comment");
    Integer detailIndex = findIndex(headersByIndex, "detail");
    Integer observationDetailIndex = findIndex(headersByIndex, "observation_detail");
    Integer stateIndex = findIndex(headersByIndex, "country");
    Integer countyIndex = findIndex(headersByIndex, "county");
    Integer municipalityIndex = findIndex(headersByIndex, "municipality");
    Integer locationIndex = findIndex(headersByIndex, "place");
    Integer latitudeIndex = findIndex(headersByIndex, "coord_lat");
    Integer longitudeIndex = findIndex(headersByIndex, "coord_lon");
    Integer timeStartIndex = findIndex(headersByIndex, "time_start");
    Integer timeStopIndex = findIndex(headersByIndex, "time_stop");
    Integer atlasCodeIndex = findIndex(headersByIndex, "atlas_code");
    Integer visitCommentIndex = findIndex(headersByIndex, "daily_text_comment_rem");
    visitInfoMap = Maps.newHashMap();
    
    // TODO: use this for visit info (and add visit info!)
    Integer fullFormIndex = findIndex(headersByIndex, "full_form");
    
    // Create a taxonomy extractor from the located common/scientific/ssp. columns
    commonNameExtractor = commonIndex == null ? LineExtractors.<String> alwaysNull()
        : new DropSspFromCommon(LineExtractors.stringFromIndex(commonIndex));
    scientificExtractor = sciIndex == null ? LineExtractors.<String> alwaysNull()
        : new DropSspFromSci(LineExtractors.stringFromIndex(sciIndex));
    subspeciesExtractor = (sciIndex == null) ? LineExtractors.<String> alwaysNull()
        : new ExtractSspFromSci(LineExtractors.stringFromIndex(sciIndex));
    // Handle some splitting
    AlternativeTaxonomicExtractors<String[]> alternativeScientificExtractors = new AlternativeTaxonomicExtractors<>(
        scientificExtractor, subspeciesExtractor, commonNameExtractor);
    
    // Ornitho lumps the bean geese;  make sure that they can be found
    Taxonomy taxonomy = getTaxonomy();
    Taxon taigaBeanGoose = taxonomy.findSpecies("Anser fabalis");
    if (taigaBeanGoose == null) {
      logger.warning("Taiga Bean-Goose not found in " + taxonomy.getName());
    }
    Taxon tundraBeanGoose = taxonomy.findSpecies("Anser serrirostris");
    if (tundraBeanGoose == null) {
      logger.warning("Tundra Bean-Goose not found in " + taxonomy.getName());
    }
    if (taigaBeanGoose != null && tundraBeanGoose != null) {
      alternativeScientificExtractors.splitting("Anser fabalis", taigaBeanGoose, tundraBeanGoose);
    }
    
    // Swap the sci. and common extractor for the species
    scientificExtractor = alternativeScientificExtractors.species();
    commonNameExtractor = alternativeScientificExtractors.common();
    
    taxonomyIdExtractor = LineExtractors.joined(Joiner.on('|').useForNull(""),
        ImmutableList.of(commonNameExtractor, scientificExtractor, subspeciesExtractor));

    // Extract the specifically named location columns
    stateExtractor = LineExtractors.stringFromIndex(stateIndex);
    countyExtractor = LineExtractors.stringFromIndex(countyIndex);
    municipalityExtractor = municipalityIndex == null ? LineExtractors.<String> alwaysNull() : LineExtractors
        .stringFromIndex(municipalityIndex);
    locationExtractor = locationIndex == null ? LineExtractors.<String> alwaysNull() : LineExtractors
        .stringFromIndex(locationIndex);
    longitudeExtractor = longitudeIndex == null 
        ? LineExtractors.<String> alwaysNull()
        : LineExtractors.stringFromIndex(longitudeIndex); 
    latitudeExtractor = latitudeIndex == null 
        ? LineExtractors.<String> alwaysNull()
        : LineExtractors.stringFromIndex(latitudeIndex); 
    List<RowExtractor<String[], String>> allLocationExtractors = Lists.newArrayList();
    allLocationExtractors.add(stateExtractor);
    allLocationExtractors.add(countyExtractor);
    allLocationExtractors.add(municipalityExtractor);
    allLocationExtractors.add(locationExtractor);

    // Join those to form a location key. Note that not all rows
    // will have all location columns, so this Joiner must not blow
    // up on null.
    idExtractor = LineExtractors.joined(Joiner.on('|').useForNull(""), allLocationExtractors);

    mappers.add(new MultiFormatDateFromStringFieldMapper<>(
        LineExtractors.stringFromIndex(dateIndex),
        "dd.MM.yy"));
    if (numberIndex != null) {
      mappers.add(new CountFieldMapper<>(
          new IncorporateEstimation(
              estimationCodeIndex == null ? LineExtractors.<String> alwaysNull() : LineExtractors.stringFromIndex(estimationCodeIndex),
              LineExtractors.stringFromIndex(numberIndex))));
    }
    
    
    timeStartExtractor = timeStartIndex == null
        ? LineExtractors.alwaysNull()
        : midnightAsNull(LineExtractors.stringFromIndex(timeStartIndex));
    timeStopExtractor = timeStopIndex == null
        ? LineExtractors.alwaysNull()
        : midnightAsNull(LineExtractors.stringFromIndex(timeStopIndex));
    visitCommentExtractor = visitCommentIndex == null
        ? LineExtractors.alwaysNull()
        : LineExtractors.stringFromIndex(visitCommentIndex);
    fullFormExtractor = fullFormIndex == null
        ? LineExtractors.constant(false)
        : LineExtractors.booleanFromIndex(fullFormIndex);
    mappers.add(new TimeMapper<>(timeStartExtractor));
    
    if (atlasCodeIndex != null) {
      LineExtractor<String> atlasCodeExtractor = LineExtractors.stringFromIndex(atlasCodeIndex);
      mappers.add(new FieldMapper<String[]>() {
        @Override
        public void map(String[] line, Builder sighting) {
          String atlasCode = atlasCodeExtractor.extract(line);
          BreedingBirdCode breedingBirdCode = ATLAS_CODES.get(atlasCode);
          if (breedingBirdCode != null) {
            sighting.getSightingInfo().setBreedingBirdCode(breedingBirdCode);
          }
        }
      });
    }
    LineExtractor<String> commentExtractor = descriptionIndex == null
        ? LineExtractors.constant(null)
        : LineExtractors.nullIfEmpty(LineExtractors.stringFromIndex(descriptionIndex));
    LineExtractor<String> privateDescriptionExtractor = privateDescriptionIndex == null
        ? LineExtractors.constant(null)
        : LineExtractors.nullIfEmpty(LineExtractors.stringFromIndex(privateDescriptionIndex));
    LineExtractor<String> detailExtractor = detailIndex == null
        ? LineExtractors.constant(null)
        : LineExtractors.nullIfEmpty(LineExtractors.stringFromIndex(detailIndex));
    LineExtractor<String> observationDetailExtractor = observationDetailIndex == null
        ? LineExtractors.constant(null)
        : LineExtractors.nullIfEmpty(LineExtractors.stringFromIndex(observationDetailIndex));
    mappers.add(new DescriptionFieldMapper<>(
        LineExtractors.appendingLatitudeAndLongitude(
            LineExtractors.joined(
                Joiner.on('\n').skipNulls(),
                commentExtractor, privateDescriptionExtractor, detailExtractor, observationDetailExtractor),
            latitudeExtractor, longitudeExtractor)));
    
    if (detailIndex != null) {
      mappers.add(new DetailMapper(LineExtractors.stringFromIndex(detailIndex)));
    }
    return new ComputedMappings<>(
        new TaxonFieldMapper(taxonomyIdExtractor),
        new LocationMapper(idExtractor),
        mappers);
  }

  private Integer findIndex(Map<String, Integer> headersByIndex, String... names) {
    for (String name : names) {
      Integer index = headersByIndex.get(name);
      if (index != null) {
        return index;
      }
    }
    return null;
  }

  /**
   * Extract Scythebill dates using its internal formatter, which supports
   * missing days and months.
   */
  static class ScythebillDateMapper implements FieldMapper<String[]> {
    private LineExtractor<String> extractor;

    ScythebillDateMapper(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public void map(String[] line, Builder sighting) {
      String date = Strings.emptyToNull(extractor.extract(line));
      if (date != null) {
        ReadablePartial datePartial = PartialIO.fromString(date);
        sighting.setDate(datePartial);
      }
    }
  }

  static class DetailMapper implements FieldMapper<String[]> {
    private final LineExtractor<String> extractor;

    public DetailMapper(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override public void map(String[] line, Sighting.Builder sighting) {
      String extracted = extractor.extract(line);
      if (extracted != null) {
        extracted = extracted.toLowerCase();
        for (String substring : Splitter.on('/').split(extracted)) {
          // TODO: non-US-DE languages
          if (substring.contains("female")
              || substring.contains("weibchen")) {            
            sighting.getSightingInfo().setFemale(true);
          } else if (substring.contains("male")
              || substring.contains("männchen")) {
            sighting.getSightingInfo().setMale(true);
          }
          
          if (substring.contains("adult")) {
            sighting.getSightingInfo().setAdult(true);
          } else if (substring.contains("diesjährige")
              || substring.contains("vorjährig")
              || substring.contains("nicht-flügge")) {
            sighting.getSightingInfo().setImmature(true);
          }
        }
      }
    }
  }

  static class StripQuotes implements LineExtractor<String> {
    private final CharMatcher QUOTE = CharMatcher.is('"');
    private final LineExtractor<String> extractor;

    public StripQuotes(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public String extract(String[] line) {
      String extracted = extractor.extract(line);
      if (extracted == null) {
        return extracted;
      }
      
      return QUOTE.trimFrom(extracted);
    }

  }

  @Override
  protected ImportLines importLines(File file) throws IOException {
    final CSVReader csvReader = new CSVReader(
        new InputStreamReader(
            new BufferedInputStream(new FileInputStream(file)), Charsets.UTF_8),
        '\t',
        // No escapes;  it's all tabs
        '\0',
        false);
    return CsvImportLines.fromReader(csvReader);
  }  

  static class IncorporateEstimation implements LineExtractor<String> {
    private LineExtractor<String> estimationExtractor;
    private LineExtractor<String> numberExtractor;

    public IncorporateEstimation(
        LineExtractor<String> estimationExtractor,
        LineExtractor<String> numberExtractor) {
      this.estimationExtractor = estimationExtractor;
      this.numberExtractor = numberExtractor;
    }
    
    @Override
    public String extract(String[] line) {
      String estimate = estimationExtractor.extract(line);
      String number = numberExtractor.extract(line);
      // No estimate, just return the number
      if (Strings.isNullOrEmpty(estimate)) {
        return number;
      }
      // An estimate of "x" means "actual number not determined";  the number field will be 1, but should be ignored.
      if (estimate.equals("x") || estimate.equals("×")) {
        return null;
      }
      // Prefix the estimate character
      return estimate + number;
    }

  }
  
  static class CountryAndState {
    final String country;
    final String state;

    CountryAndState(String country, String state) {
      this.country = country;
      this.state = state;
    }
  }

  static class OrnithoStateShortcuts {
    private final static ImmutableList<String> INCLUDED_COUNTRIES = ImmutableList.of(
        "DE",
        "AT",
        "IT",
        "PL",
        "CH",
        "AD",
        "ES",
        "LU",
        "SM", // San Marino
        "TN" // Tunisia - included in ornitho.it
        );
    private final ImmutableMap<String, CountryAndState> stateNameToCountry;
    private final BKTree<String, CountryAndState> stateNameToCountryInexact;
        
    OrnithoStateShortcuts(LocationSet locationSet, PredefinedLocations predefinedLocations) {
      ImmutableMap.Builder<String, CountryAndState> stateNameToCountryBuilder = ImmutableMap.builder();
      BKTree.Builder<String, CountryAndState> stateNameToCountryInexactBuilder = BKTree.builder(Metrics.levenshteinDistance());
      for (String includedCountry : INCLUDED_COUNTRIES) {
        Location country = locationSet.getLocationByCode(includedCountry);
        for (PredefinedLocation predefinedLocation : predefinedLocations.getPredefinedLocations(country)) {
          CountryAndState countryAndState = new CountryAndState(includedCountry, predefinedLocation.getName());
          stateNameToCountryBuilder.put(predefinedLocation.getName(), countryAndState);
          stateNameToCountryInexactBuilder.add(predefinedLocation.getName(), countryAndState);
        }
      }
      
      // Support the German "exclusive economic zone"
      CountryAndState germanEEZ = new CountryAndState("DE", "Ausschließliche Wirtschaftszone");
      stateNameToCountryBuilder.put(germanEEZ.state, germanEEZ);
      stateNameToCountryInexactBuilder.add(germanEEZ.state, germanEEZ);

      // And french
      CountryAndState frenchEEZ = new CountryAndState("FR", "Zone économique exclusive");
      stateNameToCountryBuilder.put(frenchEEZ.state, frenchEEZ);
      stateNameToCountryInexactBuilder.add(frenchEEZ.state, frenchEEZ);
      
      stateNameToCountry = stateNameToCountryBuilder.build();
      stateNameToCountryInexact = stateNameToCountryInexactBuilder.build();
    }
    
    public CountryAndState countryFromState(String stateName) {
      // First, try an exact match
      CountryAndState country = stateNameToCountry.get(stateName);
      if (country == null) {
        // Then try progressively larger searches
        for (int i = 1; i <= 3; i++) {
          List<CountryAndState> list = stateNameToCountryInexact.search(stateName, i);
          if (!list.isEmpty()) {
//            System.err.println("INEXACT MATCH FOR " + stateName + " AT DISTANCE " + i);
            country = list.get(0);
            break;
          }
        }
      }
      // Give up:  Germany it is
      if (country == null) {
//        System.err.println("NO MATCH FOR " + stateName);
        country = new CountryAndState("DE", stateName);
      }
      return country;
    }
  }

  @Override
  protected void lookForVisitInfo(
      ReportSet reportSet,
      String[] line,
      Sighting newSighting,
      VisitInfoKey visitInfoKey) {
    VisitInfo.Builder builder = VisitInfo.builder().withObservationType(ObservationType.HISTORICAL);
    if (fullFormExtractor.extract(line)) {
      builder.withCompleteChecklist(true);
    }
    
    if (visitInfoKey.startTime().isPresent()) {
      String timeStopString = timeStopExtractor.extract(line);
      if (!Strings.isNullOrEmpty(timeStopString)) {
        LocalTime timeStop = TimeIO.fromString(timeStopString);
        if (timeStop != null) {
          Duration duration = Minutes.minutesBetween(visitInfoKey.startTime().get(), timeStop).toStandardDuration();
          if (duration.isLongerThan(Duration.standardMinutes(5))) {
            builder = builder.withDuration(duration);
          }
        }
      }
    }
    
    String visitComment = visitCommentExtractor.extract(line);
    if (!Strings.isNullOrEmpty(visitComment)) {
      builder.withComments(visitComment);
    }
    
    VisitInfo visitInfo = builder.build();
    if (visitInfo.hasData()) {
      visitInfoMap.put(visitInfoKey, visitInfo);      
    }
  }

  @Override
  public Map<VisitInfoKey, VisitInfo> getAccumulatedVisitInfoMap() {
    return visitInfoMap;
  }
  
  /** Ornitho exports don't have empty times, just midnight. */
  private static LineExtractor<String> midnightAsNull(LineExtractor<String> extractor) {
    return new LineExtractor<String>() {
      @Override
      public String extract(String[] row) {
        String extracted = extractor.extract(row);
        if ("0:00".equals(extracted)
            || "00:00".equals(extracted)) {
          return null;
        }
        
        return extracted;
      }
    };
  }
}
