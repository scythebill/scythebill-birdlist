/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.Location;

/**
 * Data structure for passing location ID resolution out of an importer.
 */
public class ParsedLocationIds {
  private Map<Object, String> locationIds = Maps.newLinkedHashMap();
  private Map<Object, ToBeDecided> toBeResolvedLocationNames = Maps.newLinkedHashMap();

  public ParsedLocationIds() {
  }
  
  public boolean containsKey(Object key) {
    return locationIds.containsKey(key);
  }

  public boolean hasBeenParsed(Object key) {
    return locationIds.containsKey(key) || toBeResolvedLocationNames.containsKey(key);
  }

  public String getLocationId(Object key) {
    return locationIds.get(key);
  }

  public void put(Object id, String locationId) {
    locationIds.put(id, locationId);
    toBeResolvedLocationNames.remove(id);
  }

  public void addToBeResolvedLocationName(Object id, ToBeDecided toBeDecided) {
    toBeResolvedLocationNames.put(id, toBeDecided);
  }

  public ImmutableMap<Object, ToBeDecided> toBeResolvedNames() {
    return ImmutableMap.copyOf(toBeResolvedLocationNames);
  }
  
  static class ToBeDecided {
    enum HintType {
      location,
      other
    }

    final String name;
    final LatLongCoordinates latLong;
    final String hint;
    final HintType hintType;
    Location preferredParent; 
    
    ToBeDecided(String name) {
      this(name, null);
    }

    ToBeDecided(String name, LatLongCoordinates latLong) {
      this(name, latLong, null, HintType.other);
    }
    
    ToBeDecided(String name, LatLongCoordinates latLong, String hint, HintType hintType) {
      this.name = name;
      this.latLong = latLong;
      this.hint = hint;
      this.hintType = hintType;
    }
  }

}
