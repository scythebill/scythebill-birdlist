/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.joda.time.Duration;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;
import org.joda.time.ReadablePartial;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.io.TimeIO;
import com.scythebill.birdlist.model.sighting.Area;
import com.scythebill.birdlist.model.sighting.Distance;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Photo;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.Sighting.Builder;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfo.ObservationType;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.LocalDateFormatterChooser;
import com.scythebill.birdlist.model.util.LocalDateFormatterChooser.ChosenFormat;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Imports sightings from Scythebill files.
 */
public class ScythebillImporter extends CsvSightingsImporter {

  private final static Logger logger = Logger.getLogger(ScythebillImporter.class.getName());
  private LineExtractor<String> locationIdExtractor;
  private LineExtractor<String> taxonomyIdExtractor;
  private LineExtractor<String> commonNameExtractor;
  private LineExtractor<String> scientificExtractor;
  private LineExtractor<String> genusExtractor;
  private LineExtractor<String> speciesExtractor;
  private LineExtractor<String> subspeciesExtractor;
  private LineExtractor<String> regionExtractor;
  private LineExtractor<String> countryExtractor;
  private LineExtractor<String> stateExtractor;
  private LineExtractor<String> countyExtractor;
  private LineExtractor<String> cityExtractor;
  private LineExtractor<String> startTimeExtractor;
  private LineExtractor<String> endTimeExtractor;
  private List<LineExtractor<String>> locationExtractors;
  private LineExtractor<String> observationTypeExtractor;
  private LineExtractor<Integer> durationExtractor;
  private LineExtractor<Integer> partySizeExtractor;
  private LineExtractor<Float> distanceExtractor;
  private LineExtractor<Float> areaExtractor;
  private LineExtractor<Boolean> completeListExtractor;
  private LineExtractor<String> visitCommentsExtractor;
  private Map<VisitInfoKey, VisitInfo.Builder> visitInfoMap;
  private LineExtractor<String> latitudeExtractor;
  private LineExtractor<String> longitudeExtractor;
  private LineExtractor<String> locationDescriptionExtractor;
  private LineExtractor<String> dateExtractor;
  private Optional<ImmutableList<ChosenFormat>> chosenFormatters;
  private boolean importContainedUserInformation = false;

  public ScythebillImporter(ReportSet reportSet, Taxonomy taxonomy,
      Checklists checklists, PredefinedLocations predefinedLocations, File file) {
    super(reportSet, taxonomy, checklists, predefinedLocations, file, file);
  }

  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return taxonomyIdExtractor;
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    LineExtractor<String> combinedScientificExtractor = row -> {
      String sci = scientificExtractor.extract(row);
      if (!Strings.isNullOrEmpty(sci)) {
        return sci;
      }

      String species = speciesExtractor.extract(row);
      if (Strings.isNullOrEmpty(species)) {
        return null;
      }

      String genus = genusExtractor.extract(row);
      if (Strings.isNullOrEmpty(genus)) {
        return null;
      }
      
      return genus + " " + species;
    };

    return new FieldTaxonImporter<>(taxonomy, commonNameExtractor, combinedScientificExtractor,
        subspeciesExtractor);
  }

  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    ImportLines lines = CsvImportLines.fromFile(locationsFile, getCharset());
    try {
      computeMappings(lines);

      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }

        try {
          String id = locationIdExtractor.extract(line);
          if (locationIds.hasBeenParsed(id)) {
            continue;
          }
          ImportedLocation imported = new ImportedLocation();
          imported.state = stateExtractor.extract(line);
          imported.county = countyExtractor.extract(line);
          imported.country = countryExtractor.extract(line);
          imported.region = regionExtractor.extract(line);
          imported.city = cityExtractor.extract(line);
          for (LineExtractor<String> locationExtractor : locationExtractors) {
            imported.locationNames.add(locationExtractor.extract(line));
          }
          
          imported.latitude = Strings.emptyToNull(latitudeExtractor.extract(line));
          imported.longitude = Strings.emptyToNull(longitudeExtractor.extract(line));
          imported.description = Strings.emptyToNull(locationDescriptionExtractor.extract(line));

          String locationId = imported.addToLocationSet(reportSet, locations, locationShortcuts,
              predefinedLocations);
          locationIds.put(id, locationId);
        } catch (Exception e) {
          logger.log(Level.WARNING, "Failed to import location " + Joiner.on(',').join(line), e);
        }
      }
    } catch (IOException | RuntimeException e) {
      throw new ImportException(
          Messages.getFormattedMessage(Name.FAILED_IMPORT_ON_LINE_FORMAT, lines.lineNumber()),
          e);
    } finally {
      lines.close();
    }
  }

  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    String[] header = lines.nextLine();
    Map<String, Integer> headersByIndex = new LinkedHashMap<>();
    for (int i = 0; i < header.length; i++) {
      headersByIndex.put(CharMatcher.whitespace().removeFrom(header[i]).toLowerCase(), i);
    }

    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
    Integer commonIndex = headersByIndex.get("common");
    Integer sciIndex = headersByIndex.get("scientific");
    Integer genusIndex = headersByIndex.get("genus");
    Integer speciesIndex = headersByIndex.get("species");
    Integer sspIndex = headersByIndex.get("subspecies");
    int dateIndex = getRequiredHeader(headersByIndex, "Date");
    Integer startTimeIndex = headersByIndex.get("starttime");
    Integer endTimeIndex = headersByIndex.get("endtime");
    Integer numberIndex = headersByIndex.get("number");
    Integer femaleIndex = headersByIndex.get("female");
    Integer maleIndex = headersByIndex.get("male");
    Integer immatureIndex = headersByIndex.get("immature");
    Integer adultIndex = headersByIndex.get("adult");
    Integer heardOnlyIndex = headersByIndex.get("heardonly");
    Integer statusIndex = headersByIndex.get("status");
    Integer breedingIndex = headersByIndex.get("breeding");
    Integer photographedIndex = headersByIndex.get("photographed");
    Integer photosIndex = headersByIndex.get("photos");
    Integer descriptionIndex = headersByIndex.get("description");
    Integer regionIndex = headersByIndex.get("region");
    int countryIndex = getRequiredHeader(headersByIndex, "Country");
    Integer stateIndex = headersByIndex.get("state");
    Integer countyIndex = headersByIndex.get("county");
    Integer cityIndex = headersByIndex.get("city");
    Integer latitudeIndex = headersByIndex.get("latitude");
    Integer longitudeIndex = headersByIndex.get("longitude");
    Integer locationDescriptionIndex = headersByIndex.get("locationdescription");

    if (commonIndex == null && sciIndex == null) {
      throw new ImportException(Messages.getMessage(Name.SCYTHEBILL_IMPORT_NEITHER_SCIENTIFIC_NOR_COMMON));
    }
    // Create a taxonomy extractor from the located common/scientific/ssp.
    // columns
    commonNameExtractor = commonIndex == null ? LineExtractors.<String> alwaysNull()
        : LineExtractors.stringFromIndex(commonIndex);
    scientificExtractor = sciIndex == null ? LineExtractors.<String> alwaysNull() : LineExtractors
        .stringFromIndex(sciIndex);
    genusExtractor = genusIndex == null ? LineExtractors.<String> alwaysNull() : LineExtractors
        .stringFromIndex(genusIndex);
    speciesExtractor = speciesIndex == null ? LineExtractors.<String> alwaysNull() : LineExtractors
        .stringFromIndex(speciesIndex);
    subspeciesExtractor = (sspIndex == null) ? LineExtractors.<String> alwaysNull()
        : LineExtractors.stringFromIndex(sspIndex);
    taxonomyIdExtractor =
        LineExtractors.joined(Joiner.on('|').useForNull(""), ImmutableList.of(commonNameExtractor,
            scientificExtractor, subspeciesExtractor, speciesExtractor, genusExtractor));

    // Extract the specifically named location columns
    regionExtractor = regionIndex == null ? LineExtractors.<String> alwaysNull() : LineExtractors
        .stringFromIndex(regionIndex);
    countryExtractor = LineExtractors.stringFromIndex(countryIndex);
    stateExtractor = stateIndex == null ? LineExtractors.<String> alwaysNull() : LineExtractors
        .stringFromIndex(stateIndex);
    countyExtractor = countyIndex == null ? LineExtractors.<String> alwaysNull() : LineExtractors
        .stringFromIndex(countyIndex);
    cityExtractor = cityIndex == null ? LineExtractors.<String> alwaysNull() : LineExtractors
        .stringFromIndex(cityIndex);
    List<RowExtractor<String[], String>> allLocationExtractors = Lists.newArrayList();
    allLocationExtractors.add(regionExtractor);
    allLocationExtractors.add(countryExtractor);
    allLocationExtractors.add(stateExtractor);
    allLocationExtractors.add(countyExtractor);
    allLocationExtractors.add(cityExtractor);

    // And find all the Location N columns
    locationExtractors = Lists.newArrayList();
    
    // But first, support imports with an ordinary "location" column, since
    // that's not obvious.
    Integer locationWithoutIndex = headersByIndex.get("location");
    if (locationWithoutIndex != null) {
      locationExtractors.add(LineExtractors.stringFromIndex(locationWithoutIndex));
    }
    
    int i = 1;
    while (true) {
      String locationHeader = String.format("location%s", i);
      // Always go up to at least "location 2", just in case it starts
      // "location", "location 2".
      if (!headersByIndex.containsKey(locationHeader)) {
        if (i >= 2) {
          break;
        } else {
          i++;
          continue;
        }
      }

      int index = headersByIndex.get(locationHeader);
      locationExtractors.add(LineExtractors.stringFromIndex(index));
      i++;
    }

    // Join those to form a location key. Note that not all rows
    // will have all location columns, so this Joiner must not blow
    // up on null.
    allLocationExtractors.addAll(locationExtractors);
    locationIdExtractor = LineExtractors.joined(Joiner.on('|').useForNull(""), allLocationExtractors);
    
    latitudeExtractor = latitudeIndex == null
        ? LineExtractors.<String>alwaysNull() : LineExtractors.stringFromIndex(latitudeIndex);
    longitudeExtractor = longitudeIndex == null
        ? LineExtractors.<String>alwaysNull() : LineExtractors.stringFromIndex(longitudeIndex);
    locationDescriptionExtractor = locationDescriptionIndex == null
        ? LineExtractors.<String>alwaysNull() : LineExtractors.stringFromIndex(locationDescriptionIndex);

    startTimeExtractor = startTimeIndex == null
        ? LineExtractors.<String>alwaysNull() : LineExtractors.stringFromIndex(startTimeIndex);
    endTimeExtractor = endTimeIndex == null
        ? LineExtractors.<String>alwaysNull() : LineExtractors.stringFromIndex(endTimeIndex);
    if (startTimeIndex != null) {
      mappers.add(new TimeMapper<>(startTimeExtractor));
    }
    if (numberIndex != null) {
      mappers.add(new CountFieldMapper<>(LineExtractors.stringFromIndex(numberIndex)));
    }
    if (descriptionIndex != null) {
      mappers.add(new DescriptionFieldMapper<>(LineExtractors.stringFromIndex(descriptionIndex)));
    }
    if (femaleIndex != null) {
      mappers.add(new FemaleFieldMapper<>(LineExtractors.booleanFromIndex(femaleIndex)));
    }
    if (maleIndex != null) {
      mappers.add(new MaleFieldMapper<>(LineExtractors.booleanFromIndex(maleIndex)));
    }
    if (immatureIndex != null) {
      mappers.add(new ImmatureFieldMapper<>(LineExtractors.booleanFromIndex(immatureIndex)));
    }
    if (adultIndex != null) {
      mappers.add(new AdultFieldMapper<>(LineExtractors.booleanFromIndex(adultIndex)));
    }
    if (heardOnlyIndex != null) {
      mappers.add(new HeardOnlyFieldMapper<>(LineExtractors.booleanFromIndex(heardOnlyIndex)));
    }
    if (photographedIndex != null) {
      mappers.add(new PhotographedFieldMapper<>(LineExtractors.booleanFromIndex(photographedIndex)));
    }
    if (photosIndex != null) {
      mappers.add(new PhotosMapper(
          LineExtractors.stringFromIndex(photosIndex),
          photographedIndex == null));
    }
    if (statusIndex != null) {
      mappers.add(new StatusFieldMapper<>(LineExtractors.stringFromIndex(statusIndex)));
    }
    if (breedingIndex != null) {
      mappers.add(new BreedingBirdCodeFieldMapper<>(LineExtractors.stringFromIndex(breedingIndex)));
    }

    visitInfoMap = new LinkedHashMap<>();
    Integer observationTypeIndex = headersByIndex.get("observationtype");
    Integer durationIndex = headersByIndex.get("duration");
    Integer distanceIndex = headersByIndex.get("distance");
    Integer areaIndex = headersByIndex.get("area");
    Integer partySizeIndex = headersByIndex.get("partysize");
    Integer completeListIndex = headersByIndex.get("completelist");
    Integer visitCommentsIndex = headersByIndex.get("visitcomments");

    observationTypeExtractor = observationTypeIndex == null ? LineExtractors.<String> alwaysNull()
        : LineExtractors.stringFromIndex(observationTypeIndex);
    durationExtractor = durationIndex == null ? LineExtractors.<Integer> alwaysNull()
        : LineExtractors.intFromIndex(durationIndex);
    partySizeExtractor = partySizeIndex == null ? LineExtractors.<Integer> alwaysNull()
        : LineExtractors.intFromIndex(partySizeIndex);
    distanceExtractor = distanceIndex == null ? LineExtractors.<Float> alwaysNull()
        : LineExtractors.floatFromIndex(distanceIndex);
    areaExtractor = areaIndex == null ? LineExtractors.<Float> alwaysNull() : LineExtractors
        .floatFromIndex(areaIndex);
    completeListExtractor = completeListIndex == null
        ? LineExtractors.constant(true)
        : LineExtractors.booleanFromIndex(completeListIndex);
    visitCommentsExtractor = visitCommentsIndex == null ? LineExtractors.<String> alwaysNull()
        : LineExtractors.stringFromIndex(visitCommentsIndex);
    
    dateExtractor = LineExtractors.stringFromIndex(dateIndex);
    mappers.add(new ScythebillDateMapper(dateExtractor));
    
    // Look for any user data in the export.
    Integer observersIndex = headersByIndex.get("observers");
    Integer observerNamesIndex = headersByIndex.get("observernames");
    if (observersIndex != null || observerNamesIndex != null) {
      LineExtractor<String> observersExtractor = observersIndex == null
          ? LineExtractors.alwaysNull() : LineExtractors.stringFromIndex(observersIndex);
      LineExtractor<String> observerNamesExtractor = observerNamesIndex == null
          ? LineExtractors.alwaysNull() : LineExtractors.stringFromIndex(observerNamesIndex);
      mappers.add(new UserFieldMapper<>(reportSet, observersExtractor, observerNamesExtractor));
      importContainedUserInformation = true;
    }

    return new ComputedMappings<>(
        new TaxonFieldMapper(taxonomyIdExtractor),
        new LocationMapper(locationIdExtractor),
        mappers);
  }

  @Override
  protected void computeExtendedMappings() throws IOException {
    if (chosenFormatters != null) {
      return;
    }
    
    try (ImportLines lines = importLines(sightingsFile)) {
      // See if any of the dates are of an unknown form
      Set<String> nonStandardDates = Sets.newLinkedHashSet();
      while (true) {
        String[] nextLine = lines.nextLine();
        if (nextLine == null) {
          break;
        }
        
        String date = dateExtractor.extract(nextLine);
        if (!Strings.isNullOrEmpty(date)) {
          try {
            PartialIO.fromString(date);
          } catch (IllegalArgumentException e) {
            nonStandardDates.add(date);
          }
        }
      }
      if (nonStandardDates.isEmpty()) {
        chosenFormatters = Optional.absent();
      } else {
        chosenFormatters = Optional.of(new LocalDateFormatterChooser().chooseFormatters(nonStandardDates));
      }
    }
    
  }
  
  /**
   * Extract Scythebill dates using its internal formatter, which supports
   * missing days and months.
   */
  class ScythebillDateMapper implements FieldMapper<String[]> {
    private final LineExtractor<String> extractor;

    ScythebillDateMapper(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public void map(String[] line, Builder sighting) {
      Preconditions.checkState(chosenFormatters != null, "Date formatting not yet called");
      String date = Strings.emptyToNull(extractor.extract(line));
      if (date != null) {
        if (!chosenFormatters.isPresent()) {
          // Everything is standard;  just parse directly using PartialIO 
          ReadablePartial datePartial = PartialIO.fromString(date);
          sighting.setDate(datePartial);
        } else {
          // Non-standard entries found;  need to try PartialIO and all the custom formats
          ReadablePartial datePartial = getDateFromAllFormatters(date);
          sighting.setDate(datePartial);
        }
      }
    }

    private ReadablePartial getDateFromAllFormatters(String date) {
      try {
        return PartialIO.fromString(date);
      } catch (IllegalArgumentException e) {
      }

      for (ChosenFormat formatter : chosenFormatters.get()) {
        try {
          return formatter.parse(date);
        } catch (IllegalArgumentException e) {
        }
      }

      throw new IllegalArgumentException(String.format(
          "Could not parse date %s with any of these formats: %s", date, chosenFormatters.get()));
    }
  }

  /** Extract photos from a line-separated list. */
  static class PhotosMapper implements FieldMapper<String[]> {
    private final static Splitter LINE_SPLITTER = Splitter.on(CharMatcher.anyOf("\r\n")).omitEmptyStrings().trimResults(); 
    private final LineExtractor<String> extractor;
    private final boolean alsoSetPhotographed;

    PhotosMapper(LineExtractor<String> extractor, boolean alsoSetPhotographed) {
      this.extractor = extractor;
      this.alsoSetPhotographed = alsoSetPhotographed;
    }

    @Override
    public void map(String[] line, Builder sighting) {
      String photosString = Strings.emptyToNull(extractor.extract(line));
      if (photosString != null) {
        // Split the line on any CR or LF
        List<String> split = LINE_SPLITTER.splitToList(photosString);
        if (!split.isEmpty()) {
          List<Photo> photos = Lists.newArrayList();
          for (String photoString : split) {
            // Try to parse first as a URI, then as a file
            try {
              URI uri = new URI(photoString);
              photos.add(new Photo(uri));
            } catch (URISyntaxException e) {
              File file = new File(photoString);
              if (file.exists()) {
                photos.add(new Photo(file));
              }
            }
          }
          
          // ... and if anything came back, set the photos and (optionally)
          // turn on "photographed".
          if (!photos.isEmpty()) {
            sighting.getSightingInfo().setPhotos(photos);
            if (alsoSetPhotographed) {
              sighting.getSightingInfo().setPhotographed(true);
            }
          }
        }
      }
    }
  }

  @Override
  protected void lookForVisitInfo(
      ReportSet reportSet,
      String[] line,
      Sighting newSighting,
      VisitInfoKey visitInfoKey) {
    // Merge results in with any other data gathered for this visit (e.g.,
    // do not assume that all visit info data is set on all lines)
    VisitInfo.Builder builder = visitInfoMap.get(visitInfoKey);
    if (builder == null) {
      // Default to HISTORICAL, which is fairly safe
      builder = VisitInfo.builder().withObservationType(ObservationType.HISTORICAL);
      visitInfoMap.put(visitInfoKey, builder);
    }

    String observationTypeString = observationTypeExtractor.extract(line);
    if (!Strings.isNullOrEmpty(observationTypeString)) {
      ObservationType observationType = ObservationType.fromId(observationTypeString);
      if (observationType != null) {
        builder.withObservationType(observationType);
      }
    }
    
    Integer duration = durationExtractor.extract(line);
    if (duration != null) {
      builder.withDuration(Duration.standardMinutes(duration));
    } else {
      // Try to compute a duration from a start and end time.
      String startTime = startTimeExtractor.extract(line);
      String endTime = endTimeExtractor.extract(line);
      if (!Strings.isNullOrEmpty(startTime) && !Strings.isNullOrEmpty(endTime)) {
        try {
          LocalTime start = TimeIO.fromExternalString(startTime);
          LocalTime end = TimeIO.fromExternalString(endTime);
          Duration durationObj = Minutes.minutesBetween(start, end).toStandardDuration();
          if (durationObj.isLongerThan(Duration.standardMinutes(5))) {
            builder.withDuration(durationObj);
          }
        } catch (IllegalArgumentException e) {
          logger.log(Level.WARNING, "Failed to parse times " + startTime + ", " + endTime, e);
        }
      }
    }
    
    
    
    Float distance = distanceExtractor.extract(line);
    if (distance != null) {
      builder.withDistance(Distance.inKilometers(distance));
    }
    
    Float area = areaExtractor.extract(line);
    if (area != null) {
      builder.withArea(Area.inHectares(area));
    }
    
    Integer partySize = partySizeExtractor.extract(line);
    if (partySize != null && partySize > 0) {
      builder.withPartySize(partySize);
    }
    
    String visitComments = visitCommentsExtractor.extract(line);
    if (!Strings.isNullOrEmpty(visitComments)) {
      builder.withComments(visitComments);
    }
    
    if (completeListExtractor.extract(line)) {
      builder.withCompleteChecklist(true);
    }      
  }

  @Override
  public Map<VisitInfoKey, VisitInfo> getAccumulatedVisitInfoMap() {
    Map<VisitInfoKey, VisitInfo> accumulated = new LinkedHashMap<>();
    // Convert the assembled data into final VisitInfo objects
    for (Map.Entry<VisitInfoKey, VisitInfo.Builder> entry : visitInfoMap.entrySet()) {
      VisitInfo.Builder builder = entry.getValue();
      VisitInfo visitInfo;
      try {
        visitInfo = builder.build();
      } catch (IllegalStateException e) {
        // If building the visit info fails - there may be data missing
        // from the export (especially a missing required field). Swap
        // the observation type to HISTORICAL (which accepts everything),
        // and try again.
        builder.withObservationType(ObservationType.HISTORICAL);
        visitInfo = builder.build();
      }

      // If there's any data worth storing, do so.
      if (visitInfo.hasData()) {
        accumulated.put(entry.getKey(), visitInfo);
      }

    }
    return accumulated;
  }

  @Override
  public Optional<String> initialCheck() throws IOException {
    Optional<String> error = super.initialCheck();
    if (error.isPresent()) {
      error = Optional.of(error.get()
          + "<p>"
          + Messages.getMessage(Name.OFTEN_DATES_CANNOT_BE_READ));
    }
    
    return error;
  }

  /** Disable sighting status checks for Scythebill imports, to preserve round-tripping. */
  @Override
  protected boolean checkForDefaultSightingStatus() {
    return false;
  }  

  @Override
  protected String[] getHeaderRow(ImportLines lines) throws IOException {
    return lines.nextLine();
  }

  @Override
  protected boolean skipLine(String[] line) {
    // Skip any line with no common or scientific name
    if (Strings.isNullOrEmpty(commonNameExtractor.extract(line))
        && Strings.isNullOrEmpty(scientificExtractor.extract(line))) {
      return true;
    }
    
    return false;
  }

  @Override
  public boolean importContainedUserInformation() {
    return importContainedUserInformation;
  }
}
