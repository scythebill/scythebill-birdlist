/*   
 * Copyright 2012 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.joda.time.ReadablePartial;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Multiset;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.Sighting.Builder;
import com.scythebill.birdlist.model.sighting.SightingInfo;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.imports.TaxonImporter.ToBeDecided;

/**
 * Base class for importing sightings into Scythebill. The procedure for an import requires multiple steps.
 * <p>
 * <ol>
 * <li>Run a quick initial check, looking for a single importable line (ignoring taxa/locations).
 * <li>Parse all taxa, via a call to {@link #parseTaxonomyIds}. This returns a map of taxa that
 * could not be automatically resolved.
 * <li>An independent UI calls {@link #registerTaxon} with manually registered taxa.
 * <li>Location resolution is triggered with {@link #parseLocationIds}. This too returns a map of
 * unresolved locations.
 * <li>An independent UI calls {@link ParsedLocationIds#put} to resolve the remaining locations.
 * <li>Finally, {@link #parseSightings} finishes the process.
 * </ol>
 * 
 * <p>Most importers should inherit from CsvSightingsImporter - even if they're not *really*
 * CSV files, but merely have a tabular format that can follow the {@code ImportLines} format. 
 */
public abstract class SightingsImporter<T> {
  private final static Logger logger = Logger.getLogger(SightingsImporter.class.getName());

  protected final ReportSet reportSet;
  private final Taxonomy taxonomy;
  private final Taxonomy baseTaxonomy;
  protected final PredefinedLocations predefinedLocations;
  private final Checklists checklists;
  private final Map<Object, TaxonPossibilities> taxonomyIds = Maps.newHashMap();
  private int successfullyResolvedTaxa;
  private final Set<SightingTaxon> spToResolve = Sets.newLinkedHashSet();
  protected final ParsedLocationIds locationIds;
  private final Table<VisitInfoKey, SightingTaxon, Sighting> sightingsByVisitInfoAndTaxon =
      HashBasedTable.create();

  private ImmutableSet<Object> ignoredTaxonIds = ImmutableSet.of();
  private int sightingsFound;
  private int failureCount;
  private int sightingsMerged = 0;
  private Multiset<ReadablePartial> sightingsEBirdSpuhsDroppedCountByDate = HashMultiset.create();
  private int sightingsEBirdSpuhsDroppedCount;

  protected final LocationShortcuts locationShortcuts;

  public SightingsImporter(
      ReportSet reportSet,
      Taxonomy taxonomy,
      Checklists checklists,
      PredefinedLocations predefinedLocations) {
    this.reportSet = reportSet;
    this.taxonomy = taxonomy;
    this.checklists = checklists;
    this.predefinedLocations = predefinedLocations;
    this.baseTaxonomy = TaxonUtils.getBaseTaxonomy(taxonomy);
    this.locationShortcuts = new LocationShortcuts(reportSet.getLocations(), predefinedLocations);
    this.locationIds = new ParsedLocationIds();
  }

  // ABSTRACT APIs, all of which must be overridden
  
  public interface RowOperator<T> {
    void operate(T row) throws IOException;
  }  
  protected abstract void operateOnAllRows(RowOperator<T> operator) throws IOException;
  protected abstract TaxonImporter<T> newTaxonImporter(Taxonomy taxonomy);
  protected abstract RowExtractor<T, ? extends Object> taxonomyIdExtractor();
  protected abstract void parseLocationIds(LocationSet locations, PredefinedLocations predefinedLocations) throws IOException ;
  public abstract List<Sighting> parseSightings() throws IOException;
  public abstract String importFileName();

  /** Record that a particular row failed. */
  protected abstract void importRowFailed(T importRow);

  /**
   * Try to write failed lines to another file.
   * 
   * @return the File, if the write succeeded, or null if it did not
   */
  public abstract File writeFailedLines();  

  
  public Optional<String> initialCheck() throws IOException {
    return Optional.absent();
  }

  /**
   * Returns true iff the import contained information about the users involved in each sighting.
   */
  public boolean importContainedUserInformation() {
    return false;
  }

  /**
   * Override to return true if an import file represents a "mass export", which just
   * contains all of a user's data.  Such files should support automatic subset
   * importing of just "new dates".
   */
  public boolean isMassExport() {
    return false;
  }

  /**
   * Must be overridden if isMassExport() returns true.
   */
  public Collection<ReadablePartial> getAllDates() throws IOException {
    return null;
  }

  /** Must be overridden and honored if isMassExport() returns true. */
  public void onlyImportDatesIn(Set<ReadablePartial> newDates) {
  }  

  protected Taxonomy getTaxonomy() {
    return taxonomy;
  }

  protected Taxonomy getBaseTaxonomy() {
    return baseTaxonomy;
  }
  
  /**
   * Returns a Runnable which should be executed in a background thread before
   * beginning the import, or null if no task is needed.
   */
  public Runnable longRunningTask() {
    return null;
  }
  
  public void parseSighting(
      T importRow,
      BiConsumer<T, Sighting.Builder> importRowParser,
      Function<T, Object> taxonomyIdFunction,
      List<Sighting> sightings,
      // A parser that doesn't fully create the sighting, but should be able to extract
      // a date.
      BiConsumer<T, Sighting.Builder> attemptDateRowParser) {
    sightingsFound++;

    Sighting.Builder newBuilder = Sighting.newBuilder().setTaxonomy(getBaseTaxonomy());
    try {
      importRowParser.accept(importRow, newBuilder);
      if (checkForDefaultSightingStatus()) {
        defaultSightingStatus(newBuilder);
      }

      finishSighting(newBuilder, importRow);
      Sighting newSighting = newBuilder.build();
      VisitInfoKey visitInfoKey = VisitInfoKey.forSighting(newSighting);
      // Check if there's two sightings for the same SightingTaxon, same VisitInfoKey.
      // Scythebill is not happy with this (but some systems, e.g. Observado, are fine) so merge
      // before storing.
      if (visitInfoKey != null) {
        Sighting existingSighting = sightingsByVisitInfoAndTaxon.get(visitInfoKey, newSighting.getTaxon());
        if (existingSighting != null) {
          newSighting = existingSighting.mergeWith(newSighting);
          sightings.remove(existingSighting);
          sightingsFound--;
          sightingsMerged++;
        }

        sightingsByVisitInfoAndTaxon.put(visitInfoKey, newSighting.getTaxon(), newSighting);
      }

      sightings.add(newSighting);

      if (visitInfoKey != null) {
        lookForVisitInfo(reportSet, importRow, newSighting, visitInfoKey);
      }
    } catch (RuntimeException e) {
      Object taxonomyId = taxonomyIdFunction.apply(importRow);
      // If this line contained a sighting that should be ignored, then don't append
      // to "failed lines", and drop sightingsFound.
      // (It would be possible to do this extraction, etc. above, and never increment
      // sightingsFound.
      // This seems *slightly* more paranoid, though - if an entry got into ignoredTaxonIds that
      // shouldn't
      // be ignored, and actually parses just fine, then import would drop sightings silently.)
      if (ignoredTaxonIds.contains(taxonomyId)) {
        sightingsEBirdSpuhsDroppedCount++;
        try {
          attemptDateRowParser.accept(importRow, newBuilder);
          ReadablePartial date = newBuilder.getDate();
          if (date != null) {
            sightingsEBirdSpuhsDroppedCountByDate.add(date);
          }
        } catch (RuntimeException re) {
          // Ah well.
        }
        sightingsFound--;
      } else {
        importRowFailed(importRow);
        failureCount++;
        if (failureCount < 10) {
          logger.log(Level.WARNING, "Failure building sighting", e);
        } else if (failureCount < 100) {
          logger.log(Level.WARNING, "Failure building sighting: {0}, {1}",
              new Object[] {e.getMessage(), e.getClass().getName()});
        } else if (failureCount == 100) {
          logger.severe("Too many errors, not logging more.");
        }
      }
    }
  }
  
  /** Jan.1 1900 is treated as "no date available" for some imports.  Clear that. */
  private static final ReadablePartial NO_DATE_DATE = PartialIO.fromString("1900-01-01");
  
  protected void finishSighting(Sighting.Builder newSighting, T line) {
    ReadablePartial date = newSighting.getDate();
    // If the date is set to an arbitrary date in the past, clear the values entirely.
    // Yes, this will go poorly if someone, someday, actually imports historical data
    // for that exact date.
    if (date != null && date.equals(NO_DATE_DATE)) {
      newSighting.setDate(null);
      newSighting.setTime(null);
    }
  }
  
  protected void lookForVisitInfo(ReportSet reportSet, T line, Sighting newSighting,
      VisitInfoKey visitInfoKey) {}
  
  protected boolean checkForDefaultSightingStatus() {
    return true;
  }

  /**
   * Set a default sighting status.
   */
  private void defaultSightingStatus(Sighting.Builder builder) {
    if (builder.getSightingInfo().getSightingStatus() != SightingInfo.SightingStatus.NONE) {
      return;
    }

    // See if the taxon has a default status (domestic forms, etc.)
    SightingStatus sightingStatus = taxonToSightingStatus(builder.getTaxon());
    if (sightingStatus == SightingInfo.SightingStatus.NONE
        // Domestic sightings might be escaped, might be "introduced", might be domestic!
        || sightingStatus == SightingInfo.SightingStatus.DOMESTIC) {
      // Now, need to get the checklist for this location and look for default status
      Checklist checklist = getBuiltInChecklist(builder.getLocation());
      // TODO: Map up to the species level? Otherwise, subspecies or group imports won't properly
      // trip introduced status.
      if (checklist != null) {
        Checklist.Status status = checklist.getStatus(getBaseTaxonomy(), builder.getTaxon());
        if (status == Checklist.Status.INTRODUCED
            || status == Checklist.Status.RARITY_FROM_INTRODUCED) {
          sightingStatus = SightingStatus.INTRODUCED;
        } else if (status == Checklist.Status.ESCAPED) {
          sightingStatus = SightingStatus.INTRODUCED_NOT_ESTABLISHED;
        }
      }
    }

    if (sightingStatus != SightingStatus.NONE) {
      builder.getSightingInfo().setSightingStatus(sightingStatus);
    }
  }

  private SightingInfo.SightingStatus taxonToSightingStatus(SightingTaxon sightingTaxon) {
    if (sightingTaxon.getType() != SightingTaxon.Type.SINGLE) {
      return SightingInfo.SightingStatus.NONE;
    }

    Taxon taxon = getBaseTaxonomy().getTaxon(sightingTaxon.getId());
    switch (taxon.getStatus()) {
      case IN:
        return SightingInfo.SightingStatus.INTRODUCED;
      case DO:
        return SightingInfo.SightingStatus.DOMESTIC;
      default:
        return SightingInfo.SightingStatus.NONE;
    }
  }

  
  public final ParsedLocationIds parseLocationIds() throws IOException {
    LocationSet locations = reportSet.getLocations();    
    parseLocationIds(locations, predefinedLocations);
    
    return locationIds;
  }

  protected Checklist getBuiltInChecklist(Location location) {
    return checklists.getNearestBuiltInChecklist(taxonomy, reportSet, location);
  }

  public int getSightingsFoundCount() {
    return sightingsFound;
  }

  public int getFailedSightingsCount() {
    return failureCount;
  }

  /** Returns the total number of sightings merged away. */
  public int getSightingsMergedCount() {
    return sightingsMerged;
  }

  /** Returns the total number of sightings merged away. */
  public int getSightingsEBirdSpuhsDroppedCount() {
    return sightingsEBirdSpuhsDroppedCount;
  }

  /**
   * Return any visit info accumulated during the import; override if there 
   * is any.
   */
  public Map<VisitInfoKey, VisitInfo> getAccumulatedVisitInfoMap() {
    return ImmutableMap.of();
  }

  /**
   * Parse taxonomic entries. Must be the first step in an import. Returns a map of taxonomic
   * entries that have not yet been resolved.
   */
  public Map<Object, ToBeDecided> parseTaxonomyIds() throws IOException {
    successfullyResolvedTaxa = 0;
    taxonomyIds.clear();
    Map<Object, ToBeDecided> tbdMap = Maps.newLinkedHashMap();
    TaxonImporter<T> importer = newTaxonImporter(getTaxonomy());
    operateOnAllRows((T row) -> {
      Object key = taxonomyIdExtractor().extract(row);
      // If this taxon has been decided (or not-decided, as the case may be), skip it
      if (!taxonomyIds.containsKey(key) && !tbdMap.containsKey(key)) {
        TaxonPossibilities taxon = importer.map(row);
        if (taxon != null) {
          registerTaxon(key, taxon);
          successfullyResolvedTaxa++;
        } else {
          ToBeDecided decideLater = importer.decideLater(row);
          tbdMap.put(key, decideLater);
        }
      }
      
      preSightingAnalysis(row);
    });
    
    return tbdMap;
  }

  /**
   * Return any "spuh"s which might need resolution after the import.
   */
  public Set<SightingTaxon> getSpToResolve() {
    return Collections.unmodifiableSet(spToResolve);
  }


  /**
   * Sets a list of taxon ids that were explicitly ignored, and where failure to import should
   * be considered irrelevant.
   */
  public void setIgnoredTaxonIds(Set<Object> ignoredTaxonIds) {
    this.ignoredTaxonIds = ImmutableSet.copyOf(ignoredTaxonIds);
  }

  /**
   * Register a taxon.
   */
  public void registerTaxon(Object key, TaxonPossibilities taxon) {
    if (taxonomyIds.containsKey(key)) {
      logger.warning("Re-registering " + key);
    }
    taxonomyIds.put(key, taxon);
  }

  /**
   * Returns how many taxa were successfully resolved.
   */
  public int successfullyResolvedTaxa() {
    return successfullyResolvedTaxa;
  }

  /**
   * Hook for doing parsing of the file prior to reading the Sightings. The only guarantee that can
   * be made is that this *can't* influence parsing of taxa, but *can* influence location and
   * sighting parsing (including the ComputedMapping for those two sections.
   */
  protected void preSightingAnalysis(T row) {}

  protected class LocationMapper implements FieldMapper<T> {
    private final RowExtractor<T, ? extends Object> locationKeyExtractor;

    public LocationMapper(RowExtractor<T, ? extends Object> locationKeyExtractor) {
      this.locationKeyExtractor = locationKeyExtractor;
    }

    @Override
    public void map(T line, Builder sighting) {
      Object key = locationKeyExtractor.extract(line);
      Preconditions.checkState(locationIds.containsKey(key), "Cannot find location key %s", key);
      String locationId = locationIds.getLocationId(key);
      // If null was explicitly inserted, then there's no location for this sighting
      if (locationId != null) {
        Location location = reportSet.getLocations().getLocation(locationId);
        Preconditions.checkNotNull(location, "Location id %s not in report set", locationId);
        sighting.setLocation(location);
      }
    }
  }

  protected class TaxonFieldMapper implements FieldMapper<T> {
    private final RowExtractor<T, ? extends Object> taxonKeyExtractor;

    public TaxonFieldMapper(RowExtractor<T, ? extends Object> taxonKeyExtractor) {
      this.taxonKeyExtractor = taxonKeyExtractor;
    }

    @Override
    public void map(T line, Builder sighting) {
      Object key = taxonKeyExtractor.extract(line);
      Preconditions.checkState(taxonomyIds.containsKey(key), "Cannot find taxon key %s", key);
      TaxonPossibilities possibilities = taxonomyIds.get(key);
      Checklist checklist = null;
      // Find a checklist for this sighting, to improve alternate selection
      // ... but only bother if there's some alternates to choose from, as this makes
      // imports much slower. On an extremely small sample size, relative to not using
      // checklists at all
      // - Always loading a checklist: 1000% slower
      // - Using a checklist only when alternates exist: 70% slower.
      if (possibilities.hasAlternates()) {
        if (sighting.getLocation() != null) {
          checklist = getBuiltInChecklist(sighting.getLocation());
        }
      }

      SightingTaxon chosen = possibilities.choose(checklist, getTaxonomy());
      sighting.setTaxon(chosen);

      // If checklist resolution turned this into a "sp.", remember that.
      if (possibilities.choseSpToResolve(chosen)) {
        spToResolve.add(chosen);
      }
    }
  }
  
  /**
   * If true (the default), then a typical taxon should be imported successfully.  Override to
   * return false if lots of failed matches seem reasonable.
   */
  public boolean expectTaxonSuccess() {
    return true;
  }
  
  public void beforeParseTaxonomyIds() throws IOException {
  }
}
