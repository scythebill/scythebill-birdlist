/*   
 * Copyright 2020 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import javax.annotation.Nullable;

import com.google.common.base.MoreObjects;

/**
 * Interface for computing the expected taxa of a row of data.
 */
public abstract class TaxonImporter<T> {

  public static class ToBeDecided {
    public String commonName;
    public String scientificName;
    public String subspecies;
    public String longText;
    
    public ToBeDecided() {}
    
    /** Ignore any ToBeDecided if it's definitely a "spuh". */
    public boolean shouldIgnore() {
      // NOTE: if you change this logic, then the "IMPORT_EBIRD_SPUHS_MERGED" message may not
      // make any sense
      return (commonName != null && (commonName.endsWith(" sp.") || commonName.endsWith(" sp.)")))
          && (scientificName == null || scientificName.endsWith(" sp."));
    }
    
    @Override
    public String toString() {
      return MoreObjects.toStringHelper(getClass()).omitNullValues()
          .add("common", commonName).add("scientific", scientificName).add("ssp", subspecies).add("longText", longText).toString();
    }
  }

  /**
   * Given a line, returns the expected {@link TaxonPossibilities}.
   * Returns null if no taxa could be found.
   */
  @Nullable public abstract TaxonPossibilities map(T line);

  /** Extracts enough information to attempt later resolution of a taxon. */
  public abstract ToBeDecided decideLater(T line);
}
