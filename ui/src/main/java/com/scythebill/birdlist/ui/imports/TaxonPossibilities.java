/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Type;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Possibilities computed for the import of a given taxon.
 */
public class TaxonPossibilities {
  private final SightingTaxon primary;
  private SightingTaxon anotherPrimary;
  private Set<SightingTaxon> alternates;

  public static TaxonPossibilities adding(
      TaxonPossibilities base,
      SightingTaxon more) {
    if (base == null) {
      return new TaxonPossibilities(more);
    }
    
    // "Adding" an existing entry
    if (base.primary.equals(more)) {
      return base;
    }
    
    if (base.alternates == null) {
      base.alternates = Sets.newLinkedHashSetWithExpectedSize(4);
    }
    base.alternates.add(more);
    return base;
  }
  
  /**
   * Add a primary of equivalent merit to the current one.
   */
  public static TaxonPossibilities withPrimary(
      TaxonPossibilities base,
      SightingTaxon anotherPrimary) {
    // If:
    // - there is already a base
    // - which doesn't have "another primary"
    // - and this "other primary" is not the original primary
    // ... then set "another primary"
    if (base != null
        && base.anotherPrimary == null
        && !base.primary.equals(anotherPrimary)) {
      base.anotherPrimary = anotherPrimary;
      return base;
    }
    
    return adding(base, anotherPrimary);
  }
    
  public TaxonPossibilities(SightingTaxon primary) {
    this.primary = Preconditions.checkNotNull(primary);
  }
  
  public SightingTaxon choose(Checklist checklist, Taxonomy taxonomy) {
    SightingTaxon chosen = chooseInternal(checklist, taxonomy);
    
    // After choosing, attempt to simplify any "spuhs".
    if (chosen.getType() == SightingTaxon.Type.SP) {
      Taxonomy base = (taxonomy instanceof MappedTaxonomy) ? 
          ((MappedTaxonomy) taxonomy).getBaseTaxonomy() : taxonomy;
          
      chosen = SightingTaxons.newPossiblySpTaxon(
          TaxonUtils.simplifyTaxa(base, chosen.getIds()));
    }
    
    return chosen;
  }
  
  private SightingTaxon chooseInternal(Checklist checklist, Taxonomy taxonomy) {
    if (alternates == null && anotherPrimary == null) {
      // No alternates -> nothing to choose from
      return primary;
    }
    if (checklist == null) {
      // No checklist to use to choose, return immediately
      if (anotherPrimary == null) {
        return primary;
      }
      return unify(ImmutableList.of(primary, anotherPrimary), taxonomy);
    }
    
    // An explicit "sp." or "hybrid";  don't consider alternates
    if (primary.getType() != SightingTaxon.Type.SINGLE
        && primary.getType() != SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
      return primary;
    }
    
    // See which of the current primaries are at all possible for the checklist.
    List<SightingTaxon> validPrimaries = new ArrayList<SightingTaxon>();
    if (resolveStatusOfPossibility(checklist, taxonomy, primary) != null) {
      validPrimaries.add(primary);
    }
    if (anotherPrimary != null
        && resolveStatusOfPossibility(checklist, taxonomy, anotherPrimary) != null) {
      validPrimaries.add(anotherPrimary);
    }
    // If one or both are possible, return them.
    if (!validPrimaries.isEmpty()) {
      return unify(validPrimaries, taxonomy);
    }
    
    // First strategy:  walk the list of alternates, seeing if any are present, return the first.
    // BETTER:
    //   Make a "sp." out of all that aren't rarities
    //   Then second pass, make a sp. out of any that are rarities
    List<SightingTaxon> nonRarities = Lists.newArrayList();
    List<SightingTaxon> rarities = Lists.newArrayList();
    for (SightingTaxon alternate : alternates) {
      Status status = resolveStatusOfPossibility(checklist, taxonomy, alternate);
      if (status == Status.RARITY || status == Status.RARITY_FROM_INTRODUCED) {
        rarities.add(alternate);
      } else if (status != null) {
        nonRarities.add(alternate);
      }
    }
    
    if (!nonRarities.isEmpty()) {
      return unify(nonRarities, taxonomy);
    } else if (!rarities.isEmpty()) {
      return unify(rarities, taxonomy);
    }
    
    if (anotherPrimary == null) {
      return primary;
    }
    return unify(ImmutableList.of(primary, anotherPrimary), taxonomy);
  }

  Status resolveStatusOfPossibility(Checklist checklist, Taxonomy taxonomy,
      SightingTaxon alternate) {
    SightingTaxon speciesParent = alternate.resolve(taxonomy).getParentOfAtLeastType(Taxon.Type.species);
    Status status = checklist.getStatus(taxonomy, speciesParent);
    return status;
  }
  
  /**
   * Returns true iff the chosen output was a "spuh", and the input wasn't inherently a "spuh".
   */
  public boolean choseSpToResolve(SightingTaxon chosen) {
    return chosen.getType() == SightingTaxon.Type.SP
        && primary.getType() != SightingTaxon.Type.SP;
  }

  private SightingTaxon unify(List<SightingTaxon> taxa, Taxonomy taxonomy) {
    if (taxa.size() == 1) {
      return taxa.get(0);
    }
    
    // Resolve everything, sorting by type so subspecies or groups come first
    List<Resolved> sortedResolved = taxa.stream()
        .map(taxon -> taxon.resolve(taxonomy))
        .sorted(comparing((Resolved taxon) -> taxon.getSmallestTaxonType()))
        .collect(toList());
    // ... and figure out how many distinct types occur
    long countOfTypes = sortedResolved.stream()
        .map(taxon -> taxon.getSmallestTaxonType())
        .distinct()
        .count();

    // If there's only one type, then the "sp" below will do just fine.
    // Otherwise, there's more work to do.
    if (countOfTypes > 1) {
      // If the most specific sighting is a child of everything else, then just use that;
      // this helps when one of the columns is more specific than others (which happens in some
      // eBird exports, where the scientific name is just the species, but the common name might
      // point to a specific subspecies)
      Resolved mostSpecific = sortedResolved.get(0);
      if (sortedResolved.stream().skip(1).allMatch(
          taxon -> isParentOf(mostSpecific, taxon))) {
        SightingTaxon mostSpecificSightingTaxon = mostSpecific.getSightingTaxon();
        if (taxonomy instanceof MappedTaxonomy) {
          MappedTaxonomy mappedTaxonomy = (MappedTaxonomy) taxonomy;
          mostSpecificSightingTaxon = mappedTaxonomy.getMapping(mostSpecificSightingTaxon);
        }
        // ... and done
        return mostSpecificSightingTaxon;
      } else {
        // Map everything up to species level
        Stream<SightingTaxon> speciesInTaxonomy = sortedResolved.stream()
            .map(taxon -> taxon.getParentOfAtLeastType(Taxon.Type.species))
            .distinct();
        // ... and maybe back to the base taxonomy
        if (taxonomy instanceof MappedTaxonomy) {
          MappedTaxonomy mappedTaxonomy = (MappedTaxonomy) taxonomy;
          speciesInTaxonomy = speciesInTaxonomy.map(taxon -> mappedTaxonomy.getMapping(taxon));
        }
        taxa = speciesInTaxonomy.collect(toList());
      }
    }
        
    // Collect the streams with a stable order
    LinkedHashSet<String> ids = taxa.stream()
        .flatMap(taxon -> taxon.getIds().stream())
        .collect(toCollection(LinkedHashSet::new));
    
    return SightingTaxons.newPossiblySpTaxon(ids);
  }

  private boolean isParentOf(Resolved mostSpecific, Resolved maybeParent) {
    if (mostSpecific.getType() == Type.SP || mostSpecific.getType() == Type.HYBRID) {
      return false;
    }
    Taxon mostSpecificTaxon = mostSpecific.getTaxon();
    if (maybeParent.getType() == Type.SP || maybeParent.getType() == Type.HYBRID) {
      return false;
    }
    Taxon maybeParentTaxon = maybeParent.getTaxon();
    return TaxonUtils.isChildOf(maybeParentTaxon, mostSpecificTaxon);
  }

  public boolean hasAlternates() {
    return alternates != null || anotherPrimary != null;
  }
  
  @Override
  public String toString() {
    if (alternates == null) {
      return primary.toString();
    }
    return String.format("%s[%s]", primary, Joiner.on('/').join(alternates));
  }
}
