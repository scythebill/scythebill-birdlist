/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import static com.google.common.collect.Iterables.getLast;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.text.similarity.LevenshteinDistance;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.BKTree;
import com.scythebill.birdlist.model.util.Metrics;

/**
 * Resolves a common name and scientific name to a set of possibilities
 * in a given taxonomy.
 */
public class TaxonResolver {
  private static final Splitter SP_SPLITTER = Splitter.on('/').trimResults(CharMatcher.whitespace()).omitEmptyStrings();
  private final Map<String, Taxon> newGenusNames = Maps.newHashMap();
  private final Multimap<String, Taxon> newSpeciesNames = LinkedHashMultimap.create();
  private final Multimap<String, Taxon> newCommonNames = LinkedHashMultimap.create();
  private final Multimap<String, Taxon> alternateCommonNames = LinkedHashMultimap.create();
  private final Multimap<String, Taxon> alternateSciNames = LinkedHashMultimap.create();
  private final BKTree<String, Taxon> commonNameTree;
  private final BKTree<String, Taxon> alternateCommonNameTree;
  private final Taxonomy taxonomy;
  private int maximumNameDistance = 2;

  public TaxonResolver(Taxonomy taxonomy) {
    this.taxonomy = taxonomy;
    // For every common name, store it in a BKTree to make finding species
    // by (closely-spelled) names easy.
    BKTree.Builder<String, Taxon> commonNameTree = BKTree.builder(Metrics.levenshteinDistance());
    buildIndicesForTaxonomy(commonNameTree);
    this.commonNameTree = commonNameTree.build();
    
    BKTree.Builder<String, Taxon> alternateCommonNameTree = BKTree.builder(Metrics.levenshteinDistance());
    // For any alternate name that is unique, store it off in a BKTree to make finding
    // species by (closely-spelled) names easy
    for (Entry<String, Collection<Taxon>> alternateEntry : alternateCommonNames.asMap().entrySet()) {
      if (alternateEntry.getValue().size() == 1) {
        alternateCommonNameTree.add(
                alternateEntry.getKey(),
                Iterables.getOnlyElement(alternateEntry.getValue()));
      }
    }
    this.alternateCommonNameTree = alternateCommonNameTree.build();
  }

  static private Pattern COMMON_WITH_FORM = Pattern.compile(
      "^(.+) \\(.*\\)$");

  public void setMaximumNameDistance(int maximumNameDistance) {
    this.maximumNameDistance = maximumNameDistance;
  }
  
  /** Subspecies are lowercase letters, plus (rarely) a hyphen (v-nigra). */
  private static final CharMatcher VALID_SUBSPECIES_CHARS = CharMatcher.inRange('a', 'z').or(CharMatcher.is('-'));
  
  /**
   * Handle receiving a string which may be a common name, may be a scientific name,
   * might be either plus a subspecies!
   */
  public TaxonPossibilities mapUnknown(String commonOrSci) {
    // If it's just a common name, return that
    TaxonPossibilities ifCommon = map(commonOrSci, null, null);
    if (ifCommon != null) {
      return ifCommon;
    }
    TaxonPossibilities ifSci = map(null, commonOrSci, null);
    if (ifSci != null) {
      return ifSci;
    }

    int lastSpace = commonOrSci.lastIndexOf(' ');
    if (lastSpace > 0) {
      String mainName = commonOrSci.substring(0, lastSpace);
      String subspecies = commonOrSci.substring(lastSpace + 1);
      
      // Make sure that the "subspecies" actually looks like one!
      // Otherwise, weirdness results.  E.g.: "American Wood Duck"
      // gets loaded as "American Wood", ssp. "Duck", and American Wood is
      // close to American Coot and oh no...
      if (VALID_SUBSPECIES_CHARS.matchesAllOf(subspecies)) {      
        TaxonPossibilities commonAndSubspecies = map(mainName, null, subspecies);
        if (commonAndSubspecies != null) {
          return commonAndSubspecies;
        }
  
        TaxonPossibilities sciAndSubspecies = map(null, mainName, subspecies);
        if (sciAndSubspecies != null) {
          return sciAndSubspecies;
        }
      }
    }

    return null;
  }
  
  public TaxonPossibilities map(String commonName, String sciName, String subspecies) {
    TaxonPossibilities possibilities = null;
    
    Collection<Taxon> collection;
    
    if (commonName != null) {
      commonName = normalizeCommonName(commonName, subspecies);
      collection = newCommonNames.get(commonName);
      if (collection.size() == 1) {
        possibilities = TaxonPossibilities.withPrimary(possibilities, getTaxon(collection, subspecies));
      }
    }
    
    if (sciName != null) {
      sciName = normalizeScientificName(sciName);
      collection = newSpeciesNames.get(sciName);
      if (collection.size() == 1) {
        // Add the scientific name to the set of possibilities (as a primary)
        possibilities = TaxonPossibilities.withPrimary(possibilities, getTaxon(collection, subspecies));
      }
    }
    
    // Haven't found anything yet;  add near-matches of common names and scientific names to the mix
    if (possibilities == null) {
      if (commonName != null) {
        collection = commonNameTree.search(commonName, maximumNameDistance);
        if (collection.size() == 1) {
          // Just one such match.  See if improvements can be made looking within the genus
          boolean improvedUsingGenus = false;
          if (sciName != null) {
            List<String> list = Splitter.on(' ').splitToList(sciName);
            if (list.size() >= 2) {
              String species = list.get(1);
              // Since it's not a perfect match (if it were, that would have been handled by the scientific name above),
              // see if there's a better match for the species name in that genus.
              // The basic trick here is to use the near-common name match to get us to a current genus, then
              // use the species name to get to a better fit.
              Taxon genus = TaxonUtils.getParentOfType(collection.iterator().next(), Taxon.Type.genus);
              Taxon speciesInGenus = findMatchWithin(genus, species, false);
              if (speciesInGenus != null) {
                possibilities = TaxonPossibilities.adding(possibilities, getTaxon(speciesInGenus, subspecies));
                improvedUsingGenus = true;
              }
            }
          }
          
          // Nope, using the genus didn't help.  
          if (!improvedUsingGenus) {
            possibilities = TaxonPossibilities.adding(possibilities, getTaxon(collection, subspecies));
          }
        }
      }    
    }
      
    // Now, keep using the scientific name.  Grab the genus, and then look within that
    // genus for *any* taxon with that name.  This catches a lot of lumps.
    if (sciName != null && possibilities == null) {
      Taxon matchWithinGenus = findTaxonByScientificNameInGenus(sciName);
      if (matchWithinGenus != null) {
        possibilities = TaxonPossibilities.adding(possibilities, getTaxon(matchWithinGenus, subspecies));
      }
    }

    if (commonName != null) {
      boolean noPossibilitiesYet = possibilities == null;
      // Now search exact matches for alternate common names;  add *all* of them as alternates
      for (Taxon exactAlternateCommonName : alternateCommonNames.get(commonName)) {
        // If there aren't any possibilities yet, then the first one added would be treated as a primary,
        // with latter ones alternates.  This is bad, given that they're all equally probable.
        if (noPossibilitiesYet) {
          possibilities = TaxonPossibilities.withPrimary(possibilities, getTaxon(exactAlternateCommonName, subspecies));          
        } else {
          possibilities = TaxonPossibilities.adding(possibilities, getTaxon(exactAlternateCommonName, subspecies));
        }
      }
      
      // And then close matches (at least for names of non-trivial length), and when nothing else has worked
      if (possibilities == null && commonName.length() > 4) {
        for (Taxon inexactAlternateCommonName : alternateCommonNameTree.search(commonName, maximumNameDistance)) {
          // Add as primaries so that all are considered equally plausible
          possibilities = TaxonPossibilities.withPrimary(possibilities, getTaxon(inexactAlternateCommonName, subspecies));
        }
      }

      // Now, look for common names like "Rock Pigeon (Domestic type)", and
      // extract "Rock Pigeon" (as an example)
      Matcher matcher = COMMON_WITH_FORM.matcher(commonName);
      if (matcher.matches()) {
        String reducedCommonName = matcher.group(1);
        collection = newCommonNames.get(reducedCommonName);
        if (collection.size() == 1) {
          possibilities = TaxonPossibilities.adding(possibilities, getTaxon(collection, subspecies));
        }

        collection = alternateCommonNames.get(reducedCommonName);
        if (collection.size() == 1) {
          possibilities = TaxonPossibilities.adding(possibilities, getTaxon(collection, subspecies));
        }
      }
    }

    if (sciName != null) {
      boolean noPossibilitiesYet = possibilities == null;
      for (Taxon exactAlternateSciName : alternateSciNames.get(sciName)) {
        if (noPossibilitiesYet) {
          possibilities = TaxonPossibilities.withPrimary(possibilities, getTaxon(exactAlternateSciName, subspecies));
        } else {
          possibilities = TaxonPossibilities.adding(possibilities, getTaxon(exactAlternateSciName, subspecies));
        }
      }
    }
    
    // OK, so none of *that* worked.  Let's look for sp. and hybrids (sci. name only)
    if (sciName != null && possibilities == null) {
      possibilities = checkForSpuhsInScientificName(sciName, possibilities);
    }
    
    
    // Still nothing doing.  Let's look for sp. and hybrids (common. name only)
    if (commonName != null && possibilities == null) {
      possibilities = checkForSpuhsInCommonName(commonName, possibilities);
    }

    return possibilities;
  }

  /**
   * See if the scientific name might contain a spuh or hybrid.
   */
  private TaxonPossibilities checkForSpuhsInScientificName(String sciName,
      TaxonPossibilities possibilities) {
    List<String> split = null;
    boolean hybrid = false;
    if (sciName.contains("/")) {
      split = SP_SPLITTER.splitToList(sciName);
    } else if (sciName.contains(" x ")) {
      hybrid = true;
      split = Splitter.on(" x ").splitToList(sciName);
    }

    // Bubulcus ibis/coromandus -> "Bubulcus ibis", "coromandus"
    // Egretta thula/Bubulcus ibis -> "Egretta thula", "Bubulcus ibis" 
    if (split != null && split.size() >= 2) {
      String first = split.get(0);
      // Extract the first taxon
      Taxon firstTaxon = findTaxonExactAndInGenus(first);
      if (firstTaxon != null) {
        Set<String> taxa = Sets.newLinkedHashSetWithExpectedSize(split.size());
        taxa.add(firstTaxon.getId());
        boolean allSuccessful = true;
        // Then extract all the rest
        for (String fromSplit : Iterables.skip(split, 1)) {
          // If a following name does not have a space, then it's 
          // like "coromandus" in "Bubulcus ibis/coromandus"
          if (!fromSplit.contains(" ")) {
            fromSplit = first.substring(0, first.indexOf(' ') + 1) + fromSplit;
          }
          
          // Try to resolve the unabbreviated name
          Taxon additionalTaxon = findTaxonExactAndInGenus(fromSplit);
          if (additionalTaxon != null) {
            taxa.add(additionalTaxon.getId());
          } else {
            allSuccessful = false;
          }
        }
 
        // Got it - create the sighting taxon
        if (allSuccessful) {
          SightingTaxon hybridOrSp;
          if (taxa.size() == 1) {
            // In some weird cases - specifically 
            // https://bitbucket.org/scythebill/scythebill-birdlist/issues/335/spuh-imports-that-dont-resolve-all-species
            // there might be only one species here.  (Where it ends being a spuh or hybrid of "two" things that map
            // to the same species.)  Handle gracefully.
            hybridOrSp = SightingTaxons.newSightingTaxon(taxa.iterator().next());
          } else if (hybrid) {
            hybridOrSp = SightingTaxons.newHybridTaxon(taxa);
          } else {
            hybridOrSp = SightingTaxons.newSpTaxon(taxa);
          }
          // ... and now map if needed
          if (firstTaxon.getTaxonomy() instanceof MappedTaxonomy) {
            MappedTaxonomy taxonomy = (MappedTaxonomy) firstTaxon.getTaxonomy();
            hybridOrSp = taxonomy.getMapping(hybridOrSp);
          }
          possibilities = TaxonPossibilities.adding(possibilities, hybridOrSp);
        }
      }
    }
    return possibilities;
  }

  /**
   * See if the common name might contain a spuh or hybrid.
   */
  private TaxonPossibilities checkForSpuhsInCommonName(String commonName,
      TaxonPossibilities possibilities) {
    List<String> split = null;
    boolean hybrid = false;
    if (commonName.contains("/")) {
      split = SP_SPLITTER.splitToList(commonName);
    } else if (commonName.contains(" x ")) {
      hybrid = true;
      split = Splitter.on(" x ").splitToList(commonName);
    }

    // Start with the last element, which is always a full name
    // (e.g. "Baltimore x Bullock's Oriole").
    if (split != null && split.size() >= 2) {
      String last = getLast(split);
      Taxon lastTaxon = findSpeciesForCommonName(last);
      // Try to strip out " (hybrid)" or anything similar.
      if (lastTaxon == null && last.contains(" (")) {
        last = last.substring(0, last.indexOf(" ("));
        lastTaxon = findSpeciesForCommonName(last);
      }
      if (lastTaxon != null) {
        List<String> taxa = Lists.newArrayList(lastTaxon.getId());
        boolean allSuccessful = true;
        // Then extract all the rest.  For each of these, we may or may not have to append words from
        // the last entry
        for (String fromSplit : Iterables.limit(split, split.size() - 1)) {          
          // Try to resolve the unabbreviated name
          Taxon additionalTaxon = findSpeciesForCommonNamePossiblyAppending(fromSplit, last);
          if (additionalTaxon != null) {
            taxa.add(additionalTaxon.getId());
          } else {
            allSuccessful = false;
            break;
          }
        }
 
        // Got it - create the sighting taxon
        if (allSuccessful) {
          SightingTaxon hybridOrSp;
          if (hybrid) {
            hybridOrSp = SightingTaxons.newHybridTaxon(taxa);
          } else {
            hybridOrSp = SightingTaxons.newSpTaxon(taxa);
          }
          // ... and now map if needed
          if (lastTaxon.getTaxonomy() instanceof MappedTaxonomy) {
            MappedTaxonomy taxonomy = (MappedTaxonomy) lastTaxon.getTaxonomy();
            hybridOrSp = taxonomy.getMapping(hybridOrSp);
          }
          possibilities = TaxonPossibilities.adding(possibilities, hybridOrSp);
        }
      }
    }
    
    if (possibilities == null && taxonomy.isBuiltIn()) {
      // Final desperation hack - look for some hybrid names.  Almost all eBird hybrids
      // are just named by the parent species, but these are given full names.
      // It would be better if the taxonomy directly supported these - it'd make data entry
      // easier for common hybrids, and it'd preserve the difference between Brewster's
      // and Lawrence's.  But this is quick.
      if (commonName.contains("brewster's warbler")
          || commonName.contains("lawrence's warbler")) {
        Taxon bwwa = findSpeciesForCommonName("Blue-winged Warbler");
        Taxon gwwa = findSpeciesForCommonName("Golden-winged Warbler");
        if (bwwa != null && gwwa != null) {
          possibilities = TaxonPossibilities.adding(possibilities,
              SightingTaxons.newHybridTaxon(ImmutableList.of(bwwa.getId(), gwwa.getId())));
        }
      } else if (commonName.contains("sutton's warbler")) {
        Taxon ytwa = findSpeciesForCommonName("Yellow-throated Warbler");
        Taxon nopa = findSpeciesForCommonName("Northern Parula");
        if (ytwa != null && nopa != null) {
          possibilities = TaxonPossibilities.adding(possibilities,
              SightingTaxons.newHybridTaxon(ImmutableList.of(ytwa.getId(), nopa.getId())));
        }
      }
    }
    return possibilities;
  }

  /**
   * Given two names, see if they can be combined to form a valid common name.
   */
  private Taxon findSpeciesForCommonNamePossiblyAppending(
      String commonName,
      String possiblyAppend) {
    Taxon taxon = findSpeciesForCommonName(commonName);
    if (taxon != null) {
      return taxon;
    }
    
    int lastSpace = possiblyAppend.lastIndexOf(' ');
    while (lastSpace > 0) {
      String commonNameWithAppendedSuffix = commonName + possiblyAppend.substring(lastSpace);
      taxon = findSpeciesForCommonName(commonNameWithAppendedSuffix);
      if (taxon != null) {
        return taxon;
      }
      
      lastSpace = possiblyAppend.lastIndexOf(possiblyAppend, lastSpace - 1);
    }
    
    return null;
  }

  private Taxon findSpeciesForCommonName(String commonName) {
    Collection<Taxon> collection = newCommonNames.get(commonName);
    if (collection.size() == 1) {
      return Iterables.getOnlyElement(collection);
    }

    collection = commonNameTree.search(commonName, maximumNameDistance);
    if (collection.size() == 1) {
      return Iterables.getOnlyElement(collection);
    }
    
    return null;
  }

  private Taxon findTaxonExactAndInGenus(String sciName) {
    Collection<Taxon> collection = newSpeciesNames.get(sciName);
    if (collection.size() == 1) {
      return Iterables.getOnlyElement(collection);
    }
    
    return findTaxonByScientificNameInGenus(sciName);
  }
  
  /**
   * Look for a taxon by searching for the genus, then something within the genus.
   */
  private Taxon findTaxonByScientificNameInGenus(String sciName) {
    Iterator<String> split = Splitter.on(' ').split(sciName).iterator();
    String genusName = split.next();
    // Don't blow up on malformed scientific names
    Taxon matchWithinGenus = null;
    if (split.hasNext()) {
      String species = split.next();
      // "sp." is explicitly not something to search for!
      if ("sp.".equals(species)) {
        return null;
      }

      if (!split.hasNext()) {
        Taxon genus = newGenusNames.get(genusName);
        if (genus != null) {
          // Try to find some option with the genus (perhaps as a group or subspecies)
          matchWithinGenus = findMatchWithin(genus, species, true /* prefer groups */);
        }
      }
    }
    // Fitting to the subspecies will overmatch, e.g. it'll pick the nominate subspecies
    // after an IOC lump.  Hop up a level.
    if (matchWithinGenus != null
        && matchWithinGenus.getType() == Taxon.Type.subspecies) {
      matchWithinGenus = matchWithinGenus.getParent();
      
    }
    return matchWithinGenus;
  }

  private SightingTaxon getTaxon(Taxon taxon, String subspeciesName) {
    SightingTaxon sightingTaxon = SightingTaxons.newSightingTaxon(taxon.getId());
    if (subspeciesName != null) {
      if (subspeciesName.contains("/")) {
        Set<String> taxonIds = Sets.newLinkedHashSet();
        for (String fromSpName : SP_SPLITTER.split(subspeciesName)) {
          Taxon subspecies = findMatchWithin(taxon, fromSpName, false /* don't prefer groups */ );
          if (subspecies != null) {
            taxonIds.add(subspecies.getId());
          }
        }
        
        if (!taxonIds.isEmpty()) {
          sightingTaxon = SightingTaxons.newPossiblySpTaxon(taxonIds);
        }
      } else {
        Taxon subspecies = findMatchWithin(taxon, subspeciesName, false /* don't prefer groups */ );
        if (subspecies != null) {
          sightingTaxon = SightingTaxons.newSightingTaxon(subspecies.getId());
        }
      }
    }
    
    // Map back to a SightingTaxon in the base taxonomy if needed.
    if (taxon.getTaxonomy() instanceof MappedTaxonomy) {
      MappedTaxonomy taxonomy = (MappedTaxonomy) taxon.getTaxonomy();
      
      // First, try mapping the SightingTaxon
      SightingTaxon mapping = taxonomy.getMapping(sightingTaxon);
      if (mapping == null) {
        // If that fails, try mapping the original taxon, ignoring any subspecies
        // (this is the same thing if there wasn't any subspecies provided). 
        mapping = taxonomy.getMapping(taxon);
      }
      
      return mapping;
    } else {
      return sightingTaxon;
    }
  }
  
  private SightingTaxon getTaxon(Collection<Taxon> collection, String subspeciesName) {
    return getTaxon(collection.iterator().next(), subspeciesName);
  }

  private void buildIndicesForTaxonomy(final BKTree.Builder<String, Taxon> commonNameTreeBuilder) {
    TaxonUtils.visitTaxa(taxonomy, new TaxonVisitor() {
      @Override
      public boolean visitTaxon(Taxon taxon) {
        switch (taxon.getType()) {
          case genus:
            newGenusNames.put(normalizeScientificName(taxon.getName()), taxon);
            break;
          case species:
            // Store all names, common or otherwise
            newSpeciesNames.put(normalizeScientificName(TaxonUtils.getFullName(taxon)), taxon);
            for (String alternateSciName : ((Species) taxon).getAlternateNames()) {
              alternateSciNames.put(normalizeScientificName(alternateSciName), taxon);
            }
            
            if (taxon.getCommonName() != null) {
              newCommonNames.put(normalizeCommonName(taxon.getCommonName()), taxon);
              
              // Add localized names too
              String localizedName = TaxonUtils.getCommonName(taxon);
              if (!localizedName.equals(taxon.getCommonName())) {
                newCommonNames.put(normalizeCommonName(localizedName), taxon);
              }

              commonNameTreeBuilder.add(normalizeCommonName(taxon.getCommonName()), taxon);
              for (String alternateCommonName : ((Species) taxon).getAlternateCommonNames()) {
                alternateCommonNames.put(normalizeCommonName(removeInPart(alternateCommonName)), taxon);
              }

              // Trim off " (undescribed... "
              int undescribedIndex = taxon.getCommonName().indexOf(" (undesc");
              if (undescribedIndex > 0) {
                newCommonNames.put(
                    normalizeCommonName(taxon.getCommonName().substring(0, undescribedIndex)), taxon);
                commonNameTreeBuilder.add(
                    normalizeCommonName(taxon.getCommonName().substring(0, undescribedIndex)), taxon);
              }
            }
            break;
          case group:
            newSpeciesNames.put(normalizeScientificName(TaxonUtils.getFullName(taxon)), taxon);
            for (String alternateSciName : ((Species) taxon).getAlternateNames()) {
              alternateSciNames.put(normalizeScientificName(alternateSciName), taxon);
            }
            
            String commonName = normalizeCommonName(taxon.getCommonName());
            newCommonNames.put(commonName, taxon);
            // And translated common names
            String localizedName = normalizeCommonName(TaxonUtils.getCommonName(taxon));
            if (!localizedName.equals(commonName)) {
              newCommonNames.put(localizedName, taxon);
            }

            for (String alternateCommonName : ((Species) taxon).getAlternateCommonNames()) {
              alternateCommonNames.put(removeInPart(normalizeCommonName(alternateCommonName)), taxon);
            }
            break;
          case subspecies:
            // Find explicitly named subspecies
            for (String alternateCommonName : ((Species) taxon).getAlternateCommonNames()) {
              alternateCommonNames.put(removeInPart(normalizeCommonName(alternateCommonName)), taxon);
            }
            break;
          default:
            break;
        }

        return true;
      }

      private final static String IN_PART = " - in part";
      
      /** Snip off the end of " - in part" for alternate name selection. */
      private String removeInPart(String name) {
        if (name.endsWith(IN_PART)) {
          name = name.substring(0, name.length() - IN_PART.length());
        }
        return name;
      }
    });
  }

  /** Look for a group/subspecies match within a genus (since the species couldn't be found). */
  Taxon findMatchWithin(final Taxon parent, final String name, boolean preferGroups) {
    final List<Taxon> matches = Lists.newArrayList();
    // Do exact match first
    TaxonUtils.visitTaxa(parent, new TaxonVisitor() {
      @Override
      public boolean visitTaxon(Taxon taxon) {
        if (taxon != parent && name.equals(taxon.getName())) {
          matches.add(taxon);
          return false;
        }

        return true;
      }
    });

    // Then do close-enough match. Note: do not do both exact and close-enough in a single pass
    // as that causes confusion when names are close, e.g. Cinclodes albiventris and albidiventris.
    // Also, don't do "close-enough" matches into subspecies, as this starts to get particularly 
    // risky.
    if (matches.isEmpty()) {
      TaxonUtils.visitTaxa(parent, new TaxonVisitor() {
        @Override
        public boolean visitTaxon(Taxon taxon) {
          if (taxon != parent && taxon.getType() == Taxon.Type.species && equalsOrClose(name, taxon.getName())) {
            matches.add(taxon);
            return false;
          }

          return true;
        }
      });
    }

    // Only proceed when there's exactly one match.
    if (matches.size() != 1) {
      return null;
    }

    Taxon match = matches.get(0);
    // Prefer groups over subspecies. This pure heuristic assumes that when
    // species get lumped, and a sighting was only at the species level, it's probable that
    // the sighting wasn't identified to species.
    if (preferGroups
        && (match.getType() == Type.subspecies)
        && (match.getParent().getType() == Type.group)) {
      match = match.getParent();
    }

    return match;
  }

  private static String normalizeCommonName(String name, String subspecies) {
    // If the common name ends with the subspecies name, then drop it
    if (subspecies != null && name.indexOf(subspecies) > 0) {
      String subspeciesEnding = " (" + subspecies + "(";
      if (name.endsWith(subspeciesEnding)) {
        name = name.substring(0, name.length() - subspeciesEnding.length());
      }

      subspeciesEnding = " [" + subspecies + "]";
      if (name.endsWith(subspeciesEnding)) {
        name = name.substring(0, name.length() - subspeciesEnding.length());
      }
    }
    
    return normalizeCommonName(name);
  }
  
  private static String normalizeCommonName(String name) {
    return name.toLowerCase()
        // Normalize a variety of strings to deal with alternate spellings.
        .replace("grey", "gray")
        .replace('ç', 'c')
        .replace('ñ', 'n')
        .replace('´', '\'')
        .replace('\u2010', '-')
        .replace("chequer", "checker")
        .replace("racquet", "racket")
        .replace("ö", "oe")
        .replace("ü", "ue")
        .replace("colour", "color");
  }
  
  private static String normalizeScientificName(String name) {
    // Consider normalizing differently, say keeping the initial caps?
    return name.toLowerCase();
  }
  
  private boolean equalsOrClose(String a, String b) {
    if (a.equals(b)) {
      return true;
    }

    if (maximumNameDistance == 0) {
      return false;
    }
    // "Close" is one-character off for short strings, two for longer strings.
    return LevenshteinDistance.getDefaultInstance().apply(a,
        b) <= (a.length() > 4 ? maximumNameDistance : 1);
  }
}