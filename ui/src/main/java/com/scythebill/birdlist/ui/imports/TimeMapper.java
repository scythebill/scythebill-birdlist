/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.joda.time.LocalTime;

import com.google.common.base.Strings;
import com.scythebill.birdlist.model.io.TimeIO;
import com.scythebill.birdlist.model.sighting.Sighting;

final class TimeMapper<T> implements FieldMapper<T> {
  private static final Logger logger = Logger.getLogger(TimeMapper.class.getCanonicalName());
  private final RowExtractor<T, String> timeExtractor;

  TimeMapper(RowExtractor<T, String> timeExtractor) {
    this.timeExtractor = timeExtractor;
  }

  @Override
  public void map(T line, Sighting.Builder sighting) {
    String string = timeExtractor.extract(line);
    if (!Strings.isNullOrEmpty(string)) {
      try {
        LocalTime partial = TimeIO.fromExternalString(string);
        sighting.setTime(partial);
      } catch (IllegalArgumentException e) {
        logger.log(Level.WARNING, "Failed to parse time " + string, e);
      }
    }
  }
}
