/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.user.User;
import com.scythebill.birdlist.model.user.UserSet;

/**
 * FieldMapper for importing users to a sighting;  can be any of just abbreviations,
 * just names, or both.
 */
class UserFieldMapper<T> implements FieldMapper<T> {

  private final static Splitter ABBREVIATION_SPLITTER = Splitter.on(UserSet.NOT_ELIGIBLE_ID_CHARS)
      .trimResults(CharMatcher.whitespace()).omitEmptyStrings();
  private final static Splitter NAME_SPLITTER = Splitter.on(CharMatcher.anyOf("\n\r"))
      .trimResults(CharMatcher.whitespace()).omitEmptyStrings();
  
  private final ReportSet reportSet; 
  private final RowExtractor<T, String> observersExtractor;
  private final RowExtractor<T, String> observerNamesExtractor;
  /** Cache, to avoid re-calculating sets of users (and to share those objects). */ 
  private final Map<String, ImmutableSet<User>> userCache = new HashMap<>();
  private Map<String, User> usersByName = null;

  UserFieldMapper(
      ReportSet reportSet, RowExtractor<T, String> observersExtractor,
      RowExtractor<T, String> observerNamesExtractor) {
    this.reportSet = reportSet;
    this.observersExtractor = observersExtractor;
    this.observerNamesExtractor = observerNamesExtractor;
  }

  @Override
  public void map(T line, Sighting.Builder sighting) {
    String observersStr = Strings.nullToEmpty(observersExtractor.extract(line));
    String observerNamesStr = Strings.nullToEmpty(observerNamesExtractor.extract(line));
    if (observersStr.isEmpty() && observerNamesStr.isEmpty()) {
      return;
    }
    
    String key = String.format("%s/%s", observersStr, observerNamesStr);
    // See if there's a set of Users already derived from the imported data
    ImmutableSet<User> users = userCache.get(key);
    if (users == null) {
      // Nope!  Forcibly create a user set, and parse the imported data
      UserSet userSet = getUserSet();
      List<String> abbreviationList = ABBREVIATION_SPLITTER.splitToList(observersStr);
      List<String> nameList = NAME_SPLITTER.splitToList(observerNamesStr);
      ImmutableSet.Builder<User> usersBuilder = ImmutableSet.builder();
      if (abbreviationList.isEmpty()) {
        // No abbreviations, only names.
        // Compute a by-name map;  note that this name to user is *not* a unique mapping!
        if (usersByName == null) {
          usersByName = new HashMap<>();
          for (User user : userSet.allUsers()) {
            if (user.name() != null) {
              usersByName.put(user.name(), user);
            }
          }
        }
        
        // If there's already a user with that name, use it;  otherwise add it.
        for (String name : nameList) {
          User user = usersByName.get(name);
          if (user == null) {
            User.Builder builder = userSet.newUserBuilder().setName(name);
            Optional<String> abbreviation = userSet.computeDefaultAbbreviation(name, Optional.empty());
            abbreviation.ifPresent(builder::setAbbreviation);
            user = userSet.addUser(builder);
            usersByName.put(name, user);
          }
          
          usersBuilder.add(user);
        }
      } else {
        for (int i = 0; i < abbreviationList.size(); i++) {
          String abbreviation = abbreviationList.get(i);
          if (userSet.hasUserWithAbbreviation(abbreviation)) {
            usersBuilder.add(userSet.userByAbbreviation(abbreviation));
          } else {
            User.Builder builder = userSet.newUserBuilder().setAbbreviation(abbreviation);
            if (nameList.size() > i) {
              builder.setName(nameList.get(i));
              // Clear the by-name cache
              usersByName = null;
            }
            usersBuilder.add(userSet.addUser(builder));
          }
        }
      }
      
      users = usersBuilder.build();      
      userCache.put(key, users);
    }
    
    if (users != null && !users.isEmpty()) {
      sighting.getSightingInfo().setUsers(users);
    }
  }

  private UserSet getUserSet() {
    UserSet userSet = reportSet.getUserSet();
    if (userSet == null) {
      userSet = new UserSet(); 
      reportSet.setUserSet(userSet);
    }
    return userSet;
  }
}
