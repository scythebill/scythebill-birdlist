/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.joda.time.Duration;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;
import org.joda.time.ReadablePartial;
import org.joda.time.chrono.GJChronology;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.primitives.Doubles;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.Sighting.Builder;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfo.ObservationType;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.imports.ParsedLocationIds.ToBeDecided;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Imports sightings from Wildlife Recorder files. 
 */
public class WildlifeRecorderImporter extends CsvSightingsImporter {
  private static final Logger logger = Logger.getLogger(WildlifeRecorderImporter.class.getName());
  private static final DateTimeFormatter TIME_FORMATTER = new DateTimeFormatterBuilder()
      .appendHourOfDay(1).appendLiteral(':').appendMinuteOfHour(2).toFormatter();
  private static final DateTimeFormatter DATE_FORMATTER_WITH_DOTS = DateTimeFormat.forPattern("dd.MM.yy")
      .withChronology(GJChronology.getInstance()).withPivotYear(2000);
  private static final DateTimeFormatter DATE_FORMATTER_WITH_SLASHES = DateTimeFormat.forPattern("dd/MM/yy")
      .withChronology(GJChronology.getInstance()).withPivotYear(2000);
  private LineExtractor<String> locationIdExtractor;
  private LineExtractor<String> taxonomyIdExtractor;
  private LineExtractor<String> commonNameExtractor;
  private LineExtractor<String> scientificExtractor;
  private LineExtractor<String> subspeciesExtractor;
  private LineExtractor<String> locationExtractor;
  private LineExtractor<String> longitudeExtractor;
  private LineExtractor<String> latitudeExtractor;
  private LinkedHashMap<Object, String> visitDescriptionMap = Maps.newLinkedHashMap();
  private LinkedHashMap<VisitInfoKey, VisitInfo> visitInfoMap = Maps.newLinkedHashMap();
  private LineExtractor<String> timeExtractor;
  private LineExtractor<String> descriptionExtractor;
  private LineExtractor<String> observersExtractor;
  private LineExtractor<String> dateExtractor;
  private LineExtractor<String> lastDateExtractor;
  private LineExtractor<String> lastTimeExtractor;
  private MultiFormatDateFromStringFieldMapper<String[]> dateMapper;

  public WildlifeRecorderImporter(ReportSet reportSet, Taxonomy taxonomy,
      Checklists checklists, PredefinedLocations predefinedLocations,
      File sightingsFile, File locationFile) {
    super(reportSet, taxonomy, checklists, predefinedLocations, sightingsFile, locationFile);
  }

  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return taxonomyIdExtractor;
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    return new FieldTaxonImporter<>(taxonomy, commonNameExtractor, scientificExtractor,
        subspeciesExtractor);
  }

  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    if (locationsFile != null) {
      ImportLines lines = CsvImportLines.fromFile(locationsFile, getCharset());
      try {
        String[] header = lines.nextLine();
        Map<String, Integer> headersByIndex = new LinkedHashMap<>();
        for (int i = 0; i < header.length; i++) {
          headersByIndex.put(CharMatcher.whitespace().removeFrom(header[i]).toLowerCase(), i);
        }
        
        int nameIndex = getRequiredHeader(headersByIndex, "name");
        Integer stateIndex = headersByIndex.get("state");
        Integer countryIndex = headersByIndex.get("country");
        Integer parentIndex = headersByIndex.get("parent");
        Integer countyIndex = headersByIndex.get("countyname");
        Integer nwLatitudeIndex = headersByIndex.get("nwlatitude");
        Integer seLatitudeIndex = headersByIndex.get("selatitude");
        Integer nwLongitudeIndex = headersByIndex.get("nwlongitude");
        Integer seLongitudeIndex = headersByIndex.get("selongitude");
        boolean hasLatLongIndices = nwLatitudeIndex != null
            && seLatitudeIndex != null
            && nwLongitudeIndex != null
            && seLongitudeIndex != null;
        
        while (true) {
          String[] line = lines.nextLine();
          if (line == null) {
            break;
          }
          
          ImportedLocation location = new ImportedLocation();
          String id = line[nameIndex];
          if (locationIds.hasBeenParsed(id)) {
            continue;
          }
          if (parentIndex != null && !Strings.isNullOrEmpty(line[parentIndex])) {
            location.locationNames.add(line[parentIndex]);
          }
          location.locationNames.add(line[nameIndex]);
          if (stateIndex != null) {
            location.state = line[stateIndex];
          }
          if (countryIndex != null) {
            location.country = line[countryIndex];
          }
          if (countyIndex != null) {
            location.county = line[countyIndex];
          }
          
          if (hasLatLongIndices) {
            Double nwLat = Doubles.tryParse(line[nwLatitudeIndex]);
            Double nwLong = Doubles.tryParse(line[nwLongitudeIndex]);
            Double seLat = Doubles.tryParse(line[seLatitudeIndex]);
            Double seLong = Doubles.tryParse(line[seLongitudeIndex]);
            if (nwLat != null && nwLong != null && seLat != null && seLong != null) {
              location.latitude = Double.toString((nwLat.doubleValue() + seLat.doubleValue()) / 2);
              location.longitude = Double.toString((nwLong.doubleValue() + seLong.doubleValue()) / 2);
            }
          }
             
          String locationId = location.addToLocationSet(reportSet, locations, locationShortcuts, predefinedLocations);
          if (locationId != null) {
            locationIds.put(id, locationId);
            
            if (!Strings.isNullOrEmpty(location.country)) {
              Location locationObj = locations.getLocation(locationId);
              Location country = Locations.getAncestorOfType(locationObj, Location.Type.country);
              if (country != null) {
                locationIds.put(location.country, country.getId());
              }
            }

            if (!Strings.isNullOrEmpty(location.state)) {
              Location locationObj = locations.getLocation(locationId);
              Location state = Locations.getAncestorOfType(locationObj, Location.Type.state);
              if (state != null) {
                locationIds.put(location.state, state.getId());
              }
            }

            if (!Strings.isNullOrEmpty(location.county)) {
              Location locationObj = locations.getLocation(locationId);
              Location county = Locations.getAncestorOfType(locationObj, Location.Type.county);
              if (county != null) {
                locationIds.put(location.county, county.getId());
              }
            }
          } else {
            locationIds.addToBeResolvedLocationName(id, new ToBeDecided(id, location.getLatLong()));
          }
        }
      } catch (IOException | RuntimeException e) {
        throw new ImportException(
            Messages.getFormattedMessage(Name.FAILED_WR_LOCATION_EXPORT_IMPORT_ON_LINE_FORMAT,
                lines.lineNumber(), e.getMessage()),
            e);
      } finally {
        lines.close();
      }
    }
    
    // Now do a second pass through the sightings file, looking for any locations missing from
    // the locations file.
    ImportLines lines = CsvImportLines.fromFile(sightingsFile, getCharset());
    try {
      computeMappings(lines);
      
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }
        
        String id = locationIdExtractor.extract(line);
        if (locationIds.hasBeenParsed(id)) {
          continue;
        }
        
        ImportedLocation location = new ImportedLocation();
        location.locationNames.add(id);
        location.latitude = latitudeExtractor.extract(line);
        location.longitude = longitudeExtractor.extract(line);
        
        String locationId = location.tryAddToLocationSetWithLatLong(reportSet, locations, locationShortcuts, predefinedLocations);
        if (locationId != null) {
          locationIds.put(id, locationId);
        } else {
          locationIds.addToBeResolvedLocationName(id, new ToBeDecided(id, location.getLatLong()));
        }
      }
    } catch (IOException | RuntimeException e) {
      throw new ImportException(
          Messages.getFormattedMessage(Name.FAILED_IMPORT_ON_LINE_FORMAT, lines.lineNumber()),
          e);
    } finally {
      lines.close();
    }
  }
    
  /** Take a trinomial and return the first two parts. */
  private static class DropSspFromSci implements LineExtractor<String> {
    private final LineExtractor<String> extractor;

    public DropSspFromSci(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public String extract(String[] line) {
      String name = extractor.extract(line);
      if (name == null) {
        return null;
      }
      // Don't try trimming "slash" names - they've got an extra space, but that's for the sp.
      if (name.indexOf('/') >= 0) {
        return name;
      }
      
      // And don't trim hybrid names either
      if (name.indexOf(" x ") >= 0) {
        return name;
      }
      
      int firstSpace = name.indexOf(' ');
      if (firstSpace >= 0) {
        int nextSpace = name.indexOf(' ', firstSpace + 1);
        if (nextSpace >= 0) {
          return name.substring(0, nextSpace);
        }
      }
      return name;
    }
  }

  /** Take a trinomial and return the first two parts. */
  static class ExtractSspFromSci implements LineExtractor<String> {
    private final LineExtractor<String> extractor;

    public ExtractSspFromSci(LineExtractor<String> extractor) {
      this.extractor = extractor;
    }

    @Override
    public String extract(String[] line) {
      String name = extractor.extract(line);
      if (name == null) {
        return null;
      }
      // Don't try trimming "slash" names - they've got an extra space, but that's for the sp.
      if (name.indexOf('/') >= 0) {
        return null;
      }
      
      // And don't trim hybrid names either
      if (name.indexOf(" x ") >= 0) {
        return null;
      }
      
      int firstSpace = name.indexOf(' ');
      if (firstSpace >= 0) {
        int nextSpace = name.indexOf(' ', firstSpace + 1);
        if (nextSpace >= 0) {
          return name.substring(nextSpace + 1);
        }
      }
      return null;
    }
  }

  @Override
  protected void beforeParseSightings() throws IOException {
    
    try (ImportLines lines = importLines(sightingsFile)) {
      String[] header = lines.nextLine();      
      Map<String, Integer> headersByIndex = new LinkedHashMap<>();
      for (int i = 0; i < header.length; i++) {
        headersByIndex.put(CharMatcher.whitespace().removeFrom(header[i]).toLowerCase(), i);
      }
      
      // Consider a few hypothetical date formats.  Try each one, and whichever one gets a larger number "wins". 
      LineExtractor<String> date = LineExtractors.stringFromIndex(getRequiredHeader(headersByIndex, "First Date", "firstdate", "date"));
      MultiFormatDateFromStringFieldMapper<String[]> mapper0 = new MultiFormatDateFromStringFieldMapper<>(
          date,
          "MM/dd/yy",
          "MM/yy");
      MultiFormatDateFromStringFieldMapper<String[]> mapper1 = new MultiFormatDateFromStringFieldMapper<>(
          date,
          "dd/MM/yy",
          "MM/yy");
      MultiFormatDateFromStringFieldMapper<String[]> mapper2 = new MultiFormatDateFromStringFieldMapper<>(
          date,
          "dd.MM.yy",
          "MM.yy");
      int successCount0 = 0;
      int successCount1 = 0;
      int successCount2 = 0;
      Sighting.Builder sighting = Sighting.newBuilder();
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) break;
        
        if (skipLine(line)) {
          continue;
        }
        
        try {
          mapper0.map(line, sighting);
          successCount0++;
          // If this option is way ahead, call it a victory and quit.
          if ((successCount0 > (successCount1 + 100)
              && (successCount0 > (successCount2 + 100)))) {
            break;
          }
        } catch (DateParseException e) {
          // ignore
        }
        
        try {
          mapper1.map(line, sighting);
          successCount1++;
          if ((successCount1 > (successCount0 + 100)
              && (successCount1 > (successCount2 + 100)))) {
            break;
          }
        } catch (DateParseException e) {
          // ignore
        }

        try {
          mapper2.map(line, sighting);
          successCount2++;
          if ((successCount2 > (successCount0 + 100)
              && (successCount2 > (successCount1 + 100)))) {
            break;
          }
        } catch (DateParseException e) {
          // ignore
        }
      }

      if (successCount2 >= successCount1 && successCount2 >= successCount0) {
        dateMapper = mapper2;
      } else if (successCount1 >= successCount0 && successCount1 >= successCount2) {
        dateMapper = mapper1;
      } else {
        dateMapper = mapper0;
      }
    }
  }
  
  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    String[] header = lines.nextLine();
    
    Map<String, Integer> headersByIndex = new LinkedHashMap<>();
    for (int i = 0; i < header.length; i++) {
      headersByIndex.put(CharMatcher.whitespace().removeFrom(header[i]).toLowerCase(), i);
    }
    
    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
    
    commonNameExtractor = LineExtractors.stringFromIndex(
        getRequiredHeader(headersByIndex, "Common Name", "commonname", "common"));
    int sciIndex = getRequiredHeader(headersByIndex, "Scientific Name", "scientificname", "scientific");
    scientificExtractor =
        new DropSspFromSci(LineExtractors.stringFromIndex(sciIndex));
    subspeciesExtractor =
        new ExtractSspFromSci(LineExtractors.stringFromIndex(sciIndex));
    
    taxonomyIdExtractor = LineExtractors.joined(Joiner.on('|').useForNull(""),
        ImmutableList.of(commonNameExtractor, scientificExtractor, subspeciesExtractor));

    // Extract the specifically named location columns
    locationExtractor = LineExtractors
        .stringFromIndex(getRequiredHeader(headersByIndex, "Site Name", "sitename", "site"));
    latitudeExtractor = LineExtractors.stringFromIndex(getRequiredHeader(headersByIndex, "Trip Latitude", "triplatitude", "latitude"));
    longitudeExtractor = LineExtractors.stringFromIndex(getRequiredHeader(headersByIndex, "Trip Longitude", "triplongitude", "longitude"));

    locationIdExtractor = locationExtractor;
    dateExtractor = LineExtractors.stringFromIndex(getRequiredHeader(headersByIndex, "First Date", "firstdate", "date"));

    if (dateMapper != null) {
      mappers.add(dateMapper);
    } else {
      mappers.add(new MultiFormatDateFromStringFieldMapper<>(
          dateExtractor,
          "dd.MM.yy",
          "dd/MM/yy"));
    }
    lastDateExtractor = LineExtractors.stringFromIndex(getRequiredHeader(headersByIndex, "Last Date", "lastdate"));
    timeExtractor = LineExtractors
        .stringFromIndex(getRequiredHeader(headersByIndex, "First Time", "firsttime", "time"));
    lastTimeExtractor = LineExtractors.stringFromIndex(getRequiredHeader(headersByIndex, "Last Time", "lasttime"));
    mappers.add((line, sighting) -> {
      String time = timeExtractor.extract(line);
      if (Strings.isNullOrEmpty(time)) {
        return;
      }
      
      sighting.setTime(TIME_FORMATTER.parseLocalTime(time));
    });
    
    
    LineExtractor<String> numberExtractor = LineExtractors
        .stringFromIndex(getRequiredHeader(headersByIndex, "Total Count", "totalcount", "count"));
    LineExtractor<String> estimationExtractor =
        LineExtractors.stringFromIndex(getRequiredHeader(headersByIndex, "Circa", "circa"));
    
    mappers
        .add(new CountFieldMapper<>(new IncorporateEstimation(estimationExtractor, numberExtractor)));
    
    descriptionExtractor = LineExtractors.stringFromIndex(getRequiredHeader(headersByIndex, "Notes", "notes"));
    mappers.add(new DescriptionFieldMapper<>(
          LineExtractors.joined(
              Joiner.on('\n').skipNulls(),
              new ApproximateCounts(numberExtractor),
              // TODO: merge in the rest of the extended ages info?
              descriptionExtractor)));

    mappers.add(new WildlifeRecorderStatusMapper(
        LineExtractors.stringFromIndex(getRequiredHeader(headersByIndex, "Status", "status"))));
    
    mappers.add(new WildlifeRecorderAgesMapper(
        LineExtractors.stringFromIndex(getRequiredHeader(headersByIndex, "Age", "age"))));
    
    Integer observersIndex = headersByIndex.get("observers");
    observersExtractor = observersIndex == null
        ? LineExtractors.constant("")
        : LineExtractors.stringFromIndex(observersIndex);
    mappers.add(new UserFieldMapper<>(reportSet, observersExtractor, LineExtractors.constant("")));

    return new ComputedMappings<>(
        new TaxonFieldMapper(taxonomyIdExtractor),
        new LocationMapper(locationIdExtractor),
        mappers);
  }

  @Override
  protected boolean skipLine(String[] line) {
    // Skip any line with no common or scientific name... but Wildlife Recorder
    // sticks visit descriptions there!  So stash those off for later.  
    if (Strings.isNullOrEmpty(commonNameExtractor.extract(line))
        && Strings.isNullOrEmpty(scientificExtractor.extract(line))) {
      // Found some visit-info!
      String description = descriptionExtractor.extract(line);
      if ("0".equals(line[0]) && !Strings.isNullOrEmpty(description)) {
        // Backslash seems to be the replacement for a newline?
        description = description.replace('\\', '\n');
        if (!CharMatcher.whitespace().matchesAllOf(description)) {
          String unresolvedLocationId = locationIdExtractor.extract(line);
          LocalDate date = null;
          String dateStr = dateExtractor.extract(line);
          try {
            date = DATE_FORMATTER_WITH_DOTS.parseLocalDate(dateStr);
          } catch (IllegalArgumentException e) {
            try {
              date = DATE_FORMATTER_WITH_SLASHES.parseLocalDate(dateStr);
            } catch (IllegalArgumentException e2) {
              logger.log(Level.WARNING, "Could not parse " + dateStr, e2);
            }
          }
          
          String startTimeStr = timeExtractor.extract(line);
          LocalTime startTime = null;
          if (!Strings.isNullOrEmpty(startTimeStr)) {
            try {
              startTime = TIME_FORMATTER.parseLocalTime(startTimeStr);
            } catch (IllegalArgumentException e) {
              logger.log(Level.WARNING, "Could not parse " + startTimeStr, e);
            }
          }
          if (date != null) {
            visitDescriptionMap.put(
                unparsedVisitInfoKey(unresolvedLocationId, date, startTime),
                description);
          }
        }
      }
      return true;
    }
    
    return false;
  }
  

  @Override
  protected void lookForVisitInfo(ReportSet reportSet, String[] line, Sighting newSighting,
      VisitInfoKey visitInfoKey) {
    VisitInfo.Builder builder = VisitInfo.builder()
        .withObservationType(ObservationType.HISTORICAL);

    // Fetch the description from the prior species "0" line.
    String unresolvedLocationId = locationIdExtractor.extract(line);
    ReadablePartial date = visitInfoKey.date();
    LocalTime startTime = visitInfoKey.startTime().orNull();
    String description = visitDescriptionMap.get(unparsedVisitInfoKey(unresolvedLocationId, date, startTime));
    if (!Strings.isNullOrEmpty(description)) {
      builder = builder.withComments(description);
    }
    
    // Count the number of observers (commas + 1)
    String observers = observersExtractor.extract(line);
    if (!Strings.isNullOrEmpty(observers)) {
      builder = builder.withPartySize(CharMatcher.is(',').countIn(observers) + 1);
    }
    
    // If start date == end date, then the time delta is the number of minutes
    if (Objects.equals(
        dateExtractor.extract(line), lastDateExtractor.extract(line))
        && !Strings.isNullOrEmpty(lastTimeExtractor.extract(line))) {
      LocalTime endTime = TIME_FORMATTER.parseLocalTime(lastTimeExtractor.extract(line));
      if (startTime != null && endTime != null) {
        Duration duration = Minutes.minutesBetween(startTime, endTime).toStandardDuration();
        if (duration.isLongerThan(Duration.standardMinutes(5))) {
          builder = builder.withDuration(duration);
        }
      }
    }
    
    visitInfoMap.put(visitInfoKey, builder.build());
  }


  @Override
  public Map<VisitInfoKey, VisitInfo> getAccumulatedVisitInfoMap() {
    return visitInfoMap;
  }

  private static Object unparsedVisitInfoKey(
      String unresolvedLocationId, ReadablePartial date, LocalTime startTime) {
    ArrayList<Object> list = new ArrayList<Object>(3);
    list.add(unresolvedLocationId);
    list.add(date);
    list.add(startTime);
    return list;
  }

  /** Support negative numbers, which mean Common, etc. */
  static class ApproximateCounts implements LineExtractor<String> {
    private LineExtractor<String> numberExtractor;

    public ApproximateCounts(
        LineExtractor<String> numberExtractor) {
      this.numberExtractor = numberExtractor;
    }
    
    @Override
    public String extract(String[] line) {
      String number = numberExtractor.extract(line);
      if (number == null) {
        return null;
      }
      switch (number) {
        case "-3":
          return "Very common";
        case "-2":
          return "Common";
        case "-1":
          return "Scarce";
        default:
          return null;
      }
    }
  }

  static class IncorporateEstimation implements LineExtractor<String> {
    private LineExtractor<String> estimationExtractor;
    private LineExtractor<String> numberExtractor;

    public IncorporateEstimation(
        LineExtractor<String> estimationExtractor,
        LineExtractor<String> numberExtractor) {
      this.estimationExtractor = estimationExtractor;
      this.numberExtractor = numberExtractor;
    }
    
    @Override
    public String extract(String[] line) {
      String estimate = estimationExtractor.extract(line);
      String number = numberExtractor.extract(line);
      // No estimate, just return the number
      if (Strings.isNullOrEmpty(estimate)
          || Strings.isNullOrEmpty(number)) {
        return number;
      }
      
      if ("C".equals(estimate)) {
        estimate = "~";
      }
      // Prefix the estimate character
      return estimate + number;
    }

  }

  static class WildlifeRecorderStatusMapper implements FieldMapper<String[]> {

    private final LineExtractor<String> statusExtractor;

    public WildlifeRecorderStatusMapper(
        LineExtractor<String> statusExtractor) {
      this.statusExtractor = statusExtractor;
    }

    @Override
    public void map(String[] line, Builder sighting) {
      String extracted = Strings.nullToEmpty(statusExtractor.extract(line));
      // Wish I could support "B" (breeding) and "C" (possible breeding), but
      // the eBird codes are a lot finer-grained?  Could introduce top-level breeding codes
      // for those, I suppose.
      if (extracted.contains("P")) {
        sighting.getSightingInfo().setSightingStatus(SightingStatus.RECORD_NOT_ACCEPTED);
      }
      if (extracted.contains("E")) {
        sighting.getSightingInfo().setSightingStatus(SightingStatus.INTRODUCED_NOT_ESTABLISHED);
      }
      if (extracted.contains("X") || extracted.contains("V")) {
        sighting.getSightingInfo().setPhotographed(true);
      }
      if (extracted.contains("H")) {
        sighting.getSightingInfo().setHeardOnly(true);
      }
    }
  }

  static class WildlifeRecorderAgesMapper implements FieldMapper<String[]> {

    private final LineExtractor<String> agesExtractor;

    public WildlifeRecorderAgesMapper(
        LineExtractor<String> agesExtractor) {
      this.agesExtractor = agesExtractor;
    }

    @Override
    public void map(String[] line, Builder sighting) {
      String extracted = Strings.nullToEmpty(agesExtractor.extract(line)).toLowerCase();
      // No support for sub-counts of various ages/sexes, but can at least grab some info
      if (extracted.contains("ad")) {
        sighting.getSightingInfo().setAdult(true);
      }
      if (extracted.contains("imm")) {
        sighting.getSightingInfo().setImmature(true);
      }
      if (extracted.contains("m,")) {
        sighting.getSightingInfo().setMale(true);
      }
      if (extracted.contains("f,")) {
        sighting.getSightingInfo().setFemale(true);
      }
    }
  }


  private int getRequiredHeader(Map<String, Integer> indexMap, String originalCase, String... possibilities) throws IOException {
    Preconditions.checkArgument(possibilities.length > 0);
    for (String possibility : possibilities) {
      Integer index = indexMap.get(possibility);
      if (index != null) {
        return index;
      }
    }
    throw new ImportException(
        Messages.getFormattedMessage(Name.WILDLIFE_RECORDER_NO_HEADER_ROW_FORMAT, originalCase));
  }
  
  @Override
  public boolean importContainedUserInformation() {
    return true;
  }

  @Override
  protected Charset getCharset() {  
    return Charset.availableCharsets().getOrDefault("windows-1252", StandardCharsets.ISO_8859_1);
  }
}
