/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.google.common.base.CharMatcher;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.UnmodifiableIterator;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.xml.BaseNodeParser;
import com.scythebill.xml.NodeParser;
import com.scythebill.xml.ParseContext;
import com.scythebill.xml.StringParser;
import com.scythebill.xml.TreeBuilder;

/**
 * Transforms the Wings XML sightings into an "ImportLines" columnar format.
 */
class WingsImportLines implements ImportLines {
  static final int SPECIES_COLUMN = 0;
  static final int LOCATION_COLUMN = 1;
  static final int YEAR_COLUMN = 2;
  static final int MONTH_COLUMN = 3;
  static final int DAY_COLUMN = 4;
  static final int EXACT_NUMBER_COLUMN = 5;
  static final int NUMBER_COLUMN = 6;
  static final int COMMENT_COLUMN = 7;
  static final int INDICATORS_COLUMN = 8;
  static final int SEX_COLUMN = 9;
  static final int PLUMAGE_COLUMN = 10;
  static final int S_OR_H_COLUMN = 11;
  private static final int COLUMN_COUNT = 12;
  
  private final UnmodifiableIterator<File> files;
  private int lineNumber = 0;
  private Iterator<String[]> currentLines;
  

  WingsImportLines(List<File> files) {
   this.files = ImmutableList.copyOf(files).iterator(); 
  }
  
  @Override
  public void close() throws IOException {
  }

  @Override
  public String[] nextLine() throws IOException {
    while (currentLines == null || !currentLines.hasNext()) {
      if (!files.hasNext()) {
        return null;
      }
      
      currentLines = parseCurrentFile().iterator();
    }

    if (currentLines == null && !currentLines.hasNext()) {
      return null;
    }

    lineNumber++;
    return currentLines.next();
  }

  @Override
  public int lineNumber() {
    return lineNumber;
  }

  @Override
  public ImportLines withoutTrimming() {
    throw new UnsupportedOperationException();
  }

  private List<String[]> parseCurrentFile() throws IOException {
    List<String[]> list = new ArrayList<>();
    class RootParser extends BaseNodeParser {

      @Override
      public NodeParser startChildElement(ParseContext context, String namespaceURI, String localName,
          Attributes attrs) throws SAXParseException {
        if (localName.equals("SGHT_Sightings")) {
          return new SightingsParser();
        }
        
        return this;
      }
      
      @Override
      public void addCompletedChild(ParseContext context, String namespaceURI, String localName,
          Object child) throws SAXParseException {
        list.add((String[]) child);
      }
    }
    
    File file = files.next();
    try {
      new TreeBuilder<>().parse(
          new InputSource(
              new BufferedInputStream(new FileInputStream(file))),
          new RootParser());
    } catch (SAXException e) {
      throw new IOException("Could not parse file " + file.getName(),
          e);
    }
            
    return list;
  }

  class SightingsParser extends BaseNodeParser {
    private String[] line = new String[COLUMN_COUNT];
    
    @Override
    public Object endElement(ParseContext context, String namespaceURI, String localName)
        throws SAXParseException {
      return line;
    }
    
    @Override
    public NodeParser startChildElement(ParseContext context, String namespaceURI, String localName,
        Attributes attrs) throws SAXParseException {
      return new StringParser();
    }
    
    @Override
    public void addCompletedChild(ParseContext context, String namespaceURI, String localName,
        Object child) throws SAXParseException {
      String s = CharMatcher.whitespace().trimFrom((String) child);
      switch (localName) {
        case "EXT_KEY_Species":
          line[SPECIES_COLUMN] = s;
          break;
        case "EXT_KEY_Continent":
        case "EXT_KEY_Country":
        case "EXT_KEY_Province":
        case "EXT_KEY_County":
        case "EXT_KEY_Place":
        case "EXT_KEY_Site":
        case "EXT_KEY_Station":
          line[LOCATION_COLUMN] = (localName.substring("EXT_KEY_".length()) + "_" + s).toUpperCase();
          break;
        case "DT_Year":
          line[YEAR_COLUMN] = s;
          break;
        case "DT_Month":
          line[MONTH_COLUMN] = s;
          break;
        case "DT_Day":
          line[DAY_COLUMN] = s;
          break;
        case "NBR_Is_Number_Seen_Exact":
          line[EXACT_NUMBER_COLUMN] = s;
          break;
        case "NBR_Number_Seen":
          line[NUMBER_COLUMN] = s;
          break;
        case "INFO_Comment":
          line[COMMENT_COLUMN] = s;
          break;
        case "INFO_Indicators":
          line[INDICATORS_COLUMN] = s;
          break;
        case "INFO_Sex_Code":
          line[SEX_COLUMN] = s;
          break;
        case "INFO_Plumage_Code":
          line[PLUMAGE_COLUMN] = s;
          break;
        case "INFO_S_or_H":
          line[S_OR_H_COLUMN] = s;
          break;
      }
    }
  }
}
