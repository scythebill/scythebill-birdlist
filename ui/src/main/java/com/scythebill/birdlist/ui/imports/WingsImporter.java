/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.google.common.base.CharMatcher;
import com.google.common.base.MoreObjects;
import com.google.common.base.MoreObjects.ToStringHelper;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.io.Resources;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.xml.BaseNodeParser;
import com.scythebill.xml.NodeParser;
import com.scythebill.xml.ParseContext;
import com.scythebill.xml.StringParser;
import com.scythebill.xml.TreeBuilder;

/**
 * An importer for Wings exports.  Annoyingly, requires performing
 * multiple exports - for sightings, as well as for all types of locations.
 */
class WingsImporter extends CsvSightingsImporter {
  private final ImmutableList<File> files;

  WingsImporter(ReportSet reportSet, Taxonomy taxonomy, Checklists checklists,
      PredefinedLocations predefinedLocations, List<File> files) {
    // TODO: maybe use the directory instead of the first file?  Or a dummy, non-existent file with a common prefix in
    // that directory?
    super(reportSet, taxonomy, checklists, predefinedLocations, files.get(0), files.get(0));
    this.files = ImmutableList.copyOf(files);
  }
  
  @Override
  protected LineExtractor<? extends Object> taxonomyIdExtractor() {
    return LineExtractors.stringFromIndex(WingsImportLines.SPECIES_COLUMN);
  }

  @Override
  protected TaxonImporter<String[]> newTaxonImporter(Taxonomy taxonomy) {
    Map<String, String> idToCommonName = new LinkedHashMap<>(11000);
    Map<String, String> idToSciName = new LinkedHashMap<>(11000);
    try {
      try (ImportLines importLines =
          CsvImportLines.fromUrl(
              Resources.getResource(getClass(), "wings-taxa.csv"), StandardCharsets.UTF_8)) {
        while (true) {
          String[] line = importLines.nextLine();
          if (line == null) {
            break;
          }
          
          if (line.length != 3) {
            throw new RuntimeException("Unexpected line " + Arrays.asList(line));
          }
          idToCommonName.put(line[0], line[1]);
          idToSciName.put(line[0], line[2]);
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    
    LineExtractor<String> keyExtractor = LineExtractors.stringFromIndex(WingsImportLines.SPECIES_COLUMN);
    TaxonResolver taxonResolver = new TaxonResolver(taxonomy);
    taxonResolver.setMaximumNameDistance(0);
    return new FieldTaxonImporter<>(
        taxonResolver,
        line -> idToCommonName.getOrDefault(keyExtractor.extract(line), keyExtractor.extract(line)),
        line -> idToSciName.get(keyExtractor.extract(line)));
  }

  @Override
  protected ComputedMappings<String[]> computeMappings(ImportLines lines) throws IOException {
    List<FieldMapper<String[]>> mappers = Lists.newArrayList();
    mappers.add(new DateFromYearMonthDayFieldMapper<>(
        LineExtractors.intFromIndex(WingsImportLines.YEAR_COLUMN),
        LineExtractors.intFromIndex(WingsImportLines.MONTH_COLUMN),
        LineExtractors.intFromIndex(WingsImportLines.DAY_COLUMN)));
    mappers.add(new DescriptionFieldMapper<>(
        LineExtractors.stringFromIndex(WingsImportLines.COMMENT_COLUMN)));
    mappers.add(new CountFieldMapper<>(line -> {
      String count = line[WingsImportLines.NUMBER_COLUMN];
      if (Strings.isNullOrEmpty(count)) {
        return count;
      }
      
      if ("false".equalsIgnoreCase(line[WingsImportLines.EXACT_NUMBER_COLUMN])) {
        count = "~" + count;
      }
      
      return count;
    }));
    mappers.add(new MaleFieldMapper<>(line -> {
      return "M".equals(line[WingsImportLines.SEX_COLUMN]);
    }));
    mappers.add(new FemaleFieldMapper<>(line -> {
      return "F".equals(line[WingsImportLines.SEX_COLUMN]);
    }));
    mappers.add(new ImmatureFieldMapper<>(line -> {
      return "IMM".equals(line[WingsImportLines.PLUMAGE_COLUMN])
          || "JUV".equals(line[WingsImportLines.PLUMAGE_COLUMN]);
    }));
    
    return new ComputedMappings<>(
        new TaxonFieldMapper(
            LineExtractors.stringFromIndex(WingsImportLines.SPECIES_COLUMN)),
        new LocationMapper(
            LineExtractors.stringFromIndex(WingsImportLines.LOCATION_COLUMN)),
        mappers);
  }

  static class WingsLocation {
    Location.Type type;
    String parent;
    String key;
    String name;
    String latitude;
    String longitude;
    String comment;
    public String code;
    
    @Override
    public String toString() {
      ToStringHelper stringHelper = MoreObjects.toStringHelper(this).add("code", code);
      if (parent != null) {
        stringHelper.add("parent", parent);
      }
      if (name!= null) {
        stringHelper.add("name", name);
      }
      if (key != null) {
        stringHelper.add("key", key);
      }
      return stringHelper.toString();
    }
  }
  
  static class RootLocationParser extends BaseNodeParser {
    private final List<WingsLocation> locations = new ArrayList<>();
    
    @Override
    public Object endElement(ParseContext context, String namespaceURI, String localName)
        throws SAXParseException {
      return locations;
    }

    @Override
    public NodeParser startChildElement(ParseContext context, String namespaceURI, String localName,
        Attributes attrs) throws SAXParseException {
      switch (localName) {
        case "LOCN_Continent":
          return new LocationParser(Location.Type.region);
        case "LOCN_Province":
          return new LocationParser(Location.Type.state);
        case "LOCN_Country":
          return new LocationParser(Location.Type.country);
        case "LOCN_County":
          return new LocationParser(Location.Type.county);
        case "LOCN_Place":
        case "LOCN_Site":
        case "LOCN_Station":
          return new LocationParser(null);
      }
      
      return this;
    }

    @Override
    public void addCompletedChild(ParseContext context, String namespaceURI, String localName,
        Object child) throws SAXParseException {
      locations.add((WingsLocation) child);
    }
  }
  
  static class LocationParser extends BaseNodeParser {
    private WingsLocation location;
    
    LocationParser(Location.Type type) {
      location = new WingsLocation();
      location.type = type;
    }
    
    @Override
    public Object endElement(ParseContext context, String namespaceURI, String localName)
        throws SAXParseException {
      if (location.name == null) {
        location.name = location.key;
      }
      return location;
    }

    @Override
    public NodeParser startChildElement(ParseContext context, String namespaceURI, String localName,
        Attributes attrs) throws SAXParseException {
      return new StringParser();
    }
    
    @Override
    public void addCompletedChild(ParseContext context, String namespaceURI, String localName,
        Object child) throws SAXParseException {
      String s = CharMatcher.whitespace().trimFrom((String) child);
      if (localName.equals("KEY_InternetCode")) {
        location.code = s;
      } else if (localName.startsWith("KEY_")) {
        location.key = (localName.substring("KEY_".length())+ "_" + s).toUpperCase();
      } else if (localName.startsWith("EXT_KEY_")) {
        location.parent = (localName.substring("EXT_KEY_".length()) + "_" + s).toUpperCase();
      } else {
        switch (localName) {
          case "Name":
            location.name = s;
            break;
          case "Latitude_Decimal":
            if (!"0".equals(s)) {
              location.latitude = s;
            }
            break;
          case "Longitude_Decimal":
            if (!"0".equals(s)) {
              location.longitude = s;
            }
            break;
          case "Comment":
            if (!"".equals(s)) {
              location.comment = s;
            }
            break;
        }
      }
    }
  }
  
  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    Map<String, WingsLocation> locationByKey = new LinkedHashMap<>();
    for (File file : files) {
      try {
        @SuppressWarnings("unchecked")
        List<WingsLocation> wingsLocations = (List<WingsLocation>) new TreeBuilder<>().parse(
            new InputSource(
                new BufferedInputStream(new FileInputStream(file))),
            new RootLocationParser());
        for (WingsLocation location : wingsLocations) {
          locationByKey.put(location.key, location);
        }
      } catch (SAXException e) {
        throw new IOException("Could not parse locations from " + file.getName(), e);
      }
    }
    
    ImportLines lines = importLines(locationsFile);
    try {
      computeMappings(lines);
      while (true) {
        String[] line = lines.nextLine();
        if (line == null) {
          break;
        }

        if (skipLine(line)) {
          continue;
        }
        
        if (line.length < WingsImportLines.LOCATION_COLUMN) {
          continue;
        }
        
        String locationKey = CharMatcher.whitespace().trimFrom(line[WingsImportLines.LOCATION_COLUMN]);
        WingsLocation wingsLocation = locationByKey.get(locationKey);
        if (wingsLocation == null) {
          throw new ImportException("Couldn't find the location \"" + locationKey + "\" in the Wings exports.<br>"
              + "Check your location exports to make sure everything is present.");
        }
        ImportedLocation imported = new ImportedLocation();
        populateImportedLocation(imported, wingsLocation, locationByKey);
        
        // Add some more fields that can't be added recursively
        if (wingsLocation.latitude != null && wingsLocation.longitude != null) {
          imported.latitude = wingsLocation.latitude;
          imported.longitude = wingsLocation.longitude;
        }
        if (wingsLocation.comment != null) {
          imported.description = wingsLocation.comment;
        }
        String locationId = imported.addToLocationSet(
            reportSet, locations, locationShortcuts, predefinedLocations);
        locationIds.put(wingsLocation.key, locationId);
      }
    } finally {
      lines.close();
    }
  }
  
  private static final ImmutableMap<String, String> REGION_NAMES =
      ImmutableMap.<String, String>builder()
      .put("Antarctica", "South Polar Region")
      .put("Australia", "Australasia")
      .put("Pacific", "Pacific Ocean")
      // Don't know if these actually exist
      .put("Atlantic", "Atlantic Ocean")
      .put("Indian", "Indian Ocean")
      .build();
  
  private void populateImportedLocation(
      ImportedLocation imported,
      WingsLocation location,
      Map<String, WingsLocation> locationByKey) {
    if (location.parent != null) {
      // Populate any additional data that can only be retrieved by the parent
      WingsLocation parent = locationByKey.get(location.parent);
      if (parent != null) {
        populateImportedLocation(imported, parent, locationByKey);
      }
    }
    
    if (location.type == Location.Type.region) {
      imported.region = REGION_NAMES.getOrDefault(location.name, location.name);
    } else if (location.type == Location.Type.country) {
      imported.country = location.name;
      if (location.code != null) {
        imported.countryCode = location.code.toUpperCase();
      }
    } else if (location.type == Location.Type.state) {
      imported.state = location.name;
    } else if (location.type == Location.Type.county) {
      imported.county = location.name;
    } else {
      imported.locationNames.add(location.name);
    }
  }
  
  @Override
  protected ImportLines importLines(File file) throws IOException {
    return new WingsImportLines(files);
  }

}
