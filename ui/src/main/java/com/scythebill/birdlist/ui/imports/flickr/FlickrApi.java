/*   
 * Copyright 2020 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports.flickr;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.joda.time.ReadablePartial;

import com.google.common.base.Charsets;
import com.google.common.base.MoreObjects;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.ui.guice.FlickrApiKey;

/**
 * A highly minimal subset of the Flickr API;  just enough to run an import.
 */
class FlickrApi {
  private static final Logger logger = Logger.getLogger(FlickrApi.class.getName());
  private static final RequestConfig REQUEST_CONFIG = RequestConfig.custom()
      .setConnectionRequestTimeout(5000)
      .setConnectTimeout(10000)
      .setSocketTimeout(10000)
      .build();
  private static final String FLICKR_API_URI = "https://www.flickr.com/services/rest/";
  
  private final CloseableHttpClient httpClient;
  private final Gson gson;
  private final String key;

  static class PhotosetReply  {
    PhotoSet photoset; 
    String stat;
    String message;
    int code;
  }

  static class PhotoSet {
    /** Owner, as returned by the API. */
    String owner;
    /** Original owner string from the album URL; not part of the API */
    String originalOwnerString;
    List<FlickrPhoto> photo = new ArrayList<>();
    
    int page;
    int pages;
    String title;
    
    public void populateUrls() {
      photo.forEach(p -> p.page_url = p.photoPage(originalOwnerString));
    }
  }
  
  static class FlickrPhoto {
    String id;
    String farm;
    String secret;
    String server;
    String title;
    String datetaken;
    String url_k;
    String tags;
    String latitude;
    String longitude;
    FlickrContent description;
    String page_url;
    
    @Override
    public String toString() {
      return MoreObjects.toStringHelper(this)
          .add("datetaken", datetaken)
          .add("title", title)
          .toString();
    }
    
    public ReadablePartial date() {
      if (datetaken == null) {
        return null;
      }
      
      List<String> dateComponents = Splitter.on(' ').splitToList(datetaken);
      if (dateComponents.isEmpty()) {
        return null;
      }
      
      try {
        return PartialIO.fromString(dateComponents.get(0));
      } catch (IllegalArgumentException e) {
        logger.log(Level.WARNING, "Couldn't parse date " + datetaken, e);
        return null;
      }
    }
    
    public String description() {
      if (description == null) {
        return null;
      }
      
      return Strings.emptyToNull(description._content);
    }
    
    public String photoUrl() {
      return String.format(
          "https://farm%s.staticflickr.com/%s/%s_%s.jpg",
          farm, server, id, secret);
    }
    
    public String photoPage(String owner) {
      return String.format("https://www.flickr.com/photos/%s/%s", 
          owner, id);
    }
  }
  
  static class FlickrContent {
    String _content;
  }
  
  static class PhotoReply {
    SingleFlickrPhoto photo;

    String stat;
    String message;
    int code;
  }

  static class SingleFlickrPhoto {
    static class Dates {
      String taken;      
    };
    static class Owner {
      String nsid;
      String path_alias;
    }
    static class Tags {
      static class Tag {
        String _content;
      }
      List<Tag> tag = new ArrayList<>();
    }
    
    String id;
    String farm;
    String secret;
    String server;
    FlickrContent description;
    FlickrContent title;
    Dates dates;
    Tags tags;
    
    FlickrPhoto toFlickrPhoto(String owner) {
      FlickrPhoto photo = new FlickrPhoto();
      photo.id = id;
      photo.farm = farm;
      photo.secret = secret;
      photo.server = server;
      photo.title = title._content;
      photo.description = description;
      photo.datetaken = dates.taken;
      photo.page_url = photo.photoPage(owner);
      photo.tags = tags.tag.stream().map(t->t._content).collect(Collectors.joining(" "));
      
      return photo;
    }
  }


  @Inject
  FlickrApi(CloseableHttpClient httpClient, Gson gson, @FlickrApiKey String key) {
    this.httpClient = httpClient;
    this.gson = gson;
    this.key = key;
  }
  
  public String lookupUser(String url) throws IOException, FlickrApiException {
    JsonObject userLookup;
    try {
      URIBuilder apiUri = apiUri("flickr.urls.lookupUser");
      apiUri.addParameter("url", url);        
      userLookup = sendApi(apiUri.build());
    } catch (URISyntaxException e) {
      throw new AssertionError("Unexpected Invalid URI", e);
    }

    if (!"ok".equals(userLookup.get("stat").getAsString())) {
      JsonElement codeElement = userLookup.get("code");
      JsonElement messageElement = userLookup.get("message");
      
      throw new FlickrApiException(
          codeElement == null ? -1 : codeElement.getAsInt(),
          messageElement == null ? "" : messageElement.getAsString());
    }

    // TODO: error handling for missing fields, error code in stat
    JsonObject userElement = userLookup.get("user").getAsJsonObject();
    return userElement.get("id").getAsString();
  }
  
  private static final Pattern ALBUM_URL_PATTERN =
      Pattern.compile("https://www.flickr.com/photos/(.*)/albums/(.*)");

  public PhotoSet lookupAlbum(String albumUrl) throws IOException, FlickrApiException {
    Matcher matcher = ALBUM_URL_PATTERN.matcher(albumUrl);
    if (!matcher.matches()) {
      throw new IllegalArgumentException("Not an album: " + albumUrl);
    }
    
    String user = lookupUser(albumUrl);
    PhotoSet firstPage = lookupAlbum(albumUrl, user, 1);
    for (int page = 2; page <= firstPage.pages; page++) {
      PhotoSet extraPages = lookupAlbum(albumUrl, user, page);
      firstPage.photo.addAll(extraPages.photo);
    }

    // Populate URLs so failed JSON output produces more useful results
    firstPage.populateUrls();
    return firstPage;
  }

  private static final Pattern PHOTO_URL_PATTERN =
      Pattern.compile("https://www.flickr.com/photos/(.*)/([0-9]+)");
  
  public PhotoSet lookupPhoto(String photoUrl) throws FlickrApiException, IOException {
    Matcher matcher = PHOTO_URL_PATTERN.matcher(photoUrl);
    if (!matcher.matches()) {
      throw new IllegalArgumentException("Not a photo: " + photoUrl);
    }
    
    String photoId = matcher.group(2);
    PhotoReply reply;
    try {
      URIBuilder apiUri = apiUri("flickr.photos.getInfo");
      apiUri.addParameter("photo_id", photoId);
      apiUri.addParameter("extras", "date_taken,geo,tags,description");
      reply = sendApi(apiUri.build(), PhotoReply.class);
    } catch (URISyntaxException e) {
      throw new AssertionError("Unexpected Invalid URI", e);
    }
    
    if (!"ok".equals(reply.stat)) {
      throw new FlickrApiException(reply.code, reply.message);
    }
    
    PhotoSet photoSet = new PhotoSet();
    photoSet.originalOwnerString = matcher.group(1);
    photoSet.photo.add(reply.photo.toFlickrPhoto(photoSet.originalOwnerString));
    return photoSet;
  }

  public boolean isAlbumUrl(String albumUrl) {
    return ALBUM_URL_PATTERN.matcher(albumUrl).matches();
  }  

  private PhotoSet lookupAlbum(String albumUrl, String user, int page) throws IOException, FlickrApiException {
    Matcher matcher = ALBUM_URL_PATTERN.matcher(albumUrl);
    if (!matcher.matches()) {
      throw new IllegalArgumentException("Not an album: " + albumUrl);
    }
    String photoSetId = matcher.group(2);
    PhotosetReply reply;
    try {
      URIBuilder apiUri = apiUri("flickr.photosets.getPhotos");
      apiUri.addParameter("user_id", user);
      apiUri.addParameter("photoset_id", photoSetId);
      apiUri.addParameter("extras", "date_taken,geo,tags,description");
      apiUri.addParameter("per_page", "500");
      apiUri.addParameter("page", Integer.toString(page));
      reply = sendApi(apiUri.build(), PhotosetReply.class);
    } catch (URISyntaxException e) {
      throw new AssertionError("Unexpected Invalid URI", e);
    }
    
    if (!"ok".equals(reply.stat)) {
      throw new FlickrApiException(reply.code, reply.message);
    }
    reply.photoset.originalOwnerString = matcher.group(1);
    
    return reply.photoset;
  }
  
  private URIBuilder apiUri(String method) throws URISyntaxException {
    URIBuilder uriBuilder = new URIBuilder(FLICKR_API_URI);
    uriBuilder.addParameter("method", method);
    uriBuilder.addParameter("api_key", key);
    uriBuilder.addParameter("format", "json");
    uriBuilder.addParameter("nojsoncallback", "1");
    return uriBuilder;
  }
  
  private JsonObject sendApi(URI uri) throws IOException {
    HttpGet httpGet = new HttpGet(uri);
    httpGet.setConfig(REQUEST_CONFIG);
    CloseableHttpResponse get = httpClient.execute(httpGet);
    HttpEntity entity = get.getEntity();
    Reader reader = new InputStreamReader(
        new BufferedInputStream(entity.getContent()),
        Charsets.UTF_8);
    try {
      return new JsonParser().parse(reader).getAsJsonObject();
    } finally {
      reader.close();
    }
  }

  private <T> T sendApi(URI uri, Class<T> clazz) throws JsonSyntaxException, IOException {
    return gson.fromJson(sendApi(uri), clazz);
  }

}
