/*   
 * Copyright 2020 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports.flickr;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import org.joda.time.ReadablePartial;

import com.google.common.base.CharMatcher;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.gson.Gson;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.checklist.TransposedChecklist;
import com.scythebill.birdlist.model.checklist.TransposedChecklists;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Photo;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.imports.ComputedMappings;
import com.scythebill.birdlist.ui.imports.FieldMapper;
import com.scythebill.birdlist.ui.imports.LocationShortcuts;
import com.scythebill.birdlist.ui.imports.RowExtractor;
import com.scythebill.birdlist.ui.imports.SightingsImporter;
import com.scythebill.birdlist.ui.imports.TaxonImporter;
import com.scythebill.birdlist.ui.imports.TaxonPossibilities;
import com.scythebill.birdlist.ui.imports.flickr.FlickrApi.FlickrPhoto;
import com.scythebill.birdlist.ui.imports.flickr.FlickrApi.PhotoSet;
import com.scythebill.birdlist.ui.util.FindSpeciesInText;
import com.scythebill.birdlist.ui.util.FindSpeciesInText.Results;

/**
 * Importer of Flickr albums.
 */
class FlickrImporter extends SightingsImporter<FlickrImporter.PhotoWithTaxa> {

  private final PhotoSet photoSet;
  private final String uri;
  private final Location location;
  private final File reportSetFile;
  private final Gson gson;
  private final List<PhotoWithTaxa> photosWithTaxa;
  private final List<PhotoWithTaxa> failedPhotos = new ArrayList<>();

  public FlickrImporter(ReportSet reportSet, Taxonomy taxonomy, Checklists checklists,
      PredefinedLocations predefinedLocations, PhotoSet photoSet, String uri, Location location, @Nullable File reportSetFile, Gson gson) {
    super(reportSet, taxonomy, checklists, predefinedLocations);
    this.reportSetFile = reportSetFile;
    this.gson = gson;
    this.photoSet = checkNotNull(photoSet);
    this.uri = checkNotNull(uri);
    this.location = location;
    
    photosWithTaxa = extractTaxa();
  }

  @Override
  protected void operateOnAllRows(RowOperator<PhotoWithTaxa> operator) throws IOException {
    for (PhotoWithTaxa photo : photosWithTaxa) {
      operator.operate(photo);
    }
  }

  @Override
  protected TaxonImporter<PhotoWithTaxa> newTaxonImporter(Taxonomy taxonomy) {
    return new TaxonImporter<PhotoWithTaxa>() {

      @Override
      public TaxonPossibilities map(PhotoWithTaxa line) {
        return line.possibilities;
      }

      @Override
      public ToBeDecided decideLater(PhotoWithTaxa line) {
        ToBeDecided tbd = new ToBeDecided();
        tbd.longText = Joiner.on('\n').skipNulls().join(
            line.photo.title,
            line.photo.description());
        return tbd;
      }
      
    };
  }

  @Override
  protected RowExtractor<PhotoWithTaxa, ? extends Object> taxonomyIdExtractor() {
    return new RowExtractor<PhotoWithTaxa, Object>() {
      @Override
      public Object extract(PhotoWithTaxa row) {
        return String.format("%s|%s",
            Strings.nullToEmpty(row.photo.title),
            Strings.nullToEmpty(row.photo.description()));
      }
      
    };
  }

  @Override
  protected void parseLocationIds(LocationSet locations,
      PredefinedLocations predefinedLocations) throws IOException {
    // Do nothing; locations are constant
  }

  @Override
  public List<Sighting> parseSightings() throws IOException {
    if (location != null) {
      reportSet.getLocations().ensureAdded(location);
    }

    RowExtractor<PhotoWithTaxa, ? extends Object> taxonomyIdExtractor = taxonomyIdExtractor();

    List<FieldMapper<PhotoWithTaxa>> mappers = new ArrayList<>();
    mappers.add((photo, builder) -> {
      if (location != null) {
        builder.setLocation(location);
      } else if (getTaxonomy().isBuiltIn()) {
        // No assigned location, so see if endemic status can get us anything.
        Resolved resolved = builder.getTaxon().resolve(getTaxonomy());
        TransposedChecklist transposedChecklist = TransposedChecklists.instance().getTransposedChecklist(
            reportSet, getTaxonomy());
        
        Location location = TransposedChecklists.getAssumedLocation(resolved,
            reportSet.getLocations(), predefinedLocations, transposedChecklist);
        if (location != null) {
          reportSet.getLocations().ensureAdded(location);
          builder.setLocation(location);
        }
      }
      
      ReadablePartial date = photo.photo.date();
      if (date != null) {
        builder.setDate(date);
      }
      
      String description = photo.remainingDescription;
      if (description != null) {
        ImmutableSet<String> words =
            ImmutableSet.copyOf(Splitter.on(CharMatcher.whitespace().or(CharMatcher.anyOf(",.")))
                .split(description.toLowerCase()));
        if (words.contains("male") || words.contains("males")) {
          builder.getSightingInfo().setMale(true);
        }
        if (words.contains("female") || words.contains("females")) {
          builder.getSightingInfo().setFemale(true);
        }
        if (words.contains("adult") || words.contains("adults")) {
          builder.getSightingInfo().setAdult(true);
        }
        if (words.contains("imm") || words.contains("immature") || words.contains("juv") || words.contains("juvenile")
            || words.contains("immatures") || words.contains("juveniles")) {
          builder.getSightingInfo().setImmature(true);
        }
      }
      
      if (photo.photo.latitude != null && !photo.photo.latitude.equals("0")
          && photo.photo.longitude != null && !photo.photo.longitude.equals("0")) {
        try {
          String latLong = String.format(
              Strings.isNullOrEmpty(description)
                  ? "LL:%s,%s" : "\nLL:%s,%s",
              photo.photo.latitude,
              photo.photo.longitude);
          description = Strings.nullToEmpty(description) + latLong;
          
        } catch (IllegalArgumentException e) {
          // ignore
        }
      }
      
      if (!Strings.isNullOrEmpty(photo.photo.tags)) {
        String addingHashTags =
            Splitter.on(CharMatcher.whitespace()).splitToList(photo.photo.tags).stream()
              .map(s -> s.startsWith("#") ? s : "#" + s)
              .collect(Collectors.joining(" "));
        description = Strings.isNullOrEmpty(description) ? addingHashTags : description + "\n" + addingHashTags;
      }
      
      if (!Strings.isNullOrEmpty(description)) {
        builder.getSightingInfo().setDescription(description);
      }
      
      builder.getSightingInfo().setPhotos(
          ImmutableList.of(new Photo(URI.create(photo.photo.photoPage(photoSet.originalOwnerString)))));
    });
    
    ComputedMappings<PhotoWithTaxa> computedMappings =
        new ComputedMappings<>(new TaxonFieldMapper(taxonomyIdExtractor()), null, mappers);
    List<Sighting> sightings = new ArrayList<>();
    for (PhotoWithTaxa photo : photosWithTaxa) {
      parseSighting(photo, computedMappings::mapAll, taxonomyIdExtractor::extract, sightings,
          computedMappings::skipLocationAndTaxonAndUser);
    }

    return sightings;
  }

  @Override
  public String importFileName() {
    List<String> list = Splitter.on('/').omitEmptyStrings().splitToList(uri);
    return list.get(list.size() - 1);
  }

  @Override
  protected void importRowFailed(PhotoWithTaxa importRow) {
    failedPhotos.add(importRow);
  }

  @Override
  public File writeFailedLines() {
    File reportSetFileDirectory;
    if (reportSetFile != null) {
      reportSetFileDirectory = reportSetFile.getParentFile();
    } else {
      // Null happens at startup, if someone's first action is to import from Flickr.
      // Try the current directory? Though it's pretty likely that won't work
      // either (no permissions).
      // TODO: clean this up such that there *is* a reasonable file.
      Path currentRelativePath = Paths.get("");
      reportSetFileDirectory = currentRelativePath.toFile();
    }

    // Loop through (up to) 50 file names to ensure we don't overwrite an existing file.
    File baseFailedLinesFile = new File(reportSetFileDirectory, importFileName() + ".json");
    File failedLinesFile = null;
    for (int i = 0; i < 50; i++) {
      failedLinesFile = failedFile(baseFailedLinesFile, i);
      if (!failedLinesFile.exists()) {
        break;
      }
    }
    // All 50 occupied? Give up.
    if (failedLinesFile == null || failedLinesFile.exists()) {
      return null;
    }

    try {
      if (!failedLinesFile.createNewFile()) {
        return null;
      }

      try (Writer out = new BufferedWriter(
          new OutputStreamWriter(new FileOutputStream(failedLinesFile), Charsets.UTF_8))) {
        gson.toJson(
            failedPhotos.stream().map(p -> p.photo).collect(ImmutableList.toImmutableList()).toArray(),
            out);
        return failedLinesFile;
      }
    } catch (Exception e) {
      return null;
    }
  }

  private File failedFile(File openFile, int index) {
    String failedLinesFileName = openFile.getName();
    if (failedLinesFileName.indexOf('.') >= 0) {
      failedLinesFileName = failedLinesFileName.substring(0, failedLinesFileName.lastIndexOf('.'));
    }

    if (index == 0) {
      failedLinesFileName += "-failed.json";
    } else {
      failedLinesFileName += "-" + index + "-failed.json";
    }
    return new File(openFile.getParentFile(), failedLinesFileName);
  }

  class PhotoWithTaxa {
    final FlickrPhoto photo;
    TaxonPossibilities possibilities;
    String remainingDescription;
    
    PhotoWithTaxa(FlickrPhoto photo) {
      this.photo = photo;
    }
    
    void search(FindSpeciesInText findSpecies, String text) {
      if (Strings.isNullOrEmpty(text)) {
        return;
      }

      Results results = findSpecies.search(text, findSpecies.primarySearch());
      if (results.found.size() > 2) {
        // TaxonPossibilities isn't built around > 2 entries, and it fairly arbitrarily takes the first and last
        // "primaries" and drops the others.  (It was written to deal with sci. name and common name disagreeing -
        // they're both "primaries") A single photo might have many species, though.
        // TODO: Making these a "spuh" is weak, they really should be an option to make these independent entries.
        LinkedHashSet<String> taxa = new LinkedHashSet<>();
        for (Resolved result : results.found) {
          taxa.addAll(result.getSightingTaxon().getIds());
        }
        addPrimary(SightingTaxons.newPossiblySpTaxon(taxa));
      } else {
        for (Resolved result : results.found) {
          addPrimary(result.getSightingTaxon());
        }
      }
        
      results = findSpecies.search(
          results.remainingText, findSpecies.secondarySearch(/*onlySingletonMatches=*/false));
      boolean noPrimariesYet = possibilities == null;
      for (Resolved result : results.found) {
        // TODO: working around Issue #512 - if all the results are from alternates,
        // then treat them all as primaries.
        if (noPrimariesYet) {
          addPrimary(result.getSightingTaxon());
        } else {
          addPossibility(result.getSightingTaxon());
        }
      }
      
      // Nothing still?  TERTIARY!
      if (possibilities == null) {
        results = findSpecies.search(text, findSpecies.tertiarySearch());
        for (Resolved result : results.found) {
          addPossibility(result.getSightingTaxon());
        }
      }
      
      addDescription(results.remainingText);
    }

    private void addPrimary(SightingTaxon sightingTaxon) {
      possibilities = TaxonPossibilities.withPrimary(possibilities, mapToBase(sightingTaxon));
    }
    
    private void addPossibility(SightingTaxon sightingTaxon) {
      possibilities = TaxonPossibilities.adding(possibilities, mapToBase(sightingTaxon));
    }

    private SightingTaxon mapToBase(SightingTaxon sightingTaxon) {
      if (sightingTaxon == null) {
        return null;
      }
      
      if (getTaxonomy() instanceof MappedTaxonomy) {
        return ((MappedTaxonomy) getTaxonomy()).getMapping(sightingTaxon);
      }
      
      return sightingTaxon;
    }

    private void addDescription(String newDescription) {
      if (!Strings.isNullOrEmpty(newDescription)) {
        newDescription = CharMatcher.whitespace().trimFrom(newDescription);
        if (!newDescription.isEmpty()) {
          if (remainingDescription == null) {
            remainingDescription = newDescription;
          } else {
            remainingDescription = remainingDescription + '\n' + newDescription;
          }
        }
      }
    }
  }
  
  private List<PhotoWithTaxa> extractTaxa() {
    List<PhotoWithTaxa> photosWithTaxa = new ArrayList<>(photoSet.photo.size());
    Checklist checklist = getBuiltInChecklist(location);
    FindSpeciesInText globalFindSpecies = new FindSpeciesInText(getTaxonomy(), null);
    FindSpeciesInText localFindSpecies = checklist == null ? null : new FindSpeciesInText(getTaxonomy(), checklist);
    for (FlickrPhoto photo : photoSet.photo) {
      PhotoWithTaxa photoWithTaxa = new PhotoWithTaxa(photo);
      photosWithTaxa.add(photoWithTaxa);
      
      // Search first using the checklist;  title and description
      if (localFindSpecies != null) {
        photoWithTaxa.search(localFindSpecies, photo.title);
        photoWithTaxa.search(localFindSpecies, photo.description());
      }
      
      // If that failed, search using the global checklist
      if (photoWithTaxa.possibilities == null) {
        // Clear out the "remaining description" for this second pass
        photoWithTaxa.remainingDescription = null;
        photoWithTaxa.search(globalFindSpecies, photo.title);
        photoWithTaxa.search(globalFindSpecies, photo.description());
      }
    }
    
    return photosWithTaxa;
  }

  /** Expect Flickr albums with a lot of photos that don't contain anything importable, or are from the wrong taxonomy. */
  @Override
  public boolean expectTaxonSuccess() {
    return false;
  }
}
