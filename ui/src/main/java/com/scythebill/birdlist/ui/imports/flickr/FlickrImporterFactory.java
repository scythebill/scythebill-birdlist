/*   
 * Copyright 2020 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports.flickr;

import java.io.File;
import java.io.IOException;

import javax.annotation.Nullable;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.imports.SightingsImporter;
import com.scythebill.birdlist.ui.imports.flickr.FlickrApi.PhotoSet;

/**
 * Creates {@link SightingsImporter} instances for a Flickr album.
 */
public class FlickrImporterFactory {
  private final FlickrApi flickrApi;
  private final ReportSet reportSet;
  private final File reportSetFile;
  private final TaxonomyStore taxonomyStore;
  private final Checklists checklists;
  private final PredefinedLocations predefinedLocations;
  private final Gson gson;

  @Inject
  public FlickrImporterFactory(
      ReportSet reportSet,
      @Nullable File reportSetFile,
      TaxonomyStore taxonomyStore,
      Checklists checklists,
      PredefinedLocations predefinedLocations,
      FlickrApi flickrApi,
      Gson gson) {
    this.reportSet = reportSet;
    this.reportSetFile = reportSetFile;
    this.taxonomyStore = taxonomyStore;
    this.checklists = checklists;
    this.predefinedLocations = predefinedLocations;
    this.flickrApi = flickrApi;
    this.gson = gson;
  }
  
  public SightingsImporter<?> newFlickrImporter(String albumUri, Location location)
      throws IOException, FlickrApiException {
    PhotoSet lookupAlbum = flickrApi.isAlbumUrl(albumUri) ? flickrApi.lookupAlbum(albumUri) : flickrApi.lookupPhoto(albumUri);
    return new FlickrImporter(reportSet, taxonomyStore.getTaxonomy(), checklists,
        predefinedLocations, lookupAlbum, albumUri, location, reportSetFile, gson);
  }
  
}