/*   
 * Copyright 2024 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.io;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.io.HtmlResponseWriter;
import com.scythebill.birdlist.model.io.IndentingResponseWriter;
import com.scythebill.birdlist.model.io.ResponseWriter;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences.ScientificOrCommon;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.util.ScanSeenTaxa;

/**
 * Outputs sightings to an HTML format.
 */
public class ChecklistHtmlOutput {
  private final static String PRINT_SCRIPT = "window.print();";
  private final static String TEST_FOR_SAFARI = Joiner.on('\n').join(
      "var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&",
      "    navigator.userAgent && !navigator.userAgent.match('CriOS');",
      "if (isSafari) {",
      "  alert('Compact printing is not supported in Safari;  please load in Chrome or Firefox');",
      "} else {",
      PRINT_SCRIPT,
      "}");

  // FWIW: the "COMMON" and "NO_SCI" and "scientific" fields are all a bit off now that
  // there's preferences to default to "Scientific only" or "Scientific first".  "Common"
  // just means "the first name of common/scientific", and "no sci" just means "only one name".
  private final String CSS_STYLE_COMMON = Joiner.on(System.getProperty("line.separator")).join(
      "body { font-size: 13px; }",
      "table { border-spacing: 0px; }",
      "th.familyTitle { font-weight: bold; font-size: 135%; padding-top: 10px; text-align: left }",
      ".lifer { font-weight: bold }",
      ".scientific { font-style: italic }",
      ".total { font-size: 110%; font-weight: bold }");
  private final String CSS_STYLE_COMPACT_OUTPUT = Joiner.on(System.getProperty("line.separator")).join(
      "body { font-size: 11px; }",
      "h2.familyTitle { font-weight: bold; font-size: 135%; text-transform: uppercase; font-style: italic; }",
      ".lifer { font-weight: bold }",
      ".scientific { font-style: italic }",
      ".main { column-count: 3; -webkit-column-count: 3; -moz-column-count: 3; }",
      ".total { font-size: 110%; font-weight: bold }");
  private final String CSS_STYLE_COMPACT_OUTPUT_WITH_ONE_NAME = Joiner.on(System.getProperty("line.separator")).join(
      "body { font-size: 13px; }", // Use a bigger font if there's only one name
      "h2.familyTitle { font-weight: bold; font-size: 135%; text-transform: uppercase; font-style: italic; }",
      ".lifer { font-weight: bold }",
      ".main { column-count: 3; -webkit-column-count: 3; -moz-column-count: 3; }",
      ".total { font-size: 110%; font-weight: bold }");
  private final String CSS_STYLE_SINGLE_SIGHTING =
      "td.name { margin-right: 20px; width: 45% }";
  private final String CSS_STYLE_SINGLE_SIGHTING_NO_SCI = "td.name { margin-right: 20px; width: 30% }";
  private final String CSS_STYLE_WITH_DAYS_FORMAT =      
      Joiner.on(System.getProperty("line.separator")).join(
        "th.days { font-weight: bold; font-size: 135%%; text-align: center; width %.1f%%; border-width: 1px; border-style: dotted}",
        "td.days {border-width: 1px; border-style: dotted}");
  
  private String title;
  private final Taxonomy taxonomy;
  private boolean showFamilies;
  private boolean showStatus;
  private ScientificOrCommon scientificOrCommon;
  private boolean compactOutput;
  private String totalText;
  private boolean print;
  private boolean showLifersInBold;
  private int daysToInclude = 0;

  public ChecklistHtmlOutput(Taxonomy taxonomy) {
    this.taxonomy = taxonomy;
  }

  public void setPrint(boolean print) {
    this.print = print;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void writeSpeciesList(File outFile, ReportSet reportSet, QueryResults queryResults,
      Checklist checklist, ScanSeenTaxa scannedTaxa, List<Location> locations) throws IOException {
    try (Writer out = IOUtils.openNewFile(outFile, Charsets.UTF_8)) {
      writeSpeciesList(out, reportSet, queryResults, checklist, scannedTaxa, locations);
    }
  }

  public void writeSpeciesList(Writer out, ReportSet reportSet, QueryResults queryResults,
      Checklist checklist, ScanSeenTaxa scannedTaxa, List<Location> locations)
      throws IOException {
    ResponseWriter rw = new IndentingResponseWriter(new HtmlResponseWriter(out,
        "UTF-8"));

    startHtml(rw, title);
    startChecklist(rw);

    // The input "locations" list, except:
    //  - In inverse order (from world to checklist location)
    //  - Omitting any locations that have no sightings (since obviously everything's new)
    List<Location> locationsWithSightings = new ArrayList<>();
    for (Location location : locations) {
      if (location != null) {
        // Only include one of the other locations if there's any existing sightings
        // for that location.
        Set<String> seenTaxa = scannedTaxa.getSeenTaxa(taxonomy, location);
        if (seenTaxa != null && !seenTaxa.isEmpty()) {
          locationsWithSightings.add(0, location);
        }
      }
    }
    
    List<Resolved> taxa = queryResults.getTaxaAsList();
    
    for (int i = 0; i < taxa.size(); i++) {
      Resolved taxon = taxa.get(i);

      if (taxon.getSmallestTaxonType() == Taxon.Type.family) {
        writeFamilyHeader(rw, taxon.getTaxon(), taxa.subList(i + 1, taxa.size()), queryResults);
      } else {
        writeSpecies(rw, taxon,
            checklist, scannedTaxa, locationsWithSightings);
      }
    }

    endChecklist(rw);

    if (!Strings.isNullOrEmpty(totalText)) {
      rw.startElement("p");
      rw.writeAttribute("class", "total");
      rw.write(totalText);      
      rw.endElement("p");
    }

    rw.startElement("p");
    rw.write(taxonomy.getName());
    rw.endElement("p");
    
    endHtml(rw);

    rw.flush();
  }

  private void writeSpecies(ResponseWriter rw, Resolved taxon, Checklist checklist,
      ScanSeenTaxa scannedTaxa, List<Location> locationsWithSightings) 
      throws IOException {
    if (compactOutput) {
      rw.startElement("div");
      rw.writeAttribute("class", "species");

      rw.startElement("span");
    } else {
      rw.startElement("tr");
      rw.writeAttribute("class", "species");
  
      rw.startElement("td");
      rw.writeAttribute("class", "name");
    }
    
    rw.writeText("___ ");

    rw.startElement("span");
    if (showLifersInBold && taxon.getType() == SightingTaxon.Type.SINGLE
        && !scannedTaxa.getSeenTaxa(taxonomy, null).contains(taxon.getTaxon().getId())) {
      rw.writeAttribute("class", "common lifer");
    } else {
      rw.writeAttribute("class", "common");
    }
    
    switch (scientificOrCommon) {
      case COMMON_FIRST:
        // Omit the subspecies if it'll appear anyway in the next column
        rw.writeText(taxon.getSimpleCommonName());
        break;
      case COMMON_ONLY:
        // But include it if it won't.
        rw.writeText(taxon.getCommonName());
        break;
      case SCIENTIFIC_FIRST:
      case SCIENTIFIC_ONLY:
        rw.writeText(taxon.getFullName());
        break;
    }
    
    // Output if the species is endemic to the current checklist
    SightingTaxon species;
    if (taxon.getSmallestTaxonType() == Taxon.Type.species) {
      species = taxon.getSightingTaxon();
    } else {
      species = taxon.getParentOfAtLeastType(Taxon.Type.species);
    }
    
    rw.endElement("span");

    if (scientificOrCommon == ScientificOrCommon.COMMON_FIRST
        || scientificOrCommon == ScientificOrCommon.SCIENTIFIC_FIRST) {
      rw.startElement("span");
      rw.writeAttribute("class", "scientific");
      rw.writeText('(');
      rw.writeText(scientificOrCommon == ScientificOrCommon.SCIENTIFIC_FIRST
          ? taxon.getSimpleCommonName()
          : taxon.getFullName());
      rw.writeText(')');
      rw.endElement("span");
    }
    
    Checklist.Status checklistStatus = checklist.getStatus(taxonomy, species);
    if (checklistStatus != Checklist.Status.NATIVE
        && checklistStatus != null) {
      rw.writeText(" - " + Messages.getText(checklistStatus));
    }

    if (showStatus) {
      Status status = taxon.getTaxonStatus();
      // Take note of species that are VU or worse
      if (status != Species.Status.LC && status != Species.Status.NT) {
        rw.writeText(" ("+ status + ")");
      }
    }
    
    if (compactOutput) {
      rw.endElement("span");
    } else {
      rw.endElement("td");    
    }

    if (!compactOutput && daysToInclude > 0) {
      for (int i = 1; i <= daysToInclude; i++) {
        rw.startElement("td");
        rw.writeAttribute("class", "days");
        rw.endElement("td");
      }
    }

    if (compactOutput) {
      rw.endElement("div");
    } else {
      rw.endElement("tr");
    }
  }

  private void writeFamilyHeader(ResponseWriter rw, Taxon family,
      List<Resolved> list, QueryResults queryResults) throws IOException {

    if (!showFamilies) {
      return;
    }

    if (compactOutput) {
      rw.startElement("h2");
    } else {
      rw.startElement("tr");
      rw.startElement("th");
      rw.writeAttribute("colspan", 1 + daysToInclude);
    }
    rw.writeAttribute("class", "familyTitle");
    
    switch (scientificOrCommon) {
      case COMMON_FIRST:
        rw.writeText(family.getCommonName());
        rw.writeText(" (");
        rw.writeText(family.getName());
        rw.writeText(")");
        break;
      case SCIENTIFIC_FIRST:
        rw.writeText(family.getName());
        rw.writeText(" (");
        rw.writeText(family.getCommonName());
        rw.writeText(")");
        break;
      case SCIENTIFIC_ONLY:
        rw.writeText(family.getName());
        break;
      case COMMON_ONLY:
        rw.writeText(family.getCommonName());
        break;
    }

    if (compactOutput) {
      rw.endElement("h2");
    } else {
      rw.endElement("th");
      rw.endElement("tr");
    }
  }

  private void startHtml(ResponseWriter rw, String title)
      throws IOException {
    rw.startDocument();
    rw.writeComment("saved from url=(0016)http://localhost");
    rw.startElement("html");
    rw.startElement("head");

    rw.startElement("meta");
    rw.writeAttribute("http-equiv", "Content-Type");
    rw.writeAttribute("content", "text/html; charset=UTF-8");
    rw.endElement("meta");
    if (title != null) {
      rw.startElement("title");
      rw.writeText(title);
      rw.endElement("title");
    }

    rw.startElement("style");
    rw.writeAttribute("type", "text/css");
    boolean scientificAndCommon = scientificOrCommon == ScientificOrCommon.COMMON_FIRST
        || scientificOrCommon == ScientificOrCommon.SCIENTIFIC_FIRST;
    
    if (compactOutput) {
      rw.writeText(scientificAndCommon
          ? CSS_STYLE_COMPACT_OUTPUT
          : CSS_STYLE_COMPACT_OUTPUT_WITH_ONE_NAME);
    } else {
      rw.writeText(CSS_STYLE_COMMON);
      rw.writeText(
          scientificAndCommon ? CSS_STYLE_SINGLE_SIGHTING : CSS_STYLE_SINGLE_SIGHTING_NO_SCI);
      if (daysToInclude > 0) {
        double daysPercentage = (scientificAndCommon ? 55.0 : 70.0) / daysToInclude;
        rw.writeText(String.format(CSS_STYLE_WITH_DAYS_FORMAT, daysPercentage)); 
      }
    }
    rw.endElement("style");
    
    if (print) {
      rw.startElement("script");
      rw.writeAttribute("type", "text/javascript");
      rw.writeRawText("function onload_script() {\n");
      if (compactOutput) {
        rw.writeRawText(TEST_FOR_SAFARI);
      } else {
        rw.writeRawText(PRINT_SCRIPT);
      }
      rw.writeRawText("\n}");
      rw.endElement("script");
    }

    rw.endElement("head");
    rw.startElement("body");
    if (print) {
      rw.writeAttribute("onload", "onload_script();");
    }
    
  }

 private void startChecklist(ResponseWriter rw) throws IOException {
   if (compactOutput) {
     rw.startElement("div");
     rw.writeAttribute("class", "main");
   } else {
     rw.startElement("table");
     rw.writeAttribute("width", "100%");
     if (daysToInclude > 0) {
       rw.startElement("thead");
       rw.startElement("tr");

       // Name
       rw.startElement("th");
       rw.endElement("th");
       
       for (int i = 1; i <= daysToInclude; i++) {
         rw.startElement("th");
         rw.writeAttribute("class", "days");

         rw.writeText("\u00a0\u00a0");
         rw.writeText(i);
         rw.writeText("\u00a0\u00a0");
         rw.endElement("th");
       }

       rw.endElement("tr");
       
       rw.endElement("thead");
     }
     rw.startElement("tbody");
   }
 }
 
 private void endChecklist(ResponseWriter rw) throws IOException {
    if (compactOutput) {
      rw.endElement("div");
    } else {
      rw.endElement("tbody");
      rw.endElement("table");
    }
  }

  private void endHtml(ResponseWriter rw) throws IOException {
    rw.endElement("body");
    rw.endElement("html");
    rw.endDocument();
  }

  public void setShowFamilies(boolean showFamilies) {
    this.showFamilies = showFamilies;
  }

  public void setShowStatus(boolean showStatus) {
    this.showStatus = showStatus;
  }

  public void setScientificOrCommon(ScientificOrCommon scientificOrCommon) {
    this.scientificOrCommon = scientificOrCommon;
  }
  
  public void setCompactOutput(boolean compactPrinting) {
    this.compactOutput = compactPrinting;
  }

  public void setTotalText(String totalText) {
    this.totalText = totalText;
  }

  public void setShowLifersInBold(boolean showLifersInBold) {
    this.showLifersInBold = showLifersInBold;
  }
  
  public void setDaysToInclude(int daysToInclude) {
    this.daysToInclude = daysToInclude;
  }
 }
