/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.io;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.WorkbookUtil;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Ordering;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.query.SightingComparators;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.ScanSeenTaxa;
import com.scythebill.birdlist.ui.util.SightingFlags;

/** Writes query results and a checklist to an Excel xls file. */
public class ChecklistXlsOutput {
  private final File file;
  private boolean includeScientific;
  private boolean showLifersInBold;
  private boolean showStatus;
  private boolean includeSighting;
  private boolean showNewForColumn;
  private boolean showFamilies;
  private ImmutableMap<Taxonomy, String> totalText;
  private ImmutableMap<Taxonomy, Checklist> otherTaxonomiesWithChecklists;
  private CellStyle headerStyle;
  private CellStyle familyStyle;
  private CellStyle liferStyle;
  private CellStyle newForLocationStyle;
  private CellStyle scientificStyle;
  private CellStyle headerRowStyle;
  private CellStyle totalStyle;
  private boolean showAllTaxonomies;
  private ChecklistSightingOption checklistSightingOption;

  public ChecklistXlsOutput(File file) {
    this.file = file;
  }

  public void write(
      ReportSet reportSet, QueryResults queryResults, QueryResults worldResults, Checklist checklist,
      ScanSeenTaxa scannedTaxa,
      List<Location> locations,
      int days)
      throws IOException {
    try (Workbook workbook = new HSSFWorkbook()) {
      headerStyle = workbook.createCellStyle();
      Font headerFont = workbook.createFont();
      headerFont.setBold(true);
      headerStyle.setFont(headerFont);
      headerStyle.setBorderBottom(BorderStyle.THICK);
      headerStyle.setAlignment(HorizontalAlignment.CENTER);
  
      familyStyle = workbook.createCellStyle();
      Font familyFont = workbook.createFont();
      familyFont.setBold(true);
      familyStyle.setFont(familyFont);
  
      liferStyle = workbook.createCellStyle();
      Font liferFont = workbook.createFont();
      liferFont.setBold(true);
      liferStyle.setFont(liferFont);
  
      newForLocationStyle = workbook.createCellStyle();
      Font newForLocationFont = workbook.createFont();
      newForLocationFont.setItalic(true);
      newForLocationStyle.setFont(newForLocationFont);

      scientificStyle = workbook.createCellStyle();
      Font scientificFont = workbook.createFont();
      scientificFont.setItalic(true);
      scientificStyle.setFont(scientificFont);
      
      headerRowStyle = workbook.createCellStyle();
      headerStyle.setBorderBottom(BorderStyle.THICK);
      headerStyle.setAlignment(HorizontalAlignment.CENTER);
      
      totalStyle = workbook.createCellStyle();
      totalStyle.cloneStyleFrom(headerStyle);
      totalStyle.setAlignment(HorizontalAlignment.GENERAL);
      
      Sheet sheet = workbook
          .createSheet(WorkbookUtil.createSafeSheetName(queryResults.getTaxonomy().getName()));

      writeSheet(sheet, reportSet, queryResults, worldResults, checklist, scannedTaxa, locations, days, queryResults.getTaxonomy());
      
      if (showAllTaxonomies
          && otherTaxonomiesWithChecklists != null) {
        otherTaxonomiesWithChecklists.forEach((otherTaxonomy, otherChecklist) -> {
          Sheet otherSheet = workbook.createSheet(
              WorkbookUtil.createSafeSheetName(otherTaxonomy.getName()));
          writeSheet(otherSheet, reportSet, queryResults, worldResults, otherChecklist, scannedTaxa, locations, days, otherTaxonomy);
        });
      }
      
      
      try (OutputStream out = new BufferedOutputStream(new FileOutputStream(file))) {
        workbook.write(out);
      }
    }
  }

  private void writeSheet(Sheet sheet, ReportSet reportSet, QueryResults queryResults,
      QueryResults worldResults, Checklist checklist, ScanSeenTaxa scannedTaxa, List<Location> locations, int days,
      Taxonomy taxonomy) {
    // The input "locations" list, except:
    //  - In inverse order (from world to checklist location)
    //  - Omitting any locations that have no sightings (since obviously everything's new)
    List<Location> locationsWithSightings = new ArrayList<>();
    for (Location location : locations) {
      if (location != null) {
        // Only include one of the other locations if there's any existing sightings
        // for that location.
        Set<String> seenTaxa = scannedTaxa.getSeenTaxa(taxonomy, location);
        if (seenTaxa != null && !seenTaxa.isEmpty()) {
          locationsWithSightings.add(0, location);
        }
      }
    }
    
    Set<String> worldSeenTaxa = scannedTaxa.getSeenTaxa(taxonomy, null);
 
    // Only bother including a photographed column if the person has clearly used
    // the "photographed" field.
    Set<String> photographedTaxa = scannedTaxa.getPhotographedTaxa(taxonomy);
    boolean includePhotographedColumn = photographedTaxa != null && photographedTaxa.size() > 50;
  
    Row header = sheet.createRow(0);
    header.setRowStyle(headerRowStyle);
    
    if (totalText != null) {
      Cell cell = header.createCell(0);
      cell.setCellValue(totalText.get(taxonomy));
      cell.setCellStyle(totalStyle);
      sheet.addMergedRegion(new CellRangeAddress(0, 0, 0,
          1 + (includeScientific ? 1 : 0) + (includeSighting ? 1 : 0)));
    }
    int firstDayOffset = 2;
    int sightingColumn = -1;
    if (includeScientific) {
      firstDayOffset++;
    }
    if (includeSighting) {
      sightingColumn = firstDayOffset++;
    }
    
    int newForColumn = -1;
    if (showNewForColumn) {
      newForColumn = firstDayOffset++;
      Cell cell = header.createCell(newForColumn);
      cell.setCellValue(Messages.getMessage(Name.NEW_FOR_COLUMN));
      cell.setCellStyle(headerStyle);
    }
    
    for (int i = 0; i < days; i++) {
      Cell cell = header.createCell(i + firstDayOffset);
      cell.setCellValue(i + 1);
      cell.setCellStyle(headerStyle);
    }
    sheet.createFreezePane(0, 1, 0, 1);
 
    int rowCount = 1;
    int speciesCount = 1;
    List<Resolved> taxa = taxonomy == queryResults.getTaxonomy()
        ? queryResults.getTaxaAsList()
        : queryResults.getIncompatibleTaxaAsList(taxonomy, showFamilies);
    for (Resolved resolved : taxa) {
      boolean foundFamily = resolved.getLargestTaxonType() == Taxon.Type.family;
      if (foundFamily && rowCount > 1 && showFamilies) {
        rowCount++;
      }
      
      int column = 0;
      
      if (foundFamily) {
        if (showFamilies) {
          Row row = sheet.createRow(rowCount++);
          Cell common = row.createCell(column++);
          common.setCellStyle(familyStyle);
          String text;
          if (includeScientific) {
            text = String.format("%s (%s)", resolved.getCommonName(), resolved.getFullName());
          } else {
            text = resolved.getCommonName();
          }
          
          common.setCellValue(text);
          sheet.addMergedRegion(new CellRangeAddress(
              row.getRowNum(), row.getRowNum(), 0,
              1 + (includeScientific ? 1 : 0) + (includeSighting ? 1 : 0)));
        }
      } else {
        Status status = checklist.getStatus(resolved.getTaxonomy(), resolved.getSightingTaxon());
        boolean sightingFromWorld = checklistSightingOption == ChecklistSightingOption.FIRST_IN_WORLD
            || checklistSightingOption == ChecklistSightingOption.MOST_RECENT_IN_WORLD;
        QueryResults resultsForSighting = sightingFromWorld ? worldResults : queryResults;
        Ordering<Sighting> ordering =
            checklistSightingOption == ChecklistSightingOption.FIRST_IN_LOCATION
                || checklistSightingOption == ChecklistSightingOption.FIRST_IN_WORLD
                    ? SightingComparators.preferEarlier()
                    : SightingComparators.preferMoreRecent();
        Sighting bestSighting = resultsForSighting.getBestSighting(resolved, ordering);
        
        // Skip escaped species unless the user wants to include sighting *and* there's one to show.
        // Also skip escaped species when "include sighting" is a world sighting
        if (status == Status.ESCAPED) {
          if (!includeSighting || bestSighting == null || sightingFromWorld) {
            continue;
          }
        }
        
        Row row = sheet.createRow(rowCount++);
        Cell speciesNumber = row.createCell(column);
        speciesNumber.setCellValue(Integer.toString(speciesCount++));
        
        column++;
        
        Cell common = row.createCell(column++);
        boolean isLifer = false;
        if (showLifersInBold || showNewForColumn) {
          if ((worldSeenTaxa != null) && resolved.getType() == SightingTaxon.Type.SINGLE) {
            isLifer = !worldSeenTaxa.contains(resolved.getTaxon().getId());
          }
        }

        if (showLifersInBold) {
          if (isLifer) {
            common.setCellStyle(liferStyle);
          } else if (bestSighting == null) {
            common.setCellStyle(newForLocationStyle);
          }
        }
        
        String text = resolved.getCommonName();
        
        if (status != null) {
          switch (status) {
            case ENDEMIC:
            case RARITY:
            case INTRODUCED:
            case ESCAPED:
            case EXTINCT:
              text += " (" + Messages.getText(status) + ")";
              break;
            case RARITY_FROM_INTRODUCED:
              // For this one, just show as a rarity
              text += " (" + Messages.getText(Status.RARITY) + ")";
              break;
            default:
              break;
          }
        }
                  
        if (showStatus) {
          Species.Status taxonStatus = resolved.getTaxonStatus();
          if (taxonStatus != Species.Status.LC
              && taxonStatus != Species.Status.NT) {
            text += " - " + taxonStatus.name();
          }
        }
        
        common.setCellValue(text);
        
        
        if (includeScientific) {
          Cell scientific = row.createCell(column++);
          scientific.setCellStyle(scientificStyle);
          scientific.setCellValue(resolved.getFullName());
        }
        
        if (includeSighting) {
          if (bestSighting != null) {
            String sightingText;
            if (bestSighting.getLocationId() != null) {
              Location location = reportSet.getLocations().getLocation(bestSighting.getLocationId());
              
              // For world sightings, drop some of the specificity and show state/country names instead
              // (based on what sort of locations the current checklist is using)
              if (sightingFromWorld) {
                Location.Type locationType = locations.get(0).getType();
                if (locationType == null) {
                  locationType = Location.Type.country;
                }
                Location parentOfType = Locations.getAncestorOfType(location, locationType);
                if (parentOfType != null) {
                  location = parentOfType;
                } else if (locationType != Location.Type.country) {
                  Location country = Locations.getAncestorOfType(location, Location.Type.country);
                  if (country != null) {
                    location = country;
                  }
                }
                
              }
              sightingText = location.getDisplayName();
            } else {
              sightingText = "";
            }
            
            if (bestSighting.getDateAsPartial() != null) {
              String dateText = PartialIO.toShortUserString(bestSighting.getDateAsPartial(), Locale.getDefault());
              if (sightingText.isEmpty()) {
                sightingText = dateText;
              } else {
                sightingText = sightingText + " " + dateText;
              }
            }
            
            sightingText = SightingFlags.appendSightingInfo(sightingText, bestSighting);
            
            Cell sightingCell = row.createCell(column++);
            sightingCell.setCellValue(sightingText);
          }
        }

        if (showNewForColumn && resolved.getType() == SightingTaxon.Type.SINGLE) {
          Cell newFor = row.createCell(newForColumn);
          if (isLifer) {
            newFor.setCellValue(Messages.getMessage(Name.WORLD_TEXT));
          } else if (includePhotographedColumn
              && scannedTaxa.getPhotographedTaxa(taxonomy) != null
              && !scannedTaxa.getPhotographedTaxa(taxonomy).contains(resolved.getTaxon().getId())) {
            newFor.setCellValue(Messages.getMessage(Name.PHOTOGRAPHED_TEXT));
          } else {
            for (Location location : locationsWithSightings) {
              Set<String> locationSeenTaxa = scannedTaxa.getSeenTaxa(taxonomy, location);
              if (locationSeenTaxa != null
                  && !locationSeenTaxa.contains(resolved.getTaxon().getId())) {
                newFor.setCellValue(location.getDisplayName());
                break;
              }
            }
          }
        }
        
      }
    }
    
    sheet.autoSizeColumn(0);
    sheet.autoSizeColumn(1);
    if (includeScientific) {
      sheet.autoSizeColumn(2);
    }
    sheet.setRepeatingRows(new CellRangeAddress(0, 0, 0, 1));
    sheet.getPrintSetup().setLandscape(true);
    sheet.getPrintSetup().setFitWidth((short) 1);
    sheet.getPrintSetup().setFitHeight((short) (rowCount / 20));
    sheet.setPrintGridlines(true);
    sheet.setFitToPage(true);
    
    if (sightingColumn >= 0) {
      sheet.autoSizeColumn(sightingColumn);
    }
    
    if (newForColumn >= 0) {
      sheet.autoSizeColumn(newForColumn);
    }
  }

  public void setIncludeScientific(boolean includeScientific) {
    this.includeScientific = includeScientific;
  }

  public void setShowLifersInBold(boolean showLifersInBold) {
    this.showLifersInBold = showLifersInBold;
  }
  
  public void setShowNewForColumn(boolean showNewForColumn) {
    this.showNewForColumn = showNewForColumn;
  }
  
  public void setShowFamilies(boolean showFamilies) {
    this.showFamilies = showFamilies;
  }
  
  public void setShowStatus(boolean showStatus) {
    this.showStatus = showStatus;
  }

  public void setIncludeSighting(boolean includeSighting) {
    this.includeSighting = includeSighting;
  }

  public enum ChecklistSightingOption {
    FIRST_IN_LOCATION,
    MOST_RECENT_IN_LOCATION,
    FIRST_IN_WORLD,
    MOST_RECENT_IN_WORLD
  }
  
  public void setChecklistSightingOption(ChecklistSightingOption checklistSightingOption) {
    this.checklistSightingOption = checklistSightingOption;
  }

  public void setTotalText(Map<Taxonomy, String> totalTextMap) {
    this.totalText = ImmutableMap.copyOf(totalTextMap);
  }

  public void setOtherTaxonomiesWithChecklists(
      Map<Taxonomy, Checklist> otherTaxonomiesWithChecklists) {
    this.otherTaxonomiesWithChecklists = ImmutableMap.copyOf(otherTaxonomiesWithChecklists);
  }

  public void setShowAllTaxonomies(boolean showAllTaxonomies) {
    this.showAllTaxonomies = showAllTaxonomies;
    
  }
}
