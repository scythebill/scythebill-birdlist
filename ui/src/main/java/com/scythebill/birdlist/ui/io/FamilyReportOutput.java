/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.io;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.io.HtmlResponseWriter;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences.ScientificOrCommon;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.FamilyReportPreferences.SortBy;
import com.scythebill.birdlist.ui.panels.reports.QueryPreferences;

/**
 * Writes a family-level report.
 */
public class FamilyReportOutput {
  private final QueryResults queryResults;
  private ScientificOrCommon scientificOrCommon = ScientificOrCommon.COMMON_FIRST;
  private Checklist checklist;
  private Taxonomy taxonomy;
  private QueryPreferences queryPreferences;
  private SortBy sortBy = SortBy.topCount;

  public FamilyReportOutput(QueryResults queryResults,
      Taxonomy taxonomy, QueryPreferences queryPreferences) {
    this.queryResults = queryResults;
    this.taxonomy = taxonomy;
    this.queryPreferences = queryPreferences;
  }
  
  public void setChecklist(Checklist checklist) {
    this.checklist = checklist;
  }

  public void setScientificOrCommon(ScientificOrCommon scientificOrCommon) {
    this.scientificOrCommon = scientificOrCommon;
  }

  public void setSortBy(SortBy sortBy) {
    this.sortBy = sortBy;
  }
  
  /** Write the HTML to Writer, and return plain text */
  // close() responsibility is on the caller;  this class flushes its buffers.
  @SuppressWarnings("resource")
  public String writeReport(Writer writer) throws IOException {

    Map<String, Integer> observedTaxaByFamily = new LinkedHashMap<>();
    Map<String, Integer> possibleTaxaByFamily = new LinkedHashMap<>();
    
    Collection<String> sortedFamilies =
        computeTotalsAndSort(observedTaxaByFamily, possibleTaxaByFamily);

    TeeToPlainTextResponseWriter out = new TeeToPlainTextResponseWriter(
        new HtmlResponseWriter(new BufferedWriter(writer), "UTF-8"));
    out.startDocument();
    out.startElement("document");
    out.startElement("body");
    
    int observedFamilies = 0;
    int possibleFamilies = 0;

    for (String familyId : sortedFamilies) {
      // Skip impossible families
      if (possibleTaxaByFamily.get(familyId) == 0) {
        continue;
      }

      possibleFamilies++;
      if (observedTaxaByFamily.get(familyId).intValue() > 0) {
        observedFamilies++;
      }
    }

    out.startElement("b");
    out.writeText(Messages.getFormattedMessage(
        Name.FAMILIES_OF_FORMAT, observedFamilies, possibleFamilies));    
    out.endElement("b");

    out.startElement("br");
    out.endElement("br");
    out.startElement("br");
    out.endElement("br");
    
    for (String familyId : sortedFamilies) {
      // Skip impossible families
      if (possibleTaxaByFamily.get(familyId) == 0) {
        continue;
      }

      Taxon family = taxonomy.getTaxon(familyId);
      out.startElement("b");
      switch (scientificOrCommon) {
        case COMMON_FIRST:
          if (!Strings.isNullOrEmpty(family.getCommonName())) {
            out.writeText(family.getCommonName());
            out.writeText(" (");
            out.startElement("i");
            out.writeText(family.getName());
            out.endElement("i");
            out.writeText(")");
          } else {
            out.writeText(family.getName());
          }
          break;
        case COMMON_ONLY:
          if (!Strings.isNullOrEmpty(family.getCommonName())) {
            out.writeText(family.getCommonName());
          } else {
            out.writeText(family.getName());
          }
          break;
        case SCIENTIFIC_FIRST:
          if (!Strings.isNullOrEmpty(family.getCommonName())) {
            out.writeText(family.getName());
            out.writeText(" - ");
            out.writeText(family.getCommonName());
          } else {
            out.writeText(family.getName());
          }
          break;
        case SCIENTIFIC_ONLY:
          out.writeText(family.getName());
          break;
        default:
          throw new AssertionError("Unexpected " + scientificOrCommon);
      }
      out.endElement("b");
      
      out.writeText(":\t");
      out.writeText(observedTaxaByFamily.get(familyId));
      out.writeText("/");
      out.writeText(possibleTaxaByFamily.get(familyId));
            
      out.startElement("br");
      out.endElement("br");
    }
    
    out.endElement("body");
    out.endElement("document");
    out.endDocument();
    out.flush();
    
    return out.getPlainText();
  }


  public String writeToTabDelimitedText() throws IOException {
    StringWriter writer = new StringWriter();
    
    Map<String, Integer> observedTaxaByFamily = new LinkedHashMap<>();
    Map<String, Integer> possibleTaxaByFamily = new LinkedHashMap<>();
    
    Collection<String> sortedFamilies =
        computeTotalsAndSort(observedTaxaByFamily, possibleTaxaByFamily);

    int observedFamilies = 0;
    int possibleFamilies = 0;

    for (String familyId : sortedFamilies) {
      // Skip impossible families
      if (possibleTaxaByFamily.get(familyId) == 0) {
        continue;
      }

      possibleFamilies++;
      if (observedTaxaByFamily.get(familyId).intValue() > 0) {
        observedFamilies++;
      }
    }

    writer.write(Messages.getFormattedMessage(
        Name.FAMILIES_OF_FORMAT, observedFamilies, possibleFamilies));
    writer.write('\n');
    
    for (String familyId : sortedFamilies) {
      // Skip impossible families
      if (possibleTaxaByFamily.get(familyId) == 0) {
        continue;
      }

      List<String> line = new ArrayList<>();
      Taxon family = taxonomy.getTaxon(familyId);
      switch (scientificOrCommon) {
        case COMMON_FIRST:
          if (!Strings.isNullOrEmpty(family.getCommonName())) {
            line.add(family.getCommonName());
            line.add(family.getName());
          } else {
            line.add(family.getName());
            line.add("");
          }
          break;
        case COMMON_ONLY:
          if (!Strings.isNullOrEmpty(family.getCommonName())) {
            line.add(family.getCommonName());
          } else {
            line.add(family.getName());
          }
          break;
        case SCIENTIFIC_FIRST:
          if (!Strings.isNullOrEmpty(family.getCommonName())) {
            line.add(family.getName());
            line.add(family.getCommonName());
          } else {
            line.add(family.getName());
            line.add("");
          }
          break;
        case SCIENTIFIC_ONLY:
          line.add(family.getName());
          break;
        default:
          throw new AssertionError("Unexpected " + scientificOrCommon);
      }
      
      line.add("" + observedTaxaByFamily.get(familyId));
      line.add("" + possibleTaxaByFamily.get(familyId));
      
      writer.write(Joiner.on('\t').join(line));
      writer.write('\n');
    }
    
    return writer.toString();
  }

  private Collection<String> computeTotalsAndSort(Map<String, Integer> observedTaxaByFamily,
      Map<String, Integer> possibleTaxaByFamily) throws AssertionError {
    TaxonUtils.visitTaxa(taxonomy, new TaxonVisitor() {
      @Override
      public boolean visitTaxon(Taxon taxon) {
        if (taxon.getType() == Taxon.Type.family) {
          Set<String> speciesSet = new LinkedHashSet<>();
          TaxonUtils.visitTaxa(taxon, new TaxonVisitor() {
            @Override public boolean visitTaxon(Taxon child) {
              if (queryResults.containsTaxonCountably(child)) {
                Taxon species = TaxonUtils.getParentOfType(child, Taxon.Type.species);
                speciesSet.add(species.getId());
                return false;
              }
              // TODO: could break earlier if we knew the max depth of the query
              // results (avoid iterating through subspecies when we know the
              // query results don't contain subspecies)
              return true;
            }
          });
          observedTaxaByFamily.put(taxon.getId(), speciesSet.size());
          
          int speciesCount = TaxonUtils.countChildren(
              taxon, Taxon.Type.species, checklist, queryPreferences.excludedChecklistStatuses());
          
          // Remove any undescribed species from both totals
          if (!queryPreferences.countUndescribed) {
            AtomicInteger undescribedCount = new AtomicInteger();
            TaxonUtils.visitTaxa(taxon, new TaxonVisitor() {
              @Override public boolean visitTaxon(Taxon child) {
                if (child.getType() == Taxon.Type.species) {
                  if (child.getStatus() == Species.Status.UN) {
                    undescribedCount.incrementAndGet();
                    speciesSet.remove(child.getId());
                  }
                  
                  return false;
                }
                return true;
              }
            });
            speciesCount -= undescribedCount.get();
          }
          
          possibleTaxaByFamily.put(taxon.getId(),
              Math.max(speciesSet.size(), speciesCount));
          return false;
        }
        return true;
      }
    });
    
    Collection<String> sortedFamilies;
    switch (sortBy) {
      case taxonomic:
        sortedFamilies = observedTaxaByFamily.keySet();
        break;
      case topCount:
        Comparator<String> sortByCount = Comparator.<String>comparingInt(
            observedTaxaByFamily::get)
            .reversed()
            .thenComparingInt(possibleTaxaByFamily::get);
        sortedFamilies = observedTaxaByFamily.keySet().stream()
            .sorted(sortByCount)
            .collect(ImmutableList.toImmutableList());
        break;
      case topFraction:
        Comparator<String> sortByFraction = Comparator.<String>comparingDouble(
            id -> {
              if (observedTaxaByFamily.get(id) == 0) {
                return -possibleTaxaByFamily.get(id);
              }
              
              return ((double) observedTaxaByFamily.get(id))
                  / ((double) possibleTaxaByFamily.get(id));
            }).thenComparingInt(possibleTaxaByFamily::get).reversed();
        sortedFamilies = observedTaxaByFamily.keySet().stream()
            .sorted(sortByFraction)
            .collect(ImmutableList.toImmutableList());
        break;
      default:
        throw new AssertionError("Unexpected sort: " + sortBy); 
    }
    return sortedFamilies;
  }
}
