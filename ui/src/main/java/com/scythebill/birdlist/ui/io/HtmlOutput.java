/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.io;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.joda.time.ReadablePartial;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.io.HtmlResponseWriter;
import com.scythebill.birdlist.model.io.IndentingResponseWriter;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.io.ResponseWriter;
import com.scythebill.birdlist.model.query.QueryDefinition.QueryAnnotation;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences.ScientificOrCommon;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;
import com.scythebill.birdlist.ui.util.LocationIdToString;
import com.scythebill.birdlist.ui.util.SightingFlags;

/**
 * Outputs sightings to an HTML format.
 */
public class HtmlOutput {
  // FWIW: the "COMMON" and "NO_SCI" and "scientific" fields are all a bit off now that
  // there's preferences to default to "Scientific only" or "Scientific first".  "Common"
  // just means "the first name of common/scientific", and "no sci" just means "only one name".
  private final String CSS_STYLE_COMMON = Joiner.on(System.getProperty("line.separator")).join(
      "body { font-size: 13px; }",
      "th.familyTitle { font-weight: bold; font-size: 135%; padding-top: 10px; text-align: left }",
      ".lifer { font-weight: bold }",
      ".total { font-size: 110%; font-weight: bold }");
  private final String CSS_STYLE_COMPACT_OUTPUT = Joiner.on(System.getProperty("line.separator")).join(
      "body { font-size: 11px; }",
      "h2.familyTitle { font-weight: bold; font-size: 135%; text-transform: uppercase; font-style: italic; }",
      ".lifer { font-weight: bold }",
      ".scientific { font-style: italic }",
      ".main { column-count: 3; -webkit-column-count: 3; -moz-column-count: 3; }",
      ".total { font-size: 110%; font-weight: bold }");
  private final String CSS_STYLE_COMPACT_OUTPUT_WITH_ONE_NAME = Joiner.on(System.getProperty("line.separator")).join(
      "body { font-size: 13px; }", // Use a bigger font if there's only one name
      "h2.familyTitle { font-weight: bold; font-size: 135%; text-transform: uppercase; font-style: italic; }",
      ".lifer { font-weight: bold }",
      ".scientific { font-style: italic }",
      ".main { column-count: 3; -webkit-column-count: 3; -moz-column-count: 3; }",
      ".total { font-size: 110%; font-weight: bold }");
  private final String CSS_STYLE_SINGLE_SIGHTING = Joiner.on(System.getProperty("line.separator")).join(
      "td.common { margin-right: 20px; width: 30% }",
      "td.scientific { margin-right: 20px; width: 30%; font-style: italic; }",
      "td.sighting { margin-left: 20px; width: 40% }");
  private final String CSS_STYLE_SINGLE_SIGHTING_NO_SCI = Joiner.on(System.getProperty("line.separator")).join(
      "th.familyTitle { font-weight: bold; font-size: 135%; padding-top: 10px; text-align: left }",
      "td.common { margin-right: 20px; width: 60% }");
  private final String CSS_STYLE_MULTI_SIGHTING = Joiner.on(System.getProperty("line.separator")).join(
      "td.common { margin-right: 20px; width: 20% }",
      "td.scientific { margin-right: 20px; width: 40%; font-style: italic; }",
      "td.sighting { padding-left: 50px; width: 40% }");
  private final String CSS_STYLE_MULTI_SIGHTING_NO_SCI = Joiner.on(System.getProperty("line.separator")).join(
      "td.common { margin-right: 20px}",
      "td.sighting { padding-left: 50px}");
  
  private String title;
  private final Taxonomy taxonomy;
  private final Location rootLocation;
  private final Alerts alerts;
  private String script;
  private boolean showFamilyTotals;
  private boolean showFamilies;
  private boolean showStatus;
  private ScientificOrCommon scientificOrCommon;
  private int maximumSightings;
  private boolean sortAllSightings;
  private boolean compactOutput;
  private int familyCount;
  private boolean foundAnyEntriesInFamily;
  private boolean omitSpAndHybrid;

  public HtmlOutput(
      Taxonomy taxonomy, Location rootLocation, Alerts alerts) {
    this.taxonomy = taxonomy;
    this.rootLocation = rootLocation;
    this.alerts = alerts;
  }

  public void setShowFamilyTotals(boolean showFamilyTotals) {
    this.showFamilyTotals = showFamilyTotals;
  }

  public void setScript(String script) {
    this.script = script;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void writeSpeciesList(File outFile, QueryResults queryResults,
      Ordering<Sighting> speciesOrdering,
      Ordering<Sighting> sightingOrdering, 
      LocationSet locations) {
    try {
      Writer out = IOUtils.openNewFile(outFile, Charsets.UTF_8);
      writeSpeciesList(out, queryResults, speciesOrdering, sightingOrdering, locations);
      out.close();
    } catch (IOException ioe) {
      FileDialogs.showFileSaveError(alerts, ioe, outFile);
    }
  }

  public void writeSpeciesList(Writer out, QueryResults queryResults,
      Ordering<Sighting> speciesOrdering,
      Ordering<Sighting> sightingOrdering,
      LocationSet locations) throws IOException {
    ResponseWriter rw = new IndentingResponseWriter(new HtmlResponseWriter(out,
        "UTF-8"));

    startHtml(rw, title);

    List<Resolved> taxa = queryResults.getTaxaAsList();
    
    boolean isInTable = false;
    int liferCount = 0;
    
    if (sortAllSightings) {
      TreeMap<Sighting, Resolved> allSightings = queryResults.gatherSortedSightings(
          taxa, speciesOrdering, getSightingsCount());
      int rowCount = 0;
      for (Entry<Sighting, Resolved> entry : allSightings.entrySet()) {
        if (omitSpAndHybrid &&
            (entry.getValue().getType() == SightingTaxon.Type.HYBRID
            || entry.getValue().getType() == SightingTaxon.Type.SP)) {
          continue;
        }
        // Keep family table sizes under control...
        if (rowCount % 100 == 0) {
          if (isInTable) {
            endFamilyTable(rw);
          }
          startFamilyTable(rw);
          isInTable = true;
        }
        
        liferCount = writeSpeciesAndSightingsRows(
            queryResults, locations, rw, liferCount,
            entry.getValue(),
            ImmutableList.of(entry.getKey()));
      }
    } else {
      for (int i = 0; i < taxa.size(); i++) {
        Resolved taxon = taxa.get(i);
        if (omitSpAndHybrid &&
            (taxon.getType() == SightingTaxon.Type.HYBRID
            || taxon.getType() == SightingTaxon.Type.SP)) {
          continue;
        }
  
        if (taxon.getSmallestTaxonType() == Taxon.Type.family) {
          if (isInTable) {
            endFamilyTable(rw);
          }
          
          if (foundAnyEntriesInFamily) {
            familyCount++;
          }
  
          startFamilyTable(rw);
          writeFamilyHeader(rw, taxon.getTaxon(), taxa.subList(i + 1, taxa.size()), queryResults);
          foundAnyEntriesInFamily = false;
          isInTable = true;
        } else {
          if (!isInTable) {
            startFamilyTable(rw);
            isInTable = true;
          }
  
          List<Sighting> sightings = queryResults.getBestSightings(
              taxon, sightingOrdering, getSightingsCount());
          liferCount = writeSpeciesAndSightingsRows(queryResults, locations, rw, liferCount, taxon,
              sightings);
        }
      }
    }

    if (isInTable) {
      endFamilyTable(rw);
    }

    if (foundAnyEntriesInFamily) {
      familyCount++;
    }
    
    int speciesSetSize = queryResults.getCountableSpeciesSize(taxonomy, false);
    rw.startElement("p");
    rw.writeAttribute("class", "total");
    rw.write(Messages.getFormattedMessage(Name.SPECIES_WITH_NUMBER, speciesSetSize));
    
    if (queryResults.getSmallestType() != Taxon.Type.species) {
      int groupsAndSpeciesIds = queryResults.getCountableGroupsAndSpeciesSize(taxonomy, false);
      if (groupsAndSpeciesIds != speciesSetSize) {
        rw.startElement("br");
        rw.endElement("br");
        rw.write(
            Messages.getFormattedMessage(Name.GROUPS_WITH_NUMBER, groupsAndSpeciesIds));
      }
      
      if (queryResults.getSmallestType() == Taxon.Type.subspecies) {
        int subspeciesIds = queryResults.getCountableSubspeciesGroupsAndSpeciesSize(taxonomy, false);
        if (subspeciesIds != groupsAndSpeciesIds) {
          rw.startElement("br");
          rw.endElement("br");
          rw.write(
              Messages.getFormattedMessage(Name.SUBSPECIES_WITH_NUMBER, subspeciesIds));
        }
      }
    }
    
    if (liferCount > 0) {

      Set<?> lifers = queryResults.getTaxaWithAnnotation(taxonomy, QueryAnnotation.LIFER, Taxon.Type.species);
      if (!lifers.isEmpty()) {
        rw.startElement("br");
        rw.endElement("br");
        rw.write(
            Messages.getFormattedMessage(Name.LIFERS_FORMAT_2, lifers.size()));
      }

      if (queryResults.getSmallestType() != Taxon.Type.species) {
        int groupsAndSpeciesCount = queryResults
            .getTaxaWithAnnotation(taxonomy, QueryAnnotation.LIFER, Taxon.Type.group)
            .size();
        if (groupsAndSpeciesCount != lifers.size()) {
          rw.startElement("br");
          rw.endElement("br");
          rw.write(
              Messages.getFormattedMessage(Name.LIFER_GROUPS_WITH_NUMBER, groupsAndSpeciesCount));
        }
          
        if (queryResults.getSmallestType() == Taxon.Type.subspecies) {
          int subspeciesCount = queryResults
              .getTaxaWithAnnotation(taxonomy, QueryAnnotation.LIFER, Taxon.Type.subspecies)
              .size();
          if (subspeciesCount != groupsAndSpeciesCount) {
            rw.startElement("br");
            rw.endElement("br");
            rw.write(
                Messages.getFormattedMessage(Name.LIFER_SUBSPECIES_WITH_NUMBER, subspeciesCount));
          }
        }
      }
    }

    rw.endElement("p");

    if (showFamilies) {
      rw.startElement("p");
      rw.writeAttribute("class", "total");
      if (showFamilyTotals) {
        int possibleFamilies = TaxonUtils.countFamilies(taxonomy,
            queryResults.getChecklist(taxonomy), Checklist.Status.ESCAPED);
        rw.writeText(Messages.getFormattedMessage(
            Name.FAMILIES_OF_FORMAT, familyCount, possibleFamilies));
      } else {
        rw.writeText(Messages.getFormattedMessage(
            Name.FAMILIES_FORMAT, familyCount));
      }
      rw.endElement("p");
    }
    
    rw.startElement("p");
    rw.writeAttribute("class", "total");
    rw.write(Messages.getFormattedMessage(Name.TOTAL_SIGHTINGS_WITH_NUMBER,
        queryResults.getSightingsCount(taxonomy)));
    rw.endElement("p");

    rw.startElement("p");
    rw.write(taxonomy.getName());
    rw.endElement("p");
    
    endHtml(rw);

    rw.flush();
  }

  private int writeSpeciesAndSightingsRows(QueryResults queryResults, LocationSet locations,
      ResponseWriter rw, int liferCount, Resolved taxon, List<Sighting> sightings)
      throws IOException {
    Sighting sighting = (getSightingsCount() == 1) ? sightings.get(0) : null;
    if (compactOutput) {
      rw.startElement("div");
      rw.writeAttribute("class", "species");

      rw.startElement("span");
    } else {
      rw.startElement("tr");
      rw.writeAttribute("class", "species");
  
      rw.startElement("td");
    }
    
    // If any of the sightings are countable, note that the family has a countable entry
    if (!foundAnyEntriesInFamily && queryResults.isCountable(taxon)) {
      foundAnyEntriesInFamily = true;
      
    }
    Optional<QueryAnnotation> annotation = queryResults.getAnnotation(sighting, taxon.getSightingTaxon());
    if (annotation.orNull() == QueryAnnotation.LIFER) {
      rw.writeAttribute("class", "common lifer");
      liferCount++;
    } else {
      rw.writeAttribute("class", "common");
    }
    
    switch (scientificOrCommon) {
      case COMMON_FIRST:
        // Omit the subspecies if it'll appear anyway in the next column
        rw.writeText(taxon.getSimpleCommonName());
        break;
      case COMMON_ONLY:
        // But include it if it won't.
        rw.writeText(taxon.getCommonName());
        break;
      case SCIENTIFIC_FIRST:
      case SCIENTIFIC_ONLY:
        rw.writeText(taxon.getFullName());
        break;
    }
    
    if (sighting != null) {
      rw.writeText(SightingFlags.appendSightingInfo("", sighting));
    }

    // Output if the species is endemic to the current checklist
    if (queryResults.getChecklist(taxonomy) != null) {
      SightingTaxon species;
      if (taxon.getSmallestTaxonType() == Taxon.Type.species) {
        species = taxon.getSightingTaxon();
      } else {
        species = taxon.getParentOfAtLeastType(Taxon.Type.species);
      }
      
      if (queryResults.getChecklist(taxonomy).getStatus(taxon.getTaxonomy(), species) == Checklist.Status.ENDEMIC) {
        rw.writeText(" - "  + Messages.getMessage(Name.CHECKLIST_STATUS_ENDEMIC));
      }
    }

    if (showStatus) {
      Status status = taxon.getTaxonStatus();
      // Take note of species that are VU or worse
      if (status != Species.Status.LC && status != Species.Status.NT) {
        rw.writeText(" ("+ status + ")");
      }
    }
    
    if (compactOutput) {
      rw.endElement("span");
    } else {
      rw.endElement("td");
    }

    if (scientificOrCommon == ScientificOrCommon.COMMON_FIRST
        || scientificOrCommon == ScientificOrCommon.SCIENTIFIC_FIRST) {
      if (compactOutput) {
        rw.startElement("span");
      } else {
        rw.startElement("td");
      }
      rw.writeAttribute("class", "scientific");
      rw.writeText(scientificOrCommon == ScientificOrCommon.SCIENTIFIC_FIRST
          ? taxon.getSimpleCommonName()
          : taxon.getFullName());
      if (compactOutput) {
        rw.endElement("span");
      } else {
        rw.endElement("td");
      }
    }

    if (!compactOutput) {
      if (getSightingsCount() == 1) {
        rw.startElement("td");
        rw.writeAttribute("class", "sighting");
        writeSighting(rw, sighting, locations);
        rw.endElement("td");
      } else if (getSightingsCount() > 0) {
        for (Sighting orderedSighting : sightings) {
          rw.endElement("tr");
          rw.startElement("tr");
          rw.startElement("td");
          rw.writeAttribute("class", "sighting");
          writeSighting(rw, orderedSighting, locations);
          rw.endElement("td");
        }
      }
    }
    
    if (compactOutput) {
      rw.endElement("div");
    } else {
      rw.endElement("tr");
    }
    return liferCount;
  }

  private void startFamilyTable(ResponseWriter rw) throws IOException {
    if (!compactOutput) {
      rw.startElement("table");
      rw.writeAttribute("width", "100%");
    }
  }

  private void writeFamilyHeader(ResponseWriter rw, Taxon family,
      List<Resolved> list, QueryResults queryResults) throws IOException {

    if (!showFamilies) {
      return;
    }

    if (compactOutput) {
      rw.startElement("h2");
    } else {
      rw.startElement("tr");
      rw.startElement("th");
      rw.writeAttribute("colspan", 3);
    }
    rw.writeAttribute("class", "familyTitle");
    
    switch (scientificOrCommon) {
      case COMMON_FIRST:
        rw.writeText(family.getCommonName());
        rw.writeText(" (");
        rw.writeText(family.getName());
        rw.writeText(")");
        break;
      case SCIENTIFIC_FIRST:
        rw.writeText(family.getName());
        rw.writeText(" (");
        rw.writeText(family.getCommonName());
        rw.writeText(")");
        break;
      case SCIENTIFIC_ONLY:
        rw.writeText(family.getName());
        break;
      case COMMON_ONLY:
        rw.writeText(family.getCommonName());
        break;
    }

    // Find the total number of species seen within the family when requested
    if (showFamilyTotals) {
      Set<String> speciesIds = Sets.newHashSet();
      for (Resolved taxonInFamily : list) {
        // Only considered taxa within the family
        if (!taxonInFamily.isChildOf(family)) {
          break;
        }
        
        if (queryResults.isCountable(taxonInFamily)) {
          // ... elevate to a species
          SightingTaxon speciesParent = taxonInFamily.getParentOfAtLeastType(
                  Taxon.Type.species);
          // And ignore sightings that can't be pinned down to a single species
          if (speciesParent.getType() == SightingTaxon.Type.SINGLE) {
            speciesIds.add(speciesParent.getId());
          }
        }
      }

      int speciesInFamily = TaxonUtils
          .countChildren(family, Taxon.Type.species, queryResults.getChecklist(taxonomy));
      rw.writeText(" - ");
      rw.writeText(speciesIds.size());
      rw.writeText("/");
      rw.writeText(speciesInFamily);
    }

    if (compactOutput) {
      rw.endElement("h2");
    } else {
      rw.endElement("th");
      rw.endElement("tr");
    }
  }

  private void endFamilyTable(ResponseWriter rw) throws IOException {
    if (compactOutput) {
    } else {
      rw.endElement("table");
    }
  }

  private void writeSighting(ResponseWriter rw, Sighting sighting,
      LocationSet locations) throws IOException {
    ReadablePartial date = sighting.getDateAsPartial();

    if (sighting.getLocationId() != null) {
      String locationText = LocationIdToString.getString(locations, sighting
          .getLocationId(), true, rootLocation);
      rw.writeText(locationText);
      if (date != null) {
        rw.writeText(' ');
      }
    }

    if (date != null) {
      rw.writeText(PartialIO.toShortUserString(date, Locale.getDefault()));
    }
  }

  private void startHtml(ResponseWriter rw, String title)
      throws IOException {
    rw.startDocument();
    rw.writeComment("saved from url=(0016)http://localhost");
    rw.startElement("html");
    rw.startElement("head");

    rw.startElement("meta");
    rw.writeAttribute("http-equiv", "Content-Type");
    rw.writeAttribute("content", "text/html; charset=UTF-8");
    rw.endElement("meta");
    if (title != null) {
      rw.startElement("title");
      rw.writeText(title);
      rw.endElement("title");
    }

    rw.startElement("style");
    rw.writeAttribute("type", "text/css");
    boolean scientificAndCommon = scientificOrCommon == ScientificOrCommon.COMMON_FIRST
        || scientificOrCommon == ScientificOrCommon.SCIENTIFIC_FIRST;
    
    if (compactOutput) {
      rw.writeText(scientificAndCommon
          ? CSS_STYLE_COMPACT_OUTPUT
          : CSS_STYLE_COMPACT_OUTPUT_WITH_ONE_NAME);
    } else {
      rw.writeText(CSS_STYLE_COMMON);
      rw.writeText(getSightingsCount() > 1
          ? scientificAndCommon
              ? CSS_STYLE_MULTI_SIGHTING : CSS_STYLE_MULTI_SIGHTING_NO_SCI
          : scientificAndCommon
              ? CSS_STYLE_SINGLE_SIGHTING : CSS_STYLE_SINGLE_SIGHTING_NO_SCI);
    }
    rw.endElement("style");
    
    if (script != null) {
      rw.startElement("script");
      rw.writeAttribute("type", "text/javascript");
      rw.writeRawText("function onload_script() {\n");
      rw.writeRawText(script);
      rw.writeRawText("\n}");
      rw.endElement("script");
    }

    rw.endElement("head");
    rw.startElement("body");
    if (script != null) {
      rw.writeAttribute("onload", "onload_script();");
    }
    
    if (compactOutput) {
      rw.startElement("div");
      rw.writeAttribute("class", "main");
    }
  }

  private void endHtml(ResponseWriter rw) throws IOException {
    if (compactOutput) {
      rw.endElement("div");
    }
    rw.endElement("body");
    rw.endElement("html");
    rw.endDocument();
  }

  public void setShowFamilies(boolean showFamilies) {
    this.showFamilies = showFamilies;
  }

  public void setShowStatus(boolean showStatus) {
    this.showStatus = showStatus;
  }

  public void setScientificOrCommon(ScientificOrCommon scientificOrCommon) {
    this.scientificOrCommon = scientificOrCommon;
  }
  
  private int getSightingsCount() {
    return compactOutput ? 1 : maximumSightings;
  }

  public void setSightingsCount(int maximumSightings) {
    this.maximumSightings = maximumSightings;
  }

  public void setSortAllSightings(boolean sortAllSightings) {
    this.sortAllSightings = sortAllSightings;
  }

  public void setCompactOutput(boolean compactPrinting) {
    this.compactOutput = compactPrinting;
  }

  public void setOmitSpAndHybrid(boolean omitSpAndHybrid) {
    this.omitSpAndHybrid = omitSpAndHybrid;
  }
}
