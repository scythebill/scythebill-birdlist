/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;

public class IOUtils {
  /**
   * Opens a file in a desired encoding, deleting an existing file if present.
   * 
   * @throws IOException if the file could not be deleted, created, or opened
   */
  public static Writer openNewFile(File file, Charset encoding) throws IOException {
    if (file.exists()) {
      if (!file.delete()) {
        throw new PermissionException("Couldn't delete existing file " + file);
      }
    }

    if (!file.createNewFile()) {
      throw new PermissionException("Couldn't create file " + file);
    }

    FileOutputStream fos = new FileOutputStream(file);

    return new BufferedWriter(new OutputStreamWriter(fos, encoding));
  }

  @SuppressWarnings("serial")
  private static class PermissionException extends IOException {
    public PermissionException(String message) {
      super(message);
    }
  }
}
