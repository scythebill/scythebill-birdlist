/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.io;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.ss.util.DateFormatConverter;
import org.apache.poi.ss.util.WorkbookUtil;
import org.joda.time.DateTimeFieldType;
import org.joda.time.ReadablePartial;

import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterators;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.query.QueryDefinition.QueryAnnotation;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.query.SightingComparators;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences.ScientificOrCommon;
import com.scythebill.birdlist.model.user.User;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.UserOutput;
import com.scythebill.birdlist.ui.util.LocationIdToString;

/** Writes a query report to an Excel xls file. */
public class ReportXlsOutput {
  private final File file;
  private final QueryResults queryResults;
  private final LocationSet locations;
  private boolean showStatus;
  private boolean showFamilyTotals;
  private Location rootLocation;
  private int sightingsCount = 1;
  private boolean showFamilies;
  private boolean showNotes;
  private boolean sortAllSightings;
  private CellStyle scientificStyle;
  private CellStyle dateStyle;
  private boolean showSpeciesLifers;
  private boolean showGroupLifers;
  private boolean showSspLifers;
  private Set<Resolved> lifers = ImmutableSet.of();
  private Set<Resolved> groupLifers = ImmutableSet.of();
  private Set<Resolved> sspLifers = ImmutableSet.of();
  private ScientificOrCommon scientificOrCommon;
  private UserOutput userOutput = UserOutput.NONE;
  private boolean omitSpAndHybrid = false;
  private boolean showAllTaxonomies = false;
  private CellStyle headerStyle;
  private int countableCount = 0;

  public ReportXlsOutput(
      File file,
      QueryResults queryResults,
      LocationSet locations) {
    this.file = file;
    this.queryResults = queryResults;
    this.locations = locations;
  }

  public void writeSpeciesList(
      Ordering<Sighting> speciesOrdering,
      Ordering<Sighting> sightingOrdering) throws IOException {
    try (Workbook workbook = new HSSFWorkbook()) {
      headerStyle = workbook.createCellStyle();
      Font headerFont = workbook.createFont();
      headerFont.setBold(true);
      headerStyle.setFont(headerFont);
  
      CellStyle familyStyle = workbook.createCellStyle();
      Font familyFont = workbook.createFont();
      familyFont.setBold(true);
      familyStyle.setFont(familyFont);
      
      dateStyle = workbook.createCellStyle();
      String formatPattern = DateFormatConverter.convert(Locale.getDefault(),
          DateFormat.getDateInstance(DateFormat.SHORT));
      short dateFormat = workbook.getCreationHelper().createDataFormat().getFormat(formatPattern);
      dateStyle.setDataFormat(dateFormat);
  
      scientificStyle = workbook.createCellStyle();
      Font scientificFont = workbook.createFont();
      scientificFont.setItalic(true);
      scientificStyle.setFont(scientificFont);
      
      Sheet sheet =
          workbook.createSheet(sanitizeTaxonomyName(queryResults.getTaxonomy().getName()));

      List<Resolved> taxa = queryResults.getTaxaAsList();
      writeTaxa(speciesOrdering, sightingOrdering, headerFont, familyStyle, sheet, queryResults.getTaxonomy(), taxa);
      
      if (showAllTaxonomies) {
        for (Taxonomy taxonomy : queryResults.getIncompatibleTaxonomies()) {
          List<Resolved> incompatibleTaxa = queryResults.getIncompatibleTaxaAsList(
              taxonomy, showFamilies);
          if (!incompatibleTaxa.isEmpty()) {
            sheet = workbook.createSheet(sanitizeTaxonomyName(taxonomy.getName()));
            writeTaxa(speciesOrdering, sightingOrdering, headerFont, familyStyle, sheet,
                taxonomy, incompatibleTaxa);
          }
        }
      }
  
      try (OutputStream out = new BufferedOutputStream(new FileOutputStream(file))) {
        workbook.write(out);
      }
    }
  }

  private String sanitizeTaxonomyName(String name) {
    return WorkbookUtil.createSafeSheetName(name);
  }

  private void writeTaxa(Ordering<Sighting> speciesOrdering, Ordering<Sighting> sightingOrdering, Font headerFont,
      CellStyle familyStyle, Sheet sheet, Taxonomy taxonomy, List<Resolved> taxa) {
    countableCount = 0;
    Row header = sheet.createRow(0);
    int columnNumber = 0;
    
    boolean showLifers = !queryResults.getSightingsWithAnnotation(QueryAnnotation.LIFER).isEmpty();
    showSpeciesLifers = false;
    showGroupLifers = false;
    showSspLifers = false;
    if (showLifers) {
      lifers = queryResults.getTaxaWithAnnotation(taxonomy, QueryAnnotation.LIFER, Taxon.Type.species);
      showSpeciesLifers = !lifers.isEmpty();

      if (queryResults.getSmallestType() != Taxon.Type.species) {
        groupLifers = queryResults
            .getTaxaWithAnnotation(taxonomy, QueryAnnotation.LIFER, Taxon.Type.group);
        showGroupLifers = groupLifers.size() != lifers.size();
        
        if (queryResults.getSmallestType() == Taxon.Type.subspecies) {
          sspLifers = queryResults
              .getTaxaWithAnnotation(taxonomy, QueryAnnotation.LIFER, Taxon.Type.subspecies);
          showSspLifers = groupLifers.size() != sspLifers.size();
        }
      }
    }
   
    if (sortAllSightings) {
      CellUtil.createCell(header, columnNumber++, "#", headerStyle);
    }
    
    switch (scientificOrCommon) {
      case COMMON_FIRST:
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.COMMON_NAME), headerStyle);
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.SCIENTIFIC_NAME), headerStyle);
        break;
      case COMMON_ONLY:
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.COMMON_NAME), headerStyle);
        break;
      case SCIENTIFIC_FIRST:
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.SCIENTIFIC_NAME), headerStyle);
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.COMMON_NAME), headerStyle);
        break;
      case SCIENTIFIC_ONLY:
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.SCIENTIFIC_NAME), headerStyle);
        break;
    }
    
    if (sightingsCount > 0) {
      CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.LOCATION_TEXT), headerStyle);
      CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.DATE_TEXT), headerStyle);
      if (showSpeciesLifers) {
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.LIFER_TEXT), headerStyle);
      }
      if (showGroupLifers) {
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.GROUP_LIFER), headerStyle);
      }
      if (showSspLifers) {
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.SSP_LIFER), headerStyle);
      }
      
      CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.SIGHTING_TEXT), headerStyle);
      if (showNotes) {
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.NOTES_TEXT), headerStyle);
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.NUMBER_TEXT), headerStyle);
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.HEARD_ONLY_TEXT), headerStyle);
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.ADULT_TEXT), headerStyle);
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.IMMATURE_TEXT), headerStyle);
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.FEMALE_TEXT), headerStyle);
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.MALE_TEXT), headerStyle);
      }

      if (userOutput.showAbbreviations()) {
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.OBSERVERS_TEXT), headerStyle);
      }
      if (userOutput.showNames()) {
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.OBSERVER_NAMES), headerStyle);
      }
    } else {
      if (showSpeciesLifers) {
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.LIFER_TEXT), headerStyle);
      }
      if (showGroupLifers) {
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.GROUP_LIFER), headerStyle);
      }
      if (showSspLifers) {
        CellUtil.createCell(header, columnNumber++, Messages.getMessage(Name.SSP_LIFER), headerStyle);
      }
    }
    int rowCount = 1;
    
    if (sortAllSightings) {
      // Gather all sorted sightings
      TreeMap<Sighting, Resolved> allSightings = queryResults.gatherSortedSightings(taxa,
          // Compound with the taxonomic ordering to make the listing deterministic and meaningful for ties
          speciesOrdering.compound(
              SightingComparators.inTaxonomicOrder(taxonomy)),
          sightingsCount);
 
      // Output each sighting
      for (Entry<Sighting, Resolved> entry : allSightings.entrySet()) {
        if (omitSpAndHybrid &&
            (entry.getValue().getType() == SightingTaxon.Type.HYBRID
            || entry.getValue().getType() == SightingTaxon.Type.SP)) {
          continue;
        }
        Iterator<Sighting> sightings = Iterators.singletonIterator(entry.getKey());
        rowCount = writeTaxonAndSightings(
            entry.getValue(), sightings, sheet, rowCount);
      }
    } else {
      for (int i = 0; i < taxa.size(); i++) {
        Resolved taxon = taxa.get(i);
        if (omitSpAndHybrid &&
            (taxon.getType() == SightingTaxon.Type.HYBRID
            || taxon.getType() == SightingTaxon.Type.SP)) {
          continue;
        }
  
        boolean foundFamily = taxon.getSmallestTaxonType() == Taxon.Type.family;
        if (foundFamily && !showFamilies) {
          continue;
        }
        
        // Insert a blank row before starting a new family
        if (foundFamily && rowCount > 1) {
          rowCount++;
        }
        
        int cellColumn = 0;
        if (foundFamily) {
          Row row = sheet.createRow(rowCount++);
          Cell common = row.createCell(cellColumn++);
          // Write the family header
          common.setCellStyle(familyStyle);
          String text = getFamilyTitle(taxon, taxa.subList(i + 1, taxa.size()), queryResults);
          
          common.setCellValue(text);
          sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum(), 0, 1));
        } else {
          // Write the species (and optionally sightings)
          Iterator<Sighting> sightings;
          if (sightingsCount > 0) {
            sightings = queryResults.getBestSightings(
                taxon, sightingOrdering, sightingsCount).iterator();
          } else {
            sightings = Collections.emptyIterator();
          }
          
          rowCount = writeTaxonAndSightings(taxon, sightings, sheet, rowCount);
        }
      }
    }
 
    rowCount++;
    
    int summaryColumn = sortAllSightings ? 1 : 0;
    int speciesSetSize = queryResults.getCountableSpeciesSize(taxonomy,
        /*includeNonSingle=*/false);
    Row row = sheet.createRow(rowCount++);
    Cell cell = row.createCell(summaryColumn);
    cell.setCellValue(textWithNumber(headerFont, Name.SPECIES_WITH_NUMBER, speciesSetSize));
    
    if (queryResults.getSmallestType() != Taxon.Type.species) {
      int groupsAndSpeciesIds = queryResults.getCountableGroupsAndSpeciesSize(taxonomy, false);
      if (groupsAndSpeciesIds != speciesSetSize) {
        row = sheet.createRow(rowCount++);
        cell = row.createCell(summaryColumn);
        cell.setCellValue(textWithNumber(headerFont, Name.GROUPS_WITH_NUMBER, groupsAndSpeciesIds));
      }
      
      if (queryResults.getSmallestType() == Taxon.Type.subspecies) {
        int subspeciesIds = queryResults.getCountableSubspeciesGroupsAndSpeciesSize(taxonomy, false);
        if (subspeciesIds != groupsAndSpeciesIds) {
          row = sheet.createRow(rowCount++);
          cell = row.createCell(summaryColumn);
          cell.setCellValue(textWithNumber(headerFont, Name.SUBSPECIES_WITH_NUMBER, subspeciesIds));
        }
      }
    }
 
    if (showLifers) {
      if (!lifers.isEmpty()) {
        row = sheet.createRow(rowCount++);
        cell = row.createCell(summaryColumn);
        cell.setCellValue(textWithNumber(headerFont,
            Name.LIFERS_FORMAT_2, lifers.size()));
      }
 
      if (queryResults.getSmallestType() != Taxon.Type.species) {
        int groupsAndSpeciesCount = queryResults
            .getTaxaWithAnnotation(taxonomy, QueryAnnotation.LIFER, Taxon.Type.group)
            .size();
        if (groupsAndSpeciesCount != lifers.size()) {
          row = sheet.createRow(rowCount++);
          cell = row.createCell(summaryColumn);
          cell.setCellValue(textWithNumber(headerFont,
              Name.LIFER_GROUPS_WITH_NUMBER, groupsAndSpeciesCount));
        }
          
        if (queryResults.getSmallestType() == Taxon.Type.subspecies) {
          int subspeciesCount = queryResults
              .getTaxaWithAnnotation(taxonomy, QueryAnnotation.LIFER, Taxon.Type.subspecies)
              .size();
          if (subspeciesCount != groupsAndSpeciesCount) {
            row = sheet.createRow(rowCount++);
            cell = row.createCell(summaryColumn);
            cell.setCellValue(textWithNumber(headerFont,
                Name.LIFER_SUBSPECIES_WITH_NUMBER, subspeciesCount));
          }
        }
      }
    }
    
    row = sheet.createRow(rowCount++);
    cell = row.createCell(summaryColumn);
    cell.setCellValue(textWithNumber(headerFont,
        Name.TOTAL_SIGHTINGS_WITH_NUMBER, queryResults.getSightingsCount(taxonomy)));
 
    if (!taxa.isEmpty()) {
      row = sheet.createRow(rowCount++);
      CellUtil.createCell(row, summaryColumn, taxonomy.getName());
    }
    
    if (sortAllSightings) {
      sheet.autoSizeColumn(0);
    }
    boolean commonAndScientific =
        scientificOrCommon.includesCommon() && scientificOrCommon.includesScientific();
    for (int column = 0; column < (commonAndScientific ? 2 : 1) + ((sightingsCount > 0) ? 2 : 0); column++) {
      // I used to autosize these columns, but it's relatively easy (with some wacky hybrid scenarios)
      // to get huge name columns that make it look like the spreadsheet only has those two columns.
      sheet.setColumnWidth(column + (sortAllSightings ? 1 : 0), 30 * 256);
    }
  }
  
  private int writeTaxonAndSightings(
      Resolved taxon,
      Iterator<Sighting> sightings,
      Sheet sheet,
      int rowCount) {
    Taxonomy taxonomy = taxon.getTaxonomy();
    
    Row row = sheet.createRow(rowCount++);
    
    int cellColumn = 0;
    if (sortAllSightings) {
      if (taxon.getType() == SightingTaxon.Type.SINGLE &&
          queryResults.getAllSightings(taxon).stream().anyMatch(queryResults.getCountablePredicate())) {
        Cell countCell = row.createCell(cellColumn);
        countCell.setCellValue("" + (++countableCount));
      }
      cellColumn++;
    }

    Cell firstNameCell = row.createCell(cellColumn++);
    Status status = null;
    if (queryResults.getChecklist(taxonomy) != null) {
      status = queryResults.getChecklist(taxonomy).getStatus(taxonomy, taxon.getSightingTaxon());
    }
    
    // Check if this taxon has a common name
    boolean hasCommonName = taxon.hasCommonName();
    if (!hasCommonName && taxon.getLargestTaxonType() != Taxon.Type.species) {
      // ... or if its species parent has a common name
      Resolved speciesParent = taxon.getParentOfAtLeastType(Taxon.Type.species).resolveInternal(taxonomy);
      hasCommonName = speciesParent.hasCommonName();
    }
    
    String firstText;
    switch (scientificOrCommon) {
      case COMMON_FIRST:
        firstText = hasCommonName ? getCommonNameAtLeastAtGroup(taxon) : "";
        break;
      case COMMON_ONLY:
        firstText = hasCommonName ? taxon.getCommonName() : taxon.getFullName();
        break;
      case SCIENTIFIC_FIRST:
      case SCIENTIFIC_ONLY:
        firstText = taxon.getFullName();
        break;
      default:
        throw new AssertionError(); 
    }
    
    if (status == Status.ENDEMIC) {
      firstText += " (" + Messages.getMessage(Name.CHECKLIST_STATUS_ENDEMIC) + ")";
    }
    
    if (showStatus) {
      Species.Status taxonStatus = taxon.getTaxonStatus();
      if (taxonStatus != Species.Status.LC && taxonStatus != Species.Status.NT) {
        firstText += " - " + taxonStatus.name();
      }
    }
    
    firstNameCell.setCellValue(firstText);
    
    if (scientificOrCommon.includesCommon() && scientificOrCommon.includesScientific()) {
      Cell secondCell = row.createCell(cellColumn++);
      secondCell.setCellStyle(scientificStyle);
      secondCell.setCellValue(scientificOrCommon == ScientificOrCommon.COMMON_FIRST
          ? taxon.getFullName()
          : taxon.hasCommonName() ? getCommonNameAtLeastAtGroup(taxon) : "");
    }

    if (sightingsCount > 0) {
      int sightingColumnStart = cellColumn;
      while (sightings.hasNext()) {
        // Snap back to the first column of sightings
        cellColumn = sightingColumnStart;
        Sighting sighting = sightings.next();
        Cell locationCell = row.createCell(cellColumn++);
        if (sighting.getLocationId() != null) {
          String locationText = LocationIdToString.getString(locations, sighting
              .getLocationId(), true, rootLocation);
          locationCell.setCellValue(locationText);
        }
        
        Cell dateCell = row.createCell(cellColumn++);
        if (sighting.getDateAsPartial() != null) {
          if (shouldWriteAsDateType(sighting.getDateAsPartial())) {
            setDateValue(dateCell, sighting.getDateAsPartial());
          } else {
            dateCell.setCellValue(PartialIO.toShortUserString(sighting.getDateAsPartial(), Locale.getDefault()));
          }
        }
        
        String sightingText = null;
        if (sighting.getSightingInfo().getSightingStatus() != SightingInfo.SightingStatus.NONE) {
          // This is not ideal for uncertain ID.
          sightingText = Messages.getText(sighting.getSightingInfo().getSightingStatus()).toLowerCase();
        }

        // If showNotes == true, then there'll be a whole heard-only column. Otherwise, append heard-only
        // to the status text
        if (!showNotes && sighting.getSightingInfo().isHeardOnly()) {
          if (sightingText == null) {
            sightingText = Messages.getMessage(Name.HEARD_ONLY_TEXT).toLowerCase();
          } else {
            sightingText = sightingText + " (H)";
          }
        }

        boolean wasASpeciesLifer = false;
        if (showSpeciesLifers) {
          if (queryResults.getAnnotation(sighting, taxon.getParentOfAtLeastType(Taxon.Type.species)).orNull()
              == QueryAnnotation.LIFER) {
            row.createCell(cellColumn++).setCellValue("Y");
            wasASpeciesLifer = true;
          } else {
            cellColumn++;
          }
        }

        boolean wasAGroupLifer = false;
        if (showGroupLifers) {
          if (!wasASpeciesLifer
              && queryResults.getAnnotation(sighting, taxon.getParentOfAtLeastType(Taxon.Type.group)).orNull()
                  == QueryAnnotation.LIFER) {
            row.createCell(cellColumn++).setCellValue("Y");
            wasAGroupLifer = true;
          } else {
            cellColumn++;
          }
        }

        if (showSspLifers) {
          if (!wasASpeciesLifer
              && !wasAGroupLifer
              && queryResults.getAnnotation(sighting, taxon.getParentOfAtLeastType(Taxon.Type.subspecies)).orNull()
                  == QueryAnnotation.LIFER) {
            row.createCell(cellColumn++).setCellValue("Y");
          } else {
            cellColumn++;
          }
        }

        String sightingNotes = showNotes ? sighting.getSightingInfo().getDescription() : null;
        
        if (sightingText != null) {
          row.createCell(cellColumn++).setCellValue(sightingText);
        } else if (showNotes) {
          cellColumn++;
        }
        
        if (showNotes) {
          if (sightingNotes != null) {
            row.createCell(cellColumn).setCellValue(sightingNotes);
          }
          cellColumn++;
          
          if (sighting.hasSightingInfo() && sighting.getSightingInfo().getNumber() != null) {
            row.createCell(cellColumn).setCellValue(sighting.getSightingInfo().getNumber().toString());                
          }
          
          cellColumn++;
          
          if (sighting.hasSightingInfo() && sighting.getSightingInfo().isHeardOnly()) {
            row.createCell(cellColumn).setCellValue("Y");                
          }

          cellColumn++;
          
          if (sighting.hasSightingInfo() && sighting.getSightingInfo().isAdult()) {
            row.createCell(cellColumn).setCellValue("Y");                
          }

          cellColumn++;
          
          if (sighting.hasSightingInfo() && sighting.getSightingInfo().isImmature()) {
            row.createCell(cellColumn).setCellValue("Y");                
          }

          cellColumn++;
          
          if (sighting.hasSightingInfo() && sighting.getSightingInfo().isFemale()) {
            row.createCell(cellColumn).setCellValue("Y");                
          }

          cellColumn++;
          
          if (sighting.hasSightingInfo() && sighting.getSightingInfo().isMale()) {
            row.createCell(cellColumn).setCellValue("Y");                
          }
        }
        
        if (userOutput.showAbbreviations()) {
          cellColumn++;
          if (sighting.hasSightingInfo() && !sighting.getSightingInfo().getUsers().isEmpty()) {
            row.createCell(cellColumn).setCellValue(toAbbreviations(sighting.getSightingInfo().getUsers()));            
          }
        }
        if (userOutput.showNames()) {
          cellColumn++;
          if (sighting.hasSightingInfo() && !sighting.getSightingInfo().getUsers().isEmpty()) {
            row.createCell(cellColumn).setCellValue(toNames(sighting.getSightingInfo().getUsers()));            
          }
        }

        if (sightings.hasNext()) {
          row = sheet.createRow(rowCount++);
        }
      }
    } else {
      boolean wasASpeciesLifer = false;
      if (showSpeciesLifers) {
        Resolved species = taxon.getParentOfAtLeastType(Taxon.Type.species).resolveInternal(taxonomy);
        if (lifers.contains(species)) {
          row.createCell(cellColumn++).setCellValue("Y");
          wasASpeciesLifer = true;
        } else {
          cellColumn++;
        }            
      }
      boolean wasAGroupLifer = false;
      if (showGroupLifers) {
        Resolved group = taxon.getParentOfAtLeastType(Taxon.Type.group).resolveInternal(taxonomy);
        if (!wasASpeciesLifer && groupLifers.contains(group)) {
          row.createCell(cellColumn++).setCellValue("Y");
          wasAGroupLifer = true;
        } else {
          cellColumn++;
        }            
      }
      if (showSspLifers) {
        Resolved ssp = taxon.getParentOfAtLeastType(Taxon.Type.subspecies).resolveInternal(taxonomy);
        if (!wasASpeciesLifer && !wasAGroupLifer && sspLifers.contains(ssp)) {
          row.createCell(cellColumn++).setCellValue("Y");
        } else {
          cellColumn++;
        }            
      }
    }
    return rowCount;
  }

  private void setDateValue(Cell dateCell, ReadablePartial dateAsPartial) {
    // A user reported that MacOS Numbers in a Chinese locale completely fails
    // to process these;  so let's just output as ordinary strings there.  A
    // deeper investigation would be desirable;  this is a very lazy solution.
    if (Locale.getDefault().getLanguage().equals(Locale.CHINESE.getLanguage())) {
      String date = PartialIO.toShortUserString(dateAsPartial, Locale.getDefault());
      dateCell.setCellType(CellType.STRING);
      dateCell.setCellValue(date);
      return;
    }
    
    Date date = Date.from(
        LocalDate.of(
            dateAsPartial.get(DateTimeFieldType.year()),
            dateAsPartial.get(DateTimeFieldType.monthOfYear()),
            dateAsPartial.get(DateTimeFieldType.dayOfMonth()))
        .atStartOfDay(ZoneId.systemDefault()).toInstant());
    dateCell.setCellType(CellType.NUMERIC);
    dateCell.setCellStyle(dateStyle);
    dateCell.setCellValue(date);
  }

  /**
   * Return the common name of the taxon, or its group (or species) parent if it's at
   * subspecies level.  Used to prevent doubling up subspecies names.
   */
  private String getCommonNameAtLeastAtGroup(Resolved taxon) {
    if (taxon.getSmallestTaxonType() != Taxon.Type.subspecies) {
      return taxon.getCommonName();
    }
    
    taxon = taxon.getParentOfAtLeastType(Taxon.Type.group).resolveInternal(taxon.getTaxonomy());
    return taxon.getCommonName();
  }
  
  private boolean shouldWriteAsDateType(ReadablePartial dateAsPartial) {
    return dateAsPartial.isSupported(DateTimeFieldType.dayOfMonth())
        && dateAsPartial.isSupported(DateTimeFieldType.monthOfYear())
        && dateAsPartial.isSupported(DateTimeFieldType.year());
  }

  private HSSFRichTextString textWithNumber(
      Font font,
      Name text,
      int number) {
    HSSFRichTextString string = new HSSFRichTextString(
        Messages.getFormattedMessage(text, number));
    string.applyFont(font);
    return string;
  }
  
  private String getFamilyTitle(Resolved taxon, List<Resolved> list, QueryResults queryResults) {
    String text;
    switch (scientificOrCommon) {
      case COMMON_FIRST:
        text = taxon.hasCommonName()
            ? String.format("%s (%s)", taxon.getCommonName(), taxon.getFullName())
            : taxon.getFullName();
        break;
      case SCIENTIFIC_FIRST:
        text = taxon.hasCommonName()
            ? String.format("%s (%s)", taxon.getFullName(), taxon.getCommonName())
            : taxon.getFullName();
        break;
      case SCIENTIFIC_ONLY:
        text = taxon.getFullName();
        break;
      case COMMON_ONLY:
        text = taxon.hasCommonName()
            ? taxon.getCommonName()
            : taxon.getFullName();
        break;
      default:
        throw new AssertionError();
    }

    Taxon family = taxon.getTaxon();
    // Find the total number of species seen within the family when requested
    if (showFamilyTotals) {
      Set<String> speciesIds = Sets.newHashSet();
      for (Resolved taxonInFamily : list) {
        // Only considered taxa within the family
        if (!taxonInFamily.isChildOf(family)) {
          break;
        }

        if (queryResults.isCountable(taxonInFamily)) {
          // ... elevate to a species
          SightingTaxon speciesParent = taxonInFamily.getParentOfAtLeastType(
                  Taxon.Type.species);
          // And ignore sightings that can't be pinned down to a single species
          if (speciesParent.getType() == SightingTaxon.Type.SINGLE) {
            speciesIds.add(speciesParent.getId());
          }
        }
      }

      int speciesInFamily = TaxonUtils
          .countChildren(family, Taxon.Type.species, queryResults.getChecklist(taxon.getTaxonomy()));
      
      text += String.format(" - %s / %s", speciesIds.size(), speciesInFamily);
    }
    return text;
  }

  private static String toAbbreviations(ImmutableSet<User> users) {
    return users.stream()
        .map(User::abbreviation)
        .filter(Predicates.notNull())
        .collect(Collectors.joining(","));
  }

  private static String toNames(ImmutableSet<User> users) {
    return users.stream()
        .map(User::name)
        .filter(Predicates.notNull())
        .collect(Collectors.joining("\n"));
  }

  public void setShowStatus(boolean showStatus) {
    this.showStatus = showStatus;
  }

  public void setShowFamilyTotals(boolean showFamilyTotals) {
    this.showFamilyTotals = showFamilyTotals;
  }

  public void setRootLocation(Location rootLocation) {
    this.rootLocation = rootLocation;
  }

  public void setSightingsCount(int sightingsCount) {
    this.sightingsCount = sightingsCount;
  }

  public void setShowFamilies(boolean showFamilies) {
    this.showFamilies = showFamilies;
  }

  public void setShowNotes(boolean showNotes) {
    this.showNotes = showNotes;
  }

  public void setSortAllSightings(boolean sortAllSightings) {
    this.sortAllSightings = sortAllSightings;
  }

  public void setScientificOrCommon(ScientificOrCommon scientificOrCommon) {
    this.scientificOrCommon = scientificOrCommon;
    
  }

  public void setUserOutput(UserOutput userOutput) {
    this.userOutput = userOutput;
  }

  public void setOmitSpAndHybrid(boolean omitSpAndHybrid) {
    this.omitSpAndHybrid = omitSpAndHybrid;    
  }

  public void setShowAllTaxonomies(boolean showAllTaxonomies) {
    this.showAllTaxonomies = showAllTaxonomies;
  }
}
