/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.io;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayDeque;
import java.util.ArrayList;

import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.scythebill.birdlist.model.io.ResponseWriter;

/**
 * Takes HTML ResponseWriter and echos it to plain text of sorts.
 */
class TeeToPlainTextResponseWriter extends ResponseWriter {
  private final StringBuilder plainTextBuffer = new StringBuilder();
  private final ResponseWriter inner;
  private final ArrayDeque<Object> listDeque = new ArrayDeque<>();
  private String lineIndent = "";
  private enum UnorderedList { element };
  private ArrayList<ArrayList<StringBuilder>> currentTable = null;

  public TeeToPlainTextResponseWriter(ResponseWriter inner) {
    this.inner = inner;
  }

  public String getPlainText() {
    return plainTextBuffer.toString();
  }

  @Override
  public void startDocument() throws IOException {
    inner.startDocument();
  }

  @Override
  public void endDocument() throws IOException {
    inner.endDocument();
  }

  @Override
  public void writeAttribute(String name, Object value) throws IOException {
    inner.writeAttribute(name, value);
  }

  @Override
  public void writeComment(Object comment) throws IOException {
    inner.writeComment(comment);
  }

  @Override
  public void writeText(char[] buffer, int offset, int length) throws IOException {
    inner.writeText(buffer, offset, length);
    writeToBuffer(buffer, offset, length);
  }

  @Override
  public void writeText(Object text) throws IOException {
    inner.writeText(text);
    writeToBuffer(text.toString());
  }

  @Override
  public void startElement(String name, Object objectFor) throws IOException {
    inner.startElement(name, objectFor);
    if (name.equals("br")) {
      newLineWithIndent();
    } else if (name.equals("ul")) {
      listDeque.push(UnorderedList.element);
      updateLineIndent();
      newLineWithIndent();
    } else if (name.equals("ol")) {
      listDeque.push(0);
      updateLineIndent();
      newLineWithIndent();
    } else if (name.equals("li")) {
      Object peek = listDeque.peek();
      if (peek == UnorderedList.element) {
        writeToBuffer("- ");
      } else {
        int elementCount = ((int) peek) + 1;
        listDeque.pop();
        listDeque.push(elementCount);
        writeToBuffer(String.format("%d - ", elementCount));
      }
    } else if (name.equals("th") || name.equals("td")) {
      if (currentTable == null) {
        throw new IllegalStateException("Not in table");
      }
      if (currentTable.isEmpty()) {
        throw new IllegalStateException("Not in table row");
      }
      // Move to the next column
      Iterables.getLast(currentTable).add(new StringBuilder());
    } else if (name.equals("tr")) {
      if (currentTable == null) {
        throw new IllegalStateException("Not in a table");
      }
      currentTable.add(new ArrayList<>());
    } else if (name.equals("table")) {
      currentTable = new ArrayList<>();
    }
  }

  @Override
  public void endElement(String name) throws IOException {
    inner.endElement(name);
    if (name.equals("p") || name.equals("div")) {
      writeToBuffer(System.lineSeparator());
      writeToBuffer(System.lineSeparator());
    } else if (name.equals("ul") || name.equals("ol")) {
      writeToBuffer(System.lineSeparator());
      listDeque.pop();
      updateLineIndent();
    } else if (name.equals("li")) {
      newLineWithIndent();
    } else if (name.equals("table")) {
      if (!currentTable.isEmpty()) {
        ArrayList<ArrayList<StringBuilder>> previousTable = currentTable;
        currentTable = null;
        int columnCount = previousTable.stream().mapToInt(ArrayList::size).max().getAsInt();
        if (columnCount > 0) {
          int[] columnWidth = new int[columnCount];
          for (ArrayList<StringBuilder> row : previousTable) {
            for (int i = 0; i < row.size(); i++) {
              StringBuilder cell = row.get(i);
              columnWidth[i] = Math.max(columnWidth[i], cell.length());
            }
          }

          for (ArrayList<StringBuilder> row : previousTable) {
            newLineWithIndent();
            for (int i = 0; i < row.size(); i++) {
              StringBuilder cell = row.get(i);
              writeToBuffer(cell.toString());
              writeToBuffer(Strings.repeat(" ", 2 + columnWidth[i] - cell.length()));
            }
          }
        }
      }
      writeToBuffer(System.lineSeparator());
      writeToBuffer(System.lineSeparator());
    }
  }

  @Override
  public ResponseWriter cloneWithWriter(Writer writer) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void writeRawText(String text) throws IOException {
    inner.writeRawText(text);
    writeToBuffer(text);

  }

  @Override
  public void write(char[] cbuf, int off, int len) throws IOException {
    inner.write(cbuf, off, len);
    writeToBuffer(cbuf, off, len);
  }

  @Override
  public void flush() throws IOException {
    inner.flush();
  }

  @Override
  public void close() throws IOException {
    inner.close();
  }

  private void updateLineIndent() {
    lineIndent = Strings.repeat("  ", listDeque.size());
  }
  
  private void newLineWithIndent() {
    writeToBuffer(System.lineSeparator());
    writeToBuffer(lineIndent);
  }
  
  private void writeToBuffer(String s) {
    if (currentTable != null && !currentTable.isEmpty()) {
      ArrayList<StringBuilder> row = Iterables.getLast(currentTable);
      if (!row.isEmpty()) {
        Iterables.getLast(row).append(s);
        return;
      }
    }
    plainTextBuffer.append(s);    
  }

  private void writeToBuffer(char[] cbuf, int off, int len) {
    if (currentTable != null && !currentTable.isEmpty()) {
      ArrayList<StringBuilder> row = Iterables.getLast(currentTable);
      if (!row.isEmpty()) {
        Iterables.getLast(row).append(cbuf, off, len);
        return;
      }
    }
    plainTextBuffer.append(cbuf, off, len);    
  }
}
