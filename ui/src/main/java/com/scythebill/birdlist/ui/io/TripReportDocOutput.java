/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.io;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import org.joda.time.DateTimeFieldType;
import org.joda.time.LocalTime;
import org.joda.time.ReadablePartial;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.common.base.CharMatcher;
import com.google.common.base.Strings;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.primitives.Ints;
import com.scythebill.birdlist.model.io.HtmlResponseWriter;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.io.ResponseWriter;
import com.scythebill.birdlist.model.io.TimeIO;
import com.scythebill.birdlist.model.query.QueryDefinition.QueryAnnotation;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Photo;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.sighting.VisitInfoKeyOrdering;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences.ScientificOrCommon;
import com.scythebill.birdlist.model.util.Abbreviations;
import com.scythebill.birdlist.model.util.Abbreviations.AbbreviationConfig;
import com.scythebill.birdlist.ui.util.LocationIdToString;

/**
 * Writes a trip report as HTML.
 */
public class TripReportDocOutput {
  private static final int MAX_LOCATION_IDS_TO_LIST = 3;
  private final QueryResults queryResults;
  private final ReportSet reportSet;
  private final SortedSet<VisitInfoKey> visits;
  private final Location rootLocation;
  private ScientificOrCommon scientificOrCommon = ScientificOrCommon.COMMON_FIRST;
  private BiMap<String, String> locationIdToAbbreviation;
  private boolean writeItinerary = true;
  private boolean writeSpeciesTable = true;
  private boolean writeSpeciesList = true;
  private boolean allDatesAreTheSameYear;
  private DateTimeFormatter noYearFormatter;
  private String name;
  private Multimap<Resolved, Resolved> taxaBySpecies;
  private boolean queryHasDateRestriction;
  private boolean includeFavoritePhotos;
  private boolean showAllTaxonomies;

  public TripReportDocOutput(QueryResults queryResults, ReportSet reportSet,
      Location rootLocation) {
    this.queryResults = queryResults;
    this.reportSet = reportSet;
    this.rootLocation = rootLocation;
    this.visits = new TreeSet<VisitInfoKey>(new VisitInfoKeyOrdering());
    this.visits.addAll(queryResults.getAllVisitInfoKeys());
    this.locationIdToAbbreviation = computeLocationAbbreviations();
    this.allDatesAreTheSameYear =
        visits.stream().mapToInt(TripReportDocOutput::toYear).distinct().count() == 1;
    this.noYearFormatter = noYearFormatter();
    this.taxaBySpecies = gatherTaxaIntoSpecies(queryResults.getTaxaAsList());
  }

  public void setWriteItinerary(boolean writeItinerary) {
    this.writeItinerary = writeItinerary;
  }

  public void setWriteSpeciesTable(boolean writeSpeciesTable) {
    this.writeSpeciesTable = writeSpeciesTable;
  }

  public void setWriteSpeciesList(boolean writeSpeciesList) {
    this.writeSpeciesList = writeSpeciesList;
  }

  public void setIncludeFavoritePhotos(boolean includeFavoritePhotos) {
    this.includeFavoritePhotos = includeFavoritePhotos;
  }

  public void setShowAllTaxonomies(boolean showAllTaxonomies) {
    this.showAllTaxonomies = showAllTaxonomies;
  }
  
  public void setScientificOrCommon(ScientificOrCommon scientificOrCommon) {
    this.scientificOrCommon = scientificOrCommon;
  }

  public void setName(String name) {
    this.name = name;
  }
  
  public void setQueryHasDateRestriction(boolean queryHasDateRestriction) {
    this.queryHasDateRestriction = queryHasDateRestriction;
  }

  public String writeReport(Writer writer) throws IOException {
    TeeToPlainTextResponseWriter out = new TeeToPlainTextResponseWriter(
        new HtmlResponseWriter(new BufferedWriter(writer), "UTF-8"));
    out.startDocument();
    out.startElement("document");
    out.startElement("body");

    if (!Strings.isNullOrEmpty(name)) {
      out.startElement("h1");
      out.writeAttribute("style", "text-align: center");
      out.writeText(name);
     
      if (allDatesAreTheSameYear && !queryHasDateRestriction) {
        out.writeText(" ");
        out.writeText(toYear(visits.first()));
      }
      out.endElement("h1");
    }

    out.startElement("div");

    if (writeItinerary) {
      writeItinerary(out);
    }
    if (writeSpeciesTable) {
      writeSpeciesTable(out);
    }
    if (writeSpeciesList) {
      writeSpeciesList(out);
    }

    out.endElement("div");
    out.endElement("body");
    out.endElement("document");
    out.endDocument();
    out.flush();
    
    return out.getPlainText();
  }

  private void writeItinerary(ResponseWriter out) throws IOException {
    out.startElement("ul");
    Set<String> documentedLocationIds = new LinkedHashSet<>();
    
    Multimap<ReadablePartial, VisitInfoKey> infoKeysByDate = Multimaps.<ReadablePartial, VisitInfoKey>newMultimap(
        new LinkedHashMap<ReadablePartial, Collection<VisitInfoKey>>(),
        ArrayList<VisitInfoKey>::new);
    for (VisitInfoKey visit : visits) {
      infoKeysByDate.put(visit.date(), visit);
    }
    List<ReadablePartial> dates = new ArrayList<>(infoKeysByDate.keySet());
    
    // Re-sort the visit info keys to be a bit smarter about visits with no time,
    // which are generally rather arbitrarily sorted.
    // Specifically, we want to take visits with no time and move them to the *start* if
    // they're at the same location as one on the previous day, at the *end* if they're
    // at the same location as on the next day, and arbitrarily if neither are true.
    // And all the visits with a start time go in the middle
    List<VisitInfoKey> optimizedSort = new ArrayList<>();
    for (int i = 0; i < dates.size(); i++) {
      Collection<VisitInfoKey> keysForCurrentDate = infoKeysByDate.get(dates.get(i));
      if (keysForCurrentDate.size() == 1) {
        optimizedSort.addAll(keysForCurrentDate);
        continue;
      }
      
      Collection<VisitInfoKey> keysForPreviousDate =
          i > 0 ? infoKeysByDate.get(dates.get(i - 1)) : ImmutableSet.of();
      ImmutableSet<String> locationsForPreviousDate = keysForPreviousDate.stream()
          .map(key -> key.locationId()).collect(ImmutableSet.toImmutableSet());
      Collection<VisitInfoKey> keysForNextDate =
          i < dates.size() - 1 ? infoKeysByDate.get(dates.get(i + 1)) : ImmutableSet.of();
      ImmutableSet<String> locationsForNextDate = keysForNextDate.stream()
          .map(key -> key.locationId()).collect(ImmutableSet.toImmutableSet());
      
      List<VisitInfoKey> keysForCurrentWithNoTime =
          keysForCurrentDate.stream()
          .filter(key -> !key.startTime().isPresent())
          .collect(Collectors.toCollection(ArrayList::new));
      // All the "middle" keys - keys that have an explicit time (and later, keys
      // that have none but are not found in previous or later days)
      ArrayList<VisitInfoKey> middleKeys =
          keysForCurrentDate.stream()
          .filter(key -> key.startTime().isPresent())
          .collect(Collectors.toCollection(ArrayList::new));
      List<VisitInfoKey> endKeys = new ArrayList<>();
      
      for (VisitInfoKey key : keysForCurrentWithNoTime) {
        if (locationsForPreviousDate.contains(key.locationId())) {
          optimizedSort.add(key);
        } else if (locationsForNextDate.contains(key.locationId())) {
          endKeys.add(key);
        } else {
          middleKeys.add(key);
        }
      }
      
      optimizedSort.addAll(middleKeys);
      optimizedSort.addAll(endKeys);
    }
    
    for (int i = 0; i < optimizedSort.size(); i++) {
      VisitInfoKey visit = optimizedSort.get(i);
      VisitInfo visitInfo = reportSet.getVisitInfo(visit);
      
      // Look for later visits at the same location, instead of
      // giving each one its own list item - but only if there's no comments.
      VisitInfoKey endVisitKey = null;
      int endVisitIndex = -1;
      if (!visitInfoComments(visitInfo).isPresent()) {
        for (int j = i + 1; j < optimizedSort.size(); j++) {
          VisitInfoKey laterVisit = optimizedSort.get(j);
          if (laterVisit.locationId().equals(visit.locationId())
              && !visitInfoComments(reportSet.getVisitInfo(laterVisit)).isPresent()) {
            endVisitKey = laterVisit;
            endVisitIndex = j;
          } else {
            break;
          }
        }
      }
      
      out.startElement("li");
      ReadablePartial date = visit.date();
      out.write(dateToString(date));
      if (visit.startTime().isPresent()) {
        out.writeText(" ");
        out.writeText(TimeIO.toShortUserString(visit.startTime().get(), Locale.getDefault()));
      }
      
      // If the location visit continues through later dates, add them
      if (endVisitKey != null) {
        out.writeText(" - ");
        out.write(dateToString(endVisitKey.date()));
        // And go through the *end* time (including duration if present)
        if (endVisitKey.startTime().isPresent()) {
          out.writeText(" ");
          LocalTime endTime = endVisitKey.startTime().get();
          VisitInfo endVisitInfo = reportSet.getVisitInfo(endVisitKey);
          if (endVisitInfo != null && endVisitInfo.duration().isPresent()) {
            endTime = endTime
                .plusSeconds(Ints.saturatedCast(endVisitInfo.duration().get().getStandardSeconds()));
          }
          
          out.writeText(TimeIO.toShortUserString(endTime, Locale.getDefault()));
        }
        i = endVisitIndex;
      } else if (visit.startTime().isPresent()) {
        // Add the end time, if duration is present
        if (visitInfo != null && visitInfo.duration().isPresent()) {
          LocalTime endTime = visit.startTime().get()
              .plusSeconds(Ints.saturatedCast(visitInfo.duration().get().getStandardSeconds()));
          out.writeText(" - ");
          out.writeText(TimeIO.toShortUserString(endTime, Locale.getDefault()));          
        }
      }

      out.writeText(": ");
      out.writeText(locationName(visit));
      // If there's a species list, then use the itinerary to document
      // the locations.
      if (writeSpeciesList && !documentedLocationIds.contains(visit.locationId())
          && locationIdToAbbreviation.containsKey(visit.locationId())) {
        documentedLocationIds.add(visit.locationId());
        out.writeText(" (");
        out.writeText(locationIdToAbbreviation.get(visit.locationId()));
        out.writeText(")");
      }
      
      Optional<String> comments = visitInfoComments(visitInfo);
      if (comments.isPresent()) {
        out.startElement("br");
        out.endElement("br");
        out.writeText(comments.get());
      }

      out.endElement("li");
    }
    out.endElement("ul");
  }

  private Optional<String> visitInfoComments(VisitInfo visitInfo) {
    if (visitInfo == null) {
      return Optional.empty();
    }
    
    if (!visitInfo.comments().isPresent()) {
      return Optional.empty();
    }
    
    String comments = visitInfo.comments().get();
    // Trim out the eBird "Submitted from" comments, which aren't helpful here.
    int indexOfSubmittedFrom = comments.indexOf("<br />Submitted from");
    if (indexOfSubmittedFrom >= 0) {
      comments = comments.substring(0, indexOfSubmittedFrom);
    }
     
    comments = CharMatcher.whitespace().trimFrom(comments);
    if (comments.isEmpty()) {
      return Optional.empty();
    }
    
    return Optional.of(comments);
  }
  
  private void writeSpeciesTable(ResponseWriter out) throws IOException {
    ImmutableList<ReadablePartial> dates =
        visits.stream().map(VisitInfoKey::date).distinct().collect(ImmutableList.toImmutableList());
    out.startElement("table");
    out.writeAttribute("border", 1);
    out.writeAttribute("cellspacing", 0);

    // Header row
    out.startElement("tr");

    out.startElement("th");
    out.endElement("th");

    if (!writeSpeciesList && (scientificOrCommon == ScientificOrCommon.COMMON_FIRST
        || scientificOrCommon == ScientificOrCommon.SCIENTIFIC_FIRST)) {
      // Need a second column
      out.startElement("th");
      out.endElement("th");
    }

    for (ReadablePartial date : dates) {
      out.startElement("th");
      out.writeText(dateToString(date));
      out.endElement("th");
    }
    out.endElement("tr");

    for (Resolved taxon : queryResults.getTaxaAsList()) {
      if (taxon.getSmallestTaxonType() == Taxon.Type.family) {
        continue;
      }

      out.startElement("tr");
      switch (scientificOrCommon) {
        case COMMON_FIRST:
          out.startElement("th");
          out.writeAttribute("style", "text-align:left");
          out.writeText(taxon.getCommonName());
          out.endElement("th");
          if (writeSpeciesList) {
            break;
          }
        case SCIENTIFIC_ONLY:
          out.startElement("th");
          out.writeAttribute("style", "text-align:left");
          out.startElement("i");
          out.writeText(taxon.getFullName());
          out.endElement("i");
          out.endElement("th");
          break;
        case SCIENTIFIC_FIRST:
          out.startElement("th");
          out.writeAttribute("style", "text-align:left");
          out.startElement("i");
          out.writeText(taxon.getFullName());
          out.endElement("i");
          out.endElement("th");
          if (writeSpeciesList) {
            break;
          }
        case COMMON_ONLY:
          out.startElement("th");
          out.writeAttribute("style", "text-align:left");
          out.writeText(taxon.getCommonName());
          out.endElement("th");
          break;
      }

      Multimap<ReadablePartial, Sighting> sightingsByDate =
          Multimaps.index(queryResults.getAllSightings(taxon).stream()
              .filter(s -> s.getDateAsPartial() != null).iterator(), Sighting::getDateAsPartial);
      
      for (ReadablePartial date : dates) {
        out.startElement("td");
        out.writeText(textForSightings(sightingsByDate.get(date)));
        out.endElement("td");
      }
      out.endElement("tr");
    }

    out.endElement("table");
  }

  private void writeSpeciesList(ResponseWriter out) throws IOException {
    Set<String> usedAbbreviations = new LinkedHashSet<>();
    VisitInfoKeyOrdering visitInfoKeyOrdering = new VisitInfoKeyOrdering();
    out.startElement("ol");
    // TODO: want to raise everything to species level (for the purposes of the list,
    // but then include groups?)
    for (Resolved species : taxaBySpecies.keySet()) {
      out.startElement("li");

      Collection<Resolved> taxaInSpecies = taxaBySpecies.get(species);
      if (taxaInSpecies.size() == 1) {
        Resolved taxon = taxaInSpecies.iterator().next();
        writeTaxonListText(out, taxon);
        writeTaxonListSightingText(
            out,
            visitInfoKeyOrdering,
            taxon,
            queryResults.getAllSightings(taxon),
            usedAbbreviations);
      } else {
        writeTaxonListText(out, species);
        out.startElement("ul");
        for (Resolved taxonInSpecies : taxaInSpecies) {
          out.startElement("li");
          writeAbbreviatedTaxonListText(out, species, taxonInSpecies);
          writeTaxonListSightingText(
              out,
              visitInfoKeyOrdering,
              taxonInSpecies,
              queryResults.getAllSightings(taxonInSpecies),
              usedAbbreviations);
          out.endElement("li");
        }
        out.endElement("ul");
      }

      out.endElement("li");
    }
    out.endElement("ol");

    if (showAllTaxonomies) {
      for (Taxonomy taxonomy : queryResults.getIncompatibleTaxonomies()) {
        out.startElement("br");
        out.endElement("br");
        
        out.startElement("h2");
        out.writeText(taxonomy.getName());
        out.endElement("h2");
        
        out.startElement("ol");
        for (Resolved taxon : queryResults.getIncompatibleTaxaAsList(
            taxonomy, /*showFamillies=*/false)) {
          out.startElement("li");
          writeTaxonListText(out, taxon);
          writeTaxonListSightingText(
              out,
              visitInfoKeyOrdering,
              taxon,
              queryResults.getAllSightings(taxon),
              usedAbbreviations);
          out.endElement("li");
          
        }
        out.endElement("ol");
      }
    }
    
    // If no itinerary, then there needs to be a location-to-abbreviation table.
    if (!writeItinerary) {
      out.startElement("p");
      SortedSet<String> abbreviations = new TreeSet<>(usedAbbreviations);
      out.writeText(abbreviations.stream().map(abbreviation -> {
        return String.format("%s - %s", abbreviation,
            locationName(locationIdToAbbreviation.inverse().get(abbreviation)));
      }).collect(Collectors.joining(", ")));

      out.endElement("p");
    }
  }

  private void writeTaxonListSightingText(ResponseWriter out,
      VisitInfoKeyOrdering visitInfoKeyOrdering, Resolved taxon,
      Collection<Sighting> taxonSightings,
      Set<String> usedAbbreviations) throws IOException {
    ImmutableSet<String> locationIds = taxonSightings.stream().map(VisitInfoKey::forSighting)
        .filter(key -> key != null).sorted(visitInfoKeyOrdering).map(VisitInfoKey::locationId)
        .filter(locationIdToAbbreviation::containsKey).collect(ImmutableSet.toImmutableSet());
    if (locationIds.size() <= MAX_LOCATION_IDS_TO_LIST) {
      out.writeText(" ");
      boolean first = true;
      for (String locationId : locationIds) {
        ImmutableList<Sighting> locationSightings =
            taxonSightings.stream().filter(s -> locationId.equals(s.getLocationId()))
                .collect(ImmutableList.toImmutableList());
        if (!first) {
          out.writeText(" , ");
        }
        first = false;
        String abbreviation = locationIdToAbbreviation.get(locationId);
        usedAbbreviations.add(abbreviation);
        out.writeText(abbreviation);

        if (Iterables.all(locationSightings, TripReportDocOutput::isHeardOnly)) {
          out.writeText(" (H)");
        }

        // If there's just one location, then output sighting info immediately,
        // no need for an abbreviation.
        // TODO: find a decent way to format sighting info for multiple locations.
        if (locationIds.size() == 1) {
          for (Sighting sighting : locationSightings) {
            if (sighting.hasSightingInfo()
                && !Strings.isNullOrEmpty(sighting.getSightingInfo().getDescription())) {
              out.startElement("br");
              out.endElement("br");
              out.writeText(sighting.getSightingInfo().getDescription());
            }
          }
        }
      }
      
      // If more than one location, do a second pass to output sighting info
      if (locationIds.size() > 1) {
        for (String locationId : locationIds) {
          ImmutableList<Sighting> locationSightings =
              taxonSightings.stream().filter(s -> locationId.equals(s.getLocationId()))
                  .collect(ImmutableList.toImmutableList());
          String abbreviation = locationIdToAbbreviation.get(locationId);
          // Output sighting info associated with the locations.
          for (Sighting sighting : locationSightings) {
            if (sighting.hasSightingInfo()
                && !Strings.isNullOrEmpty(sighting.getSightingInfo().getDescription())) {
              // TODO: include date/time as well if locationSightings has more than one entry?
              out.startElement("br");
              out.endElement("br");
              out.writeText(abbreviation);
              out.writeText(": ");
              out.writeText(sighting.getSightingInfo().getDescription());
            }
          }          
        }
        
      }
    }
    
    if (includeFavoritePhotos) {
      ImmutableList<Photo> favoriteFilePhotos = taxonSightings.stream().filter(Sighting::hasSightingInfo)
          .map(Sighting::getSightingInfo).map(SightingInfo::getPhotos)
          .flatMap(List::stream)
          .filter(Photo::isFileBased)
          .filter(Photo::isFavorite)
          .collect(ImmutableList.toImmutableList());
      for (Photo photo : favoriteFilePhotos) {
        try {
          BufferedImage image = ImageIO.read(photo.toFile());
          // Will be null if ImageIO could not parse it into a file
          if (image != null) {
            int width = image.getWidth();
            int height = image.getHeight();  
            int largest = Math.max(width, height);
            
            out.startElement("br");
            out.endElement("br");
            out.startElement("img");
            out.writeAttribute("src", photo.getUri().toASCIIString());
            int maxSize = 450;
            if (largest > maxSize) {
              out.writeAttribute("width", (int) (width * (((double) maxSize) / largest)));
              out.writeAttribute("height", (int) (height * (((double) maxSize) / largest)));
            }
            out.endElement("img");
          }
        } catch (IOException e) {
          // Skip
        }
      }
    }
  }

  private void writeTaxonListText(ResponseWriter out, Resolved taxon) throws IOException {
    boolean isLifer = queryResults.containsAnnotation(QueryAnnotation.LIFER, taxon);
    if (isLifer) {
      out.startElement("b");
    }
    
    switch (scientificOrCommon) {
      case COMMON_FIRST:
        out.writeText(getCommonNameAtLeastAtGroup(taxon));
        out.writeText(" (");
        out.startElement("i");
        out.writeText(taxon.getFullName());
        out.endElement("i");
        out.writeText(")");
        break;

      case COMMON_ONLY:
        out.writeText(taxon.getCommonName());
        break;

      case SCIENTIFIC_FIRST:
        out.writeText(taxon.getFullName());
        out.writeText(" - ");
        out.startElement("i");
        out.writeText(getCommonNameAtLeastAtGroup(taxon));
        out.endElement("i");
        break;

      case SCIENTIFIC_ONLY:
        out.writeText(taxon.getFullName());
        break;
    }

    if (isLifer) {
      out.endElement("b");
    }
  }

  /**
   * Return the common name of the taxon, or its group (or species) parent if it's at
   * subspecies level.  Used to prevent doubling up subspecies names.
   */
  private String getCommonNameAtLeastAtGroup(Resolved taxon) {
    if (taxon.getSmallestTaxonType() != Taxon.Type.subspecies) {
      return taxon.getCommonName();
    }
    
    taxon = taxon.getParentOfAtLeastType(Taxon.Type.group).resolveInternal(taxon.getTaxonomy());
    return taxon.getCommonName();
  }
  
  private void writeAbbreviatedTaxonListText(ResponseWriter out, Resolved species,
      Resolved taxonInSpecies) throws IOException {
    boolean isLifer = queryResults.containsAnnotation(QueryAnnotation.LIFER, taxonInSpecies);
    if (isLifer) {
      out.startElement("b");
    }
    
    switch (scientificOrCommon) {
      case COMMON_FIRST:
        if (!taxonInSpecies.hasCommonName()) {
          out.writeText(taxonInSpecies.getName());
        } else {
          out.writeText(abbreviatedCommonName(species, taxonInSpecies));
          out.writeText(" (");
          out.startElement("i");
          out.writeText(taxonInSpecies.getName());
          out.endElement("i");
          out.writeText(")");
        }
        break;

      case COMMON_ONLY:
        out.writeText(abbreviatedCommonName(species, taxonInSpecies));
        break;

      case SCIENTIFIC_FIRST:
        out.writeText(taxonInSpecies.getName());
        if (!taxonInSpecies.hasCommonName()) {
          out.writeText(" - ");
          out.startElement("i");
          out.writeText(abbreviatedCommonName(species, taxonInSpecies));
          out.endElement("i");
        }
        break;

      case SCIENTIFIC_ONLY:
        out.writeText(taxonInSpecies.getName());
        break;
    }

    if (isLifer) {
      out.endElement("b");
    }
  }

  private String abbreviatedCommonName(Resolved species, Resolved taxonInSpecies) {
    String speciesCommon = species.getCommonName();
    String name = taxonInSpecies.getCommonName();
    if (name.startsWith(speciesCommon)) {
      name = name.substring(speciesCommon.length());
      name = name.trim();
    }

    return name;
  }

  private Multimap<Resolved, Resolved> gatherTaxaIntoSpecies(Collection<Resolved> taxa) {
    LinkedHashMultimap<Resolved, Resolved> multimap = LinkedHashMultimap.create(taxa.size(), 2);
    for (Resolved taxon : taxa) {
      if (!taxon.getSmallestTaxonType().isAtOrLowerLevelThan(Taxon.Type.species)) {
        continue;
      }

      if (taxon.getSmallestTaxonType() == Taxon.Type.species) {
        multimap.put(taxon, taxon);
      } else {
        Resolved species =
            taxon.getParentOfAtLeastType(Taxon.Type.species).resolveInternal(taxon.getTaxonomy());
        multimap.put(species, taxon);
      }
    }
    return multimap;
  }

  static AbbreviationConfig abbreviationConfig = new AbbreviationConfig();
  static {
    abbreviationConfig.ignoredCharacters =
        // Ignore punctuation
        CharMatcher.anyOf("-/.,()'\"")
        // and drop numbers too
        .or(CharMatcher.inRange('0', '9'));
    abbreviationConfig.maxPerWordChars = 4;
    abbreviationConfig.maxWords = 5;
  }
  
  private BiMap<String, String> computeLocationAbbreviations() {
    BiMap<String, String> locationIdToAbbreviation = HashBiMap.create();
    for (VisitInfoKey visit : visits) {
      String locationId = visit.locationId();
      if (!locationIdToAbbreviation.containsKey(locationId)) {
        Optional<String> abbreviation = Abbreviations.abbreviate(locationName(visit),
            locationIdToAbbreviation::containsValue, abbreviationConfig);
        if (abbreviation.isPresent()) {
          locationIdToAbbreviation.put(locationId, abbreviation.get());
        }
      }
    }
    return locationIdToAbbreviation;
  }

  private String textForSightings(Collection<Sighting> collection) {
    if (collection.isEmpty()) {
      return "";
    }

    if (Iterables.all(collection, TripReportDocOutput::isHeardOnly)) {
      return "(H)";
    }

    int count = 0;
    for (Sighting sighting : collection) {
      if (sighting.hasSightingInfo() && sighting.getSightingInfo().getNumber() != null) {
        count += sighting.getSightingInfo().getNumber().getNumber();
      }
    }

    if (count > 0) {
      return DecimalFormat.getNumberInstance().format(count);
    }

    return "X";
  }

  private static boolean isHeardOnly(Sighting sighting) {
    return sighting.hasSightingInfo() && sighting.getSightingInfo().isHeardOnly();
  }

  private String dateToString(ReadablePartial date) {
    if (allDatesAreTheSameYear && noYearFormatter != null
        && date.isSupported(DateTimeFieldType.dayOfMonth())
        && date.isSupported(DateTimeFieldType.monthOfYear())) {
      return noYearFormatter.print(date);
    }

    return PartialIO.toShortUserString(date, Locale.getDefault());
  }

  /** Returns a formatter suitable for displaying just day and month, if possible. */
  private DateTimeFormatter noYearFormatter() {
    DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT);
    if (!(dateFormat instanceof SimpleDateFormat)) {
      return null;
    }

    String pattern = ((SimpleDateFormat) dateFormat).toPattern();
    // Remove year from the pattern
    pattern = pattern.replace("y", "").replace("Y", "");
    // And then trim any non-formatting characters from the beginning or end.
    pattern = CharMatcher.anyOf("MLwWDd").negate().trimFrom(pattern);
    return DateTimeFormat.forPattern(pattern);

  }

  private String locationName(VisitInfoKey visit) {
    return locationName(visit.locationId());
  }

  private String locationName(String locationId) {
    return LocationIdToString.getString(reportSet.getLocations(), locationId,
        false /* not verbose */, rootLocation);
  }

  private static int toYear(VisitInfoKey visitInfoKey) {
    ReadablePartial date = visitInfoKey.date();
    if (!date.isSupported(DateTimeFieldType.year())) {
      return 0;
    }
    return date.get(DateTimeFieldType.year());
  }
}
