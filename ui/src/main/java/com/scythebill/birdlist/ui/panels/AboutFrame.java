/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Nullable;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import com.google.common.base.Supplier;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.app.FrameRegistry;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.guice.Clements;
import com.scythebill.birdlist.ui.guice.CurrentVersion;
import com.scythebill.birdlist.ui.guice.IOC;
import com.scythebill.birdlist.ui.guice.Scythebill;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Implements the About... dialog.
 */
@Singleton
public class AboutFrame  {
  private static final int FRAME_WIDTH = 400;
  private static final int FRAME_HEIGHT = 350;
  
  private JFrame aboutFrame;
  private JLabel clementsLabel;
  private JLabel iocLabel;
  private final Supplier<Taxonomy> taxonomySupplier;
  private final Supplier<Taxonomy> iocSupplier;
  private JLabel taxonomyCreditLabel;
  private final String versionInfo;
  private final Icon applicationIcon;
  private final FrameRegistry frameRegistry;
  private JScrollPane additionalCreditScrollPane;
  
  @Inject
  public AboutFrame(
      @Clements Supplier<Taxonomy> taxonomySupplier,
      @IOC Supplier<Taxonomy> iocSupplier,
      @Scythebill Icon applicationIcon,
      @Nullable @CurrentVersion String versionInfo,
      FrameRegistry frameRegistry) {
    this.taxonomySupplier = taxonomySupplier;
    this.iocSupplier = iocSupplier;
    this.applicationIcon = applicationIcon;
    this.versionInfo = versionInfo;
    this.frameRegistry = frameRegistry;
  }
  
  public void show() {
    aboutFrame = createAboutFrame();
    setTaxonomyLabelText();
    aboutFrame.setVisible(true);
    aboutFrame.toFront();
  }

  private JFrame createAboutFrame() {
    JFrame frame = new JFrame();
    JMenuBar menuBar = new JMenuBar();
    frame.setJMenuBar(menuBar);
    
    Taxonomy taxonomy = taxonomySupplier.get();
    Rectangle screenBounds = frame.getGraphicsConfiguration().getBounds();
    frame.setLocation(screenBounds.x + ((screenBounds.width - 400) / 2),
        screenBounds.y + ((screenBounds.height - 300) / 2));

    Container contentPane = frame.getRootPane().getContentPane();
    contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
    JPanel centerBox = new JPanel();
    centerBox.setBorder(new EmptyBorder(4, 4, 4, 4));
    contentPane.add(centerBox);
    
    GroupLayout layout = new GroupLayout(centerBox);
    centerBox.setLayout(layout);
    
    JLabel image = new JLabel(applicationIcon);

    // App name - do not translate
    JLabel appName = new JLabel("Scythebill Birdlist");
    appName.setFont(new Font(FontManager.getFontName(), Font.BOLD, 13));
    
    String versionText = versionInfo == null ? "(unavailable)" : versionInfo;    
    JLabel version = new JLabel(
        Messages.getFormattedMessage(Name.VERSION_FORMAT, versionText));
    clementsLabel = new JLabel();
    iocLabel = new JLabel();
    
    taxonomyCreditLabel = new JLabel();

    List<String> additionalCredits = new ArrayList<>();
    if (taxonomy != null) {
      additionalCredits.addAll(taxonomy.additionalCredits());
    }
    additionalCredits.add(
        Messages.getMessage(Name.SCYTHEBILL_IMAGE_COPYRIGHT));
    additionalCredits.add("");
    
    Set<String> extendedTaxonomyIds = Sets.newHashSet();
    for (ReportSet reportSet : frameRegistry.loadedReportSets()) {
      for (Taxonomy extendedTaxonomy : reportSet.extendedTaxonomies()) {
        if (extendedTaxonomyIds.add(extendedTaxonomy.getId())) {
          additionalCredits.add(extendedTaxonomy.getName());
          additionalCredits.addAll(extendedTaxonomy.additionalCredits());
          additionalCredits.add("");
        }
      }
    }
    additionalCredits.add(String.format("Java %s", System.getProperty("java.version")));

    JTextArea additionalCreditTextArea = new JTextArea(4, 30);
    additionalCreditTextArea.setText(additionalCredits.stream().collect(Collectors.joining("\n")));
    additionalCreditTextArea.select(0, 0);
    additionalCreditScrollPane = new JScrollPane(additionalCreditTextArea);
    
    frame.setSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));

    ParallelGroup horizontalGroup = layout.createParallelGroup(Alignment.CENTER)
        .addComponent(image)
        .addComponent(appName)
        .addComponent(version)
        .addComponent(clementsLabel)
        .addComponent(taxonomyCreditLabel)
        .addComponent(iocLabel)
        .addComponent(additionalCreditScrollPane, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE);
    layout.setHorizontalGroup(horizontalGroup);
    
    SequentialGroup verticalGroup = layout.createSequentialGroup()
        .addComponent(image)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(appName)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(version)
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(clementsLabel)
        .addComponent(taxonomyCreditLabel)
        .addComponent(iocLabel)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(additionalCreditScrollPane);
    layout.setVerticalGroup(verticalGroup);
    
    return frame;
  }
  
  // No need to translate
  static private String CORNELL_CREDIT = 
      "eBird/Clements Checklist is \u00a9 Cornell Lab of Ornithology.";
  
  private void setTaxonomyLabelText() {
    Taxonomy taxonomy = taxonomySupplier.get();
    String clementsText = null;
    if (taxonomy == null) {
      clementsText = "loading...";
    } else if (clementsLabel != null) {
      clementsText = taxonomy.getName() == null ? "Unknown" : taxonomy.getName();
      if (taxonomy.getId() != null
          && taxonomy.getId().toLowerCase().contains("clements")) {
        taxonomyCreditLabel.setText(CORNELL_CREDIT);
      }
    }
    
    if (clementsText != null) {
      clementsLabel.setText(Messages.getFormattedMessage(Name.TAXONOMY_WITH_NAME, clementsText));
    }
    
    Taxonomy ioc = iocSupplier.get();  
    if (ioc != null) {
      iocLabel.setText(ioc.getName());
    }
  }
}
