/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import com.scythebill.birdlist.model.annotations.Preference;
import com.scythebill.birdlist.model.taxa.Taxon;

/** List of preferences used by the browse panel. */
public class BrowsePreferences {
  @Preference Taxon.Type depth = Taxon.Type.species;
  @Preference public boolean onlyVisitedLocations;
  
  @Preference public boolean showExtinctTaxa = true;
  
  // For now, remove @Preferences from these two fields;  they'll be sticky within
  // a session, but *not* from run-to-run. Users forget they've turned on
  // "only encountered species", and then ask why Scythebill is missing species!
  boolean onlyEncounteredSpecies;
  String onlyOnChecklistLocationId;
}
