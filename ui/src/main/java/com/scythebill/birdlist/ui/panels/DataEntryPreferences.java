/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import com.scythebill.birdlist.model.annotations.Preference;
import com.scythebill.birdlist.model.sighting.VisitInfo.ObservationType;
import com.scythebill.birdlist.ui.messages.Messages;

/** Preferences used for data entry. */
public class DataEntryPreferences {
  public enum ChecklistUse {
    NO(Messages.Name.NO_TEXT),
    YES(Messages.Name.YES_TEXT),
    YES_WITH_RARITIES(Messages.Name.YES_WITH_RARITIES);
    
    private final Messages.Name text;

    private ChecklistUse(Messages.Name text) {
      this.text = text;
    }
    
    @Override public String toString() { return Messages.getMessage(text); }
  }
  
  @Preference public ChecklistUse checklistUse = ChecklistUse.NO;
  
  @Preference public boolean immediateExportToEBird = false;

  @Preference public boolean immediateSave = true; 

  @Preference public ObservationType observationType = ObservationType.HISTORICAL;
}