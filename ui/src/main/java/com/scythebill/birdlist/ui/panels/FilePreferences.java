/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.awt.Point;
import java.io.File;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.filechooser.FileSystemView;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.annotations.Preference;
import com.scythebill.birdlist.model.annotations.SerializeAsJson;

/** Preferences used by various file access. */
public class FilePreferences {
  public enum FileType {
    SIGHTINGS, BACKUPS,
    OTHER
  }
  
  /** The most-recently loaded report set. */
  // TODO: add PreferenceManager support for lists, and keep a list of recently-loaded
  // files around
  @Preference public String lastLoadedReportSet;
  
  /** Last loaded directory - should default to Documents. */
  @Preference public String lastViewedDirectory = defaultDirectory();
  
  /** Last directory used for saving. */
  @Preference public String lastSavedDirectory = null;
  
  @SerializeAsJson @Preference public List<String> lastLoadedReportSets = Lists.newArrayList();
  
  @SerializeAsJson @Preference public LinkedHashMap<String, Point> fileOrigins = new LinkedHashMap<>();
  
  public List<String> getLastLoadedReportSets() {
    if (lastLoadedReportSets.isEmpty()
        && !Strings.isNullOrEmpty(lastLoadedReportSet)) {
      return ImmutableList.of(lastLoadedReportSet);
    } else {
      return ImmutableList.copyOf(lastLoadedReportSets);
    }
  }
  
  public void clearLastLoadedReportSets() {
    lastLoadedReportSet = null;
    lastLoadedReportSets.clear();
  }
  
  /** Stores a file access for a loaded file, to keep file dialogs in sync.  */
  public void rememberFile(File file, FileType fileType) {
    if (file.isDirectory()) {
      if (fileType == FileType.SIGHTINGS) {
        lastViewedDirectory = file.getAbsolutePath();
      } else {
        lastSavedDirectory = file.getAbsolutePath();
      }
    } else {
      if (fileType == FileType.SIGHTINGS) {
        lastViewedDirectory = file.getParentFile().getAbsolutePath();
      } else {
        lastSavedDirectory = file.getParentFile().getAbsolutePath();
      }
    }
  }
  
  /** Store a Point for a file (to preserve window locations). */
  public void setLocation(File file, Point point) {
    String path = file.getAbsolutePath();
    LinkedHashMap<String, Point> updated = new LinkedHashMap<>();
    updated.put(path, point);
    for (Map.Entry<String, Point> entry : fileOrigins.entrySet()) {
      // Track a max of five files
      if (updated.size() >= 5) {
        continue;
      }
      if (updated.containsKey(entry.getKey())) {
        continue;
      }
      updated.put(entry.getKey(), entry.getValue());
    }
    fileOrigins = updated; 
  }

  /** Get the Point for a file. */
  public Point getLocation(File file) {
    return fileOrigins.get(file.getAbsolutePath());
  }
  
  /**
   * Returns the last File directory used to load, or null.  If a non-null File is
   * returned, it is guaranteed to exist.
   */
  public File getLastDirectory(FileType fileType) {
    if (fileType == FileType.SIGHTINGS) {
      return getLastDirectory(lastViewedDirectory);
    } else {
      File directory = getLastDirectory(lastSavedDirectory);
      if (directory == null) {
        directory = getLastDirectory(lastViewedDirectory);
      }
      return directory;
    }
  }
  
  private File getLastDirectory(String preference) {
    if (preference == null) {
      return null;
    }
    
    // Make sure the directory exists - and if it doesn't,
    // walk up the file system to find one that does
    File directory = new File(preference);
    while (directory != null && !directory.exists()) {
      directory = directory.getParentFile();
    }
    
    return directory;
  }

  public void setLastLoadedReportSets(Collection<String> reportSetFiles) {
    // Set the new pref
    lastLoadedReportSets.clear();
    lastLoadedReportSets.addAll(reportSetFiles);
    // Clear the legacy pref
    lastLoadedReportSet = null;
  }

  private static String defaultDirectory() {
    try {
      // First, try the FileSystemView getDefaultDirectory(), which hopefully is 
      // somewhere in User
      File file = FileSystemView.getFileSystemView().getDefaultDirectory();
      if (file == null || !file.exists()) {
        // If that fails (shouldn't, but <shrug>), look at old-school user.home
        String userHome = System.getProperty("user.home");
        if (userHome != null) {
          file = new File(userHome);
        }
      }
      
      if (file != null) {
        // Supposedly, on Windows this will return "Documents".  That's nice!  But
        // it definitely doesn't on MacOS.  So look for a child called "My Documents" or "Documents".
        // This is pointless on non-English OS's, but also shouldn't be a problem.
        if (!file.getName().contains("Documents") && file.exists() && file.isDirectory()) {
          ImmutableList<String> possibleChildFolders = ImmutableList.of(
              "My Documents", "Documents");
          for (String possibleChildFolder : possibleChildFolders) {
            File childFile = new File(file, possibleChildFolder);
            if (childFile.exists() && childFile.isDirectory()) {
              return childFile.getAbsolutePath();
            }
          }
        }
        
        return file.getAbsolutePath();
      }
    } catch (Exception e) {
      // Drop-and-move on.  Nothing here should throw anything, but this gets into
      // enough system weirdness that I wouldn't be shocked to find out Java barfs in some setup.
      Logger.getGlobal().log(Level.WARNING, "Couldn't get default directory", e);
    }
    
    return null;
  }
}
