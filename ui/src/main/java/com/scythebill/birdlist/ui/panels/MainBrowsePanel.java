/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkEvent.EventType;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.eventbus.Subscribe;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.query.SightingPredicates;
import com.scythebill.birdlist.model.query.SyntheticLocation;
import com.scythebill.birdlist.model.query.SyntheticLocations;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.actions.ReturnAction;
import com.scythebill.birdlist.ui.app.Titled;
import com.scythebill.birdlist.ui.components.DepthChooser;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.components.SpeciesIndexerPanelConfigurer;
import com.scythebill.birdlist.ui.components.TaxonBrowsePanel;
import com.scythebill.birdlist.ui.components.WherePanelIndexers;
import com.scythebill.birdlist.ui.events.DefaultUserChangedEvent;
import com.scythebill.birdlist.ui.events.DefaultUserStore;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.events.TaxonomyChangedEvent;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.EncounteredTaxa;
import com.scythebill.birdlist.ui.util.VisibilityDetector;

/**
 * Top-level panel wrapping a TaxonBrowsePanel with widgets
 * for editing.
*/
public class MainBrowsePanel extends JPanel implements Titled, FontsUpdatedListener {
  private static final int SPECIES_INFO_HEIGHT = 66;
  private static final Logger logger = Logger.getLogger(MainBrowsePanel.class.getName());
  
  private JButton returnButton;
  private final TaxonBrowsePanel taxonBrowsePanel;
  private final ReportSet reportSet;
  private final Checklists checklists;
  private final ListeningExecutorService executorService;
  private final TaxonomyStore taxonomyStore;
  private final DefaultUserStore defaultUserStore;
  private final FontManager fontManager;
  private final SpeciesIndexerPanelConfigurer speciesIndexerPanelConfigurer;
  private final Alerts alerts;
  private final BrowsePreferences browsePreferences;
  private final SpeciesInfoDescriber speciesInfoDescriber;
  private final ShowSpeciesMap showSpeciesMap;
  private IndexerPanel<String> speciesIndexer;
  private JButton jumpButton;
  private DepthChooser depthChooser;
  private JEditorPane speciesInfo;
  private JScrollPane taxonScrollPane;
  private JScrollPane speciesInfoScrollPane;
  private JSeparator separator;
  private JLabel depthLabel;
  private JButton addSightingButton;
  private JButton removeSightingButton;
  private JCheckBox onlyEncounteredSpecies;
  private JLabel onlySpeciesOnChecklistLabel;
  private IndexerPanel<Object> onlySpeciesOnChecklistIndexer; 

  private Predicate<Taxon> taxonFilter;
  private Predicate<String> taxonIdFilter;
  private SyntheticLocations syntheticLocations;
  private PredefinedLocations predefinedLocations;

  @Inject
  public MainBrowsePanel(
      TaxonBrowsePanel taxonBrowsePanel,
      ReturnAction returnAction,
      SpeciesIndexerPanelConfigurer speciesIndexerPanelConfigurer,
      ReportSet reportSet,
      Checklists checklists,
      PredefinedLocations predefinedLocations,
      TaxonomyStore taxonomyStore,
      ListeningExecutorService executorService,
      DefaultUserStore defaultUserStore,
      BrowsePreferences browsePreferences,
      SpeciesInfoDescriber speciesInfoDescriber,
      ShowSpeciesMap showSpeciesMap,
      FontManager fontManager,
      EventBusRegistrar eventBusRegistrar,
      Alerts alerts,
      VisibilityDetector visibilityDetector) {
    super();
    this.taxonBrowsePanel = taxonBrowsePanel;
    this.speciesIndexerPanelConfigurer = speciesIndexerPanelConfigurer;
    this.reportSet = reportSet;
    this.checklists = checklists;
    this.predefinedLocations = predefinedLocations;
    this.taxonomyStore = taxonomyStore;
    this.executorService = executorService;
    this.defaultUserStore = defaultUserStore;
    this.browsePreferences = browsePreferences;
    this.speciesInfoDescriber = speciesInfoDescriber;
    this.showSpeciesMap = showSpeciesMap;
    this.fontManager = fontManager;
    this.alerts = alerts;
    this.syntheticLocations = new SyntheticLocations(reportSet.getLocations());
    eventBusRegistrar.registerWhenInHierarchy(this);
    initGUI();
    visibilityDetector.install(returnButton);
    hookUpContents(returnAction);    
  }

  @Override
  public String getTitle() {
    return Messages.getMessage(Name.BROWSE_BY_SPECIES);
  }

  private void hookUpContents(ReturnAction returnAction) {
    returnButton.setAction(returnAction);
    
    taxonBrowsePanel.addComponentListener(new ComponentAdapter() {
      @Override public void componentMoved(ComponentEvent e) {
        updateInfoPanels();
      }
    });
    
    addComponentListener(new ComponentAdapter() {
       @Override public void componentResized(ComponentEvent e) {
        updateInfoPanels();
      }
    });
    
    addAncestorListener(new AncestorListener() {
      @Override public void ancestorAdded(AncestorEvent event) {
      }

      @Override public void ancestorMoved(AncestorEvent event) {
      }

      @Override public void ancestorRemoved(AncestorEvent event) {
        hideInfoPanels();
      }
    });
    
    speciesIndexer.addPropertyChangeListener("value", evt -> 
        jumpButton.setEnabled(evt.getNewValue() != null));
    speciesIndexer.addActionListener(e -> jumpButton.doClick(100));
    jumpButton.addActionListener(e -> {
      taxonBrowsePanel.selectTaxon(speciesIndexer.getValue());
      updateInfoPanelsLater();
    });
    depthChooser.addPropertyChangeListener("depth", e -> {
      browsePreferences.depth = depthChooser.getDepth();
      taxonBrowsePanel.setDepth(depthChooser.getDepth());
      updateTaxonFilter();
    });
    
    taxonBrowsePanel.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener() {
      @Override public void valueChanged(TreeSelectionEvent e) {
        TreePath[] selection = taxonBrowsePanel.getSelectionPaths();
        if (selection == null || selection.length != 1) {
          hideInfoPanels();
          // 0 or > 1 selection;  can't add a sighting
          addSightingButton.setEnabled(false);
        } else {
          updateInfoPanelsLater();
          // Just 1 selection which is a species: yes, enable adding a sighting 
          addSightingButton.setEnabled(selection[0].getLastPathComponent() instanceof Species);
        }
        
        if (selection == null || selection.length == 0) {
          removeSightingButton.setEnabled(false);
          removeSightingButton.setText(Messages.getMessage(Name.REMOVE_SIGHTING));
        } else if (selection.length == 1) {
          removeSightingButton.setEnabled(selection[0].getLastPathComponent() instanceof Sighting);
          removeSightingButton.setText(Messages.getMessage(Name.REMOVE_SIGHTING));
        } else {
          boolean allAreSightings = true;
          for (TreePath path : selection) {
            if (!(path.getLastPathComponent() instanceof Sighting)) {
              allAreSightings = false;
              break;
            }
          }
          removeSightingButton.setEnabled(allAreSightings);
          removeSightingButton.setText(Messages.getMessage(Name.REMOVE_SIGHTINGS));
        }
        
        if (removeSightingButton.isEnabled()) {
          removeSightingButton.setVisible(true);
          addSightingButton.setVisible(false);
        } else {
          addSightingButton.setVisible(true);
          removeSightingButton.setVisible(false);
        }
      }
    });
    taxonBrowsePanel.setDepth(browsePreferences.depth == null
        ? Type.species : browsePreferences.depth);
    
    addSightingButton.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent e) {
        TreePath[] selection = taxonBrowsePanel.getSelectionPaths();
        if (selection != null && selection.length == 1
            && selection[0].getLastPathComponent() instanceof Species) {
          addSighting(selection[0], (Species) selection[0].getLastPathComponent());
        }
      }
    });
    removeSightingButton.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent e) {
        int okCancel = alerts.showOkCancel(MainBrowsePanel.this,
            taxonBrowsePanel.getSelectionCount() > 1 ? Name.REMOVE_SIGHTINGS : Name.REMOVE_SIGHTING,
            taxonBrowsePanel.getSelectionCount() > 1 ? Name.REMOVE_SIGHTINGS_CONFIRM : Name.REMOVE_SIGHTING_CONFIRM);
        if (okCancel == JOptionPane.OK_OPTION) {
          // Save any edits in the current preview, so that it's fully applied
          // before there's a cut.  (https://code.google.com/p/scythebill-birdlist/issues/detail?id=137)
          taxonBrowsePanel.saveCurrentPreview();
          taxonBrowsePanel.getCutAction().actionPerformed(e);
        }
      }
    });
    speciesInfo.addHyperlinkListener(this::showSpeciesInfoMap);
    
    onlyEncounteredSpecies.addActionListener(e -> {
      browsePreferences.onlyEncounteredSpecies = onlyEncounteredSpecies.isSelected();
      updateTaxonFilter();
    });
    
    onlySpeciesOnChecklistIndexer.addPropertyChangeListener("value", e -> {
      Object value = onlySpeciesOnChecklistIndexer.getValue();
      if (value instanceof SyntheticLocation synthetic) {
        browsePreferences.onlyOnChecklistLocationId = synthetic.getId();
      } else {
        Location location = WherePanelIndexers.toLocation(reportSet.getLocations(), value);
        browsePreferences.onlyOnChecklistLocationId =
            location == null ? null : location.getId();
      }
      updateTaxonFilter();
    });
  }
  
  private void showSpeciesInfoMap(HyperlinkEvent event) {
    if (event.getEventType() == EventType.ACTIVATED) {
      if (SpeciesInfoDescriber.isShowMapUrl(event.getURL())) {
        String id = SpeciesInfoDescriber.getShowMapSpeciesId(event.getURL());
        try {
          showSpeciesMap.showRange(reportSet, taxonomyStore.getTaxonomy(), id);
        } catch (IOException e) {
          alerts.showError(MainBrowsePanel.this, e,
              Name.MAP_FAILED, Name.COULDNT_WRITE_MAP);
        }
      } else {
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
          try {
            Desktop.getDesktop().browse(event.getURL().toURI());
          } catch (IOException | URISyntaxException e) {
            logger.log(Level.WARNING, "Couldn't open URL " + event.getURL(), e);
          }
        }
      }
    }
  }
  
  private void addSighting(TreePath path, Taxon taxon) {
    Sighting.Builder builder = Sighting.newBuilder().setTaxon(taxon);
    switch (taxon.getStatus()) {
      case IN:
        builder.getSightingInfo().setSightingStatus(SightingStatus.INTRODUCED);
        break;
      case DO:
        // Unfortunately, this could either be domestic *or* introduced (or neither),
        // so we can't default to anything here. 
        // builder.getSightingInfo().setSightingStatus(SightingStatus.DOMESTIC);
        break;
      default:
        break;
    }
    
    Sighting sighting = builder.build();
    if (defaultUserStore.getUser() != null) {
      sighting.getSightingInfo().setUsers(ImmutableSet.of(defaultUserStore.getUser()));
    }
    
    taxonBrowsePanel.addSighting(path, sighting);
    // Select the new child path
    taxonBrowsePanel.setSelectionPath(path.pathByAddingChild(sighting));
  }

  private void showInfoPanel(TreePath treePath) {
    hideInfoPanels();
    while (treePath.getPathCount() > 1) {
      if (treePath.getLastPathComponent() instanceof Species) {
        break;
      }
      treePath = treePath.getParentPath();
    }
    if (treePath.getLastPathComponent() instanceof Species) {
      Rectangle bounds = taxonBrowsePanel.getPathBounds(treePath);
      if (bounds == null) { 
        return;
      }
      Species species = (Species) treePath.getLastPathComponent();
      String info = speciesInfoDescriber.toSimpleLinkedHtmlText(reportSet, species);      
      speciesInfo.setText(info);
      speciesInfo.select(0, 0);
      speciesInfo.setFont(fontManager.getTextFont());
      Point rootPoint = SwingUtilities.convertPoint(
          taxonBrowsePanel, bounds.x, taxonScrollPane.getHeight() - taxonScrollPane.getInsets().bottom,
          getRootPane());
      
      getRootPane().getLayeredPane().add(speciesInfoScrollPane, JLayeredPane.POPUP_LAYER);
      speciesInfoScrollPane.setBounds(rootPoint.x, rootPoint.y, bounds.width,
          fontManager.scale(SPECIES_INFO_HEIGHT));
      speciesInfoScrollPane.setVisible(true);
    }
  }
  
  /**
   * In cases where the layout of the browser is changing, it's safer to wait to update
   * the info panels, since the relayout may not have occurred yet.
   */ 
  private void updateInfoPanelsLater() {
    SwingUtilities.invokeLater(this::updateInfoPanels);
  }
  
  private void updateInfoPanels() {
    if (isDisplayable()) {
      TreePath[] selection = taxonBrowsePanel.getSelectionPaths();
      if (selection == null || selection.length != 1) {
        hideInfoPanels();
      } else {
        showInfoPanel(selection[0]);
      }
    }
  }

  private void hideInfoPanels() {
    if (speciesInfoScrollPane.getParent() != null) {
      speciesInfoScrollPane.setVisible(false);
      speciesInfoScrollPane.getParent().remove(speciesInfoScrollPane);
    }
  }
  
  private void initGUI() {
    onlyEncounteredSpecies = new JCheckBox(
        Messages.getMessage(Name.ONLY_ENCOUNTERED_SPECIES));
    onlyEncounteredSpecies.setSelected(browsePreferences.onlyEncounteredSpecies);
    
    onlySpeciesOnChecklistLabel = new JLabel(
        Messages.getMessage(Name.ONLY_SPECIES_FOUND_IN));
    onlySpeciesOnChecklistIndexer = new IndexerPanel<Object>();
    onlySpeciesOnChecklistIndexer.setPreviewText(Name.START_TYPING_A_LOCATION);

    taxonBrowsePanel.setStyledSpeciesWithSightings(!onlyEncounteredSpecies.isSelected());
    defaultUserChanged(null);
    returnButton = new JButton();

    separator = new JSeparator();
    
    taxonScrollPane = new JScrollPane();
    taxonScrollPane.setViewportView(taxonBrowsePanel);

    speciesIndexer = new IndexerPanel<String>();

    jumpButton = new JButton(Messages.getMessage(Name.JUMP_TO_BUTTON));
    jumpButton.putClientProperty("JComponent.sizeVariant", "small");
    
    depthLabel = new JLabel(Messages.getMessage(Name.SHOW_LABEL));
    depthChooser = new DepthChooser();
    depthChooser.setDepth(browsePreferences.depth);
    
    addSightingButton = new JButton(Messages.getMessage(Name.ADD_SIGHTING));
    addSightingButton.putClientProperty("JComponent.sizeVariant", "small");
    
    removeSightingButton = new JButton(Messages.getMessage(Name.REMOVE_SIGHTING));
    removeSightingButton.putClientProperty("JComponent.sizeVariant", "small");

    speciesInfo = new JEditorPane();
    speciesInfo.setEditable(false);
    speciesInfo.setContentType("text/html");
    speciesInfo.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, true);

    fontManager.applyTo(speciesInfo);

    speciesInfoScrollPane = new JScrollPane(speciesInfo);
    speciesInfoScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
    speciesInfoScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    speciesInfoScrollPane.putClientProperty("Quaqua.Component.visualMargin", new Insets(0, 0, 0, 0));

    taxonomyChanged(null);
    if (browsePreferences.onlyOnChecklistLocationId != null) {
      SyntheticLocation syntheticLocation = syntheticLocations.byId(browsePreferences.onlyOnChecklistLocationId);
      if (syntheticLocation != null) {
        onlySpeciesOnChecklistIndexer.setValue(syntheticLocation);
      } else {
        onlySpeciesOnChecklistIndexer.setValue(browsePreferences.onlyOnChecklistLocationId);
      }
      updateTaxonFilter();
    }

    fontManager.applyTo(this);
  }

  private void createTaxonFilters() {
    taxonFilter = null;
    taxonIdFilter = null;
    
    EncounteredTaxa encounteredTaxa = null;
    // TODO: put in background if slow
    if (onlyEncounteredSpecies.isSelected() || !browsePreferences.showExtinctTaxa) {
      try {
        encounteredTaxa = EncounteredTaxa.scan(taxonomyStore.getTaxonomy(), reportSet, executorService).get();
      } catch (InterruptedException | ExecutionException e) {
        // Rare error, don't bother translating
        alerts.showError(this, e, "Scanning taxa failed", "Could not scan taxa.");
      }
    }

    final EncounteredTaxa finalEncounteredTaxa = encounteredTaxa;
    if (onlyEncounteredSpecies.isSelected()
        && finalEncounteredTaxa != null) {
      taxonFilter = taxon -> {
        // Don't filter any subspecies or groups. Browse-by-taxon is an important way to edit
        // subspecies, and that breaks if subspecies and groups are filtered.
        if (taxon.getType() == Taxon.Type.subspecies
            || taxon.getType() == Taxon.Type.group) {
          return true;
        }
        
        return finalEncounteredTaxa.isEncountered(taxon);
      };
      taxonIdFilter = finalEncounteredTaxa::isEncountered;
    }
    Taxonomy taxonomy = taxonomyStore.getTaxonomy();

    Object locationValue = onlySpeciesOnChecklistIndexer.getValue();
    Location location = WherePanelIndexers.toLocation(reportSet.getLocations(), locationValue);
    if (location != null && onlySpeciesOnChecklistIndexer.isEnabled()) {
      Checklist checklist = checklists.getChecklist(reportSet, taxonomy, location);
      ImmutableSet.Builder<String> higherLevelTaxaBuilder = ImmutableSet.builder();
      if (checklist != null) {
        for (SightingTaxon taxon : checklist.getTaxa(taxonomy)) {
          Status status = checklist.getStatus(taxonomy, taxon);
          if (status == Status.ESCAPED) {
            continue;
          }

          Resolved resolved = taxon.resolveInternal(taxonomy);
          SightingTaxon family = resolved.getParentOfAtLeastType(Taxon.Type.family);
          SightingTaxon order = resolved.getParentOfAtLeastType(Taxon.Type.order);
          higherLevelTaxaBuilder.addAll(family.getIds());
          higherLevelTaxaBuilder.addAll(order.getIds());
        }
        ImmutableSet<String> higherLevelTaxa = higherLevelTaxaBuilder.build();
        ImmutableSet<Status> excludedStatuses = ImmutableSet.of(Status.ESCAPED);
        Predicate<Taxon> checklistTaxonFilter = taxon -> {
          if (taxon.getType() == Taxon.Type.order
              || taxon.getType() == Taxon.Type.family) {
            return higherLevelTaxa.contains(taxon.getId());
          } else if (taxon.getType() != Taxon.Type.species) {
            return true;
          }
          
          return checklist.includesTaxon(taxon, excludedStatuses);
        };
        Predicate<String> checklistTaxonIdFilter = id -> {
          Taxon taxon = TaxonUtils.getParentOfType(taxonomy.getTaxon(id), Taxon.Type.species);
          return checklistTaxonFilter.apply(taxon);
        };
        
        if (taxonFilter == null) {
          taxonFilter = checklistTaxonFilter;
        } else {
          taxonFilter = Predicates.and(taxonFilter, checklistTaxonFilter);
        }
        
        if (taxonIdFilter == null) {
          taxonIdFilter = checklistTaxonIdFilter;
        } else {
          taxonIdFilter = Predicates.and(taxonIdFilter, checklistTaxonIdFilter);
        }
        
      }
    }
    
    if (depthChooser.getDepth() == Taxon.Type.species) {
      Predicate<String> onlySpeciesFilter = id -> {
        Taxon taxon = taxonomy.getTaxon(id);
        if (taxon == null) {
          return false;
        }
        return taxon.getType() == Taxon.Type.species;
      };
      
      if (taxonIdFilter == null) {
        taxonIdFilter = onlySpeciesFilter;
      } else {
        taxonIdFilter = Predicates.and(taxonIdFilter, onlySpeciesFilter);
      }
    }
    
    if (!browsePreferences.showExtinctTaxa) {
      Predicate<Taxon> noExtinctTaxa = taxon -> {
        if (taxon.getStatus() != Species.Status.EX) {
          return true;
        }

        return finalEncounteredTaxa != null
            && finalEncounteredTaxa.isEncountered(taxon);
      };
      Predicate<String> noExtinctTaxonIds = id -> {
        Taxon taxon = taxonomy.getTaxon(id);
        if (taxon == null) {
          return false;
        }
        
        return noExtinctTaxa.apply(taxon);
      };
      
      
      if (taxonIdFilter == null) {
        taxonIdFilter = noExtinctTaxonIds;
      } else {
        taxonIdFilter = Predicates.and(taxonIdFilter, noExtinctTaxonIds);
      }
      if (taxonFilter == null) {
        taxonFilter = noExtinctTaxa;
      } else {
        taxonFilter = Predicates.and(taxonFilter, noExtinctTaxa);
      }

    }
  }

  @Subscribe
  public void taxonomyChanged(TaxonomyChangedEvent event) {
    Taxonomy newTaxonomy = taxonomyStore.getTaxonomy();
    if (taxonBrowsePanel.getTaxonomy() == null
        || !taxonBrowsePanel.getTaxonomy().isBuiltIn()
        || !newTaxonomy.isBuiltIn()) {
      // Unless it's a change from one built-in taxonomy to another, the "onlySpeciesOnChecklist"
      // UI needs updating.
      boolean hasChecklists = newTaxonomy.isBuiltIn()
          || reportSet.hasExtendedTaxonomyChecklists(newTaxonomy.getId());
      if (hasChecklists) {
        onlySpeciesOnChecklistLabel.setEnabled(true);
        onlySpeciesOnChecklistIndexer.setEnabled(true);
        Object oldLocationValue = onlySpeciesOnChecklistIndexer.getValue();
        Location oldLocation = WherePanelIndexers.toLocation(reportSet.getLocations(), oldLocationValue);
        onlySpeciesOnChecklistIndexer.removeAllIndexerGroups();
        Predicate<Location> locationFilter = location -> {
          return checklists.hasChecklist(taxonomyStore.getTaxonomy(), reportSet, location);
        };
        WherePanelIndexers indexers = new WherePanelIndexers(
            reportSet.getLocations(),
            predefinedLocations,
            /* addNullName= */ false,
            /* nullName= */ null,
            locationFilter,
            syntheticLocations);
        indexers.configureIndexer(onlySpeciesOnChecklistIndexer);
        // Reset the location *if* the new taxonomy doesn't have a checklist
        if (oldLocation != null) {
          if (!checklists.hasChecklist(taxonomyStore.getTaxonomy(), reportSet, oldLocation)) {
            onlySpeciesOnChecklistIndexer.setTextValue("");
          }
        }
      } else {
        onlySpeciesOnChecklistLabel.setEnabled(false);
        onlySpeciesOnChecklistIndexer.setEnabled(false);
      }
    }
    
    createTaxonFilters();
    
    speciesIndexerPanelConfigurer.configure(speciesIndexer, taxonomyStore.getTaxonomy());
    speciesIndexer.setValue(null);
    speciesIndexer.setFilter(taxonIdFilter);
    taxonBrowsePanel.setTaxonomy(taxonomyStore.getTaxonomy(), taxonFilter);
  }

  private void updateTaxonFilter() {
    createTaxonFilters();
    taxonBrowsePanel.setTaxonFilter(taxonFilter);
    speciesIndexer.setFilter(taxonIdFilter);
    taxonBrowsePanel.setStyledSpeciesWithSightings(!onlyEncounteredSpecies.isSelected());
  }

  @Subscribe
  public void defaultUserChanged(DefaultUserChangedEvent event) {
    taxonBrowsePanel.setSightingPredicate(
        defaultUserStore.getUser() != null
            ? SightingPredicates.includesUser(defaultUserStore.getUser())
            : Predicates.alwaysTrue());
  }
  
  @Override
  public void fontsUpdated(FontManager fontManager) {
    // TODO: fix for monitors smaller than this
    Dimension size = fontManager.scale(new Dimension(1200, 800));

    // Any smaller than about 730 and the bottom buttons disappear 
    size.height = Math.max(size.height, 730);
    setPreferredSize(size);

    GroupLayout layout = new GroupLayout(this);
    this.setLayout(layout);

    layout.setVerticalGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(speciesIndexer)
            .addComponent(jumpButton)
            .addComponent(depthLabel)
            .addComponent(depthChooser)
            .addComponent(onlyEncounteredSpecies)
            .addComponent(onlySpeciesOnChecklistLabel)
            .addComponent(onlySpeciesOnChecklistIndexer))
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(taxonScrollPane, fontManager.scale(200), fontManager.scale(400), Short.MAX_VALUE)
        .addPreferredGap(ComponentPlacement.UNRELATED)
         // Add a vertical gap where the species info will float 
        .addGap(fontManager.scale(SPECIES_INFO_HEIGHT),
            fontManager.scale(SPECIES_INFO_HEIGHT),
            fontManager.scale(SPECIES_INFO_HEIGHT))
        .addComponent(separator, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
        .addGap(0, 17, PREFERRED_SIZE)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(addSightingButton)
            .addComponent(removeSightingButton)
            .addComponent(returnButton, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
        .addContainerGap());
      layout.setHorizontalGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(layout.createParallelGroup()
            .addGroup(Alignment.LEADING, layout.createSequentialGroup()
                .addComponent(speciesIndexer)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(jumpButton)
                .addPreferredGap(ComponentPlacement.UNRELATED)
                .addComponent(depthLabel)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(depthChooser, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                .addPreferredGap(ComponentPlacement.UNRELATED)
                .addComponent(onlySpeciesOnChecklistLabel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                .addPreferredGap(ComponentPlacement.UNRELATED)
                .addComponent(onlySpeciesOnChecklistIndexer, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(onlyEncounteredSpecies, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
            .addComponent(separator, Alignment.LEADING, 0, fontManager.scale(376), Short.MAX_VALUE)
            .addComponent(taxonScrollPane, Alignment.LEADING, fontManager.scale(400), fontManager.scale(700), Short.MAX_VALUE)
            .addGroup(Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(addSightingButton)
                .addComponent(removeSightingButton)
                .addPreferredGap(ComponentPlacement.UNRELATED)
                .addComponent(returnButton, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)))
        .addContainerGap());
    updateInfoPanelsLater();
  }
}
