/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FocusTraversalPolicy;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import javax.annotation.Nullable;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import org.joda.time.Duration;
import org.joda.time.Instant;

import com.google.common.collect.Lists;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.common.html.HtmlEscapers;
import com.google.common.util.concurrent.ListeningScheduledExecutorService;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Named;
import com.google.inject.name.Names;
import com.scythebill.birdlist.model.checklist.ExtendedTaxonomyChecklists;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.ui.actions.ActionBroker;
import com.scythebill.birdlist.ui.actions.MenuConfiguration;
import com.scythebill.birdlist.ui.actions.TextFieldActions;
import com.scythebill.birdlist.ui.app.FrameRegistry;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.app.ReportSetSaver;
import com.scythebill.birdlist.ui.app.Titled;
import com.scythebill.birdlist.ui.events.TaxonomyChangedEvent;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.guice.CurrentVersion;
import com.scythebill.birdlist.ui.guice.Scythebill;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Main window.
 */
public class MainFrame extends JFrame implements NavigableFrame {
  private JPanel container;
  private final Injector injector;
  private final Action saveAction;
  private final Action exportAction;
  private final Action fullExportAction;
  private final Action openContainingAction;
  private final Action identifyChecklistErrorsAction;
  private final Action identifyChecklistRaritiesAction;
  private final Action reconcileAgainstChecklistsAction;
  private final ActionBroker actionBroker;
  private final TextFieldActions textFieldActions;
  private final ReportSetSaver reportSetSaver;
  private final File reportSetFile;
  private final MenuConfiguration menuConfiguration;
  private final TaxonomyChooserPanel taxonomyChooserPanel;
  private final String versionInfo;
  private final Action importAction;
  private final List<JComponent> toolbarComponents = Lists.newArrayList();
  private final FontsUpdatedListener fontsUpdatedListener = new FontsUpdatedListener() {
    @Override public void fontsUpdated(FontManager fontManager) {
      fontManager.applyTo(MainFrame.this);
      pack();
      UIUtils.keepWindowOnScreen(MainFrame.this);
    }
  };
  private final FontManager fontManager;
  private final Alerts alerts;
  private final Toolbar toolbar = new Toolbar();
  private Dimension lastSavedSize = new Dimension();
  private JPanel toolbarPanel;
  private final ListeningScheduledExecutorService executorService;
  private ScheduledFuture<?> saveIfDirtyFuture;
  private Action importChecklistsAction;
  private OtherFileLoader otherFileLoader;
  private JPanel currentPanel;
  private final Action preferencesAction;
  private final Action saveACopyAsAction;
  private final TaxonomyStore taxonomyStore;
  private AbstractAction closeAction;
  private final Action manageTaxonomies;
  private Action onCompletion;
  private FrameRegistry frameRegistry;
  private FilePreferences filePreferences;
  private final ReportSet reportSet;
  
  public static void initCommonUI() {
    String css = String.format("<head>"+
        "<style type=\"text/css\">"+
        "b { font: 13pt \"%s\" }"+
        "p { font: 11pt \"%s\"; margin-top: 8px }"+
        "</style>"+
        "</head>",
        FontManager.getFontName(),
        FontManager.getFontName());
    UIManager.put("OptionPane.css", css);
  }
  
  public static MainFrame start(final Injector injector, String startNav) {
    MainFrame inst = injector.getInstance(MainFrame.class);
    inst.start(startNav);
    return inst;
  }

  @Inject
  public MainFrame(Injector injector,
      ActionBroker actionBroker,
      File reportSetFile,
      @Named(ActionBroker.SAVE) Action saveAction,
      @Named(ActionBroker.SAVE_A_COPY_AS) Action saveACopyAsAction,
      @Named(ActionBroker.EXPORT) Action exportAction,
      @Named(ActionBroker.FULL_EXPORT) Action fullExportAction,
      @Named(ActionBroker.IMPORT) Action importAction,
      @Named(ActionBroker.MANAGE_TAXONOMIES) Action manageTaxonomies,
      @Named(ActionBroker.OPEN_CONTAINING) Action openContainingAction,
      @Named(ActionBroker.IDENTIFY_CHECKLIST_ERRORS) Action identifyChecklistErrorsAction,
      @Named(ActionBroker.IDENTIFY_CHECKLIST_RARITIES) Action identifyChecklistRaritiesAction,
      @Named(ActionBroker.RECONCILE_AGAINST_CHECKLISTS) Action reconcileAgainstChecklistsAction,
      @Named(ActionBroker.IMPORT_CHECKLISTS) Action importChecklistsAction,
      @Named(ActionBroker.PREFERENCES) Action preferencesAction,
      TextFieldActions textFieldActions,
      MenuConfiguration menuConfiguration,
      TaxonomyStore taxonomyStore,
      TaxonomyChooserPanel taxonomyChooserPanel,
      EventBus eventBus,
      ReportSetSaver reportSetSaver,
      ReportSet reportSet,
      @Scythebill Image image,
      @Nullable @CurrentVersion String versionInfo,
      FontManager fontManager,
      Alerts alerts,
      ListeningScheduledExecutorService executorService,
      FilePreferences filePreferences,
      OtherFileLoader otherFileLoader,
      FrameRegistry frameRegistry) {
    super();
    this.injector = injector;
    this.actionBroker = actionBroker;
    this.reportSetFile = reportSetFile;
    this.saveAction = saveAction;
    this.saveACopyAsAction = saveACopyAsAction;
    this.exportAction = exportAction;
    this.fullExportAction = fullExportAction;
    this.importAction = importAction;
    this.manageTaxonomies = manageTaxonomies;
    this.openContainingAction = openContainingAction;
    this.identifyChecklistErrorsAction = identifyChecklistErrorsAction;
    this.identifyChecklistRaritiesAction = identifyChecklistRaritiesAction;
    this.reconcileAgainstChecklistsAction = reconcileAgainstChecklistsAction;
    this.importChecklistsAction = importChecklistsAction;
    this.preferencesAction = preferencesAction;
    this.textFieldActions = textFieldActions;
    this.menuConfiguration = menuConfiguration;
    this.taxonomyStore = taxonomyStore;
    this.taxonomyChooserPanel = taxonomyChooserPanel;
    this.reportSetSaver = reportSetSaver;
    this.reportSet = reportSet;
    this.versionInfo = versionInfo;
    this.fontManager = fontManager;
    this.alerts = alerts;
    this.executorService = executorService;
    this.filePreferences = filePreferences;
    this.otherFileLoader = otherFileLoader;
    this.frameRegistry = frameRegistry;
    fontManager.addListener(fontsUpdatedListener);
    setIconImage(image);
    initGUI();
    eventBus.register(this);
    
    // Update the "documentModified" MacOS client property, and default properly
    reportSetSaver.getDirty().addDirtyListener(
        e -> {
          getRootPane().putClientProperty("Window.documentModified",
              reportSetSaver.getDirty().isDirty());
        });
    getRootPane().putClientProperty("Window.documentModified",
        reportSetSaver.getDirty().isDirty());
  }
  
  void start(String startNav) {
    navigateTo(startNav);
    setLocationByPlatform(true);
    Point location = filePreferences.getLocation(reportSetFile);
    if (location != null) {
      setLocation(location);
    }
      
    setVisible(true);
    // Make sure it's visible at the start
    UIUtils.keepWindowOnScreen(this);
    
    addComponentListener(new ComponentAdapter() {
      @Override
      public void componentMoved(ComponentEvent e) {
        filePreferences.setLocation(reportSetFile, getLocation());
      }
      
    });
    // Hack:  install a focus traversal policy that does nothing.
    // This is darn useful when debugging focus tracking issues, and does
    // no harm.
    final FocusTraversalPolicy current = getFocusTraversalPolicy();
    setFocusTraversalPolicy(new FocusTraversalPolicy() {
      @Override public Component getLastComponent(Container c) {
        return current.getLastComponent(c);
      }
      
      @Override public Component getFirstComponent(Container c) {
        return current.getFirstComponent(c);
      }
      
      @Override public Component getDefaultComponent(Container c) {
        return current.getDefaultComponent(c);
      }
      
      @Override public Component getComponentBefore(Container c, Component child) {
        return current.getComponentBefore(c, child);
      }
      
      @Override public Component getComponentAfter(Container c, Component child) {
        return current.getComponentAfter(c, child);
      }
    });
    
    // Runnable that will ask the user to save the report set.  Must be run on the event dispatcher thread.
    final Runnable askToSave = new Runnable() {
      @Override public void run() {
        int okCancel = alerts.showOkCancel(
            MainFrame.this, Name.SAVE_SIGHTINGS_TITLE, Name.SAVE_SIGHTINGS_MESSAGE);
        if (okCancel == JOptionPane.OK_OPTION) {
          try {
            reportSetSaver.save();
          } catch (IOException e) {
            FileDialogs.showFileSaveError(alerts, e, reportSetSaver.file());
          }
        }
      }
    };
    // Runnable that will check if the file is dirty, and if so show the alert asking to save
    Runnable saveIfDirty = new Runnable() {
      @Override public void run() {
        Instant dirtySince = reportSetSaver.getDirty().dirtySince();
        if (dirtySince != null) {
          // If the file has been dirty for > 2 hours, ask
          Instant oldestAllowable = new Instant().minus(Duration.standardHours(2));
          if (dirtySince.isBefore(oldestAllowable)) {
            try {
              SwingUtilities.invokeAndWait(askToSave);
            } catch (InterruptedException e) {
              Thread.interrupted();
              throw new RuntimeException(e);
            } catch (InvocationTargetException e) {
              throw new RuntimeException(e.getCause());
            }
          }
        }
      }
    };
    // And check every hour.  Note that if a user clicks "cancel",
    // that means this will nag every hour.  However "withFixedDelay" means that they'll get
    // nagged an hour *after* clicking cancel, not an hour after the alert was shown.
    saveIfDirtyFuture = executorService.scheduleWithFixedDelay(saveIfDirty, 1, 1, TimeUnit.HOURS);
    
    // Warn users if it's a non-writable directory.
    if (!reportSetFile.canWrite()) {
      File directory = reportSetFile.getParentFile();
      if (!Files.isWritable(directory.toPath())) {
        alerts.showMessage(this,
            Name.NOT_WRITABLE_TITLE,
            Name.NOT_WRITABLE_FORMAT,
            directory.getName());
      }
    }
  }

  private void initGUI() {
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    getContentPane().setLayout(new BorderLayout());
    {
      container = new JPanel();
      BorderLayout containerBorderLayout = new BorderLayout();
      toolbarPanel = new JPanel();
      layoutToolbar();
      getContentPane().add(toolbarPanel, BorderLayout.NORTH);
      getContentPane().add(container, BorderLayout.CENTER);
      container.setLayout(containerBorderLayout);
      
      JMenuBar menuBar = new JMenuBar();
      menuBar.add(menuConfiguration.getFileMenu());
      
      int keyModifier = Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx();
      
      JMenu editMenu = new JMenu(Messages.getMessage(Name.EDIT_MENU));
      menuBar.add(editMenu);
      
      JMenuItem cut = new JMenuItem();
      cut.setAction(actionBroker.createAction(ActionBroker.CUT));
      cut.setText(Messages.getMessage(Name.CUT_MENU));
      cut.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, keyModifier));

      JMenuItem copy = new JMenuItem();
      copy.setAction(actionBroker.createAction(ActionBroker.COPY));
      copy.setText(Messages.getMessage(Name.COPY_MENU));
      copy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, keyModifier));

      JMenuItem paste = new JMenuItem();
      paste.setAction(actionBroker.createAction(ActionBroker.PASTE));
      paste.setText(Messages.getMessage(Name.PASTE_MENU));
      paste.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, keyModifier));

      editMenu.add(cut);
      editMenu.add(copy);
      editMenu.add(paste);

      textFieldActions.install(getRootPane());

      JMenu viewMenu = menuConfiguration.getViewMenu();
      if (viewMenu != null) {
        menuBar.add(viewMenu);
      }

      JMenu helpMenu = menuConfiguration.getHelpMenu();
      if (helpMenu != null) {
        menuBar.add(helpMenu);
      }
      
      setJMenuBar(menuBar);
    }
   
    closeAction = new AbstractAction() {
      @Override public void actionPerformed(ActionEvent event) {
        if (saveBeforeClosing()) {
          dispose();
        }
      }
    };
    
    addWindowListener(new WindowAdapter() {
      @Override public void windowClosing(WindowEvent event) {
        if (saveBeforeClosing()) {
          try {
            dispose();
          } catch (IndexOutOfBoundsException e) {
            // Hack to attempt to patch around
            // https://bitbucket.org/scythebill/scythebill-birdlist/issues/62/indexoutofboundsexception-while-closing
            // Never reproduced this myself, but it looks like *something* weird is going on with the JLayeredPane
            // in some invocations of dispose() (with something reacting to the dispose() call by deleting something
            // that then breaks the removeNotify() iteration?)  So see what happens if there's just a bonus call to
            // dispose()
            dispose();
          }
        }
      }
      
      @Override public void windowActivated(WindowEvent event) {
        publishActions();
        otherFileLoader.register();
        checkFileModified();
      }

      @Override public void windowDeactivated(WindowEvent event) {
        unpublishActions();
        otherFileLoader.unregister();
      }
    });

    // Attach the MacOS client property to set the file type of the window
    getRootPane().putClientProperty("Window.documentFile", reportSetFile);
  }

  @Override
  public JPanel navigateTo(String target) {
    JPanel panel = injector.getInstance(Key.get(JPanel.class, Names.named(target)));
    if (!navigateTo(panel)) {
      return null;
    }
    return panel;
  }
  
  @Override
  public void complete(ActionEvent evt) {
    if (this.onCompletion != null) {
      this.onCompletion.actionPerformed(evt);
      this.onCompletion = null;
    } else {
      // On completion, navigate to main if there's no explicitly registered action. 
      navigateTo("main");
    }
  }

  @Override
  public JPanel navigateToAndPush(String target, Action onCompletion) {
    JPanel panel = injector.getInstance(Key.get(JPanel.class, Names.named(target)));
    if (!navigateTo(panel)) {
      return null;
    }
    this.onCompletion = onCompletion;
    return panel;
  }
  
  @Override
  public boolean navigateTo(JPanel panel) {
    if (!reportSetSaver.beforeNavigation()) {
      return false;
    }
    
    checkFileModified();

    // If the current panel is not a "shrink-to-fit" panel, then store off its size.
    if (!(currentPanel instanceof ShrinkToFit)) {
      lastSavedSize = getSize();
    }
    
    container.removeAll();
    container.add(panel, BorderLayout.CENTER);
    currentPanel = panel;
    
    updateTitle();
    
    Dimension preferredSize = getPreferredSize();
    if (panel instanceof ShrinkToFit) {
      // If the current panel is a "shrink-to-fit" panel, then size down to the
      // preferred size.
      setSize(preferredSize);
    } else {
      // Otherwise, set the size to the max of the preferred size and the last saved size
      setSize(
          Math.max(preferredSize.width, lastSavedSize.width),
          Math.max(preferredSize.height, lastSavedSize.height));
    }
    
    UIUtils.keepWindowOnScreen(this);
    validate();
    
    // Move the focus into the first available component within that panel
    FocusTraversalPolicy traversalPolicy = getFocusTraversalPolicy();
    if (traversalPolicy != null) {
      Component firstComponent = traversalPolicy.getFirstComponent(panel);
      if (firstComponent != null) {
        firstComponent.requestFocusInWindow();
      }
    }
    
    return true;
  }

  @Override
  public void updateTitle() {
    String fileName = reportSetFile.getName();
    if (fileName.lastIndexOf('.') > 0) {
      fileName = fileName.substring(0, fileName.lastIndexOf('.'));
    }
    Component panel = container.getComponent(0);
    String baseTitle = String.format("Scythebill %s - %s", versionInfo, fileName);
    String title;
    if (panel instanceof Titled) {
      title = String.format("%s - %s", baseTitle, ((Titled) panel).getTitle()); 
    } else {
      title = baseTitle;
    }
    
    setTitle(title);
  }

  /**
   * Saves content associated with the window before closing it.
   * 
   * @return false if the window should not be closed.
   */
  public boolean saveBeforeClosing() {
    if (reportSetSaver.getDirty().isDirty()) {
      int option = alerts.showConfirm(this,
          Name.UNSAVED_CHANGES_TITLE,
          Name.UNSAVED_CHANGES_MESSAGE);
      if (option == JOptionPane.CANCEL_OPTION) {
        // Cancel - don't quit!
        return false;
      } else if (option == JOptionPane.YES_OPTION) {
        // Yes - try to save.  If that doesn't work, don't quit.
        try {
          reportSetSaver.save();
        } catch (IOException ioe) {
          FileDialogs.showFileSaveError(alerts, ioe, reportSetSaver.file());
          return false;
        }
      }
    }
    
    return true;
  }

  @Override
  public void dispose() {
    saveIfDirtyFuture.cancel(false);
    fontManager.removeListener(fontsUpdatedListener);
    super.dispose();
  }
  
  public ToolbarManager getToolbarManager() {
    return toolbar;
  }

  /** Layout the toolbar region, which always has a taxonomy chooser, and may have additional components. */
  private void layoutToolbar() {
    toolbarPanel.removeAll();
    
    BoxLayout layout = new BoxLayout(toolbarPanel, BoxLayout.X_AXIS);
    toolbarPanel.setLayout(layout);
    toolbarPanel.add(taxonomyChooserPanel);
    toolbarPanel.add(Box.createGlue());
    for (JComponent component : toolbarComponents) {
      toolbarPanel.add(component);
    }
  }
  
  /** ToolbarManager implementation. */
  private class Toolbar implements ToolbarManager {
    @Override
    public void addComponent(final JComponent component, final JComponent listenForRemoval) {
      toolbarComponents.add(component);
      // Remove the toolbar component once its referred-to component is removed
      final AtomicReference<AncestorListener> reference = new AtomicReference<AncestorListener>();
      AncestorListener listener = new AncestorListener() {
        @Override public void ancestorRemoved(AncestorEvent e) {
          toolbarComponents.remove(component);
          layoutToolbar();
          AncestorListener ancestorListener = reference.get();
          if (ancestorListener != null) {
            listenForRemoval.removeAncestorListener(ancestorListener);
          }
        }
        
        @Override public void ancestorMoved(AncestorEvent e) {}
        @Override public void ancestorAdded(AncestorEvent e) {}
      };
      reference.set(listener);
      listenForRemoval.addAncestorListener(listener);
      layoutToolbar();
    }
  }

  @Subscribe
  public void taxonomyChanged(TaxonomyChangedEvent event) {
    publishActions();
  }
  
  private void publishActions() {
    actionBroker.publishAction(ActionBroker.SAVE, saveAction);
    actionBroker.publishAction(ActionBroker.SAVE_A_COPY_AS, saveACopyAsAction);
    actionBroker.publishAction(ActionBroker.CLOSE, closeAction);
    actionBroker.publishAction(ActionBroker.EXPORT, exportAction);
    actionBroker.publishAction(ActionBroker.FULL_EXPORT, fullExportAction);
    actionBroker.publishAction(ActionBroker.IMPORT, importAction);
    actionBroker.publishAction(ActionBroker.MANAGE_TAXONOMIES, manageTaxonomies);
    actionBroker.publishAction(ActionBroker.OPEN_CONTAINING, openContainingAction);
    if (taxonomyStore.isBirdTaxonomy()) {
      actionBroker.publishAction(ActionBroker.IMPORT_CHECKLISTS, importChecklistsAction);
      actionBroker.publishAction(ActionBroker.IDENTIFY_CHECKLIST_ERRORS,
          identifyChecklistErrorsAction);
      actionBroker.publishAction(ActionBroker.IDENTIFY_CHECKLIST_RARITIES,
          identifyChecklistRaritiesAction);
      actionBroker.publishAction(ActionBroker.RECONCILE_AGAINST_CHECKLISTS,
          reconcileAgainstChecklistsAction);
    } else {
      ExtendedTaxonomyChecklists extendedTaxonomyChecklists = reportSet.getExtendedTaxonomyChecklist(taxonomyStore.getTaxonomy().getId());
      boolean hasChecklists = extendedTaxonomyChecklists != null
          && extendedTaxonomyChecklists.getChecklists() != null
          && !extendedTaxonomyChecklists.getChecklists().isEmpty();

      if (hasChecklists) {
        actionBroker.publishAction(ActionBroker.IDENTIFY_CHECKLIST_ERRORS,
            identifyChecklistErrorsAction);
        actionBroker.publishAction(ActionBroker.IDENTIFY_CHECKLIST_RARITIES,
            identifyChecklistRaritiesAction);
      } else {
        actionBroker.unpublishAction(ActionBroker.IDENTIFY_CHECKLIST_ERRORS,
            identifyChecklistErrorsAction);
        actionBroker.unpublishAction(ActionBroker.IDENTIFY_CHECKLIST_RARITIES,
            identifyChecklistRaritiesAction);
      }
      actionBroker.unpublishAction(ActionBroker.RECONCILE_AGAINST_CHECKLISTS,
          reconcileAgainstChecklistsAction);
      actionBroker.unpublishAction(ActionBroker.IMPORT_CHECKLISTS, importChecklistsAction);
    }
    actionBroker.publishAction(ActionBroker.PREFERENCES, preferencesAction);
  }
  
  private void unpublishActions() {
    actionBroker.unpublishAction(ActionBroker.SAVE, saveAction);
    actionBroker.unpublishAction(ActionBroker.SAVE_A_COPY_AS, saveACopyAsAction);
    actionBroker.unpublishAction(ActionBroker.CLOSE, closeAction);
    actionBroker.unpublishAction(ActionBroker.EXPORT, exportAction);
    actionBroker.unpublishAction(ActionBroker.FULL_EXPORT, fullExportAction);
    actionBroker.unpublishAction(ActionBroker.IMPORT, importAction);
    actionBroker.unpublishAction(ActionBroker.MANAGE_TAXONOMIES, manageTaxonomies);
    actionBroker.unpublishAction(ActionBroker.IMPORT_CHECKLISTS, importAction);
    actionBroker.unpublishAction(ActionBroker.OPEN_CONTAINING, openContainingAction);
    actionBroker.unpublishAction(ActionBroker.IDENTIFY_CHECKLIST_ERRORS,
        identifyChecklistErrorsAction);
    actionBroker.unpublishAction(ActionBroker.IDENTIFY_CHECKLIST_RARITIES,
        identifyChecklistRaritiesAction);
    actionBroker.unpublishAction(ActionBroker.RECONCILE_AGAINST_CHECKLISTS,
        reconcileAgainstChecklistsAction);
    actionBroker.unpublishAction(ActionBroker.PREFERENCES, preferencesAction);
  }

  private void checkFileModified() {
    if (reportSetSaver.hasBeenModifiedElsewhere()) {
      if (alerts.showYesNo(this, Name.FILE_HAS_BEEN_MODIFIED_TITLE,
          reportSetSaver.getDirty().isDirty()
          ? Name.FILE_HAS_BEEN_MODIFIED_AND_DIRTY_MESSAGE
          : Name.FILE_HAS_BEEN_MODIFIED_MESSAGE,
          HtmlEscapers.htmlEscaper().escape(reportSetFile.getName())) ==
          JOptionPane.YES_OPTION) {
        frameRegistry.reload(reportSetFile, this);
      }
    }

  }
}
