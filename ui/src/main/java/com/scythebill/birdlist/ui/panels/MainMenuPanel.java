/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;

import javax.annotation.Nullable;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.google.inject.Inject;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.StoredQueriesPanel;
import com.scythebill.birdlist.ui.util.VisibilityDetector;

/**
 * Panel for the top-level, per-list menu. 
 */
public class MainMenuPanel extends JPanel implements FontsUpdatedListener, ShrinkToFit {
  private JButton enterSightings;
  private JButton reportButton;
  private JButton browseButton;
  private JButton browseLocationsButton;
  private JButton extendedReportsButton;
  private final NavigableFrame navigableFrame;
  private final FontManager fontManager;
  private JPanel centerBox;
  private JLabel sightingsLabel;
  private JLabel reportsLabel;
  private JLabel browseLabel;
  private JLabel browseLocationsLabel;
  private JLabel extendedReportsLabel;
  private StoredQueriesPanel storedQueriesPanel;
  private MenuButton preferencesButton;
  private JLabel preferencesLabel;

  @Inject
  public MainMenuPanel(
      NavigableFrame navigableFrame,
      FontManager fontManager,
      StoredQueriesPanel storedQueriesPanel,
      VisibilityDetector visibilityDetector) {
    this.navigableFrame = navigableFrame;
    this.fontManager = fontManager;
    this.storedQueriesPanel = storedQueriesPanel;
    // For now, the sidebar image is always null - need to play around with better layouts
    initGUI(null);
    visibilityDetector.install(preferencesButton);
  }
  
  static class MenuButton extends JButton {
    MenuButton() {
      putClientProperty("Quaqua.Button.style", "bevel");
      putClientProperty(FontManager.BUTTON_SIZE_PROPERTY, FontManager.ButtonSize.MEDIUM_BOLD);
    }
  }
  
  private void initGUI(@Nullable final BufferedImage sidebarImage) {
    
    setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
    
    centerBox = new JPanel();
    centerBox.setBorder(new EmptyBorder(20, 60, 20, 60));
    if (!storedQueriesPanel.isEmpty()) {
      add(storedQueriesPanel);
      storedQueriesPanel.setBorder(new EmptyBorder(20, 60, 20, 60));
      storedQueriesPanel.setAlignmentX(0.5f);
    }
    add(centerBox);
    centerBox.setAlignmentX(0.5f);
    
//    if (sidebarImage != null) {
//      JPanel boxPanel = new JPanel();
//      boxPanel.setLayout(new BoxLayout(boxPanel, BoxLayout.Y_AXIS));
//      boxPanel.add(new JLabel(new ImageIcon(sidebarImage)));
//      boxPanel.setOpaque(true);
//      boxPanel.setBackground(Color.BLACK);
//      add(BorderLayout.LINE_START, boxPanel);
//    }
    enterSightings = new MenuButton();
    enterSightings.setAction(getNavigationAction("sightings"));
    enterSightings.setText(Messages.getMessage(Name.ENTER_SIGHTINGS));
    reportButton = new MenuButton();
    reportButton.setAction(getNavigationAction("reports"));
    reportButton.setText(Messages.getMessage(Name.SHOW_REPORTS));
    
    browseButton = new MenuButton();
    browseButton.setAction(getNavigationAction("browse"));
    browseButton.setText(Messages.getMessage(Name.BROWSE_BY_SPECIES));

    browseLocationsButton = new MenuButton();
    browseLocationsButton.setAction(getNavigationAction("browseLocations"));
    browseLocationsButton.setText(Messages.getMessage(Name.BROWSE_BY_LOCATION));

    extendedReportsButton = new MenuButton();
    extendedReportsButton.setAction(getNavigationAction("extendedReportsMenu"));
    extendedReportsButton.setText(Messages.getMessage(Name.SPECIAL_REPORTS));

    preferencesButton = new MenuButton();
    preferencesButton.setAction(getNavigationAction("preferences"));
    preferencesButton.setText(Messages.getMessage(Name.PREFERENCES));

    sightingsLabel = new JLabel(Messages.getMessage(Name.ENTER_SIGHTINGS_EXPLANATION));
    sightingsLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    reportsLabel = new JLabel(Messages.getMessage(Name.SHOW_REPORTS_EXPLANATION));
    reportsLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    browseLabel = new JLabel(Messages.getMessage(Name.BROWSE_BY_SPECIES_EXPLANATION));
    browseLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    browseLocationsLabel = new JLabel(Messages.getMessage(Name.BROWSE_BY_LOCATION_EXPLANATION));
    browseLocationsLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    extendedReportsLabel = new JLabel(Messages.getMessage(Name.SPECIAL_REPORTS_EXPLANATION));
    extendedReportsLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    preferencesLabel = new JLabel(Messages.getMessage(Name.PREFERENCES_EXPLANATION));
    preferencesLabel.putClientProperty(FontManager.PLAIN_LABEL, true);

    fontManager.applyTo(this);
  }
  
  private Action getNavigationAction(final String navigateTarget) {
    return new AbstractAction() {
      @Override public void actionPerformed(ActionEvent evt) {
        navigableFrame.navigateTo(navigateTarget);
      }
    };
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {    
    GroupLayout layout = new GroupLayout(centerBox);
    centerBox.setLayout(layout);

    int buttonWidth = fontManager.scale(194);
    int buttonHeight = fontManager.scale(64);
    layout.setHorizontalGroup(layout.createSequentialGroup()
      .addContainerGap()
      .addGroup(layout.createParallelGroup()
          .addComponent(enterSightings, Alignment.LEADING, PREFERRED_SIZE, buttonWidth, PREFERRED_SIZE)
          .addComponent(reportButton, Alignment.LEADING, PREFERRED_SIZE, buttonWidth, PREFERRED_SIZE)
          .addComponent(browseButton, Alignment.LEADING, PREFERRED_SIZE, buttonWidth, PREFERRED_SIZE)
          .addComponent(browseLocationsButton, Alignment.LEADING, PREFERRED_SIZE, buttonWidth, PREFERRED_SIZE)
          .addComponent(extendedReportsButton, Alignment.LEADING, PREFERRED_SIZE, buttonWidth, PREFERRED_SIZE)
          .addComponent(preferencesButton, Alignment.LEADING, PREFERRED_SIZE, buttonWidth, PREFERRED_SIZE))
      .addGap(63)
      .addGroup(layout.createParallelGroup()
          .addComponent(sightingsLabel)
          .addComponent(reportsLabel)
          .addComponent(browseLabel)
          .addComponent(browseLocationsLabel)
          .addComponent(extendedReportsLabel)
          .addComponent(preferencesLabel))
      .addContainerGap(67, 67));
    layout.setVerticalGroup(layout.createSequentialGroup()
      .addContainerGap()
      .addGroup(layout.createBaselineGroup(false, false)
          .addComponent(enterSightings, Alignment.BASELINE, PREFERRED_SIZE, buttonHeight, PREFERRED_SIZE)
          .addComponent(sightingsLabel, Alignment.BASELINE, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
      .addGap(14)
      .addGroup(layout.createBaselineGroup(false, false)
          .addComponent(reportButton, Alignment.BASELINE, PREFERRED_SIZE, buttonHeight, PREFERRED_SIZE)
          .addComponent(reportsLabel, Alignment.BASELINE, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
      .addGap(14)
      .addGroup(layout.createBaselineGroup(false, false)
          .addComponent(browseButton, Alignment.BASELINE, PREFERRED_SIZE, buttonHeight, PREFERRED_SIZE)
          .addComponent(browseLabel, Alignment.BASELINE, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
      .addGap(14)
      .addGroup(layout.createBaselineGroup(false, false)
          .addComponent(browseLocationsButton, Alignment.BASELINE, PREFERRED_SIZE, buttonHeight, PREFERRED_SIZE)
          .addComponent(browseLocationsLabel, Alignment.BASELINE, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
      .addGap(14)
      .addGroup(layout.createBaselineGroup(false, false)
          .addComponent(extendedReportsButton, Alignment.BASELINE, PREFERRED_SIZE, buttonHeight, PREFERRED_SIZE)
          .addComponent(extendedReportsLabel, Alignment.BASELINE, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
      .addGap(14)
      .addGroup(layout.createBaselineGroup(false, false)
          .addComponent(preferencesButton, Alignment.BASELINE, PREFERRED_SIZE, buttonHeight, PREFERRED_SIZE)
          .addComponent(preferencesLabel, Alignment.BASELINE, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
      .addContainerGap(82, 82));
    layout.linkSize(SwingConstants.HORIZONTAL,  enterSightings, reportButton, extendedReportsButton, browseButton, preferencesButton);
    layout.linkSize(SwingConstants.VERTICAL, enterSightings, reportButton, extendedReportsButton, browseButton, preferencesButton);

    //    centerBox.setPreferredSize(fontManager.scale(new Dimension(600, 400)));
    centerBox.setMinimumSize(centerBox.getPreferredSize());
  }
}
