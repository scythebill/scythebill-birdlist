/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FocusTraversalPolicy;
import java.awt.Image;
import java.awt.event.ActionEvent;

import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.ui.actions.MenuConfiguration;
import com.scythebill.birdlist.ui.actions.NewReportSetAction;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.guice.Scythebill;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Frame for displaying Open../New../Import...
 */
public class OpenOrNewListFrame extends JFrame implements NavigableFrame {

  private final Injector injector;
  private final FontManager fontManager;
  private final FontsUpdatedListener fontsUpdatedListener = new FontsUpdatedListener() {
    @Override public void fontsUpdated(FontManager fontManager) {
      fontManager.applyTo(OpenOrNewListFrame.this);
      pack();
      UIUtils.keepWindowOnScreen(OpenOrNewListFrame.this);
    }
  };

  @Inject
  public OpenOrNewListFrame(
      MenuConfiguration menuConfiguration,
      Injector injector,
      NewReportSetAction newReportSetAction,
      ReportSet reportSet,
      FontManager fontManager,
      @Scythebill Image image) {
    this.injector = injector;
    this.fontManager = fontManager;
    setTitle("Scythebill");
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    getContentPane().setLayout(new BorderLayout());
    
    JMenuBar menuBar = new JMenuBar();
    menuBar.add(menuConfiguration.getFileMenu());
    JMenu viewMenu = menuConfiguration.getViewMenu();
    if (viewMenu != null) {
      menuBar.add(viewMenu);
    }

    JMenu helpMenu = menuConfiguration.getHelpMenu();
    if (helpMenu != null) {
      menuBar.add(helpMenu);
    }
    setJMenuBar(menuBar);
    fontManager.addListener(fontsUpdatedListener);
    
    setIconImage(image);
  }
  
  public void start() {
    navigateTo("main");
    setLocationByPlatform(true);
    setVisible(true);
  }
  
  @Override
  public JPanel navigateTo(String target) {
    JPanel panel = injector.getInstance(Key.get(JPanel.class, Names.named(target)));
    navigateTo(panel);
    return panel;
  }

  @Override
  public JPanel navigateToAndPush(String target, Action onCompletion) {
    throw new UnsupportedOperationException("Not yet implemented for OpenOrNew");
  }
  
  @Override
  public void complete(ActionEvent evt) {
    navigateTo("main");
  }

  @Override
  public boolean navigateTo(JPanel panel) {
    getContentPane().removeAll();
    getContentPane().add(panel, BorderLayout.CENTER);
    pack();

    FocusTraversalPolicy traversalPolicy = getFocusTraversalPolicy();
    if (traversalPolicy != null) {
      Component firstComponent = traversalPolicy.getFirstComponent(panel);
      if (firstComponent != null) {
        firstComponent.requestFocusInWindow();
      }
    }
    
    return true;
  }

  @Override
  public void updateTitle() {
  }

  @Override
  public void dispose() {
    super.dispose();
    fontManager.removeListener(fontsUpdatedListener);
  }
}
