/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.awt.Dimension;

import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

import com.google.inject.Inject;
import com.scythebill.birdlist.ui.actions.NewReportSetAction;
import com.scythebill.birdlist.ui.actions.OpenAction;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.imports.ImportAction;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Panel for the a top-level menu to be displayed when there are no
 * MainFrames visible.  Allows the user to open an existing list or
 * create a new one.  Also possible with the FileMenu, but this is
 * more in-your-face about it.
 * 
 * TODO: the user feedback on clicking Open/New is undesirable.
 * No progress bar, busy cursor, etc.  Just sits there, then boom
 * the main window shows.
 */
public class OpenOrNewListPanel extends JPanel implements FontsUpdatedListener {
  private final FontManager fontManager;
  private JTextPane text;
  private JPanel centerBox;
  private JButton openListButton;
  private JButton newListButton;
  private JButton importButton;
  private JLabel openListLabel;
  private JLabel newListLabel;
  private JLabel importListLabel;

  @Inject
  public OpenOrNewListPanel(
      FontManager fontManager,
      OpenAction openAction,
      NewReportSetAction newAction,
      ImportAction importAction) {
    this.fontManager = fontManager;
    initGUI(openAction, newAction, importAction);
  }
  
  private void initGUI(Action openAction, Action newAction, Action importAction) {
    setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

    text = new JTextPane();
    text.setContentType("text/html");
    text.setText("<html><b>"
        + Messages.getMessage(Name.WELCOME_TO_SCYTHEBILL)
        + "</b><br><br>"
        + Messages.getMessage(Name.WELCOME_MESSAGE));
    text.putClientProperty(FontManager.TEXT_SIZE_PROPERTY, FontManager.TextSize.LARGE);
    text.putClientProperty(JTextPane.HONOR_DISPLAY_PROPERTIES, true);
    text.setOpaque(false);
    text.setEditable(false);
    add(text);
    
    centerBox = new JPanel();
    add(centerBox);
    
    openListButton = new JButton();
    openListButton.setAction(openAction);
    openListButton.setText(Messages.getMessage(Name.OPEN_FILE));
    openListButton.putClientProperty("Quaqua.Button.style", "bevel");
    
    newListButton = new JButton();
    newListButton.setAction(newAction);
    newListButton.setText(Messages.getMessage(Name.NEW_FILE));
    newListButton.putClientProperty("Quaqua.Button.style", "bevel");

    importButton = new JButton();
    importButton.setAction(importAction);
    importButton.setText(Messages.getMessage(Name.IMPORT_SIGHTINGS_MENU));
    importButton.putClientProperty("Quaqua.Button.style", "bevel");
    
    openListLabel = new JLabel(Messages.getMessage(Name.OPEN_FILE_EXPLANATION));
    openListLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    
    newListLabel = new JLabel(Messages.getMessage(Name.NEW_FILE_EXPLANATION));
    newListLabel.putClientProperty(FontManager.PLAIN_LABEL, true);

    importListLabel = new JLabel(Messages.getMessage(Name.IMPORT_RECORDS_EXPLANATION));
    importListLabel.putClientProperty(FontManager.PLAIN_LABEL, true);

    fontManager.applyTo(this);
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    setBorder(new EmptyBorder(fontManager.scale(20), fontManager.scale(40),
        fontManager.scale(20), fontManager.scale(40)));
    centerBox.setBorder(new EmptyBorder(fontManager.scale(10), 0, 0, 0));

    text.setPreferredSize(new Dimension(fontManager.scale(300), fontManager.scale(100)));
    GroupLayout thisLayout = new GroupLayout(centerBox);
    centerBox.setLayout(thisLayout);
    thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
      .addContainerGap()
      .addGroup(thisLayout.createParallelGroup()
          .addComponent(openListButton, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 154, GroupLayout.PREFERRED_SIZE)
          .addComponent(newListButton, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 154, GroupLayout.PREFERRED_SIZE)
          .addComponent(importButton, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 154, GroupLayout.PREFERRED_SIZE))
      .addGap(fontManager.scale(63))
      .addGroup(thisLayout.createParallelGroup()
          .addComponent(openListLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
          .addComponent(newListLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
          .addComponent(importListLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
      .addContainerGap(fontManager.scale(67), fontManager.scale(67)));
    thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
      .addContainerGap()
      .addGroup(thisLayout.createBaselineGroup(false, false)
          .addComponent(openListButton, GroupLayout.PREFERRED_SIZE, fontManager.scale(64), GroupLayout.PREFERRED_SIZE)
          .addComponent(openListLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
      .addGap(fontManager.scale(14))
      .addGroup(thisLayout.createBaselineGroup(false, false)
          .addComponent(newListButton, GroupLayout.PREFERRED_SIZE, fontManager.scale(64), GroupLayout.PREFERRED_SIZE)
          .addComponent(newListLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
      .addGap(fontManager.scale(14))
      .addGroup(thisLayout.createBaselineGroup(false, false)
          .addComponent(importButton, GroupLayout.PREFERRED_SIZE, fontManager.scale(64), GroupLayout.PREFERRED_SIZE)
          .addComponent(importListLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
      .addContainerGap(fontManager.scale(82), fontManager.scale(82)));
    thisLayout.linkSize(openListButton, newListButton, importButton);
  }  
}
