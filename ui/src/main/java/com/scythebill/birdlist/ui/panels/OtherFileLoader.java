/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.xml.sax.SAXException;

import com.google.common.base.Charsets;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.scythebill.birdlist.ui.actions.ImportChecklistsAction;
import com.scythebill.birdlist.ui.actions.ReturnAction;
import com.scythebill.birdlist.ui.app.OtherFileLoaderRegistry;
import com.scythebill.birdlist.ui.imports.ImportMenuPanel;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.taxonomy.ExtendedTaxonomyAdder;
import com.scythebill.birdlist.ui.taxonomy.ExtendedTaxonomyXmlImporter;
import com.scythebill.birdlist.ui.taxonomy.TaxonomyWithChecklists;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * Loads "other" files - either shared checklists, eBird imports, or taxonomies.
 */
class OtherFileLoader {
  private final OtherFileLoaderRegistry registry;
  private final ImportChecklistsAction importChecklistsAction;
  private final Provider<ImportMenuPanel> menuPanelProvider;
  private final ActionListener listener = new ActionListener() {    
    @Override public void actionPerformed(ActionEvent e) {
      List<File> files = registry.getAndClear();
      if (!files.isEmpty()) {
        loadArbitraryCsvFiles(files);
      }
    }
  };
  private final Action returnAction;
  private final ExtendedTaxonomyAdder extendedTaxonomyAdder;
  private final Alerts alerts;

  @Inject
  OtherFileLoader(
      OtherFileLoaderRegistry registry,
      ImportChecklistsAction importChecklistsAction,
      ReturnAction returnAction,
      ExtendedTaxonomyAdder extendedTaxonomyAdder,
      Alerts alerts,
      Provider<ImportMenuPanel> menuPanelProvider) {
    this.registry = registry;
    this.importChecklistsAction = importChecklistsAction;
    this.returnAction = returnAction;
    this.extendedTaxonomyAdder = extendedTaxonomyAdder;
    this.alerts = alerts;
    // Provider, to avoid instantiating this panel aggressively
    this.menuPanelProvider = menuPanelProvider;
  }
  
  /** Register the file loader. */
  public void register() {
    registry.addActionListener(listener);
    // If there's any pending files, load them now.
    listener.actionPerformed(null);
  }

  /** Unregister the file loader. */
  public void unregister() {
    registry.removeActionListener(listener);
  }

  /**
   * Loads arbitrary CSV files.  Might be a checklist, might be an eBird import.
   */
  private void loadArbitraryCsvFiles(List<File> files) {
    List<File> checklists = Lists.newArrayList();
    List<File> notChecklists = Lists.newArrayList();
    List<File> taxonomyFiles = Lists.newArrayList();
    
    for (File file : files) {
      if (file.getName().endsWith(ExtendedTaxonomyXmlImporter.FILE_SUFFIX)) {
        taxonomyFiles.add(file);
      } else if (importChecklistsAction.isClementsChecklist(file)) {
        checklists.add(file);
      } else {
        notChecklists.add(file);
      }
    }
    
    // Import anything that's a checklist
    if (!checklists.isEmpty()) {
      importChecklistsAction.importFiles(checklists.toArray(new File[0]));
    }
    
    for (File taxonomyFile : taxonomyFiles) {
      try (Reader in = Files.newReader(taxonomyFile, Charsets.UTF_8)) {
        TaxonomyWithChecklists taxonomy = new ExtendedTaxonomyXmlImporter().readExtendedTaxonomy(in);
        extendedTaxonomyAdder.addTaxonomy(null, taxonomy);
      } catch (IOException | SAXException e) {
        alerts.showError(null,
            Name.IMPORT_FAILED,
            Name.IMPORTING_TAXONOMY_FAILED_FORMAT,
            e.getMessage());
      }
    }
    
    // .. and import all the (hopefully) eBird files.
    if (!notChecklists.isEmpty()) {
      // Start the first eBird;  subsequent ones will run when the first completes.
      new ImportOneEBirdFileAction(notChecklists).actionPerformed(null);
    }
  }
  
  private class ImportOneEBirdFileAction extends AbstractAction {
    private Iterable<File> ebirdFiles;

    public ImportOneEBirdFileAction(Iterable<File> ebirdFiles) {
      this.ebirdFiles = ebirdFiles;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      // The next file to import 
      File next = ebirdFiles.iterator().next();
      Iterable<File> remaining = Iterables.skip(ebirdFiles, 1);
      // An Action defining what to do when that import succeeds 
      Action continuation;
      if (Iterables.isEmpty(remaining)) {
        continuation = returnAction;
      } else {
        continuation = new ImportOneEBirdFileAction(remaining);
      }
      
      ImportMenuPanel importMenuPanel = menuPanelProvider.get();
      importMenuPanel.importEBirdFile(next, continuation);
    }
  }
}
