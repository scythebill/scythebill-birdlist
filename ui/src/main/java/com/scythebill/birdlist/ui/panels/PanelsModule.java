/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import javax.swing.JPanel;

import com.google.inject.AbstractModule;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.backup.BackupPreferences;
import com.scythebill.birdlist.ui.components.VisitInfoPreferences;
import com.scythebill.birdlist.ui.imports.ImportMenuPanel;
import com.scythebill.birdlist.ui.panels.location.ChecklistPrintPreferences;
import com.scythebill.birdlist.ui.panels.location.ChecklistSpreadsheetPreferences;
import com.scythebill.birdlist.ui.panels.location.LocationBrowsePanel;
import com.scythebill.birdlist.ui.panels.reports.BigDayPanel;
import com.scythebill.birdlist.ui.panels.reports.ExtendedReportMenuPanel;
import com.scythebill.birdlist.ui.panels.reports.FamilyReportPreferences;
import com.scythebill.birdlist.ui.panels.reports.ReportPrintPreferences;
import com.scythebill.birdlist.ui.panels.reports.ReportSpreadsheetPreferences;
import com.scythebill.birdlist.ui.panels.reports.ReportsPanel;
import com.scythebill.birdlist.ui.panels.reports.ResolveTaxaPanel;
import com.scythebill.birdlist.ui.panels.reports.SplitsAndLumpsPanel;
import com.scythebill.birdlist.ui.panels.reports.StoredQueries;
import com.scythebill.birdlist.ui.panels.reports.StoredQueriesPreferences;
import com.scythebill.birdlist.ui.panels.reports.TotalTicksPanel;
import com.scythebill.birdlist.ui.panels.reports.TripReportPreferences;
import com.scythebill.birdlist.ui.panels.reports.YearComparisonPanel;
import com.scythebill.birdlist.ui.prefs.PreferencesManager;
import com.scythebill.birdlist.ui.taxonomy.TaxonomyManagementPanel;

/** 
 * Module for establishing panel bindings. 
 */
public class PanelsModule extends AbstractModule {
  @Override
  protected void configure() {
    bindPanel(MainMenuPanel.class, "main");
    bindPanel(ReportsPanel.class, "reports");
    bindPanel(ResolveTaxaPanel.class, "resolveTaxa");
    bindPanel(SingleLocationEditPanel.class, "sightings");
    bindPanel(MainBrowsePanel.class, "browse");
    bindPanel(LocationBrowsePanel.class, "browseLocations");
    bindPanel(ImportMenuPanel.class, "importMenu");
    bindPanel(TaxonomyManagementPanel.class, "manageTaxonomies");
    bindPanel(PreferencesPanel.class, "preferences");
    bindPanel(ExtendedReportMenuPanel.class, "extendedReportsMenu");
    bindPanel(BigDayPanel.class, "bigDay");
    bindPanel(TotalTicksPanel.class, "totalTicks");
    bindPanel(SplitsAndLumpsPanel.class, "splitsAndLumps");
    bindPanel(YearComparisonPanel.class, "yearComparison");
    
    bind(MainFrame.class).in(Singleton.class);
    // Reuse the MainFrame binding as a NavigableFrame binding
    bind(Key.get(NavigableFrame.class)).to(MainFrame.class);
    
    bind(StoredQueries.class).in(Singleton.class);
  }
  
  @Provides
  public BrowsePreferences provideBrowsePreferences(PreferencesManager manager) {
    return manager.getPreference(BrowsePreferences.class);
  }

  @Provides
  public DataEntryPreferences provideDataEntryPreferences(PreferencesManager manager) {
    return manager.getPreference(DataEntryPreferences.class);
  }

  // TODO: move these preferences out of this class
  
  @Provides
  public VisitInfoPreferences provideVisitInfoPreferences(PreferencesManager manager) {
    return manager.getPreference(VisitInfoPreferences.class);
  }

  @Provides
  public ReportSpreadsheetPreferences provideReportSpreadsheetPreferences(PreferencesManager manager) {
    return manager.getPreference(ReportSpreadsheetPreferences.class);
  }

  @Provides
  public TripReportPreferences provideTripReportPreferences(PreferencesManager manager) {
    return manager.getPreference(TripReportPreferences.class);
  }

  @Provides
  public FamilyReportPreferences provideFamilyReportPreferences(PreferencesManager manager) {
    return manager.getPreference(FamilyReportPreferences.class);
  }

  @Provides
  public BackupPreferences provideBackupPreferences(PreferencesManager manager) {
    return manager.getPreference(BackupPreferences.class);
  }

  @Provides
  public ReportPrintPreferences provideReportPrintPreferences(PreferencesManager manager) {
    return manager.getPreference(ReportPrintPreferences.class);
  }

  @Provides
  public ChecklistPrintPreferences provideChecklistPrintPreferences(PreferencesManager manager) {
    return manager.getPreference(ChecklistPrintPreferences.class);
  }

  @Provides
  public ChecklistSpreadsheetPreferences provideChecklistSpreadsheetPreferences(PreferencesManager manager) {
    return manager.getPreference(ChecklistSpreadsheetPreferences.class);
  }

  @Provides
  public StoredQueriesPreferences provideStoredQueriesPreferences(PreferencesManager manager) {
    return manager.getPreference(StoredQueriesPreferences.class);
  }

  @Provides
  public ToolbarManager provideToolbarManager(MainFrame mainFrame) {
    return mainFrame.getToolbarManager();
  }
  
  private void bindPanel(Class<? extends JPanel> panelClass, String name) {
    bind(Key.get(JPanel.class, Names.named(name))).to(panelClass);
  }
}