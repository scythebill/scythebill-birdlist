/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.awt.Desktop;
import java.awt.Desktop.Action;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Locale;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.edits.ChosenUsers;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences.AvailableClementsLocale;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences.AvailableIocLocale;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences.ScientificOrCommon;
import com.scythebill.birdlist.model.user.User;
import com.scythebill.birdlist.model.user.UserSet;
import com.scythebill.birdlist.model.util.LocalePreferences;
import com.scythebill.birdlist.ui.actions.ReturnAction;
import com.scythebill.birdlist.ui.app.ReportSetSaver;
import com.scythebill.birdlist.ui.app.Titled;
import com.scythebill.birdlist.ui.backup.BackupFrequency;
import com.scythebill.birdlist.ui.backup.BackupPreferences;
import com.scythebill.birdlist.ui.backup.BackupSaver;
import com.scythebill.birdlist.ui.components.TextLink;
import com.scythebill.birdlist.ui.events.DefaultUserStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryPreferences;
import com.scythebill.birdlist.ui.prefs.ReportSetPreference;
import com.scythebill.birdlist.ui.uptodate.UpToDatePreferences;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;

/**
 * Panel for explicitly specified preferences.
 */
public class PreferencesPanel extends JPanel implements FontsUpdatedListener, Titled {
  private final NamesPreferences namePreferences;
  private final QueryPreferences queryPreferences;
  private final BackupPreferences backupPreferences;
  private final UpToDatePreferences upToDatePreferences;
  private final ReportSetPreference<ChosenUsers> chosenUserPreferences;
  private final BackupSaver backupSaver;
  private final Alerts alerts;
  private JComboBox<AvailableIocLocale> iocLocalesComboBox;
  private JComboBox<AvailableClementsLocale> clementsLocalesComboBox;
  private JComboBox<AvailableUiLocales> uiLocalesComboBox;
  private JButton returnButton;
  private JLabel iocLocalesLabel;
  private JLabel clementsLocalesLabel;
  private JLabel uiLocalesLabel;
  private JComboBox<String> sciOrCommonComboBox;
  private JLabel countability;
  private JCheckBox countIntroduced;
  private JCheckBox countHeardOnly;
  private JCheckBox countUndescribed;
  private JCheckBox countRestrained;
  private JCheckBox showExtinct;
  private JLabel backups;
  private JLabel backupFrequency;
  private JComboBox<BackupFrequency> backupFrequencyCombo;
  private TextLink backupLocation;
  private JButton backupLocationButton;
  private JButton backupNowButton;
  private boolean backupOptionsChanged;
  private final File reportSetFile;
  private final ReportSetSaver reportSetSaver;
  private final ReportSet reportSet;
  private JLabel users;
  private JButton toggleMultipleUsers;
  private final UserSetPanel userSetPanel;
  private final UserDialog userDialog;
  private final DefaultUserStore defaultUserStore;
  private BrowsePreferences browsePreferences;
  
  @Inject
  PreferencesPanel(
      NamesPreferences namePreferences,
      QueryPreferences queryPreferences,
      BackupPreferences backupPreferences,
      ReportSetPreference<ChosenUsers> chosenUserPreferences,
      UpToDatePreferences upToDatePreferences,
      BrowsePreferences browsePreferences,
      BackupSaver backupSaver,
      ReportSet reportSet,
      ReportSetSaver reportSetSaver,
      UserSetPanel userSetPanel,
      UserDialog userDialog,
      DefaultUserStore defaultUserStore,
      FileDialogs fileDialogs,
      Alerts alerts,
      File reportSetFile,
      ReturnAction returnAction,
      FontManager fontManager) {
    this.namePreferences = namePreferences;
    this.queryPreferences = queryPreferences;
    this.backupPreferences = backupPreferences;
    this.chosenUserPreferences = chosenUserPreferences;
    this.upToDatePreferences = upToDatePreferences;
    this.browsePreferences = browsePreferences;
    this.backupSaver = backupSaver;
    this.reportSet = reportSet;
    this.reportSetSaver = reportSetSaver;
    this.userSetPanel = userSetPanel;
    this.userDialog = userDialog;
    this.defaultUserStore = defaultUserStore;
    this.alerts = alerts;
    this.reportSetFile = reportSetFile;
    initComponents(returnAction);
    fontManager.applyTo(this);
  }
  
  public enum AvailableUiLocales {
    DEFAULT(null),
    ENGLISH(new Locale("en")),
    GERMAN(new Locale("de")),
    SPANISH(new Locale("es"));
    
    private final Locale locale;
    
    AvailableUiLocales(Locale locale) {
      this.locale = locale;
    }
    
    public Locale getLocale() {
      return locale;
    }
    
    @Override
    public String toString() {
      if (locale == null) {
        return Messages.getMessage(Name.USE_SYSTEM_LANGUAGE);
      } else {
        return locale.getDisplayName(locale);
      }
    }

    static AvailableUiLocales forLocale(Locale locale) {
      for (AvailableUiLocales uiLocale : AvailableUiLocales.values()) {
        if (uiLocale == DEFAULT) {
          continue;
        }
        
        if (uiLocale.getLocale().equals(locale)) {
          return uiLocale;
        }
      }
      
      return DEFAULT;
    }
  }
  
  private void initComponents(ReturnAction returnAction) {
    clementsLocalesComboBox = new JComboBox<AvailableClementsLocale>(AvailableClementsLocale.values());
    clementsLocalesComboBox.setMaximumRowCount(clementsLocalesComboBox.getItemCount());
    for (AvailableClementsLocale availableLocale : AvailableClementsLocale.values()) {
      if (availableLocale.code().equals(namePreferences.clementsLocale)) {
        clementsLocalesComboBox.setSelectedItem(availableLocale);
        break;
      }
    }
    
    clementsLocalesComboBox.addActionListener(e -> {
      namePreferences.clementsLocale = ((AvailableClementsLocale) clementsLocalesComboBox.getSelectedItem()).code();
    });
    clementsLocalesLabel = new JLabel(
        Messages.getMessage(Name.SPECIES_NAME_EBIRD_CLEMENTS));

    iocLocalesComboBox = new JComboBox<AvailableIocLocale>(AvailableIocLocale.values());
    iocLocalesComboBox.setMaximumRowCount(iocLocalesComboBox.getItemCount());
    for (AvailableIocLocale availableLocale : AvailableIocLocale.values()) {
      if (availableLocale.code().equals(namePreferences.locale)) {
        iocLocalesComboBox.setSelectedItem(availableLocale);
        break;
      }
    }
    
    iocLocalesComboBox.addActionListener(e -> {
      namePreferences.locale = ((AvailableIocLocale) iocLocalesComboBox.getSelectedItem()).code();
    });
    iocLocalesLabel = new JLabel(Messages.getMessage(Name.SPECIES_NAME_IOC));
    
    uiLocalesLabel = new JLabel(Messages.getMessage(Name.LANGUAGE_PREFERENCES_NEEDS_RESTART));
    uiLocalesComboBox = new JComboBox<>(AvailableUiLocales.values());
    if (LocalePreferences.instance().isDefault()) {
      uiLocalesComboBox.setSelectedItem(AvailableUiLocales.DEFAULT);
    } else {
      uiLocalesComboBox.setSelectedItem(AvailableUiLocales.forLocale(LocalePreferences.instance().getSavedLocale()));
    }
    uiLocalesComboBox.addActionListener(e -> {
      AvailableUiLocales uiLocale = (AvailableUiLocales) uiLocalesComboBox.getSelectedItem();
      LocalePreferences.instance().saveLocale(uiLocale.locale);
    });
    
    ImmutableBiMap<ScientificOrCommon, String> sciOrCommonMap = ImmutableBiMap.of(
        ScientificOrCommon.COMMON_FIRST, Messages.getMessage(Name.COMMON_THEN_SCIENTIFIC),
        ScientificOrCommon.SCIENTIFIC_FIRST, Messages.getMessage(Name.SCIENTIFIC_THEN_COMMON),
        ScientificOrCommon.SCIENTIFIC_ONLY, Messages.getMessage(Name.SCIENTIFIC_ONLY),
        ScientificOrCommon.COMMON_ONLY, Messages.getMessage(Name.COMMON_ONLY));
    sciOrCommonComboBox = new JComboBox<String>(
        sciOrCommonMap.values().toArray(new String[4]));
    sciOrCommonComboBox.setSelectedItem(
        sciOrCommonMap.get(namePreferences.scientificOrCommon));
    sciOrCommonComboBox.addActionListener(e -> {
      namePreferences.scientificOrCommon = sciOrCommonMap.inverse().get(sciOrCommonComboBox.getSelectedItem());
    });
    
    countability = new JLabel(Messages.getMessage(Name.COUNTABILITY_PREFERENCES));
    countIntroduced = new JCheckBox(Messages.getMessage(Name.COUNT_INTRODUCED_SPECIES));
    countIntroduced.setSelected(queryPreferences.countIntroduced);
    countIntroduced.addActionListener(e -> {
      queryPreferences.countIntroduced = countIntroduced.isSelected();
    });
    countHeardOnly = new JCheckBox(Messages.getMessage(Name.COUNT_HEARD_ONLY_SIGHTINGS));
    countHeardOnly.setSelected(queryPreferences.countHeardOnly);
    countHeardOnly.addActionListener(e -> {
      queryPreferences.countHeardOnly = countHeardOnly.isSelected();
    });
    countUndescribed = new JCheckBox(Messages.getMessage(Name.COUNT_UNDESCRIBED_SPECIES));
    countUndescribed.setSelected(queryPreferences.countUndescribed);
    countUndescribed.addActionListener(e -> {
      queryPreferences.countUndescribed = countUndescribed.isSelected();
    });
    countRestrained = new JCheckBox(Messages.getMessage(Name.COUNT_RESTRAINED_INDIVIDUALS));
    countRestrained.setSelected(queryPreferences.countRestrained);
    countRestrained.addActionListener(e -> {
      queryPreferences.countRestrained = countRestrained.isSelected();
    });
    showExtinct = new JCheckBox(Messages.getMessage(Name.SHOW_EXTINCT_TAXA));
    showExtinct.setSelected(browsePreferences.showExtinctTaxa);
    showExtinct.addActionListener(e -> { 
      browsePreferences.showExtinctTaxa = showExtinct.isSelected();
    });
    
    backups = new JLabel(Messages.getMessage(Name.BACKUP_PREFERENCES));
    backupFrequency = new JLabel(Messages.getMessage(Name.HOW_OFTEN));
    backupFrequencyCombo = new JComboBox<BackupFrequency>(new BackupFrequency[]{
       BackupFrequency.NEVER, BackupFrequency.WEEKLY, BackupFrequency.MONTHLY});
    backupFrequencyCombo.setSelectedItem(backupPreferences.frequency);
    backupFrequencyCombo.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        backupPreferences.frequency = (BackupFrequency) backupFrequencyCombo.getSelectedItem();
        backupOptionsChanged = true;
      }
    });
    
    backupNowButton = new JButton(Messages.getMessage(Name.BACKUP_NOW));
    backupNowButton.setVisible(false);
    backupNowButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (reportSetSaver.getDirty().isDirty()) {
          int option = alerts.showConfirm(
              PreferencesPanel.this,
              Name.YOU_HAVE_UNSAVED_CHANGES,
              Name.SAVE_BEFORE_BACKING_UP);
          if (option == JOptionPane.CANCEL_OPTION) {
            // Cancel - don't back up
            return;
          } else if (option == JOptionPane.YES_OPTION) {
            try {
              reportSetSaver.save();
            } catch (IOException ioe) {
              FileDialogs.showFileSaveError(alerts, ioe, reportSetSaver.file());
              return;
            }
          }
        }
        backupSaver.definitelySaveAll();
      }
    });
    
    backupLocationButton = new JButton(
        Messages.getMessage(Name.CHOOSE_A_DIRECTORY));
    backupLocation = new TextLink(Messages.getMessage(Name.NONE_IN_BRACKETS));
    if (backupPreferences.backupDirectory != null) {
      File backupDirectory = new File(backupPreferences.backupDirectory);
      if (backupDirectory.exists() && backupDirectory.isDirectory()) {
        updateBackupDirectoryText(backupDirectory);
      }
    }
   
    backupLocation.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        if (backupPreferences.backupDirectory != null) {
          File backupDirectory = new File(backupPreferences.backupDirectory);
          if (backupDirectory.exists() && backupDirectory.isDirectory()) {
            if (Desktop.isDesktopSupported()
                && Desktop.getDesktop().isSupported(Action.OPEN)) {
              try {
                Desktop.getDesktop().open(backupDirectory);
                return;
              } catch (IOException exception) {
              }
            }
          }
        }
        
        // If opening the directory did not work or the directory does not exist,
        // fall through to choosing a new directory
        chooseBackupDirectory();
      }      
    });
    backupLocationButton.addActionListener(e -> chooseBackupDirectory());

    users = new JLabel(Messages.getMessage(Name.OBSERVER_PREFERENCES));
    toggleMultipleUsers = new JButton(Messages.getMessage(
        reportSet.getUserSet() == null
            ? Name.ENABLE_MULTIPLE_OBSERVERS
            : Name.DISABLE_MULTIPLE_OBSERVERS));
    toggleMultipleUsers.addActionListener(e -> toggleMultipleUsers());
    
    userSetPanel.setVisible(reportSet.getUserSet() != null);
    if (reportSet.getUserSet() != null) {
      userSetPanel.setUserSet(reportSet.getUserSet());
    }

    returnButton = new JButton(returnAction);
    returnButton.addActionListener(e -> {
      if (backupOptionsChanged) {
        // Run a backup as soon as the preferences panel closes (if backups are needed).
        backupSaver.maybeSaveAll();
      }
    });
  }

  private void toggleMultipleUsers() {
    if (reportSet.getUserSet() == null) {
      enableMultipleUsers();
    } else {
      disableMultipleUsers();
    }
  }
  
  private void enableMultipleUsers() {
    UserSet userSet = new UserSet();
    userDialog.showUserDialog(this, Messages.getMessage(Name.CREATE_A_FIRST_OBSERVER), userSet, null, u -> {
      reportSet.setUserSet(userSet);
      User addedUser = userSet.addUser(u);
      userSetPanel.setUserSet(userSet);
      toggleMultipleUsers.setText(Messages.getMessage(Name.DISABLE_MULTIPLE_OBSERVERS));
      userSetPanel.setVisible(true);
      
      // Note that multiple users are enabled.
      upToDatePreferences.multipleUsers = true;
      defaultUserStore.setUser(addedUser);

      if (!reportSet.getSightings().isEmpty()) {
        if (alerts.showYesNo(PreferencesPanel.this,
            Name.ADD_TO_ALL_SIGHTINGS,
            Name.ADD_OBSERVER_TO_ALL_SIGHTING_NOW) == JOptionPane.YES_OPTION)  {
          ImmutableSet<User> users = ImmutableSet.of(addedUser);
          for (Sighting sighting : reportSet.getSightings()) {
            sighting.getSightingInfo().setUsers(users);
          }
          
          // Set up the preferences to default to this user.
          chosenUserPreferences.get().setUsers(users);
          chosenUserPreferences.save(true /* markDirty */);
        }
      } else {
        // If there's no sightings, set the default user.
        chosenUserPreferences.get().setUsers(ImmutableSet.of(addedUser));
        chosenUserPreferences.save(true /* markDirty */);
      }
    });
  }

  private void disableMultipleUsers() {
    if (alerts.showOkCancel(this, Name.DISABLE_OBSERVERS_TITLE,
        Name.DISABLE_OBSERVERS_MESSAGE) == JOptionPane.OK_OPTION) {
      chosenUserPreferences.get().setUsers(ImmutableSet.of());
      upToDatePreferences.multipleUsers = false;
      reportSet.clearUserSet();
      defaultUserStore.setUser(null);

      toggleMultipleUsers.setText(Messages.getMessage(Name.ENABLE_MULTIPLE_OBSERVERS));
      userSetPanel.setVisible(false);
    }
  }

  private void updateBackupDirectoryText(File backupDirectory) {
    String path = backupDirectory.getAbsolutePath();
    // TODO: clearly not RTL language safe
    if (path.length() > 40) {
      path = "\u2026" + path.substring(path.length() - 40, path.length());
    }
    backupLocation.setText(path);
    backupNowButton.setVisible(true);
  }
  
  @Override
  public void fontsUpdated(FontManager fontManager) {
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    setBorder(new EmptyBorder(20, 20, 20, 20));
    
    backupLocation.putClientProperty(FontManager.PLAIN_LABEL, true);
    backupFrequency.putClientProperty(FontManager.PLAIN_LABEL, true);
    layout.setVerticalGroup(layout.createSequentialGroup()
        .addComponent(clementsLocalesLabel)
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(clementsLocalesComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(iocLocalesLabel)
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(iocLocalesComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(sciOrCommonComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(countability)
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(countIntroduced)
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(countHeardOnly)
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(countUndescribed)
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(countRestrained)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(uiLocalesLabel)
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(uiLocalesComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(showExtinct)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(backups)
        .addPreferredGap(ComponentPlacement.RELATED)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(backupFrequency)
            .addComponent(backupFrequencyCombo, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            .addComponent(backupLocationButton, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            .addComponent(backupNowButton, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            .addComponent(backupLocation))
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(users)
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(toggleMultipleUsers, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addComponent(userSetPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addGap(0, fontManager.scale(20), Short.MAX_VALUE)
        .addComponent(returnButton));
    layout.setHorizontalGroup(layout.createParallelGroup()
        .addComponent(clementsLocalesLabel)
        .addComponent(clementsLocalesComboBox, fontManager.scale(180), GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addComponent(iocLocalesLabel)
        .addComponent(iocLocalesComboBox, fontManager.scale(180), GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addComponent(sciOrCommonComboBox, fontManager.scale(180), GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addComponent(countability)
        .addComponent(countIntroduced)
        .addComponent(countHeardOnly)
        .addComponent(countUndescribed)
        .addComponent(countRestrained)
        .addComponent(uiLocalesLabel)
        .addComponent(uiLocalesComboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addComponent(showExtinct)
        .addComponent(backups)
        .addGroup(layout.createSequentialGroup()
            .addComponent(backupFrequency)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(backupFrequencyCombo, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(backupLocationButton)
            .addComponent(backupLocation, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)            
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(backupNowButton, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
        .addComponent(users)
        .addComponent(toggleMultipleUsers)
        .addComponent(userSetPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, fontManager.scale(400))
        .addGroup(layout.createSequentialGroup()
            .addGap(0, fontManager.scale(75), Short.MAX_VALUE)
            .addComponent(returnButton)));
    setPreferredSize(fontManager.scale(new Dimension(800, 600)));
  }

  private void chooseBackupDirectory() {
    if (backupSaver.chooseBackupDirectory(this, reportSetFile)) {
      updateBackupDirectoryText(new File(backupPreferences.backupDirectory));
      backupFrequencyCombo.setSelectedItem(backupPreferences.frequency);
      backupOptionsChanged = true;
    }
  }

  @Override
  public String getTitle() {
    return Messages.getMessage(Name.PREFERENCES);
  }
}
