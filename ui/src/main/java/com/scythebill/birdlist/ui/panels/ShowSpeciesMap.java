/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Set;
import org.apache.commons.text.StringEscapeUtils;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.io.Resources;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.checklist.TransposedChecklist;
import com.scythebill.birdlist.model.checklist.TransposedChecklists;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.util.DesktopUtils;
import com.scythebill.birdlist.ui.util.GoogleGeocharts;
import com.scythebill.birdlist.ui.util.GoogleGeocharts.Continent;
import com.scythebill.birdlist.ui.util.GoogleGeocharts.Subcontinent;

/**
 * Show a map of a species range.
 */
public class ShowSpeciesMap {
  private final LocationSet locations;
  private final GoogleGeocharts googleGeocharts;

  @Inject
  public ShowSpeciesMap(
      PredefinedLocations predefinedLocations,
      ReportSet reportSet) {
    this.locations = reportSet.getLocations();
    this.googleGeocharts = new GoogleGeocharts(locations, predefinedLocations);
  }

  /** Statuses, ordered from most important to least. */
  private final static ImmutableMap<Status, Integer> STATUS_INDICES = ImmutableMap.<Status, Integer>builder()
      .put(Status.ENDEMIC, 1)
      .put(Status.NATIVE, 2)
      .put(Status.INTRODUCED, 4)
      .put(Status.RARITY, 3)
      .put(Status.RARITY_FROM_INTRODUCED, 5)
      .put(Status.ESCAPED, 6)
      .put(Status.EXTINCT, 7)
      .build();
  
  public void showRange(ReportSet reportSet, Taxonomy taxonomy, String id) throws IOException {
    Species taxon = (Species) taxonomy.getTaxon(id);
    File file = File.createTempFile("range", ".html");
    Writer writer = new FileWriter(file);
    try {
      writeRange(reportSet, taxon, writer);
    } finally {
      writer.close();
    }
    
    DesktopUtils.openHtmlFileInBrowser(file);
  }
    
  public void writeRange(ReportSet reportSet, Species taxon, Writer writer) throws IOException {
    TransposedChecklist transposedChecklist =
        TransposedChecklists.instance().getTransposedChecklist(reportSet, taxon.getTaxonomy());
    
    String startTemplate = Resources.toString(
        Resources.getResource(getClass(), "speciesmap.start.html.template"),
        Charsets.UTF_8);
    String endTemplate = Resources.toString(
        Resources.getResource(getClass(), "speciesmap.end.html.template"),
        Charsets.UTF_8);

    writer.write(String.format(startTemplate, taxon.getCommonName()));
//    Multimap<Status, String> smallCountriesByStatus = LinkedHashMultimap.create();
    Set<String> alreadyRecorded = Sets.newHashSet();
    List<String> usStateEntries = Lists.newArrayList();
    List<String> caStateEntries = Lists.newArrayList();
    List<String> auStateEntries = Lists.newArrayList();
    
    for (Status status : STATUS_INDICES.keySet()) {
      Integer statusInt = Preconditions.checkNotNull(STATUS_INDICES.get(status), status);
      Iterable<String> statesAndCountries = transposedChecklist.locationsWithStatuses(taxon, ImmutableSet.of(status));
      statesAndCountries = Iterables.transform(
          statesAndCountries,
          id -> GoogleGeocharts.EBIRD_LOCATIONS_NOT_IN_GOOGLE.getOrDefault(id, id));
          
      Set<String> countries = normalizeCountries(statesAndCountries);
      if (countries.contains("US")) {
        for (String state : findStates(statesAndCountries, "US")) {
          usStateEntries.add(dataFormat(status, statusInt, state, googleGeocharts.getNameForGoogle(state)));
        }
      }
      if (countries.contains("CA")) {
        for (String state : findStates(statesAndCountries, "CA")) {
          caStateEntries.add(dataFormat(status, statusInt, state, googleGeocharts.getNameForGoogle(state)));
        }
      }
      if (countries.contains("AU")) {
        for (String state : findStates(statesAndCountries, "AU")) {
          auStateEntries.add(dataFormat(status, statusInt, state, googleGeocharts.getNameForGoogle(state)));
        }
      }
      
      // If a country was already recorded at a "higher" level, show that.
      countries.removeAll(alreadyRecorded);
      alreadyRecorded.addAll(countries);
      
      for (String country : countries) {
//        if (SMALL_COUNTRIES.contains(country)) {
//          smallCountriesByStatus.put(status, country);
//        }
        String countryName = googleGeocharts.getNameForGoogle(country);
        writer.write(dataFormat(status, statusInt, country, countryName));
      }
    }
    
    String region = null;
    ImmutableSet<Subcontinent> subcontinents = googleGeocharts.subcontinents(alreadyRecorded);
    ImmutableSet<Continent> continents = googleGeocharts.continents(subcontinents);
    if (subcontinents.size() == 1) {
      if (alreadyRecorded.size() == 1
          && ImmutableSet.of("US", "CA", "AU").contains(Iterables.getOnlyElement(alreadyRecorded))) {
        region = Iterables.getOnlyElement(alreadyRecorded);
      } else {
        region = Iterables.getOnlyElement(subcontinents).id();
      }
    } else if (continents.size() == 1) {
      region = Iterables.getOnlyElement(continents).id();
    }
    
    String regionJs = region == null
        ? "null"
        : "\"" + region + "\"";
    
    String extraText = "";
    // Ignore the "small" countries.  With zooming, this really isn't *that* interesting,
    // and it seriously messes with the layout of the page
//    for (Status status : smallCountriesByStatus.keySet()) {
//      List<String> countries = Lists.newArrayList();
//      for (String country : smallCountriesByStatus.get(status)) {
//        countries.add(getCountryName(country));
//      }
//      extraText = extraText + "\n<br>\n" + statusText(status) + ": " +
//          Joiner.on(", ").join(countries);
//    }
    
    List<String> optionsList = Lists.newArrayList();
    if (continents.size() > 1) {
      optionsList.add("<option value=\"\">Zoom to world, or...</option>");
    }
    
    String usStates = "null";
    if (!usStateEntries.isEmpty()) {
      usStates = "[" + Joiner.  on("").join(usStateEntries) + "]";
    }
    String caStates = "null";
    if (!caStateEntries.isEmpty()) {
      caStates = "[" + Joiner.on("").join(caStateEntries) + "]";
    }
    String auStates = "null";
    if (!auStateEntries.isEmpty()) {
      auStates = "[" + Joiner.on("").join(auStateEntries) + "]";
    }
    
    for (Continent continent : continents) {
      if (subcontinents.size() > 1) {
        optionsList.add(continent.asOption());
      }
      
      for (Subcontinent subcontinent : continent.subcontinents()) {
        if (subcontinents.contains(subcontinent)) {
          optionsList.add(subcontinent.asOption());
          // Add sub-options for places with states.
          if (subcontinent.contains("US") && alreadyRecorded.contains("US")) {
            optionsList.add("<option value=\"?US\">&nbsp;&nbsp;&nbsp;&nbsp;United States</option>");
          }
          if (subcontinent.contains("CA") && alreadyRecorded.contains("CA")) {
            optionsList.add("<option value=\"?CA\">&nbsp;&nbsp;&nbsp;&nbsp;Canada</option>");
          }
          if (subcontinent.contains("AU") && alreadyRecorded.contains("AU")) {
            optionsList.add("<option value=\"?AU\">&nbsp;&nbsp;&nbsp;&nbsp;Australia</option>");
          }
        }
      }
    }
    
    writer.write(String.format(
        endTemplate,
        regionJs,
        usStates,
        caStates,
        auStates,
        Joiner.on('\n').join(optionsList),
        taxon.getCommonName(),
        TaxonUtils.getRange(taxon),
        extraText));
  }

  private Iterable<String> findStates(Iterable<String> statesAndCountries, String prefix) {
    final String prefixPlusHyphen = prefix + "-"; 
    return FluentIterable.from(statesAndCountries)
        .filter(new Predicate<String>()  {
          @Override
          public boolean apply(String string) {
            return string.startsWith(prefixPlusHyphen);
          }
        });
  }

  private String dataFormat(Status status, Integer statusInt, String country,
      String countryName) {
    return String.format("[{v:\"%s\",f:\"%s\"}, {v:%d, f:\"%s\"}],",
        country,
        StringEscapeUtils.escapeJson(countryName),
        statusInt,
        StringEscapeUtils.escapeJson(statusText(status)));
  }

  private String statusText(Status status) {
    String statusText = Messages.getText(status);
    return statusText;
  }
  
  private Set<String> normalizeCountries(Iterable<String> statesAndCountries) {
    Set<String> set = Sets.newHashSet();
    for (String value : statesAndCountries) {
      if (GoogleGeocharts.EBIRD_TO_GOOGLE_COUNTRIES.containsKey(value)) {
        value = GoogleGeocharts.EBIRD_TO_GOOGLE_COUNTRIES.get(value);
      }
      int indexOf = value.indexOf('-');
      if (indexOf < 0) {
        set.add(value);
      } else {
        set.add(value.substring(0, indexOf));
      }
    }
    return set;
  }
}
