/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.awt.Dimension;
import java.io.IOException;
import java.util.Date;

import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import org.joda.time.LocalDate;
import org.joda.time.Partial;
import org.joda.time.ReadablePartial;
import org.joda.time.chrono.GJChronology;

import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.sighting.edits.ChosenUsers;
import com.scythebill.birdlist.model.sighting.edits.RecentEdits;
import com.scythebill.birdlist.model.sighting.edits.SingleLocationEdit;
import com.scythebill.birdlist.model.user.User;
import com.scythebill.birdlist.model.user.Users;
import com.scythebill.birdlist.model.util.Dirty;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.app.NavigablePanel;
import com.scythebill.birdlist.ui.app.ReportSetSaver;
import com.scythebill.birdlist.ui.events.DefaultUserStore;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.EBirdDirectExporter;
import com.scythebill.birdlist.ui.prefs.ReportSetPreference;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;
import com.scythebill.birdlist.ui.util.VisibilityDetector;

/**
 * Runs the entire single-location edit flow.
 */
public class SingleLocationEditPanel extends WizardPanelBase<SingleLocationEdit>
    implements FontsUpdatedListener, NavigablePanel {

  /** The default date used for each editing session */
  private static ReadablePartial defaultDate = new LocalDate(new Date(), GJChronology.getInstance());

  /** The default location used for each editing session */
  private static String defaultLocation = null;

  private final SingleLocationEdit edit;
  private final ReportSet reportSet;
  private final ReportSetSaver reportSetSaver;
  private final TaxonomyStore taxonomyStore;
  private final SingleLocationWhenAndWherePanel whenAndWhere;
  private final Alerts alerts;
  private final ReportSetPreference<RecentEdits> recentEdits;
  private final ReportSetPreference<ChosenUsers> chosenUsers;
  private final DataEntryPreferences dataEntryPreferences;
  private final JCheckBox exportToEBird;
  private final EBirdDirectExporter eBirdDirectExporter;
  private JCheckBox andSave;

  @Inject
  public SingleLocationEditPanel(
      NavigableFrame navigableFrame,
      VisibilityDetector visibilityDetector,
      SingleLocationWhenAndWherePanel whenAndWhere,
      SingleLocationNewLocationPanel newLocation,
      SingleLocationVisitInfoPanel visitInfo,
      SingleLocationSpeciesListPanel speciesList,
      DefaultUserStore defaultUserStore,
      TaxonomyStore taxonomyStore,
      ReportSet reportSet,
      FontManager fontManager,
      Alerts alerts,
      ReportSetPreference<RecentEdits> recentEdits,
      ReportSetPreference<ChosenUsers> chosenUsers,
      DataEntryPreferences dataEntryPreferences,
      EBirdDirectExporter eBirdDirectExporter,
      ReportSetSaver reportSetSaver) {
    super(navigableFrame, visibilityDetector);
    this.whenAndWhere = whenAndWhere;
    this.taxonomyStore = taxonomyStore;
    this.recentEdits = recentEdits;
    this.chosenUsers = chosenUsers;
    this.dataEntryPreferences = dataEntryPreferences;
    this.eBirdDirectExporter = eBirdDirectExporter;
    this.reportSetSaver = reportSetSaver;
    
    ImmutableSet.Builder<User> users = ImmutableSet.builder();
    users.addAll(chosenUsers.get().getUsers(reportSet.getUserSet()));
    // Add the current "default user" to the last set of chosen users
    if (defaultUserStore.getUser() != null) {
      users.add(defaultUserStore.getUser());
    }
    
    this.edit = new SingleLocationEdit(defaultDate, users.build());
    if (defaultLocation != null) {
      Location initialLocation = reportSet.getLocations().getLocation(defaultLocation);
      if (initialLocation != null) {
        edit.setLocation(initialLocation);
      }
    }
    this.reportSet = reportSet;
    this.alerts = alerts;
    
    addAncestorListener(new AncestorListener() {
      @Override
      public void ancestorRemoved(AncestorEvent event) {
        // Remove the editor as soon as this component is removed from the hierarchy
        reportSetSaver.releaseEditor();
      }
      
      @Override public void ancestorMoved(AncestorEvent event) {}
      @Override public void ancestorAdded(AncestorEvent event) {
        // Attach the editor when visible
        reportSetSaver.attachEditor(new SingleLocationEditEditor());        
      }
    });
    
    exportToEBird = new JCheckBox(Messages.getMessage(Name.AND_EXPORT_TO_EBIRD));
    exportToEBird.setSelected(dataEntryPreferences.immediateExportToEBird);
    
    andSave = new JCheckBox(Messages.getMessage(Name.AND_SAVE));
    andSave.setSelected(dataEntryPreferences.immediateSave);

    addPage(whenAndWhere);
    addPage(newLocation);
    addPage(visitInfo);
    addPage(speciesList);
    
    // Don't bother updating the whole tree - the individual panels do
    // that already.
    fontsUpdated(fontManager);
  }

  class SingleLocationEditEditor implements ReportSetSaver.Editor {

    @Override
    public Dirty getDirty() {
      return edit.getDirty();
    }

    @Override
    public void beforeSave() {
      // Lie to the current page to claim that the user is leaving, to force it to
      // flush any edits.
      getCurrentPage().leaving(false);
      // If there isn't even a location, don't bother trying to 
      // apply the current edit. (apply() will throw an exception)
      if (edit.getLocation() != null) {
        edit.apply(reportSet, taxonomyStore.getTaxonomy());
      }
    }

    @Override
    public boolean beforeNavigation() {
      if (edit.getDirty().isDirty()) {
        return alerts.showOkCancel(this, Name.ARE_YOU_SURE,
            Name.LOSE_ALL_THE_SIGHTINGS) == JOptionPane.OK_OPTION;
      }
      return true;
    }    
  }
  
  @Override
  protected boolean save() {
    if (edit.getDate() != null) {
      defaultDate = new Partial(edit.getDate());
    } else {
      defaultDate = null;
    }
    
    if (edit.getLocation() != null) {
      defaultLocation = edit.getLocation().getId();
    } else {
      defaultLocation = null;
    }
    
    edit.apply(reportSet, taxonomyStore.getTaxonomy());

    Location location = edit.getLocation();
    if (location != null && edit.getDate() != null) {
      recentEdits.get().add(location.getId(), edit.getDate(), edit.getTime());
      recentEdits.save(true /* mark dirty */);
    }
    
    chosenUsers.get().setUsers(edit.getUsers());
    chosenUsers.save(true /* mark dirty */);
   
    // Update the observation type to the last used
    VisitInfo visitInfo = edit.getVisitInfo();
    if (visitInfo != null && visitInfo.hasData()) {
      dataEntryPreferences.observationType = visitInfo.observationType();
    }
    
    dataEntryPreferences.immediateExportToEBird = exportToEBird.isSelected();
    if (exportToEBird.isSelected()) {
      if (location != null && edit.getDate() != null) {
        eBirdDirectExporter.doExport(this, location, edit.getDate(), edit.getTime());
      }
    }
    
    dataEntryPreferences.immediateSave = andSave.isSelected();
    if (andSave.isSelected()) {
      try {
        reportSetSaver.save();
      } catch (IOException e) {
        FileDialogs.showFileSaveError(alerts, e, reportSetSaver.file());
        return false;
      }
    }
    
    return true;
  }

  @Override
  protected String getWizardTitle() {
    return Messages.getMessage(Name.ENTER_SIGHTINGS);
  }

  @Override protected int getPreviousIndex() {
    int index = super.getPreviousIndex();
    // Skip the Visit Info panel when there's no date
    if (index == 2 && !whenAndWhere.hasWhen()) {
      index = 1;
    }
    
    // Skip the "new location" page when a full location
    // was entered on the first page
    if (index == 1 && whenAndWhere.hasWhere()) {
      index = 0;
    }
    return index;
  }

  @Override protected int getNextIndex() {
    int index = super.getNextIndex();
    // Skip the "new location" page when a full location
    // was entered on the first page
    if (index == 1 && whenAndWhere.hasWhere()) {
      index = 2;
    }

    // Skip the Visit Info panel when there's no date
    if (index == 2 && !whenAndWhere.hasWhen()) {
      index = 3;
      // ... and, just in case the user had entered a visit info, erase it.
      wizardValue().setVisitInfo(null);
    }
    
    return index;
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    super.fontsUpdated(fontManager);
    setPreferredSize(fontManager.scale(new Dimension(1000, 640)));
  }

  @Override
  protected SingleLocationEdit wizardValue() {
    return edit;
  }

  @Override
  protected void addExtraContentAfterDone(SequentialGroup horizontalButtonGroup) {
    horizontalButtonGroup.addComponent(andSave);
    horizontalButtonGroup.addComponent(exportToEBird);
  }

  @Override
  protected void addExtraContentAfterDone(ParallelGroup verticalButtonGroup) {
    verticalButtonGroup.addComponent(andSave);
    verticalButtonGroup.addComponent(exportToEBird);
  }

  @Override
  public void navigateTo(Object args) {
    VisitInfoKey visitInfoKey = (VisitInfoKey) args;
    wizardValue().setDate(visitInfoKey.date());
    Location location = reportSet.getLocations().getLocation(visitInfoKey.locationId());
    wizardValue().setLocation(location);
    wizardValue().setTime(visitInfoKey.startTime().orNull());
    
    ImmutableSet<User> users = Users.usersForVisitInfoKey(reportSet, visitInfoKey);
    wizardValue().setUsers(users);
    // And since we're navigating in from afar, act as if the species list has seen
    // these users already, so adding/removing from this page works.
    wizardValue().setUsersOnSpeciesList(users);
    
    whenAndWhere.setVisitInfoKeyAndUsers(visitInfoKey, users);

    // Jump to the last page.  This will - implicitly - trigger the same
    // snapshot flow as if the user started on the first page and entered an
    // existing date and location.  This implicitly relies on code currently
    // running in SingleLocationWhenAndWherePanel, which is poor.
    showPage(getPageCount() - 1);
  }
}
