/*   
 * Copyright 2012 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.edits.SingleLocationEdit;
import com.scythebill.birdlist.ui.app.Titled;
import com.scythebill.birdlist.ui.components.NewLocationPanel;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Wizard panel for entering a new location. 
 */
public class SingleLocationNewLocationPanel extends WizardContentPanel<SingleLocationEdit>
    implements Titled, FontsUpdatedListener {

  private final NewLocationPanel newLocation;
  private final FontManager fontManager;
  private JLabel helpLabel;

  @Inject
  public SingleLocationNewLocationPanel(
      NewLocationPanel newLocation,
      FontManager fontManager) {
    super("NewLocation", SingleLocationEdit.class);
    this.newLocation = newLocation;
    this.fontManager = fontManager;
    
    initGUI();
    hookupContents();
  }
  
  private void hookupContents() {
    newLocation.setLocationName("");
    newLocation.addPropertyChangeListener("complete", new PropertyChangeListener() {
      @Override public void propertyChange(PropertyChangeEvent e) {
        setComplete((Boolean) e.getNewValue());
      }
    });
    newLocation.addPropertyChangeListener("value", new PropertyChangeListener() {
      @Override public void propertyChange(PropertyChangeEvent e) {
        wizardValue().setLocation((Location) e.getNewValue());
      }
    });
  }

  private void initGUI() {
    setLayout(new BorderLayout());

    Box content = new Box(BoxLayout.PAGE_AXIS);
    add(content, BorderLayout.CENTER);
    
    helpLabel = new JLabel();
    helpLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    
    content.add(helpLabel);
    content.add(Box.createVerticalStrut(20));
    content.add(newLocation);
    
    helpLabel.setAlignmentX(0.0f);
    newLocation.setAlignmentX(0.0f);
    
    fontManager.applyTo(this);
  }

  @Override public String getTitle() {
    return "New Location";
  }

  @Override
  protected void shown() {
    super.shown();
    newLocation.requestFocusInWindow();
  }
  
  @Override
  protected boolean leaving(boolean validate) {
    super.leaving(validate);
    
    wizardValue().setLocationName(newLocation.getLocationName());
    if (!validate) {
      return true;
    }
    Location location = newLocation.getValueAfterValidating();
    return location != null && wizardValue().getLocation() != null;
  }

  @Override
  protected void beforeShown() {
    if (!wizardValue().getLocationName().equals(newLocation.getLocationName())) {
      newLocation.setLocationName(wizardValue().getLocationName());
    }
    helpLabel.setText(
        Messages.getFormattedMessage(Name.ENTER_LOCATION_HELP_FORMAT, wizardValue().getLocationName()));
    Location location = newLocation.getValueAfterValidating();
    wizardValue().setLocation(location);
    
    setComplete(location != null);
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    setPreferredSize(fontManager.scale(new Dimension(600, 500)));
  }  
}
