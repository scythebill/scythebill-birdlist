/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.OptionalInt;
import java.util.Set;
import java.util.concurrent.Future;

import javax.swing.AbstractAction;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.filechooser.FileFilter;

import org.joda.time.DateTimeFieldType;
import org.joda.time.ReadablePartial;

import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.base.Strings;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;
import com.google.common.eventbus.Subscribe;
import com.google.common.io.Resources;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.HtmlResponseWriter;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.query.SightingPredicates;
import com.scythebill.birdlist.model.sighting.ApproximateNumber;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Photo;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfo.ObservationType;
import com.scythebill.birdlist.model.sighting.edits.SingleLocationEdit;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.user.User;
import com.scythebill.birdlist.model.util.ResolvedComparator;
import com.scythebill.birdlist.model.util.ToString;
import com.scythebill.birdlist.ui.app.Titled;
import com.scythebill.birdlist.ui.components.ChecklistSpeciesTablePanel;
import com.scythebill.birdlist.ui.components.DirectSpeciesTablePanel;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.components.IndexerPanel.Mixer;
import com.scythebill.birdlist.ui.components.SightingInfoPanel;
import com.scythebill.birdlist.ui.components.SpeciesTablePanel;
import com.scythebill.birdlist.ui.components.photos.PhotoDrops;
import com.scythebill.birdlist.ui.components.table.DelayedUI;
import com.scythebill.birdlist.ui.components.table.ExpandableTable;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.Column;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.ColumnLayout;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.DetailView;
import com.scythebill.birdlist.ui.components.table.ExpandableTable.RowState;
import com.scythebill.birdlist.ui.components.table.LabelColumn;
import com.scythebill.birdlist.ui.components.table.TableRowsModel;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.events.TaxonomyChangedEvent;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.DataEntryPreferences.ChecklistUse;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.panels.SpeciesSpHybridEntryPanel.SpeciesEntryEvent;
import com.scythebill.birdlist.ui.panels.SpeciesSpHybridEntryPanel.SpeciesEntryListener;
import com.scythebill.birdlist.ui.panels.reports.QueryPreferences;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;
import com.scythebill.birdlist.ui.util.LocationScorer;
import com.scythebill.birdlist.ui.util.ScanSeenTaxa;
import com.scythebill.birdlist.ui.util.Scorer;
import com.scythebill.birdlist.ui.util.SightingFlags;
import com.scythebill.birdlist.ui.util.SubspeciesToString;
import com.scythebill.birdlist.ui.util.ToStringListCellRenderer;
import com.scythebill.birdlist.ui.util.UIUtils;
import com.scythebill.birdlist.ui.util.UiFutures;

/**
 * Panel for entering a list of species for a single date and location.  
 */
public class SingleLocationSpeciesListPanel extends WizardContentPanel<SingleLocationEdit>
    implements Titled {
  private final TaxonomyStore taxonomyStore;
  private final Alerts alerts;
  private Taxonomy taxonomy;
  private final FontManager fontManager;
  private SpeciesTablePanel speciesTablePanel;
  private JComboBox<Taxon> currentSubspeciesComboBox;
  private final ReportSet reportSet;
  private Scorer locationScorer;
  private final ListeningExecutorService executorService;
  private final Checklists checklists;
  private volatile Future<?> scorerFuture;
  private SpeciesSpHybridEntryPanel speciesEntryPanel;
  private ScanSeenTaxa scanSeenTaxa;
  private Checklist checklist;
  private Taxon subspeciesPopupTaxon;
  private JComboBox<DataEntryPreferences.ChecklistUse> checklistCombo;
  private JCheckBox completeCheckbox;
  private JLabel speciesCountLabel;
  @SuppressWarnings("unchecked")
  private final ListCellRenderer<String> defaultRenderer = (ListCellRenderer<String>) new JComboBox<String>().getRenderer();
  
  private final ToString<Taxon> subspeciesToString = new SubspeciesToString();
  private final ActionListener checklistUpdated = new ActionListener() {
    @Override public void actionPerformed(ActionEvent event) {
      // Save the new checklist state in prefs
      SingleLocationSpeciesListPanel.this.dataEntryPreferences.checklistUse =
          (ChecklistUse) checklistCombo.getSelectedItem();
      // and rebuild the table
      rebuildTable(taxonomy);
    }
  };
  
  private JLabel checklistLabel;
  private final DataEntryPreferences dataEntryPreferences;
  private NewForColumn sightingInfoColumn;
  private final FileDialogs fileDialogs;
  private final QueryPreferences queryPreferences;
  private JComponent speciesInfoPane;

  @Inject
  public SingleLocationSpeciesListPanel(
      TaxonomyStore taxonomyStore,
      SpeciesSpHybridEntryPanel speciesEntryPanel,
      FontManager fontManager,
      ReportSet reportSet,
      ListeningExecutorService executorService,
      EventBusRegistrar eventBusRegistrar,
      Alerts alerts,
      Checklists checklists,
      DataEntryPreferences dataEntryPreferences,
      QueryPreferences queryPreferences,
      FileDialogs fileDialogs) {
    super("speciesList", SingleLocationEdit.class);

    this.taxonomyStore = taxonomyStore;
    this.speciesEntryPanel = speciesEntryPanel;
    this.fontManager = fontManager;
    this.reportSet = reportSet;
    this.executorService = executorService;
    this.alerts = alerts;
    this.checklists = checklists;
    this.dataEntryPreferences = dataEntryPreferences;
    this.queryPreferences = queryPreferences;
    this.fileDialogs = fileDialogs;
    
    initComponents();
    
    // Some magic shortcuts:
    //   Alt-right on the indexer:  expand the selected row and focus on it
    //   Alt-left on the table: go back to the indexer
    
    KeyStroke focusOnSpecies;
    if (UIUtils.isMacOS()) {
      focusOnSpecies = KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT,
          Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() | InputEvent.ALT_MASK);
    } else {
      // Alt-ctrl-arrow twirls the monitor on Windows, so do alt-shift-arrow
      focusOnSpecies = KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT,
          InputEvent.SHIFT_MASK | InputEvent.ALT_MASK);
    }
    speciesEntryPanel.getIndexer().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
        .put(focusOnSpecies, "focusOnSpeciesList");
    speciesEntryPanel.getIndexer().getActionMap().put("focusOnSpeciesList", new AbstractAction() {
      @Override public void actionPerformed(ActionEvent event) {
        Resolved selectedSpecies = speciesTablePanel.getSelectedSpecies();
        if (selectedSpecies != null) {
          speciesTablePanel.expandSpeciesDetail(selectedSpecies);
          speciesTablePanel.focusOnSpeciesDetail();
        }
      }
    });
        
    speciesEntryPanel.getIndexer().getTextComponent().addKeyListener(new KeyAdapter() {
      @Override
      public void keyTyped(KeyEvent e) {
        // If a user types a digit (0..9) on the SpeciesEntry indexer, and:
        //  - the indexer is currently empty
        //  - and there's a selected species
        // Then move the focus to the number field, and set the text value
        if (e.getKeyChar() >= '0' && e.getKeyChar() <= '9') {
          final IndexerPanel<String> indexer = SingleLocationSpeciesListPanel.this.speciesEntryPanel.getIndexer();
          Resolved selectedSpecies = speciesTablePanel.getSelectedSpecies();
          if (indexer.getTextValue().isEmpty() && selectedSpecies != null) {
            redirectFocusToNumberFieldOfSelectedSpecies(e, selectedSpecies);
          }
        }
      }
    });
    
    speciesEntryPanel.addSpeciesEntryListener(new SpeciesEntryListener() {
      @Override public void addSpecies(SpeciesEntryEvent event) {
        // Immediately collapse the expanded index. Leaving this open - it gets closed eventually - 
        // has exposed some bugs.  Those bugs have been fixed, as best I know, but given that
        // it's going to get collapsed during row addition anyway, it's just easier to
        // eliminate possibilities for bugs in this tricky scenario.  (The basic annoyance is
        // that multiple bits of model state want to change, and too much of the code attempts
        // to synchronously handle these changes, which is error-prone, since you're likely
        // to encounter intermediate, inconsistent states.)
        speciesTablePanel.getSpeciesTable().setExpandedIndex(ExpandableTable.NO_ROW);

        Resolved resolved = event.getResolved();
        speciesTablePanel.addSpecies(resolved);
        
        SightingStatus changedStatus = null;
        // When entering introduced taxa (Feral Pigeon), automatically mark as introduced 
        if (resolved.getTaxonStatus() == Species.Status.IN) {
          changedStatus = SightingStatus.INTRODUCED;
        } else if (resolved.getTaxonStatus() == Species.Status.DO) {
          // Status is domestic
          changedStatus = SightingStatus.DOMESTIC;
          // ... unless there's a checklist which says it's introduced here
          if (checklist != null) {
            SightingTaxon taxon = resolved.getParentOfAtLeastType(Taxon.Type.species);
            Checklist.Status status = checklist.getStatus(taxonomy, taxon);
            if (status == Checklist.Status.INTRODUCED
                || status == Checklist.Status.RARITY_FROM_INTRODUCED) {
              changedStatus = SightingStatus.INTRODUCED;
            }
          }
        } else if (checklist != null) {
          // And if the checklist says it's introduced, set introduced (or escaped) status automatically.
          SightingTaxon taxon = resolved.getParentOfAtLeastType(Taxon.Type.species);
          Checklist.Status status = checklist.getStatus(taxonomy, taxon);
          if (status == Checklist.Status.INTRODUCED
              || status == Checklist.Status.RARITY_FROM_INTRODUCED) {
            changedStatus = SightingStatus.INTRODUCED;
          } else if (status == Checklist.Status.ESCAPED) {
            changedStatus = SightingStatus.INTRODUCED_NOT_ESTABLISHED;
          }
        }
        if (changedStatus != null) {
          wizardValue().getSightingInfo(resolved).setSightingStatus(changedStatus);
          // Force the table to treat the taxon as updated (to tweak the "New for" column)
          speciesTablePanel.resolvedUpdated(resolved);
        }
        speciesTablePanel.selectSpecies(resolved, true /* scroll with visible context */);
        SingleLocationSpeciesListPanel.this.speciesEntryPanel.requestFocusInWindow();
        
        wizardValue().edited();
      }
    });
    
    checklistCombo.addActionListener(checklistUpdated);
    completeCheckbox.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent event) {
        wizardValue().setVisitInfo(
            wizardValue().getVisitInfo()
                .asBuilder()
                .withCompleteChecklist(completeCheckbox.isSelected())
                .build());
      }
    });
    
    eventBusRegistrar.registerWhenInHierarchy(this);
    setTaxonomy(taxonomyStore.getTaxonomy(), true /* skip table updates */);
  }

  private void updateSubspeciesOfCurrentItem(Taxon current, Taxon newTaxon) {
    // null means go back to the species level
    if (newTaxon == null) {
      newTaxon = TaxonUtils.getParentOfType(current, Taxon.Type.species);
    }
    
    if (current != newTaxon) {
      wizardValue().swapTaxa(SightingTaxons.newResolved(current),
          SightingTaxons.newResolved(newTaxon));
      speciesTablePanel.swapSpecies(
          SightingTaxons.newResolved(current),
          SightingTaxons.newResolved(newTaxon));
      speciesTablePanel.selectSpecies(SightingTaxons.newResolved(newTaxon), false);      
    }
    
    SightingStatus changedStatus = null;
    // If the new taxon is a feral form, mark the sighting as introduced
    if (newTaxon.getStatus() == Species.Status.IN) {
      changedStatus = SightingStatus.INTRODUCED;
    } else if (newTaxon.getStatus() == Species.Status.DO) {
      // If the new taxon is a domestic form, mark the sighting as domestic
      changedStatus = SightingStatus.DOMESTIC;
    } else if (current.getStatus() == Species.Status.IN
        || current.getStatus() == Species.Status.DO) {
      // Else if the *old* taxon was a feral or domestic form, clear the sighting from "introduced" status
      SightingInfo sightingInfo = wizardValue().getSightingInfo(SightingTaxons.newResolved(newTaxon));
      if (sightingInfo.getSightingStatus() == SightingStatus.INTRODUCED
          || sightingInfo.getSightingStatus() == SightingStatus.DOMESTIC) {
        changedStatus = SightingStatus.NONE;
      }
    }
    
    if (changedStatus != null) {
      Resolved resolved = SightingTaxons.newResolved(newTaxon);
      wizardValue().getSightingInfo(resolved).setSightingStatus(changedStatus);
      // Force the table to treat the taxon as updated (to tweak the "New for" column)
      speciesTablePanel.resolvedUpdated(resolved);
    }
  }

  /** Init all components in the page. */
  private void initComponents() {
    checklistLabel = new JLabel();
    checklistCombo = new JComboBox<DataEntryPreferences.ChecklistUse>(DataEntryPreferences.ChecklistUse.values());
    checklistCombo.setVisible(false);
    
    completeCheckbox = new JCheckBox(Messages.getMessage(Name.COMPLETE_LIST_OF_SIGHTINGS_QUESTION));
    completeCheckbox.setVisible(false);

    speciesCountLabel = new JLabel();
    updateSpeciesCount(ImmutableList.<Resolved>of(), 0);
    
    // Remove the species info panel from the main entry panel to relocate below
    speciesInfoPane = speciesEntryPanel.extractSpeciesInfoPanel(); 
    
    initTable(taxonomy);
    initLayout();
    
    fontManager.applyTo(this);
  }
  
  private void updateCompleteAndTaxaCount(List<Resolved> taxa,
      Taxonomy currentTaxonomy) {
    int incompatibleTaxonomyCount = wizardValue().getIncompatibleTaxonomyCount(currentTaxonomy);
    setComplete(!taxa.isEmpty() || incompatibleTaxonomyCount > 0);
    updateSpeciesCount(taxa, incompatibleTaxonomyCount);    
  }

  private void updateSpeciesCount(List<Resolved> taxa, int otherTaxa) {
    Set<SightingTaxon> species = Sets.newHashSet();
    for (Resolved resolved : taxa) {
      SightingTaxon taxon = resolved.getParentOfAtLeastType(Taxon.Type.species);
      if (taxon.getType() == SightingTaxon.Type.SINGLE
          || taxon.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
        species.add(taxon);
      }
    }
    
    if (otherTaxa == 0) {
      speciesCountLabel.setText(
          Messages.getFormattedMessage(Name.NUMBER_OF_SPECIES_FORMAT, species.size()));
    } else {
      speciesCountLabel.setText(
          Messages.getFormattedMessage(Name.NUMBER_OF_SPECIES_AND_OTHER_TAXA_FORMAT, species.size(), otherTaxa));
    }
  }

  /** Rebuild the table (and consequently the page's layout). */
  private void rebuildTable(Taxonomy taxonomy) {
    speciesTablePanel.flushEdits();
    
    initTable(taxonomy);
    initLayout();
  }

  private void initLayout() {
    GroupLayout layout = new GroupLayout(this);
    layout.setVerticalGroup(layout.createSequentialGroup()
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(checklistLabel)
            .addComponent(checklistCombo)
            .addComponent(completeCheckbox)
            .addComponent(speciesCountLabel))
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(speciesEntryPanel)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(speciesTablePanel)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(speciesInfoPane));
    layout.setHorizontalGroup(layout.createParallelGroup()
        .addGroup(Alignment.LEADING, layout.createSequentialGroup()
            .addComponent(checklistLabel)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(checklistCombo, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
        .addGroup(Alignment.TRAILING, layout.createSequentialGroup()
            .addComponent(completeCheckbox, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(ComponentPlacement.UNRELATED)
            .addComponent(speciesCountLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
        .addComponent(speciesEntryPanel)
        .addComponent(speciesTablePanel)
        .addComponent(speciesInfoPane));
    setLayout(layout);
  }
  
  private void initTable(final Taxonomy currentTaxonomy) {
    // Save off the existing list of species;  and if the table is already present,
    // remove it (since building the new GroupLayout won't remove old components)
    ImmutableList<Resolved> existingResolved;
    if (speciesTablePanel != null && speciesTablePanel.getParent() != null) {
      speciesTablePanel.getParent().remove(speciesTablePanel);
      existingResolved = speciesTablePanel.getValue();
    } else {
      existingResolved = ImmutableList.of();
    }
    
    sightingInfoColumn = new NewForColumn();
    
    // If the user is not using a checklist, then choose the DirectSpeciesTablePanel
    if (checklist == null
        || checklistCombo.getSelectedItem() == DataEntryPreferences.ChecklistUse.NO) {
      speciesTablePanel = new DirectSpeciesTablePanel(new SightingInfoDetail(), "Show sighting details",
          fontManager);
      speciesTablePanel.setEditable(true);
      // ... which has a subspecies, sighting info, and delete column (after the name)
      speciesTablePanel.addColumn(new SubspeciesColumn());
      speciesTablePanel.addColumn(new PhotoColumn());
      speciesTablePanel.addColumn(new NumberColumn());
      speciesTablePanel.addColumn(new SightingStatusColumn());
      speciesTablePanel.addColumn(sightingInfoColumn);
      if (reportSet.getUserSet() != null) {
        speciesTablePanel.addColumn(new ObserversColumn());
      }
      ((DirectSpeciesTablePanel) speciesTablePanel).addDeleteColumn();    
    } else {
      boolean includeRarities =
          checklistCombo.getSelectedItem() == DataEntryPreferences.ChecklistUse.YES_WITH_RARITIES;
      // If the user is using a checklist, then choose the ChecklistSpeciesTablePanel
      speciesTablePanel = new ChecklistSpeciesTablePanel(
          new SightingInfoDetail(), "Show sighting details",
          fontManager,
          checklist,
          includeRarities,
          currentTaxonomy);
      speciesTablePanel.setEditable(true);
      // ... which has a subspecies and sighting info column
      speciesTablePanel.addColumn(new SubspeciesColumn());
      speciesTablePanel.addColumn(new PhotoColumn());
      speciesTablePanel.addColumn(new NumberColumn());
      speciesTablePanel.addColumn(new SightingStatusColumn());
      speciesTablePanel.addColumn(sightingInfoColumn);
      if (reportSet.getUserSet() != null) {
        speciesTablePanel.addColumn(new ObserversColumn());
      }
    }
    
    speciesTablePanel.setSortedContents(currentTaxonomy, existingResolved);
    
    // Preemptively force component creation, in case the table isn't already showing
    speciesTablePanel.getSpeciesTable().forceComponentCreation();
    
    // Give the table explicit instructions as to what component comes after and before the table.
    // This saves it from expensive (and sometimes error-prone) computation.
    if (isShowing()) {
      speciesTablePanel.getSpeciesTable().setFocusableComponentAfterTable(
          getFocusableComponentAfterContent());
      speciesTablePanel.getSpeciesTable().setFocusableComponentBeforeTable(
          speciesEntryPanel.getLastFocusableComponent());
    }

    // Add a shortcut to jump back to the species indexer
    KeyStroke backToIndexer;
    if (UIUtils.isMacOS()) {
      backToIndexer = KeyStroke.getKeyStroke(KeyEvent.VK_LEFT,
          Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() | InputEvent.ALT_MASK);
    } else {
      // Ctrl-alt-arrow twirls the monitor on Windows!
      backToIndexer = KeyStroke.getKeyStroke(KeyEvent.VK_LEFT,
          InputEvent.SHIFT_MASK | InputEvent.ALT_MASK);
    }
    
    speciesTablePanel.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
        .put(backToIndexer, "focusOnIndexer");
    speciesTablePanel.getActionMap().put("focusOnIndexer", new AbstractAction() {
      @Override public void actionPerformed(ActionEvent e) {
        SingleLocationSpeciesListPanel.this.speciesEntryPanel.getIndexer().transferFocus();
      }      
    });
    
    speciesTablePanel.addPropertyChangeListener("value", new PropertyChangeListener() {
      @SuppressWarnings("unchecked")
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        List<Resolved> taxa = (List<Resolved>) evt.getNewValue();
        updateCompleteAndTaxaCount(taxa, currentTaxonomy);
      }
    });
    // For any explicit user edits, note that the value is modified
    speciesTablePanel.addUserModificationListener(() -> {
      wizardValue().edited();
    });
    if (isShowing()) {
      updateCompleteAndTaxaCount(existingResolved, currentTaxonomy);
    }

    // Let the species entry panel know when the selected species changes,
    // so that it can show appropriate details
    speciesTablePanel.addPropertyChangeListener("selectedValue", new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        SingleLocationSpeciesListPanel.this.speciesEntryPanel.setFocusResolver(speciesTablePanel::getSelectedSpecies);
      }      
    });
    
    speciesTablePanel.setIsImportant(new Predicate<Resolved>() {
      @Override
      public boolean apply(Resolved input) {
        if (input.getType() != SightingTaxon.Type.SINGLE) {
          return false;
        }
        // Check if seen-taxa are available;  if not, return false.  Stash in a local
        // variable to avoid race conditions with resetting the set
        if (scanSeenTaxa == null) {
          return false;
        }
        
        Set<String> currentSeenTaxa = scanSeenTaxa.getSeenTaxa(input.getTaxonomy(), null);
        if (currentSeenTaxa == null) {
          return false;
        }
        
        SightingInfo sightingInfo = getCurrentSightingInfo(input);
        if (!queryPreferences.countHeardOnly && sightingInfo.isHeardOnly()) {
          return false;
        }
        if (!queryPreferences.countIntroduced
            && sightingInfo.getSightingStatus() == SightingStatus.INTRODUCED) {
          return false;
        }
        
        if (sightingInfo.getSightingStatus() == SightingStatus.INTRODUCED_NOT_ESTABLISHED
            || sightingInfo.getSightingStatus() == SightingStatus.ID_UNCERTAIN
            || sightingInfo.getSightingStatus() == SightingStatus.UNSATISFACTORY_VIEWS
            || sightingInfo.getSightingStatus() == SightingStatus.DOMESTIC
            || sightingInfo.getSightingStatus() == SightingStatus.DEAD
            || sightingInfo.getSightingStatus() == SightingStatus.SIGNS
            || sightingInfo.getSightingStatus() == SightingStatus.NOT_BY_ME) {
          return false;
        }
            
        // TODO: if this proves to be a bit slow (evaluating on every cell-paint),
        // expand seen-taxa upward to species level.
        return !TaxonUtils.isItselfOrAChildIn(input.getTaxon(), currentSeenTaxa);
      }
    });

    speciesTablePanel.getSpeciesTable().addKeyListener(new KeyAdapter() {
      @Override
      public void keyTyped(KeyEvent e) {
        if (e.getKeyChar() >= '0' && e.getKeyChar() <= '9') {
          Resolved selectedSpecies = speciesTablePanel.getSelectedSpecies();
          if (selectedSpecies != null) {
            ExpandableTable<Resolved> speciesTable = speciesTablePanel.getSpeciesTable();
            TableRowsModel enabledRowsModel = speciesTable.getEnabledRowsModel();
            if (!enabledRowsModel.isIncluded(speciesTable.getSelectedIndex())) {
              enabledRowsModel.includeRow(speciesTable.getSelectedIndex());
            }
            
            redirectFocusToNumberFieldOfSelectedSpecies(e, selectedSpecies);
          }
        }
      }});
}

  @Override
  public String getTitle() {
    return Messages.getMessage(Name.ENTER_SPECIES);
  }
  
  @Override
  protected void shown() {
    super.shown();
    
    if (taxonomy != taxonomyStore.getTaxonomy()) {
      taxonomy = taxonomyStore.getTaxonomy();
    }
    
    // See if the user set has changed, and therefore we need to add or remove users
    // from sightings.
    ImmutableSet<User> usersOnSpeciesList = wizardValue().getUsersOnSpeciesList();
    ImmutableSet<User> users = wizardValue().getUsers();

    // If the two lists are different, time to modify all the sighting info.
    if (usersOnSpeciesList != null && users != null && !users.equals(usersOnSpeciesList)) {
      SetView<User> usersToRemove = Sets.difference(usersOnSpeciesList, users);
      SetView<User> usersToAdd = Sets.difference(users, usersOnSpeciesList);
      for (SightingInfo sightingInfo : wizardValue().getSightingInfos()) {
        ImmutableSet.Builder<User> sightingUsers = ImmutableSet.builder();
        sightingUsers.addAll(Sets.difference(sightingInfo.getUsers(), usersToRemove));
        sightingUsers.addAll(usersToAdd);
        sightingInfo.setUsers(sightingUsers.build());
      }
    }
    
    // Mark that getUsers() has now been seen, mark that on wizardValue().
    wizardValue().setUsersOnSpeciesList(wizardValue().getUsers());
    
    startScoringLocations();
    
    speciesEntryPanel.setTaxonomy(taxonomy);
    speciesEntryPanel.setIndexerLabel(getWhereAndWhen());
    speciesEntryPanel.getIndexer().requestFocusInWindow();
    // After the table is shown, reset the after-and-before components
    speciesTablePanel.getSpeciesTable().setFocusableComponentAfterTable(
        getFocusableComponentAfterContent());
    speciesTablePanel.getSpeciesTable().setFocusableComponentBeforeTable(
        speciesEntryPanel.getLastFocusableComponent());
    
    updateChecklist(taxonomy);
    rebuildTable(taxonomy);
    
    VisitInfo visitInfo = wizardValue().getVisitInfo();
    // Hide the "complete" checkbox unless it's a non-incidental checklist 
    completeCheckbox.setVisible(
        visitInfo != null
        && visitInfo.observationType() != ObservationType.INCIDENTAL);
    if (completeCheckbox.isVisible()) {
      completeCheckbox.setSelected(wizardValue().getVisitInfo().completeChecklist());
    } else {
      completeCheckbox.setSelected(false);
    }
    
    // See if the table's entries match the current taxa.  If they don't, update the contents.
    // This serves to load species snapshots.
    ImmutableList<Resolved> currentTableEntries = speciesTablePanel.getValue();
    List<Resolved> wizardEntries = wizardValue().getCompatibleTaxa(taxonomyStore.getTaxonomy());
    if (!currentTableEntries.equals(wizardEntries)) {
      DelayedUI.instance().enableDelay();
      try {
        speciesTablePanel.setSortedContents(taxonomyStore.getTaxonomy(), wizardEntries);
      } finally {
        DelayedUI.instance().liftDelay();
      }
    }
  }

  /**
   * Update the checklist-related UI.
   */
  private void updateChecklist(Taxonomy currentTaxonomy) {
    // Find the checklist for this location
    checklist = null;
    checklistLocation = wizardValue().getLocation();
    while (checklistLocation != null && checklist == null) {
      checklist = checklists.getChecklist(reportSet, currentTaxonomy, checklistLocation);
      if (checklist == null) {
        checklistLocation = checklistLocation.getParent();
      }
    }
    
    speciesEntryPanel.setChecklist(checklist, checklistLocation);
    
    // Update the UI to accommodate for presence of checklists
    if (checklistLocation == null) {
      checklistLabel.setVisible(false);
      checklistCombo.setVisible(false);
    } else {
      checklistLabel.setText(
          Messages.getFormattedMessage(Name.USE_CHECKLIST_QUESTION,
              checklistLocation.getDisplayName()));
      // If there's any rarities set on the checklist, then enable the bonus "YES_WITH_RARITIES"
      // option.
      boolean hasRarities = !checklist.getTaxa(currentTaxonomy, Status.RARITY).isEmpty()
          || !checklist.getTaxa(currentTaxonomy, Status.RARITY_FROM_INTRODUCED).isEmpty();
      if (hasRarities) {
        checklistCombo.setModel(new DefaultComboBoxModel<ChecklistUse>(new ChecklistUse[]{
            ChecklistUse.NO,
            ChecklistUse.YES,
            ChecklistUse.YES_WITH_RARITIES}));
      } else {
        checklistCombo.setModel(new DefaultComboBoxModel<ChecklistUse>(new ChecklistUse[]{
            ChecklistUse.NO,
            ChecklistUse.YES}));
      }
      ChecklistUse checklistUse = dataEntryPreferences.checklistUse;
      if (checklistUse == null) {
        checklistUse = ChecklistUse.NO;
      } else if (checklistUse == ChecklistUse.YES_WITH_RARITIES && !hasRarities) {
        checklistUse = ChecklistUse.YES;
      }
      
      // Remove the action listener around this toggle;  it's wasteful (and depending on what's triggering the
      // checklist update, dangerous).
      checklistCombo.removeActionListener(checklistUpdated);
      checklistCombo.setSelectedItem(checklistUse);
      checklistCombo.addActionListener(checklistUpdated);
      
      checklistLabel.setVisible(true);
      checklistCombo.setVisible(true);
    }
  }
  
  @Override
  protected boolean leaving(boolean validate) {
    super.leaving(validate);
    
    // Flush any edits (especially if there's an expanded species detail region)
    speciesTablePanel.flushEdits();
    
    // And push all taxa into the overall edit - only if necessary, because otherwise
    // it'll unnecessarily dirty the edit.
    ImmutableList<Resolved> taxa = speciesTablePanel.getValue();
    if (!wizardValue().getCompatibleTaxa(taxonomy).equals(taxa)) {
      wizardValue().syncTaxa(taxonomy, speciesTablePanel.getValue());
    }
    
    return true;
  }
  
  private String getWhereAndWhen() {
    StringBuilder builder = new StringBuilder();
    builder.append(wizardValue().getLocation().getDisplayName());

    if (wizardValue().getDate() != null) {
      builder.append(", ");
      builder.append(PartialIO.toUserString(wizardValue().getDate(), getLocale()));
    }
    
    return builder.toString();
  }
  
  private class SightingInfoDetail implements DetailView<Resolved> {
    SightingInfoDetail() {}

    @Override public JComponent createComponent(final Resolved taxon) {
      SightingInfoPanel panel =
          new SightingInfoPanel(reportSet, SightingInfoPanel.Layout.HORIZONTAL, fontManager, fileDialogs, alerts);
      fontManager.applyTo(panel);
      panel.setSighting(wizardValue().getSightingInfo(taxon));
      panel.getDirty().addDirtyListener(e -> wizardValue().edited());
      panel.addPropertyChangeListener("value",
          // HACK:  every time the species info gets updated, tell the table panel about it.
          // This updates the photo column, as well as (depending on introduced and heard-only
          // and preferences) the "New for" column and whether the species is in bold
          e -> {
            // And also write to wizard storage, which let us simplify
            // SingleLocationSpeciesListPanel.getCurrentSightingInfo() a lot,
            // and address http://bitbucket.org/scythebill/scythebill-birdlist/issues/497
            panel.save();
            speciesTablePanel.resolvedUpdated(taxon);
          }); 
      return panel;
    }

    @Override public void onClose(JComponent component) {
      SightingInfoPanel panel = (SightingInfoPanel) component;
      if (panel.getDirty().isDirty()) {
        panel.save();
      }
    }

    @Override public int getHeight() {
      // TODO: fix SightingInfoPanel to not require this lower bounds on size
      if (reportSet.getUserSet() != null) {
        return Math.max(200, fontManager.scale(215));
      } else {
        return Math.max(135, fontManager.scale(150));
      }
    }
  }
  
  /** Key for a client property storing the current Taxon. */ 
  private final static Object TAXON_KEY = new Object();
  private Location checklistLocation;
  private Location countryLocation;
  private Location stateLocation;
  private Location countyLocation;
  
  private class NewForColumn extends LabelColumn<Resolved> {
    public NewForColumn() {
      super(ColumnLayout.weightedWidth(0.7f), Messages.getMessage(Name.NEW_FOR_COLUMN));
    }
    
    @Override public void updateComponentValue(Component component, Resolved resolved) {
      JLabel label = (JLabel) component;
      String text = "";
      String tooltip = null;
      
      SightingTaxon taxon;
      if (resolved.getSmallestTaxonType() != Taxon.Type.species) {
        taxon = resolved.getParentOfAtLeastType(Taxon.Type.species);
      } else {
        taxon = resolved.getSightingTaxon();
      }

      if (taxon.getType() == SightingTaxon.Type.SINGLE
          || taxon.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
        
        if (checklist != null) {
          // Use the taxonomy of this resolved, not the global taxonomy
          // These are typically the same, but different briefly during a taxonomy flip.
          Status status = checklist.getStatus(resolved.getTaxonomy(), taxon);
          if (status == null) {
            text = Messages.getMessage(Name.NEW_FOR_CHECKLIST);
            tooltip = Messages.getFormattedMessage(
                Name.NOT_FOUND_ON_CHECKLIST_FORMAT,
                checklistLocation.getDisplayName());
          }
        }
        
        if (scanSeenTaxa != null && text.isEmpty()) {
          SightingInfo sightingInfo = getCurrentSightingInfo(resolved);

          if (isNewForLocation(resolved.getTaxonomy(), null, taxon, 0)) {
            text = Messages.getMessage(Name.WORLD_TEXT);
          } else {
            List<String> userAbbreviations = null;
            for (User user : sightingInfo.getUsers()) {
              if (isNewForUser(resolved.getTaxonomy(), user, taxon)) {
                if (userAbbreviations == null) {
                  userAbbreviations = new ArrayList<>();
                }
                userAbbreviations.add(user.abbreviation()); 
              }
            }
            
            if (userAbbreviations != null) {
              text = Joiner.on(',').join(userAbbreviations);
            } else if (countryLocation != null 
                && isNewForLocation(resolved.getTaxonomy(), countryLocation, taxon, 0)) {
              text = locationOrAbbreviated(countryLocation);
            } else if (stateLocation != null && isNewForLocation(resolved.getTaxonomy(), stateLocation, taxon, 0)) {
              text = locationOrAbbreviated(stateLocation);
            } else if (countyLocation != null
                && isNewForLocation(resolved.getTaxonomy(), countyLocation, taxon, 0)) {
              text = locationOrAbbreviated(countyLocation);
            } else {
              Location location = wizardValue().getLocation();
              if (location != null && location.getId() != null) {
                // Require at least two prior visits to the current location to bother checking if
                // it's new for that location
                if (isNewForLocation(resolved.getTaxonomy(), location, taxon, 2)) {
                  text = locationOrAbbreviated(location);
                }
              }
            }
            
            if (sightingInfo.isPhotographed() && isNewForPhotographed(resolved.getTaxonomy(), taxon)) {
              text = Messages.getMessage(Name.PHOTOGRAPHED_TEXT); 
            }
            
            if (text.isEmpty() && isNewForYear(resolved.getTaxonomy(), taxon)) {
              text = Messages.getMessage(Name.YEAR_TEXT);
            }
          }
  
          // Reset the "new for" if the sighting is not countable
          if (!queryPreferences.countHeardOnly && sightingInfo.isHeardOnly()) {
            text = "";
          } else if (!queryPreferences.countIntroduced
              && sightingInfo.getSightingStatus() == SightingStatus.INTRODUCED) {
            text = "";
          } else if (!queryPreferences.countRestrained
              && sightingInfo.getSightingStatus() == SightingStatus.RESTRAINED) {
            text = "";
          } else if (sightingInfo.getSightingStatus() == SightingStatus.INTRODUCED_NOT_ESTABLISHED
              || sightingInfo.getSightingStatus() == SightingStatus.ID_UNCERTAIN
              || sightingInfo.getSightingStatus() == SightingStatus.UNSATISFACTORY_VIEWS
              || sightingInfo.getSightingStatus() == SightingStatus.DOMESTIC
              || sightingInfo.getSightingStatus() == SightingStatus.DEAD
              || sightingInfo.getSightingStatus() == SightingStatus.SIGNS
              || sightingInfo.getSightingStatus() == SightingStatus.NOT_BY_ME) {
            text = "";
          }
        }
      }
      
      label.setText(text);
      label.setToolTipText(tooltip);
    }

    @Override
    public void updateComponentState(Component component, Resolved resolved, Set<RowState> states) {
      component.setVisible(states.contains(RowState.ENABLED));
    }
    
    private String locationOrAbbreviated(Location location) {
      String name = location.getDisplayName();
      if (name.length() > 20) {
        return name.substring(0, 19) + '\u2026';
      }
      return name;
    }
  }
  
  private void redirectFocusToNumberFieldOfSelectedSpecies(KeyEvent originalKeyEvent,
      Resolved selectedSpecies) {
    speciesTablePanel.expandSpeciesDetail(selectedSpecies);
    JComponent expandedRowComponent = speciesTablePanel.getSpeciesTable().getExpandedRowComponent();
    if (expandedRowComponent != null) {
      SightingInfoPanel sightingInfoPanel = (SightingInfoPanel) expandedRowComponent;
      final JFormattedTextField numberField = sightingInfoPanel.getNumberField();
      final String textValue = "" + originalKeyEvent.getKeyChar();
      final FocusListener moveSelectionToEnd = new FocusAdapter() {
        @Override
        public void focusGained(FocusEvent e) {
          // Play a little game.  The number field is an "auto-select-on-focus-entry"
          // text field.  This means that if we request focus and select the value,
          // then the value will get fully selected.  So:  request focus, then
          // once focus is actually gained, replace the text and move the focus to
          // the end of the field.  (Then remove the listener, whose job is done). 
          numberField.setText(textValue);
          numberField.getCaret().setDot(1);
          numberField.removeFocusListener(this);
          
          // Mark the sighting info panel dirty, because this isn't happening consistently
          sightingInfoPanel.getDirty().setDirty(true);
          
          // And now, add a key listener, which on '\n' moves the focus back
          // to the indexer.  This means a user can, from the indexer,
          // type a number and hit enter, and *boom* done
          numberField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
              if (e.getKeyChar() == '\n') {
                originalKeyEvent.getComponent().requestFocusInWindow();
                numberField.removeKeyListener(this);
                e.consume();
              }
            }
          });
        }
      };
      numberField.addFocusListener(moveSelectionToEnd);
      numberField.requestFocusInWindow();
      originalKeyEvent.consume();
    }
  }

  private boolean isNewForLocation(Taxonomy taxonomy, Location location, SightingTaxon taxon, int minimumVisitsRequired) {
    if (scanSeenTaxa == null) {
      return false;
    }
    
    if (minimumVisitsRequired > 0) {
      int visitCount = scanSeenTaxa.getVisitCount(location);
      if (visitCount < minimumVisitsRequired) {
        return false;
      }
    }
    
    Set<String> seenTaxa = scanSeenTaxa.getSeenTaxa(
        taxonomy, location);
    if (seenTaxa == null) {
      return false;
    }
    
    return !seenTaxa.contains(taxon.getId());
  }
  
  private boolean isNewForUser(Taxonomy taxonomy, User user, SightingTaxon taxon) {
    if (scanSeenTaxa == null) {
      return false;
    }

    Set<String> seenTaxa = scanSeenTaxa.getTaxaForUser(taxonomy, user);
    if (seenTaxa == null) {
      return false;
    }
    
    return !seenTaxa.contains(taxon.getId());
  }

  private boolean isNewForYear(Taxonomy taxonomy, SightingTaxon taxon) {
    if (scanSeenTaxa == null) {
      return false;
    }
    
    ReadablePartial date = wizardValue().getDate();
    if (date == null || !date.isSupported(DateTimeFieldType.year())) {
      return false;
    }
    
    Set<String> yearTaxa = scanSeenTaxa.getYearTaxa(taxonomy, date.get(DateTimeFieldType.year()));
    if (yearTaxa == null) {
      return false;
    }
    
    return !yearTaxa.contains(taxon.getId());
  }

  private boolean isNewForPhotographed(Taxonomy taxonomy, SightingTaxon taxon) {
    if (scanSeenTaxa == null) {
      return false;
    }
    
    Set<String> photographedTaxa = scanSeenTaxa.getPhotographedTaxa(taxonomy);
    if (photographedTaxa == null) {
      return false;
    }
    
    return !photographedTaxa.contains(taxon.getId());
  }
  
  private static final Object FIRE_PENDING_ACTION = "firePendingAction";
  private static final Object POPUP_WILL_BE_VISIBLE = "popupWillBeVisible";
  
  /**
   * Column providing subspecies display and editing.
   */
  private class SubspeciesColumn implements Column<Resolved> {
    private final ColumnLayout width = ColumnLayout.weightedWidth(1.0f);
    
    @Override public JComponent createComponent(Resolved resolved, Set<RowState> states) {
      if (resolved.getType() != SightingTaxon.Type.SINGLE) {
        Taxon.Type largestTaxonType = resolved.getLargestTaxonType();
        if (largestTaxonType == Taxon.Type.group || largestTaxonType == Taxon.Type.subspecies) {
          JLabel label = new JLabel(resolved.getPreferredSingleNameWithoutSpecies());
          label.putClientProperty(FontManager.PLAIN_LABEL, true);
          return label;
        } else {
          return new JLabel("");
        }
      }
      
      // No subspecies
      Taxon taxon = resolved.getTaxon();
      if (taxon.getType() == Type.species && taxon.getContents().isEmpty()) {
        return new JLabel("") {
          @Override
          protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
            // Stop Swing from adding a bazillion AncestorListeners to the table via a vile hack.
            if (!"parent".equals(propertyName) && !"ancestor".equals(propertyName)) {
              super.firePropertyChange(propertyName, oldValue, newValue);
            }
          }
        };
      }
      
      final JComboBox<Taxon> subspeciesCombo = new JComboBox<Taxon>() {
        @Override
        protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
          // Stop Swing from adding a bazillion AncestorListeners to the table via a vile hack.
          if (!"parent".equals(propertyName) && !"ancestor".equals(propertyName)) {
            super.firePropertyChange(propertyName, oldValue, newValue);
          }
        }

        @Override
        public void updateUI() {
          // Delay creation of the combobox (which is expensive) when there's lots of components
          if (DelayedUI.instance().requestUICreation(this)) {
            super.updateUI();
          }
        }
      };
      subspeciesCombo.setRenderer(new ToStringListCellRenderer<Taxon>(subspeciesToString, Taxon.class, defaultRenderer) {
        @Override public Component getListCellRendererComponent(@SuppressWarnings("rawtypes") JList list,
            Object value, int index, boolean isSelected, boolean cellHasFocus) {
          if (isSelected) {
            subspeciesPopupTaxon = (Taxon) value;
            Resolved resolved;
            if (value != null) {
              resolved = SightingTaxons.newResolved(subspeciesPopupTaxon);
            } else {
              resolved = speciesTablePanel.getSelectedSpecies();
            }
            speciesEntryPanel.setFocusResolver(Suppliers.ofInstance(resolved));
          }
          
          return super.getListCellRendererComponent(list, value, index, isSelected,
              cellHasFocus);
        }
      });
      buildSubspeciesModel(taxon, subspeciesCombo);
      subspeciesCombo.setMaximumRowCount(Math.min(12, subspeciesCombo.getModel().getSize()));
      subspeciesCombo.addPopupMenuListener(new PopupMenuListener() {
        @Override public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
          subspeciesCombo.putClientProperty(POPUP_WILL_BE_VISIBLE, true);
        }
        
        @Override public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
          if (Boolean.TRUE.equals(subspeciesCombo.getClientProperty(FIRE_PENDING_ACTION))) {
            subspeciesCombo.putClientProperty(POPUP_WILL_BE_VISIBLE, null);
            subspeciesCombo.putClientProperty(FIRE_PENDING_ACTION, null);
            ActionEvent event = new ActionEvent(subspeciesCombo, 0, "");
            for (ActionListener listener : subspeciesCombo.getActionListeners()) {
              listener.actionPerformed(event);
            }
          }
        }
        
        @Override public void popupMenuCanceled(PopupMenuEvent e) {
          subspeciesCombo.putClientProperty(POPUP_WILL_BE_VISIBLE, null);
          subspeciesCombo.putClientProperty(FIRE_PENDING_ACTION, null);
        }
      });
      subspeciesCombo.addActionListener(new ActionListener() {
        @Override public void actionPerformed(ActionEvent e) {
          speciesEntryPanel.setFocusResolver(new Supplier<Resolved>() {
            @Override public Resolved get() {
              if (currentSubspeciesComboBox != null) {
                Taxon taxon = (Taxon) currentSubspeciesComboBox.getSelectedItem();
                if (taxon == null) {
                  taxon = (Taxon) currentSubspeciesComboBox.getClientProperty(TAXON_KEY);            
                }
                
                if (taxon != null) {
                  return SightingTaxons.newResolved(taxon);
                }
              }
              
              return speciesTablePanel.getSelectedSpecies();
            }
          });
          
          // If the popup is currently visible (and the "will-be-invisible" event hasn't fired yet),
          // then wait to fire the action until the "will-be-invisible" fires.
          if (Boolean.TRUE.equals(subspeciesCombo.getClientProperty(POPUP_WILL_BE_VISIBLE))) {
            subspeciesCombo.putClientProperty(FIRE_PENDING_ACTION, true);
            return;
          }
          
          subspeciesCombo.putClientProperty(FIRE_PENDING_ACTION, null);
          

          boolean isFocusOwner = subspeciesCombo.isFocusOwner();
          Taxon current = (Taxon) subspeciesCombo.getClientProperty(TAXON_KEY);
          Taxon subspeciesOrGroup = (Taxon) subspeciesCombo.getSelectedItem();
          updateSubspeciesOfCurrentItem(current, subspeciesOrGroup);

          // Try to move the focus back to the combobox (which will be a new component).
          // If that doesn't work, just refocus the species table 
          if (isFocusOwner) {
            boolean movedFocus = false;
            int row = speciesTablePanel.findTaxon(SightingTaxons.newResolved(subspeciesOrGroup));
            if (row >= 0) {
              Component component = speciesTablePanel.getSpeciesTable().getComponent(row, SubspeciesColumn.this);
              if (isFocusable(component)) {
                component.requestFocusInWindow();
                movedFocus = true;
              }
            }
            if (!movedFocus) {
              speciesTablePanel.requestFocusInWindow();
            }
          }
        }
      });
      subspeciesCombo.addPopupMenuListener(new PopupMenuListener() {
        @Override public void popupMenuCanceled(PopupMenuEvent e) {
        }

        @Override public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        }

        @Override public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
          currentSubspeciesComboBox = subspeciesCombo;
        }        
      });
      updateComponentState(subspeciesCombo, resolved, states);
      return subspeciesCombo;
    }

    @Override public ColumnLayout getWidth() {
      return width;
    }

    @Override
    public boolean sizeComponentsToFit() {
      // This column looks better when it's sized to fit the content
      return true;
    }

    @SuppressWarnings("unchecked")
    @Override public void updateComponentValue(Component component, Resolved value) {      
      if (!(component instanceof JComboBox)) {
        // TODO: is it possible to toggle from species w/o subspecies to one with?  Don't
        // think there's any UI affordance for that, so we can simply drop this case
      } else {
        buildSubspeciesModel(value.getTaxon(), (JComboBox<Taxon>) component);
      }
    }
    
    /** Set up the subspecies model */
    private void buildSubspeciesModel(Taxon taxon, JComboBox<Taxon> subspeciesCombo) {
      subspeciesCombo.putClientProperty(TAXON_KEY, taxon);

      final DefaultComboBoxModel<Taxon> model = new DefaultComboBoxModel<Taxon>();
      
      final Taxon species = TaxonUtils.getParentOfType(taxon, Type.species);
      model.addElement(species);
      TaxonUtils.visitTaxa(species, new TaxonVisitor() {
        @Override public boolean visitTaxon(Taxon visited) {
          if (species != visited && !visited.isDisabled()) {
            model.addElement(visited);
          }
          
          return true;
        }
        
      });
      
      subspeciesCombo.setEnabled(model.getSize() > 1);
      
      // And if the taxon is itself a subspecies/group, select it
      if (taxon.getType().compareTo(Type.species) < 0) {
        model.setSelectedItem(taxon);
      }
      
      subspeciesCombo.setModel(model);
    }

    @Override public String getName() {
      return Messages.getMessage(Name.SUBSPECIES_COLUMN);
    }

    @Override public void updateComponentState(Component component,
        Resolved resolved, Set<RowState> states) {
      component.setEnabled(states.contains(RowState.ENABLED));
    }
  }
  
  @Subscribe
  public void taxonomyChanged(TaxonomyChangedEvent event) {
    setTaxonomy(event.getTaxonomy(), false);
  }
  
  private void setTaxonomy(Taxonomy newTaxonomy, boolean skipTableUpdates) {
    if (newTaxonomy == this.taxonomy) {
      return;
    }
    
    List<Resolved> newCompatibleTaxa;
    if (skipTableUpdates) {
      newCompatibleTaxa = null;
    } else {
      // Make sure the currently selected taxa have been sync'd (for this taxonomy,
      // leaving alone incompatible taxonomies)
      wizardValue().syncTaxa(this.taxonomy, speciesTablePanel.getValue());
      // And, since changing taxonomies changes the checklist, get that sync'd too
      updateChecklist(newTaxonomy);
      
      // Taxa for the new taxonomy
      newCompatibleTaxa = new ArrayList<>();
      // The set of pre-existing taxa compatible with this taxonomy
      List<Resolved> previousCompatibleTaxa;
      // Any failed mappings in computing newCompatibleTaxa
      List<String> failedMappings = new ArrayList<>();
      
      if (TaxonUtils.areCompatible(newTaxonomy, this.taxonomy)) {
        // When mapping between compatible taxonomies, start from the taxa in the table already
        previousCompatibleTaxa = speciesTablePanel.getValue();
      } else {
        // ... but when mapping between incompatible taxonomies, go back to the wizard value
        // and grab all existing compatible taxa.
        previousCompatibleTaxa = wizardValue().getCompatibleTaxa(newTaxonomy);
      }
      
      // Map all species into the new taxonomy
      for (Resolved resolved : previousCompatibleTaxa) {
        // ... already the right taxonomy, done.
        if (resolved.getTaxonomy() == newTaxonomy) {
          newCompatibleTaxa.add(resolved);
        } else if (resolved.getTaxonomy() instanceof MappedTaxonomy) {
          // MappedTaxonomy to base
          MappedTaxonomy mapped = (MappedTaxonomy) resolved.getTaxonomy();
          Preconditions.checkState(newTaxonomy == mapped.getBaseTaxonomy());
          SightingTaxon baseMapping = mapped.getMapping(resolved.getSightingTaxon());
          if (baseMapping == null) {
            failedMappings.add(HtmlResponseWriter.htmlEscape(resolved.getCommonName()));
          } else {
            newCompatibleTaxa.add(baseMapping.resolveInternal(newTaxonomy));
          }
        } else {
          // base to MappedTaxonomy
          Preconditions.checkState(newTaxonomy instanceof MappedTaxonomy);
          MappedTaxonomy mapped = (MappedTaxonomy) newTaxonomy;
          Preconditions.checkState(resolved.getTaxonomy() == mapped.getBaseTaxonomy());
          Resolved mappedResolved = mapped.resolveInto(resolved.getSightingTaxon());
          if (mappedResolved == null) {
            failedMappings.add(HtmlResponseWriter.htmlEscape(resolved.getCommonName()));
          } else {
            newCompatibleTaxa.add(mappedResolved);
          }
        }
      }
      
      // If there are any failed mappings, warn the user first.
      if (!failedMappings.isEmpty()) {
        int option = alerts.showOkCancel(this,
            Name.FAILED_MAPPINGS_TITLE,
            Name.FAILED_MAPPINGS_FORMAT,
            newTaxonomy.getName(),
            Joiner.on("<br>").join(failedMappings));
        if (option != JOptionPane.OK_OPTION) {
          taxonomyStore.setTaxonomy(this.taxonomy);
          return;
        }
      }
  
      Collections.sort(newCompatibleTaxa, new ResolvedComparator());
            
      speciesTablePanel.flushEdits();
      // We may be toggling between checklist and non-checklist or vice versa,
      // depending on the taxonomy state.  Force a rebuild of the table in all cases.
      updateChecklist(newTaxonomy);
      // Clear the table before attempting to rebuild
      speciesTablePanel.setSortedContents(taxonomy, ImmutableList.of());
      rebuildTable(newTaxonomy);
      
      updateCompleteAndTaxaCount(newCompatibleTaxa, newTaxonomy);
    }
    
    this.taxonomy = newTaxonomy;
    
    speciesEntryPanel.setTaxonomy(newTaxonomy);
    
    if (isShowing()) {
      wizardValue().setTaxonomy(newTaxonomy, speciesTablePanel.getValue());
      startScoringLocations();
    }

    // At the very end, set the sorted contents.  Doing so *before* setting the 
    // taxonomy on the species entry panel meant that the SingleLocationEdit hadn't
    // recomputed the SightingInfo mappings and blank sighting info would be displayed
    // (even though storage had the right values).
    if (newCompatibleTaxa != null) {
      speciesTablePanel.setSortedContents(newTaxonomy, newCompatibleTaxa);
    }  
  }

  private void startScoringLocations() {
    if (scanSeenTaxa != null) {
      scanSeenTaxa.cancel();
    }

    Location rootLocation = wizardValue().getLocation();
    
    // Find the county, state, and country location (if any)
    List<Location> locations = new ArrayList<>();
    countryLocation = null;
    stateLocation = null;
    countyLocation = null;
    
    Location perhapsInterestingLocation = rootLocation;
    // Add the current location unless it's already going to be 
    // treated as a country/state/county
    if (rootLocation != null
        && rootLocation.getId() != null
        && rootLocation.getType() != Location.Type.country
        && rootLocation.getType() != Location.Type.state
        && rootLocation.getType() != Location.Type.county) {
      locations.add(rootLocation);
      perhapsInterestingLocation = rootLocation.getParent();
    }
    
    while (perhapsInterestingLocation != null) {
      if (perhapsInterestingLocation.getType() != null) {
        switch (perhapsInterestingLocation.getType()) {
          case country:
            if (countryLocation == null) {
              countryLocation = perhapsInterestingLocation;
              locations.add(perhapsInterestingLocation);
            }
            break;
          case state:
            if (stateLocation == null) {
              stateLocation = perhapsInterestingLocation;
              locations.add(perhapsInterestingLocation);
            }
            break;
          case county:
            if (countyLocation == null) {
              countyLocation = perhapsInterestingLocation;
              locations.add(perhapsInterestingLocation);
            }
            break;
          default:
            break;
        }
      }
      perhapsInterestingLocation = perhapsInterestingLocation.getParent();
    }
    locations.add(null);
    
    // Don't consider anything as "seen" if it's already in the report for the
    // very date and location here (because we're likely editing it)
    Predicate<Sighting> sightingsToInclude;
    if (wizardValue().getLocation() == null
        || wizardValue().getLocation().getId() == null) {
      sightingsToInclude = Predicates.alwaysTrue();
    } else {
      final String locationId = wizardValue().getLocation().getId();
      final ReadablePartial date = wizardValue().getDate();
      sightingsToInclude = new Predicate<Sighting>() {
        @Override public boolean apply(Sighting sighting) {
          return !Objects.equal(locationId, sighting.getLocationId())
              || !Objects.equal(date, sighting.getDateAsPartial());
        }
      };
    }
    
    // Only include "countable" sightings 
    sightingsToInclude = Predicates.and(
        sightingsToInclude,
        queryPreferences.getCountablePredicate(taxonomy, false, null));
    
    // Scan the "current" year (whatever date is being entered)
    OptionalInt yearToScan;
    ReadablePartial date = wizardValue().getDate();
    if (date != null && date.isSupported(DateTimeFieldType.year())) {
      yearToScan = OptionalInt.of(date.get(DateTimeFieldType.year()));
    } else {
      yearToScan = OptionalInt.empty();
    }
    ImmutableSet<User> users = wizardValue().getUsers();
    if (!users.isEmpty()) {
      sightingsToInclude = Predicates.and(
          sightingsToInclude,
          SightingPredicates.includesAnyOfUsers(users));
    }
    scanSeenTaxa = ScanSeenTaxa.start(
        reportSet, taxonomy, executorService, 
        (scanned) -> { speciesTablePanel.getSpeciesTable().resetColumnValues(sightingInfoColumn); },
        locations, sightingsToInclude,
        new ScanSeenTaxa.Options().setYearToScan(yearToScan).setUsers(users));
    
    // Kick off the scorer, and make sure it gets cancelled on leaving the window
    // Score based on the first location with a non-null ID (e.g.,
    // the location that was already present.)
    while (rootLocation != null && rootLocation.getId() == null) {
      // HACK.  For built-in locations with checklists, force it to be added so that it has an ID
      // so that it can be considered for scoring.  Without this, the checklist for that location
      // would be ignored!
      if (rootLocation.isBuiltInLocation()
          && checklists.hasChecklist(taxonomy, reportSet, rootLocation)) {
        // And HACK HACK HACK.  This shouldn't be enough to make the location set dirty (and
        // force a user to save).  So if it wasn't dirty before, mark it as not dirty after
        boolean isLocationSetDirty = reportSet.getLocations().getDirty().isDirty();
        reportSet.getLocations().ensureAdded(rootLocation);
        if (!isLocationSetDirty) {
          reportSet.getLocations().clearDirty();
        }
        break;
      }
      rootLocation = rootLocation.getParent();
    }
    locationScorer = LocationScorer.newScorer(taxonomy, reportSet, rootLocation, wizardValue().getDate(), checklists);
    speciesEntryPanel.getIndexer().setOrdering(locationScorer::ordering);
    speciesEntryPanel.getIndexer().setMixer(new Mixer<String>() {
      @Override
      public List<String> mix(List<String> original) {
        if (original.size() == 1) {
          List<String> mixed = Lists.newArrayList(original.get(0));
          mixed.addAll(getHighScoringSubspecies(original.get(0)));
          return mixed;
        } else if (!original.isEmpty()) {
          String highestScoringSubspecies = getHighestScoringSubspecies(original.get(0));
          if (highestScoringSubspecies != null) {
            List<String> mixed = new ArrayList<>();
            mixed.add(original.get(0));
            mixed.add(highestScoringSubspecies);
            mixed.addAll(original.subList(1, original.size()));
            return mixed;
          }
        }
        return original;
      }

      private String getHighestScoringSubspecies(String taxon) {
        final List<String> subspecies = gatherSubspecies(taxon);
        if (subspecies.isEmpty()) {
          return null;
        }
        // Find the "min" (highest scoring would come first, which is the "minimum"
        // in ordering terms)
        String min = locationScorer.ordering().min(subspecies);
        // But ignore anything that scores zero.
        if (locationScorer.getScore(min) == 0) {
          return null;
        }
        return min;
      }

      private List<String> getHighScoringSubspecies(String taxon) {
        final List<String> subspecies = gatherSubspecies(taxon);
        if (subspecies.isEmpty()) {
          return ImmutableList.of();
        }
        
        List<String> sorted = locationScorer.ordering().sortedCopy(subspecies);
        List<String> highScoring = new ArrayList<>();
        for (String id : sorted) {
          if (locationScorer.getScore(id) == 0) {
            break;
          }
          
          highScoring.add(id);
          if (highScoring.size() == 3) {
            break;
          }
        }
        return highScoring;
      }

      private List<String> gatherSubspecies(String taxon) {
        final List<String> subspecies = new ArrayList<>();
        TaxonUtils.visitTaxa(taxonomy.getTaxon(taxon), new TaxonVisitor() {
          @Override
          public boolean visitTaxon(Taxon taxon) {
            if (taxon.getType() != Taxon.Type.species) {
              subspecies.add(taxon.getId());
            }
            return true;
          }
        });
        return subspecies;
      }
    });
    if (scorerFuture != null) {
      scorerFuture.cancel(true);
    }
    scorerFuture = locationScorer.start(executorService);
    UiFutures.cancelFutureOnHide(scorerFuture, this);    
  }


  private static boolean isFocusable(Component candidate) {
    if (!candidate.isEnabled()
        || !candidate.isFocusable()
        || !candidate.isDisplayable()) {
      return false;
    }
    
    if (candidate instanceof JComboBox) {
      return ((JComboBox<?>) candidate).getUI().isFocusTraversable((JComboBox<?>) candidate);
    }

    if (candidate instanceof JComponent) {
      // Blargh;  no access to the override that doesn't force-create, so this will be expensive...
      InputMap whenFocusedMap = ((JComponent) candidate).getInputMap(JComponent.WHEN_FOCUSED);
      if (whenFocusedMap == null
          || whenFocusedMap.allKeys() == null
          || whenFocusedMap.allKeys().length == 0) {
        return false;
      }
    }
    
    return true;
  }
  
  /**
   * Column for showing the photos in an {@link ExpandableTable}.
   */
  private class PhotoColumn implements Column<Resolved> {
    private final MouseListener listener;
    private final Border defaultBorder = new EmptyBorder(2, 2, 2, 2);
    private final Border dragOverBorder;
    private ColumnLayout width;
    private ExpandableTable<Resolved> table;
    
    public PhotoColumn() {
      this.width = ColumnLayout.scalableFixedWidth(55);
      this.table = speciesTablePanel.getSpeciesTable();
      Color color = UIManager.getColor("Focus.color");
      if (color != null) {
        dragOverBorder = new LineBorder(color, 2, false);
      } else {
        dragOverBorder = defaultBorder;
      }

      listener = new MouseAdapter() {
        private Color savedForeground;
        
        @Override public void mouseClicked(MouseEvent e) {
          JLabel label = (JLabel) e.getComponent();
          if (savedForeground != null) {
            label.setForeground(savedForeground);
          }
          Point tablePoint = SwingUtilities.convertPoint(
              e.getComponent(), e.getPoint(), table);
          int row = PhotoColumn.this.table.getRow(tablePoint.y);
          addPhoto((JLabel) e.getComponent(), row);
          e.consume();
        }

        @Override public void mouseEntered(MouseEvent e) {
          JLabel label = (JLabel) e.getComponent();
          if (label.getIcon() != null) {
            label.setIcon(cameraIconLight());
          } else {
            savedForeground = getForeground();
            label.setForeground(Color.GRAY);
          }
        }

        @Override public void mouseExited(MouseEvent e) {
          JLabel label = (JLabel) e.getComponent();
          if (label.getIcon() != null) {
            label.setIcon(cameraIcon());
          } else {
            label.setForeground(savedForeground);
          }
        }
      };      
    }

    @Override public JComponent createComponent(final Resolved value, Set<RowState> states) {
      final JLabel label = new JLabel();
      label.setOpaque(false);
      label.setToolTipText(Messages.getMessage(Name.ADD_A_PHOTO));
      label.addMouseListener(listener);
      label.setHorizontalAlignment(SwingConstants.CENTER);
      updateComponentState(label, value, states);
      new PhotoDrops() {
        @Override
        protected void setDragOverBorder() {
          label.setBorder(dragOverBorder);
        }
        
        @Override
        protected void setDefaultBorder() {
          label.setBorder(defaultBorder);
        }
        
        @Override
        protected void addPhotos(List<Photo> photos) {
          PhotoColumn.this.addPhotos(label, value, photos);
        }
      }.activate(label);
      
      return label;
    }

    @Override public ColumnLayout getWidth() {
      return width;
    }

    @Override
    public boolean sizeComponentsToFit() {
      return false;
    }

    @Override public void updateComponentValue(Component component, Resolved value) {
      int photosCount = photosCount(value);
      JLabel label = (JLabel) component;
      updateLabel(photosCount, label);
    }

    private void updateLabel(int photosCount, JLabel label) {
      if (photosCount == 0) {
        label.setText("");
        label.setIcon(cameraIcon());
      } else {
        label.setText("" + photosCount);
        label.setIcon(null);
      }
    }
    
    private int photosCount(Resolved resolved) {
      return getCurrentSightingInfo(resolved).getPhotos().size();
    }
    
    @Override public void updateComponentState(Component component,
        Resolved resolved, Set<RowState> states) {
      if (states.contains(RowState.ENABLED)) {
        updateComponentValue(component, resolved);
      } else {
        JLabel label = (JLabel) component;
        label.setIcon(null);
        label.setText("");        
      }
    }
    
    private void addPhoto(JLabel label, int index) {
      File file = fileDialogs.openFile(UIUtils.findFrame(SingleLocationSpeciesListPanel.this),
          Messages.getMessage(Name.CHOOSE_A_PHOTO),
          new FileFilter() {
            @Override public String getDescription() {
              return Messages.getMessage(Name.IMAGE_FILE);
            }
            
            @Override public boolean accept(File f) {
              return true;
            }
          }, FileType.OTHER);
      
      if (file != null) {
        Resolved resolved = speciesTablePanel.getSpecies(index);
        addPhotos(label, resolved, ImmutableList.of(new Photo(file)));
      }
    }
    
    private void addPhotos(JLabel label, Resolved resolved, List<Photo> newPhotos) {
      SightingInfo sightingInfo = wizardValue().getSightingInfo(resolved);
      List<Photo> combinedPhotos = new ArrayList<Photo>(sightingInfo.getPhotos());
      combinedPhotos.addAll(newPhotos);
      sightingInfo.setPhotos(combinedPhotos);
      sightingInfo.setPhotographed(true);
      updateLabel(combinedPhotos.size(), label);

      speciesTablePanel.resolvedUpdated(resolved);
      wizardValue().edited();
    }

    @Override
    public String getName() {
      return Messages.getMessage(Name.PHOTOS_TEXT);
    }
  }
  
  private final static Joiner COMMA_JOINER = Joiner.on(',');

  /**
   * Column for showing the observers in an {@link ExpandableTable}.
   */
  private class ObserversColumn implements Column<Resolved> {
    private final ColumnLayout width;
    
    public ObserversColumn() {
      this.width = ColumnLayout.weightedWidth(0.7f);
    }

    @Override public JComponent createComponent(final Resolved value, Set<RowState> states) {
      final JLabel label = new JLabel();
      label.setOpaque(false);
      label.setHorizontalAlignment(SwingConstants.CENTER);
      updateComponentState(label, value, states);      
      return label;
    }

    @Override public ColumnLayout getWidth() {
      return width;
    }

    @Override
    public boolean sizeComponentsToFit() {
      return true;
    }

    @Override public void updateComponentValue(Component component, Resolved value) {
      ImmutableSet<User> users = getCurrentSightingInfo(value).getUsers();
      JLabel label = (JLabel) component;
      if (users.isEmpty()) {
        label.setText("");        
      } else {
        List<String> abbreviations = new ArrayList<>(users.size());
        for (User user : users) {
          if (user.abbreviation() != null) {
            abbreviations.add(user.abbreviation());
          }
        }
        
        Collections.sort(abbreviations);
        label.setText(COMMA_JOINER.join(abbreviations));
      }
    }

    @Override public void updateComponentState(Component component,
        Resolved resolved, Set<RowState> states) {
      updateComponentValue(component, resolved);
    }    

    @Override
    public String getName() {
      return Messages.getMessage(Name.OBSERVERS_TEXT);
    }
  }
  
  /**
   * Column for showing the number in an {@link ExpandableTable}.
   */
  private class NumberColumn implements Column<Resolved> {
    private final ColumnLayout width;
    
    public NumberColumn() {
      this.width = ColumnLayout.weightedWidth(0.3f);
    }

    @Override public JComponent createComponent(final Resolved value, Set<RowState> states) {
      final JLabel label = new JLabel();
      label.setOpaque(false);
      label.setHorizontalAlignment(SwingConstants.CENTER);
      label.putClientProperty(FontManager.PLAIN_LABEL, true);
      updateComponentState(label, value, states);      
      return label;
    }

    @Override public ColumnLayout getWidth() {
      return width;
    }

    @Override
    public boolean sizeComponentsToFit() {
      return true;
    }

    @Override public void updateComponentValue(Component component, Resolved value) {
      JLabel label = (JLabel) component;
      ApproximateNumber number = getCurrentSightingInfo(value).getNumber();
      label.setText(number == null ? "" : number.toFormattedString());
    }

    @Override public void updateComponentState(Component component,
        Resolved resolved, Set<RowState> states) {
      updateComponentValue(component, resolved);
    }    

    @Override
    public String getName() {
      return Messages.getMessage(Name.NUMBER_TEXT);
    }
  }
  
  /**
   * Column for showing the sighting status in an {@link ExpandableTable}.
   */
  private class SightingStatusColumn implements Column<Resolved> {
    private final ColumnLayout width;
    
    public SightingStatusColumn() {
      this.width = ColumnLayout.weightedWidth(0.3f);
    }

    @Override public JComponent createComponent(final Resolved value, Set<RowState> states) {
      final JLabel label = new JLabel();
      label.setOpaque(false);
      label.setHorizontalAlignment(SwingConstants.CENTER);
      label.putClientProperty(FontManager.PLAIN_LABEL, true);
      updateComponentState(label, value, states);      
      return label;
    }

    @Override public ColumnLayout getWidth() {
      return width;
    }

    @Override
    public boolean sizeComponentsToFit() {
      return true;
    }

    @Override public void updateComponentValue(Component component, Resolved value) {
      JLabel label = (JLabel) component;
      label.setText(
          Strings.nullToEmpty(
              SightingFlags.getAbbreviatedSightingFlags(getCurrentSightingInfo(value))));
    }

    @Override public void updateComponentState(Component component,
        Resolved resolved, Set<RowState> states) {
      updateComponentValue(component, resolved);
    }

    @Override
    public String getName() {
      return Messages.getMessage(Name.STATUS_COLUMN);
    }
  }
  
  /**
   * Returns the sighting info for the current taxon.
   */
  private SightingInfo getCurrentSightingInfo(Resolved resolved) {
    // This code used to try and do clever things with finding the current
    // expanded row and reading directly from the SightingInfoPanel.
    // This led to issues with ordering problems around collapsing the
    // expanded row vs. the list of storage.
    // Now, this class flushes directly to wizard storage on every infopanel
    // update, which is much much simpler.
    return wizardValue().getSightingInfo(resolved);
  }

  private static ImageIcon cameraIcon() {
    return new ImageIcon(Resources.getResource("com/scythebill/birdlist/ui/icons/camera_18.png"));
  }

  private static ImageIcon cameraIconLight() {
    return new ImageIcon(Resources.getResource("com/scythebill/birdlist/ui/icons/camera_18_light.png"));
  }
}
