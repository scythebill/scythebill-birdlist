/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.awt.Dimension;

import javax.swing.GroupLayout;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.edits.SingleLocationEdit;
import com.scythebill.birdlist.ui.app.Titled;
import com.scythebill.birdlist.ui.components.VisitInfoPanel;
import com.scythebill.birdlist.ui.components.VisitInfoPreferences;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * First page of the single-location wizard:  enter date and location.
 */
public class SingleLocationVisitInfoPanel extends WizardContentPanel<SingleLocationEdit>
    implements Titled, FontsUpdatedListener {

  private final VisitInfoPanel visitInfoPanel;
  private final FontManager fontManager;
  private final DataEntryPreferences dataEntryPreferences;

  @Inject
  public SingleLocationVisitInfoPanel(
      FontManager fontManager,
      DataEntryPreferences dataEntryPreferences,
      VisitInfoPreferences visitInfoPreferences) {
    super("visitInfo", SingleLocationEdit.class);
    this.fontManager = fontManager;
    this.dataEntryPreferences = dataEntryPreferences;

    this.visitInfoPanel = new VisitInfoPanel(null, fontManager, visitInfoPreferences);

    visitInfoPanel.addPropertyChangeListener("validValue", 
        evt -> setComplete((Boolean) evt.getNewValue()));
    setComplete(visitInfoPanel.isValidValue());
  }

  @Override
  protected void shown() {
    super.shown();
    visitInfoPanel.setStartTime(wizardValue().getTime());
    
    VisitInfo visitInfo = wizardValue().getVisitInfo();
    if (visitInfo != null && visitInfo.hasData()) {
      if (!visitInfoPanel.isValidValue()
          || !visitInfoPanel.getValue().equals(visitInfo)) {
        try {
          visitInfoPanel.setValue(visitInfo);
        } catch (IllegalArgumentException e) {
          // The start time may have changed, in which case what was
          // the existing value may no longer be.
          if (visitInfoPanel.isValidValue()) {
            wizardValue().setVisitInfo(visitInfoPanel.getValue());
          }
        }
      }
    } else if (visitInfo == null) {
      visitInfoPanel.tryDefaultObservationType(dataEntryPreferences.observationType);
      if (!wizardValue().getUsers().isEmpty()) {
        visitInfoPanel.setPartySize(wizardValue().getUsers().size());
      }
    }
    
    setComplete(visitInfoPanel.isValidValue());
    
    fontManager.applyTo(this);
  }

  
  @Override
  protected boolean leaving(boolean validateLeaving) {
    if (visitInfoPanel.isValidValue()) {
      wizardValue().setVisitInfo(visitInfoPanel.getValue());
    } else {
      wizardValue().setVisitInfo(null);
    }
    
    return true;
  }

  @Override
  public String getTitle() {
    return Messages.getMessage(Name.VISIT_DATA_TITLE);
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    setPreferredSize(fontManager.scale(new Dimension(400, 300)));
    
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup()
        .addComponent(visitInfoPanel));
    layout.setVerticalGroup(layout.createSequentialGroup()
        .addComponent(visitInfoPanel));
  }
}
