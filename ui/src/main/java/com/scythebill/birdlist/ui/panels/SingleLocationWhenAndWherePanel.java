/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.awt.Dimension;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.joda.time.LocalTime;
import org.joda.time.ReadablePartial;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.io.TimeIO;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.sighting.edits.RecentEdits;
import com.scythebill.birdlist.model.sighting.edits.RecentEdits.RecentEdit;
import com.scythebill.birdlist.model.sighting.edits.SingleLocationEdit;
import com.scythebill.birdlist.model.sighting.edits.SingleLocationEdit.ExistingSightingSnapshotState;
import com.scythebill.birdlist.model.user.User;
import com.scythebill.birdlist.model.user.Users;
import com.scythebill.birdlist.ui.app.Titled;
import com.scythebill.birdlist.ui.components.NewWhenAndWherePanel;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * First page of the single-location wizard:  enter date and location.
 */
public class SingleLocationWhenAndWherePanel extends WizardContentPanel<SingleLocationEdit>
    implements Titled, FontsUpdatedListener {
  private final NewWhenAndWherePanel whenAndWhere;
  private final ReportSet reportSet;
  private final FontManager fontManager;
  private final Alerts alerts;
  private final TaxonomyStore taxonomyStore;
  private final RecentEdits recentEdits;
  private JLabel recentEditsLabel;
  private JComboBox<UIRecentEdit> recentEditsCombo;

  @Inject
  public SingleLocationWhenAndWherePanel(
      NewWhenAndWherePanel whenAndWhere,
      ReportSet reportSet,
      FontManager fontManager,
      TaxonomyStore taxonomyStore,
      Alerts alerts,
      RecentEdits recentEdits) {
    super("whenAndWhere", SingleLocationEdit.class);

    this.whenAndWhere = whenAndWhere;
    this.reportSet = reportSet;
    this.fontManager = fontManager;
    this.taxonomyStore = taxonomyStore;
    this.alerts = alerts;
    this.recentEdits = recentEdits;

    initGUI();
    hookUpContents();
  }

  private void hookUpContents() {
    
    PropertyChangeListener whereListener = e -> {
      wizardValue().setLocation(whenAndWhere.getWhere());
      wizardValue().setLocationName(whenAndWhere.getWhereText());
      updateComplete();
    };
    
    whenAndWhere.addPropertyChangeListener("where", whereListener);
    whenAndWhere.addPropertyChangeListener("whereText", whereListener);
    whenAndWhere.addPropertyChangeListener("wherePopupVisible", whereListener);
    
    whenAndWhere.addPropertyChangeListener("when", 
        e -> {
          wizardValue().setDate(whenAndWhere.getWhen());
          updateComplete();
        });
    
    whenAndWhere.addPropertyChangeListener("time", 
        e -> {
          wizardValue().setTime(whenAndWhere.getTime());
        });
    
    whenAndWhere.addPropertyChangeListener("who",
        e -> {
          wizardValue().setUsers(whenAndWhere.getWho());
        });
    recentEditsCombo.addActionListener(e -> changeRecentEdit());
  }

  private void changeRecentEdit() {
    UIRecentEdit recentEdit = (UIRecentEdit) recentEditsCombo.getSelectedItem();
    if (!recentEdit.isNull()) {
      VisitInfoKey visitInfoKey = recentEdit.asVisitInfoKey();
      setVisitInfoKeyAndUsers(visitInfoKey, Users.usersForVisitInfoKey(reportSet, visitInfoKey));
      // Erase everything about the snapshot.
      // TODO: What if the edit is already dirty?  That could lose user edits.  Likely
      // rare, but still...
      wizardValue().clearSnapshot();
    }    
  }
  
  @Override
  protected void shown() {
    super.shown();
    whenAndWhere.setWhen(wizardValue().getDate());
    if (wizardValue().getLocation() != null) {
      whenAndWhere.setWhere(wizardValue().getLocation());
    } else if (!whenAndWhere.getWhereText().equals(wizardValue().getLocationName())) {
      whenAndWhere.setWhereText(wizardValue().getLocationName());
    }
    whenAndWhere.setWho(wizardValue().getUsers());
    whenAndWhere.shown();
  }
  
  public boolean hasWhen() {
    return whenAndWhere.hasWhen();
  }

  public boolean hasWhere() {
    return whenAndWhere.hasWhere();
  }
  
  public void setVisitInfoKeyAndUsers(VisitInfoKey visitInfoKey, Collection<User> users) {
    Location location = reportSet.getLocations().getLocation(visitInfoKey.locationId());
    whenAndWhere.setWhere(location);
    whenAndWhere.setWhen(visitInfoKey.date());
    whenAndWhere.setTime(visitInfoKey.startTime().orNull());
    whenAndWhere.setWho(users);
  }

  private void updateComplete() {
    setComplete(
        !whenAndWhere.isWherePopupVisible()
        && (wizardValue().getLocation() != null ||
            !Strings.isNullOrEmpty(wizardValue().getLocationName())));
  }
  
  private void initGUI() {
    recentEditsLabel = new JLabel(Messages.getMessage(Name.OR_CHOOSE_A_RECENT_VISIT));
    
    // Gather the most recent edits;  and append from the report set to get ten
    List<RecentEdit> recentEditsList = recentEdits.getRecentEditsList();
    if (recentEditsList.size() < 10) {
      for (RecentEdit recentEdit : RecentEdits.getLastEdits(reportSet, 10)) {
        if (!recentEditsList.contains(recentEdit)) {
          recentEditsList.add(recentEdit);
          if (recentEditsList.size() == 10) {
            break;
          }
        }
      }
    }
    
    List<UIRecentEdit> uiRecentEdits = toUIRecentEdits(recentEditsList);
    recentEditsCombo = new JComboBox<UIRecentEdit>(uiRecentEdits.toArray(new UIRecentEdit[0]));
    recentEditsCombo.setMaximumRowCount(recentEditsCombo.getItemCount());
    
    if (recentEditsCombo.getItemCount() == 1) {
      recentEditsLabel.setVisible(false);
      recentEditsCombo.setVisible(false);
    }
    
    fontManager.applyTo(this);    
  }

  @Override
  public String getTitle() {
    if (reportSet.getUserSet() != null) {
      return Messages.getMessage(Name.WHO_WHEN_WHERE);
    } else {
      return Messages.getMessage(Name.WHEN_AND_WHERE);
    }
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    setPreferredSize(fontManager.scale(new Dimension(400, 300)));
    
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup()
        .addComponent(whenAndWhere)
        .addGroup(layout.createSequentialGroup()
            .addGap(10)
            .addGroup(layout.createParallelGroup()
                .addComponent(recentEditsLabel)
                .addComponent(recentEditsCombo, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))));
    layout.setVerticalGroup(layout.createSequentialGroup()
        .addComponent(whenAndWhere)
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(recentEditsLabel)
        .addComponent(recentEditsCombo, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE));
  }

  @Override
  protected boolean leaving(boolean validateLeaving) {
    boolean allowLeaving;
    if (validateLeaving) {
      if (wizardValue().getDate() == null) {
        allowLeaving = (alerts.showYesNo(this,
            Name.YOU_DIDNT_ENTER_A_DATE_TITLE,
            Name.YOU_DIDNT_ENTER_A_DATE_MESSAGE) == JOptionPane.YES_OPTION);
      } else {
        if (wizardValue().getDate().isSupported(DateTimeFieldType.year())) {
          int year = wizardValue().getDate().get(DateTimeFieldType.year());
          if (year < (DateTime.now().getYear() - 100)) {
            allowLeaving = (alerts.showYesNo(this,
                Name.YEAR_MAY_BE_WRONG,
                Name.YEAR_IS_FAR_IN_THE_PAST_MESSAGE,
                year) == JOptionPane.YES_OPTION);
          } else if (year > DateTime.now().getYear()) {
            allowLeaving = (alerts.showYesNo(this,
                Name.YEAR_MAY_BE_WRONG,
                Name.YEAR_IS_IN_THE_FUTURE_MESSAGE,
                year) == JOptionPane.YES_OPTION);
          } else {
            allowLeaving = true;
          }
        } else {
          allowLeaving = true;
        }
      }
    } else {
      allowLeaving = true;
    }

    // Snapshot any existing sightings for this setup
    if (allowLeaving) {
      SingleLocationEdit edit = wizardValue();
      boolean snapshot = true;
      if (edit.getDirty().isDirty()) {
        // Figure out what's changed between when the snapshot was taken and now.
        // If, for example, the date has changed, then either the user:
        //  - Wants to change the date of all existing sightings
        //  - Or wants to start anew with sightings at that date
        // TODO: what if there's already sightings on that date?  Yee-ouch.
        // And even worse, what if there's VisitInfo?  Merging seems plausible, but
        // is likely going to be complicated to explain to a user.  So likely the
        // right thing to do is just warn the user that this change will drop all
        // edits and start fresh.  Consider doing this across the board.
        // TODO: OK, it's even worse.  Because there might be *no* snapshot (it's 
        // a brand new date), then the user realizes they meant to edit an existing
        // date, and they go back and change.   In that case, they almost certainly
        // *do* want to merge into the previous one.  So maybe this whole thing needs
        // to:
        //  - Support "drop or edit" if there's no sightings in the new location/date/time
        //  - Support "drop or merge" if there *are* sightings in the new location/date/time
        ExistingSightingSnapshotState snapshotState = edit.getSnapshotState();
        Name title = null;
        Name text = null;
        switch (snapshotState) {
          case NONE:
          case NO_CHANGE:
            snapshot = wizardValue().isEmpty();
            break;
          case DATE_CHANGED:
            title = Name.DATE_HAS_CHANGED_TITLE;
            text = Name.DATE_HAS_CHANGED_MESSAGE;
            break;
          case LOCATION_CHANGED:
            title = Name.LOCATION_HAS_CHANGED_TITLE;
            text = Name.LOCATION_HAS_CHANGED_MESSAGE;
            break;
          case DATE_AND_TIME_CHANGED:
            title = Name.DATE_AND_TIME_HAS_CHANGED_TITLE;
            text = Name.DATE_AND_TIME_HAS_CHANGED_MESSAGE;
            break;
          case TIME_CHANGED:
            title = Name.TIME_HAS_CHANGED_TITLE;
            text = Name.TIME_HAS_CHANGED_MESSAGE;
            break;
          case TIME_AND_LOCATION_CHANGED:
            title = Name.TIME_AND_LOCATION_HAS_CHANGED_TITLE;
            text = Name.TIME_AND_LOCATION_HAS_CHANGED_MESSAGE;
            break;
          case DATE_AND_LOCATION_CHANGED:
            title = Name.DATE_AND_LOCATION_HAS_CHANGED_TITLE;
            text = Name.DATE_AND_LOCATION_HAS_CHANGED_MESSAGE;
            break;
          case DATE_AND_AND_TIME_AND_LOCATION_CHANGED:
            title = Name.DATE_AND_TIME_AND_LOCATION_HAS_CHANGED_TITLE;
            text = Name.DATE_AND_TIME_AND_LOCATION_HAS_CHANGED_MESSAGE;
            break;
        }
        
        if (title != null) {
          int showConfirm = alerts.showConfirm(this, title, text);
          if (showConfirm == JOptionPane.YES_OPTION) {
            snapshot = false;
          } else if (showConfirm == JOptionPane.NO_OPTION) {
            snapshot = true;
          } else {
            return false;
          }
        }
      }
      
      // If the user confirmed, then take a new snapshot of the data
      if (snapshot) {
        edit.snapshotExistingSightings(reportSet, taxonomyStore.getTaxonomy());
      }
    }
    
    return allowLeaving;
  }

  private List<UIRecentEdit> toUIRecentEdits(List<RecentEdit> recentEditsList) {
    List<UIRecentEdit> uiRecentEdits = Lists.newArrayList(new UIRecentEdit());
    for (RecentEdit recentEdit : recentEditsList) {
      Location location = reportSet.getLocations().getLocation(recentEdit.getLocationId());
      if (location != null && recentEdit.getDate() != null) {
        uiRecentEdits.add(new UIRecentEdit(location, recentEdit.getDate(), recentEdit.getStartTime()));
      }
    }
    
    return uiRecentEdits;
  }

  static class UIRecentEdit {
    private final ReadablePartial date;
    private final LocalTime startTime;
    private final Location location;
    private final String dateString;
    private final String startTimeString;
    
    UIRecentEdit() {
      this.location = null;
      this.date = null;
      this.dateString = null;
      this.startTime = null;
      this.startTimeString = null;
    }
    
    public boolean isNull() {
      return dateString == null;
    }

    UIRecentEdit(Location location, ReadablePartial date, LocalTime time) {
      this.location = Preconditions.checkNotNull(location);
      this.date = Preconditions.checkNotNull(date);
      // Cache the stringified date
      this.dateString = PartialIO.toShortUserString(date, Locale.getDefault());
      this.startTime = time;
      this.startTimeString = time == null ? null : TimeIO.toShortUserString(startTime, Locale.getDefault());
    }
    
    public VisitInfoKey asVisitInfoKey() {
      return new VisitInfoKey(location.getId(), date, startTime);
    }
    
    public Location getLocation() {
      return location;
    }
    
    public ReadablePartial getDate() {
      return date;
    }

    public LocalTime getStartTime() {
      return startTime;
    }
    
    @Override
    public int hashCode() {
      return Objects.hashCode(location, date, startTime);
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (!(o instanceof UIRecentEdit)) {
        return false;
      }
      
      UIRecentEdit that = (UIRecentEdit) o;
      return Objects.equal(location, that.location)
          && Objects.equal(date, that.date)
          && Objects.equal(startTime, that.startTime);
    }
    
    @Override public String toString() {
      if (dateString == null) {
        return "";
      }

      if (startTime == null) {
        return String.format("%s - %s", location.getDisplayName(), dateString);
      } else {
        return String.format("%s - %s %s", location.getDisplayName(), dateString, startTimeString);
      }
    }
  }

}
