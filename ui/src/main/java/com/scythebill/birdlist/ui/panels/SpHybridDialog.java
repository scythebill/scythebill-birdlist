/*   
 * Copyright 2020 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.awt.Component;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Type;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.components.IndexerPanel.LayoutStrategy;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Dialog for creating a "spuh" or hybrid off a base taxon.
 */
public class SpHybridDialog {

  private final FontManager fontManager;
  private final NamesPreferences namesPreferences;

  @Inject
  public SpHybridDialog(
      FontManager fontManager,
      NamesPreferences namesPreferences) {
    this.fontManager = fontManager;
    this.namesPreferences = namesPreferences;
  }
  
  public interface Listener {
    void newTaxon(SightingTaxon baseTaxonomySightingTaxon);
  }
  
  public void showDialog(
      Component parent,
      Taxon baseTaxon,
      Listener listener) {
    ModalityType modality =
        Toolkit.getDefaultToolkit().isModalityTypeSupported(ModalityType.DOCUMENT_MODAL)
            ? ModalityType.DOCUMENT_MODAL
            : ModalityType.APPLICATION_MODAL;
    JDialog dialog = new JDialog(
        SwingUtilities.getWindowAncestor(parent),
        Messages.getMessage(Name.CHANGE_TO_SP_OR_HYBRID),
        modality);
    SpHybridPanel panel = new SpHybridPanel(baseTaxon, dialog, listener);
    dialog.setContentPane(panel);
    fontManager.applyTo(dialog);
    dialog.pack();
    dialog.setLocationRelativeTo(parent);
    dialog.setVisible(true);
  }
  
  class SpHybridPanel extends JPanel {
    private JButton spButton;
    private JButton hybridButton;
    private JButton cancelButton;
    private JLabel baseSpeciesText;
    private IndexerPanel<String> indexerPanel;
    private JLabel andText;

    public SpHybridPanel(Taxon baseTaxon, JDialog dialog, Listener listener) {
      setBorder(new EmptyBorder(20, 20, 15, 20));
      setPreferredSize(fontManager.scale(new Dimension(350, 200)));

      GroupLayout layout = new GroupLayout(this);
      setLayout(layout);
      JSeparator separator = new JSeparator();
      
      String name;
      switch (namesPreferences.scientificOrCommon) {
        case COMMON_FIRST:
        case COMMON_ONLY:
          name = TaxonUtils.getCommonName(baseTaxon);
          break;
        case SCIENTIFIC_FIRST:
        case SCIENTIFIC_ONLY:
          name = TaxonUtils.getFullName(baseTaxon);
          break;
        default:
          throw new AssertionError(
              "Unexpected name preference: " + namesPreferences.scientificOrCommon);            
      }

      baseSpeciesText = new JLabel();
      baseSpeciesText.setText(Messages.getFormattedMessage(Name.MAKE_SP_OR_HYBRID_FORMAT, name));
      
      andText = new JLabel(Messages.getMessage(Name.AND_LABEL));
      
      cancelButton = new JButton();
      cancelButton.setText(Messages.getMessage(Name.CANCEL_BUTTON));
      cancelButton.addActionListener(e -> dialog.dispose());
      
      spButton = new JButton();
      spButton.setText(Messages.getMessage(Name.SPUH_LABEL));
      spButton.addActionListener(
          e -> returnSpOrHybrid(listener, baseTaxon, Type.SP, dialog));

      hybridButton = new JButton();
      hybridButton.setText(Messages.getMessage(Name.HYBRID_LABEL));
      hybridButton.addActionListener(
          e -> returnSpOrHybrid(listener, baseTaxon, Type.HYBRID, dialog));

      indexerPanel = new IndexerPanel<>();
      indexerPanel.setLayoutStrategy(LayoutStrategy.BELOW);
      SpHybridEntry.configureIndexer(indexerPanel, baseTaxon, namesPreferences);
      indexerPanel.setPreviewText(Name.ENTER_SPECIES);
      
      indexerPanel.addPropertyChangeListener("value", new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
          hybridButton.setEnabled(evt.getNewValue() != null);
          spButton.setEnabled(evt.getNewValue() != null);
        }
      });
      hybridButton.setEnabled(false);
      spButton.setEnabled(false);
      
      JPanel spacer = new JPanel();
      
      layout.setAutoCreateContainerGaps(true);

      layout.setHorizontalGroup(layout.createParallelGroup()
          .addComponent(baseSpeciesText)
          .addGroup(layout.createSequentialGroup()
              .addComponent(andText)
              .addPreferredGap(ComponentPlacement.RELATED)
              .addComponent(indexerPanel))
          .addComponent(spacer)
          .addComponent(separator)
          .addComponent(cancelButton, Alignment.LEADING)
          .addGroup(Alignment.TRAILING, layout.createSequentialGroup()
              .addComponent(hybridButton)
              .addPreferredGap(ComponentPlacement.RELATED)
              .addComponent(spButton)));
      layout.setVerticalGroup(layout.createSequentialGroup()
          .addComponent(baseSpeciesText)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addGroup(layout.createBaselineGroup(false, false)
              .addComponent(andText)
              .addComponent(indexerPanel))
          .addGap(18)
          .addComponent(spacer, 0, 0, Short.MAX_VALUE)
          .addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGroup(layout.createParallelGroup()
              .addComponent(cancelButton)
              .addComponent(hybridButton)
              .addComponent(spButton)));
      layout.linkSize(cancelButton, spButton, hybridButton);
      
      UIUtils.focusOnEntry(this, indexerPanel);
      
      KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
      Action cancelAction = new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
          cancelButton.doClick(100);
        }
      };

      getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
          .put(escape, "cancelPanel");
      getActionMap().put("cancelPanel", cancelAction);
      
      indexerPanel.getTextComponent().getInputMap(JComponent.WHEN_FOCUSED)
          .put(escape, "cancelPanel");
      indexerPanel.getTextComponent().getActionMap()
          .put("cancelPanel", cancelAction);
    }
    
    private void returnSpOrHybrid(Listener listener, Taxon baseTaxon,
        SightingTaxon.Type type, JDialog dialog) {
      String taxonId = indexerPanel.getValue();
      if (taxonId != null) {
        dialog.dispose();

        Taxonomy taxonomy = baseTaxon.getTaxonomy();
        ImmutableSet<String> ids = ImmutableSet.of(baseTaxon.getId(), taxonId);
        SightingTaxon newTaxon = (type == SightingTaxon.Type.SP)
            ? SightingTaxons.newSpTaxon(ids)
            : SightingTaxons.newHybridTaxon(ids);
        if (taxonomy instanceof MappedTaxonomy) {
          newTaxon = ((MappedTaxonomy) taxonomy).getMapping(newTaxon);
        }
         
        listener.newTaxon(newTaxon);
      }
    }
  }
}
