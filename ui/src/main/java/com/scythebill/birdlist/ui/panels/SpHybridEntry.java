/*   
 * Copyright 2020 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.util.Set;

import javax.annotation.Nullable;

import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Ordering;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences;
import com.scythebill.birdlist.model.util.AlternateName;
import com.scythebill.birdlist.model.util.Indexer;
import com.scythebill.birdlist.model.util.ToString;
import com.scythebill.birdlist.ui.components.IndexerPanel;

/**
 * Utilities for supporting sp/hybrid entry.
 */
class SpHybridEntry {

  public static void configureIndexer(IndexerPanel<String> indexerPanel, Taxon taxon,
      NamesPreferences namePreferences) {
    Taxonomy taxonomy = taxon.getTaxonomy();
    Taxon root;
    if (taxon.getType() == Taxon.Type.species) {
      // Build indices restricted to this family
      root = TaxonUtils.getParentOfType(taxon, Taxon.Type.family);
    } else {
      // or just the species
      root = TaxonUtils.getParentOfType(taxon, Taxon.Type.species);
    }
    Indexer<String> local = new Indexer<>();
    Indexer<String> english = new Indexer<>();
    Indexer<String> sci = new Indexer<>();
    Indexer<AlternateName<String>> alternates = new Indexer<>();
    buildIndices(local, english, sci, alternates, root, ImmutableSet.of(taxon, root), taxon.getType());
    ToString<String> localToString = new ToString<String>() {
      @Override public String getString(String taxonId) {
        Taxon taxon = taxonomy.getTaxon(taxonId);

        return taxon == null ? null : TaxonUtils.getCommonName(taxon);
      }

      @Override public String getPreviewString(String taxonId) {
        return getString(taxonId);
      }
    };
    ToString<String> englishToString = new ToString<String>() {
      @Override public String getString(String taxonId) {
        Taxon taxon = taxonomy.getTaxon(taxonId);

        return taxon == null ? null : TaxonUtils.getEnglishCommonName(taxon);
      }

      @Override public String getPreviewString(String taxonId) {
        return getString(taxonId);
      }
    };
    ToString<String> sciToString = new ToString<String>() {
      @Override public String getString(String taxonId) {
        Taxon taxon = taxonomy.getTaxon(taxonId);
        return taxon == null ? null : TaxonUtils.getFullName(taxon);
      }

      @Override public String getPreviewString(String taxonId) {
        return getString(taxonId);
      }
    };
    indexerPanel.removeAllIndexerGroups();
    switch (namePreferences.scientificOrCommon) {
      case COMMON_FIRST:
        indexerPanel.addIndexerGroup(localToString, local);
        indexerPanel.addIndexerGroup(englishToString, english);
        indexerPanel.addAlternateIndexerGroup(localToString, alternates);
        // Fall-through
      case SCIENTIFIC_ONLY:
        indexerPanel.addIndexerGroup(sciToString, sci);
        break;
      case SCIENTIFIC_FIRST:
        indexerPanel.addIndexerGroup(sciToString, sci);
        // Fall-through
      case COMMON_ONLY:
        indexerPanel.addIndexerGroup(localToString, local);
        indexerPanel.addIndexerGroup(englishToString, english);
        indexerPanel.addAlternateIndexerGroup(localToString, alternates);
        break;
      default:
        throw new AssertionError("Unexpected name order"); 
    }

    // And sort based on taxonomy proximity to the base taxon
    indexerPanel.setOrdering(Suppliers.ofInstance(new ProximityOrdering(taxon)));
    indexerPanel.revalidate();
  }

  private static void buildIndices(
      Indexer<String> local, Indexer<String> english, Indexer<String> sci,
      Indexer<AlternateName<String>> alternates, Taxon taxon, Set<Taxon> allExcept, Taxon.Type downToLevel) {
     if (taxon instanceof Species) {
       if (!allExcept.contains(taxon)) {
         if (taxon.getCommonName() != null || downToLevel == Taxon.Type.subspecies) {
           String commonName = TaxonUtils.getCommonName(taxon);
           String englishName = TaxonUtils.getEnglishCommonName(taxon);
           if (!commonName.equals(englishName)) {
             local.add(commonName, taxon.getId());
           }
           english.add(englishName, taxon.getId());
         }
         sci.add(TaxonUtils.getFullName(taxon), taxon.getId());
         
         for (String alternateCommonName : ((Species) taxon).getAlternateCommonNames()) {
           alternates.add(trimSuffixes(alternateCommonName),
               AlternateName.forNameAndId(alternateCommonName, taxon.getId()));
         }
       }
     }
     if (taxon.getType() == downToLevel) {
       return;
     }
     for (Taxon child : taxon.getContents()) {
       buildIndices(local, english, sci, alternates, child, allExcept, downToLevel);
     }
   }

  private static class ProximityOrdering extends Ordering<String> {
    private final int fromIndex;
    private final Taxonomy taxonomy;

    ProximityOrdering(Taxon from) {
      this.fromIndex = from.getTaxonomyIndex();
      this.taxonomy = from.getTaxonomy();
    }

    @Override
    public int compare(@Nullable String left, @Nullable String right) {
      Taxon leftTaxon = taxonomy.getTaxon(left);
      Taxon rightTaxon = taxonomy.getTaxon(right);
      
      int leftDiff = Math.abs(leftTaxon.getTaxonomyIndex() - fromIndex);
      int rightDiff = Math.abs(rightTaxon.getTaxonomyIndex() - fromIndex);
      return leftDiff - rightDiff;
    }
  }


  private static String trimSuffixes(String alternate) {
    int indexOf = alternate.indexOf(" -");
    if (indexOf > 0) {
      return alternate.substring(0, indexOf);
    }
    return alternate;
  }
}
