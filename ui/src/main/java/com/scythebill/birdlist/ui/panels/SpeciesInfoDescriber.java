/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.net.URL;
import java.util.List;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.TransposedChecklists;
import com.scythebill.birdlist.model.io.HtmlResponseWriter;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.names.LocalNames;
import com.scythebill.birdlist.model.util.TaxonNames;
import com.scythebill.birdlist.ui.guice.Clements;
import com.scythebill.birdlist.ui.guice.IOC;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Utility for producing species descriptions.
 */
public class SpeciesInfoDescriber {
  private static final String SHOW_MAP_URL_HOST = "scythebill.map";
  
  private final Taxonomy clements;
  private final MappedTaxonomy ioc;
  private final static Joiner NAME_JOINER = Joiner.on('/');

  @Inject
  public SpeciesInfoDescriber(
      @Clements Taxonomy clements,
      @IOC MappedTaxonomy ioc) {
    this.clements = clements;
    this.ioc = ioc;
  }
  
  public void toHtmlText(
      Resolved taxon,
      ReportSet reportSet,
      Checklist checklist,
      Location checklistLocation,
      StringBuilder builder) {
    toHtmlText(taxon, reportSet, builder, true);
    if (checklist != null) {
      SightingTaxon species = taxon.getParentOfAtLeastType(Taxon.Type.species);
      Checklist.Status status = checklist.getStatus(taxon.getTaxonomy(), species);
      if (status == null) {
        // Don't bother showing "not on X checklist" for domestic forms.
        if (taxon.getTaxonStatus() != Status.DO
            // Or for "spuh" and "hybrid", which aren't on checklists
            && species.getType() != SightingTaxon.Type.SP
            && species.getType() != SightingTaxon.Type.HYBRID) {
          builder.append("<p><b>");
          builder.append(Messages.getFormattedMessage(Name.NOT_ON_THE_CHECKLIST_FORMAT,
              HtmlResponseWriter.htmlEscape(checklistLocation.getDisplayName())));
          builder.append("</b>");
        }
      } else if (status != Checklist.Status.NATIVE) {
        Name statusFormat;
        switch (status) {
          case ENDEMIC:
            statusFormat = Name.ENDEMIC_TO_FORMAT;
            break;
          case ESCAPED:
            statusFormat = Name.ESCAPEE_IN_FORMAT;
            break;
          case INTRODUCED:
            statusFormat = Name.INTRODUCED_IN_FORMAT;
            break;
          case EXTINCT:
            statusFormat = Name.EXTINCT_IN_FORMAT;
            break;
          case RARITY:
            statusFormat = Name.RARITY_IN_FORMAT;
            break;
          case RARITY_FROM_INTRODUCED:
            statusFormat = Name.RARITY_FROM_INTRODUCED_IN_FORMAT;
            break;
          default:
            throw new AssertionError("Unexpected status");
        }
        
        builder.append("<p>");
        builder.append(Messages.getFormattedMessage(
            statusFormat, HtmlResponseWriter.htmlEscape(checklistLocation.getDisplayName())));
      }
    }
  }
  
  public void toHtmlText(
      Resolved taxon,
      ReportSet reportSet,
      StringBuilder builder,
      boolean hyperlinkRange) {
    String commonName = taxon.getCommonName();
    builder.append(HtmlResponseWriter.htmlEscape(commonName)).append("<br>");
    
    String sciName = taxon.getFullName();
    builder.append("<i>").append(HtmlResponseWriter.htmlEscape(sciName)).append("</i>");
    Status taxonStatus = taxon.getTaxonStatus();
    if (taxonStatus != Status.LC) {
      builder.append("<br><b>");
      builder.append(HtmlResponseWriter.htmlEscape(Messages.getText(taxonStatus)));
      builder.append("</b>");
    }
    
    if (taxon.getTaxonomy().isBuiltIn()) {
      String otherTaxonomyName;
      String otherCommonName = null;
      String otherSciName = null;
      if (taxon.getTaxonomy() == clements) {
        otherTaxonomyName = "IOC";  
        Resolved iocResolved = taxon.getSightingTaxon().resolve(ioc);
        if (iocResolved != null) {
          // For common name, use the IOC species, to avoid redundant subspecies text
          Resolved iocResolvedSpecies =
              iocResolved.getParentOfAtLeastType(Taxon.Type.species).resolveInternal(ioc);
          if (iocResolved != null) {
            otherCommonName = iocResolvedSpecies.getCommonName();
            otherSciName = iocResolved.getFullName();
          }
        }
      } else {
        otherTaxonomyName = "Clements/eBird";
        SightingTaxon clementsTaxon = ioc.getMapping(taxon.getSightingTaxon());
        if (clementsTaxon != null) {
          Resolved clementsResolved = clementsTaxon.resolve(clements);
          // For common name, use the Clements group, to avoid redundant subspecies text
          Resolved clementsResolvedGroup =
              clementsResolved.getParentOfAtLeastType(Taxon.Type.group).resolveInternal(clements);
          otherCommonName = clementsResolvedGroup.getCommonName();
          otherSciName = clementsResolved.getFullName();
        }
      }
      
      if (otherCommonName != null && otherSciName != null) {
        if (!otherCommonName.equals(commonName)
            || !otherSciName.equals(sciName)) {
          builder.append("<br><b>");
          builder.append(HtmlResponseWriter.htmlEscape(otherTaxonomyName));
          builder.append(":</b> ");
          builder.append(HtmlResponseWriter.htmlEscape(otherCommonName));
          builder.append(" <i>");
          builder.append(HtmlResponseWriter.htmlEscape(otherSciName));
          builder.append("</i>");
        }
      }
    } 
    
    if (taxon.getType() == SightingTaxon.Type.SINGLE
        && taxon.getTaxon() instanceof Species) {
      Species species = (Species) taxon.getTaxon();
      List<String> englishNames = Lists.newArrayList();
      LocalNames localNames = species.getTaxonomy().getLocalNames();
      if (species.getCommonName() != null
          && !species.getCommonName().equals(localNames.getCommonName(species))) {
        englishNames.add(species.getCommonName());
      }
      if (!englishNames.isEmpty()) {
        builder.append("<br><b>");
        builder.append(Messages.getMessage(Name.ALTERNATE_NAMES_LABEL));
        builder.append("</b> ");
        builder.append(HtmlResponseWriter.htmlEscape(
            TaxonNames.joinFromEnd(englishNames, NAME_JOINER)));
      }

      String range = TaxonUtils.getRange(species);
      if (range != null) {
        if (hyperlinkRange
            && TransposedChecklists.instance().hasTransposedChecklistEntry(reportSet, species)) {
          builder.append("<br><b><a href=\"http://")
              .append(SHOW_MAP_URL_HOST)
              .append("/")
              .append(species.getId())
              .append("\">")
              .append(Messages.getMessage(Name.RANGE_LABEL))
              .append("</a> </b>");
        } else {
          builder.append("<br><b>")
              .append(Messages.getMessage(Name.RANGE_LABEL))
              .append("</b> ");
        }
        builder.append(HtmlResponseWriter.htmlEscape(range));
      }
      
      if (species.getAccountId() != null) {
        String accountUrl = species.getTaxonomy().getTaxonAccountUrl(species.getAccountId());
        if (accountUrl != null) {
          if (builder.length() > 0) {
            builder.append("<br>");
          }
          builder.append("<b><a href=\"").append(accountUrl).append("\">");
          String linkTitle = species.getTaxonomy().getAccountLinkTitle();
          if (linkTitle == null) {
            linkTitle = Messages.getMessage(Messages.Name.TAXON_PAGE);
          } else {
            linkTitle = Messages.getFormattedMessage(Messages.Name.PAGE_FORMAT, linkTitle);
          }
          builder.append(linkTitle);
          builder.append("</a></b>");
        }
      }

      if (species.getTaxonomicInfo() != null) {
        builder.append("<br><b>")
            .append(Messages.getMessage(Name.TAXONOMY_LABEL))
            .append("</b> ");
        builder.append(HtmlResponseWriter.htmlEscape(species.getTaxonomicInfo()));
      }
    }
  }
  
  /**
   * Convert to a plain, newline-delimited block of info text.
   */
  public String toPlainText(Species species) {
    String range = TaxonUtils.getRange(species);
    StringBuilder info = new StringBuilder(range == null ? "" : range);
    
    if (species.getStatus() != Status.LC) {
      if (info.length() > 0) {
        info.append('\n');
      }
      info.append(Messages.getText(species.getStatus()));
    }
    
    List<String> englishNames = Lists.newArrayList();
    LocalNames localNames = species.getTaxonomy().getLocalNames();
    if (species.getCommonName() != null
        && !species.getCommonName().equals(localNames.getCommonName(species))) {
      englishNames.add(species.getCommonName());
    }
    englishNames.addAll(species.getAlternateCommonNames());
    if (!englishNames.isEmpty()) {
      if (info.length() > 0) {
        info.append('\n');
      }
      info.append(TaxonNames.joinFromEnd(englishNames, NAME_JOINER));
    }
    
    if (species.getTaxonomicInfo() != null) {
      if (info.length() > 0) {
        info.append('\n');
      }
      info.append(species.getTaxonomicInfo());
    }
    
    return info.toString();
    
  }

  /**
   * Convert to a plain, newline-delimited block of info text.
   */
  public String toSimpleLinkedHtmlText(ReportSet reportSet, Species species) {
    StringBuilder info = new StringBuilder();
    
    String range = TaxonUtils.getRange(species);
    if (range != null) {
      if (TransposedChecklists.instance().hasTransposedChecklistEntry(reportSet, species)) {
        info = new StringBuilder("<a href=\"http://")
            .append(SHOW_MAP_URL_HOST)
            .append("/")
            .append(species.getId())
            .append("\">")
            .append(Messages.getMessage(Name.RANGE_LABEL))
            .append("</a> ");
      }
      info.append(range);
    }
    
    if (species.getAccountId() != null) {
      String accountUrl = species.getTaxonomy().getTaxonAccountUrl(species.getAccountId());
      if (accountUrl != null) {
        if (info.length() > 0) {
          info.append("<br>");
        }
        info.append("<a href=\"").append(accountUrl).append("\">");
        String linkTitle = species.getTaxonomy().getAccountLinkTitle();
        if (linkTitle == null) {
          linkTitle = Messages.getMessage(Messages.Name.TAXON_PAGE);
        } else {
          linkTitle = Messages.getFormattedMessage(Messages.Name.PAGE_FORMAT, linkTitle);
        }
        info.append(linkTitle);
        info.append("</a>");
      }
    }
    
    if (species.getStatus() != Status.LC) {
      if (info.length() > 0) {
        info.append("<br>");
      }
      info.append(Messages.getText(species.getStatus()));
    }
    
    List<String> englishNames = Lists.newArrayList();
    LocalNames localNames = species.getTaxonomy().getLocalNames();
    if (species.getCommonName() != null
        && !species.getCommonName().equals(localNames.getCommonName(species))) {
      englishNames.add(species.getCommonName());
    }
    englishNames.addAll(species.getAlternateCommonNames());
    if (!englishNames.isEmpty()) {
      if (info.length() > 0) {
        info.append("<br>");
      }
      info.append(TaxonNames.joinFromEnd(englishNames, NAME_JOINER));
    }
    
    if (species.getTaxonomicInfo() != null) {
      if (info.length() > 0) {
        info.append("<br>");
      }
      info.append(species.getTaxonomicInfo());
    }
    
    return info.toString();
  }

  public static boolean isShowMapUrl(URL url) {
    return SHOW_MAP_URL_HOST.equals(url.getHost());
  }  
  
  public static String getShowMapSpeciesId(URL url) {
    return url.getPath().replace("/", "");
  }
}
