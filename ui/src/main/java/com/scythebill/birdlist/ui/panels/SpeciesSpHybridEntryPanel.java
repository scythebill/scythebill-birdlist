/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.EventListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkEvent.EventType;
import javax.swing.event.HyperlinkListener;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences;
import com.scythebill.birdlist.ui.actions.DelegatedAction;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.components.SpeciesIndexerPanelConfigurer;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * Panel for entering a single species (or hybrid or sp.), with an informational
 * text area.
 */
public class SpeciesSpHybridEntryPanel extends JPanel implements FontsUpdatedListener {
  private Taxonomy taxonomy;
  private FontManager fontManager;
  private JButton addButton;
  private IndexerPanel<String> speciesIndexer;
  private JEditorPane speciesInfoText;
  private JLabel indexerLabel;
  private final SpeciesIndexerPanelConfigurer speciesIndexerPanelConfigurer;
  private FocusLocation focusLocation = null;
  private DelegatedAction addAction = new DelegatedAction();
  protected Taxon subspeciesPopupTaxon;
  
  private JButton spButton;
  private SpEntryPanel spEntryPanel;
  private JButton hybridButton;
  private final SpeciesInfoDescriber speciesInfoDescriber;
  private Supplier<Resolved> resolvedSource;
  private final ShowSpeciesMap showSpeciesMap;
  private final Alerts alerts;
  private final NamesPreferences namesPreferences;
  private JScrollPane speciesInfoScrollPane;
  private final ReportSet reportSet;
  
  private final static Logger logger = Logger.getLogger(SpeciesSpHybridEntryPanel.class.getName());

  /** 
   * Possible locations for Focus within the panel;  used to track
   * the currently selected taxon.
   * TODO: just use focusMoved...
   */
  private enum FocusLocation {
    SPECIES_POPUP,
    SPECIES,
    SP_SPECIES_POPUP,
    EXTERNAL
  }

  /** Creates a new SpeciesSpHybridEntryPanel. */
  @Inject
  public SpeciesSpHybridEntryPanel(
      ReportSet reportSet,
      SpeciesIndexerPanelConfigurer speciesIndexerPanelConfigurer,
      FontManager fontManager,
      SpeciesInfoDescriber speciesInfoDescriber,
      ShowSpeciesMap showSpeciesMap,
      NamesPreferences namesPreferences,
      Alerts alerts) {
    this.reportSet = reportSet;
    this.speciesIndexerPanelConfigurer = speciesIndexerPanelConfigurer;
    this.fontManager = fontManager;
    this.speciesInfoDescriber = speciesInfoDescriber;
    this.showSpeciesMap = showSpeciesMap;
    this.namesPreferences = namesPreferences;
    this.alerts = alerts;
    
    initComponents();
    
    speciesIndexer.addPropertyChangeListener("selectedValue", new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        String taxonId = (String) evt.getNewValue();
        if (taxonId == null) {
          if (focusLocation == FocusLocation.SPECIES_POPUP) {
            setFocusLocation(FocusLocation.SPECIES);
          }
        } else {
          setFocusLocation(FocusLocation.SPECIES_POPUP);
        }
        updateSpeciesText();
      }
    });
 
    speciesIndexer.addPropertyChangeListener("value", new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        updatePrimarySpeciesButtons();
        updateSpeciesText();
      }
    });
    // Button should start out disabled
    addButton.getAction().setEnabled(false);
    spButton.setEnabled(false);
    hybridButton.setEnabled(false);
    
    addAction.setDelegate(addButton.getAction());

    // Whenever a slash is typed in the indexer, treat that as triggering a "sp." 
    speciesIndexer.getTextComponent().addKeyListener(new KeyAdapter() {
      @Override
      public void keyTyped(KeyEvent e) {
        if (e.getKeyChar() == '/') {
          e.consume();
          if (spButton.isEnabled()) {
            spButton.doClick(100);
          }
        }
      }      
    });
    
    updateSpeciesText();
  }
  
  public JComponent extractSpeciesInfoPanel() {
    remove(speciesInfoScrollPane);
    return speciesInfoScrollPane;
  }
  

  /** Updates the enabled state of the primary buttons for entering species. */
  private void updatePrimarySpeciesButtons() {
    boolean enabled = speciesIndexer.getValue() != null
        && !spEntryPanel.isVisible(); 
        
    addButton.getAction().setEnabled(enabled);
    spButton.setEnabled(enabled);
    hybridButton.setEnabled(enabled);
    speciesIndexer.setEnabled(!spEntryPanel.isVisible());
  }
  
  /** Record the current focus location - used to drive updateSpeciesText() */
  private void setFocusLocation(FocusLocation focusLocation) {
    if (this.focusLocation != focusLocation) {
      this.focusLocation = focusLocation;
    }
  }

  private void updateSpeciesText() {
    StringBuilder builder = new StringBuilder("<html>");

    if (focusLocation == null) {
      builder.append("<span style='color:gray'>")
          .append(Messages.getMessage(Name.SPECIES_INFORMATION_WILL_APPEAR_HERE))
          .append("</span>");
      builder.append("</html>");
      speciesInfoText.setText(builder.toString());
      return;
    }

    Resolved taxon = getFocusedTaxon();
    if (taxon != null) {
      speciesInfoDescriber.toHtmlText(taxon, reportSet, checklist, checklistLocation, builder);
    }
    
    builder.append("</html>");
    speciesInfoText.setText(builder.toString());
    speciesInfoText.select(0, 0);
  }

  /**
   * Return the Resolved that the user is currently focused on.  Either
   * returns a direct Resolved in the species list, or a synthetic one currently
   * being worked on.
   */
  private Resolved getFocusedTaxon() {
    Taxon taxon;
    switch (focusLocation) {
      case SPECIES:
        taxon = speciesIndexer.getValue() == null ? null : taxonomy.getTaxon(speciesIndexer.getValue());
        break;
      case SPECIES_POPUP:
        taxon = speciesIndexer.getSelectedValue() == null
            ? null : taxonomy.getTaxon(speciesIndexer.getSelectedValue());
        break;
      case SP_SPECIES_POPUP:
        taxon = spEntryPanel.indexerPanel.getSelectedValue() == null
            ? null : taxonomy.getTaxon(spEntryPanel.indexerPanel.getSelectedValue());
        break;
      case EXTERNAL:
        return resolvedSource.get();
        
      default:
        throw new IllegalStateException();
    }
    
    return taxon == null ? null : SightingTaxons.newResolved(taxon);
  }

  /**
   * Attach a supplier that will resolve the currently focused Resolved species.
   */
  public void setFocusResolver(Supplier<Resolved> resolvedSource) {
    this.resolvedSource = resolvedSource;
    setFocusLocation(FocusLocation.EXTERNAL);
    updateSpeciesText();
  }
  
  /**
   * Returns an Action that will delegate to add species or add sp/hybrid.
   */
  public Action getAddAction() {
    return addAction;
  }
  
  /**
   * Supports hiding the in-panel add buttons (when AddAction is used for
   * external add functionality).
   */
  public void setInPanelAddButtonVisibility(boolean addButtonVisibility) {
    addButton.setVisible(addButtonVisibility);
    spEntryPanel.addSpOrHybrid.setVisible(addButtonVisibility);
  }
  
  private void initComponents() {

    indexerLabel = new JLabel();
    indexerLabel.setAlignmentX(0.0f);
        
    speciesIndexer = new IndexerPanel<String>();
    // Simulate a click, which will add the species, and provide visual
    // feedback on what the "Add" button does.
    speciesIndexer.addActionListener(e -> addButton.doClick(100));

    speciesInfoScrollPane = new JScrollPane();
    
    speciesInfoText = new JEditorPane();
    speciesInfoText.setContentType("text/html");
    speciesInfoText.setBackground(Color.WHITE);
    speciesInfoText.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, true);
    // Make focus visibly obvious by selecting and deselecting the text
    speciesInfoText.addFocusListener(new FocusAdapter() {
      @Override public void focusGained(FocusEvent e) {
        speciesInfoText.selectAll();
      }

      @Override public void focusLost(FocusEvent e) {
        speciesInfoText.select(0, 0);
      }
    });
    speciesInfoText.addHyperlinkListener(new HyperlinkListener() {
      @Override
      public void hyperlinkUpdate(HyperlinkEvent event) {
        if (event.getEventType() == EventType.ACTIVATED) {
          if (SpeciesInfoDescriber.isShowMapUrl(event.getURL())){
            String id = SpeciesInfoDescriber.getShowMapSpeciesId(event.getURL());
            try {
              showSpeciesMap.showRange(reportSet, taxonomy, id);
            } catch (IOException e) {
              alerts.showError(SpeciesSpHybridEntryPanel.this, e, Name.MAP_FAILED, Name.COULDNT_WRITE_MAP);
            }
          } else {
            if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
              try {
                Desktop.getDesktop().browse(event.getURL().toURI());
              } catch (IOException | URISyntaxException e) {
                logger.log(Level.WARNING, "Couldn't open URL " + event.getURL(), e);
              }
            }            
          }
        }
      }
    });
    speciesInfoScrollPane.setViewportView(speciesInfoText);
    speciesInfoScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
    speciesInfoScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    speciesInfoText.setEditable(false);

    addButton = new JButton();
    
    spButton = new JButton();
    spButton.setText(Messages.getMessage(Name.SPUH_LABEL));

    hybridButton = new JButton();
    hybridButton.setText(Messages.getMessage(Name.HYBRID_LABEL));

    BoxLayout vertical = new BoxLayout(this, BoxLayout.PAGE_AXIS);
    setLayout(vertical);
    
    JPanel topRow = new JPanel();
    topRow.setAlignmentX(0.0f);
    BoxLayout topRowLayout = new BoxLayout(topRow, BoxLayout.LINE_AXIS);
    topRow.setLayout(topRowLayout);
    topRow.add(indexerLabel);
    topRow.add(speciesIndexer);
    topRow.add(addButton);
    topRow.add(spButton);
    topRow.add(hybridButton);
 
    spEntryPanel = new SpEntryPanel();
    spEntryPanel.setVisible(false);
    
    
    add(topRow);
    add(spEntryPanel);
    add(Box.createRigidArea(new Dimension(0, 10)));
    add(speciesInfoScrollPane);

    addButton.setAction(new AbstractAction() {
      @Override public void actionPerformed(ActionEvent evt) {
        String taxonId = speciesIndexer.getValue();
        Taxon taxon = taxonomy.getTaxon(taxonId);
        Resolved resolved = SightingTaxons.newResolved(taxon);
        addSpecies(resolved);
      }
    });
    addButton.setText(Messages.getMessage(Name.ADD_SPECIES));
    
    spButton.addActionListener(evt -> openSpPanel(evt, SightingTaxon.Type.SP));

    hybridButton.addActionListener(evt -> openSpPanel(evt, SightingTaxon.Type.HYBRID));
    
    fontManager.applyTo(this);
  }

  static public class SpeciesEntryEvent {
    private final Resolved resolved;

    public SpeciesEntryEvent(Resolved resolved) {
      this.resolved = resolved;
    }

    public Resolved getResolved() {
      return resolved;
    }
  }
  
  public interface SpeciesEntryListener extends EventListener {
    void addSpecies(SpeciesEntryEvent event);
  }
  
  private final List<SpeciesEntryListener> listeners = Lists.newCopyOnWriteArrayList();
  private Checklist checklist;
  private Location checklistLocation;
  
  public void addSpeciesEntryListener(SpeciesEntryListener l) {
    listeners.add(l);
  }
  
  private void addSpecies(Resolved resolved) {
    SpeciesEntryEvent event = new SpeciesEntryEvent(resolved);
    for (SpeciesEntryListener listener : listeners) {
      listener.addSpecies(event);
    }
    speciesIndexer.setValue(null);
  }

  private void openSpPanel(ActionEvent evt, SightingTaxon.Type type) {
    String taxonId = speciesIndexer.getValue();
    Taxon taxon = taxonomy.getTaxon(taxonId);
    spEntryPanel.setVisible(true);
    spEntryPanel.setBaseTaxon(type, taxon);
    updatePrimarySpeciesButtons();
    spEntryPanel.requestFocusInWindow();
  }

  public String getTitle() {
    return "Enter Species";
  }
  
  public void setIndexerLabel(String indexerText) {
    indexerLabel.setText(indexerText);
  }
  
  public IndexerPanel<String> getIndexer() {
    return speciesIndexer;
  }

  /**
   * Panel allowing entry of Sp.
   */
  class SpEntryPanel extends JPanel {
    private JLabel baseSpeciesText;
    private IndexerPanel<String> indexerPanel;
    private JButton secondSp;
    private JButton addSpOrHybrid;
    private Taxon baseTaxon;
    private String secondTaxonId;
    private SightingTaxon.Type type;
    private JButton cancelSpOrHybrid;

    SpEntryPanel() {
      this.initComponents();

      indexerPanel.addPropertyChangeListener("selectedValue", new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
          String taxonId = (String) evt.getNewValue();
          if (taxonId == null) {
            if (focusLocation == FocusLocation.SP_SPECIES_POPUP) {
              setFocusLocation(FocusLocation.SPECIES);
            }
          } else {
            setFocusLocation(FocusLocation.SP_SPECIES_POPUP);
          }
          updateSpeciesText();
        }
      });
   
      indexerPanel.addPropertyChangeListener("value", new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
          String taxonId = (String) evt.getNewValue();
          addSpOrHybrid.getAction().setEnabled(taxonId != null);
          secondSp.setEnabled(taxonId != null);
          if (taxonId != null) {
            Resolved resolved = SightingTaxons.newResolved(taxonomy.getTaxon(taxonId));
            setFocusResolver(Suppliers.ofInstance(resolved));
          }
          updateSpeciesText();
        }
      });

      // Simulate a click, which will add the sp., and provide visual
      // feedback on what the "Add" button does.
      indexerPanel.addActionListener(e -> addSpOrHybrid.doClick(100));
      
      addSpOrHybrid.setAction(new AbstractAction() {
        @Override public void actionPerformed(ActionEvent event) {
          addSpOrHybrid(event);
        }
      });
      addSpOrHybrid.setText(Messages.getMessage(Name.ADD_BUTTON));
      
      
      secondSp.addActionListener(new ActionListener() {
        @Override public void actionPerformed(ActionEvent e) {
          secondTaxonId = indexerPanel.getValue();
          if (secondTaxonId != null) {
            String append = (type == SightingTaxon.Type.SP) ? " / " : " x ";
            Taxon secondTaxon = taxonomy.getTaxon(secondTaxonId);
            baseSpeciesText.setText(TaxonUtils.getCommonName(baseTaxon) + append
                + TaxonUtils.getCommonName(secondTaxon) + append);
            indexerPanel.setTextValue("");
            secondSp.setVisible(false);
            indexerPanel.requestFocusInWindow();
          }
        }
      });
      cancelSpOrHybrid.addActionListener(new ActionListener() {
        @Override public void actionPerformed(ActionEvent e) {
          spEntryPanel.hideSpOrHybridPanel();
          updatePrimarySpeciesButtons();
          speciesIndexer.requestFocusInWindow();
          focusLocation = FocusLocation.SPECIES;
        }
      });
    }
    
    private void initComponents() {
      baseSpeciesText = new JLabel();
      setAlignmentX(0.0f);
      indexerPanel = new IndexerPanel<String>();
      addSpOrHybrid = new JButton(Messages.getMessage(Name.ADD_BUTTON));
      cancelSpOrHybrid = new JButton(Messages.getMessage(Name.CANCEL_BUTTON));
      secondSp = new JButton(Messages.getMessage(Name.SPUH_LABEL));
      BoxLayout layout = new BoxLayout(this, BoxLayout.LINE_AXIS);
      setLayout(layout);
      
      add(baseSpeciesText);
      add(indexerPanel);
      add(addSpOrHybrid);
      add(secondSp);
      add(cancelSpOrHybrid);
      
      fontManager.applyTo(this);
    }
    
    public void setBaseTaxon(SightingTaxon.Type type, Taxon taxon) {
      this.type = type;
      baseTaxon = taxon;
      String append = (type == SightingTaxon.Type.SP) ? " / " : " x ";
      baseSpeciesText.setText(TaxonUtils.getCommonName(taxon) + append);
      
      secondSp.setVisible(type == SightingTaxon.Type.SP);
      
      SpHybridEntry.configureIndexer(indexerPanel, taxon, namesPreferences);

      indexerPanel.setPreviewText(speciesIndexer.getPreviewText());
      indexerPanel.setLayoutStrategy(speciesIndexer.getLayoutStrategy());
      indexerPanel.setFont(fontManager.getTextFont());
      indexerPanel.setValue(null);
      
      
      addSpOrHybrid.getAction().setEnabled(false);
      addAction.setDelegate(addSpOrHybrid.getAction());
    }
    
    void hideSpOrHybridPanel() {
      setVisible(false);
      addAction.setDelegate(addButton.getAction());
    }

    private void addSpOrHybrid(ActionEvent evt) {
      String taxonId = indexerPanel.getValue();
      SightingTaxon newTaxon;
      if (secondTaxonId == null) {
        newTaxon = (type == SightingTaxon.Type.SP)
            ? SightingTaxons.newSpTaxon(ImmutableSet.of(baseTaxon.getId(), taxonId))
            : SightingTaxons.newHybridTaxon(ImmutableSet.of(baseTaxon.getId(), taxonId));
      } else {
        newTaxon = (type == SightingTaxon.Type.SP)
            ? SightingTaxons.newSpTaxon(ImmutableSet.of(baseTaxon.getId(), taxonId, secondTaxonId))
            : SightingTaxons.newHybridTaxon(ImmutableSet.of(baseTaxon.getId(), taxonId, secondTaxonId));
      }
      Resolved resolved = newTaxon.resolveInternal(taxonomy);
      addSpecies(resolved);
      speciesIndexer.setValue(null);
      indexerPanel.setValue(null);
      hideSpOrHybridPanel();
      updatePrimarySpeciesButtons();
      speciesIndexer.requestFocusInWindow();
      focusLocation = FocusLocation.SPECIES;
    }

    @Override
    public boolean requestFocusInWindow() {
      return indexerPanel.requestFocusInWindow();
    }
  }
  
  public void setTaxonomy(Taxonomy newTaxonomy) {
    if (newTaxonomy == this.taxonomy) {
      return;
    }
        
    this.taxonomy = newTaxonomy;
    
    speciesIndexerPanelConfigurer.configure(speciesIndexer, newTaxonomy);
    speciesIndexer.setValue(null);
    spEntryPanel.hideSpOrHybridPanel();
    updatePrimarySpeciesButtons();
  }

  public void setChecklist(Checklist checklist, Location checklistLocation) {
    this.checklist = checklist;
    this.checklistLocation = checklistLocation;
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    speciesInfoText.setPreferredSize(fontManager.scale(new Dimension(480, 100)));
    speciesInfoText.setMaximumSize(fontManager.scale(new Dimension(Short.MAX_VALUE, 250)));
    updateSpeciesText();
  }

  @Override
  public int getBaseline(int width, int height) {
    return indexerLabel.getBaseline(indexerLabel.getWidth(), indexerLabel.getHeight()) + indexerLabel.getY();
  }

  public Component getLastFocusableComponent() {
    return speciesInfoText;
  }
  
  @Override
  public boolean requestFocusInWindow() {
    return speciesIndexer.requestFocusInWindow();
  }
}
