/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.SplashScreen;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;

import com.google.common.base.Supplier;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.scythebill.birdlist.model.util.Progress;
import com.scythebill.birdlist.ui.app.StartupView;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.guice.ScythebillSplash;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Panel showing progress as multiple Progress load. 
 */
public class StartupPanel implements StartupView {
  private static final int PROGRESS_PIXELS = 300;
  private Timer timer;
  private final FontManager fontManager;
  private final List<Supplier<Integer>> progressSuppliers = new CopyOnWriteArrayList<Supplier<Integer>>();
  private final Provider<Image> splashImageProvider; 
  private JFrame frame;

  @Inject
  public StartupPanel(
      FontManager fontManager,
      @ScythebillSplash Provider<Image> splashImageProvider) {
    this.fontManager = fontManager;
    this.splashImageProvider = splashImageProvider;
  }
  
  @Override
  public void addProgress(final Progress progress, final double fraction) {
    progressSuppliers.add(new Supplier<Integer>() {
      @Override public Integer get() {
        return (int) (PROGRESS_PIXELS * ((fraction * progress.current()) / progress.max()));
      }      
    });
  }
  
  @Override
  public void start() {
    final JProgressBar progressBar = new JProgressBar(0, PROGRESS_PIXELS);
    Dimension preferredSize = progressBar.getPreferredSize();
    preferredSize.width = PROGRESS_PIXELS;
    progressBar.setPreferredSize(preferredSize);
    
    JPanel content = new JPanel();
    content.setOpaque(false);    

    JLabel loadingLabel = new JLabel(Messages.getMessage(Name.SCYTHEBILL_IS_STARTING));
    loadingLabel.setOpaque(false);
    loadingLabel.setFont(fontManager.getLabelFont());
    loadingLabel.setPreferredSize(new Dimension(300, 40));
    loadingLabel.setHorizontalTextPosition(SwingConstants.CENTER);
    loadingLabel.setVerticalTextPosition(SwingConstants.CENTER);
    loadingLabel.setAlignmentX(0.5f);
    loadingLabel.setAlignmentY(0.5f);
    
    BoxLayout layout = new BoxLayout(content, BoxLayout.Y_AXIS);
    content.setLayout(layout);
    
    content.add(Box.createVerticalGlue());
    content.add(loadingLabel);
    content.add(Box.createRigidArea(new Dimension(10, 10)));
    content.add(progressBar);
    content.setBorder(new EmptyBorder(10, 10, 10, 10));
    
    frame = new JFrame();
    frame.getContentPane().setLayout(new BorderLayout());
    frame.getContentPane().add(content, BorderLayout.CENTER);
    ((JPanel) frame.getContentPane()).setOpaque(false);
    frame.setUndecorated(true);
    frame.setResizable(false);
    SplashScreen splashScreen = SplashScreen.getSplashScreen();
    if (splashScreen == null) {
      // If there's no splash screen, then load the splash screen image and show a mock
      // splash.
      Image splashImage = splashImageProvider.get();
      if (splashImage != null) {
        loadingLabel.setForeground(Color.WHITE);
        ImageIcon icon = new ImageIcon(splashImage);
        JLabel splash = new JLabel(icon);
        frame.getLayeredPane().add(splash, Integer.valueOf(Integer.MIN_VALUE));
        splash.setSize(icon.getIconWidth(), icon.getIconHeight());
        frame.setSize(icon.getIconWidth(), icon.getIconHeight());
      } else {
        // Can't even load the splash - just show the progress bar
        frame.pack();
      }
      frame.setLocationRelativeTo(null);
    } else {
      // If there was a splash screen, replace it with a mock using its image
      loadingLabel.setForeground(Color.WHITE);
      frame.setBounds(splashScreen.getBounds());
      ImageIcon icon = new ImageIcon(splashScreen.getImageURL());
      JLabel iconLabel = new JLabel(icon);
      frame.getLayeredPane().add(iconLabel, Integer.valueOf(Integer.MIN_VALUE));
      iconLabel.setSize(splashScreen.getSize());
    }
    frame.setVisible(true);
    frame.toFront();
    timer = new Timer(50, new ActionListener() {
      @Override public void actionPerformed(ActionEvent e) {
        progressBar.setValue(getProgress());
      }
    });
    
    timer.start();
  }
  
  @Override
  public void stop() {
    timer.stop();
    frame.setVisible(false);
  }
  
  private int getProgress() {
    int progress = 0;
    for (Supplier<Integer> supplier : progressSuppliers) {
      progress += supplier.get();
    }
    
    return progress;
  }
}
