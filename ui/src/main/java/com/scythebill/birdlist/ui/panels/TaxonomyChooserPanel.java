/*   
 * Copyright 2012 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.user.User;
import com.scythebill.birdlist.model.util.Indexer;
import com.scythebill.birdlist.model.util.ToString;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.components.IndexerPanel.LayoutStrategy;
import com.scythebill.birdlist.ui.events.DefaultUserChangedEvent;
import com.scythebill.birdlist.ui.events.DefaultUserStore;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.events.TaxonomyChangedEvent;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.guice.Clements;
import com.scythebill.birdlist.ui.guice.IOC;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Panel for selecting the taxonomy to use for operations.
 * Publishes notifications of updates to the EventBus (via TaxonomyStore).
 */
class TaxonomyChooserPanel extends JPanel implements FontsUpdatedListener{
  private final TaxonomyStore taxonomyStore;
  private final Taxonomy clements;
  private final MappedTaxonomy ioc;
  private JComboBox<PossibleTaxonomy> taxonomyChooser;
  private IndexerPanel<User> userIndexer;
  private JSeparator separator;
  private JLabel taxonomyLabel;
  private JLabel userLabel;
  private ReportSet reportSet;
  private DefaultUserStore defaultUserStore;

  @Inject
  public TaxonomyChooserPanel(
      TaxonomyStore taxonomyStore,
      @Clements Taxonomy clements,
      @IOC MappedTaxonomy ioc,
      DefaultUserStore defaultUserStore,
      ReportSet reportSet,
      FontManager fontManager,
      EventBusRegistrar eventBusRegistrar) {
    this.taxonomyStore = taxonomyStore;
    this.clements = clements;
    this.ioc = ioc;
    this.defaultUserStore = defaultUserStore;
    this.reportSet = reportSet;
    eventBusRegistrar.registerWhenInHierarchy(this);
    initGui();
    fontManager.applyTo(this);

    taxonomyChooser.setSelectedIndex(findTaxonomyIndex(taxonomyStore.getTaxonomy()));
    taxonomyChooser.addActionListener(e -> updateTaxonomy());
    
    userIndexer.addPropertyChangeListener("value", e -> {
      defaultUserStore.setUser(userIndexer.getValue());
    });

    // HACK: it's possible for the taxonomy to change between the panel being created
    // and the ancestor listener firing.  Catch this.  (Maybe the EventBusRegistrar could handle
    // this and buffer up any received events, perhaps?  but that gets awfully tricky.)
    addAncestorListener(new AncestorListener() {      
      @Override
      public void ancestorAdded(AncestorEvent event) {
        taxonomyChanged(null);
      }

      @Override
      public void ancestorRemoved(AncestorEvent event) {
      }

      @Override
      public void ancestorMoved(AncestorEvent event) {
      }
    });
  }

  private int findTaxonomyIndex(Taxonomy taxonomy) {
    for (int i = 0; i < taxonomyChooser.getItemCount(); i++) {
      if (taxonomy == taxonomyChooser.getItemAt(i).taxonomy) {
        return i;
      }
    }
    return 0;
  }

  @Subscribe
  public void taxonomyChanged(TaxonomyChangedEvent event) {
    updateTaxonomyChooser();
    int newIndex = findTaxonomyIndex(taxonomyStore.getTaxonomy());
    if (newIndex != taxonomyChooser.getSelectedIndex()) {
      taxonomyChooser.setSelectedIndex(newIndex);
    }
  }
  
  @Subscribe
  public void defaultUserChanged(DefaultUserChangedEvent event) {
    updateUserChooser();
    if (userIndexer != null && userIndexer.getValue() != event.getUser()) {
      userIndexer.setValue(event.getUser());
    }
  }
  
  private void updateTaxonomy() {
    taxonomyStore.setTaxonomy(
        ((PossibleTaxonomy) taxonomyChooser.getSelectedItem()).taxonomy);
  }
  
  private void initGui() {
    taxonomyLabel = new JLabel(Messages.getMessage(Name.TAXONOMY_LABEL));
    taxonomyChooser = new JComboBox<PossibleTaxonomy>();
    updateTaxonomyChooser();

    userLabel = new JLabel(Messages.getMessage(Name.OBSERVER_LABEL));
    userIndexer = new IndexerPanel<>();
    userIndexer.setColumns(15);
    userIndexer.setLayoutStrategy(LayoutStrategy.BELOW);
    userIndexer.setPreviewText(Name.OBSERVER_NAME);
    updateUserChooser();

    separator = new JSeparator();
  }
  
  private void updateUserChooser() {
    if (reportSet.getUserSet() != null) {
      userLabel.setVisible(true);
      userIndexer.setVisible(true);
      userIndexer.removeAllIndexerGroups();
      Indexer<User> indexer = new Indexer<>(4);
      for (User user : reportSet.getUserSet().allUsers()) {
        if (user.name() != null) {
          indexer.add(user.name(), user);
        } else {
          indexer.add(user.abbreviation(), user);
        }
      }
      ToString<User> toString = new ToString<User>() {
        @Override
        public String getString(User user) {
          if (user == null) {
            return "";
          }
          return user.name() != null ? user.name() : user.abbreviation();
        }
      };
      userIndexer.addIndexerGroup(toString, indexer);
      userIndexer.setValue(defaultUserStore.getUser());
    } else {
      userLabel.setVisible(false);
      userIndexer.setVisible(false);
    }
  }

  private void updateTaxonomyChooser() {
    // Taxonomy chooser already has the right number of elements.
    boolean taxonomiesChanged = false;
    
    // Different number of items, must be different
    if (taxonomyChooser.getItemCount() != 2 + reportSet.extendedTaxonomies().size()) {
      taxonomiesChanged = true;
    }
        
    PossibleTaxonomy[] taxonomies = new PossibleTaxonomy[2 + reportSet.extendedTaxonomies().size()];
    taxonomies[0] = new PossibleTaxonomy("eBird/Clements", clements);
    taxonomies[1] = new PossibleTaxonomy("IOC", ioc);
    int i = 2;
    for (Taxonomy extendedTaxonomy : reportSet.extendedTaxonomies()) {
      // Item at this index is different, must be different
      if (!taxonomiesChanged) {
        PossibleTaxonomy existingItem = taxonomyChooser.getItemAt(i);
        if (existingItem.taxonomy != extendedTaxonomy) {
          taxonomiesChanged = true;
        }
      }
      taxonomies[i++] = new PossibleTaxonomy(extendedTaxonomy.getName(), extendedTaxonomy);
    }
    // Only bother updating the model if necessary
    if (taxonomiesChanged) {
      taxonomyChooser.setModel(new DefaultComboBoxModel<PossibleTaxonomy>(taxonomies));
      taxonomyChooser.setMaximumRowCount(taxonomies.length);
    }
  }
  
  @Override
  public void fontsUpdated(FontManager fontManager) {
    int borderSize = fontManager.scale(10); 
    setBorder(new EmptyBorder(borderSize, borderSize, borderSize, borderSize));
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    
    layout.setVerticalGroup(layout.createSequentialGroup()
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(taxonomyLabel)
            .addComponent(taxonomyChooser)
            .addComponent(userLabel)
            .addComponent(userIndexer))
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(separator));
    layout.setHorizontalGroup(layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
            .addComponent(taxonomyLabel)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(taxonomyChooser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(ComponentPlacement.UNRELATED)
            .addComponent(userLabel)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(userIndexer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        .addComponent(separator, fontManager.scale(50), fontManager.scale(300), Short.MAX_VALUE));
  }
  
  static class PossibleTaxonomy {
    final String name;
    final Taxonomy taxonomy;

    PossibleTaxonomy(String name, Taxonomy taxonomy) {
      this.name = name;
      this.taxonomy = taxonomy;
    }
    
    @Override
    public String toString() {
      return name;
    }
  }
}
