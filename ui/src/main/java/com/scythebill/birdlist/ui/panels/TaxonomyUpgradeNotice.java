/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.Nullable;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.TransferHandler;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.io.Files;
import com.scythebill.birdlist.model.checklist.ChecklistResolution;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.io.HtmlResponseWriter;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Type;
import com.scythebill.birdlist.model.taxa.CompletedUpgrade;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.ResolvedComparator;
import com.scythebill.birdlist.model.xml.XmlReportSetImport;
import com.scythebill.birdlist.ui.backup.BackupSaver;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.location.ChecklistResolutionResults;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * UI for showing that a taxonomy has been upgraded.
 */
public class TaxonomyUpgradeNotice {
  private final Taxonomy clements;
  private final FontManager fontManager;
  private final Alerts alerts;
  private final TaxonomyStore taxonomyStore;

  public TaxonomyUpgradeNotice(
      Taxonomy clements,
      Alerts alerts,
      FontManager fontManager,
      TaxonomyStore taxonomyStore) {
    this.clements = clements;
    this.alerts = alerts;
    this.fontManager = fontManager;
    this.taxonomyStore = taxonomyStore;
  }
  
  /**
   * Shows an upgrade screen for eBird/Clements upgrades.
   * 
   * @return a collection of sighting taxa that need to be reconciled
   */
  public Collection<SightingTaxon> showClementsUpgrade(
      ReportSet reportSet,
      File reportSetFile,
      CompletedUpgrade completedUpgrade,
      BackupSaver backupSaver) {
    boolean standardBackupsAreEnabled = backupSaver.backupsAreEnabled();
    String upgradeCaution;
    if (standardBackupsAreEnabled) { 
      backupSaver.saveNow(reportSet, reportSetFile);
      upgradeCaution =
          Messages.getMessage(Name.RECORDS_HAVE_BEEN_UPDATED_AFTER_BACKUP);
    } else {
      File backupFile = getBackupFile(reportSetFile, completedUpgrade);
      try {
        Files.copy(reportSetFile, backupFile);
        upgradeCaution = Messages.getFormattedMessage(Name.RECORDS_UPDATED_WITH_ONE_OFF_COPY,
            HtmlResponseWriter.htmlEscape(backupFile.getName()));
      } catch (IOException e) {
        alerts.showError(null,
            e,
            Name.COULD_NOT_BACKUP_DURING_UPGRADE_TITLE,
            Name.COULD_NOT_BACKUP_DURING_UPGRADE_MESSAGE,
            backupFile.getParentFile().getAbsolutePath());
        upgradeCaution = Messages.getMessage(Name.RECORDS_UPDATED_WITHOUT_BACKUP);
      }
    }

    boolean warningsPresent = !completedUpgrade.warnings().isEmpty();
    Taxonomy currentTaxonomy = taxonomyStore.getTaxonomy();
    String warningsTaxaName;
    if (warningsPresent) {
      warningsTaxaName = getTaxonNames(currentTaxonomy, completedUpgrade.warnings(), true, null);
      if (warningsTaxaName.isEmpty()) {
        warningsPresent = false;
      }
    } else {
      warningsTaxaName = "";
    }

    boolean spsPresent = !completedUpgrade.sps(currentTaxonomy).isEmpty();
    Set<SightingTaxon> sightingTaxaToUpgrade = ImmutableSet.of();
    String taxonNamesOfSps = "";
    // Test out if there really *are* sps to show
    if (spsPresent) {
      sightingTaxaToUpgrade = Sets.newHashSet();
      taxonNamesOfSps = getTaxonNames(currentTaxonomy, completedUpgrade.sps(currentTaxonomy), false,
          sightingTaxaToUpgrade);
      if (taxonNamesOfSps.isEmpty()) {
        spsPresent = false;
      }
    }
    
    String openDiv = spsPresent || warningsPresent
        ? String.format("<div style=\"width:%spx;\">", fontManager.scale(600))
        : "";
    String closeDiv = spsPresent || warningsPresent
        ? "</div>"
        : "";
    
    String formattedMessage = alerts.getFormattedDialogMessage(
        Messages.getMessage(Name.UPGRADE_SUCCESSFUL),
        "");
    
    List<JComponent> components = Lists.newArrayList();
    JLabel label = new JLabel(formattedMessage);
    label.putClientProperty(FontManager.PLAIN_LABEL, true);
    components.add(label);

    JLabel upgradeLabel = new JLabel(String.format("<html><br>%s%s<br>%s%s",
        openDiv,
        Messages.getFormattedMessage(Name.SCYTHEBILL_UPGRADED_TO_TAXONOMY_FORMAT, clements.getName()),
        upgradeCaution,
        closeDiv));
    upgradeLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    components.add(upgradeLabel);

    // TODO: fix behavior for users that are *only* using extended taxonomies?
    if (currentTaxonomy == taxonomyStore.getIoc()) {
      JLabel iocToo = new JLabel("<html><br>"
          + Messages.getMessage(Name.SCYTHEBILL_UPGRADE_EVEN_FOR_IOC_USERS));
      iocToo.putClientProperty(FontManager.PLAIN_LABEL, true);
      components.add(iocToo);
    }
    
    if (warningsPresent) {
      JLabel message = new JLabel("<html><br>" +
          Messages.getMessage(Name.UPGRADE_MANUALLY_EXAMINE_TAXA));
      
      message.putClientProperty(FontManager.PLAIN_LABEL, true);
      JTextArea textArea = new JTextArea();
      textArea.setText(warningsTaxaName);
      textArea.setRows(Math.min(20, Math.max(3, completedUpgrade.warnings().size())));
      textArea.setEditable(false);
      textArea.selectAll();
      textArea.getTransferHandler().exportToClipboard(
          textArea,
          Toolkit.getDefaultToolkit().getSystemClipboard(),
          TransferHandler.COPY);
      textArea.select(0, 0);
      
      JScrollPane scrollPane = new JScrollPane(textArea);
      components.add(message);
      components.add(scrollPane);
    }
    
    if (spsPresent) {
      JLabel message = new JLabel("<html><br>" + Messages.getMessage(Name.UPGRADE_SOME_SPUH_TAXA));
      message.putClientProperty(FontManager.PLAIN_LABEL, true);
      // Count the number of rows, and size the text area accordingly (but not
      // less than three rows or more than twenty)
      JTextArea textArea = new JTextArea(taxonNamesOfSps);
      int rows = Iterables.size(Splitter.on('\n').split(taxonNamesOfSps));
      textArea.setRows(Math.min(20, Math.max(3, rows)));
      textArea.setEditable(false);
      
      JScrollPane scrollPane = new JScrollPane(textArea);
      components.add(message);
      components.add(scrollPane);
    }

    for (JComponent component : components) {
      fontManager.applyTo(component);
    }
    
    JOptionPane.showMessageDialog(null,
        components.toArray(),
        "",
        JOptionPane.INFORMATION_MESSAGE,
        alerts.getAlertIcon());
    
    return sightingTaxaToUpgrade;
  }
  
  /**
   * Shows an IOC upgrade screen.
   * 
   * @return a collection of sighting taxa that need to be reconciled
   */
  public Collection<SightingTaxon> showNonClementsUpgrade(
      ReportSet reportSet,
      File reportSetFile,
      CompletedUpgrade completedUpgrade) {
    Preconditions.checkArgument(completedUpgrade.getForcedTaxonomy() != null, "Expecting forced taxonomy");
    boolean warningsPresent = !completedUpgrade.warnings().isEmpty();
    String warningsTaxaName;
    if (warningsPresent) {
      warningsTaxaName = getTaxonNames(completedUpgrade.getForcedTaxonomy(), completedUpgrade.warnings(), true, null);
      if (warningsTaxaName.isEmpty()) {
        warningsPresent = false;
      }
    } else {
      warningsTaxaName = "";
    }

    boolean spsPresent = !completedUpgrade.sps(completedUpgrade.getForcedTaxonomy()).isEmpty();
    Set<SightingTaxon> sightingTaxaToUpgrade = ImmutableSet.of();
    String taxonNamesOfSps = "";
    // Test out if there really *are* sps to show
    if (spsPresent) {
      sightingTaxaToUpgrade = Sets.newHashSet();
      taxonNamesOfSps = getTaxonNames(completedUpgrade.getForcedTaxonomy(),
          completedUpgrade.sps(completedUpgrade.getForcedTaxonomy()), false, sightingTaxaToUpgrade);
      if (taxonNamesOfSps.isEmpty()) {
        spsPresent = false;
      }
    }
    
    String openDiv = spsPresent || warningsPresent
        ? String.format("<div style=\"width:%spx;\">", fontManager.scale(300))
        : "";
    String closeDiv = spsPresent || warningsPresent
        ? "</div>"
        : "";
    
    String formattedMessage = alerts.getFormattedDialogMessage(
        Messages.getMessage(Name.UPGRADE_SUCCESSFUL),
        "");
    
    List<JComponent> components = Lists.newArrayList();
    JLabel label = new JLabel(formattedMessage);
    label.putClientProperty(FontManager.PLAIN_LABEL, true);
    components.add(label);

    JLabel upgradeLabel = new JLabel(String.format("<html><br>%s%s<br>%s",
        openDiv,
        Messages.getFormattedMessage(Name.SCYTHEBILL_UPGRADED_TO_TAXONOMY_FORMAT, completedUpgrade.getForcedTaxonomy().getName()),
        closeDiv));
    upgradeLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    components.add(upgradeLabel);

    if (warningsPresent) {
      JLabel message = new JLabel("<html><br>"
          + Messages.getMessage(Name.UPGRADE_MANUALLY_EXAMINE_TAXA));
      
      message.putClientProperty(FontManager.PLAIN_LABEL, true);
      JTextArea textArea = new JTextArea();
      textArea.setText(warningsTaxaName);
      textArea.setRows(Math.min(20, Math.max(3, completedUpgrade.warnings().size())));
      textArea.setEditable(false);
      textArea.selectAll();
      textArea.getTransferHandler().exportToClipboard(
          textArea,
          Toolkit.getDefaultToolkit().getSystemClipboard(),
          TransferHandler.COPY);
      textArea.select(0, 0);
      
      JScrollPane scrollPane = new JScrollPane(textArea);
      components.add(message);
      components.add(scrollPane);
    }
    
    if (spsPresent) {
      JLabel message = new JLabel("<html><br>"
          + Messages.getMessage(Name.UPGRADE_SOME_IOC_SPUH_TAXA));
      message.putClientProperty(FontManager.PLAIN_LABEL, true);
      // Count the number of rows, and size the text area accordingly (but not
      // less than three rows or more than twenty)
      JTextArea textArea = new JTextArea(taxonNamesOfSps);
      int rows = Iterables.size(Splitter.on('\n').split(taxonNamesOfSps));
      textArea.setRows(Math.min(20, Math.max(3, rows)));
      textArea.setEditable(false);
      
      JScrollPane scrollPane = new JScrollPane(textArea);
      components.add(message);
      components.add(scrollPane);
    }

    for (JComponent component : components) {
      fontManager.applyTo(component);
    }
    
    String[] options = new String[]{
        Messages.getMessage(Name.RESOLVE_NOW), Messages.getMessage(Name.NOT_NOW)};
    
    int showOptionDialog = JOptionPane.showOptionDialog(
        null,
        components.toArray(),
        "",
        JOptionPane.DEFAULT_OPTION,
        JOptionPane.INFORMATION_MESSAGE,
        alerts.getAlertIcon(),
        options,
        options[0]);
    return showOptionDialog == 0 ? sightingTaxaToUpgrade : ImmutableList.of();
  }

  private File getBackupFile(File oldFile, CompletedUpgrade completedUpgrade) {
    String oldName = oldFile.getName();
    if (oldName.endsWith(XmlReportSetImport.REPORT_SET_SUFFIX)) {
      oldName = oldName.substring(0, oldName.length()
          - XmlReportSetImport.REPORT_SET_SUFFIX.length());
    }

    // TODO: don't overwrite a previous backup?
    String newName = oldName + "_backup_" + completedUpgrade.getPreviousTaxonomyId() +
        XmlReportSetImport.REPORT_SET_SUFFIX;
    
    return new File(oldFile.getParent(), newName);
  }

  /**
   * Sorting by taxonomic order, build up newline-delimited names.
   * <p>
   * In addition:
   * <li>Drop anything that is not a Sp. in the user's preferred taxonomy.
   * <li>Optionally return a set of Sp. (in the base taxonomy) that match the prior criterion 
   */
  private String getTaxonNames(
      Taxonomy currentTaxonomy,
      Collection<SightingTaxon> collection,
      boolean includeScientific,
      @Nullable Collection<SightingTaxon> stillSpInTaxonomy) {
    // Sorting by taxonomic order, build up a string of names
    TreeSet<Resolved> treeSet = Sets.newTreeSet(new ResolvedComparator());
    for (SightingTaxon taxon : collection) {
      Resolved resolved = taxon.resolve(currentTaxonomy);
      if (resolved.getSightingTaxon().getType() == Type.SP) {
        if (stillSpInTaxonomy != null) {
          stillSpInTaxonomy.add(taxon);
        }
        treeSet.add(resolved);
      }
    }
    
    final List<String> taxaNames = Lists.newArrayList();
    for (Resolved resolved : treeSet) {
      String next;
      if (includeScientific) {
        next = resolved.getCommonName() + 
            " (" + resolved.getFullName() + ")";
      } else {
        next = resolved.getCommonName();
      }
      if (next.length() > 120) {
        next = next.substring(0, 120) + "\u2026";
      }
      taxaNames.add(next);
    }
    
    return Joiner.on('\n').join(taxaNames);
  }

  /**
   * Attempt to pre-resolve SPs against checklists.
   * @param taxonomy 
   * 
   * @return null if no resolution took place, otherwise a set of remaining Sp's
   */
  public ImmutableSet<SightingTaxon> attemptChecklistResolution(
      ReportSet reportSet,
      Checklists checklists,
      ImmutableSet<SightingTaxon> spsToResolve,
      boolean forceIocResolution) {
    
    ChecklistResolution checklistResolution = new ChecklistResolution(checklists, ImmutableSet.of());
    ImmutableSet<SightingTaxon> remainingSightingTaxons =
        checklistResolution.attemptChecklistResolution(
            reportSet, spsToResolve, forceIocResolution ? taxonomyStore.getIoc() : clements);
    List<Sighting> sightingsToRemove = checklistResolution.sightingsToRemove();
    List<Sighting> sightingsToAdd = checklistResolution.sightingsToAdd();
    int simplifiedButStillSp = checklistResolution.simplifiedButNotFullyResolved();
    int notSimplified = checklistResolution.notSimplifiedCount();
    
    // Nothing found that is simpler, bail
    if (sightingsToAdd.isEmpty()) {
      return null;
    }
    
    int totalSightingsConsidered = notSimplified + sightingsToAdd.size();
    
    String message = Messages.getFormattedMessage(Name.OUT_OF_SIGHTINGS_TO_RESOLVE_FORMAT,
        totalSightingsConsidered) + "\n\n";
    int fullyCleaned = sightingsToAdd.size() - simplifiedButStillSp;
    if (fullyCleaned > 0) {
      message +=
          Messages.getFormattedMessage(Name.SOME_CAN_BE_ENTIRELY_RESOLVED_USING_CHECKLISTS, fullyCleaned)
          + "\n";
    }
    if (simplifiedButStillSp > 0) {
      message +=
          Messages.getFormattedMessage(Name.SOME_CAN_BE_PARTIALLY_RESOLVED, simplifiedButStillSp)
          + "\n";
    }
    if (notSimplified > 0) {
      message +=
          Messages.getFormattedMessage(Name.SOME_CANNOT_BE_RESOLVED_AT_ALL, notSimplified)
          + "\n";
    }
    message += "\n"
        + Messages.getMessage(Name.LET_SCYTHEBILL_RESOLVE_SIGHTINGS_AUTOMATICALLY);
    
    JComponent resultsSummary = ChecklistResolutionResults.asPanel(checklistResolution,
        forceIocResolution ? taxonomyStore.getIoc() : clements, fontManager);
    int yesOrNo = alerts.showYesNoWithPanel(null,
        alerts.getFormattedDialogMessage(
            Messages.getMessage(Name.AUTO_RESOLVE_WITH_CHECKLISTS), message),
        resultsSummary);
    
    if (yesOrNo == JOptionPane.YES_OPTION) {
      reportSet.mutator()
          .removing(sightingsToRemove)
          .adding(sightingsToAdd)
          .mutate();
      return remainingSightingTaxons;
    } else {
      return null;
    }
  }
}
