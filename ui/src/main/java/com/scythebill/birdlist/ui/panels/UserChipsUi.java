package com.scythebill.birdlist.ui.panels;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

import com.scythebill.birdlist.model.user.User;
import com.scythebill.birdlist.model.user.UserSet;
import com.scythebill.birdlist.model.util.Indexer;
import com.scythebill.birdlist.model.util.ToString;
import com.scythebill.birdlist.ui.components.ChipsTextPanel;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.components.IndexerPanel.LayoutStrategy;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Creates and manages a series of components for handling user entry.
 */
public class UserChipsUi {
  public final JLabel userLabel;
  public final IndexerPanel<User> userIndexer;
  public final JButton addUserButton;
  public final ChipsTextPanel<User> userChips;
  public final JScrollPane userChipsScrollPane;

  public UserChipsUi(UserSet userSet, FontManager fontManager) {
    userLabel = new JLabel(Messages.getMessage(Name.OBSERVERS_LABEL));
    userIndexer = new IndexerPanel<>();
    userIndexer.setColumns(15);
    userIndexer.setLayoutStrategy(LayoutStrategy.BELOW);
    Indexer<User> indexer = new Indexer<>(4);
    for (User user : userSet.allUsers()) {
      if (user.name() != null) {
        indexer.add(user.name(), user);
      } else {
        indexer.add(user.abbreviation(), user);
      }
    }
    ToString<User> toString = new ToString<User>() {
      @Override
      public String getString(User user) {
        return user.name() != null ? user.name() : user.abbreviation();
      }
    };
    userIndexer.addIndexerGroup(toString, indexer);
    userIndexer.setPreviewText(Name.OBSERVER_NAME);
    addUserButton = new JButton(Messages.getMessage(Name.ADD_BUTTON));
    addUserButton.setEnabled(false);
    userIndexer.addPropertyChangeListener("value", e -> {
      addUserButton.setEnabled(e.getNewValue() != null);
    });
    
    userChips = new ChipsTextPanel<>(toString::getString, User.comparator(), fontManager);
    userChips.setScaledWidth(270);
    userChipsScrollPane = new JScrollPane(userChips,
        JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

    ActionListener addUser = e -> {
      User user = userIndexer.getValue();
      if (user != null) {
        userChips.addChip(user);
      }
      userIndexer.clearValue();
    };
    userIndexer.addActionListener(addUser);
    addUserButton.addActionListener(addUser);
  }

  public void setEnabled(boolean enabled) {
    userLabel.setEnabled(enabled);
    userIndexer.setEnabled(enabled);
    addUserButton.setVisible(enabled);
    userChips.setEnabled(enabled);
  }  
}
