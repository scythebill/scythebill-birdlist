/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.awt.Component;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Optional;

import javax.annotation.Nullable;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.user.User;
import com.scythebill.birdlist.model.user.UserSet;
import com.scythebill.birdlist.ui.components.ErrorLabel;
import com.scythebill.birdlist.ui.components.OkCancelPanel;
import com.scythebill.birdlist.ui.components.PossiblyRequiredLabel;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.fonts.FontPreferences;

/**
 * Edits (or creates) a single User.
 */
public class UserDialog {

  public static void main(String[] args) {
    UserSet userSet = new UserSet();
    User aw = userSet.addUser(userSet.newUserBuilder().setName("Adam Winer").setAbbreviation("AW"));
    userSet.addUser(userSet.newUserBuilder().setName("Howard Winer").setAbbreviation("HW"));

    UserDialog userDialog = new UserDialog(new FontManager(new FontPreferences()));
    userDialog.showUserDialog(null, "Test Title", userSet, aw, u -> { System.err.println(userSet.addUser(u)); });
  }

  public interface ResultsListener {
    void userAvailable(User.Builder user);
  }
  
  private final FontManager fontManager;

  @Inject
  UserDialog(FontManager fontManager) {
    this.fontManager = fontManager;
  }

  public void showUserDialog(
      Component parent, String title, UserSet userSet, @Nullable User user, ResultsListener listener) {
    ModalityType modality =
        Toolkit.getDefaultToolkit().isModalityTypeSupported(ModalityType.DOCUMENT_MODAL)
            ? ModalityType.DOCUMENT_MODAL : ModalityType.APPLICATION_MODAL;
    final JDialog dialog = new JDialog(
        parent == null ? null : SwingUtilities.getWindowAncestor(parent), title, modality);
    UserPanel userPanel = new UserPanel(userSet, user);
    
    Action cancelAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        dialog.dispose();
      }
    };
    final Action okAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent event) {
        User.Builder value = userPanel.getValue();
        if (value != null) {
          listener.userAvailable(value);
          dialog.dispose();
        }
      }
    };

    userPanel.addPropertyChangeListener("value",
        e -> okAction.setEnabled(e.getNewValue() != null));
    okAction.setEnabled(userPanel.getValue() != null);
    OkCancelPanel okCancel =
        new OkCancelPanel(okAction, cancelAction, userPanel, userPanel.nameField);
    okCancel.setPreferredSize(fontManager.scale(new Dimension(450, 200)));
    dialog.setContentPane(okCancel);
    fontManager.applyTo(okCancel);
    dialog.pack();
    dialog.setVisible(true);
  }

  class UserPanel extends JPanel implements FontsUpdatedListener {
    private final UserSet userSet;
    private final User user;
    private User.Builder value;
    private PossiblyRequiredLabel nameLabel;
    private JTextField nameField;
    private PossiblyRequiredLabel abbreviationLabel;
    private JTextField abbreviationField;
    private ErrorLabel errors;
    private boolean editedAbbreviation;

    UserPanel(UserSet userSet, @Nullable User user) {
      this.userSet = userSet;
      this.user = user;
      this.value = user == null ? null : user.asBuilder();
      // If the abbreviation was set on the way in, don't set it by default
      editedAbbreviation = user == null ? false : !Strings.isNullOrEmpty(user.abbreviation());
      initComponents();
    }

    public User.Builder getValue() {
      return value;
    }
    
    private void initComponents() {
      DocumentListener recomputeValue = new DocumentListener() {
        @Override
        public void removeUpdate(DocumentEvent e) {
          recomputeValue();
        }
        
        @Override
        public void insertUpdate(DocumentEvent e) {
          recomputeValue();
        }
        
        @Override
        public void changedUpdate(DocumentEvent e) {
          recomputeValue();
        }
      };
      nameLabel = new PossiblyRequiredLabel(Messages.getMessage(Name.NAME_LABEL));
      nameLabel.setRequired(true);
      nameField = new JTextField(20);
      if (value != null) {
        nameField.setText(Strings.nullToEmpty(value.name()));
      }
      nameField.getDocument().addDocumentListener(recomputeValue);
      nameField.getDocument().addDocumentListener(new DocumentListener() {
        @Override
        public void removeUpdate(DocumentEvent e) {
          if (!editedAbbreviation) {
            recomputeAbbreviation();
          }          
        }
        
        @Override
        public void insertUpdate(DocumentEvent e) {
          if (!editedAbbreviation) {
            recomputeAbbreviation();
          }          
        }
        
        @Override
        public void changedUpdate(DocumentEvent e) {
          if (!editedAbbreviation) {
            recomputeAbbreviation();
          }          
        }
      });

      abbreviationLabel = new PossiblyRequiredLabel(Messages.getMessage(Name.ABBREVIATION_LABEL));
      abbreviationLabel.setRequired(true);
      abbreviationField = new JTextField(10);
      if (value != null) {
        abbreviationField.setText(Strings.nullToEmpty(value.abbreviation()));
      }
      abbreviationField.getDocument().addDocumentListener(recomputeValue);
      abbreviationField.addKeyListener(new KeyAdapter() {
        @Override
        public void keyTyped(KeyEvent e) {
          if (e.getKeyChar() != KeyEvent.VK_TAB) {
            editedAbbreviation = true;
          }
        }
      });
      errors = new ErrorLabel();
    }

    // Try to compute an abbreviation
    private void recomputeAbbreviation() {
      Optional<String> defaultAbbreviation =
          userSet.computeDefaultAbbreviation(nameField.getText(), Optional.ofNullable(user));
      defaultAbbreviation.ifPresent(abbreviationField::setText);
    }

    private boolean isAbbreviationAvailable(String abbreviation) {
      // If the abbreviation is unused, then it's good.
      if (!userSet.hasUserWithAbbreviation(abbreviation)) {
        return true;
      }
      
      // If the abbreviation is *the current* user, then it's also fine.
      User userByAbbreviation = userSet.userByAbbreviation(abbreviation);
      if (userByAbbreviation == user) {
        return true;
      }
      
      return false;
    }

    @Override
    public void fontsUpdated(FontManager fontManager) {
      GroupLayout groupLayout = new GroupLayout(this);

      groupLayout.setHorizontalGroup(
          groupLayout.createParallelGroup(Alignment.LEADING)
              .addGroup(groupLayout.createSequentialGroup()
                  .addGap(fontManager.scale(10))
                  .addComponent(errors))
              .addGroup(
                  groupLayout.createSequentialGroup()
                      .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                          .addComponent(nameLabel)
                          .addComponent(abbreviationLabel))
                      .addPreferredGap(ComponentPlacement.UNRELATED)
                      .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                          .addComponent(nameField, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
                              GroupLayout.PREFERRED_SIZE)
                          .addComponent(abbreviationField, GroupLayout.PREFERRED_SIZE,
                              GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))));

      groupLayout.setVerticalGroup(groupLayout.createSequentialGroup()
          .addGroup(groupLayout.createBaselineGroup(false, false).addComponent(nameLabel)
              .addComponent(nameField))
          .addGroup(groupLayout.createBaselineGroup(false, false).addComponent(abbreviationLabel)
              .addComponent(abbreviationField))
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addComponent(errors));

      setLayout(groupLayout);
    }
    
    private void recomputeValue() {
      User.Builder newValue = user == null ? userSet.newUserBuilder() : user.asBuilder();
      String name = nameField.getText();
      newValue.setName(name);
      String abbreviation = abbreviationField.getText();
      newValue.setAbbreviation(abbreviation);
      if (!abbreviation.isEmpty() && !isAbbreviationAvailable(abbreviation)) {
        errors.setText(Messages.getMessage(Name.ALREADY_AN_OBSERVER_WITH_THAT_ABBREVIATION));
      } else if (!abbreviation.isEmpty() && User.NOT_ELIGIBLE_ABBREVIATION_CHARS.matchesAnyOf(abbreviation)) {
        errors.setText(Messages.getMessage(Name.ILLEGAL_ABBREVIATION_CHARACTER));
      } else {
        errors.setText("");
      }
      
      if (!newValue.isValid(userSet)) {
        newValue = null;
      }
      
      if (!Objects.equal(value, newValue)) {
        User.Builder oldValue = value;
        value = newValue;
        firePropertyChange("value", oldValue, newValue);
      }
    }
  }
}
