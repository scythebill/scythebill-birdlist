/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.DefaultListCellRenderer;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.LayoutStyle.ComponentPlacement;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.user.User;
import com.scythebill.birdlist.model.user.UserSet;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.fonts.FontPreferences;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.ListListModel;

/**
 * Panel supporting the editing of a set of users.
 */
public class UserSetPanel extends JPanel implements FontsUpdatedListener {

  public static void main(String[] args) {
    JFrame frame = new JFrame("UserSetPanel test");
    FontManager fontManager = new FontManager(new FontPreferences());
    UserSetPanel userSetPanel = new UserSetPanel(null, new Alerts(null), new UserDialog(fontManager));
    UserSet userSet = new UserSet();
    userSet.addUser(userSet.newUserBuilder().setName("Adam Winer").setAbbreviation("AW"));
    userSet.addUser(userSet.newUserBuilder().setName("Howard Winer").setAbbreviation("HW"));
    userSetPanel.setUserSet(userSet);

    userSetPanel.fontsUpdated(fontManager);
    frame.setContentPane(userSetPanel);
    frame.pack();
    frame.setVisible(true);
  }
  
  private final ReportSet reportSet;
  private final Alerts alerts;
  private final UserDialog userDialog;
  private JList<User> userList;
  private JScrollPane scrollPane;
  private JButton addButton;
  private JButton editButton;
  private JButton deleteButton;
  private UserSet userSet;

  @Inject
  UserSetPanel(ReportSet reportSet, Alerts alerts, UserDialog userDialog) {
    this.reportSet = reportSet;
    this.alerts = alerts;
    this.userDialog = userDialog;
    initComponents();
    // A dummy user set, to be replaced
    setUserSet(new UserSet());
  }

  private void initComponents() {
    userList = new JList<>();
    userList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    userList.setModel(new ListListModel<>());
    userList.setCellRenderer(new DefaultListCellRenderer() {
      @Override
      public Component getListCellRendererComponent(JList<?> list, Object value, int index,
          boolean isSelected, boolean cellHasFocus) {
        User user = (User) value;
        if (user.name() != null) {
          if (user.abbreviation() != null) {
            value = String.format("%s (%s)", user.name(), user.abbreviation());
          } else {
            value = user.name();
          }
        } else {
          value = user.abbreviation();
        }

        return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
      }
    });
    userList.addListSelectionListener(e -> {
      boolean selectionIsEmpty = userList.getSelectedIndex() < 0;
      editButton.setEnabled(!selectionIsEmpty);
      deleteButton.setEnabled(!selectionIsEmpty);
    });
    scrollPane = new JScrollPane(userList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
        JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

    addButton = new JButton(Messages.getMessage(Name.ADD_WITH_ELLIPSIS));
    addButton.addActionListener(this::addUser);

    editButton = new JButton(Messages.getMessage(Name.EDIT_WITH_ELLIPSIS));
    editButton.addActionListener(this::editUser);
    editButton.setEnabled(false);    

    deleteButton = new JButton(Messages.getMessage(Name.DELETE_WITH_ELLIPSIS));
    deleteButton.addActionListener(this::deleteUser);
    deleteButton.setEnabled(false);    
  }
  
  public void setUserSet(UserSet userSet) {
    this.userSet = userSet;
    List<User> list = userList();
    list.clear();
    list.addAll(userSet.allUsers());
    Collections.sort(list, User.comparator());
  }

  private List<User> userList() {
    return ((ListListModel<User>) userList.getModel()).asList();
  }

  private void addUser(ActionEvent e) {
    userDialog.showUserDialog(this, Messages.getMessage(Name.ADD_OBSERVER), userSet, null,
        builder -> {
          User newUser = userSet.addUser(builder);
          insertUser(newUser);
        });
  }

  /**
   * Inserts a (not present) user into the list.
   */
  private void insertUser(User user) {
    List<User> list = userList();
    int index = Collections.binarySearch(list, user, User.comparator());
    if (index < 0) {
      // < 0: means not found, but an insertion point found
      index = (-index) - 1;
    }
    list.add(index, user);
  }

  private void editUser(ActionEvent e) {
    User user = userList.getSelectedValue();
    if (user != null) {
      int index = userList.getSelectedIndex();
      userDialog.showUserDialog(this, Messages.getMessage(Name.EDIT_OBSERVER), userSet, user,
          builder -> {
            User updatedUser = userSet.addUser(builder);
            // Remove from the list, since no longer properly sorted
            userList().remove(index);
            // ... and re-insert
            insertUser(updatedUser);
          });
    }
  }

  private void deleteUser(ActionEvent e) {
    User user = userList.getSelectedValue();
    if (user != null) {
      boolean anySightingsWithUser = false;
      boolean anySightingsWithOnlyThatUser = false;
      
      ImmutableSet<User> onlyThatUser = ImmutableSet.of(user);
      for (Sighting sighting : reportSet.getSightings()) {
        if (sighting.hasSightingInfo()) {
          ImmutableSet<User> users = sighting.getSightingInfo().getUsers();
          if (!anySightingsWithUser) {
            if (users.contains(user)) {
              anySightingsWithUser = true;
            }
          }
          
          if (users.equals(onlyThatUser)) {
            anySightingsWithOnlyThatUser = true;
            // Break here;  nothing more to do before asking the user.
            break;
          }
        }
      }
      
      boolean removeSightingsWithOnlyThatUser = false;
      String userName = user.name() != null ? user.name() : user.abbreviation();
      if (anySightingsWithOnlyThatUser) {
        int option = alerts.showWithOptions(this,
            Messages.getMessage(Name.DELETE_OBSERVER_TITLE),
            Messages.getFormattedMessage(
                Name.DELETE_OBSERVER_AND_MAYBE_SIGHTINGS_MESSAGE,
                userName),
            Messages.getMessage(Name.DROP_SIGHTINGS),
            Messages.getMessage(Name.LEAVE_EMPTY),
            Messages.getMessage(Name.CANCEL_BUTTON));
        if (option < 0 || option >= 2) {
          return;
        }
        
        removeSightingsWithOnlyThatUser = (option == 0);
      } else if (anySightingsWithUser) {
        if (alerts.showOkCancel(this,
            Name.DELETE_OBSERVER_TITLE,
            Name.DELETE_OBSERVER_MESSAGE,
            userName) != JOptionPane.OK_OPTION) {
          return;
        }
      }
      
      // Ready to delete
      int index = userList.getSelectedIndex();
      userList().remove(index);
      
      userSet.removeUser(user);
      
      if (anySightingsWithUser) {
        LoadingCache<ImmutableSet<User>, ImmutableSet<User>> removedUserCache =
            CacheBuilder.newBuilder().build(new CacheLoader<ImmutableSet<User>, ImmutableSet<User>>() {
              @Override
              public ImmutableSet<User> load(ImmutableSet<User> set) throws Exception {
                return set.stream().filter(existing -> !existing.equals(user)).collect(ImmutableSet.toImmutableSet());
              }
            });
  
        List<Sighting> sightingsToRemove = new ArrayList<>();
        for (Sighting sighting : reportSet.getSightings()) {
          if (sighting.hasSightingInfo()) {
            ImmutableSet<User> users = sighting.getSightingInfo().getUsers();
            if (removeSightingsWithOnlyThatUser && users.equals(onlyThatUser)) {
              // Remove the sighting!
              sightingsToRemove.add(sighting);
            } else if (users.contains(user)) {
              // Edit the sighting (possibly to an empty set)
              sighting.getSightingInfo().setUsers(removedUserCache.getUnchecked(users));
            }
          }
        }
        
        // If any sightings need to be removed, do so.
        if (!sightingsToRemove.isEmpty()) {
          reportSet.mutator().removing(sightingsToRemove).mutate();
        }
      }
    }
  }
  
  @Override
  public void fontsUpdated(FontManager fontManager) {
    GroupLayout groupLayout = new GroupLayout(this);

    groupLayout.setHorizontalGroup(groupLayout.createSequentialGroup().addComponent(scrollPane)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addGroup(groupLayout.createParallelGroup(Alignment.CENTER)
            .addComponent(addButton, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE)
            .addComponent(editButton, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE)
            .addComponent(deleteButton, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
                GroupLayout.PREFERRED_SIZE)));

    groupLayout.setVerticalGroup(
        groupLayout.createParallelGroup(Alignment.CENTER).addComponent(scrollPane)
            .addGroup(groupLayout.createSequentialGroup()
                .addComponent(addButton, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
                    GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(editButton, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
                    GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(ComponentPlacement.RELATED).addComponent(deleteButton,
                    GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
                    GroupLayout.PREFERRED_SIZE)));

    groupLayout.linkSize(addButton, editButton, deleteButton);

    setLayout(groupLayout);
  }

}
