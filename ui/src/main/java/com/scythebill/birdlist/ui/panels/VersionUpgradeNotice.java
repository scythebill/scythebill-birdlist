/*   
 * Copyright 2012 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.google.common.base.Joiner;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.prefs.PreferencesManager;
import com.scythebill.birdlist.ui.uptodate.UpToDateLoader.VersionInfo;
import com.scythebill.birdlist.ui.uptodate.UpToDatePreferences;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.DesktopUtils;

/**
 * UI for showing that a new version is available.
 */
public class VersionUpgradeNotice {
  private final FontManager fontManager;
  private final Alerts alerts;
  private final static Logger logger = Logger.getLogger(VersionUpgradeNotice.class.getName());
  private final PreferencesManager preferencesManager;

  @Inject
  public VersionUpgradeNotice(
      Alerts alerts,
      PreferencesManager preferencesManager,
      FontManager fontManager) {
    this.alerts = alerts;
    this.preferencesManager = preferencesManager;
    this.fontManager = fontManager;    
  }
  
  public void showUpgrade(ImmutableList<VersionInfo> versionInfoList) {    
    Preconditions.checkArgument(!versionInfoList.isEmpty());
    
    VersionInfo newestVersion = versionInfoList.get(0);
    
    // If the latest version has already received a notification, don't bug the user again. 
    UpToDatePreferences upToDate = preferencesManager.getPreference(UpToDatePreferences.class);
    if (Objects.equals(newestVersion.version, upToDate.notifiedVersion)) {
      return;
    }
    
    String formattedMessage;
    if (versionInfoList.size() == 1) {
      formattedMessage = alerts.getFormattedDialogMessage(
          Name.NEW_VERSION_IS_AVAILABLE,
          Name.NEW_VERSION_MESSAGE_ONE_VERSION,
          newestVersion.version);
    } else {
      formattedMessage = alerts.getFormattedDialogMessage(
          Name.NEW_VERSION_IS_AVAILABLE,
          Name.NEW_VERSION_MESSAGE_MULTIPLE_VERSIONS,
          newestVersion.version);
    }
    
    List<String> changes = new ArrayList<String>();
    for (VersionInfo versionInfo : versionInfoList) {
      changes.addAll(Collections2.filter(versionInfo.changes, Predicates.notNull()));
    }
    
    JTextArea textArea = new JTextArea();
    JLabel label = new JLabel(formattedMessage);
    textArea.setText(Joiner.on('\n').join(changes));
    textArea.setRows(Math.min(5, changes.size()));
    textArea.setEditable(false);
    textArea.setCaretPosition(0);
      
    JScrollPane scrollPane = new JScrollPane(textArea);
    List<JComponent> components = Lists.newArrayList((JComponent) label, scrollPane);
    for (JComponent component : components) {
      fontManager.applyTo(component);
    }

    String[] options = new String[] {
        Messages.getMessage(Name.SKIP_THIS_VERSION),
        Messages.getMessage(Name.ASK_ME_LATER),
        Messages.getMessage(Name.DOWNLOAD_NOW)
    };

    int result = JOptionPane.showOptionDialog(null,
        components.toArray(), "",
        JOptionPane.DEFAULT_OPTION,
        JOptionPane.QUESTION_MESSAGE,
        alerts.getAlertIcon(),
        options, options[2]);
    if (result == 2) {
      // Tell users that they'll need to quit Scythebill before installing.
      alerts.showMessage(null, Name.PLEASE_QUIT_TITLE, Name.PLEASE_QUIT_MESSAGE);
      URI uri = MoreObjects.firstNonNull(
          newestVersion.url,
          URI.create("http://downloads.scythebill.com")); 
      try {
        DesktopUtils.openUrlInBrowser(uri, alerts);
      } catch (IOException ioe) {
        logger.log(Level.WARNING, "Failed to browse", ioe);
      }
    } else if (result == 0) {
      upToDate.notifiedVersion = newestVersion.version;
    }
  }
}
