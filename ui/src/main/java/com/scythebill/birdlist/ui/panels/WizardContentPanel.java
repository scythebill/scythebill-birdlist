/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import java.awt.Component;
import java.awt.Container;

import javax.swing.JPanel;

import com.google.common.base.Preconditions;

/**
 * Base class for all components added to a wizard.
 */
public abstract class WizardContentPanel<T> extends JPanel {
  private final String name;
  private boolean complete;
  private Class<T> type;

  public WizardContentPanel(String name, Class<T> type) {
    this.name = name;
    this.type = type;
  }
  
  @Override
  public String getName() {
    return name;
  }
  
  public boolean isComplete() {
    return complete;
  }
  
  protected T wizardValue() {
    Container parent = getParent();
    while (parent != null) {
      if (parent instanceof WizardPanelBase) {
        @SuppressWarnings("unchecked")
        WizardPanelBase<T> wizard = (WizardPanelBase<T>) parent;
        return type.cast(wizard.wizardValue());
      }
      
      parent = parent.getParent();
    }
    
    throw new IllegalStateException("No parent found");
  }
  
  /**
   * Returns the component focusable immediately after the content.   Useful for
   * custom FocusTraversalPolicy instances.
   */
  protected Component getFocusableComponentAfterContent() {
    Container parent = getParent();
    while (parent != null) {
      if (parent instanceof WizardPanelBase) {
        WizardPanelBase<?> wizard = (WizardPanelBase<?>) parent;
        return wizard.getFocusableComponentAfterContent();
      }
      
      parent = parent.getParent();
    }
    
    throw new IllegalStateException("No parent found");
  }
  
  public void setComplete(boolean complete) {
    if (complete != this.complete) {
      this.complete = complete;
      firePropertyChange("complete", !complete, complete);
    }
  }
  
  /** Override to provide behavior when navigating to a panel */
  protected void shown() {
  }
  
  /**
   * Override to provide behavior when leaving a panel.
   * Return false to block navigation.
   */
  protected boolean leaving(boolean validateLeaving) {
    Preconditions.checkState(!validateLeaving || isComplete(), "Panel is not complete");
    return true;
  }

  /** Override to provide behavior before the panel is visible. */
  protected void beforeShown() {
  }
  
  protected boolean isActive() {
    return false;
  }
}
