/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.app.Titled;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.ButtonSize;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.VisibilityDetector;

/**
 * Base class for wizard UIs.
 */
public abstract class WizardPanelBase<T> extends JPanel implements Titled, FontsUpdatedListener {
  private JButton nextButton;
  private JButton backButton;
  private JButton cancelButton;
  private JButton doneButton;
  private AbstractAction cancelAction;
  private AbstractAction backAction;
  private AbstractAction nextAction;
  private AbstractAction saveAction;
  private JPanel wizardContent;
  private CardLayout wizardContentLayout;
  private List<WizardContentPanel<T>> pages = Lists.newArrayList();
  private Set<String> pageNames = Sets.newHashSet();

  private int currentIndex = -1;
  private JSeparator separator;
  private final NavigableFrame navigableFrame;
  private final VisibilityDetector visibilityDetector;

  public WizardPanelBase(
      NavigableFrame navigableFrame, VisibilityDetector visibilityDetector) {
    this.visibilityDetector = visibilityDetector;
    initGUI();
    this.navigableFrame = navigableFrame; 
  }

  abstract protected T wizardValue();
  
  private void initGUI() {
    getInputMap(WHEN_IN_FOCUSED_WINDOW).put(
        KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "cancel");
    getActionMap().put("cancel", getCancelAction());

    nextButton = new JButton();
    nextButton.setText(Messages.getMessage(Name.NEXT_WITH_RIGHT_ARROW));
    nextButton.setAction(getNextAction());
    nextButton.putClientProperty(
        FontManager.BUTTON_SIZE_PROPERTY, ButtonSize.MEDIUM);
    backButton = new JButton();
    backButton.setText(Messages.getMessage(Name.BACK_WITH_LEFT_ARROW));
    backButton.setAction(getBackAction());
    backButton.putClientProperty(
        FontManager.BUTTON_SIZE_PROPERTY, ButtonSize.MEDIUM);
    cancelButton = new JButton();
    cancelButton.setText(Messages.getMessage(Name.CANCEL_BUTTON));
    cancelButton.setAction(getCancelAction());
    cancelButton.putClientProperty(
        FontManager.BUTTON_SIZE_PROPERTY, ButtonSize.MEDIUM);
    doneButton = new JButton();
    doneButton.setText(Messages.getMessage(Name.DONE_BUTTON));
    doneButton.setAction(getSaveAction());
    doneButton.putClientProperty(
        FontManager.BUTTON_SIZE_PROPERTY, ButtonSize.MEDIUM);
    visibilityDetector.install(doneButton);
    separator = new JSeparator();
    wizardContent = new JPanel();
    wizardContentLayout = new CardLayout();
    wizardContent.setLayout(wizardContentLayout);
  }

  private AbstractAction getCancelAction() {
    if (cancelAction == null) {
      cancelAction = new AbstractAction(Messages.getMessage(Name.CANCEL_BUTTON), null) {
        @Override
        public void actionPerformed(ActionEvent evt) {
          WizardContentPanel<T> oldPage = getCurrentPage();
          if (oldPage != null) {
            oldPage.leaving(false);
          }
          navigableFrame.complete(evt);
        }
      };
    }
    return cancelAction;
  }

  private AbstractAction getBackAction() {
    if (backAction == null) {
      backAction = new AbstractAction(Messages.getMessage(Name.BACK_WITH_LEFT_ARROW), null) {
        @Override
        public void actionPerformed(ActionEvent evt) {
          showPage(getPreviousIndex());
        }
      };
    }
    return backAction;
  }

  private AbstractAction getNextAction() {
    if (nextAction == null) {
      nextAction = new AbstractAction(Messages.getMessage(Name.NEXT_WITH_RIGHT_ARROW), null) {
        @Override
        public void actionPerformed(ActionEvent evt) {
          showPage(getNextIndex());
        }
      };
    }
    return nextAction;
  }

  private AbstractAction getSaveAction() {
    if (saveAction == null) {
      saveAction = new AbstractAction(Messages.getMessage(Name.DONE_BUTTON), null) {
        @Override
        public void actionPerformed(ActionEvent evt) {
          WizardContentPanel<T> oldPage = getCurrentPage();
          if (oldPage != null) {
            oldPage.leaving(true);
          }

          if (save()) {
            navigableFrame.complete(evt);
          }
        }
      };
    }
    return saveAction;
  }

  /**
   * Called when the wizard is complete.
   * Return false to abort navigation.
   */
  abstract protected boolean save();

  /**
   * Adds a page to the end of the wizard.
   * 
   * @param name
   *            the name of the page (must be non-null and unique)
   * @param panel
   *            the panel
   */
  protected void addPage(final WizardContentPanel<T> panel) {
    Preconditions.checkNotNull(panel);

    String name = panel.getName();
    Preconditions.checkNotNull(name);
    Preconditions.checkArgument(!pageNames.contains(name), "Name " + name
        + " is already in this wizard.");

    pages.add(panel);
    pageNames.add(name);
    wizardContent.add(panel, name);
    panel.addPropertyChangeListener("complete", new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        if (panel.getName().equals(getCurrentName())) {
          updateButtons((Boolean) evt.getNewValue());
        }
      }
    });

    if (pages.size() == 1) {
      showPage(0);
    }
  }

  public void showPage(int index) {
    Preconditions.checkArgument(index >= 0,
        "Trying to navigate past the start of the wizard");
    Preconditions.checkArgument(index < pages.size(),
        "Trying to navigate past the end of the wizard");

    // When moving forward, let the old page know so it can finish
    // with its content.
    // NOTE: this assumes wizards are strictly in-order!!!
    WizardContentPanel<T> oldPage = getCurrentPage();
    if (oldPage != null) {
      if (index > currentIndex) {
        if (!oldPage.leaving(true)) {
          return;
        }
      } else {
        // Tell the old page that we're leaving, but don't let it
        // validate anything.
        oldPage.leaving(false);
      }
    }

    final WizardContentPanel<T> newPage = pages.get(index);
    newPage.beforeShown();
    wizardContentLayout.show(wizardContent, newPage.getName());
    currentIndex = index;
    updateButtons(newPage.isComplete());
    navigableFrame.updateTitle();
    // Tell the new page that it's visible, using invokeLater
    // so that it's been properly initialized
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        newPage.shown();
      }
    });
  }

  /**
   * Returns the title of the wizard, concated with the title of the current
   * wizard page.
   */
  @Override
  public String getTitle() {
    String title = getWizardTitle();
    WizardContentPanel<T> page = pages.get(getCurrentIndex());
    if (page instanceof Titled) {
      title = title + " - " + ((Titled) page).getTitle();
    }

    return title;
  }

  protected abstract String getWizardTitle();

  private void updateButtons(boolean complete) {
    boolean isLastPage = getCurrentIndex() == pages.size() - 1;
    saveAction.setEnabled(complete && isLastPage);
    nextAction.setEnabled(complete && !isLastPage);
    backAction.setEnabled(getCurrentIndex() != 0);
  }

  /** Returns the index of the current wizard page */
  protected final int getCurrentIndex() {
    return currentIndex;
  }
  
  protected int getPreviousIndex() {
    return getCurrentIndex() - 1;
  }

  protected int getNextIndex() {
    return getCurrentIndex() + 1;
  }

  /** Returns the current page */
  protected final WizardContentPanel<T> getCurrentPage() {
    if (getCurrentIndex() < 0 || getCurrentIndex() > pages.size()) {
      return null;
    }

    return pages.get(getCurrentIndex());
  }

  /**
   * Returns the name of the current wizard page, or null if no page is visible
   */
  protected final String getCurrentName() {
    WizardContentPanel<T> panel = getCurrentPage();
    return (panel == null) ? null : panel.getName();
  }
  
  @Override
  public void fontsUpdated(FontManager fontManager) {
    GroupLayout thisLayout = new GroupLayout(this);
    fontManager.applyTo(backButton);
    fontManager.applyTo(cancelButton);
    fontManager.applyTo(nextButton);
    fontManager.applyTo(doneButton);
    
    setLayout(thisLayout);
    
    ParallelGroup verticalButtonGroup = thisLayout.createBaselineGroup(false, false)
        .addComponent(nextButton)
        .addComponent(backButton)
        .addComponent(cancelButton)
        .addComponent(doneButton);
    addExtraContentAfterDone(verticalButtonGroup);
    thisLayout.setVerticalGroup(thisLayout.createSequentialGroup()
        .addContainerGap()
        .addComponent(wizardContent,
            fontManager.scale(400), fontManager.scale(400), Short.MAX_VALUE)
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(separator, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
        .addGap(fontManager.scale(18))
        .addGroup(verticalButtonGroup)
        .addContainerGap());
    
    SequentialGroup horizontalButtonGroup = thisLayout.createSequentialGroup()
        .addComponent(cancelButton)
        .addGap(fontManager.scale(86))
        .addComponent(backButton)
        .addGap(fontManager.scale(20))
        .addComponent(nextButton)
        .addGap(0, fontManager.scale(75), Short.MAX_VALUE)
        .addComponent(doneButton);
    addExtraContentAfterDone(horizontalButtonGroup);
    
    thisLayout.setHorizontalGroup(thisLayout.createSequentialGroup()
        .addContainerGap()
        .addGroup(thisLayout.createParallelGroup()
            .addGroup(Alignment.LEADING, horizontalButtonGroup)
            .addComponent(wizardContent, Alignment.LEADING,
                0, fontManager.scale(600), Short.MAX_VALUE)
            .addComponent(separator, Alignment.LEADING,
                0, fontManager.scale(600), Short.MAX_VALUE))
        .addContainerGap());
    thisLayout.linkSize(SwingConstants.HORIZONTAL,
        cancelButton, backButton, nextButton, doneButton);
  }

  protected void addExtraContentAfterDone(SequentialGroup horizontalButtonGroup) {
  }
  
  protected void addExtraContentAfterDone(ParallelGroup verticalButtonGroup) {
  }

  /**
   * Returns the component focusable immediately after the content.   Useful for
   * custom FocusTraversalPolicy instances.
   */
  public Component getFocusableComponentAfterContent() {
    return cancelButton;
  }

  protected int getPageCount() {
    return pages.size();
  }
}
