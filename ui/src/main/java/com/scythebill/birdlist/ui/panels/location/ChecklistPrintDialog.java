/*   
 * Copyright 2024 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.location;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.annotation.Nullable;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.text.NumberFormatter;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences.ScientificOrCommon;
import com.scythebill.birdlist.ui.components.AutoSelectJFormattedTextField;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * Dialog for configuring the print options.
 */
class ChecklistPrintDialog {
  private Alerts alerts;
  private ChecklistPrintPreferences reportPrintPreferences;
  private FontManager fontManager;
  private NamesPreferences namesPreferences;
  public JLabel daysToIncludeLabel;

  @Inject
  ChecklistPrintDialog(
      Alerts alerts,
      ChecklistPrintPreferences reportPrintPreferences,
      NamesPreferences namesPreferences,
      FontManager fontManager) {
    this.alerts = alerts;
    this.reportPrintPreferences = reportPrintPreferences;
    this.namesPreferences = namesPreferences;
    this.fontManager = fontManager;
  }
  
  @Nullable
  public ChecklistPrintPreferences getConfiguration(Component parent) {
    PrintConfigurationPanel panel = new PrintConfigurationPanel();
    fontManager.applyTo(panel);
    
    String formattedMessage = alerts.getFormattedDialogMessage(
        Name.PRINT_OPTIONS_TITLE, Name.PRINT_OPTIONS_MESSAGE);
    
    int okCancel = alerts.showOkCancelWithPanel(
        SwingUtilities.getWindowAncestor(parent), formattedMessage, panel);
    if (okCancel != JOptionPane.OK_OPTION) {
      return null;
    }

    panel.updatePreferences();
    return reportPrintPreferences;
  }
  
  class PrintConfigurationPanel extends JPanel {
    private JCheckBox includeScientific;
    private JCheckBox showFamilies;
    private JCheckBox showStatus;
    private JCheckBox showLifersInBold;
    private JCheckBox compactPrinting;
    private AutoSelectJFormattedTextField daysToInclude;

    PrintConfigurationPanel() {
      GroupLayout layout = new GroupLayout(this);
      setLayout(layout);
      
      includeScientific = new JCheckBox(Messages.getMessage(Name.SCIENTIFIC_NAME_QUESTION));
      includeScientific.setSelected(reportPrintPreferences.includeScientific);
      
      if (namesPreferences.scientificOrCommon == ScientificOrCommon.COMMON_ONLY) {
        includeScientific.setSelected(false);
      } else if (namesPreferences.scientificOrCommon == ScientificOrCommon.SCIENTIFIC_ONLY) {
        includeScientific.setSelected(true);
      }
      
      showFamilies = new JCheckBox(Messages.getMessage(Name.SHOW_FAMILIES_QUESTION));
      showFamilies.setSelected(reportPrintPreferences.showFamilies);

      showStatus = new JCheckBox(Messages.getMessage(Name.SHOW_THREATENED_STATUS_QUESTION));
      showStatus.setSelected(reportPrintPreferences.showStatus);

      showLifersInBold = new JCheckBox(Messages.getMessage(Name.LIFERS_IN_BOLD_QUESTION));
      showLifersInBold.setSelected(reportPrintPreferences.showLifersInBold);

      compactPrinting = new JCheckBox(Messages.getMessage(Name.COMPACT_PRINT_QUESTION));
      compactPrinting.setSelected(reportPrintPreferences.compactPrinting);
      
      NumberFormatter numberFormatter = new NumberFormatter();
      numberFormatter.setValueClass(Integer.class);
      numberFormatter.setMinimum(0);
      numberFormatter.setMaximum(60);

      daysToInclude = new AutoSelectJFormattedTextField(numberFormatter);
      daysToInclude.setColumns(3);
      daysToInclude.setValue(reportPrintPreferences.daysToPrint);
      daysToIncludeLabel = new JLabel(Messages.getMessage(Name.NUMBERS_OF_COLUMNS_FOR_RECORDS));
      
      daysToIncludeLabel.setEnabled(!compactPrinting.isSelected());
      daysToInclude.setEnabled(!compactPrinting.isSelected());
      compactPrinting.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          daysToIncludeLabel.setEnabled(!compactPrinting.isSelected());
          daysToInclude.setEnabled(!compactPrinting.isSelected());
        }
      });

      layout.setHorizontalGroup(layout.createParallelGroup()
          .addComponent(includeScientific)
          .addComponent(showFamilies)
          .addComponent(showStatus)
          .addComponent(showLifersInBold)
          .addComponent(compactPrinting)
          .addComponent(daysToIncludeLabel)
          .addComponent(daysToInclude,
              GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE));
      
      layout.setVerticalGroup(layout.createSequentialGroup()
          .addComponent(includeScientific)
          .addComponent(showFamilies)
          .addComponent(showStatus)
          .addComponent(showLifersInBold)
          .addComponent(compactPrinting)
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addComponent(daysToIncludeLabel)
          .addComponent(daysToInclude));
    }
    
    void updatePreferences() {
      reportPrintPreferences.includeScientific = includeScientific.isSelected();
      reportPrintPreferences.showFamilies = showFamilies.isSelected();
      reportPrintPreferences.showStatus = showStatus.isSelected();
      reportPrintPreferences.showLifersInBold = showLifersInBold.isSelected();
      reportPrintPreferences.compactPrinting = compactPrinting.isSelected();
      reportPrintPreferences.daysToPrint = (Integer) daysToInclude.getValue();
    }
  }

  /**
   * Derive the scientific-and-common ordering based on a mix of the chosen setting
   * in the dialog and the setting in the names preferences.
   */
  public ScientificOrCommon getScientificOrCommonFromConfiguration(ChecklistPrintPreferences prefs) {
    ScientificOrCommon scientificOrCommon = namesPreferences.scientificOrCommon;
    if (prefs.includeScientific) {
      if (scientificOrCommon == ScientificOrCommon.COMMON_ONLY) {
        // Can't have COMMON_ONLY if the user asks for scientific names;  use COMMON_FIRST
        return ScientificOrCommon.COMMON_FIRST;
      }
    } else {
      switch (scientificOrCommon) {
        case SCIENTIFIC_ONLY:
        case COMMON_FIRST:
        case SCIENTIFIC_FIRST:
          // None of the above make sense if scientific names are deselected;  switch to COMMON_ONLY mode.
          return ScientificOrCommon.COMMON_ONLY;
        default:
          break;
      }
    }
    
    return scientificOrCommon;
  }
}
