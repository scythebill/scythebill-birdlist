/*   
 * Copyright 2024 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.location;

import com.scythebill.birdlist.model.annotations.Preference;

/**
 * Saved preferences for how reports should be printed.
 */
public class ChecklistPrintPreferences {
  @Preference boolean includeScientific = true;
  @Preference boolean showFamilies = true;
  @Preference boolean showStatus = true;
  @Preference boolean showLifersInBold = true;
  @Preference boolean compactPrinting = false;
  @Preference int daysToPrint = 7;
}
