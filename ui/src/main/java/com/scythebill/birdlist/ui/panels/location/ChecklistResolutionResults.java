/*   
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.location;

import java.awt.Dimension;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;

import com.google.common.collect.TreeMultimap;
import com.scythebill.birdlist.model.checklist.ChecklistResolution;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.ResolvedComparator;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Summarizes the results of running ChecklistResolution.
 */
public class ChecklistResolutionResults {
  private ChecklistResolutionResults() {}

  public static JComponent asPanel(
      ChecklistResolution checklistResolution,
      Taxonomy taxonomy,
      FontManager fontManager) {
    ResolvedComparator resolvedComparator = new ResolvedComparator();
    TreeMultimap<Resolved, Resolved> resolutions = TreeMultimap.create(resolvedComparator, resolvedComparator);
    checklistResolution.taxaMappings().forEach((k, v) -> resolutions.put(k.resolve(taxonomy), v.resolve(taxonomy)));
    
    StringBuilder resultsString = new StringBuilder("<ul>");
    Set<String> encounteredGenera = new LinkedHashSet<>();
    for (Resolved from : resolutions.keySet()) {
      // Skip over anything that wasn't simplified
      Set<Resolved> toSet = resolutions.get(from);
      if (toSet.size() == 1 && toSet.contains(from)) {
        continue;
      }
      
      String fromString = speciesAsString(from, encounteredGenera);
      String toString = Messages.join(resolutions.get(from),
          r -> speciesAsString(r, encounteredGenera));
      resultsString.append("<li>");
      resultsString.append(Messages.getFormattedMessage(
          Name.WILL_BECOME_FORMAT, fromString, toString));
    }
    
    JEditorPane resultsText = new JEditorPane("text/html", resultsString.toString());
    JScrollPane scrollPane = new JScrollPane(resultsText);
    resultsText.setEditable(false);
    resultsText.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, true);
    resultsText.setCaretPosition(0);
    scrollPane.setPreferredSize(fontManager.scale(new Dimension(540, 300)));
    scrollPane.setAlignmentX(0.0f);

    Box panel = Box.createVerticalBox();
    panel.add(Box.createVerticalStrut(15));
    panel.add(scrollPane);
    return panel;
  }

  private static String speciesAsString(Resolved species, Set<String> encounteredGenera) {
    String sci = species.getFullName();
    String currentGenus = sci.substring(0, sci.indexOf(' '));
    if (!encounteredGenera.add(currentGenus)) {
      sci = currentGenus.charAt(0) + ". " + sci.substring(sci.indexOf(' ') + 1);
    }
    return String.format("<b>%s</b> (<i>%s</i>)", species.getCommonName(), sci);
  }

}
