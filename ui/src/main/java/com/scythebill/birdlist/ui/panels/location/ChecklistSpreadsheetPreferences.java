/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.location;

import com.scythebill.birdlist.model.annotations.Preference;
import com.scythebill.birdlist.ui.io.ChecklistXlsOutput.ChecklistSightingOption;


/**
 * Saved preferences for how checklists should be output as spreadsheets.
 */
public class ChecklistSpreadsheetPreferences {
  @Preference boolean includeScientific = true;
  @Preference public boolean includeSighting = false;
  @Preference public ChecklistSightingOption checklistSightingOption = ChecklistSightingOption.FIRST_IN_LOCATION;
  @Preference boolean showLifers = true;
  @Preference boolean showNewForColumn = false;
  @Preference boolean showStatus = true;
  @Preference boolean showFamilies = true;
  @Preference int daysToPrint = 7;
  @Preference public boolean showAllTaxonomies = false;
}
