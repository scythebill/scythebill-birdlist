/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.location;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.text.NumberFormatter;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.ui.components.AutoSelectJFormattedTextField;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.io.ChecklistXlsOutput.ChecklistSightingOption;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * Dialog for configuring the checklist spreadsheet options.
 */
class ChecklistXlsDialog {
  private Alerts alerts;
  private ChecklistSpreadsheetPreferences checklistSpreadsheetPreferences;
  private FontManager fontManager;

  @Inject
  ChecklistXlsDialog(
      Alerts alerts,
      ChecklistSpreadsheetPreferences checklistSpreadsheetPreferences,
      FontManager fontManager) {
    this.alerts = alerts;
    this.checklistSpreadsheetPreferences = checklistSpreadsheetPreferences;
    this.fontManager = fontManager;
  }
  
  class SpreadsheetConfiguration {
    boolean includeScientific;
    boolean includeSighting;
    boolean showLifersInBold;
    boolean showNewForColumn;
    boolean showStatus;
    boolean showFamilies;
    int daysToInclude;
    boolean showAllTaxonomies;
    ChecklistSightingOption checklistSightingOption;
  }
  
  @Nullable
  public SpreadsheetConfiguration getConfiguration(
      JComponent parent,
      boolean otherTaxonomiesPossible, List<Location> selectedLocations) {
    ChecklistConfigurationPanel panel = new ChecklistConfigurationPanel(otherTaxonomiesPossible, selectedLocations);
    fontManager.applyTo(panel);
    
    String formattedMessage = alerts.getFormattedDialogMessage(
        Name.SPREADSHEET_OPTIONS, Name.CHOOSE_OPTIONS_FOR_CHECKLIST);
    
    int okCancel = alerts.showOkCancelWithPanel(parent, formattedMessage, panel);
    if (okCancel != JOptionPane.OK_OPTION) {
      return null;
    }
    
    SpreadsheetConfiguration configuration = panel.toConfiguration();
    checklistSpreadsheetPreferences.includeScientific = configuration.includeScientific;
    checklistSpreadsheetPreferences.includeSighting = configuration.includeSighting;
    checklistSpreadsheetPreferences.checklistSightingOption = configuration.checklistSightingOption;
    checklistSpreadsheetPreferences.showLifers = configuration.showLifersInBold;
    checklistSpreadsheetPreferences.showNewForColumn = configuration.showNewForColumn;
    checklistSpreadsheetPreferences.showStatus = configuration.showStatus;
    checklistSpreadsheetPreferences.showAllTaxonomies = configuration.showAllTaxonomies;
    checklistSpreadsheetPreferences.showFamilies = configuration.showFamilies;
    checklistSpreadsheetPreferences.daysToPrint = configuration.daysToInclude;
    return configuration;
  }
  
  class ChecklistConfigurationPanel extends JPanel {
    private JCheckBox includeScientific;
    private JCheckBox includeSighting;
    private JCheckBox showLifersInBold;
    private AutoSelectJFormattedTextField daysToInclude;
    private JCheckBox showStatus;
    private JCheckBox showFamilies;
    private JCheckBox showNewForColumn;
    private JCheckBox showAllTaxonomies;
    private JComboBox<Object> checklistSightingOptionCombobox;

    ChecklistConfigurationPanel(boolean otherTaxonomiesPossible, List<Location> selectedLocations) {
      GroupLayout layout = new GroupLayout(this);
      setLayout(layout);
      
      includeScientific = new JCheckBox(
          Messages.getMessage(Name.SCIENTIFIC_NAME_QUESTION));
      includeScientific.setSelected(checklistSpreadsheetPreferences.includeScientific);
      
      showFamilies = new JCheckBox(Messages.getMessage(Name.SHOW_FAMILIES_QUESTION));
      showFamilies.setSelected(checklistSpreadsheetPreferences.showFamilies);

      showLifersInBold = new JCheckBox(Messages.getMessage(Name.LIFERS_IN_BOLD_QUESTION));
      showLifersInBold.setSelected(checklistSpreadsheetPreferences.showLifers);

      showNewForColumn = new JCheckBox(Messages.getMessage(Name.NEW_FOR_COLUMN_QUESTION));
      showNewForColumn.setSelected(checklistSpreadsheetPreferences.showNewForColumn);

      showStatus = new JCheckBox(Messages.getMessage(Name.SHOW_THREATENED_STATUS_QUESTION));
      showStatus.setSelected(checklistSpreadsheetPreferences.showStatus);

      showAllTaxonomies = new JCheckBox(
          Messages.getMessage(Name.INCLUDE_ALL_TAXONOMIES_QUESTION));
      showAllTaxonomies.setSelected(checklistSpreadsheetPreferences.showAllTaxonomies);
      showAllTaxonomies.setEnabled(otherTaxonomiesPossible);

      includeSighting = new JCheckBox(Messages.getMessage(Name.INCLUDE_ONE_SIGHTING_QUESTION));
      includeSighting.setSelected(checklistSpreadsheetPreferences.includeSighting);

      JLabel checklistSightingOptionsLabel = new JLabel(
          Messages.getMessage(Name.WHICH_SIGHTING_QUESTION));
      List<String> includeSightingTextOptions = new ArrayList<>();
      for (ChecklistSightingOption checklistSightingOption : ChecklistSightingOption.values()) {
        switch (checklistSightingOption) {
          case FIRST_IN_LOCATION:
            if (selectedLocations.size() == 1) {
              includeSightingTextOptions.add(
                  Messages.getFormattedMessage(Name.FIRST_IN_FORMAT, selectedLocations.get(0).getDisplayName()));
            } else {
              includeSightingTextOptions.add(
                  Messages.getMessage(Name.FIRST_IN_CHECKLIST_REGION));
            }
            break;
          case MOST_RECENT_IN_LOCATION:
            if (selectedLocations.size() == 1) {
              includeSightingTextOptions.add(
                  Messages.getFormattedMessage(Name.MOST_RECENT_IN_FORMAT, selectedLocations.get(0).getDisplayName()));
            } else {
              includeSightingTextOptions.add(
                  Messages.getMessage(Name.MOST_RECENT_IN_CHECKLIST_REGION));
            }
            break;
          case FIRST_IN_WORLD:
            includeSightingTextOptions.add(Messages.getMessage(Name.LIFER_SIGHTING));
            break;
          case MOST_RECENT_IN_WORLD:
            includeSightingTextOptions.add(Messages.getMessage(Name.MOST_RECENT_ANYWHERE));
            break;
          default:
            throw new AssertionError("Unknown option: " + checklistSightingOption);
        }
      }
      checklistSightingOptionCombobox = new JComboBox<>(includeSightingTextOptions.toArray());
      checklistSightingOptionCombobox.setSelectedIndex(
          Arrays.asList(ChecklistSightingOption.values())
              .indexOf(checklistSpreadsheetPreferences.checklistSightingOption));
      
      checklistSightingOptionsLabel.setEnabled(includeSighting.isSelected());
      checklistSightingOptionCombobox.setEnabled(includeSighting.isSelected());
      includeSighting.addActionListener(e -> {
        checklistSightingOptionsLabel.setEnabled(includeSighting.isSelected());
        checklistSightingOptionCombobox.setEnabled(includeSighting.isSelected());
      });

      NumberFormatter numberFormatter = new NumberFormatter();
      numberFormatter.setValueClass(Integer.class);
      numberFormatter.setMinimum(0);
      numberFormatter.setMaximum(60);

      daysToInclude = new AutoSelectJFormattedTextField(numberFormatter);
      daysToInclude.setColumns(3);
      daysToInclude.setValue(checklistSpreadsheetPreferences.daysToPrint);
      JLabel daysToIncludeLabel = new JLabel(Messages.getMessage(Name.NUMBERS_OF_COLUMNS_FOR_RECORDS));
      
      layout.setHorizontalGroup(layout.createParallelGroup()
          .addComponent(includeScientific)
          .addComponent(showFamilies)
          .addComponent(showLifersInBold)
          .addComponent(showNewForColumn)
          .addComponent(showStatus)
          .addComponent(showAllTaxonomies)
          .addComponent(includeSighting)
          .addComponent(checklistSightingOptionsLabel)
          .addComponent(checklistSightingOptionCombobox)
          .addComponent(daysToIncludeLabel)
          .addComponent(daysToInclude,
              GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE));
      
      layout.setVerticalGroup(layout.createSequentialGroup()
          .addComponent(includeScientific)
          .addComponent(showFamilies)
          .addComponent(showLifersInBold)
          .addComponent(showNewForColumn)
          .addComponent(showStatus)
          .addComponent(showAllTaxonomies)
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addComponent(includeSighting)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addComponent(checklistSightingOptionsLabel)
          .addComponent(checklistSightingOptionCombobox)
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addComponent(daysToIncludeLabel)
          .addComponent(daysToInclude));
    }
    
    SpreadsheetConfiguration toConfiguration() {
      SpreadsheetConfiguration config = new SpreadsheetConfiguration();
      config.includeScientific = includeScientific.isSelected();
      config.showLifersInBold = showLifersInBold.isSelected();
      config.showNewForColumn = showNewForColumn.isSelected();
      config.showFamilies = showFamilies.isSelected();
      config.showStatus = showStatus.isSelected();
      config.showAllTaxonomies = showAllTaxonomies.isSelected();
      config.includeSighting = includeSighting.isSelected();
      config.checklistSightingOption = ChecklistSightingOption.values()[checklistSightingOptionCombobox.getSelectedIndex()];
      config.daysToInclude = (Integer) daysToInclude.getValue();
      return config;
    }
  }
}
