/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.location;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Set;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.google.common.base.Functions;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.eventbus.Subscribe;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.checklist.ClementsChecklist;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.events.TaxonomyChangedEvent;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.location.LocationBrowsePanel.CreateChecklistPresenter;
import com.scythebill.birdlist.ui.util.FindSpeciesInText;
import com.scythebill.birdlist.ui.util.FindSpeciesInText.Results;
import com.scythebill.birdlist.ui.util.FindSpeciesInText.TaxonSearch;

/**
 * Panel for creating checklists just from entered text. 
 */
class CreateChecklistFromTextPanel extends JPanel implements FontsUpdatedListener {
  private final ReportSet reportSet;
  private final Location location;
  private final TaxonomyStore taxonomyStore;
  private final CreateChecklistPresenter createChecklistPresenter;
  private final Set<SightingTaxon> foundSet = Sets.newHashSet();
  private JScrollPane scrollPane;
  private JLabel helpText;
  private JLabel moreHelpText;
  private JButton okButton;
  private JButton cancelButton;
  private JSeparator separator;
  private JTextArea textArea;
  private JButton parse;
  private FindSpeciesInText findSpeciesInText;
  private JLabel resultsText;
  private boolean useSecondarySearch;
  private Checklist checklist;

  public CreateChecklistFromTextPanel(
      ReportSet reportSet,
      Checklists checklists,
      Location location,
      TaxonomyStore taxonomyStore,
      EventBusRegistrar eventBusRegistrar,
      FontManager fontManager,
      CreateChecklistPresenter createChecklistPresenter) {
    this.reportSet = Preconditions.checkNotNull(reportSet);
    this.location = Preconditions.checkNotNull(location);
    this.taxonomyStore = Preconditions.checkNotNull(taxonomyStore);
    this.createChecklistPresenter = Preconditions.checkNotNull(createChecklistPresenter);
    
    // Find the checklist applicable for a parent location.  (Don't look at the current location;
    // it's necessarily irrelevant.)
    Location checklistLocation = location.getParent();
    while (checklistLocation != null) {
      if (checklists.hasChecklist(taxonomyStore.getTaxonomy(), reportSet, checklistLocation)) {
        checklist = checklists.getChecklist(reportSet, taxonomyStore.getTaxonomy(), checklistLocation);
        break;
      }
      checklistLocation = checklistLocation.getParent();
    }
    
    
    eventBusRegistrar.registerWhenInHierarchy(this);
    initGUI();
    fontManager.applyTo(this);
    
    taxonomyChanged(null);
  }
  
  private void initGUI() {
    helpText = new JLabel(Messages.getMessage(Name.CLICK_START_HELP));
    moreHelpText = new JLabel("");
    resultsText = new JLabel("");
    textArea = new JTextArea(20, 60);
    scrollPane = new JScrollPane(textArea,
        JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
        JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    parse = new JButton(Messages.getMessage(Name.START_BUTTON));
    parse.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent event) {
        TaxonSearch search = useSecondarySearch 
            ? findSpeciesInText.secondarySearch(/*onlySingletonMatches=*/true)
            : findSpeciesInText.primarySearch();
        Results results = findSpeciesInText.search(textArea.getText(), search);
        int previousSize = foundSet.size();
        
        // Resolve all the new results back to the base taxonomy
        Taxonomy taxonomy = taxonomyStore.getTaxonomy();
        for (Resolved resolved : results.found) {
          SightingTaxon sightingTaxon = resolved.getSightingTaxon();
          if (taxonomy instanceof MappedTaxonomy) {
            sightingTaxon = ((MappedTaxonomy) taxonomy).getMapping(sightingTaxon);
            if (sightingTaxon != null) {
              foundSet.add(sightingTaxon);
            }
          } else {
            foundSet.add(sightingTaxon);
          }
        }

        int foundCount = results.found.size();
        int newFound = foundSet.size() - previousSize;
        if (foundCount == 0) {
          if (foundSet.isEmpty()) {
            resultsText.setText(Messages.getMessage(Name.NOTHING_FOUND));
          } else {
            resultsText.setText(
                Messages.getFormattedMessage(
                    Name.NOTHING_NEW_STILL_FORMAT, foundSet.size()));
          }
        } else if (foundCount == newFound) {
          if (previousSize == 0) {
            resultsText.setText(
                Messages.getFormattedMessage(
                    Name.FOUND_SPECIES_CLICK_REMAINING_TEXT, foundCount));
          } else {
            resultsText.setText(
                Messages.getFormattedMessage(
                    Name.FOUND_SPECIES_NOW_TOTAL, foundCount, foundSet.size()));
          }
        } else {
          resultsText.setText(
              Messages.getFormattedMessage(
                  Name.FOUND_EVEN_MORE_SPECIES, foundCount, newFound, foundSet.size()));
        }
        textArea.setText(results.remainingText);
        useSecondarySearch = true;
        updateParseButton();
        
        if (!foundSet.isEmpty()) {
          okButton.setEnabled(true);
        }
      }
    });
    parse.setEnabled(false);
    
    okButton = new JButton(Messages.getMessage(Name.CREATE_A_CHECKLIST));
    okButton.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent event) {
        Map<SightingTaxon, Status> map;
        if (checklist == null) {
          map = Maps.asMap(foundSet, Functions.constant(Status.NATIVE));
        } else {
          map = Maps.newHashMap();
          for (SightingTaxon taxon : foundSet) {
            SightingTaxon resolvedTaxon = taxon.resolve(taxonomyStore.getTaxonomy()).getSightingTaxon();
            Status status = checklist.getStatus(taxonomyStore.getTaxonomy(), resolvedTaxon);
            // By default, species are "native".  But don't copy "endemic" status down to
            // a custom checklist.
            if (status == null || status == Status.ENDEMIC) {
              // TODO: what if we found a subspecies?
              status = Status.NATIVE;
            }
            
            map.put(taxon, status);
          }
        }
        
        reportSet.setChecklist(location, new ClementsChecklist(map));
        reportSet.markDirty();
        createChecklistPresenter.created();
      }
    });
    okButton.setEnabled(false);
    
    cancelButton = new JButton(Messages.getMessage(Name.CANCEL_BUTTON));
    cancelButton.addActionListener(e -> createChecklistPresenter.cancel());

    separator = new JSeparator();
    
    textArea.getDocument().addDocumentListener(new DocumentListener() {
      @Override
      public void removeUpdate(DocumentEvent e) {
        updateParseButton();
      }
      
      @Override
      public void insertUpdate(DocumentEvent e) {
        useSecondarySearch = false;
        updateParseButton();
      }
      
      @Override
      public void changedUpdate(DocumentEvent e) {
        useSecondarySearch = false;
        updateParseButton();
      }      
    });
  }

  private void updateParseButton() {
    parse.setEnabled(textArea.getDocument().getLength() > 0);
    parse.setText(useSecondarySearch
        ? Messages.getMessage(Name.TRY_HARDER)
        : foundSet.isEmpty()
            ? Messages.getMessage(Name.START_BUTTON)
            : Messages.getMessage(Name.ADD_MORE));
                
    helpText.setText(useSecondarySearch
        ? Messages.getMessage(Name.CLICK_TRY_HARDER_HELP)
        : Messages.getMessage(Name.CLICK_START_HELP));
    moreHelpText.setText(useSecondarySearch
        ? Messages.getMessage(Name.OR_ENTER_MORE_TEXT): "");
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    setBorder(new EmptyBorder(
        fontManager.scale(20),
        fontManager.scale(20),
        fontManager.scale(15),
        fontManager.scale(20)));
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup()
        .addGroup(Alignment.LEADING, layout.createSequentialGroup()
            .addComponent(helpText))
        .addGroup(Alignment.TRAILING, layout.createSequentialGroup()
            .addComponent(parse))
        .addComponent(moreHelpText)
        .addComponent(resultsText)
        .addComponent(scrollPane)
        .addComponent(separator)
        .addGroup(Alignment.LEADING, layout.createSequentialGroup()
            .addComponent(cancelButton)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(okButton)));
    layout.setVerticalGroup(layout.createSequentialGroup()
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(helpText)
            .addComponent(parse))
        .addComponent(moreHelpText)
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(resultsText)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(scrollPane)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addGroup(layout.createParallelGroup()
            .addComponent(cancelButton)
            .addComponent(okButton)));
    layout.linkSize(cancelButton, okButton);
  }

  @Subscribe
  public void taxonomyChanged(TaxonomyChangedEvent event) {
    findSpeciesInText = new FindSpeciesInText(taxonomyStore.getTaxonomy(), checklist);
    useSecondarySearch = false;
    updateParseButton();
  }
}
