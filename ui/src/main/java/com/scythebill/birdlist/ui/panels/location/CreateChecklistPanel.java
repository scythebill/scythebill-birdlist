/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.location;

import java.util.Map;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import com.google.common.base.Predicate;
import com.google.common.collect.Maps;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.checklist.ClementsChecklist;
import com.scythebill.birdlist.model.query.QueryProcessor;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.query.SightingPredicates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.location.LocationBrowsePanel.CreateChecklistPresenter;

/** 
 * Panel for starting the "create a checklist" UI.
 */
class CreateChecklistPanel extends JPanel implements FontsUpdatedListener {
  private final ReportSet reportSet;
  private final Checklists checklists;
  private final Location location;
  private final TaxonomyStore taxonomyStore;
  private JRadioButton yourSightings;
  private JLabel helpText;
  private JRadioButton copyAndPasteMagic;
  private JButton okButton;
  private JButton cancelButton;
  private JSeparator separator;
  private CreateChecklistPresenter createChecklistPresenter;
  private JLabel yourSightingsText;
  private JLabel copyAndPasteText;
  private EventBusRegistrar eventBusRegistrar;
  private FontManager fontManager;

  public CreateChecklistPanel(
      ReportSet reportSet,
      Checklists checklists,
      Location location,
      TaxonomyStore taxonomyStore,
      EventBusRegistrar eventBusRegistrar,
      FontManager fontManager) {
    this.reportSet = reportSet;
    this.checklists = checklists;
    this.location = location;
    this.taxonomyStore = taxonomyStore;
    this.eventBusRegistrar = eventBusRegistrar;
    this.fontManager = fontManager;
    
    eventBusRegistrar.registerWhenInHierarchy(this);
    initGUI();
    fontManager.applyTo(this);
  }
  
  private void initGUI() {
    helpText = new JLabel(
        Messages.getFormattedMessage(Name.HOW_TO_GET_STARTED_WITH_CHECKLIST, location.getDisplayName()));
    yourSightings = new JRadioButton(Messages.getMessage(Name.START_WITH_YOUR_SIGHTINGS), true);
    yourSightingsText = new JLabel(
        Messages.getFormattedMessage(Name.START_WITH_YOUR_SIGHTINGS_HELP, location.getDisplayName()));
    yourSightingsText.putClientProperty(FontManager.PLAIN_LABEL, true);
    copyAndPasteMagic = new JRadioButton(Messages.getMessage(Name.COPY_AND_PASTE_MAGIC), true);
    copyAndPasteText = new JLabel(Messages.getMessage(Name.COPY_AND_PASTE_MAGIC_HELP));
    copyAndPasteText.putClientProperty(FontManager.PLAIN_LABEL, true);
    
    ButtonGroup group = new ButtonGroup();
    group.add(yourSightings);
    group.add(copyAndPasteMagic);
    
    okButton = new JButton(Messages.getMessage(Name.OK_BUTTON));
    okButton.addActionListener(e -> {
      if (yourSightings.isSelected()) {
        yourSightings();
      } else {
        copyAndPasteMagic();
      }
    });
    
    cancelButton = new JButton(Messages.getMessage(Name.CANCEL_BUTTON));
    cancelButton.addActionListener(e -> createChecklistPresenter.cancel());

    separator = new JSeparator();
  }

  private void copyAndPasteMagic() {
    CreateChecklistFromTextPanel panel = new CreateChecklistFromTextPanel(
        reportSet, checklists, location, taxonomyStore, eventBusRegistrar, fontManager, createChecklistPresenter);
    createChecklistPresenter.switchTo(panel);
  }

  private void yourSightings() {
    // Intentionally *don't* care about DefaultUserStore;  grab all the sightings.
    QueryProcessor processor = new QueryProcessor(
        reportSet, taxonomyStore.getClements());
    Predicate<Sighting> predicate = SightingPredicates.in(location, reportSet.getLocations());
    QueryResults queryResults = processor.runQuery(predicate, null, Taxon.Type.subspecies);
    Map<SightingTaxon, Checklist.Status> checklistMap = Maps.newHashMap();
    for (Resolved resolved : queryResults.getTaxaAsList()) {
      if (resolved.getSmallestTaxonType() == Taxon.Type.family) {
        continue;
      }
      Checklist.Status status = null;
      for (Sighting sighting : queryResults.getAllSightings(resolved)) {
        SightingStatus sightingStatus = sighting.getSightingInfo().getSightingStatus();
        switch (sightingStatus) {
          case ID_UNCERTAIN:
          case RECORD_NOT_ACCEPTED:
          case DOMESTIC:
          // people can band non-natives, ditto for others! not obvious what to do, so
          // hope there's another sighting that clarifies. 
          case RESTRAINED: 
          case UNSATISFACTORY_VIEWS:
          case DEAD:
          case NOT_BY_ME:
          case SIGNS:
            // Ignore all these entries
            break;
          case BETTER_VIEW_DESIRED:
          case NONE:
            if (status == null) {
              status = Checklist.Status.NATIVE;
            }
            break;
          case INTRODUCED:
            status = Checklist.Status.INTRODUCED;
            break;
          case INTRODUCED_NOT_ESTABLISHED:
            status = Checklist.Status.ESCAPED;
            break;
        }

        if (status != null) {
          break;
        }
      }
      
      if (status != null) {
        checklistMap.put(resolved.getParentOfAtLeastType(Taxon.Type.species), status);
      }
    }
    
    reportSet.setChecklist(location, new ClementsChecklist(checklistMap));
    reportSet.markDirty();
    createChecklistPresenter.created();
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    setBorder(new EmptyBorder(
        fontManager.scale(20),
        fontManager.scale(20),
        fontManager.scale(15),
        fontManager.scale(20)));
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup()
        .addComponent(helpText)
        .addComponent(yourSightings)
        .addComponent(yourSightingsText)
        .addComponent(copyAndPasteMagic)
        .addComponent(copyAndPasteText)
        .addComponent(separator)
        .addGroup(Alignment.LEADING, layout.createSequentialGroup()
            .addComponent(cancelButton)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(okButton)));
    layout.setVerticalGroup(layout.createSequentialGroup()
        .addComponent(helpText)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(yourSightings)
        .addComponent(yourSightingsText)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(copyAndPasteMagic)
        .addComponent(copyAndPasteText)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addGroup(layout.createParallelGroup()
            .addComponent(cancelButton)
            .addComponent(okButton)));
    layout.linkSize(cancelButton, okButton);
  }

  public void setPresenter(CreateChecklistPresenter createChecklistPresenter) {
    this.createChecklistPresenter = createChecklistPresenter;
  }  
}
