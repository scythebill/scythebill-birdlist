/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.location;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.EventObject;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.AbstractCellEditor;
import javax.swing.Box;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkEvent.EventType;
import javax.swing.event.HyperlinkListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.eventbus.Subscribe;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.checklist.ClementsChecklist;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Type;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.ResolvedComparator;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.components.SpeciesIndexerPanelConfigurer;
import com.scythebill.birdlist.ui.components.table.DeleteColumn;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.events.TaxonomyChangedEvent;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.ShowSpeciesMap;
import com.scythebill.birdlist.ui.panels.SpeciesInfoDescriber;
import com.scythebill.birdlist.ui.panels.location.LocationBrowsePanel.EditChecklistPresenter;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.LocationScorer;
import com.scythebill.birdlist.ui.util.Scorer;
import com.scythebill.birdlist.ui.util.UIUtils;
import com.scythebill.birdlist.ui.util.UiFutures;

/** 
 * Panel for the "edit a checklist" UI.
 */
class EditChecklistPanel extends JPanel implements FontsUpdatedListener {
  private final ReportSet reportSet;
  private final Location location;
  private final Checklists checklists;
  private JButton okButton;
  private JButton deleteButton;
  private JButton cancelButton;
  private JSeparator separator;
  private JTable table;
  private JScrollPane tableScrollPane;
  private JTableHeader columnHeader;
  private EditChecklistPresenter presenter;
  private Map<SightingTaxon, Status> checklistMap;
  private List<String[]> builtInEdits;
  private FontManager fontManager;
  private IndexerPanel<String> indexer;
  private JButton add;
  private JEditorPane speciesInfoText;
  private JScrollPane speciesInfoScrollPane;
  private Checklist parentChecklist;
  private ListeningExecutorService executorService;
  private SpeciesInfoDescriber speciesInfoDescriber;
  private Location parentChecklistLocation;
  private Alerts alerts;
  private Checklist checklist;
  private TaxonomyStore taxonomyStore;
  private JLabel count;
  private final SpeciesIndexerPanelConfigurer speciesIndexerPanelConfigurer;
  private final ShowSpeciesMap showSpeciesMap;

  private final static Logger logger = Logger.getLogger(EditChecklistPanel.class.getName());

  public EditChecklistPanel(
      ReportSet reportSet,
      Checklists checklists,
      Checklist checklist,
      Location location,
      TaxonomyStore taxonomyStore,
      EventBusRegistrar eventBusRegistrar,
      FontManager fontManager,
      ListeningExecutorService executorService,
      SpeciesInfoDescriber speciesInfoDescriber,
      SpeciesIndexerPanelConfigurer speciesIndexerPanelConfigurer,
      ShowSpeciesMap showSpeciesMap,
      Alerts alerts) {
    this.reportSet = reportSet;
    this.checklists = checklists;
    this.checklist = checklist;
    this.taxonomyStore = taxonomyStore;
    this.fontManager = fontManager;
    this.executorService = executorService;
    this.speciesInfoDescriber = speciesInfoDescriber;
    this.speciesIndexerPanelConfigurer = speciesIndexerPanelConfigurer;
    this.showSpeciesMap = showSpeciesMap;
    this.alerts = alerts;
    this.location = location;
    
    Taxonomy taxonomy;
    if (checklist.isBuiltIn()) {
      // Built-in taxonomys are always edited via IOC 
      taxonomy = taxonomyStore.getIoc();
      this.checklistMap = Maps.newHashMap();
      for (SightingTaxon taxon : checklist.getTaxa(taxonomy)) {
        checklistMap.put(taxon, checklist.getStatus(taxonomy, taxon));
      }
      builtInEdits = Lists.newArrayList();
    } else {
      taxonomy = taxonomyStore.getTaxonomy();
      this.checklistMap = Maps.newHashMap(((ClementsChecklist) checklist).baseChecklistMap());
      eventBusRegistrar.registerWhenInHierarchy(this);
    }
    
    // Find the checklist applicable for a parent location.  (Don't look at the current location;
    // it's necessarily irrelevant.)
    Location checklistLocation = location.getParent();
    while (checklistLocation != null) {
      if (checklists.hasChecklist(taxonomyStore.getTaxonomy(), reportSet, checklistLocation)) {
        parentChecklist = checklists.getChecklist(reportSet, taxonomyStore.getTaxonomy(), checklistLocation);
        parentChecklistLocation = checklistLocation;
        break;
      }
      checklistLocation = checklistLocation.getParent();
    }

    initGUI();
    setTaxonomy(taxonomy);
    fontManager.applyTo(this);    
  }

  private final static List<Messages.Name> COLUMN_NAMES = ImmutableList.of(
      Messages.Name.COMMON_COLUMN,
      Messages.Name.SCIENTIFIC_COLUMN,
      Messages.Name.STATUS_COLUMN,
      Messages.Name.DELETE_COLUMN);
  
  /** Single row in the checklist table. */
  static class TableRow implements Comparable<TableRow> {
    static private final ResolvedComparator comparator = new ResolvedComparator();
    final SightingTaxon taxon;
    final Resolved resolved;
    Status status;

    TableRow(SightingTaxon taxon, Resolved resolved, Status status) {
      this.taxon = taxon;
      this.resolved = resolved;
      this.status = status;
    }

    @Override
    public int compareTo(TableRow that) {
      return comparator.compare(this.resolved, that.resolved);
    }
  }

  /** Table model for the checklist. */
  private class ChecklistTableModel extends AbstractTableModel {
    private final List<TableRow> list = Lists.newArrayList();
    
    ChecklistTableModel(Taxonomy taxonomy, Map<SightingTaxon, Status> checklistMap) {
      for (Entry<SightingTaxon, Status> entry : checklistMap.entrySet()) {
        Resolved resolved;
        // For built-in checklists, the sighting taxa are entirely IOC
        if (checklist.isBuiltIn()) {
          resolved = entry.getKey().resolveInternal(taxonomy);
        } else {
          resolved = entry.getKey().resolve(taxonomy);
        }
        list.add(new TableRow(entry.getKey(), resolved, entry.getValue()));
      }
      
      Collections.sort(list);
    }
    
    @Override
    public String getColumnName(int index) {
      return Messages.getMessage(COLUMN_NAMES.get(index));
    }

    @Override
    public int getColumnCount() {
      return COLUMN_NAMES.size();
    }

    @Override
    public int getRowCount() {
      return list.size();
    }

    @Override
    public Object getValueAt(int row, int column) {
      TableRow tableRow = list.get(row);
      switch (column) {
        case 0:
          return tableRow.resolved.getCommonName();
        case 1:
          return tableRow.resolved.getFullName();
        case 2:
          return tableRow.status;
        case 3:
          // DELETE
          return "";
        default:
          throw new AssertionError("Unexpected column " + column);
      }
    }

    @Override
    public void setValueAt(Object o, int row, int column) {
      TableRow tableRow = list.get(row);
      if (column == 2) {
        Checklist.Status status = (Checklist.Status) o;
        Preconditions.checkNotNull(status);
        tableRow.status = status;
        fireTableCellUpdated(row, column);

        if (builtInEdits != null) {
          builtInEdits.add(new String[]{
              "setStatus",
              "ioc",
              tableRow.taxon.getId(),
              Strings.nullToEmpty(tableRow.status.text()),
              tableRow.resolved.getCommonName()});
        }
      } else {
        throw new IllegalArgumentException("Unexpected column: " + column);
      }
    }

    @Override
    public boolean isCellEditable(int row, int col) {
      return col == 2;
    }

    public void removeRow(int row) {
      TableRow tableRow = list.get(row);
      if (builtInEdits != null) {
        builtInEdits.add(new String[]{
           "remove",
           "ioc",
           tableRow.taxon.getId(),
           "",
           tableRow.resolved.getCommonName()
        });
      }
      list.remove(row);
      fireTableRowsDeleted(row, row);
    }

    public Map<SightingTaxon, Status> getChecklistMap() {
      Map<SightingTaxon, Status> map = Maps.newHashMapWithExpectedSize(list.size());
      for (TableRow row : list) {
        map.put(row.taxon, row.status);
      }
      return map;
    }

    public int addResolved(Resolved resolved) {
      // Find the base taxonomy version of SightingTaxon, if needed
      SightingTaxon taxon = resolved.getSightingTaxon();
      if (!checklist.isBuiltIn()) {
        Taxonomy taxonomy = resolved.getTaxonomy();
        if (taxonomy instanceof MappedTaxonomy) {
          taxon = ((MappedTaxonomy) taxonomy).getMapping(taxon);
        }
      }
      
      TableRow row = new TableRow(taxon, resolved, Checklist.Status.NATIVE);
      int index = Collections.binarySearch(list, row);
      if (index >= 0) {
        // Already exists - return that index so it can be selected.
        return index;
      }
      
      if (builtInEdits != null) {
        builtInEdits.add(new String[]{
            "add",
            "ioc",
            resolved.getTaxon().getId(),
            "",
            resolved.getCommonName()
         });
      }

      int insertionIndex = -(index + 1);
      list.add(insertionIndex, row);
      fireTableRowsInserted(insertionIndex, insertionIndex);
      return insertionIndex;
    }
    
    public String getTaxonAtRow(int row) {
      if (row < 0) {
        return null;
      }
      
      Resolved resolved = list.get(row).resolved;
      if (resolved.getType() != Type.SINGLE) {
        return null;
      }
      
      return resolved.getTaxon().getId();
    }
  }
  
  private void initGUI() {
    table = new JTable();
    table.setShowGrid(false);
    table.setIntercellSpacing(new Dimension(0, 0));
    table.setFillsViewportHeight(true);
    table.setDefaultRenderer(Object.class, new CellRenderer());
    tableScrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    columnHeader = new JTableHeader();
    tableScrollPane.setColumnHeaderView(columnHeader);
    
    indexer = new IndexerPanel<String>();
    add = new JButton(Messages.getMessage(Name.ADD_SPECIES));
    add.setEnabled(false);
    
    count = new JLabel();
    
    speciesInfoText = new JEditorPane();
    speciesInfoText.setContentType("text/html");
    speciesInfoText.setBackground(Color.WHITE);
    speciesInfoText.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, true);
    speciesInfoScrollPane = new JScrollPane(speciesInfoText);
    speciesInfoScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
    speciesInfoScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    speciesInfoText.setEditable(false);
    speciesInfoText.addHyperlinkListener(new HyperlinkListener() {
      @Override
      public void hyperlinkUpdate(HyperlinkEvent event) {
        if (event.getEventType() == EventType.ACTIVATED) {
          if (SpeciesInfoDescriber.isShowMapUrl(event.getURL())){
            String id = SpeciesInfoDescriber.getShowMapSpeciesId(event.getURL());
            try {
              showSpeciesMap.showRange(reportSet, taxonomy, id);
            } catch (IOException e) {
              alerts.showError(EditChecklistPanel.this, e, Name.MAP_FAILED, Name.COULDNT_WRITE_MAP);
            }
          } else {
            if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
              try {
                Desktop.getDesktop().browse(event.getURL().toURI());
              } catch (IOException | URISyntaxException e) {
                logger.log(Level.WARNING, "Couldn't open URL " + event.getURL(), e);
              }
            }
          }
        }
      }
    });
    updateSpeciesText();

    indexer.addPropertyChangeListener("selectedValue", e -> updateSpeciesText());

    indexer.addPropertyChangeListener("value", new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent e) {
        add.setEnabled(e.getNewValue() != null);
        updateSpeciesText();
      }
    });
    
    table.getSelectionModel().addListSelectionListener(e -> updateSpeciesText());
    
    // Simulate a click, which will add the species, and provide visual
    // feedback on what the "Add" button does.
    indexer.addActionListener(e -> add.doClick(100));
    
    add.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        String id = indexer.getValue();
        if (id == null) {
          return;
        }
        
        Taxon taxon = taxonomy.getTaxon(id);
        Resolved resolved = SightingTaxons.newResolved(taxon);
        int row = checklistModel().addResolved(resolved);
        table.setRowSelectionInterval(row, row);
        table.scrollRectToVisible(table.getCellRect(row, 0, true));
        add.setEnabled(false);
      }
    });
    
    okButton = new JButton(Messages.getMessage(Name.OK_BUTTON));
    okButton.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent event) {
        if (checklist.isBuiltIn()) {
          StringWriter sw = new StringWriter();
          sw.append("Location: " + Locations.getLocationCode(location) + "\n");
          sw.append("Taxonomy: " + taxonomy.getName() + "\n");
          try {
            ExportLines exportLines = CsvExportLines.fromWriter(sw);
            exportLines.nextLine(new String[]{"Action", "Checklist", "ID", "Status"});
            for (String[] line : builtInEdits) {
              exportLines.nextLine(line);
            }
          } catch (IOException e) {
            // Can't happen
            throw new AssertionError(e);
          }
          
          alerts.showFeedback(Name.CHECKLIST_CORRECTIONS_ARE_READY, sw.toString());
          presenter.done();
        } else {
          // Get the new checklist
          Map<SightingTaxon, Status> updatedChecklistMap = checklistModel().getChecklistMap();
          ClementsChecklist updatedChecklist = new ClementsChecklist(updatedChecklistMap);
          // And save it
          reportSet.setChecklist(location, updatedChecklist);
          reportSet.markDirty();
          presenter.done();
        }
      }
    });
    cancelButton = new JButton(Messages.getMessage(Name.CANCEL_BUTTON));
    cancelButton.addActionListener(e -> presenter.done());

    deleteButton = new JButton(Messages.getMessage(Name.DELETE_CHECKLIST_BUTTON));
    if (checklist.isBuiltIn()) {
      // Built-in checklists cannot be deleted
      deleteButton.setVisible(false);
    } else {
      deleteButton.addActionListener(new ActionListener() {
        @Override public void actionPerformed(ActionEvent event) {
          if (alerts.showOkCancel(EditChecklistPanel.this,
              Name.DELETE_CHECKLIST_TITLE,
              Name.DELETE_CHECKLIST_MESSAGE)
              == JOptionPane.OK_OPTION) {
            reportSet.removeChecklist(location);
            reportSet.markDirty();
            presenter.done();
          }
        }
      });
    }
    separator = new JSeparator();
    
    table.addMouseListener(new MouseAdapter() {

      @Override
      public void mouseClicked(MouseEvent e) {
        int column = table.columnAtPoint(e.getPoint());
        if (e.getClickCount() != 1) {
          return;
        }
        
        if (column != 3) {
          return;
        }
        
        int row = table.rowAtPoint(e.getPoint());
        if (row < 0) {
          return;
        }
        
        ChecklistTableModel checklistModel = checklistModel();
        checklistModel.removeRow(row);
      }
    });
  }
  
  @Override
  public void fontsUpdated(FontManager fontManager) {
    speciesInfoText.setPreferredSize(fontManager.scale(new Dimension(480, 50)));
    speciesInfoText.setMaximumSize(fontManager.scale(new Dimension(Short.MAX_VALUE, 100)));

    setBorder(new EmptyBorder(
        fontManager.scale(20),
        fontManager.scale(20),
        fontManager.scale(15),
        fontManager.scale(20)));
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup()
        .addGroup(Alignment.LEADING, layout.createSequentialGroup()
            .addComponent(indexer)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(add)
            .addPreferredGap(ComponentPlacement.UNRELATED)
            .addComponent(count))
        .addComponent(speciesInfoScrollPane)
        .addComponent(tableScrollPane)
        .addComponent(separator)
        .addGroup(Alignment.LEADING, layout.createSequentialGroup()
            .addComponent(deleteButton))
        .addGroup(Alignment.TRAILING, layout.createSequentialGroup()
            .addComponent(cancelButton)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(okButton)));
    layout.setVerticalGroup(layout.createSequentialGroup()
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(indexer)
            .addComponent(add)
            .addComponent(count))
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(speciesInfoScrollPane, fontManager.scale(50), fontManager.scale(100), fontManager.scale(100))
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(tableScrollPane)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(deleteButton)
            .addComponent(cancelButton)
            .addComponent(okButton)));
    layout.linkSize(cancelButton, okButton);
    table.setSurrendersFocusOnKeystroke(true);
  }
  
  private final static ImmutableBiMap<Integer, Checklist.Status> STATUS_INDICES;
  private Future<?> scorerFuture;
  private Taxonomy taxonomy;
  static {
    ImmutableBiMap.Builder<Integer, Checklist.Status> statusBuilder = ImmutableBiMap.builder();
    statusBuilder.put(0, Checklist.Status.NATIVE);
    statusBuilder.put(1, Checklist.Status.INTRODUCED);
    statusBuilder.put(2, Checklist.Status.ESCAPED);
    statusBuilder.put(3, Checklist.Status.RARITY);
    statusBuilder.put(4, Checklist.Status.RARITY_FROM_INTRODUCED);
    statusBuilder.put(5, Checklist.Status.ENDEMIC);
    statusBuilder.put(6, Checklist.Status.EXTINCT);
    STATUS_INDICES = statusBuilder.build();
  }
  
  private JComboBox<String> statusComboBox() {
    return new JComboBox<>(new String[]{
        "",
        Messages.getMessage(Name.CHECKLIST_STATUS_INTRODUCED),
        Messages.getMessage(Name.CHECKLIST_STATUS_NOT_ESTABLISHED),
        Messages.getMessage(Name.CHECKLIST_STATUS_RARITY),
        Messages.getMessage(Name.CHECKLIST_STATUS_RARITY_FROM_INTRODUCED),
        Messages.getMessage(Name.CHECKLIST_STATUS_ENDEMIC),
        Messages.getMessage(Name.CHECKLIST_STATUS_EXTINCT)
    });
  }
  
  private Box horizontalBox(JComboBox<?> comboBox) {
    Box box = Box.createHorizontalBox();
    box.add(comboBox);
    box.add(Box.createHorizontalGlue());
    box.setOpaque(true);
    return box;
  }

  public void setPresenter(EditChecklistPresenter editChecklistPresenter) {
    this.presenter = editChecklistPresenter;
  }
  
  @Subscribe
  public void taxonomyChanged(TaxonomyChangedEvent event) {
    // Grab the current data out of the model (since the model will get re-created)
    checklistMap = checklistModel().getChecklistMap();
    
    setTaxonomy(event.getTaxonomy());
  }


  public ChecklistTableModel checklistModel() {
    ChecklistTableModel model = (ChecklistTableModel) table.getModel();
    return model;
  }
  
  private void setTaxonomy(Taxonomy taxonomy) {
    this.taxonomy = taxonomy;
    table.setModel(new ChecklistTableModel(taxonomy, checklistMap));
    table.getModel().addTableModelListener(e -> updateCount());
    updateCount();
    // Since the model has been reset, update the columns
    TableColumn deleteColumn = table.getColumnModel().getColumn(3);
    deleteColumn.setMaxWidth(80);
    deleteColumn.setWidth(80);
    deleteColumn.setResizable(false);
    final JLabel deleteLabel = DeleteColumn.createDeleteLabel();
    deleteLabel.setHorizontalAlignment(SwingConstants.CENTER);
    deleteColumn.setCellRenderer(new TableCellRenderer() {
      @Override
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
          boolean hasFocus, int row, int column) {
        setCellAppearance(isSelected, row, deleteLabel, deleteLabel);
        // Need to show the background color, so make it opaque
        deleteLabel.setOpaque(true);
        return deleteLabel;
      }
    });
    
    TableColumn statusColumn = table.getColumnModel().getColumn(2);
    table.setRowHeight(fontManager.scale(20));
    JComboBox<String> editCombo = statusComboBox();
    statusColumn.setCellEditor(new StatusComboEditor(
        horizontalBox(editCombo), editCombo));
    final JComboBox<String> renderCombo = statusComboBox();
    final Box renderBox = horizontalBox(renderCombo);
    statusColumn.setCellRenderer(new TableCellRenderer() {
      @Override
      public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
          boolean hasFocus, int row, int column) {
        setCellAppearance(isSelected, row, renderBox, renderCombo);
        renderCombo.setSelectedIndex(STATUS_INDICES.inverse().get(value));
        return renderBox;
      }
    });
    
    speciesIndexerPanelConfigurer.configure(indexer, taxonomy);
    ReportSet emptyReports = new ReportSet(
        reportSet.getLocations(), ImmutableList.<Sighting>of(), taxonomyStore.getClements());
    Scorer scorer = LocationScorer.newScorer(taxonomy, emptyReports, location.getParent(), null, checklists);
    indexer.setOrdering(scorer::ordering);
    
    if (scorerFuture != null) {
      scorerFuture.cancel(true);
    }
    scorerFuture = scorer.start(executorService);
    UiFutures.cancelFutureOnHide(scorerFuture, this);    
  }

  private void setCellAppearance(boolean isSelected, int row,
      Component component, Component fontComponent) {
    Color color;
    if (isSelected) {
      if (UIUtils.isMacOS() || hasFocus()) {
        color = UIManager.getColor("TextPane.selectionBackground");
      } else {
        color = UIManager.getColor("Table.selectionBackground");
      }
    } else {
      color = UIManager.getColor(row % 2 == 0
          ? "Table.alternateBackground.0"
          : "Table.alternateBackground.1");
    }
    
    if (color == null) {
      color = UIManager.getColor("Table.background");
    }
    component.setBackground(color);
    fontComponent.setFont(fontManager.getTableFont());
    fontComponent.setMaximumSize(fontComponent.getPreferredSize());
  }

  private void updateSpeciesText() {
    StringBuilder builder = new StringBuilder("<html>");

    String id = indexer.getSelectedValue();
    if (id == null) {
      id = indexer.getValue();
    }
    
    if (id == null) {
      if (table.getModel() instanceof ChecklistTableModel) {
        int anchorSelectionIndex = table.getSelectionModel().getAnchorSelectionIndex();
        id = ((ChecklistTableModel) table.getModel()).getTaxonAtRow(anchorSelectionIndex);
      }
    }
    
    if (id == null) {
      builder.append("<span style='color:gray'>")
             .append(Messages.getMessage(Name.SPECIES_INFORMATION_WILL_APPEAR_HERE))
             .append("</span>");
      builder.append("</html>");
      speciesInfoText.setText(builder.toString());
      return;
    }

    Taxon taxon = taxonomy.getTaxon(id);
    if (taxon != null) {
      Resolved resolved = SightingTaxons.newResolved(taxon);
      speciesInfoDescriber.toHtmlText(resolved, reportSet, parentChecklist, parentChecklistLocation,
          builder);
    }
    
    builder.append("</html>");
    speciesInfoText.setText(builder.toString());
    speciesInfoText.select(0, 0);
  }

  private void updateCount() {
    count.setText(Messages.getFormattedMessage(Name.TOTAL_FORMAT, table.getModel().getRowCount()));
  }
  
  /** CellRenderer that overrides the default background and font, and does not show focus. */
  private class CellRenderer extends DefaultTableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
        boolean hasFocus, int row, int column) {
      hasFocus = false;
      Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      
      setCellAppearance(isSelected, row, component, component);
      return component;
    }    
  }

  /**
   * Generic ComboBox editor component.  Supports using a ComboBox that doesn't fill
   * the panel.
   */
  class ComboBoxCellEditor extends AbstractCellEditor implements TableCellEditor {
    private Component component;
    protected JComboBox<?> comboBox;

    public ComboBoxCellEditor(Component component, JComboBox<?> comboBox) {
      this.component = component;
      this.comboBox = comboBox;
      comboBox.addActionListener(e -> stopCellEditing());
    }
    
    @Override
    public Object getCellEditorValue() {
      return comboBox.getSelectedItem();
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected,
        int row, int column) {
      updateComboBox(value);
      setCellAppearance(true, row, component, comboBox);
      return component;
    }

    protected void updateComboBox(Object value) {
      comboBox.setSelectedItem(value);
    }

    @Override
    public boolean isCellEditable(EventObject event) {
      if (event instanceof MouseEvent) {
          return ((MouseEvent)event).getClickCount() >= 1;
      }
      
      return true;
    }

    @Override
    public boolean shouldSelectCell(EventObject event) {
      // Don't select the cell during drag events
      if (event instanceof MouseEvent) {
        MouseEvent mouse = (MouseEvent) event;
        return mouse.getID() != MouseEvent.MOUSE_DRAGGED;
      }
      
      return true;
    }

    @Override
    public boolean stopCellEditing() {
      fireEditingStopped();
      return true;
    }

    @Override
    public void cancelCellEditing() {
      fireEditingCanceled();
    }
  }
  
  class StatusComboEditor extends ComboBoxCellEditor {
    public StatusComboEditor(Component component, JComboBox<String> comboBox) {
      super(component, comboBox);
    }
    
    @Override
    public Object getCellEditorValue() {
      return STATUS_INDICES.get(comboBox.getSelectedIndex());
    }
    
    @Override
    protected void updateComboBox(Object value) {
      comboBox.setSelectedIndex(STATUS_INDICES.inverse().get(value));
    }
  }
}
