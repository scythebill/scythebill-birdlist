/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.location;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import javax.annotation.Nullable;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.CellRendererPane;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.TransferHandler.TransferSupport;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;

import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.checklist.ChecklistSynthesizer;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.checklist.ClementsChecklist;
import com.scythebill.birdlist.model.checklist.ClementsChecklistWriter;
import com.scythebill.birdlist.model.checklist.TransposedChecklistSynthesizer;
import com.scythebill.birdlist.model.io.HtmlResponseWriter;
import com.scythebill.birdlist.model.query.QueryDefinition;
import com.scythebill.birdlist.model.query.QueryProcessor;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.query.SightingPredicates;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.actions.ActionBroker;
import com.scythebill.birdlist.ui.actions.ReturnAction;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.app.Titled;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.components.NewLocationDialog;
import com.scythebill.birdlist.ui.components.NewLocationDialog.LocationReceiver;
import com.scythebill.birdlist.ui.components.SpeciesIndexerPanelConfigurer;
import com.scythebill.birdlist.ui.components.VisitInfoPreferences;
import com.scythebill.birdlist.ui.components.WherePanel;
import com.scythebill.birdlist.ui.components.WherePanelIndexers;
import com.scythebill.birdlist.ui.datatransfer.SightingsGroup;
import com.scythebill.birdlist.ui.events.DefaultUserChangedEvent;
import com.scythebill.birdlist.ui.events.DefaultUserStore;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.events.TaxonomyChangedEvent;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.io.ChecklistHtmlOutput;
import com.scythebill.birdlist.ui.io.ChecklistXlsOutput;
import com.scythebill.birdlist.ui.io.ChecklistXlsOutput.ChecklistSightingOption;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.BrowsePreferences;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.panels.ShowSpeciesMap;
import com.scythebill.birdlist.ui.panels.SpHybridDialog;
import com.scythebill.birdlist.ui.panels.SpeciesInfoDescriber;
import com.scythebill.birdlist.ui.panels.location.ChecklistXlsDialog.SpreadsheetConfiguration;
import com.scythebill.birdlist.ui.panels.location.LocationTree.LocationsEditedListener;
import com.scythebill.birdlist.ui.panels.reports.QueryDialog;
import com.scythebill.birdlist.ui.panels.reports.QueryDialog.QueryDefinitionReceiver;
import com.scythebill.birdlist.ui.panels.reports.QueryExecutor;
import com.scythebill.birdlist.ui.panels.reports.QueryPreferences;
import com.scythebill.birdlist.ui.panels.reports.ReportHints;
import com.scythebill.birdlist.ui.panels.reports.ReportsActionFactory;
import com.scythebill.birdlist.ui.panels.reports.ReportsBrowserPanel;
import com.scythebill.birdlist.ui.panels.reports.ReportsBrowserPanel.VisitInfoKeyChangedListener;
import com.scythebill.birdlist.ui.panels.reports.ReportsTreeModel.VisitInfoKeyNode;
import com.scythebill.birdlist.ui.panels.reports.ReportsTreeModel.VisitNode;
import com.scythebill.birdlist.ui.prefs.ReportSetPreference;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.DesktopUtils;
import com.scythebill.birdlist.ui.util.EncounteredLocations;
import com.scythebill.birdlist.ui.util.FileDialogs;
import com.scythebill.birdlist.ui.util.LocationIdToString;
import com.scythebill.birdlist.ui.util.OpenMapUrl;
import com.scythebill.birdlist.ui.util.ResolvedWithSighting;
import com.scythebill.birdlist.ui.util.ScanSeenTaxa;
import com.scythebill.birdlist.ui.util.UIUtils;
import com.scythebill.birdlist.ui.util.VisibilityDetector;

/**
 * Top-level panel wrapping a LocationTree, etc.
 * 
 * TODO:
 * - Fix ...'ing of tree entries on Linux
 * - Maybe: support Jump To a species in the queried results?
 * - Bug fix: handle reparenting top-level menu items (seemed broken, at least on Linux)
 */
public class LocationBrowsePanel extends JPanel implements Titled, FontsUpdatedListener {

  private static final int SELECTION_QUERY_DELAY_MILLIS = 200;
  private final FontManager fontManager;
  private final ReportSet reportSet;
  private final TaxonomyStore taxonomyStore;
  private final ActionBroker actionBroker;
  private final NewLocationDialog newLocationDialog;
  private final Checklists checklists;
  private final TransposedChecklistSynthesizer transposedChecklistSynthesizer;
  private final ChecklistSynthesizer checklistSynthesizer;
  private final Alerts alerts;
  private final QueryPreferences queryPreferences;
  private final BrowsePreferences browsePreferences;
  private final ReportSetPreference<LocationBrowsePreferences> locationBrowsePreferences;
  private LocationTree locationTree;
  private ReportsBrowserPanel reportsBrowser;
  private JButton returnButton;
  private IndexerPanel<Object> locationIndexPanel;
  private JButton jumpButton;
  private IndexerPanel<String> speciesIndexPanel;
  private JButton speciesJumpButton;
  private JLabel totalLabel;
  private Timer runQueryTimer;
  private LocationEditorPanel locationEditorPanel;
  private JButton deleteLocation;
  private JButton editLocation;
  private JButton newLocation;
  private JScrollPane treeScrollPane;
  private JScrollPane reportsScrollPane;
  private final ImageIcon checkIcon;
  private final ImageIcon latLongIcon;
  private final ListeningExecutorService executorService;
  private QueryResults currentQueryResults;
  private ScanSeenTaxa scanSeenTaxa;
  private boolean customScanner;
  private JComboBox<Object> reportFilterCombo;
  private JCheckBox noRarities;
  private JButton xlsButton;
  private JButton tripReportButton;
  private final FileDialogs fileDialogs;
  private final ChecklistXlsDialog checklistXlsDialog;
  private final ChecklistPrintDialog checklistPrintDialog;
  private final PredefinedLocations predefinedLocations;
  private final EventBusRegistrar eventBusRegistrar;
  private final SpeciesInfoDescriber speciesInfoDescriber;
  private final SpeciesIndexerPanelConfigurer speciesIndexerPanelConfigurer;
  private final ShowSpeciesMap showSpeciesMap;
  private final QueryDialog queryDialog;
  private JButton addSightingButton;
  private JButton removeSightingButton;
  private WherePanelIndexers whereIndexers;
  private JButton editChecklist;
  private JButton shareChecklist;
  private JPanel reportsOrEditChecklist;
  private DefaultUserStore defaultUserStore;
  private JCheckBox onlyVisitedLocations;
  // Set tracking all locations added in a single view (so they're visible even
  // when "only visited" is present)
  private Set<Location> addedLocations = new LinkedHashSet<>();
  private Predicate<Location> locationFilter;
  
  /**
   * CellRendererPane - valid only for the cachedRendererComponentPath - which can handle
   * targeting mouse events within the LocationTree. 
   */
  private final CellRendererPane cellRendererPane = new CellRendererPane();
  private TreePath cachedRendererComponentPath;
  private static final Object LAT_LONG_PROPERTY = new Object();
  private final OpenMapUrl openMapUrl;
  private ReportsActionFactory reportsActionFactory;
  private JButton printButton;

  @Inject
  public LocationBrowsePanel(
      ReportSet reportSet,
      TaxonomyStore taxonomyStore,
      DefaultUserStore defaultUserStore,
      LocationReportsBrowserPanel reportsBrowser,
      ActionBroker actionBroker,
      ReturnAction returnAction,
      FontManager fontManager,
      EventBusRegistrar eventBusRegistrar,
      NewLocationDialog newLocationDialog,
      FileDialogs fileDialogs,
      PredefinedLocations predefinedLocations,
      Checklists checklists,
      TransposedChecklistSynthesizer transposedChecklistSynthesizer,
      ChecklistSynthesizer checklistSynthesizer,
      ChecklistXlsDialog checklistXlsDialog,
      ChecklistPrintDialog checklistPrintDialog,
      QueryDialog queryDialog,
      QueryPreferences queryPreferences,
      ReportsActionFactory reportsActionFactory,
      BrowsePreferences browsePreferences,
      ReportSetPreference<LocationBrowsePreferences> locationBrowsePreferences,
      Alerts alerts,
      ListeningExecutorService executorService,
      SpeciesInfoDescriber speciesInfoDescriber,
      SpeciesIndexerPanelConfigurer speciesIndexerPanelConfigurer,
      ShowSpeciesMap showSpeciesMap,
      OpenMapUrl openMapUrl,
      VisibilityDetector visibilityDetector) {
    this.reportSet = reportSet;
    this.taxonomyStore = taxonomyStore;
    this.defaultUserStore = defaultUserStore;
    this.reportsBrowser = reportsBrowser;
    this.actionBroker = actionBroker;
    this.fontManager = fontManager;
    this.eventBusRegistrar = eventBusRegistrar;
    this.newLocationDialog = newLocationDialog;
    this.fileDialogs = fileDialogs;
    this.predefinedLocations = predefinedLocations;
    this.checklists = checklists;
    this.transposedChecklistSynthesizer = transposedChecklistSynthesizer;
    this.checklistSynthesizer = checklistSynthesizer;
    this.checklistXlsDialog = checklistXlsDialog;
    this.checklistPrintDialog = checklistPrintDialog;
    this.queryDialog = queryDialog;
    this.queryPreferences = queryPreferences;
    this.reportsActionFactory = reportsActionFactory;
    this.browsePreferences = browsePreferences;
    this.locationBrowsePreferences = locationBrowsePreferences;
    this.alerts = alerts;
    this.executorService = executorService;
    this.speciesInfoDescriber = speciesInfoDescriber;
    this.speciesIndexerPanelConfigurer = speciesIndexerPanelConfigurer;
    this.showSpeciesMap = showSpeciesMap;
    this.openMapUrl = openMapUrl;
    ImageIcon checkOriginalSize = new ImageIcon(LocationTree.class.getResource("check.png"));
    this.checkIcon = new ImageIcon(checkOriginalSize.getImage()
        .getScaledInstance(12, 12,  Image.SCALE_SMOOTH));
    ImageIcon latLongOriginalSize = new ImageIcon(WherePanel.class.getResource("latlong.png"));
    this.latLongIcon = new ImageIcon(latLongOriginalSize.getImage()
        .getScaledInstance(12, 12,  Image.SCALE_SMOOTH));
    eventBusRegistrar.registerWhenInHierarchy(this);
    
    initGUI();
    hookUpContents(returnAction);
    visibilityDetector.install(returnButton);
    
    String initialLocationId = locationBrowsePreferences.get().lastLocationId;
    if (initialLocationId != null) {
      Location location = reportSet.getLocations().getLocation(initialLocationId);
      if (location != null) {
        locationTree.selectLocation(location);
      }
    }
  }
  
  private void hookUpContents(ReturnAction returnAction) {
    updateLocationFilter();
    whereIndexers = new WherePanelIndexers(
        reportSet.getLocations(), predefinedLocations, /*addNullName=*/false, null, locationFilter,
        /* syntheticLocations= */ null);
    attachLocationIndexer();
    returnButton.setAction(returnAction);

    locationIndexPanel.addPropertyChangeListener("value",
        e -> jumpButton.setEnabled(e.getNewValue() != null));
    locationIndexPanel.addActionListener(e -> jumpButton.doClick(100));
    jumpButton.setEnabled(false);
    jumpButton.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent e) {
        Object locationId = locationIndexPanel.getValue();
        Location location = WherePanelIndexers.toLocation(reportSet.getLocations(), locationId);
        if (location != null) {
          locationTree.selectLocation(location);
        }
      }
    });
    
    speciesIndexPanel.addPropertyChangeListener("value",
        e -> speciesJumpButton.setEnabled(e.getNewValue() != null));
    speciesIndexPanel.addActionListener(e -> speciesJumpButton.doClick(100));
    speciesIndexPanel.setEnabled(false);
    speciesJumpButton.setEnabled(false);
    speciesJumpButton.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent e) {
        String id = speciesIndexPanel.getValue();
        if (id != null) {
          Taxon taxon = taxonomyStore.getTaxonomy().getTaxon(id);
          if (taxon != null) {
            reportsBrowser.selectTaxon(taxon);
            reportsBrowser.requestFocusInWindow();
          }
        }
      }      
    });
    
    // Use a timer to run the query once selection has settled down
    // (so keying through the selection won't be painfully slow)
    runQueryTimer = new Timer(
        SELECTION_QUERY_DELAY_MILLIS,
        e -> { runQueryForCurrentSelection(); updateLocationEditingForSelection(); });
    runQueryTimer.setRepeats(false);
    locationTree.addTreeSelectionListener(e -> {
      runQueryTimer.restart();
      cachedRendererComponentPath = null;
    });
    locationTree.addLocationsEditedListener(new LocationsEditedListener() {
      @Override public void onEdited() {
        rebuildLocationIndices();
        // Rerun the entire query.  This is very poor granularity, but is simple,
        // and this probably will not be a commonly used feature.
        runQueryForCurrentSelection();
      }
    });
    locationTree.addTreeExpansionListener(new TreeExpansionListener() {      
      @Override
      public void treeExpanded(TreeExpansionEvent event) {
        cachedRendererComponentPath = null;
      }
      
      @Override
      public void treeCollapsed(TreeExpansionEvent event) {
        cachedRendererComponentPath = null;
      }
    });
    if (Desktop.isDesktopSupported()
        && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
      MouseAdapter adapter = new MouseAdapter() {
        @Override
        public void mouseMoved(MouseEvent e) {
          TreePath path = locationTree.getPathForLocation(e.getX(), e.getY());
          LatLongCoordinates latLong = isOverLatLongIcon(path, e.getPoint());
          if (latLong != null) {
            locationTree.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          } else {
            locationTree.setCursor(Cursor.getDefaultCursor());
          }
         
        }
        
        @Override
        public void mouseClicked(MouseEvent e) {
          TreePath path = locationTree.getPathForLocation(e.getX(), e.getY());
          LatLongCoordinates latLong = isOverLatLongIcon(path, e.getPoint());
          if (latLong != null) {
            try {
              openMapUrl.openLatLong(latLong);
              e.consume();
            } catch (IOException e1) {
            }
          }
        }
      };
      locationTree.addMouseListener(adapter);
      locationTree.addMouseMotionListener(adapter);
    }
    reportFilterCombo.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent event) {
        if (reportFilterCombo.getSelectedItem() == ReportFilter.CUSTOM_FILTER) {
          QueryDefinitionReceiver queryDefinitionReceiver = new QueryDefinitionReceiver() {
            @Override public void queryDefinitionAvailable(QueryDefinition queryDefinition,
                Optional<String> optionalName) {
              reportFilterCombo.addItem(new CustomReportFilter(queryDefinition, optionalName));
              reportFilterCombo.setSelectedIndex(reportFilterCombo.getItemCount() - 1);
              reportFilterCombo.setMaximumRowCount(reportFilterCombo.getItemCount());
              startScanningTaxa(reportsBrowser.getTaxonomy());
            }
            
            @Override
            public void cancelled() {
              reportFilterCombo.setSelectedIndex(0);
            }
          };
          queryDialog.showQueryDefinition(
              SwingUtilities.getWindowAncestor(LocationBrowsePanel.this),
              "<html>" + Messages.getMessage(Name.CUSTOM_FILTER_HELP),
              reportSet,
              taxonomyStore.getTaxonomy(),
              queryDefinitionReceiver);
        } else {
          // If the scanner was customized or should be, rescan before running the query
          if (customScanner || (reportFilterCombo.getSelectedItem() instanceof CustomReportFilter)) {
            startScanningTaxa(reportsBrowser.getTaxonomy());
          } else {
            runQueryForCurrentSelection();
          }
        }
      }
    });
    
    noRarities.addActionListener(e -> runQueryForCurrentSelection());
    
    // When this panel is hidden, stop any further queries from executing
    addComponentListener(new ComponentAdapter() {
      @Override public void componentHidden(ComponentEvent event) {
        runQueryTimer.stop();
      }
    });
    
    locationEditorPanel.addPropertyChangeListener("value", new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent event) {
        Location oldLocation = (Location) event.getOldValue();
        Location newLocation = (Location) event.getNewValue();
        replaceWithEditedLocation(oldLocation, newLocation);
      }      
    });
    
    reportsBrowser.addVisitInfoKeyChangedListener(new VisitInfoKeyChangedListener() {
      @Override
      public void newVisitInfoKey(VisitInfoKey visitInfoKey) {
        // First, see if this new location is in any of the selected locations
        List<Location> selectedLocations = getSelectedLocations();
        Location newLocation = reportSet.getLocations().getLocation(visitInfoKey.locationId());
        boolean isInAny = false;
        for (Location selectedLocation : selectedLocations) {
          if (newLocation.isIn(selectedLocation)) {
            isInAny = true;
            break;
          }
        }
        
        TreePath lastParentPath = null;
        TreePath treePath = locationTree.getTreePath(newLocation);
        ImmutableList<Object> pathList = ImmutableList.copyOf(treePath.getPath());
        List<Object> establishedPath = Lists.newArrayList(pathList.get(0));
        // Test the path from top-to-bottom to find the last bit that's actually going to get found, 
        // to detect if this is a new location.
        for (Object nextEntry : Iterables.skip(pathList, 1)) {
          Object previousEntry = Iterables.getLast(establishedPath);
          // If we can't find the entry as a child of the parent, that means there's some cached entries
          // that are invalid.
          if (locationTree.getModel().getIndexOfChild(previousEntry, nextEntry) < 0) {
            break;
          }
          establishedPath.add(nextEntry);
        }
        
        // Since not quite the entire path was findable, this was a new location;  set lastParentPath
        // to the part of the tree that needs to be refreshed
        if (establishedPath.size() != pathList.size()) {
          lastParentPath = new TreePath(establishedPath.toArray());
        }

        // If the location given is *not* in any of the existing selection, then we need to select it
        if (!isInAny) {
          // Need to ping the tree to note the new location
          if (lastParentPath != null) {
            updateTreeForNewPath(lastParentPath, newLocation);
          } else {
            locationTree.setSelectionPath(treePath);
            locationTree.scrollPathToVisible(treePath);
          }
        } else {
          // The element *does* exist, but won't be visible.  Update the tree accordingly, but then restore the selection  
          if (lastParentPath != null) {
            TreePath[] selectionPaths = locationTree.getSelectionPaths();
            updateTreeForNewPath(lastParentPath, newLocation);            
            locationTree.setSelectionPaths(selectionPaths);
          }
        }

        // Force rerunning the query no matter what has happened to the selection
        runQueryForCurrentSelection();
        
        reportsBrowser.selectVisitInfo(visitInfoKey);
      }

      @Override
      public void deletedVisitInfoKey(VisitInfoKey visitInfoKey) {
        // For now, don't worry about being elegant.  Just get everything updated.
        runQueryForCurrentSelection();
      }      
    });
    
    onlyVisitedLocations.addActionListener(e -> {
      browsePreferences.onlyVisitedLocations = onlyVisitedLocations.isSelected();
      updateLocationFilter();
      rebuildLocationIndices();
      if (onlyVisitedLocations.isSelected()) {
        Object locationId = locationIndexPanel.getValue();
        if (locationId != null) {
          if (!locationFilter.apply(reportSet.getLocations().getLocation((String) locationId))) {
            locationIndexPanel.setTextValue("");
          }
        }
      }
    });

    updateSightingButtons(null);
    setTaxonomy(taxonomyStore.getTaxonomy());
  }

  private void rebuildLocationIndices() {
    whereIndexers.reindex(false, locationFilter);
    attachLocationIndexer();
  }

  public void attachLocationIndexer() {
    whereIndexers.configureIndexer(locationIndexPanel);
  }

  /** Replace an existing location with a new, edited location */
  private void replaceWithEditedLocation(Location oldLocation, Location newLocation) {
    if (newLocation != null) {
      // Remember the new location if the old location was added during this panel session
      // It's not particularly relevant to remove the old location.
      if (addedLocations.contains(oldLocation)) {
        addedLocations.add(newLocation);
      }

      // If the parent already has a location with exactly that name,
      // ("real" locations or predefined), block the rename
      if (!oldLocation.getModelName().equals(newLocation.getModelName())
          && (newLocation.getParent().getContent(newLocation.getModelName()) != null
              || predefinedLocations.getPredefinedLocationChild(
                  newLocation.getParent(), newLocation.getModelName()) != null)) {
        alerts.showError(LocationBrowsePanel.this,
            Name.NAME_ALREADY_EXISTS_TITLE,
            Name.NAME_ALREADY_EXISTS_MESSAGE,
            HtmlResponseWriter.htmlEscape(newLocation.getModelName()),
            HtmlResponseWriter.htmlEscape(newLocation.getParent().getModelName()));
        // Revert the editor panel.
        locationEditorPanel.setLocation(oldLocation);
      } else {
        // If the parents are the same, life is simple - just swap state.  If they *have* changed,
        // then we need to be a bit more nuclear in the approach to updating tree model state.
        boolean parentsHaveChanged = oldLocation.getParent() != newLocation.getParent();
        TreePath oldTreePath = locationTree.getTreePath(oldLocation);
        
        reportSet.replaceLocation(oldLocation, newLocation);
        // Make sure any predefined parents get properly registered 
        reportSet.getLocations().ensureAdded(newLocation);
        
        if (parentsHaveChanged) {
          TreePath newTreePath = locationTree.getTreePath(newLocation);
          // Tell the location tree model to - basically - drop everything it knows.  This is certainly overkill,
          // but sufficient.
          locationTree.getModel().valueForPathChanged(oldTreePath.getParentPath(), oldTreePath.getParentPath().getLastPathComponent());
          locationTree.getModel().valueForPathChanged(newTreePath.getParentPath(), newTreePath.getParentPath().getLastPathComponent());
          // Don't try to restore most selections - just force the selection to the new location 
          locationTree.setSelectionPath(newTreePath);
        } else {
          // Generate new selection paths for the updated selection
          TreePath[] paths = locationTree.getSelectionPaths();
          if (paths != null) {
            for (int i = 0; i < paths.length; i++) {
              paths[i] = replacePathSegment(paths[i], oldLocation, newLocation);
            }
          }
          
          // Inform the location tree that the value at this path has changed
          // (which will clear the selection)
          TreePath newTreePath = locationTree.getTreePath(newLocation);
          locationTree.getModel().valueForPathChanged(newTreePath, newLocation);
          
          // Set the selection back
          locationTree.setSelectionPaths(paths);
        }
        
        // And update the indexer to account for a changed name
        whereIndexers.remove(oldLocation);
        whereIndexers.add(newLocation);
      }
    }
  }
  
  /**
   * Given a tree path, return a tree path replacing one segment with
   * a new value.
   */
  private TreePath replacePathSegment(TreePath treePath,
      Object oldSegment, Object newSegment) {
    Object[] pathObjects = treePath.getPath();
    for (int i = 0; i < pathObjects.length; i++) {
      if (pathObjects[i] == oldSegment) {
        pathObjects[i] = newSegment;
        return new TreePath(pathObjects);
      }
    }
    
    return treePath;
  }

  private void runQueryForCurrentSelection() {
    cachedRendererComponentPath = null;
    TreePath[] paths = locationTree.getSelectionPaths();
    List<Resolved> queriedTaxa;
    if (paths == null || paths.length == 0) {
      reportsBrowser.setModel(new DefaultTreeModel(new DefaultMutableTreeNode("")));
      totalLabel.setText("");
      reportFilterCombo.setEnabled(false);
      noRarities.setEnabled(false);
      xlsButton.setVisible(false);
      printButton.setVisible(false);
      editChecklist.setVisible(false);
      shareChecklist.setVisible(false);
      queriedTaxa = ImmutableList.of();
    } else {
      List<Location> locations = Lists.newArrayList();
      for (TreePath path : paths) {
        locations.add((Location) path.getLastPathComponent());
      }
      final Checklist checklist = getChecklist(taxonomyStore.getTaxonomy(), locations);
      QueryProcessor processor = newQueryProcessor(locations, checklist, /*other taxonomies */ null).includingIncompatibleSightings();
      
      if (locations.size() != 1
          || !taxonomyStore.getTaxonomy().isBuiltIn()
          || (checklist != null && checklist.isSynthetic())
          || (checklist == null && locations.get(0).isBuiltInLocation()
              && locations.get(0).getType() == Location.Type.region)) {
        // No checklist editing/sharing for multiple checklists;
        // or for built-in "regions" (e.g., continents) that don't have checklists.
        editChecklist.setVisible(false);
        shareChecklist.setVisible(false);
      } else if (checklist != null) {
        editChecklist.setText(checklist.isBuiltIn()
            ? Messages.getMessage(Name.CORRECT_CHECKLIST)
            : Messages.getMessage((Name.EDIT_CHECKLIST)));
        editChecklist.setVisible(true);
        shareChecklist.setVisible(!checklist.isBuiltIn());
      } else {
        editChecklist.setVisible(true);
        editChecklist.setText(Messages.getMessage(Name.CREATE_CHECKLIST));
        shareChecklist.setVisible(false);
      }
      
      // TODO: support non-species depths?
      Predicate<Sighting> predicate = locationPredicate(locations);
      
      final QueryResults queryResults = processor.runQuery(
          predicate,
          queryPreferences.getCountablePredicate(taxonomyStore.getTaxonomy(), false, null),
          Taxon.Type.species);
      reportsBrowser.setLocationRoot(locations.size() == 1 ? locations.get(0) : null);

      reportsBrowser.setQueryResults(queryResults, /*showVisits=*/true);
      Object root = reportsBrowser.getModel().getRoot();
      int childCount = reportsBrowser.getModel().getChildCount(root);
      
      // Now, look for a child that is worth selecting by default.
      for (int i = 0; i < childCount; i++) {
        Object child = reportsBrowser.getModel().getChild(root, i);
        boolean selectChild = false;
        if (child instanceof Resolved) {
          Resolved resolved = (Resolved) child;
          if (resolved.getSmallestTaxonType() != Taxon.Type.family) {
            selectChild = true;
          }
        }
        
        if (reportsBrowser.isVisitsParentNode(child)) {
          selectChild = true;  
        }

        // If this child is worth making a default selection, do so. 
        if (selectChild) {
          reportsBrowser.setSelectionPath(new TreePath(new Object[]{root,child}));
          break;
          
        }
      }
      
      queryResults.addListener(() -> queryResultsUpdated(queryResults));
      
      queryResultsUpdated(queryResults);
      reportFilterCombo.setEnabled(checklist != null);
      noRarities.setEnabled(checklist != null);
      xlsButton.setVisible(checklist != null);
      printButton.setVisible(checklist != null);
      
      queriedTaxa = queryResults.getTaxaAsList();
    }
    
    speciesIndexPanel.setTextValue("");
    
    if (queriedTaxa.isEmpty()) {
      speciesIndexPanel.setEnabled(false);
    } else {
      Set<String> taxa = new LinkedHashSet<>();
      
      for (Resolved resolved : queriedTaxa) {
        if (resolved.getType() == SightingTaxon.Type.SINGLE) {
          Taxon taxon = resolved.getTaxon();
          taxa.add(taxon.getId());
        }
      }
      
      // Filter the species index panel down to just "contains"
      // It *would* be more efficient to have a species indexer containing just these
      // species, rather than filtering down... but OTOH there's a lot of code to
      // assemble the various layers of indexing with alternates.  This is waaaay simpler.
      speciesIndexPanel.setFilter(taxa::contains);
      speciesIndexPanel.setEnabled(true);
    }
    
    // Since the query just got explicitly updated, make sure it doesn't
    // re-run from anything pending.
    runQueryTimer.stop();
  }

  private Predicate<Sighting> locationPredicate(List<Location> locations) {
    List<Predicate<Sighting>> predicates = Lists.newArrayList();
    for (Location location : locations) {
      predicates.add(SightingPredicates.in(location, reportSet.getLocations()));
    }
    Predicate<Sighting> predicate = Predicates.or(predicates);
    Object filter = reportFilterCombo.getSelectedItem();
    // For "only lifers", don't include uncountable sightings as valid.  Doing so triggers two
    // problems:
    // - We don't want those to *not* show as lifers
    // - We don't want to add escapees that have been seen as potential lifers, which is doubly wrong
    if (filter == ReportFilter.ONLY_LIFERS) {
      predicate = Predicates.and(predicate,
          queryPreferences.getCountablePredicate(taxonomyStore.getTaxonomy(), false, null));
    }
    
    if (defaultUserStore.getUser() != null) {
      predicate = Predicates.and(predicate, SightingPredicates.includesUser(defaultUserStore.getUser()));
    }
    
    return predicate;
  }

  private void queryResultsUpdated(QueryResults queryResults) {
    Taxonomy taxonomy = taxonomyStore.getTaxonomy();
    int speciesCount = queryResults.getCountableSpeciesSize(taxonomy, false);
    Checklist checklist = queryResults.getChecklist(taxonomy);
    if (checklist != null) {
      String totalsText = getChecklistTotals(checklist, taxonomy, speciesCount, scanSeenTaxa);
      totalLabel.setText(totalsText);
    } else {
      totalLabel.setText(
          Messages.getFormattedMessage(Name.TOTAL_SPECIES_FORMAT_NO_CHECKLIST, speciesCount));
    }
    // Only show family filters when there is a checklist, and the filter
    // is not restricting display to endemics or lifers
    Object filter = reportFilterCombo.getSelectedItem();
    boolean filterWithFamilyTotals =
        filter == ReportFilter.ONLY_RECORDED || filter == ReportFilter.SHOW_ALL;
    reportsBrowser.setShowFamilyTotals(checklist != null && filterWithFamilyTotals);
    this.currentQueryResults = queryResults;
  }
  
  private String getChecklistTotals(
      Checklist checklist,
      Taxonomy taxonomy,
      int speciesCount,
      ScanSeenTaxa currentSeenTaxa) {
    // Count the number of lifers (if the taxa scan has completed)
    int lifers = -1;
    boolean ignoreRarities = noRarities.isSelected();
    Set<String> seenTaxa = null;
    if (currentSeenTaxa != null) {
      seenTaxa = currentSeenTaxa.getSeenTaxa(taxonomy, null);
      if (seenTaxa != null) {
        lifers = 0;
        for (SightingTaxon taxon : checklist.getTaxa(taxonomy)) {
          if (taxon.getType() == SightingTaxon.Type.SINGLE) {
            if (!seenTaxa.contains(taxon.getId())) {
              Status status = checklist.getStatus(taxonomy, taxon);
              if (status == Status.ESCAPED || status == Status.EXTINCT) {
                continue;
              } else if ((status == Status.RARITY || status == Status.RARITY_FROM_INTRODUCED) && ignoreRarities) {
                continue;
              }
              
              lifers++;
            }
          }
        }
      }
    }

    int checklistCount = checklist.getTaxa(taxonomy).size();
    checklistCount -= checklist.getTaxa(taxonomy, Status.ESCAPED).size();
    if (noRarities.isSelected()) {
      checklistCount -= checklist.getTaxa(taxonomy, Status.RARITY).size();
    }
    
    ImmutableSet<SightingTaxon> endemics =
        checklist.getTaxa(taxonomy, Checklist.Status.ENDEMIC);
    int endemicCount = endemics.size();
    int endemicsSeen = 0;
    if (seenTaxa != null) {
      for (SightingTaxon endemic : endemics) {
        if (seenTaxa.contains(endemic.getId())) {
          endemicsSeen++;
        }
      }
    }
    
    if (lifers >= 0) {
      String lifersString;
      Object reportFilterObj = reportFilterCombo.getSelectedItem();
      if (reportFilterObj instanceof CustomReportFilter) {
        lifersString = Messages.getFormattedMessage(Name.SIGHTINGS_NEW_FOR_FORMAT, lifers,
            ((CustomReportFilter) reportFilterObj).optionalName
                .or(Messages.getMessage(Name.CUSTOM_REPORT)));
      } else {
        lifersString = Messages.getFormattedMessage(
            Name.POSSIBLE_LIFERS_FORMAT, lifers);
      }
          
      if (endemicCount == 0) {
        return Messages.getFormattedMessage(Name.TOTAL_SPECIES_WITH_LIFERS_FORMAT,
                speciesCount, checklistCount, lifersString);
      } else {
        if (endemicsSeen > 0) {
          return Messages.getFormattedMessage(Name.TOTAL_SPECIES_WITH_LIFERS_AND_ENDEMICS_SOME_SEEN,
                  speciesCount, endemicsSeen, checklistCount, endemicCount, lifersString);
        } else {
          return Messages.getFormattedMessage(Name.TOTAL_SPECIES_WITH_LIFERS_AND_ENDEMICS,
                  speciesCount, checklistCount, endemicCount, lifersString);
        }
      }
    } else {
      if (endemicCount == 0) {
        return Messages.getFormattedMessage(Name.TOTAL_SPECIES_FORMAT,
                speciesCount, checklistCount);
      } else {
        if (endemicsSeen > 0) {
          return Messages.getFormattedMessage(Name.TOTAL_SPECIES_WITH_ENDEMICS_SOME_SEEN,
                  speciesCount, endemicsSeen, checklistCount, endemicCount);
        } else {
          return Messages.getFormattedMessage(Name.TOTAL_SPECIES_WITH_ENDEMICS,
                  speciesCount, checklistCount, endemicCount);
        }
      }
    }
  }
  
  private void updateLocationEditingForSelection() {
    Location location = getOnlySelectedLocation();
    locationEditorPanel.setLocation(location);
    if (location == null) {
      locationEditorPanel.setEnabled(false);
      deleteLocation.setEnabled(false);
      editLocation.setEnabled(false);
      newLocation.setEnabled(false);
      newLocation.setToolTipText(
          Messages.getMessage(Name.CREATES_A_NEW_LOCATION));
    } else {
      locationEditorPanel.setEnabled(!location.isBuiltInLocation());
      // Locations cannot be deleted when they have children
      deleteLocation.setEnabled(
          location.getParent() != null
          && location.contents().isEmpty()
          && !location.isBuiltInLocation());
      // Locations *can* be edited when they have children
      editLocation.setEnabled(
          location.getParent() != null
          && !location.isBuiltInLocation());
      newLocation.setEnabled(true);
      newLocation.setToolTipText(
          Messages.getFormattedMessage(
              Name.CREATES_A_NEW_LOCATION_INSIDE_FORMAT, location.getDisplayName()));
    }
    
    locationBrowsePreferences.get().lastLocationId = location == null ? null : location.getId();
    locationBrowsePreferences.save(false);
  }

  /** Run the delete-a-location dialog. */
  private void deleteLocation() {
    Location location = getOnlySelectedLocation();
    String locationName = LocationIdToString.getStringWithType(location);
    TreePath parentPath = locationTree.getTreePath(location.getParent());
    String parentLocationName = LocationIdToString.getStringWithType(
        location.getParent());
    
    if (alerts.showOkCancel(this,
        Name.DELETE_LOCATION_TITLE,
        Name.DELETE_LOCATION_MESSAGE,
        HtmlResponseWriter.htmlEscape(locationName),
        HtmlResponseWriter.htmlEscape(parentLocationName)) == JOptionPane.OK_OPTION) {
      // Remove the location from the indexer.
      whereIndexers.remove(location);
      reportSet.deleteLocation(location);
      locationTree.getModel().valueForPathChanged(parentPath, parentPath.getLastPathComponent());
      locationTree.setSelectionPath(parentPath);
      locationTree.expandPath(parentPath);
    }
  }
  
  /** Run the edit location dialog. */
  private void editLocation() {
    final Location selectedLocation = getOnlySelectedLocation();
    Preconditions.checkState(!selectedLocation.isBuiltInLocation());
    newLocationDialog.editLocation(selectedLocation,
        SwingUtilities.getWindowAncestor(this),
        location -> replaceWithEditedLocation(selectedLocation, location));
  }

  /** Run the new location dialog. */
  private void newLocation() {
    final Location selectedLocation = getOnlySelectedLocation();
    // If this is a predefined location, force its addition right now
    reportSet.getLocations().ensureAdded(selectedLocation);
    final TreePath path = locationTree.getTreePath(selectedLocation);
    LocationReceiver locationReceiver = new LocationReceiver() {
      @Override public void locationAvailable(Location location) {
        // Add the location to the LocationSet (until done, the location has no
        // ID and is not in its parent)
        reportSet.getLocations().ensureAdded(location);
        addedLocations.add(location);
        updateTreeForNewPath(path, location);
      }
    };
    newLocationDialog.newLocation(selectedLocation,
        SwingUtilities.getWindowAncestor(this),
        locationReceiver);
  }
  
  /**
   * Update various parts of the tree - model, selection, expansion - and
   * the indexing - for a potentially new location.
   */
  private void updateTreeForNewPath(
      TreePath originalParentPath,
      Location location) {
    // Notify the tree that the original path has been updated
    ((LocationTreeModel) locationTree.getModel()).newLocation(originalParentPath, location);
    TreePath newLocationPath = locationTree.getTreePath(location);
    // And expand and select the new location
    locationTree.expandPath(newLocationPath.getParentPath());
    locationTree.selectLocation(location);
    
    // And rebuild indexes
    rebuildLocationIndices();
  }

  /**
   * Create the query processor for a given location.
   */
  private QueryProcessor newQueryProcessor(
      List<Location> locations,
      Checklist checklist,
      @Nullable Map<Taxonomy, Checklist> extendedTaxonomyChecklists) {    
    QueryProcessor processor = new QueryProcessor(reportSet, taxonomyStore.getTaxonomy(), checklist, extendedTaxonomyChecklists);
    
    // Possibly apply a taxon filter when a checklist is available.  These will remove taxa from
    // eligibility for display, even if they meet all other criteria
    if (checklist != null) {
      Object filter = reportFilterCombo.getSelectedItem();

      if (filter == ReportFilter.ONLY_ENDEMICS) {
        // Only endemics - drop any sighting for a non-endemic species
        processor.withTaxonFilter(new Predicate<SightingTaxon.Resolved>() {
          @Override public boolean apply(Resolved resolved) {
            Status status = checklist.getStatus(resolved.getTaxonomy(), resolved.getSightingTaxon());
            return status == Status.ENDEMIC;
          }
        });
      } else if (filter == ReportFilter.ONLY_LIFERS || (filter instanceof CustomReportFilter)) {
        // Only lifers - or "custom" report filters (new for X) - don't show anything that wouldn't
        // be a lifer (or, more specifically, anything that isn't registered by ScanSeenTaxa).
        processor.withTaxonFilter(reportsBrowser::wouldBeLifer);
      } else if (filter == ReportFilter.NOT_SEEN_HERE) {
        processor.withTaxonFilter(
            resolved -> !reportsBrowser.areSightingsInThisReport(resolved));
      }
      
      // Ensure that the query processor adds checklist species even when they *haven't* been seen.
      // Necessary, of course, because the display should
      if (filter != ReportFilter.ONLY_RECORDED) {
        ImmutableSet.Builder<Status> statusToIgnore = ImmutableSet.builder();
        // Drop rarities from the list if needed
        if (noRarities.isSelected()) {
          statusToIgnore.add(Status.RARITY);
          statusToIgnore.add(Status.RARITY_FROM_INTRODUCED);
        }
        
        if (filter == ReportFilter.ONLY_LIFERS
            || filter instanceof CustomReportFilter) {
          statusToIgnore.add(Status.EXTINCT, Status.ESCAPED);
        }
        if (!queryPreferences.countIntroduced) {
          statusToIgnore.add(Status.INTRODUCED);
          statusToIgnore.add(Status.RARITY_FROM_INTRODUCED);
        }
        
        if (!browsePreferences.showExtinctTaxa) {
          statusToIgnore.add(Status.EXTINCT);
        }
        // Now don't show
        processor.includeAllChecklistSpecies(statusToIgnore.build());
      }
    }
    
    return processor;
  }

  /**
   * Gets a checklist for a list of locations.
   */
  private Checklist getChecklist(Taxonomy taxonomy, Collection<Location> locations) {
    final Checklist checklist;
    if (locations.size() == 1) {
      checklist = checklists.getChecklist(reportSet, taxonomy, locations.iterator().next());
    } else {
      boolean allBuiltIn = true;
      for (Location location : locations) {
        Checklist locationChecklist = checklists.getChecklist(reportSet, taxonomy, location);
        if (locationChecklist == null) {
          return null;
        }
        
        if (!locationChecklist.isBuiltIn()) {
          allBuiltIn = false;
          break;
        }
      }
      
      if (allBuiltIn && taxonomy.isBuiltIn()) {
        checklist =
            transposedChecklistSynthesizer.synthesizeChecklist(reportSet, taxonomy, locations);
      } else {
        checklist = checklistSynthesizer.synthesizeChecklist(taxonomy, locations);
      }
    }
    return checklist;
  }
  
  /** Write the checklist to a spreadsheet. */
  private void saveChecklistAsSpreadsheet() {
    List<Location> selectedLocations = getSelectedLocations();
    if (selectedLocations.isEmpty()) {
      return;
    }
    
    Taxonomy taxonomy = taxonomyStore.getTaxonomy();
    Checklist checklist = getChecklist(taxonomy, selectedLocations);
    if (checklist == null) {
      return;
    }

    String fileName;
    if (selectedLocations.size() == 1) {
      fileName = String.format("checklist-%s.xls", selectedLocations.get(0).getDisplayName());
    } else if (selectedLocations.size() <= 3) {
      List<String> locationNames = Lists.newArrayList();
      for (Location location : selectedLocations) {
        locationNames.add(location.getDisplayName());
      }
      fileName = String.format("checklist-%s.xls",
          Joiner.on('-').join(locationNames));
    } else {
      fileName = "checklist.xls";
    }
    
    File saveFile = fileDialogs.saveFile(
        (Frame) SwingUtilities.getWindowAncestor(this),
        Messages.getMessage(Name.SAVE_A_SPREADSHEET),
        fileName,
        null, FileType.OTHER);
    if (saveFile == null) {
      return;
    }
    
    Map<Taxonomy, Checklist> otherTaxonomiesWithChecklists = new LinkedHashMap<>();
    if (!taxonomy.isBuiltIn()) {
      // TODO: would be nice to not always use Clements here.  Currently ScanSeenTaxa doesn't know
      // how to use IOC unless it's the primary taxonomy.
      Checklist birdChecklist = getChecklist(taxonomyStore.getClements(), selectedLocations);
      if (birdChecklist != null) { 
        otherTaxonomiesWithChecklists.put(taxonomyStore.getClements(), birdChecklist);
      }
    }
    
    for (Taxonomy extendedTaxonomy : reportSet.extendedTaxonomies()) {
      if (extendedTaxonomy != taxonomy) {
        Checklist extendedChecklist = getChecklist(extendedTaxonomy, selectedLocations);
        if (extendedChecklist != null) { 
          otherTaxonomiesWithChecklists.put(extendedTaxonomy, extendedChecklist);
        }
      }
    }

    SpreadsheetConfiguration configuration = checklistXlsDialog.getConfiguration(
        this, !otherTaxonomiesWithChecklists.isEmpty(), selectedLocations);
    if (configuration == null) {
      return;
    }
    
    List<Location> locations = new ArrayList<>();
    if (selectedLocations.size() == 1) {
      Location selectedLocation = selectedLocations.get(0);
      locations.add(selectedLocation);
      Location state = Locations.getAncestorOfType(selectedLocation, Location.Type.state);
      if (state != null && state != selectedLocation) {
        locations.add(state);
      }
      Location country = Locations.getAncestorOfType(selectedLocation, Location.Type.country);
      if (country != null && country != selectedLocation) {
        locations.add(country);
      }
    } else {
      // Find common parent, use if it's a country?
    }
    locations.add(null);
    
    ScanSeenTaxa.start(reportSet, taxonomy, executorService,
        scanned -> {
          QueryProcessor processor =
              newQueryProcessor(selectedLocations, checklist, otherTaxonomiesWithChecklists)
                  .includingIncompatibleSightings();
          
          Predicate<Sighting> predicate = locationPredicate(selectedLocations);
          // TODO: support non-species depths?
          QueryResults queryResults = processor.runQuery(predicate, null, Taxon.Type.species);
          QueryResults worldResults = null;
          
          // To include a sighting from anywhere, do another query.
          if (configuration.includeSighting
              && (configuration.checklistSightingOption == ChecklistSightingOption.FIRST_IN_WORLD
                  || configuration.checklistSightingOption == ChecklistSightingOption.MOST_RECENT_IN_WORLD)) {
            QueryProcessor worldProcessor = new QueryProcessor(reportSet, queryResults.getTaxonomy())
                .includingIncompatibleSightings();
            Predicate<Sighting> worldPredicate =
                queryPreferences.getCountablePredicate(taxonomyStore.getTaxonomy(), false, null);
            if (defaultUserStore.getUser() != null) {
              predicate = Predicates.and(predicate, SightingPredicates.includesUser(defaultUserStore.getUser()));
            }
            worldResults = worldProcessor.runQuery(worldPredicate, null, Taxon.Type.species);
          }
          try {
            ChecklistXlsOutput output = new ChecklistXlsOutput(saveFile);
            output.setIncludeScientific(configuration.includeScientific);
            output.setShowLifersInBold(configuration.showLifersInBold);
            output.setShowNewForColumn(configuration.showNewForColumn);
            output.setShowStatus(configuration.showStatus);
            output.setIncludeSighting(configuration.includeSighting);
            output.setChecklistSightingOption(configuration.checklistSightingOption);
            output.setShowFamilies(configuration.showFamilies);
            output.setShowAllTaxonomies(configuration.showAllTaxonomies);
            output.setOtherTaxonomiesWithChecklists(otherTaxonomiesWithChecklists);
            
            Map<Taxonomy, String> totalTextMap = new LinkedHashMap<>();
            totalTextMap.put(
                taxonomy,
                getChecklistTotals(checklist,
                    queryResults.getTaxonomy(), queryResults.getCountableSpeciesSize(taxonomy, false),
                    scanned));
            otherTaxonomiesWithChecklists.forEach((otherTaxonomy, otherChecklist) -> {
              totalTextMap.put(
                  otherTaxonomy,
                  getChecklistTotals(otherChecklist,
                      otherTaxonomy, queryResults.getCountableSpeciesSize(otherTaxonomy, false), scanned));
            });
            output.setTotalText(totalTextMap);

            
            output.write(reportSet, queryResults, worldResults, checklist, scanned, locations, configuration.daysToInclude);
          } catch (IOException e) {
            FileDialogs.showFileSaveError(alerts, e, saveFile);
            return;
          }
          try {
            Desktop.getDesktop().open(saveFile);
          } catch (IOException e) {
            alerts.showError(this,
                Name.OPENING_FAILED,
                Name.NO_APPLICATION_FOR_XLS_FILES);
          }
        },
        locations, sightingPredicate(taxonomy),
        new ScanSeenTaxa.Options().setIncludeAllTaxonomies(configuration.showAllTaxonomies));
  }
  
  /** Print the checklist. */
  private void print() {
    List<Location> selectedLocations = getSelectedLocations();
    if (selectedLocations.isEmpty()) {
      return;
    }
    
    Taxonomy taxonomy = taxonomyStore.getTaxonomy();
    Checklist checklist = getChecklist(taxonomy, selectedLocations);
    if (checklist == null) {
      return;
    }
    
    ChecklistPrintPreferences prefs = checklistPrintDialog.getConfiguration(this);
    if (prefs == null) {
      return;
    }
    
    File temp;
    try {
      temp = File.createTempFile("checklist", ".html");
    } catch (IOException e) {
      alerts.showError(null, Name.COULD_NOT_PRINT,
          Name.COULD_NOT_CREATE_TEMPORARY_FILE_TO_PRINT,
          e);
      return;
    }

    List<Location> locations = new ArrayList<>();
    if (selectedLocations.size() == 1) {
      Location selectedLocation = selectedLocations.get(0);
      locations.add(selectedLocation);
      Location state = Locations.getAncestorOfType(selectedLocation, Location.Type.state);
      if (state != null && state != selectedLocation) {
        locations.add(state);
      }
      Location country = Locations.getAncestorOfType(selectedLocation, Location.Type.country);
      if (country != null && country != selectedLocation) {
        locations.add(country);
      }
    } else {
      // Find common parent, use if it's a country?
    }
    locations.add(null);
    
    ScanSeenTaxa.start(reportSet, taxonomy, executorService,
        scanned -> {
          QueryProcessor processor =
              newQueryProcessor(selectedLocations, checklist, null);
          
          Predicate<Sighting> predicate = locationPredicate(selectedLocations);
          QueryResults queryResults = processor.runQuery(predicate, null, Taxon.Type.species);
          try {
            ChecklistHtmlOutput output = new ChecklistHtmlOutput(taxonomy);
            output.setTitle(
                selectedLocations.stream().map(Location::getDisplayName).collect(Collectors.joining(", ")));
            output.setTotalText(
                getChecklistTotals(checklist,
                    queryResults.getTaxonomy(), queryResults.getCountableSpeciesSize(taxonomy, false),
                    scanned));
            output.setShowStatus(prefs.showStatus);
            output.setShowFamilies(prefs.showFamilies);
            output.setShowLifersInBold(prefs.showLifersInBold);
            output.setCompactOutput(prefs.compactPrinting);
            output.setScientificOrCommon(
                checklistPrintDialog.getScientificOrCommonFromConfiguration(prefs));
            output.setPrint(true);
            output.setDaysToInclude(prefs.daysToPrint);
            output.writeSpeciesList(temp, reportSet, queryResults, checklist, scanned, locations);
          } catch (IOException e) {
            alerts.showError(this, Name.COULD_NOT_SAVE, Name.SAVING_THE_SPREADSHEET_DID_NOT_SUCCEED);
            return;
          }
          try {
            DesktopUtils.openHtmlFileInBrowser(temp);
          } catch (IOException e) {
            alerts.showError(this,
                Messages.getMessage(Name.COULD_NOT_PRINT),
                Messages.getFormattedMessage(Name.COULD_NOT_CREATE_TEMPORARY_FILE_TO_PRINT_IN,
                    temp.getParentFile().getAbsolutePath()),
                e);
          }
        },
        locations, sightingPredicate(taxonomy),
        new ScanSeenTaxa.Options());
  }
  
  private Location rootVistsLocation(Iterable<VisitInfoKey> visitInfoKeys) {
    Location root = null;
    for (VisitInfoKey visitInfoKey : visitInfoKeys) {
      Location location = reportSet.getLocations().getLocation(visitInfoKey.locationId());
      if (root == null) {
        root = location;
      } else {
        root = Locations.getCommonAncestor(location, root);
      }
    }
    return root;
  }

  private void saveVisitsAsTripReport(ActionEvent e) {
    Set<VisitInfoKey> visitInfoKeys = getSelectedVisitInfoKeys();
    if (visitInfoKeys.isEmpty()) {
      return;
    }
    Predicate<Sighting> predicate = s ->
      visitInfoKeys.contains(VisitInfoKey.forSighting(s));
    Location rootLocation = rootVistsLocation(visitInfoKeys);

    QueryExecutor executor = new QueryExecutor() {
      private boolean onlyCountable = false;
      private boolean includeIncompatibleSightings = false;

      @Override
      public QueryExecutor onlyCountable() {
        this.onlyCountable = true;
        return this;
      }
      
      @Override
      public QueryExecutor includeIncompatibleSightings() {
        this.includeIncompatibleSightings = true;
        return this;
      }
      
      @Override
      public Location getRootLocation() {
        return rootLocation;
      }
      
      @Override
      public Optional<String> getReportName() {
        return Optional.absent();
      }
      
      @Override
      public Set<ReportHints> getReportHints() {
        return ImmutableSet.of();
      }
      
      @Override
      public Optional<String> getReportAbbreviation() {
        return Optional.absent();
      }
      
      @Override
      public QueryResults executeQuery(Taxon.Type depthOverride, @Nullable Taxonomy taxonomyOverride) {
        QueryProcessor processor = new QueryProcessor(reportSet, 
            taxonomyOverride == null
            ? taxonomyStore.getTaxonomy() : taxonomyOverride);
        if (includeIncompatibleSightings) {
          processor.includingIncompatibleSightings();
        }
        if (onlyCountable) {
          processor.onlyIncludeCountableSightings();
        }
        return processor.runQuery(predicate, null, depthOverride == null ? Taxon.Type.species : depthOverride);
      }
    };
    
    reportsActionFactory.saveAsTripReport(executor).actionPerformed(e);
  }

  interface CreateChecklistPresenter {
    void created();
    void cancel();
    void switchTo(JPanel panel);
  }

  interface EditChecklistPresenter {
    void done();
  }

  /** Starts either the create-a-checklist or edit-a-checklist flow. */
  private void editChecklist(boolean mustEdit) {
    final Location location = getOnlySelectedLocation();
    if (location == null) {
      return;
    }
    
    if (!taxonomyStore.getTaxonomy().isBuiltIn()) {
      // TODO: no support for checklists on non-built-in taxonomies
      return;
    }

    Checklist checklist = checklists.getChecklist(reportSet, taxonomyStore.getTaxonomy(), location);
    final CardLayout layout = (CardLayout) reportsOrEditChecklist.getLayout();
    if (checklist != null) {
      
      if (checklist.isBuiltIn()) {
        if (alerts.showOkCancel(this,
            Name.CORRECTING_A_BUILT_IN_CHECKLIST_TITLE,
            Name.CORRECTING_A_BUILT_IN_CHECKLIST_MESSAGE,
            location.getDisplayName()) != JOptionPane.OK_OPTION) {
          return;
        }
      }

      final EditChecklistPanel editChecklistPanel = new EditChecklistPanel(
          reportSet, checklists, checklist, location, taxonomyStore,
          eventBusRegistrar, fontManager, executorService,
          speciesInfoDescriber, speciesIndexerPanelConfigurer, showSpeciesMap, alerts);
      editChecklistPanel.setPresenter(new EditChecklistPresenter() {
        @Override
        public void done() {
          reportsOrEditChecklist.remove(editChecklistPanel);
          layout.first(reportsOrEditChecklist);
          reenableEverything();
          runQueryForCurrentSelection();

          // Ping the tree to let it know to flush the value at the current location
          if (locationTree.getSelectionPath() != null
              && locationTree.getSelectionPath().getLastPathComponent() == location) {
            locationTree.getModel().valueForPathChanged(
                locationTree.getSelectionPath(), newLocation);
            locationTree.selectLocation(location);
          }          
        }
      });
      reportsOrEditChecklist.add(editChecklistPanel, "editChecklist");
      editChecklist.setText(Messages.getMessage(Name.EDITING_CHECKLIST));
    } else if (!mustEdit) {
      final CreateChecklistPanel createChecklistPanel = new CreateChecklistPanel(
          reportSet, checklists, location, taxonomyStore, eventBusRegistrar, fontManager);
      createChecklistPanel.setPresenter(new CreateChecklistPresenter() {
        private JPanel current = createChecklistPanel;
        
        @Override
        public void created() {
          reset();
          
          // Re-run the query for the current selection
          runQueryForCurrentSelection();
          // ... now edit the checklist.  And pass "true" for mustEdit, just out of paranoia,
          // so that we don't re-enter the "create" flow...
          editChecklist(true);
        }
  
        @Override
        public void cancel() {
          reset();
        }
        
        @Override
        public void switchTo(JPanel panel) {
          reportsOrEditChecklist.remove(current);
          current = panel;
          reportsOrEditChecklist.add(panel, "createChecklist");
          layout.last(reportsOrEditChecklist);
        }
  
        private void reset() {
          reportsOrEditChecklist.remove(current);
          layout.first(reportsOrEditChecklist);
          reenableEverything();
          runQueryForCurrentSelection();
        }
      });
      reportsOrEditChecklist.add(createChecklistPanel, "createChecklist");
      editChecklist.setText(Messages.getMessage(Name.CREATING_CHECKLIST));
    }

    layout.last(reportsOrEditChecklist);
    
    // Disable just about everything in the UI
    locationTree.setEnabled(false);
    newLocation.setEnabled(false);
    deleteLocation.setEnabled(false);
    editChecklist.setEnabled(false);
    shareChecklist.setEnabled(false);
    locationEditorPanel.setEnabled(false);
    xlsButton.setEnabled(false);
    printButton.setEnabled(false);
    reportFilterCombo.setEnabled(false);
    jumpButton.setEnabled(false);
    returnButton.setEnabled(false);
    locationIndexPanel.setEnabled(false);
    totalLabel.setEnabled(false);
    speciesJumpButton.setEnabled(false);
    speciesIndexPanel.setEnabled(false);
    onlyVisitedLocations.setEnabled(false);
  }
  
  private void shareChecklist() {
    final Location location = getOnlySelectedLocation();
    if (location == null) {
      return;
    }

    Checklist checklist = checklists.getChecklist(reportSet, taxonomyStore.getClements(), location);
    if (checklist == null || checklist.isBuiltIn()) {
      return;
    }
    
    File saveFile = fileDialogs.saveFile(
        (Frame) SwingUtilities.getWindowAncestor(this),
        Messages.getMessage(Name.SHARE_A_CHECKLIST),
        String.format("checklist-%s.csv", location.getDisplayName()), null, FileType.OTHER);
    if (saveFile != null) {
      ClementsChecklistWriter writer = new ClementsChecklistWriter(taxonomyStore.getClements());
      try {
        writer.write(saveFile, (ClementsChecklist) checklist, location);
      } catch (IOException e) {
        FileDialogs.showFileSaveError(alerts, e, saveFile);
        return;
      }
    }
  }
  
  /** Re-enable everything that checklist creation/editing disabled. */
  private void reenableEverything() {
    locationTree.setEnabled(true);
    editChecklist.setEnabled(true);
    shareChecklist.setEnabled(true);
    xlsButton.setEnabled(true);
    printButton.setEnabled(true);
    reportFilterCombo.setEnabled(true);
    jumpButton.setEnabled(true);
    returnButton.setEnabled(true);
    locationIndexPanel.setEnabled(true);
    totalLabel.setEnabled(true);
    speciesJumpButton.setEnabled(true);
    speciesIndexPanel.setEnabled(true);
    onlyVisitedLocations.setEnabled(true);
    
    updateLocationEditingForSelection();
  }
  
  /**
   * Return the selected location, or null if no or multiple locations
   * are selected.
   */
  private Location getOnlySelectedLocation() {
    TreePath[] paths = locationTree.getSelectionPaths();
    if (paths == null || paths.length != 1) {
      return null;
    } else {
      return (Location) paths[0].getLastPathComponent();
    }
  }
  
  /**
   * Return the selected locations, or an empty list if no locations are selected.
   */
  private List<Location> getSelectedLocations() {
    TreePath[] paths = locationTree.getSelectionPaths();
    List<Location> selectedLocations = Lists.newArrayList();
    if (paths != null) {
      for (TreePath path : paths) {
        selectedLocations.add((Location) path.getLastPathComponent());
      }
    }
    return selectedLocations;
  }

  /**
   * Return the selected VisitInfoKeys, or an empty list if no locations are selected.
   */
  private Set<VisitInfoKey> getSelectedVisitInfoKeys() {
    TreePath[] paths = reportsBrowser.getSelectionPaths();
    Set<VisitInfoKey> selectedVisitInfoKeys = new LinkedHashSet<>();
    if (paths != null) {
      for (TreePath path : paths) {
        Object lastPathComponent = path.getLastPathComponent();
        if (lastPathComponent instanceof VisitInfoKey vik) {
          selectedVisitInfoKeys.add(vik);
        } else if (lastPathComponent instanceof VisitInfoKeyNode vikn) {
          selectedVisitInfoKeys.add(vikn.getVisitInfoKey());
        } else if (lastPathComponent instanceof VisitNode) {
          // Ignore these.
          continue;
        } else {
          // Mixed list - return an empty list.
          return ImmutableSet.of();
        }
      }
    }
    return selectedVisitInfoKeys;
  }

  private void initGUI() {
    locationTree = new LocationTree(
        reportSet, predefinedLocations, Predicates.alwaysTrue(), actionBroker, fontManager, alerts);
    add(cellRendererPane);
    returnButton = new JButton();
    locationTree.setCellRenderer(new LocationTreeCellRenderer());
    treeScrollPane = new JScrollPane(locationTree);
    reportsScrollPane = new JScrollPane(reportsBrowser);
    reportsBrowser.setEditableSightings(true);
    
    reportsOrEditChecklist = new JPanel();
    reportsOrEditChecklist.setLayout(new CardLayout());
    reportsOrEditChecklist.add(reportsScrollPane, "reports");
    
    
    jumpButton = new JButton(Messages.getMessage(Name.JUMP_TO_BUTTON));
    jumpButton.putClientProperty("JComponent.sizeVariant", "small");
    locationIndexPanel = new IndexerPanel<Object>();
    locationIndexPanel.setPreviewText(Name.START_TYPING_A_LOCATION);
    
    speciesJumpButton = new JButton(Messages.getMessage(Name.JUMP_TO_BUTTON));
    speciesJumpButton.putClientProperty("JComponent.sizeVariant", "small");
    speciesIndexPanel = new IndexerPanel<String>();
    
    onlyVisitedLocations = new JCheckBox(Messages.getMessage(Name.ONLY_VISITED_LOCATIONS));
    onlyVisitedLocations.setSelected(browsePreferences.onlyVisitedLocations);

    totalLabel = new JLabel();
    
    locationEditorPanel = new LocationEditorPanel();
    locationEditorPanel.setEnabled(false);
    
    editLocation = new JButton();
    editLocation.setAction(new AbstractAction(Messages.getMessage(Name.EDIT_WITH_ELLIPSIS)) {
      @Override public void actionPerformed(ActionEvent event) {
        editLocation();
      }
    });
    editLocation.setEnabled(false);
    
    deleteLocation = new JButton();
    deleteLocation.setAction(new AbstractAction(Messages.getMessage(Name.DELETE_WITH_ELLIPSIS)) {
      @Override public void actionPerformed(ActionEvent event) {
        deleteLocation();
      }
    });
    deleteLocation.setToolTipText(
        Messages.getMessage(Name.DELETE_LOCATION_HELP));
    deleteLocation.setEnabled(false);

    newLocation = new JButton();
    newLocation.setAction(new AbstractAction(Messages.getMessage(Name.NEW_MENU)) {
      @Override public void actionPerformed(ActionEvent event) {
        newLocation();
      }
    });
    newLocation.setEnabled(false);
    
    reportFilterCombo = new JComboBox<Object>(ReportFilter.values()) {
      @Override public Dimension getMaximumSize() {
        return getPreferredSize();
      }
    };
    reportFilterCombo.setEnabled(false);
    noRarities = new JCheckBox(Messages.getMessage(Name.HIDE_RARITIES));
    noRarities.setEnabled(false);
    
    printButton = new JButton();
    printButton.setAction(new AbstractAction(Messages.getMessage(Name.PRINT_MENU)) {
      @Override public void actionPerformed(ActionEvent event) {
        print();
      }
    });
    printButton.setVisible(false);
    
    xlsButton = new JButton();
    xlsButton.setAction(new AbstractAction(Messages.getMessage(Name.SAVE_AS_SPREADSHEET)) {
      @Override public void actionPerformed(ActionEvent event) {
        saveChecklistAsSpreadsheet();
      }
    });
    xlsButton.setVisible(false);
    
    
    tripReportButton = new JButton();
    tripReportButton.setAction(new AbstractAction(Messages.getMessage(Name.EXPORT_TO_TRIP_REPORT)) {
      @Override public void actionPerformed(ActionEvent event) {
        saveVisitsAsTripReport(event);
      }
    });
    tripReportButton.setVisible(false);

    editChecklist = new JButton();
    editChecklist.setAction(new AbstractAction(Messages.getMessage(Name.EDIT_CHECKLIST)) {
      @Override public void actionPerformed(ActionEvent event) {
        editChecklist(false /* create or edit */);
      }
    });
    editChecklist.setVisible(false);
    
    shareChecklist = new JButton();
    shareChecklist.setAction(new AbstractAction(Messages.getMessage(Name.SHARE_CHECKLIST)) {
      @Override public void actionPerformed(ActionEvent event) {
        shareChecklist();
      }
    });
    shareChecklist.setVisible(false);

    addSightingButton = new JButton();
    addSightingButton.setAction(new AbstractAction(Messages.getMessage(Name.ADD_SIGHTING)) {
      @Override public void actionPerformed(ActionEvent event) {
        TreePath[] selection = reportsBrowser.getSelectionPaths();
        if (selection != null
            && selection.length == 1
            && selection[0].getLastPathComponent() instanceof Resolved) {
          addSighting(selection[0], (Resolved) selection[0].getLastPathComponent());
        }
      }
    });
    removeSightingButton = new JButton();
    removeSightingButton.setAction(new AbstractAction(Messages.getMessage(Name.REMOVE_SIGHTING)) {
      @Override public void actionPerformed(ActionEvent event) {
        removeSightings(event);
      }
    });
    reportsBrowser.getSelectionModel().addTreeSelectionListener(
        e -> updateSightingButtons(reportsBrowser.getSelectionPaths()));
    reportsBrowser.setLocationEditingAllowed(false);

    fontManager.applyTo(this);
  }

  private void removeSightings(ActionEvent event) {
    int okCancel = alerts.showOkCancel(this, Name.REMOVE_SIGHTINGS,
        reportsBrowser.getSelectionCount() > 1
            ? Name.REMOVE_SIGHTINGS_CONFIRM
            : Name.REMOVE_SIGHTING_CONFIRM);
    if (okCancel == JOptionPane.OK_OPTION) {
      reportsBrowser.getCutAction().actionPerformed(event);
    }
  }
  
  private void addSighting(TreePath path, Resolved resolved) {
    // Make sure that only species or less are being added
    Preconditions.checkArgument(
        resolved.getLargestTaxonType().compareTo(Taxon.Type.species) <= 0,
        "Tried to add sighting of type %s", resolved.getLargestTaxonType());
    
    Location location = getOnlySelectedLocation();
    if (location == null) {
      return;
    }
    
    // Ensure that the location - which might be a PredefinedLocation not yet added - has
    // an ID and is added to the LocationSet.
    reportSet.getLocations().ensureAdded(location);
    
    SightingTaxon taxon = resolved.getSightingTaxon();
    Taxonomy baseTaxonomy;
    if (resolved.getTaxonomy() instanceof MappedTaxonomy) {
      MappedTaxonomy mappedTaxonomy = (MappedTaxonomy) resolved.getTaxonomy();
      taxon = mappedTaxonomy.getMapping(taxon);
      baseTaxonomy = mappedTaxonomy.getBaseTaxonomy();
    } else {
      baseTaxonomy = resolved.getTaxonomy();
    }
    
    Sighting sighting = Sighting.newBuilder()
        .setTaxonomy(baseTaxonomy)
        .setLocation(location)
        .setTaxon(taxon)
        .build();
    
    // Guess the status
    Checklist checklist = checklists.getNearestBuiltInChecklist(resolved.getTaxonomy(), reportSet, location);
    SightingStatus status = null;
    // When entering introduced taxa (Feral Pigeon), automatically mark as introduced 
    if (resolved.getTaxonStatus() == Species.Status.IN) {
      status = SightingStatus.INTRODUCED;
    } else if (resolved.getTaxonStatus() == Species.Status.DO) {
      // Status is domestic
      status = SightingStatus.DOMESTIC;
      // ... unless there's a checklist which says the species is introduced here
      if (checklist != null) {
        Checklist.Status checklistStatus = checklist.getStatus(resolved.getTaxonomy(), taxon);
        if (checklistStatus == Checklist.Status.INTRODUCED
            || checklistStatus == Checklist.Status.RARITY_FROM_INTRODUCED) {
          status = SightingStatus.INTRODUCED;
        }
      }
    } else if (checklist != null) {
      // And if the checklist says it's introduced, set introduced (or escaped) status automatically.
      Checklist.Status checklistStatus = checklist.getStatus(resolved.getTaxonomy(), taxon);
      if (checklistStatus == Checklist.Status.INTRODUCED
          || checklistStatus == Checklist.Status.RARITY_FROM_INTRODUCED) {
        status = SightingStatus.INTRODUCED;
      } else if (checklistStatus == Checklist.Status.ESCAPED) {
        status = SightingStatus.INTRODUCED_NOT_ESTABLISHED;
      }
    }

    if (status != null) {
      sighting.getSightingInfo().setSightingStatus(status);
    }
    
    if (defaultUserStore.getUser() != null) {
      sighting.getSightingInfo().setUsers(ImmutableSet.of(defaultUserStore.getUser()));
    }
    
    // Don't update taxa in the query results!
    // see https://bitbucket.org/scythebill/scythebill-birdlist/issues/357/very-odd-behavior-for-species-you-havent for why
    reportsBrowser.addSighting(path, sighting, false /* updateTaxaInQueryResults */);
    // HACK HACK - flipping the selection to the parent then back cleans up the UI.
    reportsBrowser.setSelectionPath(path.getParentPath());
    reportsBrowser.setSelectionPath(path);
  }
  
  private void updateSightingButtons(TreePath[] reportSelection) {
    boolean allAreSightings = true;
    boolean allAreVisitInfoKeys = true;
    if (reportSelection != null) {
      for (TreePath path : reportSelection) {
        Object lastPathComponent = path.getLastPathComponent();
        if (!(lastPathComponent instanceof Sighting)
            && !(lastPathComponent instanceof ResolvedWithSighting)) {
          allAreSightings = false;
        }
        if (!(lastPathComponent instanceof VisitInfoKeyNode)
            && !(lastPathComponent instanceof VisitInfoKey)) {
          allAreVisitInfoKeys = false;
        }
        
        if (!allAreSightings && !allAreVisitInfoKeys) {
          break;
        }
      }
    }
    
    if (reportSelection == null || reportSelection.length != 1) {
      addSightingButton.setEnabled(false);
    } else if (getOnlySelectedLocation() == null) {
      addSightingButton.setEnabled(false);
    } else {
      boolean addEnabled = false;
      if (reportSelection[0].getLastPathComponent() instanceof Resolved) {
        Resolved resolved = (Resolved) reportSelection[0].getLastPathComponent();
        if (resolved.getLargestTaxonType().compareTo(Taxon.Type.species) <= 0) {
          addEnabled = true;
        }
      }
      addSightingButton.setEnabled(addEnabled);
    }
    
    if (reportSelection == null || reportSelection.length == 0) {
      removeSightingButton.setEnabled(false);
      removeSightingButton.setText(Messages.getMessage(Name.REMOVE_SIGHTING));
    } else {
      removeSightingButton.setEnabled(allAreSightings);
      removeSightingButton.setText(reportSelection.length == 1
          ? Messages.getMessage(Name.REMOVE_SIGHTING)
          : Messages.getMessage(Name.REMOVE_SIGHTINGS));
    }
    
    if (removeSightingButton.isEnabled()) {
      removeSightingButton.setVisible(true);
      addSightingButton.setVisible(false);
    } else {
      addSightingButton.setVisible(true);
      removeSightingButton.setVisible(false);
    }
    
    if (reportSelection == null || reportSelection.length == 0) {
      tripReportButton.setVisible(false);
    } else {
      tripReportButton.setVisible(allAreVisitInfoKeys);
    }
  }
  
  /** Filters affecting the list of species shown for a location. */
  enum ReportFilter {
    SHOW_ALL(Name.SHOW_EVERYTHING),
    ONLY_RECORDED(Name.ONLY_RECORDED),
    ONLY_LIFERS(Name.ONLY_LIFERS),
    NOT_SEEN_HERE(Name.NOT_SEEN_HERE),
    ONLY_ENDEMICS(Name.ONLY_ENDEMICS),
    CUSTOM_FILTER(Name.CUSTOM_FILTER);
    
    private final Name text;

    private ReportFilter(Name text) {
      this.text = text;
    }
    
    @Override public String toString() {
      return Messages.getMessage(text);
    }
  }

  static class CustomReportFilter {
    private final QueryDefinition queryDefinition;
    private final Optional<String> optionalName;

    CustomReportFilter(
        QueryDefinition queryDefinition,
        Optional<String> optionalName) {
      this.queryDefinition = queryDefinition;
      this.optionalName = optionalName;
    }
    
    @Override
    public String toString() {
      if (optionalName.isPresent()) {
        return Messages.getFormattedMessage(Name.NEW_FOR_FORMAT, optionalName.get());
      } else {
        return Messages.getMessage(Name.NEW_FOR_CUSTOM_REPORT);
      }
    }

    public QueryDefinition queryDefinition() {
      return queryDefinition;
    }
  }
  
  @Override
  public String getTitle() {
    return Messages.getMessage(Name.BROWSE_BY_LOCATION);
  }

  private class LocationTreeCellRenderer extends DefaultTreeCellRenderer {
    LocationTreeCellRenderer() {
      setFont(fontManager.getTextFont());
    }
    
    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value,
        boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
      Location location = null;
      if (value instanceof Location) {
        location = (Location) value;
        value = LocationIdToString.getStringWithType(location);
      }
      
      Component component = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf,
          row, hasFocus);
      if (location != null) {
        boolean hasLatLong = location.getLatLong().isPresent();
        boolean hasChecklist = checklists.hasChecklist(taxonomyStore.getTaxonomy(), reportSet, location);
        if (location != null && (hasLatLong || hasChecklist)) {
          JPanel panel = new JPanel();
          panel.setOpaque(false);
          panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
          panel.add(component);
          if (hasChecklist) {
            panel.add(Box.createRigidArea(new Dimension(10, 10)));
            panel.add(new JLabel(checkIcon));
          }
          if (hasLatLong) {
            panel.add(Box.createRigidArea(new Dimension(10, 10)));
            JLabel latLongLabel = new JLabel(latLongIcon);
            latLongLabel.putClientProperty(LAT_LONG_PROPERTY, location.getLatLong().get());
            panel.add(latLongLabel);
          }
          return panel;
        }
      }
      return component;
    }    
  }

  static class LocationReportsBrowserPanel extends ReportsBrowserPanel {

    @Override
    protected boolean canImportSightings(TransferSupport transferSupport, TreePath target,
        SightingsGroup sightings) {
      VisitInfoKey visitInfoKey = getVisitInfoKeyForSightingImport(target);
      if (visitInfoKey != null) {
        return !getImportableSightings(sightings, visitInfoKey).isEmpty();
      } else {
        return super.canImportSightings(transferSupport, target, sightings);
      }
    }

    @Override
    protected boolean doImport(TransferSupport transferSupport, SightingsGroup sightings,
        TreePath target) {
      VisitInfoKey visitInfoKey = getVisitInfoKeyForSightingImport(target);
      if (visitInfoKey != null) {
        Location location =
            Preconditions.checkNotNull(getReportSet().getLocations().getLocation(visitInfoKey.locationId()));
        ImmutableList.Builder<Sighting> newSightingsBuilder = ImmutableList.builder();
        for (Sighting sighting : getImportableSightings(sightings, visitInfoKey)) {
          Sighting.Builder builder = sighting.asBuilder();
          builder.setDate(visitInfoKey.date());
          builder.setTime(visitInfoKey.startTime().orNull());
          builder.setLocation(location);
          newSightingsBuilder.add(builder.build());
        }
        
        if (target.getLastPathComponent() instanceof ResolvedWithSighting) {
          target = target.getParentPath();
        }
        addSightings(target, newSightingsBuilder.build(),
            /*updateTaxaInQueryResults=*/false);
        
        return true;
      } else if (target.getLastPathComponent() instanceof Resolved resolved) {
        if (resolved.getType() == SightingTaxon.Type.SINGLE) {
          Taxonomy taxonomy = resolved.getTaxonomy();
          SightingTaxon sightingTaxon;
          if (taxonomy instanceof MappedTaxonomy mapped) {
            sightingTaxon = mapped.getMapping(resolved.getSightingTaxon());
          } else {
            sightingTaxon = resolved.getSightingTaxon();
          }
          
          if (sightingTaxon == null) {
            return false;
          }
          
          ImmutableList.Builder<Sighting> newSightingsBuilder = ImmutableList.builder();
          for (Sighting sighting : getImportableSightings(sightings, sightingTaxon)) {
            Sighting.Builder builder = sighting.asBuilder();
            builder.setTaxon(sightingTaxon);
            newSightingsBuilder.add(builder.build());
          }
          
          addSightings(target, newSightingsBuilder.build(), /*updateTaxaInQueryResults=*/true);
          return true;
        } else {
          return false;
        }
      } else {
        return super.doImport(transferSupport, sightings, target);
      }
    }

    @Override
    protected String getReturnPanelName() {
      return "browseLocations";
    }

    static class ReturnToSighting implements PostNavigateAction {
      private final Sighting sighting;
      private final ReportSet reportSet;

      public ReturnToSighting(ReportSet reportSet, Sighting sighting) {
        this.reportSet = reportSet;
        this.sighting = sighting;
      }

      @Override
      public void postNavigate(JPanel panel) {
        // The previous LocationBrowsePanel is gone;  get the new one 
        LocationBrowsePanel locationBrowsePanel = (LocationBrowsePanel) panel;
        Location location = reportSet.getLocations().getLocation(sighting.getLocationId());
        if (location != null) {
          // ... and select the same location
          locationBrowsePanel.locationTree.selectLocation(location);
          // And force the query to run immediately
          locationBrowsePanel.runQueryForCurrentSelection();
          locationBrowsePanel.updateLocationEditingForSelection();

          // ... and update the visit info (a bit later after layout happens)
          SwingUtilities.invokeLater(
              () -> {
                // ... and select the taxon
                Taxonomy taxonomy = locationBrowsePanel.taxonomyStore.getTaxonomy();
                if (TaxonUtils.areCompatible(sighting.getTaxonomy(), taxonomy)) {
                  Taxon taxon = sighting.getTaxon().resolve(taxonomy).getTaxa().iterator().next();
                  locationBrowsePanel.reportsBrowser.selectTaxon(taxon);
                }
                locationBrowsePanel.reportsBrowser.requestFocus();
                locationBrowsePanel.reportsBrowser.scrollListSelectionsIntoView();
              });
        }
      }
      
    }
    
    @Override
    protected PostNavigateAction returnToSighting(Sighting sighting) {
      return new ReturnToSighting(getReportSet(), sighting);
    }

    private ImmutableList<Sighting> getImportableSightings(SightingsGroup sightings, VisitInfoKey visitInfo) {
      Set<SightingTaxon> existingTaxaForVisit = new LinkedHashSet<>();
      for (Sighting sighting : getReportSet().getSightings()) {
        if (visitInfo.matches(sighting)) {
          existingTaxaForVisit.add(sighting.getTaxon());
        }
      }
      
      ImmutableList.Builder<Sighting> importableSightings = ImmutableList.builder();
      for (Sighting sighting : sightings.getSightings()) {
        if (!existingTaxaForVisit.contains(sighting.getTaxon())) {
          importableSightings.add(sighting);
        }
      }
      return importableSightings.build();
    }
    
    private ImmutableList<Sighting> getImportableSightings(SightingsGroup sightings, SightingTaxon targetTaxon) {
      Set<VisitInfoKey> existingVisitInfosForKey = new LinkedHashSet<>();
      for (Sighting sighting : getReportSet().getSightings()) {
        if (sighting.getTaxon().equals(targetTaxon)) {
          existingVisitInfosForKey.add(VisitInfoKey.forSighting(sighting));
        }
      }
      
      ImmutableList.Builder<Sighting> importableSightings = ImmutableList.builder();
      for (Sighting sighting : sightings.getSightings()) {
        if (!existingVisitInfosForKey.contains(VisitInfoKey.forSighting(sighting))) {
          importableSightings.add(sighting);
        }
      }
      return importableSightings.build();
    }

    private final NavigableFrame navigableFrame;

    @Inject
    public LocationReportsBrowserPanel(FontManager fontManager,
        ReportSet reportSet, ActionBroker actionBroker,
        NewLocationDialog newLocationDialog,
        SpHybridDialog spHybridDialog,
        PredefinedLocations predefinedLocations,
        SpeciesInfoDescriber speciesInfoDescriber,
        OpenMapUrl openMapUrl,
        VisitInfoPreferences visitInfoPreferences,
        QueryPreferences queryPreferences, FileDialogs fileDialogs,
        Alerts alerts, NavigableFrame navigableFrame) {
      super(fontManager, reportSet, actionBroker, newLocationDialog,
          spHybridDialog, predefinedLocations, speciesInfoDescriber, openMapUrl, visitInfoPreferences,
          queryPreferences, fileDialogs, alerts, navigableFrame);
      this.navigableFrame = navigableFrame;
    }
    
    @Override
    protected Action getReturnToCurrentVisitInfoAction(VisitInfoKey visitInfoKey) {
      LocationBrowsePanel locationBrowsePanel = UIUtils.findParentOfType(this, LocationBrowsePanel.class);
      Location selectedLocation = locationBrowsePanel.locationTree.getSelectedLocation();
      return new ReturnToVisitInfoAction(navigableFrame, selectedLocation, visitInfoKey);
    }

    @Override
    protected boolean canImportSightings() {
      return getSelectionCount() == 1;
    }
  }
  
  static class ReturnToVisitInfoAction extends AbstractAction {
    private final NavigableFrame navigableFrame;
    private final Location selectedLocation;
    private final VisitInfoKey visitInfoKey;

    ReturnToVisitInfoAction(NavigableFrame navigableFrame, Location selectedLocation, VisitInfoKey visitInfoKey) {
      this.navigableFrame = navigableFrame;
      this.selectedLocation = selectedLocation;
      this.visitInfoKey = visitInfoKey;
      
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      JPanel navigateTo = navigableFrame.navigateTo("browseLocations");
      if (navigateTo != null) {
        // The previous LocationBrowsePanel is gone;  get the new one 
        final LocationBrowsePanel locationBrowsePanel = (LocationBrowsePanel) navigateTo;
        // ... and select the same location
        locationBrowsePanel.locationTree.selectLocation(selectedLocation);
        // And force the query to run immediately
        locationBrowsePanel.runQueryForCurrentSelection();
        locationBrowsePanel.updateLocationEditingForSelection();

        // ... and update the visit info (a bit later after layout happens)
        SwingUtilities.invokeLater(
            () -> {
              // ... and select the visit info.
              locationBrowsePanel.reportsBrowser.selectVisitInfo(visitInfoKey);
              locationBrowsePanel.reportsBrowser.requestFocus();
              locationBrowsePanel.reportsBrowser.scrollListSelectionsIntoView();
            });
      }
    }
    
  }
  
  @Subscribe
  public void taxonomyChanged(TaxonomyChangedEvent event) {
    setTaxonomy(event.getTaxonomy());
    runQueryForCurrentSelection(); 
    // Update the location filter, since "visited" locations are per taxonomy
    if (onlyVisitedLocations.isSelected()) {
      updateLocationFilter();
    }
  }
  
  @Subscribe
  public void defaultUserChanged(DefaultUserChangedEvent event) {
    startScanningTaxa(reportsBrowser.getTaxonomy());
    runQueryForCurrentSelection(); 
  }
  
  private void setTaxonomy(Taxonomy taxonomy) {
    reportsBrowser.setTaxonomy(taxonomy);
    speciesIndexerPanelConfigurer.configure(speciesIndexPanel, taxonomy);
    
    // Add all the families to the indexer
    speciesIndexerPanelConfigurer.addSimpleTaxa(speciesIndexPanel, taxonomy,
        TaxonUtils.getDescendantsOfType(taxonomy.getRoot(), Taxon.Type.family));
    startScanningTaxa(taxonomy);
  }

  private void updateLocationFilter() {
    locationFilter = getLocationFilter();
    locationTree.setLocationFilter(locationFilter);
  }
  
  private Predicate<Location> getLocationFilter() {
    if (!onlyVisitedLocations.isSelected()) {
      return Predicates.alwaysTrue();
    }
    
    try {
      // TODO: move to the background if necessary, but this is *probably* fast?
      EncounteredLocations encounteredLocations =
          EncounteredLocations.scan(taxonomyStore.getTaxonomy(), reportSet, executorService).get();
      return new Predicate<Location>() { 
        @Override
        public boolean apply(Location location) {
          return encounteredLocations.encounteredLocation(location) || addedLocations.contains(location);
        }
      };
    } catch (InterruptedException | ExecutionException e) {
      // Rare error - don't bother translating
      alerts.showError(this, e, "Could not scan locations", "Location scanning failed.");
      return Predicates.alwaysTrue();
    }
  }

  private void startScanningTaxa(Taxonomy taxonomy) {
    if (scanSeenTaxa != null) {
      scanSeenTaxa.cancel();
    }
    Predicate<Sighting> sightingPredicate = sightingPredicate(taxonomy);
    scanSeenTaxa = ScanSeenTaxa.start(reportSet, taxonomy, executorService,
        scanned -> {
          if (currentQueryResults != null) {
            runQueryForCurrentSelection();
          }
        },
        Lists.newArrayList((Location) null), sightingPredicate, new ScanSeenTaxa.Options());
    reportsBrowser.setSeenTaxa(scanSeenTaxa);
  }

  private Predicate<Sighting> sightingPredicate(Taxonomy taxonomy) {
    Object reportFilter = reportFilterCombo.getSelectedItem();
    Predicate<Sighting> sightingPredicate;
    if (reportFilter instanceof CustomReportFilter) {
      sightingPredicate = ((CustomReportFilter) reportFilter).queryDefinition.predicate();
      customScanner = true;
    } else {
      // Only consider things "seen" if they match the user's preference
      sightingPredicate = queryPreferences.getCountablePredicate(taxonomy, false, null);
      customScanner = false;
    }
    
    if (defaultUserStore.getUser() != null) {
      sightingPredicate = Predicates.and(
          sightingPredicate,
          SightingPredicates.includesUser(defaultUserStore.getUser()));
    }
    return sightingPredicate;
  }
  
  private LatLongCoordinates isOverLatLongIcon(TreePath path, Point locationTreePoint) {
    if (path == null || !(path.getLastPathComponent() instanceof Location)) {
      return null;
    }
    
    Location location = (Location) path.getLastPathComponent();
    if (!location.getLatLong().isPresent()) {
      return null;
    }
    
    Container container = getValidatedRendererComponent(path);
    
    for (int i = 0; i < container.getComponentCount(); i++) {
      Component child = container.getComponent(i);
      if (child instanceof JComponent jChild
          && jChild.getClientProperty(LAT_LONG_PROPERTY) != null) {
        Rectangle bounds = child.getBounds();
        Point pathLocation = locationTree.getPathBounds(path).getLocation();
        if (bounds.contains(locationTreePoint.getX() - pathLocation.x, locationTreePoint.getY() - pathLocation.y)) {
          return location.getLatLong().get();
        }              
      }
    }
    
    return null;
  }

  private Container getValidatedRendererComponent(TreePath path) {
    if (path == null || !(path.getLastPathComponent() instanceof Location)) {
      return null;
    }
    
    if (path.equals(cachedRendererComponentPath)
        && cellRendererPane.getComponentCount() == 1) {
      return (Container) cellRendererPane.getComponent(0);
    }
    
    Location location = (Location) path.getLastPathComponent();
    TreeCellRenderer renderer = locationTree.getCellRenderer();
    int row = locationTree.getRowForPath(path);
    boolean focused = locationTree.getSelectionModel().getLeadSelectionRow() == row
        && locationTree.isFocusOwner();
    Container container = (Container) renderer.getTreeCellRendererComponent(
        locationTree, location,
        locationTree.getSelectionModel().isPathSelected(path),
        locationTree.isExpanded(path),
        locationTree.getModel().isLeaf(location),
        locationTree.getRowForPath(path),
        focused);
    Rectangle pathBounds = locationTree.getPathBounds(path);
    cellRendererPane.removeAll();
    cellRendererPane.add(container);
    container.setSize(pathBounds.getSize());
    container.validate();
    cachedRendererComponentPath = path;
    return container;
  }
  
  @Override
  public void fontsUpdated(FontManager fontManager) {
    GroupLayout layout = new GroupLayout(this);
    layout.setAutoCreateContainerGaps(true);
    setLayout(layout);
    
    cachedRendererComponentPath = null;
    layout.setVerticalGroup(layout.createSequentialGroup()
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(locationIndexPanel)
            .addComponent(jumpButton)
            .addComponent(speciesIndexPanel)
            .addComponent(speciesJumpButton)
            .addComponent(onlyVisitedLocations))
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(reportFilterCombo)
            .addComponent(noRarities)
            .addComponent(tripReportButton)
            .addComponent(printButton)
            .addComponent(xlsButton)
            .addComponent(totalLabel)
            .addComponent(editChecklist)
            .addComponent(shareChecklist))
        .addGroup(layout.createParallelGroup()
          .addComponent(treeScrollPane, fontManager.scale(200), fontManager.scale(600), Integer.MAX_VALUE)
          .addComponent(reportsOrEditChecklist, fontManager.scale(200), fontManager.scale(600), Integer.MAX_VALUE))
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(locationEditorPanel)
            .addComponent(editLocation)
            .addComponent(deleteLocation)
            .addComponent(newLocation)
            .addComponent(addSightingButton)
            .addComponent(removeSightingButton)
            .addComponent(returnButton)));
    layout.setHorizontalGroup(layout.createParallelGroup()
        .addGroup(Alignment.LEADING, layout.createSequentialGroup()
            .addComponent(locationIndexPanel)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(jumpButton)
            .addPreferredGap(ComponentPlacement.UNRELATED)
            .addComponent(speciesIndexPanel)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(speciesJumpButton)
            .addPreferredGap(ComponentPlacement.UNRELATED)
            .addComponent(onlyVisitedLocations))
        .addGroup(Alignment.LEADING, layout.createSequentialGroup()
            .addComponent(totalLabel)
            .addPreferredGap(ComponentPlacement.UNRELATED)
            .addComponent(reportFilterCombo, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addComponent(noRarities, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
        .addGroup(Alignment.TRAILING, layout.createSequentialGroup()
            .addComponent(tripReportButton)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(printButton)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(xlsButton)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(editChecklist)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(shareChecklist))
        .addGroup(Alignment.LEADING, layout.createSequentialGroup()
            .addComponent(treeScrollPane, fontManager.scale(150), fontManager.scale(250), fontManager.scale(300))
            .addComponent(reportsOrEditChecklist, fontManager.scale(400), GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE))
        .addGroup(Alignment.LEADING, layout.createSequentialGroup()
            .addComponent(locationEditorPanel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(editLocation, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(deleteLocation, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(newLocation, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
        .addGroup(Alignment.TRAILING, layout.createSequentialGroup()
            .addComponent(addSightingButton, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addComponent(removeSightingButton, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addPreferredGap(ComponentPlacement.UNRELATED)
            .addComponent(returnButton, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)));
    Dimension size = fontManager.scale(new Dimension(1280, 800));
    setPreferredSize(size);
    ((LocationTreeCellRenderer) locationTree.getCellRenderer()).setFont(fontManager.getTextFont());
  }  
}
