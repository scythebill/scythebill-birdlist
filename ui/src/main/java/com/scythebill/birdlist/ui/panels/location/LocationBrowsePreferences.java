/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.location;

/**
 * Saved preferences for location browsing, but saved in a reportset
 * instead of global.
 */
public class LocationBrowsePreferences {
  /**
   * Last ID of a location. Note that this is intentionally *not* marked as a @Preference, and
   * therefore not persisted! Problem is that we definitely don't want to "dirty" the reportset
   * (force an explicit save) just for paging around in the browser. That means that this preference
   * is going to be very inconsistently saved... which makes it potentially more confusing than
   * useful. For now, leave it without saving, which effectively makes it a reportset-local variable
   * for a single run.
   */
  String lastLocationId;
}
