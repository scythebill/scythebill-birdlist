/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.location;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.google.common.base.Objects;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Panel supporting inline editing of a single location.
 * 
 * Doesn't actually do much these days - supports quick-editing of name, but most editing goes
 * to the edit dialog, alas, as locations are too complicated to support trivial inline editing.
 */
class LocationEditorPanel extends JPanel {
  private Location lastLocation = null;
  private JTextField name;
  private JLabel nameLabel;
  private boolean dontUpdateValue;

  public LocationEditorPanel() {
    super();
    initGUI();
    hookUpContents();
  }

  private void hookUpContents() {
    name.addFocusListener(new FocusAdapter() {
      @Override public void focusLost(FocusEvent e) {
        updateValue();
      }
    });
    name.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent arg0) {
        updateValue();
      }
    });
  }
  
  private void updateValue() {
    if (!dontUpdateValue) {
      Location oldValue = this.lastLocation;
      Location newValue = getValue();
      if (!Objects.equal(oldValue, newValue)) {
        this.lastLocation = newValue;
        firePropertyChange("value", oldValue, newValue);
      }
    }
  }
    
  private void initGUI() {
    BoxLayout layout = new BoxLayout(this, BoxLayout.LINE_AXIS);
    setLayout(layout);

    nameLabel = new JLabel(Messages.getMessage(Name.NAME_LABEL));
    name = new JTextField();
    name.setColumns(20);

    add(nameLabel);
    add(name);
  }
  
  public Location getValue() {
    if (lastLocation == null) {
      return null;
    }
    
    if ("".equals(name.getText())) {
      return null;
    }
    
    return lastLocation.asBuilder()
        .setName(name.getText())
        .build();
  }
  
  public void setLocation(Location location) {
    dontUpdateValue = true;
    
    lastLocation = location;
    if (location == null) {
      name.setText("");
    } else {
      name.setText(location.getDisplayName());
    }

    dontUpdateValue = false;
  }

  @Override
  public void setEnabled(boolean enabled) {
    super.setEnabled(enabled);
    nameLabel.setEnabled(enabled);
    name.setEnabled(enabled);
  }
}
