/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.location;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URI;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.EventListener;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import com.google.common.base.Charsets;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.io.Resources;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningScheduledExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.ui.actions.locationapi.google.GoogleStaticGeoImageLoader;
import com.scythebill.birdlist.ui.components.AutoSelectJFormattedTextField;
import com.scythebill.birdlist.ui.components.ProgressSpinner;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Displays a panel that loads images based on lat-long coordinates.
 */
public class LocationPreviewPanel extends JPanel {
  private static final Logger logger = Logger.getLogger(LocationPreviewPanel.class.getName());
  private static final int ZOOM_BUTTON_SIZE = 24;
  private static final int MAX_ZOOM_LEVEL = 18;
  private static final int MIN_ZOOM_LEVEL = 0;
  private int zoomLevel = 12;
  private GoogleStaticGeoImageLoader staticImageLoader;
  private LatLongCoordinates latLong;
  private ProgressSpinner spinner;
  private JLabel imageLabel;
  private JLabel zoomInLabel;
  private JLabel zoomOutLabel;
  private ListenableFuture<BufferedImage> currentImageLoad;
  private MouseAdapter mapClickedMouseAdapter;
  private MouseAdapter zoomInMouseAdapter;
  private MouseAdapter zoomOutMouseAdapter;

  public static void main(String[] args) throws IOException {
    CloseableHttpClient httpClient = HttpClients.custom()
        .disableCookieManagement()
        .setMaxConnPerRoute(3)
        .setMaxConnTotal(20)
        .setConnectionManager(new PoolingHttpClientConnectionManager())
        .build();
    JFrame frame = new JFrame();
    final JFormattedTextField latitudeField;
    final JFormattedTextField longitudeField;
    latitudeField = new AutoSelectJFormattedTextField();
    latitudeField.setColumns(10);
    NumberFormatter latitudeFormatter = allowNullDecimalFormatter();
    latitudeFormatter.setValueClass(Double.class);
    latitudeFormatter.setMinimum(-90.0d);
    latitudeFormatter.setMaximum(90.0d);
    latitudeFormatter.setCommitsOnValidEdit(true);
    latitudeField.setFormatterFactory(new DefaultFormatterFactory(latitudeFormatter));
    
    longitudeField = new AutoSelectJFormattedTextField();
    longitudeField.setColumns(10);
    NumberFormatter longitudeFormatter = allowNullDecimalFormatter();
    longitudeFormatter.setValueClass(Double.class);
    longitudeFormatter.setMinimum(-180.0d);
    longitudeFormatter.setMaximum(180.0d);
    longitudeFormatter.setCommitsOnValidEdit(true);
    longitudeField.setFormatterFactory(new DefaultFormatterFactory(longitudeFormatter));
    
    JButton ok = new JButton("OK");
    
    ListeningScheduledExecutorService executorService = MoreExecutors.listeningDecorator(Executors.newScheduledThreadPool(3));
    final LocationPreviewPanel previewPanel = new LocationPreviewPanel(
        new GoogleStaticGeoImageLoader(httpClient, executorService,
            Resources.toString(Resources.getResource("googleapikey.txt"), Charsets.UTF_8)));
    previewPanel.setPreferredSize(new Dimension(500, 500));
    Box box = Box.createVerticalBox();
    box.add(previewPanel);
    box.add(latitudeField);
    box.add(longitudeField);
    box.add(ok);
    ok.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent arg0) {
        Double latitude = (Double) latitudeField.getValue();
        Double longitude = (Double) longitudeField.getValue();
        previewPanel.setLatLong(LatLongCoordinates.withLatAndLong(latitude, longitude));
      }
      
    });
    
    previewPanel.addMapClickedListener(new MapClickedListener() {
      @Override
      public void mapClicked(LatLongCoordinates latLong) {
        String url = String.format("http://maps.google.com/maps/place/%s,%s/@%s,%s,%sz",
            latLong.latitude(),
            latLong.longitude(),
            latLong.latitude(),
            latLong.longitude(),
            12);
        try {
          Desktop.getDesktop().browse(URI.create(url));
        } catch (IOException e) {
          logger.log(Level.SEVERE, "Failed to open URL", e);
        }
      }
    });
    
    frame.setContentPane(box);
    frame.setVisible(true);
    frame.pack();
  }
  

  public interface MapClickedListener extends EventListener {
    void mapClicked(LatLongCoordinates latLong);
  }
  
  @Inject
  public LocationPreviewPanel(
      GoogleStaticGeoImageLoader staticImageLoader) {
    this.staticImageLoader = staticImageLoader;
    this.spinner = new ProgressSpinner();
    this.imageLabel = new JLabel();
    this.zoomInLabel = newClickableLabel("+");
    this.zoomOutLabel = newClickableLabel("-");
    zoomInLabel.setVisible(false);
    zoomOutLabel.setVisible(false);
    setLayout(null);
    add(spinner);
    add(imageLabel);
    add(zoomInLabel);
    add(zoomOutLabel);
  }
  
  public void addMapClickedListener(MapClickedListener listener) {
    listenerList.add(MapClickedListener.class, listener);
  }
  
  public void removeMapClickedListener(MapClickedListener listener) {
    listenerList.remove(MapClickedListener.class, listener);
  }

  private JLabel newClickableLabel(String string) {
    JLabel label = new JLabel(string);
    label.setOpaque(true);
    label.setBorder(new LineBorder(Color.BLACK, 1));
    label.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    label.setBackground(Color.WHITE);
    label.setHorizontalAlignment(SwingConstants.CENTER);
    label.setVerticalAlignment(SwingConstants.CENTER);
    label.setHorizontalTextPosition(SwingConstants.CENTER);
    label.setVerticalTextPosition(SwingConstants.CENTER);
    return label;
  }

  @Override
  public void doLayout() {
    super.doLayout();
    
    Dimension imageSize = imageLabel.getPreferredSize();
    imageLabel.setBounds(0, 0, imageSize.width, imageSize.height);
    Dimension spinnerSize = spinner.getPreferredSize();
    spinner.setLocation(
        (imageSize.width - spinnerSize.width) / 2,
        (imageSize.height - spinnerSize.height) / 2);
    spinner.setSize(spinnerSize);
    
    zoomInLabel.setSize(ZOOM_BUTTON_SIZE, ZOOM_BUTTON_SIZE);
    zoomOutLabel.setSize(ZOOM_BUTTON_SIZE, ZOOM_BUTTON_SIZE);
    
    zoomInLabel.setLocation(imageSize.width, 0);
    zoomOutLabel.setLocation(imageSize.width, ZOOM_BUTTON_SIZE - 1);
  }
    
  private void setErrorLoadingImage() {
    imageLabel.setText(Messages.getMessage(Name.ERROR_LOADING_IMAGE));
    imageLabel.setIcon(null);
  }

  private static NumberFormatter allowNullDecimalFormatter() {
    NumberFormat numberFormat = NumberFormat.getNumberInstance();
    numberFormat.setMaximumIntegerDigits(3);
    numberFormat.setMaximumFractionDigits(6);
    return new NumberFormatter(numberFormat) {
      @Override
      public Object stringToValue(String string) throws ParseException {
        if ("".equals(string)) {
          return null;
        }
        return super.stringToValue(string);
      }
    };
  }

  public LatLongCoordinates getLatLong() {
    return latLong;
  }
  
  public void setLatLong(LatLongCoordinates latLong) {
    if (Objects.equal(latLong, this.latLong)) {
      return;
    }
    
    removeClickable();
    
    this.latLong = latLong;
    reloadImage();
  }

  private void reloadImage() {
    if (latLong == null) {
      imageLabel.setIcon(null);
      imageLabel.setText(null);
      zoomInLabel.setVisible(false);
      zoomOutLabel.setVisible(false);
      spinner.stop();
      return;
    }
    
    if (currentImageLoad != null) {
      currentImageLoad.cancel(true);
    }
    
    Dimension size = getSize();
    // Try to fill the image to the entire panel size, but restricting per image loader limitations
    Dimension imageSize = staticImageLoader.allowedDimension(size);
    // Make sure there's enough room left over to fit the zoom buttons
    imageSize.width = Math.min(imageSize.width, size.width - ZOOM_BUTTON_SIZE);
    imageLabel.setPreferredSize(imageSize);
    revalidate();
    
    spinner.start();
    currentImageLoad = staticImageLoader.asyncLoad(latLong, imageSize, zoomLevel);
    
    Futures.addCallback(currentImageLoad, new FutureCallback<BufferedImage>() {
      @Override
      public void onFailure(Throwable t) {
        // Ignore cancellation exceptions - that means that we've
        // stopped loading
        if (!(t instanceof CancellationException)) {
          SwingUtilities.invokeLater(new Runnable() {
            @Override public void run() {
              currentImageLoad = null;
              setErrorLoadingImage();
              zoomInLabel.setVisible(false);
              zoomOutLabel.setVisible(false);
              spinner.stop();
            }
          });
        }
      }

      @Override
      public void onSuccess(final BufferedImage image) {
        // Get back to the event queue to do anything interesting
        SwingUtilities.invokeLater(new Runnable() {
          @Override public void run() {
            LatLongCoordinates currentLatLong = LocationPreviewPanel.this.latLong;
            if (currentLatLong != null) {
              makeClickable(LocationPreviewPanel.this.latLong);
            } else {
              removeClickable();
            }
            currentImageLoad = null;
            spinner.stop();
            zoomInLabel.setVisible(image != null);
            zoomOutLabel.setVisible(image != null);
            imageLabel.setIcon(image == null
                ? null : new ImageIcon(image));
            imageLabel.setText(null);
            repaint();
          }
        });
      }
    }, MoreExecutors.directExecutor());
  }

  private void removeClickable() {
    imageLabel.setCursor(null);
    if (mapClickedMouseAdapter != null) {
      imageLabel.removeMouseListener(mapClickedMouseAdapter);
      zoomInLabel.removeMouseListener(zoomInMouseAdapter);
      zoomOutLabel.removeMouseListener(zoomOutMouseAdapter);
      mapClickedMouseAdapter = null;
    }
  }
  
  private void makeClickable(final LatLongCoordinates latLong) {
    Preconditions.checkNotNull(latLong);
    imageLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    if (mapClickedMouseAdapter != null) {
      imageLabel.removeMouseListener(mapClickedMouseAdapter);
    }
    if (zoomInMouseAdapter != null) {
      zoomInLabel.removeMouseListener(zoomInMouseAdapter);
    }
    if (zoomOutMouseAdapter != null) {
      zoomOutLabel.removeMouseListener(zoomOutMouseAdapter);
    }
    
    mapClickedMouseAdapter = new MouseAdapter() {      
      @Override public void mouseClicked(MouseEvent event) {
        LatLongCoordinates clicked = MercatorProjection.point(
            latLong, zoomLevel, imageLabel.getWidth(), imageLabel.getHeight(), event.getPoint());
       for (MapClickedListener listener : getListeners(MapClickedListener.class)) {
          listener.mapClicked(clicked);
        }
      }
    };
    zoomInMouseAdapter = new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        if (latLong != null && zoomLevel < MAX_ZOOM_LEVEL) {
          zoomLevel++;
          reloadImage();
        }
      }
    };
    zoomOutMouseAdapter = new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        if (latLong != null && zoomLevel > MIN_ZOOM_LEVEL) {
          zoomLevel--;
          reloadImage();
        }
      }
    };
    imageLabel.addMouseListener(mapClickedMouseAdapter);    
    zoomInLabel.addMouseListener(zoomInMouseAdapter);
    zoomOutLabel.addMouseListener(zoomOutMouseAdapter);
  }
}
