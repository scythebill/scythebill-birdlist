/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.location;

import java.awt.Component;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.FlavorEvent;
import java.awt.datatransfer.FlavorListener;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Action;
import javax.swing.DropMode;
import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.TransferHandler;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Location.Type;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.ui.actions.ActionBroker;
import com.scythebill.birdlist.ui.actions.Attachable;
import com.scythebill.birdlist.ui.actions.ForwardingAction;
import com.scythebill.birdlist.ui.datatransfer.SightingsGroup;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FixDropTargetScrolling;
import com.scythebill.birdlist.ui.util.FocusTracker;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Tree for displaying location information.
 */
public class LocationTree extends JTree {
  private final ReportSet reportSet;
  private final PredefinedLocations predefinedLocations;
  private final FontManager fontManager;
  private final ActionBroker actionBroker;
  private final List<LocationsEditedListener> locationsEditedListeners =
      Lists.newCopyOnWriteArrayList();
  private final Alerts alerts;
  private final Timer hoverTimer;

  /** Interface called to receive a generic "the tree state has changed" notification. */ 
  public interface LocationsEditedListener {
    void onEdited();
  }
  
  public LocationTree(
      ReportSet reportSet,
      PredefinedLocations predefinedLocations,
      Predicate<Location> locationFilter,
      ActionBroker actionBroker,
      FontManager fontManager,
      Alerts alerts) {
    super(new LocationTreeModel(reportSet.getLocations(), predefinedLocations, locationFilter));
    this.predefinedLocations = predefinedLocations;
    this.actionBroker = actionBroker;
    this.fontManager = fontManager;
    this.alerts = alerts;
    setRootVisible(false);
    setDragEnabled(true);
    setDropMode(DropMode.ON);
    setTransferHandler(new LocationTransferHandler());
    this.reportSet = reportSet;
        
    // A timer for automatically expanding nodes as a user hovers
    this.hoverTimer = new Timer(500, 
        e -> {
          DropLocation dropLocation = getDropLocation();
          if (dropLocation != null) {
            TreePath path = dropLocation.getPath();
            if (path != null) {
              expandPath(path);
            }
          }
        });
    this.hoverTimer.setRepeats(false);
    
    new LocationTreeFocusTracker();
    
    FixDropTargetScrolling.install(this, new Insets(10, 0, 10, 0));
    
    addPropertyChangeListener(event -> {
      if ("dropLocation".equals(event.getPropertyName())) {
        JTree.DropLocation oldValue = (JTree.DropLocation) event.getOldValue();
        // Part 1 of Quaqua hack for
        // https://bitbucket.org/scythebill/scythebill-birdlist/issues/91
        // - Track dropLocation changes and force repainting
        if (isQuaqua()) {
          repaintDropLocation(oldValue);
          repaintDropLocation(getDropLocation());
        }

        if (oldValue == null) {
          hoverTimer.stop();
        } else {
          hoverTimer.restart();
        }
      }
    });
  }

  @Override
  public void doLayout() {
    Font font = fontManager.getTextFont();
    setRowHeight(UIUtils.getFontHeight(getGraphics(), font) + 4);
    super.doLayout();
  }

  private void repaintDropLocation(JTree.DropLocation loc) {
    if (loc == null) {
      return;
    }
    
    TreePath path = loc.getPath();
    if (path != null) {
      Rectangle r = getPathBounds(path);
      // To deal with Quaqua behavior (which draws selection the full width of the tree),
      // repaint the entire width
      r.x = 0;
      r.width = getWidth();
      repaint(r);
    }
  }
  
  public void setLocationFilter(Predicate<Location> locationFilter) {
    List<Location> selectedLocations = new ArrayList<>();
   
    if (getSelectionPaths() != null) {
      for (TreePath treePath : getSelectionPaths()) {
        Object lastPathComponent = treePath.getLastPathComponent();
        if (lastPathComponent instanceof Location) {
          Location location = (Location) lastPathComponent;
          if (locationFilter.apply(location)) {
            selectedLocations.add(location);
          }
        }
      }
    }
    
    setModel(new LocationTreeModel(reportSet.getLocations(), predefinedLocations, locationFilter));
    
    selectLocations(selectedLocations);
    
  }

  public void addLocationsEditedListener(LocationsEditedListener listener) {
    locationsEditedListeners.add(listener);
  }
  
  public Location getSelectedLocation() {
    TreePath treePath = getSelectionPath();
    if (treePath == null) {
      return null;
    }
    
    Object lastPathComponent = treePath.getLastPathComponent();
    if (lastPathComponent instanceof Location) {
      return (Location) lastPathComponent;
    }
    return null;
  }
  
  public void selectLocation(Location location) {
    TreePath treePath = getTreePath(location);
    setSelectionPath(treePath);
    setExpandedState(treePath, true);
    scrollPathToVisible(treePath);
  }

  public void selectLocations(Iterable<Location> locations) {
    List<TreePath> treePaths = Lists.newArrayList();
    for (Location location : locations) {
      treePaths.add(getTreePath(location));
    }
    setSelectionPaths(treePaths.toArray(new TreePath[0]));
    if (!treePaths.isEmpty()) {
      setExpandedState(treePaths.get(0), true);
      scrollPathToVisible(treePaths.get(0));
    }
  }

  private void fireLocationsEditedEvent() {
    for (LocationsEditedListener listener : locationsEditedListeners) {
      listener.onEdited();
    }
  }
  
  TreePath getTreePath(Location location) {
    if (location == null) {
      return new TreePath(getModel().getRoot());
    }
    
    // Ensure that we're using a Location that the model is happy with, since Location
    // might not be added yet if it's a predefined location
    location = ((LocationTreeModel) getModel()).findActualLocation(location);
    return getTreePath(location.getParent()).pathByAddingChild(location);
  }
  
  static class LocalLocationsSet {
    final ImmutableList<Location> locations;

    LocalLocationsSet(List<Location> locations) {
      this.locations = ImmutableList.copyOf(locations); 
    }
  }
  
  private static final DataFlavor LOCATIONS_FLAVOR;
  static {
    try {
      LOCATIONS_FLAVOR = new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType +
             ";class=" + LocalLocationsSet.class.getName());
    } catch (ClassNotFoundException e) {
      throw new RuntimeException(e);
    }
  }
  
  /** TransferHandler implementation for the Location tree. */
  private class LocationTransferHandler extends TransferHandler {
    @Override protected Transferable createTransferable(JComponent c) {
      TreePath[] paths = getSelectionPaths();
      if (paths == null || paths.length == 0) {
        return null;
      }
      
      Location commonParent = null;
      ImmutableList.Builder<Location> locations = ImmutableList.builder();
      // Build the list of locations to transfer;  only allow a transfer if:
      //  - no locations are built-in *regions* (but allow transferring countries
      //    to make it easy-ish to build meta-regions like "East Asia")
      //  - all locations share a common parent
      // TODO: weaken the latter requirement;  it should be sufficient for
      // no one location in the set to be a parent of another.
      for (TreePath path : paths) {
        Location location = (Location) path.getLastPathComponent();
        if (location.isBuiltInLocation() && location.getType() == Type.region) {
          return null;
        }
        
        if (commonParent == null) {
          commonParent = location.getParent();
        } else if (location.getParent() != commonParent) {
          return null;
        }
        
        locations.add(location);
      }
      
      return new LocationTransferable(locations.build());
    }

    @Override public int getSourceActions(JComponent component) {
      return MOVE;
    }
    
    @Override public boolean canImport(JComponent component, DataFlavor[] flavors) {
      // For drag-and-drop, the legacy canImport() is badly broken.  Fail fast.
      throw new UnsupportedOperationException("Legacy canImport() not supported");
    }

    @Override public boolean canImport(TransferSupport transferSupport) {
      TreePath target = getDropTarget(transferSupport);
      if (target == null) {
        return false;
      }
      
      if (!(target.getLastPathComponent() instanceof Location)) {
        return false;
      }
      
      Location targetLocation = (Location) target.getLastPathComponent();
      
      if (transferSupport.isDataFlavorSupported(LOCATIONS_FLAVOR)) {
        if (!transferSupport.isDrop()) {
          return false;
        }
        ImmutableList<Location> sourceLocations = getLocations(transferSupport);
        // Don't allow dropping a location onto one of its one children
        for (Location sourceLocation : sourceLocations) {
          if (targetLocation == sourceLocation ||
              Locations.isDescendentOfLocation(sourceLocation, targetLocation)) {
            return false;
          }
        }
        
        return true;
      } else if (transferSupport.isDataFlavorSupported(SightingsGroup.FLAVOR)) {
        return true;
      } else {
        return false;
      }
      
    }

    @Override public boolean importData(JComponent component, Transferable data) {
      throw new UnsupportedOperationException("Legacy importData() not supported.");
    }

    @Override public boolean importData(TransferSupport transferSupport) {
      if (!canImport(transferSupport)) {
        return false;        
      }

      try {
        TreePath target = getDropTarget(transferSupport);
        Location targetLocation = (Location) target.getLastPathComponent();
        
        if (transferSupport.isDataFlavorSupported(LOCATIONS_FLAVOR)) {
          // Dropping a location moves from one parent to another
          ImmutableList<Location> sourceLocations = getLocations(transferSupport);
          
          // Preserve all the currently expanded locations
          Set<Location> expandedLocations = Sets.newLinkedHashSet();
          for (int i = 0; i < getRowCount(); i++) {
            if (isExpanded(i)) {
              Location location = (Location) getPathForRow(i).getLastPathComponent();
              expandedLocations.add(location);
            }
          }
          
          Point scrollPoint = null;
          if (getParent() instanceof JViewport) {
            scrollPoint = ((JViewport) getParent()).getViewPosition();
          }
          
          Set<Location> oldSourceParents = Sets.newHashSet();
          // Make sure - if this is a predefined location not yet added - that
          // it (and any of its parents) are added.
          reportSet.getLocations().ensureAdded(targetLocation);
          
          // If the target location already has a child with a given name, STOP.
          for (Location sourceLocation : sourceLocations) {
            if (targetLocation.getContent(sourceLocation.getModelName()) != null
                || predefinedLocations.getPredefinedLocationChildThatHasNotBeenCreatedYet(
                    reportSet.getLocations(), targetLocation, sourceLocation.getModelName()) != null) {
              SwingUtilities.invokeLater(() -> {
                // TODO: this is ugly;  the model won't allow adding "model" name, but this is
                // adding a "display name".  In particular, this will be pretty brutal for rearranging
                // built-in names.
                alerts.showError(LocationTree.this,
                    Name.LOCATION_WITH_NAME_ALREADY_EXISTS_TITLE,
                    Name.LOCATION_WITH_NAME_ALREADY_EXISTS_FORMAT,
                    sourceLocation.getModelName(), targetLocation.getDisplayName());
              });
              return false;
            }
          }
          
          // And make sure the *source* locations are also yet added (in
          // case you are reparenting a built-in location that you haven't used
          // yet - for example, grouping counties or states in an as-yet unvisited area)
          for (Location sourceLocation : sourceLocations) {
            reportSet.getLocations().ensureAdded(sourceLocation);
          }
          
          for (Location sourceLocation : sourceLocations) {
            // Remember each parent that is losing a child (and therefore must be repainted)
            oldSourceParents.add(sourceLocation.getParent());
            // And now reparent the location
            sourceLocation.reparent(targetLocation);
          }
          
          for (Location oldSourceParent : oldSourceParents) {
            getModel().valueForPathChanged(getTreePath(oldSourceParent), oldSourceParent);
          }
          getModel().valueForPathChanged(getTreePath(targetLocation), targetLocation);
          selectLocations(sourceLocations);
          
          // Re-expand all the paths
          for (Location expandedLocation : expandedLocations) {
            expandPath(getTreePath(expandedLocation));
          }
          reportSet.getLocations().markDirty();
          
          if (scrollPoint != null) {
            ((JViewport) getParent()).setViewPosition(scrollPoint);
          }
          Rectangle selectedBounds = getSelectionBounds();
          if (selectedBounds != null) {
            scrollRectToVisible(selectedBounds);
          }
  
          fireLocationsEditedEvent();
          return true;
        } else if (transferSupport.isDataFlavorSupported(SightingsGroup.FLAVOR)) {
          // Dropping a sighting just changes the location of that sighting
          SightingsGroup sightingsGroup = SightingsGroup.fromTransferable(transferSupport.getTransferable());
          List<Sighting> newSightings = Lists.newArrayList();
          reportSet.getLocations().ensureAdded(targetLocation);
          for (Sighting.Builder sighting : sightingsGroup.sightingBuilders()) {
            sighting.setLocation(targetLocation);
            newSightings.add(sighting.build());
          }
          reportSet.mutator().adding(newSightings).mutate();
          
          // Restore any visit info keys at the new locations
          for (Map.Entry<VisitInfoKey, VisitInfo> entry : sightingsGroup.getVisitInfoMap().entrySet()) {
            VisitInfoKey updatedKey = entry.getKey().withLocationId(targetLocation.getId());
            // If there's no visit info at the new key, store the old visit info there
            if (reportSet.getVisitInfo(updatedKey) == null) {
              reportSet.putVisitInfo(updatedKey, entry.getValue());
            }
          }
          
          // TODO: this is very lazy code - it forces the entire query to be
          // re-run, which destroys context.  If pasting sightings on a location
          // turns out to be handy, this will annoy users.
          if (!transferSupport.isDrop()) {
            fireLocationsEditedEvent();
          }
          return true;
        }
        
        throw new RuntimeException("Should not reach: " +
            Lists.<DataFlavor>newArrayList(transferSupport.getDataFlavors()));
      } catch (RuntimeException e) {
        // Log and rethrow.  Basic problem is that drag-and-drop errors
        // get silently dropped, so bugs here have been very silent...
        Logger.getLogger(getClass().getName()).log(Level.WARNING, "Error", e);
        throw e;
      }
    }
    
    private Rectangle getSelectionBounds() {
      if (getSelectionCount() == 0) {
        return null;
      }
      
      TreePath[] selectionPaths = getSelectionPaths();
      Rectangle bounds = getPathBounds(selectionPaths[0]);
      for (int i = 1; i < selectionPaths.length; i++) {
        bounds.add(getPathBounds(selectionPaths[i]));
      }
      return bounds;
    }

    private ImmutableList<Location> getLocations(TransferSupport transferSupport) {
      try {
        return ((LocalLocationsSet)
            transferSupport.getTransferable().getTransferData(LOCATIONS_FLAVOR))
            .locations;
      } catch (UnsupportedFlavorException | IOException e) {
        throw new RuntimeException(e);
      }
    }

    private TreePath getDropTarget(TransferSupport transferSupport) {
      if (!transferSupport.isDrop()) {
        TreePath[] paths = getSelectionPaths();
        if (paths == null || paths.length != 1) {
          return null;
        }
        
        return paths[0];
      } 
      
      JTree.DropLocation dropLocation =
          (JTree.DropLocation) transferSupport.getDropLocation();
      return dropLocation.getPath();
    }
  }
  
  /** Transferable for a list of Locations. */
  private static class LocationTransferable implements Transferable {
    private final LocalLocationsSet locations;

    public LocationTransferable(List<Location> locations) {
      this.locations = new LocalLocationsSet(locations);
    }

    @Override public Object getTransferData(DataFlavor flavor)
        throws UnsupportedFlavorException, IOException {
      if (LOCATIONS_FLAVOR.equals(flavor)) {
        return locations;
      }
      
      throw new UnsupportedFlavorException(flavor);
    }
  
    @Override public DataFlavor[] getTransferDataFlavors() {
      return new DataFlavor[]{LOCATIONS_FLAVOR};
    }
  
    @Override public boolean isDataFlavorSupported(DataFlavor flavor) {
      return LOCATIONS_FLAVOR.equals(flavor);
    }    
  }

  private class LocationTreeFocusTracker extends FocusTracker {
    private final Action paste = new PasteActionWrapper(TransferHandler.getPasteAction());
    
    public LocationTreeFocusTracker() {
      super(LocationTree.this);
    }

    @Override protected void focusGained(Component child) {
      actionBroker.publishAction("paste", paste);
    }

    @Override protected void focusLost(Component child) {
      actionBroker.unpublishAction("paste", paste);
    }
  }

  /**
   * Wrapper to enable-disable pasting.  Enable only when the clipboard contains a SightingsGroup,
   * and when there is one selected location.
   */
  protected class PasteActionWrapper extends ForwardingAction
      implements Attachable, FlavorListener, TreeSelectionListener {

    public PasteActionWrapper(Action wrapped) {
      super(LocationTree.this, wrapped);
    }
    
    @Override public void attach() {
      Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
      clipboard.addFlavorListener(this);
      getSelectionModel().addTreeSelectionListener(this);
      // Manually get the enabled state set initially
      updateEnabled();
    }

    private void updateEnabled() {
      setEnabled(pasteAvailable());
    }

    private boolean pasteAvailable() {
      Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
      try {
        if (!clipboard.isDataFlavorAvailable(SightingsGroup.FLAVOR)) {
          return false;
        }
      } catch (IllegalStateException e) {
        // Windows blows up sometimes checking for clipboard availability
        return false;
      }
      
      TreePath[] paths = getSelectionPaths();
      return (paths != null && paths.length == 1);
    }

    @Override public void unattach() {
      Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
      clipboard.removeFlavorListener(this);
      getSelectionModel().removeTreeSelectionListener(this);
    }

    @Override public void flavorsChanged(FlavorEvent e) {
      updateEnabled();
    }

    @Override public void valueChanged(TreeSelectionEvent e) {
      updateEnabled();
    } 
  }

  @Override
  public boolean isRowSelected(int row) {
    // Part 2 of Quaqua hack for https://bitbucket.org/scythebill/scythebill-birdlist/issues/91
    // - Pretend a row is "selected" if it's the current drop location.  This would cause
    // serious problems if isRowSelected() was caused by anything other than the rendering code.
    if (isQuaqua()) {
      DropLocation dropLocation = getDropLocation();
      if (dropLocation != null && dropLocation.getPath() != null) {
        if (getRowForPath(dropLocation.getPath()) == row) { 
          return true;
        }
      }
    }
      
    return super.isRowSelected(row);
  }

  private boolean isQuaqua() {
    return getUI().getClass().getName().contains("Quaqua");    
  }
}
