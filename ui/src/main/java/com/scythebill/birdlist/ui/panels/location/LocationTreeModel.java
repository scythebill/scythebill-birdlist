/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.location;

import java.text.Collator;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.event.TreeModelEvent;
import javax.swing.tree.TreePath;

import com.google.common.base.Predicate;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;
import com.scythebill.birdlist.ui.util.BaseTreeModel;
import com.scythebill.birdlist.ui.util.LocationIdToString;

/**
 * TreeModel implementation for LocationTrees.
 */
public class LocationTreeModel extends BaseTreeModel {
  private static final Ordering<Object> COLLATOR_ORDERING =
      Ordering.from(Collator.getInstance());
  private static final Ordering<Location> LOCATION_ORDERING = new Ordering<Location>() {
    @Override
    public int compare(Location left, Location right) {
      String leftString = LocationIdToString.getStringWithType(left);
      String rightString = LocationIdToString.getStringWithType(right);
      return COLLATOR_ORDERING.compare(leftString, rightString);
    }
  };
  private static final Logger logger =  Logger.getLogger(LocationTreeModel.class.getName());
  

  final static Object ROOT = new Object();
  private final LocationSet locationSet;
  private final LoadingCache<Object, List<Location>> childrenCache;
  private final PredefinedLocations predefinedLocations;
  private Predicate<Location> filter;
  
  public LocationTreeModel(LocationSet locationSet, PredefinedLocations predefinedLocations, Predicate<Location> filter) {
    this.locationSet = locationSet;
    this.predefinedLocations = predefinedLocations;
    this.filter = filter;
    childrenCache = CacheBuilder.newBuilder()
        .build(new CacheLoader<Object, List<Location>> () {
          @Override public List<Location> load(Object key) throws Exception {
            return getSortedLocationChildren(key);
          }
        });
  }
  
  @Override
  public Object getChild(Object node, int index) {
    return childrenCache.getUnchecked(node).get(index);
  }

  @Override
  public int getChildCount(Object node) {
    return childrenCache.getUnchecked(node).size();
  }

  @Override
  public int getIndexOfChild(Object parent, Object child) {
    return childrenCache.getUnchecked(parent).indexOf(child);
  }

  @Override
  public Object getRoot() {
    return ROOT;
  }

  @Override
  public boolean isLeaf(Object node) {
    return childrenCache.getUnchecked(node).isEmpty();
  }

  @Override
  public void valueForPathChanged(TreePath path, Object value) {
    TreePath parentPath = path.getParentPath();
    if (parentPath == null) {
      childrenCache.invalidateAll();
      fireTreeStructureChanged(new TreeModelEvent(this, path));
    } else {
      childrenCache.invalidate(path.getLastPathComponent());
      childrenCache.invalidate(parentPath.getLastPathComponent());
      fireTreeStructureChanged(new TreeModelEvent(this, parentPath));
    }
  }

  /**
   * Notification that a Location has been added somewhere underneath the path.
   * 
   * @param path the Path to a node that is present in the tree and has not changed value
   *     or structure
   * @param location the newly added location
   */
  public void newLocation(TreePath path, Location location) {
    childrenCache.invalidate(path.getLastPathComponent());
    childrenCache.invalidate(location);
    // Find the immediate child of the path
    while (location.getParent() != path.getLastPathComponent()) {
      location = location.getParent();
      // Added location and path don't align...  This should not
      // happen, but has per user report.  Invalidate everything
      // and bail.      
      if (location == null) {
        logger.warning(String.format("Could not find location's parent in path %s, invalidating child cache", path));
        childrenCache.invalidateAll();
        break;
      }
      childrenCache.invalidate(location);
    }
    
    // Location is null, and the whole cache was invalidated - note that the structure has changed.
    if (location == null) {
      fireTreeStructureChanged(new TreeModelEvent(this, path));
    } else {
      int newIndex = getIndexOfChild(path.getLastPathComponent(), location);
      if (newIndex < 0) {
        // Couldn't find this element in this path - just invalidate the whole tree and
        // move on.
        logger.warning(String.format("Expected to find location %s in path %s, invalidating tree",
            location.getDisplayName(), path.getParentPath()));
        fireTreeStructureChanged(new TreeModelEvent(this, new TreePath(ROOT)));
      } else if (location.isBuiltInLocation()) { 
        // If this "new" location is a built-in location, then it was in the tree already, but
        // as a location-without-id, and now it's a different object with an ID, so fire a nodes-changed.
        // [missing text ... not sure why this isn't just a nodes-changed but at some point the
        // next sentence was added without explaining!]
        // So fire a brute-force tree-structure changed, instead of a simple tree-nodes-inserted.      
        fireTreeStructureChanged(new TreeModelEvent(this, path));
      } else {
        fireTreeNodesInserted(new TreeModelEvent(this, path, new int[]{newIndex}, new Object[]{location}));
      }
    }
  }

  private List<Location> getSortedLocationChildren(Object from) {
    Iterable<Location> unsorted; 
    if (from == ROOT) {
      unsorted = locationSet.rootLocations();
    } else {
      Location location = (Location) from;
      unsorted = location.contents();
      // See if there are predefined locations available for the parent location.
      ImmutableCollection<PredefinedLocation> predefinedList =
          predefinedLocations.getPredefinedLocations(location);
      if (!predefinedList.isEmpty()) {
        List<Location> notYetStoredList = Lists.newArrayList();
        // ... and for each, see which have not yet been stored (and so aren't in the above
        // contents() list
        for (PredefinedLocation predefined : predefinedList) {
          if (predefined.getCode() != null) {
            // Check if the location exists *anywhere* (not necessarily just in this parent) 
            Location locationByCode = locationSet.getLocationByCode(predefined.getCode());
            if (locationByCode == null) {
              notYetStoredList.add(predefined.create(locationSet, location));
            }
          }
        }
        
        unsorted = Iterables.concat(unsorted, notYetStoredList);
      }
    }
    
    return LOCATION_ORDERING.sortedCopy(Iterables.filter(unsorted, filter));
  }

  /**
   * Given a Location - which might be an as-yet not-inserted, PredefinedLocation,
   * return the Location instance actually stored in the model.  Exists only because
   * the "whereIndexer" might pull a location out of thin air at the same time as
   * this class does the same - and those *should* be the same location but aren't.
   * TODO: this points out that arguably we'd do waaaaay better to make PredefinedLocations
   * hold onto those instances for good.
   */
  public Location findActualLocation(Location location) {
    if (location.getId() != null) {
      return location;
    }
    
    Location parent = findActualLocation(location.getParent());
    List<Location> list = childrenCache.getUnchecked(parent);
    for (Location possibility : list) {
      if (location.getModelName().equals(possibility.getModelName())) {
        return possibility;
      }
    }
    
    return location;
  }
}
