/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.location;

import java.awt.Point;
import java.awt.geom.Point2D;

import com.scythebill.birdlist.model.sighting.LatLongCoordinates;

/**
 * Class for handling Mercator projections, specifically for the Google Maps static API.
 */
class MercatorProjection {
  private final static double MERCATOR_RANGE = 256;
  private final static Point2D.Double PIXEL_ORIGIN = new Point2D.Double(MERCATOR_RANGE / 2.0, MERCATOR_RANGE / 2.0);
  private final static double PIXELS_PER_DEGREE_LONGITUDE = MERCATOR_RANGE / 360.0;
  private final static double PIXELS_PER_RADIAN_LONGITUDE  = MERCATOR_RANGE / (2.0 * Math.PI);
  
  static Point2D.Double latLongToPoint(LatLongCoordinates latLong) {
    Point2D.Double point = new Point2D.Double(0, 0);
    point.x = PIXEL_ORIGIN.x + latLong.longitudeAsDouble() * PIXELS_PER_DEGREE_LONGITUDE;
    // NOTE(appleton): Truncating to 0.9999 effectively limits latitude to
    // 89.189.  This is about a third of a tile past the edge of the world tile.
    double siny = bound(Math.sin(degreesToRadians(latLong.latitudeAsDouble())), -0.9999, 0.9999);
    point.y = PIXEL_ORIGIN.y + 0.5 * Math.log((1 + siny) / (1 - siny)) * -PIXELS_PER_RADIAN_LONGITUDE;
    return point;
  }

  static LatLongCoordinates pointToLatLong(Point2D.Double point) {
    double longitude = (point.x - PIXEL_ORIGIN.x) / PIXELS_PER_DEGREE_LONGITUDE;
    double latRadians = (point.y - PIXEL_ORIGIN.y) / -PIXELS_PER_RADIAN_LONGITUDE;
    double latitude = radiansToDegrees(2 * Math.atan(Math.exp(latRadians)) - Math.PI / 2);
    if (latitude > 90.0) {
      latitude = latitude - 180.0;
    } else if (latitude < -90.0) {
      latitude = latitude + 180.0;
    }
    
    if (longitude > 180.0) {
      longitude = longitude - 360.0;
    } else if (longitude < -180.0) {
      longitude = longitude + 360.0;
    }
    
    return LatLongCoordinates.withLatAndLong(latitude, longitude);
  }

  static LatLongCoordinates northwestCorner(
      LatLongCoordinates center, int zoom, int mapWidth, int mapHeight) {
    double scale = Math.pow(2, zoom);
    Point2D.Double centerPoint = latLongToPoint(center);
    Point2D.Double southWestPoint = new Point2D.Double(
        centerPoint.x - ((mapWidth / 2) / scale),
        centerPoint.y - ((mapHeight / 2) / scale));
    return pointToLatLong(southWestPoint);
  }
  
  static LatLongCoordinates southeastCorner(
      LatLongCoordinates center, int zoom, int mapWidth, int mapHeight) {
    double scale = Math.pow(2,zoom);
    Point2D.Double centerPoint = latLongToPoint(center);
    Point2D.Double southWestPoint = new Point2D.Double(
        centerPoint.x + ((mapWidth / 2) / scale),
        centerPoint.y + ((mapHeight / 2) / scale));
    return pointToLatLong(southWestPoint);
  }

  static LatLongCoordinates point(
      LatLongCoordinates center,
      int zoomLevel,
      int width,
      int height,
      Point point) {
    double scale = Math.pow(2, zoomLevel);
    Point2D.Double centerPoint = latLongToPoint(center);
    double xFraction = (((double) point.x) / ((double) width)) - 0.5;
    double yFraction = (((double) point.y) / ((double) height)) - 0.5;
    Point2D.Double clickedPoint = new Point2D.Double(
        centerPoint.x + (width * xFraction / scale),
        centerPoint.y + (height * yFraction / scale));
    return pointToLatLong(clickedPoint);
  }

  private static double degreesToRadians(double degrees) {
    return degrees * (Math.PI / 180);
  }

  private static double radiansToDegrees(double radians) {
    return radians / (Math.PI / 180);
  }

  private static double bound(double value, double minimum, double maximum) {
    if (value < minimum) {
      return minimum;
    } else if (value > maximum) {
      return maximum;
    } else {
     return value;
    }
  }
}
