/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.query.PredicateQueryDefinition;
import com.scythebill.birdlist.model.query.QueryDefinition;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryField;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;

/**
 * Base class for query fields.
 */
abstract class AbstractQueryField implements QueryField {
  private final List<ActionListener> predicateUpdatedListeners = Lists.newArrayList();
  private final QueryFieldType type;
  
  public AbstractQueryField(QueryFieldType type) {
    this.type = type;
  }

  @Override
  public QueryFieldType getType() {
    return type;
  }
  
  @Override
  public void addPredicateUpdatedListener(ActionListener e) {
    predicateUpdatedListeners.add(e);
  }

  @Override
  public QueryDefinition queryDefinition(ReportSet reportSet, Taxon.Type depth) {
    return new PredicateQueryDefinition(predicate(depth));
  }

  protected Predicate<Sighting> predicate(Taxon.Type depth) {
    throw new UnsupportedOperationException("predicate() not overridden");
  }

  protected void firePredicateUpdated() {
    ActionEvent e = new ActionEvent(this, 0, null);
    for (ActionListener l : predicateUpdatedListeners) {
      l.actionPerformed(e);
    }
  }
}
