/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import static java.util.stream.Collectors.toCollection;
import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.swing.Action;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.Timer;
import javax.swing.TransferHandler;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.joda.time.DateTimeFieldType;
import org.joda.time.Partial;
import org.joda.time.ReadablePartial;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.query.QueryProcessor;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.query.SightingPredicates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.actions.ActionBroker;
import com.scythebill.birdlist.ui.actions.Attachable;
import com.scythebill.birdlist.ui.actions.ForwardingAction;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.app.Titled;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.events.TaxonomyChangedEvent;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;
import com.scythebill.birdlist.ui.util.FocusTracker;
import com.scythebill.birdlist.ui.util.ListListModel;

/**
 * Panel for showing top "big days" and "big years".
 */
public class BigDayPanel extends JPanel implements FontManager.FontsUpdatedListener, Titled {

  private static final long MAX_RESULTS = 50;
  private final ReportSet reportSet;
  private final QueryPanelFactory queryPanelFactory;
  private final TaxonomyStore taxonomyStore;
  private final QueryPreferences queryPreferences;
  private final ReportsBrowserPanel reportsBrowserPanel;
  private QueryPanel queryPanel;
  private JButton returnButton;
  private final NavigableFrame navigableFrame;
  private JComboBox<BigDayType> typeComboBox;
  private JList<BigDayResult> bigDayResultList;
  private JScrollPane bigDayResultScrollPane;
  private JScrollPane reportsScrollPane;
  private Timer showBigDayTimer;
  private ActionBroker actionBroker;

  enum BigDayType {
    BIG_DAY(Name.BIG_DAY) {
      @Override
      ReadablePartial extractDate(ReadablePartial in) {
        if (in == null 
            || !in.isSupported(DateTimeFieldType.dayOfMonth())
            || !in.isSupported(DateTimeFieldType.monthOfYear())
            || !in.isSupported(DateTimeFieldType.year())) {
          return null;
        }
        
        return in;
      }
      
    },
    BIG_MONTH(Name.BIG_MONTH) {
      @Override
      ReadablePartial extractDate(ReadablePartial in) {
        if (in != null
            && in.isSupported(DateTimeFieldType.year())
            && in.isSupported(DateTimeFieldType.monthOfYear())) {
          return new Partial()
              .with(
                  DateTimeFieldType.year(),
                  in.get(DateTimeFieldType.year()))
              .with(
                  DateTimeFieldType.monthOfYear(),
                  in.get(DateTimeFieldType.monthOfYear()));
        }
        return null;
      }
      
    },
    BIG_YEAR(Name.BIG_YEAR) {
      @Override
      ReadablePartial extractDate(ReadablePartial in) {
        if (in != null && in.isSupported(DateTimeFieldType.year())) {
          return new Partial().with(
              DateTimeFieldType.year(),
              in.get(DateTimeFieldType.year()));
        }
        
        return null;
      }
    };
    
    private final Name text;

    BigDayType(Name text) {
      this.text = text;
    }
    
    @Override
    public String toString() {
      return Messages.getMessage(text);
    }
    
    abstract ReadablePartial extractDate(ReadablePartial in);
  }
  
  class BigDayResult {
    final ReadablePartial date;
    final LinkedHashSet<String> species;
    Location location;
    
    BigDayResult(ReadablePartial date) {
      this.date = date;
      this.species = new LinkedHashSet<>();
      this.location = null;
    }
    
    int count() {
      return species.size();
    }
    
    @Override
    public String toString() {
      if (location == null) {
        return String.format(
            "%d: \t%s",
            count(),
            PartialIO.toShortUserString(date, Locale.getDefault()));
      }
      return String.format(
          "%d: \t%s \t%s",
          count(),
          PartialIO.toShortUserString(date, Locale.getDefault()),
          location.getDisplayName());
    }

    public void addSpecies(String id) {
      species.add(id);
    }
  }
  
  @Inject
  BigDayPanel(
      ReportSet reportSet,
      ReportsBrowserPanel reportsBrowserPanel,
      QueryPanelFactory queryPanelFactory,
      TaxonomyStore taxonomyStore,
      EventBusRegistrar eventBusRegistrar,
      FontManager fontManager,
      QueryPreferences queryPreferences,
      NavigableFrame navigableFrame,
      ActionBroker actionBroker) {
    this.reportSet = reportSet;
    this.reportsBrowserPanel = reportsBrowserPanel;
    this.queryPanelFactory = queryPanelFactory;
    this.taxonomyStore = taxonomyStore;
    this.queryPreferences = queryPreferences;
    this.navigableFrame = navigableFrame;
    this.actionBroker = actionBroker;
    initGUI(eventBusRegistrar);
    attachListeners();
    fontManager.applyTo(this);
    eventBusRegistrar.registerWhenInHierarchy(this);
    taxonomyChanged(null);    
  }

  private void attachListeners() {
    returnButton.addActionListener(e -> navigableFrame.navigateTo("extendedReportsMenu"));
    queryPanel.addPredicateChangedListener(e -> showQueryInList());
    typeComboBox.addActionListener(e -> showQueryInList());
    showBigDayTimer = new Timer(200, e -> showSingleBigDay());
    showBigDayTimer.setRepeats(false);
    bigDayResultList.addListSelectionListener(e -> showBigDayTimer.restart());
    addComponentListener(new ComponentAdapter() {
      @Override public void componentHidden(ComponentEvent event) {
        showBigDayTimer.stop();
      }
    });
    bigDayResultList.setDragEnabled(true);
    bigDayResultList.setTransferHandler(new TransferHandler() {

      @Override
      protected Transferable createTransferable(JComponent c) {
        if (bigDayResultList.isSelectionEmpty()) {
          return null;
        }
        
        String bigDaysAsText = IntStream.of(bigDayResultList.getSelectedIndices())
            .sorted()
            .mapToObj(i -> bigDayResultList.getModel().getElementAt(i).toString())
            .collect(Collectors.joining("\n"));
        return new StringSelection(bigDaysAsText);
      }

      @Override
      public int getSourceActions(JComponent c) {
        return COPY;
      }
    });

    new FocusTracker(bigDayResultList) {
      private final Action copy = new CopyActionWrapper(TransferHandler.getCopyAction());
      
      @Override protected void focusGained(Component child) {
        actionBroker.publishAction("copy", copy);
      }

      @Override protected void focusLost(Component child) {
        actionBroker.unpublishAction("copy", copy);
      }
    };
  }

  private void initGUI(EventBusRegistrar eventBusRegistrar) {    
    queryPanel = queryPanelFactory.newQueryPanel(
        // No sp/hybrid, since that's explicitly meaningless here
        QueryFieldType.SP_OR_HYBRID,
        // Or "subspecies allocated"
        QueryFieldType.SUBSPECIES_ALLOCATED,
        // Or "Times sighted", which is weird for a big-day
        QueryFieldType.TIMES_SIGHTED);
    typeComboBox = new JComboBox<>(BigDayType.values());
    returnButton = new JButton(Messages.getMessage(Name.BACK_TO_SPECIAL_REPORTS));
    bigDayResultList = new JList<>(new ListListModel<BigDayResult>());
    bigDayResultScrollPane = new JScrollPane(
        bigDayResultList,
        JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
        JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    reportsScrollPane = new JScrollPane(reportsBrowserPanel);
    reportsBrowserPanel.setEditableSightings(true);
    reportsBrowserPanel.setReturnPanelName("bigDay");
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    setBorder(new EmptyBorder(
        fontManager.scale(20),
        fontManager.scale(20),
        fontManager.scale(20),
        fontManager.scale(20)));
    GroupLayout layout = new GroupLayout(this);
    layout.setHorizontalGroup(
        layout.createParallelGroup(Alignment.LEADING)
            .addComponent(typeComboBox, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addComponent(queryPanel, PREFERRED_SIZE, PREFERRED_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(bigDayResultScrollPane)
                .addComponent(reportsScrollPane))
            .addComponent(returnButton, Alignment.TRAILING));
    
    layout.setVerticalGroup(
        layout.createSequentialGroup()
            .addComponent(typeComboBox, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(queryPanel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup()
                .addComponent(bigDayResultScrollPane, PREFERRED_SIZE, PREFERRED_SIZE, Short.MAX_VALUE)
                .addComponent(reportsScrollPane, PREFERRED_SIZE, PREFERRED_SIZE, Short.MAX_VALUE))
            .addGap(10)
            .addComponent(returnButton));
    
    setLayout(layout);
    
    Dimension preferredSize = fontManager.scale(new Dimension(760, 680));
    preferredSize.width += 200;
    setPreferredSize(preferredSize);
  }
  
 
  @Subscribe
  public void taxonomyChanged(TaxonomyChangedEvent event) {
    reportsBrowserPanel.setTaxonomy(taxonomyStore.getTaxonomy());
    queryPanel.taxonomyUpdated();
    showQueryInList();
  }
  
  private void showQueryInList() {
    Taxonomy taxonomy = taxonomyStore.getTaxonomy();
    Predicate<Sighting> countablePredicate = queryPreferences.getCountablePredicate(
        taxonomy,
        queryPanel.containsQueryFieldType(QueryFieldType.SIGHTING_STATUS),
        queryPanel.getBooleanValueIfPresent(QueryFieldType.ALLOW_HEARD_ONLY));
    QueryProcessor queryProcessor = new QueryProcessor(reportSet, taxonomy);
    queryProcessor.onlyIncludeCountableSightings().dontIncludeFamilyNames();
    
    QueryResults results = queryProcessor.runQuery(
        queryPanel.queryDefinition(taxonomy, Taxon.Type.species),
        countablePredicate,
        Taxon.Type.species);

    Map<ReadablePartial, BigDayResult> queries = new LinkedHashMap<>();
    BigDayType type = (BigDayType) typeComboBox.getSelectedItem();
    for (Sighting sighting : results.getAllSightings()) {
      ReadablePartial dateBucket = type.extractDate(sighting.getDateAsPartial());
      if (dateBucket == null) {
        continue;
      }
      
      // Resolve the taxon
      SightingTaxon species = sighting.getTaxon().resolve(taxonomy).getParentOfAtLeastType(Taxon.Type.species);
      if (species.getType() == SightingTaxon.Type.HYBRID || species.getType() == SightingTaxon.Type.SP) {
        continue;
      }
      
      BigDayResult result = queries.computeIfAbsent(dateBucket, k -> new BigDayResult(k));
      // First sighting:  choose the current location
      if (result.count() == 0) {
        if (sighting.getLocationId() != null) {
          result.location = reportSet.getLocations().getLocation(sighting.getLocationId()); 
        }
      } else {
        // If the location isn't already "world", choose a common parent
        if (result.location != null) {
          // New sighting is "world", up to that
          if (sighting.getLocationId() == null) {
            result.location = null;
          } else {
            // New location, get a common ancestor
            if (!sighting.getLocationId().equals(result.location.getId())) {
              result.location = Locations.getCommonAncestor(
                  result.location,
                  reportSet.getLocations().getLocation(sighting.getLocationId()));
            }
          }
        }
      }
      result.addSpecies(species.getId());      
    }

    showBigDayTimer.stop();
    reportsBrowserPanel.setModel(null);
    ArrayList<BigDayResult> list = queries.values().stream()
        .sorted(Comparator.comparing(BigDayResult::count).reversed())
        .limit(MAX_RESULTS)
        .collect(toCollection(ArrayList::new));
    bigDayResultList.setModel(new ListListModel<>(list));
  }
  
  private void showSingleBigDay() {
    BigDayResult result = bigDayResultList.getSelectedValue();
    if (result == null) {
      reportsBrowserPanel.setModel(null);
    } else {
      Taxonomy taxonomy = taxonomyStore.getTaxonomy();
      Predicate<Sighting> countablePredicate = queryPreferences.getCountablePredicate(
          taxonomyStore.getTaxonomy(),
          queryPanel.containsQueryFieldType(QueryFieldType.SIGHTING_STATUS),
          queryPanel.getBooleanValueIfPresent(QueryFieldType.ALLOW_HEARD_ONLY));
      QueryProcessor queryProcessor = new QueryProcessor(reportSet, taxonomyStore.getTaxonomy())
          .onlyIncludeCountableSightings()
          .withAdditionalPredicate(
              Predicates.and(SightingPredicates.afterOrEquals(result.date),
                  SightingPredicates.beforeOrEquals(result.date)));

      QueryResults results = queryProcessor.runQuery(
          queryPanel.queryDefinition(taxonomy, Taxon.Type.species),
          countablePredicate,
          Taxon.Type.species);
      reportsBrowserPanel.setLocationRoot(result.location);
      reportsBrowserPanel.setQueryResults(results, /*showVisits=*/true);
      // Oddly, necessary to make the browser panel show up
      reportsBrowserPanel.revalidate();
    }
  }

  /**
   * Wrapper to enable-disable the copy actions.  Enable when selection indicates
   * there is any selection that contains text.
   */
  private class CopyActionWrapper extends ForwardingAction
      implements Attachable, ListSelectionListener {
    public CopyActionWrapper(Action wrapped) {
      super(bigDayResultList, wrapped);
    }
    
    @Override public void attach() {
      bigDayResultList.addListSelectionListener(this);
      updateEnabled();
    }

    @Override public void unattach() {
      bigDayResultList.removeListSelectionListener(this);
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
      updateEnabled();
    }
    
    private void updateEnabled() {
      setEnabled(!bigDayResultList.isSelectionEmpty());
    }
  }

  @Override
  public String getTitle() {
    return Messages.getMessage(Name.BIG_DAY);
  }
}
