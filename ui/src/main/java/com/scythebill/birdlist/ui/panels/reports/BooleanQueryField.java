/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonPrimitive;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;

/**
 * Base class for query fields that operate on a boolean field.
 */
abstract class BooleanQueryField extends AbstractQueryField {
  private final JLabel label = new JLabel(Messages.getMessage(Name.VALUE_IS));
  private final JComboBox<String> options = new JComboBox<>(
      new String[] {
          Messages.getMessage(Name.VALUE_YES),
          Messages.getMessage(Name.VALUE_NO),
          Messages.getMessage(Name.VALUE_ANY)
          });
  
  public BooleanQueryField(QueryFieldType type, boolean defaultValue) {
    super(type);
    
    options.setSelectedIndex(defaultValue ? 0 : 1);
    options.addActionListener(e -> firePredicateUpdated());
  }

  @Override
  public JComponent getComparisonChooser() {
    return label;
  }

  @Override
  public JComponent getValueField() {
    return options;
  }

  @Override
  protected Predicate<Sighting> predicate(Taxon.Type depth) {
    if (options.getSelectedIndex() == 2) {
      return Predicates.alwaysTrue();
    }
    
    boolean currentValue = currentValue();
    return predicateFromBoolean(currentValue);
  }

  @Override public final boolean isNoOp() {
    return options.getSelectedIndex() == 2;
  }

  @Override
  public Optional<Boolean> getBooleanValue() {
    if (isNoOp()) {
      return Optional.absent();
    }
    
    return Optional.of(currentValue());
  }
  
  protected boolean currentValue() {
    return options.getSelectedIndex() == 0;
  }

  
  protected abstract Predicate<Sighting> predicateFromBoolean(boolean currentValue);

  @Override
  public JsonElement persist() {
    if (isNoOp()) {
      return JsonNull.INSTANCE;
    }
    return new JsonPrimitive(currentValue());
  }

  @Override
  public void restore(JsonElement persisted) {
    if (persisted == null || persisted.isJsonNull()) {
      options.setSelectedIndex(2);
    } else {
      options.setSelectedIndex(persisted.getAsBoolean() ? 0 : 1);
    }
  }
}
