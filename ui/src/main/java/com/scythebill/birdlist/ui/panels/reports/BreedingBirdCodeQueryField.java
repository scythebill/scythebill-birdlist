/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EnumSet;

import javax.swing.JComboBox;
import javax.swing.JComponent;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo.BreedingBirdCode;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;

/**
 * QueryField for adjusting queries relative to the breeding code.
 */
class BreedingBirdCodeQueryField extends AbstractQueryField {
  enum QueryType {
    IS(Name.CODE_IS),
    IS_AT_LEAST(Name.CODE_IS_AT_LEAST),
    IS_NOT(Name.CODE_IS_NOT),
    ANY(Name.CODE_IS_ANY);
    
    private final Name text;
    
    private QueryType(Name text) {
      this.text = text;
    }
    
    @Override
    public String toString() {
      return Messages.getMessage(text);
    }
  };
  
  enum BreedingBirdLevel {
    CONFIRMED(Name.CODE_CONFIRMED, BreedingBirdCode.NEST_WITH_YOUNG, BreedingBirdCode.NEST_WITH_EGGS,
        BreedingBirdCode.OCCUPIED_NEST, BreedingBirdCode.RECENTLY_FLEDGED,
        BreedingBirdCode.FEEDING_YOUNG, BreedingBirdCode.CARRYING_FECAL_SAC,
        BreedingBirdCode.CARRYING_FOOD, BreedingBirdCode.DISTRACTION_DISPLAY,
        BreedingBirdCode.USED_NEST),
    CONFIRMED_PROBABLE(Name.CODE_CONFIRMED_PROBABLE,
        BreedingBirdCode.NEST_BUILDING, BreedingBirdCode.CARRYING_NESTING_MATERIAL),
    PROBABLE(Name.CODE_PROBABLE,
        BreedingBirdCode.TERRITORIAL_DEFENSE, BreedingBirdCode.COURTSHIP_DISPLAY_OR_COPULATION,
        BreedingBirdCode.VISITING_PROBABLE_NEST_SITE, BreedingBirdCode.AGITATED_BEHAVIOR,
        BreedingBirdCode.PHYSIOLOGICAL_EVIDENCE, BreedingBirdCode.MULTIPLE_SINGING_BIRDS,
        BreedingBirdCode.SINGING_BIRD_7_DAYS,
        BreedingBirdCode.WOODPECKER_WREN_NEST_BUILDING),
    POSSIBLE(Name.CODE_POSSIBLE,
        BreedingBirdCode.PAIR_IN_SUITABLE_HABITAT, BreedingBirdCode.SINGING_BIRD,
        BreedingBirdCode.IN_APPROPRIATE_HABITAT),
    NEST_WITH_YOUNG(BreedingBirdCode.NEST_WITH_YOUNG),
    NEST_WITH_EGGS(BreedingBirdCode.NEST_WITH_EGGS),
    CARRYING_FECAL_SAC(BreedingBirdCode.CARRYING_FECAL_SAC),
    FEEDING_YOUNG(BreedingBirdCode.FEEDING_YOUNG),
    CARRYING_FOOD(BreedingBirdCode.CARRYING_FOOD),
    RECENTLY_FLEDGED(BreedingBirdCode.RECENTLY_FLEDGED),
    OCCUPIED_NEST(BreedingBirdCode.OCCUPIED_NEST),
    USED_NEST(BreedingBirdCode.USED_NEST),
    DISTRACTION_DISPLAY(BreedingBirdCode.DISTRACTION_DISPLAY),
    NEST_BUILDING(BreedingBirdCode.NEST_BUILDING),
    CARRYING_NESTING_MATERIAL(BreedingBirdCode.CARRYING_NESTING_MATERIAL),
    PHYSIOLOGICAL_EVIDENCE(BreedingBirdCode.PHYSIOLOGICAL_EVIDENCE),
    WOODPECKER_WREN_NEST_BUILDING(BreedingBirdCode.WOODPECKER_WREN_NEST_BUILDING),
    AGITATED_BEHAVIOR(BreedingBirdCode.AGITATED_BEHAVIOR),
    VISITING_PROBABLE_NEST_SITE(BreedingBirdCode.VISITING_PROBABLE_NEST_SITE),
    COURTSHIP_DISPLAY_OR_COPULATION(BreedingBirdCode.COURTSHIP_DISPLAY_OR_COPULATION),
    TERRITORIAL_DEFENSE(BreedingBirdCode.TERRITORIAL_DEFENSE),
    PAIR_IN_SUITABLE_HABITAT(BreedingBirdCode.PAIR_IN_SUITABLE_HABITAT),
    MULTIPLE_SINGING_BIRDS(BreedingBirdCode.MULTIPLE_SINGING_BIRDS),
    SINGING_BIRD_7_DAYS(BreedingBirdCode.SINGING_BIRD_7_DAYS),
    SINGING_BIRD(BreedingBirdCode.SINGING_BIRD),
    IN_APPROPRIATE_HABITAT(BreedingBirdCode.IN_APPROPRIATE_HABITAT),
    FLYOVER(BreedingBirdCode.FLYOVER);
    
    private final ImmutableList<BreedingBirdCode> codes;
    private final Name name;

    BreedingBirdLevel(BreedingBirdCode oneCode) {
      this.name = Messages.getName(oneCode);
      this.codes = ImmutableList.of(oneCode);
    }

    BreedingBirdLevel(Name name, BreedingBirdCode... codes) {
      this.name = name;
      this.codes = ImmutableList.copyOf(codes);
    }

    public ImmutableList<BreedingBirdCode> getCodes() {
      return codes;
    }

    @Override
    public String toString() {
      return Messages.getMessage(name);
    }
  }

  private final JComboBox<QueryType> queryOptions = new JComboBox<>(QueryType.values());
  private final JComboBox<BreedingBirdLevel> levelOptions = new JComboBox<>(BreedingBirdLevel.values());
  {
    levelOptions.setMaximumRowCount(levelOptions.getItemCount());
  }

  public BreedingBirdCodeQueryField() {
    super(QueryFieldType.BREEDING_CODE);
    queryOptions.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent e) {
        levelOptions.setEnabled(!QueryType.ANY.equals(queryOptions.getSelectedItem()));
        firePredicateUpdated();
      }
    });
    levelOptions.addActionListener(e -> firePredicateUpdated());
  }

  @Override
  public JComponent getComparisonChooser() {
    return queryOptions;
  }

  @Override
  public JComponent getValueField() {
    return levelOptions;
  }

  @Override
  public Predicate<Sighting> predicate(Taxon.Type depth) {
    if (QueryType.ANY.equals(queryOptions.getSelectedItem())) {
      return Predicates.alwaysTrue();
    }
    EnumSet<BreedingBirdCode> allowedCodes = EnumSet.noneOf(BreedingBirdCode.class);
    if (QueryType.IS.equals(queryOptions.getSelectedItem())
        || QueryType.IS_NOT.equals(queryOptions.getSelectedItem())) {
      BreedingBirdLevel level = breedingBirdLevel();
      allowedCodes.addAll(level.getCodes());
      if (QueryType.IS_NOT.equals(queryOptions.getSelectedItem())) {
        allowedCodes = EnumSet.complementOf(allowedCodes);
      }
    } else {
      for (int i = 0; i <= levelOptions.getSelectedIndex(); i++) {
        BreedingBirdLevel level = BreedingBirdLevel.values()[i];
        allowedCodes.addAll(level.getCodes());
      }
    }
    
    final EnumSet<BreedingBirdCode> computedCodes = allowedCodes;
    return new Predicate<Sighting>() {
      @Override
      public boolean apply(Sighting input) {
        BreedingBirdCode code;
        if (!input.hasSightingInfo()) {
          code = BreedingBirdCode.NONE;
        } else {
          code = input.getSightingInfo().getBreedingBirdCode();
        }
        return computedCodes.contains(code);
      }
    };
  }

  private BreedingBirdLevel breedingBirdLevel() {
    return BreedingBirdLevel.values()[levelOptions.getSelectedIndex()];
  }

  @Override public boolean isNoOp() {
    return QueryType.ANY.equals(queryOptions.getSelectedItem());
  }

  @Override
  public JsonElement persist() {
    Persisted persisted = new Persisted((QueryType) queryOptions.getSelectedItem(), breedingBirdLevel());
    return GSON.toJsonTree(persisted);
  }

  @Override
  public void restore(JsonElement json) {
    Persisted persisted = GSON.fromJson(json, Persisted.class);
    queryOptions.setSelectedItem(persisted.type);
    levelOptions.setSelectedItem(persisted.level);
  }
  
  static private final Gson GSON = new Gson();
  static class Persisted {
    public Persisted(QueryType type, BreedingBirdLevel level) {
      this.type = type;
      this.level = level;
    }
    
    QueryType type;
    BreedingBirdLevel level;
  }
}
