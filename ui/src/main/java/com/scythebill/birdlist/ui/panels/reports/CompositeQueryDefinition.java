/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.util.Collection;
import java.util.List;

import javax.annotation.Nullable;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.query.QueryDefinition;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryField;

/**
 * QueryDefinition that joins multiple query fields together.
 */
class CompositeQueryDefinition implements QueryDefinition {

  enum BooleanOperation {
    ALL(Name.QUERY_AND),
    ANY(Name.QUERY_OR),
    NEVER(Name.QUERY_NEVER),
    NEVER_ALL(Name.QUERY_NEVER_ALL);
    
    private final Name text;
    BooleanOperation(Name text) {
      this.text = text;
    }
    
    @Override
    public String toString() {
      return Messages.getMessage(text);
    }
  }

  private final Optional<Preprocessor> preprocessor;
  private final Predicate<Sighting> predicate;
  private final ImmutableList<QueryDefinition> definitions;
  
  public CompositeQueryDefinition(
      ReportSet reportSet,
      List<QueryField> queryFields,
      List<BooleanOperation> booleanOperations,
      Taxonomy taxonomy,
      Taxon.Type depth) {
    Preconditions.checkState(queryFields.size() == booleanOperations.size());
    List<Predicate<Sighting>> orPredicates = Lists.newArrayList();
    List<Predicate<Sighting>> andPredicates = Lists.newArrayList();
    List<QueryDefinition> neverAllQueryDefinitions = Lists.newArrayList();
    List<Optional<QueryDefinition.Preprocessor>> preprocessors = Lists.newArrayList();
    ImmutableList.Builder<QueryDefinition> definitionsBuilder = ImmutableList.builder();
    
    // Create all the sub-query definitions
    for (int i = 0; i < queryFields.size(); i++) {
      QueryField queryField = queryFields.get(i);
      BooleanOperation booleanOperation = booleanOperations.get(i);
      
      QueryDefinition queryDefinition = queryField.queryDefinition(reportSet, depth);
      
      if (booleanOperation == BooleanOperation.NEVER_ALL) {
        neverAllQueryDefinitions.add(queryDefinition);
      } else {
        if (booleanOperation == BooleanOperation.NEVER) {
          queryDefinition = new NeverQueryDefinition(queryDefinition, taxonomy, depth);
          booleanOperation = BooleanOperation.ALL;
        }
        
        if (booleanOperation == BooleanOperation.ALL) {
          andPredicates.add(queryDefinition.predicate());
        } else {
          orPredicates.add(queryDefinition.predicate());
        }
        preprocessors.add(queryDefinition.preprocessor());
        definitionsBuilder.add(queryDefinition);
      }
    }
    
    // Transform all "NEVER_ALL" entries into a single NeverQueryDefinition;
    // for a taxon to be excluded, it must have a sighting that matches *all*
    // of these query definitions.  
    if (!neverAllQueryDefinitions.isEmpty()) {
      NeverQueryDefinition neverAll = new NeverQueryDefinition(
          neverAllQueryDefinitions,
          taxonomy,
          depth);
      preprocessors.add(neverAll.preprocessor());
      definitionsBuilder.add(neverAll);
      andPredicates.add(neverAll.predicate());
    }

    definitions = definitionsBuilder.build();
    
    // Join all the predicates;  all the or's together, then all the ands
    if (!orPredicates.isEmpty()) {
      andPredicates.add(Predicates.or(orPredicates));
    }
    predicate = Predicates.and(andPredicates);
    
    // Join all the preprocessors (if any)
    final ImmutableList<Preprocessor> actualPreprocessors = ImmutableList.copyOf(
        Optional.presentInstances(preprocessors));
    if (actualPreprocessors.isEmpty()) {
      preprocessor = Optional.absent();
    } else {
      preprocessor = Optional.<Preprocessor>of(new Preprocessor() {
        @Override
        public void preprocess(Sighting sighting, Predicate<Sighting> countablePredicate) {
          for (Preprocessor preprocessor : actualPreprocessors) {
            preprocessor.preprocess(sighting, countablePredicate);
          }
        }

        @Override
        public void reset() {
          for (Preprocessor preprocessor : actualPreprocessors) {
            preprocessor.reset();
          }
        }
     });
    }
  }
  
  @Override
  public Optional<Preprocessor> preprocessor() {
    return preprocessor;
  }

  @Override
  public Predicate<Sighting> predicate() {
    return predicate;
  }

  @Override
  public Optional<QueryAnnotation> annotate(Sighting sighting, SightingTaxon taxon) {
    for (QueryDefinition definition : definitions) {
      Optional<QueryAnnotation> annotate = definition.annotate(sighting, taxon);
      if (annotate.isPresent()) {
        return annotate;
      }
    }
    return Optional.absent();
  }

  static class CompositePostProcessor implements PostProcessor {
    private final ImmutableList<PostProcessor> postProcessors;

    public CompositePostProcessor(Iterable<PostProcessor> postProcessors) {
      this.postProcessors = ImmutableList.copyOf(postProcessors);
    }

    @Override
    public boolean acceptTaxon(SightingTaxon sightingTaxon, Collection<Sighting> sightings,
        @Nullable Predicate<Sighting> countablePredicate) {
      for (PostProcessor postProcessor : postProcessors) {
        if (!postProcessor.acceptTaxon(sightingTaxon, sightings, countablePredicate)) {
          return false;
        }
      }
      
      return true;
    }
  }
  
  @Override
  public Optional<PostProcessor> postprocessor() {
    List<PostProcessor> postProcessors = definitions.stream()
        .map(QueryDefinition::postprocessor)
        .filter(Optional::isPresent)
        .map(Optional::get)
        .collect(ImmutableList.toImmutableList());
    if (postProcessors.isEmpty()) {
      return Optional.absent();
    }
    
    if (postProcessors.size() == 1) {
      return Optional.of(postProcessors.get(0));
    }
    
    return Optional.of(new CompositePostProcessor(postProcessors));
  }  
  
}
