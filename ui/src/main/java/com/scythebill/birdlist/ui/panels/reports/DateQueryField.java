/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Locale;

import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.joda.time.DateTimeFieldType;
import org.joda.time.LocalDate;
import org.joda.time.ReadablePartial;
import org.joda.time.chrono.GJChronology;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.query.SightingComparators;
import com.scythebill.birdlist.model.query.SightingPredicates;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.ui.components.DatePanel;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;

/**
 * QueryField for date queries.
 */
class DateQueryField extends AbstractQueryField {
  private enum QueryType {
    DURING(Name.DATE_IS_DURING),
    BETWEEN(Name.DATE_IS_BETWEEN),
    AFTER(Name.DATE_IS_AFTER),
    BEFORE(Name.DATE_IS_BEFORE),
    THIS_YEAR(Name.DATE_IS_THIS_YEAR),
    TODAY(Name.DATE_IS_TODAY),
    NOT_DURING(Name.DATE_IS_NOT_DURING),
    NOT_SET(Name.DATE_IS_NOT_SET);
    
    private final Name text;
    QueryType(Name text) {
      this.text = text;
    }
    
    @Override
    public String toString() {
      return Messages.getMessage(text);
    }
  }

  private final JComboBox<QueryType> whenOptions = new JComboBox<>(QueryType.values());
  private final DatePanel fromDatePanel = new DatePanel();
  private final DatePanel toDatePanel = new DatePanel();
  private final JPanel valuePanel;
  
  public DateQueryField() {
    super(QueryFieldType.DATE);
    
    whenOptions.addActionListener(e -> {
      updateDateVisibility();
      firePredicateUpdated();
    });
    
    valuePanel = new JPanel();
    BoxLayout box = new BoxLayout(valuePanel, SwingConstants.VERTICAL);
    valuePanel.setLayout(box);
    valuePanel.add(fromDatePanel);
    valuePanel.add(toDatePanel);
    updateDateVisibility();
    
    PropertyChangeListener setDatePredicateListener = new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        if (((DatePanel) evt.getSource()).isVisible()) {
          firePredicateUpdated();
        }
      }
    };

    fromDatePanel.addPropertyChangeListener("value", setDatePredicateListener); 
    toDatePanel.addPropertyChangeListener("value", setDatePredicateListener);
  }

  @Override
  public JComponent getComparisonChooser() {
    return whenOptions;
  }

  @Override
  public JComponent getValueField() {
    return valuePanel;
  }

  @Override
  public boolean isSingleLine() {
    // This uses two lines (at least sometimes), so it gets a slightly different layout.
    return false;
  }
  
  @Override
  public Predicate<Sighting> predicate(Taxon.Type depth) {
    QueryType whenOption = (QueryType) whenOptions.getSelectedItem();
    if (QueryType.TODAY.equals(whenOption)) {
      final LocalDate now = new LocalDate(GJChronology.getInstance());
      return new Predicate<Sighting>() {
        @Override public boolean apply(Sighting sighting) {
          ReadablePartial date = sighting.getDateAsPartial();
          if (date == null) {
            return false;
          }
          
          if (!date.isSupported(DateTimeFieldType.dayOfMonth())
              || !date.isSupported(DateTimeFieldType.monthOfYear())
              || !date.isSupported(DateTimeFieldType.year())) {
            return false;
          }
          
          return date.get(DateTimeFieldType.dayOfMonth()) == now.getDayOfMonth()
              && date.get(DateTimeFieldType.monthOfYear()) == now.getMonthOfYear()
              && date.get(DateTimeFieldType.year()) == now.getYear();
        }
      };
    } else if (QueryType.THIS_YEAR.equals(whenOption)) {
      final LocalDate now = new LocalDate(GJChronology.getInstance());
      return new Predicate<Sighting>() {
        @Override public boolean apply(Sighting sighting) {
          ReadablePartial date = sighting.getDateAsPartial();
          if (date == null) {
            return false;
          }
          
          if (!date.isSupported(DateTimeFieldType.year())) {
            return false;
          }
          
          return date.get(DateTimeFieldType.year()) == now.getYear();
        }
      };
    } else if (QueryType.NOT_SET.equals(whenOption)) {
      return sighting -> sighting.getDateAsPartial() == null;
    }
    
    // For BETWEEN queries...
    if (QueryType.BETWEEN.equals(whenOption)) {
      ReadablePartial from = fromDatePanel.getValue();
      ReadablePartial to = toDatePanel.getValue();
      // with both a "from" and "to"
      if (from != null && to != null
          // ... neither of which include years
          && !to.isSupported(DateTimeFieldType.year())
          && !from.isSupported(DateTimeFieldType.year())
          // ...and "to" < "from"  
          && SightingComparators.comparePartials(to, from) < 0) {
        // ... then reinterpret to support "between December and March".
        return Predicates.or(
            SightingPredicates.afterOrEquals(from),
            SightingPredicates.beforeOrEquals(to));
      }
    }
    
    Predicate<Sighting> fromPredicate = getFromPredicate(whenOption);
    Predicate<Sighting> toPredicate = getToPredicate(whenOption);
    
    if (fromPredicate == null) {
      return toPredicate == null ? Predicates.<Sighting>alwaysTrue() : toPredicate;
    } else if (toPredicate == null) {
      return fromPredicate;
    } else {
      if (QueryType.NOT_DURING == whenOption) {
        return Predicates.not(Predicates.and(fromPredicate, toPredicate));
      } else {
        return Predicates.and(fromPredicate, toPredicate);
      }
    }
  }

  private Predicate<Sighting> getToPredicate(QueryType whenOption) {
    Predicate<Sighting> toPredicate = null;
    if (QueryType.BETWEEN == whenOption
        || QueryType.BEFORE == whenOption
        || QueryType.DURING == whenOption
        || QueryType.NOT_DURING == whenOption) {
      ReadablePartial to = (QueryType.DURING == whenOption
                            || QueryType.NOT_DURING == whenOption)
          ? fromDatePanel.getValue() : toDatePanel.getValue();
      if (to != null) {
        toPredicate = whenOption == QueryType.BEFORE 
            ? SightingPredicates.before(to)
            : SightingPredicates.beforeOrEquals(to);
      }
    }
    return toPredicate;
  }

  private Predicate<Sighting> getFromPredicate(QueryType whenOption) {
    Predicate<Sighting> fromPredicate = null;
    if (QueryType.BETWEEN == whenOption
        || QueryType.AFTER == whenOption
        || QueryType.DURING == whenOption
        || QueryType.NOT_DURING == whenOption) {
      ReadablePartial after = fromDatePanel.getValue();
      if (after != null) {
        fromPredicate = whenOption == QueryType.AFTER 
            ? SightingPredicates.after(after)
            : SightingPredicates.afterOrEquals(after);
      }
    }
    return fromPredicate;
  }

  @Override public boolean isNoOp() {
    QueryType whenOption = (QueryType) whenOptions.getSelectedItem();
    Predicate<Sighting> fromPredicate = getFromPredicate(whenOption);
    Predicate<Sighting> toPredicate = getToPredicate(whenOption);
    return (fromPredicate == null && toPredicate == null);
  }
  
  private void updateDateVisibility() {
    Object selectedValue = whenOptions.getSelectedItem();
    toDatePanel.setVisible(
        QueryType.BETWEEN.equals(selectedValue)
        || QueryType.BEFORE.equals(selectedValue));
    fromDatePanel.setVisible(
        QueryType.BETWEEN.equals(selectedValue)
        || QueryType.AFTER.equals(selectedValue)
        || QueryType.DURING.equals(selectedValue)
        || QueryType.NOT_DURING.equals(selectedValue));
  }

  @Override
  public Optional<String> name() {
    Object selectedValue = whenOptions.getSelectedItem();
    if (QueryType.THIS_YEAR == selectedValue) {
      return Optional.of(Messages.getMessage(Name.DATE_YEAR));
    } else if (QueryType.TODAY == selectedValue) {
      return Optional.of(Messages.getMessage(Name.DATE_TODAY));
    } else if (QueryType.NOT_SET == selectedValue) {
      return Optional.of(Messages.getMessage(Name.DATE_NOT_SET));
    } else if (QueryType.DURING == selectedValue) {
      ReadablePartial from = fromDatePanel.getValue();
      if (from != null) {
        return Optional.of(PartialIO.toShortUserString(from, Locale.getDefault()));
      }      
    } else if (QueryType.NOT_DURING == selectedValue) {
      ReadablePartial from = fromDatePanel.getValue();
      if (from != null) {
        return Optional.of(
            Messages.getFormattedMessage(
                Name.DATE_NOT_FORMAT,
                PartialIO.toShortUserString(from, Locale.getDefault())));
      }      
    } else if (QueryType.AFTER == selectedValue) {
      ReadablePartial from = fromDatePanel.getValue();
      if (from != null) {
        return Optional.of(
            Messages.getFormattedMessage(Name.DATE_AFTER_FORMAT, PartialIO.toShortUserString(from, Locale.getDefault())));
      }
    } else if (QueryType.BEFORE == selectedValue) {
      ReadablePartial to = toDatePanel.getValue();
      if (to != null) {
        return Optional.of(
            Messages.getFormattedMessage(Name.DATE_BEFORE_FORMAT, PartialIO.toShortUserString(to, Locale.getDefault())));
      }
    }
    return Optional.absent();
  }
  
  @Override
  public Optional<String> abbreviation() {
    Object selectedValue = whenOptions.getSelectedItem();
    if (QueryType.BETWEEN.equals(selectedValue)) {
      ReadablePartial from = fromDatePanel.getValue();
      ReadablePartial to = toDatePanel.getValue();
      if (from == null) {
        if (to != null) {
          return Optional.of(PartialIO.toString(to));
        }
      } else { // from != null
        if (to == null) {
          return Optional.of(PartialIO.toString(from));
        } else {
          // Only provide abbreviations for "between" when the year is specified, at least for now.
          if (from.isSupported(DateTimeFieldType.year())
              && to.isSupported(DateTimeFieldType.year())) {
            int fromYear = from.get(DateTimeFieldType.year());
            int toYear = to.get(DateTimeFieldType.year());
            if (fromYear == toYear) {
              return Optional.of(Integer.toString(fromYear));
            } else {
              return Optional.of(String.format("%s-%s", fromYear, toYear));
            }
          }
        }
      }
      
      return Optional.absent();
    } else if (QueryType.AFTER.equals(selectedValue)
        || QueryType.DURING.equals(selectedValue)
        || QueryType.NOT_DURING.equals(selectedValue)) {
      ReadablePartial from = fromDatePanel.getValue();
      if (from != null) {
        return Optional.of(PartialIO.toString(from));
      }
    } else if (QueryType.BEFORE.equals(selectedValue)) {
      ReadablePartial to = fromDatePanel.getValue();
      if (to != null) {
        return Optional.of(PartialIO.toString(to));
      }
    } else if (QueryType.TODAY.equals(selectedValue)) {
      return Optional.of(PartialIO.toString(new LocalDate(GJChronology.getInstance())));
    } else if (QueryType.THIS_YEAR.equals(selectedValue)) {
      LocalDate today = new LocalDate(GJChronology.getInstance());
      return Optional.of(Integer.toString(today.getYear()));
    }
    
    return Optional.absent();
  }

  @Override
  public JsonElement persist() {
    Persisted persisted = new Persisted(
        (QueryType) whenOptions.getSelectedItem(),
        toStringMaybeNull(fromDatePanel.getValue()),
        toStringMaybeNull(toDatePanel.getValue()));
    return GSON.toJsonTree(persisted);
  }

  @Override
  public void restore(JsonElement json) {
    Persisted persisted = GSON.fromJson(json, Persisted.class);
    whenOptions.setSelectedItem(persisted.type);
    fromDatePanel.setValue(fromStringMaybeNull(persisted.from));
    toDatePanel.setValue(fromStringMaybeNull(persisted.to));
  }
  
  private String toStringMaybeNull(ReadablePartial partial) {
    return (partial == null || partial.size() == 0)
        ? null : PartialIO.toString(partial);
  }

  private ReadablePartial fromStringMaybeNull(String string) {
    return string == null ? null : PartialIO.fromString(string);
  }

  static private final Gson GSON = new Gson();
  static class Persisted {
    public Persisted(QueryType type, String from, String to) {
      this.type = type;
      this.from = from;
      this.to = to;
    }
    
    QueryType type;
    String from;
    String to;
  }
}
