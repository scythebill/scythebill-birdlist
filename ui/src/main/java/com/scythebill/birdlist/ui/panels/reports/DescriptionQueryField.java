/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;

/**
 * QueryField for description queries.
 */
class DescriptionQueryField extends AbstractQueryField {
  private enum QueryType {
    CONTAINS(Name.DESCRIPTION_CONTAINS, false),
    DOES_NOT_CONTAIN(Name.DESCRIPTION_DOESNT_CONTAIN, false),
    CONTAINS_CASE_SENSITIVE(Name.DESCRIPTION_CONTAINS_CASE_SENSITIVE, true),
    DOES_NOT_CONTAIN_CASE_SENSITIVE(Name.DESCRIPTION_DOESNT_CONTAIN_CASE_SENSITIVE, true);
    
    private final Name text;
    private final boolean caseSensitive;
    QueryType(Name text, boolean caseSensitive) {
      this.text = text;
      this.caseSensitive = caseSensitive;
    }
    
    @Override
    public String toString() {
      return Messages.getMessage(text);
    }
    
    boolean isCaseSensitive() {
      return caseSensitive;
    }
  }

  private final JComboBox<QueryType> options = new JComboBox<>(QueryType.values());
  private final JTextField text;
  private Timer timer;
  
  public DescriptionQueryField() {
    super(QueryFieldType.SIGHTING_NOTES);
    
    options.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent e) {
        firePredicateUpdated();
      }
    });
    
    text = new JTextField(20);
    timer = new Timer(200, new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        firePredicateUpdated();
      }
    });
    timer.setRepeats(false);

    text.getDocument().addDocumentListener(new DocumentListener() {
      @Override
      public void removeUpdate(DocumentEvent e) {
        timer.restart();
      }
      
      @Override
      public void insertUpdate(DocumentEvent e) {
        timer.restart();
      }
      
      @Override
      public void changedUpdate(DocumentEvent e) {
        timer.restart();
      }
    });
  }

  @Override
  public JComponent getComparisonChooser() {
    return options;
  }

  @Override
  public JComponent getValueField() {
    return text;
  }

  @Override
  public Predicate<Sighting> predicate(Taxon.Type depth) {
    QueryType whenOption = (QueryType) options.getSelectedItem();
    final boolean isCaseSensitive = whenOption.isCaseSensitive();
    final String containsText = isCaseSensitive
        ? text.getText()
        : text.getText().toLowerCase();
        
    Predicate<Sighting> containsPredicate = new Predicate<Sighting>() {
      @Override public boolean apply(Sighting input) {
        if (!input.hasSightingInfo()) {
          return false;
        }

        String description = input.getSightingInfo().getDescription();
        if (description == null) {
          return false;
        }
        
        if (!isCaseSensitive) {
          description = description.toLowerCase();
        }
        
        return description.contains(containsText);
      }
    };
    
    if (whenOption == QueryType.DOES_NOT_CONTAIN
        || whenOption == QueryType.DOES_NOT_CONTAIN_CASE_SENSITIVE) {
      return Predicates.not(containsPredicate);
    }

    return containsPredicate;
  }

  @Override public boolean isNoOp() {
    return false;
  }  

  @Override
  public JsonElement persist() {
    Persisted persisted = new Persisted(
        (QueryType) options.getSelectedItem(),
        text.getText());
    return GSON.toJsonTree(persisted);
  }

  @Override
  public void restore(JsonElement json) {
    Persisted persisted = GSON.fromJson(json, Persisted.class);
    options.setSelectedItem(persisted.type);
    text.setText(persisted.text);
  }
  
  static private final Gson GSON = new Gson();
  static class Persisted {
    public Persisted(QueryType type, String text) {
      this.type = type;
      this.text = text;
    }
    
    QueryType type;
    String text;
  }
}
