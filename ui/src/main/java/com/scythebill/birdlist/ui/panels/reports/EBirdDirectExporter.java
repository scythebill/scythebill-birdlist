/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.Set;

import javax.annotation.Nullable;

import org.joda.time.ReadablePartial;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.io.TimeIO;
import com.scythebill.birdlist.model.query.QueryProcessor;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.query.SightingPredicates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.guice.Clements;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;

/**
 * Supports direct writing of eBird exports without explicitly choosing a query.
 * (Useful for just after entering sightings.)
 */
public class EBirdDirectExporter {
  private final FileDialogs fileDialogs;
  private final ReportSet reportSet;
  private final Taxonomy clements;
  private final Alerts alerts;

  @Inject
  public EBirdDirectExporter(
      FileDialogs fileDialogs,
      ReportSet reportSet,
      @Clements Taxonomy clements,
      Alerts alerts) {
    this.fileDialogs = fileDialogs;
    this.reportSet = reportSet;
    this.clements = clements;
    this.alerts = alerts;
  }
  
  public void doExport(
      Component parent,
      final Location location,
      final ReadablePartial date,
      final @Nullable ReadablePartial time) {
    
    // Assemble a query executor that verifies the location, date, and (maybe) time.
    QueryExecutor queryExecutor = new QueryExecutor() {
      @Override
      public Location getRootLocation() {
        return location;
      }
      
      @Override
      public Set<ReportHints> getReportHints() {
        return ImmutableSet.<ReportHints>of();
      }
      
      @Override
      public Optional<String> getReportAbbreviation() {
        if (time == null) {
          return Optional.of(String.format("%s-%s", location.getDisplayName(), PartialIO.toString(date)));
        } else {
          return Optional.of(String.format("%s-%s-%s",
              location.getDisplayName(), PartialIO.toString(date), TimeIO.toFileString(time)));
        }
      }
      
      @Override
      public Optional<String> getReportName() {
        return getReportAbbreviation();
      }

      @Override
      public QueryResults executeQuery(
          Type depthOverride, Taxonomy taxonomyOverride) {
        QueryProcessor queryProcessor = new QueryProcessor(reportSet, taxonomyOverride);
        Predicate<Sighting> sightingPredicate = Predicates.and(
            SightingPredicates.is(location, reportSet.getLocations()),
            SightingPredicates.dateIsExactly(date));
        if (time != null) {
          sightingPredicate = Predicates.and(sightingPredicate,
              SightingPredicates.timeIsExactly(time));
        }
            
        // Send everything to eBird
        Predicate<Sighting> countablePredicate = Predicates.alwaysTrue();
        return queryProcessor.runQuery(sightingPredicate, countablePredicate, depthOverride);
      }

      @Override
      public QueryExecutor onlyCountable() {
        throw new UnsupportedOperationException();
      }

      @Override
      public QueryExecutor includeIncompatibleSightings() {
        throw new UnsupportedOperationException();
      }
    };
    new EbirdExportAction(fileDialogs, reportSet, queryExecutor, clements, alerts)
        .actionPerformed(new ActionEvent(parent, 0, ""));
  }
}
