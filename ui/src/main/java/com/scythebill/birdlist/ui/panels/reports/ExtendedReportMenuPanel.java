/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.ExtendedTaxonomyChecklists;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.ui.actions.IdentifyLifersFromChecklistsAction;
import com.scythebill.birdlist.ui.actions.ReturnAction;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.app.Titled;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.events.TaxonomyChangedEvent;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.ShrinkToFit;

/**
 * Panel displaying options for extended reports.
 */
public class ExtendedReportMenuPanel extends JPanel implements FontsUpdatedListener, ShrinkToFit, Titled {
  private JButton returnButton;
  private JButton bigDaysAndYears;
  private JButton yearComparison;
  private JButton identifyLifers;
  private JButton totalTicks;
  private JPanel centerBox;
  private JLabel extendedReportLabel;
  private JLabel bigDaysAndYearsLabel;
  private JLabel yearComparisonLabel;
  private JLabel identifyLifersLabel;
  private JLabel emptyLabel;
  private final FontManager fontManager;
  private JLabel totalTicksLabel;
  private TaxonomyStore taxonomyStore;
  private JButton splitsAndLumps;
  private JLabel splitsAndLumpsLabel;
  private final ReportSet reportSet;
  
  @Inject
  public ExtendedReportMenuPanel(
      ReturnAction returnAction,
      IdentifyLifersFromChecklistsAction identifyLifersFromChecklistsAction,
      NavigableFrame navigableFrame,
      FontManager fontManager,
      EventBusRegistrar eventBusRegistrar,
      TaxonomyStore taxonomyStore,
      ReportSet reportSet) {
    this.fontManager = fontManager;
    this.taxonomyStore = taxonomyStore;
    this.reportSet = reportSet;
    initGUI();
    returnButton.setAction(returnAction);
    bigDaysAndYears.addActionListener(e -> navigableFrame.navigateTo("bigDay"));
    yearComparison.addActionListener(e -> navigableFrame.navigateTo("yearComparison"));
    splitsAndLumps.addActionListener(e -> navigableFrame.navigateTo("splitsAndLumps"));
    totalTicks.addActionListener(e -> navigableFrame.navigateTo("totalTicks"));
    identifyLifers.addActionListener(identifyLifersFromChecklistsAction);
    returnButton.setAction(returnAction);
    eventBusRegistrar.registerWhenInHierarchy(this);
    taxonomyChanged(null);
  }

  private void initGUI() {
    setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

    centerBox = new JPanel();
    add(centerBox);
    
    extendedReportLabel = new JLabel(
        Messages.getMessage(Name.SPECIAL_REPORTS_TITLE));
    extendedReportLabel.putClientProperty(FontManager.PLAIN_LABEL, true);

    bigDaysAndYears = new JButton();
    bigDaysAndYears.setText(Messages.getMessage(Name.BIG_DAYS_MONTHS_YEARS));
    bigDaysAndYears.putClientProperty("Quaqua.Button.style", "bevel");
    
    yearComparison = new JButton();
    yearComparison.setText(Messages.getMessage(Name.YEAR_COMPARISONS));
    yearComparison.putClientProperty("Quaqua.Button.style", "bevel");

    totalTicks = new JButton();
    totalTicks.setText(Messages.getMessage(Name.TOTAL_TICKS));
    totalTicks.putClientProperty("Quaqua.Button.style", "bevel");
        
    identifyLifers = new JButton();
    identifyLifers.setText(Messages.getMessage(Name.WORLD_LIFER_MAP));
        
    identifyLifers.putClientProperty("Quaqua.Button.style", "bevel");

    splitsAndLumps = new JButton();
    splitsAndLumps.setText(Messages.getMessage(Name.SPLITS_AND_LUMPS));
    splitsAndLumps.putClientProperty("Quaqua.Button.style", "bevel");

    returnButton = new JButton();
    returnButton.putClientProperty("Quaqua.Button.style", "bevel");

    bigDaysAndYearsLabel = new JLabel(Messages.getMessage(Name.BIG_DAYS_MONTHS_YEARS_EXPLANATION));
    bigDaysAndYearsLabel.putClientProperty(FontManager.PLAIN_LABEL, true);

    yearComparisonLabel = new JLabel(Messages.getMessage(Name.YEAR_COMPARISONS_EXPLANATION));
    yearComparisonLabel.putClientProperty(FontManager.PLAIN_LABEL, true);

    totalTicksLabel = new JLabel(Messages.getMessage(Name.TOTAL_TICKS_EXPLANATION));
    totalTicksLabel.putClientProperty(FontManager.PLAIN_LABEL, true);

    identifyLifersLabel = new JLabel(Messages.getMessage(Name.WORLD_LIFER_MAP_EXPLANATION));
    identifyLifersLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    
    splitsAndLumpsLabel = new JLabel(Messages.getMessage(Name.SPLITS_AND_LUMPS_EXPLANATION));
    splitsAndLumpsLabel.putClientProperty(FontManager.PLAIN_LABEL, true);

    emptyLabel = new JLabel();

    fontManager.applyTo(this);
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    centerBox.setPreferredSize(fontManager.scale(new Dimension(700, 600)));
    centerBox.setMinimumSize(centerBox.getPreferredSize());
    centerBox.setBorder(new EmptyBorder(
        fontManager.scale(20),
        fontManager.scale(60),
        fontManager.scale(20),
        fontManager.scale(60)));

    GroupLayout layout = new GroupLayout(centerBox);
    centerBox.setLayout(layout);
    layout.setHorizontalGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(layout.createParallelGroup()
          .addComponent(extendedReportLabel)
          .addGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup()
                .addComponent(bigDaysAndYears, Alignment.LEADING,
                    PREFERRED_SIZE, fontManager.scale(174), PREFERRED_SIZE)
                .addComponent(totalTicks, Alignment.LEADING,
                    PREFERRED_SIZE, fontManager.scale(174), PREFERRED_SIZE)
                .addComponent(yearComparison, Alignment.LEADING,
                    PREFERRED_SIZE, fontManager.scale(174), PREFERRED_SIZE)
                .addComponent(splitsAndLumps, Alignment.LEADING,
                    PREFERRED_SIZE, fontManager.scale(174), PREFERRED_SIZE)
                .addComponent(identifyLifers, Alignment.LEADING,
                    PREFERRED_SIZE, fontManager.scale(174), PREFERRED_SIZE)
                .addComponent(returnButton, Alignment.LEADING,
                    PREFERRED_SIZE, fontManager.scale(174), PREFERRED_SIZE))
            .addGap(63)
            .addGroup(layout.createParallelGroup()
                .addComponent(bigDaysAndYearsLabel)
                .addComponent(totalTicksLabel)
                .addComponent(yearComparisonLabel)
                .addComponent(splitsAndLumpsLabel)
                .addComponent(identifyLifersLabel)
                .addComponent(emptyLabel))))
        .addContainerGap(67, 67));
      layout.setVerticalGroup(layout.createSequentialGroup()
        .addComponent(extendedReportLabel)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(bigDaysAndYears, Alignment.BASELINE,
                PREFERRED_SIZE, fontManager.scale(58), PREFERRED_SIZE)
            .addComponent(bigDaysAndYearsLabel, Alignment.BASELINE, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
        .addGap(14)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(totalTicks, Alignment.BASELINE,
                PREFERRED_SIZE, fontManager.scale(58), PREFERRED_SIZE)
            .addComponent(totalTicksLabel, Alignment.BASELINE, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
        .addGap(14)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(yearComparison, Alignment.BASELINE,
                PREFERRED_SIZE, fontManager.scale(58), PREFERRED_SIZE)
            .addComponent(yearComparisonLabel, Alignment.BASELINE, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
        .addGap(14)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(splitsAndLumps, Alignment.BASELINE,
                PREFERRED_SIZE, fontManager.scale(58), PREFERRED_SIZE)
            .addComponent(splitsAndLumpsLabel, Alignment.BASELINE, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
        .addGap(14)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(identifyLifers, Alignment.BASELINE,
                PREFERRED_SIZE, fontManager.scale(58), PREFERRED_SIZE)
            .addComponent(identifyLifersLabel, Alignment.BASELINE, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
        .addGap(14)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(returnButton, Alignment.BASELINE,
                PREFERRED_SIZE, fontManager.scale(58), PREFERRED_SIZE)
            .addComponent(emptyLabel))
        .addContainerGap(82, 82));
      layout.linkSize(bigDaysAndYears, totalTicks, identifyLifers, returnButton);
  }

  @Subscribe
  public void taxonomyChanged(TaxonomyChangedEvent event) {
    boolean hasChecklists;
    if (taxonomyStore.getTaxonomy().isBuiltIn()) {
      hasChecklists = true;
    } else {
      ExtendedTaxonomyChecklists checklists = reportSet.getExtendedTaxonomyChecklist(taxonomyStore.getTaxonomy().getId());
      hasChecklists = checklists != null && !checklists.getChecklists().isEmpty();
    }
    identifyLifers.setEnabled(hasChecklists);
    splitsAndLumps.setEnabled(taxonomyStore.getTaxonomy().isBuiltIn());
  }

  @Override
  public String getTitle() {
    return Messages.getMessage(Name.SPECIAL_REPORTS);
  }
}
