/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import javax.swing.JComponent;
import javax.swing.JPanel;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.Indexer;
import com.scythebill.birdlist.model.util.ToString;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;

/**
 * QueryField for finding all sightings within a single family.
 */
class FamilyQueryField extends AbstractQueryField {
  private final IndexerPanel<String> familyIndexer;
  private final TaxonomyStore taxonomyStore;
  private final JPanel noComparison;

  public FamilyQueryField(TaxonomyStore taxonomyStore) {
    super(QueryFieldType.FAMILY);
    this.taxonomyStore = taxonomyStore;
    
    familyIndexer = new IndexerPanel<String>();
    familyIndexer.setPreviewText(Name.START_TYPING_A_FAMILY);
    noComparison = new JPanel();
    
    attachIndexers();
    familyIndexer.addPropertyChangeListener("value",
        e -> firePredicateUpdated());
  }

  @Override
  public JComponent getComparisonChooser() {
    return noComparison;
  }

  @Override
  public JComponent getValueField() {
    return familyIndexer;
  }
  
  @Override
  public Predicate<Sighting> predicate(Taxon.Type depth) {
    String id = familyIndexer.getValue();
    if (id == null) {
      return Predicates.alwaysTrue();
    } else {
      Taxon family = taxonomyStore.getTaxonomy().getTaxon(id);
      // If the family can't be found, early abort.
      if (family == null) {
        return Predicates.alwaysFalse();
      }
      
      return sighting -> {
        // Skip incompatible taxa 
        if (!TaxonUtils.areCompatible(taxonomyStore.getTaxonomy(), sighting.getTaxonomy())) {
          return false;
        }
        
        Resolved resolved = sighting.getTaxon().resolve(taxonomyStore.getTaxonomy());
        // Get the first taxon - doesn't really matter which, since we don't have hybrids or sp's across families
        Taxon taxon = resolved.getTaxa().iterator().next();
        return TaxonUtils.isChildOf(family, taxon);
      };
    }
  }

  @Override public boolean isNoOp() {
    return familyIndexer.getValue() == null;
  }

  @Override
  public Optional<String> abbreviation() {
    String id = familyIndexer.getValue();
    if (id != null) {
      Taxon taxon = taxonomyStore.getTaxonomy().getTaxon(id);
      // If the family can't be found, early abort.
      if (taxon == null) {
        return Optional.absent();
      }
      
      if (taxon.getCommonName() != null) {
        return Optional.of(taxon.getCommonName());
      }
      
      return Optional.of(taxon.getName());
    }
    
    return Optional.absent();
  }

  @Override
  public Optional<String> name() {
    return abbreviation();
  }

  @Override
  public JsonElement persist() {
    Persisted persisted = new Persisted(familyIndexer.getValue());
    return GSON.toJsonTree(persisted);
  }

  @Override
  public void restore(JsonElement json) {
    Persisted persisted = GSON.fromJson(json, Persisted.class);
    if (persisted.id == null) {
      familyIndexer.setValue(null);
    } else {
      familyIndexer.setValue(persisted.id);
    }
  }
  
  @Override
  public void taxonomyUpdated() {
    attachIndexers();
  }
  
  static private final Gson GSON = new Gson();
  static class Persisted {
    public Persisted(String id) {
      this.id = id;
    }
    
    String id;
  }

  private void attachIndexers() {
    String value = familyIndexer.getValue();
    Taxonomy taxonomy = taxonomyStore.getTaxonomy();
    boolean resetIndexer = taxonomy.getTaxon(value) == null;
    // Index all the families
    Indexer<String> familyCommonNames = new Indexer<>();
    Indexer<String> familySciNames = new Indexer<>();
    TaxonUtils.visitTaxa(taxonomy,
        taxon -> {
          if (taxon.getType() == Taxon.Type.family) {
            if (taxon.getCommonName() != null) {
              familyCommonNames.add(taxon.getCommonName(), taxon.getId());
            }
            familySciNames.add(taxon.getName(), taxon.getId());
            return false;
          }
          return true;
        });
    ToString<String> commonNameToString = new ToString<String>() {
      @Override
      public String getString(String o) {
        Taxon taxon = taxonomy.getTaxon(o);
        if (taxon == null) {
          return Messages.getMessage(Name.NO_FAMILY);
        }
        return taxon.getCommonName() == null
            ? taxon.getName() : taxon.getCommonName();
      }

      @Override
      public String getPreviewString(String o) {
        return getString(o);
      }
    };
    ToString<String> sciNameToString = new ToString<String>() {
      @Override
      public String getString(String o) {
        Taxon taxon = taxonomy.getTaxon(o);
        if (taxon == null) {
          return Messages.getMessage(Name.NO_FAMILY);
        }
        return taxon.getName();
      }

      @Override
      public String getPreviewString(String o) {
        return getString(o);
      }
    };
    familyIndexer.removeAllIndexerGroups();
    familyIndexer.addIndexerGroup(commonNameToString, familyCommonNames);
    familyIndexer.addIndexerGroup(sciNameToString, familySciNames);
    if (resetIndexer) {
      familyIndexer.setTextValue("");
    }
  }
}
