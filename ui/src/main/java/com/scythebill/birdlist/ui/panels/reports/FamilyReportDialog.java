/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.awt.Component;

import javax.annotation.Nullable;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.LayoutStyle.ComponentPlacement;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences.ScientificOrCommon;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.FamilyReportPreferences.SortBy;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * Dialog for configuring the family report options.
 */
class FamilyReportDialog {
  private Alerts alerts;
  private FamilyReportPreferences familyReportPreferences;
  private FontManager fontManager;
  private NamesPreferences namesPreferences;

  @Inject
  FamilyReportDialog(
      Alerts alerts,
      FamilyReportPreferences familyReportPreferences,
      NamesPreferences namesPreferences,
      FontManager fontManager) {
    this.alerts = alerts;
    this.familyReportPreferences = familyReportPreferences;
    this.namesPreferences = namesPreferences;
    this.fontManager = fontManager;
  }

  @Nullable
  public FamilyReportPreferences getConfiguration(Component parent) {
    FamilyReportConfigurationPanel panel = new FamilyReportConfigurationPanel();
    fontManager.applyTo(panel);
    
    String formattedMessage = alerts.getFormattedDialogMessage(
        Name.FAMILY_REPORT_OPTIONS, Name.FAMILY_REPORT_OPTIONS_MESSAGE);
    
    int okCancel = alerts.showOkCancelWithPanel(
        SwingUtilities.getWindowAncestor(parent), formattedMessage, panel);
    if (okCancel != JOptionPane.OK_OPTION) {
      return null;
    }

    panel.updatePreferences();
    return familyReportPreferences;
  }
  
  class FamilyReportConfigurationPanel extends JPanel {
    private JCheckBox includeScientific;
    private JLabel sortByLabel;
    private JComboBox<SortBy> sortByCombobox;

    FamilyReportConfigurationPanel() {
      GroupLayout layout = new GroupLayout(this);
      setLayout(layout);
      
      includeScientific = new JCheckBox(
          Messages.getMessage(Name.SCIENTIFIC_NAME_QUESTION));
      includeScientific.setSelected(familyReportPreferences.includeScientific);
      
      if (namesPreferences.scientificOrCommon == ScientificOrCommon.COMMON_ONLY) {
        includeScientific.setSelected(false);
      } else if (namesPreferences.scientificOrCommon == ScientificOrCommon.SCIENTIFIC_ONLY) {
        includeScientific.setSelected(true);
      }
      
      sortByLabel = new JLabel(Messages.getMessage(Name.SORT_BY));
      sortByCombobox = new JComboBox<SortBy>(SortBy.values());
      sortByCombobox.setSelectedItem(familyReportPreferences.sortBy);
      layout.setHorizontalGroup(layout.createParallelGroup()
          .addComponent(includeScientific)
          .addGroup(
              layout.createSequentialGroup()
                  .addComponent(sortByLabel)
                  .addPreferredGap(ComponentPlacement.RELATED)
                  .addComponent(sortByCombobox)));

      layout.setVerticalGroup(layout.createSequentialGroup()
          .addComponent(includeScientific)
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addGroup(
              layout.createBaselineGroup(false, false)
                  .addComponent(sortByLabel)
                  .addComponent(sortByCombobox)));
    }
    
    void updatePreferences() {
      familyReportPreferences.includeScientific = includeScientific.isSelected();
      familyReportPreferences.sortBy = (SortBy) sortByCombobox.getSelectedItem();
    }
  }

  /**
   * Derive the scientific-and-common ordering based on a mix of the chosen setting
   * in the dialog and the setting in the names preferences.
   */
  public ScientificOrCommon getScientificOrCommonFromConfiguration(FamilyReportPreferences prefs) {
    ScientificOrCommon scientificOrCommon = namesPreferences.scientificOrCommon;
    if (prefs.includeScientific) {
      if (scientificOrCommon == ScientificOrCommon.COMMON_ONLY) {
        // Can't have COMMON_ONLY if the user asks for scientific names;  use COMMON_FIRST
        return ScientificOrCommon.COMMON_FIRST;
      }
    } else {
      switch (scientificOrCommon) {
        case SCIENTIFIC_ONLY:
        case COMMON_FIRST:
        case SCIENTIFIC_FIRST:
          // None of the above make sense if scientific names are deselected;  switch to COMMON_ONLY mode.
          return ScientificOrCommon.COMMON_ONLY;
        default:
          break;
      }
    }
    
    return scientificOrCommon;
  }
}
