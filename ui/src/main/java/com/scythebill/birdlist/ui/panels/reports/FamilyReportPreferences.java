/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import com.scythebill.birdlist.model.annotations.Preference;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Saved preferences for how reports should be output as family reports.
 */
public class FamilyReportPreferences {
  public enum SortBy {
    taxonomic(Name.SORT_TAXONOMICALLY),
    topCount(Name.SORT_MOST_RECORDED),
    topFraction(Name.SORT_GREATEST_FRACTION);
    
    private final Name name;    
    private SortBy(Name name) {
      this.name = name;
    }
    
    @Override
    public String toString() {
      return Messages.getMessage(name);
    }
  }
  
  @Preference boolean includeScientific = true;
  @Preference SortBy sortBy = SortBy.taxonomic;
}
