/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.util.Map;

import javax.annotation.Nullable;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonSyntaxException;
import com.scythebill.birdlist.model.query.QueryDefinition;
import com.scythebill.birdlist.model.query.SightingComparators;
import com.scythebill.birdlist.model.query.SightingPredicates;
import com.scythebill.birdlist.model.query.SyntheticLocation;
import com.scythebill.birdlist.model.query.SyntheticLocations;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;
import com.scythebill.birdlist.ui.util.LocationIdToString;

/**
 * QueryField for finding first sightings.
 */
class FirstSightingsQueryField extends AbstractQueryField {
  private enum QueryType {
    HIGHLIGHTED(Name.FIRST_RECORDS_HIGHLIGHTED),
    INCLUDED(Name.FIRST_RECORDS_INCLUDED);
    
    private final Name text;
    QueryType(Name text) {
      this.text = text;
    }
    
    @Override
    public String toString() {
      return Messages.getMessage(text);
    }
  }
  
  private final LocationSet locationSet;
  private final TaxonomyStore taxonomyStore;
  private final JPanel valuePanel;
  private final JComboBox<QueryType> shouldBeOptions = new JComboBox<>(QueryType.values());
  private final IndexerPanel<String> whereIndexer;
  private SyntheticLocations syntheticLocations;
  private final JLabel inLabel;
  private final JLabel emptyChooserLabel;

  public FirstSightingsQueryField(
      LocationSet locationSet, TaxonomyStore taxonomyStore) {
    super(QueryFieldType.FIRST_SIGHTINGS);
    this.locationSet = locationSet;
    this.taxonomyStore = taxonomyStore;
    this.syntheticLocations = new SyntheticLocations(locationSet);
    
    LocationIdToString idToString = new LocationIdToString(locationSet, syntheticLocations);
    
    inLabel = new JLabel(Messages.getMessage(Name.MY_FIRST_RECORDS_IN));
    inLabel.setBorder(new EmptyBorder(0, 0, 5, 0));
    whereIndexer = new IndexerPanel<String>();
    whereIndexer.setColumns(10);
    // This is not really localizable, alas.
    JLabel shouldLabel = new JLabel(Messages.getMessage(Name.SHOULD_BE));
    valuePanel = new JPanel();
    
    GroupLayout groupLayout = new GroupLayout(valuePanel);
    valuePanel.setLayout(groupLayout);
    groupLayout.setHorizontalGroup(groupLayout.createParallelGroup()
        .addGroup(groupLayout.createSequentialGroup()
            .addComponent(inLabel)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(whereIndexer, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
        .addGroup(groupLayout.createSequentialGroup()
            .addComponent(shouldLabel)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(shouldBeOptions, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)));
    groupLayout.setVerticalGroup(groupLayout.createSequentialGroup()
        .addGroup(groupLayout.createBaselineGroup(false, false)
            .addComponent(inLabel)
            .addComponent(whereIndexer))
        .addPreferredGap(ComponentPlacement.RELATED)
        .addGroup(groupLayout.createBaselineGroup(false, false)
            .addComponent(shouldLabel)
            .addComponent(shouldBeOptions)));
    
    whereIndexer.setPreviewText(Name.START_TYPING_A_LOCATION);
    whereIndexer.addIndexerGroup(idToString, idToString.createIndexer(true));
    whereIndexer.addPropertyChangeListener("value", e -> firePredicateUpdated());
    whereIndexer.setValue(null);
    shouldBeOptions.addActionListener(e -> firePredicateUpdated());
    emptyChooserLabel = new JLabel();
  }

  @Override
  public JComponent getComparisonChooser() {
    return emptyChooserLabel;
  }

  @Override
  public JComponent getValueField() {
    return valuePanel;
  }
  
  @Override
  public boolean isSingleLine() {
    // This uses two lines (at least sometimes), so it gets a slightly different layout.
    return false;
  }
  
  @Override
  public QueryDefinition queryDefinition(ReportSet reportSet, Type depth) {
    final Predicate<Sighting> locationPredicate;
    String id = whereIndexer.getValue();
    if (id == null) {
      locationPredicate = Predicates.<Sighting>alwaysTrue();
    } else {
      Location location = locationSet.getLocation(id);
      SyntheticLocation synthetic = null;
      if (location == null) {
        synthetic = syntheticLocations.byId(id);
      }
      if (synthetic != null) {
        locationPredicate = synthetic.isInPredicate();
      } else {
        locationPredicate = SightingPredicates.in(location, locationSet);
      }
    }
    
    return new FirstSightingsQueryDefinition(
        locationPredicate, depth, taxonomyStore.getTaxonomy(),
        (QueryType) shouldBeOptions.getSelectedItem());
  }

  static class FirstSightingsQueryDefinition
      implements QueryDefinition, QueryDefinition.Preprocessor, Predicate<Sighting> {

    private final static Ordering<Sighting> PREFERRED_SIGHTING = SightingComparators.preferEarlier();
    private final Map<SightingTaxon, Sighting> liferSightings = Maps.newHashMapWithExpectedSize(5000);
    private final Predicate<Sighting> locationPredicate;
    private final Type depth;
    private final Taxonomy taxonomy;
    private final QueryType queryType;

    public FirstSightingsQueryDefinition(
        Predicate<Sighting> locationPredicate,
        Type depth,
        Taxonomy taxonomy,
        QueryType queryType) {
      this.locationPredicate = locationPredicate;
      this.depth = depth;
      this.taxonomy = taxonomy;
      this.queryType = queryType;
    }

    @Override
    public Optional<Preprocessor> preprocessor() {
      return Optional.<Preprocessor>of(this);
    }

    @Override
    public Predicate<Sighting> predicate() {
      if (queryType == QueryType.HIGHLIGHTED) {
        return Predicates.alwaysTrue();
      }
      
      return this;
    }

    @Override
    public boolean apply(Sighting sighting) {
      if (!TaxonUtils.areCompatible(taxonomy, sighting.getTaxonomy())) {
        return false;
      }
      
      SightingTaxon taxon = toDepth(sighting.getTaxon());
      if (taxon == null) {
        return false;
      }
      return liferSightings.get(taxon) == sighting;
    }

    @Override
    public void reset() {
      liferSightings.clear();
    }

    @Override
    public void preprocess(Sighting sighting, Predicate<Sighting> countablePredicate) {
      if (countablePredicate != null
          && !countablePredicate.apply(sighting)) {
        return;
      }
      
      if (locationPredicate.apply(sighting)) {
        Resolved resolved = sighting.getTaxon().resolve(taxonomy);
        
        if (queryType == QueryType.INCLUDED) {
          // "included":  everything will run through the Predicate (apply(), above).
          // Fill the liferTaxon map accordingly
          SightingTaxon taxon = toDepth(sighting.getTaxon());
          preprocess(taxon, sighting);
        } else {
          // "Highlighted": not sure *what* depth we'll be asked about, so
          // store what's a "lifer" at every possible level
          SightingTaxon taxon = resolved.getSightingTaxon();
          SightingTaxon groupTaxon = resolved.getParentOfAtLeastType(Type.group);
          SightingTaxon speciesTaxon = resolved.getParentOfAtLeastType(Type.species);
          preprocess(speciesTaxon, sighting);
          if (!groupTaxon.equals(speciesTaxon)) {
            preprocess(groupTaxon, sighting);
          }
          if (!taxon.equals(groupTaxon)) {
            preprocess(taxon, sighting);
          }
        }
      }
    }
    
    private void preprocess(SightingTaxon taxon, Sighting sighting) {
      if (isPossibleLiferTaxon(taxon)) {
        Sighting existing = liferSightings.get(taxon);
        if (existing != null) {
          if (PREFERRED_SIGHTING.compare(existing, sighting) >= 0) {
            return;
          }
        }
        liferSightings.put(taxon, sighting);
      }
    }
    
    private SightingTaxon toDepth(SightingTaxon taxon) {
      Resolved resolved = taxon.resolve(taxonomy);
      return resolved.getParentOfAtLeastType(depth);
    }

    @Override
    public Optional<QueryAnnotation> annotate(Sighting sighting, SightingTaxon taxon) {
      if (queryType == QueryType.INCLUDED) {
        return Optional.absent();
      }
      
      if (liferSightings.get(taxon) == sighting) {
        return Optional.of(QueryAnnotation.LIFER);
      }
      
      return Optional.absent();
    }
    
    /** Only mark single taxa (not sp. or hybrid) as "lifers" */
    private boolean isPossibleLiferTaxon(SightingTaxon taxon) {
      // TODO: this is probably the wrong place to implement this...  I suspect
      // it should be handled only *after* mapping back to the report taxonomy
      return taxon != null
          && (taxon.getType() == SightingTaxon.Type.SINGLE
              || taxon.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES);
    }
  }
  
  @Override public boolean isNoOp() {
    return false;
  }
  
  @Nullable
  private Location getLocation() {
    String id = whereIndexer.getValue();
    if (id != null) {
      return locationSet.getLocation(id);
    }
    
    return null;
  }

  @Override
  public Optional<String> abbreviation() {
    return Optional.absent();
  }

  @Override
  public Optional<String> name() {
    return Optional.absent();
  }

  @Override
  public JsonElement persist() {
    Persisted persisted = new Persisted(
        (QueryType) shouldBeOptions.getSelectedItem(),
        whereIndexer.getValue());
    return GSON.toJsonTree(persisted);
  }

  @Override
  public void restore(JsonElement json) {
    Persisted persisted = GSON.fromJson(json, Persisted.class);
    shouldBeOptions.setSelectedItem(persisted.type);
    if (persisted.id == null) {
      whereIndexer.setValue(null);
    } else {
      if (locationSet.getLocation(persisted.id) == null
          && syntheticLocations.byId(persisted.id) == null) {
        throw new JsonSyntaxException("Unknown ID: " + persisted.id);
      }
      whereIndexer.setValue(persisted.id);
    }
  }
  
  static private final Gson GSON = new Gson();
  static class Persisted {
    public Persisted(QueryType type, String id) {
      this.type = type;
      this.id = id;
    }
    
    QueryType type;
    String id;
  }
}
