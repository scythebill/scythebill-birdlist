/*   
 * Copyright 2020 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.swing.AbstractAction;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import com.google.common.io.ByteSink;
import com.google.common.io.Files;
import com.scythebill.birdlist.model.export.INaturalistCsvExport;
import com.scythebill.birdlist.model.io.HtmlResponseWriter;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;

/**
 * Implements the save-to-iNaturalist action for reports.
 */
class INaturalistExportAction extends AbstractAction {
  private final QueryExecutor queryExecutor;
  private final FileDialogs fileDialogs;
  private final ReportSet reportSet;
  private final Taxonomy taxonomy;
  private final Alerts alerts;

  public INaturalistExportAction(
      FileDialogs fileDialogs,
      ReportSet reportSet,
      QueryExecutor queryExecutor,
      Taxonomy taxonomy,
      Alerts alerts) {
    this.fileDialogs = fileDialogs;
    this.reportSet = reportSet;
    this.queryExecutor = queryExecutor;
    this.taxonomy = taxonomy;
    this.alerts = alerts;
  }
  
  static private Frame getParentFrame(Object object) {
    Component c = (Component) object;
    while (c != null) {
      if (c instanceof Frame) {
        return (Frame) c;
      }

      c = c.getParent();
    }

    return null;
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    Frame parentFrame = getParentFrame(event.getSource());
    QueryResults queryResults = queryExecutor.executeQuery(Taxon.Type.group, taxonomy);
    if (Iterables.isEmpty(queryResults.getAllSightings())) {
      return;
    }
    
    Optional<String> reportName = queryExecutor.getReportAbbreviation();
    String fileName = reportName.isPresent()
        ? String.format("iNaturalist-export-%s.csv", reportName.get())
        : "Naturalist-export.csv";
    final File file = fileDialogs.saveFile(
        parentFrame,
        Messages.getMessage(Name.SAVE_FOR_INATURALIST),
        fileName,
        new FileNameExtensionFilter(Messages.getMessage(Name.CSV_FILES), "csv"), FileType.OTHER);
    if (file != null) {
      // Iterator, because we may need to split the output across multiple csvs
      Iterator<Resolved> taxonIterator = queryResults.getTaxaAsList().iterator();
      
      int loopCount = 0;
      File out = file;
      do {
        ByteSink outSupplier = Files.asByteSink(out);
        try {
          new INaturalistCsvExport().writeSpeciesList(outSupplier, taxonIterator,
              queryResults, reportSet);              
        } catch (IOException e) {
          FileDialogs.showFileSaveError(alerts, e, out);
          break;
        }

        if (taxonIterator.hasNext()) {
          try {
            out = getAuxiliaryFile(file, ++loopCount);
          } catch (IOException e) {
            alerts.showError(null,
                Name.SAVING_FAILED_TITLE,
                Name.FILE_WAS_TOO_LONG);
            break;
          }
        }

      } while (taxonIterator.hasNext());
    }
  }

  private File getAuxiliaryFile(File base, int loopCount) throws IOException {
    String name = base.getName();
    int periodIndex = name.lastIndexOf('.');
    if (periodIndex < 0) {
      name = name + "-" + loopCount;
    } else {
      name = name.substring(0, periodIndex) + "-" + loopCount + name.substring(periodIndex);
    }
    
    File auxiliary = new File(base.getParent(), name);
    if (!auxiliary.exists()) {
      if (!auxiliary.createNewFile()) {
        throw new IOException("Could not create file");
      }
    }
    
    return auxiliary;
  }
}
