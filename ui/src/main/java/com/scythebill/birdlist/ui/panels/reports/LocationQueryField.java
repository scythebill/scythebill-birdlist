/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.text.NumberFormat;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.annotation.Nullable;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonSyntaxException;
import com.scythebill.birdlist.model.query.SightingPredicates;
import com.scythebill.birdlist.model.query.SyntheticLocation;
import com.scythebill.birdlist.model.query.SyntheticLocations;
import com.scythebill.birdlist.model.sighting.Distance;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.util.Indexer;
import com.scythebill.birdlist.ui.components.AutoSelectJFormattedTextField;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.components.VisitInfoPanel.MilesOrKilometers;
import com.scythebill.birdlist.ui.components.VisitInfoPreferences;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;
import com.scythebill.birdlist.ui.util.LocationIdToString;

/**
 * QueryField for adjusting queries relative to a location.
 */
class LocationQueryField extends AbstractQueryField {
  private enum QueryType {
    IN(Name.LOCATION_IS_IN),
    NOT_IN(Name.LOCATION_IS_NOT_IN),
    IS(Name.LOCATION_IS),
    IS_NOT(Name.LOCATION_IS_NOT),
    IS_NEARER_THAN(Name.LOCATION_IS_NEARER_THAN),
    IS_NOT_SET(Name.LOCATION_IS_NOT_SET);
    
    private final Name text;
    QueryType(Name text) {
      this.text = text;
    }
    
    @Override
    public String toString() {
      return Messages.getMessage(text);
    }
  }
  private final LocationSet locationSet;
  private final JComboBox<QueryType> whereOptions = new JComboBox<>(QueryType.values());
  private final IndexerPanel<String> whereIndexer;
  private final SyntheticLocations syntheticLocations;
  private final JPanel distanceBox;
  private final AutoSelectJFormattedTextField distanceField;
  private final JComboBox<MilesOrKilometers> milesOrKilometers;
  private final JPanel valueBox;
  private final LocationIdToString idToString;
  private final Indexer<String> allLocationsIndexer;
  private final Indexer<String> locationsWithLatLongIndexer;

  public LocationQueryField(LocationSet locationSet, VisitInfoPreferences visitInfoPreferences) {
    super(QueryFieldType.LOCATION);
    this.locationSet = locationSet;
    this.syntheticLocations = new SyntheticLocations(locationSet);
    
    idToString = new LocationIdToString(locationSet, syntheticLocations);
    whereIndexer = new IndexerPanel<String>();
    whereIndexer.setPreviewText(Name.START_TYPING_A_LOCATION);
    allLocationsIndexer = idToString.createIndexer(false);
    locationsWithLatLongIndexer = new LocationIdToString(locationSet, null).createIndexer(false,
        l -> l.getLatLong().isPresent());
        
    distanceBox = new JPanel();
    distanceBox.setLayout(new BoxLayout(distanceBox, BoxLayout.LINE_AXIS));
    
    distanceField = new AutoSelectJFormattedTextField();
    distanceField.setColumns(6);
    NumberFormat numberFormat = NumberFormat.getNumberInstance();
    numberFormat.setMaximumIntegerDigits(5);
    numberFormat.setMaximumFractionDigits(2);
    NumberFormatter numberFormatter = new NumberFormatter(numberFormat);
    numberFormatter.setMinimum(0f);
    numberFormatter.setValueClass(Float.class);
    numberFormatter.setMaximum(99999.999f);
    numberFormatter.setCommitsOnValidEdit(true);
    distanceField.setFormatterFactory(new DefaultFormatterFactory(numberFormatter));
   
    milesOrKilometers = new JComboBox<>(MilesOrKilometers.values());
    milesOrKilometers.setSelectedItem(visitInfoPreferences.milesOrKilometers);
    distanceBox.add(distanceField);
    distanceBox.add(Box.createHorizontalStrut(5));
    distanceBox.add(milesOrKilometers);
    distanceBox.add(Box.createHorizontalStrut(5));
    JLabel fromLabel = new JLabel(Messages.getMessage(Name.LOCATION_FROM));
    fromLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    distanceBox.add(fromLabel);
    
    
    valueBox = new JPanel();
    valueBox.setLayout(new BoxLayout(valueBox, BoxLayout.PAGE_AXIS));
    valueBox.add(distanceBox);
    valueBox.add(whereIndexer);

    whereIndexer.addPropertyChangeListener("value", e -> firePredicateUpdated());
    whereOptions.addActionListener(e -> {
      updateLocationVisibility();
      resetIndexer();
      firePredicateUpdated();
    });
    milesOrKilometers.addActionListener(e -> {
      firePredicateUpdated();
      visitInfoPreferences.milesOrKilometers = (MilesOrKilometers) milesOrKilometers.getSelectedItem();
    });
    distanceField.addPropertyChangeListener("value", e -> firePredicateUpdated());    
    
    updateLocationVisibility();
    resetIndexer();
  }

  @Override
  public JComponent getComparisonChooser() {
    return whereOptions;
  }

  @Override
  public JComponent getValueField() {
    return valueBox;
  }

  @Override
  public boolean isSingleLine() {
    // This uses two lines (at least sometimes), so it gets a slightly different layout.
    return false;
  }
  
  /**
   * Return the location that will be the ancestor of all results
   * (and can therefore be omitted in display of query results).
   * TODO: generalize to other query types? 
   */
  public Location getRootLocation() {
    Location location = getLocation();
    if (location == null) {
      return null;
    }

    Object selectedItem = whereOptions.getSelectedItem();
    if (QueryType.IN.equals(selectedItem) || QueryType.IS.equals(selectedItem)) {
      return location;
    }
    
    return null;
  }
  
  @Override
  public Predicate<Sighting> predicate(Taxon.Type depth) {
    String id = whereIndexer.getValue();
    Object selectedItem = whereOptions.getSelectedItem();
    
    if (QueryType.IS_NOT_SET.equals(selectedItem)) {
      return sighting -> sighting.getLocationId() == null;
    } else if (id == null) {
      return Predicates.<Sighting>alwaysTrue();
    } else {
      Location location = locationSet.getLocation(id);
      SyntheticLocation synthetic = null;
      if (location == null) {
        synthetic = syntheticLocations.byId(id);
      }
      if (QueryType.IN.equals(selectedItem)) {
        if (synthetic != null) {
          return synthetic.isInPredicate();
        }
        return SightingPredicates.in(location, locationSet);
      } else if (QueryType.NOT_IN.equals(selectedItem)) {
        if (synthetic != null) {
          return Predicates.not(synthetic.isInPredicate());
        }
        return Predicates.not(SightingPredicates.in(location, locationSet));        
      } else if (QueryType.IS.equals(selectedItem)) {
        if (synthetic != null) {
          // TODO: disable "IS" in this case?
          return Predicates.alwaysFalse();
        }
        return SightingPredicates.is(location, locationSet);        
      } else if (QueryType.IS_NOT.equals(selectedItem)) {
        if (synthetic != null) {
          // TODO: disable "IS NOT" in this case?
          return Predicates.alwaysFalse();
        }
        return Predicates.not(SightingPredicates.is(location, locationSet));
      } else if (QueryType.IS_NEARER_THAN.equals(selectedItem)) {
        // No location (or a synthetic location) - can't do it
        if (location == null) {
          // TODO: disable "IS" in this case?
          return Predicates.alwaysFalse();
        }
        
        // Need a location with a lat-long
        if (!location.getLatLong().isPresent()) {
          return Predicates.alwaysFalse();
        }
        
        // Need a distance
        Object distanceValue = distanceField.getValue();
        if (distanceValue == null) {
          return Predicates.alwaysFalse();
        }
        
        float distanceInKm = (Float) distanceValue;
        if (milesOrKilometers.getSelectedItem() == MilesOrKilometers.MILES) {
          distanceInKm = Distance.inMiles(distanceInKm).kilometers();
        }
        
        // Zero?  Only this location will do.
        if (distanceInKm <= 0.0f) {
          return SightingPredicates.is(location, locationSet);
        }
        
        // Find all locations within N km
        Set<String> locationIds = new LinkedHashSet<>();
        for (Location root  : locationSet.rootLocations()) {
          addLocationsWithin(locationIds, root, distanceInKm, location.getLatLong().get());
        }

        // And then any sighting with a matching location counts
        return s -> locationIds.contains(s.getLocationId());
      } else {
        throw new IllegalStateException("Unknown location option: " + selectedItem);
      }
    }
  }

  private static void addLocationsWithin(Set<String> locationIds, Location location, float distanceInKm,
      LatLongCoordinates latLong) {
    if (location.getLatLong().isPresent()
        && latLong.kmDistance(location.getLatLong().get()) <= distanceInKm) {
      locationIds.add(location.getId());
    }

    for (Location child : location.contents()) {
      addLocationsWithin(locationIds, child, distanceInKm, latLong);
    }
  }

  @Override public boolean isNoOp() {
    return whereIndexer.getValue() == null;
  }

  @Nullable
  public Location getLocation() {
    String id = whereIndexer.getValue();
    if (id != null) {
      Location location = locationSet.getLocation(id);
      if (location == null) {
        location = syntheticLocations.byId(id);
      }
      
      return location;
    }
    
    return null;
  }

  @Override
  public Optional<String> abbreviation() {
    if (whereOptions.getSelectedItem() == QueryType.IS_NOT_SET) {
      return Optional.of(Messages.getMessage(Name.LOCATION_NOT_SET));
    }
    
    String id = whereIndexer.getValue();
    if (id != null) {
      Location location = locationSet.getLocation(id);
      if (location != null) {
        return Optional.of(location.getDisplayName());
      }
      SyntheticLocation syntheticLocation = syntheticLocations.byId(id);
      if (syntheticLocation != null) {
        return Optional.of(syntheticLocation.getDisplayName());
      }
    }
    
    return Optional.absent();
  }

  @Override
  public Optional<String> name() {
    Optional<String> name = abbreviation();
    return name.isPresent() ? name : Optional.of(Messages.getMessage(Name.WORLD_TEXT));
  }

  @Override
  public JsonElement persist() {
    Persisted persisted;
    if (whereOptions.getSelectedItem() == QueryType.IS_NEARER_THAN) {
      Object distanceValue = distanceField.getValue();
      persisted = new Persisted(
          (QueryType) whereOptions.getSelectedItem(),
          whereIndexer.getValue(),
          (MilesOrKilometers) milesOrKilometers.getSelectedItem(),
          distanceValue == null ? null : (Float) distanceValue);
      
    } else {
      persisted = new Persisted(
        (QueryType) whereOptions.getSelectedItem(),
        whereIndexer.getValue());
    }
    return GSON.toJsonTree(persisted);
  }

  @Override
  public void restore(JsonElement json) {
    Persisted persisted = GSON.fromJson(json, Persisted.class);
    whereOptions.setSelectedItem(persisted.type);
    if (persisted.id == null) {
      whereIndexer.setValue(null);
    } else {
      if (locationSet.getLocation(persisted.id) == null
          && syntheticLocations.byId(persisted.id) == null) {
        throw new JsonSyntaxException("Unknown ID: " + persisted.id);
      }
      whereIndexer.setValue(persisted.id);
    }
    
    if (persisted.distance != null) {
      distanceField.setValue(persisted.distance);
    }
    if (persisted.milesOrKilometers != null) {
      milesOrKilometers.setSelectedItem(persisted.milesOrKilometers);      
    }
  }
  
  private void updateLocationVisibility() {
    Object selectedValue = whereOptions.getSelectedItem();
    whereIndexer.setVisible(selectedValue != QueryType.IS_NOT_SET);
    distanceBox.setVisible(selectedValue == QueryType.IS_NEARER_THAN);
  }
  
  private void resetIndexer() {
    whereIndexer.removeAllIndexerGroups();
    if (whereOptions.getSelectedItem() == QueryType.IS_NEARER_THAN) {
      whereIndexer.addIndexerGroup(idToString, locationsWithLatLongIndexer);
    } else {
      whereIndexer.addIndexerGroup(idToString, allLocationsIndexer);
    }
  }
  
  
  static private final Gson GSON = new Gson();
  static class Persisted {
    public Persisted(QueryType type, String id) {
      this.type = type;
      this.id = id;
    }
    
    public Persisted(QueryType type, String id, MilesOrKilometers milesOrKilometers,
        Float distance) {
      this.type = type;
      this.id = id;
      this.milesOrKilometers = milesOrKilometers;
      this.distance = distance;
    }

    QueryType type;
    String id;
    MilesOrKilometers milesOrKilometers;
    Float distance;
  }
}
