/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.scythebill.birdlist.model.query.QueryDefinition;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * A query definition wrapper which converts a typical query definition
 * into a "never" query definition, which does most of its work
 * in the preprocess stage.  If multiple query definitions are "never"'d, then it
 * can be set up either as:
 * <ul>
 * <li>One NeverQueryDefinition with a list of N query definitions, in which case any one sighting
 *     must satisfy *all* of the query definitions in order to define an exclusion.
 * <li>N NeverQueryDefinitions each with one QueryDefinition, in which case any one sighting
 *     must satisfy *any* of the query definitions in order to define an exclusion.
 */
class NeverQueryDefinition implements QueryDefinition, QueryDefinition.Preprocessor {

  private final ImmutableList<QueryDefinition> queryDefinitions;
  private final Taxonomy taxonomy;
  private final Taxon.Type depth;
  private Set<SightingTaxon> excludedTaxa = new HashSet<>();

  public NeverQueryDefinition(QueryDefinition queryDefinition, Taxonomy taxonomy, Taxon.Type depth) {
    this(ImmutableList.of(queryDefinition), taxonomy, depth);
  }
  
  public NeverQueryDefinition(List<QueryDefinition> andedQueryDefinitions, Taxonomy taxonomy, Taxon.Type depth) {
    this.queryDefinitions = ImmutableList.copyOf(andedQueryDefinitions);
    this.taxonomy = taxonomy;
    this.depth = depth;
  }

  @Override
  public Optional<Preprocessor> preprocessor() {
    return Optional.of(this);
  }

  @Override
  public Predicate<Sighting> predicate() {
    return this::notInExcludedTaxa;
  }

  @Override
  public Optional<QueryAnnotation> annotate(Sighting sighting, SightingTaxon taxon) {
    for (QueryDefinition queryDefinition : queryDefinitions) {
      Optional<QueryAnnotation> annotation = queryDefinition.annotate(sighting, taxon);
      if (annotation.isPresent()) {
        return annotation;
      }
    }
    return Optional.absent();
  }

  @Override
  public void reset() {
    excludedTaxa.clear();
  }

  @Override
  public void preprocess(Sighting sighting, Predicate<Sighting> countablePredicate) {
    // Skip incompatible taxa 
    if (!TaxonUtils.areCompatible(taxonomy, sighting.getTaxonomy())) {
      return;
    }
    
    for (QueryDefinition queryDefinition : queryDefinitions) {
      if (!queryDefinition.predicate().apply(sighting)) {
        return;
      }
    }
    
    excludedTaxa.add(mapForDepth(sighting.getTaxon()));
  }

  private boolean notInExcludedTaxa(Sighting sighting) {
    // Skip incompatible taxa 
    if (!TaxonUtils.areCompatible(taxonomy, sighting.getTaxonomy())) {
      return false;
    }

    return !excludedTaxa.contains(mapForDepth(sighting.getTaxon()));
  }
  
  private SightingTaxon mapForDepth(SightingTaxon taxon) {
    if (depth == Taxon.Type.subspecies) {
      return taxon;
    }
    
    return taxon.resolve(taxonomy).getParentOfAtLeastType(depth);
  }
}
