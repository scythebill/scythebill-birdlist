/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.scythebill.birdlist.model.sighting.Photo;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;

/**
 * QueryField for adjusting queries of the photographed status.
 */
class PhotographedQueryField extends AbstractQueryField {
  private enum QueryType {
    HAS_PHOTO(Name.YES_TEXT),
    HAS_NO_PHOTO(Name.NO_TEXT),
    HAS_FAVORITE(Name.HAS_FAVORITE),
    HAS_NO_FAVORITE(Name.HAS_NO_FAVORITE);
    
    private final Name text;
    QueryType(Name text) {
      this.text = text;
    }
    
    @Override
    public String toString() {
      return Messages.getMessage(text);
    }
  }

  private final JComboBox<QueryType> queryOptions = new JComboBox<>(QueryType.values());
  private final JPanel emptyValueField;

  public PhotographedQueryField() {
    super(QueryFieldType.PHOTOGRAPHED);
    this.emptyValueField = new JPanel();
    queryOptions.addActionListener(e -> firePredicateUpdated());
  }

  @Override
  public JComponent getComparisonChooser() {
    return queryOptions;
  }

  @Override
  public JComponent getValueField() {
    return emptyValueField;
  }

  @Override
  public Predicate<Sighting> predicate(Taxon.Type depth) {
    QueryType type = (QueryType) queryOptions.getSelectedItem();
    switch (type) {
      case HAS_PHOTO:
      case HAS_NO_PHOTO:
        Predicate<Sighting> predicate = 
            sighting -> sighting.hasSightingInfo() && sighting.getSightingInfo().isPhotographed();
        if (type == QueryType.HAS_NO_PHOTO) {
          predicate = Predicates.not(predicate);
        }
        return predicate;
      case HAS_FAVORITE:
      case HAS_NO_FAVORITE:
        Predicate<Sighting> favoritePredicate = sighting -> sighting.hasSightingInfo()
            && sighting.getSightingInfo().getPhotos().stream().anyMatch(Photo::isFavorite);
        if (type == QueryType.HAS_NO_FAVORITE) {
          favoritePredicate = Predicates.not(favoritePredicate);
        }
        return favoritePredicate;
      default:
        throw new AssertionError("Unexpected type: " + type);
    }
  }

  @Override public boolean isNoOp() {
    return false;
  }

  @Override
  public JsonElement persist() {
    Persisted persisted = new Persisted(
        (QueryType) queryOptions.getSelectedItem());
    return GSON.toJsonTree(persisted);
  }

  @Override
  public void restore(JsonElement json) {
    // This field used to be a primitive, with values
    // 0 == yes (photographed)
    // 1 == no (not photographed)
    // 2 == any
    if (json.isJsonPrimitive()) {
      JsonPrimitive primitive = json.getAsJsonPrimitive();
      if (primitive.isNumber()) {
        int asInt = primitive.getAsInt();
        if (asInt == 0) {
          queryOptions.setSelectedItem(QueryType.HAS_PHOTO);
        } else if (asInt == 1) {
          queryOptions.setSelectedItem(QueryType.HAS_NO_PHOTO);
        }
      } else if (primitive.isBoolean()) {
        queryOptions.setSelectedItem(
            primitive.getAsBoolean() ? QueryType.HAS_PHOTO : QueryType.HAS_NO_PHOTO);            
      }
      // Other values not supported anymore
    } else {
      Persisted persisted = GSON.fromJson(json, Persisted.class);
      queryOptions.setSelectedItem(persisted.type);
    }
  }
  
  @Override
  public Optional<String> name() {
    QueryType type = (QueryType) queryOptions.getSelectedItem();
    // TODO: translate!
    switch (type) {
      case HAS_PHOTO:
        return Optional.of("photographed");
      case HAS_NO_PHOTO:
        return Optional.of("not photographed");
      case HAS_FAVORITE:
        return Optional.of("has favorite");
      case HAS_NO_FAVORITE:
        return Optional.of("no favorite");
      default:
        throw new AssertionError("Unexpected type: " + type);
    }
  }

  static private final Gson GSON = new Gson();
  static class Persisted {
    public Persisted(QueryType type) {
      this.type = type;
    }
    
    QueryType type;
  }
}
