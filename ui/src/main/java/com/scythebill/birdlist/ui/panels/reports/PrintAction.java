/*   
 * Copyright 2012 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.TimeZone;

import javax.swing.AbstractAction;

import org.joda.time.DateTimeZone;
import org.joda.time.Instant;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.io.HtmlOutput;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.DesktopUtils;

/**
 * Implements the print action for reports.
 */
class PrintAction extends AbstractAction {
  private final static DateTimeFormatter DATE_FORMAT = 
      DateTimeFormat.shortDate().withZone(DateTimeZone.forTimeZone(TimeZone.getDefault()));
  private final static String PRINT_SCRIPT = "window.print();";
  private final static String TEST_FOR_SAFARI = Joiner.on('\n').join(
      "var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&",
      "    navigator.userAgent && !navigator.userAgent.match('CriOS');",
      "if (isSafari) {",
      "  alert('Compact printing is not supported in Safari;  please load in Chrome or Firefox');",
      "} else {",
      PRINT_SCRIPT,
      "}");
      


  private final ReportSet reportSet;
  private final TaxonomyStore taxonomyStore;
  private final QueryExecutor queryExecutor;
  private final String fileName;
  private final Alerts alerts;
  private final ReportPrintDialog reportPrintDialog;

  public PrintAction(
      String fileName,
      ReportSet reportSet,
      TaxonomyStore taxonomyStore,
      QueryExecutor queryExecutor,
      Alerts alerts,
      ReportPrintDialog reportPrintDialog) {
    this.fileName = fileName;
    this.reportSet = reportSet;
    this.taxonomyStore = taxonomyStore;
    this.queryExecutor = queryExecutor;
    this.alerts = alerts;
    this.reportPrintDialog = reportPrintDialog;
  }

  @Override public void actionPerformed(ActionEvent event) {
    ReportPrintPreferences prefs = reportPrintDialog.getConfiguration((Component) event.getSource());
    if (prefs == null) {
      return;
    }
    
    File temp;
    try {
      temp = File.createTempFile(fileName, ".html");
    } catch (IOException e) {
      alerts.showError(null, Name.COULD_NOT_PRINT,
          Name.COULD_NOT_CREATE_TEMPORARY_FILE_TO_PRINT,
          e);
      return;
    }
     
    QueryExecutor executor = queryExecutor;
    if (prefs.onlyCountable) {
      executor = executor.onlyCountable();
    }
    QueryResults queryResults = executor.executeQuery(null, null);
    
    Taxonomy taxonomy = taxonomyStore.getTaxonomy();
    HtmlOutput htmlOutput = new HtmlOutput(
        taxonomy, queryExecutor.getRootLocation(), alerts);
    Set<ReportHints> reportHints = queryExecutor.getReportHints();
    if (prefs.compactPrinting) {
      htmlOutput.setScript(TEST_FOR_SAFARI);
    } else {
      htmlOutput.setScript(PRINT_SCRIPT);
    }
    if (prefs.omitSpAndHybrid) {
      htmlOutput.setOmitSpAndHybrid(prefs.omitSpAndHybrid);
    }
    Optional<String> reportName = queryExecutor.getReportAbbreviation();
    if (reportName.isPresent()) {
      htmlOutput.setTitle(String.format("%s - %s - %s", reportName.get(), fileName, DATE_FORMAT.print(new Instant())));
    } else {
      htmlOutput.setTitle(String.format("%s - %s", fileName, DATE_FORMAT.print(new Instant())));
    }
    htmlOutput.setShowStatus(prefs.showStatus);
    htmlOutput.setCompactOutput(prefs.compactPrinting);
    htmlOutput.setScientificOrCommon(
        reportPrintDialog.getScientificOrCommonFromConfiguration(prefs));
    htmlOutput.setSightingsCount(prefs.compactPrinting ? 0 : prefs.maximumSightings);
    
    if (prefs.sortType != SortType.DEFAULT) {
      htmlOutput.setSortAllSightings(true);
      htmlOutput.setShowFamilyTotals(false);
      htmlOutput.setShowFamilies(false);
    } else {
      boolean showFamilyTotals;
      if (reportHints.contains(ReportHints.SPECIES_STATUS_RESTRICTION)) {
        // Species-status restrictions: family totals are meaningless
        showFamilyTotals = false;
      } else if (reportHints.contains(ReportHints.LOCATION_RESTRICTION)
          && queryResults.getChecklist(taxonomy) == null) {
        // Location restriction *without* a checklist, ditto - we'd be reporting world list totals
        showFamilyTotals = false;
      } else {
        showFamilyTotals = true;
      }
      htmlOutput.setShowFamilyTotals(showFamilyTotals);
      htmlOutput.setShowFamilies(prefs.showFamilies);
    }

    try {
      htmlOutput.writeSpeciesList(
          temp, queryResults,
          prefs.sortType.ordering(),
          prefs.sortSightingsType.ordering(),
          reportSet.getLocations());
      
      DesktopUtils.openHtmlFileInBrowser(temp);
    } catch (IOException e) {
      alerts.showError(null,
          Messages.getMessage(Name.COULD_NOT_PRINT),
          Messages.getFormattedMessage(Name.COULD_NOT_CREATE_TEMPORARY_FILE_TO_PRINT_IN,
              temp.getParentFile().getAbsolutePath()),
          e);
    }
  }

}
