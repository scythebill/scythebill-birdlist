/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JDialog;
import javax.swing.JLabel;

import com.google.common.base.Optional;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.query.QueryDefinition;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.components.OkCancelPanel;
import com.scythebill.birdlist.ui.events.DefaultUserStore;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Displays a dialog allowing entry of reports for other parts of the UI.
 */
public class QueryDialog {
  private final QueryFieldFactory queryFieldFactory;
  private final StoredQueries storedQueries;
  private final FontManager fontManager;
  private EventBusRegistrar eventBusRegistrar;
  private DefaultUserStore defaultUserStore;

  public interface QueryDefinitionReceiver {
    void queryDefinitionAvailable(
        QueryDefinition queryDefinition, Optional<String> optional);  
    void cancelled();  
  }
  
  @Inject
  public QueryDialog(
      QueryFieldFactory queryFieldFactory,
      StoredQueries storedQueries,
      EventBusRegistrar eventBusRegistrar,
      DefaultUserStore defaultUserStore,
      FontManager fontManager) {
    this.queryFieldFactory = queryFieldFactory;
    this.storedQueries = storedQueries;
    this.eventBusRegistrar = eventBusRegistrar;
    this.defaultUserStore = defaultUserStore;
    this.fontManager = fontManager;
  }
  
  public void showQueryDefinition(
      Window parent,
      String message,
      ReportSet reportSet,
      Taxonomy taxonomy,
      final QueryDefinitionReceiver receiver) {
    ModalityType modality =
        Toolkit.getDefaultToolkit().isModalityTypeSupported(ModalityType.DOCUMENT_MODAL)
        ? ModalityType.DOCUMENT_MODAL : ModalityType.APPLICATION_MODAL;
    final JDialog dialog = new JDialog(parent, Messages.getMessage(Name.CHOOSE_A_REPORT), modality);
    final QueryPanel queryPanel = new QueryPanel(
        reportSet,
        queryFieldFactory, storedQueries, eventBusRegistrar, defaultUserStore, fontManager,
        // HACK. Exclude first-sightings, which requires preprocessing, which callers of this don't want.
        QueryFieldType.FIRST_SIGHTINGS,
        // And HACK, exclude FAMILY, which requires updating the query when the taxonomy changes, which
        // callers of this won't necessarily be able to do
        QueryFieldType.FAMILY,
        // And TIMES_SIGHTED, which requires postprocessing
        QueryFieldType.TIMES_SIGHTED) {
      @Override
      protected void maybeResizeWindow() {
        Dimension preferredSize = dialog.getPreferredSize();
        Dimension currentSize = dialog.getSize();
        Dimension resizeTo = new Dimension(
            Math.max(preferredSize.width, currentSize.width),
            Math.max(preferredSize.height, currentSize.height));
        dialog.setSize(resizeTo);
        UIUtils.keepWindowOnScreen(dialog);
      }
    };
    
    Action cancelAction = new AbstractAction() {
      @Override public void actionPerformed(ActionEvent e) {
        receiver.cancelled();
        dialog.dispose();
      }
    };
    final Action okAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent event) {
        receiver.queryDefinitionAvailable(
            queryPanel.queryDefinition(taxonomy, Taxon.Type.species),
            queryPanel.getQueryName());
        dialog.dispose();
      }
    };
    
    Box box = Box.createVerticalBox();
    box.add(new JLabel(message));
    box.add(Box.createVerticalStrut(fontManager.scale(20)));
    box.add(queryPanel);
    box.add(Box.createVerticalStrut(fontManager.scale(20)));
    OkCancelPanel okCancel = new OkCancelPanel(
        okAction, cancelAction, box, queryPanel);
    dialog.setContentPane(okCancel);
    dialog.pack();
    dialog.setVisible(true);    
  }
}
