/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.util.Set;

import javax.annotation.Nullable;

import com.google.common.base.Optional;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.Taxon.Type;

/** Query execution interface, to be passed to export/save actions. */
public interface QueryExecutor {
  QueryResults executeQuery(
      @Nullable Type depthOverride, @Nullable Taxonomy taxonomyOverride);
  Set<ReportHints> getReportHints();
  Optional<String> getReportAbbreviation();
  Optional<String> getReportName();
  Location getRootLocation();
  QueryExecutor onlyCountable();
  QueryExecutor includeIncompatibleSightings();
}