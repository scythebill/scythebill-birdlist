/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import static com.google.common.base.Optional.absent;

import java.awt.event.ActionListener;

import javax.swing.JComponent;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.gson.JsonElement;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.query.QueryDefinition;
import com.scythebill.birdlist.model.query.SightingPredicates;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.ui.components.VisitInfoPreferences;
import com.scythebill.birdlist.ui.events.TaxonomyStore;

/**
 * Factory for QueryField instances.
 */
class QueryFieldFactory {
  private final ReportSet reportSet;
  private final TaxonomyStore taxonomyStore;
  private final VisitInfoPreferences visitInfoPreferences;
  
  enum QueryFieldType {
     DATE {
      @Override
      QueryField newField(QueryFieldFactory factory) {
        return new DateQueryField();
      }
    },
    LOCATION {
      @Override
      QueryField newField(QueryFieldFactory factory) {
        return new LocationQueryField(factory.reportSet.getLocations(), factory.visitInfoPreferences);
      }
    },
    /** QueryField for "heard-only" */
    ALLOW_HEARD_ONLY {
      @Override
      QueryField newField(QueryFieldFactory factory) {
        return new BooleanQueryField(ALLOW_HEARD_ONLY, false) {
          @Override protected Predicate<Sighting> predicateFromBoolean(
              final boolean currentValue) {
            return currentValue
                ? SightingPredicates.isHeardOnly()
                : Predicates.not(SightingPredicates.isHeardOnly());
          }
        };
      }
    },
    /** QueryField for "photographed" */
    PHOTOGRAPHED {
      @Override
      QueryField newField(QueryFieldFactory factory) {
        return new PhotographedQueryField();
      }
    },
    /** QueryField for population status */
    SIGHTING_STATUS {
      @Override
      QueryField newField(QueryFieldFactory factory) {
        return new SightingStatusQueryField();
      }
    },
    SPECIES_STATUS {
      @Override
      QueryField newField(QueryFieldFactory factory) {
        return new SpeciesStatusQueryField(factory.taxonomyStore);
      }
    },
    SIGHTING_NOTES {
      @Override
      QueryField newField(QueryFieldFactory factory) {
        return new DescriptionQueryField();
      }
    },
    VISIT_COMMENTS {
      @Override
      QueryField newField(QueryFieldFactory factory) {
        return new VisitCommentsQueryField();
      }
    },
    BREEDING_CODE {
      @Override
      QueryField newField(QueryFieldFactory factory) {
        return new BreedingBirdCodeQueryField();
      }
    },
    SP_OR_HYBRID {
      @Override
      QueryField newField(QueryFieldFactory factory) {
        return new SpOrHybridStatusQueryField(factory.taxonomyStore);
      }
    },
    SEX_AGE {
      @Override
      QueryField newField(QueryFieldFactory factory) {
        return new SexAgeQueryField();
      }
    },
    SUBSPECIES_ALLOCATED {
      @Override
      QueryField newField(QueryFieldFactory factory) {
        return new SubspeciesAllocatedQueryField(factory.taxonomyStore);
      }
    },
    FIRST_SIGHTINGS {
      @Override
      QueryField newField(QueryFieldFactory factory) {
        return new FirstSightingsQueryField(factory.reportSet.getLocations(), factory.taxonomyStore);
      }
    },
    TIMES_SIGHTED {
      @Override
      QueryField newField(QueryFieldFactory factory) {
        return new TimesSightedQueryField();
      }
      
      @Override
      boolean onlyAllowsAll() {
        return true;
      }
    },
    FAMILY {
      @Override
      QueryField newField(QueryFieldFactory factory) {
        return new FamilyQueryField(factory.taxonomyStore);
      }
    },
    USER {
      @Override
      QueryField newField(QueryFieldFactory factory) {
        return new UserQueryField(factory.reportSet.getUserSet());
      }
    };
 
    
    abstract QueryField newField(QueryFieldFactory factory);

    /** If true, this query type only supports "and". */
    boolean onlyAllowsAll() {
      return false;
    }
  }
  
  /** Implementation of a single query field. */
  public interface QueryField {
    /** Get the component for choosing a comparison type (or null if one isn't needed) */
    JComponent getComparisonChooser();
    
    /** Get the list of components (one or more) for entering values */
    JComponent getValueField();
    
    /** Add a listener that receives notification that the query has changed. */
    void addPredicateUpdatedListener(ActionListener e);
    
    /** Return the current predicate for the sighting. */
    QueryDefinition queryDefinition(ReportSet reportSet, Taxon.Type depth);

    QueryFieldType getType();
   
    /** Return true if the filter is a no-op. */
    boolean isNoOp();

    /** Return the boolean value (or absent if it's not boolean or is not set. */
    default Optional<Boolean> getBooleanValue() {
      return absent();
    }

    /** Return true if the field uses a single-line. */
    default boolean isSingleLine() {
      return true;
    }
    
    /** Return an abbreviation suitable for use in file names, or absent(). */
    default Optional<String> abbreviation() {
      return absent();
    }
    
    /** Return an abbreviation suitable for use in a report name, or absent(). */
    default Optional<String> name() {
      return absent();
    }
    
    /** Persist the query field as a JSON element.*/
    JsonElement persist();
    
    /** Restore from persisted state. */
    void restore(JsonElement persisted);
    
    /** Handle taxonomy changes. */
    default void taxonomyUpdated() {};
  }
  
  @Inject
  QueryFieldFactory(ReportSet reportSet, TaxonomyStore taxonomyStore, VisitInfoPreferences visitInfoPreferences) {
    this.reportSet = reportSet;
    this.taxonomyStore = taxonomyStore;
    this.visitInfoPreferences = visitInfoPreferences;
  }
  
  public QueryField newField(QueryFieldType type) {
    return type.newField(this);
  }  
}
