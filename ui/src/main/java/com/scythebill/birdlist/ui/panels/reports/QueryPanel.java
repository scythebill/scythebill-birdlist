/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.Component;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Nullable;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.TitledBorder;

import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.eventbus.Subscribe;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.events.DefaultUserChangedEvent;
import com.scythebill.birdlist.ui.events.DefaultUserStore;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.CompositeQueryDefinition.BooleanOperation;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryField;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;
import com.scythebill.birdlist.ui.panels.reports.StoredQueries.RestoredQuery;

/**
 * Panel for constructing sighting queries.
 * 
 * TODO: support queries of the form "never", e.g., "has been seen in the ABA area, but
 * never in the US" 
 */
public class QueryPanel extends JPanel {
  /**
   * ImmutableMap from field type to title.  Since ImmutableMap is order-preserving,
   * we can guarantee that the order added here will match the order in the UI.
   */
  private static final ImmutableMap<QueryFieldType, Name> FIELD_TYPE_TO_TITLE =
      ImmutableMap.<QueryFieldType, Name>builder()
      .put(QueryFieldType.LOCATION, Name.QUERY_LOCATION)
      .put(QueryFieldType.DATE, Name.QUERY_DATE)
      .put(QueryFieldType.USER, Name.QUERY_OBSERVERS)
      .put(QueryFieldType.FIRST_SIGHTINGS, Name.QUERY_FIRST_RECORDS)
      .put(QueryFieldType.ALLOW_HEARD_ONLY, Name.QUERY_HEARD_ONLY)
      .put(QueryFieldType.SPECIES_STATUS, Name.QUERY_IUCN_READLIST)
      .put(QueryFieldType.SIGHTING_STATUS, Name.QUERY_STATUS)
      .put(QueryFieldType.BREEDING_CODE, Name.QUERY_BREEDING_CODE)
      .put(QueryFieldType.PHOTOGRAPHED, Name.QUERY_PHOTOGRAPHED)
      .put(QueryFieldType.SIGHTING_NOTES, Name.QUERY_SIGHTING_NOTES)
      .put(QueryFieldType.VISIT_COMMENTS, Name.QUERY_VISIT_COMMENTS)
      .put(QueryFieldType.SEX_AGE, Name.QUERY_SEX_AGE)
      .put(QueryFieldType.TIMES_SIGHTED, Name.QUERY_TIMES_SIGHTED)
      .put(QueryFieldType.FAMILY, Name.QUERY_FAMILY)
      .put(QueryFieldType.SUBSPECIES_ALLOCATED, Name.QUERY_SUBSPECIES)
      .put(QueryFieldType.SP_OR_HYBRID, Name.QUERY_SP_OR_HYBRID)
      .build();

  private final QueryFieldFactory queryFieldFactory;
  private final List<QueryEntry> queryFields = Lists.newArrayList();
  private final List<ActionListener> predicateChangedListeners = Lists.newArrayList();
  private final StoredQueries storedQueries;
  private final DefaultUserStore defaultUserStore;
  private GroupLayout layout;
  private final FontManager fontManager;
  private JPanel queryFieldsPanel;
  private final ImmutableSet<QueryFieldType> excludedQueryFieldTypes;
  private final ReportSet reportSet;

  
  QueryPanel(
      ReportSet reportSet,
      QueryFieldFactory queryFieldFactory,
      StoredQueries storedQueries,
      EventBusRegistrar eventBusRegistrar,
      DefaultUserStore defaultUserStore,
      FontManager fontManager,
      QueryFieldType... excludedQueryFieldTypes) {
    this.reportSet = reportSet;
    this.queryFieldFactory = queryFieldFactory;
    this.storedQueries = storedQueries;
    this.defaultUserStore = defaultUserStore;
    this.fontManager = fontManager;
    ImmutableSet.Builder<QueryFieldType> excludedQueryFieldTypesBuilder = ImmutableSet.builder();
    excludedQueryFieldTypesBuilder.addAll(Arrays.asList(excludedQueryFieldTypes));
    if (reportSet.getUserSet() == null) {
      // Omit USER queries unless users have been set up
      excludedQueryFieldTypesBuilder.add(QueryFieldType.USER);
    }
    this.excludedQueryFieldTypes = excludedQueryFieldTypesBuilder.build();
    initUi();
    addNewQueryField(null);
    
    eventBusRegistrar.registerWhenInHierarchy(this);
  }
  
  /**
   * Return a single query definition.
   */
  public CompositeQueryDefinition queryDefinition(Taxonomy taxonomy, Taxon.Type depth) {
    ArrayList<QueryField> fields = queryFields.stream()
        .map(entry -> entry.field)
        .collect(Collectors.toCollection(ArrayList::new));
    ArrayList<BooleanOperation> operations =
        queryFields.stream()
        .map(entry -> (BooleanOperation) entry.booleanOperation.getSelectedItem())
        .collect(Collectors.toCollection(ArrayList::new));
    
    // Add the default user into the query if present, and the query does not explicitly list
    // a user.
    if (defaultUserStore.getUser() != null && !containsQueryFieldType(QueryFieldType.USER)) {
      UserQueryField userField = new UserQueryField(reportSet.getUserSet());
      userField.setUser(defaultUserStore.getUser());
      fields.add(userField);
      operations.add(BooleanOperation.ALL);
    }
    
    return new CompositeQueryDefinition(
        reportSet,
        fields,
        operations,
        taxonomy,
        depth);
  }

  /**
   * Returns the set of queried-for locations.  This set may contain null if
   * synthetic locations are being queried for.
   */
  public Set<Location> getLocations() {
    Set<Location> locations = Sets.newHashSet();
    for (QueryEntry entry : queryFields) {
      if (entry.field instanceof LocationQueryField) {
        locations.add(((LocationQueryField) entry.field).getLocation());
      }
    }
    return locations;
  }
  
  /** Add a listener for notification that the query predicate has changed. */
  public void addPredicateChangedListener(ActionListener l) {
    predicateChangedListeners.add(l);
  }
  
  public String persist() {
    List<QueryField> fields = Lists.newArrayList();
    List<BooleanOperation> operations = Lists.newArrayList();
    for (QueryEntry entry : queryFields) {
      fields.add(entry.field);
      operations.add((BooleanOperation) entry.booleanOperation.getSelectedItem());
    }
    
    return storedQueries.persist(
        fields,
        operations);
  }
  
  public void restore(String json) {
    Optional<RestoredQuery> restored = storedQueries.restore(json);
    if (restored.isPresent()) {
      restore(restored.get());
    }
  }
    
  public void restore(RestoredQuery restored) {
    queryFields.clear();
    queryFieldsPanel.removeAll();

    for (int i = 0; i < restored.fields.size(); i++) {
      QueryField field = restored.fields.get(i);
      BooleanOperation operation = restored.booleanOperations.get(i);
      addQueryField(field, queryFields.size(), operation);
    }

    rebuildLayout();
    firePredicateChanged();    
  }
  
  /** Returns true if at least one query field is of the given type. */
  public boolean containsQueryFieldType(QueryFieldType type) {
    for (QueryEntry entry : queryFields) {
      if (type == entry.field.getType()) {
        return true;
      }
    }
    
    return false;
  }

  /** Returns true if at least one query field is of the given type and isn't a no-op. */
  public boolean containsQueryFieldTypeThatIsNotANoOp(QueryFieldType type) {
    for (QueryEntry entry : queryFields) {
      if (type == entry.field.getType()) {
        if (!entry.field.isNoOp()) {
          return true;
        }
      }
    }
    
    return false;
  }

  /** Returns true if at least one query field is of the given type. */
  @Nullable
  public Optional<Boolean> getBooleanValueIfPresent(QueryFieldType type) {
    for (QueryEntry entry : queryFields) {
      if (type == entry.field.getType()) {
        Optional<Boolean> booleanValue = entry.field.getBooleanValue();
        // Flip the value of "never"s
        if (booleanValue.isPresent()
            && (entry.booleanOperation.getSelectedItem() == BooleanOperation.NEVER
                || entry.booleanOperation.getSelectedItem() == BooleanOperation.NEVER_ALL)) {
          booleanValue = Optional.of(!booleanValue.get());
        }
       }
    }
    
    return null;
  }

  /**
   * Returns the location that will be the ancestor of all results
   * (and can therefore be omitted in display of query results).
   * TODO: generalize this very, very ugly method to other query types? 
   */
  public Location getRootLocation() {
    boolean foundLocationField = false;
    Location rootLocation = null;
    
    for (QueryEntry entry : queryFields) {
      if (QueryFieldType.LOCATION == entry.field.getType()) {
        if (foundLocationField) {
          return null;
        }
        
        foundLocationField = true;
        rootLocation = ((LocationQueryField) entry.field).getRootLocation();
      }
    }
    
    return rootLocation;
  }
  
  private void firePredicateChanged() {
    ActionEvent event = new ActionEvent(this, 0, null);
    for (ActionListener l : predicateChangedListeners) {
      l.actionPerformed(event);
    }
  }

  private QueryEntry addQueryField(QueryField newField, int index, BooleanOperation booleanOperation) {
    final QueryEntry entry = new QueryEntry();
    entry.typeChooser = createQueryFieldChooser(newField.getType());
    entry.remove = newRemoveButton();
    entry.add = newAddButton();
    entry.field = newField;

    entry.booleanOperation = new JComboBox<>(BooleanOperation.values());
    entry.booleanOperation.setSelectedItem(booleanOperation);
    
    queryFields.add(index, entry);
    
    // TODO: focus order is broken unless we either add components in the
    // proper order, or provide a custom focus traversal policy.  The former
    // is simpler.
    queryFieldsPanel.add(entry.booleanOperation);
    queryFieldsPanel.add(entry.typeChooser);
    queryFieldsPanel.add(newField.getComparisonChooser());
    queryFieldsPanel.add(newField.getValueField());
    queryFieldsPanel.add(entry.remove);
    queryFieldsPanel.add(entry.add);
    
    entry.booleanOperation.addActionListener(e -> firePredicateChanged());
    entry.field.addPredicateUpdatedListener(e -> firePredicateChanged());
    
    entry.typeChooser.addActionListener(e -> updateQueryField(entry));
    entry.add.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent e) {
        QueryEntry newQueryField = addNewQueryField(entry);
        JComponent valueField = newQueryField.field.getValueField();
        // Move focus to the value field - unless it's a composite, in which
        // case focus on the first focusable component
        if (valueField.isFocusable()) {
          valueField.requestFocusInWindow();
        } else {
          Component focusableComponent = valueField.getFocusTraversalPolicy()
               .getFirstComponent(valueField);
          if (focusableComponent != null) {
            focusableComponent.requestFocusInWindow();
          }
        }
        maybeResizeWindow();
      }
    });
    entry.remove.addActionListener(e -> removeQueryField(entry));
    
    return entry;
  }

  protected void maybeResizeWindow() {
  }

  private void removeQueryField(QueryEntry entry) {
    // Remove the components (in inverse order, to simplify focus changes)
    queryFieldsPanel.remove(entry.remove);
    queryFieldsPanel.remove(entry.add);
    queryFieldsPanel.remove(entry.field.getValueField());
    queryFieldsPanel.remove(entry.field.getComparisonChooser());
    queryFieldsPanel.remove(entry.typeChooser);
    queryFieldsPanel.remove(entry.booleanOperation);
    
    queryFields.remove(entry);
    
    rebuildLayout();
    firePredicateChanged();
  }

  private QueryEntry addNewQueryField(QueryEntry after) {
    BooleanOperation booleanOperation =  after == null
        ? BooleanOperation.ALL : (BooleanOperation) after.booleanOperation.getSelectedItem();

    int indexToAdd = queryFields.size();
    EnumSet<QueryFieldType> unusedFields = EnumSet.allOf(QueryFieldType.class);
    
    int loopIndex = 0;
    // Find all field types currently used, and the index where the new field
    // should be added
    for (QueryEntry entry : queryFields) {
      unusedFields.remove(entry.field.getType());
      loopIndex++;
      if (entry == after) {
        indexToAdd = loopIndex;
      }
    }
    
    // Use DATE if all fields are taken;  but otherwise choose
    // the first field that hasn't been
    QueryFieldType newType = QueryFieldType.DATE;
    for (QueryFieldType type : allowedQueryFieldTypes()) {
      if (unusedFields.contains(type)) {
        newType = type;
        break;
      }
    }
    
    QueryEntry newEntry = addQueryField(queryFieldFactory.newField(newType), indexToAdd, booleanOperation);

    rebuildLayout();
    firePredicateChanged();
    

    return newEntry;
  }

  private void updateQueryField(QueryEntry entry) {
    ImmutableList<QueryFieldType> fieldTypesList = ImmutableList.copyOf(allowedQueryFieldTypes());
    QueryFieldType newType = fieldTypesList.get(entry.typeChooser.getSelectedIndex());
    if (newType == entry.field.getType()) {
      return;
    }
    
    QueryField newField = queryFieldFactory.newField(newType);
    replaceComponent(entry.field.getComparisonChooser(), newField.getComparisonChooser());
    replaceComponent(entry.field.getValueField(), newField.getValueField());
    
    entry.field = newField;    
    entry.field.addPredicateUpdatedListener(e -> firePredicateChanged());
    if (newType.onlyAllowsAll()) {
      entry.booleanOperation.setSelectedItem(BooleanOperation.ALL);
      entry.booleanOperation.setEnabled(false);
    } else {
      entry.booleanOperation.setEnabled(true);
    }
    firePredicateChanged();
    rebuildLayout();
  }

  private Iterable<QueryFieldType> allowedQueryFieldTypes() {
    return Iterables.filter(
        FIELD_TYPE_TO_TITLE.keySet(), Predicates.not(Predicates.in(excludedQueryFieldTypes)));
  }
  private void replaceComponent(JComponent old, JComponent replacement) {
    for (int i = 0; i < queryFieldsPanel.getComponentCount(); i++) {
      if (queryFieldsPanel.getComponent(i) == old) {
        queryFieldsPanel.remove(i);
        queryFieldsPanel.add(replacement, i);
        return;
      }
    }
  }

  private JButton newAddButton() {
    JButton button = new JButton("+");
    button.setMargin(new Insets(0, 0, 0, 0));
    return button;
  }
  
  private JButton newRemoveButton() {
    JButton button = new JButton("-");
    button.setMargin(new Insets(0, 0, 0, 0));
    return button;
  }
  
  private void rebuildLayout() {
    SequentialGroup vertical = layout.createSequentialGroup();
    if (!queryFields.isEmpty()) {
      // Disable removing the first element if it's the only one
      queryFields.get(0).remove.setEnabled(queryFields.size() > 1);
      // And hide And/Or/Never as well
      queryFields.get(0).booleanOperation.setVisible(queryFields.size() > 1);
      // ... but don't let it stay on "never"  
      if (queryFields.size() == 1) {
        queryFields.get(0).booleanOperation.setSelectedIndex(0);
      }
    }
    
    for (QueryEntry entry : queryFields) {
      boolean isSingleLine = entry.field.isSingleLine();
      vertical.addGroup((isSingleLine
              ? layout.createBaselineGroup(false, false)
              : layout.createParallelGroup())
          .addComponent(entry.booleanOperation,
              PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
          .addComponent(entry.typeChooser,
              PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
          .addComponent(entry.field.getComparisonChooser(),
              PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
          .addComponent(entry.field.getValueField(),
              PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
          .addComponent(entry.remove,
              PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
          .addComponent(entry.add,
              PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE));
      vertical.addPreferredGap(ComponentPlacement.RELATED);
    }
    layout.setVerticalGroup(vertical);
    
    SequentialGroup horizontal = layout.createSequentialGroup();
    ParallelGroup allOrAnyChoosers = layout.createParallelGroup();
    for (QueryEntry entry : queryFields) {
      allOrAnyChoosers.addComponent(entry.booleanOperation,
          PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE);
    }

    ParallelGroup typeChoosers = layout.createParallelGroup();
    for (QueryEntry entry : queryFields) {
      typeChoosers.addComponent(entry.typeChooser,
          PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE);
    }

    ParallelGroup comparisonChoosers = layout.createParallelGroup();
    for (QueryEntry entry : queryFields) {
      comparisonChoosers.addComponent(entry.field.getComparisonChooser(),
          PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE);
    }

    ParallelGroup valueFields = layout.createParallelGroup();
    for (QueryEntry entry : queryFields) {
      valueFields.addComponent(entry.field.getValueField(),
          PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE);
    }

    ParallelGroup adds = layout.createParallelGroup();
    for (QueryEntry entry : queryFields) {
      adds.addComponent(entry.add,
          PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE);
    }

    ParallelGroup removes = layout.createParallelGroup();
    for (QueryEntry entry : queryFields) {
      removes.addComponent(entry.remove,
          PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE);
      layout.linkSize(entry.add, entry.remove);
    }

    horizontal
          .addGroup(allOrAnyChoosers)
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addGroup(typeChoosers)
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addGroup(comparisonChoosers)
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addGroup(valueFields)
          .addPreferredGap(ComponentPlacement.UNRELATED)
          .addGroup(removes)
          .addPreferredGap(ComponentPlacement.RELATED)
          .addGroup(adds);
                
    layout.setHorizontalGroup(horizontal);
    
    fontManager.applyTo(this);
  }

  private JComboBox<String> createQueryFieldChooser(QueryFieldType type) {
    ImmutableList<QueryFieldType> fieldTypesList = ImmutableList.copyOf(allowedQueryFieldTypes());
    List<String> titlesList = Lists.transform(
        fieldTypesList,
        t -> Messages.getMessage(FIELD_TYPE_TO_TITLE.get(t)));
    String[] titles = titlesList.toArray(new String[0]);
    JComboBox<String> chooser = new JComboBox<>(titles);
    chooser.setSelectedIndex(fieldTypesList.indexOf(type));
    chooser.setMaximumRowCount(fieldTypesList.size());
    return chooser;
  }

  private void initUi() {
    // Box layout
    setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
    
    // Panel that the main query fields go in
    queryFieldsPanel = new JPanel();
    queryFieldsPanel.setBorder(new TitledBorder((String) null));
    layout = new GroupLayout(queryFieldsPanel);
    queryFieldsPanel.setLayout(layout);
    
    queryFieldsPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
    add(queryFieldsPanel);
  }
  
  /** All fields associated with a query entry */
  static class QueryEntry {

    public JComboBox<BooleanOperation> booleanOperation;
    public JButton remove;
    public JButton add;
    public QueryField field;
    public JComboBox<String> typeChooser;
  }

  public Optional<String> getQueryAbbreviation() {
    List<Optional<String>> abbreviations = Lists.newArrayList();
    for (QueryEntry queryEntry : queryFields) {
      Optional<String> abbreviation = queryEntry.field.abbreviation();
      if (queryEntry.booleanOperation.getSelectedItem() == BooleanOperation.NEVER
          || queryEntry.booleanOperation.getSelectedItem() == BooleanOperation.NEVER_ALL) {
        abbreviation = abbreviation.transform(
            s -> Messages.getFormattedMessage(Name.NEVER_FORMAT, s));
      }
      abbreviations.add(abbreviation);
    }
    
    String joined = Joiner.on('-').join(Optional.presentInstances(abbreviations));
    if ("".equals(joined)) {
      return Optional.absent();
    }
    
    // Return the report abbreviation.  As this must be suitable for inclusion
    // in file names, replace any separators with a hyphen.
    return Optional.of(joined.replace(File.separatorChar, '-'));
  }

  public Optional<String> getQueryName() {
    List<Optional<String>> names = Lists.newArrayList();
    for (QueryEntry queryEntry : queryFields) {
      Optional<String> name = queryEntry.field.name();
      if (queryEntry.booleanOperation.getSelectedItem() == BooleanOperation.NEVER
          || queryEntry.booleanOperation.getSelectedItem() == BooleanOperation.NEVER_ALL) {
        name = name.transform(
            s -> Messages.getFormattedMessage(Name.NEVER_FORMAT, s));
      }
      names.add(name);
    }
    
    String joined = Joiner.on(' ').join(Optional.presentInstances(names));
    if ("".equals(joined)) {
      return Optional.absent();
    }
    
    return Optional.of(joined);    
  }

  /**
   * Notify any query fields that the taxonomy has changed.
   */
  public void taxonomyUpdated() {
    for (QueryEntry queryEntry : queryFields) {
      queryEntry.field.taxonomyUpdated();
    }    
  }
  
  @Subscribe
  public void defaultUserUpdated(DefaultUserChangedEvent userChangedEvent) {
    firePredicateChanged();
  }
}
