/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.ui.events.DefaultUserStore;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;

/**
 * Class for creating QueryPanel instances.
 */
public class QueryPanelFactory {
  private final ReportSet reportSet;
  private final QueryFieldFactory queryFieldFactory;
  private final StoredQueries storedQueries;
  private final EventBusRegistrar eventBusRegistrar;
  private final FontManager fontManager;
  private final DefaultUserStore defaultUserStore;

  @Inject 
  public QueryPanelFactory(
      ReportSet reportSet,
      QueryFieldFactory queryFieldFactory,
      StoredQueries storedQueries,
      EventBusRegistrar eventBusRegistrar,
      DefaultUserStore defaultUserStore,
      FontManager fontManager) {
    this.reportSet = reportSet;
    this.queryFieldFactory = queryFieldFactory;
    this.storedQueries = storedQueries;
    this.eventBusRegistrar = eventBusRegistrar;
    this.defaultUserStore = defaultUserStore;
    this.fontManager = fontManager;    
  }
  
  public QueryPanel newQueryPanel(QueryFieldType... excludedQueryFieldTypes) {
    return new QueryPanel(reportSet, queryFieldFactory, storedQueries, eventBusRegistrar,
        defaultUserStore, fontManager, excludedQueryFieldTypes);
  }
}
