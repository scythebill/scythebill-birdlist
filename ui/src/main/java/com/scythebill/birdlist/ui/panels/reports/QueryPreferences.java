/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import javax.annotation.Nullable;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.scythebill.birdlist.model.annotations.Preference;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.query.SightingPredicates;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Preferences for queries - should introduced/heard-only "count".
 */
public final class QueryPreferences {
  @Preference public boolean countIntroduced = true;
  @Preference public boolean countHeardOnly = true;
  @Preference public boolean countUndescribed = false;
  @Preference public boolean countRestrained = false;
  
  private final UndescribedTaxa undescribedTaxa;

  /**
   * A default set of countable population status - anything but
   * introduced-not-established or id-uncertain.
   */
  private static final Predicate<Sighting> DEFAULT_COUNTABLE_POPULATION_STATUS =
      Predicates.not(SightingPredicates.sightingStatusIsIn(
          ImmutableSet.of(
              SightingStatus.INTRODUCED_NOT_ESTABLISHED,
              SightingStatus.ID_UNCERTAIN,
              SightingStatus.UNSATISFACTORY_VIEWS,
              SightingStatus.DEAD,
              SightingStatus.NOT_BY_ME,
              SightingStatus.SIGNS,
              SightingStatus.DOMESTIC,
              SightingStatus.RESTRAINED)));

  /**
   * A default set of countable population status - anything but
   * introduced-not-established or id-uncertain, but does allow restrained sightings.
   */
  private static final Predicate<Sighting> COUNTABLE_POPULATION_STATUS_YES_RESTRAINED =
      Predicates.not(SightingPredicates.sightingStatusIsIn(
          ImmutableSet.of(
              SightingStatus.INTRODUCED_NOT_ESTABLISHED,
              SightingStatus.ID_UNCERTAIN,
              SightingStatus.UNSATISFACTORY_VIEWS,
              SightingStatus.DEAD,
              SightingStatus.NOT_BY_ME,
              SightingStatus.SIGNS,
              SightingStatus.DOMESTIC)));

  /**
   * An extended set of countable population status that also excludes introduced species.
   */
  private static final Predicate<Sighting> NO_INTRODUCED_COUNTABLE_POPULATION_STATUS =
      Predicates.not(SightingPredicates.sightingStatusIsIn(
          ImmutableSet.of(
              SightingStatus.INTRODUCED_NOT_ESTABLISHED,
              SightingStatus.INTRODUCED,
              SightingStatus.ID_UNCERTAIN,
              SightingStatus.UNSATISFACTORY_VIEWS,
              SightingStatus.DEAD,
              SightingStatus.NOT_BY_ME,
              SightingStatus.SIGNS,
              SightingStatus.DOMESTIC,
              SightingStatus.RESTRAINED)));

  /**
   * An extended set of countable population status that also excludes introduced species but does allow restrained sightings.
   */
  private static final Predicate<Sighting> NO_INTRODUCED_COUNTABLE_POPULATION_STATUS_YES_RESTRAINED =
      Predicates.not(SightingPredicates.sightingStatusIsIn(
          ImmutableSet.of(
              SightingStatus.INTRODUCED_NOT_ESTABLISHED,
              SightingStatus.INTRODUCED,
              SightingStatus.ID_UNCERTAIN,
              SightingStatus.UNSATISFACTORY_VIEWS,
              SightingStatus.DEAD,
              SightingStatus.NOT_BY_ME,
              SightingStatus.SIGNS,
              SightingStatus.DOMESTIC)));

  public QueryPreferences(UndescribedTaxa undescribedTaxa) {
    this.undescribedTaxa = undescribedTaxa;
  }

  @Nullable
  public Predicate<Sighting> getCountablePredicate(
      Taxonomy taxonomy,
      boolean skipSightingStatus,
      @Nullable Optional<Boolean> heardOnlyQuery) {
    // In general, don't consider tentative IDs and escapees as countable, etc..
    // But if the query is explicitly asking for a sighting status, then drop
    // that restriction (so the UI doesn't display 5 escapees and claim it as 0)
    Predicate<Sighting> countablePredicate;
    if (skipSightingStatus) {
      countablePredicate = null;
    } else if (countIntroduced) {
      countablePredicate = countRestrained ? COUNTABLE_POPULATION_STATUS_YES_RESTRAINED : DEFAULT_COUNTABLE_POPULATION_STATUS;
    } else {
      countablePredicate = countRestrained ? NO_INTRODUCED_COUNTABLE_POPULATION_STATUS_YES_RESTRAINED : NO_INTRODUCED_COUNTABLE_POPULATION_STATUS;
    }
    
    // Should "heard-only" be enforced?
    boolean enforceHeardOnly;
    // No override from the query;  just use the preference
    if (heardOnlyQuery == null) {
      enforceHeardOnly = !countHeardOnly;
    } else {
      enforceHeardOnly = heardOnlyQuery.isPresent();
    }
    
    // Yes, enforce 
    if (enforceHeardOnly) {
      boolean shouldBeHeardOnly;
      // No override from the query, heardOnly should be false
      if (heardOnlyQuery == null) {
        Preconditions.checkState(countHeardOnly == false);
        shouldBeHeardOnly = false;
      } else {
        shouldBeHeardOnly = heardOnlyQuery.get();
      }
      
      Predicate<Sighting> heardOnlyPredicate = shouldBeHeardOnly
          ? SightingPredicates.isHeardOnly()
          : Predicates.not(SightingPredicates.isHeardOnly());
      if (countablePredicate == null) {
        countablePredicate = heardOnlyPredicate;
      } else {
        countablePredicate = Predicates.and(countablePredicate, heardOnlyPredicate);
      }
    }
    
    if (!countUndescribed) {
      ImmutableSet<SightingTaxon> undescribedTaxa = this.undescribedTaxa.undescribedTaxa(taxonomy);
      Predicate<Sighting> describedPredicate =
          sighting -> !undescribedTaxa.contains(sighting.getTaxon());
      
      if (countablePredicate == null) {
        countablePredicate = describedPredicate;
      } else {
        countablePredicate = Predicates.and(countablePredicate, describedPredicate);
      }
    }
    
    return countablePredicate;
  }
  
  public ImmutableSet<Checklist.Status> excludedChecklistStatuses() {
    if (countIntroduced) {
      return ImmutableSet.of(Checklist.Status.ESCAPED);
    } else {
      return ImmutableSet.of(
          Checklist.Status.ESCAPED, Checklist.Status.INTRODUCED, Checklist.Status.RARITY_FROM_INTRODUCED);
    }
  }
}
