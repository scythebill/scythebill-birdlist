/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.annotation.Nullable;
import javax.swing.Box;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.SwingUtilities;
import javax.swing.GroupLayout.Alignment;
import javax.swing.text.NumberFormatter;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences.ScientificOrCommon;
import com.scythebill.birdlist.ui.components.AutoSelectJFormattedTextField;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * Dialog for configuring the print options.
 */
class ReportPrintDialog {
  private Alerts alerts;
  private ReportPrintPreferences reportPrintPreferences;
  private FontManager fontManager;
  private NamesPreferences namesPreferences;

  @Inject
  ReportPrintDialog(
      Alerts alerts,
      ReportPrintPreferences reportPrintPreferences,
      NamesPreferences namesPreferences,
      FontManager fontManager) {
    this.alerts = alerts;
    this.reportPrintPreferences = reportPrintPreferences;
    this.namesPreferences = namesPreferences;
    this.fontManager = fontManager;
  }
  
  @Nullable
  public ReportPrintPreferences getConfiguration(Component parent) {
    PrintConfigurationPanel panel = new PrintConfigurationPanel();
    fontManager.applyTo(panel);
    
    String formattedMessage = alerts.getFormattedDialogMessage(
        Name.PRINT_OPTIONS_TITLE, Name.PRINT_OPTIONS_MESSAGE);
    
    int okCancel = alerts.showOkCancelWithPanel(
        SwingUtilities.getWindowAncestor(parent), formattedMessage, panel);
    if (okCancel != JOptionPane.OK_OPTION) {
      return null;
    }

    panel.updatePreferences();
    return reportPrintPreferences;
  }
  
  class PrintConfigurationPanel extends JPanel {
    private JCheckBox includeScientific;
    private JCheckBox showFamilies;
    private AutoSelectJFormattedTextField maximumSightings;
    private JCheckBox showStatus;
    private JComboBox<SortType> sortSpeciesBy;
    private JComboBox<SortType> sortSightingsBy;
    private JCheckBox compactPrinting;
    private JCheckBox onlyCountable;
    private JCheckBox omitSpAndHybrid;
    private JLabel maximumSightingsLabel;

    PrintConfigurationPanel() {
      setBorder(new EmptyBorder(fontManager.scale(20),  0,  0, 0));
      
      GroupLayout layout = new GroupLayout(this);
      setLayout(layout);
      
      includeScientific = new JCheckBox(Messages.getMessage(Name.SCIENTIFIC_NAME_QUESTION));
      includeScientific.setSelected(reportPrintPreferences.includeScientific);
      
      if (namesPreferences.scientificOrCommon == ScientificOrCommon.COMMON_ONLY) {
        includeScientific.setSelected(false);
      } else if (namesPreferences.scientificOrCommon == ScientificOrCommon.SCIENTIFIC_ONLY) {
        includeScientific.setSelected(true);
      }
      
      showFamilies = new JCheckBox(Messages.getMessage(Name.SHOW_FAMILIES_QUESTION));
      showFamilies.setSelected(reportPrintPreferences.showFamilies);

      showStatus = new JCheckBox(Messages.getMessage(Name.SHOW_THREATENED_STATUS_QUESTION));
      showStatus.setSelected(reportPrintPreferences.showStatus);

      compactPrinting = new JCheckBox(Messages.getMessage(Name.COMPACT_PRINT_QUESTION));
      compactPrinting.setSelected(reportPrintPreferences.compactPrinting);
      compactPrinting.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          maximumSightingsLabel.setEnabled(!compactPrinting.isSelected());
          maximumSightings.setEnabled(!compactPrinting.isSelected());
        }
      });
      
      onlyCountable = new JCheckBox(Messages.getMessage(Name.ONLY_COUNTABLE_SIGHTINGS_QUESTION));
      onlyCountable.setSelected(reportPrintPreferences.onlyCountable);

      omitSpAndHybrid = new JCheckBox(
          Messages.getMessage(Name.OMIT_SP_AND_HYBRID_QUESTION));
      omitSpAndHybrid.setSelected(reportPrintPreferences.omitSpAndHybrid);

      NumberFormatter numberFormatter = new NumberFormatter();
      numberFormatter.setValueClass(Integer.class);
      numberFormatter.setMinimum(0);
      numberFormatter.setMaximum(100);

      maximumSightings = new AutoSelectJFormattedTextField(numberFormatter);
      maximumSightings.setColumns(3);
      maximumSightings.setValue(reportPrintPreferences.maximumSightings);
      maximumSightingsLabel = new JLabel(Messages.getMessage(Name.HOW_MANY_SIGHTINGS_QUESTION));
      
      sortSpeciesBy = new JComboBox<SortType>(SortType.values());
      JLabel sortSpeciesByLabel = new JLabel(Messages.getMessage(Name.SORT_SPECIES_BY));
      sortSpeciesBy.setSelectedItem(reportPrintPreferences.sortType);

      JLabel sortSightingsByLabel = new JLabel(Messages.getMessage(Name.SORT_SIGHTINGS_BY));
      sortSightingsBy = new JComboBox<>(SortType.values());
      sortSightingsBy.setSelectedItem(reportPrintPreferences.sortSightingsType);
      
      Box whichSpeciesOptions = Box.createVerticalBox();
      whichSpeciesOptions.add(onlyCountable);
      whichSpeciesOptions.add(omitSpAndHybrid);
      
      showFamilies.setEnabled(sortSpeciesBy.getSelectedItem() == SortType.DEFAULT);
      sortSpeciesBy.addActionListener(e -> {
        showFamilies.setEnabled(sortSpeciesBy.getSelectedItem() == SortType.DEFAULT);
      });
      
      JLabel whatData = new JLabel(Messages.getMessage(Name.WHAT_DATA_QUESTION));
      JLabel whichSpecies = new JLabel(Messages.getMessage(Name.WHICH_SPECIES_QUESTION));
      JLabel whichSightings = new JLabel(Messages.getMessage(Name.WHICH_SIGHTINGS_QUESTION));

      // Listener to update enabled state of everything that only makes sense
      // for non-compact printing, without species sorting.
      ActionListener nonCompactAndDefaultSpeciesSortOnly = e -> {
        boolean enabled =
            !compactPrinting.isSelected() && sortSpeciesBy.getSelectedItem() == SortType.DEFAULT;
        sortSightingsBy.setEnabled(enabled);
        sortSightingsByLabel.setEnabled(enabled);
        maximumSightings.setEnabled(enabled);
        maximumSightingsLabel.setEnabled(enabled);
        whichSightings.setEnabled(enabled);
      };
      sortSpeciesBy.addActionListener(nonCompactAndDefaultSpeciesSortOnly);
      compactPrinting.addActionListener(nonCompactAndDefaultSpeciesSortOnly);
      nonCompactAndDefaultSpeciesSortOnly.actionPerformed(null);
      
      JPanel whichSightingsOptions = new JPanel();
      GroupLayout whichSightingsLayout = new GroupLayout(whichSightingsOptions);
      whichSightingsLayout.setHorizontalGroup(
          whichSightingsLayout.createParallelGroup()
             .addComponent(maximumSightingsLabel)
             .addComponent(maximumSightings, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE));
      whichSightingsLayout.setVerticalGroup(
          whichSightingsLayout.createSequentialGroup()
             .addComponent(maximumSightingsLabel)
             .addComponent(maximumSightings, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE));
      whichSightingsOptions.setLayout(whichSightingsLayout);

      layout.setHorizontalGroup(
          layout.createSequentialGroup()
               .addGroup(layout.createParallelGroup()
                   .addComponent(whatData)
                   .addComponent(compactPrinting)
                   .addComponent(includeScientific)
                   .addComponent(showFamilies)
                   .addComponent(showStatus))
               .addPreferredGap(ComponentPlacement.UNRELATED)
               .addGroup(layout.createParallelGroup()
                   .addComponent(whichSpecies)
                   .addComponent(whichSpeciesOptions)
                   .addComponent(sortSpeciesByLabel)
                   .addComponent(sortSpeciesBy,
                       GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
               .addPreferredGap(ComponentPlacement.UNRELATED)
               .addGroup(layout.createParallelGroup()
                   .addComponent(whichSightings)
                   .addComponent(whichSightingsOptions)
                   .addComponent(sortSightingsByLabel)
                   .addComponent(sortSightingsBy,
                       GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)));
      layout.setVerticalGroup(
          layout.createParallelGroup(Alignment.LEADING)
              .addGroup(layout.createSequentialGroup()
                  .addComponent(whatData)
                  .addPreferredGap(ComponentPlacement.RELATED)
                  .addComponent(compactPrinting)
                  .addPreferredGap(ComponentPlacement.RELATED)
                  .addComponent(includeScientific)
                  .addComponent(showFamilies)
                  .addComponent(showStatus))
              .addGroup(layout.createSequentialGroup()
                  .addComponent(whichSpecies)
                  .addPreferredGap(ComponentPlacement.RELATED)
                  .addComponent(whichSpeciesOptions)
                  .addPreferredGap(ComponentPlacement.RELATED)
                  .addComponent(sortSpeciesByLabel)
                  .addComponent(sortSpeciesBy,
                      GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(layout.createSequentialGroup()
                  .addComponent(whichSightings)
                  .addPreferredGap(ComponentPlacement.RELATED)
                  .addComponent(whichSightingsOptions)                  
                  .addPreferredGap(ComponentPlacement.RELATED)
                  .addComponent(sortSightingsByLabel)
                  .addComponent(sortSightingsBy,
                      GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)));

      // Link the options vertical size so the sort comboboxes line up
      layout.linkSize(SwingConstants.VERTICAL, whichSightingsOptions, whichSpeciesOptions);
    }
    
    void updatePreferences() {
      reportPrintPreferences.includeScientific = includeScientific.isSelected();
      reportPrintPreferences.showFamilies = showFamilies.isSelected();
      reportPrintPreferences.showStatus = showStatus.isSelected();
      reportPrintPreferences.compactPrinting = compactPrinting.isSelected();
      reportPrintPreferences.onlyCountable = onlyCountable.isSelected();
      reportPrintPreferences.omitSpAndHybrid = omitSpAndHybrid.isSelected();
      reportPrintPreferences.maximumSightings = (Integer) maximumSightings.getValue();
      reportPrintPreferences.sortType = (SortType) sortSpeciesBy.getSelectedItem();
      reportPrintPreferences.sortSightingsType = (SortType) sortSightingsBy.getSelectedItem();
    }
  }

  /**
   * Derive the scientific-and-common ordering based on a mix of the chosen setting
   * in the dialog and the setting in the names preferences.
   */
  public ScientificOrCommon getScientificOrCommonFromConfiguration(ReportPrintPreferences prefs) {
    ScientificOrCommon scientificOrCommon = namesPreferences.scientificOrCommon;
    if (prefs.includeScientific) {
      if (scientificOrCommon == ScientificOrCommon.COMMON_ONLY) {
        // Can't have COMMON_ONLY if the user asks for scientific names;  use COMMON_FIRST
        return ScientificOrCommon.COMMON_FIRST;
      }
    } else {
      switch (scientificOrCommon) {
        case SCIENTIFIC_ONLY:
        case COMMON_FIRST:
        case SCIENTIFIC_FIRST:
          // None of the above make sense if scientific names are deselected;  switch to COMMON_ONLY mode.
          return ScientificOrCommon.COMMON_ONLY;
        default:
          break;
      }
    }
    
    return scientificOrCommon;
  }
}
