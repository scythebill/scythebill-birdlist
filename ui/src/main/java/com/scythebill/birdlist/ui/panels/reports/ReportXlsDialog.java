/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.awt.Component;

import javax.annotation.Nullable;
import javax.swing.Box;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.text.NumberFormatter;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences.ScientificOrCommon;
import com.scythebill.birdlist.ui.components.AutoSelectJFormattedTextField;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * Dialog for configuring the checklist spreadsheet options.
 */
class ReportXlsDialog {
  private Alerts alerts;
  private ReportSpreadsheetPreferences reportSpreadsheetPreferences;
  private FontManager fontManager;
  private NamesPreferences namesPreferences;
  private ReportSet reportSet;

  @Inject
  ReportXlsDialog(
      ReportSet reportSet,
      Alerts alerts,
      ReportSpreadsheetPreferences reportSpreadsheetPreferences,
      NamesPreferences namesPreferences,
      FontManager fontManager) {
    this.reportSet = reportSet;
    this.alerts = alerts;
    this.reportSpreadsheetPreferences = reportSpreadsheetPreferences;
    this.namesPreferences = namesPreferences;
    this.fontManager = fontManager;
  }
  
  @Nullable
  public ReportSpreadsheetPreferences getConfiguration(Component parent, boolean allowOtherTaxonomies) {
    XlsConfigurationPanel panel = new XlsConfigurationPanel(allowOtherTaxonomies);
    fontManager.applyTo(panel);
    
    String formattedMessage = alerts.getFormattedDialogMessage(
        Name.SPREADSHEET_OPTIONS, Name.SPREADSHEET_OPTIONS_MESSAGE);
    
    int okCancel = alerts.showOkCancelWithPanel(
        SwingUtilities.getWindowAncestor(parent), formattedMessage, panel);
    if (okCancel != JOptionPane.OK_OPTION) {
      return null;
    }

    panel.updatePreferences();
    return reportSpreadsheetPreferences;
  }
  
  class XlsConfigurationPanel extends JPanel {
    private JCheckBox includeScientific;
    private JCheckBox showFamilies;
    private JCheckBox showNotes;
    private AutoSelectJFormattedTextField maximumSightings;
    private JCheckBox showStatus;
    private JComboBox<SortType> sortSpeciesBy;
    private JComboBox<SortType> sortSightingsBy;
    private JCheckBox onlyCountable;
    private JCheckBox omitSpAndHybrid;
    private JComboBox<UserOutput> userOutput;
    private JCheckBox showAllTaxonomies;

    XlsConfigurationPanel(boolean allowOtherTaxonomies) {
      setBorder(new EmptyBorder(fontManager.scale(20),  0,  0, 0));
      GroupLayout layout = new GroupLayout(this);
      setLayout(layout);
      
      includeScientific = new JCheckBox(
          Messages.getMessage(Name.SCIENTIFIC_NAME_QUESTION));
      includeScientific.setSelected(reportSpreadsheetPreferences.includeScientific);
      
      if (namesPreferences.scientificOrCommon == ScientificOrCommon.COMMON_ONLY) {
        includeScientific.setSelected(false);
      } else if (namesPreferences.scientificOrCommon == ScientificOrCommon.SCIENTIFIC_ONLY) {
        includeScientific.setSelected(true);
      }
      
      showFamilies = new JCheckBox(
          Messages.getMessage(Name.SHOW_FAMILIES_QUESTION));
      showFamilies.setSelected(reportSpreadsheetPreferences.showFamilies);

      showStatus = new JCheckBox(
          Messages.getMessage(Name.SHOW_THREATENED_STATUS_QUESTION));
      showStatus.setSelected(reportSpreadsheetPreferences.showStatus);

      showNotes = new JCheckBox(
          Messages.getMessage(Name.SHOW_NOTES_COUNT_ETC_QUESTION));
      showNotes.setSelected(reportSpreadsheetPreferences.showNotes);

      showAllTaxonomies = new JCheckBox(
          Messages.getMessage(Name.INCLUDE_ALL_TAXONOMIES_QUESTION));
      showAllTaxonomies.setSelected(reportSpreadsheetPreferences.showAllTaxonomies);
      showAllTaxonomies.setEnabled(allowOtherTaxonomies);
      
      onlyCountable = new JCheckBox(
          Messages.getMessage(Name.ONLY_COUNTABLE_SIGHTINGS_QUESTION));
      onlyCountable.setSelected(reportSpreadsheetPreferences.onlyCountable);

      omitSpAndHybrid = new JCheckBox(
          Messages.getMessage(Name.OMIT_SP_AND_HYBRID_QUESTION));
      omitSpAndHybrid.setSelected(reportSpreadsheetPreferences.omitSpAndHybrid);

      NumberFormatter numberFormatter = new NumberFormatter();
      numberFormatter.setValueClass(Integer.class);
      numberFormatter.setMinimum(0);
      numberFormatter.setMaximum(100);

      maximumSightings = new AutoSelectJFormattedTextField(numberFormatter);
      maximumSightings.setColumns(3);
      maximumSightings.setValue(reportSpreadsheetPreferences.maximumSightings);
      JLabel maximumSightingsLabel = new JLabel(
          Messages.getMessage(Name.HOW_MANY_SIGHTINGS_QUESTION));
      
      sortSpeciesBy = new JComboBox<>(SortType.values());
      JLabel sortSpeciesByLabel = new JLabel(Messages.getMessage(Name.SORT_SPECIES_BY));
      sortSpeciesBy.setSelectedItem(reportSpreadsheetPreferences.sortType);
      showFamilies.setEnabled(sortSpeciesBy.getSelectedItem() == SortType.DEFAULT);
      sortSpeciesBy.addActionListener(e -> {
        showFamilies.setEnabled(sortSpeciesBy.getSelectedItem() == SortType.DEFAULT);
      });
      
      JLabel userOutputLabel = new JLabel(Messages.getMessage(Name.SHOW_OBSERVERS_QUESTION));
      userOutput = new JComboBox<>(UserOutput.values());
      userOutput.setSelectedItem(reportSpreadsheetPreferences.userOutput);
      userOutputLabel.setVisible(reportSet.getUserSet() != null);
      userOutput.setVisible(reportSet.getUserSet() != null);

      JLabel sortSightingsByLabel = new JLabel(Messages.getMessage(Name.SORT_SIGHTINGS_BY));
      sortSightingsBy = new JComboBox<>(SortType.values());
      sortSightingsBy.setSelectedItem(reportSpreadsheetPreferences.sortSightingsType);
      
      JLabel whatData = new JLabel(Messages.getMessage(Name.WHAT_DATA_QUESTION));
      JLabel whichSpecies = new JLabel(Messages.getMessage(Name.WHICH_SPECIES_QUESTION));
      JLabel whichSightings = new JLabel(Messages.getMessage(Name.WHICH_SIGHTINGS_QUESTION));

      showFamilies.setEnabled(sortSpeciesBy.getSelectedItem() == SortType.DEFAULT);
      sortSightingsBy.setEnabled(sortSpeciesBy.getSelectedItem() == SortType.DEFAULT);
      sortSightingsByLabel.setEnabled(sortSpeciesBy.getSelectedItem() == SortType.DEFAULT);
      sortSpeciesBy.addActionListener(e -> {
        showFamilies.setEnabled(sortSpeciesBy.getSelectedItem() == SortType.DEFAULT);
        sortSightingsBy.setEnabled(sortSpeciesBy.getSelectedItem() == SortType.DEFAULT);
        sortSightingsByLabel.setEnabled(sortSpeciesBy.getSelectedItem() == SortType.DEFAULT);
      });

      Box whichSpeciesOptions = Box.createVerticalBox();
      whichSpeciesOptions.add(onlyCountable);
      whichSpeciesOptions.add(omitSpAndHybrid);
      
      JPanel whichSightingsOptions = new JPanel();
      GroupLayout whichSightingsLayout = new GroupLayout(whichSightingsOptions);
      whichSightingsLayout.setHorizontalGroup(
          whichSightingsLayout.createParallelGroup()
             .addComponent(maximumSightingsLabel)
             .addComponent(maximumSightings, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE));
      whichSightingsLayout.setVerticalGroup(
          whichSightingsLayout.createSequentialGroup()
             .addComponent(maximumSightingsLabel)
             .addComponent(maximumSightings, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE));
      whichSightingsOptions.setLayout(whichSightingsLayout);
      
      layout.setHorizontalGroup(
          layout.createSequentialGroup()
               .addGroup(layout.createParallelGroup()
                   .addComponent(whatData)
                   .addComponent(includeScientific)
                   .addComponent(showFamilies)
                   .addComponent(showStatus)
                   .addComponent(showNotes)
                   .addComponent(showAllTaxonomies)
                   .addComponent(userOutputLabel)
                   .addComponent(userOutput,
                       GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
               .addPreferredGap(ComponentPlacement.UNRELATED)
               .addGroup(layout.createParallelGroup()
                   .addComponent(whichSpecies)
                   .addComponent(whichSpeciesOptions)
                   .addComponent(sortSpeciesByLabel)
                   .addComponent(sortSpeciesBy,
                       GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
               .addPreferredGap(ComponentPlacement.UNRELATED)
               .addGroup(layout.createParallelGroup()
                   .addComponent(whichSightings)
                   .addComponent(whichSightingsOptions)
                   .addComponent(sortSightingsByLabel)
                   .addComponent(sortSightingsBy,
                       GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)));
      layout.setVerticalGroup(
          layout.createParallelGroup(Alignment.LEADING)
              .addGroup(layout.createSequentialGroup()
                  .addComponent(whatData)
                  .addPreferredGap(ComponentPlacement.RELATED)
                  .addComponent(includeScientific)
                  .addComponent(showFamilies)
                  .addComponent(showStatus)
                  .addComponent(showNotes)
                  .addComponent(showAllTaxonomies)
                  .addPreferredGap(ComponentPlacement.UNRELATED)
                  .addComponent(userOutputLabel)
                  .addComponent(userOutput))
              .addGroup(layout.createSequentialGroup()
                  .addComponent(whichSpecies)
                  .addPreferredGap(ComponentPlacement.RELATED)
                  .addComponent(whichSpeciesOptions)
                  .addPreferredGap(ComponentPlacement.RELATED)
                  .addComponent(sortSpeciesByLabel)
                  .addComponent(sortSpeciesBy,
                      GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(layout.createSequentialGroup()
                  .addComponent(whichSightings)
                  .addPreferredGap(ComponentPlacement.RELATED)
                  .addComponent(whichSightingsOptions)                  
                  .addPreferredGap(ComponentPlacement.RELATED)
                  .addComponent(sortSightingsByLabel)
                  .addComponent(sortSightingsBy,
                      GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)));
      // Link the options vertical size so the sort comboboxes line up
      layout.linkSize(SwingConstants.VERTICAL, whichSightingsOptions, whichSpeciesOptions);
    }
    
    void updatePreferences() {
      reportSpreadsheetPreferences.includeScientific = includeScientific.isSelected();
      reportSpreadsheetPreferences.showFamilies = showFamilies.isSelected();
      reportSpreadsheetPreferences.showStatus = showStatus.isSelected();
      reportSpreadsheetPreferences.showNotes = showNotes.isSelected();
      reportSpreadsheetPreferences.showAllTaxonomies = showAllTaxonomies.isSelected();
      reportSpreadsheetPreferences.onlyCountable = onlyCountable.isSelected();
      reportSpreadsheetPreferences.omitSpAndHybrid = omitSpAndHybrid.isSelected();
      reportSpreadsheetPreferences.maximumSightings = (Integer) maximumSightings.getValue();
      reportSpreadsheetPreferences.sortType = (SortType) sortSpeciesBy.getSelectedItem();
      reportSpreadsheetPreferences.sortSightingsType = (SortType) sortSightingsBy.getSelectedItem();
      reportSpreadsheetPreferences.userOutput = (UserOutput) userOutput.getSelectedItem();
    }
  }

  /**
   * Derive the scientific-and-common ordering based on a mix of the chosen setting
   * in the dialog and the setting in the names preferences.
   */
  public ScientificOrCommon getScientificOrCommonFromConfiguration(ReportSpreadsheetPreferences prefs) {
    ScientificOrCommon scientificOrCommon = namesPreferences.scientificOrCommon;
    if (prefs.includeScientific) {
      if (scientificOrCommon == ScientificOrCommon.COMMON_ONLY) {
        // Can't have COMMON_ONLY if the user asks for scientific names;  use COMMON_FIRST
        return ScientificOrCommon.COMMON_FIRST;
      }
    } else {
      switch (scientificOrCommon) {
        case SCIENTIFIC_ONLY:
        case COMMON_FIRST:
        case SCIENTIFIC_FIRST:
          // None of the above make sense if scientific names are deselected;  switch to COMMON_ONLY mode.
          return ScientificOrCommon.COMMON_ONLY;
        default:
          break;
      }
    }
    
    return scientificOrCommon;
  }
}
