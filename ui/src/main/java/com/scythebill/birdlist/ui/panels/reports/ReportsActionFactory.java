/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.Action;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.guice.Clements;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;

/**
 * Factory for actions used by reporting.  Only exists to
 * make the reports panel's Guice bindings a little less massive.
 */
public class ReportsActionFactory {
  private final FileDialogs fileDialogs;
  private final ReportSet reportSet;
  private final TaxonomyStore taxonomyStore;
  private final String fileName;
  private final Taxonomy clements;
  private final Alerts alerts;
  private final ReportXlsDialog reportXlsDialog;
  private final Provider<TripReportDialog> tripReportDialog;
  private final ReportPrintDialog reportPrintDialog;
  private final FontManager fontManager;
  private final QueryPreferences queryPreferences;
  private final NamesPreferences namesPreferences;
  private final FamilyReportDialog familyReportDialog;
  private final Checklists checklists;

  @Inject
  public ReportsActionFactory(
      FileDialogs fileDialogs,
      ReportSet reportSet,
      TaxonomyStore taxonomyStore,
      @Clements Taxonomy clements,
      File reportSetFile,
      Alerts alerts,
      FontManager fontManager,
      ReportXlsDialog reportXlsDialog,
      ReportPrintDialog reportPrintDialog,
      Provider<TripReportDialog> tripReportDialog,
      FamilyReportDialog familyReportDialog,
      QueryPreferences queryPreferences,
      NamesPreferences namesPreferences,
      Checklists checklists) {
    this.fileDialogs = fileDialogs;
    this.reportSet = reportSet;
    this.taxonomyStore = taxonomyStore;
    this.clements = clements;
    this.alerts = alerts;
    this.fontManager = fontManager;
    this.reportXlsDialog = reportXlsDialog;
    this.reportPrintDialog = reportPrintDialog;
    this.tripReportDialog = tripReportDialog;
    this.familyReportDialog = familyReportDialog;
    this.queryPreferences = queryPreferences;
    this.namesPreferences = namesPreferences;
    this.checklists = checklists;
    String fileName = reportSetFile.getName();
    if (fileName.lastIndexOf('.') > 0) {
      fileName = fileName.substring(0, fileName.lastIndexOf('.'));
    }
    this.fileName = fileName;
  }
  
  public Action saveAsXls(QueryExecutor queryExecutor) {
    return new SaveAsXlsAction(
        fileDialogs, reportSet, queryExecutor, alerts, reportXlsDialog);
  }
  
  public Action ebirdExport(QueryExecutor queryExecutor) {
    return new EbirdExportAction(fileDialogs, reportSet, queryExecutor, clements, alerts);
  }

  public Action iNaturalistExport(QueryExecutor queryExecutor) {
    return new INaturalistExportAction(fileDialogs, reportSet, queryExecutor,
        // Run bird queries with clements, everything else with the current taxonomy
        taxonomyStore.getTaxonomy().isBuiltIn() ? taxonomyStore.getClements()
            : taxonomyStore.getTaxonomy(),
        alerts);
  }

  public Action birdTrackExport(QueryExecutor queryExecutor) {
    return new BirdTrackExportAction(fileDialogs, reportSet, queryExecutor,
        namesPreferences,
        // Run bird queries with IOC, everything else with the current taxonomy
        taxonomyStore.getTaxonomy().isBuiltIn() ? taxonomyStore.getIoc()
            : taxonomyStore.getTaxonomy(), alerts);
  }

  public Action print(QueryExecutor queryExecutor) {
    return new PrintAction(
        fileName, reportSet, taxonomyStore, queryExecutor, alerts, reportPrintDialog);
  }

  public ActionListener saveAsScythebill(QueryExecutor queryExecutor) {
    return new ScythebillCsvExportAction(
        fileDialogs, reportSet, fileName, taxonomyStore, queryExecutor, alerts);
  }
  
  public Action saveAsTripReport(QueryExecutor queryExecutor) {
    return new SaveAsTripReportAction(
        reportSet, queryExecutor, alerts, tripReportDialog, fontManager);
  }  

  public Action saveAsFamilyReport(QueryExecutor queryExecutor) {
    return new SaveAsFamilyReportAction(
        queryExecutor, alerts, familyReportDialog, fontManager,
        reportSet, taxonomyStore.getTaxonomy(), queryPreferences,
        checklists);
  }  
}

