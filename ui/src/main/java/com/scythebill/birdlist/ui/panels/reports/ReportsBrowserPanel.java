/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.util.Collection;
import java.util.EventListener;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.TransferHandler;
import javax.swing.text.JTextComponent;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.query.QueryDefinition.QueryAnnotation;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Type;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.names.LocalNames;
import com.scythebill.birdlist.ui.actions.ActionBroker;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.components.BaseTaxonBrowserPanel;
import com.scythebill.birdlist.ui.components.NewLocationDialog;
import com.scythebill.birdlist.ui.components.SightingBrowsePanel;
import com.scythebill.birdlist.ui.components.SightingBulkEditPanel;
import com.scythebill.birdlist.ui.components.VisitInfoPreferences;
import com.scythebill.birdlist.ui.datatransfer.SightingsGroup;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.SpHybridDialog;
import com.scythebill.birdlist.ui.panels.SpeciesInfoDescriber;
import com.scythebill.birdlist.ui.panels.reports.ReportsTreeModel.VisitInfoKeyNode;
import com.scythebill.birdlist.ui.panels.reports.ReportsTreeModel.VisitNode;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;
import com.scythebill.birdlist.ui.util.FocusTracker;
import com.scythebill.birdlist.ui.util.LocationIdToString;
import com.scythebill.birdlist.ui.util.OpenMapUrl;
import com.scythebill.birdlist.ui.util.ResolvedWithSighting;
import com.scythebill.birdlist.ui.util.ScanSeenTaxa;
import com.scythebill.birdlist.ui.util.SightingFlags;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Browser used to look through query results.
 */
public class ReportsBrowserPanel extends BaseTaxonBrowserPanel {
  private static final Logger logger = Logger.getLogger(ReportsBrowserPanel.class.getName());
  
  /** Currently visible query results. */
  private QueryResults queryResults;
  /** Whether family totals should be displayed. */
  private boolean showFamilyTotals;

  private boolean editableSightings;

  private boolean showSpeciesStatus;

  private ActionBroker actionBroker;

  private Taxon.Type depth = Taxon.Type.species;

  private boolean editableWhere;

  private ScanSeenTaxa scanSeenTaxa;
  private final ReportsBrowserFocusTracker reportsBrowserFocusTracker;

  private SortType sortType = SortType.DEFAULT;

  private boolean allowLocationEditing = true;
  private final VisitInfoPreferences visitInfoPreferences;

  private QueryPreferences queryPreferences;

  private String returnPanelName = "reports";

  @Inject
  public ReportsBrowserPanel(
      FontManager fontManager,
      ReportSet reportSet,
      ActionBroker actionBroker,
      NewLocationDialog newLocationDialog,
      SpHybridDialog spHybridDialog,
      PredefinedLocations predefinedLocations,
      SpeciesInfoDescriber speciesInfoDescriber,
      OpenMapUrl openMapUrl,
      VisitInfoPreferences visitInfoPreferences,
      QueryPreferences queryPreferences,
      FileDialogs fileDialogs,
      Alerts alerts,
      NavigableFrame navigableFrame) {
    super(fontManager, reportSet, newLocationDialog, spHybridDialog,
        predefinedLocations, speciesInfoDescriber, openMapUrl, fileDialogs, alerts, navigableFrame);
    this.actionBroker = actionBroker;
    this.visitInfoPreferences = visitInfoPreferences;
    this.queryPreferences = queryPreferences;
    setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
    setDragEnabled(true);
    setTransferHandler(new ReportsTransferHandler());
    // Track all focus changes to keep the edit menu in sync
    reportsBrowserFocusTracker = new ReportsBrowserFocusTracker();
  }
  
  @Override public String convertValueToText(Object value, boolean selected,
      boolean expanded, boolean leaf, int row, boolean hasFocus) {
    if (value instanceof VisitInfoKey) {
      return Messages.getMessage(Name.VISIT_DATA);
    }

    String text;
    if (value instanceof ReportsTreeModel.VisitNode) {
      text =
          Messages.getFormattedMessage(
              leaf ? Name.VISITS_CAPITALIZED_FORMAT : Name.VISITS_FORMAT,
              ((ReportsTreeModel.VisitNode) value).getVisitCount());
    } else if (value instanceof ResolvedWithSighting) {
      // Hacky - avoid calling convertValueToText, or any other overridden
      // method - for a SightingAndResolved. These are only used for
      // the special case of showing the sightings hanging off a single
      // VisitInfoKey, and all of those override methods assume that you
      // can incorporate information from the overall queryResult.
      Resolved resolved = ((ResolvedWithSighting) value).getResolved();
      // Had mostly cloned the code from other paths (LocalNames.getCompoundName(),
      // in particular), but in the end going for a more compact option seemed better. 
      text = resolved.getPreferredSingleName();
      text = SightingFlags.appendSightingInfo(text, ((ResolvedWithSighting) value).getSighting());
    } else {
      if (value instanceof ReportsTreeModel.VisitInfoKeyNode) {
        value = ((ReportsTreeModel.VisitInfoKeyNode) value).getVisitInfoKey();
      }
      text = super.convertValueToText(value, selected, expanded, leaf, row, hasFocus);
    }

    if (value instanceof Resolved || value instanceof ResolvedWithSighting) {
      Resolved resolved = value instanceof Resolved 
          ? (Resolved) value
          : ((ResolvedWithSighting) value).getResolved();
      if (resolved.getSmallestTaxonType() == Taxon.Type.family) {
        text = text.toUpperCase();
      }

      if (getChecklist() != null) {
        if (resolved.getType() == Type.SINGLE) {
          Checklist.Status status = getChecklist().getStatus(
              resolved.getTaxonomy(), resolved.getSightingTaxon());
          if (status == Checklist.Status.ENDEMIC) {
            text += " - " + Messages.getMessage(Name.CHECKLIST_STATUS_ENDEMIC).toLowerCase();
          } else if (status == Checklist.Status.INTRODUCED) {
            // Don't add "introduced" if there's sightings already, and *they're* introduced
            String introduced = Messages.getMessage(Name.CHECKLIST_STATUS_INTRODUCED).toLowerCase();
            if (!text.contains(introduced)) {
              text += " - " + introduced;
            }
          } else if (status == Checklist.Status.ESCAPED) {
            // Don't add "not established" if there's sightings already, and *they're* escapee
            String notEstablished = Messages.getMessage(Name.CHECKLIST_STATUS_NOT_ESTABLISHED).toLowerCase();
            if (!text.contains(notEstablished)) {
              text += " - " + notEstablished;
            }
          } else if (status == Checklist.Status.RARITY
              // The full "rarity from an introduced population" thing
              // is really lengthy.  Don't write it here.
              || status == Checklist.Status.RARITY_FROM_INTRODUCED) {
            text += " - " + Messages.getMessage(Name.CHECKLIST_STATUS_RARITY).toLowerCase();
          } else if (status == Checklist.Status.EXTINCT) {
            text += " - " + Messages.getMessage(Name.CHECKLIST_STATUS_EXTINCT).toLowerCase();
          }
        }
      }
    } else if (value instanceof VisitInfoKey) {
      VisitInfoKey key = (VisitInfoKey) value;
      Location rootLocation = getLocationRoot();
      if (rootLocation != null) {
        if (!rootLocation.getId().equals(key.locationId())) {
          text = text + " " +
              LocationIdToString.getString(getReportSet().getLocations(), key.locationId(),
                  false /* not verbose */, rootLocation);
        }
      } else {
        text = text + " " +
            LocationIdToString.getString(getReportSet().getLocations(), key.locationId(),
                false /* not verbose */, null);
      }
      
    }
    
    return text;
  }

  /**
   * Returns true if the referenced taxon would be a lifer.
   */
  public boolean wouldBeLifer(Resolved resolved) {
    if (scanSeenTaxa != null && resolved.getLargestTaxonType().compareTo(Taxon.Type.species) <= 0) {
      Set<String> seenTaxa = scanSeenTaxa.getSeenTaxa(resolved.getTaxonomy(), null);
      if (seenTaxa != null) {
        if (resolved.getType() == SightingTaxon.Type.SINGLE
            || resolved.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
          if (!seenTaxa.contains(resolved.getTaxon().getId())) {
            return true;
          }
        }
      }
    }
    
    return false;
  }


  /**
   * Returns true if the referenced taxon has any sightings in the current report.
   */
  public boolean areSightingsInThisReport(Resolved resolved) {
    if (queryResults == null) {
      return false;
    }
    
    return !queryResults.getAllSightings(resolved).isEmpty();
  }
  
  @Override
  protected Font deriveTaxonFont(Font base, Taxon taxon, boolean leaf) {
    base = super.deriveTaxonFont(base, taxon, leaf);
    if (taxon.getType() == Taxon.Type.family) {
      return base.deriveFont(Font.BOLD);
    } else if (getChecklist() != null) {
      // If there's a checklist set, there may be sightings here that have not yet been seen.
      // Italicize them.
      if (scanSeenTaxa != null) {
        Set<String> seenTaxa = scanSeenTaxa.getSeenTaxa(taxon.getTaxonomy(), null);
        if (seenTaxa != null) {
          if (!seenTaxa.contains(taxon.getId())) {
            return base.deriveFont(Font.ITALIC); 
          }
        }
      }
    }
    
    // If sightings are annotated with "lifer", mark it as bold.
    if (queryResults != null
        && queryResults.containsAnnotation(QueryAnnotation.LIFER, SightingTaxons.newResolved(taxon))) {
      return base.deriveFont(Font.BOLD);
    }
    
    return base;
  }

  @Override
  protected Color deriveTaxonForeground(
      Color foreground, Taxon taxon, boolean leaf, boolean selected) {
    foreground = super.deriveTaxonForeground(foreground, taxon, leaf, selected);
    if (getChecklist() != null && taxon.getType() != Taxon.Type.family) {
      // If it's a leaf taxon in a checklist
      if (leaf && !selected) {
        return Color.gray;
      }
    }
    return foreground;
  }

  /**
   * Override to change a taxon's font.
   */
  @Override
  protected Font deriveOtherFont(Font base, Object value) {
    if (value instanceof ReportsTreeModel.VisitNode || value instanceof VisitInfoKey) {
      return base.deriveFont(Font.BOLD);
    }
    
    return base;
  }

  public void setDepth(Taxon.Type depth) {
    this.depth = depth;
  }
  
  public void setEditableSightings(boolean editableSightings) {
    this.editableSightings = editableSightings;
  }
  
  public void setEditableWhere(boolean editableWhere) {
    this.editableWhere = editableWhere;
  }

  @Override
  protected void customizeSightingPreviewPanel(SightingBrowsePanel panel) {
    panel.setWhereVisible(editableWhere);
    panel.setEditable(editableSightings);
  }
  
  @Override
  protected void customizeSightingPreviewPanel(SightingBulkEditPanel panel) {
    panel.setEditable(editableSightings);
  }

  @Override
  protected boolean usePreviewColumn(TreePath[] paths) {
    boolean usePreviewDefault = super.usePreviewColumn(paths);
    if (!usePreviewDefault) {
      return false;
    }
    
    Object lastPathElement = paths[0].getLastPathComponent();
    if (lastPathElement instanceof Taxon) {
      // No previews for taxa (they either have sightings children, or they're family headings)
      return false;
    }
    
    return true;
  }
  
  @Override
  protected String convertTaxonToString(Taxon taxon) {
    LocalNames localNames = taxon.getTaxonomy().getLocalNames();
    String name;
    
    if (taxon.getCommonName() != null || taxon instanceof Species) {
      name = localNames.compoundName(
          taxon, localNames.isCommonNamePreferred() && abbreviateGenus(taxon));
    } else {
      // Don't bother generating compound names when there's no common name *and* it's a higher-order taxon 
      name = taxon.getName();
    }
    
    if (showSpeciesStatus && taxon instanceof Species) {
      Status status = TaxonUtils.getTaxonStatus(taxon);
      if (status != Status.LC) {
        name += " (" + status + ")";
      }
    }
    if (queryResults != null) {
      // Find the "best" sighting, when the sort type is default, and append information
      // on it.  (In non-default sorts, there isn't really a "best" sighting)
      if (sortType == SortType.DEFAULT) {
        Sighting sighting = queryResults.getBestSighting(taxon, sortType.ordering());
        if (sighting != null) {
          name = SightingFlags.appendSightingInfo(name, sighting);
        }
      }
    }
    
    boolean isFamily = taxon.getType() == Taxon.Type.family;
    if (isFamily && queryResults != null && showFamilyTotals) {
      final Set<String> speciesSet = Sets.newHashSet();
      TaxonUtils.visitTaxa(taxon, new TaxonVisitor() {
        @Override public boolean visitTaxon(Taxon child) {
          if (queryResults.containsTaxonCountably(child)) {
            Taxon species = TaxonUtils.getParentOfType(child, Taxon.Type.species);
            speciesSet.add(species.getId());
            return false;
          }
          // TODO: could break earlier if we knew the max depth of the query
          // results (avoid iterating through subspecies when we know the
          // query results don't contain subspecies)
          return true;
        }
      });

      StringBuilder buffer = new StringBuilder(name);
      int speciesCount = TaxonUtils.countChildren(
          taxon, Taxon.Type.species, getChecklist(), queryPreferences.excludedChecklistStatuses());
      buffer.append(" (").append(speciesSet.size()).append("/").append(speciesCount).append(")");
      name = buffer.toString();
    }

    return name;
  }

  
  @Override
  protected boolean abbreviateGenus(Taxon taxon) {
    if (taxon.getType() != Taxon.Type.species) {
      return true;
    }
    
    Taxon genus = taxon.getParent();
    List<Taxon> speciesList = genus.getContents();
    for (int i = 0; i < speciesList.size(); i++) {
      Taxon species = speciesList.get(i);
      if (species == taxon) {
        // Found the species, but had not yet found a precursor in the report.
        // Don't abbreviate.
        return false;
      } else if (queryResults.containsTaxon(species)) {
        // Found an earlier species in the query results.  Done!
        return true;        
      }
    }
    
    throw new AssertionError("Never found species in its own parent");
  }

  @Override
  protected String convertResolvedToString(Resolved taxon) {
    if (taxon.getType() == SightingTaxon.Type.SINGLE) {
      return convertTaxonToString(taxon.getTaxon());
    } else {
      String name = taxon.getPreferredSingleName();
      if (queryResults != null) {
        // Find the "best" sighting, when the sort type is default, and append information
        // on it.  (In non-default sorts, there isn't really a "best" sighting)
        if (sortType == SortType.DEFAULT) {
          Sighting sighting = queryResults.getBestSighting(taxon, sortType.ordering());
          if (sighting != null) {
            name = SightingFlags.appendSightingInfo(name, sighting);
          }
        }
      }
      return name;
    }
  }


  public void setQueryResults(QueryResults queryResults, boolean showVisits) {
    setModel(new ReportsTreeModel(queryResults, showVisits));
    this.queryResults = queryResults;
  }

  @Override
  public void setModel(TreeModel newModel) {
    super.setModel(newModel);
    // Clear any pre-existing query results  
    this.queryResults = null;
  }

  @Override
  public void setTaxonomy(Taxonomy taxonomy) {
    saveCurrentPreview();
    
    super.setTaxonomy(taxonomy);
    this.queryResults = null;
  }
  
  public void setShowFamilyTotals(boolean showFamilyTotals) {
    this.showFamilyTotals = showFamilyTotals;
  }

  public void setShowSpeciesStatus(boolean showSpeciesStatus) {
    this.showSpeciesStatus = showSpeciesStatus;
  }

  @Override
  protected void doMove(SightingsGroup sightings) {
    // TODO: test dragging sightings from here into the parent.  Will likely
    // be broken, since those new results *won't* be inserted into the query
    // results.  On the other hand, this might be desirable - otherwise, the user
    // wouldn't see that the move did anything.g
    ReportsTreeModel model = (ReportsTreeModel) getModel();
    
    // If moving this sighting means there's no remaining sightings of the species,
    // (or maybe no sightings of any species in the family), then the total count
    // will change.  Precompute some state before we update the query results and
    // report set.
    TreePath path = sightings.getParentPath();
    TreePath parentPath = path.getParentPath();
    int oldChildCount = model.getChildCount(parentPath.getLastPathComponent());
    int oldIndexOfChild = model.getIndexOfChild(
            path.getParentPath().getLastPathComponent(), path.getLastPathComponent());
    
    // Otherwise, time to remove the sightings and update the model
    getReportSet().mutator().removing(sightings.getSightings()).mutate();
    queryResults.removeSightings(sightings.getSightings());
    
    model.invalidateState();
    // Now, see what happened to the list
    int newChildCount = model.getChildCount(parentPath.getLastPathComponent());
    
    // Same number of children (or, though it should not happen, more);  just report
    // the sightings are gone.
    if (newChildCount >= oldChildCount) {
      if (newChildCount > oldChildCount) {
        logger.log(Level.WARNING, "Child count increased!  Was {0}, now {1}",
                new Object[]{oldChildCount, newChildCount});
      }
      model.sightingsRemoved(
          path,
          sightings.getSightings(), sightings.getOriginalIndices());
    } else if (newChildCount == oldChildCount - 2) {
      // Two children are gone;  that'll be the family heading and the species.  Report both.
      model.taxaRemoved(
          parentPath,
          // Use "" for the family.  In fact, nothing really uses the values here
          ImmutableList.of("", path.getLastPathComponent()),
          ImmutableList.of(oldIndexOfChild - 1, oldIndexOfChild));
    } else {
      // One children are gone;  that's just the species
      model.taxaRemoved(
          parentPath,
          ImmutableList.of(path.getLastPathComponent()),
          ImmutableList.of(oldIndexOfChild));
    }
  }
  
  public void addSighting(TreePath path, Sighting sighting, boolean updateTaxaInQueryResults) {
    getReportSet().mutator().adding(ImmutableList.of(sighting)).mutate();
    ReportsTreeModel model = (ReportsTreeModel) getModel();
    model.sightingAdded(path, sighting, updateTaxaInQueryResults);
  }

  public void addSightings(TreePath path, Collection<Sighting> sightings, boolean updateTaxaInQueryResults) {
    if (sightings.isEmpty()) {
      return;
    }
    getReportSet().mutator().adding(sightings).mutate();
    ReportsTreeModel model = (ReportsTreeModel) getModel();
    model.sightingsAdded(path, sightings, updateTaxaInQueryResults);
  }

  private class ReportsTransferHandler extends TaxonTransferHandler {
    @Override
    public int getSourceActions(JComponent component) {
      if (canImportSightings()) {
        return COPY_OR_MOVE;
      } else {
        return editableSightings ? COPY_OR_MOVE : NONE;
      }
    }
  }

  @Override
  protected boolean canImportSightings() {
    return false;
  }

  @Override
  protected boolean canImportSightingsToLastPathComponent(Object lastPathComponent) {
    if (lastPathComponent instanceof VisitInfoKeyNode) {
      return true;
    }
    
    return super.canImportSightingsToLastPathComponent(lastPathComponent);
  }
  
  public Action getCutAction() {
    return reportsBrowserFocusTracker.cut;
  }

  private class ReportsBrowserFocusTracker extends FocusTracker {
    private final Action cut = new CutActionWrapper(TransferHandler.getCutAction());
    private final Action copy = new CopyActionWrapper(TransferHandler.getCopyAction());
    private final Action paste = new PasteActionWrapper(TransferHandler.getPasteAction());
    
    public ReportsBrowserFocusTracker() {
      super(ReportsBrowserPanel.this);
    }

    @Override protected void focusGained(Component child) {
      if (editableSightings) {
        actionBroker.publishAction("cut", cut);
        actionBroker.publishAction("copy", copy);
        actionBroker.publishAction("paste", paste);
      }
    }

    @Override protected void focusLost(Component child) {
      if (child instanceof JTextComponent) {
        return;
      }
      
      if (editableSightings) {
        actionBroker.unpublishAction("cut", cut);
        actionBroker.unpublishAction("copy", copy);
        actionBroker.unpublishAction("paste", paste);
      }
    }
  }

  @Override
  protected void sightingsReplaced(
      TreePath parent,
      List<Sighting> oldSightings,
      List<Sighting> newSightings,
      int[] oldIndices) {
    ((ReportsTreeModel) getModel()).sightingsReplaced(
            parent, oldSightings, newSightings, oldIndices);    
  }

  @Override
  protected TreePath pathToSighting(TreePath oldParentPath, Sighting sighting) {
    TreeModel model = getModel();
    if (oldParentPath.getLastPathComponent() instanceof VisitInfoKeyNode) {
      VisitInfoKey visitInfoKey = VisitInfoKey.forSighting(sighting);
      if (visitInfoKey != null) {
        // Regenerate the path to the visit info key node, which might go through the root or might be
        // underneath a VisitNode
        VisitInfoKeyNode node = new VisitInfoKeyNode(visitInfoKey);
        TreePath pathToNode;
        if (model.getIndexOfChild(model.getRoot(), node) < 0) {
          pathToNode = new TreePath(new Object[] {model.getRoot(), new ReportsTreeModel.VisitNode(0), node});
        } else {
          pathToNode = new TreePath(new Object[] {model.getRoot(), node});
        }
            
        Resolved resolved = sighting.getTaxon().resolve(getTaxonomy());
        return pathToNode.pathByAddingChild(
            new ResolvedWithSighting(resolved, sighting));
      }
      // otherwise fall through and select the resolved directly.
    }
    
    Resolved resolved = sighting.getTaxon().resolve(getTaxonomy());
    // Because the browser shows taxa clipped to a specific depth, re-resolve up to
    // that depth.
    resolved = resolved.getParentOfAtLeastType(depth).resolveInternal(getTaxonomy());
    return new TreePath(
        new Object[]{ getModel().getRoot(), resolved, sighting});
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    super.fontsUpdated(fontManager);
    setFixedCellWidth(fontManager.scale(300));
  }

  public void setSeenTaxa(ScanSeenTaxa scanSeenTaxa) {
    this.scanSeenTaxa = scanSeenTaxa;
  }  

  private Checklist getChecklist() {
    if (queryResults != null) {
      return queryResults.getChecklist(getTaxonomy());
    }
    
    return null;
  }

  public void setSortType(SortType sortType) {
    this.sortType = sortType;
  }

  @Override
  protected boolean allowLocationEditing() {
    return allowLocationEditing;
  }
  
  public void setLocationEditingAllowed(boolean allowLocationEditing) {
    this.allowLocationEditing = allowLocationEditing;
    
  }

  @Override
  protected VisitInfoPreferences getVisitInfoPreferences() {
    return visitInfoPreferences;
  }

  public interface VisitInfoKeyChangedListener extends EventListener {
    void newVisitInfoKey(VisitInfoKey visitInfoKey);
    void deletedVisitInfoKey(VisitInfoKey visitInfoKey);
  }
  
  public void addVisitInfoKeyChangedListener(VisitInfoKeyChangedListener listener) {
    listenerList.add(VisitInfoKeyChangedListener.class, listener);
  }
  
  /**
   * Notify the model of the updated visit info key, and then reselect the updated 
   * visit info.
   */
  @Override
  protected void replaceVisitInfoKey(VisitInfoKey updatedVisitInfoKey) {
    // Just notify a listener, who'll have to requery the world.
    // An earlier version tried to be clever, but wasn't clever enough to handle boundary
    // conditions like dealing with visits that are now merged, and was never going to
    // be clever enough to support locations changing.
    for (VisitInfoKeyChangedListener listener : getListeners(VisitInfoKeyChangedListener.class)) {
      listener.newVisitInfoKey(updatedVisitInfoKey);
    }
  }
  
  @Override
  protected void visitInfoKeyDeleted(VisitInfoKey deletedVisitInfo) {
    ((ReportsTreeModel) getModel()).removeVisitInfo(deletedVisitInfo);
    for (VisitInfoKeyChangedListener listener : getListeners(VisitInfoKeyChangedListener.class)) {
      listener.deletedVisitInfoKey(deletedVisitInfo);
    }
  }

  @Override
  protected int getSightingIndex(Sighting sighting, TreePath parentPath) {
    return ((ReportsTreeModel) getModel()).getIndexOfSighting(parentPath.getLastPathComponent(), sighting);
  }

  public void invalidateSightings() {
    ((ReportsTreeModel) getModel()).invalidateState();
    TreePath selectionPath = getSelectionPath();
    if (selectionPath != null &&
        (selectionPath.getLastPathComponent() instanceof Sighting
        || selectionPath.getLastPathComponent() instanceof VisitInfoKeyNode)) {
      setSelectionPath(selectionPath.getParentPath());
    }
  }

  /** Select a visit info, whether it's at the root or underneath the Visits header. */
  public void selectVisitInfo(VisitInfoKey visitInfoKey) {
    Object root = getModel().getRoot();
    VisitInfoKeyNode visitInfoKeyNode = new ReportsTreeModel.VisitInfoKeyNode(visitInfoKey);
    int visitInfoIndex = getModel().getIndexOfChild(root, visitInfoKeyNode);
    if (visitInfoIndex >= 0) {
      setSelectionPath(new TreePath(new Object[]{root, visitInfoKeyNode, visitInfoKey}));
    } else {
      // VisitNode has an "equals" implementation that doesn't actually care about the visit count,
      // which lets us locate the actual visit node. (HACK!)
      VisitNode dummyVisitNode = new ReportsTreeModel.VisitNode(0);
      int visitNodeIndex = getModel().getIndexOfChild(root, dummyVisitNode);
      if (visitNodeIndex >= 0) {
        Object actualVisitNode = getModel().getChild(root, visitNodeIndex);
        visitInfoIndex = getModel().getIndexOfChild(actualVisitNode, visitInfoKeyNode);
        if (visitInfoIndex >= 0) {
          TreePath treePath = new TreePath(new Object[]{root, actualVisitNode, visitInfoKeyNode, visitInfoKey});
          setSelectionPath(treePath);
          scrollRectToVisible(getPathBounds(treePath));
        }
      }
    }
  }

  /**
   * Returns the VisitInfoKey for this tree path, if it includes one.
   */
  public VisitInfoKey getVisitInfoKeyForSightingImport(TreePath treePath) {
    Object lastPathComponent = treePath.getLastPathComponent();
    if (lastPathComponent instanceof VisitInfoKeyNode) {
      return ((VisitInfoKeyNode) lastPathComponent).getVisitInfoKey();
    }
    if (lastPathComponent instanceof ResolvedWithSighting) {
      return VisitInfoKey.forSighting(((ResolvedWithSighting) lastPathComponent).getSighting());
    }
    
    return null;
  }
    
  
  /**
   * Selects a taxon, whether it's stored as a Taxon or a Resolved.
   */
  public void selectTaxon(Taxon taxon) {
    if (getModel() == null) {
      return;
    }

    Object root = getModel().getRoot();
    TreePath treePath = null;
    if (getModel().getIndexOfChild(root, taxon) >= 0) {
      treePath = new TreePath(new Object[]{root, taxon});
    } else {
      Resolved resolved = SightingTaxons.newResolved(taxon);
      if (getModel().getIndexOfChild(root, resolved) >= 0) {
        treePath = new TreePath(new Object[]{root, resolved});
      }
    }
    
    if (treePath != null) {
      TreePath selectedTreePath = treePath;
      setSelectionPath(selectedTreePath);
      scrollListSelectionsIntoView();
    }
  }
  
  /**
   * Returns true if the given model node object is the parent of a list of Visits. 
   */
  public boolean isVisitsParentNode(Object node) {
    if (!(node instanceof ReportsTreeModel.VisitNode)) {
      return false;
    }
    
    return !getModel().isLeaf(node);
  }

  @Override
  protected String getReturnPanelName() { 
    return returnPanelName;
  }

  public void setReturnPanelName(String returnPanelName) {
    this.returnPanelName = returnPanelName;
  }

  static class ReturnToSighting implements PostNavigateAction {
    private final Sighting sighting;

    public ReturnToSighting(Sighting sighting) {
      this.sighting = sighting;
    }

    @Override
    public void postNavigate(JPanel panel) {
      ReportsBrowserPanel reportsBrowserPanel =
          UIUtils.findChildOfType(panel, ReportsBrowserPanel.class);
      if (reportsBrowserPanel != null) {      
        Taxonomy taxonomy = reportsBrowserPanel.getTaxonomy();
        if (TaxonUtils.areCompatible(sighting.getTaxonomy(), taxonomy)) {
          Taxon taxon = sighting.getTaxon().resolve(taxonomy).getTaxa().iterator().next();
          Taxon finalTaxon = TaxonUtils.getParentOfAtLeastType(taxon, reportsBrowserPanel.depth);
          // Invoke later - effectively delaying until after the query has run
          EventQueue.invokeLater(() -> {
            // Select the taxon, if possible
            reportsBrowserPanel.selectTaxon(finalTaxon);
            reportsBrowserPanel.requestFocusInWindow();
            // Now try to select the original sighting, if it exists.
            VisitInfoKey visitKey = VisitInfoKey.forSighting(sighting);
            if (visitKey != null) {
              TreePath path = reportsBrowserPanel.getSelectionPath();
              if (path != null) {
                int childCount = reportsBrowserPanel.getModel().getChildCount(path.getLastPathComponent());
                for (int i = 0; i < childCount; i++) {
                  Object child = reportsBrowserPanel.getModel().getChild(
                      path.getLastPathComponent(), i);
                  if (child instanceof Sighting childSighting) {
                    // Same visit key == effectively, same sighting.
                    if (visitKey.equals(VisitInfoKey.forSighting(childSighting))) {
                      reportsBrowserPanel.setSelectionPath(
                          path.pathByAddingChild(childSighting));
                      reportsBrowserPanel.scrollListSelectionsIntoView();
                      reportsBrowserPanel.requestFocusInWindow();
                      return;
                    }
                  }
                }
              }
            }
          });
        }
        return;
      }
    }
    
  }
  
  @Override
  protected PostNavigateAction returnToSighting(Sighting sighting) {
    return new ReturnToSighting(sighting);
  }
}
