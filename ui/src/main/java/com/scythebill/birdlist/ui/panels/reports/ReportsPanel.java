/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.tree.TreePath;

import com.google.common.base.MoreObjects;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.query.QueryDefinition;
import com.scythebill.birdlist.model.query.QueryDefinition.QueryAnnotation;
import com.scythebill.birdlist.model.query.QueryProcessor;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.actions.ActionBroker;
import com.scythebill.birdlist.ui.app.Titled;
import com.scythebill.birdlist.ui.components.DepthChooser;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.components.SightingBulkEditDialog;
import com.scythebill.birdlist.ui.components.SpeciesIndexerPanelConfigurer;
import com.scythebill.birdlist.ui.components.SightingBulkEditPanel.UpdatedListener;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.events.TaxonomyChangedEvent;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.imports.FinishedImport;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;
import com.scythebill.birdlist.ui.panels.reports.StoredQueries.RestoredQuery;
import com.scythebill.birdlist.ui.util.VisibilityDetector;

/**
 * Panel for displaying reports, etc.
 * 
 * TODOs:
 * - Round out set of queryable fields
 * - Support queries for "none of/all of" sightings
 * - Maybe: support Jump To a species in the queried results?
 */
public class ReportsPanel extends JPanel implements Titled, FontsUpdatedListener {
  private QueryResults queryResults;
  private QueryPanel queryPanel;
  private final TaxonomyStore taxonomyStore;
  private final ReportSet reportSet;
  private JButton returnToMainButton;
  private JLabel totalLabel;
  private final ReportsBrowserPanel reportsBrowserPanel;
  private DepthChooser depthChooser;
  private final FontManager fontManager;
  private final QueryExecutor queryExecutor = new Executor();
  
  class Executor implements QueryExecutor {
    private boolean onlyCountable;
    private boolean includeIncompatibleSightings;
 
    @Override public QueryResults executeQuery(Type depth, Taxonomy taxonomyOverride) {
      return ReportsPanel.this.executeQuery(depth, taxonomyOverride, onlyCountable, includeIncompatibleSightings);
    }

    @Override
    public Location getRootLocation() {
      return queryPanel.getRootLocation();
    }

    @Override public Set<ReportHints> getReportHints() {
      return ReportsPanel.this.getReportHints();
    }

    @Override
    public Optional<String> getReportAbbreviation() {
      return queryPanel.getQueryAbbreviation();
    }

    @Override
    public Optional<String> getReportName() {
      return queryPanel.getQueryName();
    }

    @Override
    public QueryExecutor onlyCountable() {
      Executor executor = new Executor();
      executor.includeIncompatibleSightings = this.includeIncompatibleSightings;
      executor.onlyCountable = true;
      return executor;
    }

    @Override
    public QueryExecutor includeIncompatibleSightings() {
      Executor executor = new Executor();
      executor.includeIncompatibleSightings = true;
      executor.onlyCountable = this.onlyCountable;
      return executor;
    }
  }
  
  private final ReportsActionFactory reportsActionFactory;
  private final ActionBroker actionBroker;
  private final SightingBulkEditDialog sightingBulkEditDialog;
  private final Checklists checklists;
  private final StoredQueries storedQueries;
  private final QueryPreferences queryPreferences;
  private final SpeciesIndexerPanelConfigurer speciesIndexerPanelConfigurer;

  private Action printAction;
  private JComboBox<ExportOption> exportComboBox;
  private JButton updateReport;
  private JButton editButton;
  private JButton printButton;
  private boolean isSimplifiedForResolution;
  private JLabel depthLabel;
  private JButton rememberButton;
  private QueryDefinition currentQueryDefinition;
  private Action finishedReport;
  private JButton speciesJumpButton;
  private IndexerPanel<String> speciesIndexPanel;
  private QueryPanelFactory queryPanelFactory;

  @Inject
  public ReportsPanel(
      @FinishedImport Action finishedReport,
      TaxonomyStore taxonomyStore,
      ReportSet reportSet,
      QueryPanelFactory queryPanelFactory,
      ReportsBrowserPanel reportsBrowserPanel,
      FontManager fontManager,
      ReportsActionFactory reportsActionFactory,
      ActionBroker actionBroker,
      EventBusRegistrar eventBusRegistrar,
      SightingBulkEditDialog sightingBulkEditDialog,
      SpeciesIndexerPanelConfigurer speciesIndexerPanelConfigurer,
      Checklists checklists,
      StoredQueries storedQueries,
      QueryPreferences queryPreferences,
      VisibilityDetector visibilityDetector) {
    this.finishedReport = finishedReport;
    this.taxonomyStore = taxonomyStore;
    this.reportSet = reportSet;
    this.queryPanelFactory = queryPanelFactory;
    this.reportsBrowserPanel = reportsBrowserPanel;
    this.fontManager = fontManager;
    this.reportsActionFactory = reportsActionFactory;
    this.actionBroker = actionBroker;
    this.sightingBulkEditDialog = sightingBulkEditDialog;
    this.speciesIndexerPanelConfigurer = speciesIndexerPanelConfigurer;
    this.checklists = checklists;
    this.storedQueries = storedQueries;
    this.queryPreferences = queryPreferences;
    eventBusRegistrar.registerWhenInHierarchy(this);
    initGUI();
    
    // Restore a query (and do so *before* attaching listeners, so we don't run each intermediate query!)
    if (storedQueries.getDefaultQuery() != null) {
      restoreQuery(storedQueries.getDefaultQuery());
    }
    
    attachListeners();
    visibilityDetector.install(returnToMainButton);

    // Show the initial query results
    EventQueue.invokeLater(() -> showQueryInList());
  }
  
  public void restoreQuery(RestoredQuery restored) {
    queryPanel.restore(restored);
  }
                              
  public void restoreQuery(String json) {
    queryPanel.restore(json);
  }
                              
  @Override
  public String getTitle() {
    return Messages.getMessage(Name.SHOW_REPORTS);
  }

  protected void simplifyForResolution(Taxon.Type depth) {
    // Hide all the options that aren't necessary when simply resolving taxa,
    // or are just confusing.
    queryPanel.setVisible(false);
    exportComboBox.setVisible(false);
    rememberButton.setVisible(false);
    printButton.setVisible(false);
    editButton.setVisible(false);
    totalLabel.setVisible(false);
    depthChooser.setDepth(depth);
    depthChooser.setVisible(false);
    depthLabel.setVisible(false);
    speciesIndexPanel.setVisible(false);
    speciesJumpButton.setVisible(false);
    isSimplifiedForResolution = true;
  }
  
  protected Taxonomy getCurrentTaxonomy() {
    return taxonomyStore.getTaxonomy();
  }
  
  enum ExportOption {
    EXPORT(Name.EXPORT_MENU),
    EBIRD(Name.EXPORT_TO_EBIRD),
    EXCEL(Name.EXPORT_TO_SPREADSHEET),
    SCYTHEBILL(Name.EXPORT_TO_SCYTHEBILL),
    TRIP_REPORT(Name.EXPORT_TO_TRIP_REPORT),
    FAMILY_REPORT(Name.EXPORT_TO_FAMILY_REPORT),
    INATURALIST(Name.EXPORT_TO_INATURALIST),
    BIRDTRACK(Name.EXPORT_TO_BIRDTRACK);
    
    private final Name name;
    ExportOption(Name name) {
      this.name = name;
    }
    
    @Override public String toString() {
      return Messages.getMessage(name); 
    }
  }
  
  private void initGUI() {
    queryPanel = queryPanelFactory.newQueryPanel();

    totalLabel = new JLabel();
    
    JComponent helpLabel = getHelpLabel();
    
    GroupLayout groupLayout = new GroupLayout(this);
    groupLayout.setAutoCreateContainerGaps(true);
    setLayout(groupLayout);
    
    JScrollPane browserScrollPane = new JScrollPane();
    browserScrollPane.setViewportView(reportsBrowserPanel);
    reportsBrowserPanel.setDragEnabled(false);
    reportsBrowserPanel.setEditableSightings(true);
    reportsBrowserPanel.setEditableWhere(true);
    reportsBrowserPanel.setTaxonomy(taxonomyStore.getTaxonomy());
    
    depthLabel = new JLabel(Messages.getMessage(Name.SHOW_LABEL));
    depthChooser = new DepthChooser();
    // Update the ReportsBrowserPanel to choose a good default depth
    reportsBrowserPanel.setDepth(depthChooser.getDepth());
    
    updateReport = new JButton();
    updateReport.setText(Messages.getMessage(Name.UPDATE_REPORT));
    updateReport.setVisible(false);
    updateReport.addActionListener(e -> showQueryInList());
        
    speciesJumpButton = new JButton(Messages.getMessage(Name.JUMP_TO_BUTTON));
    speciesJumpButton.putClientProperty("JComponent.sizeVariant", "small");
    speciesIndexPanel = new IndexerPanel<String>();
    speciesIndexPanel.addPropertyChangeListener("value",
        e -> speciesJumpButton.setEnabled(e.getNewValue() != null));
    speciesIndexPanel.addActionListener(e -> speciesJumpButton.doClick(100));
    speciesIndexPanel.setEnabled(false);
    speciesJumpButton.setEnabled(false);
    speciesJumpButton.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent e) {
        String id = speciesIndexPanel.getValue();
        if (id != null) {
          Taxon taxon = taxonomyStore.getTaxonomy().getTaxon(id);
          if (taxon != null) {
            reportsBrowserPanel.selectTaxon(taxon);
            reportsBrowserPanel.requestFocusInWindow();
          }
        }
      }      
    });
    
    
    returnToMainButton = new JButton();
    returnToMainButton.setText(Messages.getMessage(Name.RETURN_TO_MAIN_MENU));

    exportComboBox = new JComboBox<ExportOption>();
    exportComboBox.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent event) {
        ExportOption exportOption = (ExportOption) exportComboBox.getSelectedItem();
        if (exportOption != ExportOption.EXPORT) {
          exportComboBox.setSelectedItem(ExportOption.EXPORT);
        }
        
        switch (exportOption) {
          case EXPORT:
            return;
          case EBIRD:
            reportsActionFactory.ebirdExport(queryExecutor).actionPerformed(event);
            break;
          case EXCEL:
            reportsActionFactory.saveAsXls(queryExecutor).actionPerformed(event);
            break;
          case INATURALIST:
            reportsActionFactory.iNaturalistExport(queryExecutor).actionPerformed(event);
            break;
          case BIRDTRACK:
            reportsActionFactory.birdTrackExport(queryExecutor).actionPerformed(event);
            break;
          case SCYTHEBILL:
            reportsActionFactory.saveAsScythebill(queryExecutor).actionPerformed(event);
            break;
          case TRIP_REPORT:
            reportsActionFactory.saveAsTripReport(queryExecutor).actionPerformed(event);
            break;
          case FAMILY_REPORT:
            reportsActionFactory.saveAsFamilyReport(queryExecutor).actionPerformed(event);
            break;
        }
      }
    });
    updateExportCombobox();
    updateSpeciesIndexer(taxonomyStore.getTaxonomy());
    
    printButton = new JButton(reportsActionFactory.print(queryExecutor));
    printButton.setText(Messages.getMessage(Name.PRINT_MENU));
    
    editButton = new JButton(Messages.getMessage(Name.BULK_EDIT));
    editButton.addActionListener(e -> showBulkEdit());

    rememberButton = new JButton(Messages.getMessage(Name.REMEMBER_MENU));
    rememberButton.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent event) {
        String name = getName();
        if (name != null) {
          String persisted = queryPanel.persist();
          storedQueries.addReportsQuery(name, persisted);
        }
      }
      
      /**
       * Find the name for the report.  Loops until a valid name is
       * returned (or the user hits cancel).
       */
      private String getName() {
        String defaultName = Messages.getMessage(Name.REPORT_DEFAULT_NAME);
        Optional<String> betterName = queryPanel.getQueryName();
        if (betterName.isPresent()) {
          defaultName = betterName.get();
        }
        
        return storedQueries.chooseName(ReportsPanel.this, defaultName);
      }
    });

    groupLayout.setVerticalGroup(groupLayout.createSequentialGroup()
        .addComponent(helpLabel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
        .addGroup(groupLayout.createBaselineGroup(false, false)
            .addComponent(depthLabel)
            .addComponent(depthChooser)
            .addComponent(updateReport)
            .addComponent(speciesIndexPanel)
            .addComponent(speciesJumpButton))
        .addPreferredGap(ComponentPlacement.RELATED)
        .addComponent(queryPanel)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addComponent(totalLabel)
        .addComponent(browserScrollPane)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addGroup(groupLayout.createBaselineGroup(false, false)
            .addComponent(exportComboBox)
            .addComponent(printButton)
            .addComponent(editButton)
            .addComponent(rememberButton)
            .addComponent(returnToMainButton)));
    
    groupLayout.setHorizontalGroup(groupLayout.createParallelGroup()
        .addComponent(helpLabel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
        .addGroup(groupLayout.createSequentialGroup()
            .addComponent(depthLabel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(depthChooser, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addPreferredGap(ComponentPlacement.UNRELATED)
            .addComponent(speciesIndexPanel)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(speciesJumpButton)
            .addPreferredGap(ComponentPlacement.UNRELATED)
            .addComponent(updateReport))
        .addComponent(queryPanel)
        .addComponent(totalLabel)
        .addComponent(browserScrollPane)
        .addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
            .addComponent(exportComboBox, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addComponent(printButton, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addComponent(editButton, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addComponent(rememberButton, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
         .addComponent(returnToMainButton, Alignment.TRAILING, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE));

    fontManager.applyTo(this);    
  }

  /** Return a help label for the reports panel. By default, empty, but can be overidden to add help text. */
  protected JComponent getHelpLabel() {
    JPanel panel = new JPanel();
    panel.setVisible(false);
    return panel;
  }

  private void attachListeners() {
    printAction = reportsActionFactory.print(queryExecutor);
    
    addAncestorListener(new AncestorListener() {
      @Override public void ancestorRemoved(AncestorEvent e) {
        actionBroker.unpublishAction(ActionBroker.PRINT, printAction);
      }
      
      @Override public void ancestorMoved(AncestorEvent e) {
      }
      
      @Override public void ancestorAdded(AncestorEvent e) {
        actionBroker.publishAction(ActionBroker.PRINT, printAction);
      }
    });
    
    queryPanel.addPredicateChangedListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent event) {
        if (storedQueries != null) {
          storedQueries.setDefaultQuery(queryPanel.persist());
        }
        showQueryInList();
      }
    });
    
    returnToMainButton.addActionListener(finishedReport);
    
    depthChooser.addPropertyChangeListener("depth", new PropertyChangeListener() {
      @Override public void propertyChange(PropertyChangeEvent evt) {
        showQueryInList();
        reportsBrowserPanel.setDepth(depthChooser.getDepth());
      }      
    });
  }

  /**
   * Show the bulk editing dialog (and update sightings afterwards if editing happens).
   */
  private void showBulkEdit() {
    UpdatedListener onSaved = new UpdatedListener() {
      @Override public void sightingsUpdated(List<Sighting> sightings) {
        queryResults.updateSightings(sightings, sightings, true /* updateTaxa */);
        reportsBrowserPanel.invalidateSightings();
      }
      
      @Override public void sightingsSwapped(List<Sighting> oldSightings, List<Sighting> newSightings) {
        queryResults.updateSightings(oldSightings, newSightings, true /* updateTaxa */);
        reportsBrowserPanel.invalidateSightings();
      }

      @Override
      public void sightingsRemoved(List<Sighting> sightings) {
        // Clear the selection... which resolves
        // https://bitbucket.org/scythebill/scythebill-birdlist/issues/262/indexoutofboundsexception-during-bulk
        // ... in a somewhat brute-force way, but "sightings removed" basically implies that everything is getting
        // deleted so why care about the selection?
        reportsBrowserPanel.clearSelection();
        queryResults.removeSightings(sightings);
        reportsBrowserPanel.invalidateSightings();
        showQueryInList();
      }
    };
    sightingBulkEditDialog.bulkEditSightings(
        SwingUtilities.getWindowAncestor(this),
        onSaved,
        taxonomyStore.getTaxonomy(),
        reportSet,
        ImmutableList.copyOf(queryResults.getAllSightings()));
  }
  
  /**
   * After a change, execute a query and show the results.
   */
  private void showQueryInList() {
    TreePath originalSelectionPath = reportsBrowserPanel.getSelectionPath();
    final Resolved selectedTaxon;
    if (originalSelectionPath != null && originalSelectionPath.getPathCount() >= 2) {
      selectedTaxon = (Resolved) originalSelectionPath.getPathComponent(1);
    } else {
      selectedTaxon = null;
    }
    
    // Only show family totals if there's no location restriction
    Set<ReportHints> reportHints = getReportHints();
    reportsBrowserPanel.setShowSpeciesStatus(
        reportHints.contains(ReportHints.SPECIES_STATUS_RESTRICTION));
    reportsBrowserPanel.setLocationRoot(queryPanel.getRootLocation());
    
    queryResults = executeQuery(null, null, false /* not only countable */, false /* not including incompatible sightings */);
    queryResults.addListener(new QueryResults.Listener() {
      @Override public void resultsChanged() {
        queryResultsUpdated(queryResults);
      }
    });
    
    queryResultsUpdated(queryResults);
    
    reportsBrowserPanel.setQueryResults(queryResults, /*showVisits=*/false);

    if (selectedTaxon != null
        && selectedTaxon.getTaxonomy() == taxonomyStore.getTaxonomy()
        && !queryResults.getAllSightings(selectedTaxon).isEmpty()) {
      TreePath treePath = new TreePath(new Object[]{reportsBrowserPanel.getModel().getRoot(), selectedTaxon});
      reportsBrowserPanel.setSelectionPath(treePath);
      reportsBrowserPanel.ensurePathIsVisible(treePath);
    }
 
    List<Resolved> queriedTaxa = queryResults.getTaxaAsList();
    if (queriedTaxa.isEmpty()) {
      speciesIndexPanel.setEnabled(false);
    } else {
      Set<String> taxa = new LinkedHashSet<>();
      
      for (Resolved resolved : queriedTaxa) {
        if (resolved.getType() == SightingTaxon.Type.SINGLE) {
          Taxon taxon = resolved.getTaxon();
          taxa.add(taxon.getId());
        }
      }
      
      // Filter the species index panel down to just "contains"
      // It *would* be more efficient to have a species indexer containing just these
      // species, rather than filtering down... but OTOH there's a lot of code to
      // assemble the various layers of indexing with alternates.  This is waaaay simpler.
      speciesIndexPanel.setFilter(taxa::contains);
      speciesIndexPanel.setEnabled(true);
    }    
  }

  private void queryResultsUpdated(QueryResults updatedQueryResults) {
    updateReport.setVisible(updatedQueryResults.isQueryInvalid());
    Taxonomy taxonomy = taxonomyStore.getTaxonomy();
    int speciesCount = updatedQueryResults.getCountableSpeciesSize(
        taxonomy, false);
    String total = Messages.getFormattedMessage(Name.TOTAL_SPECIES_FORMAT_NO_CHECKLIST, speciesCount);
    if (depthChooser.getDepth() != Taxon.Type.species) {
      String otherTotals = "";
      int groupsAndSpeciesCount = updatedQueryResults.getCountableGroupsAndSpeciesSize(taxonomy, false);
      if (groupsAndSpeciesCount != speciesCount) {
        otherTotals += Messages.getFormattedMessage(Name.GROUPS_FORMAT, groupsAndSpeciesCount);
      }
        
      if (depthChooser.getDepth() == Taxon.Type.subspecies) {
        int subspeciesCount = updatedQueryResults.getCountableSubspeciesGroupsAndSpeciesSize(taxonomy, false);
        if (subspeciesCount != groupsAndSpeciesCount) {
          if (!otherTotals.isEmpty()) {
            otherTotals += ", ";
          }
          otherTotals += Messages.getFormattedMessage(Name.SSPS_FORMAT, subspeciesCount);
        }
      }
      
      int spuhCount = queryResults.getSpuhCount();
      if (spuhCount > 0) {
        if (!otherTotals.isEmpty()) {
          otherTotals += ", ";
        }
        otherTotals += Messages.getFormattedMessage(Name.SPUHS_FORMAT, spuhCount);
      }
      
      if (!otherTotals.isEmpty()) {
        total += " (" + otherTotals + ")";
      }
    } else {
      int spuhCount = queryResults.getSpuhCount();
      if (spuhCount > 0) {
        total += " (" + Messages.getFormattedMessage(Name.SPUHS_FORMAT, spuhCount) + ")";
      }
    }

    if (queryPanel.containsQueryFieldTypeThatIsNotANoOp(QueryFieldType.FIRST_SIGHTINGS)) {
      Set<?> lifers = updatedQueryResults.getTaxaWithAnnotation(taxonomy, QueryAnnotation.LIFER, Taxon.Type.species);
      if (!lifers.isEmpty()) {
        total += "   " + Messages.getFormattedMessage(Name.LIFERS_FORMAT_2, lifers.size());
      }

      if (depthChooser.getDepth() != Taxon.Type.species) {
        String otherTotals = "";
        int groupsAndSpeciesCount = updatedQueryResults
            .getTaxaWithAnnotation(taxonomy, QueryAnnotation.LIFER, Taxon.Type.group)
            .size();
        if (groupsAndSpeciesCount != lifers.size()) {
          otherTotals += Messages.getFormattedMessage(Name.GROUPS_FORMAT, groupsAndSpeciesCount);
        }
          
        if (depthChooser.getDepth() == Taxon.Type.subspecies) {
          int subspeciesCount = updatedQueryResults
              .getTaxaWithAnnotation(taxonomy, QueryAnnotation.LIFER, Taxon.Type.subspecies)
              .size();
          if (subspeciesCount != groupsAndSpeciesCount) {
            if (!otherTotals.isEmpty()) {
              otherTotals += ", ";
            }
            otherTotals += Messages.getFormattedMessage(Name.SSPS_FORMAT, subspeciesCount);
          }
        }
        
        if (!otherTotals.isEmpty()) {
          total += " (" + otherTotals + ")";
        }
      }
    }
    
    total += ", " + Messages.getFormattedMessage(
        Name.FAMILIES_FORMAT, updatedQueryResults.getFamilyCount(taxonomyStore.getTaxonomy()));
    totalLabel.setText(total);
    
    int sightingsCount = updatedQueryResults.getSightingsCount(taxonomy);
    // Bulk editing is only available for >1 sighting
    editButton.setEnabled(sightingsCount > 1);
    // And the others require at least one sighting
    printButton.setEnabled(sightingsCount > 0);
    exportComboBox.setEnabled(sightingsCount > 0);
  }

  private Set<ReportHints> getReportHints() {
    EnumSet<ReportHints> set = EnumSet.noneOf(ReportHints.class);
    if (queryPanel.containsQueryFieldTypeThatIsNotANoOp(QueryFieldType.LOCATION)) {
      set.add(ReportHints.LOCATION_RESTRICTION);
    }
    if (queryPanel.containsQueryFieldTypeThatIsNotANoOp(QueryFieldType.SPECIES_STATUS)) {
      set.add(ReportHints.SPECIES_STATUS_RESTRICTION);
    }
    if (queryPanel.containsQueryFieldTypeThatIsNotANoOp(QueryFieldType.DATE)) {
      set.add(ReportHints.DATE_RESTRICTION);
    }
    return set;
  }

  @Subscribe
  public void taxonomyChanged(TaxonomyChangedEvent event) {
    Taxonomy taxonomy = taxonomyStore.getTaxonomy();
    reportsBrowserPanel.setTaxonomy(taxonomy);
    queryPanel.taxonomyUpdated();
    updateExportCombobox();
    showQueryInList();
    
    updateSpeciesIndexer(taxonomy);
  }

  private void updateSpeciesIndexer(Taxonomy taxonomy) {
    speciesIndexerPanelConfigurer.configure(speciesIndexPanel, taxonomy);    
    // Add all the families to the indexer
    speciesIndexerPanelConfigurer.addSimpleTaxa(speciesIndexPanel, taxonomy,
        TaxonUtils.getDescendantsOfType(taxonomy.getRoot(), Taxon.Type.family));
  }

  /** Set the list of export options for a report. */
  private void updateExportCombobox() {
    List<ExportOption> options = Lists.newArrayList(ExportOption.values());
    // eBird is only valid for avian taxonomy
    if (!taxonomyStore.isBirdTaxonomy()) {
      options.remove(ExportOption.EBIRD);
    }
    exportComboBox.setModel(new DefaultComboBoxModel<ExportOption>(options.toArray(new ExportOption[0])));
  }
  
  /** Get the set of locations referred to by the queries. */
  private Set<Location> getLocations() {
    return queryPanel.getLocations();
  }
  
  protected QueryDefinition getQueryDefinition(Type depth) {
    return queryPanel.queryDefinition(taxonomyStore.getTaxonomy(), depth);
  }
  
  /**
   * Executes a query, to a specific depth, either using a specific taxonomy or the 
   * one chosen by the user.
   * <p>
   * Specific taxonomies are used for eBird exports, which must always be Clements.
   */
  private QueryResults executeQuery(@Nullable Type depth, @Nullable Taxonomy taxonomyOverride,
      boolean onlyCountable, boolean includeIncompatibleSightings) {
    // Use the depth chooser if one is not explicitly chosen
    depth = MoreObjects.firstNonNull(depth, depthChooser.getDepth());
    currentQueryDefinition = getQueryDefinition(depth);
    Taxonomy taxonomy = MoreObjects.firstNonNull(taxonomyOverride, taxonomyStore.getTaxonomy());

    Set<Location> locations = getLocations();
    Checklist checklist = null;
    Map<Taxonomy, Checklist> otherTaxonomyChecklists = null;
    if (locations.size() == 1) {
      // TODO: support merging? 
      Location location = Iterables.getOnlyElement(locations);
      if (location != null) {
        checklist = checklists.getChecklist(reportSet, taxonomy, location);
        if (includeIncompatibleSightings) {
          otherTaxonomyChecklists = new LinkedHashMap<>();
          if (!taxonomy.isBuiltIn()) {
            Checklist clementsChecklist = checklists.getChecklist(reportSet, taxonomyStore.getClements(), location);
            if (clementsChecklist != null) {
              otherTaxonomyChecklists.put(taxonomyStore.getClements(), clementsChecklist);
            }
            Checklist iocChecklist = checklists.getChecklist(reportSet, taxonomyStore.getIoc(), location);
            if (iocChecklist != null) {
              otherTaxonomyChecklists.put(taxonomyStore.getIoc(), iocChecklist);
            }
          }
          for (Taxonomy extendedTaxonomy : reportSet.extendedTaxonomies()) {
            Checklist extendedTaxonomyChecklist = checklists.getChecklist(reportSet, extendedTaxonomy, location);
            if (extendedTaxonomyChecklist != null) {
              otherTaxonomyChecklists.put(extendedTaxonomy, extendedTaxonomyChecklist);
            }
          }
        }
      }
    }
    
    Set<ReportHints> reportHints = getReportHints();
    // Skip showing family totals if either:
    //  - the reports panel is being used in a simplified mode
    //  - there's a location restriction for which there isn't a checklist
    boolean dontShowFamilyTotals = 
        isSimplifiedForResolution
        || (reportHints.contains(ReportHints.LOCATION_RESTRICTION) && (checklist == null));
    reportsBrowserPanel.setShowFamilyTotals(!dontShowFamilyTotals);
    
    QueryProcessor processor = new QueryProcessor(reportSet, taxonomy, checklist, otherTaxonomyChecklists);
    // ReportsPanel wants the QueryResults to go to invalid instead of simply deleting all
    // invalid results.
    processor.keepInvalidSightingsAfterUpdate();
    if (isSimplifiedForResolution) {
      processor.dontIncludeFamilyNames();
    }
    if (onlyCountable) {
      processor.onlyIncludeCountableSightings();
    }
    if (includeIncompatibleSightings) {
      processor.includingIncompatibleSightings();
    }
    
    // Find the countable predicate based on the user's query preferences
    Predicate<Sighting> countablePredicate = queryPreferences.getCountablePredicate(
        taxonomy,
        queryPanel.containsQueryFieldType(QueryFieldType.SIGHTING_STATUS),
        queryPanel.getBooleanValueIfPresent(QueryFieldType.ALLOW_HEARD_ONLY));
    return processor.runQuery(currentQueryDefinition, countablePredicate, depth);
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    Dimension preferredSize = fontManager.scale(new Dimension(760, 680));
    preferredSize.width += 200;
    if (reportSet.getUserSet() != null) {
      preferredSize.height += fontManager.scale(60);
    }
    setPreferredSize(preferredSize);
  }
}
