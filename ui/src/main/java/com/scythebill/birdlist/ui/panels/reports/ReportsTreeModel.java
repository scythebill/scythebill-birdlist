/*
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import javax.swing.event.TreeModelEvent;
import javax.swing.tree.TreePath;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Streams;
import com.google.common.primitives.Ints;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.query.SightingComparators;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.sighting.VisitInfoKeyOrdering;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.ui.util.BaseTreeModel;
import com.scythebill.birdlist.ui.util.ResolvedWithSighting;

/**
 * TreeModel providing access to the queried results of a report.
 */
public class ReportsTreeModel extends BaseTreeModel {
  private static final Object ROOT = new Object();
  private final QueryResults queryResults;
  private final LoadingCache<Resolved, List<? extends Object>> sortedSightingsCache;
  private final LoadingCache<VisitInfoKey, List<? extends Object>> sortedVisitInfoKeySightingsCache;
  private ImmutableList<VisitInfoKeyNode> visitInfoKeyNodes;

  private static final int MAX_INLINE_VISIT_INFO_KEYS = 5;

  public static class VisitNode {
    private final int visitCount;

    VisitNode(int visitCount) {
      this.visitCount = visitCount;
    }

    public int getVisitCount() {
      return visitCount;
    }

    @Override
    public boolean equals(Object that) {
      // Treat all VisitNodes as equal, making "indexOf()" trivial for finding the VisitNode
      return that instanceof VisitNode;
    }

    @Override
    public int hashCode() {
      return VisitNode.class.hashCode();
    }
  }

  public static class VisitInfoKeyNode {
    private final VisitInfoKey visitInfoKey;
    
    VisitInfoKeyNode(VisitInfoKey visitInfoKey) {
      this.visitInfoKey = visitInfoKey;
    }
    
    public VisitInfoKey getVisitInfoKey() {
      return visitInfoKey;
    }
    
    @Override
    public boolean equals(Object o) {
      if (o == this) {
        return true;
      }
      
      if (!(o instanceof VisitInfoKeyNode)) {
        return false;
      }
      
      VisitInfoKeyNode that = (VisitInfoKeyNode) o;
      return that.getVisitInfoKey().equals(getVisitInfoKey());
    }
    
    @Override
    public int hashCode() {
      return getVisitInfoKey().hashCode();
    }
  }

  public ReportsTreeModel(QueryResults queryResults, boolean showVisits) {
    this.queryResults = queryResults;
    visitInfoKeyNodes =
        showVisits
            ? queryResults.getAllVisitInfoKeys()
                .stream()
                .sorted(new VisitInfoKeyOrdering().reverse())
                .map(VisitInfoKeyNode::new)
                .collect(ImmutableList.toImmutableList())
            : null;
    sortedSightingsCache = CacheBuilder.newBuilder().softValues()
        .build(new CacheLoader<Resolved, List<? extends Object>>() {
          @Override
          public List<? extends Object> load(Resolved taxon) throws Exception {
            return getTaxonChildren(taxon);
          }
        });
    sortedVisitInfoKeySightingsCache = CacheBuilder.newBuilder().softValues()
        .build(new CacheLoader<VisitInfoKey, List<? extends Object>>() {
          @Override
          public List<? extends Object> load(VisitInfoKey taxon) throws Exception {
            return getVisitInfoKeyChildren(taxon);
          }
        });
  }
  
  @Override
  public Object getChild(Object node, int index) {
    return getChildNodes(node).get(index);
  }

  @Override
  public int getChildCount(Object node) {
    return getChildNodes(node).size();
  }

  @Override
  public int getIndexOfChild(Object parent, Object child) {
    return getChildNodes(parent).indexOf(child);
  }

  @Override
  public Object getRoot() {
    return ROOT;
  }

  @Override
  public boolean isLeaf(Object node) {
    if (node instanceof Sighting || node instanceof ResolvedWithSighting || node instanceof VisitInfoKey) {
      return true;
    }

    if (node instanceof VisitInfoKeyNode) {
      return false;
    }

    if (node == ROOT) {
      return false;
    }

    if (node instanceof VisitNode) {
      return visitInfoKeyNodes.size() <= MAX_INLINE_VISIT_INFO_KEYS;
    }

    Resolved taxon = (Resolved) node;
    // Families (or any other higher-level taxa) are leaves,
    // species will have sightings
    if (taxon.getLargestTaxonType().compareTo(Type.species) > 0) {
      return true;
    }

    return getChildCount(node) == 0;
  }


  @Override
  public void valueForPathChanged(TreePath path, Object newValue) {
    // Do nothing - to the extent that editing is supported in a ReportsTreeModel
    // (currently, only in Locations) it does not affect the display
  }

  /**
   * Invalidate all cached state.
   */
  public void invalidateState() {
    sortedSightingsCache.invalidateAll();
    sortedVisitInfoKeySightingsCache.invalidateAll();
  }

  public void removeVisitInfo(VisitInfoKey visitInfoKey) {
    if (visitInfoKeyNodes == null) {
      return;
    }

    int index = visitInfoKeyNodes.indexOf(new VisitInfoKeyNode(visitInfoKey));
    if (index < 0) {
      return;
    }

    visitInfoKeyNodes = visitInfoKeyNodes.stream().filter(node -> !node.getVisitInfoKey().equals(visitInfoKey))
        .collect(ImmutableList.toImmutableList());
    int newSize = visitInfoKeyNodes.size();
    if (newSize == 0) {
      // No nodes remaining. We're deleting both the VisitNode *and* the visit info
      fireTreeNodesRemoved(new TreeModelEvent(this, new TreePath(ROOT), new int[] {0, 1},
          new Object[] {new VisitNode(1), new VisitInfoKeyNode(visitInfoKey)}));
    } else if (newSize < MAX_INLINE_VISIT_INFO_KEYS) {
      // Old state was inline, new state is inline too. Just delete the visit info from the root
      fireTreeNodesRemoved(new TreeModelEvent(this, new TreePath(ROOT),
          new int[] {index + 1 /* +1 for the visit node */}, new Object[] {new VisitInfoKeyNode(visitInfoKey)}));
    } else if (newSize == MAX_INLINE_VISIT_INFO_KEYS) {
      // Old state had a level, new state is inline. Whole structure is changed.
      fireTreeStructureChanged(new TreeModelEvent(this, new TreePath(ROOT)));
    } else {
      // Old state was not inline, new state is still not inline. Delete from the path containing
      // the visit node.
      fireTreeNodesRemoved(
          new TreeModelEvent(this, new TreePath(new Object[] {ROOT, new VisitNode(newSize + 1)}),
              new int[] {index}, new Object[] {new VisitInfoKeyNode(visitInfoKey)}));
      // And the number of visit keys has changed
      fireTreeNodesChanged(new TreeModelEvent(this, new TreePath(ROOT), new int[] {0},
          new Object[] {new VisitNode(newSize)}));
    }
  }

  public void addVisitInfo(VisitInfoKey visitInfoKey) {
    if (visitInfoKeyNodes == null) {
      return;
    }

    VisitInfoKeyNode newNode = new VisitInfoKeyNode(visitInfoKey);
    if (visitInfoKeyNodes.contains(newNode)) {
      return;
    }

    visitInfoKeyNodes = 
        Stream.concat(
            visitInfoKeyNodes.stream().map(VisitInfoKeyNode::getVisitInfoKey),
            Stream.of(visitInfoKey))
        .sorted(new VisitInfoKeyOrdering().reverse())
        .map(VisitInfoKeyNode::new)
        .collect(ImmutableList.toImmutableList());
    
    int insertedIndex = visitInfoKeyNodes.indexOf(newNode);
    int newSize = visitInfoKeyNodes.size();
    if (newSize == 1) {
      // First visit added. We're adding both the VisitNode *and* the visit info
      fireTreeNodesInserted(new TreeModelEvent(this, new TreePath(ROOT), new int[] {0, 1},
          new Object[] {new VisitNode(1), newNode}));
    } else if (newSize <= MAX_INLINE_VISIT_INFO_KEYS) {
      // Old state was inline, new state is inline too. Just add the visit info 
      fireTreeNodesInserted(new TreeModelEvent(this, new TreePath(ROOT),
          new int[] {insertedIndex + 1 /* +1 for the visit node */}, new Object[] {newNode}));
    } else if (newSize == MAX_INLINE_VISIT_INFO_KEYS + 1) {
      // Old state was inline, new state has an extra level. Whole structure is changed.
      fireTreeStructureChanged(new TreeModelEvent(this, new TreePath(ROOT)));
    } else {
      // Old state was not inline, new state is not inline. Delete from the path containing
      // the visit node.
      fireTreeNodesInserted(
          new TreeModelEvent(this, new TreePath(new Object[] {ROOT, new VisitNode(newSize + 1)}),
              new int[] {insertedIndex}, new Object[] {newNode}));
      // And the number of visit keys has changed
      fireTreeNodesChanged(new TreeModelEvent(this, new TreePath(ROOT), new int[] {0},
          new Object[] {new VisitNode(newSize)}));
    }
  }

  /**
   * Notify the model that sightings have been removed. Do not call if removing the sightings means
   * a taxa is entirely gone from the query.
   */
  public void sightingsRemoved(TreePath parentPath, List<Sighting> sightings,
      List<Integer> originalIndices) {
    int[] indices = new int[originalIndices.size()];
    for (int i = 0; i < indices.length; i++) {
      indices[i] = originalIndices.get(i);
    }
    // TODO: this leaves the indices array not matching the values array
    // IS THIS NECESSARY?
    Arrays.sort(indices);

    Object[] values = sightings.toArray();
    fireTreeNodesRemoved(new TreeModelEvent(this, parentPath, indices, values));
  }

  public void sightingAdded(TreePath parentPath, Sighting sighting,
      boolean updateTaxaInQueryResults) {
    sightingsAdded(parentPath, ImmutableList.of(sighting), updateTaxaInQueryResults);
  }

  public void sightingsAdded(TreePath parentPath, Collection<Sighting> sightings,
      boolean updateTaxaInQueryResults) {
    // Update the query results
    queryResults.updateSightings(ImmutableList.<Sighting>of(), sightings,
        updateTaxaInQueryResults);
    // Flush sightings cache. (Could optimize with more selective replacement
    // if useful)
    sortedSightingsCache.invalidateAll();
    sortedVisitInfoKeySightingsCache.invalidateAll();

    if (getChildCount(parentPath.getLastPathComponent()) == sightings.size()) {
      fireTreeStructureChanged(new TreeModelEvent(this, parentPath));
    }
    
    int[] indices = new int[sightings.size()];
    Object[] objects = sightings.toArray();
    for (int i = 0; i < objects.length; i++) {
      int index = getIndexOfSighting(parentPath.getLastPathComponent(), (Sighting) objects[i]);
      if (index < 0) {
        throw new IllegalStateException("Could not find sighting in " + parentPath.getLastPathComponent());
      }
      indices[i] = index;
    }
    
    // Update the model
    fireTreeNodesInserted(new TreeModelEvent(this, parentPath,
        indices,
        objects));
  }

  /**
   * Notify the model that some taxa are entirely removed.
   */
  public void taxaRemoved(TreePath parentPath, List<Object> values, List<Integer> originalIndices) {
    int[] indices = new int[originalIndices.size()];
    for (int i = 0; i < indices.length; i++) {
      indices[i] = originalIndices.get(i);
    }

    fireTreeNodesRemoved(new TreeModelEvent(this, parentPath, indices, values.toArray()));
  }

  public void sightingsReplaced(TreePath parent, List<Sighting> oldSightings,
      List<Sighting> newSightings, int[] oldIndices) {
    // In case any taxa are no longer present, find all taxa in the old sightings,
    // and track their previous indices.
    Set<Resolved> oldResolvedSet = new HashSet<>();
    Set<VisitInfoKey> oldVisitInfoKeys = new HashSet<>();
        
    for (Sighting oldSighting : oldSightings) {
      oldResolvedSet.add(queryResults.resolveToType(oldSighting.getTaxon()));
      VisitInfoKey visitInfoKey = VisitInfoKey.forSighting(oldSighting);
      if (visitInfoKey != null) {
        oldVisitInfoKeys.add(visitInfoKey);
      }
    }
    ImmutableList<Resolved> oldResolved = ImmutableList.copyOf(oldResolvedSet);
    int[] oldResolvedIndices = new int[oldResolved.size()];
    for (int i = 0; i < oldResolvedIndices.length; i++) {
      oldResolvedIndices[i] = getIndexOfChild(ROOT, oldResolved.get(i));
    }

    // Figure out if any new sightings are new taxa or visit info keys.
    Set<Resolved> newResolved = new HashSet<>();
    Set<VisitInfoKey> newVisitInfoKeys = new HashSet<>();
    for (Sighting newSighting : newSightings) {
      newResolved.add(queryResults.resolveToType(newSighting.getTaxon()));
      VisitInfoKey visitInfoKey = VisitInfoKey.forSighting(newSighting);
      if (visitInfoKey != null) {
        newVisitInfoKeys.add(visitInfoKey);
      }
    }
    newResolved.removeAll(queryResults.getTaxaAsList());

    if (visitInfoKeyNodes != null) {
      // Remove any visit info keys that were already the list of nodes.
      // After this "new" only contains anything that truly needs to be added to the tree.
      for (VisitInfoKeyNode oldVisitInfoKeyNode : visitInfoKeyNodes) {
        newVisitInfoKeys.remove(oldVisitInfoKeyNode.getVisitInfoKey());
      }
    }
    
    // TODO: do we need to be concerned about new families? Not technically possible given
    // current editing patterns.

    queryResults.updateSightings(oldSightings, newSightings,
        !newResolved.isEmpty() /* updateTaxa */);
    
    // And remove any "old" visit info keys that are still present in the query results.
    // After this, "old" only contains keys that need to be removed from the tree
    oldVisitInfoKeys.removeAll(queryResults.getAllVisitInfoKeys());
    
    for (VisitInfoKey oldVisitInfoKey : oldVisitInfoKeys) {
      removeVisitInfo(oldVisitInfoKey);
    }

    for (VisitInfoKey newVisitInfoKey : newVisitInfoKeys) {
      addVisitInfo(newVisitInfoKey);
    }
    
    // Flush caches. (Could optimize with more selective replacement if useful, but this seems fine.)
    sortedSightingsCache.invalidateAll();
    sortedVisitInfoKeySightingsCache.invalidateAll();


    // Now compute the list of removed sightings (where the sighting's taxon is still
    // present), and the list of changed sightings.
    List<Sighting> changedSightings = Lists.newArrayList();
    List<Integer> changedSightingIndices = Lists.newArrayList();
    List<Sighting> removedSightings = Lists.newArrayList();
    List<Integer> removedSightingIndices = Lists.newArrayList();
    List<Sighting> addedSightings = Lists.newArrayList();
    List<Integer> addedSightingIndices = Lists.newArrayList();

    for (int i = 0; i < oldIndices.length; i++) {
      int newIndex = getIndexOfSighting(parent.getLastPathComponent(), newSightings.get(i));
      // Sighting has been removed from its original location. If the
      // taxon is gone altogether, then we'll send a tree-nodes-removed event
      // for the taxon; but if it isn't, then we have to record the removal
      // of the sighting.
      if (newIndex < 0) {
        Resolved resolved = queryResults.resolveToType(oldSightings.get(i).getTaxon());
        // Sighting is removed, taxon is still present
        if (getIndexOfChild(ROOT, resolved) >= 0) {
          removedSightings.add(oldSightings.get(i));
          removedSightingIndices.add(oldIndices[i]);
        }
      } else {
        if (newIndex == oldIndices[i]) {
          // Sighting is still in its previous location.
          changedSightings.add(newSightings.get(i));
          changedSightingIndices.add(newIndex);
        } else {
          removedSightings.add(oldSightings.get(i));
          removedSightingIndices.add(oldIndices[i]);
          addedSightings.add(newSightings.get(i));
          addedSightingIndices.add(newIndex);
        }
      }
    }

    // Send an event for any removed taxa
    if (!oldResolved.isEmpty()) {
      List<Resolved> removedResolvedList = Lists.newArrayList();
      List<Integer> removedResolvedIndices = Lists.newArrayList();
      for (int i = 0; i < oldResolvedIndices.length; i++) {
        // Catch cases where the old Resolved taxon was, somehow, already removed?
        // Not obvious why this happens - I think it's a weird ordering problem -
        // but lots of users have, over the years, reported what inherently must mean
        // a negative index right here.
        if (oldResolvedIndices[i] >= 0) {
          if (getIndexOfChild(ROOT, oldResolved.get(i)) <= -1) {
            removedResolvedList.add(oldResolved.get(i));
            removedResolvedIndices.add(oldResolvedIndices[i]);
          }
        }
      }
      if (!removedResolvedList.isEmpty()) {
        fireTreeNodesRemoved(new TreeModelEvent(this, new TreePath(ROOT),
            Ints.toArray(removedResolvedIndices), removedResolvedList.toArray()));
      }
    }

    // Send an event for any new taxa
    if (!newResolved.isEmpty()) {
      List<Resolved> newResolvedList = Lists.newArrayList();
      List<Integer> newResolvedIndices = Lists.newArrayList();
      for (Resolved resolved : newResolved) {
        newResolvedList.add(resolved);
        newResolvedIndices.add(getIndexOfChild(ROOT, resolved));
      }
      fireTreeNodesInserted(new TreeModelEvent(this, new TreePath(ROOT),
          Ints.toArray(newResolvedIndices), newResolvedList.toArray()));
    }

    // Send an event for any removed sightings
    if (!removedSightings.isEmpty()) {
      fireTreeNodesRemoved(new TreeModelEvent(this, parent, Ints.toArray(removedSightingIndices),
          removedSightings.toArray()));
    }

    // ... and another for any added sightings
    if (!addedSightings.isEmpty()) {
      fireTreeNodesInserted(new TreeModelEvent(this, parent, Ints.toArray(addedSightingIndices),
          addedSightings.toArray()));
    }

    // Send an event for any changed sightings
    if (!changedSightings.isEmpty()) {
      fireTreeNodesChanged(new TreeModelEvent(this, parent, Ints.toArray(changedSightingIndices),
          changedSightings.toArray()));
    }
  }
  
  /**
   * Get the index of a sighting within a parent node.  This might be a direct sighting,
   * or it might be a sighting attached to a ResolvedWithSighting. 
   */
  public int getIndexOfSighting(Object parentNode, Sighting sighting) {
    List<? extends Object> childNodes = getChildNodes(parentNode);
    for (int i = 0; i < childNodes.size(); i++) {
      Object o = childNodes.get(i);
      if (o == sighting) {
        return i;
      } else if (o instanceof ResolvedWithSighting && ((ResolvedWithSighting) o).getSighting() == sighting) {
        return i;
      }
    }
    
    return -1;
  }

  private List<? extends Object> getChildNodes(Object node) {
    if (node instanceof Resolved) {
      return sortedSightingsCache.getUnchecked((Resolved) node);
    } else if (node instanceof Sighting || node instanceof ResolvedWithSighting || node instanceof VisitInfoKey) {
      return ImmutableList.of();
    } else if (node == ROOT) {
      if (visitInfoKeyNodes == null || visitInfoKeyNodes.isEmpty()) {
        return queryResults.getTaxaAsList();
      } else {
        if (visitInfoKeyNodes.size() > MAX_INLINE_VISIT_INFO_KEYS) {
          return ImmutableList.copyOf(Iterables.concat(
              ImmutableList.of(new VisitNode(visitInfoKeyNodes.size())), queryResults.getTaxaAsList()));
        } else {
          return ImmutableList
              .copyOf(Iterables.concat(ImmutableList.of(new VisitNode(visitInfoKeyNodes.size())),
                  visitInfoKeyNodes, queryResults.getTaxaAsList()));
        }
      }
    } else if (node instanceof VisitNode) {
      return visitInfoKeyNodes;
    } else if (node instanceof VisitInfoKeyNode) {
      return sortedVisitInfoKeySightingsCache.getUnchecked(((VisitInfoKeyNode) node).getVisitInfoKey());
    } else {
      throw new IllegalArgumentException("Unexpected node type " + node);
    }
  }

  private List<Sighting> getTaxonChildren(Resolved taxon) {
    Collection<Sighting> collection = queryResults.getAllSightings(taxon);
    // "prefer earlier" - that is, put earliest *at the end*.
    return SightingComparators.preferEarlier().immutableSortedCopy(collection);
  }

  private List<Object> getVisitInfoKeyChildren(VisitInfoKey visitInfoKey) {
    return Stream.concat(
        Stream.of(visitInfoKey), 
        Streams.stream(queryResults.getAllSightings())
            .filter(visitInfoKey::matches)
            .map(sighting -> new ResolvedWithSighting(sighting.getTaxon().resolve(queryResults.getTaxonomy()), sighting))
            .sorted())
        .collect(ImmutableList.toImmutableList());
  }

}
