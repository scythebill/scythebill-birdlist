package com.scythebill.birdlist.ui.panels.reports;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.query.PredicateQueryDefinition;
import com.scythebill.birdlist.model.query.QueryDefinition;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Type;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.ui.actions.ActionBroker;
import com.scythebill.birdlist.ui.components.SightingBulkEditDialog;
import com.scythebill.birdlist.ui.components.SpeciesIndexerPanelConfigurer;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.imports.FinishedImport;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.VisibilityDetector;

/**
 * A panel purely used to resolve taxa - a small subset of the ReportsPanel.
 */
public class ResolveTaxaPanel extends ReportsPanel {
  private ImmutableSet<SightingTaxon> spsToResolve;

  @Inject
  public ResolveTaxaPanel( 
      @FinishedImport Action finishedResolution,
      TaxonomyStore taxonomyStore,
      ReportSet reportSet, QueryPanelFactory queryPanelFactory,
      QueryFieldFactory queryFieldFactory,
      ReportsBrowserPanel reportsBrowserPanel, FontManager fontManager,
      ReportsActionFactory reportsActionFactory, ActionBroker actionBroker,
      EventBusRegistrar eventBusRegistrar, SightingBulkEditDialog sightingBulkEditDialog,
      SpeciesIndexerPanelConfigurer speciesIndexerPanelConfigurer,
      Checklists checklists, Alerts alerts, QueryPreferences queryPreferences,
      VisibilityDetector visibilityDetector) {
    super(undoResolveTaxonomy(finishedResolution, taxonomyStore),
        // HACK: force the switch to a bird taxonomy *right here*.  Doing this in the constructor is waaaaay ugly,
        // but the alternatives (so far) are worse.  Would be best to move all of the interesting logic out of the
        // constructor, at which point tweaking the global state would be much less unreasonable (and could be done
        // generically, most likely)
        forceResolveTaxonomy(taxonomyStore, reportSet),
        reportSet, queryPanelFactory, reportsBrowserPanel,
        fontManager, reportsActionFactory, actionBroker, eventBusRegistrar, sightingBulkEditDialog,
        speciesIndexerPanelConfigurer, checklists,
        new StoredQueries(
            queryFieldFactory,
            alerts,
            new StoredQueriesPreferences(),
            null), // Use dummy stored queries, instead of the "real" one.
        queryPreferences,
        visibilityDetector); 
    if (!taxonomyStore.isBirdTaxonomy()) {
      throw new IllegalStateException("ResolveTaxaPanel requires a bird taxonomy!");
    }
    
    spsToResolve = Preconditions.checkNotNull(reportSet.getSpsToResolve());
    Taxon.Type depth = Taxon.Type.species;
    for (SightingTaxon taxon : spsToResolve) {
      Resolved resolved = taxon.resolve(taxonomyStore.getTaxonomy());
      if (resolved == null) {
        throw new IllegalStateException("Could not resolve " + taxon + " into " + taxonomyStore.getTaxonomy().getName());
      }
      Taxon.Type taxonType = resolved.getSmallestTaxonType();
      if (taxonType.compareTo(depth) < 0) {
        depth = taxonType;
      }
    }
      
    simplifyForResolution(depth);
  }

  private static Action undoResolveTaxonomy(Action finishedResolution, TaxonomyStore taxonomyStore) {
    return new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        // TODO: show a message if this method returns a non-null taxonomy (and therefore
        // is restoring something?)
        taxonomyStore.restoreRememberedTaxonomy();
        finishedResolution.actionPerformed(e);
      }
    };
  }

  private static TaxonomyStore forceResolveTaxonomy(
      TaxonomyStore taxonomyStore,
      ReportSet reportSet) {
    // Remember the current taxonomy before forcing one.
    taxonomyStore.rememberCurrentTaxonomy();
    
    // If the resolution is necessarily for a specific taxonomy, use that
    if (reportSet.getResolveTaxonomy() != null) {
      taxonomyStore.setTaxonomy(reportSet.getResolveTaxonomy());
      return taxonomyStore;
    }
    // Otherwise use the preferred bird taxonomy
    return taxonomyStore.switchToPreferredBirdTaxonomy();
  }

  /** Return a help label for the reports panel. */
  @Override
  protected JComponent getHelpLabel() {
    JPanel panel = new JPanel();
    BoxLayout layout = new BoxLayout(panel, BoxLayout.Y_AXIS);
    panel.setLayout(layout);
    panel.add(new JLabel(
        Messages.getMessage(Name.RESOLVE_SELECT_EACH_SPECIES)));
    JLabel dontNeedToFinish = new JLabel(
        Messages.getMessage(Name.RESOLVE_DONT_NEED_TO_FINISH));
    dontNeedToFinish.putClientProperty(FontManager.PLAIN_LABEL, true);
    panel.add(dontNeedToFinish);
    return panel;
  }

  @Override
  public String getTitle() {
    return "Resolve splits";
  }

  @Override
  protected QueryDefinition getQueryDefinition(Taxon.Type depth) {
    Predicate<Sighting> predicate = getCurrentTaxonomy() instanceof MappedTaxonomy
        ? new Predicate<Sighting>() {
          @Override public boolean apply(Sighting sighting) {
            if (!spsToResolve.contains(sighting.getTaxon())) {
              return false;
            }
            // If it's a MappedTaxonomy, map back to the sighting taxon.
            SightingTaxon sightingTaxon = sighting.getTaxon().resolve(getCurrentTaxonomy()).getSightingTaxon();
            return sightingTaxon.getType() == Type.SP;
          }
        } 
        : new Predicate<Sighting>() {
          @Override public boolean apply(Sighting sighting) {
            return spsToResolve.contains(sighting.getTaxon());
          }
        };
    return new PredicateQueryDefinition(predicate);
  }
}
