/*
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.StringWriter;

import javax.swing.AbstractAction;

import com.google.common.base.Optional;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.components.HtmlResultPanel;
import com.scythebill.birdlist.ui.components.OkCancelPanel;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.io.FamilyReportOutput;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * Implements the save-to-family-report action for reports.
 */
class SaveAsFamilyReportAction extends AbstractAction {
  private final QueryExecutor queryExecutor;
  private final Alerts alerts;
  private final FontManager fontManager;
  private final QueryPreferences queryPreferences;
  private final Taxonomy taxonomy;
  private final ReportSet reportSet;
  private final FamilyReportDialog familyReportDialog;
  private final Checklists checklists;

  public SaveAsFamilyReportAction(QueryExecutor queryExecutor, Alerts alerts,
      FamilyReportDialog familyReportDialog, FontManager fontManager,
      ReportSet reportSet, Taxonomy taxonomy, QueryPreferences queryPreferences,
      Checklists checklists) {
    this.queryExecutor = queryExecutor;
    this.alerts = alerts;
    this.familyReportDialog = familyReportDialog;
    this.fontManager = fontManager;
    this.reportSet = reportSet;
    this.taxonomy = taxonomy;
    this.queryPreferences = queryPreferences;
    this.checklists = checklists;
  }

  static private Frame getParentFrame(Component c) {
    while (c != null) {
      if (c instanceof Frame) {
        return (Frame) c;
      }

      c = c.getParent();
    }

    return null;
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    QueryResults queryResults =
        queryExecutor.executeQuery(null, null);
    // Get a new instance of the dialog for each run
    Frame source = getParentFrame((Component) event.getSource());

    FamilyReportPreferences prefs = familyReportDialog.getConfiguration(source);
    if (prefs == null) {
      return;
    }

    FamilyReportOutput familyReportOutput =
        new FamilyReportOutput(queryResults, taxonomy, queryPreferences);
    familyReportOutput.setScientificOrCommon(familyReportDialog.getScientificOrCommonFromConfiguration(prefs));
    Checklist checklist = queryResults.getChecklist(taxonomy);
    if (checklist == null) {
      Location rootLocation = queryExecutor.getRootLocation();
      if (rootLocation != null) {
        checklist = checklists.getNearestBuiltInChecklist(taxonomy, reportSet, rootLocation);
      }
    }
    familyReportOutput.setChecklist(checklist);
    familyReportOutput.setSortBy(prefs.sortBy);

    try (StringWriter out = new StringWriter()) {
      familyReportOutput.writeReport(out);
      String plainText = familyReportOutput.writeToTabDelimitedText();
            
      HtmlResultPanel htmlResultPanel =
          new HtmlResultPanel(Name.FAMILY_REPORT_EXPLANATION, out.toString(), fontManager);
      htmlResultPanel.setPlainText(plainText);
      htmlResultPanel.copyToClipboard();
      Optional<String> reportName = queryExecutor.getReportName();
      if (reportName.isPresent()) {
        htmlResultPanel.setDocumentName(reportName.get());
      }

      OkCancelPanel okCancelPanel = new OkCancelPanel(
          OkCancelPanel.CLOSE_WINDOW_ACTION,
          htmlResultPanel.printAction(),
          htmlResultPanel,
          null);
      
      okCancelPanel.showInDialog(source, fontManager);
    } catch (IOException e) {
      alerts.showError(source, Name.SAVING_FAILED_TITLE, Name.COULD_NOT_SAVE_REPORT);
      return;
    }
  }
}
