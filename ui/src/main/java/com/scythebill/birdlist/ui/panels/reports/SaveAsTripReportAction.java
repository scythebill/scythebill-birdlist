/*
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.StringWriter;

import javax.swing.AbstractAction;

import com.google.common.base.Optional;
import com.google.inject.Provider;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.ui.components.HtmlResultPanel;
import com.scythebill.birdlist.ui.components.OkCancelPanel;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.io.TripReportDocOutput;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * Implements the save-to-trip-report action for reports.
 */
class SaveAsTripReportAction extends AbstractAction {
  private static final int MAX_VISIT_SIZE_FOR_SPECIES_TABLE = 200;
  private final QueryExecutor queryExecutor;
  private final ReportSet reportSet;
  private final Alerts alerts;
  private final Provider<TripReportDialog> tripReportDialog;
  private FontManager fontManager;

  public SaveAsTripReportAction(ReportSet reportSet, QueryExecutor queryExecutor, Alerts alerts,
      Provider<TripReportDialog> tripReportDialog, FontManager fontManager) {
    this.reportSet = reportSet;
    this.queryExecutor = queryExecutor;
    this.alerts = alerts;
    this.tripReportDialog = tripReportDialog;
    this.fontManager = fontManager;
  }

  static private Frame getParentFrame(Component c) {
    while (c != null) {
      if (c instanceof Frame) {
        return (Frame) c;
      }

      c = c.getParent();
    }

    return null;
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    QueryResults queryResults =
        queryExecutor.includeIncompatibleSightings().executeQuery(null, null);
    // Get a new instance of the dialog for each run
    TripReportDialog dialog = tripReportDialog.get();
    if (queryResults.getAllVisitInfoKeys().size() > MAX_VISIT_SIZE_FOR_SPECIES_TABLE) {
      dialog.disableSpeciesTable();
    }

    if (queryResults.getIncompatibleTaxonomies().isEmpty()) {
      dialog.disableMultipleTaxonomies();
    }

    Frame source = getParentFrame((Component) event.getSource());

    TripReportPreferences prefs = dialog.getConfiguration(source);
    if (prefs == null) {
      return;
    }

    TripReportDocOutput tripReportOutput =
        new TripReportDocOutput(queryResults, reportSet, queryExecutor.getRootLocation());
    queryExecutor.getReportName().toJavaUtil().ifPresent(tripReportOutput::setName);
    tripReportOutput.setScientificOrCommon(dialog.getScientificOrCommonFromConfiguration(prefs));
    tripReportOutput.setWriteItinerary(prefs.includeItinerary);
    tripReportOutput.setWriteSpeciesList(prefs.includeSpeciesList);
    tripReportOutput.setWriteSpeciesTable(prefs.includeSpeciesTable);
    tripReportOutput.setShowAllTaxonomies(prefs.showAllTaxonomies);
    tripReportOutput.setIncludeFavoritePhotos(prefs.includeFavoritePhotos);
    tripReportOutput.setQueryHasDateRestriction(queryExecutor.getReportHints().contains(ReportHints.DATE_RESTRICTION));

    try (StringWriter out = new StringWriter()) {
      String plainText = tripReportOutput.writeReport(out);

      HtmlResultPanel htmlResultPanel =
          new HtmlResultPanel(Name.TRIP_REPORT_EXPLANATION, out.toString(), fontManager);
      htmlResultPanel.setPlainText(plainText);
      htmlResultPanel.copyToClipboard();
      Optional<String> reportName = queryExecutor.getReportName();
      if (reportName.isPresent()) {
        htmlResultPanel.setDocumentName(reportName.get());
      }

      OkCancelPanel okCancelPanel = new OkCancelPanel(
          OkCancelPanel.CLOSE_WINDOW_ACTION,
          htmlResultPanel.printAction(),
          htmlResultPanel,
          null);
      
      okCancelPanel.showInDialog(source, fontManager);
    } catch (IOException e) {
      alerts.showError(source, Name.SAVING_FAILED_TITLE, Name.COULD_NOT_SAVE_REPORT);
      return;
    }
  }
}
