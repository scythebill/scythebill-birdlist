/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.awt.Component;
import java.awt.Desktop;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.google.common.base.Optional;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.ui.io.ReportXlsOutput;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;

/**
 * Implements the save-to-XLS action for reports.
 */
class SaveAsXlsAction extends AbstractAction {
  private final QueryExecutor queryExecutor;
  private final FileDialogs fileDialogs;
  private final ReportSet reportSet;
  private final Alerts alerts;
  private final ReportXlsDialog reportXlsDialog;

  public SaveAsXlsAction(
      FileDialogs fileDialogs,
      ReportSet reportSet,
      QueryExecutor queryExecutor,
      Alerts alerts,
      ReportXlsDialog reportXlsDialog) {
    this.fileDialogs = fileDialogs;
    this.reportSet = reportSet;
    this.queryExecutor = queryExecutor;
    this.alerts = alerts;
    this.reportXlsDialog = reportXlsDialog;
  }
  
  static private Frame getParentFrame(Component c) {
    while (c != null) {
      if (c instanceof Frame) {
        return (Frame) c;
      }

      c = c.getParent();
    }

    return null;
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    Optional<String> reportName = queryExecutor.getReportAbbreviation();
    String fileName = reportName.isPresent()
        ? String.format("report-%s.xls", reportName.get())
        : "report.xls";
    File out = fileDialogs.saveFile(getParentFrame((Component) event.getSource()),
        Messages.getMessage(Name.PICK_FILE_TITLE),
        fileName,
        new FileNameExtensionFilter(
            Messages.getMessage(Name.XLS_FILES), "xls"), FileType.OTHER);
    if (out == null) {
      return;
    }

    ReportSpreadsheetPreferences prefs = reportXlsDialog.getConfiguration(
        (Component) event.getSource(),
        !reportSet.extendedTaxonomies().isEmpty());
    if (prefs == null) {
      return;
    }
    
    QueryExecutor executor = queryExecutor;
    if (prefs.onlyCountable) {
      executor = executor.onlyCountable();
    }
    if (prefs.showAllTaxonomies) {
      executor = executor.includeIncompatibleSightings();
    }
    QueryResults queryResults = executor.executeQuery(null, null);

    ReportXlsOutput reportXlsOutput = new ReportXlsOutput(
        out, queryResults, reportSet.getLocations());
    reportXlsOutput.setScientificOrCommon(
        reportXlsDialog.getScientificOrCommonFromConfiguration(prefs));
    Set<ReportHints> reportHints = queryExecutor.getReportHints();
    reportXlsOutput.setShowFamilies(prefs.showFamilies);
    reportXlsOutput.setShowFamilyTotals(
        !reportHints.contains(ReportHints.SPECIES_STATUS_RESTRICTION));
    reportXlsOutput.setRootLocation(queryExecutor.getRootLocation());
    reportXlsOutput.setSightingsCount(prefs.maximumSightings);
    reportXlsOutput.setShowStatus(prefs.showStatus);
    reportXlsOutput.setShowNotes(prefs.showNotes);
    reportXlsOutput.setShowAllTaxonomies(prefs.showAllTaxonomies);
    reportXlsOutput.setOmitSpAndHybrid(prefs.omitSpAndHybrid);
    reportXlsOutput
        .setUserOutput(reportSet.getUserSet() == null ? UserOutput.NONE : prefs.userOutput);
    if (prefs.sortType != SortType.DEFAULT) {
      reportXlsOutput.setSortAllSightings(true);
    }
    try {
      reportXlsOutput.writeSpeciesList(
          prefs.sortType.ordering(),
          prefs.sortSightingsType.ordering());
    } catch (IOException e) {
      FileDialogs.showFileSaveError(alerts, e, out);
      return;
    }
    
    try {
      Desktop.getDesktop().open(out);
    } catch (IOException e) {
      alerts.showError((Component) event.getSource(),
          Name.OPENING_FAILED,
          Name.NO_APPLICATION_FOR_XLS_FILES);
    }      
  }
}
