/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;

import com.google.common.base.Predicate;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;

/**
 * QueryField for adjusting queries to the sex or age of the sighting.
 */
class SexAgeQueryField extends AbstractQueryField {
  private enum QueryType {
    IS_FEMALE(Name.SEX_IS_FEMALE),
    IS_MALE(Name.SEX_IS_MALE),
    IS_ADULT(Name.AGE_IS_ADULT),
    IS_IMMATURE(Name.AGE_IS_IMMATURE);
    
    private final Name text;
    QueryType(Name text) {
      this.text = text;
    }
    
    @Override
    public String toString() {
      return Messages.getMessage(text);
    }
  }

  private final JComboBox<QueryType> queryOptions = new JComboBox<>(QueryType.values());
  private final JPanel emptyValueField;

  public SexAgeQueryField() {
    super(QueryFieldType.SEX_AGE);
    this.emptyValueField = new JPanel();
    queryOptions.addActionListener(e -> firePredicateUpdated());
  }

  @Override
  public JComponent getComparisonChooser() {
    return queryOptions;
  }

  @Override
  public JComponent getValueField() {
    return emptyValueField;
  }

  @Override
  public Predicate<Sighting> predicate(Taxon.Type depth) {
    QueryType type = (QueryType) queryOptions.getSelectedItem();
    switch (type) {
      case IS_FEMALE:
        return sighting -> sighting.hasSightingInfo() && sighting.getSightingInfo().isFemale();
      case IS_MALE:
        return sighting -> sighting.hasSightingInfo() && sighting.getSightingInfo().isMale();
      case IS_ADULT:
        return sighting -> sighting.hasSightingInfo() && sighting.getSightingInfo().isAdult();
      case IS_IMMATURE:
        return sighting -> sighting.hasSightingInfo() && sighting.getSightingInfo().isImmature();
      default:
        throw new AssertionError("Unexpected type: " + type);
    }
  }

  @Override public boolean isNoOp() {
    return false;
  }

  @Override
  public JsonElement persist() {
    Persisted persisted = new Persisted(
        (QueryType) queryOptions.getSelectedItem());
    return GSON.toJsonTree(persisted);
  }

  @Override
  public void restore(JsonElement json) {
    Persisted persisted = GSON.fromJson(json, Persisted.class);
    queryOptions.setSelectedItem(persisted.type);
  }
  
  static private final Gson GSON = new Gson();
  static class Persisted {
    public Persisted(QueryType type) {
      this.type = type;
    }
    
    QueryType type;
  }
}
