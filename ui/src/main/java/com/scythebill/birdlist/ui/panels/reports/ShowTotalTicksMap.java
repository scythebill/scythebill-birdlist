/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.apache.commons.lang3.StringEscapeUtils;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.google.common.io.Resources;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.ui.panels.reports.TotalTicksProcessor.TotalTickType;
import com.scythebill.birdlist.ui.util.DesktopUtils;
import com.scythebill.birdlist.ui.util.GoogleGeocharts;
import com.scythebill.birdlist.ui.util.GoogleGeocharts.Continent;
import com.scythebill.birdlist.ui.util.GoogleGeocharts.Subcontinent;

/**
 * Show a map of a species range.
 */
public class ShowTotalTicksMap {
  private final LocationSet locations;
  private final GoogleGeocharts googleGeocharts;

  @Inject
  public ShowTotalTicksMap(
      PredefinedLocations predefinedLocations,
      ReportSet reportSet) {
    this.locations = reportSet.getLocations();
    this.googleGeocharts = new GoogleGeocharts(locations, predefinedLocations);
  }

  public void showRange(
      Multimap<String, String> locationToSpecies,
      TotalTickType totalTickType,
      String reportName) throws IOException {
    if (totalTickType == TotalTickType.COUNTY) {
      throw new UnsupportedOperationException("Total tick maps not supported for counties");
    }
    File file = File.createTempFile("totalticks", ".html");
    try (Writer writer = new FileWriter(file)){
      writeRange(locationToSpecies, totalTickType, reportName, writer);
    }
    
    DesktopUtils.openHtmlFileInBrowser(file);
  }
    
  private void writeRange(
      Multimap<String, String> locationToSpecies, TotalTickType totalTickType,
      String reportName, Writer writer)
      throws IOException {
    String startTemplate = Resources.toString(
        Resources.getResource(getClass(), "ticksmap.start.html.template"),
        Charsets.UTF_8);
    String endTemplate = Resources.toString(
        Resources.getResource(getClass(), "ticksmap.end.html.template"),
        Charsets.UTF_8);

    Set<String> codes = new LinkedHashSet<>();
    writer.write(String.format(startTemplate, reportName, totalTickType.toString()));
    
    Map<String, Integer> codeToCount = new LinkedHashMap<>();
    
    for (String locationId : locationToSpecies.keySet()) {
      Location location = locations.getLocation(locationId);
      String locationCode = Locations.getLocationCode(location);
      
      if (locationCode != null) {
        locationCode = GoogleGeocharts.EBIRD_STATE_TO_GOOGLE_STATE.getOrDefault(
            locationCode, locationCode);
        codes.add(locationCode);
        int count = locationToSpecies.get(locationId).size();
        codeToCount.put(locationCode, count + codeToCount.getOrDefault(locationCode, 0));
      }
    }
    
    for (Map.Entry<String, Integer> entry : codeToCount.entrySet()) {
      writer.write(dataFormat(entry.getValue(), entry.getKey(),
          googleGeocharts.getNameForGoogle(entry.getKey())));
      
    }
    
    List<String> optionsList = new ArrayList<>();
    if (totalTickType == TotalTickType.COUNTRY) {
      // For countries, figure out which regions are included, and generate one option
      // for each region
      ImmutableSet<Subcontinent> subcontinents = googleGeocharts.subcontinents(
          codes);
      ImmutableSet<Continent> continents = googleGeocharts.continents(subcontinents);
      if (continents.size() > 1) {
        optionsList.add("<option value=\"\">Zoom to world, or...</option>");
      }
      
      if (subcontinents.size() == 1) {
        optionsList.add(subcontinents.iterator().next().asOption());
      } else {
        for (Continent continent : continents) {
          if (subcontinents.size() > 1) {
            optionsList.add(continent.asOption());
          }
          
          for (Subcontinent subcontinent : continent.subcontinents()) {
            if (subcontinents.contains(subcontinent)) {
              optionsList.add(subcontinent.asOption());
            }
          }
        }
      }
    } else if (totalTickType == TotalTickType.STATE) {
      Set<String> countries = normalizeCountries(codes);
      TreeMap<String, String> countryNamesToCodes = new TreeMap<>();
      for (String country : countries) {
        String nameForGoogle = googleGeocharts.getNameForGoogle(country);
        if (nameForGoogle != null) {
          countryNamesToCodes.put(nameForGoogle, country);
        }
      }
      
      countryNamesToCodes.forEach((name, id) -> {
        optionsList.add(String.format("<option value=\"?%s\">%s</option>", id, name));
      });
    }
    
    // For states, find a list of all countries, and generate one option for each
    // country
    
    
    String resolution = totalTickType == TotalTickType.COUNTRY ? "countries" : "provinces";
    writer.write(String.format(
        endTemplate,
        resolution,
        Joiner.on('\n').join(optionsList)));
  }

  private String dataFormat(int count, String country,
      String countryName) {
    return String.format("[{v:\"%s\",f:\"%s\"}, {v:%d, f:\"%s\"}],",
        country,
        StringEscapeUtils.escapeJson(countryName),
        count,
        count);
  }

  private Set<String> normalizeCountries(Iterable<String> statesAndCountries) {
    Set<String> set = Sets.newHashSet();
    for (String value : statesAndCountries) {
      if (GoogleGeocharts.EBIRD_TO_GOOGLE_COUNTRIES.containsKey(value)) {
        value = GoogleGeocharts.EBIRD_TO_GOOGLE_COUNTRIES.get(value);
      }
      int indexOf = value.indexOf('-');
      if (indexOf < 0) {
        set.add(value);
      } else {
        set.add(value.substring(0, indexOf));
      }
    }
    return set;
  }
}
