/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.JComboBox;
import javax.swing.JComponent;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonSyntaxException;
import com.scythebill.birdlist.model.query.SightingPredicates;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;

/**
 * QueryField for adjusting queries relative to the status of a sighting.
 */
class SightingStatusQueryField extends AbstractQueryField {
  private enum QueryType {
    IS(Name.STATUS_IS),
    IS_NOT(Name.STATUS_IS_NOT),
    ANY(Name.STATUS_ANY);
    
    private final Name text;
    QueryType(Name text) {
      this.text = text;
    }
    
    @Override
    public String toString() {
      return Messages.getMessage(text);
    }
  }
  private final JComboBox<QueryType> queryOptions = new JComboBox<>(QueryType.values());
  private final JComboBox<String> statusOptions = new JComboBox<>(getStatusText());

  public SightingStatusQueryField() {
    super(QueryFieldType.SIGHTING_STATUS);
    queryOptions.addActionListener(new ActionListener() {
      @Override public void actionPerformed(ActionEvent e) {
        statusOptions.setEnabled(!QueryType.ANY.equals(queryOptions.getSelectedItem()));
        firePredicateUpdated();
      }
    });
    statusOptions.addActionListener(e -> firePredicateUpdated());
    statusOptions.setMaximumRowCount(statusOptions.getItemCount());
  }

  private static String[] getStatusText() {
    return Arrays.stream(SightingStatus.values())
        .map(Messages::getText)
        .toArray(String[]::new);
  }

  @Override
  public JComponent getComparisonChooser() {
    return queryOptions;
  }

  @Override
  public JComponent getValueField() {
    return statusOptions;
  }

  @Override
  public Predicate<Sighting> predicate(Taxon.Type depth) {
    if (QueryType.ANY.equals(queryOptions.getSelectedItem())) {
      return Predicates.alwaysTrue();
    }
    SightingStatus status = SightingStatus.values()[statusOptions.getSelectedIndex()];
    Predicate<Sighting> predicate = SightingPredicates.sightingStatusIsIn(ImmutableSet.of(status));
    if (QueryType.IS_NOT.equals(queryOptions.getSelectedItem())) {
      predicate = Predicates.not(predicate);
    }
    
    return predicate;
  }

  @Override public boolean isNoOp() {
    return QueryType.ANY.equals(queryOptions.getSelectedItem());
  }

  @Override
  public JsonElement persist() {
    SightingStatus status = SightingStatus.values()[statusOptions.getSelectedIndex()];
    Persisted persisted = new Persisted(
        (QueryType) queryOptions.getSelectedItem(), status.getId());
    return GSON.toJsonTree(persisted);
  }

  @Override
  public void restore(JsonElement json) {
    Persisted persisted = GSON.fromJson(json, Persisted.class);
    queryOptions.setSelectedItem(persisted.type);
    SightingStatus sightingStatus = SightingStatus.forId(persisted.statusId);
    if (sightingStatus == null) {
      throw new JsonSyntaxException("Invalid sighting status: " + persisted.statusId);
    }
    statusOptions.setSelectedItem(Messages.getText(sightingStatus));
  }
  
  static private final Gson GSON = new Gson();
  static class Persisted {
    public Persisted(QueryType type, String statusId) {
      this.type = type;
      this.statusId = statusId;
    }
    
    QueryType type;
    String statusId;
  }
}
