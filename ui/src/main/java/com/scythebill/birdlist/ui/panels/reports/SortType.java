/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import com.google.common.collect.Ordering;
import com.scythebill.birdlist.model.query.SightingComparators;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * List of sort types understood for reporting.
 */
enum SortType {
  DEFAULT(Name.SORT_BY_DEFAULT, SightingComparators.preferNotHeardOnly()
      .compound(SightingComparators.preferNotIntroduced())
      .compound(SightingComparators.preferEarlier())),
  BY_DATE(Name.SORT_BY_DATE,
      SightingComparators.preferEarlier()),
  BY_DATE_DESCENDING(Name.SORT_BY_DATE_DESCENDING,
      SightingComparators.preferEarlier().reverse()),
  BY_DAY_OF_YEAR(Name.SORT_BY_DAY_OF_YEAR,
      SightingComparators.preferEarlierInTheYear()),
  BY_DAY_OF_YEAR_DESCENDING(Name.SORT_BY_DAY_OF_YEAR_DESCENDING,
      SightingComparators.preferEarlierInTheYear().reverse()),
  BY_SIGHTING_COUNT(Name.SORT_BY_NUMBER_RECORDED,
      SightingComparators.preferHigherCounts());
  
  private final Name text;
  private final Ordering<Sighting> ordering;
  SortType(Name text, Ordering<Sighting> sightingOrdering) {
    this.text = text;
    this.ordering = sightingOrdering;
  }
  
  @Override
  public String toString() {
    return Messages.getMessage(text);
  }

  public Ordering<Sighting> ordering() {
    return ordering;
  }
}
