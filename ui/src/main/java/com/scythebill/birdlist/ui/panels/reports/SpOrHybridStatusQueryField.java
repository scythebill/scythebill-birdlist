/*   
 * Copyright 2012 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import javax.swing.JComboBox;
import javax.swing.JComponent;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;

/**
 * QueryField for adjusting queries relative to the status of a species.
 */
class SpOrHybridStatusQueryField extends AbstractQueryField {
  private enum QueryType {
    IS(Name.SPECIES_IS),
    IS_NOT(Name.SPECIES_IS_NOT);
    
    private final Name text;
    QueryType(Name text) {
      this.text = text;
    }
    
    @Override
    public String toString() {
      return Messages.getMessage(text);
    }
  }

  private enum StatusType {
    SP(Name.SPUH_LABEL, SightingTaxon.Type.SP),
    HYBRID(Name.HYBRID_LABEL, SightingTaxon.Type.HYBRID),
    NEITHER(Name.NEITHER_TEXT, SightingTaxon.Type.SINGLE);
    
    private final Name text;
    private final SightingTaxon.Type type;
    
    StatusType(Name text, SightingTaxon.Type type) {
      this.text = text;
      this.type = type;
    }
    
    public SightingTaxon.Type getType() {
      return type;
    }
    
    @Override
    public String toString() {
      return Messages.getMessage(text);
    }
  }

  private final JComboBox<QueryType> queryOptions = new JComboBox<>(QueryType.values());
  private final JComboBox<StatusType> typeOptions = new JComboBox<>(StatusType.values());
  private final TaxonomyStore taxonomyStore;

  public SpOrHybridStatusQueryField(TaxonomyStore taxonomyStore) {
    super(QueryFieldType.SP_OR_HYBRID);
    this.taxonomyStore = taxonomyStore;
    queryOptions.addActionListener(e -> firePredicateUpdated());
    typeOptions.addActionListener(e -> firePredicateUpdated());
  }

  @Override
  public JComponent getComparisonChooser() {
    return queryOptions;
  }

  @Override
  public JComponent getValueField() {
    return typeOptions;
  }

  @Override
  public Predicate<Sighting> predicate(final Taxon.Type depth) {
    StatusType statusType = (StatusType) typeOptions.getSelectedItem();
    final SightingTaxon.Type type = statusType.getType();
    Predicate<Sighting> predicate = new Predicate<Sighting>() {
      @Override public boolean apply(Sighting sighting) {
        // Skip incompatible taxa
        if (!TaxonUtils.areCompatible(taxonomyStore.getTaxonomy(), sighting.getTaxonomy())) {
          return false;
        }
        
        // For MappedTaxonomy, resolve (in case it becomes a sp.).
        Resolved resolved = sighting.getTaxon().resolve(taxonomyStore.getTaxonomy());
        SightingTaxon parentOfAtLeastType = resolved.getParentOfAtLeastType(depth);
        return type == mapAwaySingleWithSubspecies(parentOfAtLeastType.getType());
      }

      /**
       * From the perspective of this code, SINGLE_WITH_SECONDARY_SUBSPECIES should
       * be treated identically to SINGLE.  Map the former to the latter.
       */
      private SightingTaxon.Type mapAwaySingleWithSubspecies(SightingTaxon.Type type) {
        if (type == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
          return SightingTaxon.Type.SINGLE;
        }
        return type;
      }
    };
    if (QueryType.IS_NOT.equals(queryOptions.getSelectedItem())) {
      predicate = Predicates.not(predicate);
    }
    
    return predicate;
  }

  @Override public boolean isNoOp() {
    return false;
  }

  @Override
  public JsonElement persist() {
    Persisted persisted = new Persisted(
        (QueryType) queryOptions.getSelectedItem(),
        (StatusType) typeOptions.getSelectedItem());
    return GSON.toJsonTree(persisted);
  }

  @Override
  public void restore(JsonElement json) {
    Persisted persisted = GSON.fromJson(json, Persisted.class);
    queryOptions.setSelectedItem(persisted.type);
    typeOptions.setSelectedItem(persisted.statusType);
  }
  
  static private final Gson GSON = new Gson();
  static class Persisted {
    public Persisted(QueryType type, StatusType statusType) {
      this.type = type;
      this.statusType = statusType;
    }
    
    QueryType type;
    StatusType statusType;
  }
}
