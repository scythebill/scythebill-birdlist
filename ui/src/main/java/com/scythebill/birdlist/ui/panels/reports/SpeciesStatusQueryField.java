/*   
 * Copyright 2012 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JComponent;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonSyntaxException;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;

/**
 * QueryField for adjusting queries relative to the status of a species.
 */
class SpeciesStatusQueryField extends AbstractQueryField {
  private enum QueryType {
    IS(Name.STATUS_IS),
    IS_NOT(Name.STATUS_IS_NOT),
    IS_OR_WORSE(Name.STATUS_IS_OR_WORSE);
    
    private final Name text;
    QueryType(Name text) {
      this.text = text;
    }
    
    @Override
    public String toString() {
      return Messages.getMessage(text);
    }
  }

  private final JComboBox<QueryType> queryOptions = new JComboBox<>(QueryType.values());
  private final JComboBox<String> statusOptions = new JComboBox<>(getStatusText());
  private final TaxonomyStore taxonomyStore;

  public SpeciesStatusQueryField(TaxonomyStore taxonomyStore) {
    super(QueryFieldType.SPECIES_STATUS);
    this.taxonomyStore = taxonomyStore;
    queryOptions.addActionListener(e -> firePredicateUpdated());
    statusOptions.addActionListener(e -> firePredicateUpdated());
    statusOptions.setMaximumRowCount(statusOptions.getItemCount());
  }

  private static String[] getStatusText() {
    return getValidStatuses()
        .stream()
        .map(Messages::getText)
        .toArray(String[]::new);
  }

  private static List<Status> getValidStatuses() {
    List<Status> statusValues = Lists.newArrayList(Status.values());
    // "Introduced" (Feral) is a special taxon status, not relevant here.
    statusValues.remove(Status.IN);
    // Ditto "Domestic"
    statusValues.remove(Status.DO);
    // Ditto "Undescribed"
    statusValues.remove(Status.UN);
    return statusValues;
  }

  @Override
  public JComponent getComparisonChooser() {
    return queryOptions;
  }

  @Override
  public JComponent getValueField() {
    return statusOptions;
  }

  @Override
  public Predicate<Sighting> predicate(Taxon.Type depth) {
    final Status status = getValidStatuses().get(statusOptions.getSelectedIndex());
    final boolean isOrWorse = QueryType.IS_OR_WORSE.equals(queryOptions.getSelectedItem());
    Predicate<Sighting> predicate = new Predicate<Sighting>() {
      @Override public boolean apply(Sighting sighting) {
        Taxonomy taxonomy = taxonomyStore.getTaxonomy();
        if (!TaxonUtils.areCompatible(taxonomy, sighting.getTaxonomy())) {
          return false;
        }
        // See if the sighting is of a species with that status
        // TODO: is resolving on every predicate evaluation going to be painfully expensive?
        // Test if Status queries are slower than others.
        Resolved resolved = sighting.getTaxon().resolve(taxonomy);
        // Bump up to the current minimum depth
        if (resolved.getSmallestTaxonType().isLowerLevelThan(depth)) {
          resolved = resolved.getParentOfAtLeastType(depth).resolveInternal(taxonomy);
        }
        
        Status taxonStatus = resolved.getTaxonStatus();
        // Never consider IN or DO or UN status species
        if (taxonStatus == Status.IN || taxonStatus == Status.DO || taxonStatus == Status.UN) {
          return false;
        }
        
        if (isOrWorse) {
          if (taxonStatus.compareTo(status) >= 0) {
            return true;
          }
        } else {
          if (taxonStatus == status) {
            return true;
          }
        }
        return false;
      }
    };
    if (QueryType.IS_NOT.equals(queryOptions.getSelectedItem())) {
      predicate = Predicates.not(predicate);
    }
    
    return predicate;
  }

  @Override public boolean isNoOp() {
    return false;
  }

  @Override
  public JsonElement persist() {
    Status status = getValidStatuses().get(statusOptions.getSelectedIndex());
    Persisted persisted = new Persisted(
        (QueryType) queryOptions.getSelectedItem(), status.name());
    return GSON.toJsonTree(persisted);
  }

  @Override
  public void restore(JsonElement json) {
    Persisted persisted = GSON.fromJson(json, Persisted.class);
    queryOptions.setSelectedItem(persisted.type);
    try {
      Status status = Status.valueOf(persisted.statusId);
      statusOptions.setSelectedItem(Messages.getText(status));
    } catch (IllegalArgumentException e) {
      throw new JsonSyntaxException("Invalid species status: " + persisted.statusId);
    }
  }
  
  static private final Gson GSON = new Gson();
  static class Persisted {
    public Persisted(QueryType type, String statusId) {
      this.type = type;
      this.statusId = statusId;
    }
    
    QueryType type;
    String statusId;
  }
}
