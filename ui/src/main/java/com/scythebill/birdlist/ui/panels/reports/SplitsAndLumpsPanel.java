/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.Dimension;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.app.Titled;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.events.TaxonomyChangedEvent;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;
import com.scythebill.birdlist.ui.panels.reports.SplitsAndLumpsProcessor.SplitsAndLumpsResult;
import com.scythebill.birdlist.ui.panels.reports.SplitsAndLumpsProcessor.SplitsAndLumpsResult.OldSpecies;
import com.scythebill.birdlist.ui.panels.reports.SplitsAndLumpsProcessor.SplitsAndLumpsResult.SingleResult;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * Panel for triggering the "Splits and Lumps" report.
 */
public class SplitsAndLumpsPanel extends JPanel implements FontManager.FontsUpdatedListener, Titled {

  private final ReportSet reportSet;
  private final QueryPanelFactory queryPanelFactory;
  private final TaxonomyStore taxonomyStore;
  private final QueryPreferences queryPreferences;
  private final NavigableFrame navigableFrame;
  private QueryPanel queryPanel;
  private JButton returnButton;
  private JLabel splitsAndLumpsExplanation;
  private JEditorPane splitsAndLumpsText;
  private JScrollPane splitsAndLumpsScrollPane;
  private JLabel versionLabel;
  private JComboBox<Version> versionCombobox;
  private JLabel splitsAndLumpsTotal;
  
  private final static String CURRENT_CLEMENTS_VERSION = "2024";
  private final static String CURRENT_IOC_VERSION = "15.1";
  
  static enum Version {
    CLEMENTS_2023("2023", "2023", CURRENT_CLEMENTS_VERSION),
    CLEMENTS_2022("2022", "2022", CURRENT_CLEMENTS_VERSION),
    CLEMENTS_2021("2021", "2021", CURRENT_CLEMENTS_VERSION),
    CLEMENTS_2019("2019", "2019", CURRENT_CLEMENTS_VERSION),
    CLEMENTS_2018("2018", "2018", CURRENT_CLEMENTS_VERSION),
    CLEMENTS_2017("2017", "2017", CURRENT_CLEMENTS_VERSION),
    CLEMENTS_2016("2016", "2016", CURRENT_CLEMENTS_VERSION),
    CLEMENTS_2015("2015", "2015", CURRENT_CLEMENTS_VERSION),
    CLEMENTS_69("6.9 (August 2014)", "69", CURRENT_CLEMENTS_VERSION),
    CLEMENTS_68("6.8 (August 2013)", "68", CURRENT_CLEMENTS_VERSION),
    CLEMENTS_67("6.7 (September 2012)", "67", CURRENT_CLEMENTS_VERSION),
    CLEMENTS_66("6.6 (August 2011)", "66", CURRENT_CLEMENTS_VERSION),
    CLEMENTS_65("6.5 (December 2010)", "65", CURRENT_CLEMENTS_VERSION),
    IOC_142("14.2 (August 2024)", "142", CURRENT_IOC_VERSION),
    IOC_141("14.1 (December 2023)", "141", CURRENT_IOC_VERSION),
    IOC_132("13.2 (July 2023)", "132", CURRENT_IOC_VERSION),
    IOC_131("13.1 (January 2023)", "131", CURRENT_IOC_VERSION),
    IOC_122("12.2 (August 2022)", "122", CURRENT_IOC_VERSION),
    IOC_121("12.1 (January 2022)", "121", CURRENT_IOC_VERSION),
    IOC_112("11.2 (July 2021)", "112", CURRENT_IOC_VERSION),
    IOC_111("11.1 (January 2021)", "111", CURRENT_IOC_VERSION),
    IOC_102("10.2 (July 2020)", "102", CURRENT_IOC_VERSION),
    IOC_101("10.1 (January 2020)", "101", CURRENT_IOC_VERSION),
    IOC_92("9.2 (June 2019)", "92", CURRENT_IOC_VERSION),
    IOC_91("9.1 (January 2019)", "91", CURRENT_IOC_VERSION),
    IOC_82("8.2 (June 2018)", "82", CURRENT_IOC_VERSION),
    IOC_81("8.1 (January 2018)", "81", CURRENT_IOC_VERSION),
    IOC_71("7.1 (January 2017)", "71", CURRENT_IOC_VERSION),
    IOC_61("6.1 (January 2016)", "61", CURRENT_IOC_VERSION),
    IOC_51("5.1 (January 2015)", "51", CURRENT_IOC_VERSION);

    private final String text;
    private final String id;
    private final String currentVersion;

    Version(String text, String id, String currentVersion) {
      this.text = text;
      this.id = id;
      this.currentVersion = currentVersion;
    };
    
    @Override
    public String toString() {
      return Messages.getFormattedMessage(Name.VERSUS_FORMAT, text, currentVersion);
    }
  }
  
  private static final ImmutableList<Version> IOC_VERSIONS = 
      Arrays.stream(Version.values())
          .filter(v -> v.name().startsWith("IOC_"))
          .collect(ImmutableList.toImmutableList());
  private static final ImmutableList<Version> CLEMENTS_VERSIONS = 
      Arrays.stream(Version.values())
          .filter(v -> v.name().startsWith("CLEMENTS_"))
          .collect(ImmutableList.toImmutableList());
  
  @Inject
  SplitsAndLumpsPanel(
      ReportSet reportSet,
      QueryPanelFactory queryPanelFactory,
      TaxonomyStore taxonomyStore,
      EventBusRegistrar eventBusRegistrar,
      FontManager fontManager,
      QueryPreferences queryPreferences,
      NavigableFrame navigableFrame,
      Alerts alerts) {
    this.reportSet = reportSet;
    this.queryPanelFactory = queryPanelFactory;
    this.taxonomyStore = taxonomyStore;
    this.queryPreferences = queryPreferences;
    this.navigableFrame = navigableFrame;
    initGUI();
    attachListeners();
    fontManager.applyTo(this);
    eventBusRegistrar.registerWhenInHierarchy(this);
    taxonomyChanged(null);
  }

  private void attachListeners() {
    returnButton.addActionListener(e -> navigableFrame.navigateTo("extendedReportsMenu"));
    queryPanel.addPredicateChangedListener(e -> showQueryResults());
    versionCombobox.addActionListener(e -> showQueryResults());
  }

  private void initGUI() {    
    queryPanel = queryPanelFactory.newQueryPanel(
        // Drop a few query types which are weird here given that the actual species aren't (much) displayed.
        QueryFieldType.SUBSPECIES_ALLOCATED,
        QueryFieldType.FIRST_SIGHTINGS,
        QueryFieldType.SP_OR_HYBRID,
        QueryFieldType.TIMES_SIGHTED);
    
    splitsAndLumpsTotal = new JLabel();
    
    splitsAndLumpsExplanation = new JLabel();
    splitsAndLumpsExplanation.putClientProperty(FontManager.PLAIN_LABEL, true);
    splitsAndLumpsExplanation.setText("<html>"
        + Messages.getMessage(Name.SPLITS_AND_LUMPS_FULL_EXPLANATION));
    returnButton = new JButton(Messages.getMessage(Name.BACK_TO_SPECIAL_REPORTS));
    
    versionLabel = new JLabel(Messages.getMessage(Name.TAXONOMY_LABEL));
    versionCombobox = new JComboBox<>();
    splitsAndLumpsText = new JEditorPane("text/html", "");
    splitsAndLumpsScrollPane = new JScrollPane(splitsAndLumpsText);
    splitsAndLumpsText.setEditable(false);
    splitsAndLumpsText.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, true);
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    // TODO: fix for monitors smaller than this
    Dimension size = fontManager.scale(new Dimension(1200, 720));
    setPreferredSize(size);

    setBorder(new EmptyBorder(
        fontManager.scale(20),
        fontManager.scale(20),
        fontManager.scale(20),
        fontManager.scale(20)));
    GroupLayout layout = new GroupLayout(this);
    layout.setHorizontalGroup(
        layout.createParallelGroup(Alignment.LEADING)
            .addGroup(
                layout.createSequentialGroup()
                   .addComponent(versionLabel)
                   .addComponent(versionCombobox, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
            .addComponent(queryPanel, PREFERRED_SIZE, PREFERRED_SIZE, Short.MAX_VALUE)
            .addComponent(splitsAndLumpsExplanation)
            .addComponent(splitsAndLumpsTotal)
            .addComponent(splitsAndLumpsScrollPane)
            .addComponent(returnButton, Alignment.TRAILING));
    
    layout.setVerticalGroup(
        layout.createSequentialGroup()
            .addGroup(layout.createBaselineGroup(false, false)
                .addComponent(versionLabel)
                .addComponent(versionCombobox, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
            .addPreferredGap(ComponentPlacement.UNRELATED)
            .addComponent(queryPanel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addPreferredGap(ComponentPlacement.UNRELATED)
            .addComponent(splitsAndLumpsExplanation)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(splitsAndLumpsTotal)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(splitsAndLumpsScrollPane, fontManager.scale(80), PREFERRED_SIZE, Short.MAX_VALUE)
            .addPreferredGap(ComponentPlacement.UNRELATED)
            .addGroup(
                layout.createBaselineGroup(false, false).addComponent(returnButton)));
    
    setLayout(layout);
  }
  
 
  @Subscribe
  public void taxonomyChanged(TaxonomyChangedEvent event) {
    Version[] versions = (taxonomyStore.getTaxonomy() instanceof MappedTaxonomy
        ? IOC_VERSIONS : CLEMENTS_VERSIONS).toArray(new Version[0]);
    
    queryPanel.taxonomyUpdated();
    versionCombobox.setModel(
        new DefaultComboBoxModel<>(versions));
    showQueryResults();
  }
  
  private void showQueryResults() {
    SplitsAndLumpsResult result = computeSplitsAndLumps();
    StringBuilder builder = new StringBuilder("<html>");
    if (result.isEmpty()) {
      builder.append(Messages.getMessage(Name.NO_SPLITS_OR_LUMPS_FOUND));
      splitsAndLumpsTotal.setText("");
    } else {
      StringBuilder total = new StringBuilder();
      total.append(Messages.getFormattedMessage(Name.GAINED_FORMAT, result.knownSplitCount));
      if (result.possibleSplitCount > 0) {
        total.append(" ");
        total.append(Messages.getFormattedMessage(Name.PLUS_POSSIBLE_FORMAT, result.possibleSplitCount));
      }

      total.append(" ");
      total.append(Messages.getFormattedMessage(Name.LOST_FORMAT, result.knownLumpCount));
      if (result.possibleLumpCount > 0) {
        total.append(" ");
        total.append(Messages.getFormattedMessage(Name.PLUS_POSSIBLE_FORMAT, result.possibleLumpCount));
      }
      
      splitsAndLumpsTotal.setText(total.toString());
      
      if (!result.splits.isEmpty()) {
        builder.append("<h1>").append(Messages.getMessage(Name.SPLITS_TEXT)).append("</h1>");
        builder.append("<ul>\n");
        for (SingleResult split : result.splits) {
          builder.append("<li>");
          builder.append(singleResultAsString(split));
        }
        builder.append("</ul>\n");
      }
        
      if (!result.possibleSplits.isEmpty()) {
        builder.append("<h1>").append(Messages.getMessage(Name.POSSIBLE_SPLITS)).append("</h1>");
        builder.append("<ul>\n");
        for (SingleResult split : result.possibleSplits) {
          builder.append("<li>");
          builder.append(singleResultAsString(split));
        }
        builder.append("</ul>\n");
      }

      if (!result.lumps.isEmpty()) {
        builder.append("<h1>").append(Messages.getMessage(Name.LUMPS_TEXT)).append("</h1>");
        builder.append("<ul>\n");
        for (SingleResult lump : result.lumps) {
          builder.append("<li>");
          builder.append(singleResultAsString(lump));
        }

        builder.append("</ul>\n");
      }

      if (!result.possibleLumps.isEmpty()) {
        builder.append("<h1>").append(Messages.getMessage(Name.POSSIBLE_LUMPS)).append("</h1>");
        builder.append("<ul>\n");
        for (SingleResult lump : result.possibleLumps) {
          builder.append("<li>");
          builder.append(singleResultAsString(lump));
        }

        builder.append("</ul>\n");
      }

      if (!result.possibleLumpOrSplit.isEmpty()) {
        builder.append("<h1>").append(Messages.getMessage(Name.UNKNOWN_LUMPS_OR_SPLITS)).append("</h1>");
        builder.append("<ul>\n");
        for (SingleResult shrug : result.possibleLumpOrSplit) {
          builder.append("<li>");
          builder.append(singleResultAsString(shrug));
        }

        builder.append("</ul>\n");
      }
    }
    
    splitsAndLumpsText.setText(builder.toString());
    splitsAndLumpsText.setCaretPosition(0);
  }
  
  private String singleResultAsString(SingleResult result) {
    Set<String> encounteredGenera = new HashSet<>();
    String newTaxa = Messages.join(result.currentTaxa, currentTaxon -> currentTaxonAsString(currentTaxon, encounteredGenera));
    if (result.oldTaxa.isEmpty()) {
      String oldTaxa = Messages.join(result.possibleOldTaxa, oldSpecies -> oldSpeciesAsString(oldSpecies, encounteredGenera));
      Name name = result.oldTaxa.size() == 1
          ? Name.SOME_OR_ALL_OF_IS_NOW_FORMAT
          : Name.SOME_OR_ALL_OF_ARE_NOW_FORMAT;
      return Messages.getFormattedMessage(
          name, oldTaxa, newTaxa);
    } else {
      String oldTaxa = Messages.join(result.oldTaxa, oldSpecies -> oldSpeciesAsString(oldSpecies, encounteredGenera));
      if (!result.possibleOldTaxa.isEmpty()) {
        if (result.possibleOldTaxa.size() == 1) {
          Name name = result.oldTaxa.size() == 1
              ? Name.SOME_OR_ALL_OF_AND_POSSIBLY_IS_NOW_FORMAT
              : Name.SOME_OR_ALL_OF_AND_POSSIBLY_ARE_NOW_FORMAT;
          return Messages.getFormattedMessage(
              name,
              oldTaxa,
              oldSpeciesAsString(result.possibleOldTaxa.get(0), encounteredGenera),
              newTaxa);
        } else {
          Name name = result.oldTaxa.size() == 1
              ? Name.SOME_OR_ALL_OF_AND_SOME_NONE_ALL_IS_NOW_FORMAT
              : Name.SOME_OR_ALL_OF_AND_SOME_NONE_ALL_ARE_NOW_FORMAT;
          String possibleOldTaxa = Messages.join(result.possibleOldTaxa, oldSpecies -> oldSpeciesAsString(oldSpecies, encounteredGenera));
          return Messages.getFormattedMessage(
              name,
              oldTaxa,
              possibleOldTaxa,
              newTaxa);
        }
      } else {
        Name name = result.oldTaxa.size() == 1
            ? Name.IS_NOW_FORMAT
            : Name.ARE_NOW_FORMAT;
        return Messages.getFormattedMessage(
            name, oldTaxa, newTaxa);
      }
    }
  }
  
  private String oldSpeciesAsString(OldSpecies oldSpecies, Set<String> encounteredGenera) {
    String sci = oldSpecies.scientific;
    String currentGenus = sci.substring(0, sci.indexOf(' '));
    if (!encounteredGenera.add(currentGenus)) {
      sci = currentGenus.charAt(0) + ". " + sci.substring(sci.indexOf(' ') + 1);
    }
    return String.format("<b>%s</b> (<i>%s</i>)", oldSpecies.common, sci);
  }
  
  private String currentTaxonAsString(String id, Set<String> encounteredGenera) {
    Taxon taxon = taxonomyStore.getTaxonomy().getTaxon(id);
    Taxon genus = TaxonUtils.getParentOfType(taxon, Taxon.Type.genus);
    String sciName;
    if (encounteredGenera.add(genus.getName())) {
      sciName = TaxonUtils.getFullName(taxon);
    } else {
      sciName = TaxonUtils.getAbbreviatedName(taxon);
    }
    return String.format("<b>%s</b> (<i>%s</i>)", taxon.getCommonName(), sciName);
  }

  private SplitsAndLumpsResult computeSplitsAndLumps() {
    Taxonomy taxonomy = taxonomyStore.getTaxonomy();
    Predicate<Sighting> countablePredicate = queryPreferences.getCountablePredicate(
        taxonomy, 
        queryPanel.containsQueryFieldType(QueryFieldType.SIGHTING_STATUS),
        queryPanel.getBooleanValueIfPresent(QueryFieldType.ALLOW_HEARD_ONLY));

    Version version = (Version) versionCombobox.getSelectedItem();
    if (version == null) {
      return new SplitsAndLumpsResult();
    }
    
    SplitsAndLumpsProcessor processor = new SplitsAndLumpsProcessor();
    return processor.process(
        reportSet, taxonomy, queryPanel.queryDefinition(taxonomy, Type.species), countablePredicate, version.id);
  }

  @Override
  public String getTitle() {
    return Messages.getMessage(Name.SPLITS_AND_LUMPS);
  }
}
