/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.primitives.Ints;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.query.QueryDefinition;
import com.scythebill.birdlist.model.query.QueryProcessor;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Handles the data processing for producing "split and lump" reports.
 */
class SplitsAndLumpsProcessor {
  private static final String CLEMENTS_REPORT_NAME_PATTERN = "/report-%s.csv";
  private static final String IOC_REPORT_NAME_PATTERN = "/report-ioc-%s.csv";
  private static final Splitter SLASH_SPLITTER = Splitter.on('/');
  
  /** The overall result for a split and lump report. */
  static class SplitsAndLumpsResult {
    /** A single "old species" - since the actual taxonomy isn't available, enough to be useful. */
    static class OldSpecies {
      private OldSpecies(String oldSci, BundleDefinition bundleDefinition) {
        this.scientific = oldSci;
        this.common = bundleDefinition.oldSciToOldCommon.get(oldSci);
      }
      
      final String scientific;
      final String common;
    }
    
    /** A single overall result - a list of old species to a list of current species. */
    static class SingleResult {
      List<String> currentTaxa;
      List<OldSpecies> oldTaxa;
      List<OldSpecies> possibleOldTaxa;      
    }
    
    List<SingleResult> splits = new ArrayList<>();
    List<SingleResult> lumps = new ArrayList<>();
    List<SingleResult> possibleSplits = new ArrayList<>();
    List<SingleResult> possibleLumps = new ArrayList<>();
    List<SingleResult> possibleLumpOrSplit = new ArrayList<>();
    int knownSplitCount = 0;
    int knownLumpCount = 0;
    int possibleSplitCount = 0;
    int possibleLumpCount = 0;
    
    /** True for an empty report with no splits or lumps. */
    public boolean isEmpty() {
      return splits.isEmpty()
          && lumps.isEmpty()
          && possibleSplits.isEmpty()
          && possibleLumps.isEmpty()
          && possibleLumpOrSplit.isEmpty();
    }

    /** Transforms a result from intermediate processing into one suitable for display. */ 
    private void addBundle(BundleResult bundleResult) {
      int currentSize = bundleResult.currentTaxaInBundle.size();
      int knownOldSize = bundleResult.knownOldSci.size();
      int possibleOldSize = bundleResult.possibleOldSci.size();
      // If *all* the old items are simply "possible", clearly at least one is known,
      // though no way to know which one.
      if (knownOldSize == 0) {
        knownOldSize = 1;
        possibleOldSize--;
      }
      
      // Similarly, should we ever stumble onto a case where there's exactly
      // one "possible" and no "known", that one is clearly actually known
      // (and the knownOldSize and possibleOldSize bit above matches).
      if (bundleResult.knownOldSci.size() == 0
          && bundleResult.possibleOldSci.size() == 1) {
        bundleResult.knownOldSci.addAll(bundleResult.possibleOldSci);
        bundleResult.possibleOldSci.clear();
      }
      
      // Nothing ambiguous... the easy case.
      if (possibleOldSize == 0) {
        // Same size before and after
        if (currentSize == knownOldSize) {
          return;
        }
        
        SingleResult result = new SingleResult();
        result.currentTaxa = ImmutableList.copyOf(bundleResult.currentTaxaInBundle);
        result.oldTaxa = bundleResult.knownOldSci.stream().map(
            oldSci -> new OldSpecies(oldSci, bundleResult.bundleDefinition))
            .collect(ImmutableList.toImmutableList());
        result.possibleOldTaxa = ImmutableList.of();
            
        // Lump
        if (currentSize < knownOldSize) {
          lumps.add(result);
          knownLumpCount += knownOldSize - currentSize;
        } else {
          splits.add(result);
          knownSplitCount += currentSize - knownOldSize;
        }
      } else {
        // Ambiguity!
        SingleResult result = new SingleResult();
        result.currentTaxa = ImmutableList.copyOf(bundleResult.currentTaxaInBundle);
        result.oldTaxa = bundleResult.knownOldSci.stream().map(
            oldSci -> new OldSpecies(oldSci, bundleResult.bundleDefinition))
            .collect(ImmutableList.toImmutableList());
        result.possibleOldTaxa = bundleResult.possibleOldSci.stream().map(
            oldSci -> new OldSpecies(oldSci, bundleResult.bundleDefinition))
            .collect(ImmutableList.toImmutableList());

        if (currentSize < knownOldSize) {
          if (currentSize + possibleOldSize > knownOldSize) {
            // The most ambiguous case:  could be lumps *or* splits!
            possibleLumpOrSplit.add(result);
            possibleLumpCount += knownOldSize - currentSize;
            possibleSplitCount += (currentSize + possibleOldSize) - knownOldSize;
          } else if (currentSize + possibleOldSize == knownOldSize) {
            // It's possible that there's no lumps
            possibleLumps.add(result);
            possibleLumpCount += possibleOldSize;
          } else {
            // Definitely at least *some* lumps
            knownLumpCount += knownOldSize - (currentSize + possibleOldSize);
            possibleLumpCount += possibleOldSize;
            possibleLumps.add(result);
          }
        } else if (currentSize == knownOldSize) {
          possibleLumpCount += possibleOldSize;
          possibleLumps.add(result);
        } else {
          // Must be a possible split
          knownSplitCount += currentSize - knownOldSize - possibleOldSize;
          possibleSplitCount += possibleOldSize;
          if (currentSize > knownOldSize + possibleOldSize) {
            splits.add(result);
          } else {
            possibleSplits.add(result);
          }
        }
      }
    }

  }

  public SplitsAndLumpsProcessor() {}

  public SplitsAndLumpsResult process(
      ReportSet reportSet,
      Taxonomy taxonomy,
      QueryDefinition queryDefinition,
      Predicate<Sighting> countablePredicate,
      String version) {
    String name = String.format(
        (taxonomy instanceof MappedTaxonomy) ? IOC_REPORT_NAME_PATTERN : CLEMENTS_REPORT_NAME_PATTERN,
        version);
    
    ParsedReport parsedReport = new ParsedReport();
    try {
      parsedReport.parse(taxonomy, name);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    
    QueryProcessor queryProcessor = new QueryProcessor(reportSet, taxonomy);
    queryProcessor.onlyIncludeCountableSightings().dontIncludeFamilyNames();
    // Run the query at the finest level 
    QueryResults queryResults = queryProcessor.runQuery(
        queryDefinition,
        countablePredicate,
        Taxon.Type.species);
    
    // Keep bundle results sorted by the bundle index, which keeps
    // things basically taxonomically sorted.
    Map<Integer, BundleResult> bundleResults = new TreeMap<>();
    
    for (Sighting sighting : queryResults.getAllSightings()) {
      // Resolve into the current taxonomy
      Resolved resolved = sighting.getTaxon().resolve(taxonomy);
      SightingTaxon sightingTaxon = resolved.getSightingTaxon();
      if (sightingTaxon.getType() == SightingTaxon.Type.HYBRID
          || sightingTaxon.getType() == SightingTaxon.Type.SP) {
        // Ignore any sightings that are cross-species hybrids (though,
        // perhaps, they *could* resolve as a loss of a specie?  Consider
        // later.
        if (resolved.getSmallestTaxonType() == Taxon.Type.species) {
          continue;
        }
        
        Resolved speciesResolved = resolved.resolveParentOfType(Taxon.Type.species);
        // If it's a cross-species
        if (speciesResolved.getType() == SightingTaxon.Type.SP
            || speciesResolved.getType() == SightingTaxon.Type.HYBRID) {
          continue;
        }
      }
      
      // See if it falls into a "bundle" (if not, then it's not affected
      // by any splits and lumps)
      Integer bundleIndex = parsedReport.getBundleIndex(sightingTaxon);
      if (bundleIndex == null) {
        continue;
      }
      
      BundleDefinition bundleDefinition =
          Preconditions.checkNotNull(parsedReport.bundlesByIndex.get(bundleIndex));
      BundleResult result = bundleResults.computeIfAbsent(bundleIndex, index -> new BundleResult(bundleDefinition));
      
      result.processResolved(resolved);
    }
    
    SplitsAndLumpsResult results = new SplitsAndLumpsResult();
    for (BundleResult bundleResult : bundleResults.values()) {
      results.addBundle(bundleResult);
    }
    
    return results;
  }
  
  /** An internal result of analysis for a single "bundle" (set of old->current species). */
  private static class BundleResult {
    /** List of current taxa IDs (all species) that are in the bundle. */
    final Set<String> currentTaxaInBundle = new LinkedHashSet<>();
    /**
     * Old scientific names that are guaranteed to be present, because a current sighting
     * mapped uniquely to it.
     */
    final Set<String> knownOldSci = new LinkedHashSet<>();
    /**
     * Old scientific names that *might* be present, because a current sighting mapped
     * to it and to other old names as well. 
     */
    final Set<String> possibleOldSci = new LinkedHashSet<>();
    final BundleDefinition bundleDefinition;
    
    BundleResult(BundleDefinition bundleDefinition) {
      this.bundleDefinition = bundleDefinition;
    }
    
    public void processResolved(Resolved resolved) {
      SightingTaxon sightingTaxon = resolved.getSightingTaxon();
      // Figure out which old taxa are implied by the set of IDs present
      Set<String> oldTaxa = new LinkedHashSet<>();
      for (String id : sightingTaxon.getIds()) {
        oldTaxa.addAll(bundleDefinition.currentIdsToOldSci.get(id));
      }

      Resolved parent = resolved.resolveParentOfType(Taxon.Type.species);
      currentTaxaInBundle.add(parent.getSightingTaxon().getId());
      
      if (oldTaxa.size() == 1) {
        // Mapped to one - this is known explicitly
        knownOldSci.addAll(oldTaxa);
        // ... and can be removed from the list of "possible" taxa
        possibleOldSci.removeAll(oldTaxa);
      } else {
        // Mapped to more than one.  There is remaining uncertainty!  

        // Anything explicitly known shouldn't be added to "possible" taxa
        oldTaxa.removeAll(knownOldSci);
        
        // Whatever's left, goes onto the "possible" list
        possibleOldSci.addAll(oldTaxa);
      }      
    }
  }

  /**
   * The parsed version of a CSV file describing taxonomic differences.
   */
  private static class ParsedReport {
    private Map<Integer, BundleDefinition> bundlesByIndex = new LinkedHashMap<>();
    private Map<String, Integer> currentIdToBundleIndex = new LinkedHashMap<>();
    
    void parse(Taxonomy taxonomy, String resourceName) throws IOException {
      URL resource = Preconditions.checkNotNull(getClass().getResource(resourceName), "Could not load %s", resourceName);
      ImportLines importLines = CsvImportLines.fromUrl(resource, StandardCharsets.UTF_8);
      
      // Skip the header
      importLines.nextLine();
      while (true) {
        String[] line = importLines.nextLine();
        if (line == null) {
          break;
        }
        Integer index = Ints.tryParse(line[0]);
        if (index == null) {
          throw new IllegalStateException("Could not parse index " + line[0]);
        }
        
        BundleDefinition bundle = bundlesByIndex.computeIfAbsent(index, __ -> new BundleDefinition());
        List<String> currentIds = SLASH_SPLITTER.splitToList(line[4]);
        List<String> oldCommonNames = SLASH_SPLITTER.splitToList(line[2]);
        List<String> oldSciNames = SLASH_SPLITTER.splitToList(line[3]);
        Preconditions.checkState(oldCommonNames.size() == oldSciNames.size());
        
        // Store the mapping of old scientific names to old common names 
        for (int i = 0; i < oldCommonNames.size(); i++) {
          bundle.oldSciToOldCommon.put(oldSciNames.get(i), oldCommonNames.get(i));
        }
        
        if (line[1].equals("simple")) {
          // A "simple" mapping: should be just one old name, and N current species.
          // Groups and subspecies are omitted from the file.
          String oldSciName = Iterables.getOnlyElement(oldSciNames);
          for (String currentId : currentIds) {
            // Visit groups/subspecies too
            TaxonUtils.visitTaxa(taxonomy.getTaxon(currentId), new TaxonVisitor() {
              @Override
              public boolean visitTaxon(Taxon taxon) {
                bundle.currentIdsToOldSci.put(taxon.getId(), oldSciName);
                currentIdToBundleIndex.put(taxon.getId(), index);
                return true;
              }
            });
          }
        } else if (line[1].equals("complex")) {
          // A "complex" mapping: N old names, and N current taxa (species, groups, and subspecies)
          for (String currentId : currentIds) {
            for (String oldSciName : oldSciNames) {
              bundle.currentIdsToOldSci.put(currentId, oldSciName);
              currentIdToBundleIndex.put(currentId, index);
            }
          }        
        } else {
          throw new IllegalStateException("unexpected type " + line[1]);
        }
      }
    }

    Integer getBundleIndex(SightingTaxon sightingTaxon) {
      String id = sightingTaxon.getIds().iterator().next();
      return currentIdToBundleIndex.get(id);
    }
  }
  
  /**
   * Data for a single bundle definition.
   */
  private static class BundleDefinition {
    private Multimap<String, String> currentIdsToOldSci = LinkedHashMultimap.create();
    private Map<String, String> oldSciToOldCommon = new LinkedHashMap<>();
  }
}
