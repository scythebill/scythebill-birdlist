/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.awt.Component;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.swing.JOptionPane;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.query.QueryDefinition;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.CompositeQueryDefinition.BooleanOperation;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryField;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;
import com.scythebill.birdlist.ui.panels.reports.TotalTicksProcessor.TotalTickType;
import com.scythebill.birdlist.ui.prefs.ReportSetPreference;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * Handles storing queries that are saved during navigation
 * and across reloads.
 */
public class StoredQueries {
  private static final Logger logger = Logger.getLogger(StoredQueries.class.getName());

  private final List<StoredQuery> queries = Lists.newArrayList();
  private final QueryFieldFactory factory;
  private final StoredQueriesPreferences preferences;
  private final ReportSetPreference<StoredReportSetQueriesPreferences> reportSetPreferences;
  private String defaultQuery;

  private final Alerts alerts;

  @Inject
  StoredQueries(
      QueryFieldFactory factory,
      Alerts alerts,
      StoredQueriesPreferences storedQueriesPreferences,
      // Is null during the resolve-taxa step
      @Nullable ReportSetPreference<StoredReportSetQueriesPreferences> reportSetPreferences) {
    this.factory = factory;
    this.alerts = alerts;
    this.preferences = storedQueriesPreferences;
    this.reportSetPreferences = reportSetPreferences;
    try {
      restoreFromPreferences();
    } catch (RuntimeException e) {
      // Catch *any* runtime exception here (there's lots of possibilities, any one of which
      // would totally break the UI)
      logger.warning("Could not restore from preferences: " + preferences.storedQueries);
    }
  }
  
  public void setDefaultQuery(String defaultQuery) {
    this.defaultQuery = defaultQuery;
  }
  
  public String getDefaultQuery() {
    return defaultQuery;
  }
  
  public List<StoredQuery> getStoredQueries() {
    return Collections.unmodifiableList(queries);
  }
  
  public String chooseName(Component parentComponent, String defaultName) {
    while (true) {
      String name = alerts.getInput(parentComponent,
          Name.NAME_THIS_REPORT_TITLE,
          Name.NAME_THIS_REPORT_MESSAGE,
          defaultName);
      if (name == null) {
        return null;
      }
      
      if (name.isEmpty()) {
        alerts.showMessage(parentComponent,
            Name.NAME_CANT_BE_EMPTY_TITLE,
            Name.NAME_CANT_BE_EMPTY_MESSAGE);
      } else {
        boolean nameExists = false;
        // See if the name already exists
        for (StoredQuery query : getStoredQueries()) {
          if (query.name.equals(name)) {
            int showOkCancel = alerts.showOkCancel(
                parentComponent,
                Name.NAME_ALREADY_TAKEN_TITLE,
                Name.NAME_ALREADY_TAKEN_MESSAGE,
                name);
            // If OK, then this is fine, return the name
            if (showOkCancel == JOptionPane.OK_OPTION) {
              break;
            }
            
            // ... OK it does, but instead of going back to the dumb default,
            // use this manually entered name.
            nameExists = true;
            defaultName = name;
            break;
          }
        }
        if (!nameExists) {
          return name;
        }
      }
    }
  }

  public enum StoredQueryType {
    // QueryType for the 
    REPORTS,
    TOTAL_TICKS
  }
  
  /** Class representing a single stored query, name and JSON. */
  public class StoredQuery {
    final String name;
    private String persistedQuery;
    StoredQueryType storedQueryType;
    
    StoredQuery(String name, String persistedQuery, StoredQueryType storedQueryType) {
      this.name = Preconditions.checkNotNull(name);
      this.persistedQuery = Preconditions.checkNotNull(persistedQuery);
      this.storedQueryType = Preconditions.checkNotNull(storedQueryType);
    }
    
    public Optional<RestoredQuery> restore() {
      return StoredQueries.this.restore(queryJson());
    }
    
    /** Returns the total tick type, if available. */
    public TotalTickType getTotalTickType() {
      if (storedQueryType != StoredQueryType.TOTAL_TICKS) {
        throw new IllegalStateException("Type is " + storedQueryType);
      }
      
      int slashIndex = persistedQuery.indexOf('/');
      if (slashIndex < 0) {
        throw new IllegalStateException("Could not find ticks type in " + persistedQuery);
      }
      
      return TotalTickType.valueOf(persistedQuery.substring(0, slashIndex));
    }
    
    public String getName() {
      return name;
    }

    @Override
    public boolean equals(Object obj) {
      if (!(obj instanceof StoredQuery)) {
        return false;
      }
      
      if (obj == this) {
        return true;
      }
      
      StoredQuery that = (StoredQuery) obj;
      return Objects.equal(this.name, that.name);
    }

    @Override
    public int hashCode() {
      return Objects.hashCode(name);
    }

    @Override
    public String toString() {
      return MoreObjects.toStringHelper(this)
          .add("name", name)
          .add("persistedQuery", persistedQuery)
          .toString();
    }

    /** Find the persisted JSON substring of the overall persisted query. */
    public String queryJson() {
      if (storedQueryType == StoredQueryType.REPORTS) {
        // Reports:  all of the query is the query json.
        return persistedQuery;
      } else if (storedQueryType == StoredQueryType.TOTAL_TICKS) {
        int slashIndex = persistedQuery.indexOf('/');
        if (slashIndex < 0) {
          throw new IllegalStateException("Could not find ticks type in " + persistedQuery);
        }
        
        return persistedQuery.substring(slashIndex + 1);
      } else {
        throw new IllegalStateException("Unexpected query type: " + storedQueryType);
      }
    }

  }
  
  public void addTotalTicksQuery(
      String name,
      TotalTickType totalTickType,
      String persistedJson) {
    // Persist the query as "COUNTY/{jsonblob}"
    String typePlusQuery =
        totalTickType.name() + "/" + persistedJson;
    addQuery(name, StoredQueryType.TOTAL_TICKS, typePlusQuery);
  }
  
  public void addReportsQuery(
      String name, String persistedJson) {
    addQuery(name, StoredQueryType.REPORTS, persistedJson);
  }
      
  private void addQuery(String name, StoredQueryType storedQueryType, String persistedJson) {
    // See if the name already exists, in which case overwrite it
    for (StoredQuery query : queries) {
      if (name.equals(query.name)) {
        query.persistedQuery = persistedJson;
        query.storedQueryType = storedQueryType;
        updatePreferences();
        return;
      }
    }
    
    StoredQuery storedQuery = new StoredQuery(name, persistedJson, storedQueryType);
    queries.add(storedQuery);
    
    updatePreferences();
  }
  
  public void removeQuery(StoredQuery query) {
    queries.remove(query);
    updatePreferences();
  }

  public String persist(
      List<QueryField> fields,
      List<BooleanOperation> booleanOperations) {
    Preconditions.checkState(fields.size() == booleanOperations.size());
    
    JsonArray array = new JsonArray();
    for (int i = 0; i < fields.size(); i++) {
      QueryField field = fields.get(i);
      BooleanOperation booleanOperation = booleanOperations.get(i);
      
      JsonObject object = new JsonObject();
      object.addProperty("type", field.getType().name());
      object.add("value", field.persist());
      object.addProperty("bool", booleanOperation.name());
      array.add(object);
    }
    JsonObject query = new JsonObject();
    query.add("fields", array);
    return new Gson().toJson(query);
  }

  public Optional<RestoredQuery> restore(String json) throws JsonParseException {
    JsonObject query = new JsonParser().parse(json).getAsJsonObject();
    // See if there's a common "ALL/ANY" saved.  If so, use that as the default for each query.
    BooleanOperation legacyBooleanOperation = null;
    if (query.has("type")) {
      try {
        legacyBooleanOperation = BooleanOperation.valueOf(query.get("type").getAsString());
      } catch (IllegalArgumentException e) {
        logger.log(Level.WARNING, "Invalid JSON: " + query, e);
        return Optional.absent();
      }
    }
    
    JsonArray array = query.get("fields").getAsJsonArray();
    Iterator<JsonElement> elements = array.iterator();
    
    List<QueryField> fields = Lists.newArrayList();
    List<BooleanOperation> queryTypes = Lists.newArrayList();
    while (elements.hasNext()) {
      JsonObject object = elements.next().getAsJsonObject();
      try {
        QueryFieldType fieldType = QueryFieldType.valueOf(object.get("type").getAsString());
        QueryField queryField = factory.newField(fieldType);
        queryField.restore(object.get("value"));
        BooleanOperation booleanOperation = legacyBooleanOperation;
        if (object.has("bool")) {
          try {
            booleanOperation = BooleanOperation.valueOf(object.get("bool").getAsString());
          } catch (IllegalArgumentException e) {
            logger.log(Level.WARNING, "Invalid JSON: " + query, e);
            return Optional.absent();
          }
        }
        if (booleanOperation == null) {
          booleanOperation = BooleanOperation.ALL;
        }
        queryTypes.add(booleanOperation);
        
        fields.add(queryField);
      } catch (RuntimeException e) {
        // Catch any runtime exception here;  there's lots, any one of which would
        // break the UI totally.
        logger.log(Level.WARNING, "Invalid JSON: " + object, e);
        return Optional.absent();
      }
    }
    
    return Optional.of(new RestoredQuery(queryTypes, fields));
  }
  
  public static class RestoredQuery {
    final ImmutableList<BooleanOperation> booleanOperations;
    final ImmutableList<QueryField> fields;
    RestoredQuery(List<BooleanOperation> booleanOperations, List<QueryField> fields) {
      this.booleanOperations = ImmutableList.copyOf(booleanOperations);
      this.fields = ImmutableList.copyOf(fields);
    }
    
    public boolean containsQueryFieldType(QueryFieldType type) {
      return fields.stream().anyMatch(field -> field.getType() == type);
    }
    
    public QueryDefinition getQueryDefinition(Taxonomy taxonomy, ReportSet reportSet,
        QueryPreferences queryPreferences, Taxon.Type depth, Predicate<Sighting> additionalPredicate) {
      final CompositeQueryDefinition queryDefinition = new CompositeQueryDefinition(
          reportSet,
          fields,
          booleanOperations,
          taxonomy,
          depth);

      Predicate<Sighting> countablePredicate = countablePredicate(taxonomy, queryPreferences);
      Predicate<Sighting> intersection = countablePredicate == null
          ? additionalPredicate
          : Predicates.and(additionalPredicate, countablePredicate);
      return new QueryDefinition() {
        @Override
        public Optional<Preprocessor> preprocessor() {
          return queryDefinition.preprocessor();
        }
        
        @Override
        public Predicate<Sighting> predicate() {
          return Predicates.and(
              queryDefinition.predicate(), intersection);
        }
        
        @Override
        public Optional<QueryAnnotation> annotate(Sighting sighting, SightingTaxon taxon) {
          return queryDefinition.annotate(sighting, taxon);
        }
        
        @Override
        public Optional<PostProcessor> postprocessor() {
          return queryDefinition.postprocessor();
        }
      };
    }

    /**
     * Return the countable predicate, or null if there is nul
     */
    Predicate<Sighting> countablePredicate(
        Taxonomy taxonomy,
        QueryPreferences queryPreferences) {
      // If this is true, then we need to "and" the sighting predicate
      // with one more status predicate to exclude (e.g.) escapees, for
      // consistency with ReportsPanel.  (That code does it with an
      // explicitly separate "countable" predicate, but since this one is only
      // used for numbers it's simpler to pre-intersect)
      boolean skipStatusIntersection = false;
      Optional<Boolean> heardOnlyQuery = null;
      for (int i = 0; i < fields.size(); i++) {
        QueryField field = fields.get(i);
        // If there's a status field decision, then don't bother applying
        // the status intersection.
        if (field.getType() == QueryFieldType.SIGHTING_STATUS) {
          skipStatusIntersection = true;
        }

        // If there's a heard only field, then don't bother applying
        // the heard-only intersection.
        if (field.getType() == QueryFieldType.ALLOW_HEARD_ONLY) {
          heardOnlyQuery = field.getBooleanValue();
          if (heardOnlyQuery.isPresent()
              && (booleanOperations.get(i) == BooleanOperation.NEVER
                  || booleanOperations.get(i) == BooleanOperation.NEVER_ALL)) {
            // Flip "never" values
            heardOnlyQuery = Optional.of(!heardOnlyQuery.get());
          }
        }
      }
      
      return queryPreferences.getCountablePredicate(taxonomy, skipStatusIntersection, heardOnlyQuery);
    }
  }

  private void updatePreferences() {
    JsonArray array = new JsonArray();
    for (StoredQuery query : queries) {
      JsonObject object = new JsonObject();
      object.addProperty("name", query.name);
      object.addProperty("json", query.persistedQuery);
      object.addProperty("type", query.storedQueryType.name());
      array.add(object);
    }
    // Save the preferences, *and* mark the reportset dirty.
    saveStoredJson(array);
    reportSetPreferences.save(true);
  }
  
  private void restoreFromPreferences() {
    String storedJson = getStoredJson();
    if (!Strings.isNullOrEmpty(storedJson)) {
      try {
        JsonArray array = new JsonParser().parse(storedJson).getAsJsonArray();        
        queries.clear();
        Iterator<JsonElement> iterator = array.iterator();
        while (iterator.hasNext()) {
          JsonObject jsonObject = iterator.next().getAsJsonObject();
          String name = jsonObject.getAsJsonPrimitive("name").getAsString();
          String json = jsonObject.getAsJsonPrimitive("json").getAsString();
          StoredQueryType type = StoredQueryType.REPORTS;
          // Parse a type, but accept if it's missing
          if (jsonObject.has("type")) {
            try {
              type = StoredQueryType.valueOf(jsonObject.getAsJsonPrimitive("type").getAsString());
            } catch (IllegalArgumentException e) {
              logger.log(Level.WARNING, "Couldn't parse query type", e);
            }
          } else {
            // The early total-ticks code didn't add a type, and just
            // relied on a prefix.
            if (json.startsWith("COUNTRY/")
                || json.startsWith("STATE/")
                || json.startsWith("COUNTY/")) {
              type = StoredQueryType.TOTAL_TICKS;
            }
          }
          queries.add(new StoredQuery(name, json, type));
        }
      } catch (JsonParseException e) {
        logger.log(Level.WARNING, "Could not parse saved queries", e);
      }
    }
    
    JsonArray array = new JsonArray();
    for (StoredQuery query : queries) {
      JsonObject object = new JsonObject();
      object.addProperty("name", query.name);
      object.addProperty("json", query.persistedQuery);
      array.add(object);
    }
    // Save the reparsed json - but don't mark the report set as dirty!
    saveStoredJson(array);
  }
  
  private String getStoredJson() {
    // Prefer the report-set copy if available
    String storedJson = reportSetPreferences == null
        ? null : reportSetPreferences.get().storedQueries;
    if (Strings.isNullOrEmpty(storedJson)) {
      storedJson = preferences.storedQueries;
    }
    return storedJson;
  }
  
  private void saveStoredJson(JsonArray array) {
    String json = new Gson().toJson(array);
    if (reportSetPreferences != null) {
      reportSetPreferences.get().storedQueries = json;
    }
    preferences.storedQueries = json;
    
  }
}
