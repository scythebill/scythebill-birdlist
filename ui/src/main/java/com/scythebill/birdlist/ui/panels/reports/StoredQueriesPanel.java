/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.eventbus.Subscribe;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.gson.JsonParseException;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.query.QueryDefinition;
import com.scythebill.birdlist.model.query.QueryProcessor;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.query.SightingPredicates;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.components.TextLink;
import com.scythebill.birdlist.ui.components.table.DeleteColumn;
import com.scythebill.birdlist.ui.events.DefaultUserChangedEvent;
import com.scythebill.birdlist.ui.events.DefaultUserStore;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.events.TaxonomyChangedEvent;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;
import com.scythebill.birdlist.ui.panels.reports.StoredQueries.RestoredQuery;
import com.scythebill.birdlist.ui.panels.reports.StoredQueries.StoredQuery;
import com.scythebill.birdlist.ui.panels.reports.StoredQueries.StoredQueryType;
import com.scythebill.birdlist.ui.panels.reports.TotalTicksProcessor.TotalTickType;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * Executes and displays stored query totals, and lets users jump to
 * the ReportsPanel for a full view or delete those queries.
 */
public class StoredQueriesPanel extends JPanel implements FontsUpdatedListener {
  private static final Logger logger = Logger.getLogger(StoredQueriesPanel.class.getName());
  private final StoredQueries storedQueries;
  private final QueryPreferences queryPreferences;
  private final ListeningExecutorService executorService;
  private final ReportSet reportSet;
  private final PredefinedLocations predefinedLocations;
  private final NavigableFrame navigableFrame;
  private final Alerts alerts;
  private final FontManager fontManager;
  private final DefaultUserStore defaultUserStore;
  private Taxonomy taxonomy;
  private List<Future<?>> futures = Lists.newArrayList();
  private List<TextLink> labels = Lists.newArrayList();
  private List<JLabel> deletes = Lists.newArrayList();
  private List<JLabel> results = Lists.newArrayList();

  @Inject
  public StoredQueriesPanel(
      ReportSet reportSet,
      PredefinedLocations predefinedLocations,
      StoredQueries storedQueries,
      QueryPreferences queryPreferences,
      ListeningExecutorService executorService,
      TaxonomyStore taxonomyStore,
      DefaultUserStore defaultUserStore,
      EventBusRegistrar registrar,
      NavigableFrame navigableFrame,
      Alerts alerts,
      FontManager fontManager) {
    this.reportSet = reportSet;
    this.predefinedLocations = predefinedLocations;
    this.storedQueries = storedQueries;
    this.queryPreferences = queryPreferences;
    this.executorService = executorService;
    this.defaultUserStore = defaultUserStore;
    this.navigableFrame = navigableFrame;
    this.alerts = alerts;
    this.fontManager = fontManager;
    registrar.registerWhenInHierarchy(this);
    setTaxonomy(taxonomyStore.getTaxonomy());
    
    // Make sure all futures are cancelled when this is removed
    addAncestorListener(new AncestorListener() {
      @Override
      public void ancestorAdded(AncestorEvent e) {
      }

      @Override
      public void ancestorMoved(AncestorEvent e) {
      }

      @Override
      public void ancestorRemoved(AncestorEvent e) {
        cancelAllFutures();
      }
    });
  }

  @Subscribe
  public void taxonomyChanged(TaxonomyChangedEvent event) {
    setTaxonomy(event.getTaxonomy());
    // Force a layout rebuild
    fontsUpdated(fontManager);
  }

  @Subscribe
  public void defaultUserChanged(DefaultUserChangedEvent event) {
    recreateQueriesAndComponents();
    // Force a layout rebuild
    fontsUpdated(fontManager);
  }
  
  private void setTaxonomy(Taxonomy taxonomy) {
    this.taxonomy = taxonomy;
    recreateQueriesAndComponents();
  }
  
  /**
   * Rebuild the UI for the stored queries panel - called after any change.
   */
  private void recreateQueriesAndComponents() {
    removeAll();
    cancelAllFutures();
    labels.clear();
    results.clear();
    deletes.clear();
    
    if (storedQueries.getStoredQueries().isEmpty()) {
      return;
    }
    
    // Create the components for each row
    for (StoredQuery storedQuery : storedQueries.getStoredQueries()) {
      Optional<RestoredQuery> optionalRestored = storedQuery.restore();
      // If restoring is unsuccessful, just drop the query.
      if (!optionalRestored.isPresent()) {
        continue;
      }
      RestoredQuery restoredQuery = optionalRestored.get();
      StoredQueryType storedQueryType = storedQuery.storedQueryType;
      final StoredQuery localQuery = storedQuery;
      
      TextLink label = new TextLink(storedQuery.getName() + ":");
      label.setToolTipText(
          Messages.getFormattedMessage(Name.SHOW_THE_FULL_REPORT_FORMAT, storedQuery.getName()));
      label.setBorder(new EmptyBorder(0, 0, 0, 0));
      label.addActionListener(e -> {
        if (storedQueryType == StoredQueryType.REPORTS) {
          ReportsPanel reports = (ReportsPanel) navigableFrame
              .navigateTo("reports");
          if (reports != null) {
            reports.restoreQuery(restoredQuery);
          }
        } else if (storedQueryType == StoredQueryType.TOTAL_TICKS) {
          TotalTicksPanel totalTicks = (TotalTicksPanel) navigableFrame.navigateTo("totalTicks");
          totalTicks.restoreQuery(localQuery);
        }
      });
      
      labels.add(label);
      final JLabel resultLabel = new JLabel("...");
      resultLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
      results.add(resultLabel);
      
      // Parse the query results (in the main UI thread, *not* on the executor service, since
      // these functions actually build UI components!)
      SwingUtilities.invokeLater(() -> {
        try {
          Predicate<Sighting> sightingPredicate = 
              defaultUserStore.getUser() != null && !restoredQuery.containsQueryFieldType(QueryFieldType.USER)
                  ? SightingPredicates.includesUser(defaultUserStore.getUser())
                  : Predicates.alwaysTrue();
          
          QueryDefinition queryDefinition = restoredQuery
              .getQueryDefinition(taxonomy, reportSet, queryPreferences, Type.species, sightingPredicate);
          Predicate<Sighting> countablePredicate = restoredQuery
              .countablePredicate(taxonomy, queryPreferences);
          
          ListenableFuture<Integer> future;

          // Queue the appropriate type of query.
          if (storedQueryType == StoredQueryType.REPORTS) {
            future = executorService.submit(() -> {
              QueryProcessor processor = new QueryProcessor(reportSet, taxonomy);
              processor.dontIncludeFamilyNames();
              QueryResults queryResults = processor.runQuery(queryDefinition,
                  countablePredicate, Type.species);
              return queryResults.getCountableSpeciesSize(
                  taxonomy, false);
            });
          } else {
            future = executorService.submit(() -> {
              TotalTickType type = localQuery.getTotalTickType();
              Multimap<String, String> computedTotalTicks = new TotalTicksProcessor()
                  .computeTotalTicks(
                      reportSet, predefinedLocations, type, taxonomy, queryDefinition, countablePredicate);
              return computedTotalTicks.size();
            });
          }
          
          Futures.addCallback(future, new FutureCallback<Integer>() {
            @Override public void onFailure(Throwable t) {
              if (!(t instanceof CancellationException)) {
                logger.log(Level.WARNING, "Couldn't execute " + localQuery.name, t);
              }
            }
            @Override public void onSuccess(Integer count) {
              SwingUtilities.invokeLater(
                  () -> resultLabel.setText(String.format("%d", count)));
            }
          }, executorService);      
          futures.add(future);
        } catch (JsonParseException e) {
          logger.log(Level.WARNING, "Couldn't execute " + localQuery.name, e);
        }
      });
      
      JLabel deleteLabel = DeleteColumn.createDeleteLabel();
      deletes.add(deleteLabel);
      deleteLabel.addMouseListener(new MouseAdapter() {

        @Override
        public void mouseClicked(MouseEvent e) {
          int okCancel = alerts.showOkCancel(
              StoredQueriesPanel.this,
              Name.DELETE_REMEMBERED_REPORT_TITLE,
              Name.DELETE_REMEMBERED_REPORT_FORMAT,
              localQuery.name);
          if (okCancel == JOptionPane.OK_OPTION) {
            storedQueries.removeQuery(localQuery);
            recreateQueriesAndComponents();
          }
        }
      });
    }
    // Force a layout rebuild
    fontsUpdated(fontManager);
  }

  private void cancelAllFutures() {
    for (Future<?> future : futures) {
      future.cancel(true);
    }
    futures.clear();
  }

  public boolean isEmpty() {
    return storedQueries.getStoredQueries().isEmpty();
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    GroupLayout layout = new GroupLayout(this);
    SequentialGroup verticalGroup = layout.createSequentialGroup();
    SequentialGroup horizontalGroup = layout.createSequentialGroup();
    ParallelGroup labelGroup = layout.createParallelGroup();
    ParallelGroup resultGroup = layout.createParallelGroup();
    ParallelGroup deleteGroup = layout.createParallelGroup();
    horizontalGroup
        .addGroup(labelGroup)
        .addGap(fontManager.scale(40))
        .addGroup(resultGroup)
        .addPreferredGap(ComponentPlacement.UNRELATED)
        .addGroup(deleteGroup);
    
    for (int i = 0; i < labels.size(); i++) {
      TextLink label = labels.get(i);
      JLabel result = results.get(i);
      JLabel delete = deletes.get(i);
      
      verticalGroup.addGroup(layout.createParallelGroup(Alignment.CENTER)
          .addComponent(label)
          .addComponent(result)
          .addComponent(delete));
      verticalGroup.addPreferredGap(ComponentPlacement.RELATED);
      labelGroup.addComponent(label);
      resultGroup.addComponent(result);
      deleteGroup.addComponent(delete);
    }
    
    layout.setHorizontalGroup(horizontalGroup);
    layout.setVerticalGroup(verticalGroup);
    
    setLayout(layout);
  }
}
