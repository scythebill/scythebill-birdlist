/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import com.scythebill.birdlist.model.annotations.Preference;

/**
 * Saved preferences for stored queries, at a global level.
 * {@link StoredReportSetQueriesPreferences} should be preferred when
 * present, but this one should be written to if not.
 */
public class StoredQueriesPreferences {
  @Preference String storedQueries;
}
