/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;

import com.google.common.base.Predicate;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Type;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;

/**
 * QueryField for adjusting queries relative to whether subspecies are allocated or not.
 */
class SubspeciesAllocatedQueryField extends AbstractQueryField {
  private enum QueryType {
    IS(Name.SUBSPECIES_IS_ALLOCATED),
    IS_NOT(Name.SUBSPECIES_IS_NOT_ALLOCATED);
    
    private final Name text;
    QueryType(Name text) {
      this.text = text;
    }
    
    @Override
    public String toString() {
      return Messages.getMessage(text);
    }
  }

  private final JComboBox<QueryType> queryOptions = new JComboBox<QueryType>(QueryType.values());
  private final JPanel statusOptions = new JPanel();
  private final TaxonomyStore taxonomyStore;

  public SubspeciesAllocatedQueryField(TaxonomyStore taxonomyStore) {
    super(QueryFieldType.SUBSPECIES_ALLOCATED);
    this.taxonomyStore = taxonomyStore;
    queryOptions.addActionListener(e -> firePredicateUpdated());
  }

  @Override
  public JComponent getComparisonChooser() {
    return queryOptions;
  }

  @Override
  public JComponent getValueField() {
    return statusOptions;
  }

  @Override
  public Predicate<Sighting> predicate(Taxon.Type depth) {
    final boolean isAllocated = QueryType.IS == queryOptions.getSelectedItem();
    Predicate<Sighting> predicate = new Predicate<Sighting>() {
      @Override public boolean apply(Sighting sighting) {
        // Skip incompatible taxa 
        if (!TaxonUtils.areCompatible(taxonomyStore.getTaxonomy(), sighting.getTaxonomy())) {
          return false;
        }
        
        Resolved resolved = sighting.getTaxon().resolve(taxonomyStore.getTaxonomy());
        if (resolved.getType() == Type.HYBRID || resolved.getType() == Type.SP) {
          return false;
        }
        
        Taxon taxon = resolved.getTaxon();
        if (isAllocated) {
          // "Is allocated": look for children that are fully allocated already
          // If there's no children of this taxon, then it's fully allocated.
          // And drop monotypic species
          return taxon.getContents().isEmpty() && taxon.getType() != Taxon.Type.species;
        } else {
          // "Not allocated": the current taxon must have children
          return hasTrueChildren(taxon);
        }
      }
    };
    
    return predicate;
  }

  @Override public boolean isNoOp() {
    return false;
  }

  private static boolean hasTrueChildren(Taxon taxon) {
    List<Taxon> contents = taxon.getContents();
    if (contents.isEmpty()) {
      return false;
    }
    
    // Ignore introduced/domestic "children"
    for (int i = 0; i < contents.size(); i++) {
      Taxon child = contents.get(i);
      Status status = child.getStatus();
      if (status != Status.IN && status != Status.DO) {
        return true;
      }
    }
    
    return false;
  }
  
  @Override
  public JsonElement persist() {
    Persisted persisted = new Persisted((QueryType) queryOptions.getSelectedItem());
    return GSON.toJsonTree(persisted);
  }

  @Override
  public void restore(JsonElement json) {
    Persisted persisted = GSON.fromJson(json, Persisted.class);
    queryOptions.setSelectedItem(persisted.type);
  }
  
  static private final Gson GSON = new Gson();
  static class Persisted {
    public Persisted(QueryType type) {
      this.type = type;
    }
    
    QueryType type;
  }
}
