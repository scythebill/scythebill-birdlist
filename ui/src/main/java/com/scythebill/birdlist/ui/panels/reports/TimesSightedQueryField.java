/*   
 * Copyright 2020 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.text.NumberFormat;
import java.util.Collection;

import javax.annotation.Nullable;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.text.NumberFormatter;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.scythebill.birdlist.model.query.QueryDefinition;
import com.scythebill.birdlist.model.query.QueryDefinition.PostProcessor;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.ui.components.AutoSelectJFormattedTextField;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;

/**
 * QueryField for "times sighted".
 */
class TimesSightedQueryField extends AbstractQueryField {
  private enum QueryType {
    EXACTLY(Name.EXACTLY),
    AT_MOST(Name.AT_MOST),
    AT_LEAST(Name.AT_LEAST),
    LESS_THAN(Name.LESS_THAN),
    MORE_THAN(Name.MORE_THAN);
    
    private final Name text;
    QueryType(Name text) {
      this.text = text;
    }
    
    @Override
    public String toString() {
      return Messages.getMessage(text);
    }
  }

  private final JComboBox<QueryType> typeOptions = new JComboBox<>(QueryType.values());
  private final JFormattedTextField countField;
  
  public TimesSightedQueryField() {
    super(QueryFieldType.TIMES_SIGHTED);
    
    typeOptions.addActionListener(e -> {
      firePredicateUpdated();
    });
    
    NumberFormatter formatter = new NumberFormatter(NumberFormat.getIntegerInstance());
    formatter.setValueClass(Integer.class);
    formatter.setMinimum(1);
    countField = new AutoSelectJFormattedTextField(formatter);
    countField.setValue(1);
    countField.setColumns(3);
    countField.addPropertyChangeListener("value", e -> {firePredicateUpdated();});
  }

  @Override
  public JComponent getComparisonChooser() {
    return typeOptions;
  }

  @Override
  public JComponent getValueField() {
    return countField;
  }

  @Override
  public boolean isSingleLine() {
    return true;
  }
  
  static class TimesSightedQueryDefinition implements QueryDefinition, PostProcessor {
    private final QueryType type;
    private final int count;

    public TimesSightedQueryDefinition(QueryType type, int count) {
      this.type = Preconditions.checkNotNull(type);
      this.count = count;
    }

    @Override
    public Optional<Preprocessor> preprocessor() {
      return Optional.absent();
    }

    @Override
    public Predicate<Sighting> predicate() {
      return Predicates.alwaysTrue();
    }

    @Override
    public Optional<QueryAnnotation> annotate(Sighting sighting, SightingTaxon taxon) {
      return Optional.absent();
    }

    @Override
    public Optional<PostProcessor> postprocessor() {
      return Optional.of(this);
    }

    @Override
    public boolean acceptTaxon(
        SightingTaxon sightingTaxon, Collection<Sighting> sightings,
        @Nullable Predicate<Sighting> countablePredicate) {
      int size = countablePredicate == null
          ? sightings.size()
          : (int) sightings.stream().filter(countablePredicate).count();
      switch (type) {
        case EXACTLY:
          return size == count;
        case AT_MOST:
          return size <= count;
        case AT_LEAST:
          return size >= count;
        case LESS_THAN:
          return size < count;
        case MORE_THAN:
          return size > count;
        default:
          throw new AssertionError("Unknown type " + type);
      }
    }
  }
  
  @Override
  public QueryDefinition queryDefinition(ReportSet reportSet, Taxon.Type depth) {
    return new TimesSightedQueryDefinition(
        (QueryType) typeOptions.getSelectedItem(),
        getCount());
  }

  @Override public boolean isNoOp() {
    return false;
  }
  
  @Override
  public Optional<String> name() {
    return Optional.absent();
  }
  
  @Override
  public Optional<String> abbreviation() {
    return Optional.absent();
  }

  private int getCount() {
    return (Integer) countField.getValue();
  }
  
  
  @Override
  public JsonElement persist() {
    Persisted persisted = new Persisted((QueryType) typeOptions.getSelectedItem(), getCount());
    return GSON.toJsonTree(persisted);
  }

  @Override
  public void restore(JsonElement json) {
    Persisted persisted = GSON.fromJson(json, Persisted.class);
    typeOptions.setSelectedItem(persisted.type);
    countField.setValue(persisted.count);
  }
  
  static private final Gson GSON = new Gson();
  static class Persisted {
    public Persisted(QueryType type, int count) {
      this.type = type;
      this.count = count;
    }
    
    QueryType type;
    int count;
  }
}
