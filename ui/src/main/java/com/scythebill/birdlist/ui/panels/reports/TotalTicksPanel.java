/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import static java.util.Comparator.comparing;
import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.toCollection;
import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.swing.Action;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.Timer;
import javax.swing.TransferHandler;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.query.QueryProcessor;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.query.SightingPredicates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.names.LocalNames;
import com.scythebill.birdlist.ui.actions.ActionBroker;
import com.scythebill.birdlist.ui.actions.Attachable;
import com.scythebill.birdlist.ui.actions.ForwardingAction;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.app.Titled;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.events.TaxonomyChangedEvent;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;
import com.scythebill.birdlist.ui.panels.reports.StoredQueries.StoredQuery;
import com.scythebill.birdlist.ui.panels.reports.TotalTicksProcessor.TotalTickType;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;
import com.scythebill.birdlist.ui.util.FocusTracker;
import com.scythebill.birdlist.ui.util.ListListModel;
import com.scythebill.birdlist.ui.util.LocationIdToString;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Panel for showing total ticks.
 */
public class TotalTicksPanel extends JPanel implements FontManager.FontsUpdatedListener, Titled {

  private final ReportSet reportSet;
  private final PredefinedLocations predefinedLocations;
  private final QueryPanelFactory queryPanelFactory;
  private final TaxonomyStore taxonomyStore;
  private final StoredQueries storedQueries;
  private final QueryPreferences queryPreferences;
  private final ReportsBrowserPanel reportsBrowserPanel;
  private final FileDialogs fileDialogs;
  private final NavigableFrame navigableFrame;
  private QueryPanel queryPanel;
  private JButton saveSpreadsheetButton;
  private JButton showMapButton;
  private JButton rememberButton;
  private JButton returnButton;
  private JComboBox<TotalTickType> typeComboBox;
  private JLabel totalTicksLabel;
  private JLabel totalTicksExplanation;
  private JLabel totalTicksTitle;
  private JList<TotalTicksResult> totalTicksResultList;
  private JScrollPane totalTicksResultScrollPane;
  private JScrollPane reportsScrollPane;
  private Timer showTotalTicksTimer;
  private Alerts alerts;
  private TotalTicksProcessor totalTicksProcessor;
  private final ActionBroker actionBroker;
  
  class TotalTicksResult {
    private final int size;
    private final String locationId;

    public TotalTicksResult(String locationId, int size) {
      this.locationId = locationId;
      this.size = size;
    }
    
    @Override
    public String toString() {
      String locationText;
      Location location = reportSet.getLocations().getLocation(getLocationId());
      if (location.getType() == Location.Type.country) {
        // For countries, just use the display name.  Otherwise, the US/Russia/Indoensia
        // get shown arbitrarily with a continent suffix.
        locationText = location.getDisplayName();
      } else {
        Location rootLocation = queryPanel.getRootLocation();
        locationText = LocationIdToString.getString(reportSet.getLocations(), getLocationId(), /*verbose=*/true, rootLocation);
      }
      return "%s: \t%d".formatted(locationText, size);
    }

    public String getLocationId() {
      return locationId;
    }
    
  }

  @Inject
  TotalTicksPanel(
      ReportSet reportSet,
      PredefinedLocations predefinedLocations,
      QueryPanelFactory queryPanelFactory,
      ReportsBrowserPanel reportsBrowserPanel,
      TaxonomyStore taxonomyStore,
      EventBusRegistrar eventBusRegistrar,
      FontManager fontManager,
      StoredQueries storedQueries,
      QueryPreferences queryPreferences,
      NavigableFrame navigableFrame,
      FileDialogs fileDialogs,
      ActionBroker actionBroker,
      Alerts alerts) {
    this.reportSet = reportSet;
    this.predefinedLocations = predefinedLocations;
    this.queryPanelFactory = queryPanelFactory;
    this.reportsBrowserPanel = reportsBrowserPanel;
    this.taxonomyStore = taxonomyStore;
    this.storedQueries = storedQueries;
    this.queryPreferences = queryPreferences;
    this.navigableFrame = navigableFrame;
    this.fileDialogs = fileDialogs;
    this.actionBroker = actionBroker;
    this.alerts = alerts;
    this.totalTicksProcessor = new TotalTicksProcessor();
    initGUI();
    attachListeners();
    fontManager.applyTo(this);
    eventBusRegistrar.registerWhenInHierarchy(this);
    taxonomyChanged(null);    
  }

  private void attachListeners() {
    returnButton.addActionListener(e -> navigableFrame.navigateTo("extendedReportsMenu"));
    queryPanel.addPredicateChangedListener(e -> showQueryResults());
    typeComboBox.addActionListener(e -> showQueryResults());
    saveSpreadsheetButton.addActionListener(e -> saveSpreadsheet());
    rememberButton.addActionListener(e -> rememberQuery());
    showMapButton.addActionListener(e -> showMap());
    
    showTotalTicksTimer = new Timer(200, e -> showSingleTotalTicks());
    showTotalTicksTimer.setRepeats(false);
    totalTicksResultList.addListSelectionListener(e -> showTotalTicksTimer.restart());
    addComponentListener(new ComponentAdapter() {
      @Override public void componentHidden(ComponentEvent event) {
        showTotalTicksTimer.stop();
      }
    });
    totalTicksResultList.setDragEnabled(true);
    totalTicksResultList.setTransferHandler(new TransferHandler() {

      @Override
      protected Transferable createTransferable(JComponent c) {
        if (totalTicksResultList.isSelectionEmpty()) {
          return null;
        }
        
        String totalTicksAsText = IntStream.of(totalTicksResultList.getSelectedIndices())
            .sorted()
            .mapToObj(i -> totalTicksResultList.getModel().getElementAt(i).toString())
            .collect(Collectors.joining("\n"));
        return new StringSelection(totalTicksAsText);
      }

      @Override
      public int getSourceActions(JComponent c) {
        return COPY;
      }
    });

    new FocusTracker(totalTicksResultList) {
      private final Action copy = new CopyActionWrapper(TransferHandler.getCopyAction());
      
      @Override protected void focusGained(Component child) {
        actionBroker.publishAction("copy", copy);
      }

      @Override protected void focusLost(Component child) {
        actionBroker.unpublishAction("copy", copy);
      }
    };
    
  }
  
  private void showSingleTotalTicks() {
    TotalTicksResult result = totalTicksResultList.getSelectedValue();
    if (result == null) {
      reportsBrowserPanel.setModel(null);
    } else {
      List<Location> locations = new ArrayList<>();
      Location location = reportSet.getLocations().getLocation(result.getLocationId());
      String code = Locations.getLocationCode(location);
      if (code == null) {
        locations.add(location);
      } else {
        // Map from codes *back* to the list of all countries that share that code.
        // But: exclude US West Indies, as those are treated as separate countries.
        locations.addAll(reportSet.getLocations().getLocationsByCode(code));
        if (code.equals("US")) {
          locations.removeIf(l -> l.getParent().getModelName().equals("West Indies"));
        }
      }

      Taxonomy taxonomy = taxonomyStore.getTaxonomy();
      Predicate<Sighting> countablePredicate = queryPreferences.getCountablePredicate(
          taxonomyStore.getTaxonomy(),
          queryPanel.containsQueryFieldType(QueryFieldType.SIGHTING_STATUS),
          queryPanel.getBooleanValueIfPresent(QueryFieldType.ALLOW_HEARD_ONLY));
      QueryProcessor queryProcessor = new QueryProcessor(reportSet, taxonomyStore.getTaxonomy())
          .onlyIncludeCountableSightings()
          .withAdditionalPredicate(
              Predicates.or(
                  locations.stream()
                      .map(l -> SightingPredicates.in(l, reportSet.getLocations()))
                      .collect(ImmutableList.toImmutableList())));

      QueryResults results = queryProcessor.runQuery(
          queryPanel.queryDefinition(taxonomy, Taxon.Type.species),
          countablePredicate,
          Taxon.Type.species);
      reportsBrowserPanel.setLocationRoot(location);
      reportsBrowserPanel.setQueryResults(results, /*showVisits=*/true);
      // Oddly, necessary to make the browser panel show up
      reportsBrowserPanel.revalidate();
      
    }
     
  }
  
  /** Remember a total ticks query. */
  private void rememberQuery() {
    TotalTickType totalTickType = totalTickType();
    String defaultName = totalTickType.reportName();
    Optional<String> queryName = queryPanel.getQueryName();
    if (queryName.isPresent()) {
      defaultName = queryName.get() + " " + defaultName;
    }
    
    String name = storedQueries.chooseName(this, defaultName);
    if (name != null) {
      storedQueries.addTotalTicksQuery(name, totalTickType, queryPanel.persist());
    }
  }

  private void initGUI() {    
    queryPanel = queryPanelFactory.newQueryPanel(
        // No sp/hybrid, since that's explicitly meaningless here
        QueryFieldType.SP_OR_HYBRID,
        // Or "subspecies allocated"
        QueryFieldType.SUBSPECIES_ALLOCATED,
        // Or "times sighted"
        QueryFieldType.TIMES_SIGHTED);
    typeComboBox = new JComboBox<>(TotalTickType.values());
    totalTicksExplanation = new JLabel();
    totalTicksExplanation.putClientProperty(FontManager.PLAIN_LABEL, true);
    totalTicksTitle = new JLabel(Messages.getMessage(Name.TOTAL_TICKS_LABEL));
    totalTicksTitle.putClientProperty(FontManager.TEXT_SIZE_PROPERTY, FontManager.TextSize.VERY_LARGE);
    totalTicksLabel = new JLabel();
    totalTicksLabel.putClientProperty(FontManager.TEXT_SIZE_PROPERTY, FontManager.TextSize.VERY_LARGE);
    
    totalTicksResultList = new JList<>(new ListListModel<>());
    totalTicksResultScrollPane = new JScrollPane(
        totalTicksResultList,
        JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
        JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    reportsScrollPane = new JScrollPane(reportsBrowserPanel);
    reportsBrowserPanel.setEditableSightings(true);
    
    returnButton = new JButton(Messages.getMessage(Name.BACK_TO_SPECIAL_REPORTS));
    saveSpreadsheetButton = new JButton(Messages.getMessage(Name.SAVE_AS_SPREADSHEET));
    showMapButton = new JButton(Messages.getMessage(Name.TOTAL_TICKS_MAP));
    rememberButton = new JButton(Messages.getMessage(Name.REMEMBER_MENU));
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    setBorder(new EmptyBorder(
        fontManager.scale(20),
        fontManager.scale(20),
        fontManager.scale(20),
        fontManager.scale(20)));
    GroupLayout layout = new GroupLayout(this);
    layout.setHorizontalGroup(
        layout.createParallelGroup(Alignment.LEADING)
            .addComponent(typeComboBox, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addComponent(queryPanel, PREFERRED_SIZE, PREFERRED_SIZE, Short.MAX_VALUE)
            .addComponent(totalTicksTitle)
            .addComponent(totalTicksLabel)
            .addGroup(layout.createSequentialGroup()
                .addComponent(totalTicksResultScrollPane)
                .addComponent(reportsScrollPane))
            .addComponent(totalTicksExplanation)
            .addGroup(layout.createSequentialGroup()
                .addComponent(saveSpreadsheetButton)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(showMapButton)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(rememberButton))
            .addComponent(returnButton, Alignment.TRAILING));
    
    layout.setVerticalGroup(
        layout.createSequentialGroup()
            .addComponent(typeComboBox, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(queryPanel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addPreferredGap(ComponentPlacement.UNRELATED)
            .addComponent(totalTicksTitle)
            .addComponent(totalTicksLabel)
            .addPreferredGap(ComponentPlacement.UNRELATED)
            .addComponent(totalTicksExplanation)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup()
                .addComponent(totalTicksResultScrollPane, PREFERRED_SIZE, PREFERRED_SIZE, Short.MAX_VALUE)
                .addComponent(reportsScrollPane, PREFERRED_SIZE, PREFERRED_SIZE, Short.MAX_VALUE))
            .addGap(fontManager.scale(10), fontManager.scale(10), fontManager.scale(10))
            .addGroup(
                layout.createBaselineGroup(false, false)
                    .addComponent(saveSpreadsheetButton)
                    .addComponent(showMapButton)
                    .addComponent(rememberButton)
                    .addComponent(returnButton)));
    
    setLayout(layout);

    Dimension preferredSize = fontManager.scale(new Dimension(760, 680));
    preferredSize.width += 200;
    setPreferredSize(preferredSize);
  }
  
 
  @Subscribe
  public void taxonomyChanged(TaxonomyChangedEvent event) {
    reportsBrowserPanel.setTaxonomy(taxonomyStore.getTaxonomy());
    queryPanel.taxonomyUpdated();
    showQueryResults();
  }
  
  private void showQueryResults() {
    TotalTickType type = totalTickType();
    switch (type) {
      case COUNTRY:
        totalTicksExplanation.setText(
            Messages.getMessage(Name.TOTAL_TICKS_COUNTRY_EXPLANATION));
        break;
      case COUNTY:
        totalTicksExplanation.setText(
            Messages.getMessage(Name.TOTAL_TICKS_COUNTY_EXPLANATION));
        break;
      case STATE:
        totalTicksExplanation.setText(
            Messages.getMessage(Name.TOTAL_TICKS_STATE_EXPLANATION));
        break;
    }
    
    
    Multimap<String, String> totalTicks = computeTotalTicks();
    totalTicksLabel.setText(NumberFormat.getIntegerInstance().format(totalTicks.size()));
    
    saveSpreadsheetButton.setEnabled(totalTicks.size() > 0);
    showMapButton.setEnabled(totalTicks.size() > 0 && type != TotalTickType.COUNTY);
    
    showTotalTicksTimer.stop();
    reportsBrowserPanel.setModel(null);
    // Invert into total species per location
    ArrayListMultimap<String, String> inverted = Multimaps.invertFrom(totalTicks, ArrayListMultimap.create());
    
    ArrayList<TotalTicksResult> list = inverted.asMap()
        .entrySet()
        .stream()
        .sorted(Comparator.comparing(
            (Map.Entry<String, Collection<String>> entry) -> entry.getValue().size()).reversed())
        .map(e -> new TotalTicksResult(e.getKey(), e.getValue().size()))
        .collect(toCollection(ArrayList::new));
    totalTicksResultList.setModel(new ListListModel<>(list));
  }
  
  private Multimap<String, String> computeTotalTicks() {
    Taxonomy taxonomy = taxonomyStore.getTaxonomy();
    Predicate<Sighting> countablePredicate = queryPreferences.getCountablePredicate(
        taxonomy,
        queryPanel.containsQueryFieldType(QueryFieldType.SIGHTING_STATUS),
        queryPanel.getBooleanValueIfPresent(QueryFieldType.ALLOW_HEARD_ONLY));
    
    return totalTicksProcessor.computeTotalTicks(
        reportSet, predefinedLocations, totalTickType(), taxonomy, queryPanel.queryDefinition(taxonomy, Type.species), countablePredicate);
  }

  private void showMap() {
    // Compute the main total ticks multimap (keys are taxa, values are locations)
    Multimap<String, String> totalTicks = computeTotalTicks();
    // Invert into total species per location
    ArrayListMultimap<String, String> inverted = Multimaps.invertFrom(totalTicks, ArrayListMultimap.create());
    
    try {
      new ShowTotalTicksMap(PredefinedLocations.loadAndParse(), reportSet)
          .showRange(
              inverted, totalTickType(), queryPanel.getQueryName().or(""));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
  
  private void saveSpreadsheet() {
    Optional<String> reportName = queryPanel.getQueryAbbreviation();
    String tickType = totalTickType().name().toLowerCase();
    String fileName = reportName.isPresent()
        ? String.format("%s-ticks-%s.xlsx", tickType, reportName.get())
        : String.format("%s-ticks.xlsx", tickType);
    File out = fileDialogs.saveFile(UIUtils.findFrame(this),
        Messages.getMessage(Name.PICK_FILE_TITLE),
        fileName,
        new FileNameExtensionFilter(
            Messages.getMessage(Name.XLSX_FILES), "xlsx"), FileType.OTHER);
    if (out != null) {
      // Compute the main total ticks multimap (keys are taxa, values are locations)
      Multimap<String, String> totalTicks = computeTotalTicks();
      // And invert too, so we can get total species per location
      ArrayListMultimap<String, String> inverted = Multimaps.invertFrom(totalTicks, ArrayListMultimap.create());
      
      // Identify the rows and columns
      
      // Sort taxa in taxonomic order
      List<Taxon> sortedTaxa = totalTicks.keySet().stream()
          .map(taxonomyStore.getTaxonomy()::getTaxon)
          .distinct()
          .sorted(comparing(Taxon::getTaxonomyIndex))
          .collect(toCollection(ArrayList::new));
      
      // And sort location in "most numerous" order
      Comparator<Location> speciesCountComparator = comparingInt(
          location -> inverted.get(location.getId()).size());
      List<Location> sortedLocations = totalTicks.values().stream()
          .distinct()
          .map(reportSet.getLocations()::getLocation)
          .sorted(speciesCountComparator.reversed())
          .collect(toCollection(ArrayList::new));
      
      @SuppressWarnings("resource")
      Workbook workbook = new XSSFWorkbook();
      
      CellStyle headerStyle = workbook.createCellStyle();
      Font headerFont = workbook.createFont();
      headerFont.setBold(true);
      headerStyle.setFont(headerFont);
          
      Sheet sheet = workbook.createSheet();
      
      // First row:  locations
      Row locationHeader = sheet.createRow(0);
      for (int i = 0; i < sortedLocations.size(); i++) {
        CellUtil.createCell(locationHeader, i + 1, sortedLocations.get(i).getDisplayName(), headerStyle);
      }

      // Second row: location totals at the end
      Row totalRow = sheet.createRow(1);
      for (int i = 0; i < sortedLocations.size() + 1; i++) {
        Cell cell = totalRow.createCell(i + 1);
        CellRangeAddress cellRangeAddress = new CellRangeAddress(2, sortedTaxa.size() + 1, i + 1, i + 1);
        cell.setCellFormula(String.format("SUM(%s)", cellRangeAddress.formatAsString()));
      }
      
      LocalNames localNames = taxonomyStore.getTaxonomy().getLocalNames();
      // Each taxon on a separate row
      for (int i = 0; i < sortedTaxa.size(); i++) {
        Row taxonRow = sheet.createRow(i + 2);
        Taxon taxon = sortedTaxa.get(i);
        taxonRow.createCell(0).setCellValue(localNames.compoundName(taxon, true));
        for (int j = 0; j < sortedLocations.size(); j++) {
          if (totalTicks.containsEntry(taxon.getId(), sortedLocations.get(j).getId())) {
            Cell cell = taxonRow.createCell(j + 1);
            cell.setCellValue(1.0);
          }
        }
        // A total column at the end
        Cell totalCell = taxonRow.createCell(sortedLocations.size() + 1);
        CellRangeAddress cellRangeAddress = new CellRangeAddress(i + 2, i + 2, 1, sortedLocations.size());
        totalCell.setCellFormula(String.format("SUM(%s)", cellRangeAddress.formatAsString()));
      }
      
      // Auto-size the species names
      sheet.autoSizeColumn(0);
      
      // Freeze the left column and top two rows
      sheet.createFreezePane(1, 2);
      
      try (OutputStream stream = new BufferedOutputStream(new FileOutputStream(out))) {
        workbook.write(stream);
      } catch (IOException e) {
        FileDialogs.showFileSaveError(alerts, e, out);
        return;
      }
      
      try {
        Desktop.getDesktop().open(out);
      } catch (IOException e) {
        alerts.showError(this, e, 
            Name.OPENING_FAILED, Name.NO_APPLICATION_FOR_XLS_FILES);
        return;
      }
    }
  }

  private TotalTickType totalTickType() {
    return (TotalTickType) typeComboBox.getSelectedItem();
  }

  public void restoreQuery(StoredQuery localQuery) {
    TotalTickType totalTickType = localQuery.getTotalTickType();
    typeComboBox.setSelectedItem(totalTickType);
    queryPanel.restore(localQuery.queryJson());
  }

  /**
   * Wrapper to enable-disable the copy actions.  Enable when selection indicates
   * there is any selection that contains text.
   */
  private class CopyActionWrapper extends ForwardingAction
      implements Attachable, ListSelectionListener {
    public CopyActionWrapper(Action wrapped) {
      super(totalTicksResultList, wrapped);
    }
    
    @Override public void attach() {
      totalTicksResultList.addListSelectionListener(this);
      updateEnabled();
    }

    @Override public void unattach() {
      totalTicksResultList.removeListSelectionListener(this);
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
      updateEnabled();
    }
    
    private void updateEnabled() {
      setEnabled(!totalTicksResultList.isSelectionEmpty());
    }
  }

  @Override
  public String getTitle() {
    return Messages.getMessage(Name.TOTAL_TICKS);
  }
}
