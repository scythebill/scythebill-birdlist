/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.scythebill.birdlist.model.query.QueryDefinition;
import com.scythebill.birdlist.model.query.QueryProcessor;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Processor for computing total ticks.
 */
public class TotalTicksProcessor {

  public enum TotalTickType {
    COUNTRY("Country") {
      @Override
      Location bucketLocation(Location in, ReportSet reportSet, PredefinedLocations predefinedLocations) {
        if (in == null) {
          return null;
        }
        Location country = Locations.getAncestorOfType(in, Location.Type.country);
        if (country == null) {
          // If no country was available, then look for the state, and see
          // if it's a state like the Galapagos that isn't inside its country
          Location state = Locations.getAncestorOfType(in, Location.Type.state);
          if (state != null) {
            String stateCode = Locations.getLocationCode(state);
            if (stateCode != null) {
              String countryCode = STATE_TO_COUNTRY.get(stateCode);
              if (countryCode != null) {
                country = reportSet.getLocations().getLocationByCode(countryCode);
              }
            }
          }
        }
        
        return country;
      }
    },
    STATE("State") {
      @Override
      Location bucketLocation(Location in, ReportSet reportSet, PredefinedLocations predefinedLocations) {
        if (in == null) {
          return null;
        }
        Location state = Locations.getAncestorOfType(in, Location.Type.state);
        if (state == null) {
          Location country = Locations.getAncestorOfType(in, Location.Type.country);
          if (country != null) {
            String countryCode = Locations.getLocationCode(country);
            String stateCode = COUNTRY_TO_STATE.get(countryCode);
            if (stateCode != null) {
              state = Locations.getLocationByCodePossiblyCreating(
                  reportSet.getLocations(), predefinedLocations, stateCode);
            }
          }
        }
        
        return state;
      }
    },
    COUNTY("County") {
      @Override
      Location bucketLocation(Location in, ReportSet reportSet, PredefinedLocations predefinedLocations) {
        if (in == null) {
          return null;
        }
        return Locations.getAncestorOfType(in, Location.Type.county);
      }
    };

    
    private final String text;

    TotalTickType(String text) {
      this.text = text;
    }
    
    @Override
    public String toString() {
      return text;
    }
    
    abstract Location bucketLocation(Location in, ReportSet reportSet, PredefinedLocations predefinedLocations);

    String reportName() {
      return text + " Total Ticks";
    }
  }
 
  /**
   * A map of states that are placed outside the hierarchy of their country,
   * and therefore (for country mappings in total ticks) need to be mapped
   * back into the country.
   */
  private final static ImmutableMap<String, String> STATE_TO_COUNTRY =
      ImmutableMap.<String, String>builder()
          .put("EC-W", "EC")
          .put("PM", "FR")
          .put("CO-SAP", "CO")
          .build();
  
  /**
   * A map of "countries" that are really parts of states! (but are placed
   * in a different location hierarchy than the state) so need to be mapped to
   * a state.
   */
  private final static ImmutableMap<String, String> COUNTRY_TO_STATE =
      ImmutableMap.<String, String>builder()
          .put("AU-TAS-Macquarie", "AU-TAS")
          .put("CL-VS-JuanFernandez", "CL-VS")
          .put("CL-VS-Easter", "CL-VS")
          .put("YE-HD-SOC", "YE-HD")
          .put("ZA-WC-EdwardMarion", "ZA-WC")
          .build();
  
  private final static ImmutableMap<String, String> PSEUDO_COUNTRY_TO_COUNTRY =
      ImmutableMap.<String, String>builder()
          .put("AX", "FI")
          .put("AC", "AU")
          .put("CS", "AU")
          .put("CC", "AU")
          .put("CX", "AU")
          .put("NF", "AU")
          .put("HM", "AU")
          .put("BV", "NO")
          .put("SJ", "NO")
          .put("UM", "US")
          .build();
          
  TotalTicksProcessor() {}

  public Multimap<String, String> computeTotalTicks(
      ReportSet reportSet,
      PredefinedLocations predefinedLocations,
      TotalTickType type,
      Taxonomy taxonomy,
      QueryDefinition queryDefinition,
      Predicate<Sighting> countablePredicate) {
    QueryProcessor queryProcessor = new QueryProcessor(reportSet, taxonomy);
    queryProcessor.onlyIncludeCountableSightings().dontIncludeFamilyNames();
    
    QueryResults results = queryProcessor.runQuery(
        queryDefinition,
        countablePredicate,
        Taxon.Type.species);

    Multimap<String, String> totalTicksMultimap = LinkedHashMultimap.create();
    for (Sighting sighting : results.getAllSightings()) {
      if (sighting.getLocationId() == null) {
        continue;
      }
      
      Location location = reportSet.getLocations().getLocation(sighting.getLocationId());
      Location locationBucket = type.bucketLocation(location, reportSet, predefinedLocations);
      if (locationBucket == null) {
        continue;
      }

      // Ugly, but for "state" ticks, it's possible to have sightings for
      // something like Socotra, which needs to get mapped back to YE-HD
      // (Hydramawt) which might not have been created yet!
      reportSet.getLocations().ensureAdded(locationBucket);

      String bucketId = locationBucket.getId();
      // Countries need some extra normalization since:
      //   - Some countries are split and occur twice, but most only count once
      //   - Some "countries" aren't really countries
      if (type == TotalTickType.COUNTRY) {
        String eBirdCode = locationBucket.getEbirdCode();
        if (eBirdCode != null) {
          // Strip down to just the country (e.g. to go from EC-W (Galapagos) to EC (Ecuador))
          int hyphenIndex = eBirdCode.indexOf('-');
          if (hyphenIndex > 0) {
            eBirdCode = eBirdCode.substring(0, hyphenIndex);
          }
          
          // And now map "pseudo-countries" back to "real" countries
          eBirdCode = PSEUDO_COUNTRY_TO_COUNTRY.getOrDefault(eBirdCode, eBirdCode);
          
          // Then, go back and look up the location by code, whether or not we had to strip down
          // to "just" the country.  This prevents double-counting the US, Indonesia, etc. for
          // appearing in multiple regions
          Location locationByCode = reportSet.getLocations().getLocationByCode(eBirdCode);
          if (locationByCode != null) {
            bucketId = locationByCode.getId();
          }
        }
      }
      
      // Resolve the taxon
      SightingTaxon species = sighting.getTaxon().resolve(taxonomy).getParentOfAtLeastType(Taxon.Type.species);
      if (species.getType() == SightingTaxon.Type.HYBRID || species.getType() == SightingTaxon.Type.SP) {
        continue;
      }
      
      totalTicksMultimap.put(species.getId(), bucketId);
    }
    
    return totalTicksMultimap;
  }
}
