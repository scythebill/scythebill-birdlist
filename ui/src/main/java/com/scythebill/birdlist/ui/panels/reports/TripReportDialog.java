/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.awt.Component;

import javax.annotation.Nullable;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences.ScientificOrCommon;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.util.Alerts;

/**
 * Dialog for configuring the trip report options.
 */
class TripReportDialog {
  private Alerts alerts;
  private TripReportPreferences tripReportPreferences;
  private FontManager fontManager;
  private NamesPreferences namesPreferences;
  private boolean speciesTableDisabled;
  private boolean multipleTaxonomiesDisabled;

  @Inject
  TripReportDialog(
      Alerts alerts,
      TripReportPreferences tripReportPreferences,
      NamesPreferences namesPreferences,
      FontManager fontManager) {
    this.alerts = alerts;
    this.tripReportPreferences = tripReportPreferences;
    this.namesPreferences = namesPreferences;
    this.fontManager = fontManager;
  }
  
  /** Support disabling the species table when there's too many sightings. */
  public void disableSpeciesTable() {
    speciesTableDisabled = true;
  }

  /** Disable multiple taxonomies, because they aren't available. */
  public void disableMultipleTaxonomies() {
    multipleTaxonomiesDisabled = true;
  }

  @Nullable
  public TripReportPreferences getConfiguration(Component parent) {
    TripReportConfigurationPanel panel = new TripReportConfigurationPanel();
    fontManager.applyTo(panel);
    
    String formattedMessage = alerts.getFormattedDialogMessage(
        Name.TRIP_REPORT_OPTIONS, Name.TRIP_REPORT_OPTIONS_MESSAGE);
    
    int okCancel = alerts.showOkCancelWithPanel(
        SwingUtilities.getWindowAncestor(parent), formattedMessage, panel);
    if (okCancel != JOptionPane.OK_OPTION) {
      return null;
    }

    panel.updatePreferences();
    if (speciesTableDisabled) {
      // If the species table is disabled, then the preferences haven't
      // fully been updated:  return a cloned version with the species table
      // forcibly set to false.
      TripReportPreferences cloned = tripReportPreferences.clone();
      cloned.includeSpeciesTable = false;
      return cloned;
    } else {
      return tripReportPreferences;
    }
  }
  
  class TripReportConfigurationPanel extends JPanel {
    private JCheckBox includeScientific;
    private JCheckBox includeItinerary;
    private JCheckBox includeSpeciesTable;
    private JCheckBox includeSpeciesList;
    private JCheckBox includeFavoritePhotos;
    private JCheckBox showAllTaxonomies;

    TripReportConfigurationPanel() {
      GroupLayout layout = new GroupLayout(this);
      setLayout(layout);
      
      includeScientific = new JCheckBox(
          Messages.getMessage(Name.SCIENTIFIC_NAME_QUESTION));
      includeScientific.setSelected(tripReportPreferences.includeScientific);
      
      if (namesPreferences.scientificOrCommon == ScientificOrCommon.COMMON_ONLY) {
        includeScientific.setSelected(false);
      } else if (namesPreferences.scientificOrCommon == ScientificOrCommon.SCIENTIFIC_ONLY) {
        includeScientific.setSelected(true);
      }
      
      includeItinerary = new JCheckBox(
          Messages.getMessage(Name.INCLUDE_ITINERARY_QUESTION));
      includeItinerary.setSelected(tripReportPreferences.includeItinerary);

      includeSpeciesTable = new JCheckBox(
          Messages.getMessage(Name.INCLUDE_SPECIES_TABLE_QUESTION));
      if (speciesTableDisabled) {
        includeSpeciesTable.setSelected(false);
        includeSpeciesTable.setEnabled(false);
      } else {
        includeSpeciesTable.setSelected(tripReportPreferences.includeSpeciesTable);
      }
      
      includeSpeciesList = new JCheckBox(
          Messages.getMessage(Name.INCLUDE_SPECIES_LIST_QUESTION));
      includeSpeciesList.setSelected(tripReportPreferences.includeSpeciesList);

      showAllTaxonomies = new JCheckBox(
          Messages.getMessage(Name.INCLUDE_OTHER_TAXONOMIES_QUESTION));
      showAllTaxonomies.setSelected(tripReportPreferences.showAllTaxonomies);
          
      includeFavoritePhotos = new JCheckBox(
          Messages.getMessage(Name.INCLUDE_FAVORITE_PHOTOS_QUESTION));
      includeFavoritePhotos.setSelected(tripReportPreferences.includeFavoritePhotos);

      layout.setHorizontalGroup(layout.createParallelGroup()
          .addComponent(includeScientific)
          .addComponent(includeItinerary)
          .addComponent(includeSpeciesTable)
          .addComponent(includeSpeciesList)
          .addComponent(showAllTaxonomies)
          .addComponent(includeFavoritePhotos));
      
      layout.setVerticalGroup(layout.createSequentialGroup()
          .addComponent(includeScientific)
          .addComponent(includeItinerary)
          .addComponent(includeSpeciesTable)
          .addComponent(includeSpeciesList)
          .addComponent(showAllTaxonomies)
          .addComponent(includeFavoritePhotos));
      
      // Include Favorite Photos only makes sense with includeSpeciesList
      includeFavoritePhotos.setEnabled(includeSpeciesList.isSelected());
      includeSpeciesList.addActionListener(e -> {
        includeFavoritePhotos.setEnabled(includeSpeciesList.isSelected());
      });
      
      // Show All Taxonomies only makes sense with includeSpeciesList
      if (multipleTaxonomiesDisabled) {
        showAllTaxonomies.setEnabled(false);
      } else {
        showAllTaxonomies.setEnabled(includeSpeciesList.isSelected());
        includeSpeciesList.addActionListener(e -> {
          showAllTaxonomies.setEnabled(includeSpeciesList.isSelected());
        });
      }
    }
    
    void updatePreferences() {
      tripReportPreferences.includeScientific = includeScientific.isSelected();
      tripReportPreferences.includeItinerary = includeItinerary.isSelected();
      if (!speciesTableDisabled) {
        tripReportPreferences.includeSpeciesTable = includeSpeciesTable.isSelected();
      }
      tripReportPreferences.includeSpeciesList = includeSpeciesList.isSelected();
      tripReportPreferences.showAllTaxonomies = showAllTaxonomies.isSelected();
      tripReportPreferences.includeFavoritePhotos = includeFavoritePhotos.isSelected();
    }
  }

  /**
   * Derive the scientific-and-common ordering based on a mix of the chosen setting
   * in the dialog and the setting in the names preferences.
   */
  public ScientificOrCommon getScientificOrCommonFromConfiguration(TripReportPreferences prefs) {
    ScientificOrCommon scientificOrCommon = namesPreferences.scientificOrCommon;
    if (prefs.includeScientific) {
      if (scientificOrCommon == ScientificOrCommon.COMMON_ONLY) {
        // Can't have COMMON_ONLY if the user asks for scientific names;  use COMMON_FIRST
        return ScientificOrCommon.COMMON_FIRST;
      }
    } else {
      switch (scientificOrCommon) {
        case SCIENTIFIC_ONLY:
        case COMMON_FIRST:
        case SCIENTIFIC_FIRST:
          // None of the above make sense if scientific names are deselected;  switch to COMMON_ONLY mode.
          return ScientificOrCommon.COMMON_ONLY;
        default:
          break;
      }
    }
    
    return scientificOrCommon;
  }
}
