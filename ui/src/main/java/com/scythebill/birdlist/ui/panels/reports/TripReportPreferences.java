/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import com.scythebill.birdlist.model.annotations.Preference;

/**
 * Saved preferences for how reports should be output as trip reports.
 */
public class TripReportPreferences implements Cloneable {
  @Preference boolean includeScientific = true;
  @Preference boolean includeItinerary = true;
  @Preference boolean includeSpeciesTable = true;
  @Preference boolean includeSpeciesList = true;
  @Preference boolean includeFavoritePhotos = false;
  @Preference boolean showAllTaxonomies = false;
  
  @Override
  public TripReportPreferences clone() {
    try {
      return (TripReportPreferences) super.clone();
    } catch (CloneNotSupportedException e) {
      throw new AssertionError(e);
    }
  }
}
