/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * A cache of undescribed taxa per taxonomy.
 */
@Singleton
public class UndescribedTaxa {
  private final LoadingCache<Taxonomy, ImmutableSet<SightingTaxon>> undescribedTaxaCache = CacheBuilder.newBuilder()
      .build(new CacheLoader<Taxonomy, ImmutableSet<SightingTaxon>>() {
        @Override
        public ImmutableSet<SightingTaxon> load(Taxonomy taxonomy) throws Exception {
          ImmutableSet.Builder<SightingTaxon> undescribedTaxa = ImmutableSet.builder();
          
          if (taxonomy instanceof MappedTaxonomy) {
            MappedTaxonomy mappedTaxonomy = (MappedTaxonomy) taxonomy;
            TaxonUtils.visitTaxa(mappedTaxonomy, new TaxonVisitor() {
              @Override
              public boolean visitTaxon(Taxon taxon) {
                if (taxon.getStatus() == Status.UN) {
                  SightingTaxon mapping = mappedTaxonomy.getExactMapping(taxon);
                  if (mapping != null) {
                    undescribedTaxa.add(mapping);                    
                  }
                }
                return true;
              }
            });
          } else {
            TaxonUtils.visitTaxa(taxonomy, new TaxonVisitor() {
              @Override
              public boolean visitTaxon(Taxon taxon) {
                if (taxon.getStatus() == Status.UN) {
                  undescribedTaxa.add(SightingTaxons.newSightingTaxon(taxon.getId()));
                }
                return true;
              }
            });
          }
          
          return undescribedTaxa.build();
        }
        
      });
  
  @Inject
  UndescribedTaxa() {
  }
  
  /**
   * Returns a set of SightingTaxons which are undescribed in this taxonomy.
   * <p>
   * (But these SightingTaxon are for the base taxonomy.)
   */
  public ImmutableSet<SightingTaxon> undescribedTaxa(Taxonomy taxonomy) {
    return undescribedTaxaCache.getUnchecked(taxonomy);
  }
}
