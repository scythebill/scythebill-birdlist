/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Types of user output possible.
 */
public enum UserOutput {
  NONE(Name.OUTPUT_NO_NAMES),
  USER_NAMES(Name.OUTPUT_USER_NAMES),
  USER_ABBREVIATIONS(Name.OUTPUT_USER_ABBREVIATIONS),
  BOTH(Name.OUTPUT_BOTH);
  
  private final Name text;
  UserOutput(Name text) {
    this.text = text;
  }
  
  @Override
  public String toString() {
    return Messages.getMessage(text);
  }
  
  public boolean showNames() {
    return this == BOTH || this == USER_NAMES;
  }

  public boolean showAbbreviations() {
    return this == BOTH || this == USER_ABBREVIATIONS;
  }
}
