/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import javax.swing.JComboBox;
import javax.swing.JComponent;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonSyntaxException;
import com.scythebill.birdlist.model.query.SightingPredicates;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.user.User;
import com.scythebill.birdlist.model.user.UserSet;
import com.scythebill.birdlist.model.util.Indexer;
import com.scythebill.birdlist.model.util.ToString;
import com.scythebill.birdlist.ui.components.IndexerPanel;
import com.scythebill.birdlist.ui.components.IndexerPanel.LayoutStrategy;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;

/**
 * QueryField for adjusting queries relative to a set of users.
 */
class UserQueryField extends AbstractQueryField {
  private enum QueryType {
    INCLUDES(Name.USER_INCLUDES),
    DOES_NOT_INCLUDE(Name.USER_DOES_NOT_INCLUDE),
    IS_ONLY(Name.USER_IS_ONLY),
    ARE_EMPTY(Name.USER_ARE_EMPTY);
    
    private final Name text;
    QueryType(Name text) {
      this.text = text;
    }
    
    @Override
    public String toString() {
      return Messages.getMessage(text);
    }
  }
  private final JComboBox<QueryType> userOptions = new JComboBox<>(QueryType.values());
  private final IndexerPanel<User> userIndexer;
  private final UserSet userSet;

  public UserQueryField(UserSet userSet) {
    super(QueryFieldType.USER);
    this.userSet = userSet;
    
    userIndexer = new IndexerPanel<>();
    userIndexer.setColumns(15);
    userIndexer.setLayoutStrategy(LayoutStrategy.BELOW);
    if (userSet == null) {
      userIndexer.setVisible(false);
    } else {
      Indexer<User> indexer = new Indexer<>(4);
      for (User user : userSet.allUsers()) {
        if (user.name() != null) {
          indexer.add(user.name(), user);
        } else {
          indexer.add(user.abbreviation(), user);
        }
      }
      ToString<User> toString = new ToString<User>() {
        @Override
        public String getString(User user) {
          return user.name() != null ? user.name() : user.abbreviation();
        }
      };
      userIndexer.addIndexerGroup(toString, indexer);
      userIndexer.setPreviewText(Name.OBSERVER_NAME);
      userIndexer.addPropertyChangeListener("value", e -> firePredicateUpdated());
      userOptions.addActionListener(e -> firePredicateUpdated());
      userOptions.addActionListener(e -> userIndexer.setVisible(userOptions.getSelectedItem() != QueryType.ARE_EMPTY));
    }
  }

  @Override
  public JComponent getComparisonChooser() {
    return userOptions;
  }

  @Override
  public JComponent getValueField() {
    return userIndexer;
  }

  public void setUser(User user) {
    userIndexer.setValue(user);
    userOptions.setSelectedItem(QueryType.INCLUDES);
  }
  
  @Override
  public Predicate<Sighting> predicate(Taxon.Type depth) {
    Object selectedItem = userOptions.getSelectedItem();
    if (QueryType.ARE_EMPTY.equals(selectedItem)) {
      return SightingPredicates.usersAreExactly(ImmutableSet.of());
    }

    User user = userIndexer.getValue();
    if (user == null) {
      return Predicates.alwaysTrue();
    } else {
      if (QueryType.INCLUDES.equals(selectedItem)) {
        return SightingPredicates.includesUser(user);
      } else if (QueryType.DOES_NOT_INCLUDE.equals(selectedItem)) {
        return Predicates.not(SightingPredicates.includesUser(user));
      } else if (QueryType.IS_ONLY.equals(selectedItem)) {
        return SightingPredicates.usersAreExactly(ImmutableSet.of(user));
      } else {
        throw new IllegalStateException("Unknown location option: " + selectedItem);
      }
    }
  }

  @Override public boolean isNoOp() {
    return userIndexer.getValue() == null;
  }

  @Override
  public Optional<String> abbreviation() {
    User user = userIndexer.getValue();
    if (user != null) {
      if (user.abbreviation() != null) {
        return Optional.of(user.abbreviation());
      }

      if (user.name() != null) {
        return Optional.of(user.name());
      }
    }
    
    return Optional.absent();
  }

  @Override
  public Optional<String> name() {
    User user = userIndexer.getValue();
    if (user != null) {
      if (user.name() != null) {
        return Optional.of(user.name());
      }

      if (user.abbreviation() != null) {
        return Optional.of(user.abbreviation());
      }
    }
    
    return Optional.absent();
  }

  @Override
  public JsonElement persist() {
    User user = userIndexer.getValue();
    Persisted persisted =
        new Persisted((QueryType) userOptions.getSelectedItem(), user == null ? null : user.id());
    return GSON.toJsonTree(persisted);
  }

  @Override
  public void restore(JsonElement json) {
    Persisted persisted = GSON.fromJson(json, Persisted.class);
    userOptions.setSelectedItem(persisted.type);
    if (persisted.id == null) {
      userIndexer.setValue(null);
    } else {
      if (userSet == null) {
        throw new JsonSyntaxException("No users");
      }
      try {
        userIndexer.setValue(userSet.userById(persisted.id));
      } catch (IllegalArgumentException e) {
        throw new JsonSyntaxException("User " + persisted.id + " not found");
      }
    }
  }
  
  static private final Gson GSON = new Gson();
  static class Persisted {
    public Persisted(QueryType type, String id) {
      this.type = type;
      this.id = id;
    }
    
    QueryType type;
    String id;
  }
}
