/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.scythebill.birdlist.model.query.PredicateQueryDefinition;
import com.scythebill.birdlist.model.query.QueryDefinition;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.VisitInfo;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;

/**
 * QueryField for visit comments queries.
 */
class VisitCommentsQueryField extends AbstractQueryField {
  private enum QueryType {
    CONTAINS(Name.DESCRIPTION_CONTAINS, false),
    DOES_NOT_CONTAIN(Name.DESCRIPTION_DOESNT_CONTAIN, false),
    CONTAINS_CASE_SENSITIVE(Name.DESCRIPTION_CONTAINS_CASE_SENSITIVE, true),
    DOES_NOT_CONTAIN_CASE_SENSITIVE(Name.DESCRIPTION_DOESNT_CONTAIN_CASE_SENSITIVE, true);
    
    private final Name text;
    private final boolean caseSensitive;
    QueryType(Name text, boolean caseSensitive) {
      this.text = text;
      this.caseSensitive = caseSensitive;
    }
    
    @Override
    public String toString() {
      return Messages.getMessage(text);
    }
    
    boolean isCaseSensitive() {
      return caseSensitive;
    }
  }

  private final JComboBox<QueryType> options = new JComboBox<>(QueryType.values());
  private final JTextField text;
  private Timer timer;
  
  public VisitCommentsQueryField() {
    super(QueryFieldType.VISIT_COMMENTS);
    
    options.addActionListener(e -> firePredicateUpdated());
    
    text = new JTextField(20);
    timer = new Timer(200, e -> firePredicateUpdated());
    timer.setRepeats(false);

    text.getDocument().addDocumentListener(new DocumentListener() {
      @Override
      public void removeUpdate(DocumentEvent e) {
        timer.restart();
      }
      
      @Override
      public void insertUpdate(DocumentEvent e) {
        timer.restart();
      }
      
      @Override
      public void changedUpdate(DocumentEvent e) {
        timer.restart();
      }
    });
  }

  @Override
  public JComponent getComparisonChooser() {
    return options;
  }

  @Override
  public JComponent getValueField() {
    return text;
  }

  @Override
  public QueryDefinition queryDefinition(ReportSet reportSet, Taxon.Type depth) {
    return new PredicateQueryDefinition(predicate(reportSet, depth));
  }

  private Predicate<Sighting> predicate(final ReportSet reportSet, Type depth) {
    QueryType whenOption = (QueryType) options.getSelectedItem();
    final boolean isCaseSensitive = whenOption.isCaseSensitive();
    final String containsText = isCaseSensitive
        ? text.getText()
        : text.getText().toLowerCase();
        
    Predicate<Sighting> containsPredicate = new Predicate<Sighting>() {
      @Override public boolean apply(Sighting input) {
        VisitInfoKey key = VisitInfoKey.forSighting(input);
        if (key == null) {
          return false;
        }
        VisitInfo visitInfo = reportSet.getVisitInfo(key);
        if (visitInfo == null) {
          return false;
        }
        
        if (!visitInfo.comments().isPresent()) {
          return false;
        }

        String comments = visitInfo.comments().get();
        if (!isCaseSensitive) {
          comments = comments.toLowerCase();
        }
        
        return comments.contains(containsText);
      }
    };
    
    if (whenOption == QueryType.DOES_NOT_CONTAIN
        || whenOption == QueryType.DOES_NOT_CONTAIN_CASE_SENSITIVE) {
      return Predicates.not(containsPredicate);
    }

    return containsPredicate;
  }

  @Override public boolean isNoOp() {
    return false;
  }  

  @Override
  public JsonElement persist() {
    Persisted persisted = new Persisted(
        (QueryType) options.getSelectedItem(),
        text.getText());
    return GSON.toJsonTree(persisted);
  }

  @Override
  public void restore(JsonElement json) {
    Persisted persisted = GSON.fromJson(json, Persisted.class);
    options.setSelectedItem(persisted.type);
    text.setText(persisted.text);
  }
  
  static private final Gson GSON = new Gson();
  static class Persisted {
    public Persisted(QueryType type, String text) {
      this.type = type;
      this.text = text;
    }
    
    QueryType type;
    String text;
  }
}
