/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.Desktop;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.function.Function;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;
import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTimeFieldType;
import org.joda.time.Partial;
import org.joda.time.ReadablePartial;
import org.joda.time.chrono.GJChronology;
import org.joda.time.format.DateTimeFormatterBuilder;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.query.SightingPredicates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.app.NavigableFrame;
import com.scythebill.birdlist.ui.app.Titled;
import com.scythebill.birdlist.ui.components.AutoSelectJFormattedTextField;
import com.scythebill.birdlist.ui.components.TextPrompt;
import com.scythebill.birdlist.ui.components.YearFormatter;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.events.TaxonomyChangedEvent;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.panels.reports.QueryFieldFactory.QueryFieldType;
import com.scythebill.birdlist.ui.panels.reports.YearComparisonProcessor.ComparisonResult;
import com.scythebill.birdlist.ui.panels.reports.YearComparisonProcessor.YearResult;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Panel for triggering the "Year Comparison" spreadsheet.
 */
public class YearComparisonPanel extends JPanel implements FontManager.FontsUpdatedListener, Titled {

  private final ReportSet reportSet;
  private final QueryPanelFactory queryPanelFactory;
  private final TaxonomyStore taxonomyStore;
  private final QueryPreferences queryPreferences;
  private final FileDialogs fileDialogs;
  private final NavigableFrame navigableFrame;
  private JFormattedTextField fromYearField;
  private JFormattedTextField toYearField;
  private QueryPanel queryPanel;
  private JButton saveSpreadsheetButton;
  private JButton returnButton;
  private JLabel yearComparisonLabel;
  private JLabel yearComparisonExplanation;
  private JLabel yearComparisonTitle;
  private Alerts alerts;
  private JLabel fromYearLabel;
  private JLabel toYearLabel;
  private JCheckBox splitYearCheckbox;
  
  static enum SplitYearKey {
    WHOLE_YEAR {
      @Override
      public String suffixString() { return ""; }

      @Override
      public String titleString() { return ""; }
    },
    H1 {
      @Override
      public String suffixString() { return " H1"; }

      @Override
      public String titleString() { return "H1"; }
    },
    H2 {
      @Override
      public String suffixString() { return " H2"; }

      @Override
      public String titleString() { return "H2"; }
    };

    public abstract String suffixString();
    public abstract String titleString();
  }
  
  @Inject
  YearComparisonPanel(
      ReportSet reportSet,
      QueryPanelFactory queryPanelFactory,
      TaxonomyStore taxonomyStore,
      EventBusRegistrar eventBusRegistrar,
      FontManager fontManager,
      QueryPreferences queryPreferences,
      NavigableFrame navigableFrame,
      FileDialogs fileDialogs,
      Alerts alerts) {
    this.reportSet = reportSet;
    this.queryPanelFactory = queryPanelFactory;
    this.taxonomyStore = taxonomyStore;
    this.queryPreferences = queryPreferences;
    this.navigableFrame = navigableFrame;
    this.fileDialogs = fileDialogs;
    this.alerts = alerts;
    initGUI();
    attachListeners();
    fontManager.applyTo(this);
    eventBusRegistrar.registerWhenInHierarchy(this);
    taxonomyChanged(null);    
  }

  private void attachListeners() {
    returnButton.addActionListener(e -> navigableFrame.navigateTo("extendedReportsMenu"));
    queryPanel.addPredicateChangedListener(e -> showQueryResults());
    fromYearField.addPropertyChangeListener("value", e -> showQueryResults());
    toYearField.addPropertyChangeListener("value", e -> showQueryResults());
    splitYearCheckbox.addActionListener(e -> showQueryResults());
    saveSpreadsheetButton.addActionListener(e -> saveSpreadsheet());
  }

  private void initGUI() {    
    queryPanel = queryPanelFactory.newQueryPanel(
        // "subspecies allocated" is pretty weird here
        QueryFieldType.SUBSPECIES_ALLOCATED,
        QueryFieldType.TIMES_SIGHTED);
    fromYearField = new AutoSelectJFormattedTextField(new YearFormatter());
    fromYearField.setColumns(5);
    toYearField = new AutoSelectJFormattedTextField(new YearFormatter());
    toYearField.setColumns(5);
    
    fromYearLabel = new JLabel(Messages.getMessage(Name.FROM_LABEL));
    toYearLabel = new JLabel(Messages.getMessage(Name.TO_LABEL));
    TextPrompt.addToField(Name.YEAR_PROMPT, fromYearField).changeAlpha(0.5f);    
    TextPrompt.addToField(Name.YEAR_PROMPT, toYearField).changeAlpha(0.5f);
    
    splitYearCheckbox = new JCheckBox(Messages.getMessage(Name.SPLIT_YEARS_CHECKBOX));

    yearComparisonExplanation = new JLabel();
    yearComparisonExplanation.putClientProperty(FontManager.PLAIN_LABEL, true);
    yearComparisonExplanation.setText("<html>"
        + Messages.getMessage(Name.YEAR_COMPARISON_FULL_EXPLANATION));
    yearComparisonTitle = new JLabel(Messages.getMessage(Name.TOTAL_SPECIES_TEXT));
    yearComparisonTitle.putClientProperty(FontManager.TEXT_SIZE_PROPERTY, FontManager.TextSize.LARGE);
    yearComparisonLabel = new JLabel();
    yearComparisonLabel.putClientProperty(FontManager.TEXT_SIZE_PROPERTY, FontManager.TextSize.LARGE);
    returnButton = new JButton(Messages.getMessage(Name.BACK_TO_SPECIAL_REPORTS));
    saveSpreadsheetButton = new JButton(Messages.getMessage(Name.SAVE_AS_SPREADSHEET));
  }

  @Override
  public void fontsUpdated(FontManager fontManager) {
    setBorder(new EmptyBorder(
        fontManager.scale(20),
        fontManager.scale(20),
        fontManager.scale(20),
        fontManager.scale(20)));
    GroupLayout layout = new GroupLayout(this);
    layout.setHorizontalGroup(
        layout.createParallelGroup(Alignment.LEADING)
            .addComponent(queryPanel, PREFERRED_SIZE, PREFERRED_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(fromYearLabel)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(fromYearField, PREFERRED_SIZE, fontManager.scale(70), PREFERRED_SIZE)
                .addPreferredGap(ComponentPlacement.UNRELATED)
                .addComponent(toYearLabel)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(toYearField, PREFERRED_SIZE, fontManager.scale(70), PREFERRED_SIZE)
                .addPreferredGap(ComponentPlacement.UNRELATED)
                .addComponent(splitYearCheckbox))
            .addComponent(yearComparisonTitle)
            .addComponent(yearComparisonLabel)
            .addComponent(yearComparisonExplanation)
            .addGroup(layout.createSequentialGroup()
                .addComponent(saveSpreadsheetButton))
            .addComponent(returnButton, Alignment.TRAILING));
    
    layout.setVerticalGroup(
        layout.createSequentialGroup()
            .addComponent(yearComparisonExplanation)
            .addPreferredGap(ComponentPlacement.UNRELATED)
            .addPreferredGap(ComponentPlacement.RELATED)
            .addGroup(layout.createBaselineGroup(false, false)
                .addComponent(fromYearLabel)
                .addComponent(fromYearField)
                .addComponent(toYearLabel)
                .addComponent(toYearField)
                .addComponent(splitYearCheckbox))
            .addPreferredGap(ComponentPlacement.RELATED)
            .addComponent(queryPanel, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE)
            .addPreferredGap(ComponentPlacement.UNRELATED)
            .addComponent(yearComparisonTitle)
            .addComponent(yearComparisonLabel)
            .addGap(fontManager.scale(10), fontManager.scale(10), Short.MAX_VALUE)
            .addGroup(
                layout.createBaselineGroup(false, false)
                    .addComponent(saveSpreadsheetButton)
                    .addComponent(returnButton)));
    
    setLayout(layout);
  }
  
 
  @Subscribe
  public void taxonomyChanged(TaxonomyChangedEvent event) {
    queryPanel.taxonomyUpdated();
    showQueryResults();
  }
  
  private void showQueryResults() {
    ComparisonResult<SplitYearKey> yearComparisonResults = computeYearComparison();
    yearComparisonLabel.setText(NumberFormat.getIntegerInstance().format(yearComparisonResults.taxaCount()));
  }
  
  private ComparisonResult<SplitYearKey> computeYearComparison() {
    Taxonomy taxonomy = taxonomyStore.getTaxonomy();
    Predicate<Sighting> countablePredicate = queryPreferences.getCountablePredicate(
        taxonomy,
        queryPanel.containsQueryFieldType(QueryFieldType.SIGHTING_STATUS),
        queryPanel.getBooleanValueIfPresent(QueryFieldType.ALLOW_HEARD_ONLY));

    Predicate<Sighting> yearRangePredicate = null;
    if (!fromYearField.getText().isEmpty()) {
      int fromYear = (Integer) fromYearField.getValue();
      Partial fromDate = new Partial(GJChronology.getInstance()).with(DateTimeFieldType.year(), fromYear);
      yearRangePredicate = SightingPredicates.afterOrEquals(fromDate);
    }

    if (!toYearField.getText().isEmpty()) {
      int toYear = (Integer) toYearField.getValue();
      Partial toDate = new Partial(GJChronology.getInstance()).with(DateTimeFieldType.year(), toYear);
      Predicate<Sighting> toPredicate = SightingPredicates.beforeOrEquals(toDate);
      
      yearRangePredicate = yearRangePredicate == null
          ? toPredicate
          : Predicates.and(yearRangePredicate, toPredicate);
    }
   
     
    Function<Sighting, SplitYearKey> splitFunction;
    if (splitYearCheckbox.isSelected()) {
      splitFunction = s -> {
        ReadablePartial date = s.getDateAsPartial();
        if (date == null || !date.isSupported(DateTimeFieldType.monthOfYear())) {
          return null;
        }
        int month = date.get(DateTimeFieldType.monthOfYear());
        return month <= 6 ? SplitYearKey.H1 : SplitYearKey.H2;
      };
    } else {
      splitFunction = s -> SplitYearKey.WHOLE_YEAR;
    }
    return new YearComparisonProcessor(reportSet,
        taxonomy, queryPanel.queryDefinition(taxonomy, Type.species), countablePredicate)
        .process(yearRangePredicate, splitFunction);
  }

  private void saveSpreadsheet() {
    Optional<String> reportName = queryPanel.getQueryAbbreviation();
    String fileName = reportName.isPresent()
        ? String.format("year-comparison-%s.xlsx", reportName.get())
        : String.format("year-comparison.xlsx");
    File out = fileDialogs.saveFile(UIUtils.findFrame(this),
        Messages.getMessage(Name.PICK_FILE_TITLE),
        fileName,
        new FileNameExtensionFilter(
            Messages.getMessage(Name.XLSX_FILES), "xlsx"), FileType.OTHER);
    if (out != null) {
      // Compute the main total ticks multimap (keys are taxa, values are locations)
      ComparisonResult<SplitYearKey> result = computeYearComparison();
      int earliestYear = result.getEarliestYear();
      int latestYear = result.getLatestYear();
      int yearCount = latestYear - earliestYear + 1;
      
      boolean splitYear = splitYearCheckbox.isSelected();
      int yearSplits = splitYear ? 2 : 1;
      SplitYearKey[] keys = splitYear
          ? new SplitYearKey[] {SplitYearKey.H1, SplitYearKey.H2}
          : new SplitYearKey[] {SplitYearKey.WHOLE_YEAR};

      // If there's more than one year, include a few extra column for overall totals 
      int totalColumnsWidth = (yearCount > 1 ? 4 : 0) * yearSplits;
      
      Workbook workbook = new XSSFWorkbook();
      
      CellStyle headerStyle = workbook.createCellStyle();
      Font headerFont = workbook.createFont();
      headerFont.setBold(true);
      headerStyle.setFont(headerFont);
      
      CellStyle centerHeaderStyle = workbook.createCellStyle();
      centerHeaderStyle.cloneStyleFrom(headerStyle);
      centerHeaderStyle.setAlignment(HorizontalAlignment.CENTER);

      CellStyle rotatedHeaderStyle = workbook.createCellStyle();
      rotatedHeaderStyle.cloneStyleFrom(headerStyle);
      rotatedHeaderStyle.setRotation((short) 90);
      
      CellStyle blankColoredCell = workbook.createCellStyle();
      ((XSSFCellStyle) blankColoredCell).setFillPattern(FillPatternType.SOLID_FOREGROUND);
      ((XSSFCellStyle) blankColoredCell).setFillForegroundColor(new XSSFColor(IndexedColors.YELLOW, new DefaultIndexedColorMap()));
      blankColoredCell.setBorderLeft(BorderStyle.THIN);
      blankColoredCell.setBorderRight(BorderStyle.THIN);
      
      Sheet sheet = workbook.createSheet();
      
      // First row - preface
      Row prefaceHeader = sheet.createRow(0);
      Location rootLocation = queryPanel.getRootLocation();
      if (rootLocation != null) {
        CellUtil.createCell(prefaceHeader, 0, rootLocation.getDisplayName(), headerStyle);
      }
      
      for (int i = 0; i < yearCount * yearSplits + (yearSplits - 1); i++) {
        CellUtil.createCell(prefaceHeader, i * 4 + 4, "", blankColoredCell);
      }
      
      // Second row:  years
      Row yearRow = sheet.createRow(1);

      // Total titles, if needed
      if (yearCount > 1) {
        for (int i = 0; i < yearSplits; i++) {
          CellUtil.createCell(
              yearRow, i * 4 + 1, keys[i].titleString(), centerHeaderStyle);
          sheet.addMergedRegion(
              new CellRangeAddress(1, 1, i * 4 + 1, i * 4 + 3));
          CellUtil.createCell(yearRow, i * 4 + 4, "", blankColoredCell);
        }
      }

      for (int i = 0; i < yearCount * yearSplits; i++) {
        String yearName = "" + (earliestYear + (i / yearSplits)) + keys[i % yearSplits].suffixString();

        CellUtil.createCell(
            yearRow, i * 4 + 1 + totalColumnsWidth, yearName, centerHeaderStyle);
        sheet.addMergedRegion(
            new CellRangeAddress(1, 1, i * 4 + 1 + totalColumnsWidth, i * 4 + 3 + totalColumnsWidth));
        CellUtil.createCell(yearRow, i * 4 + totalColumnsWidth, "", blankColoredCell);
      }

      // Third row: main headers
      Row headerRow = sheet.createRow(2);
      CellUtil.createCell(headerRow, 0, Messages.getMessage(Name.SPECIES_TEXT), rotatedHeaderStyle);
      for (int i = 0; i < ((yearCount == 1) ? 1 : yearCount + 1) * yearSplits; i++) {
        CellUtil.createCell(headerRow, i * 4 + 1, Messages.getMessage(Name.FIRST_SEEN), rotatedHeaderStyle);
        CellUtil.createCell(headerRow, i * 4 + 2, Messages.getMessage(Name.LAST_SEEN), rotatedHeaderStyle);
        CellUtil.createCell(headerRow, i * 4 + 3, Messages.getMessage(Name.NUMBER_TEXT), rotatedHeaderStyle);
        if (i < ((yearCount + 1) * yearSplits) - 1) {
          CellUtil.createCell(headerRow, i * 4 + 4, "", blankColoredCell);
        }
      }

      int[] yearTotals = new int[(yearCount == 1 ? 1 : yearCount + 1) * yearSplits];
      
      // Each taxon on a separate row
      for (int i = 0; i < result.taxaCount(); i++) {
        Row row = sheet.createRow(3 + i);
        ComparisonResult<SplitYearKey>.SpeciesResult speciesResult = result.getSpeciesResult(i);
        Resolved resolved = speciesResult.getResolved();
        CellUtil.createCell(row, 0, resolved.getCommonName());
        
        if (yearCount > 1) {
          for (int yearSplit = 0; yearSplit < yearSplits; yearSplit++) {
            YearResult total = speciesResult.total(keys[yearSplit]);
            if (total.frequency() > 0) {
              if (total.earliest() != null) {
                CellUtil.createCell(row, 4 * yearSplit + 1,
                    String.format("%s (%s)",
                        toUserStringWithoutYear(total.earliest()),
                        total.earliest().get(DateTimeFieldType.year())));
              }
  
              if (total.latest() != null) {
                CellUtil.createCell(row, 4 * yearSplit + 2,
                    String.format("%s (%s)",
                        toUserStringWithoutYear(total.latest()),
                        total.latest().get(DateTimeFieldType.year())));
              }
  
              Cell frequencyCell = row.createCell(4 * yearSplit + 3, CellType.NUMERIC);
              frequencyCell.setCellValue(total.frequency());
              
              yearTotals[yearSplit]++;
            }
  
            CellUtil.createCell(row, 4 * yearSplit + 4, "", blankColoredCell);
          }
        }
        
        for (int j = 0; j < yearCount * yearSplits; j++) {
          YearResult yearResult = speciesResult.getYearResult(
              keys[j % yearSplits],
              earliestYear + (j / yearSplits));
          if (yearResult != null) {
            // Output without a year, since the year is fully set
            CellUtil.createCell(row, j * 4 + 1 + totalColumnsWidth, toUserStringWithoutYear(yearResult.earliest()));
            CellUtil.createCell(row, j * 4 + 2 + totalColumnsWidth, toUserStringWithoutYear(yearResult.latest()));
            
            Cell frequencyCell = row.createCell(j * 4 + 3 + totalColumnsWidth, CellType.NUMERIC);
            frequencyCell.setCellValue(yearResult.frequency());
            
            yearTotals[yearCount == 1 ? j : j + yearSplits]++;
          }

          CellUtil.createCell(row, j * 4 + (4 * yearSplits), "", blankColoredCell);
        }
      }
      
      // Write the accumulated year totals in the appropriate column
      for (int i = 0; i < yearTotals.length; i++) {
        CellUtil.createCell(prefaceHeader, i * 4 + 1, Messages.getFormattedMessage(Name.SPECIES_WITH_NUMBER, yearTotals[i]));
        sheet.addMergedRegion(new CellRangeAddress(0, 0, i * 4 + 1, i * 4 + 3));
        
      }
      
      // Auto-size the species names
      sheet.autoSizeColumn(0);
      
      // Use narrow widths for each of the colored columns
      for (int i = 0; i < yearTotals.length - 1; i++) {
        sheet.setColumnWidth(i * 4 + 4, 128);
      }
      
      if (yearCount > 1) {
        // And the first and last seen dates
        for (int yearSplit = 0; yearSplit < yearSplits; yearSplit++) {
          sheet.autoSizeColumn(1 + (4 * yearSplit));
          sheet.autoSizeColumn(2 + (4 * yearSplit));
        }
      }
      
      // Freeze the leftmost four (* splits) columns and top three rows
      sheet.createFreezePane(4 * yearSplits, 3);
      
      try (OutputStream stream = new BufferedOutputStream(new FileOutputStream(out))) {
        workbook.write(stream);
        workbook.close();
      } catch (IOException e) {
        FileDialogs.showFileSaveError(alerts, e, out);
        return;
      }
      
      try {
        Desktop.getDesktop().open(out);
      } catch (IOException e) {
        alerts.showError(this, e,
            Name.OPENING_FAILED, Name.NO_APPLICATION_FOR_XLS_FILES);
        return;
      }
    }
  }
  
  private String toUserStringWithoutYear(ReadablePartial p) {
    if (p == null) {
      return "";
    }
    
    DateTimeFormatterBuilder builder = new DateTimeFormatterBuilder();
    if (p.isSupported(DateTimeFieldType.dayOfMonth())) {
      builder.appendDayOfMonth(1);
      builder.appendLiteral(' ');
    }

    if (p.isSupported(DateTimeFieldType.monthOfYear())) {
      builder.appendShortText(DateTimeFieldType.monthOfYear());
    }
    
    StringBuffer buffer = new StringBuffer();
    builder.toPrinter().printTo(buffer, p, Locale.getDefault());
    return buffer.toString();
  }

  @Override
  public String getTitle() {
    return Messages.getMessage(Name.YEAR_COMPARISONS);
  }
}
