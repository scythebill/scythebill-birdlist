/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.reports;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.joda.time.ReadablePartial;

import com.google.common.base.Predicate;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.query.QueryDefinition;
import com.scythebill.birdlist.model.query.QueryProcessor;
import com.scythebill.birdlist.model.query.QueryResults;
import com.scythebill.birdlist.model.query.SightingComparators;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Processor for generating year-comparison data.
 */
class YearComparisonProcessor {
  /** At most, support a century of comparisons. */
  private static final int MAXIMUM_YEAR_SPAN = 100;
  private Predicate<Sighting> countablePredicate;
  private ReportSet reportSet;
  private Taxonomy taxonomy;
  private QueryDefinition queryDefinition;

  public YearComparisonProcessor(
      ReportSet reportSet,
      Taxonomy taxonomy,
      QueryDefinition queryDefinition,
      Predicate<Sighting> countablePredicate) {
    this.reportSet = reportSet;
    this.taxonomy = taxonomy;
    this.queryDefinition = queryDefinition;
    this.countablePredicate = countablePredicate;
  }

  public <K> ComparisonResult<K> process(Predicate<Sighting> additionalPredicate,
      Function<Sighting, K> splitFunction) {
    QueryProcessor queryProcessor = new QueryProcessor(reportSet, taxonomy);
    queryProcessor.onlyIncludeCountableSightings().dontIncludeFamilyNames();
    if (additionalPredicate != null) {
      queryProcessor = queryProcessor.withAdditionalPredicate(additionalPredicate);
    }
    
    QueryResults queryResults = queryProcessor.runQuery(
        queryDefinition,
        countablePredicate,
        Taxon.Type.species);
    
    
    int latestYear = Integer.MIN_VALUE;
    int earliestYear = Integer.MAX_VALUE;
    
    for (Sighting sighting : queryResults.getAllSightings()) {
      ReadablePartial date = sighting.getDateAsPartial();
      if (date != null && date.isSupported(DateTimeFieldType.year())) {
        int year = date.get(DateTimeFieldType.year());
        if (year > latestYear) {
          latestYear = year;
        }
        if (year < earliestYear) {
          earliestYear = year;
        }
      }
    }
   
    // No data...
    if (latestYear == Integer.MIN_VALUE) {
      int currentYear = new DateTime().getYear();
      return new ComparisonResult<K>(currentYear, currentYear);
    }

    latestYear = Math.min(latestYear, earliestYear + MAXIMUM_YEAR_SPAN);

    ComparisonResult<K> comparisonResult = new ComparisonResult<>(earliestYear, latestYear);
    
    for (Resolved resolved : queryResults.getTaxaAsList()) {
      ComparisonResult<K>.SpeciesResult speciesResult = comparisonResult.addSpeciesResult(resolved);
      Collection<Sighting> sightings = queryResults.getAllSightings(resolved);
      for (Sighting sighting : sightings) {
        if (countablePredicate.test(sighting)) {
          speciesResult.addSighting(sighting, splitFunction);
        }
      }
    }

    return comparisonResult;
  }
  
  public static class YearResult {
    private ReadablePartial earliest;
    private ReadablePartial latest;
    private int frequency;
    
    private YearResult(ReadablePartial earliest, ReadablePartial latest, int frequency) {
      this.earliest = earliest;
      this.latest = latest;
      this.frequency = frequency;
      
    }
    
    private YearResult(Sighting sighting) {
      if (sighting.getDateAsPartial().isSupported(DateTimeFieldType.monthOfYear())) {
        earliest = latest = sighting.getDateAsPartial();
      }
      frequency = 1;
    }
    
    private void addSighting(Sighting sighting) {
      frequency++;
      ReadablePartial date = sighting.getDateAsPartial();
      // Don't include any sightings in "earliest" or "latest" that don't have 
      // a month;  these are pointless.
      if (date.isSupported(DateTimeFieldType.monthOfYear())) {
        if (earliest == null) {
          earliest = date;
        } else if (SightingComparators.comparePartials(date, earliest) < 0) {
          earliest = date;
        }
  
        if (latest == null) {
          latest = date;
        } else if (SightingComparators.comparePartials(date, latest) > 0) {
          latest = date;
        }
      }
    }
    
    public ReadablePartial earliest() {
      return earliest;
    }
    
    public ReadablePartial latest() {
      return latest;
    }
    
    public int frequency() {
      return frequency;
    }
    
    public String outputAsText() {
      return String.format("%d [%s -> %s]", frequency, PartialIO.toString(earliest), PartialIO.toString(latest));
    }
  }
  
  public static class ComparisonResult<K> {
    private final int earliestYear;
    private final int latestYear;
    private final List<SpeciesResult> speciesResults = new ArrayList<>();
    
    public ComparisonResult(int earliestYear, int latestYear) {
      this.earliestYear = earliestYear;
      this.latestYear = latestYear;
    }

    private SpeciesResult addSpeciesResult(Resolved resolved) {
      SpeciesResult speciesResult = new SpeciesResult(resolved);
      speciesResults.add(speciesResult);
      return speciesResult;
    }
    
    public class SpeciesResult {
      final Resolved resolved;
      final Map<K, YearResult[]> yearMap = new LinkedHashMap<>();  
      
      public SpeciesResult(Resolved resolved) {
        this.resolved = resolved;
      }
      
      private YearResult[] getYearResultArray(K key) {
        YearResult[] yearResults = yearMap.get(key);
        if (yearResults == null) {
          yearResults = new YearResult[getLatestYear() - earliestYear + 1];
          yearMap.put(key, yearResults);
        }
        
        return yearResults;
      }
      
      public void addSighting(Sighting sighting, Function<Sighting, K> splitFunction) {
        ReadablePartial date = sighting.getDateAsPartial();
        if (date != null && date.isSupported(DateTimeFieldType.year())) {
          int year = date.get(DateTimeFieldType.year());
          if (year >= earliestYear && year <= getLatestYear()) {
            K key = splitFunction.apply(sighting);
            if (key == null) {
              return;
            }
            
            YearResult[] years = getYearResultArray(key);
            int index = year - earliestYear;
            YearResult result = years[index];
            if (result == null) {
              years[index] = new YearResult(sighting);
            } else {
              result.addSighting(sighting);
            }
          }
        }
      }
  
      public String outputAsText() {
        StringBuilder builder = new StringBuilder(resolved.getCommonName());
        for (K key : yearMap.keySet()) {
          YearResult[] years = getYearResultArray(key);
          for (YearResult year : years) {
            builder.append(",");
            if (year != null) {
              builder.append(year.outputAsText());
            }
          }
        }
        return builder.toString();
      }

      public Resolved getResolved() {
        return resolved;
      }
      
      public YearResult getYearResult(K key, int year) {
        return getYearResultArray(key)[year - earliestYear];
      }
      
      public YearResult total(K key) {
        int frequency = 0;
        ReadablePartial earliest = null;        
        ReadablePartial latest = null;
        
        for (YearResult year : getYearResultArray(key)) {
          if (year != null) {
            frequency += year.frequency();

            if (earliest == null) {
              earliest = year.earliest();
            } else if (year.earliest() != null
                && lessThanIgnoringYear(year.earliest(), earliest)) {
              earliest = year.earliest();
            }

            if (latest == null) {
              latest = year.latest();
            } else if (year.latest() != null
                && greaterThanIgnoringYear(year.latest(), latest)) {
              latest = year.latest();
            }
          }
        }
        return new YearResult(earliest, latest, frequency);
      }
    }

    public int taxaCount() {
      return speciesResults.size();
    }

    public int getEarliestYear() {
      return earliestYear;
    }

    public int getLatestYear() {
      return latestYear;
    }

    public SpeciesResult getSpeciesResult(int i) {
      return speciesResults.get(i);
    }
    
  }  

  /**
   * Returns true if {@code a} is definitely before {@code b}, ignoring the year.
   * Anything missing a field can't be true. 
   */
  private static boolean lessThanIgnoringYear(ReadablePartial a, ReadablePartial b) {
    int aMonth = a.isSupported(DateTimeFieldType.monthOfYear())
        ? a.get(DateTimeFieldType.monthOfYear()) : 13;
    int bMonth = b.isSupported(DateTimeFieldType.monthOfYear())
        ? b.get(DateTimeFieldType.monthOfYear()) : 13;
    if (aMonth < bMonth) {
      return true;
    } else if (aMonth > bMonth) {
      return false;
    }
    
    int aDay = a.isSupported(DateTimeFieldType.dayOfMonth())
        ? a.get(DateTimeFieldType.dayOfMonth()) : 32;
    int bDay = b.isSupported(DateTimeFieldType.dayOfMonth())
        ? b.get(DateTimeFieldType.dayOfMonth()) : 32;
    
    return aDay < bDay;
  }

  /**
   * Returns true if {@code a} is definitely after {@code b}, ignoring the year.
   * Anything missing a field can't be true. 
   */
  private static boolean greaterThanIgnoringYear(ReadablePartial a, ReadablePartial b) {
    int aMonth = a.isSupported(DateTimeFieldType.monthOfYear())
        ? a.get(DateTimeFieldType.monthOfYear()) : -1;
    int bMonth = b.isSupported(DateTimeFieldType.monthOfYear())
        ? b.get(DateTimeFieldType.monthOfYear()) : -1;
    if (aMonth > bMonth) {
      return true;
    } else if (aMonth < bMonth) {
      return false;
    }
    
    int aDay = a.isSupported(DateTimeFieldType.dayOfMonth())
        ? a.get(DateTimeFieldType.dayOfMonth()) : -1;
    int bDay = b.isSupported(DateTimeFieldType.dayOfMonth())
        ? b.get(DateTimeFieldType.dayOfMonth()) : -1;
    
    return aDay > bDay;
  }
}
