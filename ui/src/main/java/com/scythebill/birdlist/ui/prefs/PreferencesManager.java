/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.prefs;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import com.google.common.collect.ClassToInstanceMap;
import com.google.common.collect.MutableClassToInstanceMap;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.scythebill.birdlist.model.annotations.Preference;
import com.scythebill.birdlist.model.annotations.SerializeAsJson;

/**
 * Handles loading and saving preferences to the generic JavaSE preferences
 * store.  Uses introspection and annotations to identify preference fields.
 */
@Singleton
public class PreferencesManager {
  private static final Logger logger = Logger.getLogger(PreferencesManager.class.getName());
  private final ClassToInstanceMap<Object> instances = MutableClassToInstanceMap.create();
  private final Gson gson;

  @Inject
  public PreferencesManager(Gson gson) {
    this.gson = gson;
  }
  
  public synchronized <T> T getPreference(Class<T> type) {
    T instance = instances.getInstance(type);
    if (instance == null) {
      try {
        // TODO: Use Guice to instantiate?  Caused painful classloading
        // issues the last time it was tried...
        Constructor<T> constructor = type.getDeclaredConstructor();
        constructor.setAccessible(true);
        instance = constructor.newInstance();
        readPreferences(type, instance);
      } catch (IllegalAccessException | InstantiationException
          | InvocationTargetException | NoSuchMethodException | SecurityException e) {
        throw new RuntimeException(e);
      }
    }
    
    instances.putInstance(type, instance);
    
    return instance;
  }
  
  public synchronized <T> T getPreference(Class<T> type, Supplier<T> supplier) {
    T instance = instances.getInstance(type);
    if (instance == null) {
      try {
        instance = supplier.get();
        readPreferences(type, instance);
      } catch (IllegalAccessException | SecurityException e) {
        throw new RuntimeException(e);
      }
    }
    
    instances.putInstance(type, instance);
    
    return instance;
  }

  public synchronized void save() {
    for (Map.Entry<Class<?>, ?> entry : instances.entrySet()) {
      try {
        savePreferences(entry.getKey(), entry.getValue());
      } catch (IllegalAccessException e) {
        throw new RuntimeException(e);
      } catch (BackingStoreException e) {
        throw new RuntimeException(e);
      }
    }
  }

  private void savePreferences(Class<?> type, Object instance) throws IllegalAccessException, BackingStoreException {
    Preferences preferences = Preferences.userNodeForPackage(type);
    for (Field f : type.getDeclaredFields()) {
      f.setAccessible(true);
      String key = getPreferenceKey(type, f);
      Preference annotation = f.getAnnotation(Preference.class);
      if (annotation != null) {
        if (f.getAnnotation(SerializeAsJson.class) != null) {
          Object value = f.get(instance);
          if (value != null) {
            String json = gson.toJson(value);
            preferences.put(key, json);
          }
        } else if (f.getType() == String.class) {
          String value = (String) f.get(instance);
          if (value == null) {
            preferences.remove(key);
          } else {
            preferences.put(key, value);
          }
        } else if (f.getType().isEnum()) {
          Enum<?> enumValue = (Enum<?>) f.get(instance);
          preferences.put(key, enumValue.name());
        } else if (f.getType() == Boolean.class || f.getType() == Boolean.TYPE) {
          Boolean boolValue = (Boolean) f.get(instance);
          if (boolValue != null) {
            preferences.putBoolean(key, boolValue);
          }
        } else if (f.getType() == Integer.class || f.getType() == Integer.TYPE) {
          Integer intValue = (Integer) f.get(instance);
          if (intValue != null) {
            preferences.putInt(key, intValue); 
          }
        } else if (f.getType() == Long.class || f.getType() == Long.TYPE) {
          Long longValue = (Long) f.get(instance);
          if (longValue != null) {
            preferences.putLong(key, longValue); 
          }
        } else if (serializeTypeAsJson(f.getType())) {
          Object value = f.get(instance);
          if (value != null) {
            String json = gson.toJson(value);
            preferences.put(key, json);
          }
        } else {
          throw new IllegalStateException("Unsupported preference type: " + f.getType().getSimpleName());
        }
      }
    }
    preferences.flush();
  }
  
  private void readPreferences(Class<?> type, Object instance) throws IllegalAccessException {
    Preferences preferences = Preferences.userNodeForPackage(type);
    for (Field f : type.getDeclaredFields()) {
      f.setAccessible(true);
      String key = getPreferenceKey(type, f);
      Preference annotation = f.getAnnotation(Preference.class);
      if (annotation != null) {
        Object defaultValue = f.get(instance);
        if (f.getAnnotation(SerializeAsJson.class) != null) {
          String stringValue = preferences.get(key, "");
          if (!stringValue.isEmpty()) {
            try {
              Object value = gson.fromJson(stringValue, f.getGenericType());
              f.set(instance, value);
            } catch (JsonSyntaxException e) {
              logger.warning("Ignoring preference (" + key + ") invalid value (" + stringValue + ")");
            }
          }
        } else if (f.getType() == String.class) {
          String value = preferences.get(key, (String) defaultValue);          
          f.set(instance, value);
        } else if (f.getType().isEnum()) {
          @SuppressWarnings({"unchecked", "rawtypes"})
          Class<? extends Enum> enumType = (Class<? extends Enum>) f.getType();
          Enum<?> enumDefaultValue = (Enum<?>) defaultValue;
          String stringValue = preferences.get(key, enumDefaultValue == null ? null : enumDefaultValue.name());
          try {
            @SuppressWarnings("unchecked")
            Enum<?> enumValue = stringValue == null ? null : Enum.valueOf(enumType, stringValue);
            f.set(instance, enumValue);
          } catch (IllegalArgumentException e) {
            logger.warning("Ignoring preference (" + key + ") invalid value (" + stringValue + ")");
          }
        } else if (f.getType() == Boolean.class || f.getType() == Boolean.TYPE) {
          try {
            boolean boolValue = preferences.getBoolean(key, (Boolean) defaultValue);
            f.set(instance, boolValue);
          } catch (IllegalArgumentException e) {
            logger.log(Level.WARNING, "Ignoring preference (" + key + ")", e);
          }
        } else if (f.getType() == Integer.class || f.getType() == Integer.TYPE) {
          try {
            int intValue = preferences.getInt(key, (Integer) defaultValue);
            f.set(instance, intValue);
          } catch (IllegalArgumentException e) {
            logger.log(Level.WARNING, "Ignoring preference (" + key + ")", e);
          }
        } else if (f.getType() == Long.class || f.getType() == Long.TYPE) {
          try {
            long longValue = preferences.getLong(key, (Long) defaultValue);
            f.set(instance, longValue);
          } catch (IllegalArgumentException e) {
            logger.log(Level.WARNING, "Ignoring preference (" + key + ")", e);
          }
        } else if (serializeTypeAsJson(f.getType())) {
          try {
            String stringValue = preferences.get(key, "");
            if (!stringValue.isEmpty()) {
              Object value = gson.fromJson(stringValue, f.getGenericType());
              f.set(instance, value);
            }
          } catch (IllegalArgumentException e) {
            logger.log(Level.WARNING, "Ignoring preference (" + key + ")", e);
          } catch (JsonSyntaxException e) {
            logger.log(Level.WARNING, "Ignoring preference (" + key + ")", e);
          }
        } else {
          throw new IllegalStateException("Unsupported preference type: " + f.getType().getSimpleName());
        }
      }
    }
  }
  
  private boolean serializeTypeAsJson(Class<?> type) {
    return type.isAnnotationPresent(SerializeAsJson.class);
  }

  private String getPreferenceKey(Class<?> type, Field f) {
    return (type.getSimpleName() + "_" + f.getName());
  }
}
