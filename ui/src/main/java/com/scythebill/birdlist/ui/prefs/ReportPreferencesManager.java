/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.prefs;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.common.base.Strings;
import com.google.common.collect.ClassToInstanceMap;
import com.google.common.collect.MutableClassToInstanceMap;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.annotations.Preference;
import com.scythebill.birdlist.model.annotations.SerializeAsJson;
import com.scythebill.birdlist.model.sighting.ReportSet;

/**
 * Handles loading and saving preferences to a JSON blob in a ReportSet.
 * Uses introspection and annotations to identify preference fields.
 */
public class ReportPreferencesManager {
  private static final Logger logger = Logger.getLogger(ReportPreferencesManager.class.getName());
  private final ClassToInstanceMap<Object> instances = MutableClassToInstanceMap.create();
  private final Gson gson;
  private final JsonObject json;
  private final ReportSet reportSet;
  
  @Inject
  public ReportPreferencesManager(ReportSet reportSet, Gson gson) {
    this.gson = gson;
    this.reportSet = reportSet;
    String preferencesJson = reportSet.getPreferencesJson();

    JsonObject json = new JsonObject();
    if (!Strings.isNullOrEmpty(preferencesJson)) {
      try {
        json = new JsonParser().parse(reportSet.getPreferencesJson()).getAsJsonObject();
      } catch (JsonParseException e) {
        logger.log(Level.WARNING, "Invalid JSON", e);
      }
    }
    this.json = json;
  }
  
  public synchronized <T> T getPreference(Class<T> type) {
    T instance = instances.getInstance(type);
    if (instance == null) {
      try {
        // TODO: Use Guice to instantiate?  Caused painful classloading
        // issues the last time it was tried...
        Constructor<T> constructor = type.getDeclaredConstructor();
        constructor.setAccessible(true);
        instance = constructor.newInstance();
        
        // If it's a top-level @SerializeAsJson object, then
        // read it directly from the prefs without a wrapper
        if (serializeTypeAsJson(type)) {
          String classKey = type.getCanonicalName();
          if (json.has(classKey)) {
            JsonObject classPrefs = json.getAsJsonObject(type.getCanonicalName());
            try {
              instance = gson.fromJson(classPrefs, type);
            } catch (JsonParseException e) {
              logger.log(Level.WARNING, "Failure parsing " + classKey, e);
            }
          }
        } else {
          readPreferences(type, instance);
        }
      } catch (IllegalAccessException | InstantiationException | IllegalArgumentException
          | InvocationTargetException | NoSuchMethodException | SecurityException e) {
        throw new RuntimeException(e);
      }
    }
    
    instances.putInstance(type, instance);
    
    return instance;
  }
  
  public synchronized void save(boolean markDirty) {
    for (Map.Entry<Class<?>, ?> entry : instances.entrySet()) {
      try {
        savePreferences(entry.getKey(), entry.getValue());
      } catch (IllegalAccessException e) {
        throw new RuntimeException(e);
      }
    }
    
    reportSet.setPreferencesJson(gson.toJson(json), markDirty);
  }

  private void savePreferences(Class<?> type, Object instance) throws IllegalAccessException{
    String classKey = type.getCanonicalName();
    // If it's a top-level JSON object, save the whole thing into the prefs tree
    // instead of looking for Prefs fields.
    if (serializeTypeAsJson(type)) {
      JsonElement jsonTree = gson.toJsonTree(instance);
      json.add(classKey, jsonTree);
    } else {
      // Add an empty property if one is not set yet
      if (!json.has(classKey)) {
        json.add(classKey, new JsonObject());
      }
      
      JsonObject classPrefs = json.getAsJsonObject(classKey);
      for (Field f : type.getDeclaredFields()) {
        f.setAccessible(true);
        String fieldKey = f.getName();
        Preference annotation = f.getAnnotation(Preference.class);
        if (annotation != null) {
          Object value = f.get(instance);
          if (value == null) {
            classPrefs.remove(fieldKey);
          } else {
            if (f.getType() == String.class) {
              classPrefs.addProperty(fieldKey, (String) value);
            } else if (f.getType().isEnum()) {
              Enum<?> enumValue = (Enum<?>) value;
              classPrefs.addProperty(fieldKey, enumValue.name());
            } else if (f.getType() == Boolean.class || f.getType() == Boolean.TYPE) {
              Boolean boolValue = (Boolean) value;
              classPrefs.addProperty(fieldKey, boolValue);
            } else if (f.getType() == Integer.class || f.getType() == Integer.TYPE) {
              Integer intValue = (Integer) value;
              classPrefs.addProperty(fieldKey, intValue);
            } else if (f.getType() == Long.class || f.getType() == Long.TYPE) {
              Long longValue = (Long) value;
              classPrefs.addProperty(fieldKey, longValue);
            } else if (serializeTypeAsJson(f.getType())) {
              JsonElement jsonTree = gson.toJsonTree(value);
              classPrefs.add(fieldKey, jsonTree);
            } else {
              throw new IllegalStateException("Unsupported preference type: " + f.getType().getSimpleName());
            }
          }
        }
      }
    }
  }
  
  private void readPreferences(Class<?> type, Object instance) throws IllegalAccessException {
    String classKey = type.getCanonicalName();
    if (json.has(classKey)) {
      JsonObject classPrefs = json.getAsJsonObject(classKey);
      for (Field f : type.getDeclaredFields()) {
        f.setAccessible(true);
        String fieldKey = f.getName();
        Preference annotation = f.getAnnotation(Preference.class);
        if (annotation != null && classPrefs.has(fieldKey)) {
          JsonElement fieldJson = classPrefs.get(fieldKey);
          if (f.getType() == String.class) {
            String value = fieldJson.getAsString();          
            f.set(instance, value);
          } else if (f.getType().isEnum()) {
            @SuppressWarnings({"unchecked", "rawtypes"})
            Class<? extends Enum> enumType = (Class<? extends Enum>) f.getType();
            String stringValue = fieldJson.getAsString();
            try {
              @SuppressWarnings("unchecked")
              Enum<?> enumValue = Enum.valueOf(enumType, stringValue);
              f.set(instance, enumValue);
            } catch (IllegalArgumentException e) {
              logger.warning("Ignoring preference (" + fieldKey + ") invalid value (" + stringValue + ")");
            }
          } else if (f.getType() == Boolean.class || f.getType() == Boolean.TYPE) {
            try {
              boolean boolValue = fieldJson.getAsBoolean();
              f.set(instance, boolValue);
            } catch (IllegalArgumentException e) {
              logger.log(Level.WARNING, "Ignoring preference (" + fieldKey + ")", e);
            }
          } else if (f.getType() == Integer.class || f.getType() == Integer.TYPE) {
            try {
              int intValue = fieldJson.getAsInt();
              f.set(instance, intValue);
            } catch (IllegalArgumentException e) {
              logger.log(Level.WARNING, "Ignoring preference (" + fieldKey + ")", e);
            }
          } else if (f.getType() == Long.class || f.getType() == Long.TYPE) {
            try {
              long longValue = fieldJson.getAsLong();
              f.set(instance, longValue);
            } catch (IllegalArgumentException e) {
              logger.log(Level.WARNING, "Ignoring preference (" + fieldKey + ")", e);
            }
          } else if (serializeTypeAsJson(f.getType())) {
            try {
              Object value = gson.fromJson(fieldJson, f.getType());
              f.set(instance, value);
            } catch (JsonParseException e) {
              logger.log(Level.WARNING, "Ignoring preference (" + fieldKey + ")", e);
            }
          } else {
            throw new IllegalStateException("Unsupported preference type: " + f.getType().getSimpleName());
          }
        }
      }
    }
  }
  
  private boolean serializeTypeAsJson(Class<?> type) {
    return type.isAnnotationPresent(SerializeAsJson.class);
  }
}
