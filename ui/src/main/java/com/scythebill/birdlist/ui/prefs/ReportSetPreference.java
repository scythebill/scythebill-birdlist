/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.prefs;

import com.google.common.base.Preconditions;

/** 
 * A wrapper around a single preference stored within a ReportSet.
 */
public class ReportSetPreference<T> {
  private final T value;
  private final ReportPreferencesManager preferencesManager;
  
  ReportSetPreference(
      T value,
      ReportPreferencesManager preferencesManager) {
    this.value = Preconditions.checkNotNull(value);
    this.preferencesManager = Preconditions.checkNotNull(preferencesManager);
  }
  
  private ReportSetPreference(T value) {
    this.value = null;
    this.preferencesManager = null;
  }
  
  public T get() {
    return value;
  }
  
  public void save(boolean markDirty) {
    preferencesManager.save(markDirty);
  }

  /** Returns a static, unattached ReportSet preference. */
  public static <T> ReportSetPreference<T> staticValue(T value) {
    return new ReportSetPreference<T>(value) {
      @Override public void save(boolean markDirty) {}
    };
  }  
}
