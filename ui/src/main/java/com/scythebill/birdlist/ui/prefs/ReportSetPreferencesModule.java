/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.prefs;

import com.google.inject.AbstractModule;
import com.google.inject.Key;
import com.google.inject.Module;
import com.google.inject.Provider;
import com.google.inject.TypeLiteral;
import com.google.inject.util.Types;

/**
 * Installs bindings for a single preference within a ReportSet.
 */
public class ReportSetPreferencesModule<T> extends AbstractModule {
  private final Class<T> clazz;

  private ReportSetPreferencesModule(Class<T> clazz) {
    this.clazz = clazz;
  }

  public static <T> Module forType(Class<T> clazz) {
    return new ReportSetPreferencesModule<T>(clazz);
  }

  @Override
  protected void configure() {
    final Provider<ReportPreferencesManager> managerProvider =
        getProvider(ReportPreferencesManager.class);
    bind(preferenceOf()).toProvider(new Provider<ReportSetPreference<T>>() {
      @Override public ReportSetPreference<T> get() {
        ReportPreferencesManager manager = managerProvider.get();
        T value = manager.getPreference(clazz);
        return new ReportSetPreference<T>(value, manager);
      }
    });

    bind(clazz).toProvider(new Provider<T>() {
      @Override public T get() {
        ReportPreferencesManager manager = managerProvider.get();
        return manager.getPreference(clazz);
      }
    });
  }
  
  Key<ReportSetPreference<T>> preferenceOf() {
    @SuppressWarnings("unchecked")
    TypeLiteral<ReportSetPreference<T>> preferenceType = (TypeLiteral<ReportSetPreference<T>>)
        TypeLiteral.get(Types.newParameterizedType(ReportSetPreference.class, clazz));
    return Key.get(preferenceType);
  }
}
