/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.taxonomy;

import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import org.joda.time.LocalDate;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.io.Files;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.ExtendedTaxonomyChecklists;
import com.scythebill.birdlist.model.export.FullReportExport;
import com.scythebill.birdlist.model.io.PartialIO;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Type;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;
import com.scythebill.birdlist.model.taxa.names.LocalNames;
import com.scythebill.birdlist.model.taxa.names.NamesPreferences;
import com.scythebill.birdlist.model.util.TaxonomyIndexer;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.imports.TaxonPossibilities;
import com.scythebill.birdlist.ui.imports.TaxonResolver;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.FileDialogs;
import com.scythebill.birdlist.ui.util.UIUtils;

/**
 * Adds an extended taxonomy, if possible.
 */
public class ExtendedTaxonomyAdder {
  private final TaxonomyStore taxonomyStore;
  private final ReportSet reportSet;
  private final Alerts alerts;
  private final FileDialogs fileDialogs;
  private final NamesPreferences namesPreferences;

  @Inject
  ExtendedTaxonomyAdder(
      TaxonomyStore taxonomyStore,
      ReportSet reportSet,
      FileDialogs fileDialogs,
      Alerts alerts,
      NamesPreferences namesPreferences) {
    this.taxonomyStore = taxonomyStore;
    this.reportSet = reportSet;
    this.fileDialogs = fileDialogs;
    this.alerts = alerts;
    this.namesPreferences = namesPreferences;
  }
  
  public void addTaxonomy(Component component, TaxonomyWithChecklists taxonomyWithChecklists) {
    addTaxonomy(component, taxonomyWithChecklists.taxonomy, taxonomyWithChecklists.checklists);
  }
  
  private void addTaxonomy(Component component, Taxonomy taxonomy, ExtendedTaxonomyChecklists checklists) {
    // Make sure the taxonomy is indexed.
    new TaxonomyIndexer(Futures.immediateFuture(taxonomy)).load(MoreExecutors.newDirectExecutorService());
    ((TaxonomyImpl) taxonomy).setLocalNames(LocalNames.trival(namesPreferences));
    
    if (reportSet.getExtendedTaxonomy(taxonomy.getId()) != null) {
      upgradeTaxonomies(component, reportSet.getExtendedTaxonomy(taxonomy.getId()), taxonomy, checklists);
    } else {
      reportSet.addExtendedTaxonomy(taxonomy, checklists);
      if (taxonomy.additionalCredits().isEmpty()) {
        alerts.showMessage(
            component,
            Name.ADDED_TAXONOMY_SUCCESSFULLY_TITLE,
            Name.ADDED_TAXONOMY_SUCCESSFULLY_MESSAGE,
            taxonomy.getName(),
            taxonomy.getSpeciesCount());
      } else {
        alerts.showMessage(
            component,
            Name.ADDED_TAXONOMY_SUCCESSFULLY_TITLE,
            Name.ADDED_TAXONOMY_SUCCESSFULLY_WITH_CREDITS_MESSAGE,
            taxonomy.getName(),
            taxonomy.getSpeciesCount(),
            Joiner.on('\n').join(taxonomy.additionalCredits()));
      }
      taxonomyStore.extendedTaxonomiesChanged();
      reportSet.markDirty();      
    }
  }

  private void upgradeTaxonomies(Component component, Taxonomy fromTaxonomy, Taxonomy toTaxonomy,
      ExtendedTaxonomyChecklists checklists) {
    List<Sighting> existingSightings = Lists.newArrayList(reportSet.getSightings(fromTaxonomy));
    if (existingSightings.isEmpty()) {
      // No sightings to upgrade, easy.
      reportSet.removeExtendedTaxonomy(fromTaxonomy);
      reportSet.addExtendedTaxonomy(toTaxonomy, checklists);
    } else {
      TaxonResolver taxonResolver = new TaxonResolver(toTaxonomy);
      // All successfully upgraded sightings
      List<Sighting> upgradedSightings = Lists.newArrayList();
      // All failed upgrades
      List<Sighting> failedUpgrades = Lists.newArrayList();
      
      // For each sighting, try to resolve to the new taxonomy
      for (Sighting sighting : existingSightings) {
        Resolved resolved = sighting.getTaxon().resolve(fromTaxonomy);
        // Non-sp:  just map the one
        if (resolved.getType() == Type.SINGLE) {
          SightingTaxon toTaxon = resolveTaxon(taxonResolver, resolved.getTaxon(), toTaxonomy);
          // Couldn't be mapped, failed sighting
          if (toTaxon == null) {
            failedUpgrades.add(sighting);
          } else {
            Sighting upgradedSighting = sighting.asBuilder()
                .setTaxon(toTaxon)
                .setTaxonomy(toTaxonomy)
                .build();
            
            upgradedSightings.add(upgradedSighting);
          }
        } else {
          // Sp or hybrid, resolve each
          Set<String> taxonIds = Sets.newLinkedHashSet(); 
          for (Taxon taxon : resolved.getTaxa()) {
            SightingTaxon toTaxon = resolveTaxon(taxonResolver, taxon, toTaxonomy);
            if (toTaxon != null) {
              taxonIds.addAll(toTaxon.getIds());
            }
          }
          // Nothing resolved, it's a failed sighting
          if (taxonIds.isEmpty()) {
            failedUpgrades.add(sighting);
          } else {
            // Some successes; create a taxon
            SightingTaxon upgradedTaxon;
            if (taxonIds.size() == 1) {
              String id = taxonIds.iterator().next();
              upgradedTaxon = SightingTaxons.newSightingTaxon(id);
            } else {
              if (resolved.getType() == Type.HYBRID) {
                upgradedTaxon = SightingTaxons.newHybridTaxon(taxonIds);
              } else {
                upgradedTaxon = SightingTaxons.newSpTaxon(taxonIds);
              }
            }
            
            Sighting upgradedSighting = sighting.asBuilder()
                .setTaxon(upgradedTaxon)
                .setTaxonomy(toTaxonomy)
                .build();
            
            upgradedSightings.add(upgradedSighting);
         }
        }
      }
      
      Preconditions.checkState(
          failedUpgrades.size() + upgradedSightings.size() == existingSightings.size(),
          "Not all sightings upgraded");
      if (!failedUpgrades.isEmpty()) {
        int showOkCancel = alerts.showOkCancel(component,
            Name.ADDED_TAXONOMY_SOME_SIGHTINGS_NOT_UPGRADED_TITLE,
            Name.ADDED_TAXONOMY_SOME_SIGHTINGS_NOT_UPGRADED_MESSAGE,
            fromTaxonomy.getName(),
            failedUpgrades.size(),
            upgradedSightings.size());
        if (showOkCancel != JOptionPane.OK_OPTION) {
          // User didn't click OK;  bail.
          return;
        }
        
        String defaultName = String.format(
            "failed-%s-%s.csv",
            fromTaxonomy.getId(),
            PartialIO.toString(new LocalDate()));
        File saveFile = fileDialogs.saveFile(
            UIUtils.findFrame(component),
            Messages.getMessage(Name.SAVE_SIGHTINGS_THAT_FAILED_UPGRADE),
            defaultName,
            new FileFilter() {
              @Override
              public String getDescription() {
                return Messages.getMessage(Name.CSV_FILES);
              }
              
              @Override
              public boolean accept(File f) {
                return f.getName().endsWith(".csv");
              }
            }, FileType.OTHER);
        // No file to export, bail.
        if (saveFile == null) {
          return;
        }
        
        // Write the export.
        try {
          new FullReportExport().exportExplicitSightings(
              Files.asByteSink(saveFile),
              reportSet,
              fromTaxonomy,
              failedUpgrades);
        } catch (IOException e) {
          alerts.showError(
              component,
              e,
              Name.SAVING_FAILED_TITLE,
              Name.SAVE_FAILED_ABORTING_UPGRADE);
          return;
        }
      }
      
      reportSet.mutator().removing(existingSightings).mutate();
      reportSet.removeExtendedTaxonomy(fromTaxonomy);
      reportSet.addExtendedTaxonomy(toTaxonomy, checklists);      
      reportSet.mutator().adding(upgradedSightings).mutate();
    }

    if (taxonomyStore.getTaxonomy() == fromTaxonomy) {
      // If already viewing the original taxonomy, then tell the taxonomy store
      // to switch to the new taxonomy.
      taxonomyStore.setTaxonomy(toTaxonomy);
    } else {
      // Otherwise, give the taxonomy store a ping so that it can clean up the chooser (if necessary)
      taxonomyStore.extendedTaxonomiesChanged();
    }
    
    reportSet.markDirty();

    alerts.showMessage(
        component,
        Name.UPGRADED_TAXONOMY_SUCCESSFULLY_TITLE,
        Name.UPGRADED_TAXONOMY_SUCCESSFULLY_MESSAGE,
        toTaxonomy.getName(),
        toTaxonomy.getSpeciesCount());
  }

  private SightingTaxon resolveTaxon(TaxonResolver taxonResolver, Taxon taxon, Taxonomy toTaxonomy) {
    Taxon species = TaxonUtils.getParentOfType(taxon, Taxon.Type.species);
    String commonName = species.getCommonName();
    String sciName = TaxonUtils.getFullName(species);
    String subspecies = taxon.getType() == Taxon.Type.subspecies ? taxon.getName() : null;
    TaxonPossibilities possibilities = taxonResolver.map(commonName, sciName, subspecies);
    if (possibilities == null) {
      return null;
    }
    
    return possibilities.choose(null, toTaxonomy);
  }
}
