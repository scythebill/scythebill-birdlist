/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.taxonomy;

import java.io.IOException;
import java.io.Writer;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.TreeMultimap;
import com.scythebill.birdlist.model.checklist.ExtendedTaxonomyChecklists;
import com.scythebill.birdlist.model.io.CsvExportLines;
import com.scythebill.birdlist.model.io.ExportLines;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.xml.ExtendedTaxonomyParsing;

/**
 * Writes an extended taxonomy as CSV.
 */
// TODO: Family and Order scientific names?  Taxonomic notes?
public class ExtendedTaxonomyCsvExporter {
  private static final ImmutableList<String> HEADER_LINE = ImmutableList.of(
      "Order",
      "Family",
      "Genus",
      "Species",
      "Subspecies",
      "Common",
      "Alternate Common",
      "Alternate Scientific",
      "Range",
      "Status",
      "Notes",
      "Account ID");
  private static final ImmutableList<String> HEADER_LINE_WITH_CHECKLISTS = ImmutableList.<String>builder()
      .addAll(HEADER_LINE)
      .add("Location Codes")
      .build();
  
  
  private static final Joiner ALTERNATE_NAME_JOINER = Joiner.on('/');
  private static final Joiner LOCATION_CODE_JOINER = Joiner.on(',');
  /** Types which are not exported. */
  private static final ImmutableSet<Taxon.Type> NOT_EXPORTED_TYPES = ImmutableSet.of(
      Taxon.Type.classTaxon,
      Taxon.Type.phylum,
      // TODO: support groups?
      Taxon.Type.group,
      Taxon.Type.genus);
  
  static final String EXTENDED_TAXONOMY_ROOT = "extended-taxonomy";

  public void writeExtendedTaxonomy(
      final Taxonomy extendedTaxonomy,
      ExtendedTaxonomyChecklists checklists,
      Writer out) throws IOException {
    final ExportLines export = CsvExportLines.fromWriter(out);
    export.nextLine(new String[]{"Taxonomy ID", extendedTaxonomy.getId()});
    export.nextLine(new String[]{"Name", extendedTaxonomy.getName()});
    ImmutableList<String> additionalCredits = ImmutableList.copyOf(extendedTaxonomy.additionalCredits());
    if (!additionalCredits.isEmpty()) {
      export.nextLine(new String[]{"Credits", Joiner.on('\n').join(additionalCredits)});
    }
    
    // The account URL format isn't directly exposed, but we can get close enough.
    String accountUrl = extendedTaxonomy.getTaxonAccountUrl("d33db33f");
    if (accountUrl != null) {
      export.nextLine(new String[] {"Account URL Format", accountUrl.replace("d33db33f", "{id}")});
    }
    String accountLinkTitle = extendedTaxonomy.getAccountLinkTitle();
    if (accountLinkTitle != null) {
      export.nextLine(new String[] {"Account Link Title", accountLinkTitle});
    }
    boolean hasChecklists = checklists != null && !checklists.getChecklists().isEmpty();
    
    String[] header = hasChecklists
        ? HEADER_LINE_WITH_CHECKLISTS.toArray(new String[0])
        : HEADER_LINE.toArray(new String[0]);
    export.nextLine(header);
    
    TreeMultimap<String, String> transposedChecklists =
        hasChecklists
            ? ExtendedTaxonomyParsing
                .buildTransposedChecklistWithStatuses(checklists.getChecklists(), extendedTaxonomy)
            : null;
        
    class State implements TaxonVisitor {
      String currentOrder;
      String currentFamily;
      String currentGenus;
      String currentSpecies;
      
      void run() {
        TaxonUtils.visitTaxa(extendedTaxonomy.getRoot(), this);        
      }

      @Override
      public boolean visitTaxon(Taxon taxon) {
        if (NOT_EXPORTED_TYPES.contains(taxon.getType())) {
          return true;
          
        }
        String[] line = new String[header.length];
        if (taxon.getCommonName() != null) {
          line[5] = taxon.getCommonName();
        }

        line[11] = taxon.getAccountId();

        // Output orders on their own line
        if (taxon.getType() == Taxon.Type.order) {
          currentOrder = line[0] = taxon.getName();
        } else if (taxon.getType() == Taxon.Type.family) {
          // And families too
          currentFamily = line[1] = taxon.getName();
          line[5] = taxon.getCommonName();
        } else {
          Preconditions.checkArgument(taxon.getType().compareTo(Taxon.Type.species) <= 0);
        
          Taxon genus = TaxonUtils.getParentOfTypeOrNull(taxon, Taxon.Type.genus);
          if (!genus.getName().equals(currentGenus)) {
            currentGenus = line[2] = genus.getName();
            
            Taxon family = TaxonUtils.getParentOfTypeOrNull(taxon, Taxon.Type.family);
            if (family != null
                && !family.getName().equals(currentFamily)) {
              currentFamily = line[1] = family.getName();
            }
            
            Taxon order = TaxonUtils.getParentOfTypeOrNull(taxon, Taxon.Type.order);
            if (order != null
                && !order.getName().equals(currentOrder)) {
              currentOrder = line[0] = order.getName();
            }
          }
          
          if (taxon.getType() == Taxon.Type.subspecies) {
            Taxon species = TaxonUtils.getParentOfTypeOrNull(taxon, Taxon.Type.species);
            if (species != null
                && !species.getName().equals(currentSpecies)) {
              currentSpecies = line[3] = species.getName();
            }
            line[4] = taxon.getName();
            
          } else if (taxon.getType() == Taxon.Type.species) {
            currentSpecies = line[3] = taxon.getName();
          }
          
          if (taxon instanceof Species) {
            Species species = (Species) taxon;
            if (species.getRange() != null) {
              line[8] = species.getRange();
            }
            
            Status status = species.getStatus();
            if (status != Status.LC) {
              line[9] = status.name();
            }
            
            String notes = species.getTaxonomicInfo();
            if (notes != null) {
              line[10] = notes;
            }
            line[6] = ALTERNATE_NAME_JOINER.join(species.getAlternateCommonNames());
            line[7] = ALTERNATE_NAME_JOINER.join(species.getAlternateNames());

            if (hasChecklists) {
              line[12] = LOCATION_CODE_JOINER.join(transposedChecklists.get(taxon.getId()));
            }
          }          
        }
        
        try {
          export.nextLine(line);
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
        
        return true;
      }
    }
    
    new State().run();
  }
}
