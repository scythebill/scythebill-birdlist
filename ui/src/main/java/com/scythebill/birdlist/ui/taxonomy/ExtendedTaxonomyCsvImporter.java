/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.taxonomy;

import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.Map;

import com.google.common.base.CaseFormat;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.scythebill.birdlist.model.checklist.ExtendedTaxonomyChecklists;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Species.Status;
import com.scythebill.birdlist.model.taxa.SpeciesImpl;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.TaxonImpl;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;

/**
 * Imports an extended taxonomy in CSV format.
 */
public class ExtendedTaxonomyCsvImporter {
  private Integer rangeIndex;
  private Integer commonNameIndex;
  private Integer alternateCommonNameIndex;
  private Integer alternateScientificNameIndex;
  private Integer statusIndex;
  private Integer extinctIndex;
  private Integer notesIndex;
  private Integer locationCodesIndex;
  private Integer accountIdIndex;
  private static final Splitter ALTERNATE_NAME_SPLITTER =
      Splitter.on(CharMatcher.anyOf("/,\n\r"))
          .omitEmptyStrings()
          .trimResults(CharMatcher.whitespace());
  private static final ImmutableMap<String, Status> STATUS_NAMES;
  
  static {
    ImmutableMap.Builder<String, Status> builder = ImmutableMap.builder();
    for (Status status : Status.values()) {
      if (status.name() != null) {
        builder.put(status.name(), status);
      }
    }
    STATUS_NAMES = builder.build();
  }

  public TaxonomyWithChecklists readExtendedTaxonomy(Reader in) throws IOException {
    ImportLines importLines = CsvImportLines.fromReader(in);
    String[] line;
    
    String id = null;
    String name = null;
    String credits = null;
    String accountUrlFormat = null;
    String accountLinkTitle = null;
    
    // First, look for all of the metadata, which comes in two-entry lines
    while (true) {
      line = trimAll(importLines.nextLine());
      if (line == null) {
        throw new EOFException("No records found");
      }

      // Line too short, keep going
      if (line.length < 2) {
        continue;
      }
      
      // Line too long, must have reached the main section
      if (lastNonEmptyEntry(line) > 2) {
        break;
      }
      
      // No point if either entry is empty
      if (Strings.isNullOrEmpty(line[0])
          || Strings.isNullOrEmpty(line[1])) {
        continue;
      }
      
      String key = CharMatcher.javaLetter().retainFrom(line[0].toLowerCase());
      // "id" - the old version.  "taxonomy id" - the new one.  Changed because of
      // https://bitbucket.org/scythebill/scythebill-birdlist/issues/417/dont-use-id-as-the-first-column-header-of
      if ("id".equals(key) || "taxonomyid".equals(key)) {
        id = line[1];
      } else if ("name".equals(key)) {
        name = line[1];
      } else if ("credits".equals(key)) {
        credits = line[1];
      } else if ("accounturlformat".equals(key)) {
        accountUrlFormat = line[1];
        // Make the assumption that they meant to append the ID.
        if (!accountUrlFormat.contains("{id}")) {
          accountUrlFormat = accountUrlFormat + "{id}";
        }
      } else if ("accountlinktitle".equals(key)) {
        accountLinkTitle = line[1];
      }
    }
    
    // ID and name are required
    if (id == null) {
      throw new EOFException("No \"ID\" row was found.");
    }
    if (name == null) {
      throw new EOFException("No \"Name\" row was found.");
    }

    TaxonomyImpl taxonomy = new TaxonomyImpl(id, name, accountUrlFormat, accountLinkTitle) {
      @Override
      public boolean isBuiltIn() {
        return false;
      }      
    };
    if (credits != null) {
      for (String credit : Splitter.on(CharMatcher.anyOf("\n\r")).omitEmptyStrings().split(credits)) {
        taxonomy.addAdditionalCredit(credit);
      }
    }
    
    TaxonImpl root = new TaxonImpl(Type.classTaxon, taxonomy);
    // Class name isn't actually displayed anywhere, but needs to exist
    root.setName(id);
    root.setCommonName(name);
    root.built();
    taxonomy.setRoot(root);

    // "line", after extracting the metadata, will be the main header line 
    Map<String, Integer> indexMap = toIndexMap(line);
    int orderIndex = getRequiredHeader(indexMap, "Order");
    int familyIndex = getRequiredHeader(indexMap, "Family");
    int speciesIndex = getRequiredHeader(indexMap, "Species");
    
    // Genus is optional;  if missing, genus is extracted from the species
    Integer genusIndex = indexMap.get("genus");
    // Subspecies is optional;  if missing, no subspecies are generated
    Integer subspeciesIndex = indexMap.get("subspecies");
    commonNameIndex = getIndex(indexMap, "common", "commonname");
    rangeIndex = getIndex(indexMap, "range", "distribution");
    alternateCommonNameIndex = getIndex(indexMap, "alternate", "alternatecommon");
    alternateScientificNameIndex = getIndex(indexMap, "alternatescientific");
    statusIndex = getIndex(indexMap, "status");
    extinctIndex = getIndex(indexMap, "extinct");
    notesIndex = getIndex(indexMap, "notes");
    locationCodesIndex = getIndex(indexMap, "locationcodes");
    accountIdIndex = getIndex(indexMap, "accountid");
    
    ExtendedTaxonomyChecklists.Builder checklistsBuilder = new ExtendedTaxonomyChecklists.Builder();
    TaxonImpl currentOrder = null;
    TaxonImpl currentFamily = null;
    TaxonImpl currentGenus = null;
    SpeciesImpl currentSpecies = null;
    while ((line = importLines.nextLine()) != null) {
      String order = atIndex(line, orderIndex);
      // If order is missing, assume it's the same as the prior line
      if (!order.isEmpty()) {
        order = toInitialCaps(order);
        
        if (currentOrder == null
            || !currentOrder.getName().equals(order)) {
          currentOrder = (TaxonImpl) root.findByName(order);
          if (currentOrder == null) {
            currentOrder = new TaxonImpl(Type.order, root);
            currentOrder.setName(order);
            root.getContents().add(currentOrder);
            
            // If this line *doesn't* have a species, and *doesn't* have a family, and there is a common name column,
            // set the common name of the order
            if (atIndex(line, speciesIndex).isEmpty()
                && atIndex(line, familyIndex).isEmpty()
                && commonNameIndex != null
                && !atIndex(line, commonNameIndex).isEmpty()) {
              currentOrder.setCommonName(atIndex(line, commonNameIndex));
              if (accountIdIndex != null) {
                currentOrder.setAccountId(Strings.emptyToNull(atIndex(line, accountIdIndex)));
              }
            }
            
            currentFamily = null;
            currentOrder.built();
          }
        }
      }
      
      String family = atIndex(line, familyIndex);
      if (!family.isEmpty()) {
        family = toInitialCaps(family);
        
        if (currentFamily == null
            || !currentFamily.getName().equals(family)) {
          if (currentOrder == null) {
            throw new IOException(
                String.format("Family \"%s\" found without any Order (line %,d)",
                    family,
                    importLines.lineNumber()));
          }
          currentFamily = (TaxonImpl) currentOrder.findByName(family);
          if (currentFamily == null) {
            currentFamily = new TaxonImpl(Type.family, currentOrder);
            currentFamily.setName(family);
            currentOrder.getContents().add(currentFamily);
            currentGenus = null;
  
            // If this line *doesn't* have a species, and there is a common name column,
            // set the common name of the family
            if (atIndex(line, speciesIndex).isEmpty()
                && commonNameIndex != null
                && !atIndex(line, commonNameIndex).isEmpty()) {
              currentFamily.setCommonName(atIndex(line, commonNameIndex));
              if (accountIdIndex != null) {
                currentFamily.setAccountId(Strings.emptyToNull(atIndex(line, accountIdIndex)));
              }
            }
            currentFamily.built();
          }
        }
      }

      String genus = genusIndex == null
          ? extractGenus(atIndex(line, speciesIndex), importLines) : atIndex(line, genusIndex);
      if (!genus.isEmpty()) {
        genus = toInitialCaps(genus);
        if (currentGenus == null
            || !currentGenus.getName().equals(genus)) {
          if (currentFamily == null) {
            throw new IOException(
                String.format("Genus \"%s\" found without any Family (line %,d)",
                    genus,
                    importLines.lineNumber()));
          }
          
          currentGenus = (TaxonImpl) currentFamily.findByName(genus);
          if (currentGenus == null) {
            currentGenus = new TaxonImpl(Type.genus, currentFamily);
            currentGenus.setName(genus);
            currentFamily.getContents().add(currentGenus);
            currentSpecies = null;
            currentGenus.built();
          }
        }
      }

      String species = genusIndex == null
          ? extractSpecies(atIndex(line, speciesIndex)) : atIndex(line, speciesIndex);
      if (!species.isEmpty()) {
        species = species.toLowerCase();
        if (currentSpecies == null
            || !currentSpecies.getName().equals(species)) {
          if (currentGenus == null) {
            throw new IOException(
                String.format("Species \"%s\" found without any Genus (line %,d)",
                    species,
                    importLines.lineNumber()));
          }
          checkNotAlreadyPresent(importLines, currentGenus, species, "A Species");
          currentSpecies = new SpeciesImpl(Type.species, currentGenus);
          currentSpecies.setName(species);
          // If there's a subspecies on this line, then the "additional fields"
          // apply to the subspecies, *not* the species.
          boolean hasSubspeciesInThisLine = subspeciesIndex != null
              && !atIndex(line, subspeciesIndex).isEmpty();
          if (!hasSubspeciesInThisLine) {
            setAdditionalFields(currentSpecies, line);
          }
          currentGenus.getContents().add(currentSpecies);
          currentSpecies.built();
          
          if (locationCodesIndex != null) {
            String locationCodes = atIndex(line, locationCodesIndex);
            if (!locationCodes.isEmpty()) {
              checklistsBuilder.addEntries(
                  SightingTaxons.newSightingTaxon(currentSpecies.getId()),
                  currentSpecies.getStatus(),
                  Splitter.on(',').splitToList(locationCodes));
            }
          }
        }
      }
       
      if (subspeciesIndex != null) {
        String subspecies = atIndex(line, subspeciesIndex);
        if (!subspecies.isEmpty()) {
          // Strip off the species from any trinomial
          if (subspecies.indexOf(' ') >= 0) {
            String speciesNamePlusSpace = TaxonUtils.getFullName(currentSpecies) + " ";
            if (subspecies.startsWith(speciesNamePlusSpace)) {
              subspecies = subspecies.substring(speciesNamePlusSpace.length());
            }
          }
          
          if (currentSpecies == null) {
            throw new IOException(
                String.format("Subspecies \"%s\" found without any Species (line %,d)",
                    subspecies,
                    importLines.lineNumber()));
          }
          checkNotAlreadyPresent(importLines, currentSpecies, subspecies, "A Subspecies");
          SpeciesImpl currentSubspecies = new SpeciesImpl(Type.subspecies, currentSpecies);
          currentSubspecies.setName(subspecies);
          setAdditionalFields(currentSubspecies, line);
          currentSpecies.getContents().add(currentSubspecies);
          currentSubspecies.built();
        }
      }
    }
    
    return new TaxonomyWithChecklists(taxonomy, checklistsBuilder.build());
  }

  private String atIndex(String[] line, int index) {
    if (line.length <= index) {
      return "";
    }
    return line[index];
  }

  private String[] trimAll(String[] nextLine) {
    for (int i = 0; i < nextLine.length; i++) {
      nextLine[i] = CharMatcher.whitespace().trimFrom(nextLine[i]);
    }
    return nextLine;
  }

  private int lastNonEmptyEntry(String[] line) {
    int lastNonEmptyEntry = -1;
    for (int i = 0; i < line.length; i++) {
      if (!Strings.isNullOrEmpty(line[i])) {
        lastNonEmptyEntry = i;
      }
    }
    return lastNonEmptyEntry;
  }

  private int getRequiredHeader(Map<String, Integer> indexMap, String key) throws IOException {
    Integer index = indexMap.get(key.toLowerCase());
    if (index == null) {
      throw new IOException("No \"" + key + "\" header was found.");
    }
    
    return index;
  }
  
  private void checkNotAlreadyPresent(
      ImportLines importLines,
      Taxon parent,
      String childName,
      String childType) throws IOException {
    if (parent.findByName(childName) != null) {
      throw new IOException(
          String.format("%s named \"%s\" was already added to %s (line %s)",
              childType,
              childName,
              parent.getName(),
              importLines.lineNumber()));
    }
  }

  private String toInitialCaps(String name) {
    boolean wrappedInQuotes = false;
    if (name.startsWith("\"")
        && name.endsWith("\"")
        && name.length() >= 2) {
      wrappedInQuotes = true;
      name = name.substring(1, name.length() - 1);
    }
    name = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, name);
    if (wrappedInQuotes) {
      name = "\"" + name + "\"";
    }
    
    return name;
  }

  private String extractGenus(String genusAndSpecies, ImportLines importLines) throws IOException {
    if (genusAndSpecies.isEmpty()) {
      return "";
    }
    
    int space = genusAndSpecies.indexOf(' ');
    if (space < 0) {
      throw new IOException(
          String.format("Expected a genus and a species, but only found \"%s\" (line %,d)",
              genusAndSpecies,
              importLines.lineNumber()));
    }
    
    return genusAndSpecies.substring(0, space);
  }
  
  private String extractSpecies(String genusAndSpecies) {
    int space = genusAndSpecies.indexOf(' ');
    if (space < 0) {
      return "";
    }
    
    return genusAndSpecies.substring(space + 1);
  }

  private Integer getIndex(Map<String, Integer> indexMap, String... possibleNames) {
    for (String possibleName : possibleNames) {
      Integer index = indexMap.get(possibleName);
      if (index != null) {
        return index;
      }
    }
    return null;
  }

  private void setAdditionalFields(SpeciesImpl species, String[] line) {
    if (commonNameIndex != null) {
      String commonName = atIndex(line, commonNameIndex);
      if (!commonName.isEmpty()) {
        if (alternateCommonNameIndex != null) {
          species.setCommonName(commonName);
        } else {
          // If common names contains multiple entries (
          List<String> commonNames = ALTERNATE_NAME_SPLITTER.splitToList(commonName);
          if (!commonNames.isEmpty()) {
            species.setCommonName(commonNames.get(0));
            if (commonNames.size() > 1) {
              species.setAlternateCommonNames(commonNames.subList(1, commonNames.size()));
            }
          }
        }
      }
    }
    
    if (accountIdIndex != null) {
      species.setAccountId(Strings.emptyToNull(atIndex(line, accountIdIndex)));
    }

    if (rangeIndex != null) {
      String range = atIndex(line, rangeIndex);
      if (!range.isEmpty()) {
        species.setRange(range);
      }
    }
    
    if (alternateCommonNameIndex != null) {
      String alternateCommonNames = atIndex(line, alternateCommonNameIndex);
      if (!alternateCommonNames.isEmpty()) {
        species.setAlternateCommonNames(ALTERNATE_NAME_SPLITTER.splitToList(alternateCommonNames));
      }
    }

    if (alternateScientificNameIndex != null) {
      String alternateSciNames = atIndex(line, alternateScientificNameIndex);
      if (!alternateSciNames.isEmpty()) {
        species.setAlternateNames(ALTERNATE_NAME_SPLITTER.splitToList(alternateSciNames));
      }
    }
    
    if (statusIndex != null) {
      String status = atIndex(line, statusIndex).toUpperCase();
      if (!status.isEmpty()) {
        Status statusEnum = STATUS_NAMES.get(status);
        if (statusEnum != null) {
          species.setStatus(statusEnum);
        }
      }
    }
    
    if (extinctIndex != null) {
      String extinct = atIndex(line, extinctIndex).toUpperCase();
      if (!extinct.isEmpty() && !extinct.equals("0") && !extinct.equals("N")) {
        species.setStatus(Status.EX);
      }
    }
    
    if (notesIndex != null) {
      String notes = atIndex(line, notesIndex);
      if (!notes.isEmpty()) {
        species.setTaxonomicInfo(notes);
      }
    }
  }

  private Map<String, Integer> toIndexMap(String[] header) {
    Map<String, Integer> indexMap = Maps.newHashMapWithExpectedSize(header.length);
    for (int i = 0; i < header.length; i++) {
      String name = CharMatcher.javaLetter().retainFrom(header[i]).toLowerCase();
      indexMap.put(name, i);
    }
    return indexMap;
  }
}
