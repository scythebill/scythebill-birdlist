/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.taxonomy;

import java.io.IOException;
import java.io.Writer;

import com.scythebill.birdlist.model.io.IndentingResponseWriter;
import com.scythebill.birdlist.model.io.ResponseWriter;
import com.scythebill.birdlist.model.io.XmlResponseWriter;
import com.scythebill.birdlist.model.xml.XmlTaxonExport;

/**
 * Writes an extended taxonomy as XML.
 */
public class ExtendedTaxonomyXmlExporter {
  static final String EXTENDED_TAXONOMY_ROOT = "extended-taxonomy";
  // close() responsibility is on the caller;  this class flushes its buffers.
  @SuppressWarnings("resource")
  public void writeExtendedTaxonomy(
      TaxonomyWithChecklists extendedTaxonomy,
      Writer out) throws IOException {
    ResponseWriter rw = new XmlResponseWriter(out, "UTF-8");
    rw = new IndentingResponseWriter(rw);
    rw.startDocument();
    // Use an additional wrapper element for future extensibility (e.g. including checklists)
    rw.startElement(EXTENDED_TAXONOMY_ROOT);
    rw.writeAttribute("xmlns", XmlTaxonExport.NAMESPACE);
    rw.writeAttribute("version", "1.0");
    new XmlTaxonExport()
        .withChecklists(extendedTaxonomy.checklists)
        .writeTaxonomy(extendedTaxonomy.taxonomy, rw);
    rw.endElement(EXTENDED_TAXONOMY_ROOT);
    rw.endDocument();
    rw.flush();
    
  }
}
