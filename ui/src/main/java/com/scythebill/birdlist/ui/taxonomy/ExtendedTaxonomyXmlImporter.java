/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.taxonomy;

import java.io.IOException;
import java.io.Reader;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.scythebill.birdlist.model.checklist.ExtendedTaxonomyChecklists;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.xml.ExtendedTaxonomyNodeParser;
import com.scythebill.xml.BaseNodeParser;
import com.scythebill.xml.NodeParser;
import com.scythebill.xml.ParseContext;
import com.scythebill.xml.TreeBuilder;

/**
 * Reads an extended taxonomy as XML.
 */
public class ExtendedTaxonomyXmlImporter {
  public static final String FILE_SUFFIX = ".btxm";

  public TaxonomyWithChecklists readExtendedTaxonomy(
      Reader in) throws IOException, SAXException {
    TreeBuilder<TaxonomyWithChecklists> builder = new TreeBuilder<TaxonomyWithChecklists>(null,
        TaxonomyWithChecklists.class);
    return builder.parse(new InputSource(in), new RootNodeParser());    
  }
  
  static class RootNodeParser extends BaseNodeParser {
    private Taxonomy taxonomy;
    private ExtendedTaxonomyChecklists checklists;

    @Override public void addCompletedChild(ParseContext context,
        String namespaceURI, String localName, Object child)
        throws SAXParseException {
      if (child instanceof Taxonomy) {
        this.taxonomy = (Taxonomy) child;
        this.checklists = ExtendedTaxonomyNodeParser.endChecklistParsing(context);
      }
    }

    @Override
    public NodeParser startChildElement(ParseContext context,
        String namespaceURI, String localName, Attributes attrs)
        throws SAXParseException {
      if (ExtendedTaxonomyNodeParser.ELEMENT_TAXONOMY.equals(localName)) {
        // Only one taxonomy per .btxm file
        if (taxonomy != null) {
          throw new SAXParseException(
              "Multiple extended taxonomies in a single .btxm file", context.getLocator());
        }
        ExtendedTaxonomyNodeParser.startChecklistParsing(context);
        return new ExtendedTaxonomyNodeParser();
      }
      
      return null;
    }

    @Override
    public void startElement(ParseContext context, String namespaceURI,
        String localName, Attributes attrs) throws SAXParseException {
      if (!ExtendedTaxonomyCsvExporter.EXTENDED_TAXONOMY_ROOT.equals(localName)) {
        throw new SAXParseException("Unexpected root element: " + localName, context.getLocator());
      }
    }
    
    @Override
    public Object endElement(ParseContext context, String namespaceURI,
        String localName) throws SAXParseException {
      return new TaxonomyWithChecklists(taxonomy, checklists);
    }
    
  }
}
