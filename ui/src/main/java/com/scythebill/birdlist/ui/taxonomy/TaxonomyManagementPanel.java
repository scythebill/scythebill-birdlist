/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.taxonomy;

import static javax.swing.GroupLayout.PREFERRED_SIZE;

import java.awt.Dimension;
import java.awt.Frame;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URI;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;

import org.xml.sax.SAXException;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.eventbus.Subscribe;
import com.google.common.io.Files;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.checklist.ExtendedTaxonomyChecklists;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.actions.ReturnAction;
import com.scythebill.birdlist.ui.app.Titled;
import com.scythebill.birdlist.ui.events.EventBusRegistrar;
import com.scythebill.birdlist.ui.events.TaxonomyChangedEvent;
import com.scythebill.birdlist.ui.events.TaxonomyStore;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.fonts.FontManager.FontsUpdatedListener;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;
import com.scythebill.birdlist.ui.panels.ShrinkToFit;
import com.scythebill.birdlist.ui.util.Alerts;
import com.scythebill.birdlist.ui.util.DesktopUtils;
import com.scythebill.birdlist.ui.util.FileDialogs;

/**
 * Class for managing extended taxonomies.
 */
public class TaxonomyManagementPanel extends JPanel implements FontsUpdatedListener, ShrinkToFit, Titled {
  private final ReportSet reportSet;
  private final TaxonomyStore taxonomyStore;
  private final ExtendedTaxonomyAdder extendedTaxonomyAdder;
  private final FontManager fontManager;
  private final FileDialogs fileDialogs;
  private final Alerts alerts;
  private JPanel centerBox;
  private JButton addTaxonomy;
  private JButton removeTaxonomy;
  private JButton exportTaxonomy;
  private JButton exportTaxonomyAsCsv;
  private JButton downloadTaxonomies;
  private JButton returnButton;
  private JLabel addTaxonomyLabel;
  private JLabel exportTaxonomyLabel;
  private JLabel exportTaxonomyAsCsvLabel;
  private JLabel removeTaxonomyLabel;
  private JLabel downloadTaxonomiesLabel;
  private JLabel emptyLabel;


  @Inject
  public TaxonomyManagementPanel(
      ReturnAction returnAction,
      ReportSet reportSet,
      TaxonomyStore taxonomyStore,
      ExtendedTaxonomyAdder extendedTaxonomyAdder,
      FileDialogs fileDialogs,
      Alerts alerts,
      FontManager fontManager,
      EventBusRegistrar eventBusRegistrar) {
    this.reportSet = reportSet;
    this.extendedTaxonomyAdder = extendedTaxonomyAdder;
    this.fileDialogs = fileDialogs;
    this.taxonomyStore = taxonomyStore;
    this.alerts = alerts;
    this.fontManager = fontManager;
    initGUI();
    
    exportTaxonomy.addActionListener(e ->
       exportTaxonomyAsXml(augmentWithChecklists(TaxonomyManagementPanel.this.taxonomyStore.getTaxonomy())));
    exportTaxonomyAsCsv.addActionListener(e ->
      exportTaxonomyAsCsv(augmentWithChecklists(TaxonomyManagementPanel.this.taxonomyStore.getTaxonomy())));
    addTaxonomy.addActionListener(e -> addTaxonomy());
    removeTaxonomy.addActionListener(e ->
      removeTaxonomy(TaxonomyManagementPanel.this.taxonomyStore.getTaxonomy()));
    downloadTaxonomies.addActionListener(event -> {
      try {
        DesktopUtils.openUrlInBrowser(
            URI.create("https://www.scythebill.com/download.html#extended"),
            alerts);
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    });
    
    returnButton.setAction(returnAction);

    eventBusRegistrar.registerWhenInHierarchy(this);
    updateUiForTaxonomy();    
  }

  private TaxonomyWithChecklists augmentWithChecklists(Taxonomy taxonomy) {
    ExtendedTaxonomyChecklists checklists = reportSet.getExtendedTaxonomyChecklist(taxonomy.getId());
    return new TaxonomyWithChecklists(taxonomy, checklists);
  }

  private void addTaxonomy() {
    File openFile = fileDialogs.openFile(
        (Frame) SwingUtilities.getWindowAncestor(this),
        Messages.getMessage(Name.OPEN_A_TAXONOMY), new FileFilter() {
          @Override
          public String getDescription() {
            return Messages.getMessage(Name.BTXM_OR_CSV_FILES);
          }
          
          @Override
          public boolean accept(File f) {
            return f.getName().endsWith(".csv") || f.getName().endsWith(ExtendedTaxonomyXmlImporter.FILE_SUFFIX);
          }
        }, FileType.OTHER);
    if (openFile == null) {
      return;
    }
    
    try {
      Reader in = Files.newReader(openFile, Charsets.UTF_8);
      try {
        TaxonomyWithChecklists taxonomy;
        if (openFile.getName().endsWith(ExtendedTaxonomyXmlImporter.FILE_SUFFIX)) {
          taxonomy = new ExtendedTaxonomyXmlImporter().readExtendedTaxonomy(in);
        } else {
          taxonomy = new ExtendedTaxonomyCsvImporter().readExtendedTaxonomy(in);
        }
        extendedTaxonomyAdder.addTaxonomy(this, taxonomy);
      } finally {
        in.close();
      }
    } catch (IOException | SAXException e) {
      alerts.showError(this,
          e,
          Name.IMPORT_FAILED,
          Name.IMPORTING_TAXONOMY_FAILED_FORMAT);
    }
  }

  /**
   * Write an extended taxonomy as an XML file.
   */
  private void exportTaxonomyAsXml(TaxonomyWithChecklists extendedTaxonomy) {
    String fileName = extendedTaxonomy.taxonomy.getId() + ExtendedTaxonomyXmlImporter.FILE_SUFFIX;
    
    try {
      File saveFile = fileDialogs.saveFile(
          (Frame) SwingUtilities.getWindowAncestor(this),
          Messages.getMessage(Name.SAVE_TAXONOMY),
          fileName,
          null, FileType.OTHER);
      if (saveFile != null) {
        saveFile.createNewFile();
        Writer out = new OutputStreamWriter(
            new BufferedOutputStream(new FileOutputStream(saveFile)), Charsets.UTF_8);
        try {
          new ExtendedTaxonomyXmlExporter().writeExtendedTaxonomy(extendedTaxonomy, out);
        } finally {
          out.close();
        }
      }
    } catch (IOException e) {
      alerts.showError(this, Name.EXPORT_FAILED_TITLE, Name.EXPORT_FAILED_MESSAGE, e.getMessage());
    }
  }

  /**
   * Write an extended taxonomy as a CSV file.
   */
  private void exportTaxonomyAsCsv(TaxonomyWithChecklists extendedTaxonomy) {
    String fileName = extendedTaxonomy.taxonomy.getId() + ".csv";
    
    try {
      File saveFile = fileDialogs.saveFile(
          (Frame) SwingUtilities.getWindowAncestor(this),
          Messages.getMessage(Name.SAVE_TAXONOMY_AS_CSV),
          fileName,
          null, FileType.OTHER);
      if (saveFile != null) {
        saveFile.createNewFile();
        Writer out = new OutputStreamWriter(
            new BufferedOutputStream(new FileOutputStream(saveFile)), Charsets.UTF_8);
        try {
          new ExtendedTaxonomyCsvExporter().writeExtendedTaxonomy(extendedTaxonomy.taxonomy,
              extendedTaxonomy.checklists, out);
        } finally {
          out.close();
        }
      }
    } catch (IOException e) {
      alerts.showError(this, Name.EXPORT_FAILED_TITLE, Name.EXPORT_FAILED_MESSAGE, e.getMessage());
    }
  }

  private void initGUI() {
    setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

    centerBox = new JPanel();
    add(centerBox);
    
    addTaxonomy = new JButton();
    addTaxonomy.setText(Messages.getMessage(Name.ADD_OR_UPGRADE_A_TAXONOMY));
    addTaxonomy.putClientProperty("Quaqua.Button.style", "bevel");
    
    exportTaxonomy = new JButton();
    exportTaxonomy.setText(Messages.getMessage(Name.EXPORT_THIS_TAXONOMY));
    exportTaxonomy.putClientProperty("Quaqua.Button.style", "bevel");
    
    exportTaxonomyAsCsv = new JButton();
    exportTaxonomyAsCsv.setText(Messages.getMessage(Name.EXPORT_THIS_TAXONOMY_AS_CSV));
    exportTaxonomyAsCsv.putClientProperty("Quaqua.Button.style", "bevel");
    
    removeTaxonomy = new JButton();
    removeTaxonomy.setText(Messages.getMessage(Name.DELETE_THIS_TAXONOMY));
    removeTaxonomy.putClientProperty("Quaqua.Button.style", "bevel");
    
    downloadTaxonomies = new JButton();
    downloadTaxonomies.setText(Messages.getMessage(Name.DOWNLOAD_TAXONOMIES));
    downloadTaxonomies.putClientProperty("Quaqua.Button.style", "bevel");    

    returnButton = new JButton();
    returnButton.putClientProperty("Quaqua.Button.style", "bevel");

    addTaxonomyLabel = new JLabel(Messages.getMessage(Name.ADD_OR_UPGRADE_A_TAXONOMY_EXPLANATION));
    addTaxonomyLabel.putClientProperty(FontManager.PLAIN_LABEL, true);
    
    exportTaxonomyLabel = new JLabel(Messages.getMessage(Name.EXPORT_THIS_TAXONOMY_EXPLANATION));
    exportTaxonomyLabel.putClientProperty(FontManager.PLAIN_LABEL, true);

    exportTaxonomyAsCsvLabel = new JLabel(Messages.getMessage(Name.EXPORT_THIS_TAXONOMY_AS_CSV_EXPLANATION));
    exportTaxonomyAsCsvLabel.putClientProperty(FontManager.PLAIN_LABEL, true);

    removeTaxonomyLabel = new JLabel(Messages.getMessage(Name.DELETE_THIS_TAXONOMY_EXPLANATION));
    removeTaxonomyLabel.putClientProperty(FontManager.PLAIN_LABEL, true);

    downloadTaxonomiesLabel = new JLabel(Messages.getMessage(Name.DOWNLOAD_TAXONOMIES_EXPLANATION));
    downloadTaxonomiesLabel.putClientProperty(FontManager.PLAIN_LABEL, true);

    emptyLabel = new JLabel();

    fontManager.applyTo(this);
  }
  
  @Override
  public void fontsUpdated(FontManager fontManager) {
    centerBox.setPreferredSize(fontManager.scale(new Dimension(700, 600)));
    centerBox.setMinimumSize(centerBox.getPreferredSize());
    centerBox.setBorder(new EmptyBorder(
        fontManager.scale(20),
        fontManager.scale(60),
        fontManager.scale(20),
        fontManager.scale(60)));

    GroupLayout layout = new GroupLayout(centerBox);
    centerBox.setLayout(layout);
    layout.setHorizontalGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(layout.createParallelGroup()
          .addGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup()
                .addComponent(addTaxonomy, Alignment.LEADING,
                    PREFERRED_SIZE, fontManager.scale(220), PREFERRED_SIZE)
                .addComponent(exportTaxonomy, Alignment.LEADING,
                    PREFERRED_SIZE, fontManager.scale(220), PREFERRED_SIZE)
                .addComponent(exportTaxonomyAsCsv, Alignment.LEADING,
                    PREFERRED_SIZE, fontManager.scale(220), PREFERRED_SIZE)
                .addComponent(removeTaxonomy, Alignment.LEADING,
                    PREFERRED_SIZE, fontManager.scale(220), PREFERRED_SIZE)
                .addComponent(downloadTaxonomies, Alignment.LEADING,
                    PREFERRED_SIZE, fontManager.scale(220), PREFERRED_SIZE)
                .addComponent(returnButton, Alignment.LEADING,
                    PREFERRED_SIZE, fontManager.scale(220), PREFERRED_SIZE))
            .addGap(63)
            .addGroup(layout.createParallelGroup()
                .addComponent(addTaxonomyLabel)
                .addComponent(exportTaxonomyLabel)
                .addComponent(exportTaxonomyAsCsvLabel)
                .addComponent(removeTaxonomyLabel)
                .addComponent(downloadTaxonomiesLabel)
                .addComponent(emptyLabel))))
        .addContainerGap(67, 67));
      layout.setVerticalGroup(layout.createSequentialGroup()
        .addGap(reportSet.getSightings().isEmpty() ? 0 : fontManager.scale(24))
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(addTaxonomy, Alignment.BASELINE,
                PREFERRED_SIZE, fontManager.scale(58), PREFERRED_SIZE)
            .addComponent(addTaxonomyLabel, Alignment.BASELINE, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
        .addGap(14)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(exportTaxonomy, Alignment.BASELINE,
                PREFERRED_SIZE, fontManager.scale(58), PREFERRED_SIZE)
            .addComponent(exportTaxonomyLabel, Alignment.BASELINE, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
        .addGap(14)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(exportTaxonomyAsCsv, Alignment.BASELINE,
                PREFERRED_SIZE, fontManager.scale(58), PREFERRED_SIZE)
            .addComponent(exportTaxonomyAsCsvLabel, Alignment.BASELINE, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
        .addGap(14)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(removeTaxonomy, Alignment.BASELINE,
                PREFERRED_SIZE, fontManager.scale(58), PREFERRED_SIZE)
            .addComponent(removeTaxonomyLabel, Alignment.BASELINE, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
        .addGap(14)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(downloadTaxonomies, Alignment.BASELINE,
                PREFERRED_SIZE, fontManager.scale(58), PREFERRED_SIZE)
            .addComponent(downloadTaxonomiesLabel, Alignment.BASELINE, PREFERRED_SIZE, PREFERRED_SIZE, PREFERRED_SIZE))
        .addGap(14)
        .addGroup(layout.createBaselineGroup(false, false)
            .addComponent(returnButton, Alignment.BASELINE,
                PREFERRED_SIZE, fontManager.scale(58), PREFERRED_SIZE)
            .addComponent(emptyLabel))
        .addContainerGap(82, 82));
    layout.linkSize(addTaxonomy, exportTaxonomy, exportTaxonomyAsCsv, removeTaxonomy,
        downloadTaxonomies, returnButton);
  }

  @Subscribe
  public void taxonomyChanged(TaxonomyChangedEvent event) {
    updateUiForTaxonomy();
  }

  private void updateUiForTaxonomy() {
    boolean isBirds = taxonomyStore.isBirdTaxonomy();
    exportTaxonomy.setEnabled(!isBirds);
    exportTaxonomyLabel.setEnabled(!isBirds);
    exportTaxonomyAsCsv.setEnabled(!isBirds);
    exportTaxonomyAsCsvLabel.setEnabled(!isBirds);
    removeTaxonomy.setEnabled(!isBirds);
    removeTaxonomyLabel.setEnabled(!isBirds);
  }

  private void removeTaxonomy(Taxonomy taxonomy) {
    int okOrCancel = alerts.showOkCancel(this,
        Name.REMOVE_TAXONOMY_TITLE,
        Name.REMOVE_TAXONOMY_MESSAGE,
        taxonomy.getName());
    if (okOrCancel == JOptionPane.OK_OPTION) {
      List<Sighting> sightingsToDelete = Lists.newArrayList();
      for (Sighting sighting : reportSet.getSightings()) {
        if (sighting.getTaxonomy() == taxonomy) {
          sightingsToDelete.add(sighting);
        }
      }
      // Remove all the sightings
      reportSet.mutator().removing(sightingsToDelete).mutate();
      // ... then remove the extended taxonomy from the report set
      reportSet.removeExtendedTaxonomy(taxonomy);
      // ... and let the taxonomy store know it doesn't exist either
      taxonomyStore.extendedTaxonomyRemoved();
    }
  }

  @Override
  public String getTitle() {
    return Messages.getMessage(Name.MANAGE_TAXONOMIES);
  }
}