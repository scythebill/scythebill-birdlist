/*   
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.taxonomy;

import com.scythebill.birdlist.model.checklist.ExtendedTaxonomyChecklists;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Simple data class to encapsulate parsing a Taxonomy with associated checklists.
 */
public class TaxonomyWithChecklists {
  public final Taxonomy taxonomy;
  public final ExtendedTaxonomyChecklists checklists;

  public TaxonomyWithChecklists(Taxonomy taxonomy, ExtendedTaxonomyChecklists extendedTaxonomyChecklists) {
    this.taxonomy = taxonomy;
    this.checklists = extendedTaxonomyChecklists;
  }
}
