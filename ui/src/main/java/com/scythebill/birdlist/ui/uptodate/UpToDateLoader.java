/*   
 * Copyright 2012 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.uptodate;

import java.io.BufferedReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import org.joda.time.Duration;
import org.joda.time.Instant;
import org.joda.time.ReadableDuration;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.MoreObjects;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Streams;
import com.google.common.io.CharSource;
import com.google.common.io.Resources;
import com.google.common.primitives.Ints;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListenableScheduledFuture;
import com.google.common.util.concurrent.ListeningScheduledExecutorService;
import com.google.common.util.concurrent.SettableFuture;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;
import com.scythebill.birdlist.ui.guice.CurrentVersion;
import com.scythebill.birdlist.ui.prefs.PreferencesManager;

/**
 * Loads whether the application is up-to-date, asynchronously.
 */
public class UpToDateLoader {
  /**
   * Describes a single version of the app.
   */
  static public class VersionInfo {
    public String version;
    public List<String> changes;
    public URI url;
    
    @Override public String toString() {
      return MoreObjects.toStringHelper(this)
          .add("version", version)
          .add("changes", changes)
          .add("url", url)
          .toString();
    }
  }
 
  private static final String VERSION_INFO_URL = "http://updates.scythebill.com/version-info.json";
  private static final int TIMEOUT_IN_SECONDS = 60;
  /** Check (and therefore nag) at most once every 5 days. */
  private static final ReadableDuration TIME_BETWEEN_CHECKS = Duration.standardDays(5);
  private final VersionNumber currentVersion;
  private final PreferencesManager preferencesManager;
  private final ListeningScheduledExecutorService executorService;

  @Inject
  UpToDateLoader(
      @Nullable @CurrentVersion String currentVersion,
      PreferencesManager preferencesManager,
      ListeningScheduledExecutorService executorService) {
    this.currentVersion = new VersionNumber(versionMinusSnapshot(currentVersion));
    this.preferencesManager = preferencesManager;
    this.executorService = executorService;
  }
  
  /**
   * Returns a Future that will either:
   * <ul>
   * <li>Contain null if the app is up-to-date 
   * <li>Contain a VersionInfo describing the newest version
   * <li>Return an error if version info could not be loaded sufficiently quickly
   * </ul>
   */
  public ListenableFuture<ImmutableList<VersionInfo>> load() {
    final UpToDatePreferences upToDatePreferences =
        preferencesManager.getPreference(UpToDatePreferences.class);
    // When did we last read?
    Instant lastRead = new Instant(upToDatePreferences.lastRead);
    Instant timeToCheckAgain = lastRead.plus(TIME_BETWEEN_CHECKS);
    // It's not time to check again;  don't bother loading, and immediately return "null"
    if (timeToCheckAgain.isAfterNow()) {
      return Futures.immediateFuture(ImmutableList.<VersionInfo>of());
    }
    
    // Future performing the load, which might take ages
    final ListenableFuture<ImmutableList<VersionInfo>> loader = executorService.submit(new Callable<ImmutableList<VersionInfo>>() {
      @Override public ImmutableList<VersionInfo> call() throws Exception {
        String queryParams = Joiner.on("&").withKeyValueSeparator("=").join(ImmutableMap.of(
            "v", URLEncoder.encode(currentVersion.toString(), "UTF-8"),
            "os", URLEncoder.encode(System.getProperty("os.name"), "UTF-8"),
            "f", getFeaturesString(upToDatePreferences),
            "l", Locale.getDefault().toLanguageTag()));
        // TODO: use an I/O call that doesn't block a thread
        CharSource source = Resources.asCharSource(
            new URL(VERSION_INFO_URL + "?" + queryParams), Charsets.UTF_8);
        Reader input = new BufferedReader(source.openStream());
        try {
          List<VersionInfo> infoList = deserialize(input);
          
          ImmutableList.Builder<VersionInfo> versionInfoSinceCurrent = ImmutableList.builder();
          for (VersionInfo info : infoList) {
            VersionNumber oldVersion = new VersionNumber(info.version);
            // As soon as the "old version" is less than or equal to the current one, then break
            if (oldVersion.compareTo(currentVersion) <= 0) {
              break;
            }
            versionInfoSinceCurrent.add(info);
          }
          return versionInfoSinceCurrent.build();
        } finally {
          input.close();
        }
      }
    });

    // The future we'll actually return - which might get cancelled if the
    // loading takes too long, and is also responsible for saving "last loaded time"
    // on successful reads.
    final SettableFuture<ImmutableList<VersionInfo>> results = SettableFuture.create();
    Futures.addCallback(loader, new FutureCallback<ImmutableList<VersionInfo>>() {
      @Override public void onFailure(Throwable t) {
        results.setException(t);
      }

      @Override public void onSuccess(ImmutableList<VersionInfo> info) {
        // If the result is successfully written (we haven't already timed out)...
        if (results.set(info)) {
          // Then save the current time as the last successful read
          upToDatePreferences.lastRead = System.currentTimeMillis();
          preferencesManager.save();         
        }
      }
    }, executorService);

    // Finally, schedule (with a timeout) a loader-canceller
    // Blindly cancel (which will push an exception to the results);  if the results
    // have already succeeded, this will be ignored.
    @SuppressWarnings("unused")
    ListenableScheduledFuture<Boolean> unused =
        executorService.schedule(() -> loader.cancel(true), TIMEOUT_IN_SECONDS, TimeUnit.SECONDS);
    return results;
  }
  
  /** Generate a feature string off the UpToDatePreferences, for some extremely basic analytics (is feature "X" used at all?) */
  private static String getFeaturesString(UpToDatePreferences upToDatePreferences) {
    List<String> features = new ArrayList<>();
    if (upToDatePreferences.multipleUsers) {
      features.add("u");
    }
    return Joiner.on(',').join(features);
  }

  @SuppressWarnings("unchecked")
  private List<VersionInfo> deserialize(Reader reader) throws JsonParseException {
    Gson gson = new Gson();
    Type versionInfoType = new TypeToken<List<VersionInfo>>() {}.getType();

    return (List<VersionInfo>) gson.fromJson(reader, versionInfoType);
  }

  static class VersionNumber implements Comparable<VersionNumber> {
    private static Splitter PERIOD_SPLITTER = Splitter.on('.');
    private final ImmutableList<Integer> versions;
    private final String versionString;
    
    VersionNumber(String versionString) {
      this.versionString = versionString;
      versions = Streams.stream(PERIOD_SPLITTER.split(versionString))
          .map(s -> {Integer i = Ints.tryParse(s);  return i == null ? 0 : i;})
          .collect(ImmutableList.toImmutableList());
    }

    @Override
    public int compareTo(VersionNumber other) {
      // Only run with this having the same length or longer
      if (other.versions.size() > this.versions.size()) {
        return other.compareTo(this);
      }
      
      for (int i = 0; i < versions.size(); i++) {
        int compare = Ints.compare(versions.get(i), other.versions.get(i));
        if (compare != 0) { 
          return compare;
        }
      }
      
      if (versions.size() > other.versions.size()) {
        return 1;
      }
      return 0;
    }
    
    @Override
    public String toString() {
      return versionString;
    }
  }
  
  /** Treat 0.8.5-SNAPSHOT as 0.8.5, etc... */
  private static String versionMinusSnapshot(String currentVersion) {
    return currentVersion.replace("-SNAPSHOT", "");
  }
}
