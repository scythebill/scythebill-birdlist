/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.awt.Component;
import java.awt.Toolkit;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.TransferHandler;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import org.joda.time.Duration;
import org.joda.time.Instant;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.base.Throwables;
import com.google.inject.Inject;
import com.scythebill.birdlist.ui.fonts.FontManager;
import com.scythebill.birdlist.ui.guice.Scythebill;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Utilities dealing with alerts.
 */
public class Alerts {
  private final Icon appIcon;

  @Inject
  public Alerts(
      @Scythebill Icon appIcon) {
    this.appIcon = appIcon;
  }

  /** Returnn the icon to be shown in alerts. */
  public Icon getAlertIcon() {
    return appIcon;
  }
  
  public int showConfirm(Object parentObject, Messages.Name title, Messages.Name message,
      Object... args) {
    // TODO: use Quaqua JSheet on MacOS?
    String formattedMessage = getFormattedDialogMessage(title, message, args);
  
    return JOptionPane.showConfirmDialog(parentComponent(parentObject), formattedMessage, "",
        JOptionPane.YES_NO_CANCEL_OPTION,
        JOptionPane.QUESTION_MESSAGE,
        appIcon);
  }

  @Deprecated
  public int showConfirm(Object parentObject, String title, String message,
      Object... args) {
    // TODO: use Quaqua JSheet on MacOS?
    String formattedMessage = getFormattedDialogMessage(title, message, args);
  
    return JOptionPane.showConfirmDialog(parentComponent(parentObject), formattedMessage, "",
        JOptionPane.YES_NO_CANCEL_OPTION,
        JOptionPane.QUESTION_MESSAGE,
        appIcon);
  }

  public int showYesNo(Object parentObject, Messages.Name title, Messages.Name message,
      Object... args) {
    String formattedMessage = getFormattedDialogMessage(title, message, args);
  
    return JOptionPane.showConfirmDialog(parentComponent(parentObject), formattedMessage, "",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.QUESTION_MESSAGE,
        appIcon);
  }

  @Deprecated
  public int showYesNo(Object parentObject, String title, String message,
      Object... args) {
    String formattedMessage = getFormattedDialogMessage(title, message, args);
  
    return JOptionPane.showConfirmDialog(parentComponent(parentObject), formattedMessage, "",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.QUESTION_MESSAGE,
        appIcon);
  }

  public int showOkCancel(Object parentObject, Messages.Name title,
      Messages.Name message, Object... args) {
    String formattedMessage = getFormattedDialogMessage(title, message, args);
  
    return JOptionPane.showConfirmDialog(parentComponent(parentObject), formattedMessage, "",
        JOptionPane.OK_CANCEL_OPTION,
        JOptionPane.INFORMATION_MESSAGE,
        appIcon);
  }

  @Deprecated
  public int showOkCancel(Object parentObject, String title,
      String message, Object... args) {
    String formattedMessage = getFormattedDialogMessage(title, message, args);
  
    return JOptionPane.showConfirmDialog(parentComponent(parentObject), formattedMessage, "",
        JOptionPane.OK_CANCEL_OPTION,
        JOptionPane.INFORMATION_MESSAGE,
        appIcon);
  }

  public int showWithOptions(Object parentObject, Messages.Name title,
      Messages.Name message, Object... options) {
    String formattedMessage = getFormattedDialogMessage(title, message);
  
    return JOptionPane.showOptionDialog(parentComponent(parentObject),
        formattedMessage, "",
        JOptionPane.DEFAULT_OPTION,
        JOptionPane.WARNING_MESSAGE,
        appIcon,
        options, options[0]);
  }
  
  @Deprecated
  public int showWithOptions(Object parentObject, String title,
      String message, Object... options) {
    String formattedMessage = getFormattedDialogMessage(title, message);
  
    return JOptionPane.showOptionDialog(parentComponent(parentObject),
        formattedMessage, "",
        JOptionPane.DEFAULT_OPTION,
        JOptionPane.WARNING_MESSAGE,
        appIcon,
        options, options[0]);
  }
  
  public String getInput(Object parentObject, Messages.Name title, Messages.Name message,
      String defaultValue) {
    String formattedMessage = getFormattedDialogMessage(title, message);
    // Add a little more visual spacing before the text field
    formattedMessage += "<br>";
    Object result = JOptionPane.showInputDialog(parentComponent(parentObject),
        formattedMessage, "",
        JOptionPane.QUESTION_MESSAGE,
        appIcon,
        null,
        defaultValue);
    return result == null ? null : result.toString();
  }

  public void showError(Object parentObject, Messages.Name title, Messages.Name message,
      Object... args) {
    String formattedMessage = getFormattedDialogMessage(title, message, args);
  
    JOptionPane.showMessageDialog(parentComponent(parentObject), formattedMessage, "",
        JOptionPane.ERROR_MESSAGE,
        appIcon);
  }

  @Deprecated
  public void showError(Object parentObject, String title, String message,
      Object... args) {
    String formattedMessage = getFormattedDialogMessage(title, message, args);
  
    JOptionPane.showMessageDialog(parentComponent(parentObject), formattedMessage, "",
        JOptionPane.ERROR_MESSAGE,
        appIcon);
  }

  public void showMessage(Object parentObject, Messages.Name title, Messages.Name message,
      Object... args) {
    String formattedMessage = getFormattedDialogMessage(title, message, args);
  
    JOptionPane.showMessageDialog(parentComponent(parentObject), formattedMessage, "",
        JOptionPane.PLAIN_MESSAGE, appIcon);
  }
  
  @Deprecated
  public void showMessage(Object parentObject, String title, String message,
      Object... args) {
    String formattedMessage = getFormattedDialogMessage(title, message, args);
  
    JOptionPane.showMessageDialog(parentComponent(parentObject), formattedMessage, "",
        JOptionPane.PLAIN_MESSAGE, appIcon);
  }
  
  public void showOkWithPanel(Object parentObject, String formattedMessage, JComponent component) {
    JOptionPane.showConfirmDialog(parentComponent(parentObject),
        new Object[]{formattedMessage, component}, "",
        JOptionPane.OK_OPTION,
        JOptionPane.INFORMATION_MESSAGE,
        null);
  }

  public int showOkCancelWithPanel(Object parentObject, String formattedMessage, JComponent component) {
    return JOptionPane.showConfirmDialog(parentComponent(parentObject),
        new Object[]{formattedMessage, component}, "",
        JOptionPane.OK_CANCEL_OPTION,
        JOptionPane.INFORMATION_MESSAGE,
        appIcon);
  }
  
  public int showYesNoWithPanel(Object parentObject, String formattedMessage, JComponent component) {
    return JOptionPane.showConfirmDialog(parentComponent(parentObject),
        new Object[]{formattedMessage, component}, "",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.QUESTION_MESSAGE,
        appIcon);
  }

  public int showOptionsWithPanel(Object parentObject, String formattedMessage, JComponent component, Object... options) {
    return JOptionPane.showOptionDialog(parentComponent(parentObject),
        new Object[]{formattedMessage, component}, "",
        JOptionPane.DEFAULT_OPTION,
        JOptionPane.QUESTION_MESSAGE,
        appIcon,
        options,
        options[0]);
  }

  public String getFormattedDialogMessage(Messages.Name title, Messages.Name message,
      Object... args) {
    return getFormattedDialogMessage(
        title.toString(), message.toString(), args);
    
  }
  
  @Deprecated
  public String getFormattedDialogMessage(String title, String message,
      Object... args) {
    if (args.length > 0) {
      message = String.format(message, args);
    }
    
    String fontName = FontManager.getFontName();
    String css = String.format(
        "<head><style type=\"text/css\">b { font: %spt \"%s\" } p { font: %spt \"%s\"; margin-top: %spx }</style></head>",
        // I experimented with scaling these with FontManager, but it looked *weird*.
        14, fontName, 12, fontName, 8);
    message = message.replace("\n", "<br>");
 
    return String.format("<html>" + css + "<b>%1$s</b><p>%2$s</p>", title, message);
  }
  
  public String getFormattedDialogMessageUnscaled(Messages.Name titleName, Messages.Name messageName,
      Object... args) {
    String message;
    if (args.length > 0) {
      message = Messages.getFormattedMessage(messageName, args);
    } else {
      message = Messages.getMessage(messageName);
    }

    return String.format("<html><b>%1$s</b><br><br>%2$s", Messages.getMessage(titleName), message);    
  }
  
  private static volatile Instant lastError = null;
  private static final Duration ERROR_INTERVAL = Duration.standardSeconds(30);
  
  public void showError(
      Object parentObject,
      Throwable t,
      Messages.Name title, Messages.Name message,
      Object... args) {
    String formattedMessage = getFormattedDialogMessage(title, message, args);
  
    JScrollPane scrollPane = scrollableErrorDisplay(t);
    JOptionPane.showMessageDialog(parentComponent(parentObject), 
        new Object[]{formattedMessage, "", scrollPane},
        "",
        JOptionPane.ERROR_MESSAGE,
        appIcon);
  }

  @Deprecated
  public void showError(
      Object parentObject,
      Throwable t,
      String title,
      String message,
      Object... args) {
    String formattedMessage = getFormattedDialogMessage(title, message, args);
  
    JScrollPane scrollPane = scrollableErrorDisplay(t);
    JOptionPane.showMessageDialog(parentComponent(parentObject), 
        new Object[]{formattedMessage, "", scrollPane},
        "",
        JOptionPane.ERROR_MESSAGE,
        appIcon);
  }

  public void reportError(Throwable t) {
    if (isIgnorableError(t)) {
      return;
    }
    
    // Don't report errors more than once every thirty seconds or so
    Instant now = new Instant();
    if (lastError != null
        && lastError.plus(ERROR_INTERVAL).isAfter(now)) {
      return;
    }
    lastError = now;
    
    String formattedMessage = getFormattedDialogMessage(
        Name.EXCEPTION_TITLE, Name.EXCEPTION_MESSAGE);
    JScrollPane scrollPane = scrollableErrorDisplay(t);
    
    JOptionPane.showMessageDialog(null, new Object[]{formattedMessage, "", scrollPane}, "",
        JOptionPane.ERROR_MESSAGE, appIcon);
  }

  private JScrollPane scrollableErrorDisplay(Throwable t) {
    final JTextArea textArea = new JTextArea();
    String stackTrace = Throwables.getStackTraceAsString(t);
    // Abbreviate a bit (by dropping the usual prefix)
    stackTrace = stackTrace.replaceAll("com\\.scythebill\\.birdlist", "");
    textArea.setText(t.getMessage() + "\n" + stackTrace);
    textArea.setColumns(40);
    textArea.setRows(6);
    textArea.setEditable(false);
    JScrollPane scrollPane = new JScrollPane(
        textArea,
        JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
        JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    textArea.addAncestorListener(new AncestorListener() {
      @Override public void ancestorRemoved(AncestorEvent event) {
      }
      
      @Override public void ancestorMoved(AncestorEvent event) {
      }
      
      @Override public void ancestorAdded(AncestorEvent event) {
        textArea.selectAll();
        textArea.getTransferHandler().exportToClipboard(
            textArea,
            Toolkit.getDefaultToolkit().getSystemClipboard(),
            TransferHandler.COPY);
        textArea.setTabSize(2);
        textArea.select(0, 0);
        textArea.requestFocusInWindow();
      }
    });
    return scrollPane;
  }

  private boolean isIgnorableError(Throwable t) {
    // Don't report any error that doesn't contain a hint of "scythebill"; it's almost certainly
    // a Java bug.
    String stackTrace = Throwables.getStackTraceAsString(t);
    if (!stackTrace.contains("scythebill")) {
      return true;
    }

    // See https://bugs.openjdk.java.net/browse/JDK-8229821 ; triggered by the
    // JDK for MacOS when unplugging a monitor.  Fixed in JDK 14, should
    // be safe to remove then.  This is generally caught by the prior check,
    // but I've seen it appear sometimes with Scythebill on the stack.
    // In theory, this could hide bugs in Scythebill, but they will at least
    // get logged.
    if (t.getMessage() != null
        && t.getMessage().equals("Width (0) and height (0) cannot be <= 0")) {
      return true;
    }
    
    return false;
  }

  public void showFeedback(Messages.Name title, String feedback) {
    String formattedMessage = getFormattedDialogMessage(
        title, Name.FEEDBACK_IS_IN_CLIPBOARD);
    final JTextArea textArea = new JTextArea();
    textArea.setColumns(40);
    textArea.setRows(6);
    textArea.setEditable(false);
    textArea.setText(feedback);
    JScrollPane scrollPane = new JScrollPane(
        textArea,
        JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
        JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    textArea.addAncestorListener(new AncestorListener() {
      @Override public void ancestorRemoved(AncestorEvent event) {
      }
      
      @Override public void ancestorMoved(AncestorEvent event) {
      }
      
      @Override public void ancestorAdded(AncestorEvent event) {
        textArea.selectAll();
        textArea.getTransferHandler().exportToClipboard(
            textArea,
            Toolkit.getDefaultToolkit().getSystemClipboard(),
            TransferHandler.COPY);
        textArea.setTabSize(2);
        textArea.select(0, 0);
        textArea.requestFocusInWindow();
      }
    });
    
    JOptionPane.showMessageDialog(null, new Object[]{formattedMessage, "", scrollPane}, "",
        JOptionPane.INFORMATION_MESSAGE, appIcon);
  }

  public void showScrollableInfo(Object parentObject, String formattedMessage, String info) {
    final JTextArea textArea = new JTextArea();
    textArea.setColumns(40);
    int lines = CharMatcher.is('\n').countIn(info) + 1;
    // Add an extra line for every string longer than 60 characters.
    // 60 > 40, right?  But "columns" is in terms of the width of an 'm',
    // which is relatively wide (in English).  Exact computations would
    // be better, but not so much better that I care.
    lines += Splitter.on('\n').splitToList(info).stream()
        .mapToInt(s -> s.length() / 60)
        .sum();
    
    textArea.setRows(Math.min(15, Math.max(5, lines)));
    textArea.setEditable(false);
    textArea.setText(info);
    textArea.setWrapStyleWord(true);
    textArea.setLineWrap(true);
    textArea.setCaretPosition(0);
    JScrollPane scrollPane = new JScrollPane(
        textArea,
        JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
        JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    
    JOptionPane.showMessageDialog(parentComponent(parentObject), new Object[]{formattedMessage, "", scrollPane}, "",
        JOptionPane.INFORMATION_MESSAGE, appIcon);
  }
  
  private Component parentComponent(Object parentObject) {
    if (parentObject instanceof Component) {
      return (Component) parentObject; 
    }
    
    return null;
  }
}
