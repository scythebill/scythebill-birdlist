/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;

/**
 * Base implementation of TreeModel with listener implementations.
 */
public abstract class BaseTreeModel implements TreeModel {
  private final CopyOnWriteArrayList<TreeModelListener> listeners = new CopyOnWriteArrayList<TreeModelListener>();

  @Override
  public void addTreeModelListener(TreeModelListener listener) {
    if (listener != null && !listeners.contains(listener)) {
      listeners.add(listener);
    }
  }

  @Override
  public void removeTreeModelListener(TreeModelListener listener) {
    if (listener != null) {
      listeners.remove(listener);
    }
  }

  protected void fireTreeNodesChanged(TreeModelEvent e) {
    for (TreeModelListener listener : listeners) {
      listener.treeNodesChanged(e);
    }
  }

  protected void fireTreeNodesInserted(TreeModelEvent e) {
    for (TreeModelListener listener : listeners) {
      listener.treeNodesInserted(e);
    }
  }

  protected void fireTreeNodesRemoved(TreeModelEvent e) {
    for (TreeModelListener listener : listeners) {
      listener.treeNodesRemoved(e);
    }
  }

  protected void fireTreeStructureChanged(TreeModelEvent e) {
    for (TreeModelListener listener : listeners) {
      listener.treeStructureChanged(e);
    }
  }

}