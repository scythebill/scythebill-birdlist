/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.awt.Desktop;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.Desktop.Action;
import java.io.File;
import java.io.IOException;
import java.net.URI;

import com.scythebill.birdlist.ui.messages.Messages.Name;

/** Simple utilities for working with the Desktop class. */
public class DesktopUtils {
  private DesktopUtils() {
  }

  /**
   * Tries top open an HTML file in a browser.
   */
  public static void openHtmlFileInBrowser(File file) throws IOException {
    if (Desktop.getDesktop().isSupported(Action.BROWSE)) {
      try {
        Desktop.getDesktop().browse(file.toURI());
        return;
      } catch (IOException e) {
        // Swallow the exception and fall through to trying open(); seems like for at least some
        // users (Windows 7?), browse() failed. The code wasn't testing whether BROWSE was
        // supported, so it's possible that simply testing BROWSE would have done the trick, but
        // I never had this locally reproduced.
      }
    }
    
    // Browse didn't work... try opening the file directly.
    Desktop.getDesktop().open(file);
  }

  public static void openUrlInBrowser(URI uri, Alerts alerts) throws IOException {
    try {
      Desktop.getDesktop().browse(uri);
    } catch (Exception e) {
      Toolkit.getDefaultToolkit().getSystemClipboard().setContents(
          new StringSelection(uri.toASCIIString()), null);
      
      alerts.showMessage(null, Name.COULDNT_OPEN_WEB_PAGE_TITLE, Name.COULDNT_OPEN_WEB_PAGE_MESSAGE,
          uri.toASCIIString());
    }
  }
}
