/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Base class for listeners looking for double-clicks. 
 */
abstract public class DoubleClickListener implements MouseListener {

  abstract protected void doubleClicked(MouseEvent event);

  @Override
  public void mouseClicked(MouseEvent event) {
    if (event.getClickCount() == 2)
      doubleClicked(event);
  }

  @Override
  public void mousePressed(MouseEvent event) {
  }

  @Override
  public void mouseReleased(MouseEvent event) {
  }

  @Override
  public void mouseEntered(MouseEvent event) {
  }

  @Override
  public void mouseExited(MouseEvent event) {
  }

}
