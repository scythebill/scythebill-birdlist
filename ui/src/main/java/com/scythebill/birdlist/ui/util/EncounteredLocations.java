/*   
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.util.LinkedHashSet;
import java.util.Set;

import com.google.common.collect.ImmutableSet;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Scans a ReportSet to identify visited locations.
 */
public class EncounteredLocations {
  private ImmutableSet<String> locationIds;

  private EncounteredLocations(ImmutableSet<String> locationIds) {
    this.locationIds = locationIds;
  }
  
  /**
   * Returns true if the provided location is encountered.
   */
  public boolean encounteredLocation(Location location) {
    return locationIds.contains(location.getId());
  }

  /**
   * Starts a background scan of locations.
   */
  public static ListenableFuture<EncounteredLocations> scan(Taxonomy taxonomy, ReportSet reportSet,
      ListeningExecutorService executorService) {
    LocationScanner scanner = new LocationScanner(taxonomy, reportSet);
    return executorService.submit(scanner);
  }

  static class LocationScanner extends ReportSetScanner<EncounteredLocations> {
    private final Set<String> locationIds = new LinkedHashSet<>();
    private final LocationSet locations;

    LocationScanner(Taxonomy taxonomy, ReportSet reportSet) {
      super(taxonomy, reportSet);
      locations = reportSet.getLocations();
    }

    @Override
    protected void process(Sighting sighting, SightingTaxon taxon) throws Exception {
      String locationId = sighting.getLocationId();
      Location location = null;
      while (locationId != null) {
        if (!locationIds.add(locationId)) {
          // Location already existed, no need to look for parents
          break;
        }

        if (location == null) {
          location = locations.getLocation(locationId).getParent();
        } else {
          location = location.getParent();
        }

        locationId = location == null ? null : location.getId();
      }
    }

    @Override
    protected EncounteredLocations accumulated() {
      return new EncounteredLocations(ImmutableSet.copyOf(locationIds));
    }

    @Override
    protected boolean resolveTaxaToTaxonomy() {
      // Doesn't care about the sighting taxa, so don't bother
      return false;
    }
    
  }
}
