/*   
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.util.LinkedHashSet;
import java.util.Set;

import com.google.common.collect.ImmutableSet;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Scans a taxonomy to extract encountered taxa of all levels.
 */
public class EncounteredTaxa {
  private ImmutableSet<String> taxonIds;

  private EncounteredTaxa(ImmutableSet<String> taxonIds) {
    this.taxonIds = taxonIds;
  }
  
  public boolean isEncountered(Taxon taxon) {
    return taxonIds.contains(taxon.getId());
  }
  
  public boolean isEncountered(String taxonId) {
    return taxonIds.contains(taxonId);
  }

  public static ListenableFuture<EncounteredTaxa> scan(Taxonomy taxonomy, ReportSet reportSet,
      ListeningExecutorService executorService) {
    TaxaScanner scanner = new TaxaScanner(taxonomy, reportSet);
    return executorService.submit(scanner);
  }

  static class TaxaScanner extends ReportSetScanner<EncounteredTaxa> {
    private final Set<String> taxonIds = new LinkedHashSet<>();

    TaxaScanner(Taxonomy taxonomy, ReportSet reportSet) {
      super(taxonomy, reportSet);
    }

    @Override
    protected void process(Sighting sighting, SightingTaxon taxon) throws Exception {
      if (taxon.getType() == SightingTaxon.Type.SINGLE) {
        addTaxa(taxon.getId());
      } else {
        for (String taxonId : taxon.getIds()) {
          addTaxa(taxonId);
        }
      }
    }

    private void addTaxa(String taxonId) {
      Taxon taxon = null;
      while (taxonId != null) {
        if (!taxonIds.add(taxonId)) {
          // Taxon already encountered, no need to look for parents
          break;
        }

        if (taxon == null) {
          taxon = getTaxonomy().getTaxon(taxonId).getParent();
        } else {
          taxon = taxon.getParent();
        }

        taxonId = taxon == null ? null : taxon.getId();
      }
    }

    @Override
    protected EncounteredTaxa accumulated() {
      return new EncounteredTaxa(ImmutableSet.copyOf(taxonIds));
    }
  }
}
