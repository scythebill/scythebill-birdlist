/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.awt.Component;
import java.awt.FileDialog;
import java.awt.Frame;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Optional;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;

import org.apache.commons.lang3.SystemUtils;

import com.google.common.collect.ImmutableSet;
import com.google.inject.ImplementedBy;
import com.google.inject.Inject;
import com.scythebill.birdlist.model.io.HtmlResponseWriter;
import com.scythebill.birdlist.ui.app.ReportSetSaver.WindowsPermissionException;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;
import com.scythebill.birdlist.ui.panels.FilePreferences;
import com.scythebill.birdlist.ui.panels.FilePreferences.FileType;

/**
 * Utilities handling FileDialogs.
 */
public class FileDialogs {
  private final static ImmutableSet<String> DANGEROUS_PATHS =
      SystemUtils.IS_OS_WINDOWS ?  ImmutableSet.of("Program Files", "Program Files (x86)") : ImmutableSet.of();
  
  /** Flags adjusting FileDialogs behavior */
  @ImplementedBy(FlagsDefaults.class)
  public interface Flags {
    boolean useNativeSaveDialog();
    boolean useNativeOpenDialog();
  }

  /** Default implementation. */
  static class FlagsDefaults implements Flags {
    @Inject FlagsDefaults() {}
    
    @Override public boolean useNativeOpenDialog() {
      return false;
    }

    @Override public boolean useNativeSaveDialog() {
      return false;
    }
    
  }
  
  private final FilePreferences filePreferences;
  private final Flags flags;
  private final Alerts alerts;

  @Inject
  public FileDialogs(
      FilePreferences filePreferences, Flags flags, Alerts alerts) {
    this.filePreferences = filePreferences;
    this.flags = flags;
    this.alerts = alerts;
  }
  
  /**
   * Shows an open-file dialog.
   */
  public File openFile(Frame parent, String title, FileFilter fileFilter, FileType fileType) {
    File file = null;
    if (flags.useNativeOpenDialog()) {
      if (parent == null) {
        parent = new Frame();
      }
      // use the native file dialog on the mac
      FileDialog dialog = new FileDialog(parent, title, FileDialog.LOAD);
      if (fileFilter != null) {
        dialog.setFilenameFilter(FileDialogs.toFilenameFilter(fileFilter));
      }

      File directory = filePreferences.getLastDirectory(fileType);
      if (directory != null) {
        dialog.setDirectory(directory.getAbsolutePath());
      }

      dialog.setVisible(true);
      
      String filename = dialog.getFile();
      if (filename != null) {
        file = new File(dialog.getDirectory(), filename);
      }
    } else {
      // use a swing file dialog on the other platforms
      JFileChooser chooser = new JFileChooser();
      chooser.setDialogTitle(title);
      if (fileFilter != null) {
        chooser.setFileFilter(fileFilter);
      }
  
      File directory = filePreferences.getLastDirectory(fileType);
      if (directory != null) {
        chooser.setCurrentDirectory(directory);
      }

      if (chooser.showOpenDialog(parent) == JFileChooser.APPROVE_OPTION) {
        file = chooser.getSelectedFile();
      }
    }
  
    if (file != null) {
      filePreferences.rememberFile(file, fileType);
    }
    return file;
  }

  /**
   * Show a save-file dialog.
   * TODO: instead of taking a file filter, force a file suffix on the saved file?
   */
  public File saveFile(Frame parent, String title, String defaultName,
      FileFilter fileFilter, FileType fileType) {
    File file = null;
    
    while (true) {
      if (flags.useNativeSaveDialog()) {
        FileDialog dialog = new FileDialog(parent, title, FileDialog.SAVE);
        if (defaultName != null) {
          dialog.setFile(defaultName);
        }
    
        if (fileFilter != null) {
          dialog.setFilenameFilter(FileDialogs.toFilenameFilter(fileFilter));
        }
    
        File directory = filePreferences.getLastDirectory(fileType);
        if (directory != null) {
          dialog.setDirectory(directory.getAbsolutePath());
        }
        dialog.setVisible(true);
    
        String filename = dialog.getFile();
        if (filename != null) {
          file = new File(dialog.getDirectory(), filename);
        }
      } else {
        // use a swing file dialog on the other platforms
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle(title);
        if (fileFilter != null) {
          chooser.setFileFilter(fileFilter);
        }
        
        File directory = filePreferences.getLastDirectory(fileType);
        if (directory != null) {
          chooser.setCurrentDirectory(directory);
        }
  
        if (chooser.getCurrentDirectory() != null) {
          chooser.setSelectedFile(new File(chooser.getCurrentDirectory(), defaultName));
        }
        
        if (chooser.showSaveDialog(parent) == JFileChooser.APPROVE_OPTION) {
          filePreferences.rememberFile(chooser.getSelectedFile(), fileType);
          file = chooser.getSelectedFile();
        }
        
        // For Swing codepaths, warn the user if the file already exists.
        // Native file dialogs are assumed to do this.
        // TODO: test this assertion on Windows
        if (file != null) {
          if (file.exists()) {
            Object[] options = {
                Messages.getMessage(Name.CANCEL_BUTTON),
                Messages.getMessage(Name.REPLACE_TEXT)
            };
            String message = Messages.getFormattedMessage(
                Name.FILE_EXISTS_MESSAGE,
                HtmlResponseWriter.htmlEscape(file.getName()),
                HtmlResponseWriter.htmlEscape(file.getParentFile().getName()));
            
            String alertTitle = Messages.getFormattedMessage(
                Name.FILE_EXISTS_TITLE,
                HtmlResponseWriter.htmlEscape(file.getName()));
            
            int option = alerts.showWithOptions(
                parent,
                alertTitle,
                message, options);
            // 1 == Replace (index 1 in the options above)
            if (option != 1) {
              return null;
            }
          }
        }
      }
      
      // See if the user picked a "dangerous" path.  If so, warn the user,
      // and get another filename.
      if (file != null) {
        Optional<String> dangerousPath = findDangerousPath(file);
        if (dangerousPath.isPresent()) {
          if (alerts.showYesNo(null,
              Name.PICK_A_NEW_LOCATION,
              Name.SAVING_FILES_NOT_RECOMMENDED_FORMAT,
              dangerousPath.get()) == JOptionPane.YES_OPTION) {
            continue;
          }
        }
      }
      
      // Done with the while loop
      break;
    }
  
    if (file != null) {
      filePreferences.rememberFile(file, fileType);
    }
    
    return file;
  }

  /**
   * Shows an open-file dialog.
   */
  public File[] openFiles(Frame parent, String title, FileFilter fileFilter, FileType fileType) {
    File[] files;
    if (flags.useNativeOpenDialog()) {
      if (parent == null) {
        parent = new Frame();
      }
      // use the native file dialog on the mac
      FileDialog dialog = new FileDialog(parent, title, FileDialog.LOAD);
      setMultipleMode(dialog);
      if (fileFilter != null) {
        dialog.setFilenameFilter(FileDialogs.toFilenameFilter(fileFilter));
      }

      File directory = filePreferences.getLastDirectory(fileType);
      if (directory != null) {
        dialog.setDirectory(directory.getAbsolutePath());
      }

      dialog.setVisible(true);
      
      files = getFiles(dialog);
    } else {
      // use a swing file dialog on the other platforms
      JFileChooser chooser = new JFileChooser();
      chooser.setDialogTitle(title);
      chooser.setMultiSelectionEnabled(true);
      if (fileFilter != null) {
        chooser.setFileFilter(fileFilter);
      }
  
      File directory = filePreferences.getLastDirectory(fileType);
      if (directory != null) {
        chooser.setCurrentDirectory(directory);
      }

      if (chooser.showOpenDialog(parent) == JFileChooser.APPROVE_OPTION) {
        files = chooser.getSelectedFiles();
      } else {
        files = new File[0];
      }
    }
  
    if (files.length > 0) {
      filePreferences.rememberFile(files[0], fileType);
    }
    return files;
  }

  public File openDirectory(Frame frame, String title, File directory, FileType fileType) {
    JFileChooser chooser = new JFileChooser();
    chooser.setDialogTitle(title);
    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    // Use "save", just so we have a "New dialog" option
    chooser.setDialogType(JFileChooser.SAVE_DIALOG);
    chooser.setFileFilter(new FileFilter() {
      @Override
      public String getDescription() {
        return Messages.getMessage(Name.ALL_DIRECTORIES);
      }
      
      @Override
      public boolean accept(File f) {
        return f.isDirectory();
      }
    });
    chooser.setApproveButtonText(Messages.getMessage(Name.SELECT_TEXT));
    
    if (directory == null) {
      directory = filePreferences.getLastDirectory(fileType);
    }
    
    if (directory != null) {
      chooser.setCurrentDirectory(directory);
    }
    
    // Horrifying hack:  save dialogs include a "Save as..." region.  Hide it. 
    for (Component child : chooser.getComponents()) {
      if (child instanceof JPanel) {
        ((JPanel) child).getComponent(0).setVisible(false);
        break;
      }
    }
    if (chooser.showSaveDialog(frame) == JFileChooser.APPROVE_OPTION) {
      // Horrifying hack 2: because we hid the "Save as..." region, its text might
      // leak into the selection
      File selectedFile = chooser.getSelectedFile();
      if (!selectedFile.exists()) {
        selectedFile = selectedFile.getParentFile();
      }
      return selectedFile;
    } else {
      return null;
    }
  }

  /**
   * Given a file, returns a non-empty Optional with the name
   * of a "dangerous" path segment if one is found.
   */
  public Optional<String> findDangerousPath(File path) {
    if (path.isDirectory()
        && DANGEROUS_PATHS.contains(path.getName())) {
      return Optional.of(path.getName());
    }
    
    path = path.getParentFile();
    if (path == null) {
      return Optional.empty();
    }
    
    return findDangerousPath(path);
  }
  
  public static void showFileSaveError(
      Alerts alerts, IOException e, File file) {
    if (e instanceof WindowsPermissionException) {
      alerts.showError(null, Name.SAVING_FAILED_TITLE, Name.WINDOWS_PERMISSION_MESSAGE,
          file.getParentFile().getAbsolutePath());
    } else if (e.getMessage().contains("Read-only")
        || e.getMessage().contains("Access is denied")
        || e.getMessage().contains("Operation not permitted")) {
      alerts.showError(null,
          Name.COULD_NOT_WRITE_INTO_DIRECTORY_TITLE,
          Name.COULD_NOT_WRITE_INTO_DIRECTORY_FORMAT,
          HtmlResponseWriter.htmlEscape(file.getParentFile().getName()));
    } else {
      alerts.showError(null,
          Name.SAVING_FAILED_TITLE,
          Name.COULD_NOT_WRITE_FORMAT,
          HtmlResponseWriter.htmlEscape(file.getName()));
    }
  }

  private void setMultipleMode(FileDialog dialog) {
    dialog.setMultipleMode(true);
  }

  private File[] getFiles(FileDialog dialog) {
    return dialog.getFiles();
  }


  static FilenameFilter toFilenameFilter(final FileFilter fileFilter) {
    return new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        return fileFilter.accept(new File(dir, name));
      }
    };
  }

  public boolean isInsideZipFile(File file) {
    return file.getAbsolutePath().contains("\\AppData\\Local\\Temp\\");
  }

}
