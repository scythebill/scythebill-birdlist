/*
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.scythebill.birdlist.ui.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;

import javax.annotation.Nullable;

import org.apache.commons.text.similarity.LevenshteinDistance;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Species;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.util.BKTree;
import com.scythebill.birdlist.model.util.Metrics;
import com.scythebill.birdlist.model.util.ResolvedComparator;

/**
 * Finds species in a block of text.
 */
public class FindSpeciesInText {
  private static final int MAX_BUFFER_SIZE = 5;
  private static final CharMatcher NOT_IN_WORD = CharMatcher.whitespace().or(CharMatcher.inRange('0', '9'))
      .or(CharMatcher.anyOf("-\u2010_,/{}*\\|!@#$%^&\";:.<>?=+`~"));
  private static final CharMatcher IN_WORD = NOT_IN_WORD.negate();

  private final TaxonImporter taxonImporter;

  public static class Results {
    public final ImmutableSet<Resolved> found;
    public final String remainingText;

    Results(Set<Resolved> found, String remainingText) {
      this.found = ImmutableSet.copyOf(found);
      this.remainingText = remainingText;
    }
  }

  private static class Word {
    final int start;
    final int end;

    Word(String text, int start, int end) {
      this.start = start;
      this.end = end;
    }
  }

  public FindSpeciesInText(Taxonomy taxonomy, @Nullable Checklist checklist) {
    taxonImporter = new TaxonImporter(taxonomy, checklist);
  }

  public TaxonSearch primarySearch() {
    return taxonImporter.primarySearch();
  }

  public TaxonSearch secondarySearch(boolean onlySingletonMatches) {
    return taxonImporter.secondarySearch(onlySingletonMatches);
  }

  public TaxonSearch tertiarySearch() {
    return taxonImporter.tertiarySearch();
  }

  public Results search(String text, TaxonSearch search) {
    SortedSet<Resolved> taxa = Sets.newTreeSet(new ResolvedComparator());
    String remaining = searchPass(text, taxa, search);
    String remainingMinusEmptyLines = removeEmptyLines(remaining);
    return new Results(taxa, remainingMinusEmptyLines);
  }


  private String searchPass(String text, Set<Resolved> taxa, TaxonSearch searcher) {
    List<Word> buffer = Lists.newArrayListWithCapacity(MAX_BUFFER_SIZE);
    StringBuilder unusedText = new StringBuilder();
    int startOfTextToBeFlushed = 0;

    // Start at the start of the first word
    int wordStart = IN_WORD.indexIn(text);

    while (wordStart < text.length() && wordStart >= 0) {
      // Find the end of the next word
      int nextWordEnd = NOT_IN_WORD.indexIn(text, wordStart);
      if (nextWordEnd < 0) {
        // Go through to the end of the text (in case the last species
        // name ends without any whitespace, etc.)
        nextWordEnd = text.length();
        // ... but if that's nothing, then done.
        if (wordStart >= nextWordEnd) {
          break;
        }
      }

      // Skip trivial "words"
      if (nextWordEnd - wordStart == 1) {
        // Flush the current buffer
        while (!buffer.isEmpty()) {
          startOfTextToBeFlushed =
              flushBuffer(text, taxa, searcher, buffer, unusedText, startOfTextToBeFlushed);
        }
        // And then start after this word
        wordStart = IN_WORD.indexIn(text, nextWordEnd);
        continue;
      }

      Word next = new Word(text, wordStart, nextWordEnd);
      buffer.add(next);
      wordStart = IN_WORD.indexIn(text, nextWordEnd);

      // When the buffer is full, and a word will be discarded, search for
      // words using that word - starting with the full buffer, and going down
      // to just that word.
      if (buffer.size() == MAX_BUFFER_SIZE) {
        startOfTextToBeFlushed =
            flushBuffer(text, taxa, searcher, buffer, unusedText, startOfTextToBeFlushed);
      }
    }

    // Flush the rest of the buffer
    while (!buffer.isEmpty()) {
      startOfTextToBeFlushed =
          flushBuffer(text, taxa, searcher, buffer, unusedText, startOfTextToBeFlushed);
    }

    // And flush the last of the text to the unusedText
    appendTrimmingWhitespace(unusedText, text.substring(startOfTextToBeFlushed));

    return unusedText.toString();
  }

  /**
   * Does one round of flushing the word buffer, looking for a taxon.
   *
   * @return the updated "startOfTextToBeFlushed" value
   */
  private int flushBuffer(String text, Set<Resolved> taxa, TaxonSearch searcher, List<Word> buffer,
      StringBuilder unusedText, int startOfTextToBeFlushed) {
    int startingBufferSize = buffer.size();
    Word start = buffer.get(0);
    // Start from the end of the buffer (trying the longest names first),
    // and work towards shorter names.
    for (int i = buffer.size() - 1; i >= 0; i--) {
      Word end = buffer.get(i);
      String possibility = toLowerCaseAndMore(text.substring(start.start, end.end));
      Collection<Taxon> found = searcher.search(possibility);
      if (found != null && !found.isEmpty()) {
        for (Taxon taxon : found) {
          // Found a taxon: need to add it
          taxa.add(SightingTaxons.newResolved(taxon));
        }
        // Clear all the words used in this taxon
        for (int j = 0; j <= i; j++) {
          buffer.remove(0);
        }
        // And flush all previous words into unused text
        appendTrimmingWhitespace(unusedText, text.substring(startOfTextToBeFlushed, start.start));
        startOfTextToBeFlushed = end.end;
        // And done - don't search for shorter names that match too (some bird
        // names are substrings of other bird names!)
        break;
      }
    }

    // If nothing was removed, then no words were found, and
    // the first item of the buffer needs to be dropped
    if (buffer.size() == startingBufferSize) {
      buffer.remove(0);
    }
    return startOfTextToBeFlushed;
  }

  public interface TaxonSearch {
    Collection<Taxon> search(String possibility);
  }

  /**
   * A whitespace matcher which excludes newlines. {@link #appendTrimmingWhitespace} will try to
   * avoid having trailing whitespace and leading whitespace ... but that's not a good thing of one
   * of those is a newline and the other is not.
   */
  private static final CharMatcher WHITESPACE_BUT_NOT_NEWLINES =
      CharMatcher.whitespace().and(CharMatcher.isNot('\n')).precomputed();

  /**
   * StringBuilder.append(), but don't add more whitespace if the buffer already ends in whitespace.
   */
  private static void appendTrimmingWhitespace(StringBuilder builder, CharSequence text) {
    if (builder.length() != 0
        && WHITESPACE_BUT_NOT_NEWLINES.matches(builder.charAt(builder.length() - 1))) {
      builder.append(WHITESPACE_BUT_NOT_NEWLINES.trimLeadingFrom(text));
    } else {
      builder.append(text);
    }
  }

  /**
   * Maps the taxon of a field.
   */
  static class TaxonImporter {
    private final Map<String, Taxon> newGenusNames = Maps.newTreeMap(String.CASE_INSENSITIVE_ORDER);
    private final Multimap<String, Taxon> newSpeciesNames = HashMultimap.create();
    private final Multimap<String, Taxon> newCommonNames = HashMultimap.create();
    private final Multimap<String, Taxon> alternateCommonNames = HashMultimap.create();
    private final Multimap<String, Taxon> alternateSciNames = HashMultimap.create();
    private final BKTree<String, Taxon> commonNameTree;
    private final BKTree<String, Taxon> alternateCommonNameTree;
    private final Taxonomy taxonomy;

    public TaxonImporter(Taxonomy taxonomy, Checklist checklist) {
      this.taxonomy = taxonomy;

      // For every common name, store it in a BKTree to make finding species
      // by (closely-spelled) names easy.
      BKTree.Builder<String, Taxon> commonNameTree = BKTree.builder(Metrics.levenshteinDistance());

      if (checklist == null) {
        buildIndicesForTaxonomy(commonNameTree);
      } else {
        buildIndicesForTaxonomyFromChecklist(commonNameTree, checklist);
      }
      this.commonNameTree = commonNameTree.build();

      BKTree.Builder<String, Taxon> alternateCommonNameTree =
          BKTree.builder(Metrics.levenshteinDistance());
      // For any alternate name that is unique, store it off in a BKTree to make finding
      // species by (closely-spelled) names easy
      for (Entry<String, Collection<Taxon>> alternateEntry : alternateCommonNames.asMap()
          .entrySet()) {
        if (alternateEntry.getValue().size() == 1) {
          alternateCommonNameTree.add(trimSuffixes(alternateEntry.getKey()),
              Iterables.getOnlyElement(alternateEntry.getValue()));
        }
      }
      this.alternateCommonNameTree = alternateCommonNameTree.build();
    }

    public TaxonSearch primarySearch() {
      return new TaxonSearch() {
        @Override
        public Collection<Taxon> search(String possibility) {
          Collection<Taxon> collection = newCommonNames.get(possibility);
          if (collection.size() == 1) {
            return collection;
          }

          collection = newSpeciesNames.get(possibility);
          if (collection.size() == 1) {
            return collection;
          }

          return null;
        }
      };
    }

    /**
     * Search common names with a bit of distance; and search for lumps. Can return multiple
     * elements.
     */
    public TaxonSearch tertiarySearch() {
      return new TaxonSearch() {
        @Override
        public Collection<Taxon> search(String possibility) {
          if (possibility.length() > 6) {
            Collection<Taxon> collection = commonNameTree.search(possibility, 1);
            return collection;
          }

          Iterator<String> split = Splitter.on(' ').split(possibility).iterator();
          String genusName = split.next();
          if (split.hasNext()) {
            String species = split.next();
            if (!split.hasNext()) {
              Taxon genus = newGenusNames.get(genusName);
              if (genus != null) {
                // Try to find some option with the genus (perhaps as a group or subspecies)
                Taxon matchWithinGenus = findMatchWithin(genus, species, true /* prefer groups */);
                if (matchWithinGenus != null) {
                  return ImmutableList.of(matchWithinGenus);
                }
              }
            }
          }

          return null;
        }
      };
    }

    /** Search alternate common names. */
    public TaxonSearch secondarySearch(boolean onlySingletonMatches) {
      return new TaxonSearch() {
        @Override
        public Collection<Taxon> search(String possibility) {
          Collection<Taxon> collection = alternateCommonNames.get(possibility);
          if (!onlySingletonMatches || collection.size() == 1) {
            return collection;
          }

          return null;
        }
      };
    }

    public Taxon search(String possibility) {
      Collection<Taxon> collection = newCommonNames.get(possibility);
      if (collection.size() == 1) {
        return Iterables.getOnlyElement(collection);
      }

      collection = newSpeciesNames.get(possibility);
      if (collection.size() == 1) {
        return Iterables.getOnlyElement(collection);
      }

      if (possibility.length() > 3) {
        int distanceAllowed = possibility.length() < 5 ? 1 : 2;
        collection = commonNameTree.search(possibility, distanceAllowed);
        if (collection.size() == 1) {
          return Iterables.getOnlyElement(collection);
        }
      }

      Iterator<String> split = Splitter.on(' ').split(possibility).iterator();
      String genusName = split.next();
      if (split.hasNext()) {
        String species = split.next();
        if (!split.hasNext()) {
          Taxon genus = newGenusNames.get(genusName);
          if (genus != null) {
            // Try to find some option with the genus (perhaps as a group or subspecies)
            Taxon matchWithinGenus = findMatchWithin(genus, species, true /* prefer groups */);
            if (matchWithinGenus != null) {
              return matchWithinGenus;
            }
          }
        }
      }

      // Now search exact matches for alternate common names
      collection = alternateCommonNames.get(possibility);
      if (collection.size() == 1) {
        return Iterables.getOnlyElement(collection);
      }

      // And then close matches
      if (possibility.length() > 3) {
        int distanceAllowed = possibility.length() < 5 ? 1 : 2;
        collection = alternateCommonNameTree.search(possibility, distanceAllowed);
        if (collection.size() == 1) {
          return Iterables.getOnlyElement(collection);
        }
      }

      return null;
    }

    private void buildIndicesForTaxonomy(
        final BKTree.Builder<String, Taxon> commonNameTreeBuilder) {
      TaxonUtils.visitTaxa(taxonomy, new IndexBuilder(commonNameTreeBuilder));
    }

    private class IndexBuilder implements TaxonVisitor {
      private BKTree.Builder<String, Taxon> commonNameTreeBuilder;

      IndexBuilder(BKTree.Builder<String, Taxon> commonNameTreeBuilder) {
        this.commonNameTreeBuilder = commonNameTreeBuilder;
      }

      @Override
      public boolean visitTaxon(Taxon taxon) {
        switch (taxon.getType()) {
          case genus:
            newGenusNames.put(toLowerCaseAndMore(taxon.getName()), taxon);
            break;
          case species:
            // Store all names, common or otherwise
            newSpeciesNames.put(toLowerCaseAndMore(TaxonUtils.getFullName(taxon)), taxon);
          case group:
          case subspecies:            
            if (taxon.getCommonName() != null) {
              String commonName = toLowerCaseAndMore(taxon.getCommonName());
              newCommonNames.put(commonName, taxon);
              commonNameTreeBuilder.add(commonName, taxon);
            }
            for (String alternateCommonName : ((Species) taxon).getAlternateCommonNames()) {
              // Drop single-name alternates ("Wren", "Crane", etc.... they cause lots
              // of problems when family names are encountered)
              // TODO: make this an option
              if (alternateCommonName.indexOf(' ') >= 0) {
                alternateCommonName = toLowerCaseAndMore(trimSuffixes(alternateCommonName));
                alternateCommonNames.put(alternateCommonName, taxon);
              }
            }
            for (String alternateSciName : ((Species) taxon).getAlternateNames()) {
              alternateSciNames.put(toLowerCaseAndMore(alternateSciName), taxon);
            }
            break;
          default:
            break;
        }

        return true;
      }
    }

    private void buildIndicesForTaxonomyFromChecklist(
        final BKTree.Builder<String, Taxon> commonNameTreeBuilder, Checklist checklist) {

      IndexBuilder indexBuilder = new IndexBuilder(commonNameTreeBuilder);
      Set<String> visited = Sets.newHashSet();
      for (SightingTaxon sightingTaxon : checklist.getTaxa(taxonomy)) {
        for (String id : sightingTaxon.getIds()) {
          Taxon taxon = taxonomy.getTaxon(id);
          if (taxon.getType() == Taxon.Type.species) {
            if (!visited.contains(id)) {
              visited.add(id);
              indexBuilder.visitTaxon(taxon);
            }
          } else {
            Taxon species = TaxonUtils.getParentOfType(taxon, Taxon.Type.species);
            if (!visited.contains(species.getId())) {
              visited.add(species.getId());
              indexBuilder.visitTaxon(species);
            }
          }

          Taxon genus = TaxonUtils.getParentOfType(taxon, Taxon.Type.genus);
          if (!visited.contains(genus.getId())) {
            visited.add(genus.getId());
            indexBuilder.visitTaxon(genus);
          }
        }
      }
    }

    /** Look for a group/subspecies match within a genus (since the species couldn't be found). */
    static Taxon findMatchWithin(final Taxon parent, final String name, boolean preferGroups) {
      final List<Taxon> matches = Lists.newArrayList();
      // Do exact match first
      TaxonUtils.visitTaxa(parent, taxon -> {
        if (taxon != parent && name.equals(taxon.getName())) {
          matches.add(taxon);
          return false;
        }

        return true;
      });

      // Then do close-enough match. Note: do not do both exact and close-enough in a single pass
      // as that causes confusion when names are close, e.g. Cinclodes albiventris and
      // albidiventris!
      if (matches.isEmpty()) {
        TaxonUtils.visitTaxa(parent, taxon -> {
          if (taxon != parent && equalsOrClose(name, taxon.getName())) {
            matches.add(taxon);
            return false;
          }

          return true;
        });
      }

      // Only proceed when there's exactly one match.
      if (matches.size() != 1) {
        return null;
      }

      Taxon match = matches.get(0);
      // Prefer groups over subspecies. This pure heuristic assumes that when
      // species get lumped, and a sighting was only at the species level, it's probable that
      // the sighting wasn't identified to species.
      if (preferGroups && (match.getType() == Type.subspecies)
          && (match.getParent().getType() == Type.group)) {
        match = match.getParent();
      }

      return match;
    }

    private static boolean equalsOrClose(String a, String b) {
      return (a.equals(b) || LevenshteinDistance.getDefaultInstance().apply(a, b) <= 2);
    }
  }

  /**
   * Remove whitespace, hyphens, quotes... just simpler. Characters that are in this list, but not
   * in the NOT_IN_WORD matcher above, will be dropped from "unusedText" if they're within a found
   * species name. (So, for example a species name like "(Madagascar) Little Grebe" might get found.
   */
  private static CharMatcher CHARS_TO_REMOVE_IN_NORMALIZATION =
      CharMatcher.anyOf("- \"'\u2019\u2010()[]");

  /**
   * Hack the names to eliminate some common alternate spellings, or odd punctuation. Largely only
   * helps in English.
   */
  private static String toLowerCaseAndMore(String s) {
    s = CHARS_TO_REMOVE_IN_NORMALIZATION.removeFrom(s);
    s = s.toLowerCase();
    s = s.replace("gray", "grey");
    s = s.replace("colour", "color");
    s = s.replace("racquet", "racket");
    s = s.replace("\u00f6", "oe");
    s = s.replace("\u00fc", "ue");
    s = s.replace('´', '\'');
    return s;
  }

  private static String removeEmptyLines(String string) {
    StringBuilder buffer = new StringBuilder(string.length());
    for (String line : Splitter.on('\n').split(string)) {
      if (!CharMatcher.whitespace().matchesAllOf(line)) {
        buffer.append(line);
        buffer.append('\n');
      }
    }
    // Remove a trailing '\n' if added above
    if (buffer.length() > 0 && buffer.charAt(buffer.length() - 1) == '\n') {
      return buffer.substring(0, buffer.length() - 1);
    }

    return buffer.toString();
  }

  /** Trim suffixes from alternate names. Basically just used for "- in part" suffixes. */
  private static String trimSuffixes(String alternate) {
    int indexOf = alternate.indexOf(" -");
    if (indexOf > 0) {
      return alternate.substring(0, indexOf);
    }
    return alternate;
  }
}
