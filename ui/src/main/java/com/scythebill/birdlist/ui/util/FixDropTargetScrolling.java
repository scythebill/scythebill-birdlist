/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TooManyListenersException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

/**
 * Works around lousy auto-scrolling during drag-and-drop on MacOS.
 */
public class FixDropTargetScrolling {
  private static final Logger logger = Logger.getLogger(FixDropTargetScrolling.class.getName());
  
  public static void install(Component c, Insets insets) {
    // Only bother on MacOS;  the behavior on Windows is good out-of-the-box.
    if (UIUtils.isMacOS()) {
      try {
        c.getDropTarget().addDropTargetListener(new Listener(c, insets));
      } catch (TooManyListenersException e) {
        logger.log(Level.WARNING, "Could not install drop-target scrolling", e);
      }
    }
  }
  
  static class Listener implements DropTargetListener, ActionListener {
    private final Component component;
    private final Insets insets;
    private Timer timer = new Timer(100, this);
    private Point cursorLocation;
    private int count;
    
    Listener(Component component, Insets insets) {
      this.component = component;
      this.insets = insets;
    }
    
    @Override
    public void dragEnter(DropTargetDragEvent e) {
      cursorLocation = e.getLocation();
      timer.start();
      count = 0;
    }

    @Override
    public void dragExit(DropTargetEvent e) {
      timer.stop();
      cursorLocation = null;
    }

    @Override
    public void dragOver(DropTargetDragEvent e) {
      cursorLocation = e.getLocation();
    }

    @Override
    public void drop(DropTargetDropEvent e) {
      timer.stop();
    }

    @Override
    public void dropActionChanged(DropTargetDragEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      if (cursorLocation == null) {
        count = 0;
        return;
      }
      
      JViewport viewport = getViewport();
      Point viewPosition = viewport.getViewPosition();
      Dimension extentSize = viewport.getExtentSize();
      
      // Translate the cursor location to viewport relative
      int viewportCursorX = cursorLocation.x - viewPosition.x;
      int viewportCursorY = cursorLocation.y - viewPosition.y;
      
      int scrollX = 0;
      int scrollY = 0;
      
      int multiplier;
      if (count >= 20) {
        multiplier = 4;
      } else if (count >= 10) {
        multiplier = 2;
      } else {
        multiplier = 1;
      }
      Dimension size = component.getSize();
      if (viewportCursorX < insets.left) {
        // On the left edge
        scrollX = multiplier * -insets.left;
      } else if (viewportCursorX > extentSize.width - insets.right) {
        // On the right edge
        scrollX = multiplier * insets.right;
      }
      
      if (viewportCursorY < insets.top) {
        // On the top edge
        scrollY = multiplier * -insets.top;
      } else if (viewportCursorY > extentSize.height - insets.bottom) {
        // On the bottom edge
        scrollY = multiplier * insets.bottom;
      }
      
      if (scrollX == 0 && scrollY == 0) {
        count = 0;
        return;
      }

      Point newViewPosition = new Point(viewPosition); 

      newViewPosition.x = newViewPosition.x + scrollX;
      newViewPosition.x = Math.max(0,  newViewPosition.x);
      newViewPosition.x = Math.min(newViewPosition.x, size.width - extentSize.width);
      
      newViewPosition.y = newViewPosition.y + scrollY;
      newViewPosition.y = Math.max(0,  newViewPosition.y);
      newViewPosition.y = Math.min(newViewPosition.y, size.height- extentSize.height);

      if (!newViewPosition.equals(viewPosition)) {
        viewport.setViewPosition(newViewPosition);
        // Hack the cursor location:  if the mouse holds steady, there won't be 
        // a dragOver invocation.
        cursorLocation.x = cursorLocation.x + (newViewPosition.x - viewPosition.x);
        cursorLocation.y = cursorLocation.y + (newViewPosition.y - viewPosition.y);
        count++;
      } else {
        count = 0;
      }
    }
    

    private JViewport getViewport() {
      return (JViewport) SwingUtilities.getAncestorOfClass(JViewport.class, component);
    }
  }
}
