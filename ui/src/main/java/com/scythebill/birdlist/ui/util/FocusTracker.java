/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JRootPane;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.text.JTextComponent;

/**
 * Tracks focus within a component tree, support a single "focusOwner" property.
 */
public abstract class FocusTracker {
  private final JComponent component;
  private final PropertyChangeListener focusChangeListener = new FocusListener();
  private Component lastFocused = null;

  protected FocusTracker(JComponent component) {
    this.component = component;
    component.addAncestorListener(new AncestorListener() {
      @Override public void ancestorAdded(AncestorEvent event) {
        installFocus();
      }

      @Override public void ancestorMoved(AncestorEvent event) {
      }

      @Override public void ancestorRemoved(AncestorEvent event) {
        uninstallFocus();
      }
      
    });

    if (component.isShowing()) {
      installFocus();
    }
  }
  
  abstract protected void focusGained(Component child);

  abstract protected void focusLost(Component child);

  private void installFocus() {
    KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
    manager.addPropertyChangeListener("focusOwner", focusChangeListener);
    focusChangeListener.propertyChange(
        new PropertyChangeEvent(manager, "focusOwner", null, manager.getFocusOwner()));
  }

  private class FocusListener implements PropertyChangeListener {

    @Override public void propertyChange(PropertyChangeEvent event) {
      Component oldOwner = lastFocused != null ? lastFocused : (Component) event.getOldValue();
      Component newOwner = (Component) event.getNewValue();
      // Ignore some focus transitions
      if (shouldIgnoreLostFocus(newOwner)) {
        return;
      }
      
      boolean hadFocus = isTargetOfTrackerInternal(oldOwner);
      boolean hasFocus = isTargetOfTrackerInternal(newOwner);
      if (hasFocus) {
        focusGained(newOwner);
        lastFocused = newOwner;
      } else if (hadFocus) {
        focusLost(oldOwner);
        lastFocused = null;
      }
    }

    private boolean shouldIgnoreLostFocus(Component newOwner) {
      if (newOwner == null
          || newOwner instanceof JRootPane) {
        return true;
      }
      
      while (newOwner != null) {
        if (newOwner instanceof JMenuBar || newOwner instanceof JMenu) {
          return true;
        }
        newOwner = newOwner.getParent();
      }
      return false;
    }
  }
  
  private boolean isTargetOfTrackerInternal(Component potentialTarget) {
    if (!includeTextComponents()
        && potentialTarget instanceof JTextComponent) {
      return false;
    }
    
    return isTargetOfTracker(potentialTarget);
  }
  
  /**
   * Return true if this component is a relevant target of the tracker.  By default
   * considers all children of the tracked root relevant.
   */
  protected boolean isTargetOfTracker(Component potentialTarget) {
    return potentialTarget != null
        && (component == potentialTarget
            || component.isAncestorOf(potentialTarget));
  }
  
  /**
   * If false, focus into text components will be ignored.
   */
  protected boolean includeTextComponents() {
    return false;
  }

  private void uninstallFocus() {
    KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
    manager.removePropertyChangeListener("focusOwner", focusChangeListener);
    focusChangeListener.propertyChange(
        new PropertyChangeEvent(manager, "focusOwner", manager.getFocusOwner(), null));
  }
}
