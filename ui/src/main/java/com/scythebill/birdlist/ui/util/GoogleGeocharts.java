/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;

/**
 * Useful constants for Google's Geocharts visualization.
 */
public class GoogleGeocharts {
  /** eBird Locations not supported by Google. */
  public final static ImmutableMap<String, String> EBIRD_LOCATIONS_NOT_IN_GOOGLE = ImmutableBiMap.of(
      "UM-71", "US-HI",
      "BQ-BO", "BQ");

  /** Location IDs that are supported in both eBird and Google, but with different codes. */ 
  public final static ImmutableBiMap<String, String> EBIRD_TO_GOOGLE_COUNTRIES = ImmutableBiMap.of(
      "FI-01", "AX");
  
  public final static ImmutableMap<String, String> EBIRD_STATE_TO_GOOGLE_STATE =
      ImmutableMap.<String, String>builder()
      .put("PH-ABR","PH-15")
      .put("PH-AGN","PH-13")
      .put("PH-AGS","PH-13")
      .put("PH-AKL","PH-06")
      .put("PH-ALB","PH-05")
      .put("PH-ANT","PH-06")
      .put("PH-APA","PH-15")
      .put("PH-AUR","PH-03")
      .put("PH-BAS","PH-09")
      .put("PH-BAN","PH-03")
      .put("PH-BTN","PH-02")
      .put("PH-BTG","PH-40")
      .put("PH-BEN","PH-15")
      .put("PH-BIL","PH-08")
      .put("PH-BOH","PH-07")
      .put("PH-BUK","PH-10")
      .put("PH-BUL","PH-03")
      .put("PH-CAG","PH-02")
      .put("PH-CAN","PH-05")
      .put("PH-CAS","PH-05")
      .put("PH-CAM","PH-10")
      .put("PH-CAP","PH-06")
      .put("PH-CAT","PH-05")
      .put("PH-CAV","PH-40")
      .put("PH-CEB","PH-07")
      .put("PH-COM","PH-11")
      .put("PH-NCO","PH-12")
      .put("PH-DAV","PH-11")
      .put("PH-DAS","PH-11")
      .put("PH-DVO","PH-11")
      .put("PH-DAO","PH-11")
      .put("PH-DIN","PH-13")
      .put("PH-EAS","PH-08")
      .put("PH-GUI","PH-06")
      .put("PH-IFU","PH-15")
      .put("PH-ILN","PH-01")
      .put("PH-ILS","PH-01")
      .put("PH-ILI","PH-06")
      .put("PH-ISA","PH-02")
      .put("PH-KAL","PH-15")
      .put("PH-LUN","PH-01")
      .put("PH-LAG","PH-40")
      .put("PH-LAN","PH-12")
      .put("PH-LAS","PH-14")
      .put("PH-LEY","PH-08")
      .put("PH-MAG","PH-14")
      .put("PH-MAD","PH-41")
      .put("PH-MAS","PH-05")
      .put("PH-MDC","PH-41")
      .put("PH-MDR","PH-41")
      .put("PH-MSC","PH-10")
      .put("PH-MSR","PH-10")
      .put("PH-MOU","PH-15")
      .put("PH-NEC","PH-06")
      .put("PH-NER","PH-07")
      .put("PH-NSA","PH-08")
      .put("PH-NUE","PH-03")
      .put("PH-NUV","PH-02")
      .put("PH-PLW","PH-41")
      .put("PH-PAM","PH-03")
      .put("PH-PAN","PH-01")
      .put("PH-QUE","PH-40")
      .put("PH-QUI","PH-02")
      .put("PH-RIZ","PH-40")
      .put("PH-ROM","PH-41")
      .put("PH-WSA","PH-08")
      .put("PH-SAR","PH-11")
      .put("PH-SIG","PH-07")
      .put("PH-SOR","PH-05")
      .put("PH-SCO","PH-11")
      .put("PH-SLE","PH-08")
      .put("PH-SUK","PH-12")
      .put("PH-SLU","PH-14")
      .put("PH-SUN","PH-13")
      .put("PH-SUR","PH-13")
      .put("PH-TAR","PH-03")
      .put("PH-TAW","PH-14")
      .put("PH-ZMB","PH-03")
      .put("PH-ZAN","PH-09")
      .put("PH-ZAS","PH-09")
      .put("PH-ZSI","PH-09")
      .build();
  
  private final static ImmutableMap<String, String> GOOGLE_LOCATION_NAMES =
      ImmutableMap.<String, String>builder()
      .put("PH-14", "Autonomous Region in Muslim Mindanao")
      .put("PH-05", "Bicol")
      .put("PH-02", "Cagayan Valley")
      .put("PH-40", "Calabarzon")
      .put("PH-13", "Caraga")
      .put("PH-03", "Central Luzon")
      .put("PH-07", "Central Visayas")
      .put("PH-15", "Cordillera Administrative Region")
      .put("PH-11", "Davao")
      .put("PH-08", "Eastern Visayas")
      .put("PH-01", "Ilocos")
      .put("PH-41", "Mimaropa")
      .put("PH-00", "National Capital Region")
      .put("PH-10", "Northern Mindanao")
      .put("PH-12", "Soccsksargen Rehiyon")
      .put("PH-06", "Western Visayas")
      .put("PH-09", "Zamboanga Peninsula")
      .build();
  
  public static class Region {
    final String name;
    final String id;

    Region(String name, String id) {
      this.name = name;
      this.id = id;
    }
   
    public String id() {
      return id;
    }
    
    public String asOption() {
      return String.format("<option value=\"?%s\">%s</option>", id, name);
    }
  }
  
  public static class Subcontinent extends Region {
    final ImmutableSet<String> countries;

    Subcontinent(String name, String id, ImmutableSet<String> countries) {
      super("&nbsp;&nbsp;" + name, id);
      this.countries = countries;
    }
    
    public boolean containsAnyOf(Set<String> countries) {
      return !Sets.intersection(countries, this.countries).isEmpty();
    }

    public boolean contains(String country) {
      return countries.contains(country);
    }
}
  
  public static class Continent extends Region {
    final ImmutableSet<Subcontinent> subcontinents;

    Continent(String name, String id, ImmutableSet<Subcontinent> subcontinents) {
      super(name, id);
      this.subcontinents = subcontinents;
    }
    
    boolean containsAnyOf(Set<Subcontinent> subcontinents) {
      return !Sets.intersection(subcontinents, this.subcontinents).isEmpty();
    }

    public ImmutableSet<Subcontinent> subcontinents() {
      return subcontinents;
    }
  }
  
  private final LocationSet locations;
  private final PredefinedLocations predefinedLocations;

  public GoogleGeocharts(LocationSet locations, PredefinedLocations predefinedLocations) {
    this.locations = locations;
    this.predefinedLocations = predefinedLocations;
  }
  
  public String getNameForGoogle(String ebirdCode) {
    if (GOOGLE_LOCATION_NAMES.containsKey(ebirdCode)) {
      return GOOGLE_LOCATION_NAMES.get(ebirdCode);
    }
    // Map back to eBird if necessary
    if (EBIRD_TO_GOOGLE_COUNTRIES.containsValue(ebirdCode)) {
      ebirdCode = EBIRD_TO_GOOGLE_COUNTRIES.inverse().get(ebirdCode);
    }
    Location location = locations.getLocationByCode(ebirdCode);
    if (location != null) {
      return location.getDisplayName();
    } else {
      Collection<Location> possibleLocations;
      int hyphen = ebirdCode.indexOf('-');
      if (hyphen > 0) {
        String justCountry = ebirdCode.substring(0, hyphen);
        possibleLocations = locations.getLocationsByCode(justCountry);
      } else if (STATE_TO_COUNTRY_CODE.containsKey(ebirdCode)) {
        String actualCountry = STATE_TO_COUNTRY_CODE.get(ebirdCode);
        possibleLocations = locations.getLocationsByCode(actualCountry);
      } else {
        possibleLocations = ImmutableList.of();
      }
      
      for (Location possibleLocation : possibleLocations) {
        PredefinedLocation predefinedChild = predefinedLocations.getPredefinedLocationChildByCode(possibleLocation, ebirdCode);
        if (predefinedChild != null) {
          return predefinedChild.getName();
        }
      }
    }
    
    return ebirdCode;
  }

  public ImmutableSet<Subcontinent> subcontinents(Set<String> countries) {
    ImmutableSet.Builder<Subcontinent> builder = ImmutableSet.builder();
    for (Subcontinent subcontinent : ALL_SUBCONTINENTS) {
      if (subcontinent.containsAnyOf(countries)) {
        builder.add(subcontinent);
      }
    }
    return builder.build();
  }

  public ImmutableSet<Continent> continents(Set<Subcontinent> subcontinents) {
    ImmutableSet.Builder<Continent> builder = ImmutableSet.builder();
    for (Continent continent : ALL_CONTINENTS) {
      if (continent.containsAnyOf(subcontinents)) {
        builder.add(continent);
      }
    }
    return builder.build();
  }

  private static Subcontinent subcontinent(String name, String id, String... countries) {
    Subcontinent subcontinent = new Subcontinent(name, id, ImmutableSet.copyOf(countries));
    ALL_SUBCONTINENTS.add(subcontinent);
    return subcontinent;
  }
  
  private static Continent continent(String name, String id, Subcontinent... subcontinents) {
    Continent continent = new Continent(name, id, ImmutableSet.copyOf(subcontinents));
    ALL_CONTINENTS.add(continent);
    return continent;
  }  

  private final static ImmutableMap<String, String> STATE_TO_COUNTRY_CODE = ImmutableMap.of(
      "CW", "BQ");

  private final static ImmutableSet<String> SMALL_COUNTRIES = ImmutableSet.of(
      "PF",
      "CV",
      "CP",
      "CX",
      "CC",
      "AC",
      "CK",
      "FJ",
      "TF",
      "HM",
      "KI",
      "LI",
      "MH",
      "MU",
      "YT",
      "FM",
      "NR",
      "NU",
      "NF",
      "PN",
      "RE",
      "SC",
      "WS",
      "TO",
      "TK",
      "TV",
      "GS",
      "SH",
      "WF",
      "VU",
      "AD",
      "SM",
      "HK",
      "SG",
      "AQ",
      "BH",
      "BM",
      "AS",
      "KM",
      "SC",
      "GU",
      "PW",
      "MP",
      "TL",
      "IC",
      "TC",
      "FO");
      
  private final static List<Subcontinent> ALL_SUBCONTINENTS = new ArrayList<Subcontinent>();
  private final static List<Continent> ALL_CONTINENTS = new ArrayList<Continent>();

  private final static Subcontinent NORTHERN_AFRICA_015 =
      subcontinent("Northern Africa", "015", "DZ", "EG", "EH", "LY", "MA", "SD", "SS", "TN");
  private final static Subcontinent WESTERN_AFRICA_011 =
      subcontinent("Western Africa", "011","BF", "BJ", "CI", "CV", "GH", "GM", "GN", "GW", "LR", "ML", "MR", "NE", "NG", "SL", "SN", "TG");
  private final static Subcontinent MIDDLE_AFRICA_017 =
      subcontinent("Middle Africa", "017","AO", "CD", "ZR", "CF", "CG", "CM", "GA", "GQ", "ST", "TD");
  private final static Subcontinent EASTERN_AFRICA_014 =
      subcontinent("Eastern Africa", "014","BI", "DJ", "ER", "ET", "KE", "KM", "MG", "MU", "MW", "MZ", "RE", "RW", "SC", "SO", "TZ", "UG", "YT", "ZM", "ZW");
  private final static Subcontinent SOUTHERN_AFRICA_018 =
      subcontinent("Southern Africa", "018","BW", "LS", "NA", "SZ", "ZA");

  private final static Subcontinent NORTHERN_EUROPE_154 =
      subcontinent("Northern Europe", "154","GG", "JE", "AX", "DK", "EE", "FI", "FO", "GB", "IE", "IM", "IS", "LT", "LV", "NO", "SE", "SJ");
  private final static Subcontinent WESTERN_EUROPE_155 =
      subcontinent("Western Europe", "155","AT", "BE", "CH", "DE", "DD", "FR", "FX", "LI", "LU", "MC", "NL");
  private final static Subcontinent EASTERN_EUROPE_151 =
      subcontinent("Eastern Europe", "151","BG", "BY", "CZ", "HU", "MD", "PL", "RO", "RU", "SU", "SK", "UA");
  private final static Subcontinent SOUTHERN_EUROPE_039 =
      subcontinent("Southern Europe", "039","AD", "AL", "BA", "ES", "GI", "GR", "HR", "IT", "ME", "MK", "MT", "RS", "PT", "SI", "SM", "VA", "YU");

  private final static Subcontinent NORTHERN_AMERICA_021 =
      subcontinent("North America", "021","BM", "CA", "GL", "PM", "US");
  private final static Subcontinent CARIBBEAN_029 =
      subcontinent("Caribbean", "029","AG", "AI", "AW", "BB", "BL", "BQ", "BS", "CU", "DM", "DO", "GD", "GP", "HT", "JM", "KN", "KY", "LC", "MF", "MQ", "MS", "PR", "TC", "TT", "VC", "VG", "VI");
  private final static Subcontinent CENTRAL_AMERICA_013 =
      subcontinent("Central America", "013","BZ", "CR", "GT", "HN", "MX", "NI", "PA", "SV");
  private final static Subcontinent SOUTH_AMERICA_005 =
      subcontinent("South America", "005","AR", "BO", "BR", "CL", "CO", "EC", "FK", "GF", "GY", "PE", "PY", "SR", "UY", "VE");
  
  private final static Subcontinent CENTRAL_ASIA_143 =
      subcontinent("Central Asia", "143","TM", "TJ", "KG", "KZ", "RU", "UZ");
  private final static Subcontinent EASTERN_ASIA_030 =
      subcontinent("Eastern Asia", "030","CN", "HK", "JP", "KP", "KR", "MN", "MO", "TW");
  private final static Subcontinent SOUTHERN_ASIA_034 =
      subcontinent("Southern Asia", "034","AF", "BD", "BT", "IN", "IR", "LK", "MV", "NP", "PK");
  private final static Subcontinent SOUTHEASTERN_ASIA_035 =
      subcontinent("Southeastern Asia", "035","BN", "ID", "KH", "LA", "MM", "BU", "MY", "PH", "SG", "TH", "TL", "TP", "VN");
  private final static Subcontinent WESTERN_ASIA_145 =
      subcontinent("Western Asia", "145","AE", "AM", "AZ", "BH", "CY", "GE", "IL", "IQ", "JO", "KW", "LB", "OM", "PS", "QA", "SA", "NT", "SY", "TR", "YE", "YD");
  
  private final static Subcontinent AUSTRALIA_NEW_ZEALAND_053 =
      subcontinent("Australia and New Zealand", "053","AU", "NF", "NZ");
  private final static Subcontinent MELANESIA_054 =
      subcontinent("Melanesia", "054","FJ", "NC", "PG", "SB", "VU");
  private final static Subcontinent MICRONESIA_057 =
      subcontinent("Micronesia", "057","FM", "GU", "KI", "MH", "MP", "NR", "PW");
  private final static Subcontinent POLYNESIA_061 =
      subcontinent("Polynesia", "061","AS", "CK", "NU", "PF", "PN", "TK", "TO", "TV", "WF", "WS");
  
  static {
    continent("Africa", "002", NORTHERN_AFRICA_015, WESTERN_AFRICA_011, MIDDLE_AFRICA_017, EASTERN_AFRICA_014, SOUTHERN_AFRICA_018);
    continent("Europe", "150", NORTHERN_EUROPE_154, WESTERN_EUROPE_155, EASTERN_EUROPE_151, SOUTHERN_EUROPE_039);
    continent("Americas", "019", NORTHERN_AMERICA_021, CARIBBEAN_029, CENTRAL_AMERICA_013, SOUTH_AMERICA_005);
    continent("Asia", "142", CENTRAL_ASIA_143, EASTERN_ASIA_030, SOUTHERN_ASIA_034, SOUTHEASTERN_ASIA_035, WESTERN_ASIA_145);
    continent("Oceania", "009", AUSTRALIA_NEW_ZEALAND_053, MELANESIA_054, MICRONESIA_057, POLYNESIA_061);
  }
}
