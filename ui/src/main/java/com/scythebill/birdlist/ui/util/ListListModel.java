/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import com.google.common.collect.ForwardingList;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 * ListModel that wraps a mutable List.  Modifications
 * to the list should not be made directly, but must be
 * made through the List returned by {@link #asList}.
 * 
 * @param T type of the list entries
 */
public class ListListModel<T> extends AbstractListModel<T> {

  private List<T> _list;

  static public <T> ListListModel<T> newInstance() {
    return new ListListModel<T>();
  }
  
  static public <T> ListListModel<T> newInstance(List<T> list) {
    return new ListListModel<T>(list);
  }
  
  public ListListModel() {
    this(Lists.<T>newArrayList());
  }

  public ListListModel(List<T> list) {
    _list = new Forward(list);
  }

  /**
   * @return a modifiable List view of the list model.
   */
  public List<T> asList() {
    return _list;
  }

  @Override
  public int getSize() {
    return _list.size();
  }

  @Override
  public T getElementAt(int pos) {
    return _list.get(pos);
  }

  private class Forward extends ForwardingList<T> {
    
    private	 List<T> delegate;

    public Forward(List<T> list) {
      this.delegate = list;
    }
    

    @Override
    public void add(int index, T element) {
      super.add(index, element);
      fireIntervalAdded(this, index, index);
    }

    @Override
    public boolean addAll(int index,  Collection<? extends T> elements) {
      
      boolean retVal = false;
      int currentIndex = index;
      for (T element: elements) {
        super.add(currentIndex, element);
        currentIndex++;
        retVal = true;
      }
      
      if (retVal) {
        fireIntervalAdded(this, index, index + elements.size() - 1);
      }
      return retVal;
    }

    /* TODO(awiner): make unmodifiable 
    @Override
    public ListIterator<T> listIterator() {
    }

    @Override
    public ListIterator<T> listIterator( int index) {
    }
    */
    
    @Override
    public T remove(int index) {
      T retVal = super.remove(index);
      fireIntervalRemoved(this, index, index);
      return retVal;
    }

    @Override
    public T set(int index,  T element) {
      T retVal = super.set(index, element);
      fireContentsChanged(this, index, index);
      return retVal;
    }

    @Override
    public List<T> subList(int fromIndex,  int toIndex) {
      throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Iterator<T> iterator() {
      return Iterators.unmodifiableIterator(super.iterator());
    }

    @Override
    public boolean add(T element) {
      add(0, element);
      return true;
    }

    @Override
    public boolean remove(Object element) {
      int index = indexOf(element);
      if (index < 0) {
        return false;
      }
      
      remove(index);
      return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
      return addAll(0, collection);
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
      boolean retVal = false;
      for (Object o : collection) {
        retVal = remove(o) || retVal;
      }
      
      return retVal;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
      throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void clear() {
      int size = size();
      super.clear();
      
      if (size > 0) {
        fireIntervalRemoved(this, 0, size - 1);
      }
    }


    @Override
    protected List<T> delegate() {
      return delegate;
    }
  }
}
