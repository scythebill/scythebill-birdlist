/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.util.Collection;

import javax.annotation.Nullable;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMultimap;
import com.scythebill.birdlist.model.query.SyntheticLocation;
import com.scythebill.birdlist.model.query.SyntheticLocations;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.util.Indexer;
import com.scythebill.birdlist.model.util.ToString;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Converts a location to a string.
 */
public class LocationIdToString implements ToString<String> {
  private boolean verbose;
  private final LocationSet locations;
  private Location verboseRoot;
  private final SyntheticLocations syntheticLocations;
  private String nullName = Messages.getMessage(Name.WORLD_TEXT);

  public static final ImmutableMultimap<String, String> ALTERNATE_INDEX_ENTRIES;
  static {
    ImmutableMultimap.Builder<String, String> builder = ImmutableMultimap.builder();
    builder.put("Mount", "Mt");
    builder.put("Mountain", "Mt");
    builder.put("Mt", "Mount");
    builder.put("Fort", "Ft");
    builder.put("Ft", "Fort");
    builder.put("St", "Saint");
    builder.put("Ste", "Sainte");
    builder.put("Saint", "St");
    ALTERNATE_INDEX_ENTRIES = builder.build();
  }
    
  public LocationIdToString(LocationSet locations,
      SyntheticLocations syntheticLocations) {
    this.locations = locations;
    this.syntheticLocations = syntheticLocations;
  }

  public void setVerboseRoot(Location verboseRoot) {
    this.verboseRoot = verboseRoot;
  }
  
  public void setNullName(String nullName) {
    this.nullName = nullName;
  }
  
  public boolean isVerbose() {
    return verbose;
  }

  public void setVerbose(boolean verbose) {
    this.verbose = verbose;
  }
  
  public Indexer<String> createIndexer(boolean addNullName) {
    return createIndexer(addNullName, Predicates.alwaysTrue());
  }

  public Indexer<String> createIndexer(boolean addNullName, Predicate<Location> locationPredicate) {
    Indexer<String> index = new Indexer<String>();
    index.setAlternateIndexEntries(ALTERNATE_INDEX_ENTRIES);
    for (Location loc : locations.rootLocations()) {
      addToLocationIndex(index, loc, locationPredicate);
    }
    
    if (addNullName) {
      index.add(nullName, null);
    }

    if (syntheticLocations != null) {
      registerSyntheticLocations(index, syntheticLocations);
    }
    return index;
  }

  @Override public String getString(String id) {
    String syntheticName = getSyntheticName(id);
    if (syntheticName != null) {
      return syntheticName;
    }
    
    Location loc = locations.getLocation(id);
    return loc.getDisplayName();
  }
  

  @Override public String getPreviewString(String id) {
    String syntheticName = getSyntheticName(id);
    if (syntheticName != null) {
      return syntheticName;
    }
    
    return getString(locations, id, isVerbose(), verboseRoot);
  }

  /** Return the name of the location if synthetic, null otherwise. */
  @Nullable
  private String getSyntheticName(String id) {
    if (id == null)
      return nullName;

    if (syntheticLocations != null) {
      SyntheticLocation synthetic = syntheticLocations.byId(id);
      if (synthetic != null) {
        return synthetic.getDisplayName();
      }
    }
    
    return null;
  }

  static public String getString(LocationSet locations, String id) {
    return getString(locations, id, false, null);
  }

  static public String getString(LocationSet locations, String id, boolean verbose, Location verboseRoot) {
    if (id == null)
      return Messages.getMessage(Name.WORLD_TEXT);

    Location loc = locations.getLocation(id);
    
    if (loc == verboseRoot) {
      // Short-circuit if we're displaying only one location
      return locationNameAtStart(locations, loc, /*standalone=*/true);
    }
    
    if (verbose) {
      StringBuilder builder = new StringBuilder();
      while (loc != verboseRoot && loc != null) {
        if (builder.length() > 0) {
          builder.append(", ");
        }
        
        boolean isLastEntry = loc.getType() == Location.Type.country;
        
        boolean usefulEbirdCode = loc.getEbirdCode() != null
            && !loc.getEbirdCode().contains("-");
        
        // For intermediate steps, don't bother 
        if (builder.length() > 0
            && !isLastEntry
            && usefulEbirdCode) {
          // TODO: only for states?
          builder.append(loc.getEbirdCode());
        } else {
          if (builder.length() == 0) {
            builder.append(locationNameAtStart(locations, loc, /*standalone=*/false));
          } else {
            builder.append(getNameForLocationInFullAncestorList(loc));
          }
        }
        
        // Don't bother with finer-grained details than country 
        if (isLastEntry) {
          break;
        }
        
        Location parent = loc.getParent();
        // Skip repeated names
        if (parent != null 
            && loc.getDisplayName().equals(parent.getDisplayName())) {
          parent = parent.getParent();
        }
        loc = parent;
      }
      
      return builder.toString();
    } else {
      return getNonVerboseString(locations, loc, /*standalone=*/true);
    }
  }

  private static String locationNameAtStart(LocationSet locations, Location loc,
      boolean standalone) {
    // For the first entry, use the non-verbose logic
    // For "county" at the start, always append a type
    if (loc.getType() == Location.Type.county) {
      return getStringWithType(loc);
    } else {
      return getNonVerboseString(locations, loc, standalone);
    }
  }

  private static String getNonVerboseString(LocationSet locations, Location loc,
      boolean standalone) {
    // Handle multiple locations with the exact same name. 
    Collection<Location> locationsWithSameName = locations.getLocationsByDisplayName(loc.getDisplayName());
    if (locationsWithSameName.size() == 1) {
      return standalone ? loc.getDisplayName() : getNameForLocationInFullAncestorList(loc);
    }
    
    // See if the type is enough (e.g. "San Francisco" is both
    // a city and a county)
    boolean canBeDisambiguatedByType = true;
    for (Location locationWithSameName : locationsWithSameName) {
      if (locationWithSameName != loc &&
          locationWithSameName.getType() == loc.getType()) {
        canBeDisambiguatedByType = false;
      }
    }
    
    if (canBeDisambiguatedByType || loc.getParent() == null) {
      return getStringWithType(loc);
    } else {
      // Type isn't enough - use the parent name.  Typically,
      // this will be sufficient
      return standalone
          ? loc.getDisplayName() + " (" + loc.getParent().getDisplayName() + ")"
          : getNameForLocationInFullAncestorList(loc) + " (" + getNameForLocationInFullAncestorList(loc.getParent()) + ")"; 
    }
  }

  /**
   * Returns a location name that 
   */
  private static String getNameForLocationInFullAncestorList(Location loc) {
    String displayName = loc.getDisplayName();
    String groupedLocationPrefix = Locations.getGroupedLocationPrefix(displayName);
    if (groupedLocationPrefix != null) {
      if (loc.getParent() != null
          && loc.getParent().getDisplayName().equals(groupedLocationPrefix)) {
        String suffix = Locations.getGroupedLocationGroupSuffix(displayName);
        if (suffix != null) {
          return suffix;
        }
      }
    }
    
    return displayName;
  }

  public static String getStringWithType(Location loc) {
    String name = loc.getDisplayName();
    if (loc.getType() != null && loc.getType() != Location.Type.region) {
      name = String.format("%s (%s)", name, Messages.getText(loc.getType()));
    }

    return name;
  }

  private void addToLocationIndex(Indexer<String> index, Location loc, Predicate<Location> locationPredicate) {
    if (locationPredicate.apply(loc)) {
      index.add(loc.getDisplayName(), loc.getId());
      boolean sameDisplayName = loc.getDisplayName().equals(loc.getModelName());
      if (!sameDisplayName) {
        index.add(loc.getModelName(), loc.getId());
      }
      // Also index the combo of this location with its parent, for easier disambiguation
      if (loc.getParent() != null) {
        index.add(loc.getDisplayName() + " " + loc.getParent().getDisplayName(), loc.getId());
        if (!loc.getParent().getDisplayName().equals(loc.getParent().getModelName())) {
          index.add(loc.getDisplayName() + " " + loc.getParent().getModelName(), loc.getId());
          if (!sameDisplayName) {
            index.add(loc.getModelName() + " " + loc.getParent().getModelName(), loc.getId());
          }
        }
      }
    }
    
    for (Location child : loc.contents()) {
      addToLocationIndex(index, child, locationPredicate);
    }    
  }

  private void registerSyntheticLocations(Indexer<String> indexer, SyntheticLocations syntheticLocations) {
    for (SyntheticLocation location : syntheticLocations.locations()) {
      indexer.add(location.getDisplayName(), location.getId());
    }
    
  }
}
