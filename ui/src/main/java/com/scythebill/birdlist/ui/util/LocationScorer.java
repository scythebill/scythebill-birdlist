/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.util.List;

import javax.annotation.Nullable;

import org.joda.time.DateTimeFieldType;
import org.joda.time.ReadablePartial;

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;
import com.google.inject.Provider;
import com.scythebill.birdlist.model.checklist.Checklist;
import com.scythebill.birdlist.model.checklist.Checklist.Status;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.util.Scorer.ScoreAccumulator;

/**
 * Scorer for ranking species by how near and often they've been seen to
 * a "current" location.
 * <p> 
 * For every level of location closer to the current one,
 * assign 10 times as many points.  E.g., 1 point for each sighting in the
 * same region (e.g. North America), 10 points for those in the same country
 * (e.g. United States), 100 for the same state (e.g. California), etc.
 * <p>
 * In addition, a second multiplier is applied for how near to the current
 * time the species has been seen (so that wintering birds aren't suggested
 * in the summertime and vice versa).
 */
public class LocationScorer {
  /**
   * Random string used to mark checklist rarities so they are lower score.
   * Contents don't even matter, since instance equality is sufficient.
   */
  private static String RARITY_MARKER = "#%!(\\|";

  public static Scorer newScorer(
      Taxonomy taxonomy,
      ReportSet reportSet,
      Location currentLocation,
      @Nullable ReadablePartial date,
      Checklists checklists) {
    
    // Create a dummy "ReportSet" based on species found in the checklists
    ReportSet checklistReports = createChecklistReports(
        taxonomy,
        reportSet, 
        currentLocation,
        date,
        checklists);
    return new Scorer(taxonomy, ImmutableList.of(checklistReports, reportSet),
        new LocationScoreAccumulatorFactory(
            reportSet.getLocations(),
            currentLocation,
            date));
  }

  /**
   * Create a "ReportSet" as if everything on the relevant checklists was seen once,
   * claiming the date of entry as the day.  Effectively, this gives a small but meaningful
   * scoring bump.
   * <p>
   * An initial attempt used dateless sightings.  However, the date boost is sufficient that this
   * meant that sightings from the right month in a related (but different) location 
   * (say, New Zealand relative to Fiji) were boosted over the checklist.  That went poorly. 
   */
  private static ReportSet createChecklistReports(Taxonomy taxonomy, ReportSet reportSet,
      Location currentLocation, @Nullable ReadablePartial date, Checklists checklists) {
    List<Sighting> checklistSightings = Lists.newArrayList();
    Location checklistLocation = currentLocation;
    while (checklistLocation != null) {
      Checklist checklist = checklists.getChecklist(reportSet, taxonomy, checklistLocation);
      if (checklist != null) {
        addSightingsSimulatingChecklist(
            checklistSightings,
            checklist,
            taxonomy,
            date,
            checklistLocation);
      }
      
      checklistLocation = checklistLocation.getParent();
    };
    
    ReportSet checklistReports = new ReportSet(
        reportSet.getLocations(), checklistSightings, taxonomy);
    return checklistReports;
  }

  /** Adds sightings to the List based on the Checklist. */
  private static void addSightingsSimulatingChecklist(
      List<Sighting> checklistSightings,
      Checklist checklist,
      Taxonomy taxonomy,
      @Nullable ReadablePartial date,
      Location checklistLocation) {
    if (taxonomy instanceof MappedTaxonomy) {
      MappedTaxonomy mapped = (MappedTaxonomy) taxonomy;
      for (SightingTaxon taxon : checklist.getTaxa(taxonomy)) {
        // Skip rarities. 
        Status status = checklist.getStatus(taxonomy, taxon);
        boolean isRarity = status == Status.RARITY || status == Status.RARITY_FROM_INTRODUCED;
        
        SightingTaxon base = mapped.getMapping(taxon);
        if (base.getType() == SightingTaxon.Type.SINGLE
            || base.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
          Sighting sighting = Sighting.newBuilder()
              .setTaxonomy(mapped.getBaseTaxonomy())
              .setLocation(checklistLocation)
              .setTaxon(base)
              .setDate(date)
              .build();
          if (isRarity) {
            sighting.getSightingInfo().setDescription(RARITY_MARKER);
          }
          checklistSightings.add(sighting);
        } else {
          // "Sp" - give a bump for each part.
          for (String id : base.getIds()) {
            Sighting sighting = Sighting.newBuilder().setLocation(checklistLocation)
                .setTaxonomy(mapped.getBaseTaxonomy())
                .setDate(date)
                .setTaxon(SightingTaxons.newSightingTaxon(id))
                .build();
            if (isRarity) {
              sighting.getSightingInfo().setDescription(RARITY_MARKER);
            }
            checklistSightings.add(sighting);
          }
        }
      }
    } else {
      for (SightingTaxon taxon : checklist.getTaxa(taxonomy)) {
        // Skip rarities. (Ideally, would boost non-rarities and demote rarities.) 
        Status status = checklist.getStatus(taxonomy, taxon);
        boolean isRarity = status == Status.RARITY || status == Status.RARITY_FROM_INTRODUCED;
        Sighting sighting = Sighting.newBuilder()
            .setTaxonomy(taxonomy)
            .setLocation(checklistLocation)
            .setTaxon(taxon)
            .setDate(date)
            .build();
        if (isRarity) {
          sighting.getSightingInfo().setDescription(RARITY_MARKER);
        }
        checklistSightings.add(sighting);
      }
    }
  }

  static class LocationScoreAccumulatorFactory implements Provider<ScoreAccumulator> {
    private final LocationSet locationSet;
    private final ImmutableMap<String, Integer> bucketMap;
    private final int month;

    public LocationScoreAccumulatorFactory(
        LocationSet locationSet,
        Location currentLocation,
        @Nullable ReadablePartial time) {
      this.locationSet = locationSet;
      int currentDepth = getDepth(currentLocation);
      ImmutableMap.Builder<String, Integer> mapBuilder = ImmutableMap.builder();
      while (currentLocation != null) {
        mapBuilder.put(currentLocation.getId(), currentDepth - 1);
        currentDepth--;
        currentLocation = currentLocation.getParent();
      }
      bucketMap = mapBuilder.build();      
      
      if (time != null && time.isSupported(DateTimeFieldType.monthOfYear())) {
        month = time.get(DateTimeFieldType.monthOfYear());
      } else {
        month = -1;
      }
    }

    @Override
    public ScoreAccumulator get() {
      return new LocationScoreAccumulator();
    }
    
    class LocationScoreAccumulator implements ScoreAccumulator {
      private final int[] buckets = new int[bucketMap.size()];
      private final int[] dateMultiplierBuckets = new int[bucketMap.size()];
      private volatile int score = -1;
      
      @Override
      public String toString() {
        return MoreObjects.toStringHelper(this)
           .add("buckets", Ints.asList(buckets))
           .add("dateBuckets", Ints.asList(dateMultiplierBuckets))
           .add("score", reportScore())
           .toString();
      }

      @Override
      // TODO: synchronization would be a Good Thing, but probably not at this level.
      public void accumulate(Sighting sighting) {
        int bucket = getLocationBucket(sighting);
        if (bucket >= 0) {
          
          if (month != -1) {
            ReadablePartial sightingDate = sighting.getDateAsPartial();
            if (sightingDate != null
                && sightingDate.isSupported(DateTimeFieldType.monthOfYear())) {
              int sightingMonth = sightingDate.get(DateTimeFieldType.monthOfYear());
              int multiplier = 7 - Math.abs(sightingMonth - month);
              int existingDateMultiplier = dateMultiplierBuckets[bucket]; 
              if (existingDateMultiplier < multiplier) {
                dateMultiplierBuckets[bucket] = multiplier;
              }
            }
          }
          
          boolean isRarity = sighting.hasSightingInfo() && sighting.getSightingInfo().getDescription() == RARITY_MARKER;
          // Give rarities a significantly lower boost
          if (isRarity) {
            buckets[bucket]++;
          } else {
            buckets[bucket]+=3;
          }
        }
        
        score = -1;
      }

      /**
       * Return the scoring bucket to which a sighting should be added.
       * -1 is used for "no bucket, ignore".
       */
      private int getLocationBucket(Sighting sighting) {
        if (sighting.getLocationId() == null) {
          return -1;
        }
        
        Integer bucket = bucketMap.get(sighting.getLocationId());
        if (bucket != null) {
          return bucket;
        }
        
        Location location = locationSet.getLocation(sighting.getLocationId()).getParent();
        while (location != null) {
          bucket = bucketMap.get(location.getId());
          if (bucket != null) {
            return bucket;
          }
          location = location.getParent();
        }
        
        return -1;
      }
      
      @Override
      public int reportScore() {
        if (score > 0) {
          return score;
        }
        
        int total = 0;
        int baseMultiplier = 1;
        for (int i = 0; i < buckets.length; i++) {
          int bucketCount = buckets[i];
          // Max out at 9 sightings in a bucket
          if (bucketCount > 9) {
            bucketCount = 9;
          }
          int increment = baseMultiplier * bucketCount;
          // Only count date multipliers above 3 (basically, within 2 months of the current month).
          // And cube it.  So sightings in the same month give a bonus of 64.  In adjacent months,
          // 27.  And two months away, 8.
          int dateMultiplier = dateMultiplierBuckets[i] - 3;
          if (dateMultiplier > 1) {
            increment *= dateMultiplier * dateMultiplier * dateMultiplier;
          }
          total += increment;
          baseMultiplier *= 10;
        }
        score = total;
        return total;
      }
    }    
  }
  
  private static int getDepth(Location location) {
    int depth = 0;
    while (location != null) {
      location = location.getParent();
      depth++;
    }
    
    return depth;
  }
}
