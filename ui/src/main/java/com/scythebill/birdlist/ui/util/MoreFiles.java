/*   
 * Copyright 2023 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.google.common.collect.ImmutableList;

/**
 * Additional utilities dealing with Files.
 */
public class MoreFiles {
  private MoreFiles() {}
  
  public record CommonSuffixResults(ImmutableList<String> commonSuffix, File oldDirectory, File newDirectory) {

    public File replacePrefix(File file) {
      List<String> pathComponents = new ArrayList<>();
      // Record path components until what's left is the "old directory"
      while (file != null) {
        pathComponents.add(file.getName());
        file = file.getParentFile();
        if (Objects.equals(file, oldDirectory)) {
          break;
        }
      }
      
      // Start again with the newDirectory
      file = newDirectory;
      for (int i = pathComponents.size() - 1; i >= 0; i--) {
        if (file == null) {
          file = new File(pathComponents.get(i));
        } else {
          file = new File(file, pathComponents.get(i));
        }
      }
      
      return file;
    }
  }

  /**
   * For two directories, computes the common *suffix* for the two, and the remnant
   * bits of the parent directory structure.
   * <p>Example:
   * For two directories: /a/b/c/ and /a/d/b/c/, would return:
   * <ul>
   * <li>Suffix: [b,c]
   * <li>Remnant first: /a/
   * <li>Remnant second: /a/d/
   * </ul> 
   * There is no requirement that the files actually exist.
   */
  public static CommonSuffixResults commonSuffix(File oldDirectory, File newDirectory) {
    ImmutableList.Builder<String> commonSuffix = ImmutableList.builder();
    while (oldDirectory != null && newDirectory != null) {
      if (oldDirectory.getName().equals(newDirectory.getName())) {
        commonSuffix.add(oldDirectory.getName());
        oldDirectory = oldDirectory.getParentFile();
        newDirectory = newDirectory.getParentFile();
      } else {
        break;
      }
    }

    return new CommonSuffixResults(commonSuffix.build().reverse(), oldDirectory, newDirectory); 
    
  }

  public static boolean isChildOf(File possibleParent, File file) {
    file = file.getParentFile();
    while (file != null) {
      if (possibleParent.equals(file)) {
        return true;
      }
      file = file.getParentFile();
    };
    
    return false;
  }
}
