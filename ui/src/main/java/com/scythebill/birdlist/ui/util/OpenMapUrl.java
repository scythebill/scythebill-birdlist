/*   
 * Copyright 2024 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;

import com.google.inject.Inject;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;

/**
 * Opens lat-long coordinates to a browser map.
 */
public class OpenMapUrl {
  @Inject
  public OpenMapUrl() {
  }
  
  public void openLatLong(LatLongCoordinates latLong) throws IOException {
    String url = String.format("https://www.google.com/maps/search/?api=1&query=%s%%2C%s",
        latLong.latitudeAsCanonicalString(), latLong.longitudeAsCanonicalString());
    Desktop.getDesktop().browse(URI.create(url));
  }
}
