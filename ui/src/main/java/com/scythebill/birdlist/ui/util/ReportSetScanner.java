/*   
 * Copyright 2012 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.locks.ReadWriteLock;

import com.google.common.collect.ImmutableList;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.taxa.MappedTaxonomy;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Scans a report set to produce a result.
 */
public abstract class ReportSetScanner<T> implements Callable<T> {
  private static final int BLOCK_SIZE = 1000;
  private final List<Sighting> list;
  private final ReadWriteLock readWriteLock;
  private final MappedTaxonomy mappedTaxonomy;
  private final Taxonomy taxonomy;
  private final Taxonomy baseTaxonomy;

  protected ReportSetScanner(
      Taxonomy taxonomy,
      ReportSet reportSet) {
    this.taxonomy = taxonomy;
    this.baseTaxonomy = TaxonUtils.getBaseTaxonomy(taxonomy);
    this.mappedTaxonomy = taxonomy instanceof MappedTaxonomy
        ? (MappedTaxonomy) taxonomy : null;
    this.list = reportSet.getSightings();
    this.readWriteLock = reportSet.sightingsLock();
  }
  
  @Override public T call() throws Exception {
    try {
      readWriteLock.readLock().lockInterruptibly();
      int size;
      try {
        size = list.size();
      } finally {
        readWriteLock.readLock().unlock();
      }
      
      // Score blocks of the list.  This strategy could, in theory miss records (as they're added)
      // or even double-count records (if records get inserted at the beginning).
      // This will be rare, and overall irrelevant.  Bigger problems would come if:
      // - Records are deleted (could lead to out-of-bounds-exceptions in the first block)
      // - Records are mutated (until all Sighting changes are guarded by write locks)
      for (int i = (size / BLOCK_SIZE) * BLOCK_SIZE; i >= 0; i -= BLOCK_SIZE) {
        readWriteLock.readLock().lockInterruptibly();
        
        // Grab a copy of a sublist, releasing the lock as quickly as possible.
        ImmutableList<Sighting> sublist;
        try {
         sublist = ImmutableList.copyOf(list.subList(i, Math.min(size, i + BLOCK_SIZE)));
        } finally {
          readWriteLock.readLock().unlock();
        }
        
        for (Sighting entry : sublist) {
          // Skip incompatible sightings 
          if (entry.getTaxonomy() != baseTaxonomy) {
            processIncompatibleSighting(entry, entry.getTaxon());
          } else {
            SightingTaxon taxon = entry.getTaxon();
            if (resolveTaxaToTaxonomy()) {
              if (mappedTaxonomy != null) {
                taxon = mappedTaxonomy.resolveInto(taxon).getSightingTaxon();
              }
            }
            
            process(entry, taxon);
          }
        }
      }
    } catch (InterruptedException e) {
      // Done, by interruption.  Fine.
    }
    
    return accumulated();
  }

  abstract protected void process(Sighting sighting, SightingTaxon taxon) throws Exception;
  
  /** Called for taxa not compatible with the main taxonomy. By default, does nothing. */
  protected void processIncompatibleSighting(Sighting sighting, SightingTaxon taxon) throws Exception {
  }

  abstract protected T accumulated(); 
  
  protected boolean resolveTaxaToTaxonomy() {
    return true;
  }

  protected Taxonomy getTaxonomy() {
    return taxonomy;
  }
}
