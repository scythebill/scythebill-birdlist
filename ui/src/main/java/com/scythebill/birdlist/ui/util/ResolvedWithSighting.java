/*   
 * Copyright 2021 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.util.Objects;

import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.util.ResolvedComparator;

/**
 * Stores a resolved with a single associated sighting.  An occasional convenience.
 */
public class ResolvedWithSighting implements Comparable<ResolvedWithSighting> {
  private static final ResolvedComparator COMPARATOR = new ResolvedComparator();
  private final Sighting sighting;
  private final Resolved resolved;

  public ResolvedWithSighting(Resolved resolved, Sighting sighting) {
    this.sighting = sighting;
    this.resolved = resolved;
  }

  public Sighting getSighting() {
    return sighting;
  }

  public Resolved getResolved() {
    return resolved;
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    
    if (!(o instanceof ResolvedWithSighting)) {
      return false;
    }
    
    ResolvedWithSighting that = (ResolvedWithSighting) o;
    return resolved.equals(that.resolved) && sighting == that.sighting;
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(sighting, resolved);
  }
  
  @Override
  public int compareTo(ResolvedWithSighting o) {
    return COMPARATOR.compare(this.resolved, o.resolved);
  }    
}