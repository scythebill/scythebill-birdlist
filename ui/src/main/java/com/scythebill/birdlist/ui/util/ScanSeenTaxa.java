/*   
 * Copyright 2012 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;
import java.util.Set;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nullable;
import javax.swing.SwingUtilities;

import org.joda.time.DateTimeFieldType;
import org.joda.time.ReadablePartial;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.VisitInfoKey;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.user.User;

/**
 * Scans a report set in the background for a list of seen taxa.
 */
public class ScanSeenTaxa {
  private final static Logger logger = Logger.getLogger(
      ScanSeenTaxa.class.getName());
  
  private final ListenableFuture<ScannerResults> future;
  private volatile boolean done = false;
  private volatile ImmutableMap<Taxonomy, List<Set<String>>> seenTaxaForLocations = ImmutableMap.of();
  private volatile ImmutableList<Set<VisitInfoKey>> visitInfoKeysForLocations = ImmutableList.of();
  private volatile ImmutableMap<Taxonomy, Set<String>> yearIds = ImmutableMap.of();
  private volatile ImmutableMap<Taxonomy, Set<String>> photographedIds = ImmutableMap.of();
  private volatile ImmutableMap<Taxonomy, List<Set<String>>> seenTaxaForUsers = ImmutableMap.of();
  private final List<Location> locations;
  private final List<User> users;
  private final Options options;

  public static class Options implements Cloneable {
    public OptionalInt yearToScan = OptionalInt.empty();
    public ImmutableSet<User> users = ImmutableSet.of();
    public boolean includeAllTaxonomies = false;
    
    public Options setYearToScan(OptionalInt yearToScan) {
      this.yearToScan = yearToScan;
      return this;
    }

    public Options setUsers(Collection<User> users) {
      this.users = ImmutableSet.copyOf(users);
      return this;
    }

    public Options setIncludeAllTaxonomies(boolean includeAllTaxonomies) {
      this.includeAllTaxonomies = includeAllTaxonomies;
      return this;
    }

    @Override
    public Options clone() {
      try {
        return (Options) super.clone();
      } catch (CloneNotSupportedException e) {
        throw new AssertionError(e);
      }
    }
  }
  /**
   * Start scanning;  will call {@code whenReady} when the scan is complete.
   * 
   * @param locations a list of locations to scan, each of which must be an ancestor of the prior one
   * @param whenReady {@code Runnable} called on the Swing UI thread when scanning has successfully completed 
   */
  public static ScanSeenTaxa start(
      ReportSet reportSet,
      Taxonomy taxonomy,
      ListeningExecutorService executorService,
      Consumer<ScanSeenTaxa> whenReady,
      List<Location> locations,
      Predicate<Sighting> sightingsToInclude,
      Options options) {
    options = options.clone();
    Preconditions.checkArgument(Iterables.getLast(locations) == null);
    for (int i = 0; i < locations.size() - 2; i++) {
      Location location = locations.get(i);
      Location mustBeParent = locations.get(i + 1);
      Preconditions.checkArgument(Locations.isDescendentOfLocation(mustBeParent, location));
    }
    
    Scanner scanner = new Scanner(reportSet, taxonomy, locations, sightingsToInclude, options);
    ListenableFuture<ScannerResults> future = executorService
        .submit(scanner);
    
    return new ScanSeenTaxa(future, whenReady, locations, options);
  }
      
  ScanSeenTaxa(
      ListenableFuture<ScannerResults> future,
      Consumer<ScanSeenTaxa> whenReady,
      List<Location> locations,
      Options options) {
    this.future = future;
    this.options = options;
    // Can't be an immutable list - contains null
    this.locations = Collections.unmodifiableList(Lists.newArrayList(locations));
    // Convert to a List, which is more useful in the implementation.
    this.users = ImmutableList.copyOf(options.users);
    Futures.addCallback(future, new FutureCallback<ScannerResults>() {
      @Override public void onSuccess(ScannerResults result) {
        seenTaxaForLocations = result.taxaPerLocation;
        visitInfoKeysForLocations = result.visitInfoKeysPerLocation;
        yearIds = result.yearIds;
        photographedIds = result.photographedIds;
        seenTaxaForUsers = result.taxaPerUser;
        done = true;
        SwingUtilities.invokeLater(() -> whenReady.accept(ScanSeenTaxa.this));
      }

      @Override
      public void onFailure(Throwable t) {
        logger.log(Level.WARNING, "Could not scan taxa", t);
      }
    }, MoreExecutors.directExecutor());
  }
  
  public boolean isDone() {
    return done;
  }
  
  /** Cancel scanning, if it's still going. */
  public void cancel() {
    future.cancel(true);
  }
  
  /**
   * Get the list of seen taxa (or null if retrieval failed or is not complete yet).
   * 
   * @param location the location, or null for the world list
   */
  @Nullable
  public Set<String> getSeenTaxa(Taxonomy taxonomy, @Nullable Location location) {
    if (!isDone()) {
      return null;
    }
    if (!seenTaxaForLocations.containsKey(taxonomy)) {
      return ImmutableSet.of();
    }
    int index = locations.indexOf(location);
    Preconditions.checkArgument(index >= 0,
        "Location %s was not requested when scanning",
        location == null ? "null" : location.getModelName());
    
    return seenTaxaForLocations.get(taxonomy).get(index);
  }

  
  /**
   * Get the count of visits to a particular location (or any sublocations).
   * Returns zero if retrieval failed or is not yet complete.
   * Does not support a null location (world).
   */
  public int getVisitCount(Location location) {
    Preconditions.checkNotNull(location, "World visit count not supported");
    if (visitInfoKeysForLocations.isEmpty()) {
      return 0;
    }
  
    int index = locations.indexOf(location);
    if (index < 0) {
      logger.warning(String.format(
          "Location %s was not requested when scanning",
          location == null ? "null" : location.getModelName()));
      return 0;
    }
    
    return visitInfoKeysForLocations.get(index).size();
  }
  
  /**
   * Gets the set of taxa seen in a given year.
   * 
   * Returns null if retrieval failed, is not yet complete, or the requested
   * year was not explicitly scanned.
   */
  @Nullable
  public Set<String> getYearTaxa(Taxonomy taxonomy, int year) {
    if (!options.yearToScan.isPresent()
        || options.yearToScan.getAsInt() != year
        || !isDone()) {
      return null;
    }

    return yearIds.getOrDefault(taxonomy, ImmutableSet.of());
  }

  /**
   * Gets the set of photographed taxa.
   *
   * Returns null if retrieval failed, is not yet complete.
   */
  @Nullable
  public Set<String> getPhotographedTaxa(Taxonomy taxonomy) {
    if (!isDone()) {
      return null;
    }
    return photographedIds.getOrDefault(taxonomy, ImmutableSet.of());
  }

  /**
   * Gets the set of taxa for a user.
   *
   * Returns null if retrieval failed, is not yet complete.
   */
  @Nullable
  public Set<String> getTaxaForUser(Taxonomy taxonomy, User user) {
    if (!isDone() || users.isEmpty()) {
      return null;
    }

    if (!seenTaxaForUsers.containsKey(taxonomy)) {
      return ImmutableSet.of();
    }
    
    int index = users.indexOf(user);
    if (index < 0) {
      logger.warning(String.format(
          "User %s was not requested when scanning", user));
      return ImmutableSet.of();
    }

    return seenTaxaForUsers.get(taxonomy).get(index);
  }

  static class ScannerResults {
    final ImmutableMap<Taxonomy, List<Set<String>>> taxaPerLocation;
    final ImmutableList<Set<VisitInfoKey>> visitInfoKeysPerLocation;
    final ImmutableMap<Taxonomy, Set<String>> yearIds;
    final ImmutableMap<Taxonomy, Set<String>> photographedIds;
    final ImmutableMap<Taxonomy, List<Set<String>>> taxaPerUser;

    ScannerResults(
        ImmutableMap<Taxonomy, List<Set<String>>> taxaPerLocation,
        ImmutableList<Set<VisitInfoKey>> visitInfoKeysPerLocation,
        ImmutableMap<Taxonomy, Set<String>> yearIds,
        ImmutableMap<Taxonomy, Set<String>> photographedIds,
        ImmutableMap<Taxonomy, List<Set<String>>> taxaPerUser) {
      this.taxaPerLocation = taxaPerLocation;
      this.visitInfoKeysPerLocation = visitInfoKeysPerLocation;
      this.yearIds = yearIds;
      this.photographedIds = photographedIds;
      this.taxaPerUser = taxaPerUser;
    }
    
  }
  
  static class Scanner extends ReportSetScanner<ScannerResults> {
    private final List<Set<VisitInfoKey>> visits;
    private final Map<Taxonomy, List<Set<String>>> idsPerTaxonomy = new LinkedHashMap<>();
    private final Map<Taxonomy, List<Set<String>>> idsByUserPerTaxonomy = new LinkedHashMap<>();
    private final Map<Taxonomy, Set<String>> yearIdsPerTaxonomy = new LinkedHashMap<>();
    private final Map<Taxonomy, Set<String>> photographedIdsPerTaxonomy  = new LinkedHashMap<>();
    
    
    // MUST be from narrowest to widest (e.g. null must be last)
    private final Location[] locations;
    private final User[] users;
    private final LocationSet locationSet;
    private final Predicate<Sighting> sightingsToInclude;
    private final Options options;
    private final Taxonomy primaryTaxonomy;

    Scanner(ReportSet reportSet, Taxonomy taxonomy, List<Location> locations,
        Predicate<Sighting> sightingsToInclude, Options options) {
      super(taxonomy, reportSet);
      this.sightingsToInclude = sightingsToInclude;
      this.options = options;
      locationSet = reportSet.getLocations();
      this.locations = locations.toArray(new Location[locations.size()]);
      this.users = options.users.toArray(new User[options.users.size()]);
      this.visits = Lists.newArrayList();
      for (int i = 0; i < locations.size(); i++) {
        visits.add(new LinkedHashSet<>());
      }
      primaryTaxonomy = taxonomy;
    }
    
    @Override
    protected void process(Sighting sighting, SightingTaxon taxon) throws Exception {
      process(sighting, taxon, false, primaryTaxonomy);
    }
    
    private void process(Sighting sighting, SightingTaxon taxon, boolean incompatible, Taxonomy taxonomy) throws Exception {
      if (!sightingsToInclude.apply(sighting)) {
        return;
      }
      
      // For Sp. and Hybrid - resolve up to species, so that group/ssp. sp. or hybrid
      // still counts as seeing the species
      if (taxon.getType() == SightingTaxon.Type.SP
          || taxon.getType() == SightingTaxon.Type.HYBRID) {
        taxon = taxon.resolveInternal(taxonomy)
            .getParentOfAtLeastType(Taxon.Type.species);
      }
      
      if (taxon.getType() == SightingTaxon.Type.SINGLE
          || taxon.getType() == SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
        Location location;
        if (sighting.getLocationId() == null) {
          location = null;
        } else {
          location = locationSet.getLocation(sighting.getLocationId());
        }

        // Figure out which index is the first one to update
        int locationIndex = -1;
        while (locationIndex == -1 && location != null) {
          for (int i = 0; i < locations.length; i++) {
            if (locations[i] == location) {
              locationIndex = i;
              break;
            }
          }
          if (location != null) {
            location = location.getParent();
          }
        }
        // Not found at all in the hierarchy;  so this sighting just qualifies for the world list 
        if (locationIndex < 0) {
          locationIndex = locations.length - 1;
        }
        
        VisitInfoKey visitInfoKey = VisitInfoKey.forSighting(sighting);
        if (visitInfoKey != null) {
          for (int i = locationIndex; i < locations.length - 1; i++) {
            boolean added = visits.get(i).add(visitInfoKey);
            // If this visit was already tallied for this location, it'll certainly be
            // tallied for the parents too
            if (!added) {
              break;
            }
          }
        }
        
        List<Set<String>> ids = idsPerTaxonomy.computeIfAbsent(taxonomy,
            t -> {
              List<Set<String>> newIds = new ArrayList<>(locations.length);
              for (int i = 0; i < locations.length; i++) {
                newIds.add(new LinkedHashSet<>());
              }
              return newIds;
            });
        Taxon tx = taxonomy.getTaxon(taxon.getId());
        Taxon upToSpecies = tx;
        boolean earlyTerminate = false;
        while (!earlyTerminate && upToSpecies.getType().compareTo(Taxon.Type.species) <= 0) {
          for (int i = locationIndex ; i < locations.length; i++) {
            boolean added = ids.get(i).add(upToSpecies.getId());
            if (!added) {
              // If this taxon was already present for this location, it'll
              // certainly be present for all subsequent locations, no need to try.
              // Hence the break.
              
              // And if it's present in the very first location, then there's no need to even bother
              // processing higher taxa at all, so we can terminate early altogether
              // TODO: I have no idea why I wrote added this early termination...  it seems wrong.
              if (i == locationIndex) {
                earlyTerminate = true;
              }
              break;
            }
          }
          upToSpecies = upToSpecies.getParent();
        }
        
        // Scan for the year list if needed
        if (options.yearToScan.isPresent()) {
          ReadablePartial date = sighting.getDateAsPartial();
          if (date != null
              && date.isSupported(DateTimeFieldType.year())
              && (date.get(DateTimeFieldType.year()) == options.yearToScan.getAsInt())) {
            upToSpecies = tx;
            Set<String> yearIds = yearIdsPerTaxonomy.computeIfAbsent(taxonomy,
                t -> new LinkedHashSet<>());
            while (upToSpecies.getType().compareTo(Taxon.Type.species) <= 0) {
              boolean added = yearIds.add(upToSpecies.getId());
              // If this taxon is already on the year list, it'll certainly be present
              // for higher-level taxa.
              if (!added) {
                break;
              }
              upToSpecies = upToSpecies.getParent();
            }
          }
        }
        
        if (sighting.hasSightingInfo() && sighting.getSightingInfo().isPhotographed()) {
          upToSpecies = tx;
          Set<String> photographedIds = photographedIdsPerTaxonomy.computeIfAbsent(taxonomy,
              t -> new LinkedHashSet<>());
          while (upToSpecies.getType().compareTo(Taxon.Type.species) <= 0) {
            boolean added = photographedIds.add(upToSpecies.getId());
            // If this taxon is already on the photographed list, it'll certainly be present
            // for higher-level taxa.
            if (!added) {
              break;
            }
            upToSpecies = upToSpecies.getParent();
          }
        }
        
        if (users.length > 0 && sighting.hasSightingInfo()) {
          List<Set<String>> idsByUser = idsByUserPerTaxonomy.computeIfAbsent(taxonomy,
              t -> {
                List<Set<String>> newIds = new ArrayList<>(users.length);
                for (int i = 0; i < users.length; i++) {
                  newIds.add(new LinkedHashSet<>());
                }
                return newIds;
              }); 
          for (int i = 0; i < users.length; i++) {
            if (sighting.getSightingInfo().getUsers().contains(users[i])) {
              upToSpecies = tx;
              Set<String> idsForUser = idsByUser.get(i);
              while (upToSpecies.getType().compareTo(Taxon.Type.species) <= 0) {
                boolean added = idsForUser.add(upToSpecies.getId());
                // If this taxon is already on this user's list, it'll certainly be present
                // for higher-level taxa.
                if (!added) {
                  break;
                }
                upToSpecies = upToSpecies.getParent();
              }
            }
          }
        }
      }
    }

    @Override
    protected void processIncompatibleSighting(Sighting sighting, SightingTaxon taxon) throws Exception {
      if (!options.includeAllTaxonomies) {
        return;
      }

      process(sighting, taxon, true, sighting.getTaxonomy());
    }
    
    @Override
    protected ScannerResults accumulated() {
      return new ScannerResults(ImmutableMap.copyOf(idsPerTaxonomy), ImmutableList.copyOf(visits),
          ImmutableMap.copyOf(yearIdsPerTaxonomy), ImmutableMap.copyOf(photographedIdsPerTaxonomy),
          ImmutableMap.copyOf(idsByUserPerTaxonomy));
    }
  }
}
