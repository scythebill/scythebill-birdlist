/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.util.List;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Nullable;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.inject.Provider;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * Produces scores for report set sightings.  Sorts through the list of
 * sightings in the background, building up scores.
 */
public class Scorer {
  private final static Logger logger = Logger.getLogger(Scorer.class.getName());
  
  private final LoadingCache<String, ScoreAccumulator> accumulators;
  private final Taxonomy taxonomy;
  private final ImmutableList<ReportSet> reportSets;

  interface ScoreAccumulator {
    void accumulate(Sighting sighting);
    int reportScore();
  }
  
  public Scorer(
      Taxonomy taxonomy,
      Iterable<ReportSet> reportSets,
      final Provider<ScoreAccumulator> accumulatorFactory) {
    this.taxonomy = taxonomy;
    this.reportSets = ImmutableList.copyOf(reportSets);
    accumulators = CacheBuilder.newBuilder().build(CacheLoader.from(accumulatorFactory::get));
  }
  
  public int getScore(String key) {
    ScoreAccumulator accumulator = accumulators.getIfPresent(key);
    return accumulator == null ? 0 : accumulator.reportScore();
  }
  
  /**
   * An ordering over taxa IDs sorting the highest scored taxa first. 
   */
  public Ordering<String> ordering() {
    return new StableOrdering();
  }
  
  class StableOrdering extends Ordering<String> {
    private final LoadingCache<String, Integer> cache;
    
    private StableOrdering() {
      this.cache = newCache();
    }
    
    @Override
    public <E extends String> List<E> sortedCopy(Iterable<E> iterable) {
      try {
        return super.sortedCopy(iterable);
      } catch (IllegalArgumentException e) {
        // TODO: https://bitbucket.org/scythebill/scythebill-birdlist/issues/368/exceptions-while-typing-names-in-a-custom
        // This code is probably not necessary anymore - I suspect the change for IndexerPanel
        // to explicitly use Supplier<Ordering> rather than Ordering resolves this problem.  But
        // we used to see "Comparison method violates its general contract!" errors from TimSort.
        logger.log(Level.WARNING, "Failed to sort" + iterable, e);
        return Lists.newArrayList(iterable);
      }
    }

    @Override
    public <E extends String> ImmutableList<E> immutableSortedCopy(Iterable<E> iterable) {
      try {
        return super.immutableSortedCopy(iterable);
      } catch (IllegalArgumentException e) {
        // TODO: https://bitbucket.org/scythebill/scythebill-birdlist/issues/368/exceptions-while-typing-names-in-a-custom
        // This code is probably not necessary anymore - I suspect the change for IndexerPanel
        // to explicitly use Supplier<Ordering> rather than Ordering resolves this problem.  But
        // we used to see "Comparison method violates its general contract!" errors from TimSort.
        logger.log(Level.WARNING, "Failed to sort" + iterable, e);
        return ImmutableList.copyOf(iterable);
      }
    }

    @Override public int compare(String first, String second) {
      // Yes, backwards; the scorer is expected to return *high* scoring entries first. 
      return Integer.compare(cache.getUnchecked(second), cache.getUnchecked(first));
    }
    
    private LoadingCache<String, Integer> newCache() {
      return CacheBuilder.newBuilder()
          .build(CacheLoader.from(
              key -> {
                ScoreAccumulator accumulator = accumulators.getIfPresent(key);
                return accumulator == null ? 0 : accumulator.reportScore();
              }));
    }
  }
  
  /**
   * Starts the scoring processing.  Returns a Future that
   * can/should be cancelled when the scorer is no longer being
   * used, e.g. with {@link UiFutures#cancelFutureOnHide}.
   */
  public Future<?> start(ListeningExecutorService executorService) {
    ListenableFuture<Void> submitted = executorService.submit(() -> {
      for (ReportSet reportSet : reportSets) {
        new ScoringRunnable(taxonomy, reportSet).call();
      }
      return null;
    });
    Futures.addCallback(submitted, new FutureCallback<Void>() {
      @Override
      public void onSuccess(Void result) {
      }

      @Override
      public void onFailure(Throwable t) {
        logger.log(Level.WARNING, "Scoring failed", t);
      }
    }, executorService);
    return submitted;
  }
  
  private class ScoringRunnable extends ReportSetScanner<Void> {

    protected ScoringRunnable(Taxonomy taxonomy, ReportSet reportSet) {
      super(taxonomy, reportSet);
    }

    /** 
     * Get the per-sighting scoring key.
     * Returns the ID of the species for the sighting, or null for none.
     */
    @Nullable
    private String getSightingKey(Sighting entry, SightingTaxon sightingTaxon) {
      if (sightingTaxon.getType() != SightingTaxon.Type.SINGLE) {
        // TODO: consider scoring hybrids and sp.s
        return null;
      }
      Taxon taxon = taxonomy.getTaxon(sightingTaxon.getId());
      Taxon species = TaxonUtils.getParentOfType(taxon, Taxon.Type.species);
      return species.getId();
    }

    /** 
     * Get an additional sighting key, if one exists.
     */
    @Nullable
    private String getSecondarySightingKey(Sighting entry, SightingTaxon sightingTaxon) {
      if (sightingTaxon.getType() != SightingTaxon.Type.SINGLE) {
        // TODO: consider scoring hybrids and sp.s
        return null;
      }
      Taxon taxon = taxonomy.getTaxon(sightingTaxon.getId());
      if (taxon.getType() == Taxon.Type.species) {
        return null;
      }
      return taxon.getId();
    }

    @Override
    protected void process(Sighting sighting, SightingTaxon taxon) throws Exception {
      String key = getSightingKey(sighting, taxon);
      if (key == null) {
        return;
      }
      
      ScoreAccumulator accumulator = accumulators.get(key);
      accumulator.accumulate(sighting);

      String secondaryKey = getSecondarySightingKey(sighting, taxon);
      if (secondaryKey != null) {
        accumulator = accumulators.get(secondaryKey);
        accumulator.accumulate(sighting);
      }
    }

    @Override
    protected Void accumulated() {
      return null;
    }
    
  }
}
