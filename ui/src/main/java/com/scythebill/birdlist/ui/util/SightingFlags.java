/*   
 * Copyright 2018 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingInfo;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Utilities for appending textual flags describing sightings.
 * @author awiner
 *
 */
public final class SightingFlags {
  private SightingFlags() {}

  /** Adds sighting information to a piece of text. */
  public static String appendSightingInfo(String text, Sighting sighting) {
    switch (sighting.getSightingInfo().getSightingStatus()) {
      case INTRODUCED:
      case RECORD_NOT_ACCEPTED:
      case INTRODUCED_NOT_ESTABLISHED:
      case NOT_BY_ME:
      case SIGNS:
      case DOMESTIC:
      case RESTRAINED:
      case UNSATISFACTORY_VIEWS:
        text += " - " + Messages.getText(sighting.getSightingInfo().getSightingStatus()).toLowerCase();
        break;
      case DEAD:
        text += " (†)";
        break;
      case ID_UNCERTAIN:
        text += " (?)";
        break;
      case BETTER_VIEW_DESIRED:
        text += " (BVD)";
      case NONE:
        break;
    }
    if (sighting.getSightingInfo().isHeardOnly()) {
      text += " (H)";
    }
    
    return text;
  }

  /** Adds sighting information to a piece of text. */
  public static String getAbbreviatedSightingFlags(SightingInfo sightingInfo) {
    String text = null;
    switch (sightingInfo.getSightingStatus()) {
      case INTRODUCED:
        text = Messages.getMessage(Name.SIGHTING_STATUS_INTRODUCED_ABBREVIATION);
        break;
      case INTRODUCED_NOT_ESTABLISHED:
        text = Messages.getMessage(Name.SIGHTING_STATUS_INTRODUCED_NOT_ESTABLISHED_ABBREVIATION);
        break;
      case RECORD_NOT_ACCEPTED:
        text = Messages.getMessage(Name.SIGHTING_STATUS_RECORD_NOT_ACCEPTED_ABBREVIATION);
        break;
      case BETTER_VIEW_DESIRED:
        text = Messages.getMessage(Name.SIGHTING_STATUS_BETTER_VIEW_DESIRED_ABBREVIATION);
        break;
      case UNSATISFACTORY_VIEWS:
        text = Messages.getMessage(Name.SIGHTING_STATUS_UNSATISFACTORY_VIEWS_ABBREVIATION);
        break;
      case NOT_BY_ME:
      case SIGNS:
      case RESTRAINED:
      case DOMESTIC:
        text = Messages.getText(sightingInfo.getSightingStatus());
        break;
      case DEAD:
        text = "(†)";
        break;
      case ID_UNCERTAIN:
        text = "(?)";
        break;
      case NONE:
        text = null;
        break;
    }
    if (sightingInfo.isHeardOnly()) {
      text = text == null ? "(H)" : text + " (H)";
    }
    
    return text;
  }
}
