/*   
 * Copyright 2012 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import com.google.common.base.CharMatcher;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.util.ToString;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

public class SubspeciesToString implements ToString<Taxon> {
  @Override public String getString(Taxon t) {
    if (t == null || t.getType() == Type.species) {
      return Messages.getMessage(Name.SUBSPECIES_UNSPECIFIED);
    }
    
    if (t.getCommonName() != null) {
      String name = t.getCommonName();
      Taxon species = TaxonUtils.getParentOfType(t, Type.species);
      if (species.getCommonName() != null
          && name.startsWith(species.getCommonName())) {
        name = CharMatcher.whitespace().trimFrom(name.substring(species.getCommonName().length()));
      }
      
      if (name.equals(t.getName())) {
        // Feral/domestic forms in eBird/Clements have matching sci. and common names 
        return name;
      } else {
        return t.getName() + " " + name;
      }
    } else {
      String groupIndent = (t.getType() == Type.subspecies && t.getParent().getType() == Type.group)
        ? "    " : "";
      Taxon genus = TaxonUtils.getParentOfType(t, Type.genus);
      Taxon species = TaxonUtils.getParentOfType(t, Type.species);
      return groupIndent + genus.getName().charAt(0) + "." + species.getName().charAt(0)
          + "." + t.getName();
    }
  }

  @Override public String getPreviewString(Taxon t) {
    return getString(t);
  }
}
