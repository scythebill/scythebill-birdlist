/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.util.ToString;

import java.awt.Component;
import java.awt.Font;
import java.util.Set;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.ListModel;

/**
 * Renders a taxon in a JList.
 */
public class TaxonListCellRenderer extends DefaultListCellRenderer {
  private Predicate<Taxon> isImportantPredicate = Predicates.alwaysFalse();
  private boolean showFamilyTotals = true;
  private ToString<Taxon> toString = TaxonToCommonName.getInstance();

  @Override
  public Component getListCellRendererComponent(JList<? extends Object> list, Object value, int index,
      boolean isSelected, boolean cellHasFocus) {
    Taxon taxon = (Taxon) value;
    boolean isFamily = taxon.getType() == Taxon.Type.family;
    String text = taxonToString(taxon);
    if (isFamily && showFamilyTotals) {
      ListModel<? extends Object> model = list.getModel();
      int i = index + 1;
      Set<String> speciesSet = Sets.newHashSet();
      while (i < model.getSize()) {
        Taxon child = (Taxon) model.getElementAt(i);
        
        if (child.getType() == Taxon.Type.family) {
          break;
        }

        if (child.getType().compareTo(Taxon.Type.species) <= 0) {
          Taxon species = TaxonUtils.getParentOfType(child, Taxon.Type.species);
          speciesSet.add(species.getId());
        }
        
        i++;
      }

      StringBuilder buffer = new StringBuilder(text);
      int speciesCount = TaxonUtils.countChildren(taxon, Taxon.Type.species, null);
      buffer.append(" (").append(speciesSet.size()).append("/").append(speciesCount).append(")");
      text = buffer.toString();
    }
    Component component = super.getListCellRendererComponent(list, text, index, isSelected,
        cellHasFocus);
    Font font = list.getFont();
    if (isFamily) {
      font = new Font(font.getName(), Font.BOLD, font.getSize() * 4 / 3);
    }

    if (isImportantPredicate.apply(taxon)) {
      font = new Font(font.getName(), Font.BOLD, font.getSize());
    }
    
    component.setFont(font);
    return component;
  }

  private String taxonToString(Taxon taxon) {
    return toString.getString(taxon);
  }

  public void setIsImportant(Predicate<Taxon> isImportantPredicate) {
    this.isImportantPredicate = isImportantPredicate;
  }

  public void setShowFamilyTotals(boolean showFamilyTotals) {
    this.showFamilyTotals = showFamilyTotals;
  }

  public void setToString(ToString<Taxon> toString) {
    this.toString = toString;
  }
}
