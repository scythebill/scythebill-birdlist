/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.util.ToString;

/**
 * Converts a taxon to its common name, handling taxa without
 * common names.
 */
public class TaxonToCommonName implements ToString<Taxon> {
  static private final ToString<Taxon> INSTANCE = new TaxonToCommonName();
  
  static public ToString<Taxon> getInstance() {
    return INSTANCE;
  }

  private TaxonToCommonName() {}
  
  @Override public String getString(Taxon taxon) {
    if (taxon.getType() == Taxon.Type.family) {
      return taxon.getName();
    } else if (taxon.getCommonName() != null) {
      return taxon.getCommonName();
    } else {
      Taxon withCommon = getParentWithCommonName(taxon);
      return String.format("%1$s (%2$s)", withCommon.getCommonName(), taxon.getName());
    }
  }

  @Override public String getPreviewString(Taxon taxon) {
    return getString(taxon);
  }

  private Taxon getParentWithCommonName(Taxon taxon) {
    while ((taxon = taxon.getParent()) != null) {
      if (taxon.getCommonName() != null) {
        return taxon;
      }
    }
    
    throw new IllegalArgumentException("No parent with a common name");
  }
}
