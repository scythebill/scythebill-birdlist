/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.swing.event.TreeModelEvent;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.scythebill.birdlist.model.query.SightingComparators;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxon.Resolved;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxon.Type;
import com.scythebill.birdlist.model.taxa.TaxonUtils;
import com.scythebill.birdlist.model.taxa.TaxonVisitor;
import com.scythebill.birdlist.model.taxa.Taxonomy;

/**
 * TreeModel providing access to the taxonomy.
 */
public class TaxonTreeModel extends BaseTreeModel implements TreeModel {
  public enum Node {
    SEPARATOR;
    
    @Override public String toString() {
      return "";
    }
  }
  
  private final Taxonomy taxonomy;
  private final Map<Taxon.Type, Taxon.Type> nextType;
  private final LoadingCache<Taxon, List<? extends Object>> childrenCache;
  private ReportSet reportSet;
  private Taxon currentSightingsAreFor;
  private Multimap<String, Sighting> currentSightings;
  private Type smallestType;
  private Predicate<Sighting> sightingPredicate = Predicates.alwaysTrue();
  private Predicate<Taxon> includedTaxa;
  
  /**
   * @param taxonomy
   * @param types list of levels to display, from smallest to largest (e.g. species/family/order)
   */
  public TaxonTreeModel(Taxonomy taxonomy, List<Taxon.Type> types, Predicate<Taxon> includedTaxa) {
    this.includedTaxa = includedTaxa == null ? Predicates.alwaysTrue() : includedTaxa;
    // Verify the list is in sorted order
    Preconditions.checkArgument(ImmutableList.copyOf(Sets.newTreeSet(types))
        .equals(types));

    this.taxonomy = taxonomy;
    this.nextType = computeNextTypeMap(types);
    this.smallestType = types.get(0);
    childrenCache = CacheBuilder.newBuilder()
        .softValues()
        .build(new CacheLoader<Taxon, List<? extends Object>>() {
          @Override
          public List<? extends Object> load(Taxon key) throws Exception {
            return getTaxonChildren(key);
          }          
        });
  }
  
  public void setReportSet(ReportSet reportSet) {
    this.reportSet = reportSet;
    currentSightingsAreFor = null;
    childrenCache.invalidateAll();
  }

  public void setSightingPredicate(Predicate<Sighting> sightingPredicate) {
    this.sightingPredicate = Preconditions.checkNotNull(sightingPredicate);
    currentSightingsAreFor = null;
    childrenCache.invalidateAll();
  }
  
  @Override public Object getChild(Object node, int index) {
    return getChildNodes(node).get(index);
  }

  @Override public int getChildCount(Object node) {
    return getChildNodes(node).size();
  }

  @Override public int getIndexOfChild(Object parent, Object child) {
    return getChildNodes(parent).indexOf(child);
  }

  @Override public Object getRoot() {
    return taxonomy.getRoot();
  }

  @Override public boolean isLeaf(Object node) {
    if (node instanceof Sighting || node == Node.SEPARATOR) {
      return true;
    }
    
    return getChildCount(node) == 0;
  }

  public void sightingsAdded(TreePath parent, List<Sighting> sightings) {
    // Add the sighting to the results for anything on the path
    // (though really, it only matters for species and below)
    if (currentSightings != null) {
      for (Object o : parent.getPath()) {
        Taxon taxon = (Taxon) o;
        childrenCache.invalidate(taxon);
        currentSightings.putAll(taxon.getId(), sightings);
      }
    }
    
    int[] indices = new int[sightings.size()];
    for (int i = 0; i < indices.length; i++) {
      indices[i] = getIndexOfChild(parent.getLastPathComponent(), sightings.get(i));
    }
    // TODO: this leaves the indices array not matching the values...
    Arrays.sort(indices);

    Object[] values = sightings.toArray();
    fireTreeNodesInserted(new TreeModelEvent(this, parent, indices, values)); 
  }

  public void sightingsRemoved(TreePath parentPath, List<Sighting> sightings,
      List<Integer> originalIndices) {
    boolean fullyInvalid = false;
    
    // Remove the sighting from the results for anything on the path
    // (though really, it only matters for species and below)
    if (currentSightings != null) {
      for (Object o : parentPath.getPath()) {
        Taxon taxon = (Taxon) o;
        childrenCache.invalidate(taxon);
        for (Sighting sighting : sightings) {
          currentSightings.remove(taxon.getId(), sighting);
        }
      }
      
      // Now clear out any non-single taxa, being somewhat brutal about it
      for (Sighting sighting : sightings) {
        // Resolve the sighting taxon into the local taxonomy
        SightingTaxon sightingTaxon = sighting.getTaxon().resolve(taxonomy)
            .getSightingTaxon();
        if (sightingTaxon.getType() != SightingTaxon.Type.SINGLE
            && sightingTaxon.getType() != SightingTaxon.Type.SINGLE_WITH_SECONDARY_SUBSPECIES) {
          for (String id : sightingTaxon.getIds()) {
            Taxon taxon = taxonomy.getTaxon(id);
            while (taxon != null) {
              fullyInvalid = true;
              childrenCache.invalidate(taxon);
              currentSightings.remove(taxon.getId(), sighting);
              taxon = taxon.getParent();
            }
          }
        }
      }
    }    
    
    int[] indices = new int[originalIndices.size()];
    for (int i = 0; i < indices.length; i++) {
      indices[i] = originalIndices.get(i);
    }
    // TODO: this leaves the indices array not matching the values...
    Arrays.sort(indices);
    
    if (fullyInvalid) {
      // TODO: this is terrible, but it does address at least one manifestation of issue 206,
      // specifically: Find a sighting assigned to a polytypic Clements group.
      // Switch to IOC.
      // Drag that sighting from one of the IOC subspecies (where it will show as "(sp)")
      // onto a different subspecies. Boom!
      fireTreeStructureChanged(new TreeModelEvent(this, parentPath.getParentPath()));
    } else {
      Object[] values = sightings.toArray();
      fireTreeNodesRemoved(new TreeModelEvent(this, parentPath, indices, values));
    }
    
  }

  @Override public void valueForPathChanged(TreePath path, Object newValue) {
    TreePath parent = path.getParentPath();
    int indexOf = getIndexOfChild(parent.getLastPathComponent(), path.getLastPathComponent());
    childrenCache.invalidate(parent.getLastPathComponent());
    
    fireTreeNodesChanged(new TreeModelEvent(this, parent, new int[]{indexOf}, new Object[]{newValue})); 
  }

  private List<? extends Object> getChildNodes(Object node) {
    if (node instanceof Taxon) {
      Taxon taxon = (Taxon) node;
      if (nextType.get(taxon.getType()) == null) {
        return getSightingsOf(taxon);
      }
      
      return childrenCache.getUnchecked(taxon);
    } else if (node instanceof Sighting) {
      return ImmutableList.of();
    } else if (node == Node.SEPARATOR) {
      return ImmutableList.of();
    } else {
      throw new IllegalArgumentException("Unexpected node type " + node);
    }
  }

  private List<? extends Object> getTaxonChildren(Taxon taxon) {
    final Taxon.Type nextLevel = nextType.get(taxon.getType());
    if (nextLevel == null) {
      return getSightingsOf(taxon);
    }

    final ImmutableList.Builder<Object> children = ImmutableList.builder();
    
    final boolean[] needSeparator = new boolean[]{false};
    if (taxon.getType().compareTo(Type.species) <= 0) {
      for (Sighting sighting : getSightingsOf(taxon)) {
        Resolved resolved = sighting.getTaxon().resolve(taxonomy);
        // If this type is lower in rank than the smallest type displayed, boost it
        // so it's displayed correctly.
        if (resolved.getSmallestTaxonType().compareTo(smallestType) < 0) {
          // ... and re-resolve within the taxonomy (since it's already in the correct taxonomy)
          resolved = resolved.getParentOfAtLeastType(smallestType).resolveInternal(taxonomy);
        }
        if (resolved.getSightingTaxon().shouldBeDisplayedWith(taxon.getId())) {
          children.add(sighting);
          needSeparator[0] = true;
        }
      }
    }
    
    TaxonUtils.visitTaxa(taxon, new TaxonVisitor() {
      @Override
      public boolean visitTaxon(Taxon taxon) {
        if (taxon.getType().compareTo(nextLevel) <= 0 &&
            taxon.getType().compareTo(smallestType) >= 0) {
          // TODO(awiner): re-enable separator.  It currently has a few glitches:
          // - Drag-and-drop: dragging all sightings of a species into a separator
          // leads to broken selection, as sightingsRemoved() doesn't properly report
          // the disappearance of the separator
          // - Separator shouldn't be selectable.  That's just lame.
//          if (needSeparator[0]) {
//            children.add(Node.SEPARATOR);
//            needSeparator[0] = false;
//          }
          if (includedTaxa.apply(taxon)) {
            children.add(taxon);
          }
          return false;
        }

        return true;
      }

    });
    
    return children.build();
  }

  /**
   * Returns the list of sightings for a taxon.  This is only available for species
   * and below, and only cached for the last-used family (so only call for a species
   * that should already be visible).  For each taxon, 
   */
  public List<Sighting> getSightingsOf(Taxon taxon) {
    if (reportSet == null) {
      return ImmutableList.of();
    }

    // Find the non-species parent (usually family)
    // TODO(awiner): if we ever add superspecies...
    Taxon familyParent = getFamilyParent(taxon);
    if (currentSightingsAreFor != familyParent) {
      currentSightings = ArrayListMultimap.create();
      currentSightingsAreFor = familyParent;
    
      // Build up a multimap of taxon->sightings for every taxon in "nonSpeciesParent".
      for (Sighting sighting : reportSet.getSightings(taxonomy)) {
        if (!sightingPredicate.apply(sighting)) {
          continue;
        }

        Resolved resolved = sighting.getTaxon().resolve(taxonomy);
        // Not mappable into the current taxonomy.
        // This only really happens (as of 2018) for Forbes's Snipe, which exists in Clements/eBird
        // but not in IOC (presumably from a dispute over how long it persisted).
        if (resolved == null) {
          continue;
        }
        if (resolved.isChildOf(familyParent)) {
          if (resolved.getType() == SightingTaxon.Type.SINGLE) {
            Taxon sightingTaxon = resolved.getTaxon();
            while (sightingTaxon != familyParent) {
              Preconditions.checkState(sightingTaxon != null,
                  "Somehow skipped over parent in " + sighting);
              
              // Don't bother gathering sightings for genera - they're not used
              if (sightingTaxon.getType() != Taxon.Type.genus) {
                currentSightings.put(sightingTaxon.getId(), sighting);
              }
              sightingTaxon = sightingTaxon.getParent();
            }
          } else {
            // For a Sp. sighting, register it in each and every parent
            SightingTaxon sightingTaxon = resolved.getSightingTaxon();
            while (resolved.getSmallestTaxonType().compareTo(Taxon.Type.genus) < 0) {
              for (String id : sightingTaxon.getIds()) {
                currentSightings.put(id, sighting);
              }
              sightingTaxon = resolved.getParent();
              // resolveInternal, since this is already in the correct taxonomy
              resolved = sightingTaxon.resolveInternal(taxonomy);
            }
          }
        }
      }
    }

    List<Sighting> sightingList = (List<Sighting>) currentSightings.get(taxon.getId());
    // Sort the list in-place, on the fly.
    Collections.sort(sightingList, SightingComparators.preferEarlier());
    return sightingList;
  }

  private Taxon getFamilyParent(Taxon taxon) {
    return TaxonUtils.getParentOfType(taxon, Type.family);
  }

  private static Map<Type, Type> computeNextTypeMap(List<Type> displayedTypes) {
    ImmutableMap.Builder<Type, Type> builder = ImmutableMap.builder();

    List<Type> typesFromLargestToSmallest = Lists.newArrayList(Type.values());
    Collections.reverse(typesFromLargestToSmallest);

    for (Type type : typesFromLargestToSmallest) {
      Type nextType = null;
      for (Type displayedType : displayedTypes) {
        if (displayedType.compareTo(type) >= 0) {
          break;
        } else {
          nextType = displayedType;
        }
      }

      if (nextType != null) {
        builder.put(type, nextType);
      }
    }

    return builder.build();
  }

  public void sightingsReplaced(
      TreePath parent,
      List<Sighting> oldSightings,
      List<Sighting> newSightings,
      int[] oldIndices) {
    // Flush sightings cache.  (Could optimize with more selective replacement
    // if useful)
    currentSightingsAreFor = null;
    // .. and generally flush the child cache for the taxon.  Have to flush
    // the whole cache as other taxa may have also been touched
    childrenCache.invalidateAll();

    final int[] newIndices = new int[newSightings.size()];
    boolean anyDifferent = false;
    for (int i = 0; i < newIndices.length; i++) {
      newIndices[i] = getIndexOfChild(
          parent.getLastPathComponent(), newSightings.get(i)); 
      if (newIndices[i] != oldIndices[i]) {
        anyDifferent = true;
      }
    }

    if (anyDifferent) {
      fireTreeNodesRemoved(new TreeModelEvent(
              this, parent, oldIndices, oldSightings.toArray()));
      fireTreeNodesInserted(new TreeModelEvent(
              this, parent, newIndices, newSightings.toArray())); 
    } else {
      fireTreeNodesChanged(new TreeModelEvent(
          this, parent, newIndices, newSightings.toArray())); 
    }
  }

  /**
   * Remove sightings from the tree model.
   * TODO:  This is actually used for some cases where the nodes aren't truly removed:
   * they're being moved to a different taxon (for Sp. resolution), and have therefore
   * dropped out of the currently visible node.  This code is therefore somewhat bogus -
   * it should also be delivering a TreeNodesAdded for the new nodes.  In practice, because
   * this is only associated with JBrowser, it doesn't happen to matter.
   */
  public void sightingsRemoved(TreePath parent, List<Sighting> oldSightings, int[] indices) {
    // Flush sightings cache.  (Could optimize with more selective replacement
    // if useful)
    currentSightingsAreFor = null;
    // .. and generally flush the child cache for the taxon.  Have to flush
    // the whole cache as other taxa may have also been touched.  Could also
    // optimize this.
    childrenCache.invalidateAll();

    fireTreeNodesRemoved(new TreeModelEvent(this, parent, indices, oldSightings.toArray()));
  }

  /** Indicates that a tree path has changed in not-specified ways. Never ideal. */
  public void structureChanged(TreePath path) {
    fireTreeStructureChanged(new TreeModelEvent(this, path));
  }
}
