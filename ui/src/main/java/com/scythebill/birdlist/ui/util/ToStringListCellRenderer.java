/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.awt.Component;

import javax.swing.JList;
import javax.swing.ListCellRenderer;

import com.scythebill.birdlist.model.util.ToString;

/**
 * JList cell renderer that uses a ToString implementation.
 */
public class ToStringListCellRenderer<T> implements ListCellRenderer<Object> {
  private final Class<T> clazz;
  private final ToString<T> toString;
  private final ListCellRenderer<String> base;

  public ToStringListCellRenderer(ToString<T> toString, Class<T> clazz, ListCellRenderer<String> base) {
    this.toString = toString;
    this.clazz = clazz;
    this.base = base;
  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
  @Override
  public Component getListCellRendererComponent(JList list, Object value, int index,
      boolean isSelected, boolean cellHasFocus) {
    T typedValue = clazz.cast(value);
    String stringValue = toString.getString(typedValue);
    return base.getListCellRendererComponent(list, stringValue, index, isSelected, cellHasFocus);
  }
}
