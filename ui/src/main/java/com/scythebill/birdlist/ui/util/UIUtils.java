/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.KeyEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.text.DefaultEditorKit;

import com.google.common.primitives.Ints;
import com.scythebill.birdlist.ui.messages.Messages;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Assorted UI utilities.
 * 
 * TODO: refactor into sensibly named classes.
 */
public class UIUtils {

  private UIUtils() {
  }

  /**
   * Focuses on a component as soon as the container becomes visible.
   */
  public static void focusOnEntry(JComponent container, final Component toFocus) {
    container.addAncestorListener(new AncestorListener() {
      @Override public void ancestorRemoved(AncestorEvent e) {
      }
      
      @Override public void ancestorMoved(AncestorEvent e) {
      }
      
      @Override public void ancestorAdded(AncestorEvent e) {
        // Run this later (so that it comes after the focus-on-first code in Swing) 
        SwingUtilities.invokeLater(() -> toFocus.requestFocusInWindow());
      }
    });
  }
  
  
  static public Action getCopyAction() {
    Action copy = new DefaultEditorKit.CopyAction();
    copy.putValue(Action.NAME, Messages.getMessage(Name.COPY_MENU));
    copy.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_C,
        Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx()));
    return copy;
  }

  static public Action getCutAction() {
    Action cut = new DefaultEditorKit.CutAction();
    cut.putValue(Action.NAME, Messages.getMessage(Name.CUT_MENU));
    cut.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_X,
        Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx()));
    return cut;
  }

  static public Action getPasteAction() {
    Action paste = new DefaultEditorKit.PasteAction();
    paste.putValue(Action.NAME, Messages.getMessage(Name.PASTE_MENU));
    paste.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(
        KeyEvent.VK_V, Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx()));
    return paste;
  }

  static public boolean isWindows() {
    return System.getProperty("os.name").toLowerCase().contains("win");
  }
  
  /** Returns true if the the machine is running Mac OS X */
  static public boolean isMacOS() {
    return System.getProperty("os.name").toLowerCase().startsWith("mac os x");
  }
  
  /** Returns true if the the machine is running Mac OS X Yosemite. */
  static public boolean isMacOSYosemite() {
    Integer pointVersion = getMacOsPointVersion();
    return pointVersion != null && pointVersion.intValue() == 10;
  }

  static private Integer getMacOsPointVersion() {
    if (!isMacOS()) {
      return null;
    }
    
    String version = System.getProperty("os.version");
    Pattern pattern = Pattern.compile("10\\.([0-9]*)\\..*");
    Matcher matcher = pattern.matcher(version);
    if (matcher.matches()) {
      String subversion = matcher.group(1);
      return Ints.tryParse(subversion);
    }
    return null;
  }
  
  
  /**
   *  Fix the behavior of the tab key for JTextArea to *not* insert tab keys
   */
  static public void fixTabOnTextArea(JTextArea textArea) {
    // Inherit the set from the parent for both forward and backwards (blowing
    // off the overrides set on textarea)
    textArea.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS,
        null);
    textArea.setFocusTraversalKeys(
        KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, null);
  }
  
  static public void requestFirstFocusInComponent(Container c) {
    FocusTraversalPolicy policy = getFocusTraversalPolicy(c);
    if (policy != null) {
      Component firstComponent = policy.getFirstComponent(c);
      if (firstComponent != null) {
        firstComponent.requestFocusInWindow();
      }
    }
  }
  
  static public FocusTraversalPolicy getFocusTraversalPolicy(Container c) {
    while (c != null) {
      FocusTraversalPolicy focusTraversalPolicy = c.getFocusTraversalPolicy();
      if (focusTraversalPolicy != null) {
        return focusTraversalPolicy;
      }
      c = c.getParent();
    }
    return null;
  }
  
  /**
   * Move a window to keep it entirely on screen.
   */
  public static void keepWindowOnScreen(Window window) {
    Rectangle bounds = window.getBounds();
    Rectangle originalBounds = new Rectangle(bounds);
    Rectangle screenBounds = window.getGraphicsConfiguration().getBounds();
    // Add any needed insets (esp. windows taskbar)
    Insets insets = window.getToolkit().getScreenInsets(window.getGraphicsConfiguration());
    
    // Trim the width and height to fit on the screen
    bounds.width = Math.min(bounds.width,
            screenBounds.width - insets.left - insets.right);
    bounds.height = Math.min(bounds.height,
            screenBounds.height - insets.top - insets.bottom);
    // Move top and left down until that corner is onscreen
    int minX = screenBounds.x + insets.left;
    int minY = screenBounds.y + insets.top;
    bounds.x = Math.max(minX, bounds.x);
    bounds.y = Math.max(minY, bounds.y);
    
    // And now move top and left up until the bottom-right corner is onscreen 
    int maxX = screenBounds.x + screenBounds.width - insets.right;
    int maxY = screenBounds.y + screenBounds.height - insets.bottom;
    bounds.x = Math.min(bounds.x, maxX - bounds.width);
    bounds.y = Math.min(bounds.y, maxY - bounds.height);

    if (originalBounds.equals(bounds)) {
      return;
    }
    
    if (originalBounds.x == bounds.x && originalBounds.y == bounds.y) {
      // Avoid setting the position (in hopes of making setLocationByPlatform happier)
      window.setSize(bounds.width, bounds.height);
    } else {
      window.setBounds(bounds);
    }
  }
  
  /**
   * Maximize a window on screen.
   */
  public static void maximizeWindow(Window window) {
    window.setBounds(getScreenBounds(window));
  }

  public static Rectangle getScreenBounds(Window window) {
    Rectangle screenBounds = window.getGraphicsConfiguration().getBounds();
    // Add any needed insets (esp. windows taskbar)
    Insets insets = window.getToolkit().getScreenInsets(window.getGraphicsConfiguration());
    
    Rectangle bounds = new Rectangle();
    bounds.x = screenBounds.x + insets.left;
    bounds.y = screenBounds.y + insets.top;
    bounds.width = screenBounds.width - (insets.left + insets.right);
    bounds.height = screenBounds.height - (insets.top + insets.bottom);
    return bounds;
  }
  
  /**
   * Return the overall height of the font.
   */
  public static int getFontHeight(Graphics graphics, Font font) {
    FontMetrics fontMetrics = graphics.getFontMetrics(font);
    return fontMetrics.getMaxAscent() + fontMetrics.getMaxDescent();   
  }

  public static Frame findFrame(Component component) {
    while (component != null) {
      if (component instanceof Frame) {
        return (Frame) component;
      }
      component = component.getParent();
    }
    return null;
  }

  public static <T extends Component> T findParentOfType(Component component, Class<T> type) {
    while ((component = component.getParent()) != null) {
      if (type.isInstance(component)) {
        return type.cast(component);
      }
    }
    return null;
  }
  
  public static boolean containsFocus(Component component) {
    Window w = SwingUtilities.getWindowAncestor(component);
    if (w == null) {
      return false;
    }
    
    Component focusOwner = w.getFocusOwner();
    if (focusOwner == null) {
      return false;
    }
    
    return SwingUtilities.isDescendingFrom(focusOwner, component);
  }
  
  public static <T extends Component> T findChildOfType(Container container, Class<T> type) {
    for (Component child : container.getComponents()) {
      if (type.isInstance(child)) {
        return type.cast(child);
      }
      if (child instanceof Container c) {
        T found = findChildOfType(c, type);
        if (found != null) {
          return found;
        }
      }
    }
    return null;
  }
}
