/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.awt.Component;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.concurrent.Future;

/**
 * Utilities for connecting Futures to the UI. 
 */
public class UiFutures {
  static public void cancelFutureOnHide(final Future<?> future, Component component) {
    component.addComponentListener(new ComponentAdapter() {
      @Override public void componentHidden(ComponentEvent event) {
        future.cancel(true);
      }      
    });
  }
}
