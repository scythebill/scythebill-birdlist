/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.awt.Component;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.logging.Logger;

import javax.inject.Singleton;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import com.google.inject.Inject;
import com.scythebill.birdlist.ui.messages.Messages.Name;

/**
 * Utility to see if an important portion of the UI has moved off the screen
 * and the user can't see it;  warns the user.
 */
@Singleton
public class VisibilityDetector {
  private final static Logger logger = Logger.getLogger(VisibilityDetector.class.getName());

  /**
   * A small fudge factor, so if a few pixels are offscreen, we don't alert.
   */
  private static final int FUDGE_FACTOR = 3; 

  private final Alerts alerts;
  private final Timer timer;
  
  /** Current component being tracked. */
  private Component componentToCheck;

  /** Track if any alerts have happened.  Only alert the user once per session. */
  private boolean alertedYet; 

  @Inject
  public VisibilityDetector(Alerts alerts) {
    this.alerts = alerts;
    this.timer = new Timer(2000, e -> checkIfComponentIsVisible());
    this.timer.setRepeats(false);
  }
  
  public void install(Component component) {
    GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
    GraphicsDevice[] screenDevices = graphicsEnvironment.getScreenDevices();
    // The code below is not particularly savvy with dealing with multiple monitors.
    // And the code only really existed to deal with users that had extremely small laptop
    // displays.  If you have multiple monitors, just stop here and don't try harder.
    if (screenDevices.length > 1) {
      return;
    }
    
    component.addComponentListener(new ComponentAdapter() {

      @Override
      public void componentMoved(ComponentEvent e) {
        componentToCheck = e.getComponent();
        timer.restart();
      }
    });
  }

  private void checkIfComponentIsVisible() {
    if (componentToCheck == null) {
      return;
    }
    if (!componentToCheck.isDisplayable()) {
      componentToCheck = null;
      return;
    }
    Window window = SwingUtilities.windowForComponent(componentToCheck);
    if (window == null) {
      logger.warning("No window for " + componentToCheck.getClass());
      return;
    }

    // Get the bottom-right point (this assuming a left-to-right layout, alas)
    Point bottomRightInWindow = SwingUtilities.convertPoint(
        componentToCheck,
        componentToCheck.getWidth(),
        componentToCheck.getHeight(),
        SwingUtilities.getRoot(componentToCheck));
    // Get the available screen size for this window
    Rectangle screenBounds = UIUtils.getScreenBounds(window);
    if (screenBounds.x + screenBounds.width + FUDGE_FACTOR < bottomRightInWindow.x
        || screenBounds.y + screenBounds.height + FUDGE_FACTOR < bottomRightInWindow.y) {
      if (!alertedYet) {
        alertedYet = true;
        alerts.showMessage(window,
            Name.SCYTHEBILL_DOESNT_FIT_TITLE,
            Name.SCYTHEBILL_DOESNT_FIT_MESSAGE);
      }
    }
  }
}
