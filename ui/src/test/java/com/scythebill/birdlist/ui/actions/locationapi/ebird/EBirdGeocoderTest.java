/*   
 * Copyright 2015 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.actions.locationapi.ebird;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableList;
import com.google.common.io.Resources;
import com.scythebill.birdlist.model.io.CsvImportLines;
import com.scythebill.birdlist.model.io.ImportLines;
import com.scythebill.birdlist.ui.actions.locationapi.ebird.EBirdGeocoder.EBirdHotspot;
import com.scythebill.birdlist.ui.actions.locationapi.ebird.EBirdGeocoder.EBirdHotspots;

/**
 * Basic "tests" to drive the eBird code with a sample CSV file.
 */
@RunWith(JUnit4.class)
public class EBirdGeocoderTest {
  private static EBirdHotspots hotspots;

  @BeforeClass
  public static void parseSample() throws IOException {
    ImportLines lines = CsvImportLines.fromUrl(
        Resources.getResource(EBirdGeocoderTest.class, "ebird-sample.csv"), Charsets.ISO_8859_1);
    try {
      hotspots = new EBirdHotspots();
      String[] line;
      while ((line = lines.nextLine()) != null) {
        hotspots.hotspots.add(new EBirdHotspot(line));
      }
    } finally {
      lines.close();
    }
  }
  
  @Test
  public void empty() {
    ImmutableList<EBirdHotspot> found = hotspots.findByName("Not there");
    assertEquals(0, found.size());
  }

  @Test
  public void simpleGeocode() {
    ImmutableList<EBirdHotspot> found = hotspots.findByName("Anse Cochon");
    assertEquals(1, found.size());
    EBirdHotspot hotspot = found.get(0);
    assertEquals("LC-01", hotspot.state);
  }

  @Test
  public void prefixSearch() {
    ImmutableList<EBirdHotspot> found = hotspots.findByName("Santa Clara River");
    assertEquals(5, found.size());
  }

  @Test
  public void suffixSearch() {
    ImmutableList<EBirdHotspot> found = hotspots.findByName("City Cemetery");
    assertEquals(1, found.size());
  }

  @Test
  public void periodIgnored() {
    ImmutableList<EBirdHotspot> found = hotspots.findByName("Avra Valley Rd");
    assertEquals(1, found.size());
  }

  @Test
  public void roadAndRdAreIdentical() {
    ImmutableList<EBirdHotspot> found = hotspots.findByName("Avra Valley Road");
    assertEquals(1, found.size());
  }
  
  @Test
  public void accentedCharactersAreFound() {
    ImmutableList<EBirdHotspot> found = hotspots.findByName("Chaparrí Ecolodge");
    assertEquals(1, found.size());
  }

  @Test
  public void accentedCharactersAreFoundIfNotSpecified() {
    ImmutableList<EBirdHotspot> found = hotspots.findByName("Chaparri Ecolodge");
    assertEquals(1, found.size());
  }
  
  @Test
  public void parentheticalPhrasesDropped() {
    ImmutableList<EBirdHotspot> found = hotspots.findByName("Urban Fishery");
    assertEquals(1, found.size());
  }

  @Test
  public void quoteCharactersDropped() {
    ImmutableList<EBirdHotspot> found = hotspots.findByName("Puu Oo");
    assertEquals(1, found.size());
  }
}
