package com.scythebill.birdlist.ui.imports;

import static org.junit.Assert.assertEquals;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;

public class DateFromStringFieldMapperTest {

  private DateFromStringFieldMapper<String[]> mapper;
  private Taxonomy taxonomy;

  @Before
  public void createMapper() {
    mapper = new DateFromStringFieldMapper<>(
        "MM/dd/yy",
        LineExtractors.stringFromIndex(0));
    taxonomy = new TaxonomyImpl();
  }
  
  @Test
  public void fourYearDate() {
    Sighting.Builder builder = Sighting.newBuilder()
        .setTaxonomy(taxonomy)
        .setTaxon(SightingTaxons.newSightingTaxon("dummyId"));
    mapper.map(new String[]{"1/1/1999"}, builder);
    
    LocalDate parsed = new LocalDate(builder.build().getDateAsPartial());
    assertEquals(1999, parsed.getYear());

    mapper.map(new String[]{"1/1/2001"}, builder);
    parsed = new LocalDate(builder.build().getDateAsPartial());
    assertEquals(2001, parsed.getYear());
  }

  @Test
  public void twoYearDate() {
    Sighting.Builder builder = Sighting.newBuilder()
        .setTaxonomy(taxonomy)
        .setTaxon(SightingTaxons.newSightingTaxon("dummyId"));
    mapper.map(new String[]{"1/1/99"}, builder);
    
    LocalDate parsed = new LocalDate(builder.build().getDateAsPartial());
    assertEquals(1999, parsed.getYear());

    mapper.map(new String[]{"1/1/01"}, builder);
    parsed = new LocalDate(builder.build().getDateAsPartial());
    assertEquals(2001, parsed.getYear());
  }
}
