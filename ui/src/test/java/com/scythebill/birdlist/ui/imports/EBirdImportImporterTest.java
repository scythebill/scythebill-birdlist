/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.Iterables;
import com.google.common.io.Files;
import com.google.common.io.Resources;
import com.scythebill.birdlist.model.checklist.Checklists;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations.PredefinedLocation;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.ui.app.TaxonomyLoader;
import com.scythebill.birdlist.ui.app.TaxonomyReference;
import com.scythebill.birdlist.ui.imports.TaxonImporter.ToBeDecided;

/**
 * Tests {@link EBirdImportImporter}.
 */
public class EBirdImportImporterTest {
  private static Taxonomy testTaxonomy;
  private static PredefinedLocations predefinedLocations;
  private static Checklists checklists;

  @BeforeClass
  public static void loadStatics() throws Exception {
    URL taxonUrl = Resources.getResource("com/scythebill/birdlist/ui/util/testTaxonomy.xml");
    testTaxonomy = new TaxonomyLoader(new TaxonomyReference(taxonUrl)).call();
    predefinedLocations = PredefinedLocations.loadAndParse();
    checklists = new Checklists(null);
  }

  private ReportSet reportSet;
  private LocationSet locations;
  private Location california;
  private EBirdImportImporter importer;
  private File file;
  
  @Before
  public void setUp() throws Exception {
    reportSet = ReportSets.newReportSet(testTaxonomy);
    locations = reportSet.getLocations();
    california = locations.getLocationByCode("US-CA");
    file = File.createTempFile("eBirdImport", ".csv");
    file.deleteOnExit();
    
    URL csvUrl = Resources.getResource(getClass(), "eBirdImport.csv");
    Files.write(Resources.toByteArray(csvUrl), file);
    
    
    importer = new EBirdImportImporter(reportSet, testTaxonomy, checklists, predefinedLocations, file);
  }
  
  @Test
  public void importEBirdImport() throws Exception {
    PredefinedLocation predefined = predefinedLocations.getPredefinedLocationChild(california, "Orange");
    Location orangeCounty = predefined.create(locations, california);
    Location bolsaChica = Location.builder()
        .setName("Bolsa Chica")
        .setParent(orangeCounty)
        .build();
    locations.ensureAdded(bolsaChica);
    
    assertFalse(importer.initialCheck().isPresent());
    
    Map<Object, ToBeDecided> toBeDecidedTaxa = importer.parseTaxonomyIds();
    assertTrue(toBeDecidedTaxa.isEmpty());
    
    List<Sighting> runImport = importer.runImport();
    Sighting sighting = Iterables.getOnlyElement(runImport);
    // Verify that the sighting was directly imported into Bolsa Chica, even
    // though the information about "Orange County" is missing from the eBird import file.
    assertEquals(bolsaChica.getId(), sighting.getLocationId());
    
    assertEquals("grStrutcam[ca", sighting.getTaxon().getId());
    
    LocalDate when = new LocalDate(sighting.getDateAsPartial());
    assertEquals(1999, when.getYear());
    assertEquals(6, when.getMonthOfYear());
    assertEquals(11, when.getDayOfMonth());
  }
}
