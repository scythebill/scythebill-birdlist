/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.joda.time.Duration;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.xml.sax.SAXException;

import com.scythebill.birdlist.model.sighting.Area;
import com.scythebill.birdlist.model.sighting.Distance;
import com.scythebill.birdlist.model.sighting.LatLongCoordinates;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.xml.XmlTaxonImport;
import com.scythebill.birdlist.ui.imports.EBird.CommonNameExtractor;
import com.scythebill.birdlist.ui.imports.EBird.NameAndLatLong;
import com.scythebill.birdlist.ui.imports.EBird.ScientificNameExtractor;

@RunWith(JUnit4.class)
public class EBirdTest {
  private static Taxonomy taxonomy;
  
  @BeforeClass
  public static void loadTaxonomy() throws UnsupportedEncodingException, IOException, SAXException {
    XmlTaxonImport importer = new XmlTaxonImport();
    try (InputStream testTaxon = EBirdTest.class.getResourceAsStream("/com/scythebill/birdlist/ui/util/testTaxonomy.xml")) {
      taxonomy = importer.importTaxa(new InputStreamReader(testTaxon, "UTF-8"));
    }
  }
  
  @Test
  public void extractsCommonNameFromCompositeGroup() {
    TaxonResolver taxonResolver = new TaxonResolver(taxonomy);
    CommonNameExtractor commonNameExtractor = new CommonNameExtractor(
        LineExtractors.constant("Gray Heron (Gray) (Ardea cinerea cinerea/jouyi)"), taxonResolver);
    assertEquals("Gray Heron (Gray)", commonNameExtractor.extract(new String[0]));
  }
  
  @Test
  public void extractsScientificNameFromCompositeGroup() {
    TaxonResolver taxonResolver = new TaxonResolver(taxonomy);
    ScientificNameExtractor scientificNameExtractor = new ScientificNameExtractor(
        LineExtractors.constant("Gray Heron (Gray) (Ardea cinerea cinerea/jouyi)"), taxonResolver);
    assertEquals("Ardea cinerea cinerea/jouyi", scientificNameExtractor.extract(new String[0]));
  }
  
  @Test
  public void extractsCommonNameFromTranslatedCompositeGroup() {
    TaxonResolver taxonResolver = new TaxonResolver(taxonomy);
    CommonNameExtractor commonNameExtractor = new CommonNameExtractor(
        LineExtractors.constant("Grey Heron (Grey) (Ardea cinerea cinerea/jouyi)"), taxonResolver);
    assertEquals("Grey Heron (Grey)", commonNameExtractor.extract(new String[0]));
  }

  @Test
  public void extractsCommonNameForRockDoveFeralPigeon() {
    TaxonResolver taxonResolver = new TaxonResolver(taxonomy);
    CommonNameExtractor commonNameExtractor = new CommonNameExtractor(
        LineExtractors.constant("Rock Dove (Feral Pigeon) (Columba livia (Feral Pigeon))"), taxonResolver);
    assertEquals("Rock Dove (Feral Pigeon)", commonNameExtractor.extract(new String[0]));
  }
  
  @Test
  public void extractsScientificNameForRockDoveFeralPigeon() {
    TaxonResolver taxonResolver = new TaxonResolver(taxonomy);
    ScientificNameExtractor scientificNameExtractor = new ScientificNameExtractor(
        LineExtractors.constant("Rock Dove (Feral Pigeon) (Columba livia (Feral Pigeon))"), taxonResolver);
    assertEquals("Columba livia (Feral Pigeon)", scientificNameExtractor.extract(new String[0]));
  }
  
  @Test
  public void durationHours() {
    Duration duration = EBird.durationHrsMins("2 hour(s)");
    assertEquals(120, duration.getStandardMinutes());
  }

  @Test
  public void durationMinutes() {
    Duration duration = EBird.durationHrsMins("30 minute(s)");
    assertEquals(30, duration.getStandardMinutes());
  }

  @Test
  public void durationHoursAndMinutes() {
    Duration duration = EBird.durationHrsMins("1 hour(s), 30 minute(s)");
    assertEquals(90, duration.getStandardMinutes());
  }

  @Test
  public void distanceKilometers() {
    assertEquals(
        Distance.inKilometers(2.0f), 
        EBird.distanceMilesOrKilometers("2.0 kilometer(s)"));
  }

  @Test
  public void distanceMiles() {
    assertEquals(
        Distance.inMiles(3.5f), 
        EBird.distanceMilesOrKilometers("3.5 mile(s)"));
  }

  @Test
  public void areaAcres() {
    assertEquals(
        Area.inAcres(2.0f),
        EBird.areaHectaresOrAcres("2.0 ac"));
  }

  @Test
  public void areaHectares() {
    assertEquals(
        Area.inHectares(1.25f),
        EBird.areaHectaresOrAcres("1.25 ha"));
  }
  
  @Test
  public void extractLatLongFromNameNoMatch() {
    NameAndLatLong nameAndLatLong = EBird.extractLatLongFromName("My Spot");
    assertEquals("My Spot", nameAndLatLong.name);
    assertNull(nameAndLatLong.latLong);
  }

  @Test
  public void extractLatLongFromNameAndParens() {
    NameAndLatLong nameAndLatLong =
        EBird.extractLatLongFromName("Juan Dolio (18.444, -69.392)");
    assertEquals("Juan Dolio", nameAndLatLong.name);
    assertEquals(
        LatLongCoordinates.withLatAndLong("18.444", "-69.392"),
        nameAndLatLong.latLong);
  }

  @Test
  public void extractLatLongFromNameAndX() {
    NameAndLatLong nameAndLatLong =
        EBird.extractLatLongFromName("250 Main Street - 17.791x-112.391");
    assertEquals("250 Main Street", nameAndLatLong.name);
    assertEquals(
        LatLongCoordinates.withLatAndLong("17.791", "-112.391"),
        nameAndLatLong.latLong);
  }

  @Test
  public void extractLatLongFromNameAndLatLongWithComma() {
    NameAndLatLong nameAndLatLong =
        EBird.extractLatLongFromName("250 Main Street, Somewhere US-MA 42.75291, -72.53121");
    assertEquals("250 Main Street, Somewhere US-MA", nameAndLatLong.name);
    assertEquals(
        LatLongCoordinates.withLatAndLong("42.75291", "-72.53121"),
        nameAndLatLong.latLong);
  }
}
