/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.common.collect.ImmutableList;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.Locations;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;

import org.junit.Assert;

/**
 * Tests for HBW imports, largely around territory names.
 */
@RunWith(JUnit4.class)
public class HbwTest {
  private static final ImmutableList<String> HBW_TERRITORIES = ImmutableList.of(
      "Afghanistan",
      "Alabama",
      "Alaska",
      "Albania",
      "Alberta",
      "Algeria",
      "American Samoa",
      "Andaman and Nicobar",
      "Andorra",
      "Angola",
      "Anguilla",
      "Antarctica",
      "Antigua and Barbuda",
      "Argentina",
      "Arizona",
      "Arkansas",
      "Armenia",
      "Ascension Island",
      "Australia",
      "Austria",
      "Azerbaijan",
      "Azores",
      "Bahamas",
      "Bahrain",
      "Bali",
      "Bangladesh",
      "Barbados",
      "Belarus",
      "Belgium",
      "Belize",
      "Benin",
      "Bermuda",
      "Bhutan",
      "Bioko",
      "Bismarck Archipelago",
      "Bolivia",
      "Borneo",
      "Bosnia and Herzegovina",
      "Botswana",
      "Brazil",
      "British Columbia",
      "British Virgin Islands",
      "Brunei",
      "Bulgaria",
      "Burkina Faso",
      "Burkina Faso",
      "Burundi",
      "California",
      "Cambodia",
      "Cameroon",
      "Canada",
      "Canary Islands",
      "Cape Verde",
      "Cayman Islands",
      "Central African Republic",
      "Chad",
      "Chagos Archipelago (British Indian Ocean Territory)",
      "Chile",
      "China (Northern)",
      "China (Southern)",
      "Christmas Island",
      "Cocos (Keeling) Islands",
      "Colombia",
      "Colorado",
      "Comoro Islands",
      "Congo Republic",
      "Connecticut",
      "Cook Islands",
      "Costa Rica",
      "Croatia",
      "Croatia",
      "Crozet Islands",
      "Cuba",
      "Cyprus",
      "Czech Republic",
      "Delaware",
      "Denmark",
      "Djibouti",
      "Dominica",
      "Dominican Republic",
      "Easter Island",
      "Ecuador",
      "Egypt",
      "El Salvador",
      "Equatorial Guinea",
      "Eritrea",
      "Estonia",
      "Estonia",
      "Ethiopia",
      "Falkland (Malvinas) Islands",
      "Faroe Islands",
      "Federated States Micronesia",
      "Fiji",
      "Finland",
      "Florida",
      "France",
      "French Guiana",
      "French Polynesia",
      "Gabon",
      "Galapagos Islands",
      "Gambia",
      "Georgia",
      "Georgia (USA)",
      "Germany",
      "Ghana",
      "Gibraltar",
      "Great Britain",
      "Greece",
      "Greenland",
      "Grenada",
      "Guadeloupe",
      "Guam",
      "Guatemala",
      "Guinea",
      "Guinea-Bissau",
      "Guyana",
      "Haiti",
      "Hawaiian Islands",
      "Heard Island and McDonald Islands",
      "Honduras",
      "Hong Kong",
      "Hungary",
      "Iceland",
      "Idaho",
      "Illinois",
      "India",
      "Indiana",
      "Iowa",
      "Iran",
      "Iraq",
      "Ireland",
      "Israel",
      "Italy",
      "Ivory Coast",
      "Jamaica",
      "Japan",
      "Java",
      "Johnston Island",
      "Jordan",
      "Kalimantan",
      "Kalimantan",
      "Kansas",
      "Kazakhstan",
      "Kentucky",
      "Kenya",
      "Kerguelen Islands",
      "Kiribati",
      "Kuwait",
      "Kyrgyzstan",
      "Kyrgyzstan",
      "Laos",
      "Latvia",
      "Lebanon",
      "Lesotho",
      "Lesser Sundas",
      "Liberia",
      "Libya",
      "Liechtenstein",
      "Lithuania",
      "Louisiana",
      "Luxembourg",
      "Macau",
      "Madagascar",
      "Madeira Islands",
      "Maine",
      "Malawi",
      "Maldive Islands",
      "Mali",
      "Malta",
      "Maluku Islands",
      "Manitoba",
      "Marshall Islands",
      "Martinique",
      "Maryland",
      "Massachusetts",
      "Mauritania",
      "Mauritius",
      "Mexico (Northern)",
      "Mexico (Southern)",
      "Michigan",
      "Minnesota",
      "Mississippi",
      "Missouri",
      "Moldova",
      "Monaco",
      "Mongolia",
      "Montana",
      "Montenegro",
      "Montserrat",
      "Morocco",
      "Mozambique",
      "Myanmar",
      "Myanmar",
      "Namibia",
      "Nauru",
      "Nebraska",
      "Nepal",
      "Netherlands",
      "Netherlands Antilles",
      "Nevada",
      "New Brunswick",
      "New Caledonia",
      "New Guinea",
      "New Hampshire",
      "New Jersey",
      "New Mexico",
      "New York",
      "New Zealand",
      "Newfoundland and Labrador",
      "Nicaragua",
      "Niger",
      "Nigeria",
      "Niue",
      "Norfolk Island",
      "North Carolina",
      "North Dakota",
      "North Korea",
      "North Solomons",
      "North Solomons",
      "Northern Mariana Islands",
      "Northwest Territories",
      "Norway",
      "Nova Scotia",
      "Nunavut",
      "Ohio",
      "Oklahoma",
      "Oman",
      "Ontario",
      "Oregon",
      "Pakistan",
      "Palau",
      "Panama",
      "Papua New Guinea (mainland)",
      "Papua New Guinea (mainland)",
      "Paraguay",
      "Peninsular Malaysia",
      "Pennsylvania",
      "Peru",
      "Philippines",
      "Pitcairn Islands",
      "Poland",
      "Portugal",
      "Prince Edward Island",
      "Prince Edward Islands",
      "Puerto Rico",
      "Qatar",
      "Quebec",
      "Republic of Macedonia",
      "Réunion",
      "Rhode Island",
      "Rodrigues",
      "Romania",
      "Russia",
      "Russia",
      "Russia (Eastern)",
      "Russia (Western)",
      "Rwanda",
      "Sabah",
      "Sabah",
      "Saint Barthélemy",
      "Saint Helena Island",
      "Saint Kitts and Nevis",
      "Saint Lucia",
      "Saint Pierre and Miquelon",
      "Saint Vincent and the Grenadines",
      "Saint Vincent and the Grenadines",
      "Samoa",
      "San Marino",
      "São Tomé and Principe",
      "Sarawak",
      "Sarawak",
      "Saskatchewan",
      "Saudi Arabia",
      "Senegal",
      "Serbia",
      "Serbia",
      "Seychelles",
      "Sierra Leone",
      "Singapore",
      "Slovakia",
      "Slovenia",
      "Socotra",
      "Solomon Archipelago",
      "Solomon Islands (country)",
      "Somalia",
      "South Africa",
      "South Carolina",
      "South Dakota",
      "South Georgia",
      "South Korea",
      "South Sandwich Islands",
      "South Sudan",
      "Spain",
      "Sri Lanka",
      "St. Paul and Amsterdam Islands",
      "Sudan",
      "Sulawesi",
      "Sumatra",
      "Suriname",
      "Svalbard Islands and Jan Mayen",
      "Swaziland",
      "Sweden",
      "Switzerland",
      "Syria",
      "Taiwan",
      "Tajikistan",
      "Tanzania",
      "Tennessee",
      "Texas",
      "Thailand",
      "Timor-Leste",
      "Togo",
      "Tokelau",
      "Tonga",
      "Trinidad and Tobago",
      "Tristan da Cunha Archipelago",
      "Tunisia",
      "Turkey",
      "Turkmenistan",
      "Turks and Caicos Islands",
      "Tuvalu",
      "Uganda",
      "Ukraine",
      "United Arab Emirates",
      "United States Virgin Islands",
      "Uruguay",
      "USA (United States)",
      "Utah",
      "Uzbekistan",
      "Vanuatu",
      "Venezuela",
      "Vermont",
      "Vietnam",
      "Virgin Islands",
      "Virginia",
      "Wake Island",
      "Wallis and Futuna",
      "Washington DC",
      "Washington State",
      "West Papua (Indonesia)",
      "West Timor",
      "West Virginia",
      "Western Sahara",
      "Wisconsin",
      "Wyoming",
      "Yemen",
      "Yukon",
      "Zaire (Democratic Republic of the Congo)",
      "Zambia",
      "Zimbabwe");
  private PredefinedLocations predefinedLocations;
  private LocationSet locations;
  private LocationShortcuts shortcuts;
  private ReportSet reportSet;


  @Before
  public void setUp() {
    predefinedLocations = PredefinedLocations.loadAndParse();
    reportSet = ReportSets.newReportSet(new TaxonomyImpl());
    locations = reportSet.getLocations();
    shortcuts = new LocationShortcuts(locations, predefinedLocations);
  }
  
  @Test
  public void allTerritoriesAreMappable() {
    for (String territory : HBW_TERRITORIES) {
      Assert.assertNotNull("Could not find " + territory, HbwImporter.importedLocation(shortcuts, territory));
    }
  }
  
  @Test
  public void stateCodesResolve() {
    Location northSolomons = importTerritory("North Solomons");
    Assert.assertTrue(northSolomons.isBuiltInLocation());
    Assert.assertEquals("PG-NSA", Locations.getLocationCode(northSolomons));

    Location hawaii = importTerritory("Hawaiian Islands");
    Assert.assertTrue(hawaii.isBuiltInLocation());
    Assert.assertEquals("US-HI", Locations.getLocationCode(hawaii));
    Assert.assertEquals("Pacific Ocean", hawaii.getParent().getParent().getModelName());

    Location dc = importTerritory("Washington DC");
    Assert.assertTrue(dc.isBuiltInLocation());
    Assert.assertEquals("US-DC", Locations.getLocationCode(dc));
    Assert.assertEquals("North America", dc.getParent().getParent().getModelName());    
  }
  
  @Test
  public void madeiras() {
    Location madeiras = importTerritory("Madeira Islands");
    Assert.assertTrue(madeiras.isBuiltInLocation());
    Assert.assertEquals("PT-30", Locations.getLocationCode(madeiras));
  }
  
  @Test
  public void galapagos() {
    Location galapagos = importTerritory("Galapagos Islands");
    Assert.assertTrue(galapagos.isBuiltInLocation());
    Assert.assertEquals("EC-W", Locations.getLocationCode(galapagos));
    Assert.assertEquals("Pacific Ocean", galapagos.getParent().getModelName());
  }
  
  @Test
  public void subStateLocations() {
    Location kerguelens = importTerritory("Kerguelen Islands");
    Assert.assertFalse(kerguelens.isBuiltInLocation());
    Assert.assertEquals("Kerguelen Islands", kerguelens.getModelName());
    Assert.assertEquals("TF", Locations.getLocationCode(kerguelens.getParent()));
  }
  
  @Test
  public void russianContinents() {
    Location westRussia = importTerritory("Russia (Western)");
    Assert.assertTrue(westRussia.isBuiltInLocation());
    Assert.assertEquals("RU", Locations.getLocationCode(westRussia));
    Assert.assertEquals("Europe", westRussia.getParent().getModelName());

    Location eastRussia = importTerritory("Russia (Eastern)");
    Assert.assertTrue(eastRussia.isBuiltInLocation());
    Assert.assertEquals("RU", Locations.getLocationCode(eastRussia));
    Assert.assertEquals("Asia", eastRussia.getParent().getModelName());
  }

  private Location importTerritory(String territory) {
    String locationId = HbwImporter.importedLocation(shortcuts, territory)
        .addToLocationSet(reportSet, locations, shortcuts, predefinedLocations);
    return locations.getLocation(locationId); 
  }
  
  @Test
  public void removesDate() {
    Assert.assertEquals(
        "Cape of Good Hope",
        HbwImporter.maybeRemoveDateOrDateAndTime("Cape of Good Hope (28 Feb 2019)"));
  }

  @Test
  public void removesDateAndTime() {
    Assert.assertEquals(
        "Cape of Good Hope",
        HbwImporter.maybeRemoveDateOrDateAndTime("Cape of Good Hope (28 Feb 2019 06:00)"));
  }

  @Test
  public void doesntRemoveOtherContent() {
    Assert.assertEquals(
        "Cape of Good Hope (Foo)",
        HbwImporter.maybeRemoveDateOrDateAndTime("Cape of Good Hope (Foo)"));
  }
}
