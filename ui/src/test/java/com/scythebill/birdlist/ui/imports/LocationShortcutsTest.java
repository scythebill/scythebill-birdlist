/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;

@RunWith(JUnit4.class)
public class LocationShortcutsTest {
  private static LocationShortcuts shortcuts;

  @BeforeClass
  public static void buildShortcuts() {
    LocationSet locations = ReportSets.newReportSet(new TaxonomyImpl()).getLocations();
    PredefinedLocations predefinedLocations = PredefinedLocations.loadAndParse();
    shortcuts = new LocationShortcuts(locations, predefinedLocations);
  }
  
  @Test
  public void testUnitedStates() {
    Location us = shortcuts.getCountryInexactMatch("United States", null, null);
    assertEquals("US", us.getEbirdCode());
  }

  @Test
  public void testCaribbeanNetherlandsExactMatches() {
    Location arubaCaribbeanNetherlands = shortcuts.getCountryExactMatch("Caribbean Netherlands", null, "Aruba");
    assertEquals("South America", arubaCaribbeanNetherlands.getParent().getModelName());

    Location sabaCaribbeanNetherlands = shortcuts.getCountryExactMatch("Caribbean Netherlands", null, "Saba");
    assertEquals("West Indies", sabaCaribbeanNetherlands.getParent().getModelName());
  }

  @Test
  public void testCaribbeanNetherlandsInexactMatches() {
    Location arubaCaribbeanNetherlands = shortcuts.getCountryInexactMatch("Caribbean Netherlands", null, "Aruba");
    assertEquals("South America", arubaCaribbeanNetherlands.getParent().getModelName());

    Location sabaCaribbeanNetherlands = shortcuts.getCountryInexactMatch("Caribbean Netherlands", null, "Saba");
    assertEquals("West Indies", sabaCaribbeanNetherlands.getParent().getModelName());
  }

  @Test
  public void testCaribbeanNetherlandsCountryCodes() {
    Location arubaCaribbeanNetherlands = shortcuts.getCountryFromCodeWithExpectedChild("BQ", "Bonaire");
    assertEquals("South America", arubaCaribbeanNetherlands.getParent().getModelName());

    Location sabaCaribbeanNetherlands = shortcuts.getCountryFromCodeWithExpectedChild("BQ", "Saba");
    assertEquals("West Indies", sabaCaribbeanNetherlands.getParent().getModelName());
  }

  @Test
  public void testUnitedKingdom() {
    Location gb = shortcuts.getCountryInexactMatch("United Kingdom", null, null);
    assertEquals("GB", gb.getEbirdCode());
  }

  @Test
  public void testGreatBritain() {
    Location gb = shortcuts.getCountryInexactMatch("Great Britain", null, null);
    assertEquals("GB", gb.getEbirdCode());
  }

  @Test
  public void testEngland() {
    Location england = shortcuts.getCountryInexactMatch("England", null, null);
    assertEquals("GB-ENG", england.getEbirdCode());
  }
}
