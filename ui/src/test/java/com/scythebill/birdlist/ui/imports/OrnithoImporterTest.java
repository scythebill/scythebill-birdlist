/*   
 * Copyright 2016 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;

@RunWith(JUnit4.class)
public class OrnithoImporterTest {
  @Test
  public void verifyOrnithoShortcutsParsed() {
    LocationSet locationSet = ReportSets.newReportSet(new TaxonomyImpl()).getLocations();
    // Make sure the ImmutableMap at the heart of that loads correctly
    new OrnithoImporter.OrnithoStateShortcuts(locationSet, PredefinedLocations.loadAndParse());
  }
}
