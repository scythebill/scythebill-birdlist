/*   
 * Copyright 2014 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.imports;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.xml.sax.SAXException;

import com.google.common.collect.ImmutableSet;
import com.scythebill.birdlist.model.sighting.SightingTaxon;
import com.scythebill.birdlist.model.sighting.SightingTaxons;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.xml.XmlTaxonImport;

@RunWith(JUnit4.class)
public class TaxonResolverTest {
  private static Taxonomy taxonomy;
  
  @BeforeClass
  public static void loadTaxonomy() throws UnsupportedEncodingException, IOException, SAXException {
    XmlTaxonImport importer = new XmlTaxonImport();
    try (InputStream testTaxon = TaxonResolverTest.class.getResourceAsStream("/com/scythebill/birdlist/ui/util/testTaxonomy.xml")) {
      taxonomy = importer.importTaxa(new InputStreamReader(testTaxon, "UTF-8"));
    }
  }
  
  @Test
  public void resolveFeralRockPigeonByCommonName() {
    TaxonResolver taxonResolver = new TaxonResolver(taxonomy);
    TaxonPossibilities possibilities = taxonResolver.map("Rock Pigeon (Feral Pigeon)", null, null);
    SightingTaxon chosen = possibilities.choose(null, taxonomy);
    assertEquals(chosen, SightingTaxons.newSightingTaxon("grColumliv(Fe"));
  }

  @Test
  public void resolveFeralRockPigeonByScientificName() {
    TaxonResolver taxonResolver = new TaxonResolver(taxonomy);
    TaxonPossibilities possibilities = taxonResolver.map(null, "Columba livia", "(Feral Pigeon)");
    SightingTaxon chosen = possibilities.choose(null, taxonomy);
    assertEquals(chosen, SightingTaxons.newSightingTaxon("grColumliv(Fe"));
  }

  @Test
  public void resolveHybridWithHybridNotInName() {
    TaxonResolver taxonResolver = new TaxonResolver(taxonomy);
    TaxonPossibilities possibilities = taxonResolver.map("Lesser x Greater Rhea", null, null);
    SightingTaxon chosen = possibilities.choose(null, taxonomy);
    assertEquals(chosen, SightingTaxons.newHybridTaxon(ImmutableSet.of("spRheaame", "spRheapen")));
  }

  @Test
  public void resolveHybridWithHybridInName() {
    TaxonResolver taxonResolver = new TaxonResolver(taxonomy);
    TaxonPossibilities possibilities = taxonResolver.map("Lesser x Greater Rhea (hybrid)", null, null);
    SightingTaxon chosen = possibilities.choose(null, taxonomy);
    assertEquals(chosen, SightingTaxons.newHybridTaxon(ImmutableSet.of("spRheaame", "spRheapen")));
  }
}
