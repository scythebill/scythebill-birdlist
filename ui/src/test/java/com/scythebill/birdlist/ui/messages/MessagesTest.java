/*   
 * Copyright 2019 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.messages;

import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.scythebill.birdlist.model.sighting.SightingInfo.BreedingBirdCode;
import com.scythebill.birdlist.model.sighting.SightingInfo.SightingStatus;
import com.scythebill.birdlist.ui.messages.Messages.Name;

import static com.google.common.truth.Truth.assertThat;

@RunWith(Parameterized.class)
public class MessagesTest {

  @Parameters(name = "{0}")
  public static Object[] data() {
      return new Object[] {"en", "es", "de"};
  }

  private final Locale locale;
  private Locale savedLocale;

  public MessagesTest(String locale) {
    this.locale = new Locale(locale);
  }
  
  @Before
  public void setLocale() {
    this.savedLocale = Locale.getDefault();
    Locale.setDefault(locale);
  }
  
  @After
  public void restoreLocale() {
    Locale.setDefault(savedLocale);
  }
  
  @Test
  public void allMessagesAreAvailable() {
    for (Messages.Name name : Messages.Name.values()) {
      Assert.assertNotNull(Messages.getMessage(name));
    }
  }
  
  @Test
  @SuppressWarnings("ReturnValueIgnored")
  public void allMessagesHaveEnums() {
    ResourceBundle bundle = ResourceBundle.getBundle("com.scythebill.birdlist.ui.messages.messages");
    for (String key : bundle.keySet()) {
      try {
        Enum.valueOf(Messages.Name.class, key);
      } catch (IllegalArgumentException e) {
        Assert.fail(
            String.format("Key [%s] -> [%s] does not have an enum",
                key,
                bundle.getString(key)));
      }
    }
  }
  
  @Test
  public void locationTypeNamesAreUnique() {
    Set<String> names = new LinkedHashSet<>();
    names.add(Messages.getMessage(Name.LOCATION_TYPE_CITY));
    names.add(Messages.getMessage(Name.LOCATION_TYPE_COUNTRY));
    names.add(Messages.getMessage(Name.LOCATION_TYPE_COUNTY));
    names.add(Messages.getMessage(Name.LOCATION_TYPE_PARK));
    names.add(Messages.getMessage(Name.LOCATION_TYPE_STATE));
    names.add(Messages.getMessage(Name.LOCATION_TYPE_TOWN));
    
    // Don't want accidental translation collisions
    Assert.assertEquals(names.size(), 6);
  }
  
  @Test
  public void sightingStatusNamesAreUniqueAndAllSet() {
    Set<String> names = new LinkedHashSet<>();
    for (SightingStatus status : SightingStatus.values()) {
      names.add(Messages.getText(status));
    }
    
    Assert.assertEquals(names.size(), SightingStatus.values().length);    
  }

  @Test
  public void breedingBirdCodeNamesAreUniqueAndAllSet() {
    Set<String> names = new LinkedHashSet<>();
    for (BreedingBirdCode code : BreedingBirdCode.values()) {
      names.add(Messages.getText(code));
    }
    
    Assert.assertEquals(names.size(),
        BreedingBirdCode.values().length);    
  }
  
  @Test
  public void breedingBirdCodeDescriptionAreUniqueAndAllSet() {
    Set<String> names = new LinkedHashSet<>();
    for (BreedingBirdCode code : BreedingBirdCode.values()) {
      names.add(Messages.getDescription(code));
    }
    
    Assert.assertEquals(names.size(),
        BreedingBirdCode.values().length);    
  }

  @Test
  public void newUserContainsNewObserver() {
    // Message won't make much sense if it doesn't contain
    assertThat(Messages.getMessage(Name.NEW_USER_IMPORT_HELP_MESSAGE))
        .contains(Messages.getMessage(Name.NEW_OBSERVER));   
  }
}
