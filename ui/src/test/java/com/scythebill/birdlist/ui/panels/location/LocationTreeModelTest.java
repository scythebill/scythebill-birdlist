/*   
 * Copyright 2013 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.panels.location;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import javax.swing.tree.TreePath;

import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Predicates;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.PredefinedLocations;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;

/**
 * Tests {@link LocationTreeModel}.
 */
public class LocationTreeModelTest {
  private LocationSet locations;
  private PredefinedLocations predefinedLocations;
  private LocationTreeModel model;

  @Before
  public void startUp() {
    locations = ReportSets.newReportSet(new TaxonomyImpl()).getLocations();
    predefinedLocations = PredefinedLocations.loadAndParse();
    model = new LocationTreeModel(locations, predefinedLocations, Predicates.alwaysTrue());
  }
  
  @Test
  public void predefinedChildrenThenAddedAsATree() {
    Location comoros = locations.getLocationByCode("KM");
    assertFalse(model.isLeaf(comoros));
    assertEquals(3, model.getChildCount(comoros));
    Location anjouan = (Location) model.getChild(comoros, 0);
    assertEquals("Anjouan", anjouan.getModelName());
    assertNull(anjouan.getId());
    assertEquals(comoros, anjouan.getParent());
    assertTrue(model.isLeaf(anjouan));
    
    // Now, create Anjouan
    Location createdAnjouan = predefinedLocations.getPredefinedLocationChild(comoros, "Anjouan")
        .create(locations, comoros);
    assertNotSame(createdAnjouan, anjouan);
    
    // And a child of Anjouan
    Location somewhere = Location.builder()
        .setName("Somewhere")
        .setParent(createdAnjouan)
        .build();
    
    // And add that child, and notify the model of its addition
    locations.ensureAdded(somewhere);
    model.newLocation(getTreePath(comoros), somewhere);
    
    assertFalse(model.isLeaf(comoros));
    assertEquals(3, model.getChildCount(comoros));
    assertSame(createdAnjouan, model.getChild(comoros, 0));
    
    assertFalse(model.isLeaf(createdAnjouan));
  }

  @Test
  public void predefinedChildrenThenAdded() {
    Location comoros = locations.getLocationByCode("KM");
    assertFalse(model.isLeaf(comoros));
    assertEquals(3, model.getChildCount(comoros));
    Location anjouan = (Location) model.getChild(comoros, 0);
    assertEquals("Anjouan", anjouan.getModelName());
    assertNull(anjouan.getId());
    assertEquals(comoros, anjouan.getParent());
    assertTrue(model.isLeaf(anjouan));
    
    // Now, create Anjouan
    Location createdAnjouan = predefinedLocations.getPredefinedLocationChild(comoros, "Anjouan")
        .create(locations, comoros);
    assertNotSame(createdAnjouan, anjouan);

    locations.ensureAdded(createdAnjouan);
    model.newLocation(getTreePath(comoros), createdAnjouan);
    
    assertFalse(model.isLeaf(comoros));
    assertEquals(3, model.getChildCount(comoros));
    assertSame(createdAnjouan, model.getChild(comoros, 0));
    
    assertTrue(model.isLeaf(createdAnjouan));
  }
  
  private TreePath getTreePath(Location location) {
    if (location == null) {
      return new TreePath(model.getRoot());
    }
    
    return getTreePath(location.getParent()).pathByAddingChild(location);
  }
}
