/*   
 * Copyright 2020 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;
import java.io.InputStreamReader;

import org.junit.BeforeClass;
import org.junit.Test;

import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.xml.XmlTaxonImport;
import com.scythebill.birdlist.ui.util.FindSpeciesInText.Results;

public class FindSpeciesInTextTest {

  private static Taxonomy taxonomy;

  @BeforeClass
  public static void loadTaxonomy() throws Exception {
    XmlTaxonImport importer = new XmlTaxonImport();

    try (InputStream testTaxon =
        FindSpeciesInTextTest.class.getResourceAsStream("findSpeciesTaxonomy.xml")) {
      taxonomy = importer.importTaxa(new InputStreamReader(testTaxon, "UTF-8"));
    }
  }
  
  @Test
  public void testWhiteHawk() {
    FindSpeciesInText findSpecies = new FindSpeciesInText(taxonomy, null);
    Results results = findSpecies.search("White Hawk", findSpecies.primarySearch());
    
    assertEquals(1, results.found.size());
    assertEquals(results.found.iterator().next().getCommonName(), "White Hawk");
    // TODO: where did that newline come from?
    assertEquals("", results.remainingText);
  }

  @Test
  public void testWhiteHawkWithText() {
    FindSpeciesInText findSpecies = new FindSpeciesInText(taxonomy, null);
    Results results = findSpecies.search("Some White Hawk Thing", findSpecies.primarySearch());
    
    assertEquals(1, results.found.size());
    assertEquals(results.found.iterator().next().getCommonName(), "White Hawk");
    assertEquals("Some Thing", results.remainingText);
  }

  @Test
  public void testSpizaetusMelanoleucus() {
    FindSpeciesInText findSpecies = new FindSpeciesInText(taxonomy, null);
    Results results = findSpecies.search("Some Spizaetus melanoleucus Thing", findSpecies.primarySearch());
    
    assertEquals(1, results.found.size());
    assertEquals(results.found.iterator().next().getCommonName(), "Black-and-white Hawk-Eagle");
  }

  @Test
  public void testBlackAndWhiteHawkEagle() {
    FindSpeciesInText findSpecies = new FindSpeciesInText(taxonomy, null);
    Results results = findSpecies.search("Some Black-and-white Hawk-Eagle Thing", findSpecies.primarySearch());
    
    assertEquals(1, results.found.size());
    assertEquals(results.found.iterator().next().getCommonName(), "Black-and-white Hawk-Eagle");
  }

  @Test
  public void testMadagascarHarrier() {
    FindSpeciesInText findSpecies = new FindSpeciesInText(taxonomy, null);
    Results results = findSpecies.search("Some Madagascar Harrier Thing", findSpecies.primarySearch());
    
    assertEquals(1, results.found.size());
    assertEquals(results.found.iterator().next().getCommonName(), "Madagascar Harrier");
    assertEquals("Some Thing", results.remainingText);
  }

  @Test
  public void testMadagascarHarrierHawk() {
    FindSpeciesInText findSpecies = new FindSpeciesInText(taxonomy, null);
    Results results = findSpecies.search("Some Madagascar Harrier-Hawk Thing", findSpecies.primarySearch());
    
    assertEquals(1, results.found.size());
    assertEquals(results.found.iterator().next().getCommonName(), "Madagascar Harrier-Hawk");
  }

  @Test
  public void testParenthesesMadagascarHarrierHawk() {
    FindSpeciesInText findSpecies = new FindSpeciesInText(taxonomy, null);
    Results results = findSpecies.search("(Small) (Madagascar) Harrier Hawk (Large)", findSpecies.primarySearch());
    
    assertEquals(1, results.found.size());
    assertEquals(results.found.iterator().next().getCommonName(), "Madagascar Harrier-Hawk");
    assertEquals("(Small) (Large)", results.remainingText);
  }
  
  @Test
  public void testSpeciesFollowedByInitial() {
    FindSpeciesInText findSpecies = new FindSpeciesInText(taxonomy, null);
    Results results = findSpecies.search("Red Kite A",
        findSpecies.primarySearch());
    
    assertEquals(1, results.found.size());
    assertEquals(results.found.iterator().next().getCommonName(), "Red Kite");
    assertEquals(" A", results.remainingText);
  }
}
