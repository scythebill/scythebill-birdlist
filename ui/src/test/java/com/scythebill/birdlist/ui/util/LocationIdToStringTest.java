/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Locale;

import org.junit.Test;

import com.google.common.collect.Iterables;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSets;
import com.scythebill.birdlist.model.taxa.TaxonomyImpl;

public class LocationIdToStringTest {
  @Test
  public void duplicateNamesNotVerbose() {
    // Test wouldn't pass in non-English locale.
    Locale.setDefault(Locale.US);
    LocationSet locations = ReportSets.newReportSet(
            new TaxonomyImpl()).getLocations();
    Collection<Location> unitedStates = locations.getLocationsByModelName("United States");
    assertEquals(3, unitedStates.size());
    
    Location northAmerica = Iterables.getFirst(unitedStates, null);
    LocationIdToString locationIdToString = new LocationIdToString(locations, /*synthetic locations */ null);
    assertEquals("United States (North America)",
        locationIdToString.getPreviewString(northAmerica.getId()));
  }
}
