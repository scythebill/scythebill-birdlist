/*   
 * Copyright 2023 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.Test;

import com.google.common.collect.ImmutableList;
import com.scythebill.birdlist.ui.util.MoreFiles.CommonSuffixResults;

public class MoreFilesTest {

  @Test
  public void commonSuffix() {
    File oldFile = new File("/a/b/c/foo.txt");
    File newFile = new File("/a/d/b/c/foo.txt");
    
    CommonSuffixResults results = MoreFiles.commonSuffix(oldFile, newFile);
    assertEquals("/a", results.oldDirectory().getPath());
    assertEquals("/a/d", results.newDirectory().getPath());
    assertEquals(ImmutableList.of("b", "c", "foo.txt"), results.commonSuffix());
    
    assertEquals(
        new File("/a/d/b/foo/bar.txt"),
        results.replacePrefix(new File("/a/b/foo/bar.txt")));
  }

  @Test
  public void commonSuffixNewIsSuperset() {
    File oldFile = new File("/a/b/c/foo.txt");
    File newFile = new File("/d/a/b/c/foo.txt");
    
    CommonSuffixResults results = MoreFiles.commonSuffix(oldFile, newFile);
    assertEquals("/", results.oldDirectory().getPath());
    assertEquals("/d", results.newDirectory().getPath());
    assertEquals(ImmutableList.of("a", "b", "c", "foo.txt"), results.commonSuffix());
    
    assertEquals(
        new File("/d/a/b/c/bar.txt"),
        results.replacePrefix(new File("/a/b/c/bar.txt")));
  }

  @Test
  public void commonSuffixOldIsSuperset() {
    File oldFile = new File("/d/a/b/c/foo.txt");
    File newFile = new File("/a/b/c/foo.txt");
    
    CommonSuffixResults results = MoreFiles.commonSuffix(oldFile, newFile);
    assertEquals("/d", results.oldDirectory().getPath());
    assertEquals("/", results.newDirectory().getPath());
    assertEquals(ImmutableList.of("a", "b", "c", "foo.txt"), results.commonSuffix());
    
    assertEquals(
        new File("/a/b/c/bar.txt"),
        results.replacePrefix(new File("/d/a/b/c/bar.txt")));
  }
}
