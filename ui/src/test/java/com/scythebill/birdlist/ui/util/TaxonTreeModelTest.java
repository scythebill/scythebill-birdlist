/*   
 * Copyright 2010 Adam Winer
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scythebill.birdlist.ui.util;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import junit.framework.TestCase;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.scythebill.birdlist.model.sighting.Location;
import com.scythebill.birdlist.model.sighting.LocationSet;
import com.scythebill.birdlist.model.sighting.ReportSet;
import com.scythebill.birdlist.model.sighting.Sighting;
import com.scythebill.birdlist.model.taxa.Taxon;
import com.scythebill.birdlist.model.taxa.Taxonomy;
import com.scythebill.birdlist.model.xml.XmlTaxonImport;

public class TaxonTreeModelTest extends TestCase {

  private Taxonomy taxonomy;
  private TaxonTreeModel model;
  private Object root;
  private ReportSet reportSet;
  private Location location;

  @Override
  protected void setUp() throws Exception {
    super.setUp();

    XmlTaxonImport importer = new XmlTaxonImport();

    InputStream testTaxon = getClass().getResourceAsStream("testTaxonomy.xml");
    taxonomy = importer.importTaxa(new InputStreamReader(testTaxon, "UTF-8"));
    testTaxon.close();
    
    model = new TaxonTreeModel(taxonomy, ImmutableList.of(Taxon.Type.species, Taxon.Type.family), Predicates.alwaysTrue());
    root = model.getRoot();
    
    LocationSet.Builder locationBuilder = new LocationSet.Builder();
    location = Location.builder().setName("Somewhere").build();
    locationBuilder.addLocation(location, "somewhere");
    reportSet = new ReportSet(
        locationBuilder.build(),
        Lists.<Sighting>newArrayList(),
        taxonomy);
    model.setReportSet(reportSet);
  }
  
  public void testRoot() {
    assertSame(root, taxonomy.getRoot());
  }
  
  public void testGetChild() {
    Object family = model.getChild(root, 0);
    assertSame(taxonomy.getTaxon("famStr"), family);

    family = model.getChild(root, 1);
    assertSame(taxonomy.getTaxon("famRhe"), family);
    
    Object species = model.getChild(family, 0);
    assertSame(taxonomy.getTaxon("spRheaame"), species);

    species = model.getChild(family, 1);
    assertSame(taxonomy.getTaxon("spRheapen"), species);
  }

  public void testGetChildCount() {
    assertEquals(4, model.getChildCount(root));
    Object family = model.getChild(root, 0);
    assertEquals(1, model.getChildCount(family));
    Object species = model.getChild(family, 0);
    assertEquals(0, model.getChildCount(species));
  }

  public void testGetChildCount_groupsAndSubspecies() {
    model = new TaxonTreeModel(taxonomy, ImmutableList.of(
            Taxon.Type.subspecies, Taxon.Type.group, Taxon.Type.species, Taxon.Type.family),
        Predicates.alwaysTrue());
    assertEquals(4, model.getChildCount(root));
    Object family = model.getChild(root, 0);
    assertEquals(1, model.getChildCount(family));
    Object species = model.getChild(family, 0);
    assertEquals(2, model.getChildCount(species));
    Object group = model.getChild(species, 0);
    assertEquals(4, model.getChildCount(group)); 
  }

  public void testGetIndexOfChild() {
    Object family = model.getChild(root, 0);
    assertEquals(0, model.getIndexOfChild(root, family));

    family = model.getChild(root, 1);
    assertEquals(1, model.getIndexOfChild(root, family));
  }
  
  public void testIsLeaf() {
    assertFalse(model.isLeaf(root));
    Object family = model.getChild(root, 0);
    assertFalse(model.isLeaf(family));
    Object species = model.getChild(family, 0);
    assertTrue(model.isLeaf(species));
  }
  
  public void testSightings() {
    List<Sighting> newSightings = Lists.newArrayList(
            newSighting("spStrutcam"),
            newSighting("grStrutcam[ca"),
            newSighting("sspStrutcam[casyr"));
    reportSet.mutator().adding(newSightings).mutate();
    
    Taxon ostrich = taxonomy.getTaxon("spStrutcam");
    assertEquals(3, model.getChildCount(ostrich));
    
    Sighting sighting = reportSet.getSightings().get(0);
    assertTrue(model.isLeaf(sighting));
    assertEquals(0, model.getChildCount(sighting));
  }

  public void testSightings_groupsAndSubspecies() {
    List<Sighting> newSightings = Lists.newArrayList(
            newSighting("spStrutcam"),
            newSighting("grStrutcam[ca"),
            newSighting("sspStrutcam[casyr"));
    reportSet.mutator().adding(newSightings).mutate();
    model = new TaxonTreeModel(taxonomy, ImmutableList.of(Taxon.Type.subspecies, Taxon.Type.group,
        Taxon.Type.species, Taxon.Type.family), Predicates.alwaysTrue());
    model.setReportSet(reportSet);
    
    Taxon ostrich = taxonomy.getTaxon("spStrutcam");
    // One sighting and 2 groups
    assertEquals(3, model.getChildCount(ostrich));
    List<Object> objects = extractChildren(ostrich);
    Sighting speciesSighting = (Sighting) objects.get(0);
    assertEquals(ostrich.getId(), speciesSighting.getTaxon().getId());
    assertTrue(objects.get(1) instanceof Taxon);
    assertTrue(objects.get(2) instanceof Taxon);
    
    Taxon commonOstrich = taxonomy.getTaxon("grStrutcam[ca");
    assertSame(commonOstrich, objects.get(1));
  }

  public void testGetSightingsOf() {
    List<Sighting> newSightings = Lists.newArrayList(
            newSighting("spStrutcam"),
            newSighting("grStrutcam[ca"),
            newSighting("sspStrutcam[casyr"),
            newSighting("spRheapen"));
    reportSet.mutator().adding(newSightings).mutate();
    
    Taxon ostrich = taxonomy.getTaxon("spStrutcam");
    Taxon commonOstrich = taxonomy.getTaxon("grStrutcam[ca");
    Taxon subOstrich = taxonomy.getTaxon("sspStrutcam[casyr");
    Taxon rhea = taxonomy.getTaxon("spRheapen");
    
    List<Sighting> sightings = model.getSightingsOf(ostrich);
    assertEquals(3, sightings.size());
    sightings = model.getSightingsOf(commonOstrich);
    assertEquals(2, sightings.size());
    sightings = model.getSightingsOf(subOstrich);
    assertEquals(1, sightings.size());
    sightings = model.getSightingsOf(rhea);
    assertEquals(1, sightings.size());
  }
  
  private List<Object> extractChildren(Object node) {
    List<Object> children = Lists.newArrayList();
    for (int i = 0; i < model.getChildCount(node); i++) {
      children.add(model.getChild(node, i));
    }
    return children;
  }

  private Sighting newSighting(String taxonId) {
    Taxon taxon = Preconditions.checkNotNull(taxonomy.getTaxon(taxonId));
    return Sighting.newBuilder()
        .setLocation(location)
        .setTaxon(taxon)
        .build();
  }
}
