/* The following class is modified from an original in the Apache MyFaces project. */
/*
 *  Licensed to the Apache Software Foundation (ASF) under one
 *  or more contributor license agreements.  See the NOTICE file
 *  distributed with this work for additional information
 *  regarding copyright ownership.  The ASF licenses this file
 *  to you under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in compliance
 *  with the License.  You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied.  See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */
package com.scythebill.xml;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Implements a map between a namespace+key and a value.
 * TODO: genericize
 */
public class NamespaceMap implements Cloneable {
  /**
   * Creates a NamespaceMap, using a default size
   * for the number of values per namespace.
   */
  public NamespaceMap() {
    this(16);
  }

  /**
   * Creates a NamespaceMap.
   * @param defaultSize the default size of each per-namespace
   *   storage.
   */
  public NamespaceMap(int defaultSize) {
    _namespaces = new HashMap<String, Map<Object, Object>>();
    _defaultSize = defaultSize;
  }

  /**
   * associates a value with a namespace and a key
   * @param namespace namespace of the value
   * @param key the key to associate the value with
   * @param value the value to associate with the key.
   */
  public void put(String namespace, Object key, Object value) {
    _put(namespace, key, value);
  }

  /**
   * Returns the stored object.
   * @param namespace the namespace to search.
   * @param key the key to search the namespace for.
   * @return null if such a namespace/key does not exist. else returns the
   *  associated value.
   */
  public Object get(String namespace, Object key) {
    Map<Object, Object> map = _namespaces.get(namespace);

    if (map != null)
      return map.get(key);

    return null;
  }

  /**
   * Removes a key from a namespace.
   * @param namespace the namespace to search.
   * @param key the key to search the namespace for.
   * @return the associated value, or null if the namespace/key does not exist.
   */
  public Object remove(String namespace,
      Object key) {
    Map<Object, Object> map = _namespaces.get(namespace);

    if (map != null) {
      Object o = map.remove(key);
      if (map.isEmpty())
        _namespaces.remove(namespace);
      return o;
    }

    return null;
  }

  /**
   * clears all keys from a namespace.
   * @param namespace the namespace to clear.
   */
  public void remove(String namespace) {
    _namespaces.remove(namespace);
  }

  /**
   * clears all bindings for all namespaces.
   */
  public void clear() {
    _namespaces = new HashMap<String, Map<Object, Object>>();
  }

  /**
   * Returns an Iterator over all the namespaces added to the map.
   */
  public Iterator<String> namespaces() {
    return _namespaces.keySet().iterator();
  }

  /**
   * Returns a clone of the NamespaceMap
   */
  @Override public Object clone() {
    NamespaceMap namespaceMap;

    try {
      namespaceMap = (NamespaceMap) super.clone();
    } catch (CloneNotSupportedException e) {
      // this should never happen
      throw new IllegalStateException();
    }

    //
    // clone the key/value pairs
    //
    if (_namespaces != null) {
      Map<String, Map<Object, Object>> newMap = new HashMap<>(_namespaces.size());
      for (Map.Entry<String, Map<Object, Object>> entry : _namespaces
          .entrySet()) {
        newMap.put(entry.getKey(),
            new HashMap<Object, Object>(entry.getValue()));
      }

      namespaceMap._namespaces = newMap;
    }

    return namespaceMap;
  }

  
  // =-=AEW Package-private version, needed by BindableNamespaceMap
  // since it returns the old value.  It'd be nice to change the
  // real version, but backwards compatibility says no.
  //
  // associates a value with a namespace and a key
  // @param namespace namespace of the value
  // @param key the key to associate the value with
  // @param value the value to associate with the key.
  // @return the previous value associated with the namespace/value
  private Object _put(String namespace, Object key, Object value) {
    if (value == null) {
      return remove(namespace, key);
    } else {
      Map<Object, Object> map = _namespaces.get(namespace);

      if (map == null) {
        map = new HashMap<Object, Object>(_defaultSize);
        _namespaces.put(namespace, map);
      }

      return map.put(key, value);
    }
  }

  private Map<String, Map<Object, Object>> _namespaces;
  private int _defaultSize;
}
