/* The following class is modified from an original in the Apache MyFaces project. */
/*
 *  Licensed to the Apache Software Foundation (ASF) under one
 *  or more contributor license agreements.  See the NOTICE file
 *  distributed with this work for additional information
 *  regarding copyright ownership.  The ASF licenses this file
 *  to you under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in compliance
 *  with the License.  You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied.  See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */
package com.scythebill.xml;

import org.xml.sax.Locator;

/**
 * An interface providing contextual information for the current
 * parse state.
 */
public interface ParseContext extends Cloneable {
  /**
   * Returns the default node parser that should be used
   * for a specific element name, given the type of expected object.
   * <p>
   * @param expectedType the Class of the Java object expected for this element
   * @param namespaceURI the namespace of the XML element
   * @param localName the local name of the XML element
   */
  public NodeParser getParser(Class<?> expectedType, String namespaceURI,
      String localName);

  /**
   * Returns the parser manager.
   */
  public ParserManager getParserManager();

  /**
   * Return a SAX Locator object for identifying the document location.
   * @return a locator, or null if none is available
   */
  public Locator getLocator();

  /**
   * Gets a property stored on the context.
   */
  public Object getProperty(Object key);

  /**
   * Stores a property on the context.
   */
  public void setProperty(Object key, Object value);

  /**
   * Clones the ParseContext so that it can be used for a new
   * set of parsing.
   */
  public Object clone();
}
