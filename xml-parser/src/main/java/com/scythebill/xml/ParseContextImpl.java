/* The following class is modified from an original in the Apache MyFaces project. */
/*
 *  Licensed to the Apache Software Foundation (ASF) under one
 *  or more contributor license agreements.  See the NOTICE file
 *  distributed with this work for additional information
 *  regarding copyright ownership.  The ASF licenses this file
 *  to you under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in compliance
 *  with the License.  You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied.  See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */
package com.scythebill.xml;

import java.util.HashMap;

import org.xml.sax.Locator;

/**
 * The base implementation of ParseContext.  Clients can subclass
 * this if they wish to change the behavior, but a more common
 * use is simply creating a ParseContext so you can set properties.
 * <p>
 */
public class ParseContextImpl implements ParseContext, Cloneable {
  /**
   * Creates a ParseContext that uses the provided
   * ParserManager to get NodeParsers.
   */
  public ParseContextImpl() {
  }

  /**
   * Returns the default node parser that shold be used
   * for a specific element name, given the type of expected object.
   * <p>
   * @param expectedType the Class of the Java object expected for this element
   * @param namespaceURI the namespace of the XML element
   * @param localName the local name of the XML element
   */
  @Override
  public NodeParser getParser(Class<?> expectedType, String namespaceURI,
      String localName) {
    return null;
  }

  /**
   * Returns the parser manager.
   */
  @Override
  public ParserManager getParserManager() {
    return null;
  }

  /**
   * Return a SAX Locator object for identifying the document location.
   * @return a locator, or null if none is available
   */
  @Override
  public Locator getLocator() {
    return null;
  }

  /**
   * Gets a property stored on the context.
   */
  @Override
  public Object getProperty(Object key) {
    return _properties.get(key);
  }

  /**

   * Stores a property on the context.
   */
  @Override
  public void setProperty(Object key, Object value) {
    if (value != null) {
      _properties.put(key, value);
    } else {
      _properties.remove(key);
    }
  }

  /**
   *
   */
  @Override @SuppressWarnings("unchecked") public Object clone() {
    try {
      ParseContextImpl copy = (ParseContextImpl) super.clone();
      copy._properties = (HashMap<Object, Object>) _properties.clone();
      return copy;
    } catch (CloneNotSupportedException cnse) {
      return null;
    }
  }

  private HashMap<Object, Object> _properties = new HashMap<Object, Object>();
}
