/* The following class is modified from an original in the Apache MyFaces project. */
/*
 *  Licensed to the Apache Software Foundation (ASF) under one
 *  or more contributor license agreements.  See the NOTICE file
 *  distributed with this work for additional information
 *  regarding copyright ownership.  The ASF licenses this file
 *  to you under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in compliance
 *  with the License.  You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied.  See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */
package com.scythebill.xml;

import org.xml.sax.Locator;

/**
 * Wrapper class that turns a client-created ParseContext into
 * one sufficient for ParseContextImpl
 */
final class ParseContextWrapper implements ParseContext {
  /**
   * Creates a ParseContext that uses the provided
   * ParserManager to get NodeParsers.
   */
  public ParseContextWrapper(ParseContext base, ParserManager manager) {
    _base = base;
    _manager = manager;
  }

  /**
   * Returns the default node parser that shold be used
   * for a specific element name, given the type of expected object.
   * <p>
   * @param expectedType the Class of the Java object expected for this element
   * @param namespaceURI the namespace of the XML element
   * @param localName the local name of the XML element
   */
  @Override
  public NodeParser getParser(Class<?> expectedType, String namespaceURI,
      String localName) {
    return getParserManager().getParser(this, expectedType, namespaceURI,
        localName);
  }

  /**
   * Returns the parser manager.
   */
  @Override
  public ParserManager getParserManager() {
    return _manager;
  }

  /**
   * Return a SAX Locator object for identifying the document location.
   * @return a locator, or null if none is available
   */
  @Override
  public Locator getLocator() {
    return _locator;
  }

  /**
   * Gets a property stored on the context.
   */
  @Override
  public Object getProperty(Object key) {
    return _base.getProperty(key);
  }

  /**
   * Stores a property on the context.
   */
  @Override
  public void setProperty(Object key, Object value) {
    _base.setProperty(key, value);
  }

  /**
   *
   */
  @Override public Object clone() {
    return _base.clone();
  }

  /**
   * Set the locator.  A callback used by TreeBuilder.
   */
  void __setLocator(Locator locator) {
    _locator = locator;
  }

  /**
   * A callback used by TreeBuilder.
   */
  void __startElement() {
    if (!_contextPushed)
      _namespaces.pushContext();
    else
      _contextPushed = false;
  }

  /**
   * A callback used by TreeBuilder.
   */
  void __endElement() {
    _namespaces.popContext();
  }

  /**
   * A callback used by TreeBuilder.
   */
  void __addPrefixMapping(String prefix, String namespaceURI) {
    if (!_contextPushed) {
      _contextPushed = true;
      _namespaces.pushContext();
    }

    _namespaces.declarePrefix(prefix, namespaceURI);
  }

  ParseContext __getWrappedContext() {
    return _base;
  }

  private final ParseContext _base;
  private final ParserManager _manager;

  // A SAX helper object that keeps track of namespace prefixes
  private NamespaceSupport _namespaces = new NamespaceSupport();

  // Boolean to keep track of whether we've called "pushContext()" yet.
  private boolean _contextPushed;

  // The SAX locator
  private Locator _locator;
}
