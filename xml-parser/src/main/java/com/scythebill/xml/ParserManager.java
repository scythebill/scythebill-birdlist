/* The following class is modified from an original in the Apache MyFaces project. */
/*
 *  Licensed to the Apache Software Foundation (ASF) under one
 *  or more contributor license agreements.  See the NOTICE file
 *  distributed with this work for additional information
 *  regarding copyright ownership.  The ASF licenses this file
 *  to you under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in compliance
 *  with the License.  You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied.  See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */
package com.scythebill.xml;

/**
 * ParserManager maintains a table of ParserFactories, keyed
 * by return-type Class.  Clients can use a single ParserManager, or
 * create their own.  By default, none of the ParserManagers
 * have any registered ParserFactories.
 */

public class ParserManager implements Cloneable {
  /**
   * Creates a new, empty ParserManager.
   */
  public ParserManager() {
  }

  /**
   * Returns a globally shared instance of ParserManager.
   */
  static public ParserManager getDefaultParserManager() {
    return _sDefaultInstance;
  }

  /**
   * Utility method for retrieving a NodeParser.
   */
  final public NodeParser getParser(ParseContext context,
      Class<?> expectedType, String namespaceURI, String localName) {
    ParserFactory factory = getFactory(expectedType, namespaceURI);
    if (factory == null)
      return null;

    return factory.getParser(context, namespaceURI, localName);
  }

  /**
   * Gets the factory registered for the namespace.
   */
  public ParserFactory getFactory(Class<?> expectedType, String namespaceURI) {
    return (ParserFactory) _factories.get(namespaceURI, expectedType);
  }

  /**
   * Registers a factory for a type and namespace.
   */
  synchronized public void registerFactory(Class<?> expectedType,
      String namespaceURI, ParserFactory factory) {
    _unshareState();
    _factories.put(namespaceURI, expectedType, factory);
  }

  /**
   * Unregisters a factory for a type and namespace.
   */
  synchronized public void unregisterFactory(Class<?> expectedType,
      String namespaceURI) {
    _unshareState();
    _factories.remove(namespaceURI, expectedType);
  }

  /**
   * Makes a deep copy of the ParserManager.
   */
  @Override synchronized public Object clone() {
    try {
      // it is very important that we set this to true, before we clone:
      _sharedState = true;

      // Optimize to lazily clone the contents
      ParserManager pm = (ParserManager) super.clone();
      return pm;
    } catch (CloneNotSupportedException cnse) {
      // Shouldn't happen
      throw new IllegalStateException();
    }
  }

  // Unshare any parts of the state that have been shared.
  // Must be called by synchronized functions!
  private synchronized void _unshareState() {
    if (_sharedState) {
      _factories = (NamespaceMap) _factories.clone();
      _sharedState = false;
    }
  }

  private NamespaceMap _factories = new NamespaceMap();

  // If true, our state is shared with another parser manager - so
  // make a copy before mutating.  This value can easily have
  // false positives;  for instance, we don't store _which_ PM
  // we're sharing state with, so if both mutate their state,
  // then both will make copies even though only one really needed to.
  private boolean _sharedState = false;

  static private final ParserManager _sDefaultInstance = new ParserManager();
}
