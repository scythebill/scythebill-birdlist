/* The following class is modified from an original in the Apache MyFaces project. */
/*
 *  Licensed to the Apache Software Foundation (ASF) under one
 *  or more contributor license agreements.  See the NOTICE file
 *  distributed with this work for additional information
 *  regarding copyright ownership.  The ASF licenses this file
 *  to you under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in compliance
 *  with the License.  You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an
 *  "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied.  See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 */
package com.scythebill.xml;

import java.io.IOException;

import java.util.Stack;

import java.util.logging.Level;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;

import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

/**
 * Class responsible for building a tree of objects from
 * an XML stack.  TreeBuilders are thread safe, and so can
 * be used from multiple threads simultaneously.
 */
public class TreeBuilder<T> {
  /**
   * Creates a TreeBuilder with no ParserManager or root class.
   * Such a TreeBuilder can only be used with NodeParsers that never
   * try to use {@link ParseContext#getParser ParseContext.getParser()}.
   */
  public TreeBuilder() {
    this(null, null);
  }

  /**
   * Creates a TreeBuilder using a given ParserManager.
   * @param manager the ParserManager to use
   * @param rootClass the desired type of object to return as the root
   */
  public TreeBuilder(ParserManager manager, Class<T> rootClass) {
    // Allow a null ParserManager.  It may sound wacky,
    // but it's sometimes easiest to always explicitly state
    // which parser you want to use, and never rely on type information.
    if (manager == null)
      _manager = new ParserManager();
    else
      // Clone the ParserManager, so changes to it will not
      // "escape" - see bug 1852367
      _manager = (ParserManager) manager.clone();

    _rootClass = rootClass;
  }

  public void setEntityResolver(EntityResolver resolver) {
    _entityResolver = resolver;
  }

  public EntityResolver getEntityResolver() {
    return _entityResolver;
  }

  /**
   * Parses the document.
   * @param source a SAX input source
   * @return an object that is an instance of the desired class
   */
  public T parse(InputSource source) throws IOException, SAXException {
    ParseContextImpl context = new ParseContextImpl();
    return parse(source, context);
  }

  /**
   * Parses the document.
   * @param source a SAX input source
   * @param context a parsing context
   * @return an object that is an instance of the desired class
   */
  public T parse(InputSource source, ParseContext context) throws IOException,
      SAXException {
    return parse(source, context, null);
  }

  public T parse(InputSource source, NodeParser rootParser) throws IOException,
      SAXException {
    return parse(source, new ParseContextImpl(), rootParser);
  }

  /**
   * Parses the document.
   * @param provider an implementation of the XMLProvider interface
   * @param source a SAX input source
   * @param context a parsing context
   * @param rootParser the root parser to start with;  if null,
   *         a root parser will be derived based on the rootClass
   *         requested in the constructor.
   * @return an object that is the result of parsing.
   */
  @SuppressWarnings("unchecked") public T parse(InputSource source,
      ParseContext context, NodeParser rootParser) throws IOException,
      SAXException {
    if ((_rootClass == null) && (rootParser == null))
      throw new NullPointerException("Both rootClass and rootParser are null");

    ParseContextWrapper wrappedContext = new ParseContextWrapper(context,
        _manager);
    Handler handler = new Handler(wrappedContext, rootParser);

    XMLReader reader = getXMLReader();

    // Force these two features to be set the way we want.
    // These are the default values, but we'd crash and burn
    // if they're wrong.
    reader.setFeature("http://xml.org/sax/features/namespaces", true);
    reader.setFeature("http://xml.org/sax/features/namespace-prefixes", false);

    reader.setContentHandler(handler);
    reader.setErrorHandler(handler);
    if (getEntityResolver() != null)
      reader.setEntityResolver(getEntityResolver());

    reader.parse(source);

    return (T) handler.getRoot();
  }

  protected XMLReader getXMLReader() throws SAXException {
    try {
      SAXParserFactory factory = SAXParserFactory.newInstance();
      factory.setNamespaceAware(true);
      return factory.newSAXParser().getXMLReader();
    } catch (ParserConfigurationException pce) {
      _LOG.log(Level.SEVERE, null, pce);
    }

    return null;
  }

  // Workhorse class;  contains all the parse-specific state
  private class Handler implements ContentHandler, ErrorHandler {
    public Handler(ParseContextWrapper context, NodeParser rootParser) {
      _context = context;
      _parsers = new Stack<NodeParser>();
      _rootParser = rootParser;
    }

    /**
     * Returns the root object;  can only be called after a parse
     * has finished.  The root object will be an instance of
     * the requested root class.
     */
    public Object getRoot() {
      if (!_parsers.isEmpty())
        throw new IllegalStateException();

      return _root;
    }

    /**
     * Begin the scope of a prefix-URI namespace mapping.
     */
    @Override
    public void startPrefixMapping(String prefix, String uri) {
      _context.__addPrefixMapping(prefix, uri);
    }

    @Override
    public void endPrefixMapping(String prefix) {
    }

    /**
     * Receive notification of the beginning of a document.
     */
    @Override
    public void startDocument() {
      _parsers.setSize(0);
      _root = null;
    }

    /**
     * Receive notification of the end of a document.
     */
    @Override
    public void endDocument() {
      assert (_parsers.isEmpty());
    }

    /**
     * Receive notification of the beginning of an element.
     */
    @Override
    public void startElement(String namespaceURI, String localName,
        String rawName, Attributes atts) throws SAXException {
      _context.__startElement();

      NodeParser parser = _getCurrentParser();
      if (parser == null) {
        // No parser yet - we're starting at the top.  Get
        // a default parser
        if (_rootParser != null)
          parser = _rootParser;
        else
          parser = _context.getParser(_rootClass, namespaceURI, localName);

        if (parser == null) {
          String message = "No " + _rootClass.getName()
              + " parser registered for top element;"
              + "check your namespace declaration." + "   Namespace: "
              + namespaceURI + "\n" + "   Local name: " + localName + ")";

          SAXParseException e = new SAXParseException(message, _context
              .getLocator());
          fatalError(e);
          throw e;
        }

        _pushParser(parser);
      } else {
        // Notify the current parser that we're starting a child element.
        // If it returns null, we'll continue talking to the same
        // parser, and only call startChildElement() and endChildElement().
        NodeParser newParser;
        try {
          newParser = parser.startChildElement(_context, namespaceURI,
              localName, atts);
        } catch (SAXParseException e) {
          // Log the error, and bail
          fatalError(e);
          throw e;
        }

        // The NodeParser will continue handling parsing itself.
        // Note that on the stack, and bail.
        if (newParser == parser) {
          _pushParser(null);
          return;
        } else {
          if (newParser == null) {
            ParseErrorUtils.log(_context, "<" + localName + ">"
                + " is not an understood element.\n"
                + "This sometimes means the element's " + "namespace ("
                + namespaceURI + ") is set "
                + "incorrectly.  This may also be an "
                + "issue with the syntax of its parent " + "element.", null,
                Level.WARNING, _LOG);
            newParser = BaseNodeParser.getIgnoreParser();
          }

          _pushParser(newParser);
          parser = newParser;
        }
      }

      // Start the (non-child) element
      try {
        parser.startElement(_context, namespaceURI, localName, atts);
      } catch (SAXParseException e) {
        // Log the error, and bail
        fatalError(e);
        throw e;
      }
    }

    /**
     * Receive notification of the end of an element.
     */
    @Override
    public void endElement(String namespaceURI, String localName, String rawName)
        throws SAXException {
      NodeParser oldParser = _popParser();
      NodeParser parser = _getCurrentParser();

      // If the "old parser" was null, then that the "parser"
      // is handling its own children.  Otherwise, we need to
      // terminate the element, and pass the Java object the "old"
      // parser produced to "addCompletedChild()" on the parser
      if (oldParser != null) {
        Object child;

        try {
          child = oldParser.endElement(_context, namespaceURI, localName);
        } catch (SAXParseException e) {
          // Log the error, and bail
          fatalError(e);
          throw e;
        }

        if (parser == null) {
          // If there's no current parser, we must be at
          // the end.  Store the root, and be done.
          assert (_parsers.isEmpty());
          _root = child;
        } else {
          try {
            parser.addCompletedChild(_context, namespaceURI, localName, child);
          } catch (SAXParseException e) {
            // Log the error, and bail
            fatalError(e);
            throw e;
          }
        }
      }

      if ((parser != null) && (oldParser == null)) {
        try {
          parser.endChildElement(_context, namespaceURI, localName);
        } catch (SAXParseException e) {
          // Log the error, and bail
          fatalError(e);
          throw e;
        }
      }

      _context.__endElement();
    }

    /**
     * Receive notification of character data.
     */
    @Override
    public void characters(char[] text, int start, int length)
        throws SAXException {
      try {
        NodeParser parser = _getCurrentParser();
        if (parser != null) {
          parser.addText(_context, text, start, length);
        }
      } catch (SAXParseException e) {
        // Log the error, and bail
        fatalError(e);
        throw e;
      }
    }

    @Override
    public void setDocumentLocator(Locator locator) {
      _context.__setLocator(locator);
    }

    /**
     * Receive notification of ignorable whitespace in element content.
     */
    @Override
    public void ignorableWhitespace(char[] text, int start, int length)
        throws SAXException {
      try {
        NodeParser parser = _getCurrentParser();
        if (parser != null) {
          parser.addWhitespace(_context, text, start, length);
        }
      } catch (SAXParseException e) {
        // Log the error, and bail
        fatalError(e);
        throw e;
      }
    }

    /**
     * Receive notification of a processing instruction.
     */
    @Override
    public void processingInstruction(String target, String data) {
    }

    /**
     * Receive notification of a skipped entity.
     */
    @Override
    public void skippedEntity(String name) {
    }

    //
    // ErrorHandler implementation
    //

    /**
     * Receive notification of a warning.
     */
    @Override
    public void warning(SAXParseException exception) throws SAXException {
      _logError(exception, Level.INFO);
    }

    /**
     * Receive notification of a recoverable error.
     */
    @Override
    public void error(SAXParseException exception) throws SAXException {
      _logError(exception, Level.WARNING);
    }

    /**
     * Receive notification of a fatal error.
     */
    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
      if (!_fatalError) {
        _fatalError = true;
        _logError(exception, Level.SEVERE);
      }
    }

    private void _logError(SAXParseException exception, Level verbosity) {
      ParseErrorUtils
          .log(_context, null, exception, exception.getLineNumber(), exception
              .getColumnNumber(), exception.getSystemId(), verbosity, _LOG);
    }

    private void _pushParser(NodeParser parser) {
      if (parser == null) {
        _parsers.push(null);
      } else {
        _parsers.push(parser);
        _current = parser;
      }

      assert (_current == _getLastNonNullParser());
    }

    private NodeParser _popParser() {
      NodeParser entry = _parsers.pop();
      if (entry != null) {
        _current = _getLastNonNullParser();
        return entry;
      } else {
        return null;
      }
    }

    private NodeParser _getCurrentParser() {
      assert (_current == _getLastNonNullParser());
      return _current;
    }

    private NodeParser _getLastNonNullStackEntry() {
      for (int i = _parsers.size() - 1; i >= 0; i--) {
        NodeParser entry = _parsers.elementAt(i);
        if (entry != null) {
          return entry;
        }
      }

      return null;
    }

    private NodeParser _getLastNonNullParser() {
      NodeParser entry = _getLastNonNullStackEntry();
      if (entry != null)
        return entry;

      return null;
    }

    private ParseContextWrapper _context;

    // A stack of parsers (stored in StackEntries);  null at any position means the
    // parser didn't change
    // -= Simon Lessard =- 
    // TODO:  Check if synchronization is required since Stack is bad.
    private Stack<NodeParser> _parsers;

    // The current parser.  This will always equal the last
    // non-null parser
    private NodeParser _current;

    // The root
    private Object _root;

    // Has a fatal error already been logged?  Used to work around
    // the Oracle parser's tendency to fire the same fatal error
    // multiple times
    private boolean _fatalError;

    // The parser to use as the root of the tree
    private final NodeParser _rootParser;
  }

  final Class<?> _rootClass;
  private EntityResolver _entityResolver;
  final private ParserManager _manager;
  private static final Logger _LOG = Logger.getLogger(TreeBuilder.class
      .getName());
}
